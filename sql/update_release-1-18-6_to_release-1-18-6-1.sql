ALTER TABLE `cm_collection_point` 
ADD `workingDays` VARCHAR( 50 ) NULL DEFAULT 'Monday to Friday',
ADD `cp_fax` VARCHAR( 50 ) NULL ,
ADD `sameday` ENUM( 'Y', 'N' ) NULL DEFAULT 'N',
ADD `openingTime` TIME NULL DEFAULT '08:30',
ADD `closingTime` TIME NULL DEFAULT '16:30',
ADD `operatesWeekend` ENUM( 'Y', 'N' ) NULL DEFAULT 'N',
ADD `weekendWorkingDays` VARCHAR( 50 ) NULL ,
ADD `weekendOpeningTime` TIME NULL ,
ADD `weekendClosingTime` TIME NULL ;


CREATE TABLE `Campaign_Type` (
  `id` bigint(20) NOT NULL auto_increment,
  `heading` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL,
  `lastChange` datetime NOT NULL default '0000-00-00 00:00:00',
  `changeBy` varchar(35) NOT NULL,
  `isEnable` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`id`)
);


CREATE TABLE `configure_commission` (
  `confID` bigint(20) NOT NULL auto_increment,
  `commRate` float NOT NULL,
  `commType` varchar(35) NOT NULL,
  `limitValue` float NOT NULL,
  `lastModified` datetime NOT NULL,
  `modified_by` varchar(35) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`confID`)
);