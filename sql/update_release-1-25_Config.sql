INSERT INTO `config` (`id`, `varName`, `value`, `oldValue`, `description`, `detailDesc`, `isFlag`, `client`) VALUES (8, 'CONFIG_NO_OF_DAYS', '30', '0', 'No Of Days', '', 'N', 'express'),
(7, 'CONFIG_COMPLIANCE_MAX_AMOUNT', '10000', '0', 'Accumulative upper threshold', '', 'N', 'express'),
(6, 'CONFIG_AMOUNT_CONTROLLER', '1', '0', 'Amount Controller for AML report', '', 'N', 'express'),
(5, 'CONFIG_COMPLIANCE_MIN_AMOUNT', '800', '0', 'Compliance Amount limit', 'Compliance Amount limit for the pop up on create transaction page.', 'N', 'express');