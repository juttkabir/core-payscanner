-- 
-- Table structure for table `admin`
-- 

CREATE TABLE `admin` (
  `userID` bigint(20) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL default '',
  `password` varchar(16) NOT NULL default '',
  `changedPwd` date NOT NULL default '0000-00-00',
  `name` varchar(25) NOT NULL default '',
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `isMain` enum('N','Y') NOT NULL default 'N',
  `email` varchar(100) NOT NULL default '',
  `role` text NOT NULL,
  `rights` varchar(255) NOT NULL default '',
  `adminType` enum('Supper','System','Admin','Agent','Admin Manager','Call','COLLECTOR','Branch Manager') NOT NULL default 'Agent',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `parentID` bigint(20) unsigned NOT NULL default '0',
  `agentType` enum('Supper','Sub') NOT NULL default 'Supper',
  `isCorrespondent` enum('Y','N','ONLY') NOT NULL default 'N',
  `IDAcountry` varchar(255) NOT NULL default '',
  `agentNumber` int(10) unsigned NOT NULL default '0',
  `subagentNum` int(10) unsigned NOT NULL default '0',
  `agentCompany` varchar(255) NOT NULL default '',
  `agentContactPerson` varchar(100) NOT NULL default '',
  `agentAddress` varchar(255) NOT NULL default '',
  `agentAddress2` varchar(255) NOT NULL default '',
  `agentCity` varchar(100) NOT NULL default '',
  `agentZip` varchar(16) NOT NULL default '',
  `agentCountry` varchar(100) NOT NULL default '',
  `agentCountryCode` char(3) NOT NULL default '',
  `agentPhone` varchar(32) NOT NULL default '',
  `mobile` varchar(32) NOT NULL,
  `agentFax` varchar(32) NOT NULL default '',
  `agentURL` varchar(255) NOT NULL default '',
  `agentMSBNumber` varchar(255) NOT NULL default '',
  `agentMCBExpiry` datetime NOT NULL default '0000-00-00 00:00:00',
  `agentCompRegNumber` varchar(100) NOT NULL default '',
  `agentCompDirector` varchar(100) NOT NULL default '',
  `designation` varchar(255) NOT NULL,
  `agentDirectorAdd` varchar(255) NOT NULL default '',
  `agentProofID` varchar(100) NOT NULL default '',
  `agentIDExpiry` datetime NOT NULL default '0000-00-00 00:00:00',
  `agentDocumentProvided` varchar(100) NOT NULL default '',
  `agentBank` varchar(255) NOT NULL default '',
  `agentAccountName` varchar(255) NOT NULL default '',
  `agentAccounNumber` varchar(255) NOT NULL default '',
  `agentBranchCode` varchar(255) NOT NULL default '',
  `agentAccountType` varchar(255) NOT NULL default '',
  `agentCurrency` varchar(255) NOT NULL default '',
  `agentAccountLimit` float NOT NULL default '0',
  `limitUsed` float NOT NULL default '0',
  `commPackage` enum('001','002','003') NOT NULL default '001',
  `agentCommission` float NOT NULL default '0',
  `agentStatus` enum('New','Active','Disabled','Suspended') NOT NULL default 'New',
  `suspensionReason` text NOT NULL,
  `suspendedBy` varchar(32) NOT NULL default '',
  `activatedBy` varchar(32) NOT NULL default '',
  `disableReason` text NOT NULL,
  `disabledBy` varchar(32) NOT NULL default '',
  `logo` varchar(30) NOT NULL default '',
  `accessFromIP` varchar(255) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `authorizedFor` varchar(200) NOT NULL default '',
  `balance` double NOT NULL default '0',
  `postCode` varchar(10) default NULL,
  `payinBook` varchar(25) NOT NULL default '',
  UNIQUE KEY `userID` (`userID`),
  KEY `agentType` (`agentType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=100180 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_account`
-- 

CREATE TABLE `agent_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` bigint(20) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(60) NOT NULL default '',
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=476 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_logfile`
-- 

CREATE TABLE `agent_logfile` (
  `logID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `agentID` bigint(20) NOT NULL default '0',
  `transID` bigint(20) NOT NULL default '0',
  `benID` bigint(20) NOT NULL default '0',
  `compID` bigint(200) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `remarks` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`logID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agents_customer_account`
-- 

CREATE TABLE `agents_customer_account` (
  `caID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `Date` date NOT NULL default '0000-00-00',
  `tranRefNo` varchar(30) NOT NULL default '',
  `payment_mode` varchar(50) NOT NULL default '0',
  `Type` varchar(10) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  `modifiedBy` varchar(35) NOT NULL,
  PRIMARY KEY  (`caID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `amended_transactions`
-- 

CREATE TABLE `amended_transactions` (
  `amendID` bigint(20) unsigned NOT NULL auto_increment,
  `transID` bigint(20) unsigned NOT NULL,
  `transType` enum('Pick up','Bank Transfer','Home Delivery','') NOT NULL default '',
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `benID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `benAgentID` bigint(20) NOT NULL default '0',
  `exchangeID` bigint(20) unsigned NOT NULL default '0',
  `refNumber` varchar(32) NOT NULL default '',
  `refNumberIM` varchar(50) NOT NULL default '',
  `transAmount` float NOT NULL default '0',
  `exchangeRate` float NOT NULL default '0',
  `localAmount` double NOT NULL default '0',
  `IMFee` float NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `totalAmount` float NOT NULL default '0',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `fundSources` varchar(100) NOT NULL default '',
  `moneyPaid` varchar(100) NOT NULL default '',
  `Declaration` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `verifiedBy` varchar(30) NOT NULL default '',
  `authorisedBy` varchar(30) NOT NULL default '',
  `cancelledBy` varchar(30) NOT NULL default '',
  `recalledBy` varchar(20) NOT NULL default '',
  `isSent` enum('Y','N') NOT NULL default 'N',
  `trackingNum` varchar(30) NOT NULL default '',
  `trans4Country` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `authoriseDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryOutDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rejectDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `failedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspeciousDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `recalledDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fromCountry` varchar(30) NOT NULL default '',
  `toCountry` varchar(30) NOT NULL default '',
  `currencyFrom` varchar(10) NOT NULL default '',
  `currencyTo` varchar(10) NOT NULL default '',
  `custAgentParentID` int(11) NOT NULL default '0',
  `benAgentParentID` int(11) NOT NULL default '0',
  `is_exported` varchar(10) NOT NULL default '',
  `PINCODE` int(5) NOT NULL default '0',
  `createdBy` varchar(10) NOT NULL default '',
  `collectionPointID` bigint(20) NOT NULL default '0',
  `transRefID` varchar(30) NOT NULL default '',
  `AgentComm` float NOT NULL default '0',
  `distributorComm` float NOT NULL default '0',
  `CommType` varchar(30) NOT NULL default '',
  `other_pur` varchar(35) NOT NULL default '',
  `admincharges` varchar(30) NOT NULL default '',
  `cashCharges` int(11) NOT NULL default '0',
  `holdedBy` varchar(20) NOT NULL default '',
  `refundFee` varchar(10) NOT NULL default '',
  `question` varchar(100) NOT NULL default '',
  `answer` varchar(200) NOT NULL default '',
  `tip` text,
  `outCurrCharges` float NOT NULL default '0',
  `discountRequest` varchar(255) NOT NULL,
  `discountType` varchar(255) NOT NULL,
  `discounted_amount` float NOT NULL default '0',
  `deliveredBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(40) NOT NULL,
  `modificationDate` datetime NOT NULL,
  `holdDate` datetime NOT NULL,
  `verificationDate` datetime NOT NULL,
  `unholdBy` varchar(40) NOT NULL,
  `unholdDate` datetime NOT NULL,
  PRIMARY KEY  (`amendID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bank-transfer-country`
-- 

CREATE TABLE `bank-transfer-country` (
  `id` int(20) NOT NULL auto_increment,
  `country` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bankDetails`
-- 

CREATE TABLE `bankDetails` (
  `bankID` int(11) NOT NULL auto_increment,
  `benID` int(11) NOT NULL default '0',
  `transID` int(11) NOT NULL default '0',
  `bankName` varchar(150) NOT NULL default '',
  `accNo` varchar(100) NOT NULL default '',
  `branchCode` varchar(100) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `ABACPF` varchar(100) NOT NULL default '',
  `IBAN` varchar(100) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`bankID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bank_account`
-- 

CREATE TABLE `bank_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `bankID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` bigint(20) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=538 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `barclayspayments`
-- 

CREATE TABLE `barclayspayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `tlaCode` varchar(5) NOT NULL default '',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `barclayspayments_customermoudule`
-- 

CREATE TABLE `barclayspayments_customermoudule` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `tlaCode` varchar(5) NOT NULL default '',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=162 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `beneficiary`
-- 

CREATE TABLE `beneficiary` (
  `benID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `created` date NOT NULL default '0000-00-00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `benAccount` varchar(32) NOT NULL default '',
  `Password` varchar(32) NOT NULL default '',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `City` varchar(100) NOT NULL default '',
  `State` varchar(50) NOT NULL default '',
  `Zip` varchar(16) NOT NULL default '',
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `IDType` varchar(100) NOT NULL default '',
  `IDNumber` varchar(32) NOT NULL default '',
  `isTransfered` enum('Y','N') NOT NULL default 'N',
  `agentID` int(11) NOT NULL default '0',
  `other_title` varchar(30) NOT NULL default '',
  `otherId` varchar(30) NOT NULL default '',
  `otherId_name` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`benID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cities`
-- 

CREATE TABLE `cities` (
  `country` varchar(50) NOT NULL default '',
  `city` varchar(255) NOT NULL default '',
  `countryCode` varchar(4) NOT NULL default '',
  `Currency` char(3) NOT NULL default '',
  `currDesc` varchar(32) NOT NULL default '',
  `deliveryTime` varchar(100) NOT NULL default '',
  `currencyRec` varchar(255) NOT NULL default '',
  `serviceAvailable` varchar(255) NOT NULL default '',
  `isoCode` char(2) NOT NULL default '',
  `countryType` int(11) NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `outCurrCharges` float NOT NULL default '0',
  KEY `country` (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_bankdetails`
-- 

CREATE TABLE `cm_bankdetails` (
  `bankID` int(11) NOT NULL auto_increment,
  `benID` int(11) NOT NULL default '0',
  `transID` int(11) NOT NULL default '0',
  `bankName` varchar(150) NOT NULL default '',
  `BankCode` varchar(50) NOT NULL default '',
  `accNo` varchar(100) NOT NULL default '',
  `branchCode` varchar(100) NOT NULL default '',
  `BranchName` varchar(200) NOT NULL default '',
  `BranchCity` varchar(100) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `ABACPF` varchar(100) NOT NULL default '',
  `IBAN` varchar(100) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `BankStatus` varchar(40) NOT NULL default '',
  `BankType` varchar(40) NOT NULL default '',
  `DepositMethod` varchar(40) NOT NULL default '',
  `StatmentID` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`bankID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_beneficiary`
-- 

CREATE TABLE `cm_beneficiary` (
  `benID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `created` date NOT NULL default '0000-00-00',
  `DateDisabled` date NOT NULL default '0000-00-00',
  `DateLastPaid` date NOT NULL default '0000-00-00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `benAccount` varchar(32) NOT NULL default '',
  `Password` varchar(32) NOT NULL default '',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `address2` varchar(255) NOT NULL default '',
  `City` varchar(100) NOT NULL default '',
  `State` tinyint(50) NOT NULL default '0',
  `Zip` varchar(16) NOT NULL default '',
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `IDType` varchar(100) NOT NULL default '',
  `NICNumber` varchar(32) NOT NULL default '',
  `oldNICNumber` varchar(32) NOT NULL default '',
  `bankDetailsID` int(20) NOT NULL default '0',
  `transType` varchar(25) NOT NULL default '',
  `loginID` bigint(20) NOT NULL default '0',
  `editedBy` varchar(30) NOT NULL default '',
  `editDate` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`benID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_collection_point`
-- 

CREATE TABLE `cm_collection_point` (
  `cp_id` int(20) NOT NULL auto_increment,
  `isAdmin` varchar(10) NOT NULL default 'No',
  `cp_corresspondent_name` varchar(50) default NULL,
  `cp_ria_branch_code` varchar(20) default NULL,
  `cp_ida_id` varchar(20) default NULL,
  `cp_branch_name` varchar(70) default NULL,
  `cp_branch_no` varchar(20) default NULL,
  `cp_branch_address` varchar(255) default NULL,
  `cp_city` varchar(50) default NULL,
  `cp_state` varchar(50) default NULL,
  `cp_country` varchar(50) default NULL,
  `cp_phone` varchar(50) default NULL,
  `cp_active` varchar(10) NOT NULL default '',
  `disabledDate` datetime NOT NULL,
  `disabledBy` varchar(100) NOT NULL,
  `disableReason` varchar(256) NOT NULL,
  PRIMARY KEY  (`cp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1256 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_credit_trans`
-- 

CREATE TABLE `cm_credit_trans` (
  `cID` int(20) NOT NULL auto_increment,
  `transID` varchar(20) NOT NULL default '0',
  `cardType` varchar(15) NOT NULL default '',
  `cardNo` varchar(16) NOT NULL default '',
  `expiryDate` varchar(6) default NULL,
  `Card_CVV` varchar(4) default NULL,
  `TransactionTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `AttemptCode` varchar(4) default NULL,
  `PQTransID` varchar(17) default NULL,
  `ApprovalCode` varchar(6) default NULL,
  `ResultCode` varchar(4) default NULL,
  `ResultText` varchar(100) default NULL,
  `IPCountryCode` char(3) default NULL,
  `SenderOFAC` varchar(25) default NULL,
  `SenderOFACRecord` varchar(255) default NULL,
  `BenOFAC` varchar(25) default NULL,
  `BenOFACRecord` varchar(255) default NULL,
  `First_Name` varchar(25) NOT NULL default '',
  `Last_Name` varchar(25) NOT NULL default '',
  `Address_1` varchar(30) NOT NULL default '',
  `Address_2` varchar(30) NOT NULL default '',
  `City` varchar(25) NOT NULL default '',
  `State` char(2) NOT NULL default '',
  `Postal_Code` varchar(9) NOT NULL default '',
  `Country_Code` char(3) NOT NULL default '',
  `IP_Address` varchar(15) NOT NULL default '',
  `Telephone` varchar(15) NOT NULL default '',
  `Email` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`cID`),
  KEY `country` (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_cust_credit_card`
-- 

CREATE TABLE `cm_cust_credit_card` (
  `cID` int(20) NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `cardNo` varchar(20) NOT NULL default '0',
  `cardName` varchar(40) NOT NULL default '',
  `cardCVV` varchar(4) NOT NULL default '',
  `expiryDate` varchar(10) NOT NULL default '',
  `firstName` varchar(20) NOT NULL default '0000-00-00',
  `lastName` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `address` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `city` varchar(40) default NULL,
  `state` varchar(40) default NULL,
  `country` varchar(40) default NULL,
  `postalCode` varchar(20) default NULL,
  PRIMARY KEY  (`cID`),
  KEY `country` (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_customer`
-- 

CREATE TABLE `cm_customer` (
  `c_id` int(6) unsigned NOT NULL auto_increment,
  `c_name` varchar(50) default NULL,
  `Title` varchar(10) NOT NULL default '',
  `FirstName` varchar(20) NOT NULL default '',
  `MiddleName` varchar(20) NOT NULL default '',
  `LastName` varchar(20) NOT NULL default '',
  `c_pass` varchar(50) default NULL,
  `c_date` date default NULL,
  `c_email` varchar(30) default NULL,
  `c_phone` varchar(40) default NULL,
  `c_mobile` varchar(40) default NULL,
  `c_address` varchar(255) default NULL,
  `c_address2` varchar(255) default NULL,
  `c_country` varchar(30) default NULL,
  `c_state` varchar(30) default NULL,
  `c_city` varchar(30) default NULL,
  `c_zip` varchar(30) default NULL,
  `c_p_question1` tinyint(6) default NULL,
  `c_answer1` varchar(50) default NULL,
  `c_p_question2` tinyint(6) default NULL,
  `c_answer2` varchar(50) default NULL,
  `c_info_source` varchar(50) default NULL,
  `c_benef_country` varchar(30) default NULL,
  `customerStatus` varchar(20) NOT NULL default '',
  `accessFromIP` varchar(50) NOT NULL default '',
  `username` varchar(50) NOT NULL default '',
  `last_login` date default NULL,
  `dateConfrimed` date NOT NULL default '0000-00-00',
  `SecuriyCode` int(5) NOT NULL default '0',
  `dateDisabled` date NOT NULL default '0000-00-00',
  `reason` varchar(200) NOT NULL default '',
  `holdAccountID` int(20) NOT NULL default '0',
  `accountType` varchar(20) NOT NULL default '',
  `dateAccepted` date NOT NULL default '0000-00-00',
  `dob` date NOT NULL default '0000-00-00',
  `limit1` float NOT NULL default '0',
  `Balance` float NOT NULL default '0',
  `limit3` float NOT NULL default '0',
  `determind` varchar(50) NOT NULL default '',
  `changedPwd` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2869 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_questions`
-- 

CREATE TABLE `cm_questions` (
  `qID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  PRIMARY KEY  (`qID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complaintInteracts`
-- 

CREATE TABLE `complaintInteracts` (
  `id` double NOT NULL auto_increment,
  `cmpID` double NOT NULL default '0',
  `agent` varchar(32) NOT NULL default '',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `comments` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complaintinteracts`
-- 

CREATE TABLE `complaintinteracts` (
  `id` double NOT NULL auto_increment,
  `cmpID` double NOT NULL default '0',
  `agent` varchar(32) NOT NULL default '',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `comments` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complaints`
-- 

CREATE TABLE `complaints` (
  `cmpID` double NOT NULL auto_increment,
  `transID` double NOT NULL default '0',
  `agent` varchar(32) NOT NULL default '0',
  `subject` varchar(255) NOT NULL default '',
  `details` text NOT NULL,
  `status` varchar(15) NOT NULL default 'New',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `logedUserID` int(20) NOT NULL default '0',
  `logedUserName` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`cmpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `contacts`
-- 

CREATE TABLE `contacts` (
  `contactID` bigint(20) NOT NULL auto_increment,
  `officeName` varchar(255) NOT NULL default '',
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL default '',
  `fax` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `image` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`contactID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `currencies`
-- 

CREATE TABLE `currencies` (
  `cID` bigint(20) NOT NULL auto_increment,
  `country` varchar(45) NOT NULL default '',
  `currencyName` varchar(200) NOT NULL default '',
  `numCode` int(11) NOT NULL default '0',
  `description` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=276 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer`
-- 

CREATE TABLE `customer` (
  `customerID` bigint(20) unsigned NOT NULL auto_increment,
  `agentID` bigint(20) unsigned NOT NULL default '0',
  `created` date NOT NULL default '0000-00-00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `accountName` varchar(32) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `customerNumber` int(10) unsigned NOT NULL default '0',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `City` varchar(100) NOT NULL default '',
  `State` varchar(50) NOT NULL default '',
  `Zip` varchar(16) NOT NULL default '',
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `dob` date NOT NULL default '0000-00-00',
  `acceptedTerms` enum('Y','N') NOT NULL default 'Y',
  `IDType` varchar(100) NOT NULL default '',
  `IDNumber` varchar(32) NOT NULL default '',
  `IDExpiry` datetime NOT NULL default '0000-00-00 00:00:00',
  `issuedBy` varchar(255) NOT NULL default '',
  `documentProvided` varchar(100) NOT NULL default '',
  `fundsSources` varchar(100) NOT NULL default '',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `other_title` varchar(30) NOT NULL default '',
  `otherId` varchar(30) NOT NULL default '',
  `otherId_name` varchar(30) NOT NULL default '',
  `payinBook` varchar(35) NOT NULL default '',
  PRIMARY KEY  (`customerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3627 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer_account`
-- 

CREATE TABLE `customer_account` (
  `caID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `Date` date NOT NULL default '0000-00-00',
  `tranRefNo` varchar(30) NOT NULL default '',
  `payment_mode` varchar(50) NOT NULL default '0',
  `Type` varchar(10) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  PRIMARY KEY  (`caID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer_docs`
-- 

CREATE TABLE `customer_docs` (
  `ID` bigint(20) NOT NULL auto_increment,
  `docName` varchar(100) NOT NULL default '',
  `details` varchar(200) NOT NULL default '',
  `filepath` varchar(100) NOT NULL default '',
  `customerID` bigint(20) NOT NULL default '0',
  `docDate` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exchangerate`
-- 

CREATE TABLE `exchangerate` (
  `erID` bigint(20) unsigned NOT NULL auto_increment,
  `country` varchar(100) NOT NULL default '',
  `primaryExchange` float NOT NULL default '0',
  `sProvider` varchar(150) NOT NULL default '0',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `currency` varchar(10) NOT NULL default '',
  `marginPercentage` int(11) NOT NULL default '1',
  `rateFor` varchar(40) NOT NULL default '',
  `rateValue` varchar(75) NOT NULL default '',
  PRIMARY KEY  (`erID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `faqs`
-- 

CREATE TABLE `faqs` (
  `faqID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`faqID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `hcompanyaddress`
-- 

CREATE TABLE `hcompanyaddress` (
  `ID` int(11) NOT NULL auto_increment,
  `CustID` int(11) NOT NULL default '0',
  `line1` varchar(50) NOT NULL default '',
  `line2` varchar(50) NOT NULL default '',
  `line3` varchar(50) NOT NULL default '',
  `postcode` varchar(10) NOT NULL default '',
  `country` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `hcustomers`
-- 

CREATE TABLE `hcustomers` (
  `ID` int(10) NOT NULL auto_increment,
  `CompanyName` varchar(60) NOT NULL default '',
  `Person` varchar(50) default NULL,
  `Phone` varchar(20) NOT NULL default '',
  `Ext` varchar(10) default NULL,
  `Mobile` varchar(20) default NULL,
  `Email` varchar(60) NOT NULL default '',
  `URL` varchar(70) default NULL,
  `Description` longtext,
  `EnquiryID` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `hservices`
-- 

CREATE TABLE `hservices` (
  `ID` int(10) NOT NULL auto_increment,
  `CustID` varchar(10) NOT NULL default '',
  `Service` varchar(80) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `imbanks`
-- 

CREATE TABLE `imbanks` (
  `id` int(11) NOT NULL auto_increment,
  `bankName` varchar(150) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `bank_address` varchar(255) NOT NULL default '',
  `dd_code` varchar(20) NOT NULL default '',
  `dd_locations` varchar(50) NOT NULL default '',
  `bankCode` varchar(15) NOT NULL default '0',
  `branchName` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=75890 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `imfee`
-- 

CREATE TABLE `imfee` (
  `feeID` bigint(20) unsigned NOT NULL auto_increment,
  `origCountry` varchar(100) NOT NULL default '',
  `destCountry` varchar(100) NOT NULL default '',
  `amountRangeFrom` float NOT NULL default '0',
  `amountRangeTo` float NOT NULL default '0',
  `Fee` float NOT NULL default '0',
  `feeType` varchar(15) NOT NULL default '',
  `payinFee` float NOT NULL default '0',
  `agentNo` bigint(20) NOT NULL,
  PRIMARY KEY  (`feeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=820 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `login_activities`
-- 

CREATE TABLE `login_activities` (
  `activity_id` bigint(20) NOT NULL auto_increment,
  `login_history_id` bigint(20) NOT NULL,
  `activity` varchar(254) NOT NULL,
  `activity_time` datetime NOT NULL,
  `action_for` varchar(45) NOT NULL,
  `table_name` varchar(60) NOT NULL,
  `description` varchar(254) NOT NULL,
  PRIMARY KEY  (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `login_history`
-- 

CREATE TABLE `login_history` (
  `history_id` bigint(20) NOT NULL auto_increment,
  `login_time` datetime NOT NULL,
  `login_name` varchar(45) NOT NULL,
  `access_ip` varchar(140) NOT NULL,
  `login_type` varchar(60) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `natwestpayments`
-- 

CREATE TABLE `natwestpayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `tlaCode` varchar(5) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sessions`
-- 

CREATE TABLE `sessions` (
  `username` varchar(15) NOT NULL default '',
  `session_id` varchar(32) NOT NULL default '',
  `session_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ip` varchar(20) NOT NULL default '',
  `cookie_remember` enum('N','Y') NOT NULL default 'N',
  `is_online` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `states`
-- 

CREATE TABLE `states` (
  `country` varchar(50) NOT NULL default '',
  `state` varchar(30) NOT NULL default '',
  `code` varchar(10) NOT NULL default '',
  KEY `country` (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_error_code`
-- 

CREATE TABLE `tbl_error_code` (
  `ERROR_CODE` bigint(20) NOT NULL default '0',
  `ERROR_NAME` varchar(250) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_transaction_partner`
-- 

CREATE TABLE `tbl_transaction_partner` (
  `id` bigint(20) NOT NULL auto_increment,
  `TRANSID` bigint(20) NOT NULL default '0',
  `TDISPLAYID` varchar(50) NOT NULL default '',
  `TTYPE` int(10) NOT NULL default '0',
  `TRANS_DATE` date NOT NULL default '0000-00-00',
  `SEND_PTID` varchar(20) NOT NULL default '',
  `TRANS_RECV_DATE` date NOT NULL default '0000-00-00',
  `AUTH_CODE` varchar(100) NOT NULL default '',
  `PASSWORD` varchar(100) NOT NULL default '',
  `POUT_CURRENCYID` varchar(20) NOT NULL default '',
  `EXRATE` float NOT NULL default '0',
  `PAYOUT_AMT` float NOT NULL default '0',
  `STL_CURRENCYID` varchar(50) NOT NULL default '',
  `STL_AMT` float NOT NULL default '0',
  `STL_CHARGE` float NOT NULL default '0',
  `SCOUNTRY` varchar(50) NOT NULL default '',
  `SEND_POINT` varchar(50) NOT NULL default '',
  `RCOUNTRY` varchar(50) NOT NULL default '',
  `RCITY` varchar(50) NOT NULL default '',
  `PICKUP_POINT` varchar(50) NOT NULL default '',
  `PICKUP_PT_DETAILS` varchar(50) NOT NULL default '',
  `SENDER_FNAME` varchar(50) NOT NULL default '',
  `SENDER_LNAME` varchar(50) NOT NULL default '',
  `RECV_FNAME` varchar(50) NOT NULL default '',
  `RECV_LNAME` varchar(50) NOT NULL default '',
  `RECV_ID` varchar(100) NOT NULL default '',
  `RECV_ADDRESS1` varchar(200) NOT NULL default '',
  `RECV_ADDRESS2` varchar(200) NOT NULL default '',
  `RECV_PHONE` varchar(50) NOT NULL default '',
  `RECV_BANKINFO1` varchar(50) NOT NULL default '',
  `RECV_BANKINFO2` varchar(50) NOT NULL default '',
  `RECV_BANKINFO3` varchar(50) NOT NULL default '',
  `RECV_BANKINFO4` varchar(50) NOT NULL default '',
  `RECV_BANKINFO5` varchar(50) NOT NULL default '',
  `TSTATUSID` int(10) NOT NULL default '0',
  `PARTNER_READ` int(10) NOT NULL default '0',
  `TC_READ` int(10) NOT NULL default '0',
  `Error_Code` int(10) NOT NULL default '0',
  `Remarks` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_transaction_staus`
-- 

CREATE TABLE `tbl_transaction_staus` (
  `TSTATUSID` bigint(20) NOT NULL default '0',
  `DESCRIPTION` varchar(250) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `tblsys_stat`
-- 

CREATE TABLE `tblsys_stat` (
  `BUSY` int(10) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='(1 if BUSY , 0 if free) ( Mandatory) If this is 1 then your ';

-- --------------------------------------------------------

-- 
-- Table structure for table `teller`
-- 

CREATE TABLE `teller` (
  `tellerID` bigint(20) NOT NULL auto_increment,
  `collection_point` bigint(20) NOT NULL,
  `loginName` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` varchar(10) NOT NULL default 'Active',
  `isMain` varchar(5) NOT NULL default 'No',
  `changedPwd` datetime NOT NULL,
  `account_limit` varchar(30) NOT NULL,
  `limit_alert` varchar(30) NOT NULL,
  PRIMARY KEY  (`tellerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `teller_account_limit`
-- 

CREATE TABLE `teller_account_limit` (
  `id` bigint(20) NOT NULL auto_increment,
  `teller_id` int(11) NOT NULL,
  `limit_date` date NOT NULL,
  `account_limit` varchar(30) NOT NULL,
  `used_limit` varchar(30) NOT NULL,
  `limit_alert` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `test`
-- 

CREATE TABLE `test` (
  `userId` int(10) NOT NULL auto_increment,
  `userName` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `transactions`
-- 

CREATE TABLE `transactions` (
  `transID` bigint(20) unsigned NOT NULL auto_increment,
  `transType` enum('Pick up','Bank Transfer','Home Delivery','') NOT NULL default '',
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `benID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `benAgentID` bigint(20) NOT NULL default '0',
  `exchangeID` bigint(20) unsigned NOT NULL default '0',
  `refNumber` varchar(32) NOT NULL default '',
  `refNumberIM` varchar(50) NOT NULL default '',
  `transAmount` float NOT NULL default '0',
  `exchangeRate` float NOT NULL default '0',
  `localAmount` double NOT NULL default '0',
  `IMFee` float NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `totalAmount` float NOT NULL default '0',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `fundSources` varchar(100) NOT NULL default '',
  `moneyPaid` varchar(100) NOT NULL default '',
  `Declaration` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `verifiedBy` varchar(30) NOT NULL default '',
  `authorisedBy` varchar(30) NOT NULL default '',
  `cancelledBy` varchar(30) NOT NULL default '',
  `recalledBy` varchar(20) NOT NULL default '',
  `isSent` enum('Y','N') NOT NULL default 'N',
  `trackingNum` varchar(30) NOT NULL default '',
  `trans4Country` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `authoriseDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryOutDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rejectDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `failedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspeciousDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `recalledDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fromCountry` varchar(30) NOT NULL default '',
  `toCountry` varchar(30) NOT NULL default '',
  `currencyFrom` varchar(10) NOT NULL default '',
  `currencyTo` varchar(10) NOT NULL default '',
  `custAgentParentID` int(11) NOT NULL default '0',
  `benAgentParentID` int(11) NOT NULL default '0',
  `is_exported` varchar(10) NOT NULL default '',
  `PINCODE` int(5) NOT NULL default '0',
  `createdBy` varchar(10) NOT NULL default '',
  `collectionPointID` bigint(20) NOT NULL default '0',
  `transRefID` varchar(30) NOT NULL default '',
  `AgentComm` float NOT NULL default '0',
  `distributorComm` float NOT NULL default '0',
  `CommType` varchar(30) NOT NULL default '',
  `other_pur` varchar(35) NOT NULL default '',
  `admincharges` varchar(30) NOT NULL default '',
  `cashCharges` int(11) NOT NULL default '0',
  `holdedBy` varchar(20) NOT NULL default '',
  `refundFee` varchar(10) NOT NULL default '',
  `question` varchar(100) NOT NULL default '',
  `answer` varchar(200) NOT NULL default '',
  `tip` text,
  `outCurrCharges` float NOT NULL default '0',
  `discountRequest` varchar(255) NOT NULL,
  `discountType` varchar(255) NOT NULL,
  `discounted_amount` float NOT NULL default '0',
  `deliveredBy` varchar(45) NOT NULL,
  `holdDate` datetime NOT NULL,
  `verificationDate` datetime NOT NULL,
  `unholdBy` varchar(40) NOT NULL,
  `unholdDate` datetime NOT NULL,
  `creation_date_used` date NOT NULL,
  PRIMARY KEY  (`transID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=266 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `transtatus`
-- 

CREATE TABLE `transtatus` (
  `faqID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`faqID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `transtype`
-- 

CREATE TABLE `transtype` (
  `faqID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`faqID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user_registration`
-- 

CREATE TABLE `user_registration` (
  `id` bigint(20) NOT NULL default '0',
  `user_id` varchar(30) NOT NULL default '',
  `password` varchar(30) NOT NULL default '',
  `Company_name` varchar(50) NOT NULL default '',
  `address` varchar(200) NOT NULL default '',
  `country` varchar(40) NOT NULL default '',
  `email` varchar(40) NOT NULL default '',
  `post_code` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
