-- Ticket #3759 Prime Currency Change in Cheque model
-- Script added by Waqas Bin Hasan on September 12, 2008
ALTER TABLE `agent_account` ADD `modeOfPayment` VARCHAR( 10 ) NULL DEFAULT 'CASH', ADD `chequeNumber` VARCHAR( 30 ) NULL, ADD `chequeStatus` VARCHAR( 10 ) NULL ;

-- Ticker #3864
-- Poland states in Polish langiuuage


INSERT INTO `cities` (`country`, `city`, `countryCode`, `Currency`, `currDesc`, `isoCode`, `countryRegion`, `state`) VALUES 
('Poland', '', 'POL', '', '', 'pl', 'European', 'Zachodniopomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Warmi&#324;sko-Mazurskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Podkarpackie'),
('Poland', '', 'POL', '', '', 'pl', 'European', '&#346;l&#261;skie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Pomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Podlaskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Opolskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Mazowieckie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Lubuskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Lubelskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Dolno&#347;l&#261;skie'),
('Poland', '', 'POL', '', '', 'pl', 'European', '&#321;&oacute;dzkie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Ma&#322;opolskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Kieleckie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Kujawsko-Pomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Wielkopolskie');


-- Ticket# 3854
-- Multiple Ids for online sender and beneficiary


CREATE TABLE IF NOT EXISTS `cm_user_id_types` (
  `id` bigint(20) NOT NULL auto_increment,
  `id_type_id` mediumint(9) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type` enum('B','C') NOT NULL COMMENT 'B for beneficiary and C for customer/sender',
  `id_number` varchar(255) NOT NULL,
  `issued_by` varchar(255) NOT NULL,
  `issue_date` date NOT NULL default '0000-00-00',
  `expiry_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id_type_id` (`id_type_id`,`user_id`,`user_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- Ticket #4018 Prime Currency Old & New Transactions for Export for RT & RQ
-- Script added by Waqas Bin Hasan on October 25, 2008
ALTER TABLE `transactions` ADD `dispatchNumber` INT( 11 ) NOT NULL DEFAULT '0',
ADD `dispatchDate` DATE NULL COMMENT 'will store the dispatch date related to dispatch number';

-- Ticket #4138
-- In case of virtual agent, the associated agent value will store
ALTER TABLE `cm_customer` ADD `agentId` BIGINT( 15 ) NULL COMMENT 'In case of virtual agent, the associated agent value will store';

