ALTER TABLE `admin` ADD `mobile` VARCHAR( 32 ) NOT NULL ,
ADD `designation` VARCHAR( 255 ) NOT NULL ;


ALTER TABLE `agent_account` ADD `note` VARCHAR( 255 ) NOT NULL ;




ALTER TABLE `cm_collection_point` ADD `disabledDate` DATETIME NOT NULL ,
ADD `disabledBy` VARCHAR( 100 ) NOT NULL ,
ADD `disableReason` VARCHAR( 255 ) NOT NULL ;


ALTER TABLE `cm_collection_point`
  DROP `loginName`,
  DROP `password`,
  DROP `name`;



CREATE TABLE `teller` (
  `tellerID` bigint(20) NOT NULL auto_increment,
  `collection_point` bigint(20) NOT NULL,
  `loginName` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` varchar(10) NOT NULL default 'Active',
  `isMain` varchar(5) NOT NULL default 'No',
  `changedPwd` datetime NOT NULL,
  PRIMARY KEY  (`tellerID`)
);


ALTER TABLE `transactions` ADD `deliveredBy` VARCHAR( 45 ) NOT NULL ;