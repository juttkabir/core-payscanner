
-- Ticket 4851 passport validation

ALTER TABLE `customer` ADD `passport_number` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `admin` ADD `bankCharges` FLOAT NOT NULL ;

-- Ticket 3337
-- 3337: Global Exchange- Retain amendment history

ALTER TABLE `amended_transactions` ADD `history` TEXT NULL COMMENT 'To retain the ammended transaction history';

CREATE TABLE IF NOT EXISTS `transaction_modify_history` (
  `id` int(5) NOT NULL auto_increment,
  `transid` int(11) NOT NULL,
  `change_log` text  NOT NULL,
  `userid` varchar(11) NOT NULL,
  `datetime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='to log about the what is modify in the amended transaction c' AUTO_INCREMENT=7 ;

UPDATE `tbl_export_fields` SET `Value` = '' WHERE `tbl_export_fields`.`id` =86 LIMIT 1 ;

UPDATE `tbl_export_fields` SET `isFixed` = 'N',
`Value` = '' WHERE `tbl_export_fields`.`id` =87 LIMIT 1 ;

UPDATE `tbl_export_fields` SET `isFixed` = 'N' WHERE `tbl_export_fields`.`id` =86 LIMIT 1 ;

ALTER TABLE `banks` ADD `branchName` VARCHAR( 150 ) NOT NULL ;