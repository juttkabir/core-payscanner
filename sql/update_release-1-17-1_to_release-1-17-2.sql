CREATE TABLE `profit_earning` (
`profit_id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`entry_time` DATETIME NOT NULL ,
`origin_country` VARCHAR( 60 ) NOT NULL ,
`dest_country` VARCHAR( 60 ) NOT NULL ,
`origin_amount` DOUBLE NOT NULL ,
`origin_currency` VARCHAR( 10 ) NOT NULL ,
`intermediate_rate` FLOAT NOT NULL ,
`intermediate_curr` VARCHAR( 10 ) NOT NULL ,
`send_rate_inter_to_dest` FLOAT NOT NULL ,
`dest_currency` VARCHAR( 10 ) NOT NULL ,
`bank_charges` FLOAT NOT NULL ,
`reciev_rate_inter_to_dest` FLOAT NOT NULL ,
`update_time` DATETIME NOT NULL
) ENGINE = MYISAM ;


ALTER TABLE `amended_transactions` ADD `distCommPackage` VARCHAR( 30 ) NOT NULL ;


ALTER TABLE `teller_account` ADD `note` VARCHAR( 256 ) NOT NULL ;


ALTER TABLE `customer` CHANGE `payinBook` `payinBook`  varchar(35) NULL;

ALTER TABLE `imfee` CHANGE `agentNo` `agentNo` bigint(20) NOT NULL default '0';

ALTER TABLE `manageFee` CHANGE `origCountry` `origCountry` varchar(100) NOT NULL default '';
ALTER TABLE `manageFee` CHANGE `destCountry` `destCountry` varchar(100) NOT NULL default '';

ALTER TABLE `manageFee` CHANGE `feeType` `feeType` varchar(30) NOT NULL DEFAULT '';

ALTER TABLE `teller_account` CHANGE `description` `description` VARCHAR( 90 ) NOT NULL DEFAULT '';


ALTER TABLE `customer` CHANGE `payinBook` `payinBook` VARCHAR( 35 ) NULL;

ALTER TABLE `CurrenryNotes` ADD `date` VARCHAR( 30 ) NOT NULL;

ALTER TABLE `admin` ADD `colorView` varchar(30) NOT NULL;

ALTER TABLE `admin` CHANGE `adminType` `adminType` ENUM( 'Supper', 'System', 'Admin', 'Agent', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager' ) NOT NULL DEFAULT 'Agent';

ALTER TABLE `cm_beneficiary` ADD `CPF` varchar(50) NOT NULL;

ALTER TABLE `teller` ADD `rights` varchar(240) NOT NULL;