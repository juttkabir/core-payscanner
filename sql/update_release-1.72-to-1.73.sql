-- #4949 - AMB paypoint transactions functionality

CREATE TABLE IF NOT EXISTS `paypointTransactions` (
  `transID` bigint(20) unsigned NOT NULL auto_increment,
  `transType` varchar(50) NOT NULL default '',
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `serviceID` bigint(20) unsigned NOT NULL default '0',
  `categoryID` bigint(20) unsigned NOT NULL default '0' COMMENT 'category is sub-type of serviceID',
  `refNumber` varchar(32) NOT NULL default '',
  `transAmount` float(15,4) unsigned NOT NULL default '0.0000',
  `IMFee` float NOT NULL default '0',
  `totalAmount` float(15,4) NOT NULL default '0.0000',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelledBy` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `country` varchar(530) NOT NULL default 'United Kingdom',
  `currency` varchar(10) NOT NULL default 'GBP',
  `createdBy` varchar(10) NOT NULL default '',
  `agentComm` float NOT NULL default '0',
  `admincharges` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`transID`)
);

-- #4949 - AMB paypoint transactions functionality. services for transactions

CREATE TABLE `servicesPaypoint` (
`serviceID` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`serviceName` VARCHAR( 50 ) NOT NULL ,
`countryID` BIGINT NOT NULL DEFAULT '1' ,
`countryName` VARCHAR( 50 ) NOT NULL DEFAULT 'United Kingdom',
`currency` VARCHAR( 5 ) NOT NULL DEFAULT 'GBP',
`createdBy` VARCHAR( 50 ) NOT NULL ,
`created_at` DATETIME NOT NULL
);

-- #4949 - AMB paypoint transactions functionality. categories for services

CREATE TABLE `categoryPaypoint` (
`categoryID` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`categoryName` VARCHAR( 50 ) NOT NULL ,
`serviceID` BIGINT NOT NULL ,
`commission` FLOAT( 15, 4 ) NOT NULL DEFAULT '0.0000',
`createdBy` VARCHAR( 50 ) NOT NULL ,
`created_at` DATETIME NOT NULL
);
-- #4949 - AMB paypoint transactions functionality. account summary table

CREATE TABLE IF NOT EXISTS `account_summary_paypoint` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `dated` date NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `serviceID` bigint(20) NOT NULL,
  `categoryID` bigint(20) NOT NULL,
  `currency` varchar(10) NOT NULL default 'GBP',
  PRIMARY KEY  (`id`)
);

ALTER TABLE `transactions` ADD `isResolved` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' COMMENT 'item is resolved or not';

-- Ticket #4998-AMB
ALTER TABLE `admin` ADD `adminLimitTransAmount` FLOAT NOT NULL ,
ADD `adminLimitCheque` FLOAT NOT NULL ,
ADD `adminLimitCurrExchange` FLOAT NOT NULL ;

-- Ticket #4950-AMB

ALTER TABLE `agents_customer_account` ADD `chequeNumber` VARCHAR( 30 ) NULL ,
ADD `chequeStatus` VARCHAR( 10 ) NULL ;


ALTER TABLE `admin` ADD `companyType` ENUM( 'MSB', '3rd party' ) NOT NULL DEFAULT 'MSB' COMMENT 'MSB or 3rd Party company type';
ALTER TABLE `admin` ADD `tradingName` VARCHAR( 50 ) NOT NULL COMMENT 'This is trading name againse company';

-- 
-- Table structure for table `users_configuration`
-- Ticket #5004

CREATE TABLE IF NOT EXISTS `users_configuration` (
  `id` int(120) NOT NULL auto_increment,
  `trans_hours_limit` varchar(240) NOT NULL,
  `customer_account_days_limit` varchar(240) NOT NULL,
  `customer_id` varchar(240) NOT NULL,
  `customer_limit_apply_at` varchar(240) NOT NULL,
  `customer_amount_limit` varchar(240) NOT NULL,
  `isEnable` enum('Y','N') NOT NULL,
  `client` varchar(240) NOT NULL,
  `userType` varchar(50) NOT NULL,
  `user_db_id` varchar(50) NOT NULL,
  `created_by` varchar(240) NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;