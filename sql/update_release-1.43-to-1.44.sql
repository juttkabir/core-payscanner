-- Ticket #3632 -  Global Exchange - Further Points (12 Aug 2008) - Master Ticket
-- Script added by khola
-- Auto increment is not present in document_upload table's id field after the new configurations.
ALTER TABLE `document_upload` CHANGE `id` `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT;  

-- Ticket #3544 - Now Transfer - Online module enhancements
-- Scripts added by Usman Ghani
-- Altered the table cm_beneficiary as the data type of 'State' field was not according to requirement.

ALTER TABLE `cm_beneficiary` CHANGE `State` `State` VARCHAR( 50 ) NOT NULL;

-- End of scripts by Usman Ghani

-- Ticket #3603 Connect Plus Distributor Commission
-- Script added by Waqas Bin Hasan on August 20, 2008
CREATE TABLE IF NOT EXISTS `commission` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`userId` BIGINT( 20 ) NOT NULL ,
`collectionPointId` BIGINT( 20 ) NOT NULL ,
`fixedValue` FLOAT( 5, 2 ) NOT NULL ,
`percentValue` FLOAT( 5, 2 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = MYISAM;

-- Ticket #3603 Connect Plus Distributor Commission
-- Script added by Waqas Bin Hasan on August 25, 2008
ALTER TABLE `transactions` ADD `settlementDistributorCommisson` FLOAT( 7, 2 ) NULL DEFAULT '0.00';

-- Ticket #3638
-- new field to have faxNumber based on agents
ALTER TABLE `transactions` ADD `faxNumber` INT NULL ;

-- Ticket #3680, #3683
-- new table for dispatch trasaction  and extra field

ALTER TABLE `transactions` ADD `dispatchNumber` INT NOT NULL DEFAULT '0';

CREATE TABLE `dispatch_book` (
`recId` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`dispatchNumber` VARCHAR( 50 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`reportQuery` TEXT NOT NULL
) ENGINE = MYISAM ;


-- Ticket #3642 - Prime Currency - Agent Account Balance Modification
-- Scripts added by Usman Ghani
-- Added new field in the transactions table to store part cheque and part cash transactions amount.

ALTER TABLE `transactions` ADD `chequeAmount` FLOAT NOT NULL DEFAULT '0' AFTER `chequeNo`;

-- End of scripts by Usman Ghani