CREATE TABLE `setUserRights` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`userGroup` VARCHAR( 50 ) NOT NULL ,
`rightName` VARCHAR( 50 ) NOT NULL ,
`rightStatus` CHAR( 2 ) NOT NULL,
`rightValue` VARCHAR( 200 ) NOT NULL ,
`updated` DATE NOT NULL
) ENGINE = MYISAM ;