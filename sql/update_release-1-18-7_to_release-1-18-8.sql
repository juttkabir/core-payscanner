ALTER TABLE `configure_commission`
ADD `currency` varchar(10) NOT NULL,
ADD `country` varchar(45) NOT NULL;

ALTER TABLE `customer` ADD `placeOfBirth` VARCHAR( 50 ) NULL AFTER `dob` ;
ALTER TABLE `cm_customer` ADD `placeOfBirth` VARCHAR( 50 ) NULL AFTER `dob` ;

ALTER TABLE `beneficiary` CHANGE `IDissuedate` `IDissuedate` DATE NOT NULL DEFAULT '0000-00-00 00:00:00',
CHANGE `IDexpirydate` `IDexpirydate` DATE NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `customer` CHANGE `IDissuedate` `IDissuedate` DATE NOT NULL DEFAULT '0000-00-00 ';

INSERT INTO `tbl_export_fields` ( `id` , `fileID` , `payex_field` , `tableName` , `isActive` , `Lable` , `tempID` , `exportDefaultID` , `field_title` )
VALUES (
NULL , '3', 'agentID', 'customer', 'Y', 'Agent Code', '0', '', ''
), (
NULL , '3', 'Zip', 'customer', 'Y', 'Postal Code', '0', '', ''
);

INSERT INTO `tbl_export_fields` ( `id` , `fileID` , `payex_field` , `tableName` , `isActive` , `Lable` , `tempID` , `exportDefaultID` , `field_title` )
VALUES (
NULL , '3', 'payinBook', 'customer', 'Y', 'Customer Type', '0', '', ''
);

CREATE TABLE `user` (
  `userID` bigint(20) NOT NULL auto_increment,
  `accountID` bigint(20) NOT NULL,
  `userName` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(32) NOT NULL,
  `lastModified` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastModifiedBy` varchar(32) NOT NULL,
  `accessFromIP` varchar(255) default NULL,
  `isMain` ENUM( 'N', 'Y' ) NULL DEFAULT 'N',
   `isAdmin` ENUM( 'N', 'Y' ) NULL DEFAULT 'N',
  PRIMARY KEY  (`userID`)
);

