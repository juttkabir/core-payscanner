-- Script for Curency Exchange Module added in release-1.59 for MC

CREATE TABLE IF NOT EXISTS `curr_exchange` (
  `id` bigint(20) NOT NULL auto_increment,
  `operationCurrency` bigint(20) NOT NULL COMMENT 'operational currency id',
  `buysellCurrency` bigint(20) NOT NULL COMMENT 'buying or selling currency id',
  `buying_rate` float NOT NULL,
  `selling_rate` float NOT NULL,
  `createdBy` bigint(20) default NULL COMMENT 'rate creator id',
  `updatedBy` bigint(20) default NULL COMMENT 'rate updator id',
  `created_at` datetime default NULL COMMENT 'rate creation time',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `curr_exchange_account` (
  `id` bigint(20) NOT NULL auto_increment,
  `operationCurrency` bigint(20) NOT NULL,
  `buysellCurrency` bigint(20) NOT NULL COMMENT 'currency to be bought or sold',
  `buy_sell` char(1) collate utf8_unicode_ci NOT NULL default 'B' COMMENT 'buy or sell',
  `rate` float NOT NULL COMMENT 'rate t buy or sell',
  `localAmount` float NOT NULL,
  `totalAmount` float NOT NULL,
  `customerID` bigint(20) default NULL,
  `createdBy` bigint(20) default NULL,
  `updatedBy` bigint(20) default NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
