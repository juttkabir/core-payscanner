-- phpMyAdmin SQL Dump
-- version 2.9.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 17, 2008 at 11:46 AM
-- Server version: 5.0.22
-- PHP Version: 5.0.4
-- 
-- Database: `test_express`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `CampaignUsed`
-- 

CREATE TABLE `CampaignUsed` (
  `id` bigint(50) NOT NULL auto_increment,
  `campaignID` bigint(50) NOT NULL,
  `amountUser` varchar(250) NOT NULL,
  `loggedUserID` bigint(50) NOT NULL,
  `transID` varchar(250) NOT NULL,
  `userID` varchar(250) NOT NULL,
  `loggedUserType` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `Campaign_Type`
-- 

CREATE TABLE `Campaign_Type` (
  `id` bigint(20) NOT NULL auto_increment,
  `heading` varchar(200) character set utf8 collate utf8_unicode_ci NOT NULL,
  `message` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `lastChange` datetime NOT NULL default '0000-00-00 00:00:00',
  `changeBy` varchar(35) NOT NULL,
  `isEnable` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `CurrenryNotes`
-- 

CREATE TABLE `CurrenryNotes` (
  `id` int(11) NOT NULL auto_increment,
  `Currency` varchar(10) NOT NULL,
  `DistributerID` varchar(10) NOT NULL,
  `TellerID` varchar(10) NOT NULL,
  `date` varchar(30) NOT NULL,
  `Note1` int(11) NOT NULL,
  `Note2` int(11) NOT NULL,
  `Note5` int(11) NOT NULL,
  `Note10` int(11) NOT NULL,
  `Note20` int(11) NOT NULL,
  `Note50` int(11) NOT NULL,
  `Coin05` int(11) NOT NULL,
  `Coin02` int(11) NOT NULL,
  `Coin01` int(11) NOT NULL,
  `Coin005` int(11) NOT NULL,
  `Coin002` int(11) NOT NULL,
  `Coin001` int(11) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `SharedExchangeRate`
-- 

CREATE TABLE `SharedExchangeRate` (
  `shareXchangeId` int(11) NOT NULL auto_increment,
  `localXchangeId` int(11) NOT NULL,
  `remoteXchangeId` int(11) NOT NULL,
  `createdServer` varchar(25) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  PRIMARY KEY  (`shareXchangeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `SharedFee`
-- 

CREATE TABLE `SharedFee` (
  `shareFeeId` int(11) NOT NULL auto_increment,
  `localFeeId` int(11) NOT NULL,
  `remoteFeeId` int(11) NOT NULL,
  `createdServer` varchar(25) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  PRIMARY KEY  (`shareFeeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `account_summary`
-- 

CREATE TABLE `account_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` varchar(20) NOT NULL,
  `dated` date NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `currency` varchar(10) NOT NULL default 'GBP',
  `settlementOpening_balance` double NOT NULL,
  `settlementClosing_balance` double NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=36423 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `admin`
-- 

CREATE TABLE `admin` (
  `userID` bigint(20) NOT NULL auto_increment,
  `username` varchar(32) NOT NULL default '',
  `password` varchar(16) NOT NULL default '',
  `changedPwd` date NOT NULL default '0000-00-00',
  `name` varchar(25) NOT NULL default '',
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  `isMain` enum('N','Y') NOT NULL default 'N',
  `email` varchar(100) NOT NULL default '',
  `role` text NOT NULL,
  `rights` varchar(255) NOT NULL default '',
  `backDays` int(11) NOT NULL default '0',
  `adminType` enum('Supper','System','Admin','Agent','Call','COLLECTOR','Branch Manager','Admin Manager','Support','SUPI Manager','MLRO') NOT NULL default 'Agent',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `parentID` bigint(20) unsigned NOT NULL default '0',
  `agentType` enum('Supper','Sub') NOT NULL default 'Supper',
  `isCorrespondent` enum('Y','N','ONLY') NOT NULL default 'N',
  `IDAcountry` text NOT NULL,
  `custCountries` varchar(255) NOT NULL,
  `agentNumber` int(10) unsigned NOT NULL default '0',
  `subagentNum` varchar(50) NOT NULL,
  `PPType` varchar(50) NOT NULL,
  `agentCompany` varchar(255) NOT NULL default '',
  `agentContactPerson` varchar(100) NOT NULL default '',
  `agentAddress` varchar(255) NOT NULL default '',
  `agentAddress2` varchar(255) NOT NULL default '',
  `agentCity` varchar(100) NOT NULL default '',
  `agentZip` varchar(16) NOT NULL default '',
  `agentCountry` varchar(100) NOT NULL default '',
  `agentCountryCode` char(3) NOT NULL default '',
  `agentPhone` varchar(32) NOT NULL default '',
  `agentFax` varchar(32) NOT NULL default '',
  `agentURL` varchar(255) NOT NULL default '',
  `agentMSBNumber` varchar(255) NOT NULL default '',
  `agentMCBExpiry` datetime NOT NULL default '0000-00-00 00:00:00',
  `agentCompRegNumber` varchar(100) NOT NULL default '',
  `agentCompDirector` varchar(100) NOT NULL default '',
  `agentDirectorAdd` varchar(255) NOT NULL default '',
  `agentProofID` varchar(100) NOT NULL default '',
  `agentIDExpiry` datetime NOT NULL default '0000-00-00 00:00:00',
  `agentDocumentProvided` varchar(100) NOT NULL default '',
  `agentBank` varchar(255) NOT NULL default '',
  `agentAccountName` varchar(255) NOT NULL default '',
  `agentAccounNumber` varchar(255) NOT NULL default '',
  `agentBranchCode` varchar(255) NOT NULL default '',
  `agentAccountType` varchar(255) NOT NULL default '',
  `agentCurrency` varchar(255) NOT NULL default '',
  `agentAccountLimit` float NOT NULL default '0',
  `limitUsed` float NOT NULL default '0',
  `commPackage` enum('001','002','003') NOT NULL default '001',
  `agentCommission` float NOT NULL default '0',
  `agentStatus` enum('New','Active','Disabled','Suspended') NOT NULL default 'New',
  `suspensionReason` text NOT NULL,
  `suspendedBy` varchar(32) NOT NULL default '',
  `activatedBy` varchar(32) NOT NULL default '',
  `disableReason` text NOT NULL,
  `disabledBy` varchar(32) NOT NULL default '',
  `logo` varchar(30) NOT NULL default '',
  `accessFromIP` varchar(255) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `authorizedFor` varchar(200) NOT NULL default '',
  `balance` double NOT NULL default '0',
  `postCode` varchar(10) default NULL,
  `payinBook` varchar(25) NOT NULL default '',
  `mobile` varchar(32) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `commPackageAnDDist` enum('001','002','003') NOT NULL default '001',
  `commAnDDist` float NOT NULL default '0',
  `colorView` varchar(30) NOT NULL,
  `linked_Agent` varchar(255) NOT NULL,
  `linked_Distibutor` varchar(250) NOT NULL,
  `defaultMoneyPaid` varchar(100) NOT NULL,
  `isOnline` enum('N','Y') NOT NULL default 'N',
  `defaultDistrib` enum('N','Y') NOT NULL default 'N',
  `fromServer` varchar(20) NOT NULL,
  `paymentMode` enum('include','exclude') NOT NULL default 'include',
  `settlementCurrency` varchar(20) NOT NULL,
  PRIMARY KEY  (`userID`),
  KEY `agentType` (`agentType`),
  KEY `username` (`username`),
  KEY `name_2` (`name`,`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=100633 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_Dist_account`
-- 

CREATE TABLE `agent_Dist_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `actAs` varchar(20) NOT NULL default 'Agent',
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_account`
-- 

CREATE TABLE `agent_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL default '',
  `note` varchar(256) NOT NULL,
  `SID` bigint(20) NOT NULL,
  `settleAmount` varchar(30) NOT NULL,
  `currency` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`),
  KEY `type` (`type`),
  KEY `status` (`status`),
  KEY `agentId` (`agentID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=521365 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `agents_customer_account`
-- 

CREATE TABLE `agents_customer_account` (
  `caID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `Date` date NOT NULL default '0000-00-00',
  `tranRefNo` varchar(30) NOT NULL default '',
  `payment_mode` varchar(50) NOT NULL default '0',
  `Type` varchar(10) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  `modifiedBy` varchar(35) NOT NULL default '',
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`caID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=56156 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `amended_transactions`
-- 

CREATE TABLE `amended_transactions` (
  `amendID` bigint(20) unsigned NOT NULL auto_increment,
  `transID` bigint(20) unsigned NOT NULL,
  `transType` enum('Pick up','Bank Transfer','Home Delivery','') NOT NULL default '',
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `benID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `benAgentID` bigint(20) NOT NULL default '0',
  `exchangeID` bigint(20) unsigned NOT NULL default '0',
  `refNumber` varchar(32) NOT NULL default '',
  `refNumberIM` varchar(50) NOT NULL default '',
  `transAmount` float NOT NULL default '0',
  `exchangeRate` float NOT NULL default '0',
  `localAmount` double NOT NULL default '0',
  `IMFee` float NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `totalAmount` float NOT NULL default '0',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `fundSources` varchar(100) NOT NULL default '',
  `moneyPaid` varchar(100) NOT NULL default '',
  `Declaration` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `verifiedBy` varchar(30) NOT NULL default '',
  `authorisedBy` varchar(30) NOT NULL default '',
  `cancelledBy` varchar(30) NOT NULL default '',
  `recalledBy` varchar(20) NOT NULL default '',
  `isSent` enum('Y','N') NOT NULL default 'N',
  `trackingNum` varchar(30) NOT NULL default '',
  `trans4Country` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `authoriseDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryOutDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rejectDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `failedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspeciousDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `recalledDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fromCountry` varchar(30) NOT NULL default '',
  `toCountry` varchar(30) NOT NULL default '',
  `currencyFrom` varchar(10) NOT NULL default '',
  `currencyTo` varchar(10) NOT NULL default '',
  `custAgentParentID` int(11) NOT NULL default '0',
  `benAgentParentID` int(11) NOT NULL default '0',
  `is_exported` varchar(10) NOT NULL default '',
  `PINCODE` int(5) NOT NULL default '0',
  `createdBy` varchar(10) NOT NULL default '',
  `collectionPointID` bigint(20) NOT NULL default '0',
  `transRefID` varchar(30) NOT NULL default '',
  `AgentComm` float NOT NULL default '0',
  `distributorComm` float NOT NULL default '0',
  `CommType` varchar(30) NOT NULL default '',
  `other_pur` varchar(35) NOT NULL default '',
  `admincharges` varchar(30) NOT NULL default '',
  `cashCharges` int(11) NOT NULL default '0',
  `holdedBy` varchar(20) NOT NULL default '',
  `refundFee` varchar(10) NOT NULL default '',
  `question` varchar(100) NOT NULL default '',
  `answer` varchar(200) NOT NULL default '',
  `tip` text,
  `outCurrCharges` float NOT NULL default '0',
  `discountRequest` varchar(255) NOT NULL,
  `discountType` varchar(255) NOT NULL,
  `discounted_amount` float NOT NULL default '0',
  `deliveredBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(40) NOT NULL,
  `modificationDate` datetime NOT NULL,
  `holdDate` datetime NOT NULL,
  `verificationDate` datetime NOT NULL,
  `unholdBy` varchar(40) NOT NULL,
  `unholdDate` datetime NOT NULL,
  `distCommPackage` varchar(30) NOT NULL,
  `agentExchangeRate` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`amendID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=19901 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bankCharges`
-- 

CREATE TABLE `bankCharges` (
  `chargeID` bigint(20) unsigned NOT NULL auto_increment,
  `origCountry` varchar(100) NOT NULL default '',
  `currencyOrigin` varchar(10) NOT NULL,
  `destCountry` varchar(100) NOT NULL default '',
  `currencyDest` varchar(10) NOT NULL,
  `amountRangeFrom` float NOT NULL default '0',
  `amountRangeTo` float NOT NULL default '0',
  `Charges` float NOT NULL default '0',
  `chargeType` varchar(30) NOT NULL,
  `agentNo` bigint(20) NOT NULL default '0',
  `chargeBasedOn` varchar(100) default NULL,
  PRIMARY KEY  (`chargeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bankDetails`
-- 

CREATE TABLE `bankDetails` (
  `bankID` int(11) NOT NULL auto_increment,
  `benID` int(11) NOT NULL default '0',
  `transID` int(11) NOT NULL default '0',
  `bankName` varchar(150) NOT NULL default '',
  `accNo` varchar(100) NOT NULL default '',
  `branchCode` varchar(100) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `ABACPF` varchar(100) NOT NULL default '',
  `IBAN` varchar(100) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `accountType` varchar(30) NOT NULL default 'Regular',
  `Remarks` varchar(256) NOT NULL,
  PRIMARY KEY  (`bankID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=606 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `bank_account`
-- 

CREATE TABLE `bank_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `bankID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `currency` varchar(10) NOT NULL,
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `settlementCurrency` varchar(20) NOT NULL,
  `settlementValue` varchar(20) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=277511 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `banklinepayments`
-- 

CREATE TABLE `banklinepayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `isDeleted` enum('N','Y') NOT NULL default 'N',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `tlaCode` varchar(5) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `barclayspayments`
-- 

CREATE TABLE `barclayspayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `isDeleted` enum('N','Y') NOT NULL default 'N',
  `tlaCode` varchar(5) NOT NULL default '',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `beneficiary`
-- 

CREATE TABLE `beneficiary` (
  `benID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `created` date NOT NULL default '0000-00-00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `SOF` varchar(250) NOT NULL,
  `benAccount` varchar(32) NOT NULL default '',
  `Password` varchar(32) NOT NULL default '',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `City` varchar(100) NOT NULL default '',
  `State` varchar(50) NOT NULL default '',
  `Zip` varchar(16) NOT NULL default '',
  `Citizenship` varchar(100) NOT NULL,
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `IDissuedate` date NOT NULL default '0000-00-00',
  `issuedBy` varchar(255) NOT NULL,
  `IDexpirydate` date NOT NULL default '0000-00-00',
  `IDType` varchar(100) NOT NULL default '',
  `IDNumber` varchar(32) NOT NULL default '',
  `CPF` varchar(50) NOT NULL,
  `isTransfered` enum('Y','N') NOT NULL default 'N',
  `agentID` int(11) NOT NULL default '0',
  `other_title` varchar(30) NOT NULL default '',
  `otherId` varchar(30) NOT NULL default '',
  `otherId_name` varchar(30) NOT NULL default '',
  `beneficiaryName` varchar(50) NOT NULL,
  `dob` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`benID`),
  KEY `firstName` (`firstName`),
  KEY `lastName` (`lastName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=228993 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `campaign`
-- 

CREATE TABLE `campaign` (
  `id` bigint(30) NOT NULL auto_increment,
  `campaignCategory` enum('transaction','user') NOT NULL default 'user',
  `Type` varchar(250) NOT NULL,
  `currencyType` enum('sending','recieving') NOT NULL default 'sending',
  `currencyValue` varchar(250) NOT NULL,
  `PaymentMode` varchar(100) NOT NULL,
  `NoOfTransactions` bigint(50) NOT NULL,
  `upperAmount` bigint(50) NOT NULL,
  `startdate` date NOT NULL,
  `endDate` date NOT NULL,
  `offerType` enum('free trans','discount') NOT NULL default 'discount',
  `offerValue` varchar(250) NOT NULL,
  `transactionType` varchar(250) NOT NULL,
  `status` enum('active','deactive') NOT NULL default 'active',
  `transactionFrom` date NOT NULL,
  `transationTo` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `category`
-- 

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cities`
-- 

CREATE TABLE `cities` (
  `country` varchar(50) NOT NULL default '',
  `city` varchar(255) NOT NULL default '',
  `countryCode` varchar(4) NOT NULL default '',
  `Currency` char(3) NOT NULL default '',
  `currDesc` varchar(32) NOT NULL default '',
  `isoCode` char(2) NOT NULL default '',
  `countryRegion` varchar(100) NOT NULL,
  KEY `country` (`country`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_bankdetails`
-- 

CREATE TABLE `cm_bankdetails` (
  `bankID` int(11) NOT NULL auto_increment,
  `benID` int(11) NOT NULL default '0',
  `transID` int(11) NOT NULL default '0',
  `bankName` varchar(150) NOT NULL default '',
  `BankCode` varchar(50) NOT NULL default '',
  `accNo` varchar(100) NOT NULL default '',
  `branchCode` varchar(100) NOT NULL default '',
  `BranchName` varchar(200) NOT NULL default '',
  `BranchCity` varchar(100) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `ABACPF` varchar(100) NOT NULL default '',
  `IBAN` varchar(100) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `BankStatus` varchar(40) NOT NULL default '',
  `BankType` varchar(40) NOT NULL default '',
  `DepositMethod` varchar(40) NOT NULL default '',
  `StatmentID` varchar(40) NOT NULL default '',
  `accountType` varchar(30) NOT NULL default 'Regular',
  `Remarks` varchar(256) NOT NULL,
  PRIMARY KEY  (`bankID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_beneficiary`
-- 

CREATE TABLE `cm_beneficiary` (
  `benID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `created` date NOT NULL default '0000-00-00',
  `DateDisabled` date NOT NULL default '0000-00-00',
  `DateLastPaid` date NOT NULL default '0000-00-00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `benAccount` varchar(32) NOT NULL default '',
  `Password` varchar(32) NOT NULL default '',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `address2` varchar(255) NOT NULL default '',
  `City` varchar(100) NOT NULL default '',
  `State` tinyint(50) NOT NULL default '0',
  `Zip` varchar(16) NOT NULL default '',
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `IDType` varchar(100) NOT NULL default '',
  `NICNumber` varchar(32) NOT NULL default '',
  `oldNICNumber` varchar(32) NOT NULL default '',
  `CPF` varchar(50) NOT NULL,
  `bankDetailsID` int(20) NOT NULL default '0',
  `transType` varchar(25) NOT NULL default '',
  `loginID` bigint(20) NOT NULL default '0',
  `editedBy` varchar(30) NOT NULL default '',
  `editDate` date NOT NULL default '0000-00-00',
  `other_title` varchar(100) NOT NULL,
  `otherId` bigint(20) NOT NULL,
  `otherid_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`benID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_collection_point`
-- 

CREATE TABLE `cm_collection_point` (
  `cp_id` int(20) NOT NULL auto_increment,
  `isAdmin` varchar(10) NOT NULL default 'No',
  `cp_corresspondent_name` varchar(50) default NULL,
  `cp_ria_branch_code` varchar(20) default NULL,
  `cp_ida_id` varchar(20) default NULL,
  `cp_branch_name` varchar(70) default NULL,
  `cp_branch_no` varchar(20) default NULL,
  `cp_branch_address` varchar(255) default NULL,
  `cp_city` varchar(50) default NULL,
  `cp_state` varchar(50) default NULL,
  `cp_country` varchar(50) default NULL,
  `cp_phone` varchar(50) default NULL,
  `cp_active` varchar(10) NOT NULL default '',
  `disabledDate` datetime NOT NULL,
  `disabledBy` varchar(100) NOT NULL,
  `disableReason` varchar(256) NOT NULL,
  `workingDays` varchar(50) default 'Monday to Friday',
  `cp_fax` varchar(50) default NULL,
  `sameday` enum('Y','N') default 'N',
  `openingTime` time default '08:30:00',
  `closingTime` time default '16:30:00',
  `operatesWeekend` enum('Y','N') default 'N',
  `weekendWorkingDays` varchar(50) default NULL,
  `weekendOpeningTime` time default NULL,
  `weekendClosingTime` time default NULL,
  `cp_contact_person_name` varchar(40) NOT NULL,
  `fromServer` varchar(20) NOT NULL,
  PRIMARY KEY  (`cp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=605 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_credit_trans`
-- 

CREATE TABLE `cm_credit_trans` (
  `cID` int(20) NOT NULL auto_increment,
  `transID` varchar(20) NOT NULL default '0',
  `cardType` varchar(15) NOT NULL default '',
  `cardNo` varchar(16) NOT NULL default '',
  `expiryDate` varchar(6) default NULL,
  `Card_CVV` varchar(4) default NULL,
  `TransactionTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `AttemptCode` varchar(4) default NULL,
  `PQTransID` varchar(17) default NULL,
  `ApprovalCode` varchar(6) default NULL,
  `ResultCode` varchar(4) default NULL,
  `ResultText` varchar(100) default NULL,
  `IPCountryCode` char(3) default NULL,
  `SenderOFAC` varchar(25) default NULL,
  `SenderOFACRecord` varchar(255) default NULL,
  `BenOFAC` varchar(25) default NULL,
  `BenOFACRecord` varchar(255) default NULL,
  `First_Name` varchar(25) NOT NULL default '',
  `Last_Name` varchar(25) NOT NULL default '',
  `Address_1` varchar(30) NOT NULL default '',
  `Address_2` varchar(30) NOT NULL default '',
  `City` varchar(25) NOT NULL default '',
  `State` char(2) NOT NULL default '',
  `Postal_Code` varchar(9) NOT NULL default '',
  `Country_Code` char(3) NOT NULL default '',
  `IP_Address` varchar(15) NOT NULL default '',
  `Telephone` varchar(15) NOT NULL default '',
  `Email` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`cID`),
  KEY `country` (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_cust_credit_card`
-- 

CREATE TABLE `cm_cust_credit_card` (
  `cID` int(20) NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `cardNo` varchar(20) NOT NULL default '0',
  `cardName` varchar(40) NOT NULL default '',
  `PIN` varchar(4) NOT NULL,
  `expiryDate` date NOT NULL default '0000-00-00',
  `firstName` varchar(20) NOT NULL default '0000-00-00',
  `lastName` varchar(20) NOT NULL default '0000-00-00 00:00:00',
  `address` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `city` varchar(40) default NULL,
  `state` varchar(40) default NULL,
  `country` varchar(40) default NULL,
  `postalCode` varchar(20) default NULL,
  `transID` varchar(50) NOT NULL,
  PRIMARY KEY  (`cID`),
  KEY `country` (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_customer`
-- 

CREATE TABLE `cm_customer` (
  `c_id` int(6) unsigned NOT NULL auto_increment,
  `c_name` varchar(50) default NULL,
  `Title` varchar(10) NOT NULL default '',
  `FirstName` varchar(20) NOT NULL default '',
  `MiddleName` varchar(20) NOT NULL default '',
  `LastName` varchar(20) NOT NULL default '',
  `status` enum('Enable','Disable') NOT NULL default 'Enable',
  `c_pass` varchar(50) default NULL,
  `c_date` date default NULL,
  `c_email` varchar(30) default NULL,
  `c_phone` varchar(40) default NULL,
  `c_mobile` varchar(40) default NULL,
  `c_address` varchar(255) default NULL,
  `c_address2` varchar(255) default NULL,
  `proveAddress` enum('Y','N') NOT NULL default 'N',
  `c_country` varchar(30) default NULL,
  `c_state` varchar(30) default NULL,
  `c_city` varchar(30) default NULL,
  `c_zip` varchar(30) default NULL,
  `c_p_question1` tinyint(6) default NULL,
  `c_answer1` varchar(50) default NULL,
  `c_p_question2` tinyint(6) default NULL,
  `c_answer2` varchar(50) default NULL,
  `c_info_source` varchar(50) default NULL,
  `c_benef_country` varchar(30) default NULL,
  `customerStatus` varchar(20) NOT NULL default '',
  `accessFromIP` varchar(50) NOT NULL default '',
  `username` varchar(50) NOT NULL default '',
  `last_login` date default NULL,
  `dateConfrimed` date NOT NULL default '0000-00-00',
  `SecuriyCode` int(5) NOT NULL default '0',
  `dateDisabled` date NOT NULL default '0000-00-00',
  `reason` varchar(200) NOT NULL default '',
  `holdAccountID` int(20) NOT NULL default '0',
  `accountType` varchar(20) NOT NULL default '',
  `dateAccepted` date NOT NULL default '0000-00-00',
  `dob` date NOT NULL default '0000-00-00',
  `placeOfBirth` varchar(50) default NULL,
  `limit1` float NOT NULL default '0',
  `Balance` float NOT NULL default '0',
  `limit3` float NOT NULL default '0',
  `determind` varchar(50) NOT NULL default '',
  `changedPwd` date NOT NULL default '0000-00-00',
  `documentProvided` varchar(100) NOT NULL,
  `remarks` varchar(256) NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2866 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cm_questions`
-- 

CREATE TABLE `cm_questions` (
  `qID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  PRIMARY KEY  (`qID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complaintInteracts`
-- 

CREATE TABLE `complaintInteracts` (
  `id` double NOT NULL auto_increment,
  `cmpID` double NOT NULL default '0',
  `agent` varchar(32) NOT NULL default '',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `comments` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=223 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complaints`
-- 

CREATE TABLE `complaints` (
  `cmpID` double NOT NULL auto_increment,
  `transID` double NOT NULL default '0',
  `agent` varchar(32) NOT NULL default '0',
  `subject` varchar(255) NOT NULL default '',
  `details` text NOT NULL,
  `status` varchar(15) NOT NULL default 'New',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `logedUserID` int(20) NOT NULL default '0',
  `logedUserName` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`cmpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=174 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complianceLists`
-- 

CREATE TABLE `complianceLists` (
  `listID` smallint(6) NOT NULL auto_increment,
  `listName` varchar(50) NOT NULL,
  `Description` varchar(150) NOT NULL,
  `LastModified` datetime NOT NULL,
  `CriticalLevel` enum('L','M','H') NOT NULL default 'H',
  PRIMARY KEY  (`listID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complianceLogic`
-- 

CREATE TABLE `complianceLogic` (
  `ruleID` bigint(20) NOT NULL auto_increment,
  `applyAt` varchar(50) NOT NULL default 'User Creation',
  `userType` varchar(50) NOT NULL,
  `matchCriteria` varchar(50) NOT NULL,
  `listID` smallint(6) NOT NULL,
  `isEnable` enum('Y','N') NOT NULL default 'Y',
  `builtByUserType` varchar(50) NOT NULL,
  `builtByUserID` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL,
  `autoBlock` enum('Y','N') NOT NULL default 'Y',
  `dated` datetime NOT NULL,
  `applyTrans` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `cumulativeFromDate` date NOT NULL default '0000-00-00',
  `cumulativeToDate` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`ruleID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complianceMatchList`
-- 

CREATE TABLE `complianceMatchList` (
  `matchListID` bigint(20) NOT NULL auto_increment,
  `compliancePersonID` bigint(20) NOT NULL,
  `listID` smallint(6) NOT NULL,
  `dated` datetime NOT NULL,
  `isFullMatch` enum('Y','N') NOT NULL default 'Y',
  `matchUserType` varchar(20) NOT NULL,
  `userDataBaseID` varchar(50) NOT NULL,
  `enteredBy` varchar(30) NOT NULL,
  `enterByID` int(11) NOT NULL,
  `isRegistered` enum('Y','N') NOT NULL default 'Y',
  `isBlocked` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`matchListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `compliancePerson`
-- 

CREATE TABLE `compliancePerson` (
  `compliancePersonID` bigint(20) NOT NULL auto_increment,
  `listID` smallint(6) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `PersonID` varchar(32) NOT NULL,
  `LastModified` datetime NOT NULL,
  `aka1` varchar(50) NOT NULL,
  `aka2` varchar(50) NOT NULL,
  `aka3` varchar(50) NOT NULL,
  `address` varchar(250) NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `dob` date NOT NULL,
  PRIMARY KEY  (`compliancePersonID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complianceQuery`
-- 

CREATE TABLE `complianceQuery` (
  `RID` bigint(20) NOT NULL auto_increment,
  `reportName` varchar(100) NOT NULL,
  `logedUser` varchar(50) NOT NULL,
  `logedUserID` varchar(50) NOT NULL,
  `queryDate` datetime NOT NULL,
  `reportLabel` varchar(100) NOT NULL,
  PRIMARY KEY  (`RID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `complianceQueryList`
-- 

CREATE TABLE `complianceQueryList` (
  `queryID` bigint(20) NOT NULL auto_increment,
  `RID` bigint(20) NOT NULL,
  `filterName` varchar(100) NOT NULL,
  `filterValue` varchar(50) NOT NULL,
  PRIMARY KEY  (`queryID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `config`
-- 

CREATE TABLE `config` (
  `id` int(11) NOT NULL auto_increment,
  `varName` varchar(100) collate utf8_unicode_ci NOT NULL,
  `value` varchar(256) collate utf8_unicode_ci NOT NULL,
  `oldValue` varchar(256) collate utf8_unicode_ci NOT NULL,
  `description` varchar(100) collate utf8_unicode_ci NOT NULL,
  `detailDesc` text collate utf8_unicode_ci NOT NULL,
  `isFlag` enum('Y','N') collate utf8_unicode_ci NOT NULL default 'N',
  `client` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `configure_commission`
-- 

CREATE TABLE `configure_commission` (
  `confID` bigint(20) NOT NULL auto_increment,
  `commRate` float NOT NULL,
  `commType` varchar(35) NOT NULL,
  `limitValue` float NOT NULL default '0',
  `lastModified` datetime NOT NULL,
  `modified_by` varchar(35) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `country` varchar(45) NOT NULL,
  PRIMARY KEY  (`confID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `countries`
-- 

CREATE TABLE `countries` (
  `countryId` bigint(20) NOT NULL auto_increment,
  `countryName` varchar(60) NOT NULL,
  `countryCode` varchar(10) NOT NULL,
  `countryType` varchar(45) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `countryRegion` varchar(100) NOT NULL,
  PRIMARY KEY  (`countryId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `countryTime`
-- 

CREATE TABLE `countryTime` (
  `timeDiff_id` bigint(20) NOT NULL auto_increment,
  `countryCode` varchar(10) NOT NULL,
  `countryName` varchar(100) NOT NULL,
  `timeDiff` varchar(100) NOT NULL default '00:00',
  PRIMARY KEY  (`timeDiff_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=154 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `currencies`
-- 

CREATE TABLE `currencies` (
  `cID` bigint(20) NOT NULL auto_increment,
  `country` varchar(45) NOT NULL default '',
  `currencyName` varchar(200) NOT NULL default '',
  `numCode` int(11) NOT NULL default '0',
  `description` varchar(60) NOT NULL default '',
  `currencyType` varchar(20) NOT NULL,
  PRIMARY KEY  (`cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=277 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer`
-- 

CREATE TABLE `customer` (
  `customerID` bigint(20) unsigned NOT NULL auto_increment,
  `agentID` bigint(20) unsigned NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `Title` varchar(10) NOT NULL default '',
  `firstName` varchar(25) NOT NULL default '',
  `middleName` varchar(25) NOT NULL default '',
  `lastName` varchar(25) NOT NULL default '',
  `SOF` varchar(250) NOT NULL,
  `accountName` varchar(32) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `customerNumber` int(10) unsigned NOT NULL default '0',
  `Address` varchar(255) NOT NULL default '',
  `Address1` varchar(255) NOT NULL default '',
  `proveAddress` enum('Y','N') NOT NULL default 'N',
  `City` varchar(100) NOT NULL default '',
  `State` varchar(50) NOT NULL default '',
  `Zip` varchar(16) NOT NULL default '',
  `Country` varchar(100) NOT NULL default '',
  `Phone` varchar(32) NOT NULL default '',
  `Mobile` varchar(32) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `dob` date NOT NULL default '0000-00-00',
  `placeOfBirth` varchar(50) default NULL,
  `acceptedTerms` enum('Y','N') NOT NULL default 'Y',
  `IDType` varchar(100) NOT NULL default '',
  `IDNumber` varchar(32) NOT NULL default '',
  `IDExpiry` date NOT NULL default '0000-00-00',
  `IDissuedate` date NOT NULL default '0000-00-00',
  `issuedBy` varchar(255) NOT NULL default '',
  `documentProvided` enum('Y','N') NOT NULL,
  `remarks` varchar(256) NOT NULL,
  `fundsSources` varchar(100) NOT NULL default '',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `other_title` varchar(30) NOT NULL default '',
  `otherId` varchar(30) NOT NULL default '',
  `otherId_name` varchar(30) NOT NULL default '',
  `payinBook` varchar(35) default NULL,
  `balance` float NOT NULL,
  `customerName` varchar(50) NOT NULL,
  `addressDescription` varchar(250) NOT NULL,
  `accumulativeAmount` bigint(250) default NULL,
  `customerStatus` enum('Enable','Disable') NOT NULL default 'Enable',
  `disableReason` text,
  `addressApprovedVia` varchar(200) NOT NULL,
  `addressProvedOn` date NOT NULL,
  `isCompliance` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`customerID`),
  KEY `firstName` (`firstName`),
  KEY `lastName` (`lastName`),
  KEY `IDNumber` (`IDNumber`),
  KEY `IDNumber_2` (`IDNumber`),
  KEY `Mobile` (`Mobile`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=186666 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer_account`
-- 

CREATE TABLE `customer_account` (
  `caID` bigint(20) unsigned NOT NULL auto_increment,
  `customerID` bigint(20) NOT NULL default '0',
  `Date` date NOT NULL default '0000-00-00',
  `tranRefNo` varchar(30) NOT NULL default '',
  `payment_mode` varchar(50) NOT NULL default '0',
  `Type` varchar(10) NOT NULL default '0',
  `amount` float NOT NULL default '0',
  `modifiedBy` varchar(35) NOT NULL,
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`caID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `customer_docs`
-- 

CREATE TABLE `customer_docs` (
  `ID` bigint(20) NOT NULL auto_increment,
  `docName` varchar(100) NOT NULL default '',
  `details` varchar(200) NOT NULL default '',
  `filepath` varchar(100) NOT NULL default '',
  `customerID` bigint(20) NOT NULL default '0',
  `docDate` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `document_category`
-- 

CREATE TABLE `document_category` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(500) NOT NULL,
  `categoryId` varchar(250) NOT NULL,
  `documentId` bigint(15) NOT NULL,
  `tableField` varchar(250) NOT NULL,
  `payexTable` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `document_names`
-- 

CREATE TABLE `document_names` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `document_upload`
-- 

CREATE TABLE `document_upload` (
  `id` bigint(20) NOT NULL auto_increment,
  `documentId` varchar(250) NOT NULL,
  `userId` varchar(250) NOT NULL,
  `path` varchar(500) NOT NULL,
  `status` varchar(250) NOT NULL,
  `isUploaded` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `email`
-- 

CREATE TABLE `email` (
  `id` bigint(120) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `attachment` varchar(250) NOT NULL,
  `textBody` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `emailAlert`
-- 

CREATE TABLE `emailAlert` (
  `id` int(120) NOT NULL auto_increment,
  `report` varchar(240) NOT NULL,
  `day` varchar(240) NOT NULL,
  `message` text NOT NULL,
  `isEnable` enum('Y','N') NOT NULL,
  `client` varchar(240) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `emailReminder`
-- 

CREATE TABLE `emailReminder` (
  `id` int(120) NOT NULL auto_increment,
  `agentID` varchar(250) NOT NULL,
  `messageId` varchar(250) NOT NULL,
  `email` int(120) NOT NULL,
  `reminder` int(120) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `email` (`email`),
  KEY `reminder` (`reminder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exRateUsed`
-- 

CREATE TABLE `exRateUsed` (
  `rateUsedID` bigint(20) NOT NULL auto_increment,
  `transID` bigint(20) NOT NULL,
  `rateWithMargin` float NOT NULL,
  `rateWithoutMargin` float NOT NULL,
  `agentMargin` float NOT NULL,
  `companyMargin` float NOT NULL,
  `currencyFrom` varchar(10) NOT NULL,
  `currencyTo` varchar(10) NOT NULL,
  PRIMARY KEY  (`rateUsedID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `exchangerate`
-- 

CREATE TABLE `exchangerate` (
  `erID` bigint(20) unsigned NOT NULL auto_increment,
  `country` varchar(100) NOT NULL default '',
  `primaryExchange` float NOT NULL default '0',
  `sProvider` varchar(150) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `currency` varchar(10) NOT NULL default '',
  `marginPercentage` float NOT NULL default '1',
  `rateFor` varchar(40) NOT NULL default '',
  `rateValue` varchar(75) NOT NULL default '',
  `updationDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `countryOrigin` varchar(100) NOT NULL,
  `currencyOrigin` varchar(10) NOT NULL,
  `marginType` enum('percent','fixed') NOT NULL default 'percent',
  `agentMarginType` varchar(15) NOT NULL,
  `agentMarginValue` float NOT NULL,
  `CalculatedPrimaryRate` double NOT NULL,
  `intermediateCurrency` varchar(20) NOT NULL,
  `intermediatePrimRate` double NOT NULL,
  `intermediateSecondaryRate` double NOT NULL,
  `relatedID` varchar(20) NOT NULL,
  `isActive` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`erID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=941 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `faqs`
-- 

CREATE TABLE `faqs` (
  `faqID` bigint(20) NOT NULL auto_increment,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY  (`faqID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `forum`
-- 

CREATE TABLE `forum` (
  `cmpID` int(32) NOT NULL auto_increment,
  `parentID` int(32) default '0',
  `transID` int(32) default '0',
  `agent` varchar(32) default NULL,
  `emails` varchar(255) default NULL,
  `subject` varchar(255) default NULL,
  `details` text,
  `status` enum('New','Close') default 'New',
  `dated` datetime default '0000-00-00 00:00:00',
  `logedUserID` int(20) default '0',
  `logedUserName` varchar(100) default NULL,
  `enquiryNumber` varchar(50) default NULL,
  PRIMARY KEY  (`cmpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Created by Niaz Ahmad for Rich Almond #2052 at 10-09-2007' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `forum_thread`
-- 

CREATE TABLE `forum_thread` (
  `id` int(32) NOT NULL auto_increment,
  `cmpID` int(32) default '0',
  `userID` int(32) default NULL,
  `userTbl` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Created by Niaz Ahmad for Rich Almond #2052 at 10-09-2007' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `historicalExchangeRate`
-- 

CREATE TABLE `historicalExchangeRate` (
  `id` bigint(20) NOT NULL auto_increment,
  `country` varchar(100) NOT NULL,
  `primaryExchange` float(8,4) NOT NULL default '0.0000',
  `sProvider` varchar(150) NOT NULL,
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `currency` varchar(10) NOT NULL,
  `marginPercentage` float NOT NULL,
  `rateFor` varchar(40) NOT NULL,
  `rateValue` varchar(75) NOT NULL,
  `updationDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `countryOrigin` varchar(100) NOT NULL,
  `currencyOrigin` varchar(10) NOT NULL,
  `marginType` enum('percent','fixed') NOT NULL,
  `intermediateCurrency` varchar(20) NOT NULL,
  `intermediatePrimRate` double NOT NULL,
  `agentMarginType` varchar(15) NOT NULL,
  `agentMarginValue` float NOT NULL,
  `relatedID` varchar(20) NOT NULL,
  `erID` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=5145 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `homeDeliveryService`
-- 

CREATE TABLE `homeDeliveryService` (
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `service` varchar(30) NOT NULL default 'Home Delivery',
  `Active` varchar(10) NOT NULL default 'No',
  `dist_ID` bigint(10) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table `imbanks`
-- 

CREATE TABLE `imbanks` (
  `id` int(11) NOT NULL auto_increment,
  `bankName` varchar(150) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `bank_address` varchar(255) NOT NULL default '',
  `dd_code` varchar(20) NOT NULL default '',
  `dd_locations` varchar(50) NOT NULL default '',
  `bankCode` varchar(15) NOT NULL default '0',
  `branchName` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `imfee`
-- 

CREATE TABLE `imfee` (
  `feeID` bigint(20) unsigned NOT NULL auto_increment,
  `origCountry` varchar(100) NOT NULL default '',
  `destCountry` varchar(100) NOT NULL default '',
  `amountRangeFrom` float NOT NULL default '0',
  `amountRangeTo` float NOT NULL default '0',
  `rangeToWithFee` float NOT NULL default '0',
  `Fee` float NOT NULL default '0',
  `feeType` varchar(30) NOT NULL,
  `payinFee` float NOT NULL default '0',
  `agentNo` bigint(20) NOT NULL default '0',
  `manageID` bigint(20) NOT NULL,
  `feeBasedOn` varchar(100) NOT NULL,
  `transactionType` varchar(100) NOT NULL,
  `currencyOrigin` varchar(10) NOT NULL,
  `currencyDest` varchar(10) NOT NULL,
  `isActive` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`feeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `importConnectplusTrans`
-- 

CREATE TABLE `importConnectplusTrans` (
  `importID` bigint(20) NOT NULL auto_increment,
  `consecValue` varchar(50) NOT NULL,
  `transRef` varchar(50) NOT NULL,
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `deseciveValue` varchar(50) NOT NULL,
  `beneID` varchar(35) NOT NULL,
  `beneName` varchar(65) NOT NULL,
  `amountGot` float NOT NULL,
  `messageText` varchar(150) NOT NULL,
  `notes` varchar(50) NOT NULL,
  `isResolved` enum('Y','N') NOT NULL default 'N',
  `importDated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`importID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `jointClients`
-- 

CREATE TABLE `jointClients` (
  `clientId` smallint(6) NOT NULL auto_increment,
  `clientName` varchar(75) NOT NULL,
  `serverAddress` varchar(150) NOT NULL default 'localHost',
  `userName` varchar(75) NOT NULL,
  `password` varchar(75) NOT NULL,
  `dataBaseName` varchar(75) NOT NULL,
  `isEnabled` enum('Y','N') NOT NULL default 'N',
  `baseAgentId` int(11) NOT NULL,
  `baseDistributor` int(11) NOT NULL,
  `baseAnD` int(11) NOT NULL,
  `commissionShare` float NOT NULL,
  `commissionShareType` varchar(30) NOT NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `login_activities`
-- 

CREATE TABLE `login_activities` (
  `activity_id` bigint(20) NOT NULL auto_increment,
  `login_history_id` bigint(20) NOT NULL,
  `activity` varchar(254) NOT NULL,
  `activity_time` datetime NOT NULL,
  `action_for` varchar(45) NOT NULL,
  `table_name` varchar(60) NOT NULL,
  `description` varchar(254) NOT NULL,
  PRIMARY KEY  (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1484316 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `login_history`
-- 

CREATE TABLE `login_history` (
  `history_id` bigint(20) NOT NULL auto_increment,
  `login_time` datetime NOT NULL,
  `login_name` varchar(45) NOT NULL,
  `access_ip` varchar(140) NOT NULL,
  `login_type` varchar(60) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=88434 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `manageFee`
-- 

CREATE TABLE `manageFee` (
  `manageFeeID` bigint(20) unsigned NOT NULL auto_increment,
  `origCountry` varchar(100) NOT NULL default '',
  `destCountry` varchar(100) NOT NULL default '',
  `amountRangeFrom` float NOT NULL default '0',
  `amountRangeTo` float NOT NULL default '0',
  `rangeToWithFee` float NOT NULL default '0',
  `regularFee` float NOT NULL default '0',
  `feeType` varchar(30) NOT NULL default '',
  `payinFee` float NOT NULL default '0',
  `agentNo` bigint(20) NOT NULL,
  `intervalUsed` int(11) NOT NULL,
  `feeBasedOn` varchar(100) NOT NULL,
  `transactionType` varchar(100) NOT NULL,
  `currencyOrigin` varchar(10) NOT NULL,
  `currencyDest` varchar(10) NOT NULL,
  `isActive` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`manageFeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `natwestpayments`
-- 

CREATE TABLE `natwestpayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `isDeleted` enum('N','Y') NOT NULL default 'N',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `tlaCode` varchar(5) NOT NULL default '',
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=26724 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `profit_earning`
-- 

CREATE TABLE `profit_earning` (
  `profit_id` bigint(20) NOT NULL auto_increment,
  `entry_time` datetime NOT NULL,
  `origin_country` varchar(60) NOT NULL,
  `dest_country` varchar(60) NOT NULL,
  `origin_amount` double NOT NULL,
  `origin_currency` varchar(10) NOT NULL,
  `intermediate_rate` float NOT NULL,
  `intermediate_curr` varchar(10) NOT NULL,
  `send_rate_inter_to_dest` float NOT NULL,
  `dest_currency` varchar(10) NOT NULL,
  `bank_charges` float NOT NULL,
  `reciev_rate_inter_to_dest` float NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY  (`profit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `receipt_range`
-- 

CREATE TABLE `receipt_range` (
  `id` int(11) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `rangeFrom` float NOT NULL default '0',
  `rangeTo` float NOT NULL default '0',
  `prefix` varchar(20) NOT NULL,
  `used` varchar(30) NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `receiptbook`
-- 

CREATE TABLE `receiptbook` (
  `bookID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL,
  `issueDate` datetime NOT NULL,
  `bookNo` varchar(30) NOT NULL,
  `bookRange` varchar(100) NOT NULL,
  `seriesNo` varchar(30) NOT NULL,
  PRIMARY KEY  (`bookID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `ref_generator`
-- 

CREATE TABLE `ref_generator` (
  `id` int(11) NOT NULL auto_increment,
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `origCountries` varchar(256) NOT NULL default 'All',
  `prefix` varchar(200) NOT NULL,
  `customPrefix` varchar(10) NOT NULL,
  `transType` varchar(100) NOT NULL default 'All',
  `moneyPaid` varchar(100) NOT NULL default 'All',
  `formula` varchar(100) NOT NULL,
  `customFormula` varchar(20) NOT NULL,
  `customLength` int(11) NOT NULL default '0',
  `postFix` varchar(100) NOT NULL,
  `customPostFix` varchar(50) NOT NULL,
  `incementBased` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `relatedExchangeRate`
-- 

CREATE TABLE `relatedExchangeRate` (
  `id` varchar(20) NOT NULL,
  `DefaultSendingCurrency` varchar(20) NOT NULL,
  `defaultRecievingCurrency` varchar(20) NOT NULL,
  `defaultSendingCountry` varchar(50) NOT NULL,
  `defaultRecievingCountry` varchar(50) NOT NULL,
  `erID` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `reminder`
-- 

CREATE TABLE `reminder` (
  `id` int(11) NOT NULL,
  `day` varchar(250) NOT NULL,
  `msg` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table `reminderReport`
-- 

CREATE TABLE `reminderReport` (
  `id` int(11) NOT NULL,
  `report` varchar(250) NOT NULL,
  `reminder` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `reminder` (`reminder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table `reusedDistRef`
-- 

CREATE TABLE `reusedDistRef` (
  `id` int(11) NOT NULL auto_increment,
  `refNumber` varchar(35) NOT NULL,
  `distributor` int(11) NOT NULL,
  `transFrom` bigint(20) NOT NULL,
  `transTo` bigint(20) NOT NULL,
  `datedGot` datetime NOT NULL default '0000-00-00 00:00:00',
  `datedAssign` datetime NOT NULL default '0000-00-00 00:00:00',
  `reason` varchar(150) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `services`
-- 

CREATE TABLE `services` (
  `serviceId` bigint(20) NOT NULL auto_increment,
  `fromCountryId` bigint(20) NOT NULL,
  `toCountryId` bigint(20) NOT NULL,
  `serviceAvailable` varchar(255) NOT NULL,
  `deliveryTime` varchar(100) NOT NULL,
  `bankCharges` float NOT NULL,
  `outCurrCharges` float NOT NULL,
  `currencyRec` varchar(25) NOT NULL,
  PRIMARY KEY  (`serviceId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sessions`
-- 

CREATE TABLE `sessions` (
  `username` varchar(25) NOT NULL,
  `session_id` varchar(32) NOT NULL default '',
  `session_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ip` varchar(20) NOT NULL default '',
  `cookie_remember` enum('N','Y') NOT NULL default 'N',
  `is_online` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Table structure for table `settlementAmount`
-- 

CREATE TABLE `settlementAmount` (
  `SID` bigint(20) NOT NULL auto_increment,
  `settlementCurrency` varchar(30) NOT NULL,
  `currencyFrom` varchar(30) NOT NULL,
  `settleAmount` varchar(30) NOT NULL,
  `agentID` bigint(20) NOT NULL,
  `transID` bigint(20) NOT NULL,
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`SID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `shareConfig`
-- 

CREATE TABLE `shareConfig` (
  `shareConfigID` bigint(20) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `shareType` varchar(25) NOT NULL,
  `shareValue` float NOT NULL,
  `lastModified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `userType` varchar(35) NOT NULL,
  `shareFrom` varchar(25) NOT NULL default 'company',
  PRIMARY KEY  (`shareConfigID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `shareCountries`
-- 

CREATE TABLE `shareCountries` (
  `shareId` int(11) NOT NULL auto_increment,
  `countryId` int(11) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  PRIMARY KEY  (`shareId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `shareProfit`
-- 

CREATE TABLE `shareProfit` (
  `profitShareID` bigint(20) NOT NULL auto_increment,
  `transID` bigint(20) NOT NULL,
  `exchRateUsed` float NOT NULL,
  `exchRateActual` float NOT NULL,
  `currencyTo` varchar(10) NOT NULL,
  `agentMargin` float NOT NULL,
  `agentID` bigint(20) NOT NULL,
  `calculationTime` datetime NOT NULL,
  `actAs` varchar(35) NOT NULL,
  PRIMARY KEY  (`profitShareID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sharedTransactions`
-- 

CREATE TABLE `sharedTransactions` (
  `sharedTransId` bigint(20) NOT NULL auto_increment,
  `localTrans` bigint(20) NOT NULL,
  `remoteTrans` bigint(20) NOT NULL,
  `generatedLocally` enum('Y','N') NOT NULL,
  `remoteServerId` smallint(6) NOT NULL,
  `totalComm` float NOT NULL,
  `remoteComm` float NOT NULL,
  PRIMARY KEY  (`sharedTransId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sharedUsers`
-- 

CREATE TABLE `sharedUsers` (
  `id` smallint(6) NOT NULL auto_increment,
  `localServerId` int(11) NOT NULL,
  `availableAt` smallint(6) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  `availableAs` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=640 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `shortcuts`
-- 

CREATE TABLE `shortcuts` (
  `shortcutID` int(11) NOT NULL auto_increment,
  `link` varchar(256) NOT NULL,
  `link_title` varchar(256) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY  (`shortcutID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sub_account_summary`
-- 

CREATE TABLE `sub_account_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` varchar(20) NOT NULL,
  `dated` date NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `currency` varchar(10) NOT NULL default 'GBP',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sub_agent_Dist_account`
-- 

CREATE TABLE `sub_agent_Dist_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `actAs` varchar(20) NOT NULL default 'Agent',
  `currency` varchar(10) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sub_agent_account`
-- 

CREATE TABLE `sub_agent_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `sub_bank_account`
-- 

CREATE TABLE `sub_bank_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `bankID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `currency` varchar(10) NOT NULL,
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  `settlementCurrency` varchar(20) NOT NULL,
  `settlementValue` varchar(50) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_export_fields`
-- 

CREATE TABLE `tbl_export_fields` (
  `id` bigint(20) NOT NULL auto_increment,
  `fileID` bigint(20) NOT NULL,
  `payex_field` varchar(100) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `isActive` enum('Y','N') NOT NULL default 'N',
  `Lable` varchar(100) NOT NULL,
  `tempID` int(11) NOT NULL,
  `exportDefaultID` int(11) NOT NULL,
  `field_title` varchar(100) NOT NULL,
  `isFixed` enum('Y','N') NOT NULL default 'N',
  `Value` varchar(250) NOT NULL,
  `fieldSperator` varchar(50) NOT NULL,
  `isPattern` enum('Y','N') NOT NULL default 'N',
  `patternType` varchar(150) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_export_file`
-- 

CREATE TABLE `tbl_export_file` (
  `id` bigint(20) NOT NULL auto_increment,
  `File_Name` varchar(100) NOT NULL,
  `isEnable` enum('Yes','No') NOT NULL default 'Yes',
  `Format` enum('xls','csv','txt') NOT NULL default 'csv',
  `FileLable` varchar(100) NOT NULL,
  `fieldSpacing` varchar(100) NOT NULL,
  `lineBreak` varchar(100) NOT NULL,
  `client` text character set latin1 NOT NULL,
  `conditions` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `showLable` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_export_labels`
-- 

CREATE TABLE `tbl_export_labels` (
  `id` int(250) NOT NULL auto_increment,
  `fileID` bigint(20) NOT NULL,
  `heading` text NOT NULL,
  `address` enum('Y','N') NOT NULL default 'Y',
  `label1` text NOT NULL,
  `label2` text NOT NULL,
  `label3` text NOT NULL,
  `client` varchar(250) NOT NULL,
  `logo` enum('Y','N') NOT NULL default 'Y',
  `Enable` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `teller`
-- 

CREATE TABLE `teller` (
  `tellerID` bigint(20) NOT NULL auto_increment,
  `collection_point` bigint(20) NOT NULL,
  `loginName` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` varchar(10) NOT NULL default 'Active',
  `Reason` text NOT NULL,
  `isMain` varchar(5) NOT NULL default 'No',
  `changedPwd` datetime NOT NULL,
  `account_limit` varchar(30) NOT NULL,
  `limit_alert` varchar(30) NOT NULL,
  `balance` bigint(20) NOT NULL default '0',
  `accessFromIP` varchar(255) NOT NULL,
  `rights` varchar(240) NOT NULL,
  `email` varchar(100) default NULL,
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`tellerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `teller_account`
-- 

CREATE TABLE `teller_account` (
  `taID` bigint(20) NOT NULL auto_increment,
  `tellerID` bigint(20) NOT NULL default '0',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL default '',
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`taID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=216612 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `teller_account_limit`
-- 

CREATE TABLE `teller_account_limit` (
  `id` bigint(20) NOT NULL auto_increment,
  `teller_id` int(11) NOT NULL,
  `limit_date` date NOT NULL,
  `account_limit` varchar(30) NOT NULL,
  `used_limit` varchar(30) NOT NULL,
  `limit_alert` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=18684 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `temp_account`
-- 

CREATE TABLE `temp_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL default '',
  `note` varchar(256) NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=8280 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `temp_bank_account`
-- 

CREATE TABLE `temp_bank_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `bankID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) collate utf8_unicode_ci NOT NULL default '',
  `amount` double NOT NULL default '0',
  `currency` varchar(10) collate utf8_unicode_ci NOT NULL,
  `modified_by` varchar(30) collate utf8_unicode_ci NOT NULL default '0',
  `TransID` varchar(20) collate utf8_unicode_ci NOT NULL default '0',
  `description` varchar(90) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`aaID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `trans_limit`
-- 

CREATE TABLE `trans_limit` (
  `limitID` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `limit_type` enum('Transaction','Days') NOT NULL default 'Days',
  `duration` int(11) NOT NULL,
  `limitValue` float NOT NULL,
  `updation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `apply_date` date NOT NULL,
  `currency` varchar(10) NOT NULL,
  PRIMARY KEY  (`limitID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `transactions`
-- 

CREATE TABLE `transactions` (
  `transID` bigint(20) unsigned NOT NULL auto_increment,
  `transType` enum('Pick up','Bank Transfer','Home Delivery','ATM Card') default NULL,
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `benID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `benAgentID` bigint(20) NOT NULL default '0',
  `exchangeID` bigint(20) unsigned NOT NULL default '0',
  `refNumber` varchar(32) NOT NULL default '',
  `refNumberIM` varchar(50) NOT NULL default '',
  `distRefNumber` varchar(60) NOT NULL,
  `transAmount` float NOT NULL default '0',
  `exchangeRate` float NOT NULL default '0',
  `localAmount` double NOT NULL default '0',
  `IMFee` float NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `totalAmount` float NOT NULL default '0',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `fundSources` varchar(100) NOT NULL default '',
  `moneyPaid` varchar(100) NOT NULL default '',
  `Declaration` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `verifiedBy` varchar(30) NOT NULL default '',
  `authorisedBy` varchar(30) NOT NULL default '',
  `cancelledBy` varchar(30) NOT NULL default '',
  `recalledBy` varchar(20) NOT NULL default '',
  `isSent` enum('Y','N') NOT NULL default 'N',
  `trackingNum` varchar(30) NOT NULL default '',
  `trans4Country` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `authoriseDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryOutDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspendDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rejectDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `failedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspeciousDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `recalledDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fromCountry` varchar(30) NOT NULL default '',
  `toCountry` varchar(30) NOT NULL default '',
  `currencyFrom` varchar(10) NOT NULL default '',
  `currencyTo` varchar(10) NOT NULL default '',
  `custAgentParentID` int(11) NOT NULL default '0',
  `benAgentParentID` int(11) NOT NULL default '0',
  `is_exported` varchar(10) NOT NULL default '',
  `PINCODE` int(5) NOT NULL default '0',
  `createdBy` varchar(10) NOT NULL default '',
  `collectionPointID` bigint(20) NOT NULL default '0',
  `transRefID` varchar(30) NOT NULL default '',
  `AgentComm` float NOT NULL default '0',
  `CommType` varchar(30) NOT NULL default '',
  `other_pur` varchar(35) NOT NULL default '',
  `admincharges` varchar(30) NOT NULL default '',
  `cashCharges` float NOT NULL default '0',
  `refundFee` varchar(10) NOT NULL default '',
  `question` varchar(100) NOT NULL default '',
  `answer` varchar(200) NOT NULL default '',
  `tip` text,
  `holdedBy` varchar(20) NOT NULL default '',
  `outCurrCharges` float NOT NULL default '0',
  `distributorComm` float NOT NULL default '0',
  `discountRequest` varchar(255) NOT NULL default '',
  `discountType` varchar(255) NOT NULL default '',
  `discounted_amount` float NOT NULL default '0',
  `creation_date_used` date NOT NULL,
  `holdDate` datetime NOT NULL,
  `verificationDate` datetime NOT NULL,
  `unholdBy` varchar(40) NOT NULL,
  `unholdDate` datetime NOT NULL,
  `deliveredBy` varchar(45) NOT NULL,
  `internalRemarks` text NOT NULL,
  `distCommPackage` varchar(30) NOT NULL,
  `benIDPassword` varchar(250) NOT NULL,
  `agentExchangeRate` enum('Y','N') NOT NULL default 'N',
  `commissionMode` varchar(100) NOT NULL,
  `payPointType` varchar(50) NOT NULL,
  `returnDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `returnedBy` varchar(30) NOT NULL,
  `settlementCurrency` varchar(20) NOT NULL,
  `settlementValue` varchar(20) NOT NULL,
  `senderBankBranch` varchar(50) NOT NULL,
  `restoreDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `restoredBy` varchar(30) NOT NULL,
  `statusChangedBy` varchar(30) NOT NULL,
  `statusChangedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `settlementRate` varchar(50) NOT NULL,
  PRIMARY KEY  (`transID`),
  KEY `custAgentID` (`custAgentID`),
  KEY `benID` (`benID`),
  KEY `createdBy` (`createdBy`),
  KEY `customerID` (`customerID`),
  KEY `transDate` (`transDate`),
  KEY `transStatus` (`transStatus`),
  KEY `refNumberIM` (`refNumberIM`),
  KEY `refNumber` (`refNumber`),
  KEY `custAgentParentID` (`custAgentParentID`),
  KEY `cancelDate` (`cancelDate`),
  KEY `deliveredBy` (`deliveredBy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=275172 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `uc_job_queue`
-- 

CREATE TABLE `uc_job_queue` (
  `id` int(11) NOT NULL auto_increment,
  `action` varchar(30) NOT NULL,
  `ident` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL,
  `error_code` int(11) NOT NULL,
  `error_text` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `action` (`action`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=69103 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `userID` bigint(20) NOT NULL auto_increment,
  `accountID` bigint(20) NOT NULL,
  `userName` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(32) NOT NULL,
  `lastModified` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastModifiedBy` varchar(32) NOT NULL,
  `accessFromIP` varchar(255) default NULL,
  `isMain` enum('N','Y') default 'N',
  `isAdmin` enum('N','Y') default 'N',
  PRIMARY KEY  (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `user_functionality`
-- 

CREATE TABLE `user_functionality` (
  `func_id` bigint(20) NOT NULL auto_increment,
  `functionality` varchar(254) NOT NULL,
  `functionCode` varchar(25) NOT NULL,
  `users` varchar(254) NOT NULL,
  PRIMARY KEY  (`func_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `xml_Errors`
-- 

CREATE TABLE `xml_Errors` (
  `errorId` bigint(20) NOT NULL auto_increment,
  `code` varchar(15) NOT NULL,
  `description` varchar(100) NOT NULL,
  `lastModified` datetime NOT NULL,
  PRIMARY KEY  (`errorId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
