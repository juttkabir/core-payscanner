-- Ticket #3790 Global Exchange Cash Book Report
-- Added by Waqas Bin Hasan on September 26, 2008 1653hrs
CREATE TABLE IF NOT EXISTS `cash_book_global_exchange` (
`recId` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`openingBalance` DOUBLE( 13, 4 ) NOT NULL ,
`closingBalance` DOUBLE( 13, 4 ) NOT NULL ,
`banked` DOUBLE( 11, 4 ) NOT NULL ,
`cheques` DOUBLE( 11, 4 ) NOT NULL ,
`othersDr` DOUBLE( 11, 4 ) NOT NULL ,
`othersCr` DOUBLE( 11, 4 ) NOT NULL ,
`created` DATE NOT NULL ,
`updated` DATE NOT NULL
);

-- Ticket #3968 Global Exchange Cash Book Enhancements
-- Added by Waqas Bin Hasan on October 09, 2008 1825hrs
CREATE TABLE IF NOT EXISTS `banks_lookup` (
 `recId` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `bankId` VARCHAR( 10 ) NOT NULL ,
 `bankName` VARCHAR( 50 ) NOT NULL ,
 `created` DATETIME NOT NULL ,
 `updated` DATETIME NOT NULL,
 UNIQUE (
  `bankId`
 )
);

CREATE TABLE IF NOT EXISTS `couriers_lookup` (
 `recId` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `courierId` VARCHAR( 10 ) NOT NULL ,
 `courierName` VARCHAR( 50 ) NOT NULL ,
 `created` DATETIME NOT NULL ,
 `updated` DATETIME NOT NULL ,
 UNIQUE (
  `courierId`
 )
);

CREATE TABLE IF NOT EXISTS `cash_book_map_global_exchange` (
 `recId` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `cashBookId` INT( 11 ) NOT NULL ,
 `bankId` VARCHAR( 10 ) NOT NULL ,
 `courierId` VARCHAR( 10 ) NOT NULL ,
 `voucherNumber` VARCHAR( 20 ) NOT NULL ,
 `bankAmount` DOUBLE( 11, 4 ) NOT NULL
);

