


-- 
-- Dumping data for table `tbl_export_file`
-- 

INSERT INTO `tbl_export_file` (`id`, `File_Name`, `isEnable`, `Format`, `FileLable`, `fieldSpacing`, `lineBreak`, `client`) VALUES (1, 'export_trans_file.php', 'Yes', 'xls', 'Export Transactions', 'space', '', 'bayba,beaconcrest'),
(2, 'export_trans_file.php', 'Yes', 'csv', 'Export Transactions', 'space', '', 'bayba,beaconcrest'),
(3, 'Customer_Data_Output.php', 'Yes', 'txt', 'Export Customer Data', 'space', 'CRLF', 'express');






-- 
-- Dumping data for table `tbl_export_fields`
-- 

INSERT INTO `tbl_export_fields` (`id`, `fileID`, `payex_field`, `tableName`, `isActive`, `Lable`, `tempID`, `exportDefaultID`, `field_title`) VALUES (1, 1, 'refNumberIM', 'transactions', 'Y', 'Order Number', 0, 0, ''),
(2, 1, 'transDate', 'transactions', 'Y', 'Remittance Date', 0, 0, ''),
(4, 1, 'transAmount', 'transactions', 'Y', 'Dollar Amount', 0, 0, ''),
(5, 1, 'exchangeRate', 'transactions', 'Y', 'test rate', 0, 0, ''),
(6, 1, 'localAmount', 'transactions', 'Y', 'Real Amount', 0, 0, ''),
(7, 1, 'firstName', 'customer', 'Y', 'Sender Name', 0, 0, ''),
(9, 1, 'firstName', 'beneficiary', 'Y', 'Beneficiary Name', 0, 0, ''),
(11, 1, 'Address', 'beneficiary', 'Y', 'Beneficiary Address', 0, 0, ''),
(13, 1, 'City', 'beneficiary', 'Y', 'Beneficiary City', 0, 0, ''),
(15, 1, 'State', 'beneficiary', 'Y', 'Beneficiary State', 0, 0, ''),
(17, 1, 'Phone', 'beneficiary', 'Y', 'Beneficiary Telephone', 0, 0, ''),
(19, 1, 'CPF', 'beneficiary', 'Y', 'CPF', 0, 0, ''),
(21, 1, 'bankNumber', 'bank_data', 'Y', 'Bank Number', 0, 0, ''),
(23, 1, 'bankName', 'bankDetails', 'Y', 'Bank Name', 0, 0, ''),
(25, 1, 'branchCode', 'bankDetails', 'Y', 'Branch', 0, 0, ''),
(27, 1, 'accNo', 'bankDetails', 'Y', 'Account Number', 0, 0, ''),
(29, 1, 'accountType', 'bankDetails', 'Y', 'Account Type', 0, 0, ''),
(48, 2, 'Remarks', 'bankDetails', 'Y', 'Comment box to have maximum 1257', 0, 0, ''),
(46, 2, 'currencyTo', 'transactions', 'Y', 'currency code ISO', 0, 0, ''),
(47, 2, 'IBAN', 'bankDetails', 'Y', 'Account Number', 0, 0, ''),
(45, 2, 'localAmount', 'transactions', 'Y', 'Amount', 0, 0, ''),
(44, 2, 'firstName,lastName', 'beneficiary', 'Y', 'Beneficiary name and surname', 0, 0, ''),
(43, 2, 'cp_corresspondent_name', 'cm_collection_point', 'Y', 'Beneficiary collection point branch name', 0, 0, ''),
(42, 2, 'initSenderPerCode', 'customer', 'Y', 'Initial sender personal code', 0, 0, ''),
(41, 2, 'initSendNameSurname', 'customer', 'Y', 'Initial sender name and surname', 0, 0, ''),
(39, 2, 'clientCode', 'customer', 'Y', 'Client code in system', 0, 0, ''),
(40, 2, 'initPaymentNum', 'customer', 'Y', 'Initial payment account number', 0, 0, ''),
(38, 2, 'BICcode', 'customer', 'Y', 'BIC code', 0, 0, ''),
(37, 2, 'registrationNumber', 'customer', 'Y', 'Sender Code Registartion number', 0, 0, ''),
(35, 2, 'accountName', 'customer', 'Y', 'Sender Number', 0, 0, ''),
(36, 2, 'firstName,lastName', 'customer', 'Y', 'Sender Name', 0, 0, ''),
(34, 2, 'PaymentNumber', 'transactions', 'Y', 'Payment Number', 0, 0, ''),
(33, 2, 'transDate', 'transactions', 'Y', 'Bank Operation Date', 0, 0, ''),
(32, 2, 'Date', 'transactions', 'Y', 'Date', 0, 0, ''),
(31, 2, 'refNumberIM', 'transactions', 'Y', 'Numeris', 0, 0, ''),
(49, 3, 'created', 'customer', 'Y', 'Date', 0, 0, ''),
(50, 3, 'accountName', 'customer', 'Y', 'User Name', 0, 0, ''),
(52, 3, 'firstName', 'customer', 'Y', 'First Name', 0, 0, ''),
(55, 3, 'Mobile', 'Customer', 'Y', 'Mobile Number', 0, 0, ''),
(54, 3, 'lastName', 'customer', 'Y', 'Last Name', 0, 0, ''),
(51, 3, 'Title', 'customer', 'Y', 'Title', 0, 0, ''),
(53, 3, 'middleName', 'customer', 'Y', 'Middle Name', 0, 0, '');



