CREATE TABLE `emailAlert` (
`id` INT( 120 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`report` VARCHAR( 240 ) NOT NULL ,
`day` VARCHAR( 240 ) NOT NULL ,
`message` TEXT NOT NULL ,
`isEnable` ENUM( 'Y', 'N' ) NOT NULL ,
`client` VARCHAR( 240 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `customer` ADD `addressDescription` VARCHAR( 250 ) NOT NULL ;

ALTER TABLE `ref_generator` ADD `postFix` VARCHAR( 100 ) NOT NULL ,
ADD `customPostFix` VARCHAR( 50 ) NOT NULL ,
ADD `incementBased` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `transactions` ADD `agentExchangeRate` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';
ALTER TABLE `amended_transactions` ADD `agentExchangeRate` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';