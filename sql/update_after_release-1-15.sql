ALTER TABLE `agent_account` CHANGE `description` `description` VARCHAR( 90 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `agent_account` CHANGE `note` `note` VARCHAR( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `bank_account` CHANGE `description` `description` VARCHAR( 90 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `cm_beneficiary` ADD `other_title` VARCHAR( 100 ) NOT NULL ,
ADD `otherId` BIGINT( 20 ) NOT NULL ,
ADD `otherid_name` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `cm_collection_point` CHANGE `disableReason` `disableReason` VARCHAR( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `imfee` CHANGE `feeType` `feeType` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `manageFee` CHANGE `feeType` `feeType` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `teller` ADD `balance` BIGINT( 20 ) NOT NULL DEFAULT '0' AFTER `limit_alert` ;

ALTER TABLE `countryTime` CHANGE `timeDiff` `timeDiff` VARCHAR( 100 ) NOT NULL DEFAULT '00:00';

ALTER TABLE `cities` DROP `deliveryTime` ,
DROP `currencyRec` ,
DROP `serviceAvailable` ,
DROP `countryType` ,
DROP `bankCharges` ,
DROP `outCurrCharges` ;

CREATE TABLE `teller_account` (
  `taID` bigint(20) NOT NULL auto_increment,
  `tellerID` bigint(20) NOT NULL default '0',
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` bigint(20) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`taID`)
);