ALTER TABLE `customer` CHANGE `documentProvided` `documentProvided` ENUM( 'Y', 'N' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `exchangerate` ADD `marginType` ENUM( 'percent', 'fixed' ) NOT NULL DEFAULT 'percent';

CREATE TABLE `shareConfig` (
  `shareConfigID` bigint(20) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `shareType` varchar(25) NOT NULL,
  `shareValue` float NOT NULL,
  `lastModified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `userType` varchar(35) NOT NULL,
  `shareFrom` varchar(25) NOT NULL default 'company',
  PRIMARY KEY  (`shareConfigID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `shareProfit` (
  `profitShareID` bigint(20) NOT NULL auto_increment,
  `transID` bigint(20) NOT NULL,
  `exchRateUsed` float NOT NULL,
  `exchRateActual` float NOT NULL,
  `currencyTo` varchar(10) NOT NULL,
  `agentMargin` float NOT NULL,
  `agentID` bigint(20) NOT NULL,
  `calculationTime` datetime NOT NULL,
  `actAs` varchar(35) NOT NULL,
  PRIMARY KEY  (`profitShareID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;