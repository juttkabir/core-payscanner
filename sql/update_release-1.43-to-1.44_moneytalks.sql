-- THIS SCRIPT IS ONLY FOR MONEYTALKS. script to remove Distributor account statement and dstributor statment from left menu.
-- against ticket #3535 by khola
-- Note: developers are not required to run this script on their machines since this is just to remove some pages from left menu that are not required for moneytalks.
DELETE FROM `user_functionality` WHERE `functionCode` = 'right62';
DELETE FROM `user_functionality` WHERE `functionCode` = 'right76';