ALTER TABLE `admin` ADD `linked_Distributor` VARCHAR( 255 ) NOT NULL AFTER `linked_Agent` ;
ALTER TABLE `transactionExtended` ADD `minRateAlert` FLOAT NOT NULL ,
ADD `maxRateAlert` FLOAT NOT NULL ,
ADD `isAlertProcessed` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';

ALTER TABLE `customer` ADD `fillingNo` VARCHAR( 50 ) NOT NULL ,
ADD `profession` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `forum_thread` CHANGE `userID` `userID` BIGINT NULL DEFAULT NULL ;
ALTER TABLE `forum` CHANGE `logedUserID` `logedUserID` BIGINT NULL DEFAULT '0';



CREATE TABLE `comp_ofac_address` (
`add_id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`compliancePersonID` BIGINT NOT NULL ,
`add_num` BIGINT NOT NULL ,
`address` TEXT NOT NULL ,
`city` TEXT NOT NULL ,
`country` TEXT NOT NULL ,
`add_remarks` TEXT NOT NULL
) ENGINE = MYISAM ;


CREATE TABLE `comp_ofac_alt` (
`alt_id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`compliancePersonID` BIGINT NOT NULL ,
`alt_num` BIGINT NOT NULL ,
`alt_type` VARCHAR( 8 ) NOT NULL ,
`alt_name` VARCHAR( 350 ) NOT NULL ,
`alt_remarks` VARCHAR( 200 ) NOT NULL
) ENGINE = MYISAM ;


ALTER TABLE `configure_commission` ADD `actAs` VARCHAR( 30 ) NOT NULL ;

ALTER TABLE `comp_ofac_alt` ADD `listID` SMALLINT( 6 ) NOT NULL ;

ALTER TABLE `comp_ofac_address` ADD `listID` SMALLINT( 6 ) NOT NULL ;


ALTER TABLE `admin` ADD `rateCountries` TEXT NOT NULL ,
ADD `unlimitedRate` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';

ALTER TABLE `admin` ADD `notes` VARCHAR( 200 ) NOT NULL ;
ALTER TABLE `admin` CHANGE `agentMCBExpiry` `agentMCBExpiry` DATE NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `emailAlert` ADD `userType` VARCHAR( 50 ) NOT NULL ;






ALTER TABLE `compliancePerson` ADD `ent_num` BIGINT NOT NULL AFTER `compliancePersonID` ,
ADD `sdn_type` TEXT NOT NULL AFTER `ent_num` ,
ADD `sdn_title` TEXT NOT NULL AFTER `sdn_type` ;

ALTER TABLE `compliancePerson` DROP `aka1` ,
DROP `aka2` ,
DROP `aka3` ;


ALTER TABLE `transactions` ADD `branchName` VARCHAR( 250 ) NOT NULL ;