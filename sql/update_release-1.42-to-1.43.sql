-- #3592: Prime Currency- Cash Book Report

CREATE TABLE IF NOT EXISTS `cash_book` (
  `recId` int(11) NOT NULL auto_increment,
  `userId` bigint(20) default NULL,
  `openingBalance` double(13,4) default NULL,
  `closingBalance` double(13,4) default NULL,
  `banks` double(11,4) default NULL,
  `overShort` double(11,4) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`recId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS `expenses` (
  `recId` int(11) NOT NULL auto_increment,
  `userId` bigint(20) default NULL,
  `narration` varchar(255) collate latin1_general_ci default NULL,
  `amount` double(7,2) default NULL,
  `created` datetime default NULL,
  `updated` datetime default NULL,
  PRIMARY KEY  (`recId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

