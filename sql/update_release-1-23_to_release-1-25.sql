ALTER TABLE `beneficiary` ADD `dob` DATE NOT NULL DEFAULT '0000-00-00' AFTER `beneficiaryName` ;
ALTER TABLE `beneficiary` ADD `SOF` VARCHAR( 250 ) NOT NULL AFTER `lastName` ;
ALTER TABLE `customer` ADD `SOF` VARCHAR( 250 ) NOT NULL AFTER `lastName` ;
ALTER TABLE `customer` ADD `customerStatus` ENUM( 'Enable', 'Disable' ) NOT NULL DEFAULT 'Enable';
ALTER TABLE `customer` ADD `disableReason` TEXT NULL ;

UPDATE `tbl_export_file` SET `conditions` = 'and (toCountry = ''Georgia'' or toCountry = ''United Kingdom'')' WHERE `id` =3 LIMIT 1 ;
UPDATE `tbl_export_fields` SET `payex_field` = 'localAmount' WHERE `id` =51 LIMIT 1 ;
UPDATE `tbl_export_fields` SET `Lable` = 'Beneficiary Name' WHERE `id` =54 LIMIT 1 ;
UPDATE `tbl_export_fields` SET `Lable` = 'Receiving currency' WHERE `id` =52 LIMIT 1 ;

ALTER TABLE `cm_customer` ADD `status` ENUM( 'Enable', 'Disable' ) NOT NULL DEFAULT 'Enable' AFTER `LastName` ;

ALTER TABLE `customer` CHANGE `accumulativeAmount` `accumulativeAmount` BIGINT( 250 ) NULL DEFAULT NULL ; 

ALTER TABLE `admin` CHANGE `adminType` `adminType` ENUM( 'Supper', 'System', 'Admin', 'Agent', 'Call', 'COLLECTOR', 
'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Agent';
ALTER TABLE `admin` ADD `fromServer` varchar( 20 ) NOT NULL ;


ALTER TABLE `admin` ADD `paymentMode` enum('include','exclude') NOT NULL default 'include' AFTER `fromServer`;

ALTER TABLE `document_category` ADD `payexTable` varchar( 250 ) NOT NULL ;


INSERT INTO `config` (`id`, `varName`, `value`, `oldValue`, `description`, `detailDesc`, `isFlag`, `client`) VALUES (8, 'CONFIG_NO_OF_DAYS', '30', '0', 'No Of Days', '', 'N', 'express'),
(7, 'CONFIG_COMPLIANCE_MAX_AMOUNT', '10000', '0', 'Accumulative upper threshold', 'Maximum Compliance Amount limit for the pop up .', 'N', 'express'),
(6, 'CONFIG_AMOUNT_CONTROLLER', '1', '0', 'Amount Controller for AML report', '', 'N', 'express'),
(5, 'CONFIG_COMPLIANCE_MIN_AMOUNT', '800', '0', 'Accumulative lower threshold', 'Minimum Compliance Amount limit for the pop up .', 'N', 'express');

CREATE TABLE `forum` (
  `cmpID` int(32) NOT NULL auto_increment,
  `parentID` int(32) default '0',
  `transID` int(32) default '0',
  `agent` varchar(32) default NULL,
  `emails` varchar(255) default NULL,
  `subject` varchar(255) default NULL,
  `details` text,
  `status` enum('New','Close') default 'New',
  `dated` datetime default '0000-00-00 00:00:00',
  `logedUserID` int(20) default '0',
  `logedUserName` varchar(100) default NULL,
  `enquiryNumber` varchar(50) default NULL,
  PRIMARY KEY  (`cmpID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Created by Niaz Ahmad for Rich Almond #2052 at 10-09-2007' AUTO_INCREMENT=133 ;

CREATE TABLE `forum_thread` (
  `id` int(32) NOT NULL auto_increment,
  `cmpID` int(32) default '0',
  `userID` int(32) default NULL,
  `userTbl` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Created by Niaz Ahmad for Rich Almond #2052 at 10-09-2007' AUTO_INCREMENT=192 ;


ALTER TABLE `teller` ADD `email` VARCHAR( 100 ) NULL DEFAULT NULL AFTER `rights` ,
ADD `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `email` ;
