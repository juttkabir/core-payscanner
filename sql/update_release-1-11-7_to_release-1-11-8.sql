CREATE TABLE `manageFee` (
`manageFeeID` bigint( 20 ) unsigned NOT NULL AUTO_INCREMENT ,
`origCountry` varchar( 100 ) NOT NULL default '',
`destCountry` varchar( 100 ) NOT NULL default '',
`amountRangeFrom` float NOT NULL default '0',
`amountRangeTo` float NOT NULL default '0',
`regularFee` float NOT NULL default '0',
`feeType` varchar( 15 ) NOT NULL default '',
`payinFee` float NOT NULL default '0',
`agentNo` bigint( 20 ) NOT NULL ,
PRIMARY KEY ( `manageFeeID` )
);


ALTER TABLE `imfee` ADD `manageID` BIGINT NOT NULL ;

ALTER TABLE `manageFee` ADD `intervalUsed` INT NOT NULL ;

ALTER TABLE `customer` ADD `balance` float NOT NULL ;