--#9362 - AMB Exchange
ALTER TABLE `account_details` CHANGE `transID` `transID` VARCHAR( 50 ) NOT NULL 

-- #7591 - Premier Exchange - commission report for client
ALTER TABLE `transactions` ADD `agentComDuePaid` FLOAT NOT NULL DEFAULT '0' COMMENT 'This is the paid commission to agent, it comes from AgentComm field if not custom';

-- #7337 - Premier exchange - IBAN and fieldsChecked are added in accounts table
ALTER TABLE `accounts` ADD `IBAN` VARCHAR( 50 ) NOT NULL AFTER `branchAddress`;
ALTER TABLE `accounts` ADD `fieldsChecked` VARCHAR( 500 ) NOT NULL COMMENT 'table fields having checkboxes, here info is stored either respective checkbox is clicked or not.';
ALTER TABLE `accounts` ADD `showOnDeal` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' COMMENT 'If checkbox is checked on accounts then bank details of this would be displayed on transaction receipt, while other accounts with same currency would not be displayed.';
-- #7286 - Premier exchange - source field is added in customer table
ALTER TABLE `customer` CHANGE `source` `source` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
-- #6272 - Premier exchange - iban field used in beneficiary form also used on transaction page with bank details.
ALTER TABLE `ben_banks` ADD `IBAN` VARCHAR( 100 ) NOT NULL ;

--6258-premier fx
ALTER TABLE `admin` CHANGE `agentAccountLimit` `agentAccountLimit` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00';
-- #5893- forex trading
-- 
-- Table structure for table `accounts`
-- 

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL auto_increment,
  `userType` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL default '',
  `bankName` varchar(255) NOT NULL default '',
  `bankAddress` varchar(255) NOT NULL default '',
  `branchName` varchar(255) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `city` varchar(100) NOT NULL default '',
  `phone` varchar(32) NOT NULL default '',
  `mobile` varchar(32) NOT NULL default '',
  `fax` varchar(32) NOT NULL default '',
  `accounNumber` varchar(255) NOT NULL default '',
  `accounType` varchar(255) NOT NULL default '',
  `balance` double NOT NULL default '0',
  `currency` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` enum('AC','IA','DL') NOT NULL default 'AC',
  `createdBy` varchar(255) NOT NULL default '',
  `accountName` varchar(100) NOT NULL,
  `sortCode` varchar(50) NOT NULL,
  `transNote` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1  ;

-- Table structure for table `account_details`
-- 

CREATE TABLE IF NOT EXISTS `account_details` (
  `acid` int(11) NOT NULL auto_increment,
  `id` int(11) NOT NULL,
  `transNumber` varchar(150) NOT NULL,
  `transID` bigint(20) NOT NULL,
  `refNumberIM` varchar(50) NOT NULL,
  `currencyBuy` varchar(10) NOT NULL,
  `currencySell` varchar(10) NOT NULL,
  `amountBuy` float NOT NULL,
  `amountSell` float NOT NULL,
  `exchangeRate` float NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `profitLoss` float NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `accountSell` varchar(50) NOT NULL,
  `accountBuy` varchar(50) NOT NULL,
  `status` enum('TE','TC','TN','TD') NOT NULL default 'TN' COMMENT 'TE=Trade Edit,TC=Trade Cancell,TN=Trade New,TD=Trade Deleted',
  `note` text NOT NULL,
  `dispatchNumber` varchar(50) NOT NULL, 
  PRIMARY KEY  (`acid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

ALTER TABLE `transactions` ADD `trading_sell_account` VARCHAR( 50 ) NOT NULL COMMENT 'account number against which transactions exported';

-- #5955:premierexchange payments are in gbp,euror usd
ALTER TABLE `currencies` ADD `currency_for` VARCHAR( 50 ) NOT NULL ;
 ALTER TABLE `currencies` CHANGE `currency_for` `currency_for` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'S=sending,R=receiving,B=both';

-- #5966:premier exchange enquiry
ALTER TABLE `complaints` ADD `enquiry_type` VARCHAR( 100 ) NOT NULL ;

-- #5957:premier exchange
-- Database: `dev_premier`
-- 
-- Table structure for table `customer_category`
-- 

CREATE TABLE IF NOT EXISTS `customer_category` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `enabled` enum('Y','N') NOT NULL default 'Y',
  `createdBy` varchar(255) NOT NULL,
  `dated` datetime NOT NULL,
  `updated` datetime NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

ALTER TABLE `customer` ADD `cust_category_id` VARCHAR( 50 ) NOT NULL ;

-- #5933-AMb Exchange - currency revaluation report
ALTER TABLE `currency_stock` ADD `revaluation_rate` FLOAT NOT NULL ;

-- #7337 - Premier exchange - IBAN and fieldsChecked are added in accounts table
ALTER TABLE `accounts` ADD `IBAN` VARCHAR( 50 ) NOT NULL AFTER `branchAddress`;
ALTER TABLE `accounts` ADD `fieldsChecked` VARCHAR( 500 ) NOT NULL COMMENT 'table fields having checkboxes, here info is stored either respective checkbox is clicked or not.';
ALTER TABLE `accounts` ADD `showOnDeal` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' COMMENT 'If checkbox is checked on accounts then bank details of this would be displayed on transaction receipt, while other accounts with same currency would not be displayed.';
-- #7286 - Premier exchange - source field is added in customer table
ALTER TABLE `customer` CHANGE `source` `source` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
-- #6272 - Premier exchange - iban field used in beneficiary form also used on transaction page with bank details.
ALTER TABLE `ben_banks` ADD `IBAN` VARCHAR( 100 ) NOT NULL ;

--6258-premier fx
ALTER TABLE `admin` CHANGE `agentAccountLimit` `agentAccountLimit` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00';
-- #5893- forex trading
-- 
-- Table structure for table `accounts`
-- 

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL auto_increment,
  `userType` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL default '',
  `bankName` varchar(255) NOT NULL default '',
  `bankAddress` varchar(255) NOT NULL default '',
  `branchName` varchar(255) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `city` varchar(100) NOT NULL default '',
  `phone` varchar(32) NOT NULL default '',
  `mobile` varchar(32) NOT NULL default '',
  `fax` varchar(32) NOT NULL default '',
  `accounNumber` varchar(255) NOT NULL default '',
  `accounType` varchar(255) NOT NULL default '',
  `balance` double NOT NULL default '0',
  `currency` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` enum('AC','IA','DL') NOT NULL default 'AC',
  `createdBy` varchar(255) NOT NULL default '',
  `accountName` varchar(100) NOT NULL,
  `sortCode` varchar(50) NOT NULL,
  `transNote` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- Table structure for table `account_details`
-- 

CREATE TABLE IF NOT EXISTS `account_details` (
  `acid` int(11) NOT NULL auto_increment,
  `id` int(11) NOT NULL,
  `transNumber` varchar(150) NOT NULL,
  `transID` bigint(20) NOT NULL,
  `refNumberIM` varchar(50) NOT NULL,
  `currencyBuy` varchar(10) NOT NULL,
  `currencySell` varchar(10) NOT NULL,
  `amountBuy` float NOT NULL,
  `amountSell` float NOT NULL,
  `exchangeRate` float NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `profitLoss` float NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `accountSell` varchar(50) NOT NULL,
  `accountBuy` varchar(50) NOT NULL,
  `status` enum('TE','TC','TN','TD') NOT NULL default 'TN' COMMENT 'TE=Trade Edit,TC=Trade Cancell,TN=Trade New,TD=Trade Deleted',
  `note` text NOT NULL,
  `dispatchNumber` varchar(50) NOT NULL 
  PRIMARY KEY  (`acid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

ALTER TABLE `transactions` ADD `trading_sell_account` VARCHAR( 50 ) NOT NULL COMMENT 'account number against which transactions exported';

-- #5955:premierexchange payments are in gbp,euror usd
ALTER TABLE `currencies` ADD `currency_for` VARCHAR( 50 ) NOT NULL ;
 ALTER TABLE `currencies` CHANGE `currency_for` `currency_for` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'S=sending,R=receiving,B=both';

-- #5966:premier exchange enquiry
ALTER TABLE `complaints` ADD `enquiry_type` VARCHAR( 100 ) NOT NULL ;

-- #5957:premier exchange
-- Database: `dev_premier`
-- 
-- Table structure for table `customer_category`
-- 

CREATE TABLE IF NOT EXISTS `customer_category` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `enabled` enum('Y','N') NOT NULL default 'Y',
  `createdBy` varchar(255) NOT NULL,
  `dated` datetime NOT NULL,
  `updated` datetime NOT NULL,
   PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

ALTER TABLE `customer` ADD `cust_category_id` VARCHAR( 50 ) NOT NULL ;

-- #5933-AMb Exchange - currency revaluation report
ALTER TABLE `currency_stock` ADD `revaluation_rate` FLOAT NOT NULL ;
>>>>>>> .r8864
-- #5088-AMb Exchange - Distributor Deal Sheet
-- Table structure for table `deal_sheet`

CREATE TABLE IF NOT EXISTS `deal_sheet` (
  `id` int(11) NOT NULL auto_increment,
  `userId` varchar(30) NOT NULL,
  `deal_ref` varchar(10) NOT NULL,
  `currency_origin` varchar(3) NOT NULL,
  `currency_destination` varchar(3) NOT NULL,
  `purchase_from` varchar(100) NOT NULL,
  `purchase_amount` float NOT NULL,
  `local_amount` float NOT NULL,
  `rate` float NOT NULL,
  `balance` float NOT NULL,
  `profit_loss` float NOT NULL,
  `status` enum('AC','IA','DL') NOT NULL default 'AC',
  `deal_date` date NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `userType` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `deal_ref` (`deal_ref`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;


-- Table structure for table `deal_trans_details`
--

CREATE TABLE IF NOT EXISTS `deal_trans_details` (
  `dtd_id` int(11) NOT NULL auto_increment,
  `id` int(11) NOT NULL,
  `trans_no` int(11) NOT NULL,
  `transID` bigint(20) NOT NULL,
  `refNumberIM` varchar(50) NOT NULL,
  `currencyFrom` varchar(10) NOT NULL,
  `currencyTo` varchar(10) NOT NULL,
  `transAmount` float NOT NULL,
  `localAmount` float NOT NULL,
  `exchangeRate` float NOT NULL,
  `transDate` datetime NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `PL` float NOT NULL,
  `customerID` bigint(20) NOT NULL,
  PRIMARY KEY  (`dtd_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- #5287 - AMB Exchange - currency stock list can be added/managed
CREATE TABLE IF NOT EXISTS `currency_stock` (
 `id` int(11) NOT NULL auto_increment,
 `currency` varchar(10) NOT NULL,
 `currencySymbol` varchar(10) NOT NULL,
 `amount` float(15,4) NOT NULL,
 `addedBy` varchar(100) NOT NULL,
 `updatedBy` varchar(100) NOT NULL,
 `created_on` datetime NOT NULL,
 `updated_on` datetime NOT NULL,
 PRIMARY KEY  (`id`)
) ENGINE=MyISAM;

-- Ticket #5541:AMB Exchange - Western union export file. Operator ID added in AnD forms.
ALTER TABLE `admin` ADD `OperatorID` VARCHAR( 50 ) NOT NULL COMMENT 'Operator ID initially used in Output file of AMB, Western Union';

-- Ticket #4995:AMB Exchange - Daily Expense Limit
ALTER TABLE `admin` ADD `limitDailyExpense` FLOAT NOT NULL ;
ALTER TABLE `expenses` ADD `limit_status` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';
ALTER TABLE `expenses` ADD `status` VARCHAR( 50 ) NOT NULL ;

--- #5367 Minas Center- User Denomination can be saved temporarily 
--- and also permanently. then it becomes un-editable
ALTER TABLE `CurrenryNotes` ADD `saveTemp` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y' COMMENT 'If report is saved permanently then value becomes N. by default it is editable and Saved Temporarily and is Y' AFTER `TellerID` ;

--- #5367 Minas Center- user denomination added 3 cards totalling Collected,Calculated 
--- and Over/Short Cheque Transactions Field added here.
ALTER TABLE `CurrenryNotes` ADD `totalChequeGbp` FLOAT( 10, 4 ) NOT NULL COMMENT 'This field contains value for Cheque Transactions' AFTER `date` ;

--- #5367 Minas Center- user denomination added 3 cards totalling Collected,Calculated 
--- and Over/Short Bank Transactions Field added here.
ALTER TABLE `CurrenryNotes` ADD `totalBankGbp` FLOAT( 10, 4 ) NOT NULL COMMENT 'This field contains value for Bank Transactions' AFTER `date` ;

--- ticket 5365
ALTER TABLE `transactions` ADD `transaction_notes` TEXT NOT NULL ;
---Ticket 5282
ALTER TABLE `bankDetails` ADD `routingNumber` VARCHAR( 100 ) NOT NULL DEFAULT '0';
--- Ticet 5279 & 5278
ALTER TABLE `id_types` ADD `show_on_sender_as_company` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y',
ADD `show_on_beneficiary_as_company` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';

ALTER TABLE `beneficiary` ADD `ben_type` VARCHAR( 20 ) NOT NULL DEFAULT 'user';

ALTER TABLE `beneficiary` ADD `company_msb_number` VARCHAR( 255 ) NOT NULL ;

--- Ticet 5278
ALTER TABLE `customer` ADD `company_msb_number` VARCHAR( 255 ) NOT NULL ;
-- Ticket 5274
-- Table structure for table `company_detail`
-- 

CREATE TABLE IF NOT EXISTS `company_detail` (
  `id` bigint(20) NOT NULL auto_increment,
  `company_name` varchar(255) NOT NULL,
  `company_country` varchar(100) NOT NULL,
  `company_url` varchar(100) NOT NULL,
  `enabled` enum('Y','N') NOT NULL default 'Y',
  `dated` datetime NOT NULL,
  `companyType` varchar(20) NOT NULL,
  `company_terms_conditions` TEXT NOT NULL,

  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- Ticket 4893
-- Cash Book Related Reports
ALTER TABLE `CurrenryNotes` ADD `currency_data` TEXT NULL COMMENT 'containt the data element of all attributes' AFTER `Coin001` ,
ADD `denomination_total` FLOAT( 10, 4 ) NULL COMMENT 'denomination total of all the notes and coins' AFTER `currency_data` ;

ALTER TABLE `CurrenryNotes` CHANGE `TellerID` `TellerID` BIGINT( 20 ) NOT NULL;


