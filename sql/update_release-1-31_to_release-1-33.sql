UPDATE login_activities SET description = "Transaction is Authorized." WHERE description LIKE "Transaction Authorized successfully.";


ALTER TABLE `currencies` ADD `currencyType` VARCHAR( 20 ) NOT NULL ;

UPDATE `currencies` SET currencyType = 'Core' WHERE `currencyName` LIKE 'EUR';
UPDATE `currencies` SET currencyType = 'Intermediate' WHERE `currencyName` LIKE 'USD';