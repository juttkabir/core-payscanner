 -- #3496 exchange rate to be added by agent for the sub agent
 
 ALTER TABLE `exchangerate` ADD `createdBy` VARCHAR( 30 ) NOT NULL, 
  ADD `updatedBy` VARCHAR( 30 ) NOT NULL;

-- #3549 global exchange - field for customer reference number
ALTER TABLE `receipt_range` ADD `userType` VARCHAR( 20 ) NOT NULL ;

-- #3549 global exchange - more fileds on customer's ledger
ALTER TABLE `agents_customer_account` ADD `refNo` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `agents_customer_account` ADD `paymentType` VARCHAR( 30 ) NULL ;

-- #3544 Now Transfer - Online Sender Module Enhancements
-- Please run the scripts in the same order.
ALTER TABLE `cm_beneficiary` ADD `proveAddType` VARCHAR( 50 ) NULL AFTER Country;
ALTER TABLE `cm_beneficiary` ADD `proveAddress` ENUM( 'Y', 'N' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'N' AFTER `proveAddType`;

-- modify the fields in managefee and imfee table
-- @Ticket# 3564
ALTER TABLE `manageFee` CHANGE `paymentMode` `paymentMode` ENUM( '', 'cash', 'bank', 'cheque', 'card' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `imfee` CHANGE `paymentMode` `paymentMode` ENUM( '', 'cash', 'bank', 'cheque', 'card' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;



-- added by Khola @ ticket #3546
ALTER TABLE `transactions` ADD `senderBank` VARCHAR( 100 ) NOT NULL ;
--added by javeria ticket#3265-PC
ALTER TABLE `amended_transactions` ADD `chequeNo` VARCHAR( 30 ) NOT NULL ;
ALTER TABLE `transactions` ADD `chequeNo` VARCHAR( 30 ) NOT NULL ;



-- New table added to cater new requirments for MINAS CENTER
-- Ticket# 3558 by Jahangir
DROP TABLE IF EXISTS `ben_banks`;

CREATE TABLE `banks` (
`id` int(11) NOT NULL auto_increment,
`bankId` varchar(100) NOT NULL,
`name` varchar(150) NOT NULL,
`country` varchar(100) NOT NULL,
`branchCode` varchar(50) default NULL,
`branchAddress` varchar(255) default NULL,
`swiftCode` varchar(50) default NULL,
`extra1` varchar(50) default NULL,
`extra2` varchar(50) default NULL,
`extra3` varchar(50) default NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `ben_banks` (
`id` int(11) NOT NULL auto_increment,
`benId` int(11) NOT NULL,
`bankId` int(11) NOT NULL,
`accountNo` varchar(100) NOT NULL,
`accountType` varchar(10) NOT NULL,
`extra` varchar(100) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM;


-- modify the table structure of the ben_banks
-- Ticket# 3558
ALTER TABLE `ben_banks` ADD `branchCode` VARCHAR( 50 ) NULL AFTER `accountType` ,
ADD `branchAddress` VARCHAR( 200 ) NULL AFTER `branchCode` ,
ADD `swiftCode` VARCHAR( 50 ) NULL AFTER `branchAddress` ;


-- End #3558



-- settlement currency fields added in sub agent ledger against ticket 3565
ALTER TABLE `sub_agent_account` ADD `settleAmount` VARCHAR( 30 ) NOT NULL ,
ADD `SID` BIGINT( 20 ) NOT NULL ;
ALTER TABLE `sub_account_summary` ADD `settlementOpening_balance` double NOT NULL ,
ADD `settlementClosing_balance` double NOT NULL ;

-- Creating new field to trasaction table to handle the amount recieved field for global exchange
-- Ticket# 3547

ALTER TABLE `transactions` ADD `recievedAmount` DOUBLE NOT NULL ;


-- Add Query
ALTER TABLE `exRateUsed` ADD `exRateHistory` BIGINT( 20 ) NOT NULL ; 
-- Modify query
ALTER TABLE `exRateUsed` CHANGE `exRateHistory` `exRateHistory` BIGINT( 20 ) NOT NULL;


UPDATE `tbl_export_fields` SET `payex_field` = 'firstName,lastName,nodata,Address,Zip' WHERE `Lable` ='Counterparty name and address';
-- Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
-- New tables added for multiple ID Types functionality

CREATE TABLE `id_types` (
  `id` mediumint(9) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `mandatory` enum('Y','N') NOT NULL default 'N',
  `show_on_sender_page` enum('Y','N') NOT NULL default 'Y',
  `show_on_ben_page` enum('Y','N') NOT NULL default 'Y',
  `active` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;


CREATE TABLE `user_id_types` (
  `id` bigint(20) NOT NULL auto_increment,
  `id_type_id` mediumint(9) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type` enum('B','C') NOT NULL COMMENT 'B for beneficiary and C for customer/sender',
  `id_number` varchar(255) NOT NULL,
  `issued_by` varchar(255) NOT NULL,
  `issue_date` date NOT NULL default '0000-00-00',
  `expiry_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id_type_id` (`id_type_id`,`user_id`,`user_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- End of scripts by Usman Ghani against #3299: MasterPayex - Multiple ID Types

-- By Usman Ghani against #3211 Minas Centre - Import Sender
-- Please run all three queries in the same order as they are copied here ascendingly.
ALTER TABLE `customer` 
	ADD `gender` VARCHAR( 15 ) NOT NULL AFTER `lastName` ;

ALTER TABLE `customer` 
	ADD `marital_status` VARCHAR( 15 ) NOT NULL AFTER `gender` ,
	ADD `secret_word` VARCHAR( 255 ) NOT NULL AFTER `marital_status` ;

ALTER TABLE `customer` 
	ADD `no_of_transactions` INT NOT NULL AFTER `transactionPurpose`;

ALTER TABLE `customer` CHANGE `payinBook` `payinBook` VARCHAR( 35 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `customer`
	CHANGE `firstName` `firstName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
	CHANGE `middleName` `middleName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
	CHANGE `lastName` `lastName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `beneficiary` 
		CHANGE `firstName` `firstName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		CHANGE `middleName` `middleName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		CHANGE `lastName` `lastName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- End of sql scripts by Usman Ghani against #3211


ALTER TABLE `exchangerate` ADD `_24hrMarginType` VARCHAR( 10 ) NOT NULL ,
ADD `_24hrMarginValue` FLOAT NOT NULL ;
ALTER TABLE `transactions` ADD `bankingType` VARCHAR( 20 ) NOT NULL;
-- By jahanir againt ticket # 3321
CREATE TABLE `ben_banks` (
  `id` int(11) NOT NULL auto_increment,
  `benId` int(11) NOT NULL,
  `bankName` varchar(150) NOT NULL default '',
  `account` varchar(100) NOT NULL,
  `branchName` varchar(100) NOT NULL,
  `branchAddress` varchar(255) NOT NULL default '',
  `IBAN` varchar(100) NOT NULL default '',
  `swiftCode` varchar(100) NOT NULL default '',
  `accountType` varchar(30) NOT NULL default 'Current',
  PRIMARY KEY  (`id`)
);

ALTER TABLE `beneficiary` ADD `userType` VARCHAR( 20 ) NOT NULL DEFAULT 'Private User';


ALTER TABLE `transactions` ADD `clientRef` VARCHAR( 200 ) NOT NULL AFTER `distRefNumber`;

-- By Jahangir against ticket # 3474. IBAN field for quick beneficiary 

ALTER TABLE `beneficiary` ADD `IBAN` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `cities` ADD `state` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `customer` ADD `proveAddType` VARCHAR( 50 ) NULL ;

-- By Khola against ticket # 3421. Note field added in in complianceQuery table   
ALTER TABLE `complianceQuery` ADD `note` TEXT NOT NULL ;

-- By Usman Ghani against #3428 Connect Plus - Select Distributors for Agents
-- Date: Wednesday, June 25, 2008
-- Added a new field in "admin" table against ticket "#3428: Connect Plus - Select Distributors for Agents:"
-- Sql script:

ALTER TABLE `admin` ADD `associated_distributors` TEXT NULL AFTER `agentCommission` ;

-- End of sql scripts by Usman Ghani against #3428


ALTER TABLE `admin` ADD `agentHouseNumber` VARCHAR( 100 ) NULL AFTER `agentMSBNumber` ;

--- Ticket# 3319
--- CreatedBy Jahangir
--- To handle the commission based on payment mode for Minas Center

ALTER TABLE `imfee` ADD `paymentMode` ENUM( '', 'cash', 'bank', 'cheque' ) NOT NULL ;

ALTER TABLE `manageFee` ADD `paymentMode` ENUM( '', 'cash', 'bank', 'cheque' ) NOT NULL ;

ALTER TABLE `tbl_export_fields` ADD `customPattern` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';
ALTER TABLE `tbl_export_fields` ADD `appearOnce` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';
ALTER TABLE `tbl_export_file` ADD `fileName` VARCHAR( 40 ) NOT NULL; 

INSERT INTO `tbl_export_file` (`id`, `File_Name`, `isEnable`, `Format`, `FileLable`, `fieldSpacing`, `lineBreak`, `client`, `conditions`, `description`, `showLable`, `fileName`) VALUES
(11, 'select_trans_to_export_opal.php', 'Yes', 'txt', 'Transactions For Poland', ',', '', 'nowtransfer,', 'and toCountry = ''Poland'' and transType=''Bank Transfer''', '', 'N', 'PL');

INSERT INTO `tbl_export_fields` (`fileID`, `payex_field`, `tableName`, `isActive`, `Lable`, `tempID`, `exportDefaultID`, `field_title`, `isFixed`, `Value`, `isPattern`, `patternType`, `fieldSperator`, `customPattern`, `appearOnce`) VALUES 
(11, '', '', 'Y', 'Transaction type ', 0, 0, '', 'Y', '110', 'N', '', '', 'N', 'N'),
(11, 'transDate', 'transactions', 'Y', 'Execution date ', 0, 0, '', 'N', '', 'N', '', '', 'N', 'N'),
(11, 'localAmount', 'transactions', 'Y', 'Amount in cents', 0, 0, '', 'N', '', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Ordering party bank routing number', 0, 0, '', 'Y', '', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Null field', 0, 0, '', 'Y', '0', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Ordering party account', 0, 0, '', 'Y', '"101160220200011872955"', 'N', '', '', 'N', 'N'),
(11, 'IBAN', 'bankDetails', 'Y', 'Counterparty Account', 0, 0, '', 'N', '', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Ordering Party name and address', 0, 0, '', 'Y', '""', 'N', '', '', 'N', 'N'),
(11, 'firstName,lastName,Address,Address1', 'beneficiary', 'Y', 'Counterparty name and address', 0, 0, '', 'N', '', 'N', '', '', 'Y', 'N'),
(11, '', '', 'Y', 'Null field', 0, 0, '', 'Y', '0', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Counterparty bank routing number ', 0, 0, '', 'N', '', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Order Title  ', 0, 0, '', 'Y', '"Order Title1|Order Title 2|Order Title3|Order Title4"', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Empty field ', 0, 0, '', 'Y', '""', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Empty field ', 0, 0, '', 'Y', '""', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Transaction classification', 0, 0, '', 'Y', '"51"', 'N', '', '', 'N', 'N'),
(11, '', '', 'Y', 'Annotations ', 0, 0, '', 'Y', '"REFERENCJE|TRANS65348261|OPAKOWANIA"', 'N', '', '', 'N', 'N');

ALTER TABLE `receipt_range` ADD `status` VARCHAR( 20 ) NOT NULL ;

CREATE TABLE `cashierCurrency` (
  `id` int(11) NOT NULL auto_increment,
  `userID` varchar(10) NOT NULL,
  `date` datetime NOT NULL,
  `note5` int(11) NOT NULL,
  `note10` int(11) NOT NULL,
  `note20` int(11) NOT NULL,
  `note50` int(11) NOT NULL,
  `coin1GBP` int(11) NOT NULL,
  `coin2GBP` int(11) NOT NULL,
  `coin50` int(11) NOT NULL,
  `coin20` int(11) NOT NULL,
  `coin10` int(11) NOT NULL,
  `coin05` int(11) NOT NULL,
  `coin01` int(11) NOT NULL,
  `total` float NOT NULL,
  `cashierRemarks` varchar(250) NOT NULL,
  `editDate` datetime NOT NULL,
  `pettyCash` float NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE `cashierCollectAmount` (
`cashierAmountId` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`cashierCurrId` bigint( 20 ) NOT NULL ,
`benAgentId` int( 11 ) NOT NULL ,
`amount` float NOT NULL ,
PRIMARY KEY ( `cashierAmountId` )
);

CREATE TABLE `currencyDenomination` (
  `id` int(11) NOT NULL auto_increment,
  `transId` int(11) NOT NULL,
  `notes1` int(11) NOT NULL,
  `notes10` int(11) NOT NULL,
  `notes2` int(11) NOT NULL,
  `notes20` int(11) NOT NULL,
  `notes5` int(11) NOT NULL,
  `notes50` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

ALTER TABLE `cashierCurrency` CHANGE `userID` `userID` BIGINT( 40 ) NOT NULL;
ALTER TABLE `cashierCollectAmount` CHANGE `benAgentId` `benAgentId` BIGINT( 40 ) NOT NULL;
