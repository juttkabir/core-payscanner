CREATE TABLE `account_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` varchar(20) NOT NULL,
  `dated` date NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `currency` varchar(10) NOT NULL default 'GBP',
  PRIMARY KEY  (`id`)
);

CREATE TABLE `agent_Dist_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` bigint(20) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `actAs` varchar(20) NOT NULL default 'Agent',
  PRIMARY KEY  (`aaID`)
);

ALTER TABLE `teller` ADD `rights` varchar(240) NOT NULL;