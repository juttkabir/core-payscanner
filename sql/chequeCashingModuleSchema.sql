CREATE TABLE IF NOT EXISTS `cheque_order` (
  `order_id` int(11) NOT NULL auto_increment,
  `customer_id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL,
  `cheque_ref` varchar(15) collate utf8_unicode_ci NOT NULL COMMENT 'For which customer can communicate with cheque cashing system',
  `cheque_no` varchar(50) collate utf8_unicode_ci NOT NULL,
  `account_no` varchar(50) collate utf8_unicode_ci NOT NULL,
  `bank_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `branch` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cheque_date` date NOT NULL,
  `cheque_amount` float(7,2) NOT NULL,
  `cheque_currency` varchar(3) collate utf8_unicode_ci NOT NULL,
  `fee` float(5,2) NOT NULL,
  `fee_id` int(5) default NULL COMMENT 'Id of the fee fetched during calculation',
  `manual_fee_reason` text collate utf8_unicode_ci COMMENT 'Full reason, why to have manual fee',
  `paid_amount` float(7,2) NOT NULL,
  `status` varchar(2) collate utf8_unicode_ci NOT NULL,
  `estimated_date` varchar(10) collate utf8_unicode_ci NOT NULL COMMENT 'Estimated date of cheque amount delivery',
  `paid_on` datetime NOT NULL,
  `paid_by` varchar(50) collate utf8_unicode_ci default NULL COMMENT 'Cheque order paid by',
  `cleared_on` datetime NOT NULL,
  `order_note` text collate utf8_unicode_ci COMMENT 'To maintain the notes entered by the comapny staff',
  `note` text collate utf8_unicode_ci NOT NULL COMMENT 'some notes about order managmenet, not available to the client/customer',
  `created_by` varchar(30) collate utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY  (`order_id`),
  KEY `fee_id` (`fee_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='To maintain the sperate queue to cash the cheques' AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `cheque_order_fee`
-- 

CREATE TABLE IF NOT EXISTS `cheque_order_fee` (
  `fee_id` int(11) NOT NULL auto_increment,
  `type` enum('F','P','C') collate utf8_unicode_ci NOT NULL,
  `type_value` int(7) NOT NULL,
  `amount_from` float(7,2) NOT NULL,
  `amount_upto` float(7,2) default NULL,
  `fee` float(7,2) NOT NULL,
  `created_by` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT 'userid|user group',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY  (`fee_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tbl used to calculate fee for cheque order amount' AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `company`
-- 

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL auto_increment,
  `name` varchar(150) collate utf8_unicode_ci NOT NULL,
  `address` varchar(255) collate utf8_unicode_ci NOT NULL,
  `city` varchar(30) collate utf8_unicode_ci NOT NULL,
  `country` varchar(30) collate utf8_unicode_ci NOT NULL,
  `zip` varchar(10) collate utf8_unicode_ci NOT NULL,
  `phone` varchar(15) collate utf8_unicode_ci NOT NULL,
  `fax` varchar(15) collate utf8_unicode_ci NOT NULL,
  `email` varchar(80) collate utf8_unicode_ci NOT NULL,
  `contact_person` varchar(50) collate utf8_unicode_ci NOT NULL,
  `no_of_days` int(3) NOT NULL default '1' COMMENT 'Days required to clear the cheque of particular company',
  PRIMARY KEY  (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Company table will use with cheque order table' AUTO_INCREMENT=35 ;

