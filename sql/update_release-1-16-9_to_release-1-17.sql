CREATE TABLE `CurrenryNotes` (
  `id` int(11) NOT NULL auto_increment,
  `Currency` varchar(10) NOT NULL,
  `DistributerID` varchar(10) NOT NULL,
  `TellerID` varchar(10) NOT NULL,
  `Note1` int(11) NOT NULL,
  `Note2` int(11) NOT NULL,
  `Note5` int(11) NOT NULL,
  `Note10` int(11) NOT NULL,
  `Note20` int(11) NOT NULL,
  `Note50` int(11) NOT NULL,
  `Coin05` int(11) NOT NULL,
  `Coin02` int(11) NOT NULL,
  `Coin01` int(11) NOT NULL,
  `Coin005` int(11) NOT NULL,
  `Coin002` int(11) NOT NULL,
  `Coin001` int(11) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
);

ALTER TABLE `admin` ADD `commPackageAnDDist` ENUM( '001', '002', '003' ) NOT NULL DEFAULT '001',
ADD `commAnDDist` FLOAT NOT NULL ;

ALTER TABLE `admin` CHANGE `commAnDDist` `commAnDDist` FLOAT NOT NULL DEFAULT '0';


ALTER TABLE `transactions` ADD `distCommPackage` VARCHAR( 30 ) NOT NULL ;


ALTER TABLE `beneficiary` ADD `Citizenship` VARCHAR( 100 ) NOT NULL AFTER `Zip` ;

ALTER TABLE `beneficiary` ADD `IDissuedate` VARCHAR( 11 ) NOT NULL AFTER `email` ,
ADD `IDexpirydate` VARCHAR( 11 ) NOT NULL AFTER `IDissuedate` ;

ALTER TABLE `agent_Dist_account` CHANGE `modified_by` `modified_by` VARCHAR( 30 ) NOT NULL DEFAULT '0';
ALTER TABLE `bank_account` CHANGE `modified_by` `modified_by` VARCHAR( 30 ) NOT NULL DEFAULT '0';
ALTER TABLE `agent_account` CHANGE `modified_by` `modified_by` VARCHAR( 30 ) NOT NULL DEFAULT '0';
ALTER TABLE `CurrenryNotes` ADD `date` VARCHAR( 20 ) NOT NULL AFTER `TellerID` ;