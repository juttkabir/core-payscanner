-- #9897: Premier Exchange:Sender registration through website
CREATE TABLE `dev_premier`.`compliance_GB` (
`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`customerID` BIGINT NOT NULL ,
`auth_id` VARCHAR( 100 ) NOT NULL ,
`validate_info` VARCHAR( 60 ) NOT NULL ,
`type` VARCHAR( 50 ) NOT NULL ,
`test_result` VARCHAR( 30 ) NOT NULL ,
`verify_date` DATE NOT NULL
) ENGINE = MYISAM 

-- #9834: PremierFX: Bank Reconciliation
ALTER TABLE `barclayspayments` ADD `accountNo` varchar(45) NOT NULL;
ALTER TABLE `bank_account` ADD INDEX ( `bankID` );
ALTER TABLE `bankDetails` ADD `IbanOrBank` varchar( 50 ) NOT NULL default 'iban' COMMENT 'By default iban is assigned to all 

records';
ALTER TABLE `amended_transactions` ADD `discountedUniAmount` float NOT NULL default '0' COMMENT 'this contains total amount 

after discount+company commission';
ALTER TABLE `agent_account` ADD INDEX ( `type` );
ALTER TABLE `agent_account` ADD INDEX ( `dated` );
ALTER TABLE `agent_account` ADD INDEX ( `TransID` );
ALTER TABLE `agent_account` ADD INDEX ( `status` );
ALTER TABLE `admin` ADD `localAmountOnly` enum( 'Y', 'N' ) NOT NULL default 'N';
ALTER TABLE `admin` ADD `manualCommission` enum( 'Y', 'N' ) NOT NULL ;
ALTER TABLE `admin` ADD `extraCommission` enum( 'Y', 'N' ) NOT NULL ;
ALTER TABLE `admin` ADD INDEX ( `username` );
ALTER TABLE `admin` ADD INDEX ( `name` );
ALTER TABLE `admin` ADD INDEX ( `parentID` );
ALTER TABLE `account_summary` ADD INDEX ( `user_id` );
ALTER TABLE `transactionSummary` ADD `IBAN` varchar(100);
ALTER TABLE `transactions` ADD INDEX ( `custAgentID` );
ALTER TABLE `transactions` ADD INDEX ( `benID` );
ALTER TABLE `transactions` ADD INDEX ( `createdBy` );
ALTER TABLE `transactions` ADD INDEX ( `customerID` );
ALTER TABLE `transactions` ADD INDEX ( `transDate` );
ALTER TABLE `transactions` ADD INDEX ( `transStatus` );
ALTER TABLE `transactions` ADD INDEX ( `deliveredBy` );
ALTER TABLE `transactions` ADD INDEX ( `benAgentID` );
ALTER TABLE `transactions` ADD INDEX ( `transType` );
ALTER TABLE `transactions` ADD INDEX ( `localAmount` );
ALTER TABLE `transactions` ADD INDEX ( `transAmount` );
ALTER TABLE `transactions` ADD INDEX ( `custAgentParentID` );
ALTER TABLE `transactions` ADD INDEX ( `refNumber` );
ALTER TABLE `transactions` ADD INDEX ( `currencyFrom` );
ALTER TABLE `transactions` ADD INDEX ( `refNumberIM` );
ALTER TABLE `transactions` ADD INDEX ( `addedBy` );
ALTER TABLE `transactions` ADD INDEX ( `cancelDate` );
ALTER TABLE `transactions` ADD INDEX ( `deliveryDate` );
ALTER TABLE `transactions` ADD INDEX ( `benAgentParentID` );
ALTER TABLE `transactions` ADD `exportedDate` datetime NOT NULL COMMENT 'When transaction is exported then date is updated to 

be displayed in Old transaction links';
ALTER TABLE `transactions` ADD `discountedUniAmount` float NOT NULL default '0' COMMENT 'this contains total amount after 

discount+company commission';
ALTER TABLE `tbl_export_fields` ADD `position` int(11) NOT NULL COMMENT 'This is used to display in certain order';

CREATE TABLE IF NOT EXISTS `tblexportedfiles` (
`id` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`name` varchar( 250 ) NOT NULL ,
`dated` timestamp NOT NULL default CURRENT_TIMESTAMP ,
`user` varchar( 35 ) NOT NULL ,
`counter` int( 11 ) NOT NULL ,
`updated` date NOT NULL ,
PRIMARY KEY ( `id` )
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =1;
ALTER TABLE `payment_action` ADD INDEX ( `action_date` );

CREATE TABLE IF NOT EXISTS `payments_resolved` (
`aID` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`payID` bigint( 20 ) NOT NULL ,
`transID` bigint( 20 ) NOT NULL ,
`agentAmount` float( 7, 2 ) NOT NULL ,
`action_date` timestamp NOT NULL default CURRENT_TIMESTAMP ,
`resolvedBy` bigint( 20 ) NOT NULL ,
`status` enum( 'Y', 'N' ) NOT NULL default 'Y',
`refNumberIMs` varchar( 500 ) NOT NULL COMMENT 'reference number of transaction(s) reconciled',
`batchNumber` int( 10 ) NOT NULL ,
`format` varchar( 10 ) NOT NULL COMMENT '1=one transaction,2=one payment many transactions,3=one transaction and many 

payments,4=many payments and many transactions',
PRIMARY KEY ( `aID` ) ,
KEY `action_date` ( `action_date` )
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =1;

ALTER TABLE `login_activities` ADD INDEX ( `login_history_id` );
ALTER TABLE `login_activities` ADD INDEX ( `action_for` );
ALTER TABLE `login_activities` ADD INDEX ( `table_name` );
ALTER TABLE `login_activities` ADD INDEX ( `description` );

ALTER TABLE `customerBank` ADD `City` varchar(50) NOT NULL;
ALTER TABLE `customer` ADD INDEX ( `accountName` );
ALTER TABLE `customer` ADD INDEX ( `payinBook` );
ALTER TABLE `customer` ADD INDEX ( `firstName` );
ALTER TABLE `customer` ADD INDEX ( `lastName` );
ALTER TABLE `customer` ADD INDEX ( `agentID` );
ALTER TABLE `customer` ADD INDEX ( `customerNumber` );
ALTER TABLE `customer` ADD INDEX ( `IDNumber` );
ALTER TABLE `customer` ADD INDEX ( `Mobile` );
ALTER TABLE `customer` ADD INDEX ( `Phone` );
ALTER TABLE `customer` ADD INDEX ( `middleName` );

ALTER TABLE `company_detail` ADD `company_address1` varchar(255) NOT NULL;
ALTER TABLE `company_detail` ADD `company_address2` varchar(255) NOT NULL;
ALTER TABLE `company_detail` ADD `company_city` varchar(100) NOT NULL;
ALTER TABLE `company_detail` ADD `company_state` varchar(100) NOT NULL;
ALTER TABLE `company_detail` ADD `company_zip` varchar(20) NOT NULL;
ALTER TABLE `company_detail` ADD `company_phone1` varchar(32) NOT NULL;
ALTER TABLE `company_detail` ADD `company_phone2` varchar(32) NOT NULL;
ALTER TABLE `company_detail` ADD `company_email` varchar(100) NOT NULL;
ALTER TABLE `company_detail` ADD `company_promotion_heading` varchar(50) NOT NULL;
ALTER TABLE `company_detail` ADD `company_promotion_message` varchar(255) NOT NULL;

ALTER TABLE `cm_questions` ADD `qNumber` int(4) NOT NULL;

ALTER TABLE `cm_beneficiary` ADD `proveAddress` enum('Y','N') NOT NULL default 'N';

ALTER TABLE `beneficiary` ADD INDEX ( `firstName` );
ALTER TABLE `beneficiary` ADD INDEX ( `lastName` );
ALTER TABLE `beneficiary` ADD INDEX ( `middleName` );
ALTER TABLE `beneficiary` ADD INDEX ( `customerID` );
ALTER TABLE `beneficiary` ADD INDEX ( `beneficiaryName` );

-- #9829: PremierFx: Profit and Loss of Trade
ALTER TABLE `tradeBatchHistory` ADD `transResolveAmount` FLOAT( 11, 4 ) NULL ,
ADD `tradeResolveAmount` FLOAT( 11, 4 ) NULL ;

-- #9808: PremierFx: Import and Export of MT103
ALTER TABLE `transactions` ADD `trans_source` ENUM( '', 'I', 'R', 'M', 'N', 'A' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'I=import,R=RTGS,M=MT103,N=NTM,A=API' ;

-- #9588: AMB: Edit Transaction Enhancement
ALTER TABLE `tt_transactions` ADD `prevAmountDetail` TEXT NOT NULL
COMMENT 'to save
prevAmountDetails[inCaseOfUpdatingAmountDetailAndTransStausChangeToHold]' AFTER
`internalDetails` ;

-- #9590: AMB Exchange: Remaining Bugs in TT Module
ALTER TABLE `tt_transactions` CHANGE `exchange_rate` `exchange_rate` DOUBLE( 28, 8 ) NOT NULL 
-- #9589: AMB Exchange: Enhancements in Compliance
ALTER TABLE `tt_transactions` ADD `internalDetails` TEXT NOT NULL COMMENT 'ifSender/Benf lies in compliance';

-- #9363: AMB: Need to Record Profit/Loss Currency Chunk Bought and Sold at Specific Rate
ALTER TABLE  `currency_stock` ADD  `actualAmount` FLOAT( 15, 4 ) NOT NULL ;
-- #9363: AMB: Need to Record Profit/Loss Currency Chunk Bought and Sold at Specific Rate
-- Table structure for table `currency_stock_history_used`
--

CREATE TABLE IF NOT EXISTS `currency_stock_history_used` (
  `id` int(11) NOT NULL auto_increment,
  `from_batch_id` varchar(255) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `sell_rate` float(15,4) NOT NULL,
  `date_out` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- #9363: AMB: Need to Record Profit/Loss Currency Chunk Bought and Sold at Specific Rate
-- Table structure for table `currency_stock_history`
--

CREATE TABLE IF NOT EXISTS `currency_stock_history` (
  `id` int(11) NOT NULL auto_increment,
  `batch_id` varchar(255) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `amount` float(15,2) NOT NULL,
  `used_amount` float(15,2) NOT NULL,
  `available_amount` float(15,2) NOT NULL,
  `buying_rate` float(15,4) NOT NULL,
  `purshased_from` varchar(255) NOT NULL,
  `status` enum('Open','Closed') NOT NULL default 'Open',
  `created_date` date NOT NULL,
  `closing_date` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;


-- #9524 - AMB Exchange : manual deposit/withdraw entries in trading accounts
ALTER TABLE `account_ledgers` CHANGE `transType` `transType` ENUM( 'FT', 'CE', 'TT', 'MN' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'CE' COMMENT 'transaction type either CE for currency exchange or FT for fund transfer or MN for manual deposit/withdraw';

-- #9328: PremeirFx: Association of introducer with account manager
ALTER TABLE `admin` ADD `IsDefault` ENUM( 'N', 'Y' ) NOT NULL DEFAULT 'N' AFTER `source` ;

-- #8794: AMB Exchange:Transaction ID must be assigned to interfund transfer
ALTER TABLE `account_ledgers` ADD `transType` ENUM( 'FT', 'CE' ) NOT NULL DEFAULT 'CE' COMMENT 'transaction type either CE for currency exchange or FT for fund transfer';
UPDATE `account_ledgers` SET transType = 'FT' WHERE crAccount != '' AND drAccount != '' ORDER BY id;

-- #8635: AMB Exchange: Inter fund transfer enhancement 
ALTER TABLE `curr_exchange_account` CHANGE `localAmount` `localAmount` DECIMAL( 13, 2 ) NOT NULL ,CHANGE `total_localAmount` `total_localAmount` DECIMAL( 13, 2 ) NOT NULL ,CHANGE `totalAmount` `totalAmount` DECIMAL( 13, 2 ) NOT NULL;

-- #8399 - AMB Exchange: Inter account transfers
ALTER TABLE `account_ledgers` CHANGE `crAmount` `crAmount` FLOAT( 13, 4 ) NOT NULL ,CHANGE `drAmount` `drAmount` FLOAT( 13, 4 ) NOT NULL;

-- #7653 Premier Fx: Multiple Remarks on Customer

-- 
-- Table structure for table `userRemarksHistory`
-- 

CREATE TABLE `userRemarksHistory` (
`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`userID` BIGINT NOT NULL ,
`userType` ENUM( 'C', 'B', 'A', 'T' ) NOT NULL COMMENT 'Note: C for customer, B for 

benificiary, A for agent, T for transaction',
`remarks` TEXT NOT NULL ,
`datetime` DATETIME NOT NULL ,
`updated_by` BIGINT NOT NULL
) ENGINE = MYISAM ;


-- #6054 AMB: double entry system
ALTER TABLE `account_summary` ADD `accountNumber` VARCHAR( 50 ) NOT NULL ;

-- 
-- Table structure for table `accounts`
-- 

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL auto_increment,
  `userType` varchar(50) NOT NULL,
  `userId` varchar(30) NOT NULL,
  `name` varchar(255) NOT NULL default '',
  `bankName` varchar(255) NOT NULL default '',
  `bankAddress` varchar(255) NOT NULL default '',
  `branchName` varchar(255) NOT NULL default '',
  `branchAddress` varchar(255) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `city` varchar(100) NOT NULL default '',
  `phone` varchar(32) NOT NULL default '',
  `mobile` varchar(32) NOT NULL default '',
  `fax` varchar(32) NOT NULL default '',
  `accounNumber` varchar(255) NOT NULL default '',
  `accounType` varchar(255) NOT NULL default '',
  `balance` double NOT NULL default '0',
  `currency` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` enum('AC','IA','DL') NOT NULL default 'AC',
  `createdBy` varchar(255) NOT NULL default '',
  `accountName` varchar(100) NOT NULL,
  `sortCode` varchar(50) NOT NULL,
  `transNote` text NOT NULL,
  `swiftCode` varchar(50) NOT NULL,
  `transFlag` enum('Y','N') NOT NULL default 'N',
  `depositFlag` enum('Y','N') NOT NULL default 'N',
  `balanceType` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;


-- 
-- Table structure for table `accounts_chart`
-- 

CREATE TABLE IF NOT EXISTS `accounts_chart` (
  `id` int(11) NOT NULL auto_increment,
  `accountName` varchar(50) NOT NULL,
  `accountNumber` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `currency` varchar(255) NOT NULL default '',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` enum('AC','IA','DL') NOT NULL default 'AC',
  `createdBy` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

-- 
-- Table structure for table `account_ledgers`
-- 

CREATE TABLE IF NOT EXISTS `account_ledgers` (
  `id` int(11) NOT NULL auto_increment,
  `userID` bigint(20) NOT NULL,
  `userType` varchar(50) NOT NULL,
  `transID` bigint(20) NOT NULL,
  `crAmount` float NOT NULL,
  `drAmount` float NOT NULL,
  `crAccount` varchar(50) NOT NULL,
  `drAccount` varchar(50) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `description` varchar(255) NOT NULL default '',
  `status` enum('NEW','E','C','D') NOT NULL default 'NEW' COMMENT 'E=Edit,C=Cancel,TD=Trade Deleted',
  `note` text NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  `fid` int(11) NOT NULL,
  `tblName` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- #5781 AMB Exchange - Currency Exchange fee module in Currency Exchange module
CREATE TABLE IF NOT EXISTS `currency_exchange_fee` (
  `fee_id` int(11) NOT NULL auto_increment,
  `type` enum('F','P') character set utf8 collate utf8_unicode_ci NOT NULL,
  `fee` float(7,2) NOT NULL,
  `currencyF` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL default 'GBP',
  `currencyT` varchar(5) character set utf8 collate utf8_unicode_ci NOT NULL,
  `created_by` varchar(100) character set utf8 collate utf8_unicode_ci NOT NULL COMMENT 'userid|user group',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY  (`fee_id`)
) ENGINE=MyISAM;
ALTER TABLE `curr_exchange_account` ADD `currency_fee_id` BIGINT( 20 ) NOT NULL COMMENT 'Crrency Exchange fee id'  AFTER `rate` ;
ALTER TABLE `curr_exchange_account` ADD `currency_fee` FLOAT NOT NULL COMMENT 'Currency Exchange fee value'  AFTER `localAmount` ;
ALTER TABLE `curr_exchange_account` ADD `total_localAmount` FLOAT NOT NULL COMMENT 'Total Local amount after adding fee' AFTER  `currency_fee` ;
UPDATE curr_exchange_account SET total_localAmount = total_localAmount+ localAmount;

-- ticket #5601 AMB-Compliance partial match
ALTER TABLE compliance_hm_treasury ADD FULLTEXT(full_name)
ALTER TABLE compliancePerson ADD FULLTEXT(firstName,middleName,lastName)
ALTER TABLE comp_ofac_alt ADD FULLTEXT(alt_name)
ALTER TABLE compliancePEP ADD FULLTEXT(fullName)

-- Ticket #5409:Minas Center - ransactions on Hold
ALTER TABLE `transactions` ADD `hold_trans_status` VARCHAR( 30 ) NOT NULL AFTER `holdedBy`  

-- Ticket #5052:Global Exchange - Documents categories Check boxes

ALTER TABLE `transactions` ADD `custDocumentProvided` VARCHAR( 250 ) NOT NULL ;
