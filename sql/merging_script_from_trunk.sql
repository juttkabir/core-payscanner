
-- ALL these scripts come after merging in trunk from different releases.

ALTER TABLE `admin` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `admin` ADD `settlementCurrencies` VARCHAR( 50 ) NULL COMMENT 'this field contains both settlement curriecy of AnD as agent and as distributor separeted with ''I'' sign respectivly';

ALTER TABLE `agent_account` ADD `modeOfPayment` VARCHAR( 10 ) NULL DEFAULT 'CASH', ADD `chequeNumber` VARCHAR( 30 ) NULL, ADD `chequeStatus` VARCHAR( 10 ) NULL ;

ALTER TABLE `amended_transactions` ADD `history` TEXT NULL COMMENT 'To retain the ammended transaction history';

ALTER TABLE `beneficiary` ADD `SOF_Type` VARCHAR( 20 ) NOT NULL DEFAULT 'S/O' COMMENT 'Trackes either s/o,w/o or d/o' AFTER `SOF` ;

ALTER TABLE `beneficiary` ADD `IbanOrBank` VARCHAR( 20 ) NOT NULL DEFAULT 'iban' COMMENT 'This field decides Which details to be used on Transaction' AFTER `IBAN` ;

ALTER TABLE `beneficiary` ADD `status` ENUM( 'Enabled', 'Disabled' ) NOT NULL DEFAULT 'Enabled' COMMENT 'Enabled_Disabled beneficiaries. Disabled Beneficiaries will not come in search beneficiary on create transaction.';

ALTER TABLE `bankDetails` ADD `BankCode` VARCHAR( 100 ) NULL ,
ADD `BranchName` VARCHAR( 100 ) NULL ,
ADD `BranchCity` VARCHAR( 100 ) NULL ;

ALTER TABLE `bankDetails` ADD `originalBankId` VARCHAR( 100 ) NOT NULL;

INSERT INTO `cities` (`country`, `city`, `countryCode`, `Currency`, `currDesc`, `isoCode`, `countryRegion`, `state`) VALUES
('Poland', '', 'POL', '', '', 'pl', 'European', 'Zachodniopomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Warmi&#324;sko-Mazurskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Podkarpackie'),
('Poland', '', 'POL', '', '', 'pl', 'European', '&#346;l&#261;skie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Pomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Podlaskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Opolskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Mazowieckie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Lubuskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Lubelskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Dolno&#347;l&#261;skie'),
('Poland', '', 'POL', '', '', 'pl', 'European', '&#321;&oacute;dzkie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Ma&#322;opolskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Kieleckie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Kujawsko-Pomorskie'),
('Poland', '', 'POL', '', '', 'pl', 'European', 'Wielkopolskie');


ALTER TABLE `compliancePerson` CHANGE `isDisabled` `isDisabled` ENUM( 'Y', 'N' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Y'

ALTER TABLE `compliancePerson` ADD `userDataBaseID` VARCHAR( 50 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS `commission` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`userId` BIGINT( 20 ) NOT NULL ,
`collectionPointId` BIGINT( 20 ) NOT NULL ,
`fixedValue` FLOAT( 5, 2 ) NOT NULL ,
`percentValue` FLOAT( 5, 2 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = MYISAM;

ALTER TABLE `configure_commission` ADD `interval` FLOAT( 7, 1 ) NULL COMMENT 'If the commission is based on the interval or denomination than this field will hold the interval value';

ALTER TABLE `cm_beneficiary` CHANGE `State` `State` VARCHAR( 50 ) NOT NULL;

ALTER TABLE `cm_beneficiary` ADD `IBAN` VARCHAR( 50 ) NOT NULL,
ADD `IbanOrBank` VARCHAR( 10 ) NOT NULL DEFAULT 'iban';

ALTER TABLE `cm_bankdetails` ADD `IbanOrBank` VARCHAR( 50 ) NOT NULL DEFAULT 'iban' COMMENT 'by defiault iban is assigned here';

ALTER TABLE `cm_customer` ADD `agentId` BIGINT( 15 ) NULL COMMENT 'In case of virtual agent, the associated agent value will store';

CREATE TABLE IF NOT EXISTS `cm_user_id_types` (
  `id` bigint(20) NOT NULL auto_increment,
  `id_type_id` mediumint(9) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type` enum('B','C') NOT NULL COMMENT 'B for beneficiary and C for customer/sender',
  `id_number` varchar(255) NOT NULL,
  `issued_by` varchar(255) NOT NULL,
  `issue_date` date NOT NULL default '0000-00-00',
  `expiry_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id_type_id` (`id_type_id`,`user_id`,`user_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

ALTER TABLE `customer` ADD `SOF_Type` VARCHAR( 20 ) NOT NULL DEFAULT 'S/O' COMMENT 'Trackes either s/o,w/o or d/o' AFTER `SOF` ;


CREATE TABLE `cheque_order` (
  `order_id` int(11) NOT NULL auto_increment,
  `customer_id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL,
  `cheque_ref` varchar(15) collate utf8_unicode_ci NOT NULL COMMENT 'For which customer can communicate with cheque cashing system',
  `cheque_no` varchar(50) collate utf8_unicode_ci NOT NULL,
  `account_no` varchar(50) collate utf8_unicode_ci NOT NULL,
  `bank_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `branch` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cheque_date` date NOT NULL,
  `cheque_amount` float(7,2) NOT NULL,
  `cheque_currency` varchar(3) collate utf8_unicode_ci NOT NULL,
  `fee` float(5,2) NOT NULL,
  `fee_id` int(5) default NULL COMMENT 'Id of the fee fetched during calculation',
  `manual_fee_reason` text collate utf8_unicode_ci COMMENT 'Full reason, why to have manual fee',
  `paid_amount` float(7,2) NOT NULL,
  `status` varchar(2) collate utf8_unicode_ci NOT NULL,
  `estimated_date` varchar(10) collate utf8_unicode_ci NOT NULL COMMENT 'Estimated date of cheque amount delivery',
  `paid_on` datetime NOT NULL,
  `paid_by` varchar(50) collate utf8_unicode_ci default NULL COMMENT 'Cheque order paid by',
  `cleared_on` datetime NOT NULL,
  `order_note` text collate utf8_unicode_ci COMMENT 'To maintain the notes entered by the comapny staff',
  `note` text collate utf8_unicode_ci NOT NULL COMMENT 'some notes about order managmenet, not available to the client/customer',
  `created_by` varchar(30) collate utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY  (`order_id`),
  KEY `fee_id` (`fee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='To maintain the sperate queue to cash the cheques' AUTO_INCREMENT=54 ;

CREATE TABLE `cheque_order_fee` (
  `fee_id` int(11) NOT NULL auto_increment,
  `type` enum('F','P','C') collate utf8_unicode_ci NOT NULL,
  `type_value` int(7) NOT NULL,
  `amount_from` float(7,2) NOT NULL,
  `amount_upto` float(7,2) default NULL,
  `fee` float(7,2) NOT NULL,
  `created_by` varchar(100) collate utf8_unicode_ci NOT NULL COMMENT 'userid|user group',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY  (`fee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tbl used to calculate fee for cheque order amount' AUTO_INCREMENT=17 ;

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL auto_increment,
  `name` varchar(150) collate utf8_unicode_ci NOT NULL,
  `address` varchar(255) collate utf8_unicode_ci NOT NULL,
  `city` varchar(30) collate utf8_unicode_ci NOT NULL,
  `country` varchar(30) collate utf8_unicode_ci NOT NULL,
  `zip` varchar(10) collate utf8_unicode_ci NOT NULL,
  `phone` varchar(15) collate utf8_unicode_ci NOT NULL,
  `fax` varchar(15) collate utf8_unicode_ci NOT NULL,
  `email` varchar(80) collate utf8_unicode_ci NOT NULL,
  `contact_person` varchar(50) collate utf8_unicode_ci NOT NULL,
  `no_of_days` int(3) NOT NULL default '1' COMMENT 'Days required to clear the cheque of particular company',
  PRIMARY KEY  (`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Company table will use with cheque order table' AUTO_INCREMENT=41 ;

ALTER TABLE `complianceMatchList` ADD `remarks` VARCHAR( 150 ) NOT NULL ;

CREATE TABLE `denominationBasedRate` (
`id` INT( 8 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`parentRateId` INT( 8 ) NOT NULL ,
`amountFrom` INT( 6 ) NOT NULL ,
`amountTo` INT( 6 ) NOT NULL ,
`exchangeRate` FLOAT( 6, 3 ) NOT NULL ,
`margin` FLOAT( 6, 3 ) NOT NULL
) ENGINE = MYISAM COMMENT = 'To store the related exchange rate for denomination based exchange rate';

CREATE TABLE `distributor_email` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`distributorId` BIGINT( 20 ) NOT NULL ,
`emailTo` VARCHAR( 255 ) NOT NULL ,
`emailCC` VARCHAR( 255 ) NOT NULL ,
`emailBCC` VARCHAR( 255 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = MYISAM ;


CREATE TABLE `dispatch_book` (
`recId` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`dispatchNumber` VARCHAR( 50 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`reportQuery` TEXT NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `document_upload` CHANGE `id` `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT;



ALTER TABLE `exchangerate` ADD `isDenomination` CHAR( 1 ) NOT NULL DEFAULT 'N' COMMENT 'Is exchange rate is denomination based';

ALTER TABLE `imfee` ADD `feeSubType` VARCHAR( 20 ) NOT NULL DEFAULT 'fix';

ALTER TABLE `manageFee` ADD `feeSubType` VARCHAR( 20 ) NOT NULL DEFAULT 'fix';

CREATE TABLE IF NOT EXISTS `oldTransactionGroupRight` (
  `id` int(11) NOT NULL auto_increment,
  `group` varchar(255) collate utf8_unicode_ci NOT NULL,
  `haveRight` enum('Y','N') collate utf8_unicode_ci NOT NULL default 'N',
  `backDays` int(5) NOT NULL,
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Rights for user groups to view the old transaction in create' AUTO_INCREMENT=17 ;

INSERT INTO `oldTransactionGroupRight` (`id`, `group`, `haveRight`, `backDays`, `updated`) VALUES 
(1, 'admin', 'Y', 90, '2008-11-27 08:30:19'),
(2, 'SUPA', 'Y', 90, '2008-11-27 08:30:19'),
(3, 'SUPAI', 'Y', 90, '2008-11-27 08:30:19'),
(4, 'SUBA', 'Y', 90, '2008-11-27 08:30:19'),
(5, 'SUBAI', 'Y', 90, '2008-11-27 08:30:19'),
(6, 'SUPI', 'N', 0, '2008-11-27 08:30:19'),
(7, 'SUBI', 'N', 0, '2008-11-27 08:30:19'),
(8, 'Admin', 'Y', 90, '2008-11-27 08:30:19'),
(9, 'Admin Manager', 'Y', 90, '2008-11-27 08:30:19'),
(10, 'Branch Manager', 'Y', 90, '2008-11-27 08:30:19'),
(11, 'TELLER', 'Y', 90, '2008-11-27 08:30:19'),
(12, 'Call', 'Y', 90, '2008-11-27 08:30:19'),
(13, 'COLLECTOR', 'Y', 90, '2008-11-27 08:30:19'),
(14, 'Support', 'Y', 90, '2008-11-27 08:30:19'),
(15, 'SUPI Manager', 'Y', 90, '2008-11-27 08:30:19'),
(16, 'MLRO', 'Y', 90, '2008-11-27 08:30:19');

CREATE TABLE IF NOT EXISTS `payout_reciept` (
  `reciept_id` int(11) NOT NULL auto_increment,
  `doc_id` varchar(255) collate utf8_unicode_ci NOT NULL,
  `doc_name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `doc_mime` varchar(255) collate utf8_unicode_ci NOT NULL,
  `compressed` varchar(1) collate utf8_unicode_ci NOT NULL,
  `compression_type` varchar(10) collate utf8_unicode_ci NOT NULL,
  `doc_binary` mediumblob NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `ref_id` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`reciept_id`)
) ENGINE=MyISAM ;


ALTER TABLE `ref_generator` ADD `prefixValue` VARCHAR( 100 ) NULL COMMENT 'In case of distributor than distributor id will be stored here' AFTER `prefix` ;

ALTER TABLE `ref_generator` ADD `userPrefix` VARCHAR( 20 ) NULL COMMENT 'prefix for specific distributor';

ALTER TABLE `relatedExchangeRate` CHANGE `id` `id` INT NOT NULL AUTO_INCREMENT;

CREATE TABLE `setRoutineRight` (
  `id` bigint(20) NOT NULL auto_increment,
  `rightValue` varchar(250) NOT NULL,
  `rightName` varchar(50) NOT NULL,
  `country` varchar(20) NOT NULL,
  `status` enum('Enabled','Disabled') NOT NULL default 'Enabled',
  `updated` date NOT NULL,
  PRIMARY KEY  (`id`)
);

ALTER TABLE `sub_agent_account` ADD `settleAmount` varchar(30) NOT NULL;

ALTER TABLE `sub_agent_account` ADD `SID` bigint(20) NOT NULL;

ALTER TABLE `sub_agent_Dist_account` ADD `actAs` varchar(20) NOT NULL default 'Agent';

ALTER TABLE `transactions` ADD `chequeAmount` FLOAT NOT NULL DEFAULT '0' AFTER `chequeNo`;

ALTER TABLE `transactions` ADD `faxNumber` INT NULL ;

ALTER TABLE `transactions` ADD `dispatchNumber` INT NOT NULL DEFAULT '0';

ALTER TABLE `transactions` ADD `settlementDistributorCommisson` FLOAT( 7, 2 ) NULL DEFAULT '0.00';

ALTER TABLE `transactions` ADD `chequeAmount` FLOAT NOT NULL DEFAULT '0' AFTER `chequeNo`;

ALTER TABLE `transactions` ADD `dispatchDate` DATE NULL COMMENT 'will store the dispatch date related to dispatch number';

CREATE TABLE IF NOT EXISTS `transaction_modify_history` (
  `id` int(5) NOT NULL auto_increment,
  `transid` int(11) NOT NULL,
  `change_log` text  NOT NULL,
  `userid` varchar(11) NOT NULL,
  `datetime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='to log about the what is modify in the amended transaction c' AUTO_INCREMENT=7 ;


DELETE FROM user_functionality WHERE user_functionality.functionCode = 'right160 ';