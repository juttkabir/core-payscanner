DROP TABLE `config` ;

CREATE TABLE `config` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`varName` VARCHAR( 100 ) NOT NULL ,
`value` VARCHAR( 256 ) NOT NULL ,
`oldValue` VARCHAR( 256 ) NOT NULL ,
`description` VARCHAR( 100 ) NOT NULL ,
`detailDesc` TEXT NOT NULL ,
`isFlag` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N',
`client` VARCHAR( 50 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `teller` ADD `Reason` TEXT NOT NULL AFTER `is_active` ;