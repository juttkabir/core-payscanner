CREATE TABLE `receipt_range` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`agentID` BIGINT( 20 ) NOT NULL DEFAULT '0',
`rangeFrom` VARCHAR( 30 ) NOT NULL ,
`rangeTo` VARCHAR( 30 ) NOT NULL ,
`used` VARCHAR( 30 ) NOT NULL DEFAULT '0',
`created` DATE NOT NULL DEFAULT '0000-00-00'
) ENGINE = MYISAM ;

ALTER TABLE `receipt_range` CHANGE `created` `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `receipt_range` CHANGE `rangeFrom` `rangeFrom` FLOAT NOT NULL DEFAULT '0',
CHANGE `rangeTo` `rangeTo` FLOAT NOT NULL DEFAULT '0',
CHANGE `used` `used` FLOAT NOT NULL DEFAULT '0';

ALTER TABLE `receipt_range` CHANGE `used` `used` VARCHAR( 30 ) NOT NULL DEFAULT '0';

CREATE TABLE `trans_limit` (
  `limitID` bigint(20) NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL,
  `limit_type` enum('Transaction','Days') NOT NULL default 'Days',
  `duration` int(11) NOT NULL,
  `limitValue` float NOT NULL,
  `updation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `apply_date` date NOT NULL,
  `currency` varchar(10) NOT NULL,
  PRIMARY KEY  (`limitID`)
);
