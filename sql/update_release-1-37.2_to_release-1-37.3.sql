ALTER TABLE `admin` ADD `hasClave` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';
ALTER TABLE `customer` ADD `secMiddleName` VARCHAR( 40 ) NOT NULL ;
ALTER TABLE `beneficiary` ADD `secMiddleName` VARCHAR( 40 ) NOT NULL ;
CREATE TABLE `clave_range` (
  `id` int(11) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `rangeFrom` double NOT NULL default '0',
  `rangeTo` double NOT NULL default '0',
  `prefix` varchar(20) NOT NULL,
  `used` varchar(30) NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
);

CREATE TABLE `transactionExtended` (
  `id` bigint(20) NOT NULL auto_increment,
  `transID` bigint(20) NOT NULL,
  `agentID` bigint(20) NOT NULL,
  `margin` double NOT NULL,
  `payeeRef` varchar(25) NOT NULL,
  `clave` varchar(25) NOT NULL,
  `transmissionRef` varchar(25) NOT NULL,
  `distSubRef` varchar(25) NOT NULL,
  `benAgentID` bigint(20) NOT NULL,
  `isExported` char(5) NOT NULL default '',
  `transmissionRefCount` bigint(20) NOT NULL,
  `distRef` varchar(25) NOT NULL,
  PRIMARY KEY  (`id`)
);

ALTER TABLE `transactions` CHANGE `custAgentParentID` `custAgentParentID` BIGINT NOT NULL DEFAULT '0',
CHANGE `benAgentParentID` `benAgentParentID` BIGINT NOT NULL DEFAULT '0';


ALTER TABLE `cm_cust_credit_card` CHANGE `expiryDate` `expiryDate` DATE NOT NULL DEFAULT '0000-00-00';

ALTER TABLE `tbl_export_fields` ADD `fieldSperator` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `tbl_export_file` ADD `showLable` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';


ALTER TABLE `exchangerate` ADD `CalculatedPrimaryRate` DOUBLE NOT NULL AFTER `marginType` ,
ADD `intermediateSecondaryRate` DOUBLE NOT NULL AFTER `CalculatedPrimaryRate` ;

ALTER TABLE `tbl_export_fields` ADD `isPattern` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N',
ADD `patternType` VARCHAR( 150 ) NOT NULL ;

ALTER TABLE `transactions` ADD `settlementRate` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `transactions` ADD `restoredBy` VARCHAR( 30 ) NOT NULL ;
ALTER TABLE `complianceLogic` CHANGE `message` `message` TEXT  NOT NULL ;