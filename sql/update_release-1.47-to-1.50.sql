-- Denomination based exchange rate 
-- Ticket 4269
ALTER TABLE `exchangerate` ADD `isDenomination` CHAR( 1 ) NOT NULL DEFAULT 'N' COMMENT 'Is exchange rate is denomination based';

CREATE TABLE `denominationBasedRate` (
`id` INT( 8 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`parentRateId` INT( 8 ) NOT NULL ,
`amountFrom` INT( 6 ) NOT NULL ,
`amountTo` INT( 6 ) NOT NULL ,
`exchangeRate` FLOAT( 6, 3 ) NOT NULL ,
`margin` FLOAT( 6, 3 ) NOT NULL
) ENGINE = MYISAM COMMENT = 'To store the related exchange rate for denomination based exchange rate';
