-- Ticket #3601 Merchant Finance Automated Export Transactions
-- Script added by Waqas Bin Hasan on September 06, 2008

CREATE TABLE `distributor_email` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`distributorId` BIGINT( 20 ) NOT NULL ,
`emailTo` VARCHAR( 255 ) NOT NULL ,
`emailCC` VARCHAR( 255 ) NOT NULL ,
`emailBCC` VARCHAR( 255 ) NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = MYISAM ;

-- For A and D users settlement currencies
-- Ticket #3709
ALTER TABLE `admin` ADD `settlementCurrencies` VARCHAR( 50 ) NULL COMMENT 'this field contains both settlement curriecy of AnD as agent and as distributor separeted with ''I'' sign respectivly';

-- Ticket #3820 
-- adding the fee sub type for "Denomination Based Fee", "Agent and Denomination Based Fee" and "Agent Base Fee"
-- which can be fixed or percent based
ALTER TABLE `imfee` ADD `feeSubType` VARCHAR( 20 ) NOT NULL DEFAULT 'fix';

ALTER TABLE `manageFee` ADD `feeSubType` VARCHAR( 20 ) NOT NULL DEFAULT 'fix';

-- Ticket # 3956
-- Refrence number generation based on the distributor specific critreia
ALTER TABLE `ref_generator` ADD `prefixValue` VARCHAR( 100 ) NULL COMMENT 'In case of distributor than distributor id will be stored here' AFTER `prefix` ;
ALTER TABLE `ref_generator` ADD `userPrefix` VARCHAR( 20 ) NULL COMMENT 'prefix for specific distributor';

-- Update the field length of name field of admin table
-- Ticket #3956
ALTER TABLE `admin` CHANGE `name` `name` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-- Ticket #3965
ALTER TABLE `transactions` ADD `dispatchDate` DATE NULL COMMENT 'will store the dispatch date related to dispatch number';
-- Ticket #3676 
CREATE TABLE IF NOT EXISTS `payout_reciept` (
  `reciept_id` int(11) NOT NULL auto_increment,
  `doc_id` varchar(255) collate utf8_unicode_ci NOT NULL,
  `doc_name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `doc_mime` varchar(255) collate utf8_unicode_ci NOT NULL,
  `compressed` varchar(1) collate utf8_unicode_ci NOT NULL,
  `compression_type` varchar(10) collate utf8_unicode_ci NOT NULL,
  `doc_binary` mediumblob NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `ref_id` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`reciept_id`)
) ENGINE=MyISAM ;

-- Ticket #3965
ALTER TABLE `bankDetails` ADD `originalBankId` VARCHAR( 100 ) NOT NULL ;


-- Ticket 4044
-- alter table to adjust the denomination based fee interval
ALTER TABLE `configure_commission` ADD `interval` FLOAT( 7, 1 ) NULL COMMENT 'If the commission is based on the interval or denomination than this field will hold the interval value';

