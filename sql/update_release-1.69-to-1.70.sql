/*
* Added by Niaz Ahmad at 10-06-2009 @5020 
* Added HM Treasury List under Compliance Module
* This table use for core compliance functionalities
*/
-- 
-- Table structure for table `compliance_hm_treasury`
-- 

CREATE TABLE IF NOT EXISTS `compliance_hm_treasury` (
  `hm_id` int(11) NOT NULL auto_increment,
  `list_id` smallint(6) NOT NULL,
  `title` text NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `full_name` varchar(250) NOT NULL,
  `name1` varchar(100) NOT NULL,
  `name2` varchar(100) NOT NULL,
  `name3` varchar(100) NOT NULL,
  `name4` varchar(50) NOT NULL,
  `name5` varchar(50) NOT NULL,
  `name6` varchar(50) NOT NULL,
  `name7` varchar(50) NOT NULL,
  `full_address` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `address3` text NOT NULL,
  `address4` text NOT NULL,
  `address5` text NOT NULL,
  `address6` text NOT NULL,
  `add_remarks` text NOT NULL,
  `zip_code1` varchar(150) NOT NULL,
  `zip_code2` varchar(150) NOT NULL,
  `zip_code3` varchar(150) NOT NULL,
  `country1` varchar(100) NOT NULL,
  `country2` varchar(100) NOT NULL,
  `country3` varchar(100) NOT NULL,
  `nationality1` varchar(100) NOT NULL,
  `nationality2` varchar(100) NOT NULL,
  `nationality3` varchar(100) NOT NULL,
  `dob1` date NOT NULL,
  `dob2` date NOT NULL,
  `dob_town` varchar(100) NOT NULL,
  `dob_country` varchar(100) NOT NULL,
  `passport_no` varchar(50) NOT NULL,
  `passport_detail` text NOT NULL,
  `id_title` varchar(100) NOT NULL,
  `id_no` varchar(100) NOT NULL,
  `other_details` text NOT NULL,
  `group_type` varchar(100) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  `alias_type` varchar(100) NOT NULL,
  `regime` varchar(100) NOT NULL,
  `listed_on` date NOT NULL,
  `last_updated` date NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `is_disabled` enum('Y','N') NOT NULL default 'Y',
  `user_database_id` varchar(50) NOT NULL,
  `dated` datetime NOT NULL,
  PRIMARY KEY  (`hm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8116 ;

INSERT INTO `complianceLists` (
`listName` ,
`Description` ,
`LastModified` ,
`CriticalLevel` ,
`remarks`
)
VALUES (
 'HM Treasury', 'HM Treasury', '2009-06-13 12:52:00', 'H', ''
);
