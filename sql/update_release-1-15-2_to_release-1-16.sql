CREATE TABLE `tbl_export_file` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`File_Name` VARCHAR( 100 ) NOT NULL ,
`isEnable` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'Yes',
`Format` ENUM( 'xls', 'csv', 'txt' ) NOT NULL DEFAULT 'csv',
`FileLable` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `tbl_export_fields` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`fileID` BIGINT( 20 ) NOT NULL ,
`payex_field` VARCHAR( 100 ) NOT NULL ,
`isActive` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N',
`Lable` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `tbl_export_fields` ADD `tableName` VARCHAR( 100 ) NOT NULL AFTER `payex_field` ;

ALTER TABLE `beneficiary` ADD `CPF` VARCHAR( 50 ) NOT NULL AFTER `IDNumber` ;

ALTER TABLE `cm_beneficiary` ADD `CPF` VARCHAR( 5 ) NOT NULL AFTER `oldNICNumber` ;

INSERT INTO `tbl_export_fields` (`id`, `fileID`, `payex_field`, `tableName`, `isActive`, `Lable`) VALUES (1, 1, 'refNumberIM', 'transactions', 'Y', 'Order Number'),
(2, 1, 'transDate', 'transactions', 'Y', 'Remittance Date'),
(4, 1, 'transAmount', 'transactions', 'Y', 'Dollar Amount'),
(5, 1, 'exchangeRate', 'transactions', 'Y', 'Rate'),
(6, 1, 'localAmount', 'transactions', 'Y', 'Real Amount'),
(7, 1, 'firstName', 'customer', 'Y', 'Sender Name'),
(9, 1, 'firstName', 'beneficiary', 'Y', 'Beneficiary Name'),
(11, 1, 'Address', 'beneficiary', 'Y', 'Beneficiary Address'),
(13, 1, 'City', 'beneficiary', 'Y', 'Beneficiary City'),
(15, 1, 'State', 'beneficiary', 'Y', 'Beneficiary State'),
(17, 1, 'Phone', 'beneficiary', 'Y', 'Beneficiary Telephone'),
(19, 1, 'CPF', 'beneficiary', 'Y', 'CPF'),
(21, 1, 'bankNumber', 'bank_data', 'Y', 'Bank Number'),
(23, 1, 'bankName', 'bankDetails', 'Y', 'Bank Name'),
(25, 1, 'branchCode', 'bankDetails', 'Y', 'Branch'),
(27, 1, 'accNo', 'bankDetails', 'Y', 'Account Number'),
(29, 1, 'accountType', 'bankDetails', 'Y', 'Account Type');

INSERT INTO `tbl_export_file` (`id`, `File_Name`, `isEnable`, `Format`, `FileLable`) VALUES (1, 'export_trans_file.php', 'Yes', 'txt', 'Export Transactions');
