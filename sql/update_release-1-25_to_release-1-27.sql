ALTER TABLE `customer` CHANGE `created` `created` DATETIME NOT NULL DEFAULT '0000-00-00';


UPDATE `config` SET `description` = 'Compliance Amount Limit',
`detailDesc` = 'This is the lower threshold of the cumulative Sender transaction amount.' WHERE `id` =5 LIMIT 1 ;


UPDATE `config` SET `description` = 'Amount Controller for AML Report',
`detailDesc` = 'This is the control for which the AML Report will run by default. However, the amount can be changed while running the report.' WHERE `id` =6 LIMIT 1 ;

UPDATE `config` SET `detailDesc` = 'This is the upper threshold of the cumulative Sender transaction amount.' WHERE `id` =7 LIMIT 1 ;

UPDATE `config` SET `description` = 'No. of Days',
`detailDesc` = 'This is the number of back days when the cumulative transaction amounts for the sender are calculated.' WHERE `id` =8 LIMIT 1 ;


UPDATE `config` SET `detailDesc` = 'This is the amount to trigger the individual transaction compliance value.' WHERE `id` =9 LIMIT 1 ;


CREATE TABLE `SharedExchangeRate` (
  `shareXchangeId` int(11) NOT NULL auto_increment,
  `localXchangeId` int(11) NOT NULL,
  `remoteXchangeId` int(11) NOT NULL,
  `createdServer` varchar(25) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  PRIMARY KEY  (`shareXchangeId`)
);

CREATE TABLE `SharedFee` (
  `shareFeeId` int(11) NOT NULL auto_increment,
  `localFeeId` int(11) NOT NULL,
  `remoteFeeId` int(11) NOT NULL,
  `createdServer` varchar(25) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  PRIMARY KEY  (`shareFeeId`)
);

CREATE TABLE `email` (
  `id` bigint(120) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `attachment` varchar(250) NOT NULL,
  `textBody` text NOT NULL,
  PRIMARY KEY  (`id`)
);


CREATE TABLE `emailReminder` (
  `id` int(120) NOT NULL auto_increment,
  `agentID` varchar(250) NOT NULL,
  `messageId` varchar(250) NOT NULL,
  `email` int(120) NOT NULL,
  `reminder` int(120) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `email` (`email`),
  KEY `reminder` (`reminder`)
);



CREATE TABLE `jointClients` (
  `clientId` smallint(6) NOT NULL auto_increment,
  `clientName` varchar(75) NOT NULL,
  `serverAddress` varchar(150) NOT NULL default 'localHost',
  `userName` varchar(75) NOT NULL,
  `password` varchar(75) NOT NULL,
  `dataBaseName` varchar(75) NOT NULL,
  `isEnabled` enum('Y','N') NOT NULL default 'N',
  `baseAgentId` int(11) NOT NULL,
  `baseDistributor` int(11) NOT NULL,
  `baseAnD` int(11) NOT NULL,
  `commissionShare` float NOT NULL,
  `commissionShareType` varchar(30) NOT NULL,
  PRIMARY KEY  (`clientId`)
);


CREATE TABLE `reminder` (
  `id` int(11) NOT NULL,
  `day` varchar(250) NOT NULL,
  `msg` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE `reminderReport` (
  `id` int(11) NOT NULL,
  `report` varchar(250) NOT NULL,
  `reminder` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `reminder` (`reminder`)
);

CREATE TABLE `sharedTransactions` (
  `sharedTransId` bigint(20) NOT NULL auto_increment,
  `localTrans` bigint(20) NOT NULL,
  `remoteTrans` bigint(20) NOT NULL,
  `generatedLocally` enum('Y','N') NOT NULL,
  `remoteServerId` smallint(6) NOT NULL,
  PRIMARY KEY  (`sharedTransId`)
);


CREATE TABLE `sharedUsers` (
  `id` smallint(6) NOT NULL auto_increment,
  `localServerId` int(11) NOT NULL,
  `availableAt` smallint(6) NOT NULL,
  `remoteServerId` int(11) NOT NULL,
  `availableAs` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
);