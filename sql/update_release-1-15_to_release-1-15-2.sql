ALTER TABLE `agents_customer_account` ADD `note` VARCHAR( 256 ) NOT NULL ;

ALTER TABLE `customer_account` ADD `modifiedBy` VARCHAR( 35 ) NOT NULL ,
ADD `note` VARCHAR( 256 ) NOT NULL ;

ALTER TABLE `barclayspayments` ADD `isDeleted` ENUM( 'N', 'Y' ) NOT NULL DEFAULT 'N' AFTER `isProcessed` ;

ALTER TABLE `natwestpayments` ADD `isDeleted` ENUM( 'N', 'Y' ) NOT NULL DEFAULT 'N' AFTER `isProcessed` ;


ALTER TABLE `agent_account` CHANGE `description` `description` VARCHAR( 90 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `agent_account` CHANGE `note` `note` VARCHAR( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `bank_account` CHANGE `description` `description` VARCHAR( 90 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `cm_beneficiary` ADD `other_title` VARCHAR( 100 ) NOT NULL ,
ADD `otherId` BIGINT( 20 ) NOT NULL ,
ADD `otherid_name` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `cm_collection_point` CHANGE `disableReason` `disableReason` VARCHAR( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `imfee` CHANGE `feeType` `feeType` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `manageFee` CHANGE `feeType` `feeType` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `teller` ADD `balance` BIGINT( 20 ) NOT NULL DEFAULT '0' AFTER `limit_alert` ;

ALTER TABLE `countryTime` CHANGE `timeDiff` `timeDiff` VARCHAR( 100 ) NOT NULL DEFAULT '00:00';

ALTER TABLE `cities` DROP `deliveryTime` ,
DROP `currencyRec` ,
DROP `serviceAvailable` ,
DROP `countryType` ,
DROP `bankCharges` ,
DROP `outCurrCharges` ;
