
ALTER TABLE `compliancePerson` ADD `isDisabled` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';

ALTER TABLE `compliancePerson` ADD `userDataBaseID` VARCHAR( 50 ) NOT NULL ;

-- Table structure for table `compliancePEP`
-- 

CREATE TABLE IF NOT EXISTS `compliancePEP` (
  `comp_PEPID` bigint(20) NOT NULL auto_increment,
  `listID` smallint(6) NOT NULL,
  `sno` smallint(6) NOT NULL,
  `pep_title` text NOT NULL,
  `fullName` varchar(150) NOT NULL,
  `position` varchar(50) NOT NULL,
  `portfolio` varchar(100) NOT NULL,
  `LastModified` datetime NOT NULL,
  `isDisabled` enum('Y','N') NOT NULL default 'Y',
  `userDataBaseID` varchar(50) NOT NULL,
  PRIMARY KEY  (`comp_PEPID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2172 ;

