ALTER TABLE `configure_commission` CHANGE `limitValue` `limitValue` FLOAT NOT NULL DEFAULT '0';
ALTER TABLE `customer` CHANGE `IDissuedate` `IDissuedate` DATE NOT NULL DEFAULT '0000-00-00';
ALTER TABLE `customer` CHANGE `IDExpiry` `IDExpiry` DATE NOT NULL DEFAULT '0000-00-00';
ALTER TABLE `beneficiary` CHANGE `IDissuedate` `IDissuedate` DATE NOT NULL DEFAULT '0000-00-00';
ALTER TABLE `beneficiary` CHANGE `IDexpirydate` `IDexpirydate` DATE NOT NULL DEFAULT '0000-00-00';