ALTER TABLE `exchangerate` ADD `countryOrigin` VARCHAR( 100 ) NOT NULL ;

CREATE TABLE `countries` (
  `countryId` bigint(20) NOT NULL auto_increment,
  `countryName` varchar(60) NOT NULL,
  `countryCode` varchar(10) NOT NULL,
  `countryType` varchar(45) NOT NULL,
  `currency` varchar(10) NOT NULL,
  PRIMARY KEY  (`countryId`)
);

CREATE TABLE `services` (
  `serviceId` bigint(20) NOT NULL auto_increment,
  `fromCountryId` bigint(20) NOT NULL,
  `toCountryId` bigint(20) NOT NULL,
  `serviceAvailable` varchar(255) NOT NULL,
  `deliveryTime` varchar(100) NOT NULL,
  `bankCharges` float NOT NULL,
  `outCurrCharges` float NOT NULL,
  `currencyRec` varchar(25) NOT NULL,
  PRIMARY KEY  (`serviceId`)
);