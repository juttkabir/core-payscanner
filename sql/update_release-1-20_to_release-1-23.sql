ALTER TABLE `customer` ADD `accumulativeAmount` VARCHAR( 250 ) NULL ;

CREATE TABLE `document_names` (
`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`description` VARCHAR( 500 ) NOT NULL
) ENGINE = MYISAM ;


CREATE TABLE `document_category` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(500) NOT NULL,
  `categoryId` varchar(250) NOT NULL,
  `documentId` bigint(15) NOT NULL,
  `tableField` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;




CREATE TABLE `document_upload` (
  `id` bigint(20) NOT NULL auto_increment,
  `documentId` varchar(250) NOT NULL,
  `userId` varchar(250) NOT NULL,
  `path` varchar(500) NOT NULL,
  `status` varchar(250) NOT NULL,
  `isUploaded` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;




CREATE TABLE `category` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `category`
-- 

INSERT INTO `category` (`id`, `description`) VALUES (1, 'ID Details'),
(2, 'Bank Details');





