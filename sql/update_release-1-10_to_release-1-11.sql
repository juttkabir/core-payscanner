CREATE TABLE `login_history` (
  `history_id` bigint(20) NOT NULL auto_increment,
  `login_time` datetime NOT NULL,
  `login_name` varchar(45) NOT NULL,
  `access_ip` varchar(140) NOT NULL,
  `login_type` varchar(60) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`history_id`)
);


CREATE TABLE `teller_account_limit` (
  `id` bigint(20) NOT NULL auto_increment,
  `teller_id` int(11) NOT NULL,
  `limit_date` date NOT NULL,
  `account_limit` varchar(30) NOT NULL,
  `used_limit` varchar(30) NOT NULL,
  `limit_alert` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
);


CREATE TABLE `login_activities` (
  `activity_id` bigint(20) NOT NULL auto_increment,
  `login_history_id` bigint(20) NOT NULL,
  `activity` varchar(254) NOT NULL,
  `activity_time` datetime NOT NULL,
  `action_for` varchar(45) NOT NULL,
  `table_name` varchar(60) NOT NULL,
  `description` varchar(254) NOT NULL,
  PRIMARY KEY  (`activity_id`)
);


CREATE TABLE `amended_transactions` (
  `amendID` bigint(20) unsigned NOT NULL auto_increment,
  `transID` bigint(20) unsigned NOT NULL,
  `transType` enum('Pick up','Bank Transfer','Home Delivery','') NOT NULL default '',
  `customerID` bigint(20) unsigned NOT NULL default '0',
  `benID` bigint(20) unsigned NOT NULL default '0',
  `custAgentID` bigint(20) unsigned NOT NULL default '0',
  `benAgentID` bigint(20) NOT NULL default '0',
  `exchangeID` bigint(20) unsigned NOT NULL default '0',
  `refNumber` varchar(32) NOT NULL default '',
  `refNumberIM` varchar(50) NOT NULL default '',
  `transAmount` float NOT NULL default '0',
  `exchangeRate` float NOT NULL default '0',
  `localAmount` double NOT NULL default '0',
  `IMFee` float NOT NULL default '0',
  `bankCharges` float NOT NULL default '0',
  `totalAmount` float NOT NULL default '0',
  `transactionPurpose` varchar(100) NOT NULL default '',
  `fundSources` varchar(100) NOT NULL default '',
  `moneyPaid` varchar(100) NOT NULL default '',
  `Declaration` varchar(100) NOT NULL default '',
  `transStatus` varchar(20) NOT NULL default 'Pending',
  `addedBy` varchar(32) NOT NULL default '',
  `transDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `verifiedBy` varchar(30) NOT NULL default '',
  `authorisedBy` varchar(30) NOT NULL default '',
  `cancelledBy` varchar(30) NOT NULL default '',
  `recalledBy` varchar(20) NOT NULL default '',
  `isSent` enum('Y','N') NOT NULL default 'N',
  `trackingNum` varchar(30) NOT NULL default '',
  `trans4Country` varchar(30) NOT NULL default '',
  `remarks` varchar(255) NOT NULL default '',
  `authoriseDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryOutDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `deliveryDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `cancelDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rejectDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `failedDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `suspeciousDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `recalledDate` datetime NOT NULL default '0000-00-00 00:00:00',
  `fromCountry` varchar(30) NOT NULL default '',
  `toCountry` varchar(30) NOT NULL default '',
  `currencyFrom` varchar(10) NOT NULL default '',
  `currencyTo` varchar(10) NOT NULL default '',
  `custAgentParentID` int(11) NOT NULL default '0',
  `benAgentParentID` int(11) NOT NULL default '0',
  `is_exported` varchar(10) NOT NULL default '',
  `PINCODE` int(5) NOT NULL default '0',
  `createdBy` varchar(10) NOT NULL default '',
  `collectionPointID` bigint(20) NOT NULL default '0',
  `transRefID` varchar(30) NOT NULL default '',
  `AgentComm` float NOT NULL default '0',
  `distributorComm` float NOT NULL default '0',
  `CommType` varchar(30) NOT NULL default '',
  `other_pur` varchar(35) NOT NULL default '',
  `admincharges` varchar(30) NOT NULL default '',
  `cashCharges` int(11) NOT NULL default '0',
  `holdedBy` varchar(20) NOT NULL default '',
  `refundFee` varchar(10) NOT NULL default '',
  `question` varchar(100) NOT NULL default '',
  `answer` varchar(200) NOT NULL default '',
  `tip` text,
  `outCurrCharges` float NOT NULL default '0',
  `discountRequest` varchar(255) NOT NULL,
  `discountType` varchar(255) NOT NULL,
  `discounted_amount` float NOT NULL default '0',
  `deliveredBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(40) NOT NULL,
  `modificationDate` datetime NOT NULL,
  `holdDate` datetime NOT NULL,
  `verificationDate` datetime NOT NULL,
  `unholdBy` varchar(40) NOT NULL,
  `unholdDate` datetime NOT NULL,
  PRIMARY KEY  (`amendID`)
);


ALTER TABLE `transactions` ADD `creation_date_used` DATE NOT NULL ;

ALTER TABLE `transactions` ADD `holdDate` DATETIME NOT NULL ,
ADD `verificationDate` DATETIME NOT NULL ,
ADD `unholdBy` VARCHAR( 40 ) NOT NULL ,
ADD `unholdDate` DATETIME NOT NULL ;

ALTER TABLE `teller` ADD `account_limit` VARCHAR( 30 ) NOT NULL ,
ADD `limit_alert` VARCHAR( 30 ) NOT NULL ;