ALTER TABLE `customer` ADD `remarks` VARCHAR( 256 ) NOT NULL AFTER `documentProvided` ;

ALTER TABLE `cm_customer` ADD `documentProvided` VARCHAR( 100 ) NOT NULL ,
ADD `remarks` VARCHAR( 256 ) NOT NULL ;

ALTER TABLE `admin` ADD `defaultMoneyPaid` VARCHAR( 100 ) NOT NULL ;

ALTER TABLE `sessions` CHANGE `username` `username` VARCHAR( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `admin` ADD `isOnline` ENUM( 'N', 'Y' ) NOT NULL DEFAULT 'N';

ALTER TABLE `manageFee` ADD `rangeToWithFee` FLOAT NOT NULL DEFAULT '0' AFTER `amountRangeTo` ;

ALTER TABLE `imfee` ADD `rangeToWithFee` FLOAT NOT NULL DEFAULT '0' AFTER `amountRangeTo` ;

ALTER TABLE `tbl_export_file` ADD `fieldSpacing` VARCHAR( 100 ) NOT NULL ,
ADD `lineBreak` VARCHAR( 100 ) NOT NULL ;

ALTER TABLE `teller` ADD `rights` VARCHAR( 240 ) NOT NULL ;

ALTER TABLE `cm_beneficiary` CHANGE `CPF` `CPF` VARCHAR( 50 ) NOT NULL ;

INSERT INTO `tbl_export_file` (`id`, `File_Name`, `isEnable`, `Format`, `FileLable`) 
VALUES (2, 'export_trans_file.php', 'Yes', 'txt', 'Export Transactions');

ALTER TABLE `tbl_export_fields` ADD `tempID` INT NOT NULL ,
ADD `exportDefaultID` INT NOT NULL ,
ADD `field_title` VARCHAR( 100 ) NOT NULL ;

INSERT INTO `tbl_export_fields` (`id`, `fileID`, `payex_field`, `tableName`, `isActive`, `Lable`, `tempID`, `exportDefaultID`, `field_title`) 
VALUES (31, 2, 'refNumberIM', 'transactions', 'Y', 'Numeris', 0, 0, ''),
(32, 2, 'Date', 'transactions', 'Y', 'Date', 0, 0, ''),
(33, 2, 'transDate', 'transactions', 'Y', 'Bank Operation Date', 0, 0, ''),
(34, 2, 'PaymentNumber', 'transactions', 'Y', 'Payment Number', 0, 0, ''),
(35, 2, 'accountName', 'customer', 'Y', 'Sender Number', 0, 0, ''),
(36, 2, 'firstName,lastName', 'customer', 'Y', 'Sender Name', 0, 0, ''),
(37, 2, 'registrationNumber', 'customer', 'Y', 'Sender Code Registartion number', 0, 0, ''),
(38, 2, 'BICcode', 'customer', 'Y', 'BIC code', 0, 0, ''),
(39, 2, 'clientCode', 'customer', 'Y', 'Client code in system', 0, 0, ''),
(40, 2, 'initPaymentNum', 'customer', 'Y', 'Initial payment account number', 0, 0, ''),
(41, 2, 'initSendNameSurname', 'customer', 'Y', 'Initial sender name and surname', 0, 0, ''),
(42, 2, 'initSenderPerCode', 'customer', 'Y', 'Initial sender personal code', 0, 0, ''),
(43, 2, 'cp_corresspondent_name', 'cm_collection_point', 'Y', 'Beneficiary collection point branch name', 0, 0, ''),
(44, 2, 'firstName,lastName', 'beneficiary', 'Y', 'Beneficiary name and surname', 0, 0, ''),
(45, 2, 'localAmount', 'transactions', 'Y', 'Amount', 0, 0, ''),
(46, 2, 'currencyTo', 'transactions', 'Y', 'currency code ISO', 0, 0, ''),
(47, 2, 'IBAN', 'bankDetails', 'Y', 'Account Number', 0, 0, ''),
(48, 2, 'Remarks', 'bankDetails', 'Y', 'Comment box to have maximum 1257', 0, 0, '');

CREATE TABLE `sub_account_summary` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` varchar(20) NOT NULL,
  `dated` date NOT NULL,
  `opening_balance` double NOT NULL,
  `closing_balance` double NOT NULL,
  `currency` varchar(10) NOT NULL default 'GBP',
  PRIMARY KEY  (`id`)
);

CREATE TABLE `sub_agent_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`)
);

CREATE TABLE `sub_agent_Dist_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `agentID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `status` varchar(30) NOT NULL,
  `note` varchar(256) NOT NULL,
  `actAs` varchar(20) NOT NULL default 'Agent',
  `currency` varchar(10) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`)
);

CREATE TABLE `sub_bank_account` (
  `aaID` bigint(20) NOT NULL auto_increment,
  `bankID` bigint(20) NOT NULL default '0',
  `dated` date NOT NULL default '0000-00-00',
  `type` varchar(15) NOT NULL default '',
  `amount` double NOT NULL default '0',
  `currency` varchar(10) NOT NULL,
  `modified_by` varchar(30) NOT NULL default '0',
  `TransID` varchar(20) NOT NULL default '0',
  `description` varchar(90) NOT NULL,
  `commission` float NOT NULL,
  `commPackage` varchar(30) NOT NULL,
  PRIMARY KEY  (`aaID`)
);

CREATE TABLE `xml_Errors` (
  `errorId` bigint(20) NOT NULL auto_increment,
  `code` varchar(15) NOT NULL,
  `description` varchar(100) NOT NULL,
  `lastModified` datetime NOT NULL,
  PRIMARY KEY  (`errorId`)
);
