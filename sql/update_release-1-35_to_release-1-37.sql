ALTER TABLE `admin` CHANGE `subagentNum` `subagentNum` VARCHAR( 50 ) NOT NULL;
CREATE TABLE `historicalExchangeRate` (
`id` BIGINT NOT NULL ,
`country` VARCHAR( 100 ) NOT NULL ,
`primaryExchange` FLOAT( 8, 4 ) NOT NULL DEFAULT '0.0000',
`sProvider` VARCHAR( 150 ) NOT NULL ,
`dated` DATETIME NOT NULL DEFAULT '000-00-00 00:00:00',
`currency` VARCHAR( 10 ) NOT NULL ,
`marginPercentage` FLOAT NOT NULL ,
`rateFor` VARCHAR( 40 ) NOT NULL ,
`rateValue` VARCHAR( 75 ) NOT NULL ,
`updationDate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`countryOrigin` VARCHAR( 100 ) NOT NULL ,
`currencyOrigin` VARCHAR( 10 ) NOT NULL ,
`marginType` ENUM( 'percent', 'fixed' ) NOT NULL ,
`intermediateCurrency` VARCHAR( 20 ) NOT NULL ,
`intermediatePrimRate` DOUBLE NOT NULL ,
`agentMarginType` VARCHAR( 15 ) NOT NULL ,
`agentMarginValue` FLOAT NOT NULL ,
`relatedID` VARCHAR( 20 ) NOT NULL ,
`erID` BIGINT( 20 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `historicalExchangeRate` ADD PRIMARY KEY ( `id` ) ;
ALTER TABLE `historicalExchangeRate` CHANGE `id` `id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `exchangerate` ADD `isActive` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';
ALTER TABLE `imfee` ADD `isActive` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';
ALTER TABLE `manageFee` ADD `isActive` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'Y';

CREATE TABLE `shareCountries` (
`shareId` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`countryId` INT NOT NULL ,
`remoteServerId` INT NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `agent_account` ADD INDEX `agentId` ( `agentID` ); 
ALTER TABLE `cm_collection_point` ADD `fromServer` VARCHAR( 20 ) NOT NULL;
CREATE TABLE `importConnectplusTrans` (
  `importID` bigint(20) NOT NULL auto_increment,
  `consecValue` varchar(50) NOT NULL,
  `transRef` varchar(50) NOT NULL,
  `dated` datetime NOT NULL default '0000-00-00 00:00:00',
  `deseciveValue` varchar(50) NOT NULL,
  `beneID` varchar(35) NOT NULL,
  `beneName` varchar(65) NOT NULL,
  `amountGot` float NOT NULL,
  `messageText` varchar(150) NOT NULL,
  `notes` varchar(50) NOT NULL,
  `isResolved` enum('Y','N') NOT NULL default 'N',
  `importDated` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`importID`)
);

ALTER TABLE `agent_account` ADD `settleAmount` VARCHAR( 30 ) NOT NULL ,
ADD `SID` BIGINT( 20 ) NOT NULL ;

ALTER TABLE `agent_account` ADD `currency` VARCHAR( 10 ) NOT NULL ;
