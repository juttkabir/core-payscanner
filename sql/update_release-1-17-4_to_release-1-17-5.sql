ALTER TABLE `beneficiary` CHANGE `IDexpirydate` `IDexpirydate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `admin` ADD `custCountries` VARCHAR( 255 ) NOT NULL AFTER `IDAcountry` ;

ALTER TABLE `customer` ADD `proveAddress` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' AFTER `Address1` ;

ALTER TABLE `cm_customer` ADD `proveAddress` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' AFTER `c_address2` ;



ALTER TABLE `bankDetails` ADD `Remarks` VARCHAR( 256 ) NOT NULL ;

ALTER TABLE `cm_bankdetails` ADD `Remarks` VARCHAR( 256 ) NOT NULL ;

ALTER TABLE `cities` ADD `countryRegion` VARCHAR( 100 ) NOT NULL ;

ALTER TABLE `countries` ADD `countryRegion` VARCHAR( 100 ) NOT NULL ;

UPDATE `cities` SET `countryRegion` = 'European' WHERE `country` IN (
'Austria',
'Belgium',
'Bulgaria',
'Cyprus',
'Denmark',
'Estonia',
'Finland',
'France',
'Germany',
'Greece',
'Hungary',
'Irish Republic',
'Italy',
'Latvia',
'Lithuania',
'Luxembourg',
'Malta',
'Netherlands',
'Poland',
'Portugal',
'Romania',
'Slovakia',
'Slovenia',
'Spain',
'Sweden',
'United Kingdom'
);

UPDATE cities SET countryCode = 'EE' WHERE country = 'Estonia';

UPDATE cities SET countryCode = 'LV' WHERE country = 'Latvia';

UPDATE cities SET countryCode = 'LT' WHERE country = 'Lithuania';

UPDATE cities SET countryCode = 'RO' WHERE country = 'Romania';

UPDATE cities SET countryCode = 'CZ',
countryRegion = 'European' WHERE country = 'Czech Republic';

INSERT INTO `cities` ( `country` , `city` , `countryCode` , `Currency` , `currDesc` , `isoCode` , `countryRegion` )
VALUES ('Irish Republic', '', 'IE', '', '', '', 'European'), ('Luxembourg', '', 'LU', '', '', '', 'European');

INSERT INTO `cities` ( `country` , `city` , `countryCode` , `Currency` , `currDesc` , `isoCode` , `countryRegion` )
VALUES ('Slovakia', '', 'SK', '', '', '', 'European');


ALTER TABLE `admin` ADD `linked_Agent` VARCHAR( 255 ) NOT NULL ;

CREATE TABLE `shortcuts` (
`shortcutID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`link` VARCHAR( 256 ) NOT NULL ,
`link_title` VARCHAR( 256 ) NOT NULL ,
`menu` VARCHAR( 100 ) NOT NULL ,
`username` VARCHAR( 100 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `customer` CHANGE `IDExpiry` `IDExpiry` DATE NOT NULL DEFAULT '0000-00-00' ;

UPDATE `admin` SET `email` = 'payex-feedback@support.horivert.com' WHERE `username` = 'imsuper' ;