-- #8850: AMB: TT Transaction
ALTER TABLE `accounts` ADD `CustID` BIGINT( 20 ) NOT NULL AFTER `showOnDeal` ;
CREATE TABLE `dev_amb`.`tt_transactions` (
`customer_Id` INT( 20 ) NOT NULL ,
`ben_Id` INT( 20 ) NOT NULL ,
`sender_account_number` VARCHAR( 20 ) NOT NULL ,
`sending_currency` VARCHAR( 3 ) NOT NULL ,
`sending_amount` FLOAT NOT NULL ,
`receiving_account_number` VARCHAR( 20 ) NOT NULL ,
`receiving_currency` VARCHAR( 3 ) NOT NULL ,
`receiving_amount` FLOAT NOT NULL ,
`exchange_rate` FLOAT NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL ,
`created_by` VARCHAR( 30 ) NOT NULL ,
`status` VARCHAR( 20 ) NOT NULL ,
`remarks` TEXT NOT NULL
) ENGINE = MYISAM 
ALTER TABLE `tt_transactions` ADD `id` INT( 20 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST ;
ALTER TABLE `account_ledgers` CHANGE `transType` `transType` ENUM( 'FT', 'CE', 'TT' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'CE' COMMENT 'transaction type either CE for currency exchange or FT for fund transfer';
CREATE TABLE IF NOT EXISTS `TTbankDetails` (
`bankID` int( 11 ) NOT NULL AUTO_INCREMENT ,
`benID` int( 11 ) NOT NULL default '0',
`transID` int( 11 ) NOT NULL default '0',
`bankName` varchar( 150 ) NOT NULL default '',
`accNo` varchar( 100 ) NOT NULL default '',
`branchCode` varchar( 100 ) NOT NULL default '',
`branchAddress` varchar( 255 ) NOT NULL default '',
`ABACPF` varchar( 100 ) NOT NULL default '',
`IBAN` varchar( 100 ) NOT NULL default '',
`swiftCode` varchar( 100 ) NOT NULL default '',
`accountType` varchar( 30 ) NOT NULL default 'Regular',
`Remarks` varchar( 256 ) NOT NULL ,
`originalBankId` varchar( 100 ) NOT NULL ,
`sortCode` varchar( 30 ) NOT NULL COMMENT 'to hold sort code for bank details',
`BankCode` varchar( 100 ) default NULL ,
`BranchName` varchar( 100 ) default NULL ,
`BranchCity` varchar( 100 ) default NULL ,
`routingNumber` varchar( 100 ) NOT NULL default '0',
PRIMARY KEY ( `bankID` )
) ENGINE = MYISAM DEFAULT CHARSET = latin1 AUTO_INCREMENT =2; 
ALTER TABLE `tt_transactions` CHANGE `status` `status` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '1:Pending-2:Hold-3:Cancelled'; 
-- #6276 - Premier FX - added account manager dropdown on add/update sender page.
ALTER TABLE `customer` ADD `parentID` BIGINT( 20 ) NOT NULL COMMENT 'Customer can be associated with any user i.e admin staff, super admin ';
-- #6276 - Premier FX - Agent commission report shows agent currency's amount 
ALTER TABLE  `transactionExtended` ADD  `agentCurrency` VARCHAR( 10 ) NOT NULL ,ADD  `agentCurrencyCommission` FLOAT NOT NULL ;

-- #6752 Prem FX: Points done
ALTER TABLE `transactions` ADD `transDetails` VARCHAR( 10 ) NOT NULL DEFAULT 'Yes' COMMENT 'If transfer type i.e bank details provided then it should be yes',
ADD `valueDate` DATETIME NOT NULL COMMENT 'value date is entered to maintain separate queue before authorizing transaction';
ALTER TABLE `transactions` ADD INDEX ( `transDetails` , `valueDate` ) ;

ALTER TABLE `transactions` ADD `validValue` CHAR( 2 ) NOT NULL DEFAULT 'Y' COMMENT 'When value dated transaction is validated then it is Y';
ALTER TABLE `transactions` ADD INDEX ( `validValue` ) ;

ALTER TABLE `transactions` ADD `isTraded` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' COMMENT 'isTraded is Y if trading account is matched with any non-traded transaction(s)';
ALTER TABLE `transactions` ADD INDEX ( `isTraded` ) ;

ALTER TABLE `account_details` ADD `isTraded` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' COMMENT 'isTraded is Y if trading account is matched with any non-traded transaction(s)';

CREATE TABLE IF NOT EXISTS `tradeBatchHistory` (
  `id` bigint(20) NOT NULL auto_increment,
  `tradeIDs` text NOT NULL COMMENT 'trade id(s) which is mapped',
  `transIDs` text NOT NULL COMMENT 'trans id(s) mapped to certain trade(s)',
  `amount` float(11,4) NOT NULL,
  `currencyBuy` varchar(10) NOT NULL,
  `currencySell` varchar(10) NOT NULL,
  `tradeRate` float(7,4) NOT NULL,
  `transRate` float(7,4) NOT NULL,
  `profitLoss` float(11,4) NOT NULL COMMENT 'Profit Loss calculated from trade and transactions mapped',
  `batchID` bigint(20) NOT NULL,
  `dated` date NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `batchID` (`batchID`,`dated`)
) ENGINE=MyISAM ; 
ALTER TABLE `transactions` ADD `tradeBatchID` VARCHAR( 50 ) NOT NULL COMMENT 'This is batch ID for trade(s) matched to transaction(s)';
ALTER TABLE `transactions` ADD `tradeProfitLoss` FLOAT( 7, 4 ) NOT NULL COMMENT 'This is profit/loss after making Deal between traded and not-traded';
ALTER TABLE `admin` ADD `agentUserType` VARCHAR( 50 ) NOT NULL COMMENT 'agent user type li.e introducer,management company etc.';