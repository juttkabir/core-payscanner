ALTER TABLE `Campaign_Type` CHANGE `heading` `heading` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
CHANGE `message` `message` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
ALTER TABLE `beneficiary` ADD `issuedBy` VARCHAR( 255 ) NOT NULL AFTER `IDissuedate` ;
ALTER TABLE `customer` ADD `IDissuedate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `IDExpiry` ;
ALTER TABLE `customer` ADD `customerName` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `beneficiary` ADD `beneficiaryName` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `cm_collection_point` ADD `cp_contact_person_name` VARCHAR( 40 ) NOT NULL ;
