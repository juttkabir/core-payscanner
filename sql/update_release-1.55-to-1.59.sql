-- Ticket 4230 
-- New remakrs field to add heavy amount of remarks about customer / sender
ALTER TABLE `customer` ADD `senderRemarks` TEXT NULL COMMENT 'store the remarks at the add update customer';
-- Ticket 3672
-- New fields to display old exchage rates for bank24hr type.
ALTER TABLE `historicalExchangeRate` ADD `_24hrMarginType` VARCHAR( 10 ) NULL AFTER `agentMarginValue` ,
ADD `_24hrMarginValue` FLOAT NULL AFTER `_24hrMarginType` ;