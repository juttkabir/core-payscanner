CREATE TABLE `tbl_export_labels` (
  `id` int(250) NOT NULL auto_increment,
  `fileID` bigint(20) NOT NULL,
  `heading` text NOT NULL,
  `address` enum('Y','N') NOT NULL default 'Y',
  `label1` text NOT NULL,
  `label2` text NOT NULL,
  `label3` text NOT NULL,
  `client` varchar(250) NOT NULL,
  `logo` enum('Y','N') NOT NULL default 'Y',
  `Enable` enum('Y','N') NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
);

ALTER TABLE `tbl_export_file` ADD `conditions`  VARCHAR( 250 ) NOT NULL ,
ADD `description` VARCHAR( 250 ) NOT NULL ;

ALTER TABLE `transactions` ADD `benIDPassword`  VARCHAR( 250 ) NOT NULL ;