CREATE TABLE `oldTransactionGroupRight` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`group` VARCHAR( 255 ) NOT NULL ,
`haveRight` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N',
`backDays` INT( 5 ) NOT NULL ,
`updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = MYISAM COMMENT = 'Rights for user groups to view the old transaction in create transaction';

INSERT INTO `oldTransactionGroupRight` (
`id` ,
`group` ,
`haveRight` ,
`backDays` ,
`updated`
)
VALUES (
NULL , 'admin', 'Y', '30',
CURRENT_TIMESTAMP
), (
NULL , 'SUPA', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUPAI', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUBA', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUBAI', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUPI', 'N', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUBI', 'N', '10',
CURRENT_TIMESTAMP
), (
NULL , 'Admin', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'Admin Manager', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'Branch Manager', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'TELLER', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'Call', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'COLLECTOR', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'Support', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'SUPI Manager', 'Y', '10',
CURRENT_TIMESTAMP
), (
NULL , 'MLRO', 'Y', '10',
CURRENT_TIMESTAMP
);

