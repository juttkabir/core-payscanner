ALTER TABLE `cm_beneficiary` ADD `IBAN` VARCHAR( 50 ) NOT NULL,
ADD `IbanOrBank` VARCHAR( 10 ) NOT NULL DEFAULT 'iban';
ALTER TABLE `cm_bankdetails` ADD `IbanOrBank` VARCHAR( 50 ) NOT NULL DEFAULT 'iban' COMMENT 'by defiault iban is assigned here';
ALTER TABLE `bankDetails` ADD `BankCode` VARCHAR( 100 ) NULL ,
ADD `BranchName` VARCHAR( 100 ) NULL ,
ADD `BranchCity` VARCHAR( 100 ) NULL ;