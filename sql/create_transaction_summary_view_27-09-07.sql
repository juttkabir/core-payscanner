-- phpMyAdmin SQL Dump
-- version 2.8.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Sep 27, 2007 at 05:36 AM
-- Server version: 5.0.22
-- PHP Version: 5.0.4
-- 
-- Database: `dev_moneyhouse`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `benAgent`
-- 

CREATE VIEW `benAgent` AS 
select `ad`.`name` AS `distributorName`,`t`.`benAgentID` AS `benAgentID`,`t`.`transID` AS `transID` from 
(`transactions` `t` join `admin` `ad`) 
where (`t`.`benAgentID` = `ad`.`userID`);


-- phpMyAdmin SQL Dump
-- version 2.8.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Sep 27, 2007 at 05:27 AM
-- Server version: 5.0.22
-- PHP Version: 5.0.4
-- 
-- Database: `dev_moneyhouse`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `transactionSummary`
-- 

CREATE VIEW 
`transactionSummary` AS 
(select `t`.`currencyTo` AS `currencyTo`,`t`.`transStatus` AS `transStatus`,`t`.`currencyFrom` AS `currencyFrom`,`t`.`transDate` AS `transDate`,right(`t`.`transDate`,8) AS `Time`,`t`.`transID` AS `transID`,`t`.`transType` AS `transType`,`t`.`refNumberIM` AS `refNumberIM`,`t`.`totalAmount` AS `totalAmount`,`t`.`transAmount` AS `transAmount`,`a`.`name` AS `name`,`a`.`userID` AS `userID`,`t`.`addedBy` AS `addedBy`,`t`.`moneyPaid` AS `moneyPaid`,`t`.`toCountry` AS `toCountry`,`t`.`IMFee` AS `IMFee`,`t`.`refNumber` AS `refNumber`,`a`.`commPackage` AS `commPackage`,`a`.`agentCommission` AS `agentCommission`,`t`.`AgentComm` AS `AgentComm`,`t`.`CommType` AS `CommType`,`t`.`customerID` AS `customerID`,`t`.`benID` AS `benID`,`t`.`exchangeRate` AS `exchangeRate`,`t`.`localAmount` AS `localAmount`,`t`.`benAgentID` AS `benAgentID`,`t`.`collectionPointID` AS `collectionPointID`,`c`.`firstName` AS `firstName`,`c`.`lastName` AS `lastName`,`b`.`firstName` AS `BeneficiaryFirstName`,`b`.`lastName` AS `BeneficiaryLastName`,`cp`.`cp_branch_name` AS `BranchName`,`cp`.`cp_corresspondent_name` AS `correspondentName`,_utf8'' AS `BankName` 
from ((((`transactions` `t` join `admin` `a`) 
join `customer` `c`) 
join `beneficiary` `b`) 
join `cm_collection_point` `cp`) 
where ((`t`.`custAgentID` = `a`.`userID`) 
and (`t`.`customerID` = `c`.`customerID`) 
and (`t`.`benID` = `b`.`benID`) 
and (`t`.`collectionPointID` = `cp`.`cp_id`))) 
union 
(select `t`.`currencyTo` AS `currencyTo`,`t`.`transStatus` AS `transStatus`,`t`.`currencyFrom` AS `currencyFrom`,`t`.`transDate` AS `transDate`,right(`t`.`transDate`,8) AS `Time`,`t`.`transID` AS `transID`,`t`.`transType` AS `transType`,`t`.`refNumberIM` AS `refNumberIM`,`t`.`totalAmount` AS `totalAmount`,`t`.`transAmount` AS `transAmount`,`a`.`name` AS `name`,`a`.`userID` AS `userID`,`t`.`addedBy` AS `addedBy`,`t`.`moneyPaid` AS `moneyPaid`,`t`.`toCountry` AS `toCountry`,`t`.`IMFee` AS `IMFee`,`t`.`refNumber` AS `refNumber`,`a`.`commPackage` AS `commPackage`,`a`.`agentCommission` AS `agentCommission`,`t`.`AgentComm` AS `AgentComm`,`t`.`CommType` AS `CommType`,`t`.`customerID` AS `customerID`,`t`.`benID` AS `benID`,`t`.`exchangeRate` AS `exchangeRate`,`t`.`localAmount` AS `localAmount`,`t`.`benAgentID` AS `benAgentID`,`t`.`collectionPointID` AS `collectionPointID`,`c`.`firstName` AS `firstName`,`c`.`lastName` AS `lastName`,`b`.`firstName` AS `BeneficiaryFirstName`,`b`.`lastName` AS `BeneficiaryLastName`,_utf8'' AS `BranchName`,_utf8'' AS `correspondentName`,`bd`.`bankName` AS `BankName` 
from ((((`transactions` `t` join `admin` `a`) 
join `customer` `c`) 
join `beneficiary` `b`) 
join `bankDetails` `bd`) 
where ((`t`.`custAgentID` = `a`.`userID`) 
and (`t`.`customerID` = `c`.`customerID`) 
and (`t`.`benID` = `b`.`benID`) and 
(`t`.`transID` = `bd`.`transID`)));
