ALTER TABLE `admin` ADD `defaultDistrib` ENUM( 'N', 'Y' ) NOT NULL DEFAULT 'N';
ALTER TABLE `transactions` ADD `internalRemarks` TEXT NOT NULL AFTER `deliveredBy` ;
CREATE TABLE `banklinepayments` (
  `id` mediumint(9) NOT NULL auto_increment,
  `importedOn` datetime NOT NULL default '0000-00-00 00:00:00',
  `paymentFrom` enum('','customer','agent') NOT NULL default '',
  `username` varchar(30) NOT NULL default '',
  `currency` varchar(10) NOT NULL default '',
  `amount` int(11) NOT NULL default '0',
  `entryDate` date NOT NULL default '0000-00-00',
  `isResolved` enum('N','Y') NOT NULL default 'N',
  `isProcessed` enum('N','Y') NOT NULL default 'N',
  `isDeleted` enum('N','Y') NOT NULL default 'N',
  `unResReason` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `tlaCode` varchar(5) NOT NULL default '',
  PRIMARY KEY  (`id`)
);
