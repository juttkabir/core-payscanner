ALTER TABLE `transactions` ADD `from_server` VARCHAR( 20 ) NULL COMMENT 'store information, if the transaction from the API';
ALTER TABLE `beneficiary` ADD `moth_maiden_name` VARCHAR( 50 ) NULL ;
ALTER TABLE `transactions` ADD `sending_agent` BIGINT NULL COMMENT 'In case of transaction is of API than the sending agent id will kept here.' AFTER `from_server` ;

ALTER TABLE `transactions` ADD `partner_id` VARCHAR( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'CMT txn have multiple partner id to map on payout agent id' AFTER `from_server` ;
 
ALTER TABLE `transactions` ADD `is_parsed` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N';

