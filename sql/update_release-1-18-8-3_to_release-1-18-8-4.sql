ALTER TABLE `tbl_export_fields` ADD `isFixed` ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N',
ADD `Value` VARCHAR( 250 ) NOT NULL ;

ALTER TABLE `ref_generator` ADD `customLength` INT NOT NULL DEFAULT '0' ;

 ALTER TABLE `admin` CHANGE `IDAcountry` `IDAcountry` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;