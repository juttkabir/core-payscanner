ALTER TABLE `admin` ADD INDEX ( `name` );
ALTER TABLE `admin` ADD INDEX ( `username` );
ALTER TABLE `transactions` ADD INDEX ( `custAgentID` );
ALTER TABLE `transactions` ADD INDEX ( `benID` );
ALTER TABLE `transactions` ADD INDEX ( `createdBy` );
ALTER TABLE `transactions` ADD INDEX ( `customerID` );
ALTER TABLE `transactions` ADD INDEX ( `transDate` );
ALTER TABLE `transactions` ADD INDEX ( `transStatus` );
ALTER TABLE `beneficiary` ADD INDEX ( `firstName` );
ALTER TABLE `beneficiary` ADD INDEX ( `lastName` );
ALTER TABLE `customer` ADD INDEX ( `firstName` );
ALTER TABLE `customer` ADD INDEX ( `lastName` );
ALTER TABLE `agent_account` ADD INDEX ( `type` );
ALTER TABLE `agent_account` ADD INDEX ( `status` );

ALTER TABLE `transactions` ADD INDEX ( `transDate` );
ALTER TABLE `transactions` ADD INDEX ( `transStatus` );

ALTER TABLE `customer` ADD INDEX ( `accountName` );
ALTER TABLE `customer` ADD INDEX ( `payinBook` );