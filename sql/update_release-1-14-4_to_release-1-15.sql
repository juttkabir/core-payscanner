DROP TABLE `agent_logfile` ,
`bank-transfer-country` ,
`barclayspayments_customermoudule` ,
`complaintinteracts` ,
`contacts` ,
`states` ,
`user_registration` ,
`transtype` ,
`transtatus` ,
`tblsys_stat` ,
`tbl_error_code` ,
`tbl_transaction_partner` ,
`tbl_transaction_staus` ,
`hcompanyaddress` ,
`hcustomers` ,
`hservices` ;


ALTER TABLE `teller` ADD `accessFromIP` VARCHAR( 255 ) NOT NULL ;

ALTER TABLE `exchangerate` ADD `currencyOrigin` VARCHAR( 10 ) NOT NULL ;
ALTER TABLE `exchangerate` CHANGE `marginPercentage` `marginPercentage` FLOAT( 11 ) NOT NULL DEFAULT '1';

