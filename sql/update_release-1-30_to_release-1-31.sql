


ALTER TABLE `transactions` ADD `payPointType` VARCHAR( 50 ) NOT NULL ;

ALTER TABLE `admin` ADD `PPType` VARCHAR( 50 ) NOT NULL AFTER `subagentNum` ;

ALTER TABLE `cm_cust_credit_card` CHANGE `cardCVV` `PIN` VARCHAR( 4 )  NOT NULL; 

ALTER TABLE `cm_cust_credit_card` ADD `transID` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE `receipt_range` ADD `prefix` VARCHAR( 20 ) NOT NULL AFTER `rangeTo` ;






ALTER TABLE `exchangerate` ADD `CalculatedPrimaryRate` DOUBLE  NOT NULL;


ALTER TABLE `transactions` CHANGE `cashCharges` `cashCharges` FLOAT( 11 ) NOT NULL DEFAULT '0';





ALTER TABLE `exchangerate` ADD `intermediateCurrency` VARCHAR( 20 ) NOT NULL ,
ADD `intermediatePrimRate` FLOAT( 50 ) NOT NULL ,
ADD `intermediateSecondaryRate` FLOAT( 50 ) NOT NULL ;



CREATE TABLE `bankCharges` (
  `chargeID` bigint(20) unsigned NOT NULL auto_increment,
  `origCountry` varchar(100) NOT NULL default '',
  `currencyOrigin` varchar(10) NOT NULL,
  `destCountry` varchar(100) NOT NULL default '',
  `currencyDest` varchar(10) NOT NULL,
  `amountRangeFrom` float NOT NULL default '0',
  `amountRangeTo` float NOT NULL default '0',
  `Charges` float NOT NULL default '0',
  `chargeType` varchar(30) NOT NULL,
  `agentNo` bigint(20) NOT NULL default '0',
  `chargeBasedOn` varchar(100) default NULL,
  PRIMARY KEY  (`chargeID`)
);


ALTER TABLE `transactions` ADD `returnDate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ;



ALTER TABLE `transactions` ADD `returnedBy` VARCHAR( 30 ) NOT NULL ;


ALTER TABLE `transactions` CHANGE `transType` `transType` ENUM( 'Pick up', 'Bank Transfer', 'Home Delivery', 'ATM Card' );


ALTER TABLE `sub_bank_account` ADD `settlementCurrency` VARCHAR( 20 ) NOT NULL ,
ADD `settlementValue` VARCHAR( 50 ) NOT NULL ;


ALTER TABLE `imfee` ADD `currencyOrigin` VARCHAR( 10 ) NOT NULL ,
ADD `currencyDest` VARCHAR( 10 ) NOT NULL ;
ALTER TABLE `manageFee` ADD `currencyOrigin` VARCHAR( 10 ) NOT NULL ,
ADD `currencyDest` VARCHAR( 10 ) NOT NULL ;

UPDATE login_activities SET description = "Transaction is Authorized." WHERE description LIKE "Transaction Authorized successfully.";

ALTER TABLE `transactions` ADD `distRefNumber` VARCHAR( 60 ) NOT NULL AFTER `refNumberIM` ;

ALTER TABLE `bank_account` ADD `settlementCurrency` VARCHAR( 20 ) NOT NULL AFTER `description` ,
ADD `settlementValue` VARCHAR( 20 ) NOT NULL AFTER `settlementCurrency` ;

ALTER TABLE `transactions` CHANGE `cashCharges` `cashCharges` FLOAT NOT NULL DEFAULT '0';

ALTER TABLE `transactions` ADD `settlementCurrency` VARCHAR( 20 ) NOT NULL AFTER `returnedBy` ,
ADD `settlementValue` VARCHAR( 20 ) NOT NULL ;


ALTER TABLE `transactions` ADD `senderBankBranch` VARCHAR( 50 ) NOT NULL;

ALTER TABLE `exchangerate` ADD `agentMarginType` VARCHAR( 15 ) NOT NULL AFTER `marginType` ,
ADD `agentMarginValue` FLOAT NOT NULL AFTER `agentMarginType` ;

