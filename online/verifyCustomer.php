<?php
    include_once "includes/configs.php";
    include_once "includes/database_connection.php";
    dbConnect();
    include_once "includes/functions.php";

    $querCust = "SELECT Mobile FROM " . TBL_CUSTOMER . " WHERE customerID=".$_GET['customerID'];
    $getCust = selectFrom($querCust);

    $mobile = $getCust["Mobile"];
    $OTP = genRandomOTP();
    update("UPDATE customer SET OTPNumber = $OTP WHERE customerID=".$_GET['customerID']);
    $message = "Your OTP is ".$OTP;
    if ($mobile !== '' && strlen($mobile) === 13 && substr($mobile, 0, 1) === '+')
        sendSMS($message, $mobile);
    else
        header("LOCATION:  index.php");
?>
<html>
    <head>
        <title>Private Client Verification - Payscanner Online Currency Transfers</title>
    </head>
    <body>
        <div>
            <form method="post" action="verifyCustomerConf.php?type=<?php echo $_GET["type"]; ?>&customerID=<?php echo $_GET["customerID"]; ?>">
                <input type="text" name="OTPNumber" placeholder="Enter OTP Number" autofocus />
                <input type="submit" value="Verify" />
                <a href="verifyCustomer.php?customerID=<?php echo $_GET["customerID"]; ?>">Resend OTP Number</a>
            </form>
        </div>
    </body>
</html>