<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Payment Details
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	//ini_set("display_errors","on");
	//error_reporting(E_ALL);
	
	$intTransDataFlag = false;
	if(!empty($_SESSION['exchangeRate']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['receivingCurrency']) && (!empty($_SESSION['sendingAmount']) && $_SESSION['sendingAmount'] > '0') && (!empty($_SESSION['receivingAmount']) && $_SESSION['receivingAmount'] > '0')){
		if(isset($_POST['iban'])){
			$patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
			$strIBAN = trim($_POST['iban']);
			/*if(!preg_match($patternIBAN, $strIBAN)){
				//debug('match', true);
				$_SESSION['benName'] = trim($_POST['benName']);
				$_SESSION['benCountry'] = $_POST['benCountry'];
				$_SESSION['benCurrency'] = trim($_POST['benCurrency']);
				$_SESSION['accountName'] = trim($_POST['accountName']);
				$_SESSION['accountNumber'] = trim($_POST['accountNumber']);
				$_SESSION['branchNameNumber'] = trim($_POST['branchNameNumber']);
				$_SESSION['branchAddress'] = trim($_POST['branchAddress']);
				$_SESSION['routingNumber'] = trim($_POST['routingNumber']);
				$_SESSION['sortCode'] = trim($_POST['sortCode']);
				$_SESSION['swift'] = trim($_POST['swift']);
				$_SESSION['refernce'] = trim($_POST['reference']);
				$_SESSION['existBen'] = $_POST['existBen'];
				$_SESSION['errorIBAN'] = "IBAN number is not valid";
				header("LOCATION: select_beneficiary_trans.php");
			}*/
		}
		 
		{
			//debug('false', true);
			/**************Geting Guzatted Holidays****************/
			$holidayQuery="select date from ".TBL_GUZATTED_HOLIDAY." order by date";
			$holidayResult=selectMultiRecords($holidayQuery);
			
			$allCount=count($holidayResult);
			
			$intTransDataFlag = true;
			$strBenName = trim($_POST['benName']);
			$strBenCountryID = $_POST['benCountry'];
			$strQueryCountry = "SELECT countryName FROM ".TBL_COUNTRIES." WHERE countryId = '$strBenCountryID'";
			$arrCountry = selectFrom($strQueryCountry);
			$strBenCountry = $arrCountry['countryName'];
			$strBenCurrency = trim($_POST['benCurrency']);
			if(isset($_POST['accountName']))
				$strBenAccountName = trim($_POST['accountName']);
			else
				$strBenAccountName = '';
			if(isset($_POST['accountNumber']))
				$strBenAccountNumber = trim($_POST['accountNumber']);
			else
				$strBenAccountNumber = '';
			if(isset($_POST['branchNameNumber']))
				$strBranchNameNumber = trim($_POST['branchNameNumber']);
			else
				$strBranchNameNumber = '';
				
			if(isset($_POST['branchAddress']))
				$strBranchAddress = trim($_POST['branchAddress']);
			else
				$strBranchAddress = '';
			
			if(isset($_POST['sortCode']))
				$strSortCode = trim($_POST['sortCode']);
			else
				$strSortCode = '';
			
			if(isset($_POST['routingNumber']))
				$strRoutingNumber = trim($_POST['routingNumber']);
			else
				$strRoutingNumber = '';
				
			if(isset($_POST['swift']))
				$strSwiftCode = trim($_POST['swift']);
			else
				$strSwiftCode = '';
		
			$strReference = trim($_POST['reference']);
			$intBenID = $_POST['existBen'];
			$intCustomerID = $_SESSION['loggedInUser']['userID'];
			$intExchangeRate = $_SESSION['exchangeRate'];
			$strSendCurrency=$_POST['sendingCurrency'];
			$strSendingCurrency = $_SESSION['sendingCurrency'];
			$strReceivingCurrency = $_SESSION['receivingCurrency'];
			$strSendingAmount = $_SESSION['sendingAmount'];
			$strReceivingAmount = $_SESSION['receivingAmount'];
			$_SESSION["benBame"];
            $_SESSION["firstBen"];
            $_SESSION["secondBen"];
			$strCurrentTimeStamp = date("Y-m-d h:i:s");
			$valueDate=date('Y-m-d H:i:s', strtotime( $strCurrentTimeStamp. ' + 2 day'));
			$valueDateWithoutTime=date('Y-m-d', strtotime( $valueDate));
			$dateflag=true;
			while($dateflag!=false){
				$dateflag=false;
				for($i=0;$i<$allCount;$i++){
					if($valueDateWithoutTime==$holidayResult[$i]['date']){
						$valueDate=date('Y-m-d H:i:s', strtotime( $valueDate. ' + 1 day'));
						$valueDateWithoutTime=date('Y-m-d', strtotime( $valueDate));
						$dateflag=true;
						
					
					}
				
				}
				
				
			$valueDateDay=date('l', strtotime( $valueDate));
			if($valueDateDay== "Saturday")
			{
				$valueDate=date('Y-m-d H:i:s', strtotime( $valueDate. ' + 2 day'));
				$dateflag=true;
			
			}else if( $valueDateDay=="Sunday"){
				$valueDate=date('Y-m-d H:i:s', strtotime( $valueDate. ' + 1 day'));
				$dateflag=true;
			
			}
			}
			
			$strCurrentDate = date("Y-m-d");
			$strIP = $_SERVER['REMOTE_ADDR'];
			$strMsg = '';
			$strError = '';
			
			if(!empty($_POST['existBen'])){
				$strQueryUpdateBen = "UPDATE ".TBL_BENEFICIARY." SET ";
				if(!empty($strBenName)){
					$strQueryUpdateBen .="`firstName` = '$strBenName',"; 
					} 
				if(!empty($strBenName)){
					$strQueryUpdateBen .="`beneficiaryName` = '$strBenName',"; 
					} 
				if(!empty($strBenCountry)){
					$strQueryUpdateBen .="`Country` = '$strBenCountry',"; 
					} 
					
				if(!empty($strReference)){
					$strQueryUpdateBen .="`reference` = '$strReference',"; 
					} 
					$strQueryUpdateBen = substr($strQueryUpdateBen,0,strlen($strQueryUpdateBen )-1);
				
					$strQueryUpdateBen .=" WHERE benID = '$intBenID' AND customerID = '$intCustomerID'";
				if(update($strQueryUpdateBen))
					$strMsg = "Beneficiary updated successfully";
				else
					$strError = "Beneficiary cannot be updated. please try again.";
				
			}else{
				$strQueryInsertBen = "INSERT INTO ".TBL_BENEFICIARY." (`firstName`, `middleName`, `lastName`,`created`, `beneficiaryName`, `Country`, `reference`, `customerID`) VALUES('$strBenName', '', '','$strCurrentDate', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID')";
			
				if(insertInto($strQueryInsertBen)){
					$strMsg = "Beneficiary added successfully";
					$intBenID = mysql_insert_id();
				}else{
					$strError = "Beneficiary cannot be added successfully";
				}
			}
			if(!empty($intBenID)){
				$strQueryChkBankInfo = "SELECT count(id) AS record FROM ".TBL_BEN_BANK_DETAILS." WHERE benID = '$intBenID'";
				$arrBenBankInfo = selectFrom($strQueryChkBankInfo);
				if($arrBenBankInfo['record'] == 0){
					
					$strQueryInsertBenBank = "INSERT INTO ".TBL_BEN_BANK_DETAILS." (`benId`, `IBAN`, `swiftCode`, `bankName`, `accountNo`, `branchCode`, `branchAddress`, `sortCode`, `routingNumber`,sendCurrency) VALUES('$intBenID', '$strIBAN', '$strSwiftCode', '$strBenAccountName', '$strBenAccountNumber', '$strBranchNameNumber', '$strBranchAddress', '$strSortCode', '$strRoutingNumber','$strSendCurrency')";
					if(insertInto($strQueryInsertBenBank))
						$strMsg .= " and beneficiary bank details are added against the beneficiary";
					else
						$strError .= " and beneficiary bank info cannot added.";
				}else{
					$strQueryUpdateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET";
					if(!empty($strIBAN)){ 
					$strQueryUpdateBenBank .="`IBAN` = '$strIBAN',"; 
					} 
					if(!empty($strSwiftCode)){ 
					$strQueryUpdateBenBank .="`swiftCode` = '$strSwiftCode',"; 
					} 
					 if(!empty($strBenAccountName)){ 
					$strQueryUpdateBenBank .="`bankName` = '$strBenAccountName',"; 
					} 
					if(!empty($strBenAccountNumber)){
					$strQueryUpdateBenBank .="`accountNo` = '$strBenAccountNumber',"; 
					} 
					if(!empty($strBranchNameNumber)){ 
					$strQueryUpdateBenBank .="`branchCode` = '$strBranchNameNumber',"; 
					} 
					 if(!empty($strBranchAddress)){
					$strQueryUpdateBenBank .="`branchAddress` = '$strBranchAddress',"; 
					} 
					 if(!empty($strSortCode)){
					$strQueryUpdateBenBank .="`sortCode` = '$strSortCode',"; 
					} 
					 if(!empty($strRoutingNumber)){ 
					$strQueryUpdateBenBank .="`routingNumber` = '$strRoutingNumber',"; 
					} 
					if(!empty($strSendCurrency)){
					$strQueryUpdateBenBank .="`sendCurrency` = '$strSendCurrency',"; 
					} 
					$strQueryUpdateBenBank = substr($strQueryUpdateBenBank,0,strlen($strQueryUpdateBenBank )-1);
					$strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";
					if(update($strQueryUpdateBenBank))
						$strMsg .= " and beneficiary bank details are updated";
					else
						$strError .= " and beneficiary bank info cannot be updated";
				}
				$getCustAgent=selectFrom("select agentID from ".TBL_CUSTOMER." where customerID='".$intCustomerID."'");
				$strRefNumber = generateRefNumber();
				$strQueryTrans = "INSERT INTO ".TBL_TRANSACTIONS." (`transType`, `customerID`, `benID`, `custAgentID`, `benAgentID`, `refNumberIM`, `transAmount`, `exchangeRate`, `localAmount`, `totalAmount`, `transStatus`, `transDate`, `valueDate`,`validValue`, `currencyFrom`, `currencyTo`, `transDetails`, `agentExchangeRate`, `trans_source`, `transIP`) VALUES('Bank Transfer', '$intCustomerID', '$intBenID', '".$getCustAgent['agentID']."', '', '$strRefNumber', '$strSendingAmount', '$intExchangeRate', '$strReceivingAmount', '$strSendingAmount', 'Pending', '$strCurrentTimeStamp', '$valueDate','N', '$strSendingCurrency', '$strReceivingCurrency', 'Y', 'N', 'O', '$strIP')";
				if(insertInto($strQueryTrans)){
					$intTransID = mysql_insert_id();
					$strQueryBankDetails = "INSERT INTO ".TBL_BANK_DETAILS." (`benID`, `transID`, `bankName`, `accNo`, `IBAN`, `swiftCode`, `branchCode`, `branchAddress`, `routingNumber`, `sortCode`) VALUES('$intBenID', '$intTransID', '$strBenAccountName', '$strBenAccountNumber', '$strIBAN', '$strSwiftCode', '$strBranchNameNumber', '$strBranchAddress', '$strRoutingNumber', '$strSortCode')";
					insertInto($strQueryBankDetails);
					update("UPDATE ".TBL_CUSTOMER." SET no_of_transactions= no_of_transactions+1 WHERE customerID=$intCustomerID ");
					$strMsg = "Transaction Created Successfully. Reference Number: $strRefNumber";
					include_once 'sendmail-premier-online-pdf.php';
					
					unset($_SESSION['exchangeRate']);
					unset($_SESSION['sendingCurrency']);
					unset($_SESSION['receivingCurrency']);
					unset($_SESSION['sendingAmount']);
					unset($_SESSION['receivingAmount']);
					
				}
			}
		}
	}else{
		header("LOCATION: make_payment-i.php");
	}
	
	$queryBankDetails = "select * from accounts where currency = '".$strSendingCurrency."' ";
	$arrBankRecord = selectFrom($queryBankDetails);
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Beneficiary Details Page</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<style type="text/css">
		#content{
				position:relative;
			}
			.cont-details
			{
			
				padding-left:236px;
			}
			.create_button
			{
				position:absolute;
				left:67px;
				top:338px;
				
				
				z-index:800;
			
			}
			#imgContainer{
				position:absolute;
				left:60px;
				top:110px;
				/*right:682px;
				top:117px;*/
				
				
				z-index:800;
			}
			</style>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				require_once "includes/header.php";
				if(!empty($strMsg) || !empty($strError)){
			?>
			<table class="table">
				<?php 
					//debug($strMsg);
					if(!empty($strMsg)){
				?>
				<tr class="success">
					<td class="align_center"><?php echo $strMsg; ?></td>
				</tr>
				<?php 
					}
					if(!empty($strError)){
				?>
				<tr class="error">
					<td class="align_center"><?php echo $strError; ?></td>
				</tr>
				<?php
					}
				?>
			</table>
			<?php 
				}
			?>
			<div id="container" >
				
				<div id="content" class="left">
                <div id="imgContainer"><img src="images/amounts_big.png" alt="some_text"> </div>
                <h1 id="cont_head" class="cont_head">Payment Details</h1>
						<h2>
							
							Please now instruct your bank to transfer the funds to the details below.
						</h2>
						<br />
						<div class="cont-details">
						<div class = "create_button">
						<div class="row align_left">
							<input name="proceed" id="proceed" type="submit" value="Create New Transaction" class="btn actv_btn" onClick="window.location.href = 'make_payment-i.php';"/>
						</div>
						</div>
						<div class="border small">
						<?php /***Commet Beneficary Account**/?>
							<h2>Bank Name: <?  echo $arrBankRecord["bankName"]; ?></h2>
							<h2>Account Name: <?  echo $arrBankRecord["accountName"]; ?></h2>
							<h2>Account Number: <? echo $arrBankRecord["accounNumber"]; ?></h2>
							<h2>Sort Code: <? echo $arrBankRecord["sortCode"]; ?></h2>
                            <h2>IBAN: <? echo $arrBankRecord["IBAN"]; ?></h2>
                            <h2>SWIFT Code: <? echo $arrBankRecord["swiftCode"]; ?></h2>
							<br />
							<p style="font-weight:bold; color:#000000;">Once your funds have been received your currency will be transferred to the beneficiary selected.
							</p>
							<br />
							<p style="color:
							#000000; font-weight:lighter">
								This deal has now been booked into the market and no further amendments can be made.
							</p>
                            <br />
							<p style="color: #D64434;">
								You must now instruct your bank to send the funds to us, using the details above. We do not take the funds from your account, you must send them to us.
							</p>
						</div>
						</div>
						
				</div>
				<?php 
					if($intTransDataFlag){
				?>
				<!--<div id="sidebar">
					<div class="step_head">
						<h2>STEP 1 TRANSACTION DETAILS</h2>
					</div>
					<p class="inner_step">Buying <?php echo number_format($strReceivingAmount, 2, ".", ",")." ".$strReceivingCurrency; ?></p>
					<p class="inner_step">&#64; <?php echo $intExchangeRate; ?></p>
					<p class="inner_step">Selling <?php echo number_format($strSendingAmount, 2, ".", ",")." ".$strSendingCurrency; ?></p>
					<div class="step_head">
						<h2>STEP 2 BENEFICIARY DETAILS</h2>
					</div>
                    
                    
                    <p class="inner_step"><? echo $_SESSION["benBame"]; ?></p>
                    <p class="inner_step"><? echo $_SESSION["firstBen"]; ?></p>
                    <p class="inner_step"><? echo $_SESSION["secondBen"]; ?></p>
                    
					<div class="step_head">
						<h2>STEP 3 PAYMENT DETAILS</h2>
					</div>
					<p class="inner_step"><? echo $arrBankRecord["bankName"]; ?>&nbsp;</p>
					<p class="inner_step"><? echo $arrBankRecord["accountName"]; ?>&nbsp;</p>
					<p class="inner_step"><? echo $arrBankRecord["accounNumber"]; ?>&nbsp;</p>
					<p class="inner_step"><? echo $arrBankRecord["sortCode"]; ?>&nbsp;</p>
				</div>-->
			</div>
			<?php 
				}
			?>
		</div>
	</body>
</html>