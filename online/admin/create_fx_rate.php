<?php
require '../../src/bootstrap.php';
//error_reporting(E_ALL);
//ini_set("display_errors","on");
	/**
	 * @package: Online Module	
	 * @subpackage: Create FX Rates
	 * @author: Awais Umer
	 */
	session_start();
	/*if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php?notLogin=1");
	}*/
	include_once "../includes/configs.php";
	include_once "../includes/database_connection.php";
	
	dbConnect();
	include_once "../includes/functions.php";
	//include ("../../include/config.php");
	//include ("../../admin/security.php");
	$username=loggedUser();
	if ($username=="")
	{
		header("location: ../../admin/index.php");
		exit();
	
	}

//$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];

$agentRepository = new \Payex\Repository\AgentRepository($app->getDb());
$currencyRepository = new \Payex\Repository\CurrencyRepository($app->getDb());
$agentHelper = new \Payex\Helper\AgentHelper();
$agent = $agentRepository->getById($userID);

//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent'){
    $where_sql="   AND userID='".	$userID."' ";
}

foreach($_REQUEST as $key => $val)
    $$key = $val;
		
$currentDateTime = date("Y-m-d H:i:s");
if ($save == "Save"){
    if($baseCurrency && $quotedCurrency && $fromAmount  && $marginType && $margin && $statusType){
			$strSelQuery = "select * from `fx_rates_margin`
							WHERE
								baseCurrency='".$baseCurrency."'
								AND
								quotedCurrency='".$quotedCurrency."'
								AND
								fromAmount='".$fromAmount."'
								AND
								toAmount='".$toAmount."'
								AND
								marginType = '".$marginType."'
								
								AND
								agentID = '".$agentName."'
							";
			$arrMarginRS = selectFrom($strSelQuery);
			
			if(!empty($arrMarginRS["baseCurrency"]))
			{
				$strFailure = "FX Rates already exists.";
			}
			else
			{
				$strInsertQuery = "INSERT INTO `fx_rates_margin` (`baseCurrency`, `quotedCurrency`, `fromAmount`, `toAmount`, `marginType`, `margin`, `addedOn`,`statusType`,`limitCategory`,`rateFor`,`agentID`) VALUES('".$baseCurrency."', '".$quotedCurrency."', '".$fromAmount."', '".$toAmount."', '".$marginType."','".$margin."', '".$currentDateTime."','".$statusType."','$amountLimit','$rateFor','$agentName')";
				
				$flagInsert = insertInto($strInsertQuery);
				if($flagInsert)
				{
					$strSuccess = "FX Rates created successfully";
				
				}
				else
				{
					$strFailure = "FX Rates could not be created";
				
				}
			}
		}
		else
		{
			$strFailure = "FX Rates could not be created";
		
		}
	}
	elseif ($save == "Update")
	{
		$strUpdateQuery = "update fx_rates_margin set rateFor ='$rateFor',agentID = '".$agentName."',baseCurrency = '".$baseCurrency."',quotedCurrency = '".$quotedCurrency."', fromAmount = '".$fromAmount."',toAmount = '".$toAmount."',marginType = '".$marginType."',margin = '".$margin."',addedOn = '".$currentDateTime."',statusType = '".$statusType."',limitCategory = '$amountLimit'  where id = '".$id."' ";
		$flagUpdate = update($strUpdateQuery);
		if($flagUpdate)
		{
			$strSuccess = "FX Rates updated successfully";
		
		}
		else
		{
			$strFailure = "FX Rate could not be updated";
		}
		
	}
		if(!empty($id))
		{
			$queryEditRecords = "select * from fx_rates_margin where id = '".$id."'";
			$arrEditRecords = selectFrom($queryEditRecords);
			//debug($queryEditRecords);
		}
//	debug($_REQUEST);
?>

<html>
	<head>
		<title> Add Exchange Rate Margin</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="css/style_admin.css"/>
		<script type="text/javascript" src="scripts/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="../scripts/jquery.validate.js"></script>
		
		<script src="scripts/bootstrap.js"></script>
		<style type="text/css">
			.error{
				color:red;
			
			}
		</style>
	</head>
	<body>
		<div class="container">
			<?php 
				require_once "includes/header.php";
			?>
			<h3 class="well-small align_center well"> Add Exchange Rate Margin</h3>
			<div class="form_cont">	
				<form name = "addFxRates" id = "addFxRates" action="" method="post">
					<table class="table table-striped">
					
					<tr>
						<th>Agent For</th>
				 
<td colspan="7">
<?php $agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active'   $where_sql  ";


 
 
 ?>
 <?php  if($_SESSION["loggedUserData"]['adminType']!='Agent')
{
?>				<select  name="agentName" id="agentName" >
					<option value="">Select One</option>
 	          		<?php 
					
							$agentQuery .= "order by agentCompany";
								 $agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
 	    		        		?>
 	            				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            				<?
 	                			}
								?>
 	          			</select>
						
						<?php } else {?>
						<input  type="hidden" name="agentName" id="agentName" value="<?=$userID?>"><?=$_SESSION["loggedUserData"]['name']?>
						<?php }?>

</td>
                    </tr>
                        <tr>
                            <th>Rate for</th>
                            <td colspan="7">
                                <select name="rateFor" id="rateFor">
									<option value="">- Select -</option>
									<option value="T" <?= $arrEditRecords['rateFor'] == 'T' ? 'selected': ''?> >Transactions</option>
									<option value="P" <?= $arrEditRecords['rateFor'] == 'P' ? 'selected': ''?>>Prepaid Card Transactions</option>
                                </select>
                            </td>
                        </tr>
						<tr>
                            <th>Base Currency:</th>
                            <td colspan="7">
                                <select name="baseCurrency" id="baseCurrency">
                                    <option value="">- Select base currency -</option>
                                    <?php
                                    if ($agent && $agentHelper->isAgent($agent)){
                                        $countryCondition = [0 => $agent->getCustCountries()];
                                        $baseCountryList = $agentHelper->getAgentRelatedCountries($countryCondition);
                                        $baseCurrenciesCollection = $currencyRepository->getByCountryList($baseCountryList);
                                        /* @var $item \Payex\Model\Currency */
                                        foreach ($baseCurrenciesCollection as $item){
                                            echo '<option value="' . $item->getCurrencyName() . '">' . $item->getCurrencyName() . '</option>';
                                        }
                                    }else{
                                    ?>
                                        <option value="EUR">EUR</option>
                                        <option value="GBP">GBP</option>
                                        <option value="USD">USD</option>
                                   <?php } ?>
                                </select>
							</td>
                        </tr>
						<?php
                        if($status == 'edit'){
                            $arrEditRecords["baseCurrency"];
                        }
						?>
						<tr>
							<th>Quoted Currency:</th>
							<td colspan="7">
								<select name="quotedCurrency" id="quotedCurrency">
									<option value="">- Select quoted currency -</option>
									<?php
                                    if ($agent && $agentHelper->isAgent($agent)){
                                        $countryCondition = [0 => $agent->getIDACountry()];
                                        $quotedCountryList = $agentHelper->getAgentRelatedCountries($countryCondition);
                                        $quotedCurrenciesCollection = $currencyRepository->getByCountryList($quotedCountryList);
                                        /* @var $item \Payex\Model\Currency */
                                        foreach ($quotedCurrenciesCollection as $item){
                                            echo '<option value="' . $item->getCurrencyName() . '">' . $item->getCurrencyName() . '</option>';
                                        }
                                    }else{
                                        $query = "select distinct(currencyName) from currencies group by currencyName ASC";
                                        $arrCurrencies = selectMultiRecords($query);
                                        for($i=0;$i<count($arrCurrencies);$i++){
                                            echo '<option value="' . $arrCurrencies[$i]["currencyName"] . '">' . $arrCurrencies[$i]["currencyName"]. '</option>';
                                        }
                                    }
                                    ?>
								</select>
							</td>
						</tr>
						<!--<tr>
							<th>Amount Range:</th>
							<td>
								<input name="amountRate" id="amountRate" value="any" type="radio" class="radio"/>
								Any
							</td>
							<td >
								<input name="amountRate" id="amountRate" value="Range" type="radio" class="radio" />
								Range
							</td>
							<td colspan="3">&nbsp;</td>
						</tr>-->
						
						
						<tr>
							<th>From Amount:</th>
							<td colspan="4">
								<input name="fromAmount" id="fromAmount" type="text" class="input-large" />
							</td>
						</tr>
						<tr>
							
							<th>To Amount:</th>
							<td colspan="4">
							<?php 
							$anyAmountSelect = '';
							$specificAmountSelect = '';
							
							if($arrEditRecords["limitCategory"] == 'anyAmount'){
							$anyAmountSelect = 'checked';
							}else{
							$specificAmountSelect = 'checked';
							}?>
							
			<input name="amountLimit" value="anyAmount" type="radio" <?=$anyAmountSelect?> /><font style=" font-size: 12px !important; font-colr:#333; ">Any</font>
			
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			<input name="amountLimit" value="specificAmount" type="radio" <?=$specificAmountSelect?> /> <font style=" font-size: 12px !important;">Specific Amount</font>

			<br />	<br />	 

			<input name="toAmount" id="toAmount" type="text" placeholder="Enter Amount" class="input-large"/>
							</td>
						</tr>
						<tr>
							<th>Margin Type:</th>
							<td colspan="4">
								<select name="marginType" id="marginType">
									<option value = "">-Select Margin Type-</option>
									<option value = "PIPS">PIPS</option>
									<option value = "FIXED">FIXED</option>
									<option value = "PERCENTAGE">PERCENTAGE</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<th>Margin:</th>
							<td colspan="4">
								<input name="margin" id="margin" type="text" class="input-small"/>
							</td>
						</tr>
						<tr>
							<th>Status:</th>
							<td colspan="4">
								<select name="statusType" id="statusType">
								   <option value = "">-Select Status-</option>
								   <option value = "Enable">Enable</option>
								   <option value = "Disable">Disable</option>
								</select>   
							</td>
						</tr>
			
						<tr>
							<th colspan="5" class="control-group error align_center">
								<label><? echo $strFailure; ?></label>
							</th>
						</tr>
						<tr>
							<th colspan="5" class="control-group success align_center">
								<label><? echo $strSuccess; ?></label>
							</th>
						</tr>
						<tr>
							<td colspan="5" class="align_center">
							<?php if($status == 'edit') { ?>
								<input name="save" id="save" type="submit" class="btn-primary btn" value="Update"/>
								<? } else { ?>
								
								<input name="save" id="save" type="submit" class="btn-primary btn" value="Save"/>
								<input name="clear" id="clear" type="reset" class="btn offset1 btn-danger" value="Clear"/>
								<? } ?>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<script type="text/javascript">
		$('#addFxRates').validate({
				rules: {
					
					baseCurrency: {
						required: true
					},agentName: {
						required: true
					},
					quotedCurrency: {
						required: true
					},
					fromAmount: {
						required: true
					},
					toAmount: {
						required: true
					},
					marginType: {
						required: true
					},
					margin: {
						required: true
					},
					statusType: {
					    required: true 
					}
					
				}
			});
			
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			<?php if($status == 'edit') { ?>
			SelectOption(document.forms[0].baseCurrency, "<?php echo $arrEditRecords["baseCurrency"]; ?>");
			SelectOption(document.forms[0].quotedCurrency, "<?php echo $arrEditRecords["quotedCurrency"]; ?>");
			document.getElementById('fromAmount').value = "<?php echo $arrEditRecords["fromAmount"]; ?>";
			document.getElementById('toAmount').value = "<?php echo $arrEditRecords["toAmount"]; ?>";
			SelectOption(document.forms[0].marginType, "<?php echo $arrEditRecords["marginType"]; ?>");
			document.getElementById('margin').value = "<?php echo $arrEditRecords["margin"]; ?>";
			SelectOption(document.forms[0].statusType, "<?php echo $arrEditRecords["statusType"]; ?>");
			<? } ?>
			
				
			$(function(){
				$('#toAmount').css('display','none');
			
			checkAmountLimit();
			$('input[name=amountLimit]').click(function(){
				checkAmountLimit();
				
			});
			
			function checkAmountLimit()
			{
			
				checkVal = $('input[name=amountLimit]:checked').val();
			 
				if(checkVal == 'anyAmount'){
				$('#toAmount').rules('remove','required');
				$('#toAmount').css('display','none');
				}else if(checkVal == 'specificAmount'){
				$('#toAmount').css('display','block');
				$('#toAmount').rules('add','required');
				}
			
			};
			});
			</script>
	</body>
</html>