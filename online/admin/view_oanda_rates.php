<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: View XE.com Rates
	 * @author: Awais Umer
	 */
	session_start();
	/*if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php?notLogin=1");
	}*/
	include_once "../includes/configs.php";
	include_once "../includes/database_connection.php";
	dbConnect();
	include_once "../includes/functions.php";
	$username=loggedUser();
	if ($username=="")
	{
		header("location: ../../admin/index.php");
		exit();
	
	}
	$maxDate =  selectFrom("select max(createdDate) as MaxDate from online_exchange_rates");
	$maxDate = $maxDate["MaxDate"];
	$query = "select distinct baseCurrency,quotedCurrency,exchangeRate,inverseRate,createdDate from online_exchange_rates where createdDate = '".$maxDate."' group by baseCurrency,quotedCurrency ";
	$contentQuery = selectMultiRecords($query);
	$dateTime = explode(" ",$maxDate);
	$finalDate = $dateTime[0];
	$finalTime = $dateTime[1];
	$finalDate1 = explode("-",$finalDate);
	$finalDate = $finalDate1[2]."-".$finalDate1[1]."-".$finalDate1[0];
?>
	



<html>
	<head>
		<title>View XE.com Rates</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="css/style_admin.css"/>
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/bootstrap.js"></script>
	</head>
	<body>
		<div class="container">
			<?php 
				require_once "includes/header.php";
			?>
			<h3 class="well-small align_center well">View XE.com Rates</h3>
			<strong>Date/Time: </strong>
			<small><? echo $finalDate."/".$finalTime; ?></small>
			<div class = "xRatesTmr">
            
            <div class="xRatesTmrBx">
            
       	  <div class="xRatesTmrBxRt">
                	<div class="xRctrSec">
            			<span class = "timer">0 :&nbsp;<span id='countdown'>60</span></span>
                </div>
                
                </div>
            </div>
            </div>
			<table class="table table-striped table-bordered cust_size2">
				<tr>
					<th>Currency Pair</th>
					<th>Rate</th>
					<th>Rate Inverse</th>
					<th>Operation</th>
				</tr>
				<?php 
				for($i=0;$i<count($contentQuery);$i++)
				{
				?>
				<tr>
					<td><?php echo $contentQuery[$i]["baseCurrency"]?>/<? echo $contentQuery[$i]["quotedCurrency"]?></td>
					<td><?php echo $contentQuery[$i]["exchangeRate"] ?></td>
					<td><?php echo $contentQuery[$i]["inverseRate"] ?></td>
					<td><a href="create_fx_rate.php">Add Margin</a></td>
				</tr>
				<?
				}
				?>
			</table>
		</div>
		<script>
		    $(function() {
        var cd = $('#countdown');
        var c = parseInt(cd.text(),10);
        var interv = setInterval(function() {
            c--;
            cd.html(c);
            if (c == 0) {
                window.location.reload(false);
				//window.location.href = 'make_payment-ii.php';
				 //$('#changeTrans').trigger('click');
				 //window.location.href = window.location.protocol +'//'+ window.location.host + window.location.pathname;
				 location.reload(true);
                clearInterval(interv);
            }
        }, 1000);
    });
		</script>
	</body>
</html>