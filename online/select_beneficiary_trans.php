<?php  
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	include_once "includes/functions.php";
	$benCountries="select countryId,countryName,isocode,ibanlength from countries";
		//debug($benCountries);
		$benCountriesResult=selectMultiRecords($benCountries);
	//debug($_SESSION);
	$intValid = false;
	
	if(!empty($_POST['receivingAmount']) && $_POST['receivingAmount'] > '0' && !empty($_POST['sendingAmount']) && $_POST['sendingAmount'] > '0' && !empty($_POST['receivingCurrency']) && !empty($_POST['sendingCurrency']) && !empty($_POST['exchangeRate'])){
		$intValid = true;
		$_SESSION['receivingAmount'] = $_POST['receivingAmount'];
		$_SESSION['sendingAmount'] = $_POST['sendingAmount'];
		$_SESSION['receivingCurrency'] = $_POST['receivingCurrency'];
		$_SESSION['sendingCurrency'] = $_POST['sendingCurrency'];
		$_SESSION['exchangeRate'] = $_POST['exchangeRate'];
		
		$strQueryInsertAccept_Quote = "INSERT INTO acceptQuoteHistory (custID,buy_currency,sell_currency,buy_amount,sell_amount,rate) VALUES('".$_SESSION['loggedInUser']['userID']."','".$_POST['sendingCurrency']."','".$_POST['receivingCurrency']."','".$_POST['sendingAmount']."','".$_POST['receivingAmount']."','".$_POST['exchangeRate']."')";
		insertInto($strQueryInsertAccept_Quote);
		
	}
	if(!empty($_SESSION['receivingAmount']) && !empty($_SESSION['sendingAmount']) && !empty($_SESSION['receivingCurrency']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['exchangeRate'])){
		$intValid = true;
		$strReceivingAmount = $_SESSION['receivingAmount'];
		$strSendingAmount = $_SESSION['sendingAmount'];
		$strReceivingCurrency = $_SESSION['receivingCurrency'];
		$strSendingCurrency = $_SESSION['sendingCurrency'];
		$strExchangeRate = $_SESSION['exchangeRate'];
	}
	if(!empty($_SESSION['errorIBAN'])){
		$intValid = true;
		$intError = true;
		$strBenName = $_SESSION['benName'];
		$strBenCountry = $_SESSION['benCountry'];
		$strBenCurrency = $_SESSION['benCurrency'];
		$strSwift = $_SESSION['swift'];
		$strReference = $_SESSION['refernce'];
		$strExistBen = $_SESSION['existBen'];
		$strErrorIBAN = $_SESSION['errorIBAN'];
		
		unset($_SESSION['benCountry']);
		unset($_SESSION['benCurrency']);
		unset($_SESSION['swift']);
		unset($_SESSION['refernce']);
		unset($_SESSION['existBen']);
		unset($_SESSION['errorIBAN']);
	}
	
	if(!$intValid)
		header("LOCATION: make_payment-i.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Beneficiary Details Page</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
		<script type='text/javascript'></script>
		<style type="text/css">
#container {
	width:970px;
}
#content {
	position:relative;
	overflow:visible;
}
#imgContainer {
	position:absolute;
	right:-123px;
	top:-50px;
	z-index:800;
}
</style>
		</head>
		<body>
<!--<div id="overlay">
			<div id="errorRule">
				
			</div>
		</div>-->
<div id="wrapper" >
          <?php 
				require_once "includes/header.php";
			?>
          <div id="container" >
    <h1 id="selben_cont_head1" class="cont_head">Beneficiary Details</h1>
    <div id="content" class="left">
              <div id="imgContainer"><img src="images/dollar_small.png" alt="some_text"> </div>
              <div id="errorRule"></div>
              <h2 id="selben_slcben">Choose an existing beneficiary or enter new beneficiary</h2>
              <form id="benForTransaction" action="confirm-transaction.php" method="post" onSubmit="return checkAgree();">
        <div class="inner_cont">
                  <div class="row">
            <div class="pull-left">
                      <label id="selben_extben" class="">Existing Beneficiary <em>*</em>: </label>
                    </div>
            <div class="pull-left">
                      <select name="existBen" id="existBen" class="input-xlarge" onChange="this.form.action = '<?php echo $_SERVER['PHP_SELF']; ?>'; this.form.submit();">
                <option value="">- Select beneficiary for transaction -</option>
                <?php 
											//$strQueryBen = "SELECT benID, beneficiaryName AS benName FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
											$strQueryBen = "SELECT benID, firstName AS benName FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
											$arrBens = selectMultiRecords($strQueryBen);
											
												
											
											for($x = 0;$x < count($arrBens); $x++)
											{
											$strQueryBen1 = "SELECT bankName FROM  ben_banks WHERE benId = '".$arrBens[$x]['benID']."'";
											$arrBens1 = selectFrom($strQueryBen1);
											
											
												echo "<option value='".$arrBens[$x]['benID']."'>".$arrBens1['bankName']."</option>";
												}
										?>
              </select>
                    </div>
            <a href="javascript:void(0);" id="benTip" class="tip" data-placement="above" rel="popover" data-content="Select existing beneficiary from the drop down" title="Existing Beneficiary"> [?] </a> </div>
                  <?php 
								if($intError){
							?>
                  <table class="table">
            <tr class="error">
                      <td class="error align_center"><?php echo $strErrorIBAN; ?></td>
                    </tr>
          </table>
                  <?php 
								}
							?>
                </div>
        <?php 
							$intBenExist = false;
							if(!empty($_POST['existBen']) || isset($_SESSION['ben'])){
								if(!empty($_POST['existBen']))
									$intBenID = trim($_POST['existBen']);
								elseif(!empty($_SESSION['ben']))
									$intBenID = $_SESSION['ben'];
								$intBenExist = true;
								
								$strQueryBenInfo = "SELECT countries.countryId, Country, firstName AS benName, benBankDetails.IBAN AS IBAN, benBankDetails.swiftCode AS swiftCode,benBankDetails.sortCode AS sortCode,benBankDetails.bankName AS accountName,benBankDetails.accountNo AS accountNumber,benBankDetails.routingNumber AS routingNumber,reference,benBankDetails.sendCurrency as sendCurrency  FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS benBankDetails ON ben.benID = benBankDetails.benID left join countries on ben.Country=countries.countryName WHERE ben.benID = '$intBenID'";
								//debug($strQueryBenInfo);
								$arrBenInfo = selectFrom($strQueryBenInfo);
								
								$strSendCurrency=$arrBenInfo['sendCurrency'];
								//debug($arrBenInfo);
							}
						?>
        <div id="benSection">
                  <?php 
								if(!$intBenExist){
							?>
                  <h1 id="selben_cont_head" class="cont_head">Add New Beneficiary</h1>
                  <?php 
								}
							?>
                  <h2 id="selben_info">Beneficiary Information</h2>
                  <div class="inner_highlight">
            <table class="table">
                      <tr>
                <td><label id="selben_benC" >Beneficiary Country: </label></td>
                <td class="input-medium"><span style="position:relative">
                  <div style="position:absolute; top:-30px; left:3px; width:200px; font-size:13px; color:#666666; font-weight:bold;display:inline-block;text-align:center;	background-color:#E6A028;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;margin-right:5px;" id="countrymsgnote" class="input-medium"></div>
                  </span>
                          <select name="benCountry" id="benCountry" class="input-xlarge">
                    <option value="">- Select country for beneficiary -</option>
                    <?php 
						$strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
													$arrCountry = selectMultiRecords($strQueryCountries);       			//debug($strQueryCountries);
										//debug($arrCountry);
													for($x = 0;$x < count($arrCountry);$x++){
														echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']."</option>";
													}
												?>
                  </select></td>
                <td><a href="javascript:void(0);" id="countryTip" class="tip" data-placement="above" rel="popover" data-content="To change beneficiary country, please click the drop down option." title="Beneficiary Country"> [?] </a></td>
              </tr>
                      <tr id="currency">
                <td><label>Currency You Wish To Send:</label></td>
                <td><select name="sendingCurrency" id="sendingCurrency" type="text" class="input-medium">
                    <option value="">- Select Currency -</option>
                    <?php 
												//$strQueryCurrency = "SELECT currencyName, baseCurrency FROM ".TBL_CURRENCY." INNER JOIN ".TBL_ONLINE_EXCHANGE_RATES." ON cID = quotedCurrency GROUP BY currencyName ORDER BY currencyName ASC";
												//$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_FX_RATES_MARGIN." ORDER BY quotedCurrency ASC";
												$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";			
//debug($strQueryCurrency);
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
                  </select></td>
                <td><a href="#" class="tip" id="currencyTip" data-placement="above" rel="popover" data-content="Select Currency You Want To Send" title="Select Currency"> [?] </a></td>
              </tr>
                      <!--<tr>
										<td>
											<label id="selben_nameop">Name of Beneficiary <em>*</em>:</label>
										</td>
										<td>
											<input name="benName" id="benName" type="text" class="input-medium" />
										</td>
										<td>
											<a href="#"  class="tip" id="benNameTip" data-placement="above" rel="popover" data-content="Enter the beneficiary complete name" title="Beneficiary Name">
												[?]
											</a>
										</td>
									</tr>
									<tr>
										<td>
											<label id="bencur">Beneficiary Currency :</label>
										</td>
										<td>
											<input name="benCurrency" id="benCurrency" type="text" class="input-small" />
										</td>
									</tr>-->
                      <tr id="accountNameCont">
                <td><label id="accname">Name of Beneficiary<em>*</em> :</label></td>
                <td><input name="accountName" id="accountName" type="text" class="input-medium" /></td>
                <td><a href="#"  class="tip" id="benNameTip" data-placement="above" rel="popover" data-content="Enter the beneficiary complete name" title="Beneficiary Name"> [?] </a></td>
              </tr>
                      <tr id="accountNumberCont">
                <td><label id="selben_accnum">Account Number<em id="account_aesterik">*</em> :</label></td>
                <td><input name="accountNumber" id="accountNumber" type="text" class="input-medium" maxlength="40" /></td>
                <td><a href="#" class="tip" id="accountNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary account number" title="Beneficiary Account Number"> [?] </a></td>
              </tr>
                      <tr id="branchNameNumberCont">
                <td><label id="selben_bname">Branch Name / Number<em>*</em> :</label></td>
                <td><input name="branchNameNumber" id="branchNameNumber" type="text" class="input-medium" maxlength="40"/></td>
                <td><a href="#" class="tip" id="branchNameNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch name or number" title="Beneficiary Bank Branch Name / Number"> [?] </a></td>
              </tr>
                      <tr id="branchAddressCont">
                <td><label id="selben_badd">Branch Address<em>*</em> :</label></td>
                <td><input name="branchAddress" id="branchAddress" type="text" class="input-medium" maxlength="40"/></td>
                <td><a href="#" class="tip" id="branchAddressTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch address" title="Beneficiary Bank Branch Address"> [?] </a></td>
              </tr>
                      <tr id="routingNumberCont">
                <td><label id="selben_rtnum">Routing Number<em>*</em> :</label></td>
                <td><input name="routingNumber" id="routingNumber" type="text" class="input-medium" maxlength="40"/></td>
                <td><a href="#" class="tip" id="routingNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch routing number" title="Beneficiary Bank Branch Routing Number"> [?] </a></td>
              </tr>
                      <tr id="sortCodeCont">
                <td><label id="selben_sortcode">Sort Code<em>*</em> :</label></td>
                <td><input name="sortCode" id="sortCode" type="text" class="input-medium" maxlength="40"/></td>
                <td><a href="#" class="tip" id="sortCodeTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank sort code" title="Beneficiary Bank Sort Code"> [?] </a></td>
              </tr>
                      <tr id="ibanCont">
                <td><label id="selben_ibnnum">IBAN Number<em id="ibn_aesterik">*</em> :</label></td>
                <td><input name="iban" id="iban" type="text" class="input-medium" maxlength="40" onBlur = "checkIBANvalid()"/>
                          <input name="isiban" id = "isiban" type="hidden" /></td>
                <td><a href="#" class="tip" id="ibanTip" data-placement="above" rel="popover" data-content="Please enter your full IBAN number" title="Beneficiary IBAN"> [?] </a></td>
              </tr>
                      <tr id="swiftCont">
                <td><label id="selben_swf">Swift/BIC code<em id="swift_aesterik">*</em> :</label></td>
                <td><input name="swift" id="swift" type="text" class="input-medium" maxlength="16"/></td>
                <td><a href="#" class="tip" id="swiftTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank swift code" title="Swift Code"> [?] </a></td>
              </tr>
                      <tr>
                <td><label id="selben_ref">Your Reference :</label></td>
                <td><input name="reference" id="reference" type="text" class="input-medium"/></td>
                <td><a href="#" class="tip" id="refTip" data-placement="above" rel="popover" data-content="Please enter some character used for the reference of the beneficiary" title="Reference for beneficiary"> [?] </a></td>
              </tr>
                    </table>
            <div class="row align_right">
                      <input name="agreeBenInfo" id="agreeBenInfo" type="checkbox" class="bottom"/>
                      I confirm the above beneficiary details are correct. I understand that if a payment is returned there will be a &pound;30 fee. </div>
          </div>
                  <p id="selben_mand"><em>*</em> Mandatory Fields</p>
                </div>
        <div class="row align_center">
                  <input name="back" id="back" type="button" value="Back" class="btn simple-btn" onClick="this.form.action='make_payment-ii.php';this.form.submit();"/>
                  <input name="goBack" value="back" type="hidden"/>
                  <input name="clear" id="clear" type="reset" value="Clear Details" class="btn simple-btn"/>
                  <input name="proceed" id="proceed" type="submit" value="Proceed" class="btn actv_btn"/>
                  <input name="receivingAmount" type="hidden" value="<?php echo $strReceivingAmount; ?>"/>
                  <input name="sendingAmount" type="hidden" value="<?php echo $strSendingAmount; ?>"/>
                  <input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; ?>"/>
                  <input name="sendingCurrency" type="hidden" value="<?php echo $strSendingCurrency; ?>"/>
                  <input name="exchangeRate" type="hidden" value="<?php echo $strExchangeRate; ?>"/>
                  <input name="strSendCurrency" type="hidden" value="<?php echo $strSendCurrency; ?>"/>
                </div>
        <input name="sendBenValue" id = "sendBenValue" type="hidden" />
      </form>
            </div>
    
    <!--<div id="sidebar">
					<div class="step_head">
						<h2>STEP 1 TRANSACTION DETAILS</h2>
					</div>
					<p class="inner_step">Buying <?php echo number_format($strReceivingAmount, 2, ".", ",")." ".$strReceivingCurrency; ?></p>
					<p class="inner_step">&#64; <?php echo $strExchangeRate; ?></p>
					<p class="inner_step">Selling <?php echo number_format($strSendingAmount, 2, ".", ",")." ".$strSendingCurrency; ?></p>
                    
                    <?
					if(isset($arrBenInfo['countryId']))
					{
										 
						$arrBenBankRules = benBankDetailRule($arrBenInfo['countryId']);
						$strBenName = "Name: ".$arrBenInfo["benName"];
						$accountName = $arrBenBankRules["accountName"];
						$accNo = $arrBenBankRules["accNo"];
						$branchNameNumber = $arrBenBankRules["branchNameNumber"];
						$branchAddress = $arrBenBankRules["branchAddress"];
						$swiftCode = $arrBenBankRules["swiftCode"];
						$IBAN = $arrBenBankRules["iban"];
						$routingNumber = $arrBenBankRules["routingNumber"];
						$sortCode = $arrBenBankRules["sortCode"];
						$strBenDetailFirst = "";
						
						$strBenDetailSecond = "";
				
							
							if($accountName == "Y")
							{
								
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Account Name: ".$arrBenInfo["accountName"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Account Name: ".$arrBenInfo["accountName"];
									
								}
							}
							if($accNo == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Account No: ".$arrBenInfo["accountNumber"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Account No: ".$arrBenInfo["accountNumber"];
									
								}
							}
							if($branchNameNumber == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Branch Name: ".$arrBenInfo["branchName"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Branch Name: ".$arrBenInfo["branchName"];
									
								}
							}
							if($branchAddress == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Branch Address: ".$arrBenInfo["branchAddress"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Branch Address: ".$arrBenInfo["branchAddress"];
									
								}
							}
							if($swiftCode == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Swift Code: ".$arrBenInfo["swiftCode"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Swift Code: ".$arrBenInfo["swiftCode"];
									
								}
							}
							if($IBAN == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "IBAN: ".$arrBenInfo["IBAN"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "IBAN: ".$arrBenInfo["IBAN"];
									
								}
							}
							if($routingNumber == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Routing Number: ".$arrBenInfo["routingNumber"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Routing Number: ".$arrBenInfo["routingNumber"];
									
								}
							}
							if($sortCode == "Y")
							{
								if($strBenDetailFirst == "")
								{
									$strBenDetailFirst = "Sort Code: ".$arrBenInfo["sortCode"]; 
									
								}
								else if($strBenDetailSecond == "")
								{
									$strBenDetailSecond = "Sort Code: ".$arrBenInfo["sortCode"];
									
								}
							}
							
							$_SESSION["benBame"] = $strBenName;
							$_SESSION["firstBen"] = $strBenDetailFirst;
							$_SESSION["secondBen"] = $strBenDetailSecond;
					}
							?>
					<div class="step_head">
						<h2>STEP 2 BENEFICIARY DETAILS</h2>
					</div>
                   
                    <p class="inner_step">&nbsp;<? echo $_SESSION["benBame"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["firstBen"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["secondBen"]; ?></p>
                    
					<div class="step_head">
						<h2>STEP 3 PAYMENT DETAILS</h2>
					</div>
					<p class="inner_step">&nbsp;</p>
					<p class="inner_step">&nbsp;</p>
					<p class="inner_step">&nbsp;</p>
				</div>--> 
  </div>
          <?php 
			//debug($arrBenInfo);
			//debug($arrBenInfo["routingNumber"]);
				require_once "includes/footer.php";
			?>
        </div>
<script>
		<?php
			
		/*$strCountries = "{";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "'".$benCountriesResult[$ii]["countryName"]."':{'c':'".$benCountriesResult[$ii]["countryName"]."','i':'".$benCountriesResult[$ii]["isocode"]."','g':'".$benCountriesResult[$ii]["ibanlength"]."'}";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= "}";*/
		$strCountries = "Array(";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "Array('".strtoupper($benCountriesResult[$ii]["countryName"])."','".strtoupper($benCountriesResult[$ii]["isocode"])."','".$benCountriesResult[$ii]["ibanlength"]."','".$benCountriesResult[$ii]["countryId"]."')";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= ")";
		
		echo 'var arrCountrues ='.$strCountries.';'; 
		?>
		function isValidIBAN($v){ //This function check if the checksum if correct
			$v = $v.replace(/^(.{4})(.*)$/,"$2$1"); //Move the first 4 chars from left to the right
			$v = $v.replace(/[A-Z]/g,function($e){return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10}); //Convert A-Z to 10-25
			var $sum = 0;
			var $ei = 1; //First exponent 
			for(var $i = $v.length - 1; $i >= 0; $i--){
				$sum += $ei * parseInt($v.charAt($i),10); //multiply the digit by it's exponent 
				$ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
			}; 
			return $sum % 97 == 1;
		}
		
		var $patterns = { //Map automatic generated by IBAN-Patterns Online Tool
				IBAN: function($v){
					$v = $v.replace(/[ ]/g,''); 
					return /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/.test($v) && isValidIBAN($v);		
				},
		};
		
		
		
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			
			$('#countryTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#currencyTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#benTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#benNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#branchNameNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			
			$('#branchAddressTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#routingNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#sortCodeTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#ibanTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#swiftTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#refTip').popover({
				trigger: 'hover',
				offset: 5
			});
			
			<?php 
				if($intBenExist){
			?>
				SelectOption(document.forms[0].existBen, '<?php echo $intBenID; ?>');
				SelectOption(document.forms[0].benCountry, '<?php echo $arrBenInfo['countryId'];?>');
				<? $_SESSION["ben"] = $intBenID;?>
				//$('#benCountry').val('<?php echo $arrBenInfo['Country']; ?>');
				$("#sendBenValue").val($("#benCountry option:selected").val());
				$('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
				$('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
				$('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
				$('#routingNumber').val('<?php echo $arrBenInfo["routingNumber"]; ?>');
				
				$('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
				$('#iban').val('<?php echo $arrBenInfo['IBAN']; ?>');
				$('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
				$('#sortCode').val('<?php echo trim($arrBenInfo['sortCode']); ?>');
				$('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
			<?php
				}
				if($intError){
			?>
				SelectOption(document.forms[0].existBen, '<?php echo $strExistBen; ?>');
				SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountry; ?>');
				//$('#benCountry').val('<?php echo $arrBenInfo['Country']; ?>');
				$('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
				$('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
				$('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
				$('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
				$('#iban').focus();
				$('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
				$('#sortCode').val('<?php echo $arrBenInfo['sortCode']; ?>');
				$('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
			<?php 
				}
			?>
			
		var changeStausFlag='YES';
	function changeStausValidation()
		{
			var countryVal2= $("#benCountry option:selected").text();
			//var countryVal2=$("#sendBenValue").val();
			/**1=Uk*/
			
			if(countryVal2=='United Kingdom')
			{
				$('#countrymsgnote').html("<b>Enter an account no. or IBAN</b>");
				if(($('#accountNumber').val() !=''))
				{
					changeStausFlag='no';
					$("#iban").rules("remove");
					//$("#sortCode").rules("remove");
					$('#isiban').val('NO');
					//$("#accountNumber").rules("add");
					//$("#sortCode").rules("add");
				}else if($('#iban').val() !='')
				{
					//$("#iban").rules("add");
					$("#accountNumber").rules("remove");
					$("#sortCode").rules("remove");
					
					changeStausFlag='YES';
					$('#isiban').val('YES');
				}
			}else if(countryVal2=='United States')
			{
				$('#countrymsgnote').html("");
				changeStausFlag='YES';
				if($('#accountNumber').val() !='')
				{
					$("#swift").rules("remove");
					$("#routingNumber").rules("remove");
				}
			}else {
				$('#countrymsgnote').html("");
				changeStausFlag='YES';
				//$("#routingNumber").rules("add");
				//$("#swift").rules("add");
			}
		
		}
		
		
			
			
			/*function changeMySize(myvalue,iden)
{
if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}

var ch1 = document.getElementById("selben_cont_head1");
var slcben = document.getElementById("selben_slcben");
var extben = document.getElementById("selben_extben");
var ch = document.getElementById("selben_cont_head");
var info= document.getElementById("selben_info");
var benc = document.getElementById("selben_benC");

var nop = document.getElementById("selben_nameop");
//var accname = document.getElementById("selben_accname");
var accnum = document.getElementById("selben_accnum");
var bname = document.getElementById("selben_bname");
var badd = document.getElementById("selben_badd");
var rtn = document.getElementById("selben_rtnum");
var stcd = document.getElementById("selben_sortcode");
var ibnn= document.getElementById("selben_ibnnum");
var swf = document.getElementById("selben_swf");
var ref = document.getElementById("selben_ref");

//var bencur = document.getElementById("bencur");


var abi = document.getElementById("agreeBenInfo");
 
ch1.style.fontSize = myvalue + "px";
slcben.style.fontSize = myvalue + "px";
extben.style.fontSize = myvalue + "px";
info.style.fontSize = myvalue + "px";
benc.style.fontSize = myvalue + "px";
nop.style.fontSize = myvalue + "px";
accnum.style.fontSize = myvalue + "px";
swf.style.fontSize = myvalue + "px";
ref.style.fontSize = myvalue + "px";
ch.style.fontSize = myvalue + "px";
bname.style.fontSize = myvalue + "px";
badd.style.fontSize = myvalue + "px";
rtn.style.fontSize = myvalue + "px"; 
stcd.style.fontSize = myvalue + "px"; 
ibnn.style.fontSize = myvalue + "px"; 

bencur.style.fontSize = myvalue + "px";

abi.style.fontSize = myvalue + "px"; 
}*/

			function checkAgree(){
				changeStausValidation();
				var iban = $('#iban').val();
				var country=$('#sendBenValue').val();
				var isiban = $('#isiban').val();
					var checkLength=checkIBANLength(country,iban);
					if(checkLength==false && isiban == "YES"){
						$('#iban').focus();
						alert("Please enter the correct IBAN number for your beneficiary country");
						$('#iban').focus();
						return false;
					}
				
				if(!($('#agreeBenInfo').is(':checked'))){
					alert('Please click the confirmation of beneficiary details correction');
					return false;
				}
		else if(!$patterns.IBAN(iban) && isiban == "YES")
				{
					alert("Please enter a valid IBAN");
					return false;
					
				}
				else
				{
					/*if($("#sendingCurrency").val() == 'EUR')
					{
						if($("#benCountry option:selected").text().toUpperCase() == 'AUSTRALIA')
						{
							if(iban.substr(0,2) != 'AU')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'BELGIUM')
						{
							if(iban.substr(0,2).toUpperCase() != 'BE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FINLAND')
						{
							if(iban.substr(0,2) != 'FI')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRANCE')
						{
							//alert(iban);
							//alert($("#sendBenValue").val().toUpperCase());
							//alert(iban.substr(0,2));
							if(iban.substr(0,2) != 'FR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GERMANY')
						{
							if(iban.substr(0,2) != 'DE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GREECE')
						{
							if(iban.substr(0,2) != 'GR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'IRELAND')
						{
							if(iban.substr(0,2) != 'IR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'ITALY')
						{
							if(iban.substr(0,2) != 'IT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'NETHERLANDS')
						{
							if(iban.substr(0,2) != 'NL')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'PORTUGAL')
						{
							if(iban.substr(0,2) != 'PT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'SPAIN')
						{
							if(iban.substr(0,2) != 'ES')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'ANDORRA')
						{
							if(iban.substr(0,2) != 'AD')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'SLOVAKIA')
						{
							if(iban.substr(0,2) != 'SK')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRENCH GUIANA')
						{
							if(iban.substr(0,2) != 'GF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRENCH SOUTHERN TERRITORIES')
						{
							if(iban.substr(0,2) != 'TF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GUADELOUPE')
						{
							if(iban.substr(0,2) != 'GP')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'MARTINIQUE')
						{
							if(iban.substr(0,2) != 'MQ')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'MAYOTTE')
						{
							if(iban.substr(0,2) != 'YT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else
						{
							return true;
						}
					}
					else
					{
						return true;	
					}*/
					return true;
				}
					
			}
			function checkIBANvalid(){
				var iban = $('#iban').val();
				var country=$('#sendBenValue').val();
				
					var checkLength=checkIBANLength(country,iban);
					if(checkLength==false){
						$('#iban').focus();
						alert("Please enter the correct IBAN number for your beneficiary country");
						$('#iban').focus();
						return false;
					}
				var isiban = $('#isiban').val();
				
				//alert(changeStausFlag);
				if(!$patterns.IBAN(iban) && isiban == "YES" )
				{
					
					alert("Please enter the correct IBAN number for your beneficiary country");
					return false;
				}
				else
				{
					//alert('in valid condition');
					/*if($("#sendingCurrency").val() == 'EUR')
					{
						if($("#benCountry option:selected").text().toUpperCase() == 'AUSTRALIA')
						{
							if(iban.substr(0,2) != 'AU')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'BELGIUM')
						{
							if(iban.substr(0,2).toUpperCase() != 'BE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FINLAND')
						{
							if(iban.substr(0,2) != 'FI')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRANCE')
						{
							//alert(iban);
							//alert($("#sendBenValue").val().toUpperCase());
							//alert(iban.substr(0,2));
							if(iban.substr(0,2) != 'FR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GERMANY')
						{
							if(iban.substr(0,2) != 'DE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GREECE')
						{
							if(iban.substr(0,2) != 'GR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'IRELAND')
						{
							if(iban.substr(0,2) != 'IR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'ITALY')
						{
							if(iban.substr(0,2) != 'IT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'NETHERLANDS')
						{
							if(iban.substr(0,2) != 'NL')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'PORTUGAL')
						{
							if(iban.substr(0,2) != 'PT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'SPAIN')
						{
							if(iban.substr(0,2) != 'ES')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'ANDORRA')
						{
							if(iban.substr(0,2) != 'AD')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'SLOVAKIA')
						{
							if(iban.substr(0,2) != 'SK')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRENCH GUIANA')
						{
							if(iban.substr(0,2) != 'GF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'FRENCH SOUTHERN TERRITORIES')
						{
							if(iban.substr(0,2) != 'TF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'GUADELOUPE')
						{
							if(iban.substr(0,2) != 'GP')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'MARTINIQUE')
						{
							if(iban.substr(0,2) != 'MQ')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#benCountry option:selected").text().toUpperCase() == 'MAYOTTE')
						{
							if(iban.substr(0,2) != 'YT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else
						{
							return true;
						}
					}
					else
					{
						return true;	
					}*/
					return true;
				}
			}
			/*$('#benForTransaction').submit(
				function(){
					return checkAgree();
				}
			);*/
			/* $(document).click(
				function(){
					$('#overlay').fadeOut('slow');
				}
			);
			$('#errorRule').click(
				function(){
					return false;
				}
			); */
			
			
			function onChangeCurrency(country){
					//alert(country);
				$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_currency.php"?>",
					type:"POST",
					data: {
						country: country,
						
					},
					cache:false,
					async: true,
					dataType: 'json',
					success: function(data){
						//alert(data['currency']);
						$('#sendingCurrency').val(data['currency']);
						//getCountry(data['currency']);
						changeStausValidation();
					
					}
			});
		}
		function checkIBANLength(country,iban){
			//alert(arrCountrues.length);
			//alert("Checking Length etc."+iban);
			for (var m=0;m<arrCountrues.length;m++) {
				if(country==arrCountrues[m][3]){
				if(iban.length !=arrCountrues[m][2]){
					
					return false;
					break;
				
				}
				if(iban.substr(0,2) !=arrCountrues[m][1]){
					
					return false;
					break;
				
				}
				}
			
			}
			return true;
		
		}
		
			function getCountry(currency){
				$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_country.php"?>",
					type:"POST",
					data: {
						currency: currency,
						
					},
					cache:false,
					dataType: 'json',
					success: function(dataFun){
						//$('#benCountry').val(dataFun['c']);
						$('#sendBenValue').val(dataFun['c']);
						getBenRule(dataFun['c']);
					    
					}
			});
		}
		
			
			function getBenRule(country){
				$.ajax({
					url: "http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/ben_country_base_rule.php"; ?>",
					type: "POST",
					data: {
						country: country
					},
					cache: false,
					dataType: 'json',
					success: function(data){
						if(!data){
							errorTxt = "<p style='font-weight:bold;text-align:justify;'>We are sorry for the inconvenience, we are not allowing online deals in this country. Please call support on the following numbers and place your order by phone.</p><br /><div style='font-weight:bold;'>United Kingdom : +44 (0)845 021 2370 Portugal: +351 289 358 511 Spain:+34 971 576 724</div>";
							//$('#overlay').fadeIn('slow');
							$('#errorRule').fadeIn('slow');
							$('#errorRule').html(errorTxt);
						}else{
							$('#errorRule').hide();
							accountName = data.accountName;
							accNo = data.accNo;
							branchNameNumber = data.branchNameNumber;
							branchAddress = data.branchAddress;
							swiftCode = data.swiftCode;
							IBAN = data.iban;
							routingNumber = data.routingNumber;
							sortCode = data.sortCode;
							if(accountName == 'N'){
								$('#accountName').attr('disabled', 'disabled');
								$('#accountNameCont').fadeOut('fast');
							}else if(accountName == 'Y'){
								$('#accountName').removeAttr('disabled');
								$('#accountNameCont').fadeIn('fast');
							}
							
							if(accNo == 'N'){
								$('#accountNumber').attr('disabled', 'disabled');
								$('#accountNumberCont').fadeOut('fast');
							}else if(accNo == 'Y'){
								$('#accountNumber').removeAttr('disabled');
								$('#accountNumberCont').fadeIn('fast');
							}
							
							if(branchNameNumber == 'N'){
								$('#branchNameNumber').attr('disabled', 'disabled');
								$('#branchNameNumberCont').fadeOut('fast');
							}else if(branchNameNumber == 'Y'){
								$('#branchNameNumber').removeAttr('disabled');
								$('#branchNameNumberCont').fadeIn('fast');
							}
							
							if(branchAddress == 'N'){
								$('#branchAddress').attr('disabled', 'disabled');
								$('#branchAddressCont').fadeOut('fast');
							}else if(branchAddress == 'Y'){
								$('#branchAddress').removeAttr('disabled');
								$('#branchAddressCont').fadeIn('fast');
							}
							
							if(swiftCode == 'N'){
								$('#swift').attr('disabled', 'disabled');
								$('#swiftCont').fadeOut('fast');
							}else if(swiftCode == 'Y'){
								$('#swift').removeAttr('disabled');
								$('#swiftCont').fadeIn('fast');
							}
							
							if(IBAN == 'N'){
								$('#iban').attr('disabled', 'disabled');
								$('#ibanCont').fadeOut('fast');
								$('#isiban').val('NO');
							}else if(IBAN == 'Y'){
								$('#iban').removeAttr('disabled');
								$('#ibanCont').fadeIn('fast');
								$('#isiban').val('YES');
							}
							
							if(routingNumber == 'N'){
								$('#routingNumber').attr('disabled', 'disabled');
								$('#routingNumberCont').fadeOut('fast');
							}else if(routingNumber == 'Y'){
								$('#routingNumber').removeAttr('disabled');
								$('#routingNumberCont').fadeIn('fast');
							}
							
							if(sortCode == 'N'){
								$('#sortCode').attr('disabled', 'disabled');
								$('#sortCodeCont').fadeOut('fast');
							}else if(sortCode == 'Y'){
								$('#sortCode').removeAttr('disabled');
								$('#sortCodeCont').fadeIn('fast');
							}
							
							onChangeCurrency(country);
						}
						
					}
				});
			}
			
			if($('#benCountry').val())
				getBenRule($('#benCountry').val());
			
			$('#benCountry').change(
				function(){
					$('#errorRule').hide();
					if($(this).val() != ''){
						$('#iban').val('');
						$('#sendBenValue').val($(this).val());
						getBenRule($(this).val());
					}
				}
			);
			
			$('#sendingCurrency').change(
				function(){
					//alert($(this).val());
					//if($(this).val() != '')
					
					//var benCountryVal = $('#benCountry').val();
						//getCountry($(this).val());
						
//$('#benCountry').val(benCountryVal)
				//countryVal=$(this).attr('');
						
						
				}
			);
			
			/*$.validator.addMethod("ibanValidate", function(value, element) {
				//alert('test');
				pattern = /^((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/;
			  return this.optional(element) || pattern.test(value);
			}, "Please enter correct IBAN number");*/
			
			$('#benForTransaction').validate({
				rules: {
					benName: {
						required: true
					},
					
					accountNumber: {
						required: true
						
					},
					branchNameNumber: {
						required: true
						
					},
					branchAddress: {
						required: true
						
					},
					routingNumber: {
						required: true
						
					},
					sortCode: {
						required: true
						
					},
					
					iban: {
						required: true
						//},
						//ibanValidate: true
					},
					swift: {
						required: true
					}
				}
			});
		</script>
</body>
</html>