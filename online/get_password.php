<?php 
	/**
	 * @Package: Online Module
	 * @SubPackage: self request to issue passoword
	 */
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	if(!empty($_POST['customerNumber']) && !empty($_POST['day']) && !empty($_POST['month']) && !empty($_POST['year'])){
		$strCustomerNumber = trim($_POST['customerNumber']);
		$strBirthDate = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
		$strQueryVerification = "SELECT customerID, customerStatus, password FROM ".TBL_CUSTOMER." WHERE accountName = '$strCustomerNumber' AND dob = '$strBirthDate'";
		$arrCustomer = selectFrom($strQueryVerification);
		$intFlagVerify = false;
		if(!empty($arrCustomer['customerID']) && !empty($arrCustomer['password'])){
			$intFlagVerify = true;
			$_SESSION['issuePassCustomerID'] = $arrCustomer['customerID'];
		}elseif(empty($arrCustomer['password']) && !empty($arrCustomer['customerID'])){
			$strError = "The customer has already issued password.";
		}else{
			$strError = "The details you entered are not correct.";
		}
	}
	
	if(!empty($_POST['email']) && !empty($_POST['pass1']) && !empty($_POST['pass2'])){
		require_once("../admin/lib/phpmailer/class.phpmailer.php");
		$strEmail = trim($_POST['email']);
		$strPassword1 = $_POST['pass1'];
		$strPassword2 = $_POST['pass2'];
		$intCustomerID = $_SESSION['issuePassCustomerID'];
		$strQueryPassword = "UPDATE ".TBL_CUSTOMER." SET `email` = '$strEmail', `password` = '$strPassword'";
		
		
		/*
			Mailer
			$arrCustomer = selectFrom($strQueryCustomer);
				$subject = "Issue Password";
				$customerMailer = new PHPMailer();
				$strBody = "Dear ".$arrCustomer[$x]['customerName']."\r\n<br />";
				$strBody .= "Your Trading Account password has been set.\n<br />
				Your username is: ".$arrCustomer[$x]['email']."\r\n<br />
				Your new password is: [ ".$password." ]\r\n\n\r<br /><br />
				\n<br />".$arrCustomer[$x]["adminName"]."<br />\n
				Follow this link to login: http://clients.premfx.com/private_registration.php\n<br />
				<table>
					<tr>
						<td align='left' width='30%'>
						<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='20%'>&nbsp;</td>
						<td align='left'>
							<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
							55 Old Broad Street,London, EC2M 1RX.\n<br>
							<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
							<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
							Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
							<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
							<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
							<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
							C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
							<font color='#F7A30B'>Tel:</font> +34 971 576 724 
							 \n <br> 
							 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
						</td>
					</tr>
				</table>
				";
				$fromName  = SYSTEM;
				$customerMailer->FromName =  $fromName;
				$customerMailer->From =  $arrCustomer[$x]['adminEmail']; 
				$customerMailer->AddAddress($arrCustomer[$x]['email'],'');
				$customerMailer->Subject = $subject;
				$customerMailer->IsHTML(true);
				$customerMailer->Body = $strBody;
				$custEmailFlag = $customerMailer->Send();
		 */
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Premier FX - Get Passowrd</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<img alt="PremierFX" src="images/logo.png" />
				<div class="pull-right links">
					<ul>
						<li><a href="index.php">Click here to Login</a></li>
					</ul>
				</div>
			</div>
			<div id="container">
				<?php 
					if(!$intFlagVerify){
				?>
				<h1 class="title">Verify your Identity</h1>
				<p>
				We can help you set your password to access Premier FX Online Trading<br />
				For your security, we will need to verify your identity first. Please have the following information available:
				</p>
				<?php 
					if(!empty($strError)){
				?>
				<table class="table medium clear-top">
					<tr class="error">
						<td class="error align_center"><?php echo $strError; ?></td>
					</tr>
				</table>
				<?php 
					}
				?>
				<form id="verifyIdentityFrom" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
					<table class="clear-top medium">
						<tr>
							<td class="bold">Customer Number <em>*</em> :</td>
							<td><input name="customerNumber" id="customerNumber" class="input-medium" type="text" maxlength="8"/></td>
						</tr>
						<tr>
							<td class="bold">Date of birth <em>*</em> :</td>
							<td>
								<select name="day" id="day" class="input-small">
									<option value="">Day</option>
									<?php 
										for($x = 1; $x <= 31; $x++){
											$strDay = str_pad($x, 2,"0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="month" id="month" class="input-small">
									<option value="">Month</option>
									<?php 
										for($x = 0;$x <= 12;$x++){
											$strMonth = str_pad($x, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strMonth'>$strMonth</option>";
										}
									?>
								</select>
								<select name="year" id="year" class="input-small">
									<option value="">Year</option>
									<?php 
										for($y = 1970; $y <= 1996; $y++)
											echo "<option value='$y'>$y</option>";
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="align_center" colspan="2">
								<input id="verify" type="submit" value="Verify" class="actv_btn btn"/>
							</td>
						</tr>
					</table>
				</form>
				<?php 
					}
					if($intFlagVerify){
				?>
				<h1 class="title">Set Your Password</h1>
				<table class="table">
					<tr class="success medium">
						<td class="align_center" colspan="2">Your identity is verified</td>
					</tr>
				</table>
				<form id="savePasswordForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
					<table class="medium clear-top">
						<tr>
							<td class="bold">Email <em>*</em>:</td>
							<td>
								<input name="email" id="email" type="text" maxlength="32" class="input-medium"/>
							</td>
						</tr>
						<tr>
							<td class="bold">Password <em>*</em>:</td>
							<td>
								<input name="pass1" id="pass1" type="password" class="input-medium"/>
							</td>
						</tr>
						<tr>
							<td class="bold">Confirm Password <em>*</em>:</td>
							<td>
								<input name="pass2" id="pass2" class="input-medium" type="password" />
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" class="align_center">
								<input name="savePassword" id="savePassword" class="btn actv_btn" type="submit" value="Save Password"/>
							</td>
						</tr>
					</table>
				</form>
				<?php
					}
				?>
			</div>
		</div>
		<script type="text/javascript">
			function checkDob(){
				if($('#day').val() == ''){
					alert('Enter the day of your date of birth');
					$(this).focus();
					return false;
				}
				if($('#month').val() == ''){
					alert('Enter the month of your date of birth');
					$(this).focus();
					return false;
				}
				if($('#year').val() == ''){
					alert('Enter the year of your date of birth');
					$(this).focus();
					return false;
				}
				return true;
			}
			if($('#verifyIdentityFrom').length > 0){
				$('#verifyIdentityFrom').submit(
					function(){
						return checkDob();
					}
				);
				
				$('#verifyIdentityFrom').validate({
					rules: {
						customerNumber: {
							required: true,
							maxlength: 8
						}
					}
				});
			}
			
			if($('#savePasswordForm').length > 0){
				$('#savePasswordForm').validate({
					rules: {
						email: {
							required: true,
							email: true
						},
						pass1: {
							required: true,
							equalTo: '#pass2',
							minlength: 6
						},
						pass2: {
							required: true,
							equalTo: '#pass1',
							minlength: 6
						}
					},
					messages: {
						pass1: {
							equalTo: "Password did not match"
						},
						pass2: {
							equalTo: "password did not match"
						}
					}
				});
			}
		</script>
	</body>
</html>