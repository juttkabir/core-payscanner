<? 
	/*
		*Function Page
		*Discription: the function that we are using in premier_fx_online_module section will write  
		*in this Page
		* @author: Imran Naqvi
	*/
	session_start();
	include_once "includes/configs.php";
	include_once "includes/database_connection.php"; 
	dbConnect();
	include_once "includes/functions.php";
	
	// ini_set('display_errors',1);
// ini_set('display_startup_errors',1);
// error_reporting(-1);
	
	function getExchangeRate($args){
		$intReceivingAmount = 0;
		
		if(!empty($args['receivingCurrency']) && !empty($args['sendingCurrency']) && (!empty($args['receivingAmount']) && $args['receivingAmount'] > '0') || ($args['sendingAmount'] > '0'&& !empty($args['sendingAmount']))){
			
			$strBaseCurrency1 = "";
			$strBaseCurrency2 = "";  
			$queryBaseCurrencies = "select distinct(baseCurrency) from online_exchange_rates";
			
			$arrBaseCurrencies = selectMultiRecords($queryBaseCurrencies);
			
			$strSendingCurrency = trim($args['sendingCurrency']);
			$strReceivingCurrency = trim($args['receivingCurrency']);
			$strReceivingAmount = number_format((trim($args['receivingAmount'])), 4, ".", "");
			$strSendingAmount = number_format((trim($args['sendingAmount'])), 4, ".", "");
			
			if($strReceivingAmount != 0 && $strSendingAmount != 0)
			{
				$strReceivingAmount = 0;
			}
			if($strReceivingAmount != 0)
			{
				$strBaseAmountCurrency = $strReceivingCurrency;
				$strQuotedAmountCurrency = $strSendingCurrency;
				$fltAmountBase = $strReceivingAmount;
				$fltAmountIsSending = false; 
				
			}
			elseif($strSendingAmount != 0)
			{
				$strBaseAmountCurrency = $strSendingCurrency;
				$strQuotedAmountCurrency = $strReceivingCurrency;
				$fltAmountBase = $strSendingAmount;
				$fltAmountIsSending = true; 
			}
			for($i=0;$i<count($arrBaseCurrencies);$i++)
			{
				if($arrBaseCurrencies[$i]["baseCurrency"] == $strSendingCurrency)
				{
					$strBaseCurrency1 = $strSendingCurrency;
					$strFinalBaseCurrency = $strSendingCurrency;
					$strFinalQuotedCurrency = $strReceivingCurrency;
					$flagBaseIsSending = true;
				}
				elseif($arrBaseCurrencies[$i]["baseCurrency"] == $strReceivingCurrency)
				{
					$strBaseCurrency2 = $strReceivingCurrency;
					$strFinalBaseCurrency = $strReceivingCurrency;
					$strFinalQuotedCurrency = $strSendingCurrency;
					$flagBaseIsSending = false;
				}
			}
			
			if($strBaseCurrency1 !="" && $strBaseCurrency2 != "")
			{
				$flagBaseCurrency = "Both";
				
			}
			elseif($strBaseCurrency1 == "" && $strBaseCurrency2 == "")
			{
				$flagBaseCurrency = "None";
				
			}
			else
			{
				$flagBaseCurrency = "One";
				
			}
			
			if($flagBaseCurrency == "Both")
			{
				if($strBaseAmountCurrency == 'GBP')
				{
				}
				else if($strQuotedAmountCurrency == 'GBP')
				{
					$strFirstCurr = $strBaseAmountCurrency;
					$strBaseAmountCurrency = $strQuotedAmountCurrency;
					$strQuotedAmountCurrency = $strFirstCurr;
				}
				else if($strQuotedAmountCurrency == 'EUR')
				{
					$strFirstCurr = $strBaseAmountCurrency;
					$strBaseAmountCurrency = $strQuotedAmountCurrency;
					$strQuotedAmountCurrency = $strFirstCurr;
				}
				if($strSendingCurrency == "GBP")
				{
					$flgExtendMargin = "Sub";
				}
				else if($strReceivingCurrency == "GBP")
				{
					$flgExtendMargin = "Add";
				}
				else if($strSendingCurrency == "EUR")
				{
					$flgExtendMargin = "Sub";
				}
				else if($strReceivingCurrency == "EUR")
				{
					$flgExtendMargin = "Add";
				}
				$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' order by id DESC LIMIT 1";
				$arrExchangeRate = selectFrom($strQueryRate);
				$descript = "Get rate called"; 
				
				$customerID=$_SESSION['loggedUserData']['userID'];
				
				activities($_SESSION["loginHistoryID"],"SELECTION",$customerID,TBL_ONLINE_EXCHANGE_RATE,$descript);
				
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' and $fltAmountBase between fromAmount AND toAmount   AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				if(empty($arrExchangeMargin['margin'])){
					$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' and $fltAmountBase between fromAmount AND toAmount   order by id DESC LIMIT 1";
					
					
					
				
				//debug($strQueryMargin);
				$arrExchangeMargin = selectFrom($strQueryMargin);
				}
				$fltExchangeRate = $arrExchangeRate['exchangeRate'];
				$fltInverseRate = $arrExchangeRate['inverseRate'];
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
				if(empty($arrExchangeMargin['margin'])){
					
					if($args['sendingAmount']!='')
					{
						$convertedCurrency=$args['receivingCurrency'];
						if($args['sendingCurrency']=='GBP')
						{
							if($args['sendingAmount']<100 && $args['sendingAmount']>50000)
							{
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $strSendingAmount;
								
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								
								exit;
								
								
							}
							
						}
						
						else
						{	
							$sendingCurrency=$args['sendingCurrency'];
							$sendingAmount=$args['sendingAmount'];
							$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
							$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
							$totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
							if($totalAmountInGBP<100 && $totalAmountInGBP>50000){
								
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
					}
					
					
					
					else{
						$convertedCurrency=$args['sendingCurrency'];
						if($args['receivingCurrency']=='GBP')
						{
							
							if($args['receivingAmount']<100 && $args['receivingAmount']>50000)
							{
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
								//$_SESSION['exchangeRateError1'] = "Test Case16";
								
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
						else
						{	
							$recCurrency=$args['receivingCurrency'];
							$recAmount=$args['receivingAmount'];
							$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
							$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
							$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
							if($totalAmountInGBP<100 && $totalAmountInGBP>50000){
								
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
								
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
						
					}
					
				}
				if($strMarginType == "FIXED")
				{
					if($flgExtendMargin == "Add")
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($flgExtendMargin == "Add")
						{
							$fltExchangeRate = $fltExchangeRate + $fltMargin;
						}
						else
						{
							$fltExchangeRate = $fltExchangeRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						//debug($fltMargin);
						if($flgExtendMargin == "Add")
						{
							$fltExchangeRate = $fltExchangeRate + $fltMargin;
						}
						else
						{
							$fltExchangeRate = $fltExchangeRate - $fltMargin;
						}
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($flgExtendMargin == "Add")
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
				//$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
				if($strSendingCurrency =='GBP' && $strSendingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
				}
				else if($strSendingCurrency =='GBP' && $strReceivingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
				}
				else if($strReceivingCurrency == 'GBP' && $strReceivingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
				}
				else if($strReceivingCurrency == 'GBP' && $strSendingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
				}
				else if($strSendingCurrency =='EUR' && $strSendingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
				}
				else if($strSendingCurrency =='EUR' && $strReceivingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
				}
				else if($strReceivingCurrency == 'EUR' && $strReceivingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
				}
				else if($strReceivingCurrency == 'EUR' && $strSendingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
				}
				$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,4);
				////debug($arrExchangeRate,true);
			}
			elseif($flagBaseCurrency == "One")
			{
				if($strBaseCurrency1 != "")
				{
					$flgExtendMargin = "Sub";
				}
				else
				{
					$flgExtendMargin = "Add";
				}
				//debug($flagRateOrInverse);
				$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' order by id DESC LIMIT 1";
				$arrExchangeRate = selectFrom($strQueryRate);
				
				if($strBaseCurrency1 != "" && $strSendingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase;
				}
				else if($strBaseCurrency2 != "" && $strReceivingAmount != 0)
				{
					$fltConvertedAmnt = $fltAmountBase;
				}
				else
				{
					$fltConvertedAmnt = $fltAmountBase/$arrExchangeRate['exchangeRate'];
				}
				
				//debug($arrExchangeRate);
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' and $fltConvertedAmnt between fromAmount AND toAmount and statusType='Enable' AND rateFor = 'P' order by id DESC LIMIT 1";
				//debug($strQueryMargin);
				$arrExchangeMargin = selectFrom($strQueryMargin);
				
				
				if(empty($arrExchangeMargin['margin'])){
					
					if($args['sendingAmount']!='')
					{
						$convertedCurrency=$args['receivingCurrency'];
						
						if($args['sendingCurrency']=='GBP')
						{
							if($args['sendingAmount']<100 || $args['sendingAmount']>50000)
							{
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $strSendingAmount;
								//$_SESSION['exchangeRateError1'] = "Test Case14";
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								
								exit;
							}
							
						}
						
						else
						{	
							$sendingCurrency=$args['sendingCurrency'];
							$sendingAmount=$args['sendingAmount'];
							$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
							$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
							$totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
							if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
								
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
								//$_SESSION['exchangeRateError1'] = "Test Case13";
								
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
					}
					
					
					
					else{
						$convertedCurrency=$args['sendingCurrency'];
						if($args['receivingCurrency']=='GBP')
						{
							
							if($args['receivingAmount']<100 || $args['receivingAmount']>50000)
							{
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
								//$_SESSION['exchangeRateError1'] = "Test Case12";
								
								echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
						else
						{	
							$recCurrency=$args['receivingCurrency'];
							$recAmount=$args['receivingAmount'];
							$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
							$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
							$totalAmountInGBP=$recAmount/$getExchangeRateResult[0]['inverseRate'];
							if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
								
								$_SESSION['receivingAmount'] = $strReceivingAmount;
								$_SESSION['sendingAmount'] = $strSendingAmount;
								$_SESSION['sendingCurrency'] = $strSendingCurrency;
								$_SESSION['receivingCurrency'] = $strReceivingCurrency;
								$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
								
								echo	$returnVal = "Please contact us for an exchange rate - 0845 021 2370";
								exit;
							}
							
						}
						
						
					}
					
				}  
				
				if(!empty($arrExchangeMargin))
				{
					$fltExchangeRate = $arrExchangeRate['exchangeRate'];
					$fltInverseRate = $arrExchangeRate['inverseRate'];
					$strMarginType = $arrExchangeMargin['marginType'];
					$fltMargin = $arrExchangeMargin['margin'];
					
					$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
					
					if($strMarginType == "FIXED")
					{
						if($flgExtendMargin =='Add')
						{
							$fltExchangeRate = $fltExchangeRate + $fltMargin;
						}
						else
						{
							$fltExchangeRate = $fltExchangeRate - $fltMargin;
						}
					}
					elseif($strMarginType == "PIPS")
					{
						if($strQuotedAmountCurrency == "JPY")
						{
							$fltMargin = $fltMargin*0.01;
							if($flgExtendMargin =='Add')
							{
								$fltExchangeRate = $fltExchangeRate + $fltMargin;
							}
							else
							{
								$fltExchangeRate = $fltExchangeRate - $fltMargin;
							}
						}
						else
						{
							$fltMargin = $fltMargin*0.0001;
							//debug($fltMargin);
							if($flgExtendMargin =='Add')
							{
								$fltExchangeRate = $fltExchangeRate + $fltMargin;
							}
							else
							{
								$fltExchangeRate = $fltExchangeRate - $fltMargin;
							}
						}
					}
					elseif($strMarginType == "PERCENTAGE")
					{
						$fltMargin = $fltMargin*0.01;
						if($flgExtendMargin =='Add')
						{
							$fltExchangeRate = $fltExchangeRate + $fltMargin;
						}
						else
						{
							$fltExchangeRate = $fltExchangeRate - $fltMargin;
						}
					}
					
					if($strBaseCurrency1 != "" && $strSendingAmount != 0)
					{
						$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
					}
					else if($strBaseCurrency2 != "" && $strReceivingAmount != 0)
					{
						$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
					}
					else
					{
						$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;
					}
					$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,4);
				}
				if(empty($fltExchangeRate)){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['re']="N";
					echo $returnVal = "We are sorry for the inconvenience, currently we are not dealing in this currency";
					exit;
				}		
			}
			elseif($flagBaseCurrency == "None")
			{
				
				if($args['sendingAmount']!='')
				{
					$convertedCurrency=$args['receivingCurrency'];
					if($args['sendingCurrency']=='GBP')
					{
						if($args['sendingAmount']<100 || $args['sendingAmount']>50000)
						{
							$_SESSION['receivingAmount'] = $strReceivingAmount;
							$_SESSION['sendingAmount'] = $strSendingAmount;
							$_SESSION['sendingCurrency'] = $strSendingCurrency;
							$_SESSION['receivingCurrency'] = $strReceivingCurrency;
							$_SESSION['totalAmountInGBP'] = $strSendingAmount;
							//$_SESSION['exchangeRateError1'] = "Test Case10";
							echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
							exit;
						}
						
					}
					
					else
					{	
						$sendingCurrency=$args['sendingCurrency'];
						$sendingAmount=$args['sendingAmount'];
						$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
						$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
						$totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
						if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
							
							$_SESSION['receivingAmount'] = $strReceivingAmount;
							$_SESSION['sendingAmount'] = $strSendingAmount;
							$_SESSION['sendingCurrency'] = $strSendingCurrency;
							$_SESSION['receivingCurrency'] = $strReceivingCurrency;
							$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
							//$_SESSION['exchangeRateError1'] = "Test Case9";
							echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
							exit;
						}
						
					}
				}
				
				
				
				else{
					$convertedCurrency=$args['sendingCurrency'];
					if($args['receivingCurrency']=='GBP')
					{
						
						if($args['receivingAmount']<100 || $args['receivingAmount']>50000)
						{
							$_SESSION['receivingAmount'] = $strReceivingAmount;
							$_SESSION['sendingAmount'] = $strSendingAmount;
							$_SESSION['sendingCurrency'] = $strSendingCurrency;
							$_SESSION['receivingCurrency'] = $strReceivingCurrency;
							$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
							//$_SESSION['exchangeRateError1'] = "Test Case8";
							echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
							exit;
						}
						
					}
					else
					{	
						$recCurrency=$args['receivingCurrency'];
						$recAmount=$args['receivingAmount'];
						$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
						$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
						$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
						if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
							
							$_SESSION['receivingAmount'] = $strReceivingAmount;
							$_SESSION['sendingAmount'] = $strSendingAmount;
							$_SESSION['sendingCurrency'] = $strSendingCurrency;
							$_SESSION['receivingCurrency'] = $strReceivingCurrency;
							$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
							//$_SESSION['exchangeRateError1'] = "Test Case7";
							echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
							exit;
						}
						
					}
					
					
				}
				
				if($strBaseAmountCurrency == $strSendingCurrency)
				{
					$fromCurrency = $strSendingCurrency;
					$toCurrency = $strReceivingCurrency;
					
				}
				else
				{
					$fromCurrency = $strReceivingCurrency;
					$toCurrency = $strSendingCurrency;
				}
				//debug($strSendingCurrency);
				//debug($strReceivingCurrency);
				$queryUSDFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateFrom = selectFrom($queryUSDFrom);
				$fromRate = $arrExchangeRateFrom["exchangeRate"];
				//Calculate margin of From for USD
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				//Margin END From for USD
				
				$queryUSDTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateTo = selectFrom($queryUSDTo);
				
				$toRate = $arrExchangeRateTo["exchangeRate"];
				
				//Calculate margin of From for USD
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				//Margin END to for USD
				$fltConvertedAmnt1 = $fltAmountBase/$fromRate*$toRate;
				//USD END
				//start GBP
				$queryGBPFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateFrom = selectFrom($queryGBPFrom);
				$fromRate = $arrExchangeRateFrom["exchangeRate"];
				//Calculate margin of From for GBP
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					$fromRate = $fromRate-$fltMargin;
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				//Margin END From for GBP
				
				$queryGBPTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateTo = selectFrom($queryGBPTo);
				
				$toRate = $arrExchangeRateTo["exchangeRate"];
				
				//Calculate margin of From for GBP
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency'  and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				//Margin END to for GBP
				$fltConvertedAmnt2 = $fltAmountBase/$fromRate*$toRate;
				//GBP END
				
				//start EUR
				$queryEURFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateFrom = selectFrom($queryEURFrom);
				$fromRate = $arrExchangeRateFrom["exchangeRate"];
				//Calculate margin of From for EUR
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}
						
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$fromRate = $fromRate + $fltMargin;
						}
						else
						{
							$fromRate = $fromRate - $fltMargin;
						}				
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				//Margin END From for EUR
				
				$queryEURTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
				$arrExchangeRateTo = selectFrom($queryEURTo);
				
				$toRate = $arrExchangeRateTo["exchangeRate"];
				
				//Calculate margin of From for EUR
				$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount AND rateFor = 'P' order by id DESC LIMIT 1";
				$arrExchangeMargin = selectFrom($strQueryMargin);
				$strMarginType = $arrExchangeMargin['marginType'];
				$fltMargin = $arrExchangeMargin['margin'];
				if($strMarginType == "FIXED")
				{
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				elseif($strMarginType == "PIPS")
				{
					if($strQuotedAmountCurrency == "JPY")
					{
						$fltMargin = $fltMargin*0.01;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}
					}
					else
					{
						$fltMargin = $fltMargin*0.0001;
						if($strSendingAmount !=0)
						{
							$toRate = $toRate + $fltMargin;
						}
						else
						{
							$toRate = $toRate - $fltMargin;
						}				
					}
				}
				elseif($strMarginType == "PERCENTAGE")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				//Margin END to for EUR
				$fltConvertedAmnt3 = $fltAmountBase/$fromRate*$toRate;
				//EUR END
				
				if($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "" && $fltConvertedAmnt3 !="")
				{
					$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2, $fltConvertedAmnt3));
				}
				elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "")
				{
					$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2));
				}
				elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt3 != "")
				{
					$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt3));
				}
				elseif($fltConvertedAmnt2!="" && $fltConvertedAmnt3 != "")
				{
					$fltConvertedAmnt = min(array($fltConvertedAmnt2, $fltConvertedAmnt3));
				}
				elseif($fltConvertedAmnt1!="")
				{
					$fltConvertedAmnt = $fltConvertedAmnt1;
				}
				elseif($fltConvertedAmnt2!="")
				{
					$fltConvertedAmnt = $fltConvertedAmnt2;
				}
				elseif($fltConvertedAmnt3!="")
				{
					$fltConvertedAmnt = $fltConvertedAmnt3;
				}
				if($fltConvertedAmnt1!="" || $fltConvertedAmnt2 != "" || $fltConvertedAmnt3 !="")
				{
					$arrExchangeRate = $fromRate;
					$fltExchangeRate = $toRate/$fromRate;
					$fltInverseRate =  $fromRate/$toRate;
				}
				//debug($fltConvertedAmnt);
				
				
			}
			
			if($args['sendingAmount']!=''){
				$convertedCurrency=$args['receivingCurrency'];
				if($args['sendingCurrency']=='GBP'){
					if($args['sendingAmount']<100 || $args['sendingAmount']>50000){
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strSendingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case6";
						$_SESSION['re']="y";
						echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
						
						exit();
						
					}
					
				}
				
				else
				{	
					$sendingCurrency=$args['sendingCurrency'];
					$sendingAmount=$args['sendingAmount'];
					$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
					if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
						
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case5";
						$_SESSION['re']="y";
						echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
						
						exit();
						
					}
					
				}
			}
			else{
				$convertedCurrency=$args['sendingCurrency'];
				if($args['receivingCurrency']=='GBP'){
					
					if($args['receivingAmount']<100 || $args['receivingAmount']>50000)
					{
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case4";
						$_SESSION['re']="y";
						echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
						
						exit();
						
					}
					
				}
				else
				{	
					$recCurrency=$args['receivingCurrency'];
					$recAmount=$args['receivingAmount'];
					$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
					if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
						
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case3";
						$_SESSION['re']="y";
						echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
						
						exit;
						
						
					}
					
				}
				
				
			}
			if($convertedCurrency=='GBP'){
				if($fltConvertedAmnt<100 && $fltConvertedAmnt>50000){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					$_SESSION['re']="y";
					echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
					
					exit;
					
				}
			}
			else{
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$convertedCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$fltConvertedAmnt*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 && $totalAmountInGBP>50000){
					
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					$_SESSION['re']="y";
					echo $returnVal = "Please contact us for an exchange rate - 0845 021 2370";
					exit;
					
				}
				
			}
			
			if(empty($arrExchangeRate)){
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['re']="N";
				echo $returnVal = "We are sorry for the inconvenience, currently we are not dealing in this currency";
				exit;
			}
		}
		
		
		
		$receivingAmount = "";
		if($strReceivingAmount != 0)
		{
			$receivingAmount = $strReceivingAmount;
			$intReceivingAmount = number_format($strReceivingAmount,2,".",",");
		}
		else
		{
			$receivingAmount = $fltConvertedAmnt;
			$intReceivingAmount = number_format($fltConvertedAmnt,2,".",",");
		}
		
		$_SESSION['receivingAmount'] = $receivingAmount;
		$_SESSION['sendingAmount'] = $strSendingAmount;
		$_SESSION['sendingCurrency'] = $strSendingCurrency;
		$_SESSION['receivingCurrency'] = $strReceivingCurrency;
		$_SESSION['exchangeRate']	= $fltExchangeRate;
		
		
		echo $intReceivingAmount.'|'.number_format($fltExchangeRate,4,".",",").'|'.$fltInverseRate;
		
		exit;
	}
	
	
	// function intercashAPI_credentials(){
		// $wsdl ="https://wdcs.prepaidgate.com/wswc/Service1.asmx?wsdl";
		// $user_name = '9R3MFx';
		// $password = 'pre^^7878ierk';
		
		
		// return $wsdl.'|'.$user_name.'|'.$password;
	// }
	
	function balanceInquiry($arg)
	{
		$customerID = $arg['customerID'];
		$san = $arg['san'];
		//$strCredentials = intercashAPI_credentials();
		
	 //$querCardNumber = mysql_fetch_assoc(mysql_query("SELECT SAN FROM  `card_issue_response`  WHERE customerID = $customerID "));
		

		
		// $arrCredentials = explode('|',$strCredentials);
		
		// $wsdl = $arrCredentials[0];
		// $user_name = $arrCredentials[1];
		// $password = $arrCredentials[2];
		
		$wsdl = CONFIG_INTERCASH_WSDL;
		$user_name = CONFIG_INTERCASH_USERNAME;
		$password = CONFIG_INTERCASH_PASSWORD;
		
		$client = new SoapClient($wsdl, array('USERNAME'=>$user_name,'PASSWORD'=>$password));  
		
		
		$objCardHolderBalanceEnquiry = new stdClass();
		
		$objCardHolderBalanceEnquiry->Username = $user_name;
		$objCardHolderBalanceEnquiry->Password = $password;
		//$Cardnumber = $querCardNumber['SAN'];
		$Cardnumber = $san;
		$objCardHolderBalanceEnquiry->Cardnumber = "$Cardnumber";
		$CardHolderBalanceEnquiryResponse =$client->  __soapCall('CardHolderBalanceEnquiry',array($objCardHolderBalanceEnquiry));
		$arrBalanceInquiry = (array) $CardHolderBalanceEnquiryResponse -> CardholderBalanceEnquiryResult;
		
		$strBalanceInquiry = implode($arrBalanceInquiry,',');
		
		echo( $strBalanceInquiry);
		//echo number_format($strBalanceInquiry, 2, ".", ",");
		
		exit;
	}
	
	function IssueCardRequest($arg){
		
		$customerID = $arg['customerID'];
		$currency = $arg['currency'];
		//$initialLoad = $arg['initialLoad'];
		$initialLoad = 0.00;
		

		$matches = array();
		
		
	//$strCredentials = intercashAPI_credentials();
	// $arrCredentials = explode('|',$strCredentials);
		
		// $wsdl = $arrCredentials[0];
		// $user_name = $arrCredentials[1];
		// $password = $arrCredentials[2];
	
	$wsdl = CONFIG_INTERCASH_WSDL;

		$user_name = CONFIG_INTERCASH_USERNAME;
		$password = CONFIG_INTERCASH_PASSWORD;

	
		$client = new SoapClient($wsdl, array('USERNAME'=>$user_name,'PASSWORD'=>$password,'trace' => true,'SOAPAction'=>'https://www.prepaidgate.com/ppRequestc/CardIssueRequestloaded'));
// echo "<pre>";
		// var_dump($client->__getFunctions());
		// echo "</pre>";
		// exit;
		
		$queryData = mysql_fetch_assoc(mysql_query("SELECT  c.Title,c.firstName,c.lastName,c.City,c.email,c.Address,c.Address1,c.Country,c.Zip,c.dob,uit.id,uit.id_number,c.Phone,c.initial_load,c.card_issue_currency FROM customer as c JOIN user_id_types as uit ON uit.user_id = c.customerID WHERE c.customerID =  $customerID"));
		
		$fetchIsoCode = mysql_fetch_assoc(mysql_query("SELECT intIsoCode FROM countries WHERE countryName = '".$queryData["Country"]."'"));
		
		$countryCode = $fetchIsoCode["intIsoCode"];
		preg_match_all('!\d+!', $queryData['id_number'], $matches);
		$passport = implode('', $matches[0]);
		
		$address1 = $queryData['Address'];
		$address2 = $queryData['Address1'];
		$email = $queryData['email'];
		$city = $queryData['City'];
		if($address1 ==""){
		$address1 = "Address";	
			}
			
			if($address2 ==""){
		$address2 = "Address1";	
			}
			
		if($email ==""){
		$email = "test@test.com";	
			}
			
			if($queryData['Zip'] ==""){
				
				$queryData['Zip'] = "123";
				}
		
		$objCardIssueRequest = new stdClass();	
		$objCardIssueRequest->Username = $user_name;
		$objCardIssueRequest->Password = $password;
		$objCardIssueRequest->Title = 'Mr';
		$objCardIssueRequest->Firstname = $queryData['firstName'];
		$objCardIssueRequest->Lastname = $queryData['lastName'];
		$objCardIssueRequest->Email = $email;
		$objCardIssueRequest->Address1 = $address1;
		$objCardIssueRequest->Address2 = $address2;
		$objCardIssueRequest->City = $city;
		$objCardIssueRequest->CountyState = 'Test State';
		$objCardIssueRequest->CountryCode = $countryCode;
		$objCardIssueRequest->PostCode = $queryData['Zip'];
		$objCardIssueRequest->DateofBirth = $queryData['dob'];
		$objCardIssueRequest->CityofBirth = 'Test City';
		$objCardIssueRequest->IDType = 1;
		$objCardIssueRequest->IDNumber = $passport;
		$objCardIssueRequest->Phone = $queryData['Phone'];
		$objCardIssueRequest->Amount = $initialLoad;
		//$objCardIssueRequest->Amount = $queryData['initial_load'];
		//$objCardIssueRequest->Currency = $queryData['card_issue_currency'];
		$objCardIssueRequest->Currency = $currency;
		$objCardIssueRequest->RemoteID = '';
		$objCardIssueRequest->IsVirtual = false;  
$objCardIssueRequest->ConvertToPlastic = true;	
$objCardIssueRequest->EmbossLine4 = "";	

		try {
			
		$responseCardIssueRequest = $client->__soapCall("CardIssueRequest", array($objCardIssueRequest));
		
		
			
			$arrStatusResult = (array)($responseCardIssueRequest -> CardIssueRequestResult);
			
			
			$soap_response = $client->__getLastResponse();
			$soap_request = $client->__getLastRequest();
			
			
			//echo $soap_response;
			 //echo "<br><br><br><br>";
			 //echo $soap_request;
			//$soap_request=$responseCardIssueRequest->__getLastRequest();

			
			if(empty($arrStatusResult['RemoteID'])){
				$arrStatusResult['RemoteID'] = 0;
			}
			
			
			if(empty($arrStatusResult['Description'])){
				$arrStatusResult['Description'] = '';
			}
$tbl = DATABASE;
loginDetailsLogs($customerID, $arrStatusResult['Status'],$currency,$tbl);
			if($arrStatusResult['Status'] === 0)
			{
				
				$issueResponseInsertion = "INSERT INTO card_issue_response (`customerID`,`IssueID`,`InitialLoadTxID`,`RemoteID`,`IssueAmount`,`Status`,`Description`,`SAN`,`Dated`,`card_issue_currency`) VALUES ( $customerID,".$arrStatusResult['IssueID'].",".$arrStatusResult['InitialLoadTxID'].",".$arrStatusResult['RemoteID'] .",".$arrStatusResult['IssueAmount'] .",".$arrStatusResult['Status'].",'".$arrStatusResult['Description'] ."',". $arrStatusResult['SAN'] . ",NOW(),'".$currency ."' ) ";  
				
				insertInto($issueResponseInsertion);
				
				update("UPDATE customer SET `card_issued` = 'Y' WHERE customerID =  $customerID");
				
			}
			
			
			$strInterCashLastRequest =  htmlspecialchars($client-> __getLastRequest()); 
			$strInterCashLastResponse =  htmlspecialchars($client->__getLastResponse()); 
			
			/* SOAP API LOG maintain */			
	 		$strSoapAPILogQuery = "INSERT into soap_Api_Logs (method ,transID,request ,response,responseCode ,dated ,remarks) VALUES ('SAL',$customerID,'".addslashes($strInterCashLastRequest)."','".addslashes($strInterCashLastResponse)."',".$arrStatusResult['Status'].",NOW(),'".addslashes($arrStatusResult['Description'])."')";
			insertInto($strSoapAPILogQuery); 
			
			/* END SOAP API LOG */
			
			
			
			}catch (Exception $e) {
			echo "<h2>Exception Error!</h2>";
			echo $e->getMessage();
		}
		
		$arrStatusResult = (array)($responseCardIssueRequest -> CardIssueRequestResult);
		
		echo json_encode($arrStatusResult);
		exit;
	}	
	
/* Log login details ARSLAN ZAFAR TICKET 13181 */	
	
	function loginDetailsLogs($customerID, $status,$currency,$tbl_name){
	session_start();
		$userid = $_SESSION["loggedUserData"]["userID"];
		
		
		$historyQuery = selectFrom("SELECT `history_id` FROM `login_history` WHERE `user_id` = $userid ORDER BY `history_id` DESC LIMIT 1");
		$historyID = $historyQuery['history_id'];
	
	if($status == 0){
		$desc = "Customer ".$currency." has been issued";
		}else{
	$desc =$currency." Card issue request REJECTED";
		}
	
	
	
	$insertLogin = "INSERT INTO login_activities (login_history_id,activity, action_for, description,table_name)
VALUES ('".$historyID."','UPDATE','".$customerID."','".$desc."','".$tbl_name."')";
	
	$loginLogs = insertInto($insertLogin);
	//echo $loginLogs;
		}
	
	
	function amountLimitPerMonth($args)
	{
		$customerID = $args["customerID"];
		$getTotalAmount = mysql_fetch_assoc(mysql_query("SELECT SUM(localAmount) AS loads FROM transactions WHERE customerID = $customerID AND transType = 'Topup' AND transDate > DATE_ADD( NOW( ) , INTERVAL -1 MONTH)"));
		if($getTotalAmount["loads"] ==null){
		$getTotalAmount["loads"] = 0;
		}
		echo $getTotalAmount["loads"];
		exit;
	}
	
	function loadBalance($arg){
		$customerID = $arg['customerID'];
		$sendingAmount = $arg['sendingAmount'];
		$sendingTypes = $arg['sendingTypes'];
	$transID = $arg['transID'];
		// $strCredentials = intercashAPI_credentials();
		
		// $arrCredentials = explode('|',$strCredentials);
		
		// $wsdl = $arrCredentials[0];
		// $user_name = $arrCredentials[1];
		// $password = $arrCredentials[2];
		
		$wsdl = CONFIG_INTERCASH_WSDL;
		$user_name = CONFIG_INTERCASH_USERNAME;
		$password = CONFIG_INTERCASH_PASSWORD;
		
		
		$client = new SoapClient($wsdl, array('USERNAME'=>$user_name,'PASSWORD'=>$password,'trace'=>1));
		
		$querAccount =mysql_fetch_assoc(mysql_query( "SELECT * FROM card_issue_response WHERE customerID = $customerID"));

		$objLoadBalance = new stdClass() ;
		$objLoadBalance -> Username = $user_name;
		$objLoadBalance -> Password = $password;
		$objLoadBalance -> Cardnumber = $querAccount["SAN"] ;
		$objLoadBalance -> Amount = $sendingAmount;
		$objLoadBalance -> Description = $querAccount["Description"];
		$objLoadBalance -> RemoteID = $querAccount["RemoteID"];  
		try {
			
			$responseCardLoadRequest = $client->__soapCall("CardLoadRequest", array($objLoadBalance));
			$loadResultArray = (array)($responseCardLoadRequest -> CardLoadRequestResult);
			
			
			$request = mysql_real_escape_string($client->__getLastRequest());
			
			$response = mysql_real_escape_string($client->__getLastResponse());

			

			
			}catch (Exception $e) {
			echo "<h2>Exception Error!</h2>";
			echo $e->getMessage();
		}

		
//echo "status code = ".$loadResultArray["Status"];
		if($loadResultArray["Status"] === 0)
		{
	if($sendingTypes == 'payex'){
	
	$nowUpdateTime = date('Y-m-d H:i:s');
	
	
	$update_query = update("update transactions set `transStatus` = 'Credited', `isSent` = 'Y',`deliveryDate` = '".$nowUpdateTime."'  WHERE transID = '".$transID."'");

	if($update_query){
	
	$logs_api = mysql_query("INSERT INTO soap_Api_Logs (transID,request,response,dated,remarks) VALUES('".$transID."','".$request."','".$response."','".$nowUpdateTime."','load balance api runs')");
	
	
		echo "successful";
		}else{
		echo "fail";
		}	
		}else{
		
		echo "successful";
		}
				
			}else{
			echo "fail";
		}
		exit;
	}
	
	function dailyTransLimit($arg){
		
		$customerID = $arg['customerID'];
		//
		$querTransaction = "SELECT count(transID) AS TransNum FROM transactions WHERE customerID = $customerID AND transDate LIKE '".date('Y-m-d')."%' AND transType = 'Topup'";
		
		$returnVal = mysql_fetch_array(mysql_query($querTransaction));
		echo (int)($returnVal['TransNum']);
	}
	
	
	//function getCurrencyWithLoad($reqArr)
	
	function getCurrencyWithLoad($reqArr){
		//echo $reqArr['customerID'];
		
		$issue_query = mysql_query("SELECT initial_load FROM prepaidCustData WHERE custID = ".$reqArr['customerID']." AND card_issue_currency = '".$reqArr['issueCurrency']."'");
		$issue_query_result = mysql_fetch_array($issue_query);
		
		if(!empty($issue_query_result)){
			$arrIssue =array("initial_load"=>$issue_query_result['initial_load'],"currency"=>$reqArr['issueCurrency']);
			}else{
			$arrIssue =array("initial_load"=>"","currency"=>$reqArr['issueCurrency']);
		}
		
		echo   json_encode($arrIssue);
		//echo $reqArr['issueCurrency'];
		//print_r(json_encode($reqArr));
	}
	
	
	//function to update/insert cardIssuedSender
	
	function cardIssuedSender($reqArr){
		
		/* === prepaidCustData table entry ==== */
		
		$customerID = $reqArr['customerID'];
		$currency = $reqArr['issueCurrency'];
		$initialLoad = $reqArr['initialLoad'];
		
		
		$checkExistence = mysql_query("SELECT * FROM `prepaidCustData` WHERE `custID` = $customerID AND card_issue_currency = '".$currency."' ");
		$exists =mysql_num_rows($checkExistence);
		
		
		
		
		if($exists >0){
			//update prepaidcustdata table
			$prepaidCustData =mysql_query("UPDATE prepaidCustData SET is_sent = 'Y' ,initial_load = 'NOT' WHERE custID = $customerID");
			}else{
			//insert new entry for customer
			$prepaidCustData = mysql_query("INSERT INTO prepaidCustData (custID,initial_load,card_issue_currency,is_sent,services) VALUES ($customerID,'NOT','".$currency."','Y','PC')");
		}		
		if($prepaidCustData){
		
		$updateCustomerRecord = mysql_query("UPDATE customer SET online_customer_services = 'TC,PC'  WHERE customerID = $customerID");
		
			echo  1;
			}else{
			echo  0;
		}
		
	}

/* WORK DONE AGAINST 13137 EMAIL TOP UP  ###### ARSLAN ZAFAR */	
function emailTopup($reqArr){
$transID = $reqArr['transID'];

$getTo = "select t.transDate, t.customerID,t.transAmount,t.localAmount,t.refNumberIM,t.transID,a.name as accountmanager,c.agentID, c.email,c.Title,c.firstName,CONCAT(c.Title ,c.middleName,c.lastName)as custname, c.middleName,c.lastName from transactions as t
inner join customer as c on c.customerID = t.customerID
inner join admin as a on c.agentID = a.userID
where t.refNumberIM ='".$transID."'
";
$too = selectFrom ($getTo);
$date= $too["transDate"];
$to = $too["email"];
$amount = $too["transAmount"];
$localAmount = $too["localAmount"];
$fullName =  $too["firstName"]; 
$custName = $too["custname"];

$accountManager = $too["accountmanager"];
$referenceNumber = $too["refNumberIM"];
$logoCompany = "<img height='100' border='0' src='http://".$_SERVER['HTTP_HOST']."/premier_fx_online_module/images/logo.png' />";

$queryTemp = "Select temp.* FROM email_templates as temp LEFT JOIN events as events ON temp.eventID=events.id WHERE events.name ='Email Top up' AND  temp.status='Enable'";
$resultQueryTemp = selectFrom($queryTemp); 
$subject=$resultQueryTemp['subject'];
$bodyQuery=$resultQueryTemp['body'];
$varEmail = array("{customer}","{transDate}","{custname}","{transAmount}","{localAmount}","{accountmanager}","{logo}");

$contentEmail = array($fullName,$date,$custName,$amount,$localAmount,$accountManager,$logoCompany);
$messageT = str_replace($varEmail,$contentEmail,$bodyQuery);
$strFromEmail=$resultQueryTemp['fromEmail'];
$status= '';
$from = 'Peter';
sendMail($to,$subject,$messageT,$from,$strFromEmail,$status);

if(sendMail){
echo "1";
}else{
echo "0";
}
}

    /*Get and update sender SAN number link against ticket number 13324 ### ARSLAN ZAFAR*/ 
 	function getSan($arg){ 
 	 
	$customerID =$arg['customerID']; 
 		$getSanQuery = selectFrom("SELECT `SAN` FROM `card_issue_response` WHERE `customerID` =$customerID"); 
 	 
 	if(empty($getSanQuery['SAN'])){ 
 	 
 	echo 0; 
 	}else{ 
	echo $getSanQuery['SAN']; 
 	} 
 	exit; 
 	} 
 		 
	 
 		        /*Get and update sender SAN number link against ticket number 13324 ### ARSLAN ZAFAR*/ 
 		function saveSan($arg){ 
 	 
 	$customerID =$arg['customerID']; 
 	$current_san =$arg['current_san']; 
 		$update_san =$arg['update_san']; 
 		 
 		$getSanQuery = selectFrom("SELECT `SAN_OLD` FROM `card_issue_response` WHERE `customerID` =$customerID"); 
 		$old_san = $getSanQuery['SAN_OLD']; 
 		$old_san = $old_san.",".$current_san; 
 	 if(!empty($customerID))
	 {
 		$query = update("UPDATE `card_issue_response` SET `SAN` = $update_san,`SAN_OLD`= '".$old_san."' WHERE `customerID` = $customerID") ; 
	 }	 
 		if($query){ 
 		 
 		echo 1; 
 		}else{ 
 		echo 0; 
 		} 
 		exit; 
 } 