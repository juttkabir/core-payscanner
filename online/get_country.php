<?php 
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	$currency= $_POST['currency'];
	$strRule = getCountry($currency);
	//debug($strRule);

	$strRule = json_encode($strRule);
	echo $strRule;
	
?>