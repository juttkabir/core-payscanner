<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Transaction History
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	include_once "includes/functions.php";
	
	if(isset($_GET['limit']) && $_GET['limit'] > 0)
		$limit = intval(trim($_GET['limit']));
	else
		$limit = 5;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	elseif(!empty($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;
		
	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	dbConnect();
	
	$intSearchFlag = false;
	if(!empty($_POST['search']) || !empty($_GET['search'])){
		$intSearchFlag = true;
		if(!empty($_POST['search'])){
			$strFDay = $_POST['fDay'];
			$strFMonth = $_POST['fMonth'];
			$strFYear = $_POST['fYear'];
			$strFromDate = $strFYear."-".$strFMonth."-".$strFDay." 00:00:00";
			$strTDay = $_POST['tDay'];
			$strTMonth = $_POST['tMonth'];
			$strTYear = $_POST['tYear'];
			$strToDate = $strTYear."-".$strTMonth."-".$strTDay." 23:59:59";
			$strSearchText = trim($_POST['searchText']);
			$strSearchType = $_POST['searchType'];
			$strAmountSearchCriteria = $_POST['amountSearch'];
			$strAmount = trim($_POST['amount']);
			$strCurrency = $_POST['currency'];
			$strTransStatus = $_POST['status'];
			$strApplyOnAmount = $_POST['applyOnAmount'];
			$strCondition = " ";
		}else{
			$strFDay = $_GET['fDay'];
			$strFMonth = $_GET['fMonth'];
			$strFYear = $_GET['fYear'];
			$strFromDate = $strFYear."-".$strFMonth."-".$strFDay." 00:00:00";
			$strTDay = $_GET['tDay'];
			$strTMonth = $_GET['tMonth'];
			$strTYear = $_GET['tYear'];
			$strToDate = $strTYear."-".$strTMonth."-".$strTDay." 23:59:59";
			$strSearchText = trim($_GET['searchText']);
			$strSearchType = $_GET['searchType'];
			$strAmountSearchCriteria = $_GET['amountSearch'];
			$strAmount = trim($_GET['amount']);
			$strCurrency = $_GET['currency'];
			$strTransStatus = $_GET['status'];
			$strApplyOnAmount = $_GET['applyOnAmount'];
			$strCondition = " ";
		}
		if(!empty($strSearchText) && $strSearchType == 'transRefNumber')
			$strCondition .= " AND refNumberIM = '$strSearchText'";
		elseif(!empty($strSearchText) && $strSearchType == 'beneficiaryName')
			$strCondition .= " AND (ben.firstName LIKE '$strSearchText%' OR ben.middleName LIKE '$strSearchText%' OR ben.lastName LIKE '$strSearchText%' OR ben.beneficiaryName LIKE '$strSearchText%') ";
		
		if(!empty($strApplyOnAmount)){
			if($strApplyOnAmount == 'sendingAmount')
				$strApplyAmount = "transAmount";
			elseif($strApplyOnAmount == 'receivingAmount')
				$strApplyAmount = "localAmount";
			else
				$strApplyAmount = "transAmount";
		}
		
		if($strAmountSearchCriteria != 'all' && !empty($strAmountSearchCriteria)){
			switch($strAmountSearchCriteria){
				case ">":
					$strCondition .= " AND $strApplyAmount > '$strAmount' ";
					break;
				case "<":
					$strCondition .= " AND $strApplyAmount < '$strAmount' ";
					break;
				case "=":
					$strCondition .= " AND $strApplyAmount = '$strAmount'";
					break;
				case ">=":
					$strAmount .= " AND $strApplyAmount >= '$strAmount' ";
					break;
				case "<=":
					$strCondition .= " AND $strApplyAmount <= '$strAmount' ";
					break;
			}
		}
		
		if(!empty($strCurrency) && $strCurrency != 'all'){
			$strCondition .= " AND currencyFrom = '$strCurrency' ";
		}
		
		/* elseif(!empty($strSearchText) && $strSearchType == 'sendingAmount')
			$strCondition .= " AND transAmount = '$strSearchText'";
		elseif(!empty($strSearchText) && $strSearchType == 'receivingAmount')
			$strCondition .= " AND localAmount = '$strSearchText'"; */
		if($strTransStatus != 'all' && !empty($strTransStatus))
			$strCondition .= " AND transStatus = '$strTransStatus' ";
		$strCondition .= " AND (transDate >= '$strFromDate' AND transDate <= '$strToDate')";
		$strCondition .= " AND trans_source = 'O'";
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."'";
		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."'".$strCondition;
		$strQueryGroupBy = " group by transDate DESC";
		$strQueryPagination = " LIMIT $offset, $limit";
		$strQuerySearch .= $strCondition;
		$strQuerySearch .= $strQueryGroupBy;
		$exportQuery = $strQuerySearch;
		//debug($strQuerySearch);
		$strQuerySearch .= $strQueryPagination;
		$arrTotalRecords = selectFrom($strQueryCountTotal);
		$intTotalRecords = $arrTotalRecords['records'];
		//debug($strQueryCountTotal);
		$arrTrans = selectMultiRecords($strQuerySearch);
		$intTotalTrans = count($arrTrans);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Transaction History</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="scripts/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="scripts/jquery.validate.js"></script>
		<style type="text/css">
			#content_one{
				overflow:visible;
			}
			.filters{
				position:relative;
				overflow:visible;
			}
			#imgContainer{
				position:absolute;
				left:-153px;
				top:4px;
				z-index:800;
			}
		</style>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				$currentPage = 'transaction history';
				include_once("includes/header.php");
			?>
			<div id="container">
				<h1 class="cont_head">History Search</h1>
				<!--<div id="imgContainer"><img src="images/amounts.png" alt="some_text"> </div>-->
				<div id="content_one">
					<a href="export_transaction_history.php?query=<? echo base64_encode(urlencode($exportQuery)); ?>&querycount=<? echo $intTotalRecords; ?>"><img src="images/excel_icon.png" alt="Excel" class="icon"/></a>
					<div class="filters">
                    <div id="imgContainer"><img src="images/amounts.png" alt="some_text"> </div>
						<form id="filters" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
							<table cellpadding="0" cellspacing="0" border="1" class="his_filters">
								<!--<tr>
									<th>&nbsp;</th>
									<th>Day</th>
									<th>Month</th>
									<th>Year</th>
									<th>Search Criteria:</th>
									<th>Search for:</th>
								</tr>-->
								<tr>
									<td class="bold">
										Date From:
									</td>
									<td>
										<select name="fDay" id="fDay" class="input-mini">
											<?php 
												for($d = 1; $d <= 31; $d++){
													$day = str_pad($d, 2, "0", STR_PAD_LEFT);
													echo "<option value='$day'>$day</option>";
												}
											?>
										</select>
										<select name="fMonth" id="fMonth" class="input-mini">
											<?php 
												for($d = 1; $d <= 12; $d++){
													$month = str_pad($d, 2, "0", STR_PAD_LEFT);
													echo "<option value='$month'>$month</option>";
												}
											?>
										</select>
										<select name="fYear" id="fYear" class="input-small">
											<option value="2005">2005</option>
											<option value="2006">2006</option>
											<option value="2007">2007</option>
											<option value="2008">2008</option>
											<option value="2009">2009</option>
											<option value="2010">2010</option>
											<option value="2011">2011</option>
											<option value="2012">2012</option>
											<option value="2013">2013</option>
										</select>
									</td>
									<td class="bold">
										Date To:
									</td>
									<td colspan="2">
										<select name="tDay" id="tDay" class="input-mini">
											<?php 
												for($d = 1; $d <= 31; $d++){
													$day = str_pad($d, 2, "0", STR_PAD_LEFT);
													echo "<option value='$day'>$day</option>";
												}
											?>
										</select>
										<select name="tMonth" id="tMonth" class="input-mini">
											<?php 
												for($d = 1; $d <= 12; $d++){
													$month = str_pad($d, 2, "0", STR_PAD_LEFT);
													echo "<option value='$month'>$month</option>";
												}
											?>
										</select>
										<select name="tYear" id="tYear" class="input-small">
											<option value="2005">2005</option>
											<option value="2006">2006</option>
											<option value="2007">2007</option>
											<option value="2008">2008</option>
											<option value="2009">2009</option>
											<option value="2010">2010</option>
											<option value="2011">2011</option>
											<option value="2012">2012</option>
											<option value="2013">2013</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="bold">
										Search Criteria:
									</td>
									<td>
										<select name="searchType" id="searchType">
											<option value="">- Select Search Criteria -</option>
											<option value="transRefNumber">Transaction Reference Number</option>
											<option value="beneficiaryName">Beneficiary Name</option>
											<!--<option value="sendingAmount">Sending Amount </option>
											<option value="receivingAmount">Receiving Amount </option>-->
										</select>
									</td>
									<td class="bold">
										Search for:
									</td>
									<td>
										<input name="searchText" id="searchText" class="input-medium"  type="text"/>
									</td>
								</tr>
								<tr>
									<td class="bold">
										Amount Search Criteria:
									</td>
									<td>
										<select id="amountSearch" name="amountSearch">
											<option value="">- Select Amount Criteria -</option>
											<option value=">">Greater Than</option>
											<option value="<">Less Than</option>
											<option value="=">Equal to</option>
											<option value=">=">Greater than and equal to</option>
											<option value="<=">Less than and equal to</option>
										</select>
									</td>
									<td class="bold">
										Amount:
									</td>
									<td>
										<input name="amount" id="amount" type="text" maxlength="8" class="input-medium"/>
									</td>
								</tr>
								<tr>
									<td class="bold">Apply on:</td>
									<td>
										<select name="applyOnAmount" id="applyOnAmount">
											<option value="">- Select Amount to apply on -</option>
											<option value="sendingAmount">Sending Amount</option>
											<option value="receivingAmount">Receving Amount</option>
										</select>
									</td>
									<td class="bold">
										Currency:
									</td>
									<td>
										<select name="currency" id="currency" class="input-small">
											<option value="all">All</option>
											<?php 
												$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_ONLINE_EXCHANGE_RATES." ORDER BY quotedCurrency ASC";
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['quotedCurrency']."'>".$arrCurrency[$x]['quotedCurrency']."</option>";
											?>
										</select>
									</td>
								</tr>
								<!--<tr>
									<td class="bold">
										Transaction Status:
									</td>
									<td>
										<select name="status" id="status" class="input-small">
											<option value="all">All</option>
											<option value="Pending">Pending</option>
											<option value="Suspended">Suspended</option>
											<option value="amended">Amended</option>
											<option value="Cancelled">Cancelled</option>
											<option value="Authorize">Authorized</option>
											<option value="Picked up">Picked up</option>
											<option value="Credited">Credited</option>
										</select>
									</td>
								</tr>-->
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="align_center" colspan="5">
										<input name="search" id="search" class="btn small-input simple-btn" type="submit" value="Search"/>
									</td>
								</tr>
							</table>
						</form>
						<!--<div class="row">
							<label class="txt_focus">Please enter search criteria</label>
						</div>-->
					</div>
					<?php 
						if($intTotalTrans > 0){
					?>
					<table class="table clear-top">
						<tr>
							<td class="bold">Showing <?php echo ($offset+1)." - ".($intTotalTrans + $offset)." of ".$intTotalRecords ?></td>
							<td class="bold align_right">
								Records Per Page:
								<select name="limit" id="limit" class="input-small" onchange="javascript:document.location.href='<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&search=search&limit='+this.value;" >
									<option value="5" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '5'){ echo ' selected="selected"';}?>>5</option>
									<option value="10" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '10'){ echo ' selected="selected"';}?>>10</option>
									<option value="20" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '20'){ echo ' selected="selected"';}?>>20</option>
									<option value="30" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '30'){ echo ' selected="selected"';}?>>30</option>
									<option value="50" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '50'){ echo ' selected="selected"';}?>>50</option>
								</select>
							</td>
							<td class="align_right" >
								<?php 
									if($prev >= 0){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">First</a>&nbsp;
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev;?>&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Previous</a>&nbsp;
								<?php 
									}
									if($next > 0 && $next < $intTotalRecords){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Next</a>&nbsp;
									<?php 
										$intLastOffset = (ceil($intTotalRecords / $limit) - 1) * $limit;
									?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset;?>&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=search">Last</a>
								<?php 
									}
								?>
							</td>
						</tr>
					</table>
					<table class="table cell-centered table-striped">
						<tr>
							<th width="170px;">
								Transaction Reference No.
								<!-- <a href="#" class="link-caret caret-down">&nbsp;</a>
								<a href="#" class="link-caret caret-up">&nbsp;</a> -->
							</th>
							<th>
								<!--Time(CET)-->
								Transaction Date
							</th>
							<th>
								Beneficiary Name
							</th>
							<th>
								Sending Amount
							</th>
							<th>
								Sending Currency
							</th>
							<th>
								Receiving Amount
							</th>
							<th>
								Receiving Currency
							</th>
							<th width="50px">
								Rate
							</th>
							<!--<th width="50px">
								Status
							</th>-->
						</tr>
						<?php 
							for($x = 0;$x < $intTotalTrans;$x++){
						?>
						<tr>
							<td width="80px;">
								<?php echo $arrTrans[$x]['refNumber']; ?>
							</td>
							<td>
								<?php echo $arrTrans[$x]['transDate']; ?>
							</td>
							<td>
								<?php echo $arrTrans[$x]['benName']; ?>
							</td>
							<td>
								<?php 
									//echo number_format($arrTrans[$x]['sendingAmount'], 2, ",",".");
								?>
								<?php echo $arrTrans[$x]['sendingAmount']; ?>
							</td>
							<td>
								<?php echo $arrTrans[$x]['sendingCurrency']; ?>
							</td>
							<td>
								<?php echo $arrTrans[$x]['recievingAmount']; ?>
							</td>
							<td>
								<?php echo $arrTrans[$x]['recievingCurrency']; ?>
							</td>
							<td width="50px">
								<?php echo $arrTrans[$x]['rate']; ?>
							</td>
							<!--<td width="50px">
								<?php echo $arrTrans[$x]['transStatus']; ?>
							</td>-->
						</tr>
						<?php 
							}
						?>
					</table>
					<?php 
						}elseif(isset($_REQUEST['search'])){
					?>
					<table class="table">
						<tr class="warning">
							<td class="align_center error">
								There are no transaction(s) to display.
							</td>
						</tr>
					</table>
					<?php 
						}
					?>
				</div>
			</div>
			<?php 
				include_once("includes/footer.php");
			?>
		</div>
		<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			
			SelectOption(document.forms[0].tDay, "<?php if(isset($strTDay)) echo $strTDay; else echo date('d'); ?>");
			SelectOption(document.forms[0].tMonth, "<?php if(isset($strTMonth)) echo $strTMonth; else echo date('m');?>");
			SelectOption(document.forms[0].tYear, "<?php if(isset($strTYear)) echo $strTYear; else echo date('Y');?>");
			<?php 
				if(isset($strFDay) && isset($strFMonth) && isset($strFYear)){
			?>
			SelectOption(document.forms[0].fDay, "<?php echo $strFDay; ?>");
			SelectOption(document.forms[0].fMonth, "<?php echo $strFMonth; ?>");
			SelectOption(document.forms[0].fYear, "<?php echo $strFYear; ?>");
			<?php 
				}
			?>
			<?php 
				//if(isset($strSearchType)){
				if($intSearchFlag){
			?>
			SelectOption(document.forms[0].searchType, "<?php echo $strSearchType;?>");
			SelectOption(document.forms[0].status, "<?php echo $strTransStatus;?>");
			$('#searchText').val('<?php echo $strSearchText; ?>');
			$('#amount').val('<?php echo $strAmount; ?>');
			SelectOption(document.forms[0].amountSearch, '<?php echo $strAmountSearchCriteria; ?>');
			SelectOption(document.forms[0].currency, '<?php echo $strCurrency; ?>');
			SelectOption(document.forms[0].applyOnAmount, '<?php echo $strApplyOnAmount; ?>');
			SelectOption(document.getElementById('limit'), '<?php echo $limit; ?>');
			$('#limit').change(
				function(){
					limit = $(this).val();
					window.location.href = '<?php echo $_SERVER['PHP_SELF'];?>?offset=0&fDay=<?php echo $strFDay; ?>&fMonth=<?php echo $strFMonth;?>&fYear=<?php echo $strFYear; ?>&tDay=<?php echo $strTDay; ?>&tMonth=<?php echo $strTMonth; ?>&tYear=<?php echo $strTYear; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit='+limit+'&search=search';
				}
			);
			<?php 
				}
			?>
			$('#filters').validate({
				rules: {
					searchType: {
						required: function(){
							searchTxt = $.trim($('#searchText').val());
							if(searchTxt != '')
								return true;
							else
								return false;
						}
					},
					searchText: {
						required: function(){
							searchType = $('#searchType').val();
							if(searchType != '')
								return true;
							else
								return false;
						}
					},
					amountSearch: {
						required: function(){
							amount = $.trim($('#amount').val());
							if(amount != '')
								return true;
							else
								return false;
						}
					},
					amount: {
						required: function(){
							amountSearch = $('#amountSearch').val();
							if(amountSearch != '')
								return true;
							else
								return false;
						}
					},
					applyOnAmount: {
						required: function(){
							amountSearch = $('#amountSearch').val();
							amount = $.trim($('#amount').val());
							if(amountSearch == '' && amount == '')
								return  false;
							else
								return true;
						}
					}
				}
			});
		</script>
        
	</body>
</html>
