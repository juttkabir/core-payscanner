<?php 
/**
* @package: Online Module	
* @Ticket : 12671
* @author :Imran Naqvi
*/
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
header("LOCATION: logout.php");
}
//ini_set("display_errors","on");
//error_reporting(E_ALL);  
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

?>
<!DOCTYPE HTML >
<html>
<head>
<title><?php echo CLIENT_NAME_ONLINE; ?></title>
<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
<meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/jquery.validate.js"></script>
<script type="text/javascript" src="javascript/date.js"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="javascript/ajaxupload.js"></script>
<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
<script>
$(function() {
var cd = $('#countdown');
var c = parseInt(cd.text(),10);
var interv = setInterval(function() {
c--;
cd.html(c);
if (c == 0) {
window.location.reload(false);
location.reload(true);
clearInterval(interv);
}
}, 1000);
});
</script>
</head>

<body>
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_payscanner.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<? if($_SESSION["loggedInUser"]["online_customer_services"] == "PC"){ ?>

<li class="menu_li"><a href="topup.php" class="menu_link" target="_parent">Top Up</a></li>

<li class="menu_li"><a href="balance_inquiry.php" class="menu_link" target="_parent">Balance Inquiry</a></li>

<? } else { ?>


<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>

<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>

<? } ?>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>

<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Balance Inquiry</h2>
<?php include('top-menu.php');?>

<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Available Balance</p>

</div>
<!-- content area left-->
<div class="content_area_left">

<div class="field_wrapper">
<!-- ticket number 12917 balance inquiry for multiple currency -->

<select name="receivingCurrency" id="receivingCurrency" class="select_field">

<?php 
$balanceQuery = mysql_query("SELECT `SAN`,`card_issue_currency` FROM `card_issue_response` WHERE `customerID` ='".$_SESSION['loggedInUser']['userID']."'");


if(mysql_num_rows($balanceQuery) >0){
while($result =mysql_fetch_array($balanceQuery)){
echo '<option value="'.$result['SAN'].'">'.$result['card_issue_currency'].'</option>';	
}

}else{

}

?>


</select>


</div>


<div class="field_wrapper">

<button id="balance_inquiry_btn" class="submit_btn">Balance Inquiry</button>
</div>


<div class="field_wrapper">
<label class="form_label rate_info_label bal_in_label">Your available balance is:</label>

<span id="numfo" class="rate_info bal_in_value" ></span>

<!--<br><br><span class="cant_see"><a href="#" style="font-size:13px">Click here to top up your card</a></span>-->
</div>
<br>





</div>

<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password margin_less">
<p><b>The rate is refreshed every 60 seconds</b><br>
If you are happy with the exchange rate click accept quote.
The rate you see is the rate you get.
</p>
</div>-->
</div>
<input type="hidden" id="customerID" name="customerID" value="<?=$_SESSION['loggedInUser']['userID']?>" />

<p class="bottom_info"></p>
</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>

<script type="text/javascript">
$("#balance_inquiry_btn").click(function(){


var currency = $("#receivingCurrency").val();

$.ajax({

url : 'ajaxCalls.php',
method: 'POST',
async : false,
data : {
rtypeMode: 'prepaidBalanceInquiry',
san:currency,
customerID: $("#customerID").val()
},
success: function(data){

balanceInquiryResponse = data.split(',');
var balance = balanceInquiryResponse[0];

var balanceTwoDecimal = parseFloat(balance).toFixed(2);
if(balanceInquiryResponse[2] == 0)
{
$('#numfo').html(balanceTwoDecimal+' '+balanceInquiryResponse[1]);
}


}

});
})
</script>
</html>
