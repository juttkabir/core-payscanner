<?php
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}

$strUserId = $_SESSION['loggedInUser']['userID'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Contact us</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
		<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
        	<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
        <script type="text/javascript" src="../admin/javascript/jquery.maskedinput.min.js"></script>
	</head>
	<body>

<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://payscanner.com"><img id="logo" src="images/logo_payscanner.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link " target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link menu_selected" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Contact us</h2>

<?php include 'top-menu.php';?>
<!-- content area -->

<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading font_style">Contact Us</p>
<p class="content_subheading font_style">If you have any questions about your account or any aspect of the Payscanner Online system please get in touch and we will assist you.  </p>
</div>
<!-- content area left-->
<div class="content_area_left">
<p class="contact_desc font_style" style="font-size:16px;">Talk to us</p>

<div class="field_wrapper">
<p class="contact_head font_style">Call our London Office on</p>
<p class="contact_desc font_style">+44 20 7097 8628</p>
</div>

<!--
<div class="field_wrapper">
<p class="contact_head font_style">Call our Algarve,Portugal Office on</p>
<p class="contact_desc font_style">+351 289 358 511</p>
</div>

<div class="field_wrapper">
<p class="contact_head font_style">Call our Mallorca, Spain Office on</p>
<p class="contact_desc font_style">+34 971 576 724</p>
</div>

<!--<div class="field_wrapper">
<p class="contact_head font_style">Call our UAE Office on</p>
<p class="contact_desc font_style">+971044394299</p>
</div>-->




<div class="field_wrapper">
<p class="contact_desc font_style">Email Us</p>
<p class="contact_head font_style">hello@payscanner.com</p>
</div>

<!--<div class="field_wrapper">
<p class="contact_desc font_style">Fax us</p>
<p class="contact_head font_style">+351 289 358 513</p>
</div>-->


</div>

<!-- content area right-->
<div class="content_area_right" style="margin:27px;">

<p class="contact_desc font_style" style="font-size:16px;">Office addresses</p>


<div class="field_wrapper">
<div class="media">
      <div class="media-left">
        <a href="#">
          <img class="media-object" data-src="holder.js/64x64" alt="55x55" src="images/uk_flag.png" >
        </a>
      </div>
      <div class="media-body contact_head">
        <h4 class="media-heading contact_desc font_style">Office</h4>
      PAYscanner.
      85 Great Portland Street, London,
      England,
      W1W 7LT
      </div>
    </div>
</div>

<!--
<div class="field_wrapper">
<div class="media">
      <div class="media-left">
        <a href="#">
          <img class="media-object" data-src="holder.js/64x64" alt="55x55" src="images/purtgal_flag.png" >
        </a>
      </div>
      <div class="media-body contact_head">
        <h4 class="media-heading contact_desc font_style">Portugal Office</h4>
      Premier FX Rua Sacadura Cabral Edificio Golfe 1a Almancil 8135 144 Algarve Portugal
      </div>
    </div>
</div>

<div class="field_wrapper">
<div class="media">
      <div class="media-left">
        <a href="#">
          <img class="media-object" data-src="holder.js/64x64" alt="55x55" src="images/spain_flag.png" >
        </a>
      </div>
      <div class="media-body contact_head">
        <h4 class="media-heading contact_desc font_style">Spain Office</h4>
     Premier FX C/Rambla Dels Duvs 13 - 107003 Palma de Mallorca 07014 Mallorca Spain
      </div>
    </div>
</div>
-->


<!--<div class="field_wrapper">
<div class="media">
      <div class="media-left">
        <a href="#">
          <img class="media-object" data-src="holder.js/64x64" alt="55x55" src="images/icon-flag-uae.png" >
        </a>
      </div>
      <div class="media-body contact_head">
        <h4 class="media-heading contact_desc font_style">UAE Office</h4>
     Premier FX Boulevard Plaza Tower 1 Sh Mohammed Bin Rashid Boulevard Downtown Dubai PO BOX 334036 Dubai UAE
      </div>
    </div>
</div>-->



</div>

</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include 'footer.php';?>
</div>



</div>
</body>
				</html>
