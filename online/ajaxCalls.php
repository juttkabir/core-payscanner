<?  
include("includes/configs.php");

$date_time = date('d-m-Y  h:i:s A');
/**
*	Ajax Request Page
*   To manage ajax requests
*	Imran Naqvi
**/


//include("security.php"); 
include_once ("ajaxOnlineFunctions.php");

$reqArr		= 	$_REQUEST;
$reqType	=	$_REQUEST["rtypeMode"];

switch($reqType){
 
case "getExchangeRate":
			$returnValue = getExchangeRate($reqArr);
			break;
case "prepaidBalanceInquiry":
			$returnValue = balanceInquiry($reqArr);
			break;
case "IssueCardRequest":
			$returnValue = IssueCardRequest($reqArr);
			break;
case "amountLimitPerMonth":
			$returnValue = amountLimitPerMonth($reqArr);
			break;
case "loadBalance":
			$returnValue = loadBalance($reqArr);
			break;
case "dailyTransLimit":
			$returnValue = dailyTransLimit($reqArr);
			break;
			
			case "getCurrencyWithLoad":
			$returnValue = getCurrencyWithLoad($reqArr);
			break;
			
			case "cardIssuedSender":
			$returnValue = cardIssuedSender($reqArr);
			break;
			
case "emailTopup":
			$returnValue = emailTopup($reqArr);
			break;
  
  case "getSan": 
 	$returnValue = getSan($reqArr); 
 	break; 
 	 
 	case "saveSan": 
 	$returnValue = saveSan($reqArr); 
 	break;
	
	
default: 
			$returnValue = NULL;
			break;

}


	exit;
?>	