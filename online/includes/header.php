<div id="header">
	<img src="images/logo.png" alt="PremierFX"/>
	<div class="row">
		<ul id="menu">
			<li <?php if($currentPage == 'send money') echo "class='current'"; ?>><a href="make_payment-i.php">Send Money</a></li>
			<li <?php if($currentPage == 'transaction history') echo "class='current'"; ?>><a href="transaction_history.php">Transaction History</a></li>
			<li <?php if($currentPage == 'view beneficiaries') echo "class='current'"; ?>><a href="view_beneficiaries.php">Beneficiaries</a></li>
			<li <?php if($currentPage == 'change password') echo "class='current'"; ?>><a href="change_password.php">Change Password</a></li>
			<li <?php if($currentPage == 'FAQs') echo "class='current'"; ?>><a href="faqs.php">FAQs</a></li>
		</ul>
	</div>
	<div class="row">
		<div class="left">
			<div>
				<label>Email:</label>
				<span><?php echo $_SESSION['loggedInUser']['email']; ?></span>
			</div>
			<div>
				<label>Customer Reference Number:</label>
				<span><?php echo $_SESSION['loggedInUser']['accountName'];?></span>
			</div>
		</div>
		<div class="right">
			<input type="button" value="Logout" id="logout" class="logout btn simple-btn input-medium" onClick="window.location.href = 'logout.php'"/>
		</div>
	</div>
</div>
