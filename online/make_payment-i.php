<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Send Transaction
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) || empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	//debug($_SESSION);
	//debug($_REQUEST);
	if(!empty($_REQUEST['ben'])){
		$_SESSION['ben'] = trim($_REQUEST['ben']);
		$strBenCont = selectFrom("SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName, bd.accountNo, bd.branchName, bd.branchAddress, bd.routingNumber FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName where ben.benID = '".$_SESSION['ben']."' ");
		$strBenCountryId = $strBenCont["countryId"];
		$arrBenBankRules = benBankDetailRule($strBenCountryId);
		$strBenName = "Name: ".$strBenCont["benName"];
		$accountName = $arrBenBankRules["accountName"];
		$accNo = $arrBenBankRules["accNo"];
		$branchNameNumber = $arrBenBankRules["branchNameNumber"];
		$branchAddress = $arrBenBankRules["branchAddress"];
		$swiftCode = $arrBenBankRules["swiftCode"];
		$IBAN = $arrBenBankRules["iban"];
		$routingNumber = $arrBenBankRules["routingNumber"];
		$sortCode = $arrBenBankRules["sortCode"];
		$strBenDetailFirst = "";
		$strBenDetailSecond = "";

			
			if($accountName == "Y")
			{
				
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Bank Name: ".$strBenCont["bankName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Bank Name: ".$strBenCont["bankName"];
					
				}
			}
			if($accNo == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Account No: ".$strBenCont["accountNo"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Account No: ".$strBenCont["accountNo"];
					
				}
			}
			if($branchNameNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Name: ".$strBenCont["branchName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Name: ".$strBenCont["branchName"];
					
				}
			}
			if($branchAddress == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Address: ".$strBenCont["branchAddress"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Address: ".$strBenCont["branchAddress"];
					
				}
			}
			if($swiftCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Swift Code: ".$strBenCont["swiftCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Swift Code: ".$strBenCont["swiftCode"];
					
				}
			}
			if($IBAN == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "IBAN: ".$strBenCont["IBAN"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "IBAN: ".$strBenCont["IBAN"];
					
				}
			}
			if($routingNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Routing Number: ".$strBenCont["routingNumber"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Routing Number: ".$strBenCont["routingNumber"];
					
				}
			}
			if($sortCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Sort Code: ".$strBenCont["sortCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Sort Code: ".$strBenCont["sortCode"];
					
				}
			}
			
			$_SESSION["benBame"] = $strBenName;
			$_SESSION["firstBen"] = $strBenDetailFirst;
			$_SESSION["secondBen"] = $strBenDetailSecond;
			
			
			
		
	}
	else
	{
			unset($_SESSION["benBame"]);
			unset($_SESSION["firstBen"]);
			unset($_SESSION["secondBen"]);
			unset($_SESSION['ben']);
		
	}
	$intChangeTrans = false;
	$intReject = false;
	if(isset($_GET['reject'])){
		$intReject = true;
	}
	if(!empty($_REQUEST['changeTransaction']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') && (!empty($_REQUEST['sendingAmount']) && $_REQUEST['sendingAmount'] > '0') && !empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency'])){
		$intChangeTrans = true;
		$strReceivingAmount = trim($_REQUEST['receivingAmount']);
		$strSendingAmount = trim($_REQUEST['sendingAmount']);
		$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
		$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	}
	$strQueryName = "SELECT CONCAT(firstName, ' ', middleName, ' ', lastName) AS customerName FROM ".TBL_CUSTOMER." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' ";
	$strCustomer = selectFrom($strQueryName);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Send Money Page</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
		<style type="text/css">
			#content{
				padding-left:0px;
			}
			#container{
				position:relative;
			}
			#imgContainer{
				position:absolute;
				right:15px;
				top:72px;
				
				
				z-index:800;
			}
		</style>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				$currentPage = 'send money';
				require_once "includes/header.php";
			?>
			<div id="container" >
				<div id="imgContainer"><img src="images/dollar_big.png" alt="some_text"> </div>
				<div id="content" class="pull-left">
					
					<h1 class="cont_head">Welcome <?php echo ucwords($strCustomer['customerName']); ?></h1>
					<fieldset class="inner_highlight short">
						<legend>Get a Spot Quote</legend>
						<h2 style="text-align:justify;text-justify:inter-word;">Send money safely and securely with PFX online. You can send any amount upto &pound;30,000 (or currency equivalent). There are no transfer fees or commissions. Please contact us on 0845 021 2370 if you require assistance.</h2><br />
						<h3>Note: Enter the amount of the sell currency or enter the amount of the buy currency</h3><br />
						<form action="make_payment-ii.php" method="get" id="getRate">
							<table class="table">
								<tr>
									<td class="bold">
										<label>CLIENT SELLS <em>*</em> :<br />(Currency you will pay with)</label>
									</td>
									<td>
										<select name="sendingCurrency" id="sendingCurrency" type="text" class="input-medium">
											<option value="">- Select Currency -</option>
											<?php 
												//$strQueryCurrency = "SELECT currencyName, baseCurrency FROM ".TBL_CURRENCY." INNER JOIN ".TBL_ONLINE_EXCHANGE_RATES." ON cID = quotedCurrency GROUP BY currencyName ORDER BY currencyName ASC";
												//$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_FX_RATES_MARGIN." ORDER BY quotedCurrency ASC";
												$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";			
//debug($strQueryCurrency);
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
										</select>
									</td>
									<td class="bold" style="text-align:right;padding-top:12px">
										Amount
									</td>
									<td>
										<input name="sendingAmount" id="sendingAmount" type="text" class="input-mini" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<td class="bold" width="">
										<label>CLIENT BUYS <em>*</em> :<br />(Currency you wish to purchase)</label>
									</td>
									<td width="155px">
										<select name="receivingCurrency" id="receivingCurrency" class="input-medium">
											<option value="">- Select currency -</option>
											<?php
												$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";
												//$strQueryCurrency = "SELECT currencyName, quotedCurrency FROM ".TBL_CURRENCY." INNER JOIN ".TBL_ONLINE_EXCHANGE_RATES." ON cID = quotedCurrency GROUP BY currencyName ORDER BY currencyName ASC";
												//$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_FX_RATES_MARGIN." ORDER BY quotedCurrency ASC";
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
										</select>
									</td>
									<td class="bold" style="text-align:right;padding-top:12px">
										Amount
									</td>
									<td>
										<input name="receivingAmount" id="receivingAmount" type="text" class="input-mini" maxlength="20"/>
									</td>
								</tr>
								
								<?php 
									if(isset($_SESSION['exchangeRateError']))
										$strError = "class='error align_center'";
								?>
								<tr <?php echo $strError; ?>>
									<td colspan="3" <?php echo $strError; ?> id="error">
										<?php 
											if(isset($_SESSION['exchangeRateError'])){
												echo $_SESSION['exchangeRateError'];
												unset($_SESSION['exchangeRateError']);
											}else{
												echo "&nbsp;";
											}
										?>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="align_right">
										<a href="#" class="tip" id="getRateTip" data-placement="above" rel="popover" data-content="Click the get rate button to get the rate according to the currency selection" title="Get Rate Button">
											[?]
										</a>
										</td>
										</tr>
										<input name="getRate" id="getRate" class="btn actv_btn" type="submit" value="Get Rate"/>
										<input name="clear" id="clear" type="reset" class="btn simple-btn" onclick="clearAll();" value="Clear"/>
									</td>
								</tr>
								
							</table>
						
						</fieldset>
					</form>
					
				</div>
				<!--<div id="sidebar">
                    <div class="step_head">
                        <h2>STEP 1 TRANSACTION DETAILS</h2>
                    </div>
                    <p class="inner_step">&nbsp;</p>
                    <p class="inner_step">&nbsp;</p>
                    <p class="inner_step">&nbsp;</p>
                    <div class="step_head">
                        <h2>STEP 2 BENEFICIARY DETAILS</h2>
                    </div>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["benBame"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["firstBen"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["secondBen"]; ?></p>
                    <div class="step_head">
                        <h2>STEP 3 PAYMENT DETAILS</h2>
                    </div>
                    <p class="inner_step">&nbsp;</p>
                    <p class="inner_step">&nbsp;</p>
                    <p class="inner_step">&nbsp;</p>
                </div>-->
			</div>
			<?php 
				include_once "includes/footer.php";
			?>
		</div>
		<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			
			function checkAmounts(){
				sendingAmount = $.trim($('#sendingAmount').val());
				receivingAmount = $.trim($('#receivingAmount').val());
				//receivingAmount = parseFloat(receivingAmount).toFixed(2);
				//sendingAmount = parseFloat(sendingAmount).toFixed(2);
				if(receivingAmount == '' && sendingAmount != ''){
					$('#receivingAmount').attr('disabled', 'disabled');
					return;
				}else if(receivingAmount != '' && sendingAmount == ''){
					$('#sendingAmount').attr('disabled', 'disabled');
					return;
				}
			}
			
			function clearAll(){
				$('#receivingAmount').removeAttr('disabled');
				$('#sendingAmount').removeAttr('disabled');
				
			}
			
			<?php 
				if($intChangeTrans){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $strReceivingCurrency; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $strSendingCurrency; ?>');
				$('#sendingAmount').focus(
					function(){
						$('#receivingAmount').attr('disabled', 'disabled');
					}
				);
				$('#receivingAmount').focus(
					function(){
						$('#sendingAmount').attr('disabled', 'disabled');
					}
				);
			<?php 
				}elseif((isset($_SESSION['sendingCurrency']) || isset($_SESSION['receivingCurrency'])) && !$intReject){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $_SESSION['receivingCurrency']; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $_SESSION['sendingCurrency']; ?>');
			<?php 
				unset($_SESSION['receivingCurrency']);
				unset($_SESSION['sendingCurrency']);
				}
			?>
			$('#getRateTip').popover({
				trigger: 'hover',
				offset: 5
			});
			
			$('#sendingAmount').blur(
				function(){
					sendingAmount = $.trim($(this).val());
					if(sendingAmount != ''){
						//$('#receivingAmount').attr('readonly', 'readonly');
						$('#receivingAmount').attr('disabled', 'disabled');
						//$(this).removeAttr('readonly');
						$(this).removeAttr('disabled');
					}else{
						//$('#receivingAmount').removeAttr('readonly');
						$('#receivingAmount').removeAttr('disabled');
					}
				}
			);
			
			$('#receivingAmount').blur(
				function(){
					receivingAmount = $.trim($(this).val());
					if(receivingAmount != ''){
						receivingAmount = parseFloat(receivingAmount).toFixed(2);
						if(receivingAmount < 0){
							$('#error').addClass('error');
							$('#error').parent().addClass('error');
							$('#error').addClass('align_center');
							$('#error').text('Receiveing amount cannot be negative.');
						}else{
							$('#error').removeClass('error');
							$('#error').parent().removeClass('error');
							$('#error').text('');
							//$('#sendingAmount').attr('readonly', 'readonly');
							$('#sendingAmount').attr('disabled', 'disabled');
							//$(this).removeAttr('readonly');
							$(this).removeAttr('disabled');
						}
					}else{
						//$('#sendingAmount').removeAttr('readonly');
						$('#sendingAmount').removeAttr('disabled');
					}
				}
			);
			<?php 
				if($intChangeTrans){
					echo "$('#sendingAmount').val('".round(floor($strSendingAmount * 100) / 100,2)."');"; 
					echo "$('#receivingAmount').val('".round(floor($strReceivingAmount * 100) / 100,2)."');"; 
				}elseif((isset($_SESSION['sendingAmount']) || isset($_SESSION['receivingAmount'])) && !$intReject){
					if(isset($_SESSION['sendingAmount'])){
						echo "$('#receivingAmount').val('".number_format($_SESSION['sendingAmount'],2,".",",")."');";
						unset($_SESSION['sendingAmount']);
					}
					if(isset($_SESSION['receivingAmount'])){
						echo "$('#receivingAmount').val('".round(floor($_SESSION['receivingAmount'] * 100) / 100,2)."');";
						unset($_SESSION['receivingAmount']);
					}
				}
			?>
			$('#getRate').validate({
				rules: {
					receivingCurrency: {
						required: true
					},
					receivingAmount: {
						required: function(){
							sendingAmount = $.trim($('#sendingAmount').val());
							if(sendingAmount == '')
								return true;
							else
								return false;
						},
						number: true
					},
					sendingAmount: {
						required: function(){
							receivingAmount = $.trim($('#receivingAmount').val());
							if(receivingAmount == '')
								return true;
							else
								return false;
						},
						number: true
					},
					sendingCurrency: {
						required: true
					}
				},
				messages: {
					sendingAmount: {
						required: 'Enter amount to sell.',
						number: 'Amount can only be numbers'
					},
					receivingAmount: {
						required: 'Enter amount to buy.',
						number: 'Amount can only be numbers'
					}
				}
			});
		</script>
	</body>
</html>