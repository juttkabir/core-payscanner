<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: view beneficiaries
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	$limit = 10;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	else
		$offset = 0;
	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	
	
			if(($_REQUEST["benCountry"]=='')&&($_REQUEST["benName"]!=''))
			{
			$name=$_REQUEST["benName"];
			$query=" (firstName like '%".$name."%' OR middleName like '%".$name."%' AND lastName like '%".$name."%')AND ";
			}else if(($_REQUEST["benCountry"]!='')&&($_REQUEST["benName"]==''))
			{
			$strbenCountry=$_REQUEST["benCountry"];
			$query=" (Country like '%".$strbenCountry."%')AND ";
			}else if(($_REQUEST["benCountry"]!='')&&($_REQUEST["benName"]!=''))
			{
			$name=$_REQUEST["benName"];
			$strbenCountry=$_REQUEST["benCountry"];
			$query=" (firstName like '%".$name."%' OR middleName like '%".$name."%' AND lastName like '%".$name."%')AND (Country like '%".$strbenCountry."%') AND ";
			}else{
			$query="";
			}
	

	/**Ticket 10651**/
	//$strQueryTotalBens = "SELECT COUNT(benID) AS records FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
	$strQueryTotalBens = " SELECT   COUNT(benID)AS records FROM ".TBL_BENEFICIARY." WHERE  ".$query." `customerID` = '".$_SESSION['loggedInUser']['userID']."'  AND status = 'Enabled'";
	//debug($strQueryTotalBens);
	/* $strQueryBen = "SELECT benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' ORDER BY benID DESC status = 'Enabled' LIMIT $offset, $limit";
	*/
	
	$strQueryBen=" SELECT  benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE  ".$query." `customerID` = '".$_SESSION['loggedInUser']['userID']."'  AND status = 'Enabled' ORDER BY `benID`DESC LIMIT ".$offset." , ".$limit." ";
	//debug($strQueryBen);
	
	$arrTotalBens = selectFrom($strQueryTotalBens);
	
	$arrBens = selectMultiRecords($strQueryBen);
	$intTotalBens = count($arrBens);
	$intAllBens = $arrTotalBens['records'];
	//debug($arrBens);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>View Beneficiaries</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				$currentPage = 'view beneficiaries';
				require_once "includes/header.php";
				
				
			?>
						
			<div id="container" >
				<div id="content_one">
				<div>
				<span id="cont_head" class="cont_head" >Search Beneficiary</span>
				<form action="" name="benSearch" method="post">
				<table border="1"   style=" margin:0 auto;" width="34%">
				<tr><td ></td><td>&nbsp;</td></tr>
				
				<tr><td id="search_b_n" class="searchBenFileter" >Search by Name:</td>
				<td  align="left"><input  type="text" value="<?=$name?>"  class="input-medium" name="benName" id="benName" />
				</td></tr>
				<tr><td id="search_b_c" class="searchBenFileter">Search by Country:</td>
				<td  align="left"><select id="benCountry"  style="width: 164px;" class="input-medium" name="benCountry" >
				<option value="">-Select Country-</option>
				<?php $strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
												$arrCountry = selectMultiRecords($strQueryCountries);
												for($j = 0;$j < count($arrCountry);$j++)
													echo "<option value='".$arrCountry[$j]['countryName']."'>".$arrCountry[$j]['countryName']."</option>";
											?>
				
				
				</select></td>
				</tr>
				<tr><td colspan="2"></td></tr>
				<tr><td colspan="2" style=" text-align:right"><center><input id="searchBen" class="= btn simple-btn input-medium" type="submit" value="Search"></center></td>
				<td></td>
				
				</tr>
				
				</form><table></table>
				</div>
					<div class="row align_right">
						<a href="javascript:void(0)" class="tip" id="addBenTip" data-placement="above" rel="popover" data-content="Click new button to add new beneficiary" title="Add new beneficiary">
							[?]
						</a>
						<input id="newBeneficiry" value="New" class="btn actv_btn input-small" type="button" onClick="createBen();"/>
					</div>
					
					<div class="table_list">
						<?php 
							if($intTotalBens > 0){
								
						?>
						<table class="table">
							<tr>
								<td>Showing <?php echo ($offset+1)."-".($intTotalBens + $offset)." of ".$intAllBens; ?></td>
								<td class="align_right">
									<?php 
										if($prev >= 0){
									?>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0">First</a>&nbsp;
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev; ?>">Previous</a>&nbsp;
									<?php 
										}
										if($next > 0 && $next < $intAllBens){
											$intLastOffset = (ceil($intAllBens / $limit) - 1) * $limit;
									?>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>">Next</a>&nbsp;
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset; ?>">Last</a>
									<?php 
										}
									?>
								</td>
							</tr>
						</table>
						
						<table class="table">
							<tr>
								<th>Creation Date</th>
								<th>Name</th>
								<th>Country</th>
								<th colspan="2" class="align_center">Operations</th>
							</tr>
							<?php 
							
								for($x = 0;$x < $intTotalBens;$x++){
							
							
							?>
							<tr>
								<td><?php echo $arrBens[$x]['created'] ?></td>
								<td><?php echo $arrBens[$x]['name']; ?></td>
								<td><?php echo $arrBens[$x]['Country'] ?></td>
								<td>
									<form action="" method="post">
										<a href="javascript:void(0);" onClick="document.forms[<?php echo $x+1; ?>].action='add_beneficiary.php';document.forms[<?php echo $x+1; ?>].submit();">Edit</a>
										<input name="ben" type="hidden" value="<?php echo $arrBens[$x]['benID']?>"/>
								</td>
								<td>
										<a href="javascript:void(0);" onClick="document.forms[<?php echo $x+1; ?>].action='make_payment-i.php';document.forms[<?php echo $x+1; ?>].submit();">Create Transaction</a>
									</form>
								</td>
							</tr>
							<?php 
								}
							?>
						</table>
						<?php 
							}else{
						?>
						<table class="table">
							<tr class="error">
								<td class="align_center">There are no beneficiaries to display.</td>
							</tr>
						</table>
						<?php 
							}
						?>
					</div>
				</div>
			</div>
			<?php 
				require_once "includes/footer.php";
			?>
		</div>
		<script type="text/javascript">
		
		function changeMySize(myvalue, iden){
		if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			var content = document.getElementById("content_one");
			content.style.fontSize=myvalue + "px";
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			var content = document.getElementById("content_one");
			content.style.fontSize=myvalue + "px";
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			var content = document.getElementById("content_one");
			content.style.fontSize=20 + "px";
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
			var ch = document.getElementById("cont_head");
			var sbn = document.getElementById("search_b_n");
			var sbc= document.getElementById("search_b_c");
			var lsm1 = document.getElementById("link_send_money");
		var lt1 = document.getElementById("link_tran");
		var lb1 = document.getElementById("link_benif");
		var lcp1 = document.getElementById("link_changeP");
		var lf1 = document.getElementById("link_faq");
		var de1 = document.getElementById("div_email");
		var des1 = document.getElementById("div_email_ses");
		var dcr1 = document.getElementById("div_cust_ref");
		var dcrs1 = document.getElementById("div_cust_ref_ses");
			
			ch.style.fontSize = myvalue + "px";
			sbn.style.fontSize = myvalue + "px";
			sbc.style.fontSize = myvalue + "px";
			/*lt1.style.fontSize = myvalue + "px";
		lb1.style.fontSize = myvalue + "px";
		lcp1.style.fontSize = myvalue + "px";
		lf1.style.fontSize = myvalue + "px";
		lsm1.style.fontSize = myvalue + "px";
		de1.style.fontSize = myvalue + "px";
		des1.style.fontSize = myvalue + "px";
		dcr1.style.fontSize = myvalue + "px";
		dcrs1.style.fontSize = myvalue + "px";*/
		
		}
			function createBen(){
				window.location = 'add_beneficiary.php';
			}
			$('#addBenTip').popover({
				trigger: 'hover',
				offset: 10,
				delayOut: 10
			});
		</script>
	</body>
</html>