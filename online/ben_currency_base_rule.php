<?php 
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	//die($_POST['currency']);
	if(!empty($_POST['currency']))
	{
	$currency = $_POST['currency'];
	
	$strRule = benBankCurrencyDetailRule($currency);
	$strRule = json_encode($strRule);
	} 
	echo $strRule;
?>