<div class="footer_area">
<div class="upper_footer">
<div class="upper_footer_wrapper">
<div class="footer_left">
<!--<a href="http://www.premfx.com"><img src="images/logo_png.png"></a>-->
<!--<a href="http://demofx.hbstech.co.uk"><img src="images/logo_hbs.jpg"></a>-->
<a href="http://payscanner.com"><img src="../admin/images/spi_payscanner_com/logo.png"></a>

</div>
<div class="footer_right">
<div class="footer_social_wrapper">
<a href="https://www.linkedin.com/in/pay-scanner-8a9772161/"><img class="social_logos" src="images/in_logo.png"></a>
<a href="#"><img class="social_logos" src="images/fb_logo.png"></a>
<!--<a href="https://www.facebook.com/premier.fx"><img class="social_logos" src="images/logo_hbs.jpg"></a>-->
</div>
</div>
</div>
</div>
<div class="lower_footer">
<div class="lower_footer_wrapper">
<div class="footer_left">
<span class="footer_address">Payscanner Online is a registered trademark of Payscanner Ltd.
  Copyright (c) <?php echo date("Y"); ?> Payscanner Ltd. All rights reserved.</span>
</div>
<div class="footer_right">
<ul class="footer_menu">
<li class="footer_menu_list"><a href="#">Terms & Conditions</a></li><span class="footer_sep">|</span>
<li class="footer_menu_list"><a href="#">Privacy Policy</a></li><span class="footer_sep">|</span>
<li class="footer_menu_list"><a href="#">Cookie Policy</a></li>
</ul>
</div>

</div>
</div>
</div>