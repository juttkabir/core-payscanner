<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Send Transaction
	 * @author: M.Rizwan
	 */
	 $lifetime=60000;
  session_start();
  setcookie(session_name(),session_id(),time()+$lifetime);
	
	if(!isset($_SESSION['loggedInUser']['userID']) || empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	//debug($_SESSION);
	//print_r($_SESSION);
	//debug($_SESSION['loggedInUser']['userID']);  
	
	$strQueryCustomer = "select * from user_id_types where user_id = '".$_SESSION['loggedInUser']['userID']."'";
	$strAccountName = selectMultiRecords($strQueryCustomer);
	//debug($strQueryCustomer);
	$strCountRecords = count($strAccountName);
	//debug($strCountRecords);
	//debug($strAccountName[0]["expiry_date"]);
	$TodayDate  = date('Y-m-d');   
	//debug($TodayDate);
	
	//debug($_SESSION);
		if($strCountRecords > 1){
		for($i=1;$i < $strCountRecords;$i++){
		
			if($strAccountName[$i]["expiry_date"] == $TodayDate || $strAccountName[$i]["expiry_date"] < $TodayDate){
			
			$var = 1;
		//	debug($var);
			
			}
		}
	}
	
	
	if(!empty($_REQUEST['ben'])){
		$_SESSION['ben'] = trim($_REQUEST['ben']);
		$strBenCont = selectFrom("SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName, bd.accountNo, bd.branchName, bd.branchAddress, bd.routingNumber FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName where ben.benID = '".$_SESSION['ben']."' ");
		$strBenCountryId = $strBenCont["countryId"];
		$arrBenBankRules = benBankDetailRule($strBenCountryId);
		$strBenName = "Name: ".$strBenCont["benName"];
		$accountName = $arrBenBankRules["accountName"];
		$accNo = $arrBenBankRules["accNo"];
		$branchNameNumber = $arrBenBankRules["branchNameNumber"];
		$branchAddress = $arrBenBankRules["branchAddress"];
		$swiftCode = $arrBenBankRules["swiftCode"];
		$IBAN = $arrBenBankRules["iban"];
		$routingNumber = $arrBenBankRules["routingNumber"];
		$sortCode = $arrBenBankRules["sortCode"];
		$strBenDetailFirst = "";
		$strBenDetailSecond = "";

			
			if($accountName == "Y")
			{
				
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Bank Name: ".$strBenCont["bankName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Bank Name: ".$strBenCont["bankName"];
					
				}
			}
			if($accNo == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Account No: ".$strBenCont["accountNo"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Account No: ".$strBenCont["accountNo"];
					
				}
			}
			if($branchNameNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Name: ".$strBenCont["branchName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Name: ".$strBenCont["branchName"];
					
				}
			}
			if($branchAddress == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Address: ".$strBenCont["branchAddress"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Address: ".$strBenCont["branchAddress"];
					
				}
			}
			if($swiftCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Swift Code: ".$strBenCont["swiftCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Swift Code: ".$strBenCont["swiftCode"];
					
				}
			}
			if($IBAN == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "IBAN: ".$strBenCont["IBAN"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "IBAN: ".$strBenCont["IBAN"];
					
				}
			}
			if($routingNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Routing Number: ".$strBenCont["routingNumber"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Routing Number: ".$strBenCont["routingNumber"];
					
				}
			}
			if($sortCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Sort Code: ".$strBenCont["sortCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Sort Code: ".$strBenCont["sortCode"];
					
				}
			}
			
			$_SESSION["benBame"] = $strBenName;
			$_SESSION["firstBen"] = $strBenDetailFirst;
			$_SESSION["secondBen"] = $strBenDetailSecond;
		
	}
	else
	{
			unset($_SESSION["benBame"]);
			unset($_SESSION["firstBen"]);
			unset($_SESSION["secondBen"]);
			unset($_SESSION['ben']);
		 
	}
	$intChangeTrans = false;
	$intReject = false;
	if(isset($_GET['reject'])){
		$intReject = true;
	}
	if(!empty($_REQUEST['changeTransaction']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') && (!empty($_REQUEST['sendingAmount']) && $_REQUEST['sendingAmount'] > '0') && !empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency'])){
		$intChangeTrans = true;
		$strReceivingAmount = trim($_REQUEST['receivingAmount']);
		$strSendingAmount = trim($_REQUEST['sendingAmount']);
		$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
		$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	}
	 
	$strQueryName = "SELECT CONCAT(firstName, ' ', middleName, ' ', lastName) AS customerName FROM ".TBL_CUSTOMER." WHERE accountName = '".$_SESSION['loggedInUser']['userID']."' ";
	$strCustomer = selectFrom($strQueryName);
	if(empty($_REQUEST['reject'])){
	 
	$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
	$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	
	$fltExchangeRate = $_REQUEST["fltExchangeRate"];
	if($fltExchangeRate == ""){
	$fltExchangeRate= $_REQUEST["exchangeRate"];
	}
	if($_REQUEST["changeTransaction"] == "changeTransaction"){
	activities($_SESSION["loggedInUser"]["userID"],"Reject Quote","","","Quote rejected for ".$strSendingCurrency." - ".$strReceivingCurrency." = ".$fltExchangeRate."");
	}
	}
	
	unset($_SESSION["accountName"]);
	unset($_SESSION["accountNumber"]);
	unset($_SESSION["branchNameNumber"]);
	unset($_SESSION["branchAddress"]);
	unset($_SESSION["routingNumber"]);
	unset($_SESSION["sortCode"]);
	unset($_SESSION["swift"]);
	unset($_SESSION["reference"]);
	unset($_SESSION["existBen"]);
	unset($_SESSION["iban"]);
	unset($_SESSION["benCountry"]);
	unset($_SESSION["benName"]);
	unset($_SESSION["benCurrency"]);
   
	?>

<!DOCTYPE HTML>
<html>
<head>
<title><?php echo CLIENT_NAME_ONLINE; ?></title>
<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
<meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="imagetoolbar" content="no">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/jquery.validate.js"></script>
<script type="text/javascript" src="javascript/date.js"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="javascript/ajaxupload.js"></script>
<script src="scripts/jquery-1.7.2.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap-twipsy.js"></script>
<script src="scripts/bootstrap-popover.js"></script>
<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
</head>
<body>
		
				
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://payscanner.com"><img id="logo" src="images/logo_payscanner.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link menu_selected" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link " target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Send Money</h2>
<?php include('top-menu.php');?>


<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Get a spot quote</p>
<p class="content_subheading">Enter the amount of currency you wish to sell, or the amount of currency you wish to buy. </p>
</div>
<!-- content area left-->
<div class="content_area_left modified_transaction_left">
 <form action="make_paymentnew.php" method="get" id="getRate">

<div class="field_wrapper">
<label class="form_label">Currency to sell:</label><br>

<select name="sendingCurrency" id="sendingCurrency" type="text" class="select_field">
                                <option value="">- Select Currency -</option>
                                <?php 
												
												/*$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";	*/
$strQueryCurrency = "(SELECT quotedCurrency AS currencies FROM   fx_rates_margin  where statusType = 'enable') UNION 
            (  SELECT baseCurrency AS currencies FROM   fx_rates_margin  where statusType = 'enable'  );";	   
//debug($strQueryCurrency);

												$arrCurrency = selectMultiRecords($strQueryCurrency);
												sort($arrCurrency);
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
                              </select>


</div>

<div class="field_wrapper">
<label class="form_label">Currency to buy:</label><br>
<select name="receivingCurrency" id="receivingCurrency" class="select_field">
                                <option value="">- Select currency -</option>
                                <?php
												/*$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";*/
	   $strQueryCurrency = "(SELECT quotedCurrency AS currencies FROM   fx_rates_margin  where statusType = 'enable') UNION 
            (  SELECT baseCurrency AS currencies FROM   fx_rates_margin  where statusType = 'enable');";
	    
												//$strQueryCurrency = "SELECT currencyName, quotedCurrency FROM ".TBL_CURRENCY." INNER JOIN ".TBL_ONLINE_EXCHANGE_RATES." ON cID = quotedCurrency GROUP BY currencyName ORDER BY currencyName ASC";
												//$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_FX_RATES_MARGIN." ORDER BY quotedCurrency ASC";
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												sort($arrCurrency);
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
                              </select>

</div>


<!--<div class="field_wrapper">
<input name="getRate" id="getRate" class="submit_btn" type="submit" value="Get Rate"/>
<input name="clear" id="clear" type="reset" class="clear_btn" onClick="clearAll();" value="Clear"/>
<a href="#" class="tip" id="getRateTip" data-placement="above" rel="popover" data-content="Click the get rate button to get the rate according to the currency selection" title="Get Rate Button"> [?] </a>
</div>-->


</div>

<!-- content area right-->
<div class="content_area_right modified_transaction_right">

<br>

<div class="field_wrapper ">
<label class="form_label">Amount:</label><br>

 <input name="sendingAmount" id="sendingAmount" type="text" maxlength="20" class="form_fields min_field" autocomplete="off"/>
</div>

<div class="field_wrapper ">
<label class="form_label">Amount:</label><br>

 <input name="receivingAmount" id="receivingAmount" type="text"  maxlength="20" class="form_fields min_field" autocomplete="off"/>
</div>



</div>

<div class="modified_transaction_center">
<div class="modified_info"><p class="modified_text">Send money safely and securely with Payscanner Online

You can send any amount from £100 up to £50,000 or currency equivalent.

There are NO transfer fees and NO commissions to pay.</p></div>
</div>

<div class="button_wrapper">
<input name="getRate" id="getRate" class="submit_btn" type="submit" value="Get Rate" style="width:130px;"/>
<input name="clear" id="clear" type="reset" class="clear_btn" onClick="clearAll();" value="Clear" style="width:130px;"/>
<a href="#" class="tip" id="getRateTip" data-placement="above" rel="popover" data-content="Click the get rate button to get the rate according to the currency selection" title="Get Rate Button"> [?] </a>

</div>

</form>


<p class="bottom_info">For amounts over £50,000 or if you have any other questions, please contact us +44 20 7097 8628</p>
 <?php 
  
				if(isset($_SESSION['exchangeRateError'])){
				$strError = "class='error align_center'";
								?>
					<? //if(!empty($_SESSION['exchangeRateError'])){?>
						<div class="error_msg">	
                       
         <?php 
	//$alert_val="'".$_SESSION['exchangeRateError1'].'You can only send amount from £100 up to £50,000 or currency equivalent.'.'\n'.'Your current amount in GBP = '.$_SESSION['totalAmountInGBP']."'";
		if($_SESSION['exchangeRateError'] == 'balance'){ 
 	                
                       echo "You are not allowed to create transaction due to insufficient balance. Please contact Payscanner support team."; 
                        
                        unset($_SESSION['totalAmountInGBP']); 
                        unset($_SESSION['exchangeRateError']); 
                     unset($_SESSION['re']); 
            } else if(isset($_SESSION['exchangeRateError']))
		{
			$AmountInGBP = number_format($_SESSION['totalAmountInGBP'],2);
			
			$alert_val="'".'You can only send amount from £100 up to £50,000 or currency equivalent.'.'\n'.'Your current amount in GBP = '.$AmountInGBP."'";
		
			if($_SESSION['re']=="N")
			{
				$alert_val="'".$_SESSION['exchangeRateError']."'";
			}
			//debug($_SESSION['exchangeRateError']);
			echo "<script>alert($alert_val);</script>";
			echo $_SESSION['exchangeRateError'];
			
			unset($_SESSION['totalAmountInGBP']);
			unset($_SESSION['exchangeRateError']);
			unset($_SESSION['re']);
			//unset($_SESSION['exchangeRateError1']);
			
		}
		else{
				echo "&nbsp;";
			}
		?>
		
		</div>
		<? }?>



</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>



<script>
$(document).ready(function(){
		
		
		ToadayDate = '<?php echo $TodayDate ;?>';
		ExpiryDate = '<?php echo $strAccountName[0]["expiry_date"] ;?>';
		VarMultiRecords = '<?php echo $var ;?>';
		//alert(ToadayDate);
		//alert(ExpiryDate);

  if(ExpiryDate == ToadayDate || ExpiryDate < ToadayDate || VarMultiRecords != ''){
   //  alert("OKKK");
    //alert("You have an expired ID, Please contact Premier FX support");
    var elem = document.getElementById("test");
     elem.style.display = "none";
	 alert("You have an expired ID, Please contact Payscanner support");
  }
  
});



</script>
<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			
			function checkAmounts(){
				sendingAmount = $.trim($('#sendingAmount').val());
				receivingAmount = $.trim($('#receivingAmount').val());
				//receivingAmount = parseFloat(receivingAmount).toFixed(2);
				//sendingAmount = parseFloat(sendingAmount).toFixed(2);
				if(receivingAmount == '' && sendingAmount != ''){
					$('#receivingAmount').attr('disabled', 'disabled');
					return;
				}else if(receivingAmount != '' && sendingAmount == ''){
					$('#sendingAmount').attr('disabled', 'disabled');
					return;
				}
			}
			
			function clearAll(){
				$('#receivingAmount').removeAttr('disabled');
				$('#sendingAmount').removeAttr('disabled');
				
			}
			
			<?php 
				if($intChangeTrans){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $strReceivingCurrency; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $strSendingCurrency; ?>');
				$('#sendingAmount').focus(
					function(){
						$('#receivingAmount').attr('disabled', 'disabled');
					}
				);
				$('#receivingAmount').focus(
					function(){
						$('#sendingAmount').attr('disabled', 'disabled');
					}
				);
			<?php 
				}elseif((isset($_SESSION['sendingCurrency']) || isset($_SESSION['receivingCurrency'])) && !$intReject){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $_SESSION['receivingCurrency']; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $_SESSION['sendingCurrency']; ?>');
			<?php 
				unset($_SESSION['receivingCurrency']);
				unset($_SESSION['sendingCurrency']);
				}
			?>
			$('#getRateTip').popover({
				trigger: 'hover',
				offset: 5
			});
			
			$('#sendingAmount').blur(
				function(){
					sendingAmount = $.trim($(this).val());
					if(sendingAmount != ''){
						//$('#receivingAmount').attr('readonly', 'readonly');
						$('#receivingAmount').attr('disabled', 'disabled');
						//$(this).removeAttr('readonly');
						$(this).removeAttr('disabled');
					}else{
						//$('#receivingAmount').removeAttr('readonly');
						$('#receivingAmount').removeAttr('disabled');
					}
				}
			);
			
			$('#receivingAmount').blur(
				function(){
					receivingAmount = $.trim($(this).val());
					if(receivingAmount != ''){
						receivingAmount = parseFloat(receivingAmount).toFixed(2);
						if(receivingAmount < 0){
							$('#error').addClass('error');
							$('#error').parent().addClass('error');
							$('#error').addClass('align_center');
							$('#error').text('Receiveing amount cannot be negative.');
						}else{
							$('#error').removeClass('error');
							$('#error').parent().removeClass('error');
							$('#error').text('');
							//$('#sendingAmount').attr('readonly', 'readonly');
							$('#sendingAmount').attr('disabled', 'disabled');
							//$(this).removeAttr('readonly');
							$(this).removeAttr('disabled');
						}
					}else{
						//$('#sendingAmount').removeAttr('readonly');
						$('#sendingAmount').removeAttr('disabled');
					}
				}
			);
			<?php 
				if($intChangeTrans){
					echo "$('#sendingAmount').val('".round(floor($strSendingAmount * 100) / 100,2)."');"; 
					echo "$('#receivingAmount').val('".round(floor($strReceivingAmount * 100) / 100,2)."');"; 
				}elseif((isset($_SESSION['sendingAmount']) || isset($_SESSION['receivingAmount'])) && !$intReject){
					if(isset($_SESSION['sendingAmount'])){
						echo "$('#receivingAmount').val('".number_format($_SESSION['sendingAmount'],2,".",",")."');";
						unset($_SESSION['sendingAmount']);
					}
					if(isset($_SESSION['receivingAmount'])){
						echo "$('#receivingAmount').val('".round(floor($_SESSION['receivingAmount'] * 100) / 100,2)."');";
						unset($_SESSION['receivingAmount']);
					}
				}
			?>
			$('#getRate').validate({
				rules: {
					receivingCurrency: {
						required: true
					},
					receivingAmount: {
						required: function(){
							sendingAmount = $.trim($('#sendingAmount').val());
							if(sendingAmount == '')
								return true;
							else
								return false;
						},
						number: true
					},
					sendingAmount: {
						required: function(){
							receivingAmount = $.trim($('#receivingAmount').val());
							if(receivingAmount == '')
								return true;
							else
								return false;
						},
						number: true
					},
					sendingCurrency: {
						required: true
					}
				},
				messages: {
					sendingAmount: {
						required: '<br><font style="color:#FF0000">Enter amount to sell.</font>',
						number: '<br><font style="color:#FF0000">Amount can only be numbers</font>'
					},
					receivingAmount: {
						required: '<br><font style="color:#FF0000">Enter amount to buy.</font>', 
						number: '<br><font style="color:#FF0000">Amount can only be numbers</font>'
					}
				}
			});
			
		</script>
		
</html>
