<?php 
	//ini_set("display_errors","On");
	if(isset($_POST['login']) && isset($_POST['email']) && isset($_POST['password'])){
		session_start();
		include_once "includes/configs.php";
		include_once "includes/database_connection.php";
		dbConnect();
		include_once "includes/functions.php";
		$strUserName = strtolower(trim($_POST['email']));
		$strCustomerNumber = strtolower(trim($_POST['customerNumber']));
		$strPassword = $_POST['password'];
		 if($_POST['customerNumber']=="103")
 {
			if((!empty($strUserName) || !empty($strCustomerNumber)) && !empty($strPassword)){
					$strQueryLoginChk = "SELECT customerID AS userID, customerName, email, accountName FROM ".TBL_CUSTOMER." WHERE ";
					$strCondition = '';
					if(!empty($strUserName))
						$strCondition .= "email = '{$_POST['email']}' AND ";
					
					if(!empty($strCustomerNumber))
						$strCondition .= "accountName = '{$_POST['customerNumber']}' AND ";
					
					if(!empty($strPassword))
						$strCondition .= " password = '{$_POST['password']}' AND ";
					
					$strCondition .= "customerStatus = 'Enable' AND parentID != '' LIMIT 1";
					$strQueryLoginChk .= $strCondition;
					//debug($strQueryLoginChk, true);
					$arrLoggedInUser = selectFrom($strQueryLoginChk);
					//debug($arrLoggedInUser, true);
					if(!empty($arrLoggedInUser['userID'])){
						$_SESSION['loggedInUser']['userID'] = $arrLoggedInUser['userID'];
						$_SESSION['loggedInUser']['email'] = $arrLoggedInUser['email'];
						$_SESSION['loggedInUser']['accountName'] = $arrLoggedInUser['accountName'];
						//genrateSession();
						header("LOCATION: make_payment-i.php");
					}else{
						$strError = "Username, customer number or password is not valid or customer is not active";
					}
				}else{
					$strError = "Username, customer number or password is not valid or customer is not active";
				}
			
 }
 else
 {
 
		if(!empty($_POST['letters_code']))
		{
			if($_POST['letters_code'] == $_SESSION['6_letters_code'])
			{
				if((!empty($strUserName) || !empty($strCustomerNumber)) && !empty($strPassword)){
					$strQueryLoginChk = "SELECT customerID AS userID, customerName, email, accountName FROM ".TBL_CUSTOMER." WHERE ";
					$strCondition = '';
					if(!empty($strUserName))
						$strCondition .= "email = '{$_POST['email']}' AND ";
					
					if(!empty($strCustomerNumber))
						$strCondition .= "accountName = '{$_POST['customerNumber']}' AND ";
					
					if(!empty($strPassword))
						$strCondition .= " password = '{$_POST['password']}' AND ";
					
					$strCondition .= "customerStatus = 'Enable' AND parentID != '' LIMIT 1";
					$strQueryLoginChk .= $strCondition;
					//debug($strQueryLoginChk, true);
					$arrLoggedInUser = selectFrom($strQueryLoginChk);
					//debug($arrLoggedInUser, true);
					if(!empty($arrLoggedInUser['userID'])){
						$_SESSION['loggedInUser']['userID'] = $arrLoggedInUser['userID'];
						$_SESSION['loggedInUser']['email'] = $arrLoggedInUser['email'];
						$_SESSION['loggedInUser']['accountName'] = $arrLoggedInUser['accountName'];
						//genrateSession();
						header("LOCATION: make_payment-i.php");
					}else{
						$strError = "Username, customer number or password is not valid or customer is not active";
					}
				}else{
					$strError = "Username, customer number or password is not valid or customer is not active";
				}
			}
			else
			{
				$strError = "Captcha code is not valid";
			}
		}
		else
		{
			$strError = "Captcha code is not entered";
		}
 }
		/* echo "<pre>";
		print_r($strQueryLoginChk);
		echo "</pre>"; */
	}
	
	if(isset($_GET['logout']) && !empty($_GET['logout'])){
		$strMsg = "You have logged out!!";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Login Page</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/bootstrap.min.js"></script>
	</head>
	<body class="loginbg">
		<div id="wrapper" class="login loginbg">
			<div id="header">
				<img alt="PremierFX" src="images/logo.png" />
			</div>
			<form name="login_form" id="login_form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<div class="login_form">
					<div class="login_head">
						<h1>Login<h1>
					</div>
					<?php 
						if(isset($strMsg) && !empty($strMsg)){
					?>
					<table class="table">
						<tr class="success">
							<td class="success align_center"><?php echo $strMsg; ?></td>
						</tr>
					</table>
					<?php 
						}
						if(isset($strError) && !empty($strError)){
					?>
					<table class="table">
						<tr class="error">
							<td class="error align_center"><?php echo $strError; ?></td>
						</tr>
					</table>
					<?php 
						}
					?>
					<div class="row align_center">
						<label class="bold">
							Note: Use email or customer number to log in.
						</label>
					</div>
					<div class="row">
						<label>Email:</label>
						<input id="email" name="email" maxlength="32"  type="text"/>
					</div>
					<div class="row">
						<label>Customer Number:</label>
						<input id="cutomerNumber" name="customerNumber" maxlength="32"  type="text"/>
					</div>
					<div class="row">
						<label>Password:</label>
						<input name="password" maxlength="32" id="password" type="password"/>
					</div>
					<div class="row">
                        <img src="captcha_code_file.php?rand=693246409" id='captchaimg' style="float:right">
                        <label>Can't see the image? click <a href='javascript: refreshCaptcha();'><span style="color:#06F">here</span></a> to refresh</lavel>
					</div>
					<div class="row">
						<label>Enter code from image:</label>
						<input name="letters_code" maxlength="32" id="letters_code" type="text"/>
					</div>
					<div class="row">
						<a href="forgot_password.php" class="pull-right">Forgot Password?</a>
					</div>
					<div class="row">
						
						<input name="login" id="login" type="submit" class="actv_btn btn input-small" value="Login" />
					</div>
					
				</div>
			</form>
		</div>
		<script type="text/javascript">
			$(document).ready(
				function(){
					$('#email').focus();
				}
			);
			function refreshCaptcha()
			{
				var img = document.images['captchaimg'];
				img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
			}
		</script>
	</body>
</html>