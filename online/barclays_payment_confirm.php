<?php

/**
 * @package: Online Module
 * @subpackage: Payment Details

 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}
include_once "includes/configs.php";
include_once "includes/database_connection.php";
include_once "config_barclays.php";
dbConnect();
include_once "includes/functions.php";
$PaymentMode = $_SESSION["test"];

/*Parameters For Payment*/
$Months = $_POST["Months"];
$Years = $_POST["Years"];

$ED = $Months . "/" . $Years;
$CARDNO = $_POST["CARDNO"];
$CARDTIT = $_POST["CARDTIT"];
$CVC = $_POST["CVC"];
// $sendingCurrency =$_POST["sendingCurrency"];
$sendingCurrency = $_SESSION["sendingCurrency"];
$duponline_customer_services = $_SESSION['online_customer_services'];
$strdupSendingAmount = $_SESSION['sendingAmount'];

/*
echo $strSendingAmount;
echo "<br>";
echo $ED;
 */

$intTransDataFlag = false;
if (!empty($_SESSION['exchangeRate']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['receivingCurrency']) && (!empty($_SESSION['sendingAmount']) && $_SESSION['sendingAmount'] > '0') && (!empty($_SESSION['receivingAmount']) && $_SESSION['receivingAmount'] > '0')) {
	if (isset($_POST['iban'])) {
		$patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
		$strIBAN = trim($_POST['iban']);
		/*if(!preg_match($patternIBAN, $strIBAN)){
				//debug('match', true);
				$_SESSION['benName'] = trim($_POST['benName']);
				$_SESSION['benCountry'] = $_POST['benCountry'];
				$_SESSION['benCurrency'] = trim($_POST['benCurrency']);
				$_SESSION['accountName'] = trim($_POST['accountName']);
				$_SESSION['accountNumber'] = trim($_POST['accountNumber']);
				$_SESSION['branchNameNumber'] = trim($_POST['branchNameNumber']);
				$_SESSION['branchAddress'] = trim($_POST['branchAddress']);
				$_SESSION['routingNumber'] = trim($_POST['routingNumber']);
				$_SESSION['sortCode'] = trim($_POST['sortCode']);
				$_SESSION['swift'] = trim($_POST['swift']);
				$_SESSION['refernce'] = trim($_POST['reference']);
				$_SESSION['existBen'] = $_POST['existBen'];
				$_SESSION['errorIBAN'] = "IBAN number is not valid";
				header("LOCATION: select_beneficiary_trans.php");
			}*/
	}

	{
		//debug('false', true);
		/**************Geting Guzatted Holidays****************/
		$holidayQuery = "select date from " . TBL_GUZATTED_HOLIDAY . " order by date";
		$holidayResult = selectMultiRecords($holidayQuery);

		$allCount = count($holidayResult);

		$intTransDataFlag = true;
		$strBenName = trim($_POST['benName']);
		$strBenCountryID = $_POST['benCountry'];
		$strQueryCountry = "SELECT countryName FROM " . TBL_COUNTRIES . " WHERE countryId = '$strBenCountryID'";
		$arrCountry = selectFrom($strQueryCountry);
		$strBenCountry = $arrCountry['countryName'];
		$strBenCurrency = trim($_POST['benCurrency']);
		if (isset($_POST['accountName'])) {
			$strBenAccountName = trim($_POST['accountName']);
		} else {
			$strBenAccountName = '';
		}

		if (isset($_POST['accountNumber'])) {
			$strBenAccountNumber = trim($_POST['accountNumber']);
		} else {
			$strBenAccountNumber = '';
		}

		if (isset($_SESSION['branchNameNumber'])) {
			$strBranchNameNumber = trim($_SESSION['branchNameNumber']);
		} else {
			$strBranchNameNumber = '';
		}

		if (isset($_SESSION['branchAddress'])) {
			$strBranchAddress = trim($_SESSION['branchAddress']);
		} else {
			$strBranchAddress = '';
		}

		if (isset($_POST['sortCode'])) {
			$strSortCode = trim($_POST['sortCode']);
		} else {
			$strSortCode = '';
		}

		if (isset($_POST['routingNumber'])) {
			$strRoutingNumber = trim($_POST['routingNumber']);
		} else {
			$strRoutingNumber = '';
		}

		if (isset($_POST['swift'])) {
			$strSwiftCode = trim($_POST['swift']);
		} else {
			$strSwiftCode = '';
		}

		$strTip = trim($_POST['tip']);
		$strReference = trim($_POST['reference']);
		$intBenID = $_POST['existBen'];
		$intCustomerID = $_SESSION['loggedInUser']['userID'];
		$intExchangeRate = $_SESSION['exchangeRate'];
		$strSendCurrency = $_POST['sendingCurrency'];
		$strSendingCurrency = $_SESSION['sendingCurrency'];
		$strReceivingCurrency = $_SESSION['receivingCurrency'];
		$strSendingAmount = $_SESSION['sendingAmount'];
		$strReceivingAmount = $_SESSION['receivingAmount'];
		$_SESSION["benBame"];
		$_SESSION["firstBen"];
		$_SESSION["secondBen"];
		$strCurrentTimeStamp = date("Y-m-d h:i:s");
		$valueDate = date('Y-m-d H:i:s', strtotime($strCurrentTimeStamp . ' + 2 day'));
		$valueDateWithoutTime = date('Y-m-d', strtotime($valueDate));
		$dateflag = true;
		while ($dateflag != false) {
			$dateflag = false;
			for ($i = 0; $i < $allCount; $i++) {
				if ($valueDateWithoutTime == $holidayResult[$i]['date']) {
					$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 1 day'));
					$valueDateWithoutTime = date('Y-m-d', strtotime($valueDate));
					$dateflag = true;

				}

			}

			$valueDateDay = date('l', strtotime($valueDate));
			if ($valueDateDay == "Saturday") {
				$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 2 day'));
				$dateflag = true;

			} else if ($valueDateDay == "Sunday") {
				$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 1 day'));
				$dateflag = true;

			}
		}

		$strCurrentDate = date("Y-m-d");
		$strIP = $_SERVER['REMOTE_ADDR'];
		$strMsg = '';
		$strError = '';

		if (!empty($_POST['existBen'])) {
			$strQueryUpdateBen = "UPDATE " . TBL_BENEFICIARY . " SET ";
			if (!empty($strBenName)) {
				$strQueryUpdateBen .= "`firstName` = '$strBenName',";
			}
			if (!empty($strBenName)) {
				$strQueryUpdateBen .= "`beneficiaryName` = '$strBenName',";
			}
			if (!empty($strBenCountry)) {
				$strQueryUpdateBen .= "`Country` = '$strBenCountry',";
			}

			if (!empty($strReference)) {
				$strQueryUpdateBen .= "`reference` = '$strReference',";
			}
			$strQueryUpdateBen = substr($strQueryUpdateBen, 0, strlen($strQueryUpdateBen) - 1);

			$strQueryUpdateBen .= " WHERE benID = '$intBenID' AND customerID = '$intCustomerID'";
			if (update($strQueryUpdateBen)) {
				$strMsg = "Beneficiary updated successfully";
			} else {
				$strError = "Beneficiary cannot be updated. please try again.";
			}

		} else {
			$strQueryInsertBen = "INSERT INTO " . TBL_BENEFICIARY . " (`firstName`, `middleName`, `lastName`,`created`, `beneficiaryName`, `Country`, `reference`, `customerID`) VALUES('$strBenName', '', '','$strCurrentDate', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID')";

			if (insertInto($strQueryInsertBen)) {
				$strMsg = "Beneficiary added successfully";
				$intBenID = mysql_insert_id();
			} else {
				$strError = "Beneficiary cannot be added successfully";
			}
		}
		if (!empty($intBenID)) {
			$strQueryChkBankInfo = "SELECT count(id) AS record FROM " . TBL_BEN_BANK_DETAILS . " WHERE benID = '$intBenID'";
			$arrBenBankInfo = selectFrom($strQueryChkBankInfo);
			if ($arrBenBankInfo['record'] == 0) {

				$strQueryInsertBenBank = "INSERT INTO " . TBL_BEN_BANK_DETAILS . " (`benId`, `IBAN`, `swiftCode`, `bankName`, `accountNo`, `branchCode`, `branchAddress`, `sortCode`, `routingNumber`,sendCurrency) VALUES('$intBenID', '$strIBAN', '$strSwiftCode', '$strBenAccountName', '$strBenAccountNumber', '$strBranchNameNumber', '$strBranchAddress', '$strSortCode', '$strRoutingNumber','$strSendCurrency')";
				if (insertInto($strQueryInsertBenBank)) {
					$strMsg .= " and beneficiary bank details are added against the beneficiary";
				} else {
					$strError .= " and beneficiary bank info cannot added.";
				}

			} else {
				$strQueryUpdateBenBank = "UPDATE " . TBL_BEN_BANK_DETAILS . " SET";
				if (!empty($strIBAN)) {
					$strQueryUpdateBenBank .= "`IBAN` = '$strIBAN',";
				}
				if (!empty($strSwiftCode)) {
					$strQueryUpdateBenBank .= "`swiftCode` = '$strSwiftCode',";
				}
				if (!empty($strBenAccountName)) {
					$strQueryUpdateBenBank .= "`bankName` = '$strBenAccountName',";
				}
				if (!empty($strBenAccountNumber)) {
					$strQueryUpdateBenBank .= "`accountNo` = '$strBenAccountNumber',";
				}
				if (!empty($strBranchNameNumber)) {
					$strQueryUpdateBenBank .= "`branchCode` = '$strBranchNameNumber',";
				}
				if (!empty($strBranchAddress)) {
					$strQueryUpdateBenBank .= "`branchAddress` = '$strBranchAddress',";
				}
				if (!empty($strSortCode)) {
					$strQueryUpdateBenBank .= "`sortCode` = '$strSortCode',";
				}
				if (!empty($strRoutingNumber)) {
					$strQueryUpdateBenBank .= "`routingNumber` = '$strRoutingNumber',";
				}
				if (!empty($strSendCurrency)) {
					$strQueryUpdateBenBank .= "`sendCurrency` = '$strSendCurrency',";
				}
				$strQueryUpdateBenBank = substr($strQueryUpdateBenBank, 0, strlen($strQueryUpdateBenBank) - 1);
				$strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";
				if (update($strQueryUpdateBenBank)) {
					$strMsg .= " and beneficiary bank details are updated";
				} else {
					$strError .= " and beneficiary bank info cannot be updated";
				}

			}
			$getCustAgent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='" . $intCustomerID . "'");
			$TransSource = "";
			if ($duponline_customer_services == "PC") {
				$TransSource = "Topup";
			} else {
				$TransSource = "Bank Transfer";
			}
			$name = $getCustAgent["firstName"] . " " . $getCustAgent["lastName"];
			$strRefNumber = generateRefNumber();

			$transTypeStatus = "";
			if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC") {
				$transTypeStatus = 'Topup';

			} else {
				$transTypeStatus = "Bank Transfer";
			}

			$strQueryTrans = "INSERT INTO " . TBL_TRANSACTIONS . " (`transType`, `customerID`, `benID`, `custAgentID`, `benAgentID`, `refNumberIM`, `transAmount`, `exchangeRate`, `localAmount`, `totalAmount`, `transStatus`, `transDate`, `valueDate`,`validValue`, `currencyFrom`, `currencyTo`, `transDetails`, `agentExchangeRate`, `trans_source`, `transIP`,`tip`,`remTransAmount`,addedBy,modeOfPayment) VALUES('$transTypeStatus', '$intCustomerID', '$intBenID', '" . $getCustAgent['agentID'] . "', '', '$strRefNumber', '$strSendingAmount', '$intExchangeRate', '$strReceivingAmount', '$strSendingAmount', 'Pending', '$strCurrentTimeStamp', '$valueDate','N', '$strSendingCurrency', '$strReceivingCurrency', 'Y', 'N', 'O', '$strIP','$strReference', '$strSendingAmount','" . $name . "','$PaymentMode')";
			if (insertInto($strQueryTrans)) {
				$intTransID = mysql_insert_id();
				$_SESSION["transID"] = $intTransID;
				$querNewlyAddedTrans = "SELECT * FROM " . TBL_TRANSACTIONS . " WHERE 1 ORDER By transID DESC LIMIT 1 ";
				$getNewlyAddedTrans = selectFrom($querNewlyAddedTrans);

				if ($_POST["proceed"] = "Confirm Transaction") {
					activities($_SESSION["loginHistoryID"], "INSERTION", $getNewlyAddedTrans["transID"], "transactions", "transaction " . $getNewlyAddedTrans["refNumberIM"] . " is added");
				}

				$querAgentCustAct = "INSERT INTO agents_customer_account  (`customerID`,`Date`,`tranRefNo`,`payment_mode`,`Type`,`amount`,`currencytitle`) VALUE ('" . $_SESSION["loggedInUser"]["userID"] . "',CURDATE(),'" . $intTransID . "','Transaction is Created','WITHDRAW','" . $_SESSION["sendingAmount"] . "','" . $_SESSION["sendingCurrency"] . "')";

				insertInto($querAgentCustAct) or die("something wrong with insert query");

				/************agent_company_account************/
				$querAgentComtAct = "INSERT INTO " . TBL_AGENT_COMPANY_ACCOUNT . "  ( `Date`,`tranRefNo`,`payment_mode`,`Type`,`amount`,`currencytitle`) VALUE ( CURDATE(),'" . $intTransID . "','Transaction is Created','DEPOSIT','" . $_SESSION["sendingAmount"] . "','" . $_SESSION["sendingCurrency"] . "')";
				insertInto($querAgentComtAct) or die("something wrong with insert query");

				$strQueryBankDetails = "INSERT INTO " . TBL_BANK_DETAILS . " (`benID`, `transID`, `bankName`, `accNo`, `IBAN`, `swiftCode`, `branchCode`, `branchAddress`, `routingNumber`, `sortCode`) VALUES('$intBenID', '$intTransID', '$strBenAccountName', '$strBenAccountNumber', '$strIBAN', '$strSwiftCode', '$strBranchNameNumber', '$strBranchAddress', '$strRoutingNumber', '$strSortCode')";
				insertInto($strQueryBankDetails);

				$bankDetails = mysql_insert_id();

				update("UPDATE " . TBL_CUSTOMER . " SET no_of_transactions= no_of_transactions+1 WHERE customerID=$intCustomerID ");
				$strMsg = "Transaction Created Successfully. Reference Number: $strRefNumber";

				/** #12850: Premier FX
				 * Debit Card Request Logs
				 * Created By Mudassar Rauf
				 */

				$encodedCardNo = base64_encode($CARDNO);

				/**  //insert into cm_cust_credit_card Table
				 * $encoded = base64_encode($CARDNO);
				 * $decoded = base64_decode($CARDNO);
				 */

				//$decodedVal = base64_decode($encodedCardNo);
				/*  $querycreditCard="INSERT into ".TBL_CUST_CREDIT_CARD."(customerID ,cardNo ,currency,cardName,cvc,eci,email,operation,order_id,amount,transID
					) values
					('".$getCustAgent['agentID']."','".$encodedCardNo."','".$sendingCurrency."','".$CARDTIT."','".$CVC."','".ECI."','','".OPERATION."','$strRefNumber','".$strSendingAmount."','".$intTransID."' )
					"; */
				//debug($intCustomerID);

				$querycreditCard = "INSERT into " . TBL_CUST_CREDIT_CARD . "(customerID ,cardNo ,currency,cardName,cvc,eci,email,operation,order_id,amount,transID
					) values
					('" . $intCustomerID . "','" . $encodedCardNo . "','" . $sendingCurrency . "','" . $CARDTIT . "','" . $CVC . "','" . ECI . "','','" . OPERATION . "','$strRefNumber','" . $strSendingAmount . "','" . $intTransID . "' )
					";
				//debug($querycreditCard, true);
				insertInto($querycreditCard) or die("something wrong with insert debit card query");

				$bankcId = mysql_insert_id();

				update("update transactions set DebitCardTrans='P' where transID='" . $intTransID . "'");
				//echo $intTransID;

				//echo "CID";
				//echo $bankDetails."<br>";
				//echo $bankcId;

				update("update bankDetails set cust_debit_id ='" . $bankcId . "' where bankID='" . $bankDetails . "' ");

				//echo "hi";
				// To get data from cust Debit card table

				/* $creditCardInfo = selectFrom("select * from ".TBL_CUST_CREDIT_CARD." where transID='".$intTransID."'");
                   */
				// $strSendingAmount ="100";
				$amount = (double) $strSendingAmount * 100;
				//echo "no";
				$email = $getCustAgent["email"]; //"dev1@hbstech.co.uk";

				// echo $email;
				$orderId = $strRefNumber;
				// debug($creditCardInfo);
				$shaPassPhrase = SHA_PASS_PRASE;
				$query = "select Address,Zip from customer where email='$email'";
				$result = selectFrom($query);
				$result[0] = str_replace(' ', '', $result[0]);
				$result[1] = str_replace(' ', '', $result[1]);

				// before $shaSignVar =AMOUNT_P."".$amount."".$shaPassPhrase."".CARDNO_P."".$CARDNO."".$shaPassPhrase."".CURRENCY_P."".$sendingCurrency."".$shaPassPhrase."".CVC_P."".$CVC."".$shaPassPhrase."".ECI_P."".ECI."".$shaPassPhrase."".ED_P."".$ED."".$shaPassPhrase."".EMAIL_P."".$email."".$shaPassPhrase."".OPERATION_P."".OPERATION."".$shaPassPhrase."".ORDERID_P."".$orderId."".$shaPassPhrase."".PSPID_P."".PSPID."".$shaPassPhrase."".PSWD_P."".PSWD."".$shaPassPhrase."".USERID_P."".USERID."".$shaPassPhrase;
				$shaSignVar = "ACCEPTURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_success_page.php" . $shaPassPhrase . AMOUNT_P . "" . $amount . "" . $shaPassPhrase . "" . CARDNO_P . "" . $CARDNO . "" . $shaPassPhrase . "" . CURRENCY_P . "" . $sendingCurrency . "" . $shaPassPhrase . "" . CVC_P . "" . $CVC . "" . $shaPassPhrase . "DESCLINEURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_error_page.php" . $shaPassPhrase . "" . ECI_P . "" . ECI . "" . $shaPassPhrase . "" . ED_P . "" . $ED . "" . $shaPassPhrase . "" . EMAIL_P . "" . $email . "" . $shaPassPhrase . "EXCEPTIONURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_error_page.php" . $shaPassPhrase . "FLAG3D=Y" . "" . $shaPassPhrase . "" . OPERATION_P . "" . OPERATION . "" . $shaPassPhrase . "" . ORDERID_P . "" . $orderId . "" . $shaPassPhrase . "OWNERADDRESS=$result[0]" . "" . $shaPassPhrase . "OWNERZIP=$result[1]" . "" . $shaPassPhrase . "" . PSPID_P . "" . PSPID . "" . $shaPassPhrase . "" . PSWD_P . "" . PSWD . "" . $shaPassPhrase . "" . USERID_P . "" . USERID . "" . $shaPassPhrase;
				$shaSignVar = AMOUNT_P . "" . $amount . "" . $shaPassPhrase . "" . CARDNO_P . "" . $CARDNO . "" . $shaPassPhrase . "" . CURRENCY_P . "" . $sendingCurrency . "" . $shaPassPhrase . "" . CVC_P . "" . $CVC . "" . $shaPassPhrase . "" . ECI_P . "" . ECI . "" . $shaPassPhrase . "" . ED_P . "" . $ED . "" . $shaPassPhrase . "" . EMAIL_P . "" . $email . "" . $shaPassPhrase . "FLAG3D=Y" . "" . $shaPassPhrase . "" . OPERATION_P . "" . OPERATION . "" . $shaPassPhrase . "" . ORDERID_P . "" . $orderId . "" . $shaPassPhrase . "OWNERADDRESS=$result[0]" . "" . $shaPassPhrase . "OWNERZIP=$result[1]" . "" . $shaPassPhrase . "" . PSPID_P . "" . PSPID . "" . $shaPassPhrase . "" . PSWD_P . "" . PSWD . "" . $shaPassPhrase . "" . USERID_P . "" . USERID . "" . $shaPassPhrase;

				//please change algo on admin page   $shaSignValue= sha1($shaSignVar);
				$shaSignValue = strtoupper(hash('sha512', $shaSignVar));

				$logsQuery = "insert into soap_Api_Logs (transID, method, request, response, remarks) VALUES ('" . $orderId . "','SAL','" . $shaSignVar . "','" . $shaSignValue . "','save sha work flow')";

				insertInto($logsQuery);

				//debug($shaSignVar);
				//debug($shaSignValue);
				//$strMsg = "Transaction Created Successfully. Reference Number: $strTip";
				$queryBankDetails = "select * from accounts where currency = '" . $strSendingCurrency . "' ";
				$arrBankRecord = selectFrom($queryBankDetails);

				//include_once 'sendmail-premier-online-pdf.php';

				unset($_SESSION['exchangeRate']);
				unset($_SESSION['sendingCurrency']);
				unset($_SESSION['receivingCurrency']);
				unset($_SESSION['sendingAmount']);
				unset($_SESSION['receivingAmount']);

			}
		}
	}
} else {
	//header("LOCATION: make_payment-ii-new.php");
}

$queryBankDetails = "select * from accounts where currency = '" . $strSendingCurrency . "' ";
$arrBankRecord = selectFrom($queryBankDetails);
$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];
if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC") {
	echo '<script type="text/javascript">
                 emailTopup("' . $strRefNumber . '");
				 </script>';
}
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title><?php echo CLIENT_NAME_ONLINE; ?></title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->

		<script type="text/javascript" src="jquery.js"></script>
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
	</head>
	<body>

<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>
				</div>

<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://payscanner.com"><img id="logo" src="images/logo_payscanner.png"></a>
</div>

<div class="menu">
<ul class="menu_ul">
<? if($_SESSION["loggedInUser"]["online_customer_services"] == "PC"){ ?>

<li class="menu_li"><a href="topup.php" class="menu_link" target="_parent">Top Up</a></li>

<li class="menu_li"><a href="balance_inquiry.php" class="menu_link" target="_parent">Balance Inquiry</a></li>

<? } else { ?>


<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>

<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>

<? } ?>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>

<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>

<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Send Money</h2>
<?php include 'top-menu.php';?>


<!-- content area -->
<div class="content_area">
<!-- content area left-->
<div class="content_area_center">
<p class="content_heading">Send Money</p>
<?php
//Ticket:221927136
/*Remove double hit to barclay by hid confirm button*/

$showConfirmButton = false;
if (isset($orderId) && !empty($orderId)) {
	$showConfirmButton = true;

	$strQueryButtonCount = "SELECT * FROM soap_Api_Logs WHERE transID=" . $orderId . "";
	$arrQueryButton = selectFrom($strQueryButtonCount);

	if (empty($arrQueryButton) && $arrQueryButton >= 1) {
		$showConfirmButton = false;
	} else {
		$showConfirmButton = true;
	}

}
?>
<p class="content_subheading">Your transaction has been successfully created reference number. <?echo $strRefNumber; ?><br> Click to confirm payment by debit card.<br>We will confirm the credit on your card once we receive your funds.</p>

</div>
<div class="content_area_left barclay_larg_area">
<form name="barclays" Method="POST" action="debitCardCurl.php">




<div class="field_wrapper large_wrapper barclay_larg_wrapper">
	<?php if ($showConfirmButton) {?>
<input type="submit" class="submit_btn" id="confirmbtn" value="PRESS TO CONFIRM DEBIT CARD PAYMENT">
		<?php }?>

<?  if($_SESSION["loggedInUser"]["online_customer_services"] == "PC"){ ?>
<input name="proceed" id="proceed" type="submit" value="New Transaction" class="submit_btn" onClick="window.location.href = 'topup.php';  return false;"/>
<? } else {?>
<input name="proceed" id="proceed" type="submit" value="New Transaction" class="submit_btn" onClick="window.location.href = 'make_payment-ii-new.php'; return false;"/>
<? } ?>
<input type="hidden" id="PSPID" name="PSPID" value="<?=PSPID?>"> <br>
<input type="hidden" id="ORDERID" name="ORDERID" value="<?=$orderId?>"><br>
<input type="hidden" id="USERID" name="USERID" value="<?=USERID?>"><br>
<input type="hidden" id="PSWD" name="PSWD" value="<?=PSWD?>"><br>
<input type="hidden" id="AMOUNT" name="AMOUNT" value="<?=$amount?>"><br>
<input type="hidden" id="CURRENCY" name="CURRENCY" value="<?=$sendingCurrency?>"><br>
<input type="hidden" id="CARDNO" name="CARDNO" value="<?=$CARDNO?>"><br>
<input type="hidden" id="CARDTIT" name="CARDTIT" value="<?=$CARDTIT?>"><br>
<input type="hidden" id="ED" name="ED" value="<?=$ED?>" ><br>
<input type="hidden" id="ECI" name="ECI" value="<?=ECI?>" ><br>
<input type="hidden" id="EMAIL" name="EMAIL" value="<?echo $email;?>" ><br>
<input type="hidden" id="SHASIGN" name="SHASIGN" value='<?=$shaSignValue?>' ><br>
<input type="hidden" id="CVC" name="CVC" value="<?=$CVC?>" ><br>
<input type="hidden" id="OPERATION" name="OPERATION" value="<?=OPERATION?>" ><br>
</div>
<input type="hidden" id="customerID" name="customerID" value="<?=$_SESSION['loggedInUser']['userID']?>" />
<input type="hidden" id="online_customer_services" name="online_customer_services" value="PC" />
<input type="hidden" id="sendingAmount" name="sendingAmount" value="<?=$strdupSendingAmount?>" />
</form>
</div>

<!-- content area right-->
<div class="content_area_right ">
<!--<div class="info_change_password margin_0"><p>Click to confirm payment by debit card.You will be emailed a confirmation of your trade.</p></div>-->
</div>


</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include 'footer.php';?>
</div>



</div>
</body>


	<script>
			var succssMsg = '';
			var fileValidateFlag = 0;
			$(function() {
				$('#addressContainer').hide();
				var button = $('#file');
				var fileUpload = new AjaxUpload(button, {
					action: 'registration_form_conf.php',
					name: 'passportDoc',
					// for single upload this should have 0 (zero appended)
					autoSubmit: false,
					allowedExtensions: ['.jpg', '.png', '.gif', '.jpeg', '.pdf'],
					// max size
					onChange: function(file, ext) {
						if ((ext && /^(jpg|png|jpeg|gif|pdf)$/i.test(ext))) {
							var fileSizeBytes = this._input.files[0].size;
							if(fileSizeBytes < 2097152){
								fileValidateFlag = 1;
								//button.text("Selected");
								button.css({
									"background": "#CCCCCC",
									"font-weight": "bold"
								});
								button.html('Document Selected');
							}else{
								alert("file is too large, maximum file size is 2MB");
							}
						} else {
							alert('Please choose a standard image file to upload, JPG or JPEG or PNG or GIF or PDF');
							button.css({
								"background": "#ECB83A",
								"font-weight": "normal"
							});
							button.html('Select Document');
						}
					},
					onSubmit: function(file, ext) {
						// If you want to allow uploading only 1 file at time,
						// you can disable upload button
						this.disable();
						$('#wait').fadeOut("slow");
						$('#wait').fadeIn("slow");
						$('#fileUploader').fadeIn("slow");
					},
					onComplete: function(file, response) {
						// enable upload button

						this.enable();
						//this.disable();
						$('#emailValidator').html(' ');
						$('#wait').fadeOut("fast");
						$('#fileUploader').fadeOut("slow");
						alert(response);
						$('#msg').html(succssMsg);
					}
				});
				$(document).ready(
				function() {
					// Date Picker
					Date.format = 'dd/mm/yyyy';
					$('#passportExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').datePicker({
						clickInput: true,
						createButton: false,
						endDate: (new Date()).asString()
					});
					$('#idExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').dpSetStartDate(' ');
					// Masking
					$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');
					$('#line1').mask('**-***-*********-9-***************');
					$('#line2').mask('999999-9-*-999999-9-aaa-***********');
					// Form Validation
					$("#senderRegistrationForm").validate({
						rules: {
							forename: {
								required: true
							},
							surname: {
								required: true
							},
							postcode: {
								required: true
							},
							passportNumber: {
								required: function() {
									if ($('#line1').val() == '') return true;
									else return false;
								}
								//,minlength: 37
							},
							line1: {
								required: function() {
									if ($('#passportNumber').val() == '') return true;
									else return false;
								}
							},
							line2: {
								required: function() {
									if ($('#line1').val() == '') return false;
									else return true;
								}
							},
							line3: {
								required: function() {
									if ($('#line2').val() == '' || $('#line1').val() == '') return false;
									else return true;
								}
							},
							idExpiry: {
								required: function() {
									if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;
									else return false;
								}
							},
							passportExpiry: {
								required: function() {
									if ($('#passportNumber').val() != '') return true;
									else return false;
								}
							}
						},
						messages: {
							passportNumber: "Please provide your Passport or National Identity card number",
							line1: "Please provide your Passport or National Identity card number",
							line2: "Please enter the remaining part of your National Identity card number",
							line3: "Please enter the remaining part of your National Identity card number",
							passportExpiry: "Please enter the expiry date of your Passport"
						},
						submitHandler: function() {
							register();
							return false;
						}
					});
					// Validating Email availability checks
					$("#email").blur(
					function() {
						var email = $('#email').val();
						if (email != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									email: email,
									chkEmailID: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#emailValidator').html(data);
								}
							});
						}
					});
					// Check if the passport availability checks
					$('#passportNumber').blur(
					function() {
						setTimeout(function() {
							passportAvailabilty();
						}, 100);
					});
					function passportAvailabilty() {
						var passport = $('#passportNumber').val();
						if (passport != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									passportNum: passport,
									chkPassport: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#passportAvailability').html(data);
								}
							});
						}
					}
					// ID card availability checks
					$('#line3').blur(
					function() {
						var idLine1 = $('#line1').val();
						var idLine2 = $('#line2').val();
						var idLine3 = $('#line3').val();
						if (idLine1 != '' && idLine2 != '' && idLine3 != '') {
							var idCard = idLine1 + idLine2 + idLine3;
						} else return false;
						$.ajax({
							url: "registration_form_conf.php",
							data: {
								idCardNumber: idCard,
								chkIDCard: '1'
							},
							type: "POST",
							cache: false,
							success: function(data) {
								$('#NICAvailability').html(data);
							}
						});
					});
					// Ajax Registration of Sender
					function register() {
						//Passport Issue and Expiry Date
						var passportIssue = new Date($('#passportIssue').val());
						var passportExpiry = new Date($('#idExpiry').val());
						if (passportIssue != '') {
							if (passportIssue >= passportExpiry) {
								alert("Your Passport issued date must be before the expiry date.");
								return false;
							}
						}
						// Registration Request
						var data = $('#senderRegistrationForm').serialize();
						//alert(data);
						data += "&register=Register";
						data += "&slog=yes";
						$('#wait').fadeIn("fast");
						$("#loadingMessage").text('Submitting your registration');
						$.ajax({
							url: "registration_form_conf.php",
							data: data,
							type: "POST",
							cache: false,
							success: function(msg) {
								if (msg.search(/Sender is registered successfully/i) >= 0) {
									if (fileValidateFlag != 0) {
										succssMsg = msg;
										$("#loadingMessage").text('Please wait for the confirmation of your passport upload.');
										var plog = "yes";
										var chk = "1";
										$.ajax({
										url: "registration_form_conf.php",
										data: {
											plog: plog,
											chk: chk
										},
										type: "POST",
										cache: false,
										success: function(msg) {
										}});
										fileValidateFlag = 0;
										fileUpload.submit();
									}else{
										$('#msg').html(msg);
										$('#wait').fadeOut("fast");
									}
									resetFormData();
								}else{
									//$('#wait').fadeOut("fast");
									$('#msg').html(msg);
									$('#wait').fadeOut("fast");
								}
							}
						});
					}
					function resetFormData() {
						$('#addressContainer').fadeOut('fast');
						document.forms[0].reset();
						$('#file').css({
							"background-color": "#ECB83A",
							"border": "1px solid #E5A000",
							"font-weight": "normal"
						});
						$('#file').html('Select Document');
						$('#senderRegistrationForm')[0].reset();
					}
					// Trigger the search address function
					$('#searchAddress').click(
					function() {
						searchAddress();
					});
				});
			});
			// Populate the Address in the fields
			function getAddressData(ele) {
				var value = ele.value;
				var arrAddress = value.split('/');
				var buildingNumber = $.trim(arrAddress[0]);
				var buildingName = $.trim(arrAddress[1]);
				var subBuilding = $.trim(arrAddress[2]);
				var street = $.trim(arrAddress[3]);
				var subStreet = $.trim(arrAddress[4]);
				var town = $.trim(arrAddress[5]);
				//var postcode = $.trim(arrAddress[6]);
				var organization = $.trim(arrAddress[7]);
				var buildingNumberVal = '';
				var buildingNameVal = '';
				var streetValue = '';
				var postCode = $('#postCodeSearch').val();
				if (buildingNumber != '') buildingNumberVal += buildingNumber;
				if (buildingName != '') buildingNameVal += buildingName;
				if (subBuilding != '') buildingNameVal += ' ' + subBuilding;
				if (street != '') streetValue += street;
				if (subStreet != '') streetValue += ' ' + subStreet;
				$('#buildingNumber').val(buildingNumberVal);
				$('#buildingName').val(buildingNameVal);
				$('#street').val(streetValue);
				$('#town').val(town);
				$('#postcode').val(postCode);
			}
			// if Press Enter on any field in the address area trigger the search address function
			function enterToSearch(e) {
				if (e.which) {
					keyCode = e.which;
					if (keyCode == 13) {
						e.preventDefault();
						searchAddress();
					}
				}
			}
			// Calls the API for suggessted address
			function searchAddress() {
				$('#residenceCountry').val('United Kingdom');
				$('#addressContainer').fadeOut('fast');
				postcode = $.trim($('#postCodeSearch').val());
				buildingNumber = $.trim($('#buildingNumber').val());
				street = $.trim($('#street').val());
				town = $.trim($('#town').val());
				if (postcode == '') {
					alert("Enter a postcode to search for your address");
					$('#postCodeSearch').focus();
					return;
				}
				$("#loadingMessage").text('Searching Address...');
				$('#wait').fadeIn("fast");
				$.ajax({
					url: "http://premierfx.live.hbstech.co.uk/api/gbgroup/addresslookupCus.php",
					data: {
						postcode: postcode,
						buildingNumber: buildingNumber,
						street: street,
						town: town
					},
					type: "POST",
					cache: false,
					success: function(data) {
						//alert(data.match(/option/i));
						$('#wait').fadeOut("slow");
						if (data.search(/option/i) >= 0) {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html(data);
						} else {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');
						}
					}
				});
			}
			// Clear the address section
			function clearAddress() {
				$('#buildingNumber').val('');
				$('#buildingName').val('');
				$('#street').val('');
				$('#town').val('');
				$('#province').val('');
			}
			function passportMask() {
				passportCountry = $('#passportCountry').val();
				switch (passportCountry) {
				case 'United Kingdom':
					 $('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-<<<<<<<<<<<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Portugal':
					 $('#passportNumber').unmask().mask('*******<<-9-***-9999999-a-9999999-**********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'USA':
					$('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-*********<****-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Australia':
					 $('#passportNumber').unmask().mask('********<-9-***-9999999-a-9999999-<*********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Spain':
					$('#passportNumber').unmask().mask('********<-9-aaa-9999999-a-9999999-***********<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				default:
					alert('Please select a country and enter your passport number');
					break;
				}
			}
			/* WORK DONE AGAINST 13137 EMAIL TOP UP  ###### ARSLAN ZAFAR */
			//$("#confirmbtn").click(function(){
			/*
			$.ajax({
			url : './ajaxCalls.php',
			data: {	rtypeMode:'emailTopup',
				customerID: $("#customerID").val()  ,
				transID:'<?php echo $strRefNumber; ?>',
				sendingAmount: $("#sendingAmount").val() } ,
			success:function(data){

			if(data ==1){

			window.location = 'debit_success_page.php';

				}else{

			window.location = 'debit_error_page.php';

				}
			}

			});

			return false;
			*/

			// if($('#online_customer_services').val() != 'TC'){

			// $.ajax({
			// url : './ajaxCalls.php',
			// data: {	rtypeMode:'loadBalance',
				// customerID: $("#customerID").val()  ,
				// sendingAmount: $("#sendingAmount").val() } ,
			// success:function(data){

			// if(data == 'successful'){
			// window.location = 'debit_success_page.php';

			// }else{
			  // window.location = 'debit_error_page.php';
			// }
			// }

			// });

			// }
			//return false;

			//});
			</script>
</html>
