<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Change Password
	 * @author: Mirza Arslan Baig
	 */
	ini_set("display_errors","on");
	error_reporting(1);
	session_start();
	
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	include_once "javascript/audit-functions.php";
	
 
	
	if(isset($_POST['changePassword']) && !empty($_POST['oldPass']) && !empty($_POST['pass1'])){
		$strQueryChkOldPass = "SELECT password FROM ".TBL_CUSTOMER." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."'";
		
		$arrChkOldPass = selectFrom($strQueryChkOldPass);
		 
		$strOldPassword = $_POST['oldPass'];
		$strNewPass1 = mysql_real_escape_string($_POST['pass1']);
		$strNewPass2 = mysql_real_escape_string($_POST['pass2']);
		if($arrChkOldPass['password'] == $strOldPassword && $strNewPass1 == $strNewPass2){
		$strQueryChangePass = "UPDATE ".TBL_CUSTOMER." SET `password` = '$strNewPass1' WHERE customerID = '".$_SESSION['loggedInUser']['userID']."'";
		if(update($strQueryChangePass)){
		$strMsg = "Password changed successfully!!";
		
		activities($_SESSION["loggedInUser"]["userID"],"UPDATION",$_SESSION["loggedInUser"]["userID"],"customer","Customer changed his password");
		$id_activity_id = @mysql_insert_id();
				 
		logChangeSet($arrChkOldPass,$_SESSION["loggedInUser"]["userID"],"customer","customerID",$_SESSION["loggedInUser"]["userID"],$_SESSION["loginHistoryID"]);
		$auditModifyId = @mysql_insert_id();	
		$querInsertActivityId = "UPDATE ".TBL_AUDIT_MODIFY_HISTORY." SET id_activity_id = ".$id_activity_id." WHERE id = ".$auditModifyId."";
		update($querInsertActivityId);
			}
			else{
				$strError = "Password can not be changed.Please try again";
			}
		}elseif($arrChkOldPass['password'] != $strOldPassword){
			$strError = "Enter valid old password!!";
		}else{
			$strError = "Your passwords did not matched!!";
		}
	}
	$strUserId=$_SESSION['loggedInUser']['userID'];
		$strEmail=$_SESSION['loggedInUser']['email'];
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Change Password</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		
		
		<script type="text/javascript" src="javascript/jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
		<script type='text/javascript'></script>
		
	</head>
	<body>
	
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link menu_selected" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Change Password</h2>
<?php include('top-menu.php');?>


<!-- content area -->
<div class="content_area">
<!-- content area left-->
<div class="content_area_left">
<p class="content_heading">Change Your Password</p>
<p class="content_subheading">You may change your password using the form below</p>
<form id="changePasswordForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<div class="field_wrapper">
<label class="form_label">Old Password:<span style="color:red">*</span></label><br>

<input name="oldPass" id="oldPass" type="password" class="form_fields" />
</div>

<div class="field_wrapper">
<label class="form_label">New Password:<span style="color:red">*</span></label><br>
<input name="pass1" id="pass1" type="password" class="form_fields" />

</div>

<div class="field_wrapper">
<label class="form_label">Confirm Password:<span style="color:red">*</span></label><br>
<input name="pass2" id="pass2" type="password"  maxlength="32" class="form_fields"/>

</div>

<!--<div class="field_wrapper">
<input name="changePassword" id="changePassword" type="submit" value="Submit" class="submit_btn"/>
<input type="reset" value="Clear" class="clear_btn"/>
</div>-->


</div>


<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password"><p>You can send any amount from £100 up to £50,000 or currency equivalent.</p></div>-->
</div>

<div class="button_wrapper">
<input name="changePassword" id="changePassword" type="submit" value="Submit" class="submit_btn"/>
<input type="reset" value="Clear" class="clear_btn"/>
</div>
</form>

<!-- Error message shown here -->
<?php 
if(isset($strError) && !empty($strError)){
?>
<div class="error_msg"><?php echo $strError; ?></div>

<?php 
}
if(isset($strMsg) && !empty($strMsg)){
?>
<div class="error_msg"><?php echo $strMsg; ?></div>

<?php 
}
?> 
<!-- Error message shown here ends area -->
</div>
<!-- content area ends here-->
</div>
<!-- footer area -->

<?php include('footer.php');?>
</div>



</div>
</body>
	
	<script type="text/javascript">
			
		
			$('#changePasswordForm').validate({
				rules: {
					oldPass: {
						required: true,
						minlength: 6
					},
					pass1: {
						required: true,
						minlength: 6
					},
					pass2: {  
						required: true,
						equalTo: '#pass1'   
					},
					messages:{
						oldPass: 'Enter Old Password to change the password.',
						pass1: 'Enter new password',
						pass2: {
							required: 'Re-enter your new password'
						}
					}
				}
			});
			function changeMySize(myvalue,iden)
// this function is called by the user clicking on a text size choice
{

	if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
// find the div to apply the text resizing to
		var ch = document.getElementById("cont_head");
		var op= document.getElementById("old_pass");
		var np = document.getElementById("new_pass");
		var cnp = document.getElementById("c_new_pass");
		var man = document.getElementById("mand");		
		ch.style.fontSize = myvalue + "px";
		op.style.fontSize = myvalue + "px";
		np.style.fontSize = myvalue + "px";
		cnp.style.fontSize = myvalue + "px";
		man.style.fontSize = myvalue + "px";
		
}
		</script>
		
</html>
