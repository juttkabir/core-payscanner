<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: view beneficiaries 
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	$limit = 10;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	else
		$offset = 0;
	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	
	
			if(($_REQUEST["benCountry"]=='')&&($_REQUEST["benName"]!=''))
			{
			$name=$_REQUEST["benName"];
			$query=" (firstName like '%".$name."%' OR middleName like '%".$name."%' AND lastName like '%".$name."%')AND ";
			}else if(($_REQUEST["benCountry"]!='')&&($_REQUEST["benName"]==''))
			{
			$strbenCountry=$_REQUEST["benCountry"];
			$query=" (Country like '%".$strbenCountry."%')AND ";
			}else if(($_REQUEST["benCountry"]!='')&&($_REQUEST["benName"]!=''))
			{
			$name=$_REQUEST["benName"];
			$strbenCountry=$_REQUEST["benCountry"];
			$query=" (firstName like '%".$name."%' OR middleName like '%".$name."%' AND lastName like '%".$name."%')AND (Country like '%".$strbenCountry."%') AND ";
			}else{
			$query="";
			}
	

	/**Ticket 10651**/
	//$strQueryTotalBens = "SELECT COUNT(benID) AS records FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
	$strQueryTotalBens = " SELECT   COUNT(benID)AS records FROM ".TBL_BENEFICIARY." WHERE  ".$query." `customerID` = '".$_SESSION['loggedInUser']['userID']."'  AND status = 'Enabled'";
	//debug($strQueryTotalBens);
	/* $strQueryBen = "SELECT benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' ORDER BY benID DESC status = 'Enabled' LIMIT $offset, $limit";
	*/
	
	$strQueryBen=" SELECT  benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE  ".$query." `customerID` = '".$_SESSION['loggedInUser']['userID']."'  AND status = 'Enabled' ORDER BY `benID`DESC LIMIT ".$offset." , ".$limit." ";
	//debug($strQueryBen);
	
	$arrTotalBens = selectFrom($strQueryTotalBens);
	
	$arrBens = selectMultiRecords($strQueryBen);
	$intTotalBens = count($arrBens);
	$intAllBens = $arrTotalBens['records'];
		$strUserId=$_SESSION['loggedInUser']['accountName'];
		$strEmail=$_SESSION['loggedInUser']['email'];
	//debug($arrBens);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" /> 
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
	</head>
	<body>
				
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link menu_selected" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link " target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link " target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Beneficiaries</h2>
<?php include('top-menu.php');?>



<!-- content area -->

<!-- content area -->
<div class="content_area">
<!-- content area left-->
<div class="content_area_left">
<p class="content_heading">Search your beneficiaries</p>
<!--<p class="content_subheading">You may change your password using the form below</p>-->
<form action="" name="benSearch" method="post">

<div class="field_wrapper">
<label class="form_label">Beneficiary Name:</label><br>

<input  type="text" value="<?=$name?>" class="form_fields" name="benName" id="benName" maxlength ="30"/>
</div>


<!--<div class="field_wrapper">
<input id="searchBen"  type="submit" value="Search" class="submit_btn">
<input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();" value="Create Beneficiary">
</div>-->


</div>

<!-- content area right-->
<div class="content_area_right">


<div class="field_wrapper margin_min">
<label class="form_label">Beneficiary Country:</label><br>


<select id="benCountry"  class="select_field"   name="benCountry" >
<option value="">-Select Country-</option>
<?php $strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
$arrCountry = selectMultiRecords($strQueryCountries);
for($j = 0;$j < count($arrCountry);$j++)
echo "<option value='".$arrCountry[$j]['countryName']."'>".$arrCountry[$j]['countryName']."</option>";
?>


</select>
				
<!--<div class="field_wrapper">
<input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();" value="Create Beneficiary">

</div>-->				
</div>


</div>
<div class="button_wrapper">
<input id="searchBen"  type="submit" value="Search" class="submit_btn">
<input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();" value="Create Beneficiary">
</div>
</form>
</div>
<!-- content area ends here-->
<!-- beneficiary content area -->

<div class="content_area">

<div class="table_area">
<!-- pagination-->
<?php 
							if($intTotalBens > 0){
								
						?>
<table class="table"  width="100%">
							<tr>
								<td class="showing_pag">Showing <?php echo ($offset+1)."-".($intTotalBens + $offset)." of ".$intAllBens; ?></td>
								<td></td>
								<td></td>
								<td></td>
								<td align="right" class="showing_pag">
									<?php 
										if($prev >= 0){
									?>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0">First</a>&nbsp;
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev; ?>">Previous</a>&nbsp;
									<?php 
										}
										if($next > 0 && $next < $intAllBens){
											$intLastOffset = (ceil($intAllBens / $limit) - 1) * $limit;
									?>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>">Next</a>&nbsp;
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset; ?>">Last</a>
									<?php 
										}
									?>
								</td>
							</tr>
						</table>


<table width="100%">
<br>
<!-- table head-->
<tr>
<th align="left" class="head_td"><strong>Creation Date</strong></th>
								<th align="left" class="head_td"><strong>Name</strong></th>
								<th align="left" class="head_td"><strong>Country</strong></th>
								<th align="left" class="head_td"><strong>Operations</strong></th>
								<th class="head_td"></th>
</tr>
<?php 
							
								for($x = 0;$x < $intTotalBens;$x++){
							
							
							?>
<!-- table body-->
<tr class="desc_tr">
								<td class="desc_td"><?php echo $arrBens[$x]['created'] ?></td>
								<td class="desc_td"><?php echo $arrBens[$x]['name']; ?></td>
								<td class="desc_td"><?php echo $arrBens[$x]['Country'] ?></td>
								
									<form action="" method="post">
                                    <td class="desc_td">
										<a href="javascript:void(0);" onClick="document.forms[<?php echo $x+1; ?>].action='add_beneficiary_new.php';document.forms[<?php echo $x+1; ?>].submit();">Edit</a>
										<input name="ben" type="hidden" value="<?php echo $arrBens[$x]['benID']?>"/>
	                                  </td>
								<td class="desc_td">
										<a href="javascript:void(0);" onClick="document.forms[<?php echo $x+1; ?>].action='make_payment-ii-new.php';document.forms[<?php echo $x+1; ?>].submit();">Create Transaction</a>
									</form>
								
							</tr>
							<?php 
								}
							?>
						</table>
						<?php 
							}else{
						?>
						<table width="100%">
							<tr class="desc_tr">
								<td class="desc_td" style="  text-align: center;">There are no beneficiaries to display.</td>
							</tr>
						</table>
						<?php 
							}
						?>


</div>

</div>
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
	
	<script>
			var succssMsg = '';
			var fileValidateFlag = 0;
			$(function() {
				$('#addressContainer').hide();
				var button = $('#file');
				var fileUpload = new AjaxUpload(button, {
					action: 'registration_form_conf.php',
					name: 'passportDoc',
					// for single upload this should have 0 (zero appended)
					autoSubmit: false,
					allowedExtensions: ['.jpg', '.png', '.gif', '.jpeg', '.pdf'],
					// max size  
					onChange: function(file, ext) {
						if ((ext && /^(jpg|png|jpeg|gif|pdf)$/i.test(ext))) {
							var fileSizeBytes = this._input.files[0].size;
							if(fileSizeBytes < 2097152){
								fileValidateFlag = 1;
								//button.text("Selected");
								button.css({
									"background": "#CCCCCC",
									"font-weight": "bold"
								});
								button.html('Document Selected');
							}else{
								alert("file is too large, maximum file size is 2MB");
							}
						} else {
							alert('Please choose a standard image file to upload, JPG or JPEG or PNG or GIF or PDF');
							button.css({
								"background": "#ECB83A",
								"font-weight": "normal"
							});
							button.html('Select Document');
						}
					},
					onSubmit: function(file, ext) {
						// If you want to allow uploading only 1 file at time,
						// you can disable upload button
						this.disable();
						$('#wait').fadeOut("slow");
						$('#wait').fadeIn("slow");
						$('#fileUploader').fadeIn("slow");
					},
					onComplete: function(file, response) {
						// enable upload button
						this.enable();
						//this.disable();
						$('#emailValidator').html(' ');
						$('#wait').fadeOut("fast");
						$('#fileUploader').fadeOut("slow");
						alert(response);
						$('#msg').html(succssMsg);
					}
				});
				$(document).ready(
				function() {
					// Date Picker
					Date.format = 'dd/mm/yyyy';
					$('#passportExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').datePicker({
						clickInput: true,
						createButton: false,
						endDate: (new Date()).asString()
					});
					$('#idExpiry').datePicker({
						clickInput: true,
						createButton: false
					});
					$('#passportIssue').dpSetStartDate(' ');
					// Masking
					$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');
					$('#line1').mask('**-***-*********-9-***************');
					$('#line2').mask('999999-9-*-999999-9-aaa-***********');
					// Form Validation 
					$("#senderRegistrationForm").validate({
						rules: {
							forename: {
								required: true
							},
							surname: {
								required: true
							},
							postcode: {
								required: true
							},
							passportNumber: {
								required: function() {
									if ($('#line1').val() == '') return true;
									else return false;
								}
								//,minlength: 37
							},
							line1: {
								required: function() {
									if ($('#passportNumber').val() == '') return true;
									else return false;
								}
							},
							line2: {
								required: function() {
									if ($('#line1').val() == '') return false;
									else return true;
								}
							},
							line3: {
								required: function() {
									if ($('#line2').val() == '' || $('#line1').val() == '') return false;
									else return true;
								}
							},
							idExpiry: {
								required: function() {
									if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;
									else return false;
								}
							},
							passportExpiry: {
								required: function() {
									if ($('#passportNumber').val() != '') return true;
									else return false;

								}
							}
						},
						messages: {
							passportNumber: "Please provide your Passport or National Identity card number",
							line1: "Please provide your Passport or National Identity card number",
							line2: "Please enter the remaining part of your National Identity card number",
							line3: "Please enter the remaining part of your National Identity card number",
							passportExpiry: "Please enter the expiry date of your Passport"
						},
						submitHandler: function() {
							register();
							return false;
						}
					});
					// Validating Email availability checks
					$("#email").blur(
					function() {
						var email = $('#email').val();
						if (email != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									email: email,
									chkEmailID: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#emailValidator').html(data);
								}
							});
						}
					});
					// Check if the passport availability checks
					$('#passportNumber').blur(
					function() {
						setTimeout(function() {
							passportAvailabilty();
						}, 100);
					});
					function passportAvailabilty() {
						var passport = $('#passportNumber').val();
						if (passport != '') {
							$.ajax({
								url: "registration_form_conf.php",
								data: {
									passportNum: passport,
									chkPassport: '1'
								},
								type: "POST",
								cache: false,
								success: function(data) {
									$('#passportAvailability').html(data);
								}
							});
						}
					}
					// ID card availability checks
					$('#line3').blur(
					function() {
						var idLine1 = $('#line1').val();
						var idLine2 = $('#line2').val();
						var idLine3 = $('#line3').val();
						if (idLine1 != '' && idLine2 != '' && idLine3 != '') {
							var idCard = idLine1 + idLine2 + idLine3;
						} else return false;
						$.ajax({
							url: "registration_form_conf.php",
							data: {
								idCardNumber: idCard,
								chkIDCard: '1'
							},
							type: "POST",
							cache: false,
							success: function(data) {
								$('#NICAvailability').html(data);
							}
						});
					});
					// Ajax Registration of Sender
					function register() {
						//Passport Issue and Expiry Date
						var passportIssue = new Date($('#passportIssue').val());
						var passportExpiry = new Date($('#idExpiry').val());
						if (passportIssue != '') {
							if (passportIssue >= passportExpiry) {
								alert("Your Passport issued date must be before the expiry date.");
								return false;
							}
						}
						// Registration Request
						var data = $('#senderRegistrationForm').serialize();
						//alert(data);
						data += "&register=Register";
						data += "&slog=yes";
						$('#wait').fadeIn("fast");
						$("#loadingMessage").text('Submitting your registration');
						$.ajax({
							url: "registration_form_conf.php",
							data: data,
							type: "POST",
							cache: false,
							success: function(msg) {
								if (msg.search(/Sender is registered successfully/i) >= 0) {
									if (fileValidateFlag != 0) {
										succssMsg = msg;
										$("#loadingMessage").text('Please wait for the confirmation of your passport upload.');
										var plog = "yes";
										var chk = "1";
										$.ajax({
										url: "registration_form_conf.php",
										data: {
											plog: plog,
											chk: chk
										},
										type: "POST",
										cache: false,
										success: function(msg) {
										}});
										fileValidateFlag = 0;
										fileUpload.submit();
									}else{
										$('#msg').html(msg);
										$('#wait').fadeOut("fast");
									}
									resetFormData();
								}else{
									//$('#wait').fadeOut("fast");
									$('#msg').html(msg);
									$('#wait').fadeOut("fast");
								}
							}
						});
					}
					function resetFormData() {
						$('#addressContainer').fadeOut('fast');
						document.forms[0].reset();
						$('#file').css({
							"background-color": "#ECB83A",
							"border": "1px solid #E5A000",
							"font-weight": "normal"
						});
						$('#file').html('Select Document');
						$('#senderRegistrationForm')[0].reset();
					}
					// Trigger the search address function
					$('#searchAddress').click(
					function() {
						searchAddress();
					});
				});
			});
			// Populate the Address in the fields
			function getAddressData(ele) {
				var value = ele.value;
				var arrAddress = value.split('/');
				var buildingNumber = $.trim(arrAddress[0]);
				var buildingName = $.trim(arrAddress[1]);
				var subBuilding = $.trim(arrAddress[2]);
				var street = $.trim(arrAddress[3]);
				var subStreet = $.trim(arrAddress[4]);
				var town = $.trim(arrAddress[5]);
				//var postcode = $.trim(arrAddress[6]);
				var organization = $.trim(arrAddress[7]);
				var buildingNumberVal = '';
				var buildingNameVal = '';
				var streetValue = '';
				var postCode = $('#postCodeSearch').val();
				if (buildingNumber != '') buildingNumberVal += buildingNumber;
				if (buildingName != '') buildingNameVal += buildingName;
				if (subBuilding != '') buildingNameVal += ' ' + subBuilding;
				if (street != '') streetValue += street;
				if (subStreet != '') streetValue += ' ' + subStreet;
				$('#buildingNumber').val(buildingNumberVal);
				$('#buildingName').val(buildingNameVal);
				$('#street').val(streetValue);
				$('#town').val(town);
				$('#postcode').val(postCode);
			}
			// if Press Enter on any field in the address area trigger the search address function
			function enterToSearch(e) {
				if (e.which) {
					keyCode = e.which;
					if (keyCode == 13) {
						e.preventDefault();
						searchAddress();
					}
				}
			}
			// Calls the API for suggessted address
			function searchAddress() {
				$('#residenceCountry').val('United Kingdom');
				$('#addressContainer').fadeOut('fast');
				postcode = $.trim($('#postCodeSearch').val());
				buildingNumber = $.trim($('#buildingNumber').val());
				street = $.trim($('#street').val());
				town = $.trim($('#town').val());
				if (postcode == '') {
					alert("Enter a postcode to search for your address");
					$('#postCodeSearch').focus();
					return;
				}
				$("#loadingMessage").text('Searching Address...');
				$('#wait').fadeIn("fast");
				$.ajax({
					url: "http://premierfx.live.hbstech.co.uk/api/gbgroup/addresslookupCus.php",
					data: {
						postcode: postcode,
						buildingNumber: buildingNumber,
						street: street,
						town: town
					},
					type: "POST",
					cache: false,
					success: function(data) {
						//alert(data.match(/option/i));
						$('#wait').fadeOut("slow");
						if (data.search(/option/i) >= 0) {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html(data);
						} else {
							$('#addressContainer').fadeIn('fast');
							$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');
						}
					}
				});
			}
			// Clear the address section
			function clearAddress() {
				$('#buildingNumber').val('');
				$('#buildingName').val('');
				$('#street').val('');
				$('#town').val('');
				$('#province').val('');
			}
			function passportMask() {
				passportCountry = $('#passportCountry').val();
				switch (passportCountry) {
				case 'United Kingdom':
					 $('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-<<<<<<<<<<<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Portugal':
					 $('#passportNumber').unmask().mask('*******<<-9-***-9999999-a-9999999-**********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'USA':
					$('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-*********<****-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Australia':
					 $('#passportNumber').unmask().mask('********<-9-***-9999999-a-9999999-<*********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				case 'Spain':
					$('#passportNumber').unmask().mask('********<-9-aaa-9999999-a-9999999-***********<<<-*-9');
					$('#passportNumber').removeAttr("disabled");
					break;
				default:
					alert('Please select a country and enter your passport number');
					break;
				}
			}
			
			function createBen(){
				window.location = 'add_beneficiary_new.php';
			}
			</script>
</html>
