<?
/* Distributor Deal Sheet Functionality come here*/

function add_new_deal($arguments,$value="")
{
  //debugDealSheet($arguments);
  $created = date("Y-m-d h:i:s");
  if($arguments["insertNewDeal"] == 'ADD'){
  $localAmount = 0;
  $msg = "";
  
  if($arguments["userType"] == 'CUSTOMER')
  	{
		if(!empty($arguments["customer_no"]))
		{
			$query = "SELECT customerID FROM customer where accountName = '".$arguments["customer_no"]."'";
			//debugDealSheet($query);
			$rs = mysql_query($query)or die(__LINE__."Query Error".mysql_error());
			$custData = mysql_fetch_array($rs);
			//debugDealSheet($custData);
				if($custData["customerID"] == '')
				{
				  echo "C";
				  exit(); 	
				}else{
				$userId = $custData["customerID"];
				}
		}
		else
		{
			$userId = 'bulk';
		}
	}elseif($arguments["userType"] == 'DISTRIBUTOR'){
	$userId = $arguments["userID"];
	}
	
  if(!empty($arguments["rate"]) && !empty($arguments["value_origin"]))
  	 $localAmount = $arguments["rate"] * $arguments["value_origin"];
 
  $strSql = "INSERT INTO deal_sheet(
  									userId,
									deal_ref,
									currency_origin,
									currency_destination,
									purchase_from,
									purchase_amount,
									local_amount,
									balance,
									rate,
									userType,
									deal_date,
									created
									)
							 VALUES	(
							 		'".$userId."',
									'".$arguments["deal_ref"]."',
									'".$arguments["currency_origin"]."',
									'".$arguments["currency_destination"]."',
									'".$arguments["purchase_from"]."',
									'".$arguments["value_origin"]."',
									'".$localAmount."',
									'".$arguments["value_origin"]."',
									'".$arguments["rate"]."',
									'".$arguments["userType"]."',
									'".$arguments["deal_date"]."',
									'".$created."'
							 		)
 									";
			//debugDealSheet($strSql);	
			 if(!mysql_query($strSql))
			 {
				 //die('Query Error'.mysql_error());	
				 $msg = 'E';
			 }
			 else
			 {
			  $msg = 'S';
			 }
  }
  
  return $msg;
}
function deleteDeal($arguments)
{
	//debugDealSheet($arguments);	
	$btnAction = $arguments["btnAction"];
	$dealIDs = $arguments["dealIDs"];
	$return = '';
	if($btnAction == "Delete")
	{
		$dealIDArray	= $dealIDs;
		for ($d=0;$d < count($dealIDArray);$d++)
		{
			if(isset($dealIDArray[$d]))
			{
				$strPayoutSql = "select dtd_id from deal_trans_details where id = '".$dealIDArray[$d]."'";
				//debugDealSheet($strPayoutSql);
				$payoutResult = mysql_query($strPayoutSql) or die(__LINE__."Query Error".mysql_error());
				$existingRecord = mysql_fetch_array($payoutResult);
				//debugDealSheet($existingRecord);
				if($existingRecord["dtd_id"] == '')
				{
						$delSql ="delete from deal_sheet  where id = '".$dealIDArray[$d]."'";
							if(!mysql_query($delSql)) 
								$err = "Error occured while deleting some deal sheet(s)";
				}else{
						 $err = "some deal sheet already used so it can not be deleted";
				}			
					
			}
		} // end for loop
	
		if(trim($err) == "")
			$err = "Payment(s) deleted successfully.";
	
		$return = "<input type='hidden' name='dealListDelete' id='dealListDelete' value='".$err."'>";
	}
return $return;	
}

function payOut($arguments)
{
	// debugDealSheet($arguments);	
	 $action = $arguments["action"];
	 $agentType = $arguments["agentType"];
	 $loggedUserID = $arguments["loggedUserID"];
	 $transID = $arguments["transID"];
	 $payOutFlag = true;
	 $deal_balance = 0;
	 $pl = 0;
	 $dealLocal = 0;
	 $dealExistingPL = 0;
	 $updatedDate = date("Y-m-d h:i:s");
	 $created = date("Y-m-d h:i:s");
	
	 //if($agentType == 'SUPI' OR $agentType == 'SUBI' OR $agentType == 'admin')
	 //{
		if(!empty($transID)){
		$strTrans = "select totalAmount,transID,localAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID,currencyFrom,currencyTo,exchangeRate,transDate from transactions where transID = '". $transID."' and transStatus='Authorize'";
		
		//debugDealSheet($strTrans);
   	    $transRs = mysql_query($strTrans) or die(__LINE__."Query Error:".mysql_error());
		$rowTrans= mysql_fetch_array($transRs);
		//debugDealSheet($rowTrans);
		if($rowTrans["transID"] != '')
		{
			$transBalance = $rowTrans["transAmount"];	
			$localAmount = $rowTrans["localAmount"];	
			$currencyFrom = $rowTrans["currencyFrom"];
			$currencyTo = $rowTrans["currencyTo"];
			$refNumberIM = $rowTrans["refNumberIM"];	
			$exchangeRate = $rowTrans["exchangeRate"];	
			$transDate = $rowTrans["transDate"];	
			$benAgentID = $rowTrans["benAgentID"];
			//debugDealSheet($localAmount);
		}
	  
		if(!empty($currencyFrom) && !empty($currencyTo)){
		$dealSql = "SELECT
							id,
							deal_ref,
							rate,
							profit_loss,
							balance 
						FROM 
							deal_sheet 
						WHERE 
							(status != 'IA' AND status != 'DL') AND 
							 currency_origin = '".$currencyFrom."' AND 
							 currency_destination = '".$currencyTo."'
							  ";
							 
		if($agentType == 'admin')
			$dealSql .=  " AND userId= ".$benAgentID." ";
		else						
			$dealSql .=" AND userId = '".$loggedUserID."' ";
		
		//debugDealSheet($dealSql);
		
		if(!empty($dealSql))
			$dealRs = mysql_query($dealSql) or die(__LINE__." Query Error:".mysql_error());					
		//$strDealSql = mysql_fetch_array($dealRs);	
		//debugDealSheet($strDealSql["id"]);
		 $numRows = mysql_num_rows($dealRs);
		// debugDealSheet($numRows);
		 if($numRows < 1)
		 	$payOutFlag = false;
		}
		while($strDealSql = mysql_fetch_array($dealRs))
		{
			$deal_balance = $strDealSql["balance"];
			//debugDealSheet($deal_balance."-->".$transBalance);
			if($deal_balance >= $transBalance)
			 {
				$payOutFlag = true;
				$deal_balance = $deal_balance - $transBalance; 
				$dealExistingPL = $strDealSql["profit_loss"]; 
				$dealLocal = $strDealSql["rate"] * $transBalance;
				$pl = $dealLocal-$localAmount;
				$dealExistingPL += $pl; 
				$strUpdateSql = "update deal_sheet set balance='".$deal_balance."', updated = '".$updatedDate."',profit_loss = '".$dealExistingPL."' where id='".$strDealSql["id"]."'";
				//debugDealSheet($strUpdateSql);
				mysql_query($strUpdateSql) or die(__LINE__."Query Error:".mysql_error());;
				$strInsertSql = "INSERT INTO deal_trans_details 
															(
															id,
															transID,
															refNumberIM,
															currencyFrom,
															currencyTo,
															transAmount,
															localAmount,
															exchangeRate,
															PL,
															transDate,
															created 
															)
													  VALUES(
															'".$strDealSql["id"]."',
															'".$transID."',
															'".$refNumberIM."',
															'".$currencyFrom."',
															'".$currencyTo."',
															'".$transBalance."',
															'".$localAmount."',
															'".$exchangeRate."',
															'".$pl."',
															'".$transDate."',
															'".$created."'
															)";
				// debugDealSheet($strInsertSql);
				 mysql_query($strInsertSql) or die(__LINE__."Query Error:".mysql_error());;
			
			 }
			 else
			 {
			 	$payOutFlag = false;
			 }
			 if($payOutFlag)
			 		break;
				 //debugDealSheet($deal_balance,true);
		} // end while loop
		//debugDealSheet($payOutFlag);				
		return 	$payOutFlag;	
   }
}

function editDealRate($arguments)
{
//debugDealSheet($arguments);
	$rate = $arguments["rate"];
	$action = $arguments["action"];
	$id = $arguments["id"];
	$return = '';
	$err = '';
	
	if($action == 'editRate')
	{
		$strTrans = "SELECT transAmount,localAmount from deal_trans_details where id = '".$id."' ";
		$transRS= mysql_query($strTrans) or die(__LINE__."Query Error".mysql_error());
		$pl = 0;
		$dealLocal = 0;
		while($row = mysql_fetch_array($transRS))
		{
			$transAmount = $row[0];
			$localAmount = $row[1];
			$dealLocal = $rate * $transAmount;
			//debugDealSheet($rate."-->".$transAmount."-->".$localAmount);
			$pl += $dealLocal - $localAmount;
		}
		 $updateRate = "update deal_sheet set rate = '".$rate."', profit_loss = '".$pl."'  where id = '".$id."'";
		//debugDealSheet($updateRate);
		 $updateRs = mysql_query($updateRate) or die(__LINE__."Query Error:".mysql_error());
			 //$err = "Error occured while updating deal sheet(s) Rate";
		
		if(trim($err) == "")
			$err = "Rate updated successfully.";
	
		$return = "<input type='hidden' name='dealListDelete' id='dealListDelete' value='".$err."'>";
	}	 
	
	return $return;
	
}
/*Authorize Transaction*/

function transAuthorize($arguments)
{
	// debugDealSheet($arguments);	
	 $action = $arguments["action"];
	 $transID = $arguments["transID"];
	 $payOutFlag = true;
	 $bulkFalg = false;
	 $deal_balance = 0;
	 $pl = 0;
	 $dealLocal = 0;
	 $dealExistingPL = 0;
	 $status = '';
	 $updatedDate = date("Y-m-d h:i:s");
	 $created = date("Y-m-d h:i:s");
	
	
		if(!empty($transID)){
		$strTrans = "select totalAmount,transID,localAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID,currencyFrom,currencyTo,exchangeRate,transDate from transactions where transID = '". $transID."' and transStatus='Authorize'";
	/*	$strTrans = "select totalAmount,transID,localAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID,currencyFrom,currencyTo,exchangeRate,transDate from transactions where transID = '". $transID."' ";*/
		
		//debugDealSheet($strTrans);
   	    $transRs = mysql_query($strTrans) or die(__LINE__."Query Error:".mysql_error());
		$rowTrans= mysql_fetch_array($transRs);
		if($rowTrans["transID"] != '')
		{
			$transBalance = $rowTrans["transAmount"];	
			$localAmount = $rowTrans["localAmount"];	
			$currencyFrom = $rowTrans["currencyFrom"];
			$currencyTo = $rowTrans["currencyTo"];
			$refNumberIM = $rowTrans["refNumberIM"];	
			$exchangeRate = $rowTrans["exchangeRate"];	
			$transDate = $rowTrans["transDate"];	
			$customerID = $rowTrans["customerID"];
						
			//debugDealSheet($localAmount);
		}
	  }
	  
		if(!empty($currencyFrom) && !empty($currencyTo) && !empty($customerID)){
		$dealQuery = "SELECT
							id,
							deal_ref,
							rate,
							profit_loss,
							balance 
						FROM 
							deal_sheet 
						WHERE 
							(status != 'IA' AND status != 'DL') AND 
							 currency_origin = '".$currencyFrom."' AND 
							 currency_destination = '".$currencyTo."' AND
							 userType = 'CUSTOMER' AND
							 userId = '".$customerID."'	
							  ";
		//debugDealSheet($dealQuery);				 
			if(!empty($dealQuery))
			{
			 $dealRs = mysql_query($dealQuery) or die(__LINE__." Query Error:".mysql_error());					
			 $numRows = mysql_num_rows($dealRs);
			//debugDealSheet($numRows);
		
				while($row = mysql_fetch_assoc($dealRs))
				{
					$existing_balance = $row["balance"];
					//debugDealSheet($existing_balance."-->".$transBalance);
					if($existing_balance < $transBalance)
						 $bulkFalg = true;
				}
			 }		  
			
			if($numRows < 1 || $bulkFalg)
			{
				$payOutFlag = false;
				$dealQuery = "SELECT
								id,
								deal_ref,
								rate,
								profit_loss,
								balance 
							FROM 
								deal_sheet 
							WHERE 
								(status != 'IA' AND status != 'DL') AND 
								 currency_origin = '".$currencyFrom."' AND 
								 currency_destination = '".$currencyTo."' AND
								 userType = 'CUSTOMER' AND
								 userId = 'bulk'	
								  ";
			//debugDealSheet($dealQuery);				 
			//$dealRs = mysql_query($dealQuery) or die(__LINE__." Query Error:".mysql_error());		
			//$strDealSql = mysql_fetch_array($dealRs);
			//$numRows = mysql_num_rows($dealRs);
			}
		
		}
		if(!empty($dealQuery))
			$dealRs = mysql_query($dealQuery) or die(__LINE__." Query Error:".mysql_error());		
		
		while($row = mysql_fetch_array($dealRs))
		{
			$deal_balance = $row["balance"];
			//debugDealSheet($deal_balance."-->".$transBalance);
			if($deal_balance >= $transBalance)
			 {
				$payOutFlag = true;
				$status = 'CL';
				$deal_balance = $deal_balance - $transBalance; 
				$dealExistingPL = $row["profit_loss"]; 
				$dealLocal = $row["rate"] * $transBalance;
				$pl = $dealLocal-$localAmount;
				$dealExistingPL += $pl; 
				$strUpdateSql = "update deal_sheet set balance='".$deal_balance."', updated = '".$updatedDate."',profit_loss = '".$dealExistingPL."' where id='".$row["id"]."'";
				//debugDealSheet($strUpdateSql);
				mysql_query($strUpdateSql) or die(__LINE__."Query Error:".mysql_error());
				$strInsertSql = "INSERT INTO deal_trans_details 
															(
															id,
															transID,
															refNumberIM,
															currencyFrom,
															currencyTo,
															transAmount,
															localAmount,
															exchangeRate,
															customerID,
															PL,
															transDate,
															created 
															)
													  VALUES(
															'".$row["id"]."',
															'".$transID."',
															'".$refNumberIM."',
															'".$currencyFrom."',
															'".$currencyTo."',
															'".$transBalance."',
															'".$localAmount."',
															'".$exchangeRate."',
															'".$customerID."',
															'".$pl."',
															'".$transDate."',
															'".$created."'
															)";
				// debugDealSheet($strInsertSql);
				 mysql_query($strInsertSql) or die(__LINE__."Query Error:".mysql_error());
			
			 }
			 else
			 {
			    $status = 'BL';
				$payOutFlag = false;
			 }
		}
		//debugDealSheet($payOutFlag);	
		if(!$payOutFlag)
		{
			 $status = 'BL';
			 $query = "SELECT cif_id FROM deal_insufficient_funds WHERE transID = '".$transID."'";
			// debug($query);
			 $rs = mysql_query($query)or die(__LINE__."Query Error".mysql_error());
			 $record = mysql_fetch_array($rs);
			 //debug($record["cif_id"]);
				 if(empty($record["cif_id"]))
				 {
					 $strInsertSql = "INSERT INTO deal_insufficient_funds 
																(
																id,
																transID,
																customerID,
																status,
																created,
																updated
																)
														  VALUES(
																'".$row["id"]."',
																'".$transID."',
																'".$customerID."',
																'".$status."',
																'".$created."',
																'".$updatedDate."'
																)";
					// debugDealSheet($strInsertSql);
					 mysql_query($strInsertSql) or die(__LINE__."Query Error:".mysql_error());
			 }
		
		}				
		return 	$payOutFlag;	
}
/*function gateway($name, $arguments=false, $value=false)
{
	
	//while($record = mysql_fetch_assoc($result))
	$returnValue = '';
	
	if (is_callable($name)) {
	    
	    $ret = call_user_func($name, $arguments, $value);
	   
	   // $ret = call_user_func($cb3, $arg1, $arg2);
	  return $ret;  
	}else{
			echo("Call to undefined function as ".$record[$i]["name"]);
		}
		
	return $returnValue;	
}*/
function debugDealSheet( $variableToDebug = "", $shouldExitAfterDebug = false )
{
	if ( defined("DEBUG_ON")
		 && DEBUG_ON == true )
	{
		$arrayFileDetails = debug_backtrace();
		echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
	
		if ( !empty($variableToDebug)  )
		{
			echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
		}
		else
		{
			echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
		}
		echo "</pre>";
		if ($shouldExitAfterDebug)
			exit();
	}
}
?>