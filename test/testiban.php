<?
require_once("../admin/lib/iban.php");

$valid_iban = array(
    'DE89 3704 0044 0532 0130 00',
    'DE89 370400440532 013000',
    'IE29 AIBK 9311 5212 3456 78',
    'IE29AIBK93115212345678',
    'AD12 0001 2030 2003 5910 0100',
    'AT61 1904 3002 3457 3201',
    'BE68 5390 0754 7034',
    ' CY17 0020 0128 0000 0012 0052 7600',
    'CZ65 0800 0000 1920 0014 5399',
    'DK50 0040 0440 1162 43',
    ' EE38 2200 2210 2014 5685',
    ' FI21 1234 5600 0007 85',
    'FR14 2004 1010 0505 0001 3M02 606',
    ' DE89 3704 0044 0532 0130 00',
    ' GI75 NWBK 0000 0000 7099 453',
    ' GR16 0110 1250 0000 0001 2300 695',
    'HU42 1177 3016 1111 1018 0000 0000',
    'IS14 0159 2600 7654 5510 7303 39',
    ' IE29 AIBK 9311 5212 3456 78',
    ' IT60 X054 2811 1010 0000 0123 456',
    ' LV80 BANK 0000 4351 9500 1',
    ' LT12 1000 0111 0100 1000',
    ' LU28 0019 4006 4475 0000',
    ' NL91 ABNA 0417 1643 00',
    ' NO93 8601 1117 947',
    ' PL27 1140 2004 0000 3002 0135 5387',
    ' PT50 0002 0123 1234 5678 9015 4',
    'SK31 1200 0000 1987 4263 7541',
    'SI56 1910 0000 0123 438',
    ' ES80 2310 0001 1800 0001 2345',
    'SE35 5000 0000 0549 1000 0003',
    ' CH39 0070 0115 2018 4917 3',
    ' GB29 NWBK 6016 1331 9268 19',
);

$invalid_iban = array(
    'DE89 370400440532 130000',
    'DE89 370400440532 031000',
    'XX89 370400440532 031000',
);

$failures = 0;
$successes = 0;

/* test valid ibans */
$test = 0;

for ($i=0; $i<count($valid_iban); $i++) {
    $iban = $valid_iban[$i];
    print "TEST ".$test.": valid IBAN   [".$iban."] - ";
    if (validateIban($iban)) {
        print "SUCCESS\n";
        $successes++;
    }
    else {
        print "FAILURE\n";
        $failures++;
    }
    $test++;
}

for ($i=0; $i<count($invalid_iban); $i++) {
    $iban = $invalid_iban[$i];
    print "TEST ".$test.": invalid IBAN [".$iban."] - ";
    if (!validateIban($iban)) {
        print "SUCCESS\n";
        $successes++;
    }
    else {
        print "FAILURE\n";
        $failures++;
    }
    $test++;
}

if ($failures) {
    print "################ error #################\n";
    print "there were $failures failures\n";
}
else {
    print "=============== success ================\n";
    print "all tests succeeded!\n";
}

?>
