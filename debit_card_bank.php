<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Confirm Page
	 * @author: Awais Umer
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "javascript/audit-functions.php";
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	
	dbConnect();
	include_once "includes/functions.php";
	
	
	
	$strUserId=$_SESSION['loggedInUser']['accountName'];
	$strEmail=$_SESSION['loggedInUser']['email'];
	 
if($_POST["existBen"] != ""){
$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = ".$_POST["existBen"]." ";
$fetPreviousArr = selectFrom($getPreviousArr);	
	
	$updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
	 
	accountNo	=	'".$_POST["accountNumber"]."',
	sortCode	=	'".$_POST["sortCode"]."',
	iban		=	'".$_POST["iban"]."'
	WHERE benId = 	".$_POST["existBen"]."
	";
	if(update($updateBenBank)){
	
	activities($_SESSION["loginHistoryID"],"UPDATED",$_SESSION["loggedInUser"]["accountName"],"ben_banks","ben_banks updated successfully");

logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$_POST["existBen"],$_SESSION["loggedInUser"]["accountName"]);
	
	}
	}
?>

<!DOCTYPE HTML>
<html>
	<head>
	<style>
  form input {
   width :50%;
  }
  </style>
	
		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		 <link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->

	<script src="scripts/jquery-1.7.2.min.js"></script>
	<script src="scripts/jquery.validate.js"></script>
		 <script type="text/javascript">
		
		
		
	/*  $(document).ready(function() {
   $("#proceed").click(function(){
								window.btn_clicked = true;
								}
								);
   
   $(window).bind("beforeunload",function(event) {
    if(!window.btn_clicked){
        
		if(/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
        if(confirm("You will not get the rate as good as this for your transaction. Are you sure you want to lose it?")) {
            history.go();
        } else {
            window.setTimeout(function() {
                window.stop();
            }, 1);
        }
    } else {
        return "You will not get the rate as good as this for your transaction. Are you sure you want to lose it?";
    }
    }
										  }
	);

								}
								);
				
				 */

</script type="text/javascript"> 
<script>
function getValue() {
  
  var carNumber = document.getElementById("CARDNO").value;
  var CVC = document.getElementById("CVC").value; 
  var carName = document.getElementById('CARDTIT').value;   
   
  if(carName == "")
  {
  alert("Please Enter Card Tittle");
  return false;
  }
  
  if(carNumber == "")
  {
    alert("Please Enter Card Number");
	return false ;
  
  }
  if(CVC == "")
  {
   
   alert("Please Enter CVC");
   return false;
  
  }
 
 return true ;

	
   }
</script>
<script>

function checkLength()
{
    var fieldLength = document.getElementById('CARDNO').value.length;
    //we want 16 number of character
    if(fieldLength <= 16){
        return true;
    }
    else
    {
        var str = document.getElementById('CARDNO').value;
        str = str.substring(0, str.length - 1);
        document.getElementById('CARDNO').value = str;
    }
}
</script>

	</head>
	<body>

<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>
				</div>
				
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link"  target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link" target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Debit Card</h2>
<?php include('top-menu.php');?>
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Debit Card Details</p>
<p class="content_subheading">Please enter debit card details and press confirm.</p>
</div>

<!-- content area left-->
<div class="content_area_left">

<form name="confirmTrans" id="confirmTrans" method="post" action="barclays_payment_confirm.php" onsubmit="return getValue()">

<div class="field_wrapper">
<label class="form_label">Card Title:<span style="color:red">*</span></label><br>
<input type="text" name="CARDTIT" id="CARDTIT" class="form_fields" >
</div>


<div class="field_wrapper">
<label class="form_label">Card Number(16 Digit):<span style="color:red">*</span></label><br>
<input type="text" name="CARDNO" id="CARDNO" class="form_fields" onInput="checkLength()">
<span class="please_do">Don't Put Spaces.</span>
</div>

<div class="field_wrapper">
<label class="form_label">Expiry date (mm/yy):<span style="color:red">*</span></label><br>
<select id="Months" class="select_mini" name="Months">
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>

<select id="Years" class="select_mini" name="Years" >

<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
<option value="32">32</option>
<option value="33">33</option>
<option value="34">34</option>
<option value="35">35</option>
<option value="36">36</option>
<option value="37">37</option>
<option value="38">38</option>
<option value="39">39</option>
<option value="40">40</option>
<option value="41">41</option>
<option value="42">42</option>

</select>
								
</div>

<div class="field_wrapper">
<label class="form_label">CVC:<span style="color:red">*</span></label><br>
<input type="text" name="CVC" id="CVC" class="form_fields">
<span class="please_do">last 3 digits on the back of your card.</span>
</div>

<div class="field_wrapper">
<input id="benName" name="benName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
						<input id="benCountry" name="benCountry" type="hidden" value="<? echo $_POST['benCountry'] ?>" />
                        <input id="benCurrency" name="benCurrency" type="hidden" value="<? echo $_POST['benCurrency'] ?>" />
						<input id="accountName" name="accountName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
                        <input id="accountNumber" name="accountNumber" type="hidden" value="<? echo $_POST['accountNumber'] ?>" />
						<input id="branchNameNumber" name="branchNameNumber" type="hidden" value="<? echo $_POST['branchNameNumber'] ?>" />
                        <input id="branchAddress" name="branchAddress" type="hidden" value="<? echo $_POST['branchAddress'] ?>" />
						<input id="routingNumber" name="routingNumber" type="hidden" value="<? echo $_POST['routingNumber'] ?>" />
                        <input id="sortCode" name="sortCode" type="hidden" value="<? echo $_POST['sortCode'] ?>" />
                        <input id="swift" name="swift" type="hidden" value="<? echo $_POST['swift'] ?>" />
                        <input id="reference" name="reference" type="hidden" value="<? echo $_POST['reference'] ?>" />
                        <input id="existBen" name="existBen" type="hidden" value="<? echo $_POST['existBen'] ?>" />
                        <input id="iban" name="iban" type="hidden" value="<? echo $_POST['iban'] ?>" />
                        <input type="hidden" name="transType" value="Bank Transfer" />
	<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? echo $_POST['sendingCurrency'] ?>" />

</div>
<div class="field_wrapper">

<input name="Continue" id="Continue" type="submit"  value="Confirm Transaction" class="submit_btn"/>
</div>

</form>
</div>

<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password"><p>Please enter debit card details and press confirm. CVC number is last 3 digits on the back of your card</p></div>-->
</div>


</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php'); ?>
</div>



</div>
</body>
</html>
