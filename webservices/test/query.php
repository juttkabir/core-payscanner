<?php
/**
 * Created by PhpStorm.
 * Date: 09.04.19
 * Time: 23:38
 */

//require_once("../../include/functions.php");
//require_once("../soapConfig.php");
//require_once("../utils/getTransactionsConstrains.php");
//require_once("../utils/utils.php");

function SelectMultiRecordsSql($sql, $objDbLink)
{
    if((@$result = mysql_query ($sql, $objDbLink))==FALSE)
    {
        if(defined("DEBUG_ON"))
            echo mysql_sql_message($sql);
        return false;
    }
    else
    {
        $count = 0;
        $data = array();
        while ( $row = mysql_fetch_array($result, MYSQL_ASSOC))
        {
            $data[$count] = $row;
            $count++;
        }
        return $data;
    }
}

$payexLink = mysql_connect('payscanner_db', 'root', '111111');
mysql_select_db('payscanner', $payexLink);


$tieupNumber = '100618';

$transQuery = "SELECT
            t.benAgentID as tieupNumber,
			t.refNumberIM as transRefNumber,
			CONCAT(TRIM(c.firstName), ' ', TRIM(c.middleName), ' ', TRIM(c.lastName)) as senderName,
			CONCAT(TRIM(b.firstName), ' ', TRIM(b.middleName), ' ', TRIM(b.lastName)) as beneficiaryName,
			t.transAmount as sendingAmount,
			t.exchangeRate as exchangeRate,
			t.currencyTo as destCurrency,
			t.localAmount as destAmount,
			t.IMFee as Fee,
			t.transType as transType,
			banks.id as BankCode,
			bd.accNo as accountNumber
		FROM
			transactions t
		LEFT JOIN customer AS c ON c.customerID = t.customerID 
		LEFT JOIN beneficiary AS b ON b.benID = t.benID
		LEFT JOIN bankDetails AS bd ON bd.transID = t.transID
		LEFT JOIN banks ON banks.name = bd.bankName
		WHERE
			t.transStatus = 'Authorize'
			AND t.benAgentID = '" . $tieupNumber . "' 
        LIMIT 20";

$transResults = SelectMultiRecordsSql($transQuery,$payexLink);
//print_r($transResults);
$transactions = [];
if ($transResults){
    foreach ($transResults as $item){
        $arr = [];
        $arr['tieupNumber'] = $tieupNumber;
        $arr['transRefNumber'] = $item['transRefNumber'];
        $arr['senderName'] = $item['senderName'];
        $arr['beneficiaryName'] = $item['beneficiaryName'];
        $arr['sendingAmount'] = $item['sendingAmount'];
        $arr['exchangeRate'] = $item['exchangeRate'];
        $arr['destCurrency'] = $item['destCurrency'];
        $arr['destAmount'] = $item['destAmount'];
        $arr['Fee'] = $item['bankCharges'];
        $arr['transType'] = ($item['transType'] == 'Bank Transfer') ? 'AC' :
            ($item['transType'] == 'Pick up') ? 'COC' : $item['transType'];
        $arr['BankCode'] = $item['BankCode'];
        $arr['accountNumber'] = $item['accountNumber'];
        $arr['responseCode'] = '000';
        $arr['responseMessage'] = 'Successful';
        array_push($transactions, $arr);
        unset($arr);
    }
}else{
//    print_r($transResults);
}


if(!empty($transResults)) {
    $transactions = [];
//    foreach ($transResults as $item) {
//        $arr = [];
//        $arr['tieupNumber'] = $tieupNumber;
//        $arr['transRefNumber'] = $item['transRefNumber'];
//        $arr['senderName'] = $item['senderName'];
//        $arr['beneficiaryName'] = $item['beneficiaryName'];
//        $arr['sendingAmount'] = $item['sendingAmount'];
//        $arr['exchangeRate'] = $item['exchangeRate'];
//        $arr['destCurrency'] = $item['destCurrency'];
//        $arr['destAmount'] = $item['destAmount'];
//        $arr['Fee'] = $item['bankCharges'];
//        $arr['transType'] = ($item['transType'] == 'Bank Transfer') ? 'AC' :
//            ($item['transType'] == 'Pick up') ? 'COC' : $item['transType'];
//        $arr['BankCode'] = $item['BankCode'];
//        $arr['accountNumber'] = $item['accountNumber'];
//        $arr['responseCode'] = '000';
//        $arr['responseMessage'] = 'Successful';
//        array_push($transactions, $arr);
//        unset($arr);
//    }
//
//    print_r($transactions);

    foreach ($transResults as $item){
        $object = new stdClass();
        $object->tieupNumber = $tieupNumber;
        $object->transRefNumber = $item['transRefNumber'];
        $object->senderName = $item['senderName'];
        $object->beneficiaryName = $item['beneficiaryName'];
        $object->sendingAmount = $item['sendingAmount'];
        $object->exchangeRate = $item['exchangeRate'];
        $object->destCurrency = $item['destCurrency'];
        $object->destAmount = $item['destAmount'];
        $object->Fee = $item['Fee'];
        $object->transType = ($item['transType'] == 'Bank Transfer') ? 'AC' :
            ($item['transType'] == 'Pick up') ? 'COC' : $item['transType'];
        $object->BankCode = $item['BankCode'];
        $object->accountNumber = $item['accountNumber'];
        $object->responseCode = $item['responseCode'];
        $object->responseMessage = $item['responseMessage'];
        $transactions[] = $object;
        unset($object);
    }
    print_r($transactions);

    $arr = [];
    $mainObj = new stdClass();
    foreach ($transactions as $key => $value){
        $object = new stdClass();
        $object->tieupNumber = $value['tieupNumber'];
        $object->transRefNumber = $value['transRefNumber'];
        $object->senderName = $value['senderName'];
        $object->beneficiaryName = $value['beneficiaryName'];
        $object->sendingAmount = $value['sendingAmount'];
        $object->exchangeRate = $value['exchangeRate'];
        $object->destCurrency = $value['destCurrency'];
        $object->destAmount = $value['destAmount'];
        $object->Fee = $value['Fee'];
        $object->transType = $value['transType'];
        $object->BankCode = $value['BankCode'];
        $object->accountNumber = $value['accountNumber'];
        $object->responseCode = $value['responseCode'];
        $object->responseMessage = $value['responseMessage'];
        $arr[] = $object;
        $mainObj->$key->transaction = clone $object;
        unset($object);
    }

}