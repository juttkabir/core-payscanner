<?php

/* Requiring files */
//require_once("../include/masterConfig.php");

require_once("../include/functions.php");
require_once("soapConfig.php");
require_once("utils/getTransactionsConstrains.php");
require_once("utils/utils.php");
/*
*	getTransaction return transaction details based on refNumber, transType and status is 'Authorized'
*/
function getTransaction($apiCode, $tieupNumber, $transRefNumber, $transType){
	/* fetch request header information */
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,array("apiCode" =>$apiCode,"agentCode" =>  $tieupNumber, "refNumber" =>  $transRefNumber, "transType" => $transType));
	
	/* Temporary IPs Restriction  */	
	// if(!validateWhiteListIPs()){
		// $resHeader = setSoapHeader('Invalid credentials or IP','001',$tieupNumber,$transType);
		// $arrResponse = array("Transaction" => array());	
		// prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		// return $arrResponse;		
	// }	
	
	/* authenticate header info */	
	if(!empty($headerInfo)){
	
		/* connect with temp_mt database */	
		$mtlink = connectToMT();	
		if(!$mtlink){		
			$resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber,$transType);
			$arrResponse = array("Transaction" => array());	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
		
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$tieupNumber,$transType);
			$arrResponse = array("Transaction" => array());	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transType);
			$arrResponse = array("Transaction" => array());	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$tieupNumber,$transType);
			$arrResponse = array("Transaction" => array());	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
			return $arrResponse;
		}
		
	}else{	
		$resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}	
	/* authenticate header info end */
	
	/* Request field(s) validation */
	if(!validateGetTransRequest($apiCode, $tieupNumber, $transRefNumber, $transType)){	
		$resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}	
	
	if(!validateNumbers($apiCode) || strlen($apiCode) > 3){
		$resHeader = setSoapHeader('Invalid/Unauthorized API Code','010',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	if(!validateAlphNumeric($tieupNumber) && strlen($tieupNumber) > 20){
		$resHeader = setSoapHeader('Invalid/Unauthorized Tie-up ID','008',$tieupNumber,$transType);		
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	if(!validateAlphNumeric($transRefNumber) && strlen($transRefNumber) > 50){
		$resHeader = setSoapHeader('Invalid Trans Ref Number','011',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	if(!validateNumbers($transType) && strlen($transType) > 4){
		$resHeader = setSoapHeader('Invalid/Unauthorized Trans Type','009',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}	
	/* Request validation end */
	
	
	/* Getting global variables to be used */
	global $TRANSCTIONCODE;
	
	/* database connectivity */
	
	/*********************************************************************************************
	  Connection establish with payex database 
	  credentials get from middle tier
	*********************************************************************************************/
	
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	
	$payex_db = $arrDbConnection["client_database"];	
	if(!mysql_select_db($payex_db, $payexLink)){
		$resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array());	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}	
	
	/**
		If reference number is not mentioned, system will send all available transactions of mentioned type 
	*/	
	
	if(isset($transType) && !empty($transType)){
		if(array_key_exists($transType, $TRANSCTIONCODE)){		
			$tranWhereClause = " AND transType = '".$TRANSCTIONCODE[$transType]."' ";
		}
	}
	
	if(!empty($transRefNumber) || $transRefNumber != ""){
		$tranWhereClause .= " AND refNumber ='".mysql_real_escape_string($transRefNumber)."'";	
	}
	
	/* getTransaction Query */
	$transQuery = " SELECT
			t.refNumber,
			t.refNumberIM,
			t.currencyFrom,
			t.transAmount,
			t.exchangeRate,
			t.currencyTo,
			t.recievedAmount,
			t.bankCharges,
			t.AgentCommTax,
			t.settlementCurrency,
			t.customerID,
			t.benID,
			t.transactionPurpose,
			t.transType,
			t.benAgentID
		FROM
			transactions t
		WHERE
			t.transStatus = 'Authorize' $tranWhereClause ";	
	$transResult = selectSql($transQuery,$payexLink);
	
	$transactions = array();
	if(!empty($transResult)){	
		$transactions['tieupNumber'] = $tieupNumber;
		$transactions['originalApiCode'] = $apiCode;
		$transactions['transRefNumber'] = $transResult['refNumber'];
		$transactions['sendingCurrency'] = $transResult['currencyFrom'];
		$transactions['sendingAmount'] = $transResult['transAmount'];
		$transactions['exchangeRate'] = $transResult['exchangeRate'];
		$transactions['destCurrency'] = $transResult['currencyTo'];
		$transactions['destAmount'] = $transResult['recievedAmount'];
		$transactions['Fee'] = $transResult['bankCharges'];
		$transactions['Tax'] = $transResult['AgentCommTax'];
		$transactions['settlementCurrency'] = $transResult['settlementCurrency'];		
		$transactions['purposeOfTransaction'] = $transResult['transactionPurpose'];
		
		/* retrieving customers information */
		$customerInfo = array();
		if(!empty($transResult['customerID']) && trim($transResult['customerID']) != ''){		
			$customerInfo = getCustomerInfo(trim($transResult['customerID']),$payexLink);
		}
		
		/* retrieving beneficiary information */
		$beneficiaryInfo = array();
		if(!empty($transResult['benID']) && trim($transResult['benID']) != ''){		
			$beneficiaryInfo = getBeneficiaryInfo(trim($transResult['benID']),$payexLink);
		}
		
		/* preparing response */		
		$resHeader = setSoapHeader('Successful','000',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => array_merge(array_merge($transactions, $customerInfo), $beneficiaryInfo));
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;		
	}else{
		$resHeader = setSoapHeader('No record found','-001',$tieupNumber,$transType);
		$arrResponse = array("Transaction" => $transactions);
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransaction', $resHeader['errorCode']);
		return $arrResponse;		
	}
	
	/* Response return */
	
} // END getTransaction


/*
* 	sendTransaction store the transaction, customer, beneficiary and bank details 
*	information in system provided by third party
*/


function sendTransaction($objTrans){
	
	/* fetch request header information */
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,$objTrans);
		
	$mtlink = '';
	if(!empty($headerInfo)){
	
		/* connect with temp_mt database */			
		$mtlink = connectToMT();	
		if(!$mtlink){
			$resHeader = setSoapHeader('Correspondent server not responding','090',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '090','responseMessage' => 'Correspondent server not responding', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
		define("DATABASE",$arrDbConnection['client_database']);
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '097','responseMessage' => 'Invalid/Unauthorized Agent Id', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* IPs Restriction  */	
		if(!validateWhiteListIPs($arrDbConnection['allow_ips_list'])){
			$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials or IP', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;		
		}
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Agent Active/Inactive checking */
		if($arrDbConnection['status'] != 'Active'){
			$resHeader = setSoapHeader('Correspondent API interaction disabled','093',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '093','responseMessage' => 'Correspondent API interaction disabled', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
	
		 /* beneficiary Agent Check  */	
		if(!validatePayerList($arrDbConnection['accessable_ben_agent'],$objTrans->payourID)){
			$resHeader = setSoapHeader('Invalid Payour Id','116',$headerInfo->agentID,'014');
			
		
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '116','responseMessage' => 'Invalid Payour Id', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;		
		}
		
		
		
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '003','responseMessage' => 'Invalid Token', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Checking if Method is accessible  */
		if(!strpos($arrDbConnection['accessable_methods'],'sendTransaction')){
			$resHeader = setSoapHeader('You do not have permission to call this method.','115',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '115','responseMessage' => 'You do not have permission to call this method.', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
	}else{
		$resHeader = setSoapHeader('Message format error','096',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '096','responseMessage' => 'Message format error', 'Remarks' => '');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		return $arrResponse;	
	}
	
	
	global $mapping, $mandatory_fields;
	
	###########Manual validation########
		if(property_exists($objTrans,'sendingCurrency')==false)
		{ 
		$resHeader = setSoapHeader('Missing sendingCurrency','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing sendingCurrency', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		}  
		
			if(property_exists($objTrans,'destCurrency')==false)
		{ 
		$resHeader = setSoapHeader('Missing destCurrency','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing destCurrency', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		}  
		
		// if(property_exists($objTrans,'senderDOB')==false)
		// { 
		// $resHeader = setSoapHeader('Missing senderDOB','029',$headerInfo->agentID,'014');
		// $arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing senderDOB', 'Remarks' => '');
		// 	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		// 	return $arrResponse;	
		// }  
		
		if(property_exists($objTrans,'senderPOBox')==false)
		{ 
		$resHeader = setSoapHeader('Missing SenderPOBox','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing SenderPOBox', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		} 
		
			if(property_exists($objTrans,'senderCountry')==false)
		{ 
		$resHeader = setSoapHeader('Missing senderCountry','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing senderCountry', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		} 
		
		// if(property_exists($objTrans,'benePhone')==false)
		// { 
		// $resHeader = setSoapHeader('Missing benePhone','029',$headerInfo->agentID,'014');
		// $arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing benePhone', 'Remarks' => '');
		// 	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		// 	return $arrResponse;	
		// } 
		
		// 	if(property_exists($objTrans,'beneMobilePhone')==false)
		// { 
		// $resHeader = setSoapHeader('Missing beneMobilePhone','029',$headerInfo->agentID,'014');
		// $arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing beneMobilePhone', 'Remarks' => '');
		// 	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		// 	return $arrResponse;	
		// } 
			if(property_exists($objTrans,'purposeOfTransaction')==false)
		{ 
		$resHeader = setSoapHeader('Missing purposeOfTransaction','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing purposeOfTransaction', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		}
		
		// 	if(property_exists($objTrans,'benePhotoIdExpDate')==false)
		// { 
		// $resHeader = setSoapHeader('Missing benePhotoIdExpDate','029',$headerInfo->agentID,'014');
		// $arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing benePhotoIdExpDate', 'Remarks' => '');
		// 	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		// 	return $arrResponse;	
		// }
		// 	if(property_exists($objTrans,'beneVerificationID')==false)
		// { 
		// $resHeader = setSoapHeader('Missing beneVerificationID','029',$headerInfo->agentID,'014');
		// $arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing beneVerificationID', 'Remarks' => '');
		// 	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		// 	return $arrResponse;	
		// }
			if(property_exists($objTrans,'beneficiaryBankCode')==false)
		{ 
		$resHeader = setSoapHeader('Missing beneficiaryBankCode','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing beneficiaryBankCode', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		}
			if(property_exists($objTrans,'beneficiaryBankCode')==false)
		{ 
		$resHeader = setSoapHeader('Missing beneficiaryBankCode','029',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing beneficiaryBankCode', 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;	
		}
	###########Manual validation########
	
	
	$strValidateTransResponse = validateSenderInfo($objTrans, $mapping, $mandatory_fields);


				 
					
					
	if($strValidateTransResponse == "empty" || (trim($strValidateTransResponse) == '' && empty($strValidateTransResponse))){
	
		$resHeader = setSoapHeader('Message format error','096',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '096','responseMessage' => 'Message format error', 'Remarks' => '');	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		return $arrResponse;		
	}
	
	if(strpos($strValidateTransResponse,"::") && !is_array($strValidateTransResponse)){	
		$arrError = explode("::", $strValidateTransResponse);		
		if($arrError[1] == 'string' || $arrError[1] == 'integer' || $arrError[1] == 'date' || $arrError[1] == 'email'){
			$resHeader = setSoapHeader('Invalid '.$arrError[0],'014',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '014','responseMessage' => 'Invalid  '.$arrError[0], 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;					
		}elseif($arrError[1] == 'missing'|| $arrError[1] == 'Missing'){
			$resHeader = setSoapHeader('Missing '.$arrError[0],'029',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Missing '.$arrError[0], 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;
		}elseif($arrError[1] == 'limitexceeded'){
			$resHeader = setSoapHeader('Field Limit exceeded for '.$arrError[0],'001',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Field Limit exceeded for '.$arrError[0], 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;
		}elseif($arrError[1] == 'undefined'){
			$resHeader = setSoapHeader('Undefined '.$arrError[0],'001',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Undefined '.$arrError[0], 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}elseif($arrError[1] == 'incorrect'){
			global $blya;
			$resHeader = setSoapHeader('Incorrect '.$arrError[0],'001',$headerInfo->agentID,'014');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Incorrect '.$arrError[0], 'Remarks' => '');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
			return $arrResponse;			
		}
	}
	
	
	/* database connectivity */
	
	/*********************************************************************************************
	  Connection establish with payex database 
	  credentials get from middle tier
	*********************************************************************************************/
	
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	
	$payex_db = $arrDbConnection["client_database"];	
	if(!mysql_select_db($payex_db, $payexLink)){	
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials', 'Remarks' => '');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	/* Default transaction status */
	$strInitialTransStatus = $arrDbConnection['initial_trans_status'];	
	
	if(!isset($strValidateTransResponse['customer.agentID']) && empty($strValidateTransResponse['customer.agentID'])){
		$strValidateTransResponse['customer.agentID'] = (string)$headerInfo->agentID;
	}
	
	if (is_callable(CONFIG_FUNCTION_BEFORE_INSERTION_WS))
	{
		$strValidateTransResponse = call_user_func(CONFIG_FUNCTION_BEFORE_INSERTION_WS, $strValidateTransResponse);	
	}
	else
	{
		$strValidateTransResponse = $strValidateTransResponse;
	}
	
	/* Checking country Code */	
	if(is_string($strValidateTransResponse)){
		$arrResponse = explode("::", $strValidateTransResponse);
		$resHeader = setSoapHeader($arrResponse[0],$arrResponse[1],$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => $arrResponse[1],'responseMessage' => $arrResponse[0], 'Remarks' => '');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;
	}
	/* END */	
	
	/* checking duplication */
	
	$tranDupQuery = " SELECT count(transID) as count FROM transactions WHERE refNumber = '".$objTrans->transRefNumber."'  AND transStatus !='Delete' ";

	$tranDupResultSet = selectFrom($tranDupQuery);
	if($tranDupResultSet['count'] > 0){
		$resHeader = setSoapHeader('Duplicate Record','097',$headerInfo->agentID,'014');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '097','responseMessage' => 'Duplicate Record', 'Remarks' => '');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
		return $arrResponse;
	}
	/* end duplication */	
	
	/* adding and fetching response of transaction */	
	$strValidateTransResponse['transactions.transStatus'] = $strInitialTransStatus;
	$strValidateTransResponse['customer.agentID'] = $headerInfo->agentID;
	$strValidateTransResponse['transactions.IMFee'] = $objTrans->Fee;
	$strValidateTransResponse['ben_banks.bankId'] = $objTrans->beneficiaryBankCode;
	$strValidateTransResponse['bankDetails.accNo'] = $objTrans->beneficiaryAccNo;
	$transResponse = insertPayexData($strValidateTransResponse);
	
	/* Preparing response to send back to client */
	$arrTransResponse = array(
		'tieupNumber' => $objTrans->tieupNumber,
		'transRefNumber' => $objTrans->transRefNumber,
		'Remarks' => base64_encode(json_encode($GLOBALS['compliance_remarks'])),
	);	
	unset($GLOBALS['compliance_remarks']);
	if($transResponse == '1'){
		$resHeader = setSoapHeader('Successful','000',$headerInfo->agentID,'014');
		$arrTransResponse['responseCode'] = '000';
		$arrTransResponse['responseMessage'] = 'Successful';
	}elseif($transResponse == '2'){
		$resHeader = setSoapHeader('Transaction successful but beneficiary fall in partial match','105',$headerInfo->agentID,'014');
		$arrTransResponse['responseCode'] = '105';
		$arrTransResponse['responseMessage'] = 'Transaction successful but beneficiary fall in partial match';	
	}elseif($transResponse == '3'){
		$resHeader = setSoapHeader('Transaction successful but sender fall in partial match','103',$headerInfo->agentID,'014');
		$arrTransResponse['responseCode'] = '103';
		$arrTransResponse['responseMessage'] = 'Transaction successful but sender fall in partial match';	
	}elseif($transResponse == '4'){
		$resHeader = setSoapHeader('Others. Please contact Payex support for details','106',$headerInfo->agentID,'014');
		$arrTransResponse['responseCode'] = '106';
		$arrTransResponse['responseMessage'] = 'Others. Please contact Payex support for details';	
	}
	/* Response */
	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrTransResponse), $headerInfo, 'sendTransaction', $resHeader['errorCode']);
	return $arrTransResponse;
	
} // END sendTransaction



/*
* 	addSender store the sender information in system provided by third party
*/
function addSender($objSender){
	/* fetch request header information */
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,$objSender);
	
	/* Temporary IPs Restriction  */	
	if(!validateWhiteListIPs()){
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'018');
		$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');		
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	if(!empty($headerInfo)){
	
		/* connect with temp_mt database */			
		$mtlink = connectToMT();	
		if(!$mtlink){
			$resHeader = setSoapHeader('Correspondent server not responding','090',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '090',"responseMessage" => 'Correspondent server not responding');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
				
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '097',"responseMessage" => 'Invalid/Unauthorized Agent Id');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;			
		}		
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '003',"responseMessage" => 'Invalid Token');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;
		}
		
	}else{
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'018');
		$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;		
	}

	
	/* authenticate header info end */	
	
	global $senderMapping, $senderFieldChecks;
	$strValidateSenderResponse = validateSenderInfo($objSender, $senderMapping, $senderFieldChecks);
	if($strValidateSenderResponse == "empty"){
		$resHeader = setSoapHeader('Sender Reference not found','100',$headerInfo->agentID,'018');
		$arrResponse = array("SenderID" => '',"responseCode" => '100',"responseMessage" => 'Sender Reference not found');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;		
	}	
	if(strpos($strValidateSenderResponse,"::") && !is_array($strValidateSenderResponse)){	
		$arrError = explode("::", $strValidateSenderResponse);		
		if($arrError[1] == 'string' || $arrError[1] == 'integer' || $arrError[1] == 'date' || $arrError[1] == 'email'){
			$resHeader = setSoapHeader('Invalid '.$arrError[0],'014',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Invalid '.$arrError[0]);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;			
			
		}elseif($arrError[1] == 'Missing'){
			$resHeader = setSoapHeader('Missing '.$arrError[0],'001',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Missing '.$arrError[0]);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;			
		}elseif($arrError[1] == 'limitexceeded'){
			$resHeader = setSoapHeader('Field Limit exceeded for '.$arrError[0],'001',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Field Limit exceeded for '.$arrError[0]);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;
		}elseif($arrError[1] == 'undefined'){
			$resHeader = setSoapHeader('Undefined '.$arrError[0],'???',$headerInfo->agentID,'018');
			$arrResponse = array("SenderID" => '',"responseCode" => '001',"responseMessage" => 'Undefined '.$arrError[0]);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
			return $arrResponse;			
		}
	}
	
	/*********************************************************************************************
	  Connection establish with payex database 
	  credentials get from middle tier
	*********************************************************************************************/
	
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	
	
	$payex_db = $arrDbConnection["client_database"];	
	if(!mysql_select_db($payex_db, $payexLink)){	
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'018');
		$arrResponse = array('SenderID' => $objSender->SenderID,'responseCode' => '001','responseMessage' => 'Invalid credentials');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;		
	}
		
	/* checking duplication ... */
	
	$custDupQuery = " SELECT count(customerID) as count FROM customer WHERE customerID = '".$objSender->SenderID."'";
	$custDupResultSet = selectFrom($custDupQuery);	
	if($custDupResultSet['count'] > 0){
		$resHeader = setSoapHeader('Duplicate Record','097',$headerInfo->agentID,'018');
		$arrResponse = array("SenderID" => '',"responseCode" => '097',"responseMessage" => 'Duplicate Record');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
		return $arrResponse;		
	}
	

	/* end duplication */
	
	if (is_callable(CONFIG_FUNCTION_BEFORE_INSERTION_WS))
	{
		$strValidateSenderResponse = call_user_func(CONFIG_FUNCTION_BEFORE_INSERTION_WS, $strValidateSenderResponse, 'customer');	
	}
	else
	{
		$strValidateSenderResponse = $strValidateSenderResponse;
	}
	/* including agentID */
	if(!isset($strValidateSenderResponse['customer.agentID']) && !empty($strValidateSenderResponse['customer.agentID'])){
		$strValidateSenderResponse['customer.agentID'] = $headerInfo->agentID;
	}
	
	$custID = insertCustomerInPayex($strValidateSenderResponse);
	
	$arrResponse = array("SenderID" => $custID);
	$resHeader = '';
	if(!empty($custID) && $custID != ''){
		$resHeader = setSoapHeader('Successful','000',$headerInfo->agentID,'018');
		$arrResponse['responseCode'] = '000';
		$arrResponse['responseMessage'] = 'Successful';
	}else{
		$resHeader = setSoapHeader('Others. Please contact Payex support for details','106',$headerInfo->agentID,'018');
		$arrResponse['responseCode'] = '106';
		$arrResponse['responseMessage'] = 'Others. Please contact Payex support for details';	
	}	
	prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSender', $resHeader['errorCode']);
	return $arrResponse;
} // END addSender


/*
* 	addBeneficiary store the beneficiary information in system provided by third party
*/


function addBeneficiary($beneObject){	
	/* fetch request header information */
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,$objTrans);
	
	/* Temporary IPs Restriction  */	
	if(!validateWhiteListIPs()){
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'020');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
		return $arrResponse;		
	}
	
	$mtlink = '';
	if(!empty($headerInfo)){
	
		/* connect with temp_mt database */			
		$mtlink = connectToMT();	
		if(!$mtlink){
			$resHeader = setSoapHeader('Correspondent server not responding','090',$headerInfo->agentID,'020');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '090','responseMessage' => 'Correspondent server not responding');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
		
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$headerInfo->agentID,'020');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '097','responseMessage' => 'Invalid/Unauthorized Agent Id');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'020');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$headerInfo->agentID,'020');
			$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '003','responseMessage' => 'Invalid Token');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
	}else{
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'020');
		$arrResponse = array('tieupNumber' => $objTrans->tieupNumber,'transRefNumber' => $objTrans->transRefNumber,	'responseCode' => '001','responseMessage' => 'Invalid credentials');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addBeneficiary', $resHeader['errorCode']);
		return $arrResponse;	
	}
	
	global $senderFieldChecks, $beneficiaryMapping;
	
	$arrResponse = array(
		'beneficiaryReference' => $beneObject->beneficiaryReference,
		'responseCode' => '000',
		'responseMessage' => 'Successful',
	);
	return $arrResponse;
	
} // END addBeneficiary


/*
* 	addSenderBank store the sender bank information in system provided by third party
*/
function addSenderBank($objSenderBank){

/* fetch request header information */
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,$objSenderBank);

	/* Temporary IPs Restriction  */	
	if(!validateWhiteListIPs()){
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'022');
		$arrResponse = array("uniqueIDinPayex" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');		
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	if(!empty($headerInfo)){
	
		/* connect with temp_mt database */			
		$mtlink = connectToMT();	
		if(!$mtlink){
			$resHeader = setSoapHeader('Correspondent server not responding','090',$headerInfo->agentID,'022');
			$arrResponse = array("bankCode" => '',"payexBankId" => '',"responseCode" => '090',"responseMessage" => 'Correspondent server not responding');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
				
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$headerInfo->agentID,'022');
			$arrResponse = array("bankCode" => '',"payexBankId" => '',"responseCode" => '097',"responseMessage" => 'Invalid/Unauthorized Agent Id');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
			return $arrResponse;			
		}		
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'022');
			$arrResponse = array("bankCode" => '',"payexBankId" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$headerInfo->agentID,'022');
			$arrResponse = array("bankCode" => '',"payexBankId" => '',"responseCode" => '003',"responseMessage" => 'Invalid Token');
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
			return $arrResponse;
		}
		
	}else{
		$resHeader = setSoapHeader('Invalid credentials','001',$headerInfo->agentID,'022');
		$arrResponse = array("bankCode" => '',"payexBankId" => '',"responseCode" => '001',"responseMessage" => 'Invalid credentials');
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'addSenderBank', $resHeader['errorCode']);
		return $arrResponse;		
	}
	$arrResponse = array("bankCode" => $objSenderBank->bankCode, "payexBankId" => '123',"responseCode" => '000',"responseMessage" => 'Successful');
	return $arrResponse;	
	
} // END addSenderBank


function getCustomerInfo($custID, $dbLink){
	$custInfo = array();
	$custQuery = "SELECT
					c.firstName,
					c.middleName,
					c.lastName,
					c.Phone,
					c.Mobile,
					c.fax,
					c.email,
					c.Address,
					c.Address1,
					c.Country,
					c.State,
					c.City,
					c.Zip,
					bd.bankID,
					bd.BankCode,
					bd.branchCode,
					b.country,
					bd.BranchCity,
					c.accountName,
					bd.accNo,
					bd.accountType,
					bd.sortCode,
					bd.swiftCode,
					bd.IBAN,					
					bd.routingNumber					
				FROM
					customer c				
				LEFT JOIN bankDetails bd ON c.customerID = bd.custID
				LEFT JOIN banks b ON bd.bankID = b.bankId
				LEFT JOIN accounts a ON c.customerID = a.userId				
				WHERE customerID = '".$custID."' ";				
		$custResult = selectSql($custQuery,$dbLink);
		
		$userTypeQuery = " SELECT		
					ut.id_number,
					ut.expiry_date,
					it.title
				FROM
					user_id_types ut
				LEFT JOIN id_types it on ut.id_type_id = it.id
				WHERE ut.user_id = '".$custID."' AND ut.user_type = 'C' LIMIT 0,2 ";
						
		$userTypeResult = SelectMultiRecordsSql($userTypeQuery,$dbLink);
		
		$custInfo['senderReference'] = '';
		$custInfo['senderFirstName'] = $custResult['firstName'];
		$custInfo['senderMiddleName'] = $custResult['middleName'];
		$custInfo['senderLastName'] = $custResult['lastName'];
		$custInfo['senderPOBox'] = $custResult['Zip'];
		$custInfo['senderAddress1'] = $custResult['Address'];
		$custInfo['senderAddress2'] = $custResult['Address1'];
		$custInfo['senderCountry'] = $custResult['Country'];
		$custInfo['senderState'] = $custResult['State'];
		$custInfo['senderCity'] = $custResult['City'];
		$custInfo['senderAddressZip'] = $custResult['Zip'];
		$custInfo['senderPhone'] = $custResult['Phone'];
		$custInfo['senderMobile'] = $custResult['Mobile'];
		$custInfo['senderFax'] = $custResult['fax'];
		$custInfo['senderEmail'] = $custResult['email'];
		$custInfo['senderID1Type'] = '';
		$custInfo['senderID1No'] = '';
		$custInfo['senderID1expiryDate'] = '';
		$custInfo['senderID2Type'] = '';
		$custInfo['senderID2No'] = '';
		$custInfo['senderID2expiryDate'] = '';
		if(isset($userTypeResult['0']) && !empty($userTypeResult['0'])){
			$custInfo['senderID1Type'] = $userTypeResult['0']['title'];
			$custInfo['senderID1No'] = $userTypeResult['0']['id_number'];
			$custInfo['senderID1expiryDate'] = $userTypeResult['0']['expiry_date'];
		}
		if(isset($userTypeResult['1']) && !empty($userTypeResult['1'])){
			$custInfo['senderID2Type'] = $userTypeResult['1']['title'];
			$custInfo['senderID2No'] = $userTypeResult['1']['id_number'];
			$custInfo['senderID2expiryDate'] = $userTypeResult['1']['expiry_date'];
		}
		$custInfo['senderBankID'] = $userTypeResult['bankID'];
		$custInfo['senderBankCode'] = $custResult['BankCode'];
		$custInfo['senderBankCountry'] = $custResult['country'];
		$custInfo['senderBankCity'] = $custResult['BranchCity'];
		$custInfo['senderAccountTitle'] = $custResult['accountName'];
		$custInfo['senderAccNo'] = $custResult['accNo'];
		$custInfo['senderAccountType'] = $custResult['accountType'];
		$custInfo['senderBranchNumber'] = $custResult['branchCode'];
		$custInfo['senderSortCode'] = $custResult['sortCode'];
		$custInfo['senderSwiftCode'] = $custResult['swiftCode'];
		$custInfo['senderIBAN'] = $custResult['IBAN'];
		$custInfo['senderRroutingNumber'] = $custResult['routingNumber'];		
		return $custInfo;
}
function getBeneficiaryInfo($beneID, $dbLink){
	$beneInfo = array();
	$beneQuery = "SELECT
			b.firstName,
			b.middleName,
			b.lastName,	
			b.Address,
			b.Address1,
			b.Address3,
			b.Address4,
			b.Phone,
			b.Mobile,				
			b.Country,
			b.State,
			b.City,
			b.Zip,
			b.dob,
			b.SOF,
			b.Citizenship,
			b.email,
			b.IDexpirydate,
			b.IDissuedate,
			b.IDType,
			b.IDNumber,
			b.beneOccupation,	
			b.beneBirthCountry,	
			b.beneNationalityCountry,	
			b.beneNationalityAtBirthCountry,	
			bb.bankId,		
			bk.country,
			bk.branchCode,
			bb.BankCity,
			bb.AccountTitle,
			bb.accountNo,
			bb.accountType,
			bb.branch_id,
			bb.sortCode,
			bb.swiftCode,
			bb.IBAN,
			bb.routingNumber
		FROM
			beneficiary b
		LEFT JOIN ben_banks bb ON b.benID = bb.benId
		LEFT JOIN banks bk ON bb.bankId = bk.bankId
		WHERE b.benID = '" .$beneID ."'";
	$beneResult = selectSql($beneQuery, $dbLink);
	
	$userTypeQuery = " SELECT		
					it.category,
					it.title,
					ut.countryName,
					ut.expiry_date,
					ut.issue_date
				FROM
					user_id_types ut
				LEFT JOIN id_types it on ut.id_type_id = it.id
				WHERE ut.user_id = '".$beneID."' AND ut.user_type = 'B' LIMIT 0,1 ";
						
	$userTypeResult = SelectMultiRecordsSql($userTypeQuery,$dbLink);
	
	
	
	$beneInfo['beneficiaryReference'] = 'Yes'; // will update later
	$beneInfo['beneficiaryFirstName'] = $beneResult['firstName'];
	$beneInfo['beneficiaryMiddlleName'] = $beneResult['middleName'];
	$beneInfo['beneficiaryLastName'] = $beneResult['lastName'];
	$beneInfo['beneAddress'] = $beneResult['Address'];
	$beneInfo['beneAddress2'] = $beneResult['Address1'];
	$beneInfo['beneAddress3'] = $beneResult['Address3'];
	$beneInfo['beneAddress4'] = $beneResult['Address4'];
	$beneInfo['benePhone'] = $beneResult['Phone'];
	$beneInfo['beneMobilePhone'] = $beneResult['Mobile'];
	$beneInfo['beneCountry'] = $beneResult['Country'];
	$beneInfo['beneState'] = $beneResult['State'];
	$beneInfo['beneCity'] = $beneResult['beneCity'];
	$beneInfo['beneZipCode'] = $beneResult['Zip'];;
	$beneInfo['beneDOB'] = $beneResult['dob'];
	$beneInfo['beneOccupation'] = $beneResult['beneOccupation'];
	$beneInfo['beneBirthCity'] = $beneResult['Citizenship'];
	$beneInfo['beneBirthCountry'] = $beneResult['beneBirthCountry'];
	$beneInfo['beneNationalityCountry'] = $beneResult['beneNationalityCountry'];
	$beneInfo['beneNationalityAtBirthCountry'] = $beneResult['beneNationalityAtBirthCountry'];	
	$beneInfo['relationToSender'] = $beneResult['SOF'];
	$beneInfo['beneEmailAddress'] = $beneResult['email'];
	$beneInfo['benePhotoIDCat'] = $userTypeResult['category']; // will be changed 
	$beneInfo['benePhotoIdType'] = $userTypeResult['title'];
	$beneInfo['benePhotoIdNumber'] = $beneResult['IDNumber'];
	$beneInfo['benePhotoIdCountry'] = $userTypeResult['countryName']; // will be changed 
	$beneInfo['benePhotoIdState'] = $beneResult['State']; // will be changed 
	$beneInfo['benePhotoIdIssueDate'] = $userTypeResult['issue_date'];
	$beneInfo['benePhotoIdExpDate'] = $userTypeResult['expiry_date'];
	$beneInfo['beneVerificationID'] = ''; // will be changed 
	$beneInfo['beneficiaryBankID'] = $beneResult['bankId'];
	$beneInfo['beneficiaryBankCode'] = $beneResult['branchCode'];
	$beneInfo['bankCountry'] = $beneResult['country'];
	$beneInfo['bankCity'] = $beneResult['BankCity'];;
	$beneInfo['beneficiaryAccountTitle'] = $beneResult['AccountTitle'];
	$beneInfo['beneficiaryAccNo'] = $beneResult['accountNo'];
	$beneInfo['beneficiaryAccountType'] = $beneResult['accountType'];
	$beneInfo['beneficiaryBranchNumber'] = $beneResult['branch_id'];
	$beneInfo['beneficiarySortCode'] = $beneResult['sortCode'];
	$beneInfo['beneficiarySwiftCode'] = $beneResult['swiftCode'];
	$beneInfo['beneficiaryIBAN'] = $beneResult['IBAN'];
	$beneInfo['beneficiaryRoutingNumber'] = $beneResult['routingNumber'];
	return $beneInfo;
}

/*
* 	getTransStatus retrieve the status of the Transaction from system provided RefNo by third party.
*/

function getTransStatus(){
	
	$headerInfo = getHeaderInfo();	
	$reqParameters = setWSParameters($headerInfo,array("tieupNumber" =>$tieupNumber,"transRefNumber" =>  $transRefNumber));	
	/* authenticate header info */	
	if(!empty($headerInfo) && !empty($headerInfo->agentID)){
	
		/* connect with temp_mt database */	
		$mtlink = connectToMT();	
		if(!$mtlink){		
			$resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Retrieving agent database information */		
		$arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);
		define("DATABASE",$arrDbConnection['client_database']);
		/* Agent ID not exist */	
		if(empty($arrDbConnection)){
			$resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);		
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;			
		}
		
		/* IPs Restriction  */
		if(!validateWhiteListIPs($arrDbConnection['allow_ips_list'])){
			$resHeader = setSoapHeader('Invalid credentials or IP','001',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);	
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;		
		}

		/* Agent Active/Inactive checking */
		if($arrDbConnection['status'] != 'Active'){
			$resHeader = setSoapHeader('Correspondent API interaction disabled','093',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		}	
		
		/* Authenticate login information */
		if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
			$resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);		
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		}
		
		/* Validate token */
		if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
			$resHeader = setSoapHeader('Invalid Token','003',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);		
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		}
		/* Checking if Method is accessible  */
		if(!strpos($arrDbConnection['accessable_methods'],'getTransStatus')){
			$resHeader = setSoapHeader('You do not have permission to call this method.','115',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);		
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		}
		/* END */
	}else{	
		$resHeader = setSoapHeader('Message format error','096',$tieupNumber,$transRefNumber);
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}	
	/* authenticate header info end */
	
	/* checking tiupNumber as per doc */	
	if(!empty($tieupNumber) && trim($tieupNumber) != ''){
		if(!validateAlphNumeric($tieupNumber) && strlen($tieupNumber) > 20){
			$resHeader = setSoapHeader('Invalid/Unauthorized Tie-up ID','008',$tieupNumber,$transRefNumber);		
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		} /* END */		
	}else{
		$resHeader = setSoapHeader('Missing Tie-up ID','116',$tieupNumber,$transRefNumber);		
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}	
	/* checking tiupNumber as per doc */	
	if(!empty($transRefNumber) && trim($transRefNumber) != ''){
		/* checking transRefNumber as per doc */
		if(!validateAlphNumeric($transRefNumber) && strlen($transRefNumber) > 50){
			$resHeader = setSoapHeader('Invalid Trans Ref Number','011',$tieupNumber,$transRefNumber);
			$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);
			prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
			return $arrResponse;
		} /* END */
	}else{
		$resHeader = setSoapHeader('Missing transRefNumber','117',$tieupNumber,$transRefNumber);		
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}
	
	/* database connectivity */	/*********************************************************************************************
	  Connection establish with payex database 
	  credentials get from middle tier
	*********************************************************************************************/	
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	
	$payex_db = $arrDbConnection["client_database"];	
	if(!mysql_select_db($payex_db, $payexLink)){
		$resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber,$transType);
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}	
	
	/* getTransStatus Query */
	$transQuery = " SELECT
			t.transStatus
		FROM
			transactions t
		WHERE
			t.refNumber = '".$transRefNumber."' AND transStatus!='Delete' ";	
	$transResult = selectSql($transQuery,$payexLink);
	//$transResult = '';
	global $ARR_TRANS_STATUS;	
	if(!empty($transResult['transStatus'])){
		$strStatusCode = array_search($transResult['transStatus'],$ARR_TRANS_STATUS);
		$resHeader = setSoapHeader('Successful','000',$tieupNumber,$transRefNumber);
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => $strStatusCode,'responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);		
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}else{		
		$resHeader = setSoapHeader('Others. Please contact Payex support for details','106',$tieupNumber,$transRefNumber);
		$arrResponse = array("tieupNumber" => $tieupNumber, "transRefNumber" =>$transRefNumber, 'transStatus' => '','responseCode' => $resHeader['errorCode'],'responseMessage' => $resHeader['errorMessage']);	
		prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransStatus', $resHeader['errorCode']);
		return $arrResponse;
	}
	
} // END getTransStatus

function getTransactionAsDistributor($apiCode, $tieupNumber)
{
    $headerInfo = getHeaderInfo();
    $reqParameters = setWSParameters($headerInfo,array("apiCode" => $apiCode,"tieupNumber" => $tieupNumber));

    /* authenticate header info */
    if(!empty($headerInfo)){

        /* connect with temp_mt database */
        $mtlink = connectToMT();
        if(!$mtlink){
            $resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber);
            $arrResponse = [];
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
            return $arrResponse;
        }

        /* Retrieving agent database information */
        $arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);

        /* Agent ID not exist */
        if(empty($arrDbConnection)){
            $resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$tieupNumber);
            $arrResponse = [];
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
            return $arrResponse;
        }


        /* Authenticate login information */
        if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
            $resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber);
            $arrResponse = [];
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
            return $arrResponse;
        }

        /* Validate token */
        if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
            $resHeader = setSoapHeader('Invalid Token','003',$tieupNumber);
            $arrResponse = [];
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
            return $arrResponse;
        }

    }else{
        $resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber);
        $arrResponse = [];
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }
    /* authenticate header info end */

    /* Request field(s) validation */
//    if(!validateGetTransRequest($apiCode, $tieupNumber)){
//        $resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transType);
//        $arrResponse = array("Transaction" => array());
//        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
//        return $arrResponse;
//    }

    if(!validateNumbers($apiCode) || strlen($apiCode) > 3){
        $resHeader = setSoapHeader('Invalid/Unauthorized API Code','010',$tieupNumber, $transType);
        $arrResponse = [];
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }

    if(!validateAlphNumeric($tieupNumber) && strlen($tieupNumber) > 20){
        $resHeader = setSoapHeader('Invalid/Unauthorized Tie-up ID','008',$tieupNumber,$transType);
        $arrResponse = [];
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* Request validation end */


    /* Getting global variables to be used */
    global $TRANSCTIONCODE;

    /* database connectivity */

    /*********************************************************************************************
    Connection establish with payex database
    credentials get from middle tier
     *********************************************************************************************/

    $payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);

    $payex_db = $arrDbConnection["client_database"];
    if(!mysql_select_db($payex_db, $payexLink)){
        $resHeader = setSoapHeader('Correspondent server not responding','090',$tieupNumber,$transType);
        $arrResponse = [];
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* getTransactionAsDistributor Query */
    $transQuery = "SELECT
            t.benAgentID as tieupNumber,
			t.refNumberIM as transRefNumber,
			CONCAT(TRIM(c.firstName), ' ', TRIM(c.middleName), ' ', TRIM(c.lastName)) as senderName,
			CONCAT(TRIM(b.firstName), ' ', TRIM(b.middleName), ' ', TRIM(b.lastName)) as beneficiaryName,
			t.transAmount as sendingAmount,
			t.exchangeRate as exchangeRate,
			t.currencyTo as destCurrency,
			t.localAmount as destAmount,
			t.IMFee as Fee,
			t.transType as transType,
			banks.id as BankCode,
			bd.accNo as accountNumber,
			cm.cp_branch_no as LocationID
		FROM
			transactions t
		LEFT JOIN customer AS c ON c.customerID = t.customerID 
		LEFT JOIN beneficiary AS b ON b.benID = t.benID
		LEFT JOIN bankDetails AS bd ON bd.transID = t.transID
		LEFT JOIN banks ON banks.name = bd.bankName
		LEFT JOIN cm_collection_point AS cm ON cm.cp_id = t.collectionPointID  
		WHERE
			t.transStatus = 'Authorize'
			AND t.benAgentID = '" . $tieupNumber . "' 
        LIMIT 20";
    $transResults = SelectMultiRecordsSql($transQuery,$payexLink);

    $transactions = array();
    if(!empty($transResults)){
        foreach ($transResults as $item) {
            $object = new stdClass();
            $object->tieupNumber = $tieupNumber;
            $object->transRefNumber = $item['transRefNumber'];
            $object->senderName = $item['senderName'];
            $object->beneficiaryName = $item['beneficiaryName'];
            $object->sendingAmount = $item['sendingAmount'];
            $object->exchangeRate = $item['exchangeRate'];
            $object->destCurrency = $item['destCurrency'];
            $object->destAmount = $item['destAmount'];
            $object->Fee = $item['Fee'];
            $object->transType = ($item['transType'] == 'Bank Transfer') ? 'AC' :
                (($item['transType'] == 'Pick up') ? 'COC' : $item['transType']);
            $object->BankCode = $item['BankCode'];
            $object->accountNumber = $item['accountNumber'];
            $object->LocationID = $item['LocationID'];
            $object->responseCode = '000';
            $object->responseMessage = 'Successful';
            $transactions[] = $object;
            unset($object);
        }

        /* preparing response */
        $resHeader = setSoapHeader('Successful','000',$tieupNumber,$transType);
        $arrResponse = $transactions;
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }else{
        $resHeader = setSoapHeader('No record found','-001',$tieupNumber,$transType);
        $arrResponse = $transactions;
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'getTransactionAsDistributor', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* Response return */

}

function sendTransStatus($dataObject)
{
    $headerInfo = getHeaderInfo();
    $reqParameters = setWSParameters($headerInfo,[
        'apiCode' => $dataObject->apiCode,
        'tieupNumber' => $dataObject->tieupNumber,
        'transReferenceNumber' => $dataObject->transReferenceNumber,
        'status' => $dataObject->status,
        'rejectionReason' => $dataObject->rejectionReason
    ]);

    /* authenticate header info */
    if(!empty($headerInfo)){

        /* connect with temp_mt database */
        $mtlink = connectToMT();
        if(!$mtlink){
            $resHeader = setSoapHeader('Correspondent server not responding','090',$dataObject->tieupNumber);
            $arrResponse = array("Transaction" => array());
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }

        /* Retrieving agent database information */
        $arrDbConnection = retrievingClientInfo($headerInfo->agentID, $headerInfo->username, $headerInfo->password, $mtlink);

        /* Agent ID not exist */
        if(empty($arrDbConnection)){
            $resHeader = setSoapHeader('Invalid/Unauthorized Agent Id','097',$dataObject->tieupNumber);
            $arrResponse = array("Transaction" => array());
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }


        /* Authenticate login information */
        if($headerInfo->username != $arrDbConnection['username'] || $headerInfo->password != $arrDbConnection['password']){
            $resHeader = setSoapHeader('Invalid credentials','001',$dataObject->tieupNumber);
            $arrResponse = array("Transaction" => array());
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }

        /* Validate token */
        if(!validateToken($headerInfo->password,$headerInfo->Token,$headerInfo->datetime)){
            $resHeader = setSoapHeader('Invalid Token','003',$dataObject->tieupNumber);
            $arrResponse = array("Transaction" => array());
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }

    }else{
        $resHeader = setSoapHeader('Invalid credentials','001',$dataObject->tieupNumber);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }
    /* authenticate header info end */

    /* Request field(s) validation */
//    if(!validateGetTransRequest($apiCode, $tieupNumber)){
//        $resHeader = setSoapHeader('Invalid credentials','001',$tieupNumber,$transReferenceNumber);
//        $arrResponse = array("Transaction" => array());
//        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
//        return $arrResponse;
//    }

    if(!validateNumbers($dataObject->apiCode) || strlen($dataObject->apiCode) > 3){
        $resHeader = setSoapHeader('Invalid/Unauthorized API Code','010',$dataObject->tieupNumber, $dataObject->transReferenceNumber);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    if(!validateAlphNumeric($dataObject->tieupNumber) && strlen($dataObject->tieupNumber) > 20){
        $resHeader = setSoapHeader('Invalid/Unauthorized Tie-up ID','008',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    if(empty($dataObject->transReferenceNumber)){
        $resHeader = setSoapHeader('Invalid Trans Ref Number','011',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    if(empty($dataObject->status)){
        $resHeader = setSoapHeader('Invalid Trans Status','120',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* Request validation end */


    /* Getting global variables to be used */
    global $TRANSCTIONCODE;

    /* database connectivity */

    /*********************************************************************************************
    Connection establish with payex database
    credentials get from middle tier
     *********************************************************************************************/

    $payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);

    $payex_db = $arrDbConnection["client_database"];
    if(!mysql_select_db($payex_db, $payexLink)){
        $resHeader = setSoapHeader('Correspondent server not responding','090',$dataObject->tieupNumber,$dataObject->tr);
        $arrResponse = array("Transaction" => array());
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* sendTransStatus Query */
    $getTransQuery = 'SELECT * FROM transactions 
        WHERE refNumberIM = "' . $dataObject->transReferenceNumber . '"
            AND benAgentID = "' . $dataObject->tieupNumber . '"
        LIMIT 1';

    $transResult = selectSql($getTransQuery, $payexLink);
    $result = false;

    if ($transResult){
        $newStatus = '';
        if ($dataObject->status === 'Paid'){
            if ($transResult['transType'] === 'Bank Transfer'){
                $newStatus = 'Credited';
            }elseif ($transResult['transType'] === 'Pick up'){
                $newStatus = 'Picked up';
            }
        }elseif ($dataObject->status === 'Rejected'){
            $newStatus = 'Rejected';
        }

        if ($newStatus){
            $updateQuery = 'UPDATE transactions SET transStatus = "' . $newStatus .'"
            WHERE refNumberIM = "' . $dataObject->transReferenceNumber . '"
                AND benAgentID = "' . $dataObject->tieupNumber . '"
            ';
            $result = update($updateQuery);
        }

        if(!$result){
            $resHeader = setSoapHeader('Others. Please contact Payex support for details','106',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
            $arrResponse = array("Transaction" => array());
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }else{
            $response = [
                'tieupNumber' => $dataObject->tieupNumber,
                'transRefNumber' => $dataObject->transReferenceNumber,
                'responseCode' => '000',
                'responseMessage' => 'Successful'
            ];

            /* preparing response */
            $resHeader = setSoapHeader('Successful','000',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
            $arrResponse = $response;
            prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
            return $arrResponse;
        }
    }else{
        $resHeader = setSoapHeader('No record found','-001',$dataObject->tieupNumber,$dataObject->transReferenceNumber);
        $arrResponse = array("Transaction" => []);
        prepareWSLog($arrDbConnection["payex_client_id"],$reqParameters, setWSParameters($resHeader, $arrResponse), $headerInfo, 'sendTransStatus', $resHeader['errorCode']);
        return $arrResponse;
    }

    /* Response return */
}

ini_set("soap.wsdl_cache_enabled", "0");
$server = new SoapServer("payex.wsdl");
$server->addFunction("getTransaction");
$server->addFunction("addSender");
$server->addFunction("sendTransaction");
$server->addFunction("addBeneficiary");
$server->addFunction("addSenderBank");
$server->addFunction("getTransStatus");
$server->addFunction('getTransactionAsDistributor');
$server->addFunction('sendTransStatus');
$server->handle();

?>