<?php

/*
	Annotation of Parameters & Return Values 
	
	Notation	Description
	A			Alphabetic (A-Z and a-z)
	N			Numeric (0-9)
	AN			Alpha Numeric (0-9, A-Z and a-z)
	DOUBLE		Numeric with decimal precision
	FLOAT		Numeric with decimal precision
	ANS			Combination of Alphabets, Numeric and special characters (0-9, A-Z, a-z, special characters)
	BOOL		‘0’ or ‘1’
*/


/*
	Following letters represents
	
	Notation	Description
	M	Mandator
	O	Optional
	C	Conditional
 
*/


/* Request Constrains */

	global $mapping;
	$mapping = array(
		/*
		* transaction field mapping
		*/
		'tieupNumber' => 'test_MT.manage_api.agentID',
		'originalApiCode' => 'test_MT.manage_api.version', // Please see document appendix (a) APIs and Request Codes.
		'transRefNumber' => 'transactions.refNumber',
		'transType' => 'transactions.transType',
		'sendingCurrency' => 'transactions.currencyFrom',
		'sendingAmount' => 'transactions.transAmount',
		'exchangeRate' => 'transactions.exchangeRate',
		'destCurrency' => 'transactions.currencyTo',
		//'destAmount' => 'transactions.recievedAmount',
		'destAmount' => 'transactions.localAmount',
		'Fee' => 'transactions.IMFee',
		'Tax' => 'transactions.AgentCommTax',
		'settlementCurrency' => 'transactions.settlementCurrency',
		'purposeOfTransaction' => 'transactions.transactionPurpose',
		'senderReference' => 'transactions.senderReference',
		'payourID' => 'transactions.benAgentID',
	
		
		
		
		/* END  */
				
		/*
		* 	customerfield mapping
		*/	
		
		'senderFirstName' => 'customer.firstName',
		'senderMiddleName' => 'customer.middleName',
		'senderLastName' => 'customer.lastName',
		'senderPOBox' => 'customer.Zip',
		'senderAddress1' => 'customer.Address',
		'senderAddress2' => 'customer.Address1',
		'senderCountry' => 'customer.Country',
		'senderState' => 'customer.State',
		'senderCity' => 'customer.City',
		'senderAddressZip' => 'customer.Zip',
		'senderPhone' => 'customer.Phone',
		'senderDOB' => 'customer.dob',
		'senderMobile' => 'customer.Mobile',
		'senderFax' => 'customer.fax',
		'senderEmail' => 'customer.email',
		'senderID1Type' => 'customer.title',
		'senderID1No' => 'customer.id_number',
		'senderID1expiryDate' => 'customer.expiry_date',
		'senderID2Type' => 'customer.title2',
		'senderID2No' => 'customer.id_number2',
		'senderID2expiryDate' => 'customer.expiry_date2',
		'senderBankID' => 'bankDetails.bankID',
		'senderBankCode' => 'bankDetails.bankID',
		'senderBankCountry' => 'customer.bankCountry',
		'senderBankCity' => 'bankDetails.BranchCity',
		'senderAccountTitle' => 'customer.accountName',
		'senderAccNo' => 'bankDetails.accNo',
		'senderAccountType' => 'bankDetails.accountType',
		'senderBranchNumber' => 'bankDetails.branchCode',
		'senderSortCode' => 'bankDetails.sortCode',
		'senderSwiftCode' => 'bankDetails.swiftCode',
		'senderIBAN' => 'bankDetails.IBAN',
		'senderRoutingNumber' => 'bankDetails.routingNumber',
		
		/* END  */
				
		/*
		* 	beneficiary field mapping
		*/
		
		'beneficiaryReference' => 'transactions.beneficiaryReference',
		'beneficiaryFirstName' => 'beneficiary.firstName',
		'beneficiaryMiddlleName' => 'beneficiary.middleName',
		'beneficiaryLastName' => 'beneficiary.lastName',
		'beneAddress' => 'beneficiary.Address',
		'beneAddress2' => 'beneficiary.Address1',
		'beneAddress3' => 'beneficiary.Address3',
		'beneAddress4' => 'beneficiary.Address4',
		'benePhone' => 'beneficiary.Phone',
		'beneMobilePhone' => 'beneficiary.Mobile',
		'beneCountry' => 'beneficiary.Country',
		'beneState' => 'beneficiary.State',
		'beneCity' => 'beneficiary.City',
		'beneZipCode' => 'beneficiary.Zip',
		'beneDOB' => 'beneficiary.dob',
		'beneOccupation' => 'beneficiary.beneOccupation',
		'beneBirthCity' => 'beneficiary.Citizenship',
		'beneBirthCountry' => 'beneficiary.beneBirthCountry',
		'beneNationalityCountry' => 'beneficiary.beneNationalityCountry',
		'beneNationalityAtBirthCountry' => 'beneficiary.beneNationalityAtBirthCountry',		
		'relationToSender' => 'beneficiary.SOF',
		'beneEmailAddress' => 'beneficiary.email',
		'benePhotoIDCat' => 'beneficiary.category3',
		'benePhotoIdType' => 'beneficiary.title3',
		'benePhotoIdNumber' => 'beneficiary.id_number3',
		'benePhotoIdCountry' => 'beneficiary.country3',
		'benePhotoIdState' => 'beneficiary.country3',
		'benePhotoIdIssueDate' => 'beneficiary.issue_date3',
		'benePhotoIdExpDate' => 'beneficiary.expiry_date3',
		'beneVerificationID' => 'beneficiary.IDNumber',
		'beneficiaryBankID' => 'ben_banks.bankId',
		'beneficiaryBankCode' => 'beneficiary.branchCode',
		'bankCountry' => 'beneficiary.bankCountry',
		'bankCity' => 'ben_banks.BankCity',
		'beneficiaryAccountTitle' => 'ben_banks.accountTitle',
		'beneficiaryAccNo' => 'ben_banks.accountNo',
		'beneficiaryAccountType' => 'ben_banks.accountType',
		'beneficiaryBranchNumber' => 'ben_banks.branch_id',
		'beneficiarySortCode' => 'ben_banks.sortCode',
		'beneficiarySwiftCode' => 'ben_banks.swiftCode',
		'beneficiaryIBAN' => 'ben_banks.IBAN',
		'beneficiaryRoutingNumber' => 'ben_banks.routingNumber',
		
		/* END  */
		
	);
	
	global $bank_codes;
	$blya = '[live]';
	$bank_codes = array(
	31501,
	1,
	113,
	561,
	768,
	983,
	1081,
	1251,
	2244,		
	1792,	
	2041,	
	2414,
	111111136,
	2804,
	111111134,
	2820,
	3261,
	3482,
	3659,
	3777,
	3805,
	3985,
	4949,
	4590,
	5365,
	5517,
	5833);
	
	
	global $mandatory_fields;
	$mandatory_fields = array
	(		
			'test_MT.manage_api.agentID' =>
			array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '20',
				'C' => ''
			),
			'test_MT.manage_api.version' =>
			array(
				'O' => 'M',
				'T' => 'integer',
				'L' => '3',
				'C' => ''
			),
			'transactions.refNumber' =>
			array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),			
			'transactions.transType' =>
			array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '5',
				'C' => ''
			),
			'transactions.currencyFrom' =>array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCurrencyCode',
				'C' => ''
			),
			'transactions.localAmount' =>array(
				'O' => 'M', // M,float(15,2) Destination Amount
				'T' => 'double',
				'L' => '',
				'L' => '15',
				'C' => ''
			),
			'transactions.exchangeRate' =>array(
				'O' => 'M', //M,int(12,4)
				'T' => 'double',
				'L' => '12',
				'C' => ''
			),
			'transactions.transAmount' =>array(
				'O' => 'M',
				'T' => 'double',
				'L' => '15',
				'C' => ''
			),			
			'transactions.currencyTo' =>array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCurrencyCode',
				'C' => ''			
			),
			'transactions.localAmount' =>array(
				'O' => 'M', // M,float(15,2)
				'T' => 'double',				
				'L' => '15',
				'C' => ''			
			),
			
			// 'transactions.recievedAmount' =>array(
				// 'O' => 'M', // M,float(15,2)
				// 'T' => 'double',				
				// 'L' => '15',
				// 'C' => ''			
			// ),
			
			
			'transactions.transactionPurpose' =>array(
				'O' => 'M', // M,float(15,2)
				'T' => 'string',				
				'L' => '2',
				'C' => ''			
			),
			
			'transactions.IMFee' =>array(
				'O' => 'M', //M,int(12,4)
				'T' => 'double',
				'L' => '12',
				'C' => ''
			),
		
			'transactions.AgentCommTax' =>array(
				'O' => 'O', // M,float(15,2) Bank Charges
				'T' => 'double',				
				'L' => '15',
				'C' => ''			
			),
			'transactions.settlementCurrency' =>array(
				'O' => 'O', // M,float(15,2) Bank Charges
				'T' => 'string',				
				'L' => '3',
				'C' => ''			
			),
			'transactions.senderReference' =>array(
				'O' => 'C', 
				'T' => 'string',				
				'L' => '3',
				'C' => 'transaction.customerID'			
			),	
			'transactions.beneficiaryReference' =>array(
				'O' => 'C', 
				'T' => 'string',				
				'L' => '3',
				'C' => 'transactions.benID'			
			),	
			'transactions.benAgentID' =>array(
				'O' => 'Y', 
				'T' => 'string',				
				'L' => '10',
				'C' => ''
			),			
			
			/* End */
			
			/* customer's field level condition */
			'customer.firstName' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'customer.middleName' => array(
				'O' => 'O',
				'T' => 'string',
				'L' => '50',
				'C' => ''
			), 
			'customer.lastName' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'customer.Country' => array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'customer.State' => array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'customer.Zip' => array(
				'O' => 'O',
				'T' => 'string',
				'L' => '15',				
				'C' => ''
			),
			'customer.accountName' => array(
				'O' => 'C',
				'T' => 'string',
				'L' => '32',				
				'C' => ''
			),
			'customer.Address' => array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '150',				
				'C' => ''
			),
			'customer.Address1' => array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '150',				
				'C' => ''
			),
			'customer.City' => array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '20',				
				'C' => ''
			),
			'customer.Zip' => array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'customer.Phone' => array(
				'O' => 'N',  
				'T' => 'string',
				'L' => '15',
				'C' => ''
			),
			'customer.dob' => array( // Sender Date of birth Format(yyyy-mm-dd)
				'O' => 'O',
//				'T' => 'date',
				'T' => 'string',
				'L' => '10',			
				'C' => ''
			),
			'customer.Mobile' => array(
				'O' => 'N',  
				'T' => 'string',
				'L' => '15',
				'C' => ''
			),
			'customer.fax' => array(
				'O' => 'N',  
				'T' => 'string',
				'L' => '15',
				'C' => ''
			),
			'customer.email' => array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '30',
				'C' => ''
			),
			'customer.title' => array(
				'O' => 'M',  
				'T' => 'integer',
				'L' => '2',
				'C' => ''
			),
			'customer.title2' => array(
				'O' => 'M',  
				'T' => 'integer',
				'L' => '2',
				'C' => ''
			),						
			'customer.id_number' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'customer.id_number2' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			), 
			'customer.expiry_date' => array(
				'O' => 'O', //M,varchar(50)
//				'T' => 'date',
				'T' => 'string',
				'L' => '10',
				'C' => ''
			),
			'customer.expiry_date2' => array(
				'O' => 'O', //M,varchar(50)
//				'T' => 'date',
				'T' => 'string',
				'L' => '10',
				'C' => ''
			),
			'ben_banks.bankId' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'beneficiary.branchCode' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '50',
				'C' => ''
			),
			'bankDetails.BranchCity' => array(
				'O' => 'C', //
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'bankDetails.accNo' => array(
				'O' => 'O', //
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'bankDetails.sortCode' => array(
				'O' => 'N', //
				'T' => 'string',
				'L' => '6',
				'C' => ''
			),
			'bankDetails.swiftCode' => array(
				'O' => 'C', //
				'T' => 'string',
				'L' => '11',
				'C' => ''
			),
			'bankDetails.IBAN' => array(
				'O' => 'C', //
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'bankDetails.routingNumber' => array(
				'O' => 'C', //
				'T' => 'integer',
				'L' => '9',
				'C' => ''
			),
			'bankDetails.accountType' => array(
				'O' => 'N', //
				'T' => 'string',
				'L' => '2',
				'C' => ''
			),
			'customer.bankCountry' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''				
			),
			'beneficiary.bankCountry' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''				
			),
			'ben_banks.BankCity' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),			
			/*'ben_banks.accountNo' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '50',
				'C' => ''
			),*/
			'ben_banks.accountType' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '2',
				'C' => ''
			),
			'ben_banks.branch_id' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '2',
				'C' => ''
			),
			'bankDetails.bankID' => array(
				'O' => 'C', //
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'ben_banks.sortCode' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '6',
				'C' => ''
			),
			'ben_banks.swiftCode' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '11',
				'C' => ''
			),
			'bankDetails.IBAN' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'ben_banks.routingNumber' => array(
				'O' => 'C', //M,varchar(50)
				'T' => 'integer',
				'L' => '9',
				'C' => ''
			),
			
			/* End */
			
			
			/* beneficiary's field level condition */
			'beneficiary.firstName' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			), 
			'beneficiary.middleName' => array(
				'O' => 'O', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			), 
			'beneficiary.lastName' => array(
				'O' => 'M', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'beneficiary.Address' => array(
				'O' => 'N', //M,varchar(50)
				'T' => 'string',
				'L' => '150',
				'C' => ''
			), 
			'beneficiary.Address1' => array(
				'O' => 'N', //M,varchar(50)
				'T' => 'string',
				'L' => '150',
				'C' => ''
			), 
			'beneficiary.Address3' => array(
				'O' => 'N', //M,varchar(50)
				'T' => 'string',
				'L' => '150',
				'C' => ''
			), 'beneficiary.Address4' => array(
				'O' => 'N', //M,varchar(50)
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),  			
			'beneficiary.Phone' =>array(
				'O' => 'N',
				'T' => 'integer',
				'L' => '15',
				'C' => ''
			),
			'beneficiary.Mobile' =>array(
				'O' => 'N',
				'T' => 'integer',
				'L' => '15',
				'C' => ''
			),
			'beneficiary.Country' => array(
				'O' => 'M',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'beneficiary.State' => array(
				'O' => 'N',  
				'T' => 'string',
				'L' => '20',				
				'C' => ''
			),
			'beneficiary.City' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '20',
				'C' => ''
			),
			'beneficiary.Zip' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '15',
				'C' => ''
			),
			'beneficiary.dob' =>array(
				'O' => 'N',
				'T' => 'string',// it was date before
				'L' => '10',
				'C' => ''
			),
			'beneficiary.beneOccupation' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '20',
				'C' => ''
			),
			'beneficiary.Citizenship' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '20',
				'C' => ''
			),
			'beneficiary.beneBirthCountry' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'beneficiary.beneNationalityCountry' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'beneficiary.beneNationalityAtBirthCountry' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '10',				
				'C' => ''
			),
			'beneficiary.SOF' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '10',				
				'C' => ''
			),
			'beneficiary.email' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '32',
				'C' => ''
			),
			'beneficiary.category3' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '3',
				'C' => ''
			),
			'beneficiary.title3' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '3',
				'C' => ''
			),
			'beneficiary.id_number3' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'beneficiary.country3' =>array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '3',
				'F' => 'checkCountryCode',
				'C' => ''
			),
			'beneficiary.country3' =>array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '20',				
				'C' => ''
			),
			'beneficiary.issue_date3' =>array(
				'O' => 'N',
				'T' => 'string',//it was date
				'L' => '10',
				'C' => ''
			),
			'beneficiary.expiry_date3' =>array(
				'O' => 'O',
//				'T' => 'date',////it was date
				'T' => 'string',////it was date
				'L' => '10',
				'C' => ''
			),
			'beneficiary.IDNumber' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),
			'banks.bankId' =>array(
				'O' => 'O',
				'T' => 'integer',
				'L' => '50',
				'C' => ''
			),
			'bankDetails.branchCode' =>array(
				'O' => 'N',
				'T' => 'string',
				'L' => '50',
				'C' => ''
			),			
			'ben_banks.BankCity' =>array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '50',				
				'C' => ''
			),
			'ben_banks.accountTitle' =>array(
				'O' => 'O',  
				'T' => 'string',
				'L' => '50',				
				'C' => ''
			),
			'ben_banks.accountNo' =>array(
				'O' => 'M', //M,Integer(50)
				'T' => 'string',
				'L' => '50',				
				'C' => ''
			),
			'ben_banks.accountType' =>array(
				'O' => 'O', //M,Integer(50)
				'T' => 'integer',
				'L' => '2',				
				'C' => ''
			),
			'ben_banks.branch_id' =>array(
				'O' => 'O', //M,Integer(50)
				'T' => 'integer',
				'L' => '2',				
				'C' => ''
			),
			'ben_banks.sortCode' =>array(
				'O' => 'O', //M,Integer(50)
				'T' => 'integer',
				'L' => '6',				
				'C' => ''
			),
			'ben_banks.swiftCode' =>array(
				'O' => 'O', //M,Integer(50)
				'T' => 'integer',
				'L' => '11',				
				'C' => ''
			),
			'ben_banks.IBAN' =>array(
				'O' => 'O',
				'T' => 'string',
				'L' => '50',				
				'C' => ''
			),
			'ben_banks.routingNumber' =>array(
				'O' => 'O', //M,Integer(50)
				'T' => 'integer',
				'L' => '9',				
				'C' => ''
			),
			/* End */
	);
	
	global $senderMapping;
	$senderMapping = array(	
		'senderID' => 'customer.api_senderId',
		'senderEmail' => 'customer.email',
		'password' => 'customer.password',
		'confirmpassword' => 'customer.password',
		'senderFirstName' => 'customer.firstName',
		'senderMiddleName' => 'customer.middleName',
		'senderLastName' => 'customer.lastName',
		'streetNumber' => 'customer.streetNo',
		'streetName' => 'customer.streetName',
		'suburb' => 'customer.suburb',
		'senderAddress1' => 'customer.Address',
		'senderCity' => 'customer.City',
		'senderState' => 'customer.State',
		'senderZipCode' => 'customer.Zip',
		'senderCountryCode' => 'customer.Country',
		'senderPhoneNumber' => 'customer.Phone',
		'senderDOB' => 'customer.dob',
		'senderNationality' => 'customer.nationality',
		'senderID1Type' => 'customer.title',
		'senderID1No' => 'customer.id_number',
		'senderID1expiryDate' => 'customer.expiry_date',
		'senderID2Type' => 'customer.title2',
		'senderID2No' => 'customer.id_number2',
		'senderID2expiryDate' => 'customer.expiry_date2',
		'occupation' => 'customer.profession',
		'employerName' => 'customer.employerName',
	);
	
	global $beneficiaryMapping;
	$beneficiaryMapping = array(	
		'customerID' => 'beneficiary.customerID',
		'beneficiaryReference' => 'beneficiary.benAccount',
		'beneficiaryFirstName' => 'beneficiary.firstName',
		'beneficiaryMiddlleName' => 'beneficiary.middleName',
		'beneficiaryLastName' => 'beneficiary.lastName',
		'beneAddress' => 'beneficiary.Address',
		'beneAddress2' => 'beneficiary.Address1',
		'beneAddress3' => 'beneficiary.Address3',
		'beneAddress4' => 'beneficiary.Address4',
		'benePhone' => 'beneficiary.Phone',
		'beneMobilePhone' => 'beneficiary.Mobile',
		'beneCountry' => 'beneficiary.Country',
		'beneState' => 'beneficiary.State',
		'beneCity' => 'beneficiary.City',
		'beneZipCode' => 'beneficiary.Zip',
		'beneDOB' => 'beneficiary.dob',
		'beneOccupation' => 'beneficiary.beneOccupation',
		'beneBirthCity' => 'beneficiary.Citizenship',
		'beneBirthCountry' => 'beneficiary.beneBirthCountry',
		'beneNationalityCountry' => 'beneficiary.beneNationalityCountry',
		'beneNationalityAtBirthCountry' => 'beneficiary.beneNationalityAtBirthCountry',		
		'relationToSender' => 'beneficiary.SOF',
		'beneEmailAddress' => 'beneficiary.email',
		'benePhotoIDCat' => 'beneficiary.category3',	
		'benePhotoIdType' => 'beneficiary.title3',
		'benePhotoIdNumber' => 'beneficiary.id_number3',	
		'benePhotoIdCountry' => 'beneficiary.country3',	
		'benePhotoIdState' => 'beneficiary.idState',	
		'benePhotoIdIssueDate' => 'beneficiary.issue_date3',
		'benePhotoIdExpDate' => 'beneficiary.expiry_date3',	
	);
	
	
	global $beneBankFieldsMapping;
	$beneBankFieldsMapping = array(
		'bankCode' => 'ben_banks.bankId',
		'userType' => 'ben_banks.userType',
		'userID' => 'ben_banks.benId',
		'bankCountry' => 'ben_banks.BankCountry',
		'bankCity' => 'ben_banks.BankCity',
		'accountTitle' => 'ben_banks.AccountTitle',
		'accNo' => 'ben_banks.accountNo',
		'accountType' => 'ben_banks.accountType',
		'branchNumber' => 'ben_banks.branchCode',
		'sortCode' => 'ben_banks.sortCode',
		'swiftCode' => 'ben_banks.swiftCode',
		'IBAN' => 'ben_banks.IBAN',
		'routingNumber' => 'ben_banks.routingNumber',
	);
	
	global $senderBankFieldsMapping;
	$senderBankFieldsMapping = array(			
		'bankCode' => 'banks.id',
		'userType' => 'cust_banks.userType',
		'userID' => 'cust_banks.custId',
		'bankCountry' => 'cust_banks.BankCountry',
		'bankCity' => 'cust_banks.BankCity',
		'accountTitle' => 'cust_banks.AccountTitle',
		'accNo' => 'cust_banks.accountNo',
		'accountType' => 'cust_banks.accountType',
		'branchNumber' => 'cust_banks.branchCode',
		'sortCode' => 'cust_banks.sortCode',
		'swiftCode' => 'cust_banks.swiftCode',
		'IBAN' => 'cust_banks.IBAN',
		'routingNumber' => 'cust_banks.routingNumber',
	);
	
	global $beneBankFieldsCheck;
	$beneBankFieldsCheck = array(		
		'banks.id' => array(
			'O' => 'M',
			'T' => 'integer',
			'L' => '50',
			'C' => ''
		),
		'ben_banks.BankCountry' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '3',			
			'C' => ''
		),
		'ben_banks.BankCity' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),
		'ben_banks.AccountTitle' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '32',
			'C' => ''
		),
		'ben_banks.accountNo' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),
		'ben_banks.accountType' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '2',
			'C' => ''
		),
		'ben_banks.branchCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '2',
			'C' => ''
		),
		'ben_banks.sortCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '6',
			'C' => ''
		),
		'ben_banks.swiftCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '11',
			'C' => ''
		),
		'ben_banks.IBAN' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),
		'ben_banks.routingNumber' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '9',
			'C' => ''
		),
		'ben_banks.userType' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '1',
			'C' => ''
		),
		'ben_banks.benId' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '32',
			'C' => ''
		),
	);
	
	global $senderBankFieldsCheck;
	$senderBankFieldsCheck = array(		
		'banks.id' => array(
			'O' => 'M',
			'T' => 'integer',
			'L' => '50',
			'C' => ''
		),
		'cust_banks.BankCountry' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '3',			
			'C' => ''
		),
		'cust_banks.BankCity' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),
		'cust_banks.AccountTitle' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '32',
			'C' => ''
		),
		'cust_banks.accountNo' => array(
			'O' => 'M',
			'T' => 'integer',
			'L' => '50',
			'C' => ''
		),
		'cust_banks.accountType' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '2',
			'C' => ''
		),
		'cust_banks.branchCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '2',
			'C' => ''
		),
		'cust_banks.sortCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '6',
			'C' => ''
		),
		'cust_banks.swiftCode' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '11',
			'C' => ''
		),
		'cust_banks.IBAN' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),
		'cust_banks.routingNumber' => array(
			'O' => 'N',
			'T' => 'integer',
			'L' => '9',
			'C' => ''
		),
		'cust_banks.userType' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '1',
			'C' => ''
		),
		'cust_banks.userID' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '32',
			'C' => ''
		),
	);
	
	
	
	global $beneficiaryFieldsCheck;
	
	$beneficiaryFieldsCheck = array(
		'beneficiary.customerID' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '32',				
			'C' => ''
		),
		'beneficiary.benAccount' => array(
			'O' => 'C',
			'T' => 'string',
			'L' => '32',				
			'C' => ''
		),
		'beneficiary.firstName' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '50',				
			'C' => ''
		),
		'beneficiary.middleName' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '20',				
			'C' => ''
		),
		'beneficiary.lastName' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '20',				
			'C' => ''
		),
		'beneficiary.Address' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',				
			'C' => ''
		),
		'beneficiary.Address1' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',				
			'C' => ''
		),
		'beneficiary.Address3' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',				
			'C' => ''
		),
		'beneficiary.Address4' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',				
			'C' => ''
		),
		'beneficiary.Phone' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '15',				
			'C' => ''
		),
		'beneficiary.Mobile' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '15',				
			'C' => ''
		),
		'beneficiary.Country' => array(
			'O' => 'M',
			'T' => 'string',
			'L' => '3',				
			'C' => ''
		),
		'beneficiary.State' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '20',				
			'C' => ''
		),
		'beneficiary.City' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '20',				
			'C' => ''
		),
		'beneficiary.Zip' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '20',
			'C' => ''
		),
		'beneficiary.dob' => array(
			'O' => 'N',
			'T' => 'string',//It was date before
			'L' => '10',
			'C' => ''
		),
		'beneficiary.beneOccupation' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '20',
			'C' => ''
		),
		'beneficiary.Citizenship' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '20',
			'C' => ''
		),
		'beneficiary.beneBirthCountry' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'beneficiary.beneNationalityCountry' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'beneficiary.beneNationalityAtBirthCountry' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '10',
			'C' => ''
		),
		'beneficiary.SOF' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '10',
			'C' => ''
		),
		'beneficiary.email' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '30',
			'C' => ''
		),
		'beneficiary.category3' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'beneficiary.title3' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'beneficiary.id_number3' => array(
			'O' => 'N',
			'T' => 'string',
			'L' => '50',
			'C' => ''
		),						
		'beneficiary.country3' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'beneficiary.idState' => array(
			'O' => 'O',
			'T' => 'string',
			'L' => '20',
			'C' => ''
		),
		'beneficiary.issue_date3' => array(
			'O' => 'O',
			'T' => 'string',//it was date
			'L' => '10',
			'C' => ''
		),
		'beneficiary.expiry_date3' => array(
			'O' => 'O',
			'T' => 'string',//it was date
			'L' => '10',
			'C' => ''
		),
	);
	
	/* Response Constrains*/

	global $senderFieldChecks;
	$senderFieldChecks = array(
	
		'customer.api_senderId' => array(
			'O' => 'O',  
			'T' => 'string',
			'L' => '32',			
			'C' => ''
		),
		'customer.email' => array(
			'O' => 'N',  
			'T' => 'email',
			'L' => '100',			
			'C' => ''
		),
		'customer.password' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '32',			
			'C' => ''
		),		
		'customer.firstName' => array(
			'O' => 'M',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.middleName' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.lastName' => array(
			'O' => 'M',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.streetNo' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '10',			
			'C' => ''
		),
		'customer.streetName' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.suburb' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.Address' => array(
			'O' => 'M',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.City' => array(
			'O' => 'M',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.State' => array(
			'O' => 'M',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.Zip' => array(
			'O' => 'O',  
			'T' => 'string',
			'L' => '10',			
			'C' => ''
		),
		'customer.Country' => array(		
			'O' => 'M',
			'T' => 'string',
			'L' => '3',
			'F' => 'checkCountryCode',
			'C' => ''
		),
		'customer.Phone' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '14',			
			'C' => ''
		),
		'customer.dob' => array( // Sender Date of birth ; Format(yyyy-mm-dd)
			'O' => 'O', //it was M - before
			'T' => 'string', //it was date before
			'L' => '10',			
			'C' => ''
		),
		'customer.nationality' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '50',			
			'C' => ''
		),
		'customer.title' => array(
			'O' => 'C',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'customer.title2' => array(
			'O' => 'C',
			'T' => 'string',
			'L' => '3',
			'C' => ''
		),
		'customer.id_number' => array(
			'O' => 'C',  
			'T' => 'string',
			'L' => '30',			
			'C' => ''
		),
		'customer.id_number2' => array(
			'O' => 'C',
			'T' => 'string',
			'L' => '30',			
			'C' => ''
		),
		'customer.expiry_date' => array(
			'O' => 'N',
			'T' => 'date',
			'L' => '10',			
			'C' => ''
		),
		'customer.expiry_date2' => array(
			'O' => 'N',
			'T' => 'date',
			'L' => '10',			
			'C' => ''
		),		
		'customer.profession' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '32',			
			'C' => ''
		),
		'customer.employerName' => array(
			'O' => 'N',  
			'T' => 'string',
			'L' => '32',			
			'C' => ''
		),
	);




?>