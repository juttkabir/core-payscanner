<?php
 
	require_once("../webservices/soapConfig.php");
	require_once("../api/country_codes_class.php");
	require_once("../api/currency_codes_class.php");
	require_once("../webservices/utils/getTransactionsConstrains.php");
	
	function validateNumbers($value){
		if(!is_numeric($value)){
			return false;
		}
		return true;
	}
	
	function validateAlphabets($value){
		if(!ctype_alpha($value)){
			return false;
		}
		return true;
	}
	
	function validateAlphNumeric($value){
		if(!ctype_alnum($value)){
			return false;
		}
		return true;	
	}
	
	function validateAlphNumericSpecial($value){
		return true;
	}
	
	function validateGetTransRequest($apiCode, $tieupNumber, $transRefNumber, $transType){
	
		if((!empty($apiCode) || trim($apiCode) != "") && (!empty($tieupNumber) || trim($tieupNumber) != "") && (!empty($transType) || trim($transType) != "")){
			return true;	
		}else{
			return false;
		}	
	}
	function loginAuth($userName, $password){
		dbConnect(SERVER_MASTER,USER,PASSWORD,DATABASE);
		$authQuery = " SELECT count(userID) as loginCount FROM admin WHERE username = '".$userName."' AND password = '".$password."' " ;
		$authRes = selectFrom($authQuery);
		if($authRes['loginCount'] == 1){
			return true;
		}
		return false;
	}
	
	function validateToken($password, $clientToken, $requestDateTime){
		$strToken = $password.$requestDateTime.strrev($password);
		if($strToken != $clientToken){
			return false;
		}
		return true;
	
	}
	function getHeaderInfo(){
				
		$xmlPostData = file_get_contents('php://input', true);
		$objXML = simplexml_load_string($xmlPostData);
		$objHeaderInfo = $objXML->children('SOAP-ENV', true)->Header->children('ns1', true);
		return $objHeaderInfo;
		
	/*	if(!empty($objHeaderInfo)){
		
			if($objHeaderInfo->agentID == AGENTID && $objHeaderInfo->username == SOAPUSERNAME && $objHeaderInfo->password == SOAPPASSWORD && $objHeaderInfo->Token == TOKEN && $objHeaderInfo->sessionID == SESSIONID && $objHeaderInfo->datetime == DATETIME && $objHeaderInfo->requestType == REQUESTTYPE	
			){
				return true;
			}
		}
		return false; */
	}
	
	function setSoapHeader($msg, $msgCode = '000', $agentID, $requestType){


		$header = new SoapHeader('PayexOutputNamespace', 'errorMessage', $msg);
		$GLOBALS['server']->addSoapHeader($header);
		$header = new SoapHeader('PayexOutputNamespace', 'errorCode', $msgCode);
		$GLOBALS['server']->addSoapHeader($header);
		$header = new SoapHeader('PayexOutputNamespace', 'Time', time());
		$GLOBALS['server']->addSoapHeader($header);
		$header = new SoapHeader('PayexOutputNamespace', 'Date', date());
		$GLOBALS['server']->addSoapHeader($header);
		$header = new SoapHeader('PayexOutputNamespace', 'agentID', $agentID);
		$GLOBALS['server']->addSoapHeader($header);
		$header = new SoapHeader('PayexOutputNamespace', 'requestType', $requestType);
		$GLOBALS['server']->addSoapHeader($header);
		return getResHeader($msg, $msgCode, $agentID, $requestType);
	}
	
	function getResHeader($msg, $msgCode = '000', $agentID, $requestType){

		$header = array();
		$header['errorMessage']=  $msg;
		$header['errorCode']=  $msgCode;
		$header['Time']=  time();
		$header['Date']=  date();
		$header['agentID']=  $agentID;
		$header['requestType']=  $requestType;
		return $header;		
	}
	function selectSql($sql, $objDbLink)
	{ 
		
		if((@$result = mysql_query ($sql, $objDbLink)) == false)
		{		
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);		
		}   
		else
		{	
			if ($check=mysql_fetch_array($result, MYSQL_BOTH))		
				return $check;
			return false;	
		}
	}
	function mysql_sql_message($sql)
	{
		$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>$company MySql Debugger:</font></strong><br>";
		$msg .= "Error in your Query: $sql<BR>";
		$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
		$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
		echo $msg;
	}
	function SelectMultiRecordsSql($sql, $objDbLink)
	{   
		if((@$result = mysql_query ($sql, $objDbLink))==FALSE)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);
			return false;	
		}   
		else
		{	
			$count = 0;
			$data = array();
			while ( $row = mysql_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
			return $data;
		}
	}
	
	/*
	*	check Field level validations
	*/
	function checkValidation($arrTransFields = array() , $arrMandFields=array(), $strFileName, $mapping){

		$returnData = '';
		$arrNotValidate = array();
		$arrAccepted = array();
		$arrRefNoMissing = array();
		$arrFinal = array();
		$counter = 0;
		$arrNotUpdate = array();
		$arrMappingReverse = array();
		$arrMappingReverse = array_flip($mapping);
		
		foreach($arrTransFields as $transKey => $transVal)
		{
			$transStatusExists = SelectFrom("Select transStatus from transactions where refNumberIM = '".$transVal['transactions.refNumberIM']."'");		
			$CheckUpdate = $transStatusExists['transStatus'];
			if(!empty($CheckUpdate) && $CheckUpdate != 'Authorize' && $CheckUpdate != 'Processing' && $CheckUpdate!= 'Pending'){
				$arrNotUpdate[$counter][] = $transVal['transactions.refNumberIM'];
				$arrNotUpdate[$counter][] = $CheckUpdate;
				$counter++;
				continue;			 
			}
			$acceptedFlag = true;
			$refNumber = $transVal['transactions.refNumberIM'];
		  
			if(empty($transVal['transactions.refNumberIM'])){
			
				$arrRefNoMissing[$counter][] = $transVal['customer.firstName'];
				$arrRefNoMissing[$counter][] = $transVal['transactions.localAmount'];
				$counter++;
				continue;
			}
		
			//check column count mis match
			if($transVal[0] == "~E~"){
				$arrNotValidate[$refNumber][] = $arrMappingReverse['transactions.refNumberIM']." column count mis match.";
				continue;
			}  
			
			foreach($transVal as $fkey => $fval){
			
				$arr_field = $arrMandFields[$fkey];
				$args = array("refNo"=>$refNumber,"fval"=>$fval,"fkey"=>$fkey);
			  
				if(!empty($arr_field)){				
					/* get conditional fields value and check validation*/
					if(!empty($arr_field["C"])){
						$arrCond = explode("|",$arr_field["C"]);
						//debugVar($arrCond);	
						//debugVar($arrCond[0]."-->".$transVal[$arrCond[0]]);
						if($arrCond[1] == $transVal[$arrCond[0]]){
							$returnFlag = inputvalidation($args,$mapping,$arrMandFields, $arrNotValidate);
							if(!$returnFlag)
								$acceptedFlag = false;
						}
					}elseif($arr_field["O"] == 'M' || $arr_field["O"] == 'O'){
						$returnFlag = inputvalidation($args,$mapping,$arrMandFields, $arrNotValidate);
						if(!$returnFlag)
							$acceptedFlag = false;
					}
					/* conditional fields code end*/				  
			    }
			} // end inner foreach loop
			
			if($acceptedFlag)
				$arrAccepted[] = $transVal;		  
				
		} // end outer foreach loop
				
		$arrFinal['A'] = $arrAccepted;
		$arrFinal['E'] = $arrNotValidate;
		$arrFinal['R'] = $arrRefNoMissing;
		$arrFinal['U'] = $arrNotUpdate;
		
		return $arrFinal;
	} // end checkValidation
	
	
	/*
	*	check and validate Date format
	*/
	function checkdatetimeformat($fval)
	{
		$boolTime = true;
		$boolDate = true;
		$returnFlag = true;
		
		// correct format: yyyy-mm-dd hh:mm:ss e.g 2011-11-01 08:45:55
		
		$arrDateTime = explode(" ",$fval);
		
		if(!empty($arrDateTime))
		{
			$arrD = $arrDateTime[0];
			$arrT = $arrDateTime[1];
			$arrDate = explode("-",$arrD);
			$arrTime = explode(":",$arrT);
			
			if(!empty($arrDate))
				$boolDate = checkdate($arrDate[1],$arrDate[2],$arrDate[0]);
			
			if(!empty($arrTime)){
				//if hours > 23 or mins > 59 or second > 59 than its not valid
				if($arrTime[0] > 23 || $arrTime[1] > 59 || $arrTime[2] > 59)
					$boolTime = "false1";
				
				//if hours , mins or second less than zero then its not valid
				if($arrTime[0] < 0 || $arrTime[1] < 0 || $arrTime[2] < 0)
					$boolTime = "false2";	
					
				// if time is not integer then its also not valid
				if (ereg("[^0-9]", $arrTime[0]))
					$boolTime = "false3";
				if (ereg("[^0-9]", $arrTime[1]))	
					$boolTime = "false4";
				if (ereg("[^0-9]", $arrTime[2]))
					$boolTime = "false5";
			}	
		
		}		
		if(empty($arrDateTime) || !$boolDate || !$boolTime)
			$returnFlag = false;
		
		return 	$returnFlag; 	
	} // end checkdateformat
	
	/*
	*	check and validate DateTime format
	*/
	
	function checkdateformat($fval){
		
		$fval = trim($fval);
		$boolDate = true;
		$returnFlag = true;
		// correct format: yyyy-mm-dd e.g 2011-11-01
		$arrDate = explode("-",$fval);		
		if(!empty($arrDate))
			$boolDate = checkdate($arrDate[1],$arrDate[2],$arrDate[0]);
		
		if(!$boolDate)	
			$returnFlag = false;
		
		return 	$returnFlag; 		
	} // end checkdatetimeformat
	
	
	/*
	*	check and validate Currency Code
	*/
	
	function checkCurrencyCode($code){
		$returnFlag = true;
		$returnVal = '';
		$strMessage = '';
		$objCurrency = new Currency($code);
		// check length
		if(strlen($code) != 3){ 
			$returnFlag = false;
			$strMessage .= ' length not equal to 3, ';			
		}	
		
		// check string
		if (ereg("[^A-Za-z]", $code)){
			$returnFlag = false;
			$strMessage .= ' not valid string, ';
		}
		
		// check valid ISO code	
		if(!empty($code)){
			$currencyCode = $objCurrency->find_currency($code);
			/*
				currency object return currency name , symbole , country and id e.g
				Array
				(
					[name] => Euro
					[symbol] => 
					[country] => eu
					[id] => EUR
				)
			*/	
			
			
			if(empty($currencyCode["name"])){
				$returnFlag = false;
				$strMessage .= ' not valid ISO Code, ';				
			}	
		}			
		$returnVal = $returnFlag."|".$strMessage;
		
		return $returnVal;
	} // checkCurrencyCode
	
	
	/*
	*	check and validate Country Code
	*/

	function checkCountryCode($code)
	{
		$objCountry = new I18N_ISO_3166;
		$returnFlag = true;
		$countryName ='';
		$strMessage = '';
		$returnVal = '';
		
		/*
		  system will check following thing
		  1- length should be three
		  2- valid ISO code
		  4- should be string
		  
		  if any condition failed then system will return false flag it mean provided code is not valid
		
		*/
		
			
		// check length
		if(strlen($code) != 3){ 
			$returnFlag = false;
			$strMessage .= ' length not equal to 3, ';			
		}	
		
		// check string
		if (ereg("[^A-Za-z]", $code)){
			$returnFlag = false;
			$strMessage .= ' not valid string, ';			
		}	
			
		// check valid ISO code	
		if(!empty($code))
		{
			$countryName = $objCountry->getCountry($code);
			
			if(empty($countryName)){
				$returnFlag = false;
				$strMessage .= ' not valid ISO Code, ';				
			}	
		}
		
		$returnVal = $returnFlag."|".$strMessage;
		
		return $returnVal;
	} // End checkCountryCode
	
	
	
	/*
	*	check and validate input fields
	*/	
	
	function inputvalidation($args,$mapping,$arrMandFields=array(), &$arrValidationOfTransfers)
	{
		
		$refNumber = $args["refNo"];
		$fval = $args["fval"];
		$fkey = $args["fkey"];
		$arrMappingReverse = array();
		$arrMappingReverse = array_flip($mapping);
		$returnVal = '';
		$setFlag = true;
		
		$check = $arrMandFields[$fkey];
		
		if($check["O"] == 'M')
		{
			if(empty($fval))
			{
				$setFlag = false;
				$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." is empty";
			}	
		}
		// check value length
		if(!empty($check["L"]))
		{
			if(strlen($fval) > $check["L"])
			{
				$setFlag = false;
				$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." length exceed";
			}	
		}
		// check field type
		if($check["T"] == 'string')
		{
			if(!empty($fval))
			{
				$fval = trim($fval);
				if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $fval))
				{
					$setFlag = false;
					$arrValidationOfTransfers[$refNumber][$intCounter] = $arrMappingReverse[$fkey]." not valid string";
				}	
			}	
		}
		if($check["T"] == 'integer')
		{
			if(!empty($fval))
			{
				$fval = trim($fval);
				if (ereg("[^0-9]", $fval))
				{
					$setFlag = false;
					$arrValidationOfTransfers[$refNumber][$intCounter] = $arrMappingReverse[$fkey]." not valid number";
				}	
			}	
		}
		if($check["T"] == 'double')
		{
			if(!empty($fval))
			{
				$fval = trim($fval);
				if (ereg("[^0-9.]", $fval))
				{
					$setFlag = false;
					$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." not valid number";
				}
			}
		}
		if($check["T"] == 'date')
		{
		
			if(!empty($fval))
			{
				$dateReturn = checkdateformat($fval);
				if(!$dateReturn)
				{	
					$setFlag = false;
					$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." date format is wrong";
				}
			}

		
		}
		if($check["T"] == 'datetime')
		{
			if(!empty($fval))
			{
				$dateTimeReturn = checkdatetimeformat(trim($fval));
				if(!$dateTimeReturn)
				{	
					$setFlag = false;
					$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." date & time format is wrong";
				}
			}	
		}
		
		// call custom function
		if(!empty($check["F"]))
		{
			if (is_callable($check["F"])) 
			{
				$returnFunction = call_user_func($check["F"],$fval);
				if(!empty($returnFunction))
				{
					$arrReturn = explode("|",$returnFunction);
					if(!$arrReturn[0])
					{
						$setFlag = false;
						$arrValidationOfTransfers[$refNumber][] = $arrMappingReverse[$fkey]." ".$arrReturn[1]; 
					}
				}
			}
		}
		return $setFlag;	
	} // end function
	
	
	
	/* check white list IPs */
	
		/* check white list IPs */
	
	function validateWhiteListIPs($strIPLIst){
		$arrHBSIP = array('52.18.203.117','172.21.0.1');
		$arrNobleUSAIp=array('197.159.135.45');
		//$arrCashExpressIp=array('89.151.79.115');
		
		$arrClientIPsListNumber = explode(',',$strIPLIst);		
		$arrClientIPsList = array_merge($arrNobleUSAIp,$arrClientIPsListNumber);
		$arrAllowIPs = array_merge($arrHBSIP,$arrClientIPsList);
		
		if(!in_array(getIP(), $arrAllowIPs)){
			return false;
		}
		return true;
	}
	
	
		/*
	
	 check beneficiary Agent */
	
	function validatePayerList($arrBenAllowedValue,$intPayerId){
		
			$arrBenAllowedAgent = explode(',',$arrBenAllowedValue);
		 if((!in_array($intPayerId,$arrBenAllowedAgent))|| empty($intPayerId)){
			return false;
		} 
		return true;
	}
	
	function retrievingClientInfo($agentID,$userName, $password, $mtlink){
	
		$strDistributorSql = "SELECT
				c.client_host,
				c.client_user,
				c.client_pwd,
				c.client_database, 	
				c.payex_client_id,
				c.client_name,
				ma.* 
			FROM
				".MTDB.".client as c 
			INNER JOIN ".MTDB.".manage_api as ma
				ON c.payex_client_id = ma.client_id
			WHERE 								
				ma.agentID  = '".$agentID."' AND ma.username = '".$userName."' AND ma.password = '".$password."'";
		return selectSql($strDistributorSql, $mtlink);
	}
	
	function connectToMT(){
		$mtlink = mysql_connect(MTHOST,MT_USER, MT_PASSWORD);		
		if(!mysql_select_db(MTDB, $mtlink)){
			return false;
		}
		return $mtlink;
	}
	
	function validateSenderInfo($senderInfo, $fieldMapping, $fieldChecks){
	
		if(empty($senderInfo)){
			return "empty";
		}	

		global $bank_codes;
		
		if(is_object($senderInfo)){
			$senderInfo = (array)$senderInfo;
		}
		$arrClientData = array();
		
		if($senderInfo['transType'] == 'AC'){
			if(trim($senderInfo['beneficiaryBankCode']) == '0' && empty($senderInfo['beneficiaryBankCode'])){
						return "beneficiaryBankCode::missing";
					}
			if(trim($senderInfo['beneficiaryAccNo']) == '0' && empty($senderInfo['beneficiaryAccNo'])){
						return "beneficiaryAccNo::missing";
					}
			if(trim($senderInfo['beneficiaryAccountType']) == '0' && empty($senderInfo['beneficiaryAccountType'])){
						return "beneficiaryAccountType::missing";
					}
					
		
		}
		
			
		if(!in_array($senderInfo['beneficiaryBankCode'],$bank_codes))
		{
			return "beneficiaryBankCode::incorrect";
		}
		
		if(trim($senderInfo['beneficiaryBankID']) == '0' && empty($senderInfo['beneficiaryBankID'])){
						$senderInfo['beneficiaryBankID'] = "";
		}
		
		foreach($senderInfo as $srtField => $strValue){
			$strActualField = '';
			/*	check if field exist in mapping */
			
			if(array_key_exists(trim($srtField),$fieldMapping)){
				$strActualField = $fieldMapping[$srtField];
			}/*  elseif(trim($srtField))
			{
				
			} */
			else{
				return "$srtField::Missing";
				
			} 
			
			/*	check Field constrains if defined */		
			
			if(!empty($strActualField) && array_key_exists(trim($strActualField),$fieldChecks)){
			
				if($fieldChecks[$strActualField]['O'] == 'M'){
					if(trim($strValue) == '' && empty($strValue)){
						return "$srtField::missing";
					}
					if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', trim($strValue)))
					{
					return "$srtField::string";
					}	
					if(strlen($strValue) > $fieldChecks[$strActualField]['L']){
					return "$srtField::limitexceeded";
				}
				
				if(!empty($fieldChecks[$strActualField]['T'])){
					switch($fieldChecks[$strActualField]['T']){						
						case 'integer':
							if(!filter_var($strValue, FILTER_VALIDATE_INT)){
								return "$srtField::integer";
							}
							break;
						case 'date':
							if(!checkdateformat($strValue)){
								return "$srtField::date";
							}
							break;
						case 'email':							
							if(!filter_var($strValue, FILTER_VALIDATE_EMAIL)){
								return "$srtField::email";
							}
							break;
						case 'string':							
						default:					
							if(!filter_var($strValue,FILTER_SANITIZE_STRING) && !empty($strValue)){
								return "$srtField::string";
							}
							break;
					}
				} // END if 
				
				}
				
								
			} else{
					return "$srtField::undefined";
			}			
			$arrClientData[$strActualField] = $strValue;
			
		} // end foreach 		
		return $arrClientData;	
	}
	/* Insert Customers information */
	function insertCustomerInPayex($finalArray){
		
		$customerID = '';
		$customerArr = array();		
		$customerIDTYpeArr = array();
		$customerIdUserTypeArr = array();
		// print_r($finalArray);
		/* Parse and fetch customer information */
		foreach($finalArray as $key => $val)
		{
			$devide = explode(".",$key);
			if($devide[0] == "customer")
			{
				$customerArr[$devide[1]] = $val;				
			}			
		}	
		$customerOtherInfo = array();
		if(isset($customerArr['other_info']) && is_array($customerArr['other_info']) && !empty($customerArr['other_info'])){
			$customerOtherInfo = $customerArr['other_info'];		
			unset($customerArr['other_info']);
		}
		unset($customerArr['customerID']);
		/* inserting customer info into database */		
		$customerID = insertMySql('customer', $customerArr, true, "");		
		if(!empty($customerID) && trim($customerID) != ''){
						
			/* ID Type and User Type for customers */			
			foreach($customerOtherInfo as $key => $custValue){
				$idTypeID = '';
				$userIdTypeID = '';
				if(isset($custValue['id_types.title']) && !empty($custValue['id_types.title'])){
					$strIdTypeTitle = trim($custValue['id_types.title']);
					$idTypeQuery = "SELECT id from id_types WHERE title = '".$strIdTypeTitle. "'";
					$idTypeRes = selectFrom($idTypeQuery);
					$idTypeID = $idTypeRes['id'];
					if(empty($idTypeID)){
						$idTypeID = insertMySql('id_types', array('title' => $strIdTypeTitle), true, "");
					}
				}
				if(isset($custValue['user_id_types.id_number']) && !empty($custValue['user_id_types.id_number']) && !empty($idTypeID)){
					$arrUserIDType['id_number'] = $custValue['user_id_types.id_number'];
					$arrUserIDType['expiry_date'] = $custValue['user_id_types.expiry_date'];
					$arrUserIDType['id_type_id'] = $idTypeID;
					$arrUserIDType['user_type'] = 'C';
					$arrUserIDType['user_id'] = $customerID;
					$userIdTypeID = insertMySql('user_id_types', $arrUserIDType, true, "");
				}
			}
		} // END if
		
		return $customerID;
	}	
	/* END */
	
	/* Insert Transaction, Customers, Beneficiary Bank Details information etc.  */
	function insertPayexData($finalArray){		
	
		$transId ='';
		$transArray = array();
		
		$customerID = '';
		$customerArr = array();
		$customerBankArr = array();
		$customerBankDetailArr = array();
		$customerIDTYpeArr = array();
		$customerIdUserTypeArr = array();
		
		$beneID ='';
		$benArr = array();
		$benBankArr = array();
		$benBankDetailArr = array();
		$benIDTYpeArr = array();
		$benIdUserTypeArr = array();	
		
		$TRANSACCOUNTTYPE = array(
			"1" => "Savings",
			"2" => "Current",
			"3" => "Loans",
			"4" => "Others"
			);
			
		foreach($finalArray as $key => $val)
		{
			$devide = explode(".",$key);			
			if($devide[0] == "transactions")
			{
				$transArray[$devide[1]] = $val;
			}
			elseif($devide[0] == "customer")
			{
				$customerArr[$devide[1]] = $val;				
			}
			elseif($devide[0] == "beneficiary")
			{
				$benArr[$devide[1]] = $val;								
			}
			elseif($devide[0] == "bankDetails" )
			{
				$customerBankDetailArr[$devide[1]] = $val;
			}elseif($devide[0] == "ben_banks")
			{
				(string)$benBankDetailArr[$devide[1]] = (string) $val;
			}
		}
			


/* ============= check eligibility for instant transfer =======*/

$interswitchID = CONFIG_INTERSWITCH_BENAGENTID;
$custAgentID = $customerArr['agentID'];

$query = selectFrom("SELECT authorizedFor from admin where userID=".$custAgentID."");
//return $query['authorizedFor'];
$find = explode(",",$query['authorizedFor']);
if(in_array("Instant Transfer", $find)){
	$interswitchFlag = 1;
}else{
	$interswitchFlag = 0;
}

if($transArray['transType'] == "Bank Transfer"){
	if($interswitchFlag){
		$transArray['transType'] = "Instant Transfer";
		$transArray['benAgentID'] =$interswitchID; 	
	}else{
		$transArray['benAgentID'] = CONFIG_BENAGENTID;	
	}
}

		$customerOtherInfo = array();
		if(isset($customerArr['other_info']) && is_array($customerArr['other_info']) && !empty($customerArr['other_info'])){
			$customerOtherInfo = $customerArr['other_info'];		
			unset($customerArr['other_info']);
		}
		
		$beneOtherInfo = array();
		if(isset($benArr['other_info']) && is_array($benArr['other_info']) && !empty($benArr['other_info'])){
			$beneOtherInfo = $benArr['other_info'];		
			unset($benArr['other_info']);
		}
		/* inserting customer info into database */
		$customerID = insertMySql('customer', $customerArr, true, "");
		
		/* inserting beneficiary info into database */
		if(!empty($customerID) && trim($customerID) != ''){
			$benArr['customerID'] = $customerID;
			$beneID = insertMySql('beneficiary', $benArr, true, "");
		}
		
		/* inserting transaction info into database */
		$transArray['customerID'] = $customerID;
		$transArray['benID'] = $beneID;		
		$transArray['custAgentID'] = $customerArr['agentID'];
		unset($transArray['senderReference']);
		unset($transArray['beneficiaryReference']);
		
		/* Fetching MAX refNumberIM to increment into and put for new record */
	/* 	$maxRefNoResult = selectFrom(" SELECT refNumberIM AS MaxRefIM FROM transactions ORDER BY	CAST(trim(SUBSTRING(refNumberIM, POSITION('-' IN refNumberIM) + 1)) AS SIGNED) DESC LIMIT 0,1 ");
		$refNumberIM = explode("-", $maxRefNoResult['MaxRefIM']);
		$transArray['refNumberIM'] = $refNumberIM[0]."-".($refNumberIM[1]+1);
		 */
		/*
		*As discussed with Miss Wajiha refNumber  and refNumber should be same for same for weblink
		*/
		
		$transArray['refNumberIM']= $transArray['refNumber'];
		$transArray['trans_source']= 'A';
		
		/*Wajiha conditions 08.08.2017*/
		$transArray['locking'] = 'N';
		$transArray['is_parsed'] = 'N';
		$transArray['is_ack'] = 'Y';
		/*End Wajiha conditions 08.08.2017*/

		//$transArray['transStatus']= 'Pending';
	
		/*-----------Wajiha 10.08.2017 --------------*/	
		if(array_key_exists($benBankDetailArr['accountType'],$TRANSACCOUNTTYPE))
					{
						$benBankDetailArr['accountType']=$TRANSACCOUNTTYPE[$benBankDetailArr['accountType']];
					}else
					{
						$benBankDetailArr['accountType']='CHECKING';
					}
		
		// $benBankDetailArr['accountType'] = 'CHECKING'; Regular
		/*-----------End Wajiha 10.08.2017--------------*/
		//$transArray['IMFee']=6.000;
		
		
	 	if(!empty($benBankDetailArr['bankId']))
		{ 
				$strBanks="SELECT * FROM  banks WHERE id='".$benBankDetailArr['bankId']."'";
				$strBanksResult = selectFrom($strBanks);
			
		}
		
		$transArray['benAgentID'] = $strBanksResult['bk_ida_id'];
		
		$transID = insertMySql('transactions', $transArray, true, "");

//insert distrefnumber		
if($transArray['transType']=='Bank Transfer' || $transArray['transType']=='Instant Transfer'){
	$distArr['transID'] = $transID;
	$distId = insertMySql('distRefNumber', $distArr, true ,$payexLink);
}	
		
			
		
		if(!empty($transID) && ( $transArray['transType']=='Bank Transfer' OR   $transArray['transType']=='Instant Transfer') && !empty($strBanksResult))
		{
			
			
			$transBankDetailsArray['transID']=$transID;
			$transBankDetailsArray['benID']=$beneID;
			$transBankDetailsArray['bankName']=$strBanksResult['name'];
			$transBankDetailsArray['BankCode']=$benBankDetailArr['bankCode'];
			$transBankDetailsArray['BranchName']=$strBanksResult['branchName'];
			$transBankDetailsArray['accNo']=(string)$customerBankDetailArr['accNo']; 
			$transBankDetailsArray['branchCode']='';
			$transBankDetailsArray['branchAddress']=$strBanksResult['branchAddress'];
			$transBankDetailsArray['IBAN']='';
			$transBankDetailsArray['swiftCode']=$strBanksResult['swiftCode'];
			$transBankDetailsArray['accountType']=$benBankDetailArr['accountType'];
			$transBankDetailsArray['sortCode']=$strBanksResult['sortCode'];
			$transBankDetailsArray['accountName']='';
			$transBankDetailsArray['Remarks']='';
			
			$benBankTransDetailAdded = insertMySql('bankDetails', $transBankDetailsArray, true, "");
		}
		if(!empty($customerID) && trim($customerID) != ''){
			/* inserting bank detail of customer */
			$customerBankDetailArr['custID'] = $customerID;
			$customerBankDetailArr['benID'] = $beneID;		
			$customerBankDetailArr['transID'] = $transID;
			$strBankDetails="SELECT count(transID) as count FROM bankDetails WHERE transID='.$transID.'";
			$strBankDetailsResult = selectFrom($strBankDetails);	
			if($strBankDetailsResult['count']>0)
			{
				//Commented was at 21.08.2017 
				//$customerBankDetailID = insertMySql('bankDetails', $customerBankDetailArr, true, "");
			}	
			/* end */
			
			/* ID Type and User Type for customers */			
			foreach($customerOtherInfo as $key => $custValue){
				$idTypeID = '';
				$userIdTypeID = '';
				if(isset($custValue['id_types.title']) && !empty($custValue['id_types.title'])){
					$strIdTypeTitle = trim($custValue['id_types.title']);
					$idTypeQuery = "SELECT id from id_types WHERE title = '".$strIdTypeTitle. "'";
					$idTypeRes = selectFrom($idTypeQuery);
					$idTypeID = $idTypeRes['id'];
					if(empty($idTypeID)){
						$idTypeID = insertMySql('id_types', array('title' => $strIdTypeTitle), true, "");
					}
				}
				if(isset($custValue['user_id_types.id_number']) && !empty($custValue['user_id_types.id_number']) && !empty($idTypeID)){
					$arrUserIDType['id_number'] = $custValue['user_id_types.id_number'];
					$arrUserIDType['expiry_date'] = $custValue['user_id_types.expiry_date'];
					$arrUserIDType['id_type_id'] = $idTypeID;
					$arrUserIDType['user_type'] = 'C';
					$arrUserIDType['user_id'] = $customerID;
					$userIdTypeID = insertMySql('user_id_types', $arrUserIDType, true, "");
				}
			}
		} // END if
				
		if(!empty($beneID) && trim($beneID) != ''){
			/* Bank detail for beneficiary */
			$benBankDetailArr['benId'] = $beneID;
			$benBankDetailID = insertMySql('ben_banks', $benBankDetailArr, true, "");
			/* end */
			
			/* ID Type and User Type for beneficiary */
			
			if(isset($beneOtherInfo['id_types.title']) && !empty($beneOtherInfo['id_types.title'])){
				$beneIdTypeID = '';			
				$beneUserIdTypeID = '';	
				
				$strIdTypeTitle = trim($beneOtherInfo['id_types.title']);
				$beneIdTypeCategory = $beneOtherInfo['id_types.category'];
				
				$idTypeQuery = "SELECT id from id_types WHERE title = '".$strIdTypeTitle. "'";
				$idTypeRes = selectFrom($idTypeQuery);
				$beneIdTypeID = $idTypeRes['id'];
				if(empty($beneIdTypeID)){
					$beneIdTypeID = insertMySql('id_types', array('title' => $strIdTypeTitle, 'category' => $beneIdTypeCategory), true, "");
				}
				if(isset($beneOtherInfo['user_id_types.id_number']) && !empty($beneOtherInfo['user_id_types.id_number']) && !empty($idTypeID)){
					$arrUserIDType['id_number'] = $beneOtherInfo['user_id_types.id_number'];
					$arrUserIDType['expiry_date'] = $beneOtherInfo['user_id_types.expiry_date'];
					$arrUserIDType['issue_date'] = $beneOtherInfo['user_id_types.issue_date'];
					$arrUserIDType['countryName'] = $beneOtherInfo['user_id_types.country'];
					$arrUserIDType['id_type_id'] = $idTypeID;
					$arrUserIDType['user_type'] = 'B';
					$arrUserIDType['user_id'] = $beneID;
					$userIdTypeID = insertMySql('user_id_types', $arrUserIDType, true, "");
				}
			}
			/* ID Type and User Type for beneficiary END */
		} // END if 
		 $currentStamp=time(); 
		/* return response base on success or failure scenario */
		if(!empty($transID) && !empty($customerID) && $transArray['transType']=='Bank Transfer' && !empty($beneID) && !empty($benBankTransDetailAdded)){
			return "1";
		}else
		if(!empty($transID) && !empty($customerID) && $transArray['transType']=='Bank Transfer' && !empty($beneID)){
			$queryDeleteUpdate="Update transactions set transStatus='Delete',refNumberIM='".$transArray['refNumberIM'].'-'.$currentStamp."'
where transID=".$transID."";
			update($queryDeleteUpdate);
			return "4";
		}
		else
		if(!empty($transID) && !empty($customerID) && $transArray['transType']!='Bank Transfer' && !empty($beneID)){
			return "1";
		}
		elseif(!empty($transID) && !empty($customerID) && empty($beneID)){
			return "2";
		}elseif(!empty($transID) && empty($customerID) && empty($beneID)){
			return "3";
		}else{
			return "4";
		}
		
	}
	
	function insertMySql($table, $data, $printstatus=false, $payexLink = "")
	{
		$return = true;
		if(!is_array($data))
		{
			$msg = "Invalid input data type";
		}	
		$field_names = array();
		$field_names = array_keys($data);
		$values = array_values($data);
		
		$field_names = array_string($field_names,',',false,"`[VALUE]`");
		
		$values = array_string($values,',',false,"'[VALUE]'");
		$values = str_replace("'NULL'",'NULL',$values);
		$query = "INSERT INTO `".$table."` (".$field_names.") VALUES(".$values.");";
		
		$result = mysql_query($query);
		$return = mysql_insert_id();
		
		if($printstatus)
		{
			if(!$result)
			 {				
				die("Invalid query:<br>".$query."<br>".mysql_errno()."<br>".mysql_error()); 
				//die("Invalid query:<br>".$query."<br>".mysql_errno()."<br>".mysql_error());
				$return = false;
			}
		}

		return $return;
	}
	
	function beforeInsertionsWS($param, $callFrom = '')
	{
		$arrPhotoCat = array(
			'GOV' => 'Any government/national ID',
			'PAS' => 'Passport',
			'DRV' => 'Driving licence'
		);
		
		
		$arrIdsTypes = array(
			'01' => "National ID Card",
			'02' => "Canada Permanent Resident Card",
			'03' => "National Social Security Cards",
			'04' => "Certificate of Alien Registration",
			'05' => "Voting card",
			'06' => "SSN",
			'07' => "Municipal Identity Card",
			'08' => "Travel Document",
			'09' => "Other",
			'10' => "TIN",			
			'11' => "Passport",
			'12'=> "Driving Licence",
		);
		
		$arrRelationToSender = array(
			'R01' => "Father",
			'R02' => "Mother",
			'R03' => "Uncle",
			'R04' => "Aunt",
			'R05' => "Son",
			'R06' => "Daughter",
			'R07' => "Father-in law",
			'R08' => "Mother-in-law",
			'R09' => "Brother-in-law",
			'R10' => "Sister-in-law",
			'R11' => "Son-in-law",
			'R12' => "Daughter-in-law",
			'R13' => "Grand father",
			'R14' => "Grand mother",
			'R15' => "Grand son",
			'R16' => "Grand daughter",
			'R17' => "Other",
		);
		
				
		$arrTransType = array(
			"COC" => "Pick up",
			"AC" => "Bank Transfer",			
			"DD" => "Demand Draft",
			"MT" => "Mobile Top us",
			"ATM" => "ATM card Transactions",
			"PCT" => "Pardes Card Transactions",
			"HD" => "Home Delivery"
		);
		
		$arrAccountType = array(
			"01" => "Savings",
			"02" => "Checking",
			"03" => "Loan Account"
			
		);

		$arrTransStatus = array(
			"001" => "Pending",
			"002" => "Processing",
			"003" => "Authorize",
			"004" => "Cancelled",
			"005" => "Awaiting Cancellation",
			"006" => "Hold",
			"007" => "Paid"
		);
		
		$arrTransPurpose = array(
			"P01" => "Family Support",
			"P02" => "Allowance",
			"P03" => "Business / Office Expenses",
			"P04" => "Donation",
			"P05" => "Gift",
			"P06" => "Hospitalization",
			"P07" => "Loan Payment",
			"P08" => "Merchandise Purchase",
			"P09" => "Others",
			"P010" => "Payment of insurance Premium",
			"P011" => "Payment of Real Estate Mortgage",
			"P012" => "Promotional Expenses",
			"P013" => "Reality Investment",
			"P014" => "Savings",
			"P015" => "Travel Expenses",
			"P016" => "Tuition",
			"P017" => "Purchase Land/House"
		);
		
		$arrTransFundSource = array(
			"S02" => "Salary",
			"S03" => "Saving",
			"S04" => "Loan",
			"S05" => "Insurance Proceeds",
			"S06" => "Others",
		);
		
		$objCountryCode = new I18N_ISO_3166;
		$returnArray = array();
		
		if($param['transactions.refNumber'] != '' || $callFrom == 'customer')
		{
			foreach($param as $key => $val)
			{
				$returnArray[$key] = trim(trim($val, '"'));
			}
			if($callFrom == ""){
				$returnArray['transactions.restoreDate'] = date("Y-m-d h:i:s");
				$returnArray['transactions.transDate'] = date("Y-m-d h:i:s");
				//$returnArray['transactions.transDate'] = date("Y-m-d h:i:s",strtotime($returnArray['transactions.transDate']));			
				$returnArray['beneficiary.created'] = date("Y-m-d");
				$returnArray['transactions.transType'] = $arrTransType[$returnArray['transactions.transType']];
				$returnArray['transactions.transStatus'] = $arrTransStatus[$returnArray['transactions.transStatus']];
				$returnArray['transactions.transactionPurpose'] = $arrTransPurpose[$returnArray['transactions.transactionPurpose']];
				$returnArray['transactions.fundSources'] = $arrTransFundSource[$returnArray['transactions.fundSources']];
			}
			
			$custCountryName =$objCountryCode->getCountry($returnArray['customer.Country']);
			$returnArray['customer.created'] = date("Y-m-d h:i:s");
			$returnArray['customer.Country'] = $custCountryName;
			
			
			/*
				Sender country name and currency will fetch from payex country table please make sure country 	table contains  correct data
			*/	
			
			
			
			if($callFrom == ''){
				$returnArray['transactions.fromCountry'] = $custCountryName;
				$returnArray['beneficiary.SOF'] = $arrRelationToSender[$returnArray['beneficiary.SOF']];
			
			
				/*
					Beneficiary country name and currency will fetch from payex country table please make sure country table contains  correct data
				*/
					$benCountryName =$objCountryCode->getCountry($returnArray['beneficiary.Country']);
					$returnArray['beneficiary.Country'] = $benCountryName;
					$returnArray['transactions.toCountry'] = $benCountryName;
				
				
				/**
					Search for collection point againt the Payout code
				*/
				
				/* bank transfer */
				$returnArray['bankDetails.accountType'] = $arrAccountType[$returnArray['bankDetails.accountType']];
				
				if(isset($returnArray['bankDetails.bankName']) && !empty($returnArray['bankDetails.bankName'])){	
					$strBankSql = "SELECT * FROM banks WHERE  id = '".trim($returnArray['bankDetails.bankName'])."'";
					$arrBankDetails =selectFrom($strBankSql);
					$returnArray['bankDetails.bankName'] = $arrBankDetails["name"];
					$returnArray['bankDetails.branchAddress'] = $arrBankDetails["branchAddress"];
					$returnArray['bankDetails.BranchName'] = $arrBankDetails["branchName"];
					$returnArray['bankDetails.sortCode'] = $arrBankDetails["sortCode"];
					$returnArray['bankDetails.BranchCity'] = $arrBankDetails["extra3"];			
				}
				//$returnArray['transactions.IMFee'] = $returnArray['transactions.AgentComm'];
				$returnArray['transactions.totalAmount'] = $returnArray['transactions.transAmount'] + $returnArray['transactions.AgentComm'];
			}
			
			/* ID Type and Users ID Type Details for Customers */
			$returnArray['customer.other_info'][0]['id_types.title'] = '';
			$returnArray['customer.other_info'][0]['user_id_types.id_number'] = '';
			$returnArray['customer.other_info'][0]['user_id_types.expiry_date'] = '';
			$returnArray['customer.other_info'][1]['id_types.title'] = '';
			$returnArray['customer.other_info'][1]['user_id_types.id_number'] = '';
			$returnArray['customer.other_info'][1]['user_id_types.expiry_date'] = '';
			$returnArray['customer.other_info']['banks.country'] = '';
			
			
			if(isset($returnArray['customer.title']) && !empty($returnArray['customer.title'])){
				$returnArray['customer.other_info'][0]['id_types.title'] = $arrIdsTypes[$returnArray['customer.title']];
			}
			
			if(isset($returnArray['customer.id_number']) && !empty($returnArray['customer.id_number'])){
				$returnArray['customer.other_info'][0]['user_id_types.id_number'] = $returnArray['customer.id_number'];
			}
			
			if(isset($returnArray['customer.expiry_date']) && !empty($returnArray['customer.expiry_date'])){
				$returnArray['customer.other_info'][0]['user_id_types.expiry_date'] = $returnArray['customer.expiry_date'];
			}			
			
			if(isset($returnArray['customer.title2']) && !empty($returnArray['customer.title2'])){
				$returnArray['customer.other_info'][1]['id_types.title'] = $arrIdsTypes[$returnArray['customer.title2']];
			}
			
			if(isset($returnArray['customer.id_number2']) && !empty($returnArray['customer.id_number2'])){
				$returnArray['customer.other_info'][1]['user_id_types.id_number'] = $returnArray['customer.id_number2'];
			}
			
			if(isset($returnArray['customer.expiry_date2']) && !empty($returnArray['customer.expiry_date2'])){
				$returnArray['customer.other_info'][1]['user_id_types.expiry_date'] = $returnArray['customer.expiry_date2'];
			}
			
			if(isset($returnArray['customer.bankCountry']) && !empty($returnArray['customer.bankCountry'])){
				$returnArray['customer.other_info']['banks.country'] = $returnArray['customer.bankCountry'];
			}
			
			/* unset fields because does not exist in customer table */
			unset($returnArray['customer.title']);
			unset($returnArray['customer.id_number']);
			unset($returnArray['customer.expiry_date']);
			unset($returnArray['customer.title2']);
			unset($returnArray['customer.id_number2']);
			unset($returnArray['customer.expiry_date2']);
			unset($returnArray['customer.bankCountry']);
			
			
			/* ID Type and Users ID Type Details for Customers */
			if($callFrom == ''){
				$returnArray['beneficiary.other_info']['id_types.category'] = '';
				$returnArray['beneficiary.other_info']['id_types.title'] = '';
				$returnArray['beneficiary.other_info']['user_id_types.id_number'] = '';
				$returnArray['beneficiary.other_info']['banks.country'] = '';
				$returnArray['beneficiary.other_info']['banks.branchCode'] = '';
				$returnArray['beneficiary.other_info']['user_id_types.expiry_date'] = '';
				$returnArray['beneficiary.other_info']['user_id_types.countryName'] = '';
				$returnArray['beneficiary.other_info']['user_id_types.issue_date'] = '';
							
				if(isset($returnArray['beneficiary.category3']) && !empty($returnArray['beneficiary.category3'])){
					$returnArray['beneficiary.other_info']['id_types.category'] = $arrPhotoCat[$returnArray['beneficiary.category3']];
				}
				if(isset($returnArray['beneficiary.title3']) && !empty($returnArray['beneficiary.title3'])){
					$returnArray['beneficiary.other_info']['id_types.title'] = $arrIdsTypes[$returnArray['beneficiary.title3']];	
				}
				if(isset($returnArray['beneficiary.id_number3']) && !empty($returnArray['beneficiary.id_number3'])){
					$returnArray['beneficiary.other_info']['user_id_types.id_number'] = $returnArray['beneficiary.id_number3'];
				}
				if(isset($returnArray['beneficiary.bankCountry']) && !empty($returnArray['beneficiary.bankCountry'])){
					$returnArray['beneficiary.other_info']['banks.country'] = $returnArray['beneficiary.bankCountry'];
				}
				if(isset($returnArray['beneficiary.branchCode']) && !empty($returnArray['beneficiary.branchCode'])){
					$returnArray['beneficiary.other_info']['banks.branchCode'] = $returnArray['beneficiary.branchCode'];
				}
				if(isset($returnArray['beneficiary.expiry_date3']) && !empty($returnArray['beneficiary.expiry_date3'])){
					$returnArray['beneficiary.other_info']['user_id_types.expiry_date'] = $returnArray['beneficiary.expiry_date3'];
				}
				if(isset($returnArray['beneficiary.issue_date3']) && !empty($returnArray['beneficiary.issue_date3'])){
					$returnArray['beneficiary.other_info']['user_id_types.issue_date'] = $returnArray['beneficiary.issue_date3'];
				}
				if(isset($returnArray['beneficiary.country3']) && !empty($returnArray['beneficiary.country3'])){
					$returnArray['beneficiary.other_info']['user_id_types.countryName'] = $returnArray['beneficiary.country3'];
				}
				unset($returnArray['beneficiary.category3']);
				unset($returnArray['beneficiary.title3']);
				unset($returnArray['beneficiary.id_number3']);
				unset($returnArray['beneficiary.bankCountry']);
				unset($returnArray['beneficiary.branchCode']);
				unset($returnArray['beneficiary.expiry_date3']);
				unset($returnArray['beneficiary.issue_date3']);
				unset($returnArray['beneficiary.country3']);
			}
		}
		return $returnArray;
	}
	
	function setWSParameters($headers, $body){
		return json_encode(array(
			"WS_Header" => $headers,
			"WS_Body" => $body,
		));
	}
	function prepareWSLog($client='0',$req, $res, $otherInfo, $methodName, $resCode){
		//$encodeRef= json_decode($res);
		$refNumberRequest = json_decode($req, true);
		$remarksResponse = json_decode($res, true);
		//$resCode= json_encode($resCode);

		addLogEntry(
			array(
				'apiID' => $otherInfo->apiCode,
				'request' => $req,
				'response' => $res,
				'methodName' => $methodName,
				'responseCode' => $resCode,
				'username' => $otherInfo->username,
				'password' => $otherInfo->password,
				'agenID' => $otherInfo->agentID,
				'remarks' => $remarksResponse['WS_Body']['responseMessage'] . $otherInfo->apiCode . '',
				'refNumber' => $refNumberRequest['WS_Body']['transRefNumber'],
				'client_id' => $client
			)
		);	
	}
	function array_string($array,$seperator='',$enablelast=true,$pattern=false){
		$keys= array_keys($array);
		$String = '';
		for($i=0;$i<sizeof($array);$i++){
			if($i == sizeof($array)-1){if(!$enablelast){$seperator='';}}
			$key = $keys[$i];
			$value = $array[$keys[$i]];
			$Values .= $value.$seperator;
			if($pattern){
				$subject = $pattern;
				$subject = str_replace('[KEY]',$key,$pattern);
				$subject = str_replace('[VALUE]',$value,$subject).$seperator;
				$PatternString .= $subject;
			}
		}
		$String = $Values;
			if($pattern){$String = $PatternString;}
		$return = $String;
		return $return;
	}
	function addLogEntry($arrApiLog){		
		$logQuery = "INSERT INTO `test_MT`.`api_logs` (`apiID`,`request`,`response`,`methodName`,`responseCode`,`dated` ,`username`,`password`,`agenID`,`remarks`,`refNumber`,`client_id`) VALUES ('".$arrApiLog['apiID']."', '".$arrApiLog['request']."', '".$arrApiLog['response']."', '".$arrApiLog['methodName']."', '".$arrApiLog['responseCode']."',	CURRENT_TIMESTAMP() , '".$arrApiLog['username']."', '".$arrApiLog['password']."', '".$arrApiLog['agenID']."', '".$arrApiLog['remarks']."', '".$arrApiLog['refNumber']."', '".$arrApiLog['client_id']."') ";
		selectFrom($logQuery);
	}
		



?>
