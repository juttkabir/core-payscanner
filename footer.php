<div class="footer_area">
<div class="upper_footer">
<div class="upper_footer_wrapper">
<div class="footer_left">
<a href="http://www.premfx.com"><img src="images/logo_png.png"></a>

</div>
<div class="footer_right">
<div class="footer_social_wrapper">
<a href="https://www.linkedin.com/company/premier-fx"><img class="social_logos" src="images/in_logo.png"></a>
<a href="https://www.facebook.com/premier.fx"><img class="social_logos" src="images/fb_logo.png"></a>
</div>
</div>
</div>
</div>
<div class="lower_footer">
<div class="lower_footer_wrapper">
<div class="footer_left">
<span class="footer_address">PFX Online is a registered trademark of Premier FX Ltd.
  Copyright (c) <?php echo date("Y"); ?> Premier FX Ltd. All rights reserved.</span>
</div>
<div class="footer_right">
<ul class="footer_menu">
<li class="footer_menu_list"><a href="#">Terms & Conditions</a></li><span class="footer_sep">|</span>
<li class="footer_menu_list"><a href="#">Privacy Policy</a></li><span class="footer_sep">|</span>
<li class="footer_menu_list"><a href="#">Cookie Policy</a></li>
</ul>
</div>

</div>
</div>
</div>