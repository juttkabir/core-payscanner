<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Currency Exchange Specialists, FX, money transfers,Algarve,Portugal,PremierFX</title>
<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="imagetoolbar" content="no">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="javascript/jquery.validate.js"></script>
<script type="text/javascript" src="javascript/date.js"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="javascript/ajaxupload.js"></script>
<script src="scripts/jquery-1.7.2.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap-twipsy.js"></script>
<script src="scripts/bootstrap-popover.js"></script>
<script type="text/javascript" src="../admin/javascript/jquery.maskedinput.min.js"></script>
</head>
			
<body>
<a name="top"></a>
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link " target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link menu_selected" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Frequently Asked Questions</h2>
<?php include('top-menu.php');?>
<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">FAQs</p>
<p class="content_subheading">Answers to questions our customers often ask us. If the answer to your question is not here email or call us and we'll be happy to help you. </p>
</div>


<!--faqs questions links -->
<ul class="faqs_list">
							  		<li><a href="#1">How does Premier FX save me money?</a></li>
									<li><a href="#2">What costs / fees can I expect to pay?</a></li>
									<li><a href="#3">How do I know my money is safe?</a></li>
									<li><a href="#4">How long do you take to transfer funds?</a></li>
									<li><a href="#5">Why shouldn't I just use my bank?</a></li>
									<li><a href="#6">What happens if I am unable to pay for my trade on the day it falls due?</a></li>
									<li><a href="#7">What is a foreign exchange company?</a></li>
									<li><a href="#8">How does Premier FX make its money if there are no fees or commissions?</a></li>
							  </ul>
							  
<div class="questions_area">
<a name="1"></a>
<p class="question_heading">How does Premier FX save me money?</p>
<p class="question_answer">As a commercial client of a major international bank we transact a high volume of deals. This means we can access very competitive exchange rates from the foreign exchange markets. Coupled with our low overheads this enables us to pass on great savings to our clients. We do not charge our clients for transfers.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="2"></a>
<p class="question_heading">What costs / fees can I expect to pay?</p>
<p class="question_answer">Premier FX do not charge any fees for any of their services, so if you are worried that somewhere down the process you will be hit with a bill, dont be! Our account opening is free of charge, as are our transfers and even the one-to-one market trading advice given by our Account Executives. If you transfer your funds to our client account via on-line banking, in the majority of cases you will find the whole process completely free from charges. </p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="3"></a>
<p class="question_heading">How do I know my money is safe?</p>
<p class="question_answer">Premier FX hold secure clients accounts with a major international bank in London, which are totally separate from our day-to-day business accounts. These accounts are very similar to how Lawyer / Solicitor clients accounts work. We are also registered with HM Customs & Excise, registration number 1223621400000.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="4"></a>
<p class="question_heading">How long do you take to transfer funds?</p>
<p class="question_answer">As soon as we receive your payment we transfer your currency straight away. It should take no longer than 48 hours to reach the designated bank account anywhere in the world. In most cases it is even quicker.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="5"></a>
<p class="question_heading">Why shouldn't I just use my bank?</p>
<p class="question_answer">Banks are usually 4 - 8 % more expensive on their exchange rates, sometimes a little less, often a lot more!  They do not have the expertise on a local level to provide any guidance on Currency Exchange nor do they monitor the markets. We have our rates in front of us when you talk to us and we make sure we are up to date with the latest market movements. </p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="6"></a>
<p class="question_heading">What happens if I am unable to pay for my trade on the day it falls due?</p>
<p class="question_answer">Contact us immediately and your Account Manager can help you in this situation.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="7"></a>
<p class="question_heading">What is a foreign exchange company?</p>
<p class="question_answer">A foreign exchange company is a non-bank organisation that services the currency needs of both private and corporate clients. The company's role is to achieve the best currency exchange rates for their clients and to be as cost effective as possible. The company is able to source the currency direct from the foreign exchange (FX) market and therefore pass savings onto their clients.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="8"></a>
<p class="question_heading">How does Premier FX make its money if there are no fees or commissions?</p>
<p class="question_answer">We make a small profit between the price we sell foreign currency to our customers at and the price we pay for the currency on the international market.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>



</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
</html>
