<?
session_start();
include "header.php";
$date_time = date('d-m-Y  h:i:s A');
$customerID = $_SESSION["c_id"];
session_register("currencyTo");

$systemPre = SYSTEM_PRE;
$moneyTransfer=COMPANY_NAME;

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
$fromTotalConfig = "";
if($_GET["create"] != "")
{
	$_SESSION["transType"]		= "";
	$_SESSION["benID"] 			= "";
	$_SESSION["moneyPaid"] 		= "";
	$_SESSION["transactionPurpose"] = "";
	$_SESSION["other_pur"]		 = "";
	$_SESSION["fundSources"] 	= "";
	$_SESSION["refNumber"] 		= "";
	$_SESSION["transAmount"] 	= "";
	$_SESSION["exchangeRate"] 	= "";
	$_SESSION["exchangeID"] 	= "";
	$_SESSION["localAmount"] 	= "";
	$_SESSION["totalAmount"] 	= "";
	$_SESSION["IMFee"] 			= "";
	$_SESSION["currencyTo"]		= "";
	$_SESSION["currencyFrom"]	= "";
	$_SESSION["discount"]       = "";
	$_SESSION["checkManualRate"] = "";
	$_SESSION["chDiscount"] 	= "";
	$_SESSION["discount_request"] = "";
	
	// resetting Session vars for trans_type Bank Transfer
	$_SESSION["bankName"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["ibanRemarks"] = "";
	$_SESSION["accountType"] = "";
	$_SESSION['RequestforBD'] = "";
	
	$_SESSION["senderAgentID"]  = "";
	$_SESSION["collectionPointID"] = "";
	$_SESSION["distribut"] = "";
	
	
	$_SESSION["question"] 			= "";
	$_SESSION["answer"]     = "";	
	$_SESSION["tip"] = "";	
	$_SESSION["branchName"] = "";	
	$_SESSION["chequeNo"] = "";	
	
	$_SESSION["currencyCharge"] = "";
	
	//$_SESSION["admincharges"] = "";
	$_SESSION["bankCharges"] = "";
	$_SESSION["distribut"] ="";
	$_SESSION["transDate"] = "";
	$_SESSION["amount_transactions"] = "";
	$_SESSION["amount_left"] = "";
	$_SESSION["batch"] = "";
	$_SESSION["searchTrans"] = "";
	
	$_SESSION["benAgentParentID"] = "";
	$_SESSION["custAgentParentID"] ="";
	$_SESSION["custName"] = "";
	$_SESSION["customerSearch"] = "";
	$_SESSION["custCountry"] = "";
	$_SESSION["benCountry"] = "";
	$_SESSION["custAgentID"] = "";
	$_SESSION["notService"] = "";
	$_SESSION["dDate"] ="";
	$_SESSION["Declaration"] = "";
	$_SESSION["transID"] = "";
	$_SESSION["transSend"] = "";
	$_SESSION["act"] = "";
	$_SESSION["Submit"] = "";
	$_SESSION["mydDate"] = "";
	$_SESSION["imReferenceNumber"] = "";
	$_SESSION["document_remarks"] = "";
	$_SESSION["internalRemarks"] = "";
	$_SESSION["benIdPassword"] = "";
	$_SESSION["exRateLimitMin"] = "";
	$_SESSION["exRateLimitMax"] = "";
	
	$_SESSION["clientRef"] = "";
	
	$_SESSION["ben_bank_id"] = "";
	$_SESSION["bankingType"] = "";	
	$_SESSION["senderBank"] = "";		
	
	/**
	 * Added to fix the bug related to #3506
	 */
	$_SESSION["distribut"] = "";
	$_SESSION["benAgentID"] = "";

	$_SESSION["transType"]		= "";
	$_SESSION["collectionPointID"] = "";
	$_SESSION["agentID"] 		= "";
	$_SESSION["customerID"] 	= "";
		//$_SESSION["benID"] 			= "";
	$_SESSION["moneyPaid"] 		= "";
	$_SESSION["transactionPurpose"] = "";
	$_SESSION["fundSources"] 	= "";
	$_SESSION["refNumber"] 		= "";
	$_SESSION["transAmount"] 	= "";
	$_SESSION["exchangeRate"] 	= "";
	$_SESSION["exchangeID"] 	= "";
	$_SESSION["localAmount"] 	= "";
	$_SESSION["totalAmount"] 	= "";
	$_SESSION["IMFee"] 			= "";
	$_SESSION["currencyTo"]		= "";
	$_SESSION["currencyFrom"]		= "";
	$_SESSION["bankCharges"]	= "";
		

	$_SESSION["cc_cardType"]= "";
	$_SESSION["cc_cardNo"]= "";
	$_SESSION["ccExpYr"] = "";
	$_SESSION["ccExpMo"] = "";
	$_SESSION["cc_cvvNo"]	= "";
	$_SESSION["cc_firstName"]= "";
	$_SESSION["cc_lastName"]= "";
	$_SESSION["cc_address1"]= "";
	$_SESSION["cc_address2"]	= "";
	$_SESSION["cc_city"]= "";
	$_SESSION["cc_state"]= "";
	$_SESSION["cc_postcode"]= "";
	$_SESSION["cc_country"]	= "";		  		  
		  
	// resetting Session vars for trans_type Bank Transfer
	$_SESSION["bankName"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["branchName"] 	= "";
	$_SESSION["bankCode"] 	= "";
	$_SESSION["bankCity"] 	= "";
	$_SESSION["accountType"] = "";
	
	
	
	$_SESSION["senderAgentID"]  = "";
	$_SESSION["benAgentID"] = "";
	$_SESSION["creditCard"] = "";
	
	$_SESSION["question"] 			= "";
	$_SESSION["answer"]     = "";	

	$_SESSION["distribut"] ="";

	$_SESSION["tip"]     = "";	
	$_SESSION["currencyCharge"] = "";
	
	$_SESSION["bankTransferType"] = "";
}

if (!empty($_REQUEST["calculateBy"]))
	$_SESSION["calculateBy"] = $_REQUEST["calculateBy"];
else
	$_SESSION["calculateBy"] = "exclusive";

function currencyValue()
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;
	}
	elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",") || CONFIG_TRANS_ROUND_CURRENCY == "" )
			$compareCurrency = true;	
	}
	else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}

function RoundVal($val)
{
	if(CONFIG_TRANS_ROUND_LEVEL > 0)
	{
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	}
	else
	{
		$roundLevel = 4;
	}

	$compareCurrency2 = currencyValue();

	if((strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].",")) && $compareCurrency2)
	{
		if(CONFIG_TRANS_ROUND_NUMBER == 1)
		{
			$ArrVal = explode(".",$val);
			$return=$ArrVal[0];
		}
		else
		{
			$return=round($val,$roundLevel);
		}
	}
	else
	{
		$return=round($val,$roundLevel);        
    }
	return $return;
}

//echo ($_SESSION["transType"]."dsdsd".$_GET['benID']);
 if($countryBasedFlag){
	$serviceTypeField = ", serviceType, collectionPointID " ;
 }
if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1")
{
	$IBANField = ", IBAN";
	$IbanOrBankField = ", IbanOrBank";
}
if($_GET['benID'] != "")
{
	if($_SESSION["transType"] == "Bank Transfer")
	{
		$_SESSION["bankName"] 		= "";
		$_SESSION["branchCode"] 	= "";
		$_SESSION["branchAddress"] 	= "";
		$_SESSION["branchName"] 	= "";
		$_SESSION["bankCode"] 	= "";
		$_SESSION["bankCity"] 	= "";
		$_SESSION["branchCity"] 	= "";
		$_SESSION["swiftCode"] 		= "";
		$_SESSION["accNo"] 			= "";
		$_SESSION["ABACPF"] 		= "";
		$_SESSION["IBAN"] 			= "";
		$_SESSION["accountType"] = "";
		//$_SESSION["transType"] = "";		
	}

	$_SESSION["collectionPointID"] = '';
	$queryBen = "select Country ".$serviceTypeField.$IBANField.$IbanOrBankField." from ".TBL_CM_BENEFICIARY." where benID ='$_GET[benID]'";

	$benificiaryContent = selectFrom($queryBen);	
	$_SESSION["IBAN"] = $benificiaryContent["IBAN"];
	$_SESSION["ibanorbank"] = $benificiaryContent["IbanOrBank"];
	$_SESSION["serviceType"]= strtoupper($benificiaryContent["serviceType"]);
	$contentExchagneRate = selectFrom("select *  from exchangerate where country = '".$benificiaryContent["Country"]."'");								
	$_SESSION["currencyTo"] = $contentExchagneRate["currency"];
	 if($countryBasedFlag){
		if($benificiaryContent["serviceType"]!=""){
			if($_SESSION["serviceType"]=="HOME DELIVERY")
				$_SESSION["transType"] = "Pick up";
			elseif($_SESSION["serviceType"]=="BOTH" || $_SESSION["serviceType"]=="BANK DEPOSIT" || $_SESSION["serviceType"]=="BANK TRANSFER" || $_SESSION["serviceType"]=="IBAN")
				$_SESSION["transType"] = "Bank Transfer";
		}
		if($_SESSION["transType"]=="Pick up"){
			$_SESSION["collectionPointID"] = $benificiaryContent["collectionPointId"];
		}
	}
// mine
	if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
		$contentBank = selectFrom("select * from " . TBL_CM_BANK_DETAILS . " where benID ='".$_GET["benID"]."'");
		$_SESSION["bankName"] = $contentBank["bankName"];
		$_SESSION["branchCode"] = $contentBank["branchCode"];
		$_SESSION["bankCode"] = $contentBank["BankCode"];
		$_SESSION["branchCity"] = $contentBank["BranchCity"];
		$_SESSION["branchAddress"] = $contentBank["branchAddress"];
		$_SESSION["branchName"] = $contentBank["BranchName"];
		$_SESSION["swiftCode"] = $contentBank["swiftCode"];
		$_SESSION["accNo"] = $contentBank["accNo"];
//					$_SESSION["ABACPF"] = $contentBank["ABACPF"];
		$_SESSION["accountType"] = $contentBank["accountType"];
	}
}

$_SESSION["benAgentID2"] = $_GET['benID'];

if($_GET['agentID'] != '')
{
	$_SESSION["benAgentID"] = $_GET['agentID'];	
}	

if($_GET['collectionPointID'] != '')
{	
	$_SESSION["collectionPointID"] = $_GET['collectionPointID'];
}

//if($_SESSION["boolean"] == "" || $_SESSION["boolean"] == false)

if($_GET["empty"] == "empty")
	$_SESSION["creditCard"] = $_POST["creditCard"];

//echo $_SESSION["benAgentID"];
/*
foreach ($_SESSION as $k=>$v) 
{
	$str .= "$k : $v<br>";
}
echo $str;*/
//echo "agent type: " . $username . $agentType;
$_SESSION["pincode"] = "";

if(trim($_REQUEST["transType"])!= "" || $_SESSION["transType"]!= "")
{
	if($_GET["benID"] != "")
		$_SESSION["benID2"] = $_GET["benID"];
	else
		$_SESSION["benID2"] = $_SESSION["benID"];
		
	//if(($_SESSION["bankName"] != $_POST["bankName"]) && $_POST["bankName"] != "")
	//{
		if($_SESSION["benID2"] !='')
		{	
			$contentBank = selectFrom("select * from " . TBL_CM_BANK_DETAILS . " where benID ='$_SESSION[benID2]'");
			if(count($contentBank) > 1)
			{
				$_SESSION["bankName"] = $contentBank["bankName"];
				$_SESSION["branchCode"] = $contentBank["branchCode"];
				$_SESSION["bankCode"] = $contentBank["BankCode"];
				$_SESSION["branchCity"] = $contentBank["BranchCity"];
				$_SESSION["branchAddress"] = $contentBank["branchAddress"];
				$_SESSION["branchName"] = $contentBank["BranchName"];
				$_SESSION["swiftCode"] = $contentBank["swiftCode"];
				$_SESSION["accNo"] = $contentBank["accNo"];
				$_SESSION["ABACPF"] = $contentBank["ABACPF"];
				$_SESSION["IBAN"] = $contentBank["IBAN"];
				$_SESSION["accountType"] = $contentBank["accountType"];
	
				// mine
				if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
					$queryBen = "select Country ".$serviceTypeField.$IBANField.$IbanOrBankField." from ".TBL_CM_BENEFICIARY." where benID ='$_SESSION[benID2]'";
					$benificiaryContent = selectFrom($queryBen);
					$_SESSION["IBAN"] = $benificiaryContent["IBAN"];
					$_SESSION["ibanorbank"] = $benificiaryContent["IbanOrBank"];
					$contentBank = selectFrom("select * from " . TBL_CM_BANK_DETAILS . " where benID ='$_SESSION[benID2]'");
					$_SESSION["bankName"] = $contentBank["bankName"];
					$_SESSION["branchCode"] = $contentBank["branchCode"];
					$_SESSION["bankCode"] = $contentBank["BankCode"];
					$_SESSION["branchCity"] = $contentBank["BranchCity"];
					$_SESSION["branchAddress"] = $contentBank["branchAddress"];
					$_SESSION["branchName"] = $contentBank["BranchName"];
					$_SESSION["swiftCode"] = $contentBank["swiftCode"];
					$_SESSION["accNo"] = $contentBank["accNo"];
//					$_SESSION["ABACPF"] = $contentBank["ABACPF"];
					$_SESSION["accountType"] = $contentBank["accountType"];
				}
				$_SESSION["benID2"] = '';
			}
		}
	//}

	if($_SESSION["bankName"] != $_POST["bankName"])
	{
		$flag =1;
	}
	//echo $_SESSION["benID2"]."session 2";	
}

if ($_POST["branchCode"] != "" && $_SESSION["bankName"]=="")
{
	$_SESSION["branchCode"] = $_POST["branchCode"];
	$q = "select * from  cm_collection_point where cp_ria_branch_code  = '$branhCode'";
	$nResult = mysql_query($q)or die(__LINE__."- Invalid query: " . mysql_error());
	$rstRow = mysql_fetch_array($nResult);
	$_SESSION["branchCity"] = $rstRow["cp_city"]; 			
	$_SESSION["branchAddress"] =$rstRow["cp_branch_address"]; 			
	$_SESSION["branchName"] =$rstRow["cp_branch_name"]; 																		
}
		
		
if($_GET["transID"] != "" && trim($_POST["transType"])== "")
{
//	echo "I am here";
//	exit();
	$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
	$_SESSION["transType"] 			= $contentTrans["transType"];
	$_SESSION["benAgentID"] 		= $contentTrans["benAgentID"];
	$_SESSION["senderAgentID"] 		= $contentTrans["custAgentID"];
	$_SESSION["customerID"] 		= $contentTrans["customerID"];
	$_SESSION["benID"] 				= $contentTrans["benID"];
	$_SESSION["moneyPaid"] 			= $contentTrans["moneyPaid"];
	$_SESSION["transactionPurpose"] = $contentTrans["transactionPurpose"];
	$_SESSION["fundSources"] 		= $contentTrans["fundSources"];
	$_SESSION["refNumber"] 			= $contentTrans["refNumber"];
	$_SESSION["transAmount"]		= $contentTrans["transAmount"];
	$_SESSION["serviceType"] 		= $contentTrans["serviceType"];
	
	$_SESSION["question"] 			= $contentTrans["question"];
  $_SESSION["answer"]     = $contentTrans['answer'];	
  $_SESSION["tip"]     = $contentTrans['tip'];	
	
	$_SESSION["bankCharges"]  = $contentTrans["bankCharges"];
	//credit card information

	if(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Bank Transfer" || $_SESSION["moneyPaid"] == "Inclusive of Fee" )) // Have kept the condition $_SESSION["moneyPaid"] != "By Cash" beacuse moneypaid was storing by cash/by bank transfer in case of inclusive/exclusive,since there is a chance that it is still being done for some client thats why have  kept that conditin along with the new one.
	{ 
		$fromTotalConfig = '1';
		$_SESSION["calculateBy"] = "inclusive";
	} 
	elseif(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Cash" || $_SESSION["moneyPaid"] == "Exclusive of Fee"))
	{
		$fromTotalConfig = '0';
		$_SESSION["calculateBy"] = 'exclusive';
	}
	
//	if($_GET["getCalByValue"]== "Y")
//	{
	if( !empty($_REQUEST["calculateBy"]) )
	{
		$_SESSION["calculateBy"] = $_REQUEST["calculateBy"];
		
		if($_SESSION["calculateBy"] == "inclusive")
		{
			$fromTotalConfig = '1';
		}
		elseif($_SESSION["calculateBy"] == "exclusive")
		{
			$fromTotalConfig = '0';
		}
	}

	$contentCreditCard = selectFrom("select * from cm_creditcard where transID='".$contentTrans["transID"]."'");

	$_SESSION["cardType"]= $contentCreditCard["cardType"];
	$_SESSION["cardNo"]= $contentCreditCard["cardNo"];
	$_SESSION["expiryDate"]=$contentCreditCard["expiryDate"];
	$_SESSION["cvvNo"]	= $contentCreditCard["cvvNo"];
	
	// Session vars for trans_type Bank Transfer
	
	
/*	if($contentTrans["transType"] == "Bank Transfer")
	{
		//$contentBank = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID='".$contentTrans["transID"]."'");
		$contentBank = selectFrom("select * from " . TBL_BANK_DETAILS . " where benID ='$_SESSION[benID]'");
		$_SESSION["bankName"] 		= $contentBank["bankName"];
		$_SESSION["branchCode"] 	= $contentBank["branchCode"];
		$_SESSION["branchAddress"] 	= $contentBank["branchAddress"];
		$_SESSION["swiftCode"] 		= $contentBank["swiftCode"];
		$_SESSION["accNo"] 			= $contentBank["accNo"];
		$_SESSION["ABACPF"] 		= $contentBank["ABACPF"];
		$_SESSION["IBAN"] 			= $contentBank["IBAN"];
	

	}*/
	
	
	
}
else
{
	if($_REQUEST["transType"] != "")
	{
		if($_SESSION["transType"] !=  $_REQUEST["transType"])
		{
			$_SESSION["transType"] = (is_array($_POST) && $_GET["msg"] == "" ? $_REQUEST["transType"] : $_SESSION["transType"]);
		}
	}

	if(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Bank Transfer" || $_SESSION["moneyPaid"] == "Inclusive of Fee" )) // Have kept the condition $_SESSION["moneyPaid"] != "By Cash" beacuse moneypaid was storing by cash/by bank transfer in case of inclusive/exclusive,since there is a chance that it is still being done for some client thats why have  kept that conditin along with the new one.
	{ 
		$fromTotalConfig = '1';
		$_SESSION["calculateBy"] = "inclusive";
	} 
	elseif(CONFIG_CALCULATE_BY == '1' && ($_SESSION["moneyPaid"] == "By Cash" || $_SESSION["moneyPaid"] == "Exclusive of Fee"))
	{
		$fromTotalConfig = '0';
		$_SESSION["calculateBy"] = 'exclusive';
	}

	if( !empty($_REQUEST["calculateBy"]) )
	{
		$_SESSION["calculateBy"] = $_REQUEST["calculateBy"];
		
		if($_SESSION["calculateBy"] == "inclusive")
		{
			$fromTotalConfig = '1';
		}
		elseif($_SESSION["calculateBy"] == "exclusive")
		{
			$fromTotalConfig = '0';
		}
	}

	$_SESSION["benAgentID"] = ($_POST["agentID"] != "" ? $_POST["agentID"] : $_SESSION["benAgentID"]);
	$_SESSION["collectionPointID"] = ($_POST["collectionPointID"] != "" ? $_POST["collectionPointID"] : $_SESSION["collectionPointID"]);	
	$_SESSION["customerID"] = ($_POST["customerID"] != "" || $_GET["customerID"] ? ($_GET["customerID"] != "" ? $_GET["customerID"] : $_POST["customerID"]) : $_SESSION["customerID"] );
	//$_SESSION["benID"] = ($_POST["benID"] != "" ? $_POST["benID"] : $_SESSION["benID"]);
	$_SESSION["benID"] = ($_POST["benID"] != "" || $_GET["benID"] ? ($_GET["benID"] != "" ? $_GET["benID"] : $_POST["benID"]) : $_SESSION["benID"] );
	$_SESSION["moneyPaid"] = ($_POST["moneyPaid"] != "" ? $_POST["moneyPaid"] : $_SESSION["moneyPaid"]);
	$_SESSION["transactionPurpose"] = ($_POST["transactionPurpose"] != "" ? $_POST["transactionPurpose"] : $_SESSION["transactionPurpose"]);
	$_SESSION["fundSources"] = ($_POST["fundSources"] != "" ? $_POST["fundSources"] : $_SESSION["fundSources"]);
	$_SESSION["refNumber"] = ($_POST["refNumber"] != "" ? $_POST["refNumber"] : $_SESSION["refNumber"]);
	// Session vars for trans_type Bank Transfer
	$_SESSION["bankName"] 		= ($_POST["bankName"] != "" ? $_POST["bankName"] : $_SESSION["bankName"]);
	$_SESSION["bankCode"] 		= ($_POST["bankCode"] != "" ? $_POST["bankCode"] : $_SESSION["bankCode"]);
	$_SESSION["branchCity"] 	= ($_POST["branchCity"] != "" ? $_POST["branchCity"] : $_SESSION["branchCity"]);
	$_SESSION["branchName"] 	= ($_POST["branchName"] != "" ? $_POST["branchName"] : $_SESSION["branchName"]);
	$_SESSION["branchCode"] 	= ($_POST["branchCode"] != "" ? $_POST["branchCode"] : $_SESSION["branchCode"]);
	$_SESSION["branchAddress"] 	= ($_POST["branchAddress"] != "" ? $_POST["branchAddress"] : $_SESSION["branchAddress"]);
	$_SESSION["swiftCode"] 		= ($_POST["swiftCode"] != "" ? $_POST["swiftCode"] : $_SESSION["swiftCode"]);
	$_SESSION["accNo"] 			= ($_POST["accNo"] != "" ? $_POST["accNo"] : $_SESSION["accNo"]);
	$_SESSION["ABACPF"] 		= ($_POST["ABACPF"] != "" ? $_POST["ABACPF"] : $_SESSION["ABACPF"]);
	$_SESSION["IBAN"] 			= ($_POST["IBAN"] != "" ? $_POST["IBAN"] : $_SESSION["IBAN"]);
	if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
		if(!empty($_REQUEST["ibanorbank"]))
			$_SESSION["ibanorbank"] = $_REQUEST["ibanorbank"];
	}
	$_SESSION["cc_cardType"]= ($_POST["cc_cardType"] != "" ? $_POST["cc_cardType"] : $_SESSION["cc_cardType"]);
	$_SESSION["cc_cardNo"]= ($_POST["cc_cardNo"] != "" ? $_POST["cc_cardNo"] : $_SESSION["cc_cardNo"]);
	$_SESSION["ccExpYr"] = ($_POST["ccExpYr"] != "" ? $_POST["ccExpYr"] : $_SESSION["ccExpYr"]);
	$_SESSION["ccExpMo"] = ($_POST["ccExpMo"] != "" ? $_POST["ccExpMo"] : $_SESSION["ccExpMo"]);
	$_SESSION["cc_cvvNo"]	= ($_POST["cc_cvvNo"] != "" ? $_POST["cc_cvvNo"] : $_SESSION["cc_cvvNo"]);
	$_SESSION["cc_firstName"]= ($_POST["cc_firstName"] != "" ? $_POST["cc_firstName"] : $_SESSION["cc_firstName"]);
	$_SESSION["cc_lastName"]= ($_POST["cc_lastName"] != "" ? $_POST["cc_lastName"] : $_SESSION["cc_lastName"]);
	$_SESSION["cc_address1"]= ($_POST["cc_address1"] != "" ? $_POST["cc_address1"] : $_SESSION["cc_address1"]);
	$_SESSION["cc_address2"]	= ($_POST["cc_address2"] != "" ? $_POST["cc_address2"] : $_SESSION["cc_address2"]);
	$_SESSION["cc_city"]= ($_POST["cc_city"] != "" ? $_POST["cc_city"] : $_SESSION["cc_city"]);
	$_SESSION["cc_state"]= ($_POST["cc_state"] != "" ? $_POST["cc_state"] : $_SESSION["cc_state"]);
	$_SESSION["cc_postcode"]= ($_POST["cc_postcode"] != "" ? $_POST["cc_postcode"] : $_SESSION["cc_postcode"]);
	$_SESSION["cc_country"]	= ($_POST["cc_country"] != "" ? $_POST["cc_country"] : $_SESSION["cc_country"]);
	$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);
	$_SESSION["accountType"] = ($_POST["accountType"] != "" ? $_POST["accountType"] : $_SESSION["accountType"]);
	$_SESSION["currencyTo"] = ($_POST["currencyTo"] != "" ? $_POST["currencyTo"] : $_SESSION["currencyTo"]);
	$_SESSION["currencyFrom"] = ($_POST["currencyFrom"] != "" ? $_POST["currencyFrom"] : $_SESSION["currencyFrom"]);
	if(!$countryBasedFlag){
		$_SESSION["transType"] = ($_REQUEST["transType"] != "" ? $_REQUEST["transType"] : $_SESSION["transType"]);
	}
	if($_POST["question"] != "")
		$_SESSION["question"] = $_POST["question"];
	if($_POST["answer"] != "")
		$_SESSION["answer"] = $_POST["answer"];
	if($_POST["tip"] != "")
		$_SESSION["tip"] = $_POST["tip"];	

	/*if ($_POST["branchCode"] != "")
		{
			$_SESSION["branchCode"] = $_POST["branchCode"];
			$branhCode = $_POST["branchCode"];
			$q = "select * from  cm_collection_point where cp_ria_branch_code  = '$branhCode'";
			$nResult = mysql_query($q)or die(__LINE__."- Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
			$_SESSION["branchCity"] = $rstRow["cp_city"]; 			
			$_SESSION["branchAddress"] =$rstRow["cp_branch_address"]; 			
			$_SESSION["branchName"] =$rstRow["cp_branch_name"]; 																		
		}*/
		
		
	if($_SESSION["bankNametemp"] != "")
	{
		$_SESSION["bankName"] = $_SESSION["bankNametemp"];
		$_SESSION["bankNametemp"]="";
	}
	
			
	if ($_POST["Country"] != "")
		$_SESSION["Country"] = $_POST["Country"];
	if ($_POST["City"] != "")
		$_SESSION["City"] = $_POST["City"];
	
	$_SESSION["bankCharges"] = ($_POST["bankCharges"] != "" ? $_POST["bankCharges"] : $_SESSION["bankCharges"]);
	
	if ($_SESSION["transType"] != "Bank Transfer")
			$_SESSION["bankCharges"] = '';
	
	if(!empty($_REQUEST["bankTransferType"]))		
		$_SESSION["bankTransferType"] = $_REQUEST["bankTransferType"];
}

$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);

if($_REQUEST["transAmount"] != "")
{
	$_SESSION["transAmount"] = ($_REQUEST["transAmount"] != "" ? $_REQUEST["transAmount"] : $_SESSION["transAmount"]);
	$transAmount = (float) $_SESSION["transAmount"];
}

if($_REQUEST["localAmount"] != "")
{
	$_SESSION["localAmount"] = ($_REQUEST["localAmount"] != "" ? $_REQUEST["localAmount"] : $_SESSION["localAmount"]);
	$localAmount = (float) $_SESSION["localAmount"];
	
	if(CONFIG_ROUND_NUMBER_ENABLED == '1')
	{
		$localAmount = round($localAmount);
	}
	
}

if(CONFIG_OTHER_TRANSACTION_PURPOSE == "1")
{
	if($_SESSION["transactionPurpose"] == "") {
		$_SESSION["transactionPurpose"] = 'Other';
	}
}

if ( defined("CONFIG_SHOW_DEFAULT_FUND_SOURCE") && CONFIG_SHOW_DEFAULT_FUND_SOURCE == 1 && empty($_SESSION["fundSources"]) )
{
	$_SESSION["fundSources"] = CONFIG_DEFAULT_FUND_SOURCE_VALUE;
}

/*if ($_SESSION["transType"] != ""){

	$queryCust ="select c_id, c_name,c_country  from ".TBL_CM_CUSTOMER . " where c_id = $customerID order by c_name";
	$customers = selectMultiRecords($queryCust);
	
	//echo $queryCust . "<br>";
	if($_SESSION["customerID"] != "")
	{
		$queryBen  ="select * from ".TBL_CM_BENEFICIARY." where customerID ='$_SESSION[customerID]' order by firstName";
		$beneficiaries = selectMultiRecords($queryBen);
		//echo $queryBen . "<br>";
	}
}*/


if($_GET["aID"] != "")
{
	$_SESSION["senderAgentID"] = $_GET["aID"];
}

if($_GET["benAgentID"] != "")
{
	$_SESSION["benAgentID"] = $_GET["benAgentID"];
	$_SESSION["transType"] = "Pick up";
}

?>
<html>
<head>
<title>Create Transaction</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript" src="./javascript/jquery.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the agent's email address.");
        theForm.email.focus();
        return false;
    } else {
	}
	if(theForm.agentCompDirector.value == "" || IsAllSpaces(theForm.agentCompDirector.value)){
    	alert("Please provide the agent company director's name.");
        theForm.agentCompDirector.focus();
        return false;
    }
	if(theForm.agentBank.value == "" || IsAllSpaces(theForm.agentBank.value)){
    	alert("Please provide the agent's Bank name.");
        theForm.agentBank.focus();
        return false;
    }
	if(theForm.agentAccountName.value == "" || IsAllSpaces(theForm.agentAccountName.value)){
    	alert("Please provide the agent's bank account name.");
        theForm.agentAccountName.focus();
        return false;
    }
	if(theForm.agentAccounNumber.value == "" || IsAllSpaces(theForm.agentAccounNumber.value)){
    	alert("Please provide the agent's bank account number.");
        theForm.agentAccounNumber.focus();
        return false;
    }
	if(theForm.agentCurrency.options.selectedIndex == 0){
    	alert("Please select agent's currency.");
        theForm.agentCurrency.focus();
        return false;
    }
	if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
    	alert("Please provide the agent's account limit.");
        theForm.agentAccountLimit.focus();
        return false;
    }
	if(theForm.agentCommission.value == "" || IsAllSpaces(theForm.agentCommission.value)){
    	alert("Please provide the agent's commision percentage.");
        theForm.agentCommission.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }


	function getHTTPObject() { 
	var xmlhttp; 
	/*@cc_on
 
	 /*@if(@_jscript_version >= 5)
	  
			try {
			  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
			  try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  } catch (E) {
				xmlhttp = false;
			  }
			}
		@else @*/
	  	xmlhttp = false;
	  /*@end
	@*/
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') { 
		try { 
			xmlhttp = new XMLHttpRequest(); 
			} catch (e) { 
				xmlhttp = false; 
				} 
		} 
		return xmlhttp; 
	}
 
var http = getHTTPObject(); // We create the HTTP Object 
var url1 = "getAmounts.php"; 

function handleHttpResponse() {

  if (http.readyState == 4) {



    results = http.responseText.split(",");

    
    document.getElementById('transAmountID').value = results[0];
    document.getElementById('localAmountID').value = results[1];
    document.getElementById('totalAmountID').value = results[2];
 <? if($_SESSION["manualCommission"] == "Y") { ?>
	    document.getElementById('IMFee').value = document.getElementById("IMFee").value;
 <? } else{ ?>
    document.getElementById('IMFee').value = results[3];
 <? }?>
    document.getElementById('exchangeRate').value = results[4];
	var ccID = document.getElementById('currencyChargeID');
		if(ccID != null)
    	ccID.value = results[5];
    document.getElementById('exchangeID').value = results[6];
   

	}

}

function enableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=false;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=false;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=false;	
		<?
	}
		?>
}
function updateAmounts(param) {
 var amountFlag = param;	
 var amount = document.getElementById("transAmountID").value;
 var amountLocal = document.getElementById("localAmountID").value;
 var amountTotal = document.getElementById("totalAmountID").value;
 var benCountry =  document.getElementById("benCountryID").value;
 var custCountry =  document.getElementById("custCountryID").value;
 var dist =  document.getElementById("distribut").value;
 var exRate =  document.getElementById("exchangeRate").value;
 var manualCommissionAJAX ;
 <? if($manualCommAjaxFlag && $_SESSION["manualCommission"]=="Y") {?>
	 manualCommissionAJAX =  document.getElementById("manualCommissionAJAX").value;
 <? } ?>
	 var fee =  document.getElementById("IMFee").value;
	 
 var transType = document.getElementById("transType").value;
 var currencyFrom = document.getElementById("currencyFrom").value;
 var currencyTo = document.getElementById("currencyTo").value;
 var inclusive = 'No';
 var agentOwnRate = 'No';
 <?
if((CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.",")) || $_GET["enterOwnRate"] == "Y")
{?>
   var mRate = document.getElementById("checkManualRate");
	if(mRate){
		if(mRate.value == 'Y')
		{
					agentOwnRate = 'Y';
		}else{
					agentOwnRate = 'N';
			}
	}	
<?
}
?>
<?
if(CONFIG_CALCULATE_BY == '1')
{?>
	
	if(document.getElementById("calculateByID").value == 'inclusive')
	{
		var moneyPaid = "Bank Transfer";
		inclusive = 'Yes';
	}else{
		var moneyPaid = "By Cash";
		inclusive = 'No';
		}
<?
}else{
	?>
	 var moneyPaid = document.getElementById("moneyPaid").value;
	 inclusive = 'No';
	<?
	}
?> 

/*var customerAgentID = document.getElementById("customerAgentID").value;
 var bankChargesID = document.getElementById("bankChargesID").value;*/
 

 
 var amountValue = '?amount='+amount+'&benCountry='+benCountry+'&custCountry='+custCountry+'&dist='+dist+'&exRate='+exRate+'&fee='+fee+'&transType='+transType+'&currencyFrom='+currencyFrom+'&currencyTo='+currencyTo+'&amountFlag='+amountFlag+'&amountLocal='+amountLocal+'&moneyPaid='+moneyPaid+'&inclusive='+inclusive+'&agentOwnRate='+agentOwnRate+'&manualCommissionAJAX='+manualCommissionAJAX+'&amountTotal='+amountTotal;
  http.open("GET", url1 + amountValue, true);
  


 enableClicks();
  http.onreadystatechange = handleHttpResponse;

	
  http.send(null);

}

function disableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=true;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=true;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=true;	
		<?
	}
	?>
}

$(document).ready(function(){
	$("#transType").click(function(){
		if($(this).val() == "Bank Transfer")
			$("#bankTransferTypeRow").show();
		else
			$("#bankTransferTypeRow").hide();
	});
	
	$("input[@name='bankTransferType']").click(function(){
		//alert($(this).val());
		if($(this).val() == "I")
		{
			$("#trIBANChars").each(
				function(i){
					$(this).show();
				}
			);
			
			for(var i=1; i<7; i++)
				$("#normalBankRow"+i).hide();
		}
		else
		{
			$("#trIBANChars").each(
				function(i){
					$(this).hide();
				}
			);

			for(var i=1; i<7; i++)
				$("#normalBankRow"+i).show();
		}
	});
	
	<? if($_SESSION["bankTransferType"] == "B") { ?>
		$("#bankTransferType2").click();
	<? }else{ ?>
		$("#bankTransferType1").click();
	<? } ?>
	
});

// end of javascript -->
</script>
<style type="text/css">
<!--
.style1 {color: #CCCCCC}
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #CCCCCC; font-weight: bold; }
.style4 {
	color: #660000;
	font-weight: bold;
}
.style5 {color: #663333}
.style6 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">

  
    <tr>
      <td align="center" valign="top"><font color="#FF0000">* Compulsory Fields</font><br>
        <br>
        <table width="700" border="0" cellpadding="0" cellspacing="0">
  <tr height="30">
    <td bgcolor="#6699cc" align="left"><b><font color="#FFFFFF" size="2">&nbsp;<? echo ($_GET["transID"] != "" ? "Update" : "Create")?> Transaction</font></b></td>
  </tr>          
		  <? if ($_GET["msg"] == "Y" || $_GET["msg1"] == "Y" ){ ?>
		  <tr>
            <td>
			
			<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFCC">
              <tr>
                <td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
                <td width="635"><? echo "<font color='#990000'><b>".$_SESSION['error']."</b></font>"; ?></td>
              </tr>
            </table></td>
			<td>&nbsp;</td>
          </tr>
		  <?
		  }
		  ?>
          <tr>
            <td width="700" valign="top">
			<table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#FF0000">
              <form action="confirm-transaction.php" method="post" name="addTrans">
                <input type="hidden" name="agentID" value="<? echo $_SESSION["benAgentID"];?>">
				<input name="collectionPointID" type="hidden" id="collectionPointID" value="<? echo $_SESSION["collectionPointID"]?>">
				<? 
				
		 
	//if ($_SESSION["transType"] != "")
	//{ ?>

				<tr><td width="700" align="left" valign="top"><fieldset>
				  <legend class="style2">Beneficiary Details </legend>
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                     <tr>
					 <td colspan="4"><br>Please provide name of your beneficiary and click search<br><br></td>
					 </tr>
					  <tr>
                        <td width="150" align="right"><font color="#005b90"> Name </font></td>
                        <td colspan="2" align="left"><input name="benName" type="text" id="benName" size="15">
                            <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.benName.value != '') { window.open('search-ben.php?benName=' + document.addTrans.benName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please write something in the text box to search.')}">
                            <input name="benID" type="hidden" id="benID" value="<? echo $_SESSION["benID"]?>"></td>
                        <td width="200" align="center" bgcolor="#6699cc" class="style1"><a href="javascript:;" class="style1" onClick=" window.open('add-beneficiary.php?from=popUp&agnetID=<?=$_SESSION["agentID"]."&customerID=".$_SESSION["customerID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=500,width=740')"><b>Add New Beneficiary</b></a></td>
                      </tr>
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Mobile, Email".$IBANField.$IbanOrBankField."  from ".TBL_CM_BENEFICIARY." where benID ='" . $_SESSION[benID] . "'";
			//echo $queryBen;
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90"> Name</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip </font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Country"];  $benCountryNameTemp = $benificiaryContent["Country"];?>
                            <input type="hidden" name="benCountry" value="<? echo $benificiaryContent["Country"]?>">
							<input type="hidden" name="customerCountry" value="<? echo $logedUseCountry;?>">
							</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" align="left">
							<?
								if(CONFIG_PHONES_AS_DROPDOWN == 1)
								{
									if(empty($benificiaryContent["Phone"]))
									{
										echo $benificiaryContent["Mobile"]." (Mobile)";
									} else {
										echo $benificiaryContent["Phone"]." (Home)";
									}
								} else {
									echo $benificiaryContent["Phone"];
								}
							?>
						</td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			//}
  ?>
                    </table>
                                   
					<? if($benificiaryContent["benID"] != "" && !$countryBasedFlag)
					{
						$countryContent = selectFrom("select serviceAvailable, bankCharges, outCurrCharges  from " . TBL_SERVICE. ", ".TBL_COUNTRY." where 1 and (toCountryId = countryId and  countryName='".$benificiaryContent["Country"]."')");
					?>
					<table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr align="left">
                        <td colspan="2"><span class="style6">Services available for <strong><? echo $benificiaryContent["Country"]?></strong>: <? echo ($countryContent["serviceAvailable"] != "" ? $countryContent["serviceAvailable"] : "None")?></span></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Transaction Type</font></td>
                        <td width="500">
						<select id="transType" name="transType" style="font-family:verdana; font-size: 11px" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>'; document.addTrans.submit();">
                            <option value="">- Select Transaction Type -</option>
                            <?
							if (strstr($countryContent["serviceAvailable"], "Cash Collection"))
							{
								if ($_SESSION["transType"] == "Pick up")
									echo "<option selected value='Pick up'>Pick up</option>";
								else
									echo "<option value='Pick up'>Pick up</option>";
							}
							if (strstr($countryContent["serviceAvailable"], "Bank Deposit"))
							{
								if ($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == '')
								{
									$_SESSION["bankCharges"] = $countryContent["bankCharges"];
									echo "<option value='Bank Transfer' selected >Bank Transfer</option>";
								}
								else{
									echo "<option value='Bank Transfer'>Bank Transfer</option>";
								    }
							}
							if (strstr($countryContent["serviceAvailable"], "Home Delivery"))
							{
								if ($_SESSION["transType"] == "Home Delivery")
									echo "<option value='Home Delivery' selected>Home Delivery</option>";
								else
									echo "<option value='Home Delivery'>Home Delivery</option>";
							}

							?><?  echo $_SESSION["transType"] ;?>
                        </select>
                        
                        
                            <script language="JavaScript">//SelectOption(document.addTrans.transType, "<?=$_SESSION["transType"]; ?>");</script>
                        <input type="hidden" name="custAgentID" value="<? echo ($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI" ? $username : $senderAgentContent["userID"])?>">
						
						</td>
                      </tr>
					
					
					<? 
						if(CONFIG_IBAN_OR_BANK_TRANSFER!="1" && ($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == '')) 
						{ 
							//debug($_SESSION["bankTransferType"]);
							
							$strIbanSelect = "";
							$strNoBaSelect = "";
							if($_SESSION["bankTransferType"] == "B")
								$strNoBaSelect = 'checked="checked"';
							else
								$strIbanSelect = 'checked="checked"';
								
								
							//debug($strNoBaSelect ." ^ ".$strIbanSelect);	
					?>  
						<tr id="bankTransferTypeRow">
							<td>&nbsp;</td>
							<td align="left">
								<input type="radio" id="bankTransferType1" name="bankTransferType" value="I" <?=$strIbanSelect?> />&nbsp;IBAN<br />
								<input type="radio" id="bankTransferType2" name="bankTransferType" value="B" <?=$strNoBaSelect?> />&nbsp;Normal Bank Transfer<br />
							</td>
						</tr>
					<? } ?>
					  
                    </table>
					 <? 
					 }
	 				elseif($benificiaryContent["benID"] != "" && $countryBasedFlag){

						$countryContent = selectFrom("select serviceAvailable, bankCharges, outCurrCharges  from " . TBL_SERVICE. ", ".TBL_COUNTRY." where 1 and (toCountryId = countryId and  countryName='".$benificiaryContent["Country"]."')");
					?>
					<table width="650" border="0" cellpadding="2" cellspacing="0">
                      <TR>
                        <TD WIDTH="150" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90">Service Type</TD>
                        <TD WIDTH="500">
							<input type="text"  NAME="serviceType" ID="serviceType" value="<?=$_SESSION["serviceType"]?>">
							<input type="hidden"  NAME="transType" ID="transType" value="<?=$_SESSION["transType"]?>" readonly>
                            <script language="JavaScript">//SelectOption(document.addTrans.transType, "<?=$_SESSION["transType"]; ?>");</script>
	                        <input type="hidden" name="custAgentID" value="<? echo ($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI" ? $username : $senderAgentContent["userID"])?>">
						</td>
                      </tr>
                    </table>
				 <? 
					}
					if ($_SESSION["transType"] != "" ||$_SESSION["transType"] == "")
					{ ?>
                    <table width="650" border="0" cellpadding="2" cellspacing="0">
<?
if ($_SESSION["transType"] == "Pick up" && !$countryBasedFlag)
{
?>						
                      <tr>
                        <td colspan="3"><br>
                          Select correspondent - Please enter city name to display 
                          <? echo $company;?> Transfer's correspondents.<br>
						</td>
                      </tr>
				  
                      <tr>
                        <td width="150" align="right"><font color="#005b90">
						<?
						if ($_SESSION["transType"] == "Pick up")
						{
							echo "Search  Collection Point";
						}
						else
						{
							echo "Search correspondent";
						}
						?>
						 </font></td>
                        <td width="300" align="left"><input name="agentCity" type="text" id="agentCity" size="15">
                          <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.agentCity.value != '') { window.open('search-agent-city.php?agentCity=' + document.addTrans.agentCity.value + '&transID=<? echo $_GET["transID"]?>' +  '&benCountry=<? echo $benificiaryContent["Country"]?>', 'agentCity', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please write city name in the text box to search.')}">
                          <input name="agentID" type="hidden" id="benID" value="<? echo $_SESSION["benAgentID"]?>"></td>
                        <td width="200">&nbsp;</td>
                      </tr>

                    </table>
                                       <table width="650" border="0" cellpadding="2" cellspacing="0"> 
                      <? 
		
			$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$dist = $senderAgentContent["cp_ida_id"];
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                        <tr>
                        <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </td>
                        <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="3"><? echo $senderAgentContent["cp_branch_address"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Company</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_country"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">City</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_city"]?></td>
                      </tr> 
                      <?
			  }
			  ?>
<?
}
elseif($_SESSION["transType"] == "Pick up" && $countryBasedFlag)
{
			$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$dist = $senderAgentContent["cp_ida_id"];
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                        <tr>
                        <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </td>
                        <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="3"><? echo $senderAgentContent["cp_branch_address"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Company</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_country"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">City</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_city"]?></td>
                      </tr> 
                      <?
			  }
			  ?>
<?
}
?>  
                      </table>
                    <?
					  }
					  if($_SESSION["transType"] == "Home Delivery")
					  {?>
                    <table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td width="650" align="left"><span class="style4">Note:</span><br>
                          <span class="style5">Please make sure that Beneficiary's 
                          Home Address is correct. <? echo $company;?> Transfer will not 
                          be responsible for faulty address provided by Customer 
                          or Agent.</span></td>
                      </tr>
                    </table>
                    <?
					}
					
			  if($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == "")
			  {?>
                    <table width="100%" border="0" cellpadding="2" cellspacing="1" bordercolor="#006600">
						
						<!-- IBAN Check Starts -->
						<?
							$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
							
						/* Display the bank details fields only when beneficiary fields are selected */
						
						
						if($benificiaryContent["benID"] != "")
						{
						
							//if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
							//{
								
								$_SESSION['ibanorbank'] = $benificiaryContent['IbanOrBank']; // To ensure the real value for IbanOrBank
								$showBankDetails = true;
								$readOnlyFields = "";
								if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
									$readOnlyFields = "readonly";
									if($benificiaryContent["benID"]!="" && $_SESSION['ibanorbank']=="bank"){
										$showBankDetails = false;
									}
								
								}
								
						?>
						<? if($showBankDetails){ ?>								
								<tr valign="top" id="trIBANChars">
									<td width="135" height="20" align="right" valign="top"><font color="#005b90">IBAN<font color="#ff0000">*</font> </font></td>
									<td width="209" height="20">
										<input type="text" name="IBAN" id="IBAN" value="<?=$_SESSION["IBAN"]?>" maxlength="30" size="40" <?=$readOnlyFields?>>
									</td>
									<td width="102" height="20" align="right"><font color="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></font></td>
									<td width="188" height="20">
										<? echo ($benificiaryContent["Country"] == "GEORGIA" ? "<input type='text' name='ibanRemarks' value='".$_SESSION["ibanRemarks"]."'>" : "&nbsp;") ?>
									</td>
								</tr>
								<tr id="trIBANChars" <? if ($_SESSION["IBAN"] == '') { ?>style="display:none;"<? } ?>>
									<td width="135" height="20" align="center" colspan="3">&nbsp;</td>
									<td width="188" height="20"><input type="hidden" name="benCountryRegion" value="European">&nbsp;</td>
								</tr>
						<? }
							else{ ?>
						<?
							//} else {
						?>
						  <tr align="left" bgcolor="#ffffff" id="normalBankRow1"> 
							<td height="20" colspan="4"><font color="#005b90"><b>Beneficiary Bank Details</b></font></td>
						  </tr>
						  <tr bgcolor="#ffffff" id="normalBankRow2"> 
							<td width="150" height="20" align="right"><font color="#005b90">Bank Name* </font></td>
							<td width="200" height="20" align="left"> 
								<? 
									$benCountryName = $_SESSION['Country'];
									$q = "select * from imbanks where country  = '$benCountryNameTemp'";
									$nResult = mysql_query($q)or die(__LINE__."- Invalid query: " . mysql_error());
									$rstRow = mysql_fetch_array($nResult);						
									
									if (count($rstRow) > 1 && CONFIG_IBAN_OR_BANK_TRANSFER!="1")
									{
										$q = "select distinct bankName from imbanks where country  = '$benCountryNameTemp'";
										$imBakns = selectMultiRecords($q);
								?>
										<select name='bankName' style='font-family:verdana; font-size: 11px; width:150'   onChange="document.forms[0].action = 'add-transaction.php?from=<? echo $_GET["from"]?>&benIDs=<? echo $_GET["benIDs"]?>'; document.forms[0].submit();"> 
										<?
											for($i=0;$i < count($imBakns); $i++)
											{
												/*if($_SESSION["bankName"] == $imBakns[$i]["bankName"])
													echo "<option value='".$imBakns[$i]["bankName"]."' selected>".$imBakns[$i]["bankName"]."</option>\n";
												else*/
													echo "<option value='".$imBakns[$i]["bankName"]."'>".$imBakns[$i]["bankName"]."</option>\n";
											}
											
											//echo "<option value='".$_SESSION["bankName"]."' selected>".$_SESSION["bankName"]."</option>";
										?>
													
										</select>
										<script language="JavaScript">SelectOption(document.addTrans.bankName, "<?=$_SESSION["bankName"]; ?>");</script> 
							  <?
									}
									else
									{
							  ?>
										<input name="bankName" type="text" id="bankName" value="<?=$_SESSION["bankName"]; ?>" maxlength="25"  <?=$readOnlyFields?>> 
							  <?
									}
							  ?>
							</td>
							<td  align="right"><font color="#005b90">Acc Number* </font></td>
							<td width="200" align="left"><input name="accNo" type="text" id="accNo" value="<?=$_SESSION["accNo"]; ?>" maxlength="50" <?=$readOnlyFields?>></td>
						  </tr>
						  <tr bgcolor="#ffffff" id="normalBankRow3"> 
							<td align="right"><font color="#005b90">Bank Code* </font></td>
							<td align="left"> <input name="bankCode" type="text" id="bankCode" value="<?=$_SESSION["bankCode"]; ?>" size="30" <?=$readOnlyFields?>> 
							</td>
							<td width="150" height="20" align="right"><font color="#005b90">Branch 
							  Code</font></td>
							<td width="200" height="20" align="left"> 
	
							  <input name="branchCode" type="text" id="branchCode" value="<? echo $_SESSION["branchCode"]; ?>" maxlength="25" <?=$readOnlyFields?>> 
	
							</td>
						  </tr>
						  <tr bgcolor="#ffffff" id="normalBankRow4"> 
							<td width="150" height="20" align="right"><font color="#005b90">Branch 
							  Name* </font></td>
							<td width="200" height="20" align="left"><input name="branchName" type="text" id="branchName" value="<? echo $_SESSION["branchName"]; ?>" maxlength="25" <?=$readOnlyFields?>></td>
							<td width="100" align="right"><font color="#005b90">&nbsp;  </font></td>
							<td width="200" align="left">&nbsp;  </td>
						  </tr>
						  <tr bgcolor="#ffffff" id="normalBankRow5"> 
							<td width="150" height="20" align="right"><font color="#005b90">Branch 
							  Address</font> </td>
							<td width="200" height="20" align="left"><input name="branchAddress" type="text" id="branchAddress" value="<?=$_SESSION["branchAddress"]; ?>" size="30"maxlength="254" <?=$readOnlyFields?>></td>
							<td  height="20" align="right"><font color="#005b90">Branch 
							  City*</font></td>
							<td width="200" height="20" align="left"><input name="branchCity" type="text" id="branchCity" value="<?=$_SESSION["branchCity"]; ?>"  <?=$readOnlyFields?>></td>
						  </tr>
						   <tr id="normalBankRow6">
							 <?
						  if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
						  {
						  ?>	
							<td width="150" height="20" align="right"><font color="#005b90">Account Type</font></td>
							<td width="200" height="20"><br>
								<input name="accountType" type="text" id="accountType" <?=$readOnlyFields?> value="<?=$_SESSION["accountType"]?>">
							<!--	<input name="accountType" type="radio" id="accountType" value="Savings" <? //echo ($_SESSION["accountType"] == "Savings" ? "checked"  : "")?>> Savings -->
								</td>
							<?
							}else{
							?>
							<td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
							<td width="200" height="20" align="left">&nbsp;</td>
						  <?
						  }
						  ?>
							<?
													if(CONFIG_CPF_ENABLED == '1')
													{?>
														<td width="100" align="right"><font color="#005b90">
														<?
															/*{   It is for America etc
																echo "ABA Number*";
															}
															else*/if(strstr(CONFIG_CPF_COUNTRY , $benificiaryContent["Country"]))
															{
																echo  "CPF Number*";
															}
															else
															{
																echo "&nbsp;";
															}
															?>
													</font></td>
													<td width="200"><?
															if(strstr(CONFIG_CPF_COUNTRY , $benificiaryContent["Country"]))
															{
															?>
												  <input name="ABACPF" type="text" id="ABACPF" value="<?=$_SESSION["ABACPF"]; ?>" maxlength="100">
												  <?
															}
															else
															{
																echo "&nbsp;";
															}
															?></td>
													<?
													}else{
													?>
														<td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
												<td width="200" height="20">&nbsp;</td>
													<?
													}
													?>
						  </tr>
						<?
							//}
						?>
						<? }// CONFIG_IBAN_OR_BANK_TRANSFER also used in this check?>
						<!-- IBAN Check Ends -->	  
						  
					<tr>
					  <!--td colspan="2">Select Distributor</td-->
					  <td align="right"><font color="#005b90">Select Distributor</font></td>
					  <td align="left">
					  	<select name="distribut" id="distribut" style="font-family:verdana; font-size: 11px; width:226" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&benID=<? echo $_GET["benID"]?>'; document.addTrans.submit();">
	               <option value="">- Select One -</option>
	               <?
	              /* $defaulted = selectFrom("select userID, username, name  from admin where userID = '100111'");				

	             if($defaulted["userID"] == $_SESSION["distribut"])
	               {
	               ?>
	               <option value="<? echo $defaulted["userID"] ?>" selected >
	               	<? echo $defaulted["username"]." [".$defaulted["name"]."]"; 
	               	?>
	                </option> 
					  	<?

					  		}
					  		else{?>
					  			<option value="<? echo $defaulted["userID"] ?>">
	               	<? echo $defaulted["username"]." [".$defaulted["name"]."]"; 
	               	?>
	                </option> 
					  			<? }*/
					  	$agenCountry = $benificiaryContent["Country"];
					  	$distributors = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
					  	$ida = selectMultiRecords($distributors);
							for ($j=0; $j < count($ida); $j++)
							{
								$authority = $ida[$j]["authorizedFor"];
								$toCountry = $ida[$j]["IDAcountry"];
								if(strstr($authority,"Bank Transfer") && strstr($toCountry,$agenCountry))
								{

									if($ida[$j]["userID"] == $_SESSION["distribut"] && !empty($_SESSION["distribut"]))
									{
										$dist =  $ida[$j]["userID"];
										?>
											<option value="<? echo $_SESSION["distribut"]; ?>"selected>
											<? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
											</option>
										<?
									}
									elseif($ida[$j]["defaultDistrib"] == "Y")
									{
										$dist =  $ida[$j]["userID"];
										?>
											<option value="<?=$ida[$j]["userID"]?>" selected="selected">
											<? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
											</option>
										<?
									}
									else
									{
										?>
											<option value="<? echo $ida[$j]["userID"]; ?>">
											<? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
											</option>
										<?
									}
									
								}
							}
					  	?>
					  </select>
					  	</td>
					  </tr>				
                    </table>
                    
  			<?
					}
				}
  			
			?>
                   
                  
                
                <?
		  if($benificiaryContent["benID"] != "")
		  {
		  ?>
			<tr>
			  <td valign="top"><fieldset>
				<legend class="style2">Transaction Details </legend>
				<? 				
				//$customerContent = selectFrom("select *  from ".TBL_CM_CUSTOMER. " where c_id = '".$_SESSION["customerID"]."'");
				$custContentQuery = "select *  from ".TBL_CM_CUSTOMER. " where c_id = '".$_SESSION["loggedCustomerData"]["c_id"]."'";
				$customerContent = selectFrom( $custContentQuery );
				
				// #4331- ashahid (added for origin currency whenever transaction page is loaded)
 				$strCurrOrigin = "SELECT distinct(currencyOrigin) FROM ".TBL_EXCHANGE_RATES." where countryOrigin = '".$customerContent["c_country"]."' and country = '".$benificiaryContent["Country"]."' ORDER BY currencyOrigin ";

				$resCurrOrigin = selectMultiRecords($strCurrOrigin);
				$currFrom = $_SESSION["currencyFrom"];
				if($currFrom==""){
						$currFrom = $resCurrOrigin[0]["currencyOrigin"];
				}

				//$customerContent["c_country"] .  $benificiaryContent["Country"];

				if($_POST['currencyTo'] != "" )
				{
				
				 	 $currencyTo = $_POST['currencyTo'];	
					 $_SESSION["currencyTo"] = $_POST['currencyTo'];
							
				}

				
				$benCountryName = $benificiaryContent["Country"];
	
		
		
			if($fromTotalConfig != '1')
			{
				if(CONFIG_EXCH_RATE_NOT_EDIT == '1' && $_GET["transID"] != "")
				{
					$exRateUsed = selectFrom("select rateUsedID, rateWithMargin, currencyFrom , currencyTo from ".TBL_EXCH_RATE_USED." where transID  = '".$_GET["transID"]."'");
					$exRate = $exRateUsed["rateWithMargin"];
					$currencyFrom = $exRateUsed["currencyFrom"];
					$currencyTo 	= $exRateUsed["currencyTo"];

				}
				elseif ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")  || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_SESSION["checkManualRate"] == 'Y') || $_GET["enterOwnRate"] =="Y")
			 	{
			 		$exID = 0;
			 	
					if ($_SESSION["exchangeRate"] != "" && $_SESSION["exchangeRate"] != 0)
					{
						$exRate	= $_SESSION["exchangeRate"];
						$_SESSION["exchangeRate"] = "";
						
					}
					elseif ($_POST["exchangeRate"] != "" && $_POST["exchangeRate"] != 0)
					{
						$exRate	= $_POST["exchangeRate"];
					
					}
					else
					{
						//$exRate = 0;
						if(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr")
						{
							$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
							$exID 			  = $exchangeData[0];
							$exRate 		  = $exchangeData[1];	
							$currencyFrom = $exchangeData[2];
							$currencyTo 	= $exchangeData[3];	
							$_SESSION["currencyFrom"] = $currencyFrom;
							
						}
						else
						{
							//$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"],$_SESSION["senderAgentID"]);
							$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0, $_SESSION["currencyFrom"]);
							$exID = $exchangeData[0];
							/**
							 * Get the exchange rate DB value only in case if the session value is empty
							 * @Ticket# 3524
							 */
							if(empty($_SESSION["exchangeRate"]))
							{
								$exRate = $exchangeData[1];	
								
							}
							/* End of # 3524 */

							$currencyFrom = $exchangeData[2];
							$currencyTo 	= $exchangeData[3];

							$_SESSION["currencyFrom"] = $currencyFrom;
						}
					}
			 	}
				elseif(CONFIG_MULTI_RATE_PATTERN == '1')
			 	{
			 		$exID = 0;
			 		$exRate = getMultiPatternRate($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0, $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"], 'Yes');
			 		$currencyFrom = $_SESSION["currencyFrom"];
					$currencyTo 	= $_SESSION["currencyTo"];
					
			 	}
				elseif(CONFIG_24HR_BANKING == "1" && $_SESSION["bankingType"] == "bank24hr")
				{
		 			$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $_SESSION["bankingType"], 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
					$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];	
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];	
					
			 	}
				else
				{
					
			 		//$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"] );

					$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, $transAmount, $currFrom);
					$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];
				
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];

					$_SESSION["currencyFrom"] = $currencyFrom;				
					
				}

				if($_REQUEST["local"] == "Local Amount")
				{
					if($_SESSION["transAmount"] != "")
					{
						$localAmount = $transAmount * $exRate;
						
	
						if(CONFIG_ROUND_NUMBER_ENABLED == '1' && strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].","))
						{
							$localAmount = round($localAmount);
							
						}
						$_SESSION["localAmount"] = $localAmount;
					}
					if ($_POST["t_amount"] != "")
					{
						$_SESSION["amount_transactions"] = $_POST["t_amount"];
					}
					if ($_POST["l_amount"] != "")
					{
						$_SESSION["amount_left"] = $_POST["l_amount"];
					}
				}
	
				$compareCurrency2 = currencyValue();///it is used for #1772 calculations
	
				if($_POST["amount"] == "Amount" || (CONFIG_TRANS_ROUND_NUMBER == '1' && $_POST["moneyPaid"] == 'By Cash' && $compareCurrency2))
				{
					if($_SESSION["localAmount"] != "")
					{
						//////This condition is based on opal requirement to calculate amount on rounded value based on cash payment
						if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM')
						{
							$localAmount= RoundVal($localAmount);
							
						}
						////////////////////#1772 Ticket
						$transAmount = $localAmount / $exRate;
						$_SESSION["transAmount"] = $transAmount;
					}
					if ($_POST["t_amount"] != "")
					{
						$_SESSION["amount_transactions"] = $_POST["t_amount"];
					}
					if ($_POST["l_amount"] != "")
					{
						$_SESSION["amount_left"] = $_POST["l_amount"];
					}
				}
	
				if (CONFIG_FEE_BASED_TRANSTYPE == "1" && $_SESSION["transType"] == 'Pick up')
				{
					$amountType=$_SESSION["localAmount"];
					$feetransType=$_SESSION["transType"];
				}
				else
				{
					$amountType=$_SESSION["transAmount"];
					$feetransType=$_SESSION["transType"];
				}  
	
				$imFee = 0;
				if ($_SESSION["chDiscount"]!="")
				{
					$imFee = $_SESSION["discount"];
				}
				else if (CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 )
				{
					$imFee = 0 ;
						 //discount_request
				}
				else if (CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
				{
					if ($_SESSION["IMFee"] != "")
					{
						$imFee	= $_SESSION["IMFee"];
						$_SESSION["IMFee"] = "";
					}

					elseif ($_POST["IMFee"] != "")
					{
						$imFee	= $_POST["IMFee"];
					}
				}
				else
				{
					if(CONFIG_PAYIN_CUSTOMER != '1')
					{
						if(CONFIG_FEE_DISTRIBUTOR == 1)
						{
							$imFee = imFeeAgent($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						}
						elseif(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
						{
							$imFee = imFeeAgent($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	

						}
						if($imFee <= 0)
						{
							$imFee = imFee($amountType, $customerContent["c_country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						}
						
					}
					else
					{	
						
						if(CONFIG_BACKDATED_FEE == '1')
						{
							$feeQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
							if($_GET["transID"] != "" && $_SESSION["transAmount"] == $feeQuery["transAmount"])
							{
								$imFee = $feeQuery["IMFee"];
							}
						}
						elseif(CONFIG_FEE_DISTRIBUTOR == '1')
						{
							if($customerContent["payinBook"] != "" )
							{
								$imFee = imFeeAgentPayin($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $dist,$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							}
							else
							{
								$imFee = imFeeAgent($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							}
									 
						}
						elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
						{
							if($customerContent["payinBook"] != "" )
							{
								$imFee = imFeeAgentPayin($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							}
							else
							{ 
								$imFee = imFeeAgent($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);
							}
						}
						if($imFee == 0)
						{
							if($customerContent["payinBook"] != "" )
								$imFee = imFeePayin($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
								$imFee = imFee($amountType,  $customerContent["c_country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);
						}
					}
				}
				$totalAmount =  $_SESSION["transAmount"] ;
				$localAmount = $_SESSION["transAmount"] * $exRate;
				
				if(CONFIG_ROUND_NUMBER_ENABLED == '1')
				{
					$localAmount = round($localAmount);		
					
				}
	
				$imFeetemp = ($imFee);
	
				if(CONFIG_CURRENCY_CHARGES == '1')
				{
					$local = "No";
					$_SESSION["currencyCharge"]="";
					 $strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$_SESSION["currencyTo"].",%'";
					$currencies = selectMultiRecords($strCurrency);
					for ($c=0; $c < count($currencies); $c++)
					{
						if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
						$local = "Yes";
					}
					
					if($local=="No")
					{
						///////Out Currency Charges are working on Per 100 Amount
						$tempCharge = $_SESSION["transAmount"]/100.01;
						$tempCharge2 = (int)$tempCharge;
						$tempCharge2 = $tempCharge2 + 1;
						$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
						$_SESSION["currencyCharge"] = $tempCharge2; 
						$totalAmount = $totalAmount + $_SESSION["currencyCharge"];
					}
					
				}
				if($_SESSION["moneyPaid"] == 'By Cash')
				{
				
						$totalAmount = ( $totalAmount +  $imFee);
						$imFeetemp = ($imFee);		
						if(CONFIG_CASH_PAID_CHARGES_ENABLED)
						{
							$totalAmount = ( $totalAmount +  CONFIG_CASH_PAID_CHARGES);
							}			
										
				}
				elseif($_SESSION["moneyPaid"] == 'By Cheque')
				{
					$totalAmount = ($totalAmount +  $imFee);
					$imFeetemp = ($imFee);
							
				}
				else//if($_SESSION["moneyPaid"] == 'By Bank Transfer')
				{		 
			  
					$totalAmount = ($totalAmount + $imFee);
					$imFeetemp = ($imFee);
				}
				if($_SESSION["transType"]=="Bank Transfer")
				{		 
					$totalAmount = ( $totalAmount + $_SESSION["bankCharges"]);
						
				}
					
				if($_SESSION["transAmount"] != "" && $_SESSION["chDiscount"]=="")
				{
					
					if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
					{
						?>
							<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
							  <tr>
								<td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
								<td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".TE2."</b</font>"; ?></td>
							  </tr>
							</table>
						<?
					}
				}
			}
			else
			{
				$msgWrongTotal = "";
				/////Condition OF Calculation from Total Starts Here///////////
				if($_POST["totalAmount"] != '')
				{
					$totalAmount = $_POST["totalAmount"];
				}
				else if ( !empty( $_SESSION["totalAmount"] ) )
				{
					$totalAmount = $_SESSION["totalAmount"];
				}
				
				$totalAmountTemp = $totalAmount;
				/////////////////Bank Charges///////////////
				
				if($totalAmount > 0)
				{
				
					if($_SESSION["transType"]=="Bank Transfer")
					{
						if(CONFIG_CUSTOM_BANK_CHARGE == '1')
						{
							$_SESSION["bankCharges"] = getBankCharges($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"], $totalAmountTemp, $dist);
						}
						$totalAmountTemp = ( $totalAmountTemp - $_SESSION["bankCharges"]);
					}
						
					/////////////////////////////Cash Charges
					if($_SESSION["moneyPaid"] == 'By Cash')
					{
						if(CONFIG_CASH_PAID_CHARGES_ENABLED)
						{
							$totalAmountTemp = ( $totalAmountTemp -  CONFIG_CASH_PAID_CHARGES);
						}			
											
					}
					
					////////////////////////////Outer Currency Charges////////////
					
					if(CONFIG_CURRENCY_CHARGES == '1')
					{
						$local = "No";
						$_SESSION["currencyCharge"]="";
						 $strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$_SESSION["currencyTo"].",%'";
						$currencies = selectMultiRecords($strCurrency);
						for ($c=0; $c < count($currencies); $c++)
						{
							if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
							$local = "Yes";
						}
						
						if($local=="No")
						{
							///////Out Currency Charges are working on Per 100 Amount
							$tempCharge = $totalAmountTemp/100.01;
							$tempCharge2 = (int)$tempCharge;
							$tempCharge2 = $tempCharge2 + 1;
							$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
							$_SESSION["currencyCharge"] = $tempCharge2; 
							$totalAmountTemp = $totalAmountTemp - $_SESSION["currencyCharge"];
						}
						
					}
					///////////////////Calculation of Fee//////////////////
					$imFee = 0;
					if ($chDiscount != "")
					{ 
						$imFee = $discount;
					}
					elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0)
					{
						$imFee = 0 ;
						 //discount_request
					}
					elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
					{
						if ($_SESSION["IMFee"] != "")
						{
							$imFee	= $_SESSION["IMFee"];
							$_SESSION["IMFee"] = "";
						}
						elseif ($_REQUEST["IMFee"] != "")
						{
							$imFee	= $_REQUEST["IMFee"];
						}
					}
					if($customerContent["payinBook"] != "" )
					{
						$payinBook = true;
					}
					else
					{
						$payinBook = false;
					}

					$FeeData = CalculateFromTotal($imFee, $totalAmountTemp , $customerContent["c_country"], $benificiaryContent["Country"], $dist, 0, false, $_SESSION["transType"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);

					$transAmount = $FeeData[0];
					$imFee = $FeeData[1];

					///////////////////////Calculation of Exchange Rates//////////
		
					if(CONFIG_EXCH_RATE_NOT_EDIT == '1' && $_GET["transID"] != "")
					{
						$exRateUsed = selectFrom("select rateUsedID, rateWithMargin, currencyFrom , currencyTo from ".TBL_EXCH_RATE_USED." where transID  = '".$_GET["transID"]."'");
						$exRate = $exRateUsed["rateWithMargin"];
						$currencyFrom = $exRateUsed["currencyFrom"];
						$currencyTo 	= $exRateUsed["currencyTo"];
				
						
					}
					elseif ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $_SESSION["checkManualRate"] == 'Y') || $_GET["enterOwnRate"]=="Y")
					{
						$exID = 0;
						if ($_SESSION["exchangeRate"] != "")
						{
							$exRate	= $_SESSION["exchangeRate"];
							$_SESSION["exchangeRate"] = "";
							
						}
						elseif ($_POST["exchangeRate"] != "")
						{
							$exRate	= $_POST["exchangeRate"];
							
						}
						else
						{
							$exRate = 0;
							
						}
					}
					elseif(CONFIG_MULTI_RATE_PATTERN == '1')
					{
						$exID = 0;
						$exRate = getMultiPatternRate($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, 0, $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"], 'Yes');///Yes means to calculate Agent Margin as well
						$currencyFrom = $_SESSION["currencyFrom"];
						$currencyTo 	= $_SESSION["currencyTo"];
						
					}
					else
					{

					$exchangeData = getMultipleExchangeRates($customerContent["c_country"], $benificiaryContent["Country"], $_SESSION["currencyTo"], $dist, $transAmount, $currFrom);
						$exID = $exchangeData[0];
						$exRate	= $exchangeData[1];
						$currencyFrom = $exchangeData[2];
						$currencyTo = $exchangeData[3];
						$_SESSION["currencyFrom"] = $currencyFrom;
						
					}
					///////////////////	Calculating Local Amount
					if($_SESSION["transAmount"] != "")
					{
						$localAmount = $transAmount * $exRate;
						

						if(CONFIG_ROUND_NUMBER_ENABLED == '1')
						{
							$localAmount = round($localAmount);		
							
						}

						$_SESSION["localAmount"] = $localAmount;
					}

					if ($msgWrongTotal != "")
					{
						$localAmount = "";
						
						$_SESSION["localAmount"] = "";
					}

					/////////////////////Error Message if Fee Doesn't Exist/////////
					if($_SESSION["transAmount"] != "")
					{
						if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
						{
							?>
								<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
								  <tr>
									<td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
									<td width="635"><? echo "<font color='#990000'><b>".TE2."</b</font>"; ?></td>
								  </tr>
								</table>                    
							<?
						}
					}
				}
				if ($msgWrongTotal != "")
				{
					?>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
						  <tr>
							<td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
							<td width="635"><? echo "<font color='#990000'><b>".$msgWrongTotal . "</b</font>"; ?></td>
						  </tr>
						</table>                    
					<?	
				}
			}
			  ?>
                    <table width="676" border="0" cellpadding="2" cellspacing="0">
					<!-- Inclusive/Exclusive Drop Down Starts -->
					<?
						if(CONFIG_CALCULATE_BY == '1')
						{ 
					?>
							<tr>
							<td width="128" height="20" align="right"><font color="#005b90">Calculate :</font></td>
							<td width="195" height="20">
						
							<select name="calculateBy" id="calculateByID" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
							<?=(CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE == "Exclusive of Fee")? "<option value='exclusive'>" . CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE . "</option>":"<option value='exclusive'>By Cash</option>"?>
							<?=(CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE == "Inclusive of Fee")? "<option value='inclusive' selected>" . CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE . "</option>":"<option value='inclusive' selected>By Bank Transfer</option>"?>
							
							</select>
							<script language="JavaScript">
							SelectOption(document.addTrans.calculateBy, "<?=$_SESSION["calculateBy"]?>");
							</script></td>
							</tr>
					<?
						}
						
						/**
						 * This page will swap the amount and local amount up and down
						 * With above, currency from and currency to are also swapped
						 * For this purpose, there is a config variable CONFIG_SWAP_AMOUNT_LOCALAMOUNT
						 * [by Jamshed]
						 */
						
						include "swap-amount-local.php";
					?>
					<INPUT TYPE="hidden" NAME="benCountry" ID="benCountryID" VALUE="<? echo $benCountryName;?>">
					<INPUT TYPE="hidden" NAME="custCountry" ID="custCountryID" VALUE="<? echo $customerContent["c_country"];?>">
					<!-- Inclusive/Exclusive Drop Down Ends -->
                      <tr>
                        <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$preFix Fee");}?></font></td>
                        <td height="20" align="left">
						<!-- <input type="text" name="IMFee" value="<? echo ($imFeetemp > 0 ? round($imFeetemp, 4) : "") ?>" maxlength="25" readonly> -->
						<input type="text" name="IMFee" ID="IMFee" value="<? echo ($imFee > 0 ? round($imFee, 4) : "") ?>" maxlength="25" readonly="readonly">
						<input type="hidden" name="IMFee" value="<? echo $imFee;?>">
						</td>
                      </tr>
                      
					<tr>
		              	<td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                        <td height="20" align="left">
							<select name="transactionPurpose">
								<option value="">-select one-</option>
								<option value="Business">Business</option>
								<option value="Family Assistant">Family Assistance</option>
								<option value="Help">Help</option>
								<?
									if( defined("CONFIG_OTHER_TRANSACTION_PURPOSE")
										&& CONFIG_OTHER_TRANSACTION_PURPOSE == "1")
									{
										?>
											<option value="Other">Other</option>
										<?
									}
								?>
							</select>
                            <script language="JavaScript">
					         	SelectOption(document.addTrans.transactionPurpose, "<?=$_SESSION["transactionPurpose"]; ?>");
							</script>
							</td>
		              </tr>
                      <tr>
                      	<td width="128" height="20" align="right"><font color="#005b90">Money Paid</font></td>
                        <td width="195" height="20" align="left">
							<? 
								if( CONFIG_CALCULATE_BY == '1')
				              	{ 
              						echo "<input type='text' name='moneyPaid' id='moneyPaid' value='";
              			
			              			if($_SESSION["calculateBy"] == "inclusive" )
									{
            		  					if(CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE == "Inclusive of Fee")
										{
              								echo CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE;
										}
              							else
										{
              								echo "By Bank Transfer";
										}
									}
				              		elseif($_SESSION["calculateBy"] == 'exclusive')
									{
										if(CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE == "Exclusive of Fee")
										{
											echo CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE;
										}
										else
										{
											echo "By Cash";
										}
									}
									
									echo "' maxlength='25' readonly>";
				              	} 
								else 
								{
							?>
									<select name="moneyPaid" id="moneyPaid"  onChange="document.addTrans.action='add-transaction.php?transID=<?=$_GET["transID"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();" >
            			                <option value="">-select one-</option>
                        			    <? 
											if(CONFIG_MONEY_PAID_OPTIONS == "1")
											{ 
										?>
												<option value="By Cash HCD">By Cash HCD</option>
												<option value="By Cash HBT">By Cash HBT</option>
												<option value="By Cash HQD">By Cash HQD</option>
												<option value="By Cash BCD">By Cash BCD</option>
												<option value="By Cash BBT">By Cash BBT</option>
												<option value="By Cash BQD">By Cash BQD</option>
												<option value="By Cash WICC">By Cash WICC</option>
			                            <? 
											} 
										?>
										<option value="By Cash">By Cash</option>
										<option value="By Cheque">By Cheque</option>
										<option value="By Bank Transfer">By Bank Transfer</option> 
			                            <?
            				                if(CONFIG_MONEYPAID_ONCREDIT == "1")
                            				{ 
										?>
				                            	<option value="On Credit">On Credit</option>
                			            <? 
                            				} 
											
			                            	/**
            			                	 * @Ticket# 3564
                        			    	 */
			                            	if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1)
											{ 
            			                ?>
				                            	<option value="By Card">By Card</option>
                			            <? 
											} 
										?>
									</select>						
	                          <? 
    		                      }
	                          ?>
							<script language="JavaScript">
								SelectOption(document.addTrans.moneyPaid, "<?=$_SESSION["moneyPaid"]?>");
							</script>
						</td>
                        <td width="96" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                        <td width="241" height="20" align="left">
							<input type="text" name="totalAmount" id="totalAmountID" value="<?= ($totalAmount > 0 ? round($totalAmount, 2) : ""); ?>" maxlength="25" <?=( (empty($fromTotalConfig) || $fromTotalConfig != 1) ? "readonly" : ""); ?>  <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1" && $fromTotalConfig == 1) { ?>onblur="updateAmounts('total');" <? }?> >
							<?
								if ( $fromTotalConfig == 1 )
								{
									?>
										<input type="submit" name="fromTotal" value="Calculate" onClick="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>';">
									<?
								}
							?>
						</td>
                      </tr>
					<?
						if ( defined("CONFIG_SHOW_FUNDS_SOURCE_ON_ONLINE_MODULE") && CONFIG_SHOW_FUNDS_SOURCE_ON_ONLINE_MODULE == 1)
						{
							?>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td align="right">
										<font color="#005b90">Funds Source<? echo (CONFIG_NONCOMPUL_FUNDSOURCES != '1' ? "<font color='#ff0000'>*</font>" : "") ?></font>
									</td>

									<td>
										<select name="fundSources">
											<option value="">-select one-</option>
											<option value="Salary">Salary</option>
											<option value="Savings">Savings</option>
											<option value="Loan">Loan</option>
										</select>
										<script language="JavaScript">
											SelectOption(document.addTrans.fundSources, "<?=$_SESSION["fundSources"]?>");
										</script>
									</td>
								</tr>
							<?
						}
					?>
                      <tr>
                        <td align="right"><font color="#005b90"> <? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font> </td>
              					<td align="left"><input name="tip" type="text" id="tip" value="<?=$_SESSION["tip"]; ?>"></td>
                    
						<?
						 if($_SESSION["transType"] == "Bank Transfer")
						{
						?>										
            <td width="96" align="right"><font color="#005b90">Bank Charges</font></td>
            <td width="241" align="left">
						<input name="bankCharges" type="text" id="bankCharges" value="<?=$_SESSION["bankCharges"]?>" maxlength="20" readonly>
						<input type="hidden" name="bankCharges" value="<?=$_SESSION["bankCharges"]?>">
						</td>
						<?
						}
						else
						{
						?>
						 <td width="96" align="right"><font color="#005b90">&nbsp;</font></td>
                        <td width="241">&nbsp;</td>						
						<?
					}
						?>
                      </tr>
                      
                      <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <tr>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><input type="text" name="currencyCharge" value="<?=$_SESSION["currencyCharge"]?>" maxlength="25" readonly></td>
					  </tr>
					  <? } ?>

  <?
  if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
  {
  ?>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>
					  <?
$strQuery = "SELECT count(*) as nCount FROM cm_cust_credit_card  where customerID  = '$customerID'";
$nResult = mysql_query($strQuery)or die(__LINE__."- Invalid query: " . mysql_error()); 		
$rstRow = mysql_fetch_array($nResult);				
$nCount = $rstRow["nCount"];

if($nCount >= 1)
{

					  ?>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>
					  <tr>
					          <td align="right"><font color="#005b90">Existing 
                                Card Nos*</font> </td>
					  <td colspan="3" align="left">
					  		  <select name="creditCard" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>&empty=empty'; document.addTrans.submit();">
							  <OPTION value="">- Enter New Card No-</OPTION>
						<?
							$strQuery = "SELECT *  FROM  cm_cust_credit_card where customerID  = '$customerID'";
							$nResult = mysql_query($strQuery)or die(__LINE__."- Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								//$cardNo   = $rstRow["cardNo"];
								$cardNo = "***************". substr($rstRow["cardNo"],12,4);								
								$cID   = $rstRow["cID"];	
								//$erID  = $rstRow["erID"];							
								if($cID  == $_SESSION["creditCard"])
									echo "<option selected value ='$cID'>".$cardNo ."</option>";			
								else
								    echo "<option  value ='$cID'>".$cardNo ."</option>";	
							}
													
						?>
                          </select>
						   <script language="JavaScript">
         	SelectOption(document.addTrans.creditCard, "<?=$_SESSION["creditCard"]; ?>");
                                </script>
					  </td>
					  </tr>					  
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>					  					  
<?
}

$strQuery = "SELECT * FROM cm_cust_credit_card  where cID   = '".$_SESSION["creditCard"]."'";
$nResult = mysql_query($strQuery)or die(__LINE__."- Invalid query: " . mysql_error()); 		
//$rstRow = mysql_fetch_array($nResult);
$boolean = true;
while($rstRow = mysql_fetch_array($nResult))
{



	$_SESSION["cc_cardNo"] = $rstRow["cardNo"];
	$credCardNo = "***************". substr($rstRow["cardNo"],12,4);
	$ExpiryDate = explode("-",$rstRow["expiryDate"]);
	$_SESSION["ccExpYr"] = $ExpiryDate[1];
	$_SESSION["ccExpMo"] = $ExpiryDate[0];
	$_SESSION["cc_cardType"] = $rstRow["cardName"];
	$_SESSION["cc_cvvNo"] = $rstRow["cardCVV"];
	$_SESSION["cc_firstName"] = $rstRow["firstName"];
	$_SESSION["cc_lastName"] = $rstRow["lastName"];
	$_SESSION["cc_address1"] = $rstRow["address"];
	$_SESSION["cc_address2"] = $rstRow["address2"];
	$_SESSION["cc_postcode"] = $rstRow["postalCode"];
	$_SESSION["cc_city"] = $rstRow["city"];
	$_SESSION["cc_state"] = $rstRow["state"];
	$_SESSION["cc_country"] = $rstRow["country"];
	
	$boolean = false;
}
if($_SESSION["creditCard"] == "" && $_GET["empty"] == "empty")
{
	$_SESSION["cc_cardType"]= "";
	$_SESSION["cc_cardNo"]= "";
	$_SESSION["cc_expiryDate"]= "";
	$_SESSION["cc_cvvNo"]	= "";
	$_SESSION["cc_firstName"]= "";
	$_SESSION["cc_lastName"]= "";
	$_SESSION["cc_address1"]= "";
	$_SESSION["cc_address2"]	= "";
	$_SESSION["cc_city"]= "";
	$_SESSION["cc_state"]= "";
	$_SESSION["cc_postcode"]= "";
	$_SESSION["cc_country"]	= "";
}

//if($_SESSION["creditCard"] == "" && $_GET["empty"] == "empty")
{

?>
					  <tr>
					  <td colspan="4"><fieldset>
                    <legend class="style2">Credit/Debit Card Details </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      					  <tr>
					  <td colspan="4" align="left">
					  <br>
					  </td>
					  </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
                        <td width="196" height="20" align="left">
						
  <?
  if($_SESSION["moneyPaid"] == 'By Credit Card')
  {
  ?>						
						<select name="cc_cardType">
                            <option value="">-----select one-----</option>
                            <option value="VISA">VISA</option>
                            <option value="MASTERCARD">MASTERCARD</option>
                          </select>

                            <script language="JavaScript">
         	SelectOption(document.addTrans.cc_cardType, "<?=$_SESSION["cc_cardType"]; ?>");
                                </script>
  <?
  }
  elseif($_SESSION["moneyPaid"] == 'By Debit Card')
  {
  ?>
						<select name="cc_cardType">
                            <option value="">-----select one-----</option>
							<!--
                            <option value="MASTERCARD">Maestro</option>
                            <option value="MASTERCARD">Solo</option>
							<option value="MASTERCARD">Switch</option>-->
							<option value="VISA">Visa Delta</option>
							<option value="VISA">Visa Electron</option>							
                          </select>

                            <script language="JavaScript">
         	SelectOption(document.addTrans.cc_cardType, "<?=$_SESSION["cc_cardType"]; ?>");
                                </script>  
  <?
  }
  ?>						</td>
                        <td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
                        <td width="183" height="20" align="left"><input name="cc_cardNo" type="password" id="cc_cardNo" <? if ($boolean == false){?>value="<? echo  $credCardNo; ?>"  readonly<? }else{?> value="<? echo  $_SESSION["cc_cardNo"]; ?>"<? }?> maxlength="16"></td>

					  </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
                        <td width="196" height="20" align="left">						
						<SELECT name=ccExpMo> 
						<OPTION value="" selected>-  -<OPTION value=01>01
						<OPTION value=02>02<OPTION 
                          value=03>03<OPTION value=04>04<OPTION value=05>05<OPTION 
                          value=06>06<OPTION value=07>07<OPTION value=08>08<OPTION 
                          value=09>09<OPTION value=10>10<OPTION value=11>11<OPTION 
                          value=12>12</OPTION></SELECT> 
						    <script language="JavaScript">
         	SelectOption(document.addTrans.ccExpMo, "<?=$_SESSION["ccExpMo"]; ?>");
                                </script>
						  <SELECT name=ccExpYr> 
                          <OPTION value="" selected>- - - -<OPTION value=2005>2005<OPTION 
                          value=2006>2006<OPTION value=2007>2007<OPTION 
                          value=2008>2008<OPTION value=2009>2009<OPTION 
                          value=2010>2010<OPTION value=2011>2011<OPTION 
                          value=2012>2012<OPTION 
                      value=2013>2013</OPTION></SELECT>
						  <script language="JavaScript">
         	SelectOption(document.addTrans.ccExpYr, "<?=$_SESSION["ccExpYr"]; ?>");
                                </script>
						
						</td>
                        <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only"><input name="cc_cvvNo"  <? if ($boolean == false){?> readonly<? }?> type="password" id="cc_cvvNo" value="<?=$_SESSION["cc_cvvNo"]; ?>" maxlength="4"></td>
                      </tr>
					  
					  <tr>
						  <td colspan="4" align="left">
							  <br>
							   <legend class="style2">Credit/Debit Card Address </legend>
							  <Br>
						  </td>
					  </tr>
                      <tr>
                                    <td width="143" height="20" align="right"><font color="#005b90">First 
                                      Name *</font></td>
                        <td width="196" height="20" align="left"><input name="cc_firstName" type="text" id="cc_firstName" value="<?=$_SESSION["cc_firstName"]; ?>" <? if ($boolean == false){?> readonly<? }?> >
                                    </td>
                                    <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Last 
                                      Name * </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only"><input name="cc_lastName" type="text" id="cc_lastName" value="<?=$_SESSION["cc_lastName"]; ?>" <? if ($boolean == false){?> readonly<? }?> ></td>
                      </tr>
                      <tr>
                                    <td width="143" height="20" align="right"><font color="#005b90">Address*</font></td>
                        <td width="196" height="20" align="left"><input name="cc_address1" type="text" id="cc_address1" value="<?=$_SESSION["cc_address1"]; ?>" <? if ($boolean == false){?> readonly<? }?>>
                                    </td>
                                    <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Address2* 
                                      </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only"><input name="cc_address2" type="text" id="cc_address2" value="<?=$_SESSION["cc_address2"]; ?>" <? if ($boolean == false){?> readonly<? }?>></td>
                      </tr>
                      <tr>
                                    <td width="143" height="20" align="right"><font color="#005b90">Postcode*</font></td>
                        <td width="196" height="20" align="left"><input name="cc_postcode" type="text" id="cc_postcode" value="<?=$_SESSION["cc_postcode"]; ?>" <? if ($boolean == false){?> readonly<? }?>>
                                    </td>
                                    <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">City</font><font color="#005b90">* 
                                      </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only"><input name="cc_city" type="text" id="cc_city" value="<?=$_SESSION["cc_city"]; ?>" <? if ($boolean == false){?> readonly<? }?>></td>
                      </tr>
                      <tr>
                                    <td width="143" height="20" align="right"><font color="#005b90">State*</font></td>
                        <td width="196" height="20" align="left">
						
						
						<?
						$CountryName = $_SESSION["cc_country"];
						$q = "select * from states  where country  = '$CountryName' ";
						$nResult = mysql_query($q)or die(__LINE__."- Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);						
						
						if (count($rstRow) > 1)
						{
						?>
							<select name="cc_state">
							  <OPTION value="">- Select State-</OPTION>
						<?
							$strQuery = "SELECT *  FROM  states where country  = '$CountryName' ORDER BY country ";
							$nResult = mysql_query($strQuery)or die(__LINE__."- Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$state  = $rstRow["state"];	
								//$erID  = $rstRow["erID"];							
								if($state == $_SESSION["cc_state"])
									echo "<option selected value ='$state'>".$state."</option>";			
								else
								    echo "<option  value ='$state'>".$state."</option>";	
							}
							?>
							</select>
                            <script language="JavaScript">
         	SelectOption(document.addTrans.cc_state, "<?=$_SESSION["cc_state"]; ?>");
                                </script>							
							<?
						}						
						else
						{
						?>
						<input name="cc_state" type="text" id="cc_state" value="<?=$_SESSION["cc_state"]; ?>" <? if ($boolean == false){?> readonly<? }?>>						
						<?
						}
																			
						?>
                          						
                                    </td>
                                    <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Country</font><font color="#005b90">* 
                                      </font></td>
                                    <td width="183" height="20" align="left" title="For european Countries only"> 
                                      <select name="cc_country" style="font-family:verdana; font-size: 11px" onChange="document.addTrans.action='add-transaction.php?transID=<? echo $_GET["transID"]?>'; document.addTrans.submit();">
                                        <option value="">-Slect Country-</option>
                                        <option value="United States">United States</option>
                                        <option value="Canada">Canada</option>
                                        <?
										$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
										for ($i=0; $i < count($countires); $i++)
										{
	
											if($countires[$i]["country"] == $_SESSION["cc_country"])
														{
														$tempVar = $_SESSION["cc_country"];
														?>
												<OPTION value="<?=$tempVar; ?>" selected>
												<?=$_SESSION["cc_country"]; ?>
												</OPTION>
												<?
														}				
														?>
														
															<option value="<?=$countires[$i]["country"]; ?>"> 
															<?=$countires[$i]["country"]; ?>
															</option>
															<?
														
										}										
											?>
                                      </select>
                      </tr>
<tr>
					  <td  colspan="4"  height="20" align="left"><Br></td>
					  </tr>
                     <?
					 if($_SESSION["creditCard"] == "")
					 {
					 ?>
					  <tr>
                        <td width="128" height="20" align="right" valign="top"><input type="checkbox" name="Save" value="Y"></td>
                        <td height="20" colspan="3" align="left">Do you Want to Save this Information in your EWallet Account.</td>
                      </tr>
					  <?
					  }
					  ?>					  					  					  					  				  
  					  <tr>
					  <td  colspan="4"  height="20" align="left"><Br></td>
					  </tr>		
                    </table>				
						  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>
					  		
<?
}//end of send boolean if conditaion
}
?>			 <? if(CONFIG_SECRET_QUEST_ENABLED)
            {	?>
             <tr>
              <td align="right"><font color="#005b90"> Secret Question</font> </td>
              <td align="left"><input name="question" type="text" id="question" value="<?=$_SESSION["question"]; ?>"></td>
             	<td align="right"><font color="#005b90">Answer</font></td>
              <td align="left"><input name="answer" type="password" id="answer" value="<?=$_SESSION["answer"]; ?>"></td>
            </tr>
            <? 
          	}?>
          
                     	
            
                      <tr>
                        <td width="128" height="20" align="right" valign="top"><input type="checkbox" name="Declaration" value="Y"></td>
                              <td height="20" colspan="3" align="left"> <? if(CONFIG_TRANS_CONDITION_ENABLED)
                        	{
                        		echo(CONFIG_TRANS_COND);
                        		?>
                        		<a href="#" onClick="javascript:window.open('<?=CONFIG_CONDITIONS?>', 'Conditions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" class="style2">Full Terms and Conditions</a>
                        		<? }else{
                        	 ?>
                        	I accept 
                          all <? echo $company;?> Transfer Terms and Conditions publish 
                          on <? echo $company;?> Transfer website. I accept <? echo $company;?> Transfer Anti-Money Laundry policy. 
													<? echo $company;?> Transfer has a right to ask for any further 
                          documentation if required for your transaction. 
                          Your transaction will be executed once <? echo $company;?> Transfer 
                          received clear funds in our bank account
                        	<? } ?> </td>
                      </tr>
                      <tr>
                        <td width="128" height="20" align="right" valign="top">&nbsp;</td>
                        <td height="20" colspan="3" align="center"><input type="hidden" name="transID" value="<? echo $_GET["transID"]?>">
                            <input type="hidden" name="customerID" value="<? echo $customerID ?>">
                            <input type="submit" value="Send" id="sendID">
&nbsp;&nbsp;
<input  type="button" name="Submit2" value=" Clear " onClick=window.location.href="add-transaction.php?create=Y">
			  </td>
                      </tr>
                    </table>
                  </fieldset></td>
                </tr>
                <?
		  }
//}
?>
              </form>
            </table></td>
			
          <td valign="top"> 
            <?
				include "exchange-rates-agent.php";
				?>
          </td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>
<?
$boolean = true;
include "footer.php";
?>