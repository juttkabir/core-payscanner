<?
//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;
if(CONFIG_FEE_DEFINED == '1'){$feeses=CONFIG_FEE_NAME;}else{ $feeses= "$preFix Fee";}
							$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
							 
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
							  </tr>
							  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'><p>Thanks for choosing $company Transfer as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							
							  <tr>
								<td> Transaction Type:  ".$_POST['transType']." </td>
								<td> Status: $Status </td>
							  </tr>
							  <tr>
								<td> $systemCode:   $imReferenceNumber </td>
								<td> Transaction Date:  ".$tran_date."  </td>
							  </tr>
							  <tr>
								<td><p> Authorised Date: ".$authoDate."</p></td>								
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  
							  
							  <tr>
								<td><p><strong>Beneficiary Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td colspan='2'><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
							  </tr>
							  <tr>
								<td> Address:  $benAddress </td>
								<td> Postal / Zip Code:  $benZip </td>
							  </tr>
							  <tr>
								<td> Country:   $benCountry   </td>
								<td> Phone:   $benPhone   </td>
							  </tr>
							  <tr>
								<td><p>Email:  $benEmail   </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";
							   
							if(trim($_POST['transType']) == 'Bank Transfer')
							{
								if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
								{
									$message .= "<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										</tr>
										<tr>
											<td>IBAN</td>
											<td>" . $_SESSION["IBAN"] . "</td>
										</tr>";
								}
								else
								{
									
									$message .= "<tr>
													<td><p><strong>Beneficiary Bank Details </strong></p></td>
													<td>&nbsp;</td>
												</tr><tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									  </tr>
									  <tr>
										<td> Bank Name:  ".$_SESSION['bankName']."  </td>
										<td> Acc Number:  ".$_SESSION['accNo']."  </td>
									  </tr>
									  <tr>
										<td> Branch Code:  ".$_SESSION['branchCode']."  </td>
										<td>&nbsp;</td>
									  </tr>
									  <tr>
										<td colspan='2'> Branch Address:  ".$_SESSION['branchAddress'].", ". $_SESSION['branchCity'] . "  </td>
									  </tr>
									";
								}
							}
							elseif(trim($_POST['transType']) == "Pick up")
							{
							$message .= "
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Collection Point Details  </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Agent Name : $agentname </td>
								<td> Contact Person:  $contactperson </td>
							  </tr>
							  <tr>
								<td> Company:  $company2 </td>
								<td> Address:  $address </td>
							  </tr>
							  <tr>
								<td>Country:   $country</td>
								<td>City:  $city</td>
							  </tr>
							  <tr>
								<td>Phone:  $phone</td>
								<td>&nbsp;</td>
							  </tr> ";
							
							}
							$message .="
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Amount Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Exchange Rate:  ".$_SESSION['exchangeRate']."</td>
								<td> Amount:  ".$_SESSION['transAmount']." </td>
							  </tr>
							  <tr>
								<td> $feeses:  ".$_SESSION['IMFee']." </td>
								<td> Local Amount:  ".$_SESSION['localAmount']." </td>
							  </tr>  <tr>
								<td> Transaction Purpose:  ".$_SESSION['transactionPurpose']." </td>
								<td> Total Amount:  ".$_SESSION['totalAmount']." </td>
							  </tr>  <tr>
								<td> Money Paid:  ".$_SESSION['moneyPaid']." </td>
								<td> Bank Charges:  ".$_SESSION['bankCharges']." </td>
							  </tr>  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>  
							</table>
							";
							sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

?>