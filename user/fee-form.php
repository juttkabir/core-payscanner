<?

if($_POST["sCourntry"] != "") {
	$sCourntry = urldecode($_POST['sCourntry']);
} else {
	$sCourntry = urldecode($_GET['sCourntry']);
}
if($_POST["dCourntry"] != ""){
	$dCourntry = urldecode($_POST['dCourntry']);
}else {
	$dCourntry = urldecode($_GET['dCourntry']);
}
if($_POST["amount"] != ""){
	$amount = urldecode($_POST['amount']);
}else{
	$amount = urldecode($_GET['amount']);
}
	
$nError =  urldecode($_GET['nError']);
$fee = urldecode($_GET['fee']);
$total = urldecode($_GET['total']);
$rate = urldecode($_GET['rate']);
$damount = urldecode($_GET['damount']);
$nExtraCharges = urldecode($_GET['nExtraCharges']);
/*
$queryExchRate = "SELECT * FROM " . TBL_EXCHANGE_RATES . " WHERE `countryOrigin` = '$sCourntry' AND `country` = '$dCourntry' AND `dated` = (SELECT MAX(dated) FROM " . TBL_EXCHANGE_RATES . ") ";
$ExchRate = selectFrom($queryExchRate);

$margin = $ExchRate["marginPercentage"];

$rate = round($ExchRate["primaryExchange"] - (($ExchRate["primaryExchange"]) * $margin / 100 ) , 4);

$fee = 0;
$imFee = selectFrom("select Fee, feeType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$dCourntry' and origCountry='$sCourntry'");
if($imFee["Fee"] != "" && $imFee["Fee"] != 0) {
	if($imFee["feeType"]=="percent") {
		$fee = ($amount * $imFee["Fee"])/100;
	} else {
		$fee = $imFee["Fee"];
	}
}
echo $fee;
*/

if($_POST["rateFor"] != "")
{
	$_SESSION["rateFor"] = $_POST["rateFor"]; 
	
	if($_POST["rateFor"] == 'distributor')
	{
		$_SESSION["agentID"] = $_POST["agentID"]; 
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}

function clearAll()
{
	document.forms.frmfeecalculator.amount.value = '';
	document.forms.frmfeecalculator.fee.value = '';
	document.forms.frmfeecalculator.rate.value = '';
	document.forms.frmfeecalculator.damount.value = '';
	document.forms.frmfeecalculator.total.value = '';
}

function checkForm(theForm) {	

	 var bName = navigator.appName;
	 var bVer = parseInt(navigator.appVersion);
	
	 var MF = (bName == "Netscape");
	 if(MF)
	 {
		if(theForm.sCourntry.value == "" ){
			alert("Please provide From Country Name.");
			theForm.sCourntry.focus();
			return false;
		}
		if(theForm.dCourntry.value == "" ){
			alert("Please provide To Country Name.");
			theForm.dCourntry.focus();
			return false;
		}
	}

	if(theForm.amount.value == ""  && theForm.damount.value == "")
	{    	
		alert("Please provide Amount in Local or Destination Currency");				
		theForm.amount.focus();
        return false;
    }


if(theForm.rateFor.value != ""  && theForm.agentID.value == "")
	{    	
		alert("Please select Distributor.");				
		theForm.agentID.focus();
        return false;
    }


	return true;
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>

		

<title>Fee Calculator</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style type="text/css">
<!--
.style4 {font-family: Verdana; font-weight: bold; }
-->
</style>
</head>

<body>


<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">
<form action="fee_calculator1.php" method="post"  onSubmit="return checkForm(this);" name="frmfeecalculator">	
	<table width="600" border="0" align="center" cellpadding="1" cellspacing="0" bgColor=#99C6E2>

<tr>
	<td align="left">
		<font  size="4" color="ffffff">&nbsp;&nbsp;<strong><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$preFix Fee");}?> Calculator</strong></font>
	</td>
</tr>
<tr>
   <td>
	  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgColor=#ffffff>
		<tr>
			<Td colspan="3" align="center"><FONT face=arial size=2>
					<?					
					// if any field is empty or 
						if(!empty($nError))
						{
							if($nError == 1)
								{
									$strErrMsg = "Please Select <b>From</b> Country.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 2)
								{
									$strErrMsg = "Please Select <b>To</b> Country.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 3)
								{
									$strErrMsg = "Please Enter the Amount in <b>Local Currency</b>.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}	
							if($nError == 4)
								{
									$strErrMsg = "Amount can not be a string.";
									echo "
										<font color=ff0000 size=+1><u>Error</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}		
							if($nError == 5)
								{
									$strErrMsg = "Please Enter the Amount in <b>Destination Currency</b>.";
									echo "
										<font color=ff0000 size=+1><u>Empty Field</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";
								}
							if($nError == 6)
								{
									$strErrMsg = "Amount exceeds our limit Please enter a valid Range.";
									echo "
										<font color=ff0000 size=+1><u>Amount Exceeded</u><br></font>
										<font color=ff0000>$strErrMsg</font><br><br>
									";								
								}												
																				
			
						}
					?>	</font><br>
			</Td>
		  </tr>	
		  <tr>
		      <td align="center" class="style4">From</td>
		      <td align="center">&nbsp;</td>
		      <td align="center"><span class="style4">To</span></td>
	      </tr>
	      <tr>
			<td align="center">
					
			<select name="sCourntry" size="8"  style="WIDTH: 140px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 8pt; " >
			
				<?
				
					echo "<option selected>$logedUseCountry</option>";
				
				?>
			</select>&nbsp;</td>
			<td align="center"><font  face="Verdana"><b></font></td>
			<td align="center"><select name="dCourntry" size="8"  style="WIDTH: 140px;FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;FONT-SIZE: 8pt; " onChange="document.forms.frmfeecalculator.action='frm-fee-calculator.php'; document.forms.frmfeecalculator.submit();">
			<?
			
			 if(CONFIG_ENABLE_ORIGIN == "1"){
     		$countryTypes = " and countryType like '%destination%' ";
      	}else{
    		$countryTypes = " ";
     	}
      if(CONFIG_COUNTRY_SERVICES_ENABLED){
        	$strQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName";
			}else{
				  $strQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName";
			}
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($rstRow = mysql_fetch_array($nResult))
				{
					$strCountry = $rstRow["countryName"];															
					if($dCourntry == $strCountry)
						echo "<option value='$dCourntry' selected>".$strCountry."</option>";			
					else
					echo "<option>".$strCountry."</option>";	
				}
				
				?>
			</select>&nbsp;</td>
		  </tr>
		  <tr>
			<Td colspan="3">
				<br>
			</Td>
		  </tr>	  
	</table>	
  </td>
</tr>
<tr>
<td align="left" ><font size="2" color="#ffffff" ><b><strong>&nbsp;&nbsp;Calculate <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?> and  Amount</strong></font></td>
</tr>
<tr>
  <Td>
      <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff">
		  <tr>
			<td width="242">&nbsp;</td>
			<td width="358">&nbsp;</td>                           
		  </tr>
		  <tr>
			<td colspan="2" >
			<font size="2" color="#000000">&nbsp;&nbsp;&nbsp;
			<?
			
			if($dCourntry != "")
			{
				$strQuery = "select serviceAvailable, bankCharges, outCurrCharges  from " . TBL_SERVICE. ", ".TBL_COUNTRY." where 1 and (toCountryId = countryId and  countryName='".$dCourntry."')";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
				$rstRow = mysql_fetch_array($nResult);				
				$serviceAvailable = $rstRow["serviceAvailable"];
				$serviceAvailable1 = $rstRow["serviceAvailable"];
				$Delivery = $rstRow["deliveryTime"];	
				if($serviceAvailable=="")
					echo "None";
				else
				{
				 	$serviceAvailable  = explode (',',$serviceAvailable);
					if($serviceAvailable[0] == "Cash Collection")
					{										
						echo "Services available for <b>$dCourntry</b>:<b>&nbsp;".$serviceAvailable[0]."</b>&nbsp;";
						echo ",".$serviceAvailable[1].",&nbsp;".$serviceAvailable[2];				
					}
					else				
						echo "Services available for <b>$dCourntry</b>:".$serviceAvailable1;	
						echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;Estimated Delivery Time:&nbsp;<b>".$Delivery;		
				}
			}
			?></font></td>
			                           
		  </tr>		
		  
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>                           
		  </tr>		    
		  <tr>
			<td align="right"  style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Amount in Local Currency:&nbsp;</td>
			<td align="left">
				  <?
					if(empty($amount) || $amount == 0)
					{
				  ?>
						<INPUT maxLength=15 size=21  name=amount class="Login" value="">
					<?
					}
					else
					{
					?>
						<INPUT maxLength=15 size=21  name=amount value="<? echo number_format($amount,2,'.',',');?>" class="Login">
					<?
					}								
					?>	
					&nbsp;&nbsp;<INPUT type=submit value=" Calculate Dest. Amount" name=SUBMIT1 bgcolor="YELLOW" class="Button">							
			</td>                           
		  </tr>
		   <?
 	         /* if(CONFIG_FEE_DISTRIBUTOR == 1)
 	          {?>
 	              <tr>
 	              <td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Select <? if(CONFIG_FEE_DISTRIBUTOR == 1){echo("Distributor");}else{ echo("Agent");}?> for Fee </td>
 	             <td align="left">
 	                        <select name="feeFor" style="font-family:verdana; font-size: 11px">
 	                        <option value="">- Select One -</option>
 	                           
 	                   <?
				 	                   	
																 $agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N' order by agentCompany");
																
 	                                    for ($i=0; $i < count($agents); $i++)
 	                                    {
 	                            ?>
 	                                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
 	                                <?
 	                                    }
 	                                ?>
 	                 </select>
 	                 <script language="JavaScript">SelectOption(document.forms.frmfeecalculator.feeFor,"<?=$_SESSION["feeFor"]; ?>");</script>
 	             </td>
 	           </tr> 
 	          <?
 	          }*/
 	          ?>
		  
		  <tr> 
	      	<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Exchange Rate Based on</td>
	         <td align="left">
						<select name="rateFor" style="font-family:verdana; font-size: 11px" onChange="document.forms.frmfeecalculator.action='frm-fee-calculator.php'; document.forms.frmfeecalculator.submit();">
		            <option value="">- Select One -</option>
		            <option value="generic">- Generic Rate -</option>
		            <option value="distributor">- Distributor -</option>
		         </select>
		         <script language="JavaScript">SelectOption(document.forms.frmfeecalculator.rateFor,"<?=$_SESSION["rateFor"]; ?>");</script>
	         </td>
	       </tr>  
	       <? if($_SESSION["rateFor"] == "distributor")
	       		{
	       ?>  
	       
	       			<tr>
	       				<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Select Distributor</td>
		       			<td align="left">
		       				<select name="agentID" style="font-family:verdana; font-size: 11px">
									<option value="">- Select One -</option>
									<?
											$agents = selectMultiRecords("select distinct a.username, a.userID, a.IDAcountry, a.agentCompany, a.name, a.agentContactPerson from ".TBL_ADMIN_USERS." as a, ".TBL_EXCHANGE_RATES." as e where a.userID = e.rateValue and e.rateFor = 'distributor' and a.parentID > 0 and a.adminType='Agent' and a.agentType='Supper' and a.isCorrespondent != 'N' order by a.agentCompany");
											for ($i=0; $i < count($agents); $i++)
											{
												$toCountry = $agents[$i]["IDAcountry"];
												if(strstr($toCountry,$dCourntry))
												{
												if($agents[$i]["userID"]==$_SESSION["agentID"]){
													
										?>
										
									<option value="<?=$agents[$i]["userID"]; ?>" "selected"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
								<? }else{ ?>
								<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
										<?	}
										}
									}
										?>
								  </select>
								  <script language="JavaScript">SelectOption(document.forms.frmfeecalculator.agentID,"<?=$_SESSION["agentID"]; ?>");</script>
		       			</td>
	       			</tr>
	       
	       <?
	      		}
	       ?>       
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Fee:&nbsp;</td>
			<td align="left">
			  <?
					
			  
				if(empty($fee) && $fee != 0)
				{
			  ?>
					<INPUT maxLength=15 size=21 value=0 name=fee  readonly >
				<?								
				}
				if($fee == 0)
				{
			  ?>
					<INPUT maxLength=15 size=21 value='0' readonly  name=fee >
				<?								
				}								
				else
				{
				?>
					<INPUT maxLength=15 size=21  name=fee readonly value="<? echo number_format($fee,2,'.',',');?>" >
				<?
				}								
				?>			</td>                            
		  </tr>
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Total:&nbsp;</td>
			<td align="left">							
			<INPUT maxLength=15 size=21  name=total readonly  value="<? echo number_format($total,2,'.',',');?>">&nbsp;&nbsp;&nbsp;<input type="button" value="Reset" name="B2" class = "Button" onClick="clearAll()"></td>
			
		  </tr>
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; ">Exchange Rate:&nbsp; </td>
			<td align="left"><INPUT maxLength=15 size=21  name=rate readonly  value="<? echo number_format($rate,2,'.',',');?>"></td>
			
		  </tr>			
		  <tr>
			<td align="right" style="font-family: Verdana;font-size: 8pt;font-weight: normal;font-variant: normal; "> Amount in Destination Currency:&nbsp;</td>
			<td align="left">
			<INPUT maxLength=15 size=21  name=damount class="Login" value="<? echo number_format($damount,2,'.',',');?>">&nbsp;&nbsp;&nbsp;<INPUT type=submit value="Calculate Local Amount" name=SUBMIT2 class="Button" bgcolor="YELLOW"></td>
			
		  </tr>
		  <tr>
			<td colspan="2"><br><br></td>
		  </tr>
			<?
			if($rate != "")
			{
			?>			 

		  <tr>
			<td colspan="2" align="center">
			<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
             
			  <tr>
                 <td align="center"><font color="#333333" size="2"><b>Note: Exchange rates are subject to change after 3:00 PM everyday. <br> Any transaction made after 3:00 PM will have next working day's Exchange rate.</b></font></td>
              </tr>
            </table>
		</td>
	 </tr>
	 <?
	 }
	 ?>
		  		  <tr>
			<td colspan="2"><br></td>
		  </tr>		  
	  </table>
   </td>

</tr>
</table>
</form>	</td>
    <td align="right">	<?
	 include "exchange-rates-agent.php";
	?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>