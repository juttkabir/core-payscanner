<?
	session_start();
	require_once ("../include/config.php");
	dbConnect();

	function debugExit($var)
	{
		echo "<pre>" . print_r( $var, true ) . "</pre>";
		exit();
	}

	function debug($var)
	{
		echo "<pre>" . print_r( $var, true ) . "</pre>";
	}

	$userIP = getIP();
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$preFix = SYSTEM_PRE;

	$msg='New Customer Information';
	$date_time = date('d-m-Y  h:i:s A');
	//$cId = 2872; // Uncomment this line for testing only.

	$cId = "";
	$creatingNewUser = true;

	if ( !empty( $_REQUEST["cId"] ) )
	{
		$cId = $_REQUEST["cId"];
		$creatingNewUser = false;
	}

	if( !empty($cId) )
	{
		$strQuery =  "select * from cm_customer where c_id = '" . $cId . "'";
		$nResult = mysql_query($strQuery) or die("Invalid query:" . mysql_error());
		$dDay = 0;
		$dMonth = 0;
		$dYear = 0;

		if ( mysql_num_rows( $nResult ) > 0 )
		{
			$senderData = mysql_fetch_assoc( $nResult );

			//$
			/*$c_dDate = $_SESSION["c_dDate"] = $senderData["c_date"];
			$date = explode("-",$c_dDate);
			$dDay = $date[2];
			$dMonth = $date[1];
			$dYear = $date[0];
			$_SESSION["dMonth"] = $dMonth;
			$_SESSION["dDay"] = $dDay;
			$_SESSION["dYear"] = $dYear; */

//			$_SESSION["email"] = $senderData["c_email"];
//			$_SESSION["phone"] = $senderData["c_phone"];
//			$_SESSION["mobile"] = $senderData["c_mobile"];
//			$_SESSION["address"] = $senderData["c_address"];
//			$_SESSION["address1"] = $senderData["c_address2"];
//			$_SESSION["country"] = $senderData["c_country"];
//			$_SESSION["state"] = $senderData["c_state"];
//			$_SESSION["city"] = $senderData["c_city"];
//			$_SESSION["zip"] = $senderData["c_zip"];

			//$_SESSION["placeOfBirth"] = $senderData["placeOfBirth"];
			//$_SESSION["question1"] = $senderData["c_p_question1"];
			//$_SESSION["answer1"] = $senderData["c_answer1"];
			//$_SESSION["question2"] = $senderData["c_p_question2"];
			//$_SESSION["answer2"] = $senderData["c_answer2"];
			//$_SESSION["DocumentProvided"] = $senderData["c_info_source"];
			//echo 	strstr($_SESSION["DocumentProvided"],"MSN");

			$loginName = $senderData["username"];
			//$password = ;
			//$confirmPassword = ;
			$title = $senderData["Title"];
			$firstName = $senderData["FirstName"];
			$middleName = $senderData["MiddleName"];
			$lastName = $senderData["LastName"];
			$placeOfBirth = $senderData[""];
			$email = $senderData["c_email"];
			$phone = $senderData["c_phone"];
			$mobile = $senderData["c_mobile"];
			$address = $senderData["c_address"];
			$address1 = $senderData["c_address2"];
			$country = $senderData["c_country"];
			$state = $senderData["c_state"];
			$city = $senderData["c_city"];
			$zip = $senderData["c_zip"];
			$personalQuestion1 = $senderData["c_p_question1"];
			$personalAnswer1 = $senderData["c_answer1"];
			$personalQuestion2 = $senderData["c_p_question2"];
			$personalAnswer2 = $senderData["c_answer2"];
			$senderSourceOfInformation = $senderData["c_info_source"]; // array
			$countryToSendMoney = $senderData["c_benef_country"];

			//$_SESSION["countryToSendMoney"] = $senderData["c_benef_country"];

			$DOB = $senderData["dob"];
			$DOB = explode("-",$DOB);
			$_SESSION["dYear"] = $DOB[0];
			$nMonth = $DOB[1];
			$mMonth = Num2Month($nMonth);
			$dDay = $DOB[2];
			$dYear = $DOB[0];
/*			$dMonth = $senderData[""];
			$dDay = $senderData[""];
			$dYear = $senderData[""]; */
			//$_SESSION["limit1"] = $senderData["limit1"];
			//$_SESSION["Balance"] = $senderData["Balance"];	
			//$_SESSION["limit3"] = $senderData["limit3"];
		}
	}

	if ( !empty($_REQUEST["formSubmit"]) )
	{
		foreach( $_REQUEST as $varName => $value )
		{
			$$varName = $value;
		}

		// Validation goes here.

		$errorsArray = array();

		if ( empty($_REQUEST["loginName"]) )
		{
			$errorsArray[] = "Login name is required.";
		}

		if ( empty($_REQUEST["password"]) )
		{
			$errorsArray[] = "Password is required.";
		}

		if ( empty($_REQUEST["confirmPassword"]) )
		{
			$errorsArray[] = "Confirm password field is required.";
		}

		if ( !empty($_REQUEST["password"]) && !empty($_REQUEST["confirmPassword"])
			&& $_REQUEST["password"] != $_REQUEST["confirmPassword"] )
		{
			$errorsArray[] = "Password and Confirm password fields should match.";
		}

		if ( empty($_REQUEST["firstName"]) )
		{
			$errorsArray[] = "First name is required.";
		}

		if ( empty($_REQUEST["lastName"]) )
		{
			$errorsArray[] = "Last name is required.";
		}

		if ( empty($_REQUEST["dMonth"]) || empty($_REQUEST["dDay"]) || empty($_REQUEST["dYear"]) )
		{
			$errorsArray[] = "Proper date of birth is required.";
		}

		if ( empty($_REQUEST["address"]) )
		{
			$errorsArray[] = "Address is required.";
		}

		if ( empty($_REQUEST["country"]) )
		{
			$errorsArray[] = "Country is required.";
		}
		
/*		if ( empty($_REQUEST["state"]) )
		{
			$errorsArray[] = "State is required.";
		} */
		
		if ( empty($_REQUEST["city"]) )
		{
			$errorsArray[] = "City is required.";
		}
		
		if ( empty($_REQUEST["zip"]) )
		{
			$errorsArray[] = "Zip/Postal code is required.";
		}

		if ( empty($_REQUEST["personalQuestion1"]) || empty($_REQUEST["personalAnswer1"]) )
		{
			$errorsArray[] = "Personal question 1 and its answer is required.";
		}

		if ( empty($_REQUEST["personalQuestion2"]) || empty($_REQUEST["personalAnswer2"]) )
		{
			$errorsArray[] = "Personal question 2 and its answer is required.";
		}

		if ( empty($_REQUEST["countryToSendMoney"]) )
		{
			$errorsArray[] = "Country to transfer money to is required.";
		}

		$strRandomQuery = "SELECT * FROM " . TBL_CM_CUSTOMER . " where username = '$loginName'";
		$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
		$nRows = mysql_fetch_array($nRandomResult);
		if ( count($nRows) > 0 )
		{
			$errorsArray[] = "Sender with the same login name already exists.";
		}

		switch(trim($country))
		{
			case "United States": $ccode = "US";
			break;
			case "Canada": $ccode = "CA";
			break;
			default: 
				$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='". $country ."'");
				$ccode = $countryCode["countryCode"];
			break;
		}

		if(CONFIG_MANUAL_ENABLE_ONLINE == '1')
			$isActive = 'Disable';
		else
			$isActive = 'Active';

		$customerNumber = selectFrom("select MAX(c_id) from ".TBL_CM_CUSTOMER." where c_country='".$_POST["country"]."'");
		$customerCode = $preFix."".$ccode.($customerNumber[0]+1);

		if ( $creatingNewUser )
		{
			do
			{
				$PINCODE = rand(10000, 99999);
				
				$result = mysql_query( "SELECT * FROM cm_customer where SecuriyCode = '" . $PINCODE . "'" );
				if ( mysql_num_rows( $result ) > 0 )
					$pinCodeGenerated = false;
				else
					$pinCodeGenerated = true;
				
			} while( !$pinCodeGenerated );
	
			if ( empty( $errorsArray ) )
			{
	
				$strQuery = "insert into cm_customer( 
												 c_name , Title , FirstName , MiddleName ,
												 LastName , c_pass , c_date , c_email , c_phone , c_mobile , 
												 c_address , c_address2 , c_country , c_state , c_city , c_zip , 
												 c_p_question1 , c_answer1 , c_p_question2 , c_answer2 , c_info_source ,
												 c_benef_country , customerStatus ,  username , last_login , 
												 dateConfrimed , SecuriyCode , 
												 dateAccepted , dob ,placeOfBirth ,  limit1,accessFromIP 
												) values(
												'$customerCode','$title','$firstName','$middleName','$lastName','$password',
												'$date_time','$email','$phone','$mobile','$address','$address1','$country',
												'$state','$city','$zip','$personalQuestion1','$personalAnswer1','$personalQuestion2',
												'$personalAnswer2',	'$senderSourceOfInformation','$countryToSendMoney','".$isActive."',
												'$loginName' ,'NOW()', 'NOW()', $PINCODE , 'NOW()',
												'$dDate' , '$placeOfBirth' ,'2000','$userIP'
												)";	
		
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
				if ( $nResult )
				{
					$successMessage = "Sender Added Successfully.";
				}
			}
		}
		else
		{
			/*$strQuery = "update cm_customer 
							set
								Title = '$title',
								FirstName = '$firstName',
								MiddleName = '$middleName',
								LastName = '$lastName',
								c_name  ='$loginName',											
								c_date  = 'NOW()',
								c_email = '$email',
								c_phone  = '$phone',
								c_mobile = '$mobile',
								c_address = '$address',
								c_address2 = '$address1',
								c_country = '$country',
								c_state = '$state',
								c_city = '$city',
								c_zip  = '$zip',
								c_p_question1 = '$question1',
								c_answer1  = '$answer1',
								c_p_question2 = '$question2',
								c_answer2  = '$answer2',
								c_info_source = '$DocumentProvided',
								c_benef_country  = '$countryToSendMoney',
								username = 	'$loginName',	
								dob = '$dDate'									
							where c_id = $cId";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 	  // */
		}
	//insertInto($strQuery);	
		/*$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;

		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		$subject = "Welcome to $company-Money Transfer Service";
		$message = "
					
					
					Dear $Title $firtName $lastName							
					
					
					Your $company account is activated and you can start using $company 
					Money Transfer service. Please click the link below to access your $company 
					account.							
					
					<a href ='$companyURL'> Login 
					to your $company account</a> 							
					
					By clicking this link you will automatically be sent to the $company 
					Secure Site where you can login to your account.							
					
					
					Your Login Name: $loginName
					
					
					Your PIN code: $PINCODE									
					
					
					Your Customer Number: $customerCode
					
					
					Please use your Customer Number $customerCode when you contact 
					$company customer service department to get quick response.
					
					
					
					Note: For security reason we do not send Password by email or by 
					post.							
												
					
					If you have any question please contact us on 0870 242 7172 										
					
					
					
					Thanks,
					
					
					$company Team
					T: 0870242 7172 (For UK Customers)
					(1) 416-$company-4U (For Canadian Customers)
					W: <a href='$companyURL'> $companyURL</a>
								
				";				
				sendMail($email, $subject, $message, $fromName, $fromEmail); */

	}
?>

<html>
<head>
	<title>Untitled Document</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm)
{
	return true;
	var strErr = "";
	strEmpty = /^\s*$/;
	strDigit = /^\d+$/;
	strEml = /^[A-Za-z0-9-_.]+\@[A-Za-z0-9-_]+.[A-Za-z0-9-_.]+$/;
	strLogin = /^[A-Za-z0-9-_]+$/;

		
	if(theForm.loginName.value == "" || IsAllSpaces(theForm.loginName.value)){
    	alert("Please provide the Login name.");
        theForm.loginName.focus();
        return false;
    }
	if(theForm.password.value == "" || IsAllSpaces(theForm.password.value)){
    	alert("Please provide the Password.");
        theForm.password.focus();
        return false;
    }
	if(theForm.ConfirmPassword.value == "" || IsAllSpaces(theForm.ConfirmPassword.value)){
    	alert("Please provide the Confirm Password.");
        theForm.ConfirmPassword.focus();
        return false;
    }	
	if(theForm.password.value != theForm.ConfirmPassword.value){
    	alert("Password and Confirm Password are not Same.");
        theForm.password.focus();
        return false;
    }	
	if(theForm.firtName.value == "" || IsAllSpaces(theForm.firtName.value)){
    	alert("Please provide the First name.");
        theForm.firtName.focus();
        return false;
    }
	if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
    	alert("Please provide the Last name.");
        theForm.lastName.focus();
        return false;
    }
	
	if(theForm.dMonth.options.selectedIndex == 0){
    	alert("Please select the Date of Birth from the list.");
        theForm.dMonth.focus();
        return false;
    }
	if(theForm.dDay.options.selectedIndex == 0){
    	alert("Please select the Date of Birth from the list.");      
		theForm.dDay.focus();
        return false;
    }	
	if(theForm.dYear.options.selectedIndex == 0){
    	alert("Please select the Date of Birth from the list.");
        theForm.dYear.focus();
        return false;
    }		
	if(theForm.email.value != ""){
			if(!theForm.email.value.match(strEml))
			{
				alert("Invalid email format provided.\n");					
				theForm.email.focus();
				return (false);
			}	
    }	

/*	if(theForm.phone.value == "" || IsAllSpaces(theForm.phone.value)){
    	alert("Please provide the phone number.");
        theForm.phone.focus();
        return false;
    }	*/
	if(theForm.address.value == "" || IsAllSpaces(theForm.address.value)){
    	alert("Please provide the address line 1.");
        theForm.address.focus();
        return false;
    }
	if(theForm.country.options.selectedIndex == 0){
    	alert("Please select the country from the list.");
        theForm.country.focus();
        return false;
    }
	//if(theForm.city.options.selectedIndex == 0 ){
		if(theForm.city.value == "" || IsAllSpaces(theForm.city.value))
		{
			
			alert("Please Provide the City Name.");
			theForm.city.focus();
			return false;
		}
		/*else
		{
			alert("Please select the city from the list.");
			theForm.city.focus();
			return false;		
		}*/
   // }

		
	if(theForm.question1.options.selectedIndex == 0){
    	alert("Please select the Question 1 from the list.");
        theForm.question1.focus();
        return false;
    }
	if(theForm.answer1.value == "" || IsAllSpaces(theForm.answer1.value)){
    	alert("Please provide the answer of Question 1.");
        theForm.answer1.focus();
        return false;
    }		
	if(theForm.question2.options.selectedIndex == 0){
    	alert("Please select the Question 2 from the list.");
        theForm.question2.focus();
        return false;
    }

	if(theForm.answer2.value == "" || IsAllSpaces(theForm.answer2.value)){
    	alert("Please provide the answer of Question 2.");
        theForm.answer2.focus();
        return false;
    }		
	if(theForm.countryToSendMoney.options.selectedIndex == 0){
    	alert("Please select the Beneficiary Country from the list.");
        theForm.countryToSendMoney.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
</head>
<body onLoad="frm.loginName.focus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">

  <form action="new_registeration.php" name="frm" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="cId" value="<?=$cId; ?>">
  <tr>
    <td align="center" valign="top">
		<table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
          <?
if($cId != "")
	{
?>
          <tr height="30"> 
            <td colspan="2" bgcolor="#6699cc" align="left"> &nbsp;<strong><font color="#FFFFFF" size="2">
              <? echo $msg;?>
              </font></strong></td>
          </tr>
          <?
}
else
{
?>
          <?
}
?>
          <tr> 
            <td colspan="2" bgcolor="#000000"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>
                    <? echo $msg;?>
                    </strong></font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? if ( !empty($successMessage) ){ ?>
          <tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#FFFFCC"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"> <font size="5" color="GREEN"><b><i>!</i></b></font></td>
                  <td>
                    <? echo "<font color='GREEN'><b>".$successMessage."</b><br><br></font>"; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>

		  <? if ( !empty($errorsArray) ){ ?>
          <tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#FFFFCC"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"> <font size="5" color="#990000"><b><i>!</i></b></font></td>
                  <td>
					<font color='#990000'><b>
						<?
							foreach ($errorsArray as $error)
								echo $error . "<br />";
						?></b><br>
					</font>
						  
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Login* 
              </strong></font></td>
            <td width="344" align="left">
              <input type="text" name="loginName" value="<?=$loginName?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Password*</strong></font></td>
            <td align="left">
              <input type="password" name="password" value="<?=$password?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><b>Confirm 
              Password *</b></font></td>
            <td align="left">
              <input name="confirmPassword" type="password" id="confirmPassword" value="<?=$confirmPassword?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Title</strong></font></td>
            <td width="344" align="left"> 
              <select name="title" id="title">
                <option value="Mr." <?=$title=="Mr." ? "selected='selected'" : "";?> >Mr.</option>
                <option value="Miss." <?=$title=="Miss." ? "selected='selected'" : "";?> >Miss.</option>
                <option value="Dr." <?=$title=="Dr." ? "selected='selected'" : "";?> >Dr.</option>
                <option value="Mrs." <?=$title=="Mrs." ? "selected='selected'" : "";?> >Mrs.</option>
                <option value="Other" <?=$title=="Other" ? "selected='selected'" : "";?> >Other</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>First Name*</strong></font></td>
            <td width="344" align="left">
              <input name="firstName" type="text" id="firstName" value="<?=stripslashes($firstName); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Middle 
              Name</strong></font></td>
            <td width="344" align="left">
              <input type="text" name="middleName" value="<?=stripslashes($middleName); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Last Name*</strong></font></td>
            <td width="344" align="left">
              <input type="text" name="lastName" value="<?=stripslashes($lastName); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><b>Date of Birth* </b></font></td>
            <td align="left">
				<?
					$monthsArray = array(	"01" => "Jan", 
											"02" => "Feb", 
											"03" => "Mar", 
											"04" => "Apr", 
											"05" => "May", 
											"06" => "Jun", 
											"07" => "Jul", 
											"08" => "Aug", 
											"09" => "Sep", 
											"10" => "Oct", 
											"11" => "Nov", 
											"12" => "Dec" );
				?>
              <SELECT name="dMonth" size="1" style="font-family:verdana; font-size: 11px">
                 <OPTION value="">Month</OPTION>
				 <?
				 	foreach( $monthsArray as $key => $month )
					{
						$selected = "";
						if ( $key == $dMonth )
							$selected = "selected='selected'";
						echo "<option value='" . $key . "' " . $selected . " >" . $month . "</option>";
					}
				 ?>
              </SELECT>
              <SELECT name="dDay" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Day</OPTION>
                <? 
					  	for ($Day=1;$Day<32;$Day++)
						{
							$selected = "";
							if ($Day<10)
								$Day="0".$Day;
							if($Day == $dDay)
								$selected = "selected='selected'";
							echo "<option value='" . $Day . "' " . $selected . " >" . $Day . "</option>";
						}
					  ?>
              </select>
              <SELECT name="dYear" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="" selected>Year</OPTION>
                <?
			$dYears=date("Y");
			for ($Year=1920; $Year<=($dYears);$Year++)
			{
				if($Year == $_SESSION["dYear"])
				{
				$tempVar = $_SESSION["dYear"];
				echo "<option value=\"$tempVar\" selected>$tempVar</option>\n";
				}
				else
				echo "<option value=\"$Year\">$Year</option>\n";
			}
		  ?>
              </SELECT>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Place Of Birth</strong></font></td>
            <td width="344" align="left">
              <input type="text" name="placeOfBirth" value="<?=$_SESSION["placeOfBirth"]; ?>" size="35" >
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>Email</strong></font></td>
            <td align="left">
              <input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>Home 
              Phone Number*</strong></font></td>
            <td align="left">
              <input type="text" name="phone" value="<?=stripslashes($_SESSION["phone"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>Mobile 
              Number </strong></font></td>
            <td align="left">
              <input type="text" name="mobile" value="<?=stripslashes($_SESSION["mobile"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>Address*</strong></font></td>
            <td align="left">
              <input type="text" name="address" value="<?=stripslashes($_SESSION["address"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>Address 
              1 </strong></font></td>
            <td align="left">
              <input type="text" name="address1" value="<?=stripslashes($_SESSION["address1"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td align="left"><font color="#005b90"><b>Country*</b></font></td>
            <td align="left"> 
			<?
			
			if(CONFIG_ENABLE_ORIGIN == "1"){
         		$countryTypes = " and countryType like '%origin%' ";
        		$agentCountry = " ";
        	}else{
        		$countryTypes = " ";
        		$agentCountry = " and countryName ='United Kingdom' ";
        	}
        if(CONFIG_COUNTRY_SERVICES_ENABLED){
          	$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
					}else{
						$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
				}
        	
				
			?>
              <SELECT name="country" style="font-family:verdana; font-size: 11px" onChange="/* document.forms[0].action = 'registration.php'; document.forms[0].submit(); */">
                <OPTION value="">- Select Country-</OPTION>
                <?
				
				for ($i=0; $i < count($countires); $i++)
				{
						if($countires[$i]["countryName"] == $_SESSION["country"])
						{
							$tempVar = $_SESSION["country"];
							?>
							<OPTION value="<?=$tempVar; ?>" selected>
								<?=$_SESSION["country"]; ?>
							</OPTION>
							<?
						}else{				
						?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>">
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
              }
				
			}//end of for looop
				?>
              </SELECT>
			  
            </td>
          </tr>
          <? if ($_SESSION["country"] == "United States"){ ?>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong>State</strong></font></td>
            <td align="left"> 
              <input type="text" name="state" value="<?=stripslashes($_SESSION["state"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <? }?>
          <? /*if ( $_SESSION["country"] != "Canada" && $_SESSION["country"] != "United Kingdom"){ */?>
       <!--  <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><b>City*</b></font></td>
            <td align="left">
              <SELECT name="city" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select City-</OPTION>
                <?
				/*$country = $_SESSION["country"];
				$strQuery = "SELECT distinct city    FROM  cities where country= '$country' order by city";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($senderData = mysql_fetch_array($nResult))
				{
					$strCity = $senderData["city"];															
					
						if($strCity == $_SESSION["city"])
							echo "<option selected>".$strCity."</option>";
							
					else
						echo "<option>".$strCity."</option>";	
				}*/
				
				?>
              </SELECT>
            </td>
          </tr>-->
          <? /*} else {*/
		?>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><b>City*: 
            </td>
            <td align="left">
              <input type="text" name="city" value="<?=stripslashes($_SESSION["city"]); ?>" size="35" maxlength="50">
            </td>
          </tr>
		  <?
		//} ?>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><b> 
              <? if ($_SESSION["country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code*</b></font></td>
            <td align="left">
              <input type="text" name="zip" value="<?=stripslashes($_SESSION["zip"]); ?>" size="35" maxlength="20">
            </td>
          </tr>
          
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Personal 
              Question 1 </strong></font></td>
            <td align="left"> 
              <SELECT name="personalQuestion1" id="personalQuestion1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Secret Question 1-</OPTION>
                <?
				$country = $_SESSION["country"];
				$strQuery = "SELECT * FROM  cm_questions";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($senderData = mysql_fetch_array($nResult))
				{
					$Question = $senderData["question"];	
					$qID = $senderData["qID"];														
					if($qID <=2)
					{
						if($qID == $_SESSION["question1"])
							echo "<option  value=$question1 selected >".$Question."</option>";
							
					else
						echo "<option value=$qID>".$Question."</option>";	
					}
				}
				
				?>
              </SELECT>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong>Answer 
              1*</strong></font></td>
            <td align="left">
              <input name="personalAnswer1" type="text" id="personalAnswer1" value="<?=stripslashes($_SESSION["answer1"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td align="left"><font color="#005b90"><strong>Personal Question 2 
              </strong></font></td>
            <td align="left"> 
              <SELECT name="personalQuestion2" id="personalQuestion2" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Secret Question 1-</OPTION>
                <?
				$country = $_SESSION["country"];
				$strQuery = "SELECT * FROM  cm_questions";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
				while($senderData = mysql_fetch_array($nResult))
				{
					$Question = $senderData["question"];	
					$qID = $senderData["qID"];														
					if($qID >2)
					{
						if($qID == $_SESSION["question2"])
							echo "<option  value=$question2 selected >".$Question."</option>";
							
					else
						echo "<option value=$qID>".$Question."</option>";	
					}
				}
				
				?>
              </SELECT>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td align="left"><font color="#005b90"><strong>Answer 2*</strong></font></td>
            <td align="left">
              <input name="personalAnswer2" type="text" id="personalAnswer2" value="<?=stripslashes($_SESSION["answer2"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left" valign="top"><font color="#005b90"><strong> 
              Where did you heard about <? echo $company;?> </strong></font></td>
            <td align="left">
              <p> 
                <input name="senderSourceOfInformation[]" type="checkbox" id="senderSourceOfInformation[]" value="Google" <? echo (strstr($_SESSION["DocumentProvided"],"Google") ? "checked"  : "")?>>
                Google<br>
                <input name="senderSourceOfInformation[]" type="checkbox" id="senderSourceOfInformation[]" value="Yahoo" <? echo (strstr($_SESSION["DocumentProvided"],"Yahoo") ? "checked"  : "")?>>
                Yahoo<br>
                <input name="senderSourceOfInformation[]" type="checkbox" id="senderSourceOfInformation[]" value="MSN" <? echo (strstr($_SESSION["DocumentProvided"],"MSN") ? "checked"  : "")?>>
                MSN<br>
                <input name="senderSourceOfInformation[]" type="checkbox" id="senderSourceOfInformation[]" value="TV Ad" <? echo (strstr($_SESSION["DocumentProvided"],"TV Ad") ? "checked"  : "")?>>
                TV Ad<br>
                <input name="senderSourceOfInformation[]" type="checkbox" id="DocumentProvided[3]" value="Newspaper" <? echo (strstr($_SESSION["DocumentProvided"],"Newspaper") ? "checked"  : "")?>>
                Newspaper<br>
                <input name="senderSourceOfInformation[]" type="checkbox" id="DocumentProvided[3]" value="Magazine" <? echo (strstr($_SESSION["DocumentProvided"],"Magazine") ? "checked"  : "")?>>
                Magazine<br>
              </p>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="173" align="left"><font color="#005b90"><strong> Which country you want to transfer money *</strong></font></td>
            <td align="left">
			<?
	                     
      if(CONFIG_ENABLE_ORIGIN == "1"){
         		$countryTypes = " and countryType like '%destination%' ";
          	}else{
        		$countryTypes = " ";
         	}
        if(CONFIG_COUNTRY_SERVICES_ENABLED){
          	$countires2 = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName");
					}else{
						$countires2 = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
				}
        	
			//$strQuery="select distinct country from cities where 1 $countryTypes order by country";
			//echo $strQuery;
				//$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			?>
              <SELECT name="countryToSendMoney" style="font-family:verdana; font-size: 11px" >
                <OPTION value="">- Select Country-</OPTION>
                <?
						
				for ($i=0; $i < count($countires2); $i++)
				{
					$strCountry = $countires2[$i]["countryName"];															
					if($strCountry == $_SESSION["countryToSendMoney"])
						echo "<option selected>".$strCountry."</option>";			
					else
					echo "<option>".$strCountry."</option>";	
				}
				
				?>
              </SELECT>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center">&nbsp; </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> 
				<input type="hidden" name="formSubmit" value="1" />
              <input type="submit" value="Register Me" />
              &nbsp;&nbsp; </td>
          </tr>
        </table>
	</td>
	<?
		if( !empty($cId) )
		{
			echo "<td align='right' valign='top'>";
			include "exchange-rates-agent.php";
			echo "</td>";
		}
	?>
  </tr>
</form>
</table>
</body>
</html>