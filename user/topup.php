<?
session_start();
include ("../include/config.php");
include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;

/*****************
* [by JAMSHED]
* This variable is used to secure following things:
* Account Name , Account Number , Sort Code
* 'True' means secure the data
* 'False' means no security
*****************/
$secureData = True;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0050)http://www.imts.Samba Money Transfer.co.uk/imremit/topup.php -->
<HTML><HEAD>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<SCRIPT language=javascript src="topup_files/functions.js"></SCRIPT>
<LINK href="topup_files/interface.css" type=text/css rel=stylesheet>
<SCRIPT language=javascript>
</SCRIPT>

<META content="MSHTML 6.00.2900.2180" name=GENERATOR></HEAD>
<BODY>
<p>&nbsp;</p>
<p><B><FONT color=orange>How to Top up your eWallet: </FONT></B><BR>
  <BR>
</p>
<LI>By Online/Phone Banking: You can transfer money to <? echo $company; ?>  Transfer bank account 
using your online or phone banking. 
<LI>By Cash Deposit: You can deposit the cash into <? echo $company; ?>  Transfer bank account. 
Please note cash handling charges apply for any payment more then �500 which is 
.50p per �100. 
<LI>By Cheque: You can post your cheque to <? echo $company; ?>  Transfer to top up your account. 
  <P><B>Special instruction: </B>Please use <U>"Your reference number"</U> as payment 
    reference which you can see on top right on the screen on your ewallet.</P>
<P><BR>
    <B><FONT color=blue size=2 font><? echo $company; ?>  Transfer Bank Account for UK customers </FONT></B> 
  </P>
<LI><FONT size=2><B>Barclays Bank </B></FONT><BR><BR><B>Account Name: 
</B><? echo ($secureData ? "Company Account Name" : $company . " Transfer Ltd"); ?><BR><B>Account Number:</B> <? echo ($secureData ? "Company Account Number" : "90002321"); ?> <BR><B>Sort Code: 
</B><? echo ($secureData ? "Company Sort Code" : "20-24-64"); ?> <BR><BR><BR>
  <B>Please note: </B><? echo $company; ?>  Transfer recieved a payment from Barclays bank account 
  next working day. Payment from other bank account can take upto 3 working days. 
  <BR><Br>
<LI><FONT size=2><B>Royal Bank of Scotland </B></FONT><BR><BR><B>Account 
Name:</B> <? echo ($secureData ? "Company Account Name" : $company . " Transfer Ltd"); ?> <BR><B>Account Number:</B> <? echo ($secureData ? "Company Account Number" : "10174255"); ?> <BR>
  <B>Sort Code: </B><? echo ($secureData ? "Company Sort Code" : "15-20-25"); ?> <BR>
  <BR><B>Please note: </B><? echo $company; ?>  Transfer recieved a payment from 
Royal Bank of Scotland account next working day. Payment from other bank account 
can take upto 3 working days. </LI></BODY></HTML>
