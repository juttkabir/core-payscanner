<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$transID = (int) $_GET["transID"];

//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;


if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
}
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? echo $systemCode;?> </font></td>
                <td width="200" height="20"><? echo $contentTrans["refNumberIM"]?>
                </td>
                <td width="100" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="150" height="20"><? 
				 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
				{
					if($authoriseDate != '0000-00-00 00:00:00')
						echo date("F j, Y", strtotime("$authoriseDate"));				
				}
				?></td>
              </tr>				  
            </table>
            </fieldset></td>
          </tr>
		  
          <tr>
            <td><fieldset>
              <legend class="style2">Customer Details </legend>
              <table width="650" border="0">
                <? 
		
			$queryCust = "select *  from ".TBL_CM_CUSTOMER." where  c_id  ='" . $contentTrans['customerID'] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["c_address"] . " " . $customerContent["c_address2"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["c_zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["c_country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["c_phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["c_email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Beneficiary Name</font></td>
                        <td height="20" colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
                        <td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}

			  if($contentTrans["transType"] == "Bank Transfer") { ?>
						<tr>
							<td colspan="4" class="style2">Beneficiary Bank Details </td>
						</tr>
			<?			
			  		// Code to display IBAN number.
					//$queryBen = "select Country  from ".TBL_CM_BENEFICIARY." where benID ='" . $_REQUEST["benID"] . "'";
					//$benificiaryContent = selectFrom($queryBen);

					$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
					// In case of European countries for now transfer, it is required to bypass the checks regarding bank detail fields.
					// It is because they use only IBAN field.
					if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European" && CONFIG_IBAN_OR_BANK_TRANSFER!="1")
					{
						// In case this clause is true, only IBAN number will be displayed.
						
						$sql = "select IBAN from cm_bankdetails where benID='".$benificiaryContent["benID"]."'";
						$resultForIBAN = selectFrom($sql);
						?>
							<tr>
								<td width="150" height="20" align="right"><font color="#005b90">IBAN</font></td>
								<td><?=$resultForIBAN["IBAN"];?></td>
							</tr>
						<?
					} // Code for IBAN number check ends here.
					else
					{ 
						$benIbanOrBankDetailsQ = "select * from ". TBL_CM_BANK_DETAILS ." where benID='".$contentTrans["benID"]."'";
						if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
							$benIbanOrBankDetailsQ = "select * from " . TBL_BANK_DETAILS . " where transID='".$contentTrans["transID"]."'";
						}
						$benBankDetails = selectFrom($benIbanOrBankDetailsQ);
						if(!empty($benBankDetails["IBAN"])){
						// Display Bank Details of Beneficiary only in case the above clause is not true and 
						// this clause is executed.
						?>
							<tr>
								<td width="150" height="20" align="right"><font color="#005b90">IBAN</font></td>
								<td><?=$benBankDetails["IBAN"];?></td>
							</tr>
						<? }else {?>
						  <tr>
							<td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
							<td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
							<td width="100" align="right"><font color="#005b90">Acc Number</font></td>
							<td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
						  </tr>
						  <tr>
							<td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
							<td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
							<td width="100" align="right"><font color="#005b90">
							   <?
													if(CONFIG_CPF_ENABLED == '1')
													{
														echo  "CPF Number*";
													}
													?>
											  &nbsp; </font></td>
															<td width="200"><?
													if(CONFIG_CPF_ENABLED == '1')
													{
														echo $benBankDetails["ABACPF"];
														 
													}
													?></td>
						  </tr>
						  <tr>
							<td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
							<td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?> </td>
							<?
								if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
								{
									?>
										<td width="150" align="right"><font color="#005b90">Account Type</font></td>
										<td width="200"><?=$benBankDetails["accountType"]?> </td>
									<? 
								}
								else
								{
									?>	                        
										<td width="100" height="20"  align="right"><font color="#005b90">&nbsp;</font></td>
										<td width="200" height="20" align="left">&nbsp;</td>
									<?
								}
							?>
						  </tr>
						<?
					}
					?>
						   <tr>
                        
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
	  			}
			}

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">&nbsp;
                          </font></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Compnay </font> </td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" height="20" align="right"><font color="#005b90">City</font></td>
                        <td width="200" height="20"><?  echo $senderAgentContent["cp_city"]; ?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_country"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo $contentTrans["transAmount"]?>                </td>
              </tr>
			  <tr>
			    <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$preFix Fee");}?></font></td>
			    <td height="20"><? echo $contentTrans["IMFee"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200" height="20"><? echo $contentTrans["localAmount"]?>    in  <? echo $contentTrans["currencyTo"]?>            </td>
			  </tr>
              <tr>
                <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" height="20"><? echo $contentTrans["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font>&nbsp;</td>
                                <td width="200">&nbsp;
                </td>
              </tr>
              <tr>
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
		  
  <? //Bank Charges echo $contentTrans["bankCharges"]
  if($contentTrans["moneyPaid"] == 'By Credit Card' || $contentTrans["moneyPaid"] == 'By Debit Card')
  {
  $benCreditCardDetail = selectFrom("select * from  cm_cust_credit_card where customerID  ='".$contentTrans["customerID"]."'");
  ?>
					  
					  <td colspan="4"><fieldset>
                    <legend class="style2">Credit/Debit Card Details </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
                        <td width="196" height="20" align="left">
						<? echo $benCreditCardDetail["cardName"];?>
						</td>
                        <td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
                        <td width="183" height="20" align="left">
						<? 
						$credCardNo = "************". substr($benCreditCardDetail["cardNo"],12,4);
						echo $credCardNo;?>
						</td>
                      </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
                        <td width="196" height="20" align="left">
						<? 
						echo $ExpirtDagte =  $benCreditCardDetail["expiryDate"];
						//echo date("F j, Y", strtotime("$ExpirtDagte"));
						?>
						</td>
                        <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $benCreditCardDetail["cardCVV"];?>
						</td>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>		
<?
}
?>					  			  
		  
        </table></td>
    </tr>

</table>
</body>
</html>
