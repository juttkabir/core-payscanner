<?
	// Session Handling
	session_start();
	$date_time = date('d-m-Y  h:i:s A');
	// Including files
	include ("../include/config.php");
	include ("security.php");
	$userId = $_SESSION["c_id"];

	switch(trim($_POST["Country"]))
	{
		case "United States":
			$ccode = "US";
		break;
		case "Canada": 
			$ccode = "CA";
		break;
		default:
			$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
			$ccode = $countryCode["countryCode"];
		break;
	}

//debug( $_POST, true );

	//if ($_POST["benID"] == "")
	//{
		session_register("Title");
		session_register("firstName");
		session_register("lastName");
		session_register("middleName");
		session_register("IDType");
		session_register("IDNumber");
		session_register("Address");
		session_register("Address1");
		session_register("Address2");
		session_register("Country");
		session_register("City");
		session_register("Zip");
		session_register("State");
		session_register("Phone");
		session_register("Mobile");
		session_register("benEmail");
		session_register("transType");
		
		session_register("bankName");
		session_register("branchCode");
		session_register("branchName");
		session_register("branchAddress");
		session_register("swiftCode");
		session_register("accNo");
		session_register("accountType");
		session_register("ABACPF");
		session_register("IBAN");
		session_register("serviceType");
		session_register("beneficiaryName");
	
		$_SESSION["bankName"] = $_POST["bankName"];
		$_SESSION["branchCode"] = $_POST["branchCode"];
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
		$_SESSION["swiftCode"] = $_POST["swiftCode"];
		$_SESSION["accNo"] = $_POST["accNo"];
		$_SESSION["ABACPF"] = $_POST["ABACPF"];
		$_SESSION["IBAN"] = $_POST["IBAN"];

		$_SESSION["Title"] = $_POST["Title"];
		$_SESSION["firstName"] = $_POST["firstName"];
		$_SESSION["lastName"] = $_POST["lastName"];
		$_SESSION["middleName"] = $_POST["middleName"];
		$_SESSION["IDType"] = $_POST["IDType"];
		$_SESSION["IDNumber"] = $_POST["IDNumber"];
		$_SESSION["Address"] = $_POST["Address"];
		$_SESSION["Address1"] = $_POST["Address1"];
		$_SESSION["Address2"] = $_POST["Address2"];

		$_SESSION["proveAddress"] = $_POST["proveAddress"];
		$_SESSION["proveAddressType"] = $_POST["proveAddressType"];

		$_SESSION["Country"] = $_POST["Country"];
		$_SESSION["City"] = $_POST["City"];
		$_SESSION["Zip"] = $_POST["Zip"];
		$_SESSION["State"] = $_POST["State"];
		$_SESSION["Phone"] = $_POST["Phone"];
		$_SESSION["Mobile"] = $_POST["Mobile"];
		$_SESSION["benEmail"] = $_POST["benEmail"];
		$_SESSION["customerID"] = $_POST["customerID"];
		
		$_SESSION["bankName"] = $_POST["bankName"];
		$_SESSION["branchCode"] = $_POST["branchCode"];
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
		$_SESSION["swiftCode"] = $_POST["swiftCode"];
		$_SESSION["accNo"] = $_POST["accNo"];
		$_SESSION["bankCode"] = $_POST["bankCode"];
		$_SESSION["branchName"] = $_POST["branchName"];	
		$_SESSION["branchCity"] = $_POST["branchCity"];	
		$_SESSION["ABACPF"] = $_POST["ABACPF"];
		$_SESSION["accountType"] = $_POST["accountType"];
		$_SESSION["IBAN"] = $_POST["IBAN"];	
		$_SESSION["ibanorbank"] = $_POST["ibanbank"];	
		$_SESSION["serviceType"] = $_POST["serviceType"];
		$_SESSION["beneficiaryName"] = $_POST["beneficiaryName"];
		
		// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	
		if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
		{
			$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
		}
		
		// End of code against #3299: MasterPayex - Multiple ID Types
		
		
		if($_GET["from"] == "popUp" || $_POST["from"] == "popUp"){
//			$backURL = "add-benificiary2.php?msg=Y&from=" . $_GET["from"];
			$backURL = "add-beneficiary.php?msg=Y&from=" . $_GET["from"];
		}
		else{
			$backURL = "add-beneficiary.php?msg=Y&from=" . $_GET["from"]."&benIDs=".$_POST["benID"];
		}
	
	//}
	/*else
	{
		$backURL = "update-beneficiary.php?benID=$_POST[benID]&msg=Y";
	}*/

	if (trim($_POST["firstName"]) == "")
	{
		insertError(BE1);
		redirect($backURL);
	}
	if (trim($_POST["lastName"]) == "")
	{
		insertError(BE2);
		redirect($backURL);
	}

	if ($_POST["IDNumber"] != "" && $_POST["Country"] == "Pakistan"  && $_POST["bankName"] == "United Bank Limited")
	{
		$_SESSION["Country"]=$_POST["Country"];
		$_SESSION["bankName"]=$_POST["bankName"];
		$IDNumberArrey = explode("-",$_POST["IDNumber"]);	
		for($i=0;$i<count($IDNumberArrey);$i++)
		{
			$totalChar .= $IDNumberArrey[$i];
		}
		$totalChar = explode(" ",$totalChar);
		for($i=0;$i<count($totalChar);$i++)
		{
			$nCount .= $totalChar[$i];
		}
		$nCount = strlen($nCount);
		if($nCount != 13)
		{	
			insertError("ID number should be 13 character ");
			redirect($backURL);
		}
	}
	
	if (trim($_POST["Address"]) == "")
	{
		insertError(BE4);
		redirect($backURL);
	}
	
	if (trim($_POST["Country"]) == "")
	{
		insertError(BE5);
		redirect($backURL);
	}
	
	if (trim($_POST["City"]) == "")
	{
		insertError(BE6);
		redirect($backURL);
	}

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

	if (trim($_POST["Phone"]) == "")
	{
		insertError(BE7);
		redirect($backURL);
	}

	/*
	if(!email_check(trim($_POST["benEmail"]))){
		insertError(BE8);
		redirect($backURL);
	}
	*/

	// Validity cheks for Bank Details of Beneficiary.

	if(trim($_POST["transType"]) == "Bank Transfer")
	{
		if(trim($_POST["bankName"]) == "")
		{
			insertError("Please enter the Bank Name");	
			redirect($backURL);
		}
	
		if(trim($_POST["accNo"]) == "")
		{
			insertError("Please enter your account number");	
			redirect($backURL);
		}
	
		if(trim($_POST["accNo"]) != "" && $_POST["Country"] == "Pakistan"  && $_POST["bankName"] == "United Bank Limited")
		{		
	
			/*$totalChar = explode(" ",$_POST["accNo"]);
	
			for($i=0;$i<count($totalChar);$i++)
			{
				 $nCount .= $totalChar[$i];			
			}
			 $nCount;*/
			$nCount = strlen($_POST["accNo"]);		
			if($nCount != 8)
			{	
				insertError("Please enter 8 digit account number");	
				redirect($backURL);
			}		
		}

		/*if($_POST["bankName"] == "United Bank Limited")	
		{
			if(trim($_POST["bankCode"]) != "" && $_POST["Country"] == "Pakistan"  && $_POST["bankName"] == "United Bank Limited")
			{
				if(trim($_POST["bankCode"]) == "")
				{
							insertError("Please enter Bank Code");	
							redirect($backURL);
				}
				$nCount = strlen($_POST["bankCode"]);
					
					if($nCount != 24)
					{	
						insertError("Please enter 24 digit Bank Code");	
						redirect($backURL);
					}
			}
		}*/
	
		if(trim($_POST["branchName"]) == "")
		{
			insertError("Please enter Branch Name");	
			redirect($backURL);
		}
	
		if($_POST["bankName"] == "United Bank Limited")	
		{
			if(trim($_POST["branchCity"]) == "")
			{
				insertError("Please enter Branch City Name");	
				redirect($backURL);
			}	
		}
	
		/*if(trim($_POST["branchCode"]) == "")
		{
			insertError("Please enter Branch Code");	
			redirect($backURL);
		}*/
	
		/*if(trim($_POST["branchAddress"]) == "")
		{
			insertError("Please enter Branch Address");	
			redirect($backURL);
		}*/
	
		/*if(trim($_POST["swiftCode"]) == "")
		{
			insertError("");	
			redirect($backURL);
		}*/
	
		if(trim($_POST["ABACPF"]) == "" &&  $_POST["Country"] == "United States")
		{
			insertError("Please enter ABA numer");	
			redirect($backURL);
		}
		
		if(trim($_POST["ABACPF"]) == "" &&  $_POST["Country"] == "Brazil")
		{
			insertError("Please enter CPF number");	
			redirect($backURL);
		}
	
	/*	if(ibanCountries( $_POST["Country"]) && trim($_POST["IBAN"]) == "")
		{
			insertError("Please enter IBAN numer");	
			redirect($backURL);
		}*/
	}
	else
	{
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["branchName"] = "";
		$_SESSION["bankCode"] = "";
		$_SESSION["branchCity"] = "";
		$_SESSION["swiftCode"] 	= "";
		$_SESSION["accNo"] = "";
		$_SESSION["ABACPF"] = "";
		$_SESSION["IBAN"] = "";
		$_SESSION["transType"] = "";
		$_SESSION["accountType"] = "";
		 if(CONFIG_IBAN_ON_QUICK_BEN == "1" || CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
				$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchCode"] = $_POST["branchCode"];
				$_SESSION["branchAddress"] = $_POST["branchAddress"];
				$_SESSION["swiftCode"] = $_POST["swiftCode"];
				$_SESSION["accNo"] = $_POST["accNo"];
				$_SESSION["bankCode"] = $_POST["bankCode"];
				$_SESSION["branchName"] = $_POST["branchName"];	
				$_SESSION["branchCity"] = $_POST["branchCity"];	
				$_SESSION["ABACPF"] = $_POST["ABACPF"];
				$_SESSION["accountType"] = $_POST["accountType"];
				$_SESSION["IBAN"] = $_POST["IBAN"];
		}
	}
if ($_POST["benID"] == ""){

$dDate = date("y-m-d");
if(trim($_POST["transType"]) != "Bank Transfer")
	$_POST["transType"] = "";
	$Querry_Sqls = "INSERT INTO cm_beneficiary (
													 Title,
													 firstName, 
													 middleName, 
													 lastName, 
													 Address, 
													 Address1,
													 Address2, 
													 Country, 
													 City, 
													 Zip, 
													 State, 
													 Phone,
													 Mobile, 
													 email, 
													 IDType, 
													 NICNumber, 
													 customerID,
													 created,
													 transType,
													 proveAddress, 
													 proveAddType,
													 IBAN,
													 IbanOrBank
													 ) 
											   VALUES 
													(
													'".$_POST["Title"]."', 
													'".checkValues($_POST["firstName"])."', 
													'".checkValues($_POST["middleName"])."', 
													'".checkValues($_POST["lastName"])."', 
													'".checkValues($_POST["Address"])."', 
													'".checkValues($_POST["Address1"])."', 
													'".checkValues($_POST["Address2"])."', 
													'".$_POST["Country"]."', 
													'".checkValues($_POST["City"])."', 
													'".checkValues($_POST["Zip"])."', 
													'".checkValues($_POST["State"])."', 
													'".$phone."', 
													'".$mobile."', 
													'".checkValues($_POST["benEmail"])."',
													'".$_POST["IDType"]."', 
													'".checkValues($_POST["IDNumber"])."', 
													$userId ,
													'$dDate','".checkValues($_POST["transType"])."', 
													'".$_POST["proveAddress"]."',
													'".$_POST["proveAddressType"]."',
													'".$_POST["IBAN"]."',
													'".$_POST["ibanbank"]."'
													)";

	insertInto($Querry_Sqls);
	//$benID = @mysql_insert_id();	// This technique works only in case the autonumber field is INT, doesn't work for BigINT.
	$result = selectFrom("select LAST_INSERT_ID() as lastId");
	$benID = $result["lastId"];
	unset($result);

		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , benID   , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', $benID, $entryDate,'INSERTION',$userId)");		
		}	
		
		//if(trim($_POST["transType"]) == "Bank Transfer")
		if(!empty($benID))
		{
			//$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
			if(CONFIG_IBAN_ON_QUICK_BEN == "1" && CONFIG_IBAN_OR_BANK_TRANSFER!="1"){
				$strQuery = "delete FROM cm_bankdetails where benID = '".$benID."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
				$sqlBank = "insert into cm_bankdetails
							( 
							benID, bankName, BankCode ,BranchName,
							accNo, branchCode, branchAddress,branchCity ,
							ABACPF , IBAN,swiftCode 
							)
							Values
							(
							'".$benID."',  '".$_POST["bankName"]."','".$_POST["bankCode"]."','".$_POST["branchName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."','".$_POST["branchCity"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."'
							)
							";			
				insertInto($sqlBank);
			 }
			 elseif(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
					if($_POST['ibanbank'] = "bank"){
						$sqlBank = "insert into cm_bankdetails
									( 
									benID, bankName, BankCode ,BranchName,
									accNo, branchCode, branchAddress,BranchCity ,
									ABACPF , swiftCode, accountType
									)
									Values
									(
									'".$benID."',  '".$_POST["bankName"]."','".$_POST["bankCode"]."','".$_POST["branchName"]."',
									'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."','".$_POST["branchCity"]."',
									'".$_POST["ABACPF"]."',	'".$_POST["swiftCode"]."','".$_POST["accountType"]."'
									)
									";

					}
					else{
						$sqlBank = "insert into cm_bankdetails(benID, IBAN)Values('".$benID."','".$_POST["IBAN"]."')";
						$updateBenIban = "update cm_beneficiary 
											set 
												IBAN='" . $_POST["IBAN"] . "', 
												IbanOrBank='" . $_POST["ibanbank"] . "' 
											where 
												benID='" .$benID. "'";
						
					}
				}
				update($updateBenIban);
				insertInto($sqlBank);
			 }

			// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
			if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
			{
				$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
				foreach( $idTypeValuesData as $idTypeValues )
				{
					if ( !empty( $idTypeValues["id_type_id"] )
						 && !empty( $idTypeValues["id_number"] )
						 && !empty( $idTypeValues["issued_by"] )
						 && !empty( $idTypeValues["issue_date"] )
						 && !empty( $idTypeValues["expiry_date"] ) )
					{
						// If id is empty, its an insert request.
						// Insert new reocrd in this case.
		
						$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
						$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];
		
						$insertQuery = "insert into cm_user_id_types
												(
													id_type_id, 
													user_id, 
													user_type, 
													id_number, 
													issued_by, 
													issue_date, 
													expiry_date 
												)
											values
												(
													'" . $idTypeValues["id_type_id"] . "', 
													'" . $benID . "', 
													'B', 
													'" . $idTypeValues["id_number"] . "', 
													'" . $idTypeValues["issued_by"] . "', 
													'" . $issueDate . "', 
													'" . $expiryDate . "'
												)";
						insertInto( $insertQuery );
					}
				}
			}
			if(isset($_POST["serviceType"]) && $_POST["serviceType"]!=""){
				$updateServiceTypeSql = "update ".TBL_CM_BENEFICIARY." set serviceType='".$_POST["serviceType"]."' where benID='".$benID."'";
				update($updateServiceTypeSql);
			}
			if(isset($_POST["collectionPointID"]) && $_POST["collectionPointID"]!=""){
				update("update ".TBL_CM_BENEFICIARY." set collectionPointID='".$_POST["collectionPointID"]."' where benID='".$benID."'");
			}
			if(isset($_POST["beneficiaryName"]) && $_POST["beneficiaryName"]!=""){
				update("update ".TBL_CM_BENEFICIARY." set beneficiaryName='".$_POST["beneficiaryName"]."' where benID='".$benID."'");
			}
			// End of code against #3299: MasterPayex - Multiple ID Types
			


	$_SESSION["Title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Address2"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["bankCode"] 		= "";
	$_SESSION["branchName"] 		= "";
	$_SESSION["branchCity"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";
	$_SESSION["accountType"] = "";
	$_SESSION["ibanorbank"] = "";
	$_SESSION["serviceType"] = "";
	$_SESSION["beneficiaryName"] = "";
	
	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] );
	
	insertError(BE9);
} else {

$entryDate = date("Y-m-d");
if(trim($_POST["transType"]) != "Bank Transfer")
	$_POST["transType"] = "";

	$Querry_Sqls = "update cm_beneficiary set Title='".$_POST["Title"]."', 
	 firstName='".checkValues($_POST["firstName"])."', 
	 middleName='".checkValues($_POST["middleName"])."', 
	 lastName='".checkValues($_POST["lastName"])."', 
	 IDType='".$_POST["IDType"]."', 
	 NICNumber='".checkValues($_POST["IDNumber"])."',
	 Address='".checkValues($_POST["Address"])."', 
	 Address1='".checkValues($_POST["Address1"])."', 
	 Address2='".checkValues($_POST["Address2"])."', 
	 Zip='".$_POST["Zip"]."', 
	 Country='".$_POST["Country"]."', 
	 City='".$_POST["City"]."', 
	 State='".$_POST["State"]."', 
	 Phone='".checkValues($_POST["Phone"])."',  
	 Mobile='".checkValues($_POST["Mobile"])."',  
	 email='".checkValues($_POST["benEmail"])."',  
	 transType = '".checkValues($_POST["transType"])."'	, 
	 editDate 	 = '".$entryDate."'	, 
	 loginID  	 = '".$userId."', 
	 editedBy  = '$username', 
	 editDate  = '$entryDate', 
	 proveAddress = '".$_POST["proveAddress"]."',
	 proveAddType = '".$_POST["proveAddressType"]."'
	 where benID = '".$_POST["benID"]."'";
	if(defined("CONFIG_IBAN_ON_QUICK_BEN") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
		$Querry_Sqls = "update cm_beneficiary set Title='".$_POST["Title"]."', 
		 firstName='".checkValues($_POST["firstName"])."', 
		 middleName='".checkValues($_POST["middleName"])."', 
		 lastName='".checkValues($_POST["lastName"])."', 
		 IDType='".$_POST["IDType"]."', 
		 NICNumber='".checkValues($_POST["IDNumber"])."',
		 Address='".checkValues($_POST["Address"])."', 
		 Address1='".checkValues($_POST["Address1"])."', 
		 Address2='".checkValues($_POST["Address2"])."', 
		 Zip='".$_POST["Zip"]."', 
		 Country='".$_POST["Country"]."', 
		 City='".$_POST["City"]."', 
		 State='".$_POST["State"]."', 
		 Phone='".checkValues($_POST["Phone"])."',  
		 Mobile='".checkValues($_POST["Mobile"])."',  
		 email='".checkValues($_POST["benEmail"])."',  
		 transType = '".checkValues($_POST["transType"])."'	, 
		 editDate 	 = '".$entryDate."'	, 
		 loginID  	 = '".$userId."', 
		 editedBy  = '$username', 
		 editDate  = '$entryDate', 
		 proveAddress = '".$_POST["proveAddress"]."',
		 proveAddType = '".$_POST["proveAddressType"]."',
		 IbanOrBank = '".$_POST["ibanbank"]."'
		 where benID = '".$_POST["benID"]."'";
	}
 	update($Querry_Sqls);

		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , benID   , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', '".$_POST["benID"]."', $entryDate,'UPDATION',$userId)");		
		}	
			//if(trim($_POST["transType"]) == "Bank Transfer")
		if(!empty($_REQUEST["benID"]))
		{
			if(CONFIG_IBAN_ON_QUICK_BEN == "1" && CONFIG_IBAN_OR_BANK_TRANSFER!="1"){
				$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
				$sqlBank = "insert into cm_bankdetails
							( 
							benID, bankName, BankCode ,BranchName,
							accNo, branchCode, branchAddress,BranchCity ,
							ABACPF , IBAN,swiftCode ,accountType
							)
							Values
							(
							'".$_POST["benID"]."',  '".$_POST["bankName"]."','".$_POST["bankCode"]."','".$_POST["branchName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."','".$_POST["branchCity"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."',	'".$_POST["accountType"]."'
							)
							";
				insertInto($sqlBank);	
			
			}
			elseif(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
				$existBank = selectFrom("select * from cm_bankdetails where benID='".$_POST["benID"]."'");
				if($existBank!=""){
					if($_POST["ibanbank"]=="bank"){
						$updateQuery = "update cm_bankdetails 
											set 
												bankName='" . $_POST["bankName"] . "', 
												BankCode='" . $_POST["bankCode"] . "', 
												BranchName='" . $_POST["branchName"] . "' ,
												accNo='" .$_POST["accNo"]. "', 
												branchCode='" . $_POST["branchCode"] . "', 
												branchAddress='" . $_POST["branchAddress"] . "', 
												BranchCity='" . $_POST["branchCity"] . "' ,
												swiftCode='" . $_POST["swiftCode"] . "',
												accountType='" . $_POST["accountType"] . "'
											where 
												benID='" .$_POST["benID"] . "'";
					}
					else{
						$updateQuery = "update cm_bankdetails 
											set 
												IBAN='" . $_POST["IBAN"] . "'
											where 
												benID='" .$_POST["benID"] . "'";
						$updateIbanQuery = "update cm_beneficiary 
											set 
												IBAN='" . $_POST["IBAN"] . "'
											where 
												benID='" .$_POST["benID"] . "'";
						update( $updateIbanQuery );
					}
					update( $updateQuery );
				}
				else{
					if($_POST["ibanbank"]=="bank"){
						$sqlBank = "insert into cm_bankdetails
									( 
									benID, bankName, BankCode ,BranchName,
									accNo, branchCode, branchAddress,BranchCity ,
									ABACPF , swiftCode, accountType
									)
									Values
									(
									'".$_POST["benID"]."',  '".$_POST["bankName"]."','".$_POST["bankCode"]."','".$_POST["branchName"]."',
									'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."','".$_POST["branchCity"]."',
									'".$_POST["ABACPF"]."',	'".$_POST["swiftCode"]."',	'".$_POST["accountType"]."'
									)
									";
					}
					else{
						$sqlBank = "insert into cm_bankdetails(benID,IBAN)Values('".$_POST["benID"]."','".$_POST["IBAN"]."')";
						$updateIbanQuery = "update cm_beneficiary 
											set 
												IBAN='" . $_POST["IBAN"] . "'
											where 
												benID='" .$_POST["benID"] . "'";
						update( $updateIbanQuery );
					}
					insertInto($sqlBank);	
				}
			 }
		}
		else
		{
			$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 	

			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["branchName"] 	= "";
			$_SESSION["bankCode"] 	= "";
			$_SESSION["branchCity"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			$_SESSION["transType"] = "";
		}
			
			
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				if ( !empty($idTypeValues["id"]) )
				{
					// If id is not empty, its an update request.
					// Update the existing record in this case.

					$updateQuery = "update cm_user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
					update( $updateQuery );
				}
				else
				{
					// If id is empty, its an insert request.
					// Insert new reocrd in this case.

					

					$insertQuery = "insert into cm_user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["benID"] . "', 
												'B', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
					insertInto( $insertQuery );
				}
			}
		}
	}
	if($_POST["serviceType"]!="" && isset($_POST["serviceType"])){
		update("update ".TBL_CM_BENEFICIARY." set serviceType='".$_POST["serviceType"]."' where benID='".$_POST["benID"]."'");
	}
	if(isset($_POST["collectionPointID"]) && $_POST["collectionPointID"]!=""){
		update("update ".TBL_CM_BENEFICIARY." set collectionPointID='".$_POST["collectionPointID"]."' where benID='".$_POST["benID"]."'");
	}
	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] ); 
	if(isset($_POST["beneficiaryName"]) && $_POST["beneficiaryName"]!=""){
		update("update ".TBL_CM_BENEFICIARY." set beneficiaryName='".$_POST["beneficiaryName"]."' where benID='".$_POST["benID"]."'");
	}

	// End of code against #3299: MasterPayex - Multiple ID Types

	insertError(BE10);

	$_SESSION["Title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Address2"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["bankCode"] 		= "";
	$_SESSION["branchName"] 		= "";
	$_SESSION["branchCity"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";
	$_SESSION["accountType"] = "";
	$_SESSION["ibanorbank"] = "";
	$_SESSION["serviceType"] = "";
	if ( isset($_SESSION["proveAddress"]) )
		unset( $_SESSION["proveAddress"] );
	if ( isset($_SESSION["proveAddressType"]) )
		unset($_SESSION["proveAddressType"]);
	if(isset($_SESSION["beneficiaryName"]))
		unset($_SESSION["beneficiaryName"]);		
}
if($_GET["from"] == "popUp")
{
	$_SESSION["benID"] = $benID;
	?>
	<script language="javascript">
		opener.document.location = "add-transaction.php?msg=Y&benID="+<?=$_SESSION["benID"]?>;
		window.close();
	</script>
	<?
}
else
{
	redirect($backURL . "&success=1");
}
?>