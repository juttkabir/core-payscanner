<?
session_start();
require_once ("../include/config.php");
//require_once ("./include/flags.php");
require_once ("security.php");
$date_time = date('d-m-Y  h:i:s A');
$_SESSION["pincode"] = "";



//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;

if($_POST['agentID'] != '')
	$_SESSION["benAgentID"] = $_POST['agentID'];

	
	$_SESSION["BenTempcurrencyTo"] =  $_SESSION["currencyTo"];
	
	//echo $_SESSION["benAgentID"];
$transType = $_SESSION["transType"]  = trim($_POST["transType"]);
$_SESSION["IMFee"] = $_REQUEST["IMFee"];
$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["bankCharges"] = $_POST["bankCharges"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["bankNametemp"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["bankCode"] 		= $_POST["bankCode"];
$_SESSION["branchCity"] 	= $_POST["branchCity"];
$_SESSION["branchName"] 	= $_POST["branchName"];
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["accountType"]  = $_POST["accountType"]; 
//$_SESSION["currencyTo"] = $_POST['currencyTo'];	

$_SESSION["cc_cardType"] = $_POST["cc_cardType"];
$_SESSION["cc_cardNo"] = $_POST["cc_cardNo"];
$credCardNo = $_POST["cc_cardNo"];//"**************". substr($_SESSION["cc_cardNo"],12,4);
$_SESSION["cc_expiryDate"] = $_POST["cc_expiryDate"] ;
$_SESSION["cc_cvvNo"]	= $_POST["cc_cvvNo"];
$_SESSION["cc_firstName"] = $_POST["cc_firstName"];
$_SESSION["cc_lastName"] = $_POST["cc_lastName"];
$_SESSION["cc_address1"] = $_POST["cc_address1"] ;
$_SESSION["cc_address2"]	= $_POST["cc_address2"];
$_SESSION["cc_city"] = $_POST["cc_city"];
$_SESSION["cc_state"] = $_POST["cc_state"];
$_SESSION["cc_postcode"] = $_POST["cc_postcode"] ;
$_SESSION["cc_country"]	= $_POST["cc_country"];
$_SESSION["creditCard"] = $_POST["creditCard"];
$_SESSION["ccExpYr"] = $_POST["ccExpYr"];
$_SESSION["ccExpMo"] = $_POST["ccExpMo"];

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];	
$_SESSION["c_id"] = $_SESSION["customerID"];

$_SESSION["transAmount"] = $_POST["transAmount"];
$_SESSION["localAmount"] = $_POST["localAmount"];
$_SESSION["totalAmount"] = $_POST["totalAmount"];
$_SESSION["exchangeRate"] = $_REQUEST["exchangeRate"];
$_SESSION["exchangeID"] = $_REQUEST["exchangeID"];

$_SESSION["benCountry"] = $_REQUEST["benCountry"];
$_SESSION["customerCountry"] = $_REQUEST["customerCountry"];
$_SESSION["bankTransferType"] = $_REQUEST["bankTransferType"];

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y&transAmount=".$_POST["transAmount"];
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}

if(trim($_POST["transType"]) == "Pick up")
{
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
}


if(trim($_POST["customerID"]) == "")
{
	if($_SESSION["customerID"] == "")
	{
		$_POST["customerID"] = $_SESSION["c_id"];
		$_SESSION["customerID"] = $_SESSION["c_id"];
	}
}
if(trim($_REQUEST["benID"]) == "")
{
	if(trim($_SESSION["benID"]) == "")
	{
		insertError(TE6);
		redirect($backUrl);
	}
}



// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if(trim($_POST["distribut"]) == "")
	{
		insertError("Please select a Distributor from the list");
		redirect($backUrl);
	}

	// Code for IBAN number check starts here.
	$queryBen = "select Country,IBAN,IbanOrBank  from ".TBL_CM_BENEFICIARY." where benID ='" . $_REQUEST["benID"] . "'";
	$benificiaryContent = selectFrom($queryBen);
	$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
	
	// In case of European countries for now transfer, it is required to bypass the checks regarding bank detail fields.
	// It is because they use only IBAN field.
	//debug($qEUTrans["countryRegion"], true);
	if($qEUTrans["countryRegion"] == "European" && CONFIG_IBAN_OR_BANK_TRANSFER!="1")
	{
		// In case this clause is true, the bank details checks will be bypassed and only IBAN check will work.
		$sql = "select IBAN from cm_bankdetails where benID='".$_REQUEST["benID"]."'";
		$resultForIBAN = selectFrom($sql);
		if ( empty($benificiaryContent["IBAN"]) )
		{
			insertError("IBAN for a beneficiary must be provided in his profile.");
			redirect($backUrl);
		}
		
	} // Code for IBAN number check ends here.
	else
	{ 	
		if(CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
			$resultForIBAN = $benificiaryContent;
			if (isset($_POST["IBAN"]) && $_POST["IBAN"]=="" )
			{
				insertError("IBAN for a beneficiary must be provided in his profile.");
				redirect($backUrl);
			}
		}
		// Validity cheks for Bank Details of Beneficiary only in case the above clause is not true and 
		// this clause is executed.
		$queryBen = "select Country from ".TBL_CM_BENEFICIARY." where benID ='" . $_POST["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
		$bencountry = $benificiaryContent["Country"];
		if(isset($_POST["bankName"]) && trim($_POST["bankName"]) == "")
		{
			insertError("Please enter the Bank Name");	
			redirect($backUrl);
		}
		if(isset($_POST["accNo"]) && trim($_POST["accNo"]) == "")
		{
			insertError("Please enter your account number");
			redirect($backUrl);
		}
		if(isset($_POST["accNo"]) && trim($_POST["accNo"]) != "" && $bencountry == "Pakistan"  && $_POST["bankName"] == "United Bank Limited")
		{		
				if(isset($_POST["accNo"]) && strlen($_POST["accNo"]) != 8)
				{	
					insertError("Please enter 8 digit account number");	
					redirect($backUrl);
				}		
		}
	
		if(isset($_POST["bankCode"]) && trim($_POST["bankCode"]) == "")
		{
					insertError("Please enter Bank Code");	
					redirect($backUrl);
		}
		
		if(isset($_POST["bankCode"]) && trim($_POST["bankCode"]) != "" && $bencountry == "Pakistan"  && $_POST["bankName"] == "United Bank Limited")
		{
				if(isset($_POST["bankName"]) && strlen($_POST["bankCode"]) != 24)
				{	
					insertError("Please enter 24 digit Bank Code");	
					redirect($backUrl);
				}
		}
		
		/*if(trim($_POST["branchCode"]) == "")
		{
			insertError("Please enter Branch Code");	
			redirect($backUrl);
		}*/
		if(isset($_POST["branchName"]) && trim($_POST["branchName"]) == "")
		{
			insertError("Please enter Branch Name");	
			redirect($backUrl);
		}
		if(isset($_POST["branchCity"]) && trim($_POST["branchCity"]) == "")
		{
			insertError("Please enter Branch City Name");	
			redirect($backUrl);
		}	
		/*if(trim($_POST["branchAddress"]) == "")
		{
			insertError("Please enter Branch Address");	
			redirect($backUrl);
		}*/
	
		/*if(trim($_POST["swiftCode"]) == "")
		{
			insertError("");	
			redirect($backUrl);
		}*/
	
		/*if(trim($_POST["ABACPF"]) == "" &&  $_POST["Country"] == "United States")
		{
			insertError("Please enter ABA numer");	
			redirect($backUrl);
		}*/
	
		if(isset($_POST["ABACPF"]) && trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , $_POST["Country"]))
		{
			insertError("Please enter CPF number");	
			redirect($backUrl);
		}

	}
	// The logic regarding the IBAN field check ends here.

/*	if(ibanCountries( $_POST["Country"]) && trim($_POST["IBAN"]) == "")
	{
		insertError("Please enter IBAN numer");	
		redirect($backUrl);
	}*/
}
else
{
				$_SESSION["bankName"] 		= "";
				$_SESSION["branchCode"] 	= "";
				$_SESSION["branchAddress"] 	= "";
				$_SESSION["branchName"] 	= "";
				$_SESSION["bankCode"] 	= "";
				$_SESSION["branchCity"] 	= "";
				$_SESSION["swiftCode"] 		= "";
				$_SESSION["accNo"] 			= "";
				$_SESSION["ABACPF"] 		= "";
				$_SESSION["IBAN"] 			= "";
				$_SESSION["ibanbank"]		= "";
				$_SESSION["ibanorbank"]		= "";
				$_SESSION["accountType"]  = "";
}



if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{

}
else
{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_REQUEST["moneyPaid"]) == "")
{
	if(trim($_SESSION["moneyPaid"]) == "")
	{
		insertError(TE12);
		redirect($backUrl);
	}
}

if ( !defined(CONFIG_NONCOMPUL_FUNDSOURCES) || CONFIG_NONCOMPUL_FUNDSOURCES != '1' )
{
	if( empty($_POST["fundSources"]) )
	{
		insertError(TE30);
		redirect($backUrl);
	}
}

$_SESSION["Declaration"] = $_POST["Declaration"];
if(trim($_POST["Declaration"]) != "Y")
{
	if(trim($_SESSION["Declaration"]) != "Y")
	{
		insertError(TE13);	
		redirect($backUrl);
	}
}
  
  if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
  {
  
	   $_SESSION["Save"] = $_POST["Save"];

  		if(trim($_POST["cc_cardType"]) == "")
		{
			insertError(CR1);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_cardNo"]) == "")
		{
			insertError(CR2);	
			redirect($backUrl);
		}
		if(trim($_POST["ccExpYr"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}
		if(trim($_POST["ccExpMo"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}		
		if(trim($_POST["cc_cvvNo"]) == "")
		{
			insertError(CR4);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_firstName"]) == "")
		{
			insertError(CR5);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_lastName"]) == "")
		{
			insertError(CR6);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_address1"]) == "")
		{
			insertError(CR7);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_address2"]) == "")
		{
			insertError(CR8);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_city"]) == "")
		{
			insertError(CR9);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_state"]) == "")
		{
			insertError(CR10);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_postcode"]) == "")
		{
			insertError(CR11);	
			redirect($backUrl);
		}
		if(trim($_POST["cc_country"]) == "")
		{
			insertError(CR12);	
			redirect($backUrl);
		}

}
require_once ("header.php");
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script>
		function checkForm(){
		
			if(document.forms.addTrans.pincode.value == "" || IsAllSpaces(document.forms.addTrans.pincode.value)){
					alert("Please provide PINCODE.");
				 document.forms.addTrans.pincode.focus();
				return false;
			}
			return true;
		}	
		
		function pincodeValue()
		{
				var temp;
				temp = document.forms.addTrans.pincode.value;
				var strurl = 'check-pincode.php?from=popUp&pincode=' + temp;
				window.open(strurl,'disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=150,width=400');
		}
		function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
</script>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">

  
    <tr>
      <td align="center" valign="top"><br>
        <table width="700" border="0" bordercolor="#FF0000">
  <tr height="30">
    <td align="left" bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">&nbsp;Confirm Transaction.</font></b></td>
  </tr>		
          <tr>
            <td align="right"><a href="add-transaction.php" class="style2">Change Information</a></td>
          </tr>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <br>
            <?

	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo $width?>" height="<? echo $height?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0> </td>
              </tr>
            </table>
	<?
	}
	?>
            <br>
			<font color="#005b90">Transaction Date: </font><? echo date("F j, Y")?><br>
            </fieldset></td>
          </tr>

          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Customer Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
		
			 $queryCust = "select c_id, c_name,  c_address, c_address2, c_city, c_state, c_zip,c_country, c_phone, c_email  from ".TBL_CM_CUSTOMER." where c_id ='" . $_SESSION["c_id"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3" align="left"><? echo $customerContent["c_name"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3" align="left"><? echo $customerContent["c_address"] . " " . $customerContent["c_address2"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 

			
			{
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email from ".TBL_CM_BENEFICIARY." where benID ='" . $_POST["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Beneficiary Name</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			  if($_POST["transType"] == "Bank Transfer")
			  {
		  			//$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");

					// Code to display IBAN number.
					$queryBen = "select Country,IBAN,IbanOrBank  from ".TBL_CM_BENEFICIARY." where benID ='" . $_REQUEST["benID"] . "'";
					$benificiaryContent = selectFrom($queryBen);
				
					$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
					// In case of European countries for now transfer, it is required to bypass the checks regarding bank detail fields.
					// It is because they use only IBAN field.
					//if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
					if(($_SESSION["bankTransferType"] == "I" && CONFIG_IBAN_OR_BANK_TRANSFER!="1") || (isset($_POST['IBAN']) && $_POST['IBAN']!="" && CONFIG_IBAN_OR_BANK_TRANSFER=="1"))
					{
						// In case this clause is true, only IBAN number will be displayed.
						
						// The $resultForIBAN has been defined in the validation code area of this document.
						?>
							<tr>
								<td align="right"><font color="#005b90">IBAN</font></td>
								<td><?=$resultForIBAN["IBAN"];?></td>
							</tr>
						<?
					} // Code for IBAN number check ends here.
					else
					{ 
						// Display Bank Details of Beneficiary only in case the above clause is not true and 
						// this clause is executed.
						?>
						  <tr>
							<td colspan="2" align="left"><span class="style5">Beneficiary Bank Details </span></td>
							<td width="100">&nbsp;</td>
							<td width="200">&nbsp;</td>
						  </tr>
						  <tr>
							<td width="150" align="right"><font color="#005b90">Bank Name</font></td>
							<td width="200" align="left"><? echo $_POST["bankName"]; ?></td>
							<td width="100" align="right"><font color="#005b90">Acc Number</font></td>
							<td width="200" align="left"><? echo $_POST["accNo"]; ?>                        </td>
						  </tr>
						  <tr>
							<td width="150" align="right"><font color="#005b90">Branch Code</font></td>
							<td width="200" align="left"><? echo $_POST["branchCode"]; ?>                        </td>
							<td width="100" align="right"><font color="#005b90">
							  <?
													/*if($_POST["benCountry"] == "United States")
													{
														echo "ABA Number*";
													}
													else*/if(CONFIG_CPF_ENABLED == '1')
													{
														echo  "CPF Number*";
													}
													?>
											  &nbsp; </font></td>
															<td width="200"><?
													if(CONFIG_CPF_ENABLED == '1')
													{
														echo $_POST["ABACPF"];
														 
													}
													?> &nbsp;
														</td>
						  </tr>
						  <tr>
							<td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
							<td width="200" align="left"><? echo $_POST["branchAddress"]; ?>  </td>
							 <?
							if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
							{?>
								<td width="150" align="right"><font color="#005b90">Account Type</font></td>
								<td width="200"><?=$_SESSION["accountType"]?> </td>
							  <? 
							  }else {
							  ?>	
													<td width="100" height="20"  align="right">&nbsp;  </td>
													<td width="200" height="20" align="left">&nbsp;  </td>
													<?
													}
													?>
						  </tr>
						  <tr>
							<td width="150" align="right">&nbsp;  </td>
							<td width="200" align="left">&nbsp;  </td>
							
					<td width="100" align="right" title="For european Countries only"><font color="#005b90">Bank 
					  Code </font></td>
							
					<td width="200" align="left" title="For european Countries only"><? echo $_POST["bankCode"]; ?></td>
						  </tr>
	
						  <?
					}
  }
			  if($_POST["transType"] == "Pick up")
			  {
					$benAgentID = $_POST["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = '$benAgentID'";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr align="left">
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Address</font></td>
                        <td width="200" rowspan="2" align="left" valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">&nbsp;
                          </font></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Compnay </font> </td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" height="20" align="right"><font color="#005b90">City</font></td>
                        <td width="200" height="20" align="left"><?  echo $senderAgentContent["cp_city"]; ?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_country"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }
  ?>
            </table>
              </fieldset></td>
              </tr>
         
		<!-- 
		  <tr>
            <td><fieldset>
            <legend class="style2">Payment Collection point </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><font color="#005b90">Branch Name </font></td>
                <td><? //echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><?// echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? //echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Bank Name </font></td>
                <td width="200"><?// echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? //echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? //echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? //echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>-->
              <?
			  }
			  ?>
          <tr>
            <td></td>
          </tr>
		  
		 
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="100%" border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Exchange Rate</font></td>
                <td align="left"><? echo $_POST["exchangeRate"]?> </td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" align="left" ><? echo $_POST["transAmount"]?> in  <? echo $_POST["currencyFrom"]?></td>
              </tr>
			  <tr>
			    <td align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$preFix Fee");}?></font></td>
			    <td align="left">
				<? 
				if ($_SESSION["transType"] != "Bank Transfer")
				{
					echo $_POST["IMFee"]; 
				}
				else
				{
					echo $_POST["IMFee"] + $_POST["bankCharges"]; 
				}	
				?> </td>
                <td width="100" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200" align="left"><? echo $_POST["localAmount"]?>  in  <? echo $_SESSION["currencyTo"];?>              </td>
			  </tr>
              <tr>
                <td align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td align="left"><? echo $_POST["transactionPurpose"]?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" align="left"><? echo $_POST["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td align="right"><font color="#005b90">Money Paid</font></td>
                <td align="left"><? echo $_POST["moneyPaid"]?> </td>
								
								 <? if(CONFIG_CURRENCY_CHARGES == '1')
						  		{?>
								  
								  <td width="100" align="right"><font color="#005b90"> Currency Charges </font></td>
								  <td width="200" align="left"><?=$_SESSION["currencyCharge"]?></td>
								  
								  
								  <? }else
								  {
								   ?>
			                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
			                <td width="200">&nbsp;</td>
			                
									<?
									}
									?>
				
              </tr>
              <tr>
                <td width="100" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font></td>
                <td colspan="3" align="left"><? echo $_POST["tip"]?></td>		
               
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?
if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
{
?>
					  <tr>
					  		<td colspan="4">&nbsp;</td>
					  </tr>
					  <td colspan="4"><fieldset>
                    <legend class="style2">Credit/Debit Card Details </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">                      
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cc_cardType"];?>
						</td>
                        <td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
                        <td width="183" height="20" align="left">
						<? echo "****************";?>
						</td>
                      </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["ccExpMo"]."-".$_SESSION["ccExpYr"];?>
						</td>
                        <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo "****";?>
						</td>
                      </tr>
                      <tr>
                        
                <td width="143" height="20" align="right"><font color="#005b90">First 
                  Name *</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cc_firstName"];?>
						</td>
                        
                <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Last 
                  Name* </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $_SESSION["cc_lastName"];?>
						</td>
                      </tr>
                      <tr>
                        
                <td width="143" height="20" align="right"><font color="#005b90">Address1*</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cc_address1"];?>
						</td>
                        
                <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Address</font><font color="#005b90">2* 
                  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $_SESSION["cc_address2"];?>
						</td>
                      </tr>
                      <tr>
                        
                <td width="143" height="20" align="right"><font color="#005b90">Postcode*</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cc_postcode"];?>
						</td>
                        
                <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">City* 
                  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $_SESSION["cc_city"];?>
						</td>
                      </tr>
                      <tr>
                        
                <td width="143" height="20" align="right"><font color="#005b90">State*</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cc_state"];?>
						</td>
                        
                <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Country* 
                  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $_SESSION["cc_country"];?>
						</td>
                      </tr>

                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  	<td colspan="4">&nbsp;</td>
					  </tr>		
<?
}
?>
				
				  <form name="addTrans" action="add-transaction-conf.php" method="post" onSubmit="return checkForm();">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input name="transType" type="hidden" value="<? echo $transType?>">
				  <input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">				  				  	 			 
					  <td colspan="4"><fieldset>
                    <legend class="style2">PIN Code </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                     
                      <tr>
                        <td  height="20" align="right"><font color="#005b90">PIN Code</font> </td>
                        <td  height="20" align="left" colspan="3">
						<input type="password" name="pincode" value="<?=$_SESSION["pincode"]; ?>" maxlength="5">
                  &nbsp;&nbsp;&nbsp;<a href="javascript:;" class="style1" onClick="pincodeValue()"><font color="#FF0000"><b>Verify 
                  PIN Code</b></font></a> </td>
						 <tr>
							 <td colspan="4">&nbsp;</td>
						 </tr>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
              	<td>
              		Customer Service Number <? echo CONFIG_INVOICE_FOOTER; ?>
              	</td>
         </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>		
					  <tr>
              	<td height="20" colspan="3">
                	<? if(CONFIG_TRANS_CONDITION_ENABLED==1)
                	{
                		echo(CONFIG_TRANS_COND);
                	 }else{
                	 ?>
                	I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
                	<? } ?>
                	</td>
                </tr>
					
          <tr>
            <td align="center">
	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
//echo $hiddenFields;
	  ?>


			<input type="submit" name="Submit" value="Confirm Order">
            <input type="button" name="Submit" value="Print this Receipt" onClick="print()">
                  </form>			
			</td>
          </tr>
          <tr>
            <td align="right"><a href="add-transaction.php?transID=<? echo $_POST["transID"]?>" class="style2">Change Information</a></td>
          </tr>
      </table></td>
			<td valign="top"><?
		include "exchange-rates-agent.php";
		?></td>		
    </tr>

</table>
</body>
</html>
<?

include "footer.php";
?>