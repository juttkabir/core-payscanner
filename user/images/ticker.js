<!-- Super News Ticker (JavaScript) -->
<!-- Version 2.1 (26.11.2001) -->
<!-- Created by: Gibin Software House (http://www.gibinsoft.net) -->
<!-- Created for: Marek Malcherek (marek@malcherek.de) -->
<!-- Based on: NewsTicker of Ernst Straka (ernst.straka@central-europe.basf.org) -->

// =============== AREA FOR DEVELOPER : BEGIN ===============
var t1 = 130;				// top of ticker in pixels, 
							// or 0 to position relative
							// (0 is not recommended)
var w1 = screen.width - 50;				// width of ticker in pixels
							// must be equal to width of tickpos
var l1 = 160;				// left of ticker in pixels, 
							// or 0 to position relative
							// (0 is not recommended)

var pre_visible_size = 160;	// the visible width of previous news text
							// when new one appears
var tick_timeout = 1;		// time between text moves
// ================ AREA FOR DEVELOPER : END ================

var ie = document.all ? true : false;
var nn6 = ( !document.all && document.getElementById ) ? true : false;

var first = true;
var l2 = l1 + w1;
var l = l2;

var news_list = new Array ();
var news_pre = -1;
var news_cur = 0;
var w_cur = 0;
var w_pre = 0;

var tick_enabled = true;

function init_news_list()
{
	if ( ie )
	{
// =============== AREA FOR DEVELOPER : BEGIN ===============
		news_list[0] = ticktext1;
		//news_list[1] = ticktext2;
		//news_list[2] = ticktext3;
// ================ AREA FOR DEVELOPER : END ================
	}
	else if ( nn6 )
	{
// =============== AREA FOR DEVELOPER : BEGIN ===============
		news_list[0] = document.getElementById("ticktext1");
		//news_list[1] = document.getElementById("ticktext2");
		//news_list[2] = document.getElementById("ticktext3");
// ================ AREA FOR DEVELOPER : END ================
	}
	else
	{
// =============== AREA FOR DEVELOPER : BEGIN ===============
		news_list[0] = document.ticktext1;
		//news_list[1] = document.ticktext2;
		//news_list[2] = document.ticktext3;
		//news_list[3] = "I am from Pakistan";
// ================ AREA FOR DEVELOPER : END ================
	}
}

function tickinit()
{
	init_news_list();

	if ( l1 == 0 && t1 == 0 )
	{	
		if ( ie )
		{
			pos = document.all['tickpos'];
			l1 = getLeft(pos);
			t1 = getTop(pos);
		}
		else
		{
			pos = document.anchors['tickpos'];
			
			if ( typeof(pos) != "undefined" )
			{
				l1 = pos.x;
				t1 = pos.y;
			}
		}
	}

	news_cur = 0;

	while ( news_list[news_cur] != null &&
			typeof(news_list[news_cur]) != "undefined" )
	{
		if ( ie )
			news_list[news_cur].style.posTop = t1;
		else
			news_list[news_cur].pageY = t1;
			
		news_cur++;
	}

	news_cur = 0;

	l2 = l1 + w1;
	l = l2;
	
	if ( typeof(news_list[0]) != "undefined" )
		setInterval("tick()", tick_timeout);
}

function getLeft(ll)
{
	if ( ll.offsetParent )
		return (ll.offsetLeft + getLeft(ll.offsetParent));
	else 
		return (ll.offsetLeft);
}

function getTop(ll)
{
	if ( ll.offsetParent )
		return (ll.offsetTop + getTop(ll.offsetParent));
	else
		return (ll.offsetTop);
}

function getWidth(ll)
{
	return ll.offsetWidth;
}

function tick()
{
	if ( tick_enabled == false )
		return;

	l = l - 0.5;

	w_cur = w1;
	
	if ( ie )
		w_cur = getWidth(news_list[news_cur]);
	else
		w_cur = w1; // !!! TODO !!!

	if ( l < (l1 - w_cur  + pre_visible_size) )
	{
		news_pre = news_cur;
		
		if ( news_list[news_cur + 1] == null ||
			 typeof(news_list[news_cur + 1]) == "undefined" )
		{
			news_cur = 0;
			first = false;
		}
		else
		{
			news_cur ++;
		}

		l = l2;
	}

	cl = l1 - l;
	cr = l2 - l;

	if ( ie )
	{
		news_list[news_cur].style.posLeft = l;
		news_list[news_cur].style.posTop = t1;
		news_list[news_cur].style.clip = "rect(auto "+cr+"px auto "+cl+"px)";
	
		if ( first )
			news_list[news_cur].style.visibility = "visible";
	}
	else if ( nn6 )
	{
		news_list[news_cur].style.left = l;
		news_list[news_cur].style.top = t1;
		news_list[news_cur].style.clip = "rect(auto "+cr+"px auto "+cl+"px)";
		
		if ( first )
			news_list[news_cur].style.visibility = "visible";
	}
	else
	{
		news_list[news_cur].pageX = l;
		news_list[news_cur].clip.left = cl;
		news_list[news_cur].clip.right = cr;

		if ( first )
			news_list[news_cur].visibility = "show";
	}

	if ( l > l2 - pre_visible_size && news_pre >= 0 )
	{
		w_pre = w1;
		
		if ( ie )
			w_pre = getWidth(news_list[news_pre]);
		else
			w_pre = w1; // !!! TODO !!!

		l_pre = l1 - w_pre + (pre_visible_size + l - l2);
		cl_pre = l1 - l_pre;
		cr_pre = l2 - l_pre;
			
		if ( ie )
		{
			news_list[news_pre].style.posLeft = l_pre;
			news_list[news_pre].style.posTop = t1;
			news_list[news_pre].style.clip = "rect(auto "+cr_pre+"px auto "+cl_pre+"px)";
		}
		else if ( nn6 )
		{
			news_list[news_pre].style.left = l_pre;
			news_list[news_pre].style.top = t1;
			news_list[news_pre].style.clip = "rect(auto "+cr_pre+"px auto "+cl_pre+"px)";
		}
		else
		{
			news_list[news_pre].pageX = l_pre;
			news_list[news_pre].clip.left = cl_pre;
			news_list[news_pre].clip.right = cr_pre;
		}
	}
}

function disable_tick()
{
	tick_enabled = false;
}

function enable_tick()
{
	tick_enabled = true;
}