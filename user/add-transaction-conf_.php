<?php
session_start();
// Including files
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;

$systemPre = "CN";
$moneyTransfer="Company Name";

$agentCountry = $_POST["benCountry"];
$customerCountry = $_POST["customerCountry"];
if(trim($_POST["transType"]) == "Bank Transfer" || trim($_POST["transType"]) == "Home Delivery")
{
	
	if($agentCountry == 'Canada')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'China')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'France')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Italy')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'New Zealand')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Poland')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Switzerland')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'South Africa')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Turkey')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'United Arab Emirates')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'United States')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'India')
	{
		$benAgentIDs = 18455;
	}	
	else
	{
		$Tran_Type = $_POST["transType"];
		$agentCountry = $_POST["benCountry"];
		$query = "select * from admin where adminType='Agent' and  agentType = 'Supper' and isCorrespondent != 'N'  and isCorrespondent != '' and agentCountry = '$agentCountry'";				
		$nResult = mysql_query($query)or die("Invalid query: " . mysql_error()); 
		$rstRow = mysql_fetch_array($nResult);	
		$benAgentIDs =  $rstRow["userID"];		
		// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
		if($benAgentIDs == '')
			 $benAgentIDs = 18463;
	}
	
}
elseif(trim($_POST["transType"]) == "Pick up")
{
	/**********************************************************************/
	if($agentCountry == 'Pakistan')
	{
		$benAgentIDs = 228;
	}
	elseif($agentCountry == 'Vietnam')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Philippines')
	{
		$benAgentIDs = 18463;
	}
	
	/**********************************************************************/
	elseif($agentCountry == 'Australia')
	{
		$benAgentIDs = 18463;
	}		
	elseif($agentCountry == 'Canada')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'China')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'France')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Italy')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'New Zealand')
	{
		$benAgentIDs = 18463;
	}	
	elseif($agentCountry == 'Poland')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Switzerland')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'South Africa')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'Turkey')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'United Arab Emirates')
	{
		$benAgentIDs = 18463;
	}
	elseif($agentCountry == 'United States')
	{
		$benAgentIDs = 18463;
	}
	else
	{
		$benAgentIDs = 236;
	}

}/*
else
{
	$benAgentIDs = $_POST["agentID"];
}*/

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $benAgentIDs;
$_SESSION["bankCharges"] = $_POST["bankCharges"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];

$_SESSION["cardType"] = $_POST["cardType"];
$_SESSION["cardNo"] = $_POST["cardNo"];
$_SESSION["expiryDate"] = $_POST["expiryDate"] ;
$_SESSION["cvvNo"]	= $_POST["cvvNo"];
$_SESSION["pincode"]	= $_POST["pincode"];


if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y";
}

if(trim($_POST["transType"]) == "")
{	
	insertError(TE3);	
	redirect($backUrl);
}
if(trim($_POST["customerID"]) == "")
{
	if($_SESSION["customerID"] == "")
	{
		$_POST["customerID"] = $_SESSION["c_id"];
		$_SESSION["customerID"] = $_SESSION["c_id"];
	}
}

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}
if(trim($_POST["moneyPaid"]) == "")
{
	insertError(TE12);	
	redirect($backUrl);
}
// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if(trim($_POST["bankName"]) == "")
	{
		insertError(TE14);	
		redirect($backUrl);
	}
	
	if(trim($_POST["accNo"]) == "")
	{
		insertError(TE18);	
		redirect($backUrl);
	}

	if(trim($_POST["branchAddress"]) == "")
	{
		insertError(TE16);	
		redirect($backUrl);
	}

	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "United States")
	{
		insertError(TE19);	
		redirect($backUrl);
	}
	
	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "Brazil")
	{
		insertError(TE20);	
		redirect($backUrl);
	}

	if(ibanCountries($_POST["benCountry"]) && trim($_POST["IBAN"]) == "")
	{
		insertError(TE21);	
		redirect($backUrl);
	}
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{
}
else
{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}

if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
if(trim($_POST["pincode"]) == "")
		{	
				insertError(PIN1);	
				redirect($backUrl);	
		}
		else
		{
			$PINCODE = $_POST["pincode"];//64297
			$c_id= $_POST["customerID"];
			$strRandomQuery = "SELECT * FROM cm_customer where c_id = $c_id and SecuriyCode = $PINCODE";
			$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
			$nRows = mysql_fetch_array($nRandomResult);
			$oldPINcode = $nRows["SecuriyCode"];
			if($oldPINcode == '')
			{
				insertError(PIN1);	
				redirect($backUrl);
			}
		}		
if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
{
  		if(trim($_POST["cardType"]) == "")
		{
			insertError(CR1);	
			redirect($backUrl);
		}
		if(trim($_POST["cardNo"]) == "")
		{
			insertError(CR2);	
			redirect($backUrl);
		}
		if(trim($_POST["expiryDate"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}
		if(trim($_POST["cvvNo"]) == "")
		{
			insertError(CR4);	
			redirect($backUrl);
		}		
}

		
		
	if($_POST["transID"] == "")
	{
		//Getting New reference number
		$userId = $_SESSION["c_id"];
		$strQuery = "SELECT * FROM cm_customer where c_id = $userId";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
		$rstRow = mysql_fetch_array($nResult);				
		$username = $rstRow["c_name"];	


		$query = "select count(transID) as cnt from transactions where customerID='".$_POST["customerID"]."'";
		$nextAutoID = countRecords($query);
		$nextAutoID = $nextAutoID + 1;
		$cntChars = strlen($nextAutoID);
		if($cntChars == 1)
			$leadingZeros = "00000";
		elseif($cntChars == 2)
			$leadingZeros = "0000";
		elseif($cntChars == 3)
			$leadingZeros = "000";		
		elseif($cntChars == 4)
			$leadingZeros = "00";		
		elseif($cntChars == 5)
			$leadingZeros = "0";

		$imReferenceNumber = strtoupper($username) ."-". $leadingZeros . $nextAutoID ;

if($_SESSION["moneyPaid"] != 'By Credit Card' && $_SESSION["moneyPaid"] != 'By Debit Card')
{				
		$strRandomQuery = "SELECT * FROM cm_customer where c_id = $customerID";
		$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
		$nRows = mysql_fetch_array($nRandomResult);
		$Balance = $nRows["Balance"];		
		if($Balance>=$_POST["transAmount"])	
		{
			$Balance = ($Balance - ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]));
			$amount = ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]);
			$strQuery = "update cm_customer set Balance = '$Balance' where c_id = $customerID";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());			
			$Status	= "Authorize";
			
			$tran_date = date("Y-m-d");
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount ) 
						 values('".$customerID."','".$tran_date."','Payment From Accont','WITHDRAW','".$amount."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());						
		}
		else
		{
			$Status	= "Pending";
			$amount = ($_POST["transAmount"] + $_POST["IMFee"] + $_POST["bankCharges"]);
			$tran_date = date("Y-m-d");
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount ) 
						 values('".$customerID."','".$tran_date."','Transaction on Credit','WITHDRAW','".$amount."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());					
		}	
}
		
		 $sql = "INSERT INTO transactions
										    ( 
											customerID, benID, refNumberIM,
											exchangeID,  
											transAmount, exchangeRate, 
											localAmount, IMFee, totalAmount,benAgentID, 
											transactionPurpose, 
											transDate, transType, 											 
											currencyFrom,   
											currencyTo ,moneyPaid ,bankCharges ,PINCODE,createdBy,transStatus,toCountry,
											fromCountry,collectionPointID  
											) 
											VALUES     
											( 
											'".$_POST["customerID"]."', '".$_POST["benID"]."', '$imReferenceNumber',										 
											'".$_POST["exchangeID"]."', 
											'".$_POST["transAmount"]."', '".$_POST["exchangeRate"]."', 
											'".$_POST["localAmount"]."', '".$_POST["IMFee"]."', 
											'".$_POST["totalAmount"]."', '".$benAgentIDs."',
											'".checkValues($_POST["transactionPurpose"])."', 											 											 
											NOW(), '".$_POST["transType"]."', 											 
											'".$_POST["currencyFrom"]."', '".$_POST["currencyTo"]."', '".$_POST["moneyPaid"]."',
											'".$_POST["bankCharges"]."','".$_POST["pincode"]."','CUSTOMER','$Status','$agentCountry',
											'".$_POST["benCountry"]."','".$_POST["collectionPointID"]."'
																	
											) 
											"; 
		
		if(insertInto($sql))
		{
			
			$fromName = SUPPORT_NAME;
			$fromEmail = SUPPORT_EMAIL;
			// getting admin email address.

			//$benContents = selectFrom("select email from ". TBL_BENEFICIARY ." where benID= '". $_POST["benID"] ."'");
			//$benEmail = $benContents["email"];
			// Getting Customer Email
			$custContents = selectFrom("select * from cm_customer where c_id= '". $_POST["customerID"] ."'");
			$custEmail = $custContents["c_name"];
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["FirstName"];
			$custMiddleName = $custContents["MiddleName"];
			$custLastName = $custContents["LastName"];
			$custCountry = $custContents["c_country"];
			$custCity = $custContents["c_city"];
			$custZip = $custContents["c_zip"];
			$custLoginName = $custContents["username"];												
			$custEmail = $custContents["c_email"];
			$custPhone = $custContents["c_phone"];


			$benContents = selectFrom("select * from cm_beneficiary where benID= '". $_POST["benID"] ."'");			
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];												
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];
			

			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentIDs ."'");			
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];


			$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $_POST["collectionPointID"] ."'");			
			$agentnamee = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];						
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			$subject = "Your transaction is created";
			
/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction is created </p></td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding 
	your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important 
	information. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$_POST['transType']." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> Transaction No:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$tran_date."  </td>
  </tr>
  <tr>
    <td><p>Authorised Date: </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Benificiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";
   
if(trim($_POST['transType']) == 'Bank Transfer')
{
  
$message.="  <tr>
    <td><p><strong>Benificiary Bank Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Bank Name:  ".$_POST['bankName']."  </td>
    <td> Acc Number:  ".$_POST['accNo']."  </td>
  </tr>
  <tr>
    <td> Branch Code:  ".$_POST['branchCode']."  </td>
    <td> Branch Address:  ".$_POST['branchAddress']."  </td>
  </tr>
  <tr>
    <td><p>Swift Code:  ".$_POST['swiftCode']."  </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
";
}
elseif(trim($_POST['transType']) == "Pick up")
{
$message .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collection Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}
$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$_POST['exchangeRate']."</td>
    <td> Amount:  ".$_POST['transAmount']." </td>
  </tr>
  <tr>
    <td> $preFix Fee:  ".$_POST['IMFee']." </td>
    <td> Local Amount:  ".$_POST['localAmount']." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$_POST['transactionPurpose']." </td>
    <td> Total Amount:  ".$_POST['totalAmount']." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$_POST['moneyPaid']." </td>
    <td> Bank Charges:  ".$_POST['bankCharges']." </td>
  </tr>  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>  
</table>
";
/**********************************************************/			

			sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

			$transID = @mysql_insert_id();
			
			

			
		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', $transID, $entryDate,'INSERTION','".$_POST["customerID"]."')");		
		}
					
			if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
			{
			
/*************************         c r e d i t  c a r d   i n f o r m a t i o n ******************************/

				/*if($transID > 0)
				{
					$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
					//$contentCreditCard = selectFrom("select * from cm_creditcard where transID='".$transID."'");	
					$contentCust = selectFrom("select * from cm_customer where c_id='".$contentTrans["customerID"]."'");
					$contentBenf = selectFrom("select * from cm_beneficiary where benID='".$contentTrans["benID"]."'");
					
					$query = "select * from cities where  country ='".$contentCust["c_country"]."'";
					$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
					$rstRow = mysql_fetch_array($nResult);	
					$custContryCode =  $rstRow["countryCode"];
					
					$query = "select * from cities where  country ='".$contentBenf["Country"]."'";
					$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
					$rstRow = mysql_fetch_array($nResult);	
					$benContryCode =  $rstRow["countryCode"];
						
					
					if($contentCust["c_country"] == "United Kingdom")
					{
						$query = "select * from states where  state  ='".$contentCust["c_state"]."'";
						$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);	
						$custStateCode =  $rstRow["code"];
						
						$query = "select * from states where  state  ='".$contentBenf["State"]."'";
						$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);	
						$benStateCode =  $rstRow["code"];		
					}
					else
					{
					$benStateCode = $contentCust["State"];
					}
					
					 $strXML = "	\"
							<DEBIT> 
							
								 <TRANSACTION_ID>".$contentTrans["refNumberIM"]."</TRANSACTION_ID>
								 <TRANSACTION_AMOUNT>".$contentTrans["refNumberIM"]."</TRANSACTION_AMOUNT>
								 <ORIGINATION_CURRENCY>GBP</ORIGINATION_CURRENCY> 
								 
								 <CARD_NUMBER>".$_POST["cardNo"]."</CARD_NUMBER> 
								 <EXPIRATION_DATE>".$_POST["expiryDate"]."</EXPIRATION_DATE> 
								 <CARD_TYPE>".$_POST["cardType"]."</CARD_TYPE>
								 <CARD_CVV>".$_POST["cvvNo"]."</CARD_CVV>
								 
								 <FIRST_NAME>".$contentCust["FirstName"]."</FIRST_NAME>
								 <LAST_NAME>".$contentCust["LastName"]."</LAST_NAME>
								 <ADDRESS>".$contentCust["c_address"]."</ADDRESS>
								 <ADDRESS_2>".$contentCust["c_address2"]."</ADDRESS_2>
								 <CITY>".$contentCust["c_city"]."</CITY>
								 <STATE>".$custStateCode."</STATE>
								 <POSTAL_CODE>".$contentCust["c_zip"]."</POSTAL_CODE>
								 <COUNTRY_CODE>".$custContryCode."</COUNTRY_CODE>
								 <TELEPHONE>".$contentCust["c_phone"]."</TELEPHONE>
								 <EMAIL>".$contentCust["c_email"]."</EMAIL>
								 <IP_ADDRESS>".$contentCust["accessFromIP"]."</IP_ADDRESS>
												 
								 <B_FIRST_NAME>".$contentBenf["firstName"]."</B_FIRST_NAME>
								 <B_LAST_NAME>".$contentBenf["lastName"]."</B_LAST_NAME>
								 <B_ADDRESS>".$contentBenf["Address"]."</B_ADDRESS>
								 <B_ADDRESS_2>".$contentBenf["Address1"]."</B_ADDRESS_2>
								 <B_CITY>".$contentBenf["City"]."</B_CITY>
								 <B_STATE>".$benStateCode."</B_STATE>
								 <B_POSTAL_CODE>".$contentBenf["Zip"]."</B_POSTAL_CODE>
								 <B_COUNTRY_CODE>".$benContryCode."</B_COUNTRY_CODE>
								 <B_TELEPHONE>".$contentBenf["Phone"]."</B_TELEPHONE>
								 <B_EMAIL>".$contentBenf["email"]."</B_EMAIL>
								 
						   </DEBIT>
						 \" ";
						exec("IMCreditAppJava.jar $strXML");
						
						sleep(10);
						$query = "select * from cm_creditcard order by cID ";
						$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
						while($rstRow = mysql_fetch_array($nResult))
						{								
							$AttemptCode  =  $rstRow["AttemptCode"];
							$PQTransID =  $rstRow["PQTransID"];
							$ApprovalCode  =  $rstRow["ApprovalCode"];		  
							$ResultCode   =  $rstRow["ResultCode"];
							$ResultText   =  $rstRow["ResultText"];
						}
						if($AttemptCode == "SUCC")
						{

						}
						elseif($AttemptCode == "INVR")
						{
							insertError("Possibly Invalid Request or Wrong XML Code");	
							redirect($backUrl);						
						}
						elseif($AttemptCode == "NORS")
						{
							insertError("Possible No Response from Host Server or link Problem");	
							redirect($backUrl);	
						}
						elseif($AttemptCode == "BADR")
						{
							insertError("Possible Response send from server but not Accurate");	
							redirect($backUrl);												
						}
						
				}			*/

/*************************         c r e d i t  c a r d   i n f o r m a t i o n ******************************/
			
				/*$sqlCreditCard = "insert into cm_creditcard
							( 
							transID ,cardType , cardNo , expiryDate ,cvvNo
							)
							Values
							(
							$transID,'".$_POST["cardType"]."', '".$_POST["cardNo"]."', '".$_POST["expiryDate"]."', '".$_POST["cvvNo"]."'	
							)
							";
				insertInto($sqlCreditCard); */ 					
		}
			
			if(trim($_POST["transType"]) == "Bank Transfer")
			{
			
				$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			

				$sqlBank = "insert into cm_bankdetails
							( 
							benID, bankName, 
							accNo, branchCode, branchAddress,
							ABACPF , IBAN,swiftCode 
							)
							Values
							(
							'".$_POST["benID"]."',  '".$_POST["bankName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."'
							)
							";
				insertInto($sqlBank);

		$queryUpdate="update cm_beneficiary set 
						 transType = '".$_POST["transType"]."'						
						 where benID='".$_POST["benID"]."'";
		update($queryUpdate);
		
		
		
			}
			$_SESSION["collectionPointID"] = "";
			$_SESSION["transTypes"] = "";
			$_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
			$_SESSION["bankCharges"] = "";	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			
			$_SESSION["cardType"]= "";
			$_SESSION["cardNo"]= "";
			$_SESSION["expiryDate"]= "";
			$_SESSION["cvvNo"]	= "";
		  	$_SESSION["pincode"] = "";
			
			insertError("Transaction has been added successfully for processing and $preFix reference number is $imReferenceNumber");
			redirect("thanks.php?msg1=Y");
		}
	}
	else
	{
		$queryUpdate = 	"update transactions set 
						customerID	='".$_POST["customerID"]."', 
						benID		='".$_POST["benID"]."',
						benAgentID	=$benAgentIDs,
						exchangeID	='".$_POST["exchangeID"]."',
						refNumber 	='".$_POST["refNumber"]."', 
						transAmount	='".$_POST["transAmount"]."', 
						exchangeRate='".$_POST["exchangeRate"]."', 
						localAmount	='".$_POST["localAmount"]."', 
						IMFee		='".$_POST["IMFee"]."',
						totalAmount	='".$_POST["totalAmount"]."', 
						transactionPurpose='".checkValues($_POST["transactionPurpose"])."',
						fundSources	='".checkValues($_POST["fundSources"])."',
						moneyPaid	='".$_POST["moneyPaid"]."', 
						transType	='".$_POST["transType"]."',
						toCountry= '".$_POST["benCountry"]."',
						fromCountry= '".$_POST["custCountry"]."',
						currencyFrom= '".$_POST["currencyFrom"]."', 
						currencyTo= '".$_POST["currencyTo"]."', 
						custAgentParentID= '".$_POST["custAgentParentID"]."', 
						benAgentParentID= '".$_POST["benAgentParentID"]."'
						where transID='".$_POST["transID"]."'";
		update($queryUpdate);
		
		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', '".$_POST["transID"]."', $entryDate,'UPDATION','".$_POST["customerID"]."')");		
		}		
		if(trim($_POST["transType"]) == "Bank Transfer")
		{
			$queryUpdateBank = "update bankDetails set 
								benID 		= '".$_POST["benID"]."', 
								bankName 	= '".$_POST["bankName"]."', 
								accNo		= '".$_POST["accNo"]."',
								branchCode	= '".$_POST["branchCode"]."', 
								branchAddress='".$_POST["branchAddress"]."',
								ABACPF 		= '".$_POST["ABACPF"]."',
								IBAN		= 	'".$_POST["IBAN"]."'
								where transID='".$_POST["transID"]."'";
			update($queryUpdateBank);
		}
		insertError("Transaction has been updated successfully.");
		redirect("add-transaction.php?msg1=Y");
	}
/*
transType : Bank Transfer
customerID : 2
benID : 2
benCountry : Pakistan

bankName : State BAnk
accNo : 123-132-123
branchCode : 123
branchAddress : asdafdasdasd
swiftCode : 
IBAN : 

refNumber : 123-123
transAmount : 500
exchangeRate : 58.8
exchangeID : 3
localAmount : 29400
IMFee : 20
totalAmount : 520
transactionPurpose : Family Assistant
fundSources : assad
moneyPaid : By Cash
Declaration : Y
*/
echo $str;
exit();

?>