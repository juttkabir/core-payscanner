<?
session_start();
include "header.php";
//require("../include/common.php");
//include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
session_register("Title");
session_register("firstName");
session_register("lastName");
session_register("middleName");
session_register("IDType");
session_register("IDNumber");
session_register("Address");
session_register("Address1");
session_register("Address2");
session_register("Country");
session_register("City");
session_register("Zip");
session_register("State");
session_register("Phone");
session_register("Mobile");
session_register("bentEmail");


session_register("bankName");
session_register("accountType");
session_register("branchCode");
session_register("branchAddress");
session_register("branchName");
session_register("bankCode");
session_register("branchCity");
session_register("swiftCode");
session_register("accNo");
session_register("ABACPF");
session_register("IBAN");
session_register("accountType");
session_register("ibanorbank");
session_register("serviceType");
session_register("beneficiaryName");

$benIDs = $_GET["benIDs"];

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
	$beneficiaryNameFlag = false;
if(defined("CONFIG_BENEFICIARY_NAME_WHOLE_FIELD_BEN") && CONFIG_BENEFICIARY_NAME_WHOLE_FIELD_BEN=="1"){
	$beneficiaryNameFlag = true;
}
// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
{
	$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
}

// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

// Fetch available ID Types
$query = "select * from id_types
			where active='Y'
			and show_on_ben_page='Y'";

$IDTypes = selectMultiRecords( $query );

// End of code against #3299: MasterPayex - Multiple ID Types


if($_GET["benIDs"] == "" && $_GET["msg"] == "")
{

	$_SESSION["Title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Address2"] = "";

	if ( isset($_SESSION["proveAddress"]) )
		unset($_SESSION["proveAddress"]);
	if ( isset($_SESSION["proveAddressType"]) )
		unset($_SESSION["proveAddressType"]);

	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["accountType"] 		= "";
	$_SESSION["bankCode"] 		= "";
	$_SESSION["branchName"] 		= "";
	$_SESSION["branchCity"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["accountType"] 			= "";
	$_SESSION["ibanorbank"] = "";
	$_SESSION["transType"] = "";
	$_SESSION["serviceType"] = "";
	$_SESSION["beneficiaryName"] = "";
}

if($_GET["benIDs"] == "")
{

	if ($_POST["agentID"] != "")
		$_SESSION["agentID2"] = $_POST["agentID"];
	if ($_POST["firstName"] != "")
		$_SESSION["firstName"] = $_POST["firstName"];
	if ($_POST["lastName"] != "")
		$_SESSION["lastName"] = $_POST["lastName"];
	if ($_POST["middleName"] != "")
		$_SESSION["middleName"] = $_POST["middleName"];
	if ($_POST["Title"] != "")
		$_SESSION["Title"] = $_POST["Title"];
	if ($_POST["IDType"] != "")
		$_SESSION["IDType"] = $_POST["IDType"];
	if ($_POST["IDNumber"] != "")
		$_SESSION["IDNumber"] = $_POST["IDNumber"];
	if ($_POST["Address"] != "")
		$_SESSION["Address"] = $_POST["Address"];
	if ($_POST["Address1"] != "")
		$_SESSION["Address1"] = $_POST["Address1"];
	if ($_POST["Address2"] != "")
		$_SESSION["Address2"] = $_POST["Address2"];

	if ($_REQUEST["proveAddress"] != "")
		$_SESSION["proveAddress"] = $_POST["proveAddress"];
	if ($_REQUEST["proveAddressType"] != "")
		$_SESSION["proveAddressType"] = $_POST["proveAddressType"];

	if ($_POST["Country"] != "")
		 $_SESSION["Country"] = $_POST["Country"];
	if($countryBasedFlag){
		if($_SESSION["Country"]=="")
			$_SESSION["Country"] = $_GET["Country"];
		if($_REQUEST["collectionPointID"]!="")
			$collectionPointIDV = $_REQUEST["collectionPointID"];
			
	}
	if($beneficiaryNameFlag){
		if ($_POST["beneficiaryName"] != "")
			$_SESSION["beneficiaryName"] = $_POST["beneficiaryName"];
	}
		/*if($_SESSION["Country"] != $_POST["Country"])
		{
			 $_SESSION["bankName"]="";
			 $_SESSION["branchName"]="";
			 $_SESSION["branchCode"]="";
		}*/	 
		
	
	if ($_POST["City"] != "")
		$_SESSION["City"] = $_POST["City"];
	if ($_POST["Zip"] != "")
		$_SESSION["Zip"] = $_POST["Zip"];
	if ($_POST["State"] != "")
		$_SESSION["State"] = $_POST["State"];
	if ($_POST["Phone"] != "")
		$_SESSION["Phone"] = $_POST["Phone"];
	if ($_POST["Mobile"] != "")
		$_SESSION["Mobile"] = $_POST["Mobile"];
	if ($_POST["benEmail"] != "")
		$_SESSION["benEmail"] = $_POST["benEmail"];
	if ($_POST["transType"] != "")
		$_SESSION["transType"] = $_POST["transType"];
		
	if ($_POST["bankName"] != "")
	{
		$_SESSION["bankName"] = $_POST["bankName"];
	}	$_SESSION["accountType"] 		= "";
	if ($_POST["accountType"] != "")
	{
		$_SESSION["accountType"] = $_POST["accountType"];
	}
	if ($_POST["branchCity"] != "")
		$_SESSION["branchCity"] = $_POST["branchCity"];

	if ($_POST["branchName"] != "")
	{
		if($_SESSION["branchName"] != $_POST["branchName"])
		$_SESSION["branchName"] = $_POST["branchName"];
	}
	if ($_POST["bankCode"] != "")
		$_SESSION["bankCode"] = $_POST["bankCode"];				
	if ($_POST["branchAddress"] != "")
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
	if ($_POST["swiftCode"] != "")
		$_SESSION["swiftCode"] = $_POST["swiftCode"];
	if ($_POST["accNo"] != "")
		$_SESSION["accNo"] = $_POST["accNo"];
	if ($_POST["ABACPF"] != "")
		$_SESSION["ABACPF"] = $_POST["ABACPF"];
	if ($_POST["IBAN"] != "")
		$_SESSION["IBAN"] = $_POST["IBAN"];
	if ($_POST["ibanorbank"] != "")
		$_SESSION["ibanorbank"] = $_POST["ibanorbank"];
	if ($_POST["accountType"] != "")
		$_SESSION["accountType"] = $_POST["accountType"];

/*	if ($_POST["branchCode"] != "")
		{
			$_SESSION["branchCode"] = $_POST["branchCode"];
			$branhCode = $_POST["branchCode"];
			$q = "select * from  cm_collection_point where cp_ria_branch_code  = '$branhCode'";
			$nResult = mysql_query($q)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
			$_SESSION["branchCity"] = $rstRow["cp_city"]; 			
			$_SESSION["branchAddress"] =$rstRow["cp_branch_address"]; 			
			$_SESSION["branchName"] =$rstRow["cp_branch_name"]; 																		
		}*/
if(!empty($_REQUEST["serviceType"]) && isset($_REQUEST["serviceType"]) && $_SESSION["Country"])
	$_SESSION["serviceType"] = $_REQUEST["serviceType"];
	
	$getCustomerID = ($_REQUEST["customerID"]!="" ? $_REQUEST["customerID"] : $_SESSION["loggedCustomerData"]["c_id"]);
}
elseif( $_GET["msg"] == "Y")
{

	/*$_SESSION["bankName"] = $_POST["bankName"];
	$_SESSION["branchCode"] = $_POST["branchCode"];
	$_SESSION["branchAddress"] = $_POST["branchAddress"];
	$_SESSION["swiftCode"] = $_POST["swiftCode"];
	$_SESSION["accNo"] = $_POST["accNo"];
	$_SESSION["ABACPF"] = $_POST["ABACPF"];
	$_SESSION["IBAN"] = $_POST["IBAN"];

	$_SESSION["Title"] = $_POST["Title"];
	$_SESSION["firstName"] = $_POST["firstName"];
	$_SESSION["lastName"] = $_POST["lastName"];
	$_SESSION["middleName"] = $_POST["middleName"];
	$_SESSION["IDType"] = $_POST["IDType"];
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
	$_SESSION["Address"] = $_POST["Address"];
	$_SESSION["Address1"] = $_POST["Address1"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["Zip"] = $_POST["Zip"];
	$_SESSION["State"] = $_POST["State"];
	$_SESSION["Phone"] = $_POST["Phone"];
	$_SESSION["Mobile"] = $_POST["Mobile"];
	$_SESSION["benEmail"] = $_POST["benEmail"];
	$_SESSION["customerID"] = $_POST["customerID"];
	
	$_SESSION["bankName"] = $_POST["bankName"];
	$_SESSION["branchCode"] = $_POST["branchCode"];
	$_SESSION["branchAddress"] = $_POST["branchAddress"];
	$_SESSION["swiftCode"] = $_POST["swiftCode"];
	$_SESSION["accNo"] = $_POST["accNo"];
	$_SESSION["ABACPF"] = $_POST["ABACPF"];
	$_SESSION["IBAN"] = $_POST["IBAN"];*/
}
else
{
	
	$strQuery = "SELECT * FROM cm_beneficiary where benID = $benIDs";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	$rstRow = mysql_fetch_array($nResult);				

		$_SESSION["agentID2"] = $rstRow["agentID"];
		$_SESSION["firstName"] = $rstRow["firstName"];
		$_SESSION["lastName"] = $rstRow["lastName"];
		$_SESSION["middleName"] = $rstRow["middleName"];
		$_SESSION["Title"] = $rstRow["Title"];
		$_SESSION["IDType"] = $rstRow["IDType"];
		$_SESSION["IDNumber"] = $rstRow["NICNumber"];
		$_SESSION["Address"] = $rstRow["Address"];
		$_SESSION["Address1"] = $rstRow["Address1"];
		$_SESSION["Address2"] = $rstRow["address2"];

		$_SESSION["proveAddress"] = $rstRow["proveAddress"];
		$_SESSION["proveAddressType"] = $rstRow["proveAddType"];

		$_SESSION["Country"] = $rstRow["Country"];
		$_SESSION["City"] = $rstRow["City"];
		$_SESSION["Zip"] = $rstRow["Zip"];
		$_SESSION["State"] = $rstRow["State"];
		$_SESSION["Phone"] = $rstRow["Phone"];
		$_SESSION["Mobile"] = $rstRow["Mobile"];
		$_SESSION["benEmail"] = $rstRow["email"];
		$_SESSION["IBAN"] = $rstRow["IBAN"];
		$_SESSION["ibanorbank"] = $rstRow["IbanOrBank"];
		$getCustomerID = ($rstRow["customerID"]!="" ? $rstRow["customerID"] : $_SESSION["loggedCustomerData"]["c_id"]);
		if($countryBasedFlag){
			$_SESSION["serviceType"] = $rstRow["serviceType"];
			$collectionPointIDV = $rstRow["collectionPointID"];
			if($_GET["collectionPointID"]!="" && $_GET["collectionPointID"]!=0)
				$collectionPointIDV = $_GET["collectionPointID"];
		}
		if($beneficiaryNameFlag){
			$_SESSION["beneficiaryName"] = $rstRow["beneficiaryName"];
		}
		//debug($rstRow);
	if($_POST["Country"] != "")		 
		  $_SESSION["Country"] = $_POST["Country"];
	else
		{
		  $_SESSION["Country"] = $rstRow["Country"];
		  if($_SESSION["Country"] == "")
		  	$_SESSION["Country"] = $_POST["Country"];
		}
		
	if($_POST["transType"] == "Select Transaction Type")		 
		  $_SESSION["transType"] = $_POST["transType"];
	else
		{
		  $_SESSION["transType"] =$rstRow["transType"];
		  if($_SESSION["transType"] == "")
		  	$_SESSION["transType"] = $_POST["transType"];
		}
		
		$_SESSION["Country"] = ucfirst(strtolower($_SESSION["Country"]));
		if(CONFIG_IBAN_ON_QUICK_BEN == "1" && CONFIG_IBAN_OR_BANK_TRANSFER!="1"){
			$strQuery = "SELECT * FROM " . TBL_CM_BANK_DETAILS . "  where benID = $benIDs";
		}
		if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") || CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
			$strQuery = "SELECT * FROM " . TBL_CM_BANK_DETAILS . "  where benID = $benIDs";
		}
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
		$rstRow = mysql_fetch_array($nResult);				
	
		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["accountType"] = $rstRow["accountType"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["bankCode"] = $rstRow["BankCode"];
		$_SESSION["branchCity"] = $rstRow["BranchCity"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["branchName"] = $rstRow["BranchName"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];
		$_SESSION["ABACPF"] = $rstRow["ABACPF"];

		// If its an update sender request, find the sender's ID Type values.
		if ( !empty( $benIDs ) )
		{
			// Now fetch values against available id types.
			$numberOfIDTypes = count( $IDTypes );
			for( $i = 0; $i < $numberOfIDTypes; $i++ )
			{
				$IDTypeValuesQ = "select * from cm_user_id_types
										where
											id_type_id = '" . $IDTypes[$i]["id"] . "'
										and	user_id = '" . $benIDs . "'
										and user_type = 'B'";
				$IDTypeValues = selectFrom( $IDTypeValuesQ );
				if ( !empty( $IDTypeValues ) )
				{
					$IDTypes[$i]["values"] = $IDTypeValues;
				}
			}
		}
	
		// End of code against #3299: MasterPayex - Multiple ID Types

		
}
if($_POST["Country"] != "")	
{
	
	if($_SESSION["Country"] != $_POST["Country"])
		{
		
/*			$_SESSION["bankName"]="";
			$_SESSION["branchName"]="";
			$_SESSION["branchAddress"]="";
			$_SESSION["branchCode"]="";*/
		}
	 // if($_SESSION["Country"]=='India')	
	  //	$_SESSION["branchCode"]="";
	  $_SESSION["Country"] = $_POST["Country"];
}
/*} else {
	$_SESSION["Country"] = CONFIG_DEFAULT_COUNTRY_VALUE;
}*/
	/*else
		{
		  $_SESSION["Country"] = $rstRow["Country"];
		  if($_SESSION["Country"] == "")
		  	{
		    	$_SESSION["Country"] = $_POST["Country"];
				$_SESSION["branchCode"]="";
			}
		}*/
////////Bank Name

if ($_POST["bankName"] != "")
		{
		$_SESSION["bankName"] = $_POST["bankName"];
/*				$_SESSION["branchName"]="";
				$_SESSION["branchAddress"]="";
				$_SESSION["branchCode"]="";*/
		}
/*else
		{
		  $_SESSION["bankName"] = $rstRow["bankName"];
		  if($_SESSION["bankName"] == "")
		  {
		    	$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchCode"]="";
			}
		}
	*/	
/////Bank Code
/*if ($_POST["bankCode"] != "")
	{
	$_SESSION["bankCode"] = $_POST["bankCode"];
	$_SESSION["branchCode"]="";
	}*/
/*else
		{
		  $_SESSION["bankCode"] = $rstRow["BankCode"];
		  if($_SESSION["bankCode"] == "")
		  	{
		    	$_SESSION["bankCode"] = $_POST["bankCode"];
				$_SESSION["branchCode"]="";
			}	
		}*/
//////Branch Address			
	if ($_POST["branchAddress"] != "")
		{
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
		//$_SESSION["branchCode"]="";
		
		}
		
	/*else
		{
		  $_SESSION["branchAddress"] = $rstRow["branchAddress"];
		  if($_SESSION["branchAddress"] == "")
		  	{
		    	$_SESSION["branchAddress"] = $_POST["branchAddress"];
				$_SESSION["branchCode"]="";
			}	
		}*/
/////Branch Name
	if ($_POST["branchName"] != "")
		{
/*		$_SESSION["branchName"] = $_POST["branchName"];
		$_SESSION["branchCode"]="";
		$_SESSION["branchAddress"]="";*/
		}
		
	/*else
		{
		  $_SESSION["branchName"] = $rstRow["BranchName"];
		  if($_SESSION["branchName"] == "")
		  {
		    	$_SESSION["branchName"] = $_POST["branchName"];
				$_SESSION["branchCode"]="";
			}	
		}*/
///////	

$showIbanBankSelectRow = true;
$useIbanBankStructure  = true;
$useServiceType = "";
if($countryBasedFlag){
	$destCountryService = $_SESSION["Country"];
	$useServiceType = $_SESSION["serviceType"];
	if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE") && $destCountryService=="")
	{
		$destCountryService = CONFIG_DEFAULT_COUNTRY_VALUE;
	}
	$destCountryQSer = selectFrom("select countryId from ".TBL_COUNTRY." where countryName = '".$destCountryService."'");
	$fromCountryQSer = selectFrom("select countryId from ".TBL_COUNTRY." as count,".TBL_CM_CUSTOMER." as cust where count.countryName = cust.c_country and cust.c_id='".$getCustomerID."'");
	$contentsService = selectMultiRecords("select serviceId,serviceAvailable from " . TBL_SERVICE_NEW." where 1 and fromCountryId = '".$fromCountryQSer["countryId"]."' and toCountryId = '".$destCountryQSer["countryId"]."' and currencyRec!=''");
	$allServices = array();
	for($as=0;$as<count($contentsService);$as++){
		$allServices[] = $contentsService[$as]["serviceAvailable"];
	}
	if($contentsService[0]["serviceAvailable"]!=""){
		if(in_array($_SESSION["serviceType"],$allServices)){
			$useServiceType = $_SESSION["serviceType"];
		}
		else{
			$useServiceType = $contentsService[0]["serviceAvailable"];
		}
	}
	else{
		$useServiceType = "";
	}
	if($_GET["serviceType"]!=""){
		$useServiceType = $_GET["serviceType"];
	}
	$showIbanBankSelectRow = false;
	if(strtoupper($useServiceType)=="IBAN"){
		$_SESSION['ibanorbank'] = "iban";
	}
	elseif(strtoupper($useServiceType)=="BANK TRANSFER" || strtoupper($useServiceType)=="BANK DETAILS" ){
		$_SESSION['ibanorbank'] = "bank";
	}
	elseif(strtoupper($useServiceType)=="BOTH"){
		$showIbanBankSelectRow = true;
		if($_SESSION['ibanorbank']!="" && ($_SESSION['ibanorbank']=="iban" || $_SESSION['ibanorbank']=="bank")){
			$_SESSION['ibanorbank'] = $_SESSION['ibanorbank'];
		}
		if($_GET['ibanorbank']!=""){
			$_SESSION['ibanorbank'] = $_GET['ibanorbank'];
		}
	}
	else{
		$useIbanBankStructure  = false;
	}
	
	if(!empty($_GET["ben_bank_id"]) || $_SESSION["ben_bank_id"]!="")
	{
		$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];
		/**
		 * Getting all the bank fields based on the selected bank from bank search
		 */
//		$benBanksDataQry = "select * from ".TBL_BEN_BANK_DETAILS." where id='".$_SESSION["ben_bank_id"]."'";
		$benBanksDataQry = "select * from ".TBL_BANKS." where id='".$_SESSION["ben_bank_id"]."'";
		if(!empty($benID) && $_GET["ben_bank_id"]=="")
			$benBanksDataQry = "select bb.id,bb.bankId,b.name,bb.branchCode,bb.branchAddress,bb.accountNo,bb.swiftCode,bb.accountType from ".TBL_BANKS." as b,".TBL_BEN_BANK_DETAILS." as bb where bb.benID='".$benID."'";
		$banBanksData = selectFrom($benBanksDataQry);
		$_SESSION["bankName1"] = $banBanksData["name"];
		$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
		$_SESSION["branchCode1"] = $banBanksData["branchCode"];
		$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
		$_SESSION["account1"] = $banBanksData["accountNo"];
		$_SESSION["accountType1"] = $banBanksData["accountType"];
		$_SESSION["country1"] = $_SESSION["Country"];
	}
	

	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

	// Fetch available ID Types
	if(defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY") && CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY=="1"){
		unset($IDTypes);
		$query = "select * from id_types
					where active='Y'
					and show_on_ben_page='Y' ";	
		if($useServiceType!=""){
			$idTypeServices = selectFrom("select multiple_ids from " . TBL_SERVICE_NEW." where 1 and fromCountryId = '".$fromCountryQSer["countryId"]."' and toCountryId = '".$destCountryQSer["countryId"]."' and currencyRec!='' AND serviceAvailable ='".$useServiceType."'");
			if($idTypeServices["multiple_ids"]!=""){
				$idTypeServicesVal = substr($idTypeServices["multiple_ids"],0,strlen($idTypeServices["multiple_ids"])-1);
				if($countryBasedFlag)
					$query.= " AND id IN(".$idTypeServicesVal.") ";
					$IDTypes = selectMultiRecords( $query );
			}
		}
	}
	
	// If its an update sender request, find the sender's ID Type values.
	if ( !empty($benIDs) )
	{
		// Now fetch values against available id types.
		$numberOfIDTypes = count( $IDTypes );
		for( $j = 0; $j < $numberOfIDTypes; $j++ )
		{
			$IDTypeValuesQ = "select * from cm_user_id_types
									where
										id_type_id = '" . $IDTypes[$j]["id"] . "'
									and	user_id = '" . $benIDs . "'
									and user_type = 'B'";
			$IDTypeValues = selectFrom( $IDTypeValuesQ );
			if ( !empty( $IDTypeValues ) )
			{
				$IDTypes[$j]["values"] = $IDTypeValues;
			}
		}
	}
	// End of code against #3299: MasterPayex - Multiple ID Types
}
?>
<HTML>
<HEAD>
<TITLE>Add Beneficiary</TITLE>
<SCRIPT TYPE="text/javascript" SRC="javascript/jquery.js"></SCRIPT>
<SCRIPT LANGUAGE="javascript" SRC="./javascript/functions.js"></SCRIPT>
<SCRIPT LANGUAGE="javascript" SRC="../admin/javascript/jquery.selectboxutils.js"></SCRIPT>
<LINK HREF="images/interface.css" REL="stylesheet" TYPE="text/css">
<SCRIPT LANGUAGE="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {

	var strErr = "";
	strEmpty = '/^\s*$/';
	strDigit = '/^\d+$/';
	strEml = '/^[A-Za-z0-9-_.]+\@[A-Za-z0-9-_]+.[A-Za-z0-9-_.]+$/';
	strLogin = '/^[A-Za-z0-9-_]+$/';

	<?
		/**
		 * Code added to implement the IBAN validation functionality.
		 * @Ticket: #3544: Now Transfer - Online Module Enhancements
		 * @Author: Usman Ghani.
		 */
		 // The following javascript condition has been defined by Mr. Jahangir Alam. I used it as it is for convenience.
		if(defined('CONFIG_IBAN_ON_QUICK_BEN ') && CONFIG_IBAN_ON_QUICK_BEN == "1")
		{
			?>
				if( document.getElementById('IBAN').value == "" )
				{
					alert('Please provide IBAN number.');
					document.getElementById('IBAN').focus();
					return false;
				}
				
				if( document.getElementById('ibnStatus').value == 0 )
				{
					alert('The IBAN number '+ document.getElementById("IBAN").value +' is invalid.');
					return false;
				}
			<?
		}
	?>	
<? if($countryBasedFlag){  ?>
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the Beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }
	if(document.getElementById('serviceType') == null){
    	alert("Please Use Services for this Country before creating Beneficiary. Or select Other Country.");
        theForm.Country.focus();
        return false;	
	}
	if(document.getElementById('serviceType').value == ""){
    	alert("Please select the Beneficiary's Service Type.");
        theForm.serviceType.focus();
        return false;
    }
<? } ?>	
<? if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1" && $useIbanBankStructure){  ?>	
	var ibanOrBank = document.getElementById('IbanOrBank').value;
	var ibnstatus = document.getElementById('ibnStatus').value;
	var ibanValue = document.getElementById("IBAN").value;
	var selection = theForm.ibanbank; // radio buttons
	var ibanbankRadio = "";
	for (i=0; i<selection.length; i++){
		if (selection[i].checked == true){
			ibanbankRadio = "y";
		}
	}
	var serviceTypeObj = document.getElementById("serviceType");
	var serviceTypeVal = "";
	if(serviceTypeObj!=null){
		var serviceTypeVal = serviceTypeObj.value;
	}
	if(serviceTypeVal=="IBAN" || serviceTypeVal=="Bank Transfer"){
		ibanbankRadio="y";
	}
	if(ibanbankRadio !="" ){
		if(ibanOrBank == "i")
		{		
			if(ibanValue=="" || ibnstatus==0)
			{
					alert('The IBAN number '+ document.getElementById("IBAN").value +' is invalid.');
					theForm.IBAN.focus();
					return false;
			}
	
			/*
			var retText = document.getElementById("benIbanValidStatus").innerHTML;
			if(subText != "gre" && subText != "en>")
			{
				alert('The IBAN number '+ document.getElementById("iban").value +' is invalid.');
				return false;
			}
			*/
		}
		else if(ibanOrBank == "b" ){
			if(theForm.bankName.value == "")
			{
				alert("Please enter the bank name.");
				theForm.bankName.focus();
				return false;
			}
			if(theForm.accNo.value == "")
			{
				alert("Please enter the account number.");
				theForm.accNo.focus();
				return false;
			}
			if(theForm.bankCode.value == "")
			{
				alert("Please enter the bank code.");
				theForm.bankCode.focus();
				return false;
			}
			if(theForm.branchName.value == "")
			{
				alert("Please enter the branch name.");
				theForm.branchName.focus();
				return false;
			}
			if(theForm.branchCity.value == "")
			{
				alert("Please enter the branch city.");
				theForm.branchCity.focus();
				return false;
			}
		}
	}
	else{
		if(serviceTypeObj!=null && serviceTypeVal==""){
			alert("Please select an Option from Service(s).");			
		}
		else{
			alert("Please select an Option for IBAN or Bank Details.");
		}
		return false;
	}
<? } ?>
	if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
    	alert("Please provide the Beneficiary's first name.");
        theForm.firstName.focus();
        return false;
    }
	if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
    	alert("Please provide the Beneficiary's last name.");
        theForm.lastName.focus();
        return false;
    }
	/*if(theForm.IDNumber.value == "" || IsAllSpaces(theForm.IDNumber.value)){
    	alert("Please provide the Beneficiary's ID Number.");
        theForm.IDNumber.focus();
        return false;
    }*/
	

	<?
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY")
		&& CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == '1' 
		&& !empty( $IDTypes ))
	{
		?>
			if ( checkIdTypes( theForm ) != true )
				return false;
		<?
	}
	// End of code against #3299: MasterPayex - Multiple ID Types
?>

	
	
	
	if(theForm.Address.value == "" || IsAllSpaces(theForm.Address.value)){
    	alert("Please provide the Beneficiary's address line 1.");
        theForm.Address.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the Beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }

	if(theForm.City.value == ""){
    	alert("Please provide the Beneficiary's city.");
        theForm.City.focus();
        return false;
    }
	
	if(theForm.State.value == "" || theForm.State.options.selectedIndex == 0){
    	alert("Please provide the Beneficiary's state.");
        theForm.State.focus();
        return false;
    }

<? if ($_SESSION["Country"] == "United States" || $_SESSION["Country"] == "Canada" || $_SESSION["Country"] == "United Kingdom" || strtolower($_SESSION["Country"]) == strtolower("Poland")){ ?>
	if(theForm.Zip.value == "" || IsAllSpaces(theForm.Zip.value))
	{
		alert("Please provide the Zip/Postal Code.");
		theForm.Zip.focus();
		return false;
	}
<? }?>
<?
	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		?>
		/*
			alert(theForm.Phone.value.length);
			if ( theForm.Phone != null && theForm.Phone.value == "" && validatePhoneMobile() == false)
			{
				alert("Please provide phone number.");
				theForm.Phone.focus();
				return false;
			}
			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
		*/
		<?
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile
?>

<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{ /*
		?>
			if ( validatePhoneMobile() == false )
				return false;

			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
		<?
		*/
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>

	if ( theForm.phoneType != null && theForm.phoneType.value == "" )
	{
		alert("Please select phone type.");
		theForm.phoneType.focus();
		return false;
	}
	if(theForm.Phone.value == "" || IsAllSpaces(theForm.Phone.value)){
    	alert("Please provide the Beneficiary's phone number.");
        theForm.Phone.focus();
        return false;
    }
	
	
	var Phone=document.getElementById("Phone");
	if(document.getElementById("phoneType").value == "phone")
	{
		if((Phone.value==null)||(Phone.value==""))
		{
			alert("Please Enter your Phone Number");
			Phone.focus();
			return false;
		}
		if (phoneNumberRelatedValidation(Phone.value, "p")==false)
		{
			alert("Please Enter a Valid Phone Number");
			Phone.focus();
			return false;
		}
	}
	else if(document.getElementById("phoneType").value == "mobile")
	{
		if((Phone.value==null)||(Phone.value==""))
		{
			alert("Please Enter your Mobile Number");
			Phone.focus();
			return false;
		}
		if (phoneNumberRelatedValidation(Phone.value, "m")==false)
		{
			alert("Please Enter a Valid Mobile Number");
			Phone.focus();
			return false;
		}
	}
	
	/**
	if(theForm.benEmail.value != "" )
	{
			if(!theForm.benEmail.value.match(strEml))
			{
				alert("Invalid email format provided.\n");					
				theForm.benEmail.focus();
				return (false);
			}	
    }
	
	emailField = document.getElementById("benEmail");

	if ( emailField.value == "" )
	{
		alert("Please provide email address.");
		emailField.focus();
		return false;
	}
	else if ( !( strEml.test(emailField.value) ) )
	{
		alert("Please provide a correct email address.");
		emailField.focus();
		return false;
	}
	**/
	return true;
}

<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{
		?>
			function validatePhoneMobile()
			{
				Phone = document.getElementById("Phone");
				Mobile = document.getElementById("Mobile");
				alert(Phone.value.length);
				if ( Phone != null && Phone.value != ""
					&& !( /^\d+$/.test(Phone.value) ) 
					&& Phone.value.length != 11 ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in phone number.");
					Phone.focus();
					return false;
				}

				if ( Mobile != null && Mobile.value != ""
					&& !( /^\d+$/.test(Mobile.value) ) 
					&& (Mobile.length != 10 && Mobile.length != 11)) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in mobile number.");
					theForm.Mobile.focus();
					return false;
				}
				return true;
			}

			function phoneEventHandler( e )
			{
				keynum = -1;
				if(window.event) // IE
				{
					keynum = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					keynum = e.which;
				}

				if ( 
						keynum > -1
					&& 	keynum != 8 	// Backspace
					&& 	keynum != 13	// Enter
					&& 	keynum != 27	// Escape
					 )
				{
					validatePhoneMobile();
				}
			}
		<?
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
<?
	/**
	 * Code added to implement the IBAN validation functionality.
	 * @Ticket: #3544: Now Transfer - Online Module Enhancements
	 * @Author: Usman Ghani.
	 */
?>
	function validateIban()
	{
		vts = $("#IBAN").val();

		if(vts == "")
		{
			$("#formSubmitButton").removeAttr("disabled");
			$("#ibnStatus").val(0);
			$("#benIbanValidStatus").html("&nbsp;");
			return;
		}
		else
		{
				$.ajax
				({
					type: "GET",
					url: "validateIban.php",
					data: "iban_num="+vts,
					beforeSend: function(XMLHttpRequest)
					{
						$("#formSubmitButton").attr("disabled", "disabled");
					},
					success: function(msg)
					{
						if(msg == '1')
						{
							$("#ibnStatus").val(1);						
							$("#benIbanValidStatus").html("<font color='green'>The IBAN number is valid.</font>");						
							$("#formSubmitButton").removeAttr("disabled");
						}
						else
						{
							$("#ibnStatus").val(0);
							$("#benIbanValidStatus").html("<font color='red'>You have entered an invalid IBAN number.</font>");
							$("#formSubmitButton").attr("disabled", "disabled");
						}
				   }
				});
		}
		
	}
<?
	/**
	 * End of Usman's code against #3544: Now Transfer - Online Module Enhancements
	 */
?>
function proveAddType()
{
	if($("#proveAddress").attr("checked") == true)
		$("#proveAddressTypeID").show();
	else
		$("#proveAddressTypeID").hide();
}

function bodyOnLoadJS()
{
	if (proveAddType != null)
		proveAddType();
	<?	if(CONFIG_IBAN_ON_QUICK_BEN == "1" && !empty($_REQUEST["benIDs"]) && CONFIG_IBAN_OR_BANK_TRANSFER!="1") { 	?>		
	<?	//if(!empty($_REQUEST["benIDs"])) { 	?>
		validateIban();
	<?  } ?>
<? if(CONFIG_IBAN_ON_QUICK_BEN == "1" || CONFIG_IBAN_OR_BANK_TRANSFER=="1"){?>
	<? if($countryBasedFlag!=true){?>
		$("#ibanbank").click();
	<? }?>
	<? if($_SESSION['ibanorbank']=="iban"){?>
		$("#formSubmitButton").attr('disabled', true); 
		validateIban();
	<? }?>
<? } ?>
}


// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;
var maxDigitsInIPhoneNumber = 11;

var validWorldPhoneChars = phoneNumberDelimiters;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function phoneNumberRelatedValidation(strPhone, strType)
{
	var strPhone=trim(strPhone);
	var s=stripCharsInBag(strPhone,validWorldPhoneChars);
	if(strType == "p")
		return (isInteger(s) && (s.length == minDigitsInIPhoneNumber || s.length == maxDigitsInIPhoneNumber));
	else
		return (isInteger(s) && s.length == maxDigitsInIPhoneNumber);
}
function showHideIbanBank(id){
	var IbanOrBankOrReset = id.value;
	var selection = document.frmBenificiary.ibanbank; // radio buttons
	if(IbanOrBankOrReset == "iban"){
		document.getElementById("bankData").style.display="none";
		document.getElementById("ibanData").style.display="block";
		document.getElementById("IbanOrBank").value="i";
		validateIban();
	}
	else if(IbanOrBankOrReset == "bank"){
		document.getElementById("ibanData").style.display="none";
		document.getElementById("bankData").style.display="block";
		document.getElementById("IbanOrBank").value="b";
		document.getElementById('formSubmitButton').disabled = false;
	}
	// This functionality if Clear Button is pressed.
	else if(IbanOrBankOrReset == " Clear "){
//		document.getElementById("searchBankName").value="";
//		document.getElementById("country1").value="";
		document.getElementById("bankName").value="";
		document.getElementById("accNo").value="";
		document.getElementById("bankCode").value="";
		document.getElementById("branchCode").value="";
		document.getElementById("branchName").value="";
		document.getElementById("branchCity").value="";
		document.getElementById("branchAddress").value="";
		document.getElementById("swiftCode").value="";
		document.frmBenificiary.accountType[0].checked=true;
		
		var selection = document.frmBenificiary.ibanbank; // radio buttons
		for (i=0; i<selection.length; i++){
			selection[i].checked = false;
		}
		
		document.getElementById("IBAN").value="";
		document.getElementById("ibnStatus").value=0;
		document.getElementById("benIbanValidStatus").innerHTML = "";
		// hide both details IBAN and Bank if any of them is displayed.
		document.getElementById("bankData").style.display="none";
		document.getElementById("ibanData").style.display="none";
		document.getElementById('formSubmitButton').disabled = false;
	}
}


</SCRIPT>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<STYLE TYPE="text/css">
.mandatoryMarker {
	color: #FF0000;
}
</STYLE>
</HEAD>
<BODY onLoad="bodyOnLoadJS();">
 <FORM id="frmBenificiary" NAME="frmBenificiary" ACTION="add-ben-conf.php?from=<? echo $_GET["from"]?>" METHOD="post" >
<INPUT TYPE="hidden" NAME="benID" id="benID" VALUE="<? echo $_GET["benIDs"];?>">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="5">
  <TR>
      <TD COLSPAN="2" ALIGN="center" VALIGN="top"> 
	  <TABLE WIDTH="686" BORDER="0" CELLSPACING="1" CELLPADDING="2" ALIGN="center">
	  	    <TR HEIGHT="30">
    <TD ALIGN="left" BGCOLOR="#6699cc"><strong><FONT COLOR="#FFFFFF" SIZE="2">&nbsp;Add Beneficiary</FONT></strong></TD>
  </TR>
          <TR> 
            <TD COLSPAN="2" BGCOLOR="#000000"> <TABLE WIDTH="100%" CELLPADDING="2" CELLSPACING="0" BORDER="0" BGCOLOR="#FFFFFF">
                <TR> 
                  <TD ALIGN="center" BGCOLOR="#DFE6EA"> <FONT COLOR="#000066" SIZE="2"><STRONG>Add 
                    Beneficiary</STRONG></FONT></TD>
                </TR>
              </TABLE></TD>
          </TR>
          <? if ($_GET["msg"] != ""){ ?>
          <TR BGCOLOR="#ededed">
            <TD COLSPAN="2" BGCOLOR="#FFFFCC"><TABLE WIDTH="100%" CELLPADDING="5" CELLSPACING="0" BORDER="0">
                <TR>
                  <TD WIDTH="40" ALIGN="center">
				  	<FONT SIZE="5" COLOR="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>">
						<strong>
							<I>
								<? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?>
							</I>
						</strong>
					</FONT>
				  </TD>
                  <TD>
				  	<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><strong>".$_SESSION['error']."</strong></font>"; $_SESSION['error'] = "";?>
				  </TD>
                </TR>
              </TABLE></TD>
          </TR>
          <? } ?>
          <TR BGCOLOR="#ededed"> 
            <TD HEIGHT="19" COLSPAN="2" ALIGN="center"> <SPAN CLASS="mandatoryMarker">* Compulsory Fields&nbsp;&nbsp;&nbsp;&nbsp;+ Can be Compulsory Fields with respect to Country Selection </SPAN></TD>
          </TR>
          <TR> 
            <TD VALIGN="top" COLSPAN="2" BGCOLOR="#ffffff">
              <TABLE WIDTH="100%" CELLPADDING="2" CELLSPACING="1" BORDER="0">
			  	<?	if(CONFIG_IBAN_ON_QUICK_BEN == "1")	{ /* ?>
                <TR BGCOLOR="#ededed">
                  <TD HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>IBAN <SPAN CLASS="mandatoryMarker">*</SPAN></strong></FONT></TD>
                  <TD COLSPAN="3" ALIGN="left">
					<INPUT NAME="IBAN" TYPE="text" ID="IBAN" onBlur="validateIban();" VALUE="<?=$_SESSION["IBAN"]; ?>" SIZE="50" MAXLENGTH="50">
					&nbsp;<SPAN ID="benIbanValidStatus"></SPAN>
					<INPUT TYPE="hidden" ID="ibnStatus" VALUE="0">
				  </TD>
                </TR>
				<? */ } ?>
				<TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Country <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left"> 
				  <?
				 // echo $_SESSION["Country"];
				  ?>
				  <SELECT NAME="Country" STYLE="font-family:verdana; font-size: 11px" onChange="document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&benIDs=<?  echo $_GET["benIDs"]?>&serviceType=<?=$_REQUEST["serviceType"]?>&collectionPointID=<?=$collectionPointIDV?>'; document.forms[0].submit();  <? if($_SESSION["transType"] == "Bank Transfer") {?> document.frmBenificiary.action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&benIDs=<? echo $_GET["benIDs"]?>'; document.frmBenificiary.submit(); <? } ?>">
                       <OPTION VALUE="">-Select Country-</OPTION>
                      <!--option value="United States">United States</option>
                      <option value="Canada">Canada</option-->
                      <?
                      
                       if(CONFIG_ENABLE_ORIGIN == "1"){
						         		$countryTypes = " and countryType like '%destination%' ";
						          	}else{
						        		$countryTypes = " ";
						         	}
							        if(CONFIG_COUNTRY_SERVICES_ENABLED){
										$countryServiceQ = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName";
										   if($countryBasedFlag){
											   $countryServiceQ = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE_NEW." where 1 and countryId = toCountryId  $countryTypes order by countryName";
										   }
								          	$countires = selectMultiRecords($countryServiceQ);
											}else{
												$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
											}
					//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." where 1 order by country");
					for ($i=0; $i < count($countires); $i++)
					{
						$selected = "";
						if ( empty( $_SESSION["Country"] ) )
						{
							// Code added by Usman Ghani aginst ticket #3469 Now Transfer - Default Beneficiary Country
							if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE"))
							{
								if ( $countires[$i]["countryName"] == CONFIG_DEFAULT_COUNTRY_VALUE )
									$selected = "selected='selected'";
							}
							// End of code aginst ticket #3469 Now Transfer - Default Beneficiary Country
						}
						else if($countires[$i]["countryName"] == $_SESSION["Country"])
						{
							// Code modified by Usman Ghani against ticket #3469 Now Transfer - Default Beneficiary Country
							$selected = "selected='selected'";
						}
						?>
							<OPTION  value="<? echo $countires[$i]["countryName"]; ?>" <?=$selected;?> >
								<? echo $countires[$i]["countryName"]; ?>							</OPTION>
						<?
/*						if($countires[$i]["countryName"] == $_SESSION["Country"])
						{
						
							?>
							  <option  value="<? echo $_SESSION["Country"];?>" selected> 
								  <? echo $_SESSION["Country"];?>
							  </option>
							<?
						}
						else
						{
							?>
						  		<option value="<?=$countires[$i]["countryName"]; ?>"> 
						  			<?=$countires[$i]["countryName"]; ?>
						  		</option>
		
							<?
						} */
					}/*
					if($_SESSION["Country"] != "")
					{
				?>
				<option selected><? echo $_SESSION["Country"];?></option>
				<?
				}*/
				?>
                    </SELECT> 				  
					<?
					
				  //echo $_SESSION["Country"];
				  ?>
                  <SCRIPT LANGUAGE="JavaScript">
						SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                  </SCRIPT></TD>
				  <?
				  	/*
						// Important note:
						// Following html code has been temporarily commented out by Usman Ghani. It should not be deleted from here.
                  <td width="20%" align="right"><font color="#005b90"><strong>
                    <?							
							if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom")
								echo "City";
							else
								echo "Zip/postal code";
					?>
						
                     <span class="mandatoryMarker">*</span>&nbsp;</strong></font></td>
                  <td width="37%" align="left">
				  	<? 
						if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom")
						{
					?>
							<input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25">
					<? 	} else { ?>
							<input type="text" name="Zip" value="<? echo stripslashes($_SESSION["Zip"]); ?>" maxlength="7">
					<? } ?>
				  </td>
				  */ 
				  ?>
				<?
				if($countryBasedFlag && $useServiceType!=""){
				?>
					<td height="20" align="right"><font color="#005b90"><strong>Services<font color="#ff0000">*</font>&nbsp;</strong></font></td>
					<td>
					<select style="font-family: verdana; font-size: 11px;" id="serviceType" name="serviceType" onChange="document.forms[0].action='add-beneficiary.php?from=<? echo $_GET["from"];?>&transID=<? echo $_GET["transID"];?>&benIds=<? echo $_GET["benIds"];?>&collectionPointID=<?=$collectionPointIDV?>'; document.forms[0].submit();">
							<? $selected = "";	?>
						<? for($sA=0;$sA<count($contentsService);$sA++){?>
							<option value="<?=$contentsService[$sA]["serviceAvailable"]?>"><?=$contentsService[$sA]["serviceAvailable"]?></option>
						<? }?>
						</select>
				<script language="JavaScript">SelectOption(document.forms[0].serviceType, "<?=$useServiceType; ?>");</script>
					</td>
				<? } elseif($countryBasedFlag){?>
					<td height="20" align="left" colspan="2"><font color="#005b90"><strong>Services not avaiable for this Country</strong></font></td>
				<? }else{?>
					<td height="20" align="left" colspan="2">&nbsp;</td>
				<? }?>
                </TR>
<?
		/**
   		 * @ticket# 4561
		 * Now Beneficiaries will have IBAN and Bank details displayed
		 * Controlled via a radio button.
   		 */
 if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1"){ ?>
  <?
// This scheme is made to hadle radio buttons and its data displayed 
// in different cases.
$displayTable = "style='display:block'";
$checkRadioIban = "checked='checked'";
$showIbanTable = "style='display:none'";
$showBankTable = "style='display:none'";
$showRadioIban = "";
$showRadioBank = "";
if($_SESSION['ibanorbank'] == "iban"){
	$showRadioIban = $checkRadioIban;
	$showIbanTable = $displayTable;
}
elseif($_SESSION["ibanorbank"]=="bank"){
	$showRadioBank = $checkRadioIban;
	$showBankTable = $displayTable;
}
?>
 <!-- This is layour for selecting IBAN or BANK Details. -->
<? if($useIbanBankStructure){?>
			<? if($showIbanBankSelectRow){?>
						<tr bgcolor="#ededed">
											<td colspan="2" align="right"><font color="#005b90"><strong>Select an Option</strong></font><font color="red"> *</font><font color="#005b90"><strong> :</strong></font> </td>
											<td align="left"><input type="radio" id="ibanbank" name="ibanbank" value="iban" onClick="showHideIbanBank(this);"	<?=$showRadioIban; ?>>
												&nbsp;&nbsp;<strong>IBAN</strong> </td>
											<td align="left"><input type="radio" id="ibanbank"  name="ibanbank" value="bank" onClick="showHideIbanBank(this);" <?=$showRadioBank; ?>>
												&nbsp;&nbsp;<strong>Bank Details</strong> </td>
								</tr>
			<? }else{
	if($useServiceType=="IBAN"){
		echo "<input type='hidden' id='ibanbank' name='ibanbank' value='iban' ><script>showHideIbanBank(ibanbank);</script>";
	}
	elseif($useServiceType=="Bank Transfer"){
		echo "<input type='hidden' id='ibanbank' name='ibanbank' value='bank'><script>showHideIbanBank(ibanbank);</script>";
	}
			}?>
 <!-- Layout for IBAN/BANK DETAILS Starts. -->
						<tr bgcolor="#ededed">
											<td colspan="4">
 <!-- IBAN layout starts. -->
											<table cellpadding="2" cellspacing="1" border="1" id="ibanData"  <?=$showIbanTable;?> width="100%">
													<tr bgcolor="#ededed">
														<td height="20" width="15%" align="right"><font color="#005b90"><strong>IBAN</strong></font>&nbsp;<font color="red">*</font> </td>
														<td  width="25%" ><input type="text" name="IBAN" id="IBAN" value="<?=$_SESSION["IBAN"];?>" maxlength="50" onBlur="validateIban();" size="35">
														</td>
														<td colspan="2">
														<SPAN ID="benIbanValidStatus">&nbsp;</SPAN>
															<input type="hidden" id="ibnStatus" name="ibnStatus" value="0">
														</td>
													</tr>
												</table>
 <!-- IBAN layout ends. -->
 <!-- Bank Details layout starts. -->
											<table cellpadding="2" cellspacing="1" border="1" id="bankData"  <?=$showBankTable;?> width="100%">
													<tr>
														<td colspan="4" style="background:#CCCCCC;"><font color="#005b90"><strong>Beneficiary Bank Details</strong></font></td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="25%" height="20" align="right"><font color="#005b90"><strong>Bank Name<font color="red">*</font></strong></font> </td>
														<td width="26%"><input type="text" name="bankName" id="bankName" value="<?=$_SESSION["bankName"]?>" maxlength="30">														</td>
														<td width="22%"><font color="#005b90"><strong>Account Number<font color="red">*</font></strong></font></td>
														<td width="27%"><input type="text" name="accNo" id="accNo" value="<?=$_SESSION["accNo"]?>" maxlength="30"></td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="25%" height="20" align="right"><font color="#005b90"><strong>Bank Code <font color="red">*</font></strong></font> </td>
														<td width="26%"><input type="text" name="bankCode" id="bankCode" value="<?=$_SESSION["bankCode"]?>" maxlength="30"></td>
														<td><font color="#005b90"><strong>Branch Code </strong></font></td>
														<td><INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<?=$_SESSION["branchCode"]; ?>" MAXLENGTH="25"></td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="25%" height="20" align="right"><font color="#005b90"><strong>Branch Name&nbsp;<font color="red">*</font> </strong></font> </td>
														<td width="26%"><input type="text" name="branchName" id="branchName" value="<?=$_SESSION["branchName"]?>" maxlength="30">														</td>
														<td width="22%"><font color="#005b90"><strong>Branch City <font color="red">*</font></strong></font></td>
														<td width="27%"><INPUT NAME="branchCity" TYPE="text" ID="branchCity" VALUE="<?=$_SESSION["branchCity"]; ?>" ></td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="25%" height="20" align="right" valign="top"><font color="#005b90"><strong>Branch Address&nbsp;</strong></font> </td>
														<td width="26%"><input name="branchAddress" type="text" id="branchAddress" value="<?=$_SESSION["branchAddress"]; ?>" maxlength="254"></td>
														<td width="22%"><font color="#005b90"><strong>Swift Code</strong></font></td>
														<td width="27%"><INPUT NAME="swiftCode" TYPE="text" ID="swiftCode" VALUE="<?=$_SESSION["swiftCode"]; ?>" MAXLENGTH="25">
&nbsp;<A HREF="http://www.swift.com/biconline/index.cfm?fuseaction=display_freesearch" TARGET="_blank"><FONT COLOR="#0000FF">Help</FONT></A></td>
													</tr>
													<tr bgcolor="#ededed">
														<td width="25%" height="20" align="right" valign="top"><font color="#005b90"><strong>Select account type: </strong></font>&nbsp;</td>
														<td colspan="3"><input name="accountType" type="radio" id="radio" value="Regular" <? echo ($_SESSION["accountType"] != "Savings" ? "checked"  : "")?>>
Regular<br>
<input name="accountType" type="radio" id="radio" value="Savings" <? echo ($_SESSION["accountType"] == "Savings" ? "checked"  : "")?>>
Savings
<input type="hidden" name="usedBankId2" value="<?=$_SESSION["ben_bank_id"]?>" /></td>
													</tr>
													<?
							//}
						?>
												</table>
 <!-- Bank Details layout ends. -->
 <!-- This hidden field decides on which Layout validation is to be done for saving data. -->
							</td>
						</tr>
								<input type="hidden" id="IbanOrBank" name="IbanOrBank" value="<?=($_SESSION["ibanorbank"]=="bank"?"b":"i")?>">
<? 
}
elseif($countryBasedFlag && ($useServiceType == "Pick up" || $useServiceType == "Home Delivery"))
{ 
?>
						<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right"><font color="#005b90"><b>Collection Point Name</b></font></td>
									<td width="26%" colspan="3" align="left"><INPUT NAME="agentCity" TYPE="text" ID="agentCity" SIZE="15">
									&nbsp;&nbsp;&nbsp;
			                          <INPUT TYPE="button" NAME="SubmitColl" VALUE="Search Collection Point" onClick=" if(document.frmBenificiary.agentCity.value == '' || IsAllSpaces(document.frmBenificiary.agentCity.value)) { alert ('Please provide Collection Point to search.') } else { window.open('search-agent-city.php?agentCity=' + document.frmBenificiary.agentCity.value + '&from=<? echo $_GET["from"];?>&transID=<? echo $_GET["transID"]?>' +  '&benID=<?=$benIDs?>&benCountry=<? echo $_SESSION["Country"]?>'+  '&serviceType=<?=$useServiceType?>', 'agentCity', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=800') }">
									  </td>
						</tr>			  
					  
                      <? 
		
		$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='".$collectionPointIDV."' and a.agentStatus = 'Active'";
			$senderAgentContent = selectFrom($queryCust);
			$senderAgentContent["cp_id"];
			$dist = $senderAgentContent["cp_ida_id"];
/*			$_SESSION["distribut"] = $dist;
			$_SESSION["benAgentID"] = $dist;*/
			 $queryDistributor = "select *  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			
			if($senderAgentContent["cp_id"] != "")
			{
		?>
					<tr bgcolor="#ededed">
					<td colspan="4">
					<TABLE WIDTH="100%" BORDER="1" CELLPADDING="2" CELLSPACING="2">                         <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Collection point name</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Contact Person </FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_contact_person_name"]?></TD>
										  </TR>
											 <? if(CONFIG_DISTRIBUTOR_ALIAS != "1") { ?>
										  <TR>
										   <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor</FONT></TD>
											<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["name"]?></TD>
										  </TR> 
										  <? } ?>
										 
										 <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType != "SUPA") { ?>
										  <TR>
										   <TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor<? if($queryExecute["distAlias"] != ''){ echo "Alias"; }?></FONT></TD>
											 <? if($queryExecute["distAlias"] != '') { ?>
											   <TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
											 <? }else{ ?>
												<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["agentCompany"]?></TD>
											 <? } ?>  
										  </TR> 
										  <? } ?>
										  
										  <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $agentType == "SUPA") {?>
										 <TR>
											<TD WIDTH="150" ALIGN="right"><FONT CLASS="style2">Distributor Alias</FONT></TD>
											<TD WIDTH="200" COLSPAN="3"><? echo $queryExecute["distAlias"]?></TD>
										 </TR> 
											  <? } ?>
										  <tr>
											<td width="150" align="right"><font color="#005b90">Branch Address</font></td>
											<td width="200"><? echo $senderAgentContent["cp_branch_address"]?></td>
											<td width="100" align="right"><font color="#005b90">Location ID</font></td>
											<td width="200"><? echo $senderAgentContent["cp_branch_no"]?></td>
										  </tr>
										 
										  <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">City</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_city"]?></TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Country</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_country"]?></TD>
										  </TR>
										  <TR>
											<TD WIDTH="150" ALIGN="right"><FONT COLOR="#005b90">Phone</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_phone"]?></TD>
											<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">Fax</FONT></TD>
											<TD WIDTH="200"><? echo $senderAgentContent["cp_fax"]?></TD>
										  </TR> 
										</TABLE>  
					</td></tr>                    
<input type="hidden" name="collectionPointID" id="collectionPointID" value="<?=$senderAgentContent["cp_id"]?>">
                      <?
			  }
			  ?>
	  
 
	<? }?>
 <!-- Layout for IBAN/BANK DETAILS Ends. -->
                <TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Title&nbsp;
                    </strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left"> <SELECT NAME="Title">
                      <OPTION VALUE="Mr.">Mr.</OPTION>
                      <OPTION VALUE="Miss.">Miss</OPTION>
                      <OPTION VALUE="Dr.">Dr.</OPTION>
                      <OPTION VALUE="Mrs.">Mrs.</OPTION>
                      <OPTION VALUE="Other">Other</OPTION>
                  </SELECT><SCRIPT LANGUAGE="JavaScript">
         	SelectOption(document.forms[0].Title, "<?=$_SESSION["Title"]; ?>");
                                </SCRIPT></TD>
                  <TD WIDTH="20%" ALIGN="right"><FONT COLOR="#005b90"><strong>First 
                    Name <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
                  <TD WIDTH="37%" ALIGN="left"><INPUT TYPE="text" NAME="firstName" VALUE="<?=$_SESSION["firstName"]; ?>" MAXLENGTH="25"></TD>
                </TR>
                <TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Middle Name&nbsp;</strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left"><INPUT TYPE="text" NAME="middleName" VALUE="<?=$_SESSION["middleName"]; ?>" MAXLENGTH="25"></TD>
                  <TD WIDTH="20%" ALIGN="right"><FONT COLOR="#005b90"><strong>Last 
                    Name <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
                  <TD WIDTH="37%" ALIGN="left"><INPUT TYPE="text" NAME="lastName" VALUE="<?=$_SESSION["lastName"]; ?>" MAXLENGTH="25"></TD>
                </TR>
				<?	if($beneficiaryNameFlag){ ?>
						<tr  bgcolor="#ededed">
							<td align="right"><font color="#005b90"><b>Beneficiary Name </b></font></td>
							<td><input type="text" name="beneficiaryName" id="beneficiaryName" value="<?=$_SESSION["beneficiaryName"]; ?>" maxlength="50" size="50"></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
				<? }?>
				<?
					// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
					if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY")
						&& CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1 )
					{
						if ( !empty( $IDTypes ) )
						{
						?>
							<TR BGCOLOR="#ededed">
								<TD COLSPAN="4">
									<TABLE WIDTH="100%">
										<TR>
										<?
											$tdNumber = 1;
											$numberOfIDTypes = count( $IDTypes );
											$chkIDTypesJS = "";

											$serverDateArray["year"] = date("Y");
											$serverDateArray["month"] = date("m");
											$serverDateArray["day"] = date("d");
											for( $i = 0; $i < $numberOfIDTypes; $i++ )
											{
												$idNumber = "";
												$idTypeValuesSESS = "";
												$issuedBy = "";

												if( !empty( $_SESSION["IDTypesValuesData"] )
													&& !empty( $_SESSION["IDTypesValuesData"][$IDTypes[$i]["id"]] ) )
												{
													$idTypeValuesSESS = $_SESSION["IDTypesValuesData"];
													$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
													$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
												}
												else if (!empty($IDTypes[$i]["values"]))
												{
													$idNumber = $IDTypes[$i]["values"]["id_number"];
													$issuedBy = $IDTypes[$i]["values"]["issued_by"];
												}
												$mandatoryCheck = $IDTypes[$i]["mandatory"];
												 if(defined("CONFIG_MULTI_ID_MANDATORY_SEPARATE") && CONFIG_MULTI_ID_MANDATORY_SEPARATE=="1"){
													//$mandatoryCheck = $IDTypes[$i]["mandatoyBeneficiary"];
													$mandatoryCheck = "N";
													$serviceMultiId = selectFrom("select sim,benMandatory from service_ID_Mandatory where  serviceAvailable='" . $useServiceType . "' AND multiple_id='".$IDTypes[$i]["id"]."'");
													if($serviceMultiId["sim"]!=""){
														$mandatoryCheck = $serviceMultiId["benMandatory"];
													}
												 }
												?>
													<TD STYLE="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:1px solid black;" : "");?> ">
														<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" ALIGN="center">
														  <TR>
															<TD COLSPAN="2" STYLE="border:2px solid black; font-weight:bold;"><?=$IDTypes[$i]["title"];?> <?=($mandatoryCheck == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?></TD>
														  </TR>
														  <TR>
															<TD WIDTH="80">ID Number:</TD>
															<TD>
																<INPUT TYPE="hidden" NAME="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id]" VALUE="<?=( !empty($IDTypes[$i]["values"]) ? $IDTypes[$i]["values"]["id"] : "" );?>" />
																<INPUT TYPE="hidden" NAME="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_type_id]" VALUE="<?=$IDTypes[$i]["id"];?>" />
																<INPUT TYPE="text" ID="id_number_<?=$IDTypes[$i]["id"];?>" NAME="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_number]" VALUE="<?=$idNumber;?>" SIZE="40" />
															</TD>
														  </TR>
														  <TR>
															<TD>Issued by:</TD>
															<TD><INPUT TYPE="text" ID="id_issued_by_<?=$IDTypes[$i]["id"];?>" NAME="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issued_by]" VALUE="<?=$issuedBy?>" SIZE="40" /></TD>
														  </TR>
														  <TR>
															<TD>Issue Date:</TD>
															<TD>
																<?
																	$startYear = 1985;
																	$currentYear = $serverDateArray["year"];

																	$issueDay = 0;
																	$issueMonth = 0;
																	$issueYear = 0;

																	if ( !empty($idTypeValuesSESS)
																		&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																		$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
																		$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
																		$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
																		$issueDay = $dateArray[2] - 1;
																		$issueMonth = $dateArray[1] - 1;
																		$issueYear = $dateArray[0] - $startYear;
																	}
																?>
																<SELECT ID="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
																</SELECT>
																
																<SELECT ID="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
																</SELECT>
																
																<SELECT ID="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
																</SELECT>

																<SCRIPT TYPE="text/javascript">
																	initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>', 'issue_date_year_<?=$IDTypes[$i]["id"];?>', 
																					'', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', <?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
																</SCRIPT>
															</TD>
														  </TR>
														  <TR>
															<TD>Expiry Date:</TD>
															<TD>
																<?
																	$expiryStartYear = $currentYear;
																	$expiryDay = $serverDateArray["day"] - 1; // Didn't do "less 1" here because atleast one day further from current has to be selected.
																	$expiryMonth = $serverDateArray["month"] - 1;
																	$expiryYear = $serverDateArray["year"] - $expiryStartYear;

																	if ( !empty($idTypeValuesSESS)
																		 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																		$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
																		$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
																		$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );

																		$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );

																		$expiryDay = $dateArray[2] - 1;
																		$expiryMonth = $dateArray[1] - 1;
																		$expiryYear = $dateArray[0] - $expiryStartYear;
																	}
																?>
																<SELECT ID="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
																</SELECT>

																<SELECT ID="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
																</SELECT>

																<SELECT ID="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
																</SELECT>

																<SCRIPT TYPE="text/javascript">
																	initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>', 'expiry_date_year_<?=$IDTypes[$i]["id"];?>', 
																					'', '1:12', '<?=$expiryStartYear;?>:2035', <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
																</SCRIPT>
															</TD>
														  </TR>
														</TABLE>
													</TD>
												<?
													if ( $tdNumber == 2 )
													{
														// Starting a new TR when two TDs are output.
														?>
															</TR><TR>
														<?
														$tdNumber = 1;
													}
													else
														$tdNumber++;

													$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
																			id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

													if ( $mandatoryCheck == 'Y' )
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
													}
													else
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
													}

													$chkIDTypesJS .= "
																		issueDate = new Date();
																		todayDate = new Date( issueDate );
																		expiryDate = new Date();
																		todayDate.setFullYear( " . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " . $serverDateArray["day"] . " );
																		issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
																		expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );

																		if ( issueDate >= todayDate )
																		{
																			alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
																			document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																		if ( expiryDate < todayDate )
																		{
																			alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
																			document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																	";
											}
										?>
										</TR>
									</TABLE>
									<SCRIPT TYPE="text/javascript">
										function checkIdTypes( theForm )
										{
											<?
												if ( !empty($chkIDTypesJS) )
													echo $chkIDTypesJS;
											?>
											return true;
										}
									</SCRIPT>
								</TD>
							</TR>
						<?
						}
					} // End of code against #3299: MasterPayex - Multiple ID Types
					else
					{
				?>
				
				
				
				
				<TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90"><strong>ID Type+&nbsp;</strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left"><INPUT TYPE="radio" NAME="IDType" VALUE="Passport" <? if ($_SESSION["IDType"] == "Passport" || $_SESSION["IDType"] == "") echo "checked"; ?>> Passport <BR>
                    <INPUT TYPE="radio" NAME="IDType" VALUE="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>>
                    ID Card<BR>
                    <INPUT TYPE="radio" NAME="IDType" VALUE="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>>
                  Driving Licence</TD>
                  <TD WIDTH="20%" ALIGN="right" VALIGN="top"><FONT COLOR="#005b90"><strong>ID Number+&nbsp;</strong></FONT></TD>
                  <TD WIDTH="37%" ALIGN="left" VALIGN="top"><INPUT TYPE="text" NAME="IDNumber" VALUE="<?=$_SESSION["IDNumber"]; ?>" MAXLENGTH="32"></TD>
                </TR>
				
				
			<?
				}
			?>				

				<? if(CONFIG_DISABLE_ADDRESS2 == '1'){ ?>
					<TR BGCOLOR="#ededed"> 
					  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Address  <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
					  <TD COLSPAN="3" ALIGN="left"><INPUT TYPE="text" NAME="Address" VALUE="<?=$_SESSION["Address"]; ?>" SIZE="72" MAXLENGTH="254"></TD>
					</TR>
				<? } else { ?>
					<TR BGCOLOR="#ededed"> 
					  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Address Line 1 <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
					  <TD WIDTH="26%" ALIGN="left"><INPUT TYPE="text" NAME="Address" VALUE="<?=$_SESSION["Address"]; ?>" MAXLENGTH="254"></TD>
					  <TD WIDTH="20%" ALIGN="right"><FONT COLOR="#005b90"><strong>Address Line 2&nbsp;</strong></FONT></TD>
					  <TD WIDTH="37%" ALIGN="left"><INPUT TYPE="text" NAME="Address1" VALUE="<?=$_SESSION["Address1"]; ?>" MAXLENGTH="254"></TD>
					</TR>
					<TR BGCOLOR="#ededed"> 
					  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Address Line 3 <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
					  <TD WIDTH="26%" ALIGN="left"  colspan="3"><INPUT TYPE="text" NAME="Address2" VALUE="<?=$_SESSION["Address2"]; ?>" MAXLENGTH="254"></TD>
					</TR>
				<? } ?>
				<? /*
					if (CONFIG_PROVE_ADDRESS == "1")
					{
						?>
							<tr bgcolor="#ededed">
								<td width="17%" height="20" align="right">
									<font color="#005b90"><strong>Prove Address<?=(CONFIG_PROVE_ADDRESS_MANDATORY == "1")?"<font color='#ff0000'>*</font>": ""?></strong></font>
								</td>
								<td>
									<input type="checkbox" id="proveAddress" name="proveAddress" value="Y" <? echo ($_SESSION["proveAddress"] == "Y" ? "checked" : "") ?> onClick="proveAddType();">
								</td>
								<td colspan="2">
									<?
										if (CONFIG_PROVE_ADDRESS_TYPE == "1")
										{
					                  		$types = explode(",", CONFIG_PROVE_ADDRESS_TYPE_VALUES);
											?>
												<div id="proveAddressTypeID" style="display:none;">
													<font color="#005b90"><strong>Prove Address Type</strong></font>
													<select name="proveAddressType" style="font-family:verdana; font-size: 11px">
														<?
															for ($x = 0; $x < count($types); $x++)
															{
																?>
																	<option value="<?=$types[$x] ?>" ><? echo $types[$x] ?></option>
																<? 
															}
														?>
													</select>
													<script language="JavaScript">
														SelectOption(document.forms[0].proveAddressType, "<?=$_SESSION["proveAddressType"]; ?>");
													</script>
												</div>
											<?
										}
									?>
								</td>
							</tr>
                  		<?
					}
				*/ ?>
                <TR BGCOLOR="#ededed"> 
				  <?
				  	/*
						// Important note:
						// Following html code has been temporarily commented out by Usman Ghani. It should not be deleted from here.
                  <td width="20%" align="right"><font color="#005b90"><strong>
                    <?							
							if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom")
								echo "City";
							else
								echo "Zip/postal code";
					?>
						
                     <span class="mandatoryMarker">*</span>&nbsp;</strong></font></td>
                  <td width="37%" align="left">
				  	<? 
						if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom")
						{
					?>
							<input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25">
					<? 	} else { ?>
							<input type="text" name="Zip" value="<? echo stripslashes($_SESSION["Zip"]); ?>" maxlength="7">
					<? } ?>
				  </td>
				  */ 
				  ?>
		        <TD WIDTH="20%" ALIGN="right"><FONT COLOR="#005b90"><strong>
                    City <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT>
				</TD>
				<TD WIDTH="37%" ALIGN="left" colspan="3">
					<INPUT TYPE="text" NAME="City" VALUE="<?=$_SESSION["City"]; ?>" MAXLENGTH="25">
				</TD>
                </TR>
				
				<? if ($_SESSION["Country"] == "United States" || $_SESSION["Country"] == "Canada" || $_SESSION["Country"] == "United Kingdom" || strtolower($_SESSION["Country"]) == strtolower("Poland")){ ?>
				<TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>
                    <? if ($_SESSION["Country"] == "United States" || strtolower($_SESSION["Country"]) == strtolower("Poland")) echo "State"; else echo "Province"; ?> <SPAN CLASS="mandatoryMarker">*</SPAN>
                    &nbsp;</strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left">
				  	<? 
						if(CONFIG_SHOW_STATES == "1")
						{
					?>
						<SELECT NAME="State" ID="State" STYLE="font-family:verdana; font-size: 11px">
							<OPTION VALUE="">- Select State -</OPTION>
							<?
								$sql = "select distinct(state) from cities where 1 and state != '' and country = '".$_SESSION["Country"]."'order by state";
								$statesList = selectMultiRecords($sql);
								
								for($i=0; $i<sizeof($statesList); $i++)
								{
									$selected = "";
									if ( trim(htmlspecialchars(strtolower($statesList[$i]["state"]) ) )== trim(htmlspecialchars(strtolower($_SESSION["State"]) ) ) )
										$selected = "selected='selected'";
									
									echo "<option value=\"".$statesList[$i]["state"]."\" " . $selected . ">".$statesList[$i]["state"]."</option>";
								}
							?>
						</SELECT>
						
					<? } else { ?>
					  	<INPUT TYPE="text" NAME="State" id="State" VALUE="<?=$_SESSION["State"]; ?>" MAXLENGTH="25">
					<? } ?>				 
                  <SCRIPT LANGUAGE="JavaScript">
						SelectOption(document.forms[0].State, "<?=$_SESSION["State"]; ?>");
                  </SCRIPT></TD>
					
                	<TD WIDTH="20%" ALIGN="right">
						<FONT COLOR="#005b90">
					  		<strong>Zip/Postal Code <FONT CLASS="mandatoryMarker">*</FONT></strong>
						</FONT>
					</TD>
                	<TD WIDTH="37%" ALIGN="left">
						<INPUT TYPE="text" ID="Zip" NAME="Zip" VALUE="<? echo stripslashes($_SESSION["Zip"]); ?>" MAXLENGTH="16">
					</TD>
                </TR>
				<? } ?>
				
				<?
					// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
					$jsCode = "";
					if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
						&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
					{
						$jsCode = " onkeyup='phoneEventHandler(event);' ";
					}
					// End of code against ticket #3473 Now Transfer - Phone/Mobile validation
				?>
				
				<?
					// Added by Usman Ghani aginst ticket #3544: Now Transfer - Online Module Enhancements
					if ( defined("CONFIG_PHONES_AS_DROPDOWN")
						&& CONFIG_PHONES_AS_DROPDOWN == 1 )
					{ 
					?>
						<TR BGCOLOR="#ededed">
						  <TD WIDTH="17%" HEIGHT="20" ALIGN="right">
								<FONT COLOR="#005b90"><strong>Phone <SPAN CLASS="mandatoryMarker">*</SPAN></strong></FONT>						  </TD>
						  <TD ALIGN="left" COLSPAN="3">
							<?
								$officePhoneSelected = "";
								$mobilePhoneSelected = "";
								$phoneNumber = ( !empty($_SESSION["Phone"])  ? $_SESSION["Phone"] : ( !empty($_SESSION["Mobile"])  ? $_SESSION["Mobile"] : "" ) );
		
								if ( !empty($_POST["phoneType"]) )
								{
									if ( $_POST["phoneType"] == "phone" )
										$officePhoneSelected = "selected='selected'";
									else if ( $_POST["phoneType"] == "mobile" )
										$mobilePhoneSelected = "selected='selected'";
								}
								else
								{
									if ( !empty($_SESSION["Phone"]) )
										$officePhoneSelected = "selected='selected'";
									else if ( !empty($_SESSION["Mobile"]) )
										$mobilePhoneSelected = "selected='selected'";
								}
							?>
							<INPUT TYPE="text" ID="Phone" NAME="Phone" VALUE="<?=$phoneNumber; ?>" MAXLENGTH="30" />
							&nbsp;Type<FONT COLOR="red">*</FONT>&nbsp;<SELECT ID="phoneType" NAME="phoneType">
											<OPTION VALUE="">Select One</OPTION>
											<OPTION VALUE="phone" <?=$officePhoneSelected;?> >Home Phone</OPTION>
											<OPTION VALUE="mobile" <?=$mobilePhoneSelected;?> >Mobile Phone</OPTION>
										</SELECT>						  </TD>
						</TR>
					<?
					} // End of code aginst ticket #3544: Now Transfer - Online Module Enhancements
					else // The code, which was previously implemented, has been shifted into the following else clause after implementation of ticket #3472: Now Transfer - Phone/Mobile
					{
						?>
						<TR BGCOLOR="#ededed"> 
						  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Phone <SPAN CLASS="mandatoryMarker">*</SPAN>&nbsp;</strong></FONT></TD>
						  <TD WIDTH="26%" ALIGN="left"><INPUT TYPE="text" NAME="Phone" VALUE="<?=$_SESSION["Phone"]; ?>" MAXLENGTH="25"></TD>
						  <TD WIDTH="20%" ALIGN="right"><FONT COLOR="#005b90"><strong>Mobile&nbsp;</strong></FONT></TD>
						  <TD WIDTH="37%" ALIGN="left"><INPUT TYPE="text" NAME="Mobile" VALUE="<?=$_SESSION["Mobile"]; ?>" MAXLENGTH="25"></TD>
						</TR>
						<?
					}
				?>
                <TR BGCOLOR="#ededed"> 
                  <TD WIDTH="17%" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><strong>Email&nbsp;</strong></FONT></TD>
                  <TD WIDTH="26%" ALIGN="left"><INPUT TYPE="text" ID="benEmail" NAME="benEmail" VALUE="<?=$_SESSION["benEmail"]; ?>" MAXLENGTH="25"></TD>
                  <TD HEIGHT="20" ALIGN="right">
				  <? if( !defined("CONFIG_IBAN_ON_QUICK_BEN") || CONFIG_IBAN_ON_QUICK_BEN != "1") { ?>
						<FONT COLOR="#005b90"><strong>Transaction Type &nbsp;</strong></FONT>
				  <? } ?>
				  </TD>
                  <TD ALIGN="left">
				  	<? if( !defined("CONFIG_DONT_SHOW_TRANS_TYPE_ON_BEN_PAGE") || CONFIG_DONT_SHOW_TRANS_TYPE_ON_BEN_PAGE != "1") { ?>					
						<SELECT NAME="transType" ID="transType" STYLE="font-family:verdana; font-size: 11px" onChange="document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&benIDs=<? echo $_GET["benIDs"]?>'; document.forms[0].submit();">
						  <OPTION VALUE="Select Transaction Type">- Select Transaction Type-</OPTION>
						  <?
						  if($_SESSION["transType"] == "Bank Transfer")
						  {
						  ?>
						  <OPTION VALUE="Bank Transfer" selected>Bank Transfer</OPTION>
						  <?
						  }
						  else
						  {
						  ?>
						  <OPTION VALUE="Bank Transfer">Bank Transfer</OPTION>
						  <?
						  }
						  ?>
						</SELECT>
						<SCRIPT LANGUAGE="JavaScript">
				SelectOption(document.forms[0].transType, "<?=$_SESSION["transType"]; ?>");
									</SCRIPT>
					<? } ?>
				  </TD>
                </TR>
			<? if($_SESSION["transType"] == "Bank Transfer" && (!defined('CONFIG_IBAN_OR_BANK_TRANSFER') || CONFIG_IBAN_OR_BANK_TRANSFER != "1")){ ?>	
			<TR>
				<TD COLSPAN="4" BGCOLOR="#ffffff">
					<TABLE WIDTH="100%" BORDER="0" CELLPADDING="2" CELLSPACING="1" BORDERCOLOR="#006600">
						  <TR ALIGN="left" BGCOLOR="#ededed">
								<TD HEIGHT="20" COLSPAN="4"><strong><FONT COLOR="#005b90">Beneficiary Bank Details</FONT></strong></TD>
						  </TR>
						  <TR BGCOLOR="#ededed">
								<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Bank Name <SPAN CLASS="mandatoryMarker">*</SPAN> </FONT></TD>
								<TD WIDTH="200" HEIGHT="20" ALIGN="left">
								<? 
									$benCountryName = $_SESSION['Country'];
									$q = "select * from imbanks where country  = '$benCountryName'";
									$nResult = mysql_query($q)or die("Invalid query: " . mysql_error());
									$rstRow = mysql_fetch_array($nResult);						
						
									if (count($rstRow) > 1)
									{
										$q = "select distinct bankName from imbanks where country  = '$benCountryName'";
										$imBakns = selectMultiRecords($q);
										
										?>
										<SELECT name='bankName' style='font-family:verdana; font-size: 11px; width:150;'   onChange="document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&benIDs=<? echo $_GET["benIDs"]?>'; document.forms[0].submit();">>
										<OPTION VALUE="">-Select Bank-</OPTION>
									
										<?
										
										for($i=0;$i<count($imBakns); $i++)
										{
											if($_SESSION["bankName"] == $imBakns[$i]["bankName"])
												echo "<option value='".$imBakns[$i]["bankName"]."' selected>".$imBakns[$i]["bankName"]."</option>\n";
											else
												echo "<option value='".$imBakns[$i]["bankName"]."'>".$imBakns[$i]["bankName"]."</option>\n";
												
										}
									
										?>
										</SELECT>
										<SCRIPT LANGUAGE="JavaScript">SelectOption(document.frmBenificiary.bankName, "<?=$_SESSION["bankName"]; ?>");</SCRIPT>
										<?
									}
									else
									{
									
									?>
										<INPUT NAME="bankName" TYPE="text" ID="bankName" VALUE="<?=$_SESSION["bankName"]; ?>" MAXLENGTH="25">
									<?
									}
								?>								</TD>
								<TD  align="right"><FONT COLOR="#005b90">Acc Number <SPAN CLASS="mandatoryMarker">*</SPAN> </FONT></TD>
								<TD WIDTH="200" ALIGN="left"><INPUT NAME="accNo" TYPE="text" ID="accNo" VALUE="<?=$_SESSION["accNo"]; ?>" MAXLENGTH="50"></TD>
						  </TR>
						  <TR BGCOLOR="#ededed">
							 								
              					<TD ALIGN="right"><FONT COLOR="#005b90">&nbsp;Bank Code</FONT></TD>
																	
								<TD ALIGN="left">
									<INPUT NAME="bankCode" ID="bankCode" VALUE="<?=$_SESSION["bankCode"]?>" SIZE="30">								</TD>
								
								
								<? if(CONFIG_CPF_ENABLED == '1')
								{?>
									<TD WIDTH="100" ALIGN="right"><FONT COLOR="#005b90">
										<?
											if(strstr(CONFIG_CPF_COUNTRY , $_SESSION['Country']))
											{
												echo  "CPF Number <span class=\"mandatoryMarker\">*</span>";
											}
											?>
							        &nbsp;
							    </FONT>							   </TD>
						        <TD WIDTH="200"><?
										if(strstr(CONFIG_CPF_COUNTRY , $_SESSION['Country']))
										{
										?>
				              <INPUT NAME="ABACPF" TYPE="text" ID="ABACPF" VALUE="<?=$_SESSION["ABACPF"]; ?>">
				              <?
										}
										?> &nbsp;</TD>
								<?
								}else{
								?>
									<TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">&nbsp; </FONT></TD>
				        	<TD WIDTH="200" HEIGHT="20">&nbsp; </TD>
								<?
								}
								?>
						</TR>
						<TR BGCOLOR="#ededed">
									
                       		 <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Branch 
                        					Name <SPAN CLASS="mandatoryMarker">*</SPAN></FONT></TD>
							<TD WIDTH="200" HEIGHT="20" ALIGN="left">
								
								<?
								/////20 Oct         
									
											////For Indian Branches////
									
								$selectedBank=$_SESSION["bankName"];
								if($selectedBank!='-Select Bank-')												
								{
									$br = "select distinct branchName from imbanks where bankName  = '$selectedBank' and branchName!=''";
									$imBranches = selectMultiRecords($br);
								}		
								if(count($imBranches)>0)
								{
																				
								?>
									<SELECT name='branchName' style='font-family:verdana; font-size: 11px; width:150'   onChange="document.frmBenificiary.action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&benIDs=<? echo $_GET["benIDs"]?>'; document.frmBenificiary.submit();">>											
									<OPTION VALUE="">-Select Branch Name-</OPTION>
								<?
										
									for($k=0;$k<count($imBranches);$k++)
									{
												
									
										if($_SESSION["branchName"] == $imBranches[$k]["branchName"])
												echo "<option value='".$imBranches[$k]["branchName"]."' selected>".$imBranches[$k]["branchName"]."</option>\n";
										else
												echo "<option value='".$imBranches[$k]["branchName"]."'>".$imBranches[$k]["branchName"]."</option>\n";
									}
								?>
									</SELECT>
									<SCRIPT LANGUAGE="JavaScript">SelectOption(document.frmBenificiary.branchName, "<?=$_SESSION["branchName"]; ?>");</SCRIPT>
								<?
												
								}
								else
								{
									
								?>
									<INPUT NAME="branchName" TYPE="text" ID="branchName" VALUE="<?=$_SESSION["branchName"]; ?>" MAXLENGTH="25">
								<?
								}
									
							?>						</TD>
											 <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90"><? if($_POST["bankName"] == "United Bank Limited") echo "Branch Code"; else echo "Branch No";?></FONT></TD>
											<TD WIDTH="100" ALIGN="right"> <font color="#005b90">
											  <? 
											  /////20 Oct      to be done   dd_code
											
											////For Indian Branches////
												if($benCountryName=='India')
												{
													$BankBranch=$_SESSION["branchName"];
													if($BankBranch!='-Select Branch Name-')
													{																
														$brC = "select dd_code from imbanks where bank_address  = '$BankBranch'";
														$imBrCode = selectMultiRecords($brC);
													}
												
													//if(count($imBrCode))
													//{
														?>
														<INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<?=$imBrCode[0]["dd_code"]; ?>" MAXLENGTH="25">
													
														<?  
													//}
																							
												}
												else
											
												{
												//$_SESSION["branchCode"]=" ";
													?>
													<INPUT NAME="branchCode" TYPE="text" ID="branchCode" VALUE="<?=$_SESSION["branchCode"]; ?>" MAXLENGTH="25">
													<?
												}
											
												?>											</TD>
										  </TR>
										  <TR BGCOLOR="#ededed">
											
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Branch 
                          Address</FONT> </TD>
											<TD WIDTH="200" HEIGHT="20" ALIGN="left"><INPUT NAME="branchAddress" TYPE="text" ID="branchAddress" VALUE="<?=$_SESSION["branchAddress"]; ?>" SIZE="30" MAXLENGTH="254"></TD>
			
                        <TD  height="20" ALIGN="right"><FONT COLOR="#005b90">Branch 
                          City <SPAN CLASS="mandatoryMarker">*</SPAN></FONT></TD>
											<TD WIDTH="200" HEIGHT="20" ALIGN="left"><INPUT NAME="branchCity" TYPE="text" ID="branchCity" VALUE="<?=$_SESSION["branchCity"]; ?>" ></TD>
										  </TR>
										  <TR BGCOLOR="#ededed">
											
                        <TD WIDTH="150" HEIGHT="20" ALIGN="right"><FONT COLOR="#005b90">Swift 
                          Code</FONT></TD>
											<TD WIDTH="200" HEIGHT="20" ALIGN="left"><INPUT NAME="swiftCode" TYPE="text" ID="swiftCode" VALUE="<?=$_SESSION["swiftCode"]; ?>" MAXLENGTH="25">&nbsp;<A HREF="http://www.swift.com/biconline/index.cfm?fuseaction=display_freesearch" TARGET="_blank"><FONT COLOR="#0000FF">Help</FONT></A></TD>
											<TD  height="20" ALIGN="right" TITLE="For european Countries only"><FONT COLOR="#005b90">IBAN Number </FONT></TD>
											<TD WIDTH="200" HEIGHT="20" ALIGN="left" TITLE="For european Countries only">
												<INPUT NAME="IBAN" TYPE="text" ID="IBAN" VALUE="<?=$_SESSION["IBAN"]; ?>" MAXLENGTH="50" onBlur="validateIban();">
												<BR />
												<DIV ID="benIbanValidStatus"></DIV>
												<INPUT TYPE="hidden" ID="ibnStatus" VALUE="0">											</TD>
										  </TR>
					</TABLE>				</TD>				
				</TR>
<?
				}
?>
<? } ?>					
						<tr bgcolor="#ededed">
									<td width="17%" height="20" align="right">&nbsp;</td>
									<td width="26%">&nbsp;</td>
									<td width="20%" align="right"><font color="#005b90"><strong>&nbsp;</strong></font></td>
									<td width="37%">&nbsp;</td>

								</tr>

              </TABLE></TD>
          </TR>
					<tr bgcolor="#ededed">
						<td colspan="2" align="center"><input type="hidden" name="customerID" value="<? echo $_SESSION["customerID"]?>">
							<input name="Save" id="formSubmitButton" type="submit" value=" Save "  onClick="return checkForm(document.frmBenificiary);">
							&nbsp;&nbsp;
<?
		if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER == "1"){
			$buttonType = 'button';
			$functionCalled = "onClick=\"showHideIbanBank(this);\"";
		}
?>
							<input id="reset" name="reset" type="<?=($buttonType!="" ? $buttonType : "reset");?>" value=" Clear " <?=($functionCalled !="" ? $functionCalled : "");?>>
						</td>
				</tr>
        </TABLE>	  <!--/td-->
		<TD VALIGN="top">
			<?
				if ( empty( $_REQUEST["from"] ) || $_REQUEST["from"] != "popUp" )
				{
					include "exchange-rates-agent.php";
				}
			?>
		</TD>
  <!--/tr-->
  

</TABLE>
</FORM>
<?

	if ( empty( $_REQUEST["from"] ) || $_REQUEST["from"] != "popUp" )
	{
		include "footer.php";
	}
?>
</BODY>
</HTML>
