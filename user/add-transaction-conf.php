<?php
session_start();
// Including files
include ("../include/config.php");
include ("security.php");


//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;
$systemPre = SYSTEM_PRE;

$agentCountry = $_SESSION["benCountry"];
$customerCountry = $_SESSION["customerCountry"];

if(CONFIG_ONLINE_AGENT == '1')
{
	$custAgentQuery = selectFrom("select userID from admin where agentCountry = '".$customerCountry."' and isOnline = 'Y'");
	$custAgentID = $custAgentQuery["userID"];
}

$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
if(trim($_POST["transType"]) == "Bank Transfer" || trim($_POST["transType"]) == "Home Delivery")
{
	$Tran_Type = $_POST["transType"];
	$agentCountry = $_POST["benCountry"];
	$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";
	/*$nResult = mysql_query($query)or die("Invalid query: " . mysql_error()); 
	$rstRow = mysql_fetch_array($nResult);	
	$benAgentIDs =  $rstRow["userID"];		*/
	
	$ida = selectMultiRecords($query);
	for ($j=0; $j < count($ida); $j++)
	{
		$authority = $ida[$j]["authorizedFor"];
		$toCountry = $ida[$j]["IDAcountry"];
		if(strstr($authority,"Home Delivery") && strstr($toCountry,$agentCountry))
		{
			$benAgentIDs =  $ida[$j]["userID"];
		}
	}
	// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
	if($benAgentIDs == '')
	{
		$benAgentIDs = 100111;
	}//100098

	if($_POST["transType"] == "Bank Transfer")
	{
		$benAgentIDs = $_POST["distribut"];
	}
	
}

else if(trim($_POST["transType"]) == "Pick up")
{
	//	$_SESSION["collectionPoint"] = $_POST["collectionPointID"];
	$queryCust = "select *  from cm_collection_point where  cp_id ='" . $_SESSION["collectionPoint"] . "'";
	$senderAgentContent = selectFrom($queryCust);
	$queryIDA="select * from ".TBL_ADMIN_USERS." where userID='".$senderAgentContent["cp_ida_id"]."'";
	$IDAContent=selectFrom($queryIDA);
	//if(count($IDAContent))
	//{
		$benAgentIDs = $IDAContent["userID"];			
	//}
	if($benAgentIDs == "")
	{
		//$queryIDA="select * from ".TBL_ADMIN_USERS." where username = 'MonexIDA'";
		//$IDAContent=selectFrom($queryIDA);
		$benAgentIDs = '20066';//$IDAContent["userID"];
	}
}

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y&transAmount=$transAmounts";
}

if(trim($_POST["transType"]) == "")
{	
	insertError(TE3);	
	redirect($backUrl);
}
if(trim($_SESSION["customerID"]) == "")
{
	if($_SESSION["customerID"] == "")
	{
		$_SESSION["customerID"] = $_SESSION["c_id"];
		$_SESSION["customerID"] = $_SESSION["c_id"];
	}
}

if(trim($_REQUEST["benID"]) != "")
{
	insertError(TE6);
	redirect($backUrl);

	if(trim($_SESSION["benID"]) == "")
	{
		insertError(TE6);
		redirect($backUrl);
	}
}

if(trim($_REQUEST["moneyPaid"]) == "")
{
	if(trim($_SESSION["moneyPaid"]) == "")
	{
		insertError(TE12);	
		redirect($backUrl);
	}
}



	// Code for IBAN number check starts here.
	$queryBen = "select Country  from ".TBL_CM_BENEFICIARY." where benID ='" . $_SESSION["benID"] . "'";
	$benificiaryContent = selectFrom($queryBen);

	$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
	// In case of European countries for Now Transfer, it is required to bypass the checks regarding bank detail fields.
	// It is because they use only IBAN field.
	
	if($qEUTrans["countryRegion"] == "European"	&& CONFIG_IBAN_OR_BANK_TRANSFER != "1")
	{
		$sql = "select IBAN from cm_bankdetails where benID='". $_SESSION["benID"] . "'";
		$result = selectFrom($sql);
		if ( empty($result["IBAN"]) )
		{
			insertError("IBAN for a beneficiary must be provided in his profile.");
			redirect($backUrl);
		}
	} // Code for IBAN number check ends here.
	else
	{
		// Validity cheks for Bank Details of Beneficiary only in case the above clause is not true and 
		// this clause is executed.
		if(trim($_POST["transType"]) == "Bank Transfer")
		{
			if(CONFIG_IBAN_OR_BANK_TRANSFER != "1"){
				if(trim($_SESSION["bankName"]) == "")
				{
					insertError(TE14);	
					redirect($backUrl);
				}
				
				if(trim($_SESSION["accNo"]) == "")
				{
					insertError(TE18);	
					redirect($backUrl);
				}
			}
			else{
				if(isset($_SESSION["IBAN"]) && trim($_SESSION["IBAN"]) == "")
				{
					insertError(TE21);	
					redirect($backUrl);
				}
				if(isset($_SESSION["bankName"]) && trim($_SESSION["bankName"]) == "")
				{
					insertError(TE14);	
					redirect($backUrl);
				}
				if(isset($_SESSION["accNo"]) && trim($_SESSION["accNo"]) == "")
				{
					insertError(TE18);	
					redirect($backUrl);
				}
			}
		/*
			if(ibanCountries($_POST["benCountry"]) && trim($_POST["IBAN"]) == "")
			{
				insertError(TE21);	
				redirect($backUrl);
			}*/
		}
	}

if(trim($_SESSION["transAmount"]) == "" || $_SESSION["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_SESSION["exchangeRate"]) == "" || $_SESSION["exchangeRate"] <= 0)
{
	insertError(TE9);
	redirect($backUrl);
}

if(trim($_SESSION["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_SESSION["localAmount"]) == "" || $_SESSION["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if($_SESSION["benCountry"] == 'Pakistan' || $_SESSION["benCountry"] == 'India' || $_SESSION["benCountry"] == 'Poland' || $_SESSION["benCountry"] == 'Brazil' || $_SESSION["benCountry"] == 'Philippines')
{
}
else
{
	if(trim($_SESSION["IMFee"]) == "" || $_SESSION["IMFee"] <= 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}

if(trim($_SESSION["totalAmount"]) == "" || $_SESSION["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_SESSION["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_SESSION["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
if(trim($_REQUEST["pincode"]) == "")
{	
	insertError(PIN1);
	redirect($backUrl);
}
else
{
	$PINCODE = $_REQUEST["pincode"];//64297
	$c_id= $_SESSION["customerID"];
	$strRandomQuery = "SELECT * FROM cm_customer where c_id = '$c_id' and SecuriyCode = '$PINCODE'";
	$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
	$nRows = mysql_fetch_array($nRandomResult);
	$oldPINcode = $nRows["SecuriyCode"];
	if($oldPINcode == '')
	{
		insertError(PIN1);	
		redirect($backUrl);
	}
}

if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
{
	if(trim($_SESSION["cc_cardType"]) == "")
	{
		insertError(CR1);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_cardNo"]) == "")
	{
		insertError(CR2);	
		redirect($backUrl);
	}
	if(trim($_SESSION["ccExpYr"]) == "")
	{
		insertError(CR3);	
		redirect($backUrl);
	}
	if(trim($_SESSION["ccExpMo"]) == "")
	{
		insertError(CR3);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_cvvNo"]) == "")
	{
		insertError(CR4);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_firstName"]) == "")
	{
		insertError(CR5);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_lastName"]) == "")
	{
		insertError(CR6);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_address1"]) == "")
	{
		insertError(CR7);	
		redirect($backUrl);
	}
	/*if(trim($_SESSION["cc_address2"]) == "")
	{
		insertError(CR8);	
		redirect($backUrl);
	}*/
	if(trim($_SESSION["cc_city"]) == "")
	{
		insertError(CR9);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_state"]) == "")
	{
		insertError(CR10);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_postcode"]) == "")
	{
		insertError(CR11);	
		redirect($backUrl);
	}
	if(trim($_SESSION["cc_country"]) == "")
	{
		insertError(CR12);	
		redirect($backUrl);
	}		
}

		
		
if($_POST["transID"] == "")
{
	//Getting New reference number
	$userId = $_SESSION["c_id"];
	$strQuery = "SELECT * FROM cm_customer where c_id = '".$_SESSION["customerID"]."'";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
	$rstRow = mysql_fetch_array($nResult);				
	$username = $rstRow["c_name"];	


	$query = "select count(transID) as cnt from transactions where customerID='".$_SESSION["customerID"]."'";
	$nextAutoID = countRecords($query);
	$nextAutoID = $nextAutoID + 1;
	$cntChars = strlen($nextAutoID);
	if($cntChars == 1)
		$leadingZeros = "0000";
	elseif($cntChars == 2)
		$leadingZeros = "000";
	elseif($cntChars == 3)
		$leadingZeros = "00";		
	elseif($cntChars == 4)
		$leadingZeros = "0";		
	/*elseif($cntChars == 5)
		$leadingZeros = "0";*/

/*		if($cntChars == 1)
		$leadingZeros = "00000";
	elseif($cntChars == 2)
		$leadingZeros = "0000";
	elseif($cntChars == 3)
		$leadingZeros = "000";		
	elseif($cntChars == 4)
		$leadingZeros = "00";		
	elseif($cntChars == 5)
		$leadingZeros = "0";*/
	switch(trim($customerCountry))
	{
		case "United States": $ccode = "US"; break;
		case "Canada": $ccode = "CA"; break;
		default: 
			$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($customerCountry)."'");
			$ccode = $countryCode["countryCode"];
		break;
	}			

	$imReferenceNumber = strtoupper($username)."-". $leadingZeros . $nextAutoID ;

	if($_SESSION["moneyPaid"] != 'By Credit Card' && $_SESSION["moneyPaid"] != 'By Debit Card')
	{
		/**
		 * @Ticket #5039
		 * Modify the existing logic to calculate the account balance of customer from the customer_account
		 * rather than just from the balance field of the customer table
		 */
		$Balance = onlineCustomerAccountBalance($_SESSION["customerID"]);
		
		if($Balance >= $_SESSION["transAmount"])	
		{
			//$Balance = ($Balance - ($_SESSION["transAmount"] + $_SESSION["IMFee"]));
			//$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"]);
			$Balance = ($Balance - ($_REQUEST["transAmount"] + $_SESSION["IMFee"] + $_SESSION["bankCharges"]));
			$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"] + $_SESSION["bankCharges"]);
			
			/**
			 * @Ticket #5039
			 * Disable this updation as from now on wards the account balance will be checked from the 'customer_account' table
				$strQuery = "update cm_customer set Balance = '$Balance' where c_id = '".$_SESSION["customerID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());			
			 */
			
			$Status	= "Authorize";
			$tran_date = date("Y-m-d H:i:s");
			$AuthorizationDate	= $tran_date;
			
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo  ) 
						 values('".$_SESSION["customerID"]."','".$tran_date."','Transaction is Credit','WITHDRAW','".$amount."','".$imReferenceNumber."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());			
		}
		else
		{
			$Status	= "Pending";
			$AuthorizationDate = "";
			//$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"]);
			$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"] + $_SESSION["bankCharges"]);
			$tran_date = date("Y-m-d");
			$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo ) 
						 values('".$_SESSION["customerID"]."','".$tran_date."','Awaiting Payment','WITHDRAW','".$amount."','".$imReferenceNumber."'
						 )";						 
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());					
		}
		
		if(CONFIG_ONLINE_AGENT == '1')
		{	
			$descript = "Transaction Created";
			updateAgentAccount($custAgentID, $amount, $imReferenceNumber, "WITHDRAW", $descript, 'Agent');	
		}		
	}

	if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
	{
			
		/*************************         c r e d i t  c a r d   i n f o r m a t i o n  ******************************/

		$contentCust = selectFrom("select * from cm_customer where c_id='".$_SESSION["customerID"]."'");
		$contentBenf = selectFrom("select * from cm_beneficiary where benID='".$_SESSION["benID"]."'");
		
		$query = "select * from cities where  country ='".$_SESSION["cc_country"]."'";
		$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);	
		$custContryCode =  $rstRow["countryCode"];
		
		$query = "select * from cities where  country ='".$contentBenf["Country"]."'";
		$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);	
		$benContryCode =  $rstRow["countryCode"]; 
		$benStateCode =  $rstRow["State"]; 						
		
		
		if($_SESSION["cc_country"] == "United Kingdom")
		{
			$query = "select * from states where  state  ='".$_SESSION["cc_state"]."'";
			$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);	
			$custStateCode =  $rstRow["code"];		
		}
		else
		{					
			$custStateCode =  $_SESSION["cc_state"];
		}
		
		if($_POST["Save"] == "Y")
		{				
			$sql = "INSERT INTO cm_cust_credit_card
						( 
							customerID , cardNo , cardName ,
							cardCVV ,  
							expiryDate , firstName , 
							lastName , address , address2 ,city , 
							state , 
							country , postalCode 
						) 
					VALUES     
						( 
							'".$_SESSION["customerID"]."', '".$_SESSION["cc_cardNo"]."', '".$_SESSION["cc_cardType"]."',										 
							'".$_SESSION["cc_cvvNo"]."', 
							'".$_SESSION["ccExpMo"]."-".$_SESSION["ccExpYr"]."', '".$_SESSION["cc_firstName"]."', 
							'".$_SESSION["cc_lastName"]."', '".$_SESSION["cc_address1"]."', 
							'".$_SESSION["cc_address2"]."', '".$_SESSION["cc_city"]."',
							'".$_SESSION["cc_state"]."', 											 											 
							'".$_SESSION["cc_country"]."', '".$_SESSION["cc_postcode"]."'																								
						) "; 									
			insertInto($sql);
		}					
		
		$nTime = time();
		$nTime = $nTime-1000000000;
		$nTime = ($nTime + $_SESSION["customerID"]);
						

		$nTime = time();
		$custID = "UK".$userId;
		$nTime = $nTime.$custID;
		$nCharcter = strlen($nTime);

		if($nCharcter == 16)
			$rand = rand(1000, 9999);
		if($nCharcter == 7)
			$rand = rand(100, 999);
								
		$nTransId = $nTime.$rand;
		
		$transAmounts = ($_SESSION["transAmount"]+$_SESSION["IMFee"]+$_SESSION["bankCharges"]);
									
		/*$strXML = "\"<DEBIT><TRANSACTION_ID>".$nTransId."</TRANSACTION_ID><TRANSACTION_AMOUNT>".$_SESSION["transAmount"]."</TRANSACTION_AMOUNT><ORIGINATION_CURRENCY>GBP</ORIGINATION_CURRENCY><CARD_NUMBER>".$_POST["cc_cardNo"]."</CARD_NUMBER><EXPIRATION_DATE>".$_POST["cc_expiryDate"]."</EXPIRATION_DATE><CARD_TYPE>".$_POST["cc_cardType"]."</CARD_TYPE><CARD_CVV>".$_POST["cc_cvvNo"]."</CARD_CVV><CUSTOMER_ID>".$_SESSION["customerID"]."</CUSTOMER_ID><FIRST_NAME>".$_POST["cc_firstName"]."</FIRST_NAME><LAST_NAME>".$_POST["cc_lastName"]."</LAST_NAME><ADDRESS>".$_POST["cc_address1"]."</ADDRESS><ADDRESS_2>".$_POST["cc_address2"]."</ADDRESS_2><CITY>".$_POST["cc_city"]."</CITY><STATE>".$custStateCode."</STATE><POSTAL_CODE>".$_POST["cc_postcode"]."</POSTAL_CODE><COUNTRY_CODE>".$custContryCode."</COUNTRY_CODE><TELEPHONE>".$contentCust["c_phone"]."</TELEPHONE><EMAIL>".$contentCust["c_email"]."</EMAIL><IP_ADDRESS>".$contentCust["accessFromIP"]."</IP_ADDRESS><B_FIRST_NAME>".$contentBenf["firstName"]."</B_FIRST_NAME><B_LAST_NAME>".$contentBenf["lastName"]."</B_LAST_NAME><B_ADDRESS>".$contentBenf["Address"]."</B_ADDRESS><B_ADDRESS_2>".$contentBenf["Address1"]."</B_ADDRESS_2><B_CITY>".$contentBenf["City"]."</B_CITY><B_STATE>".$benStateCode."</B_STATE><B_POSTAL_CODE>".$contentBenf["Zip"]."</B_POSTAL_CODE><B_COUNTRY_CODE>".$benContryCode."</B_COUNTRY_CODE><B_TELEPHONE>".$contentBenf["Phone"]."</B_TELEPHONE><B_EMAIL>".$contentBenf["email"]."</B_EMAIL></DEBIT>\"";
*/

		$expiryDate = $_SESSION["ccExpMo"].$_SESSION["ccExpYr"];

		$strXML = "<DEBIT><TRANSACTION_ID>".$nTransId."</TRANSACTION_ID><TRANSACTION_AMOUNT>".$transAmounts."</TRANSACTION_AMOUNT><ORIGINATION_CURRENCY>GBP</ORIGINATION_CURRENCY><CARD_NUMBER>".$_SESSION["cc_cardNo"]."</CARD_NUMBER><EXPIRATION_DATE>".$expiryDate."</EXPIRATION_DATE><CARD_TYPE>".$_SESSION["cc_cardType"]."</CARD_TYPE><CARD_CVV>".$_SESSION["cc_cvvNo"]."</CARD_CVV><CUSTOMER_ID>".$_SESSION["customerID"]."</CUSTOMER_ID><FIRST_NAME>".$_SESSION["cc_firstName"]."</FIRST_NAME><LAST_NAME>".$_SESSION["cc_lastName"]."</LAST_NAME><ADDRESS>".$_SESSION["cc_address1"]."</ADDRESS><ADDRESS_2>".$_SESSION["cc_address2"]."</ADDRESS_2><CITY>".$_SESSION["cc_city"]."</CITY><STATE>".$custStateCode."</STATE><POSTAL_CODE>".$_SESSION["cc_postcode"]."</POSTAL_CODE><COUNTRY_CODE>".$custContryCode."</COUNTRY_CODE><TELEPHONE>".$contentCust["c_phone"]."</TELEPHONE><EMAIL>".$contentCust["c_email"]."</EMAIL><IP_ADDRESS>".$contentCust["accessFromIP"]."</IP_ADDRESS><B_FIRST_NAME>".$contentBenf["firstName"]."</B_FIRST_NAME><B_LAST_NAME>".$contentBenf["lastName"]."</B_LAST_NAME><B_ADDRESS>".$contentBenf["Address"]."</B_ADDRESS><B_ADDRESS_2>".$contentBenf["Address1"]."</B_ADDRESS_2><B_CITY>".$contentBenf["City"]."</B_CITY><B_STATE>".$benStateCode."</B_STATE><B_POSTAL_CODE>".$contentBenf["Zip"]."</B_POSTAL_CODE><B_COUNTRY_CODE>".$benContryCode."</B_COUNTRY_CODE><B_TELEPHONE>".$contentBenf["Phone"]."</B_TELEPHONE><B_EMAIL>".$contentBenf["email"]."</B_EMAIL></DEBIT>";
		//exit();
		//echo $strXML;
		//echo "<br><br><br><br><br><br>";
		//exec("IMCreditAppJava.jar $strXML");
		$strXML = urlencode($strXML);
		$strXML = "\"".$strXML."\"";
		exec("java -jar IMCreditAppJava.jar $strXML");
		
		//sleep(10);
		$query = "select * from cm_credit_trans where transID  =  '$nTransId'";
		$nResult = mysql_query($query)or die("Invalid query: " . mysql_error());
		while($rstRow = mysql_fetch_array($nResult))
		{								
			$AttemptCode  =  $rstRow["AttemptCode"];
			$PQTransID =  $rstRow["PQTransID"];
			$ApprovalCode  =  $rstRow["ApprovalCode"];		  
			$ResultCode   =  $rstRow["ResultCode"];
			$ResultText   =  $rstRow["ResultText"];
			$transRefID  = $rstRow["transID"];
		}

		if($AttemptCode == "SUCC" && $ResultCode == "S001")
		{
			/******************************************* Transactin  Working***********************************************/
			$tran_date = date("Y-m-d");
			$AuthorizationDate	= $tran_date;
			$sql = "INSERT INTO transactions
							( 
							customerID, benID, custAgentID, refNumberIM,
							exchangeID,  
							transAmount, exchangeRate, 
							localAmount, IMFee, totalAmount,benAgentID, 
							transactionPurpose, 
							transDate, transType, 											 
							currencyFrom,   
							currencyTo ,moneyPaid ,bankCharges ,PINCODE,createdBy,transStatus,toCountry,
							fromCountry,collectionPointID  ,authoriseDate, question, answer, tip, outCurrCharges 
							) 
							VALUES     
							( 
							'".$_SESSION["customerID"]."', '".$_SESSION["benID"]."', '".$nRows["agentId"]."',
							'$imReferenceNumber',										 
							'".$_SESSION["exchangeID"]."', 
							'".$_SESSION["transAmount"]."', '".$_SESSION["exchangeRate"]."', 
							'".$_SESSION["localAmount"]."', '".$_SESSION["IMFee"]."', 
							'".$_SESSION["totalAmount"]."', '".$benAgentIDs."',
							'".checkValues($_SESSION["transactionPurpose"])."', 											 											 
							NOW(), '".$_POST["transType"]."', 											 
							'".$_SESSION["currencyFrom"]."', '".$_SESSION["currencyTo"]."', '".$_SESSION["moneyPaid"]."',
							'".$_SESSION["bankCharges"]."','".$_POST["pincode"]."','CUSTOMER','$Status','".$_SESSION["benCountry"]."',
							'".$customerCountry."','".$_SESSION["collectionPointID"]."','".$AuthorizationDate."',
							'".$_SESSION["question"]."','".$_SESSION["answer"]."','".$_SESSION["tip"]."','".$_SESSION["currencyCharge"]."')"; 
			//debug($sql, true);

			if(insertInto($sql))
			{
				$intLastInsertTransactionId = @mysql_insert_id();
				
				//$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"]);
				$amount = ($_SESSION["transAmount"] + $_SESSION["IMFee"] + $_SESSION["bankCharges"]);
				$tran_date = date("Y-m-d");
				$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount ,tranRefNo) 
							 values('".$_SESSION["customerID"]."','".$tran_date."','Payment by Credit Card','DEPOSIT','".$amount."','$imReferenceNumber'
							 )";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
				
				$tran_date = date("Y-m-d");
				$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo ) 
							 values('".$_SESSION["customerID"]."','".$tran_date."','Payment by Credit Card','WITHDRAW','".$amount."','$imReferenceNumber'
							 )";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
					
				/* Updating the agent ledger */
				updateAgentAccount($nRows["agentId"], $_SESSION["totalAmount"], $intLastInsertTransactionId, "WITHDRAW", "Transaction Created", "Agent");																			
				
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting admin email address.
	
				//$benContents = selectFrom("select email from ". TBL_BENEFICIARY ." where benID= '". $_SESSION["benID"] ."'");
				//$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select * from cm_customer where c_id= '". $_SESSION["customerID"] ."'");
				$custEmail = $custContents["c_name"];
				$custTitle = $custContents["Title"];
				$custFirstName = $custContents["FirstName"];
				$custMiddleName = $custContents["MiddleName"];
				$custLastName = $custContents["LastName"];
				$custCountry = $custContents["c_country"];
				$custCity = $custContents["c_city"];
				$custZip = $custContents["c_zip"];
				$custLoginName = $custContents["username"];												
				$custEmail = $custContents["c_email"];
				$custPhone = $custContents["c_phone"];
	
	
				$benContents = selectFrom("select * from cm_beneficiary where benID= '". $_SESSION["benID"] ."'");			
				$benTitle = $benContents["Title"];
				$benFirstName = $benContents["firstName"];
				$benMiddleName = $benContents["middleName"];
				$benLastName = $benContents["lastName"];
				$benAddress  = $benContents["Address"];
				$benCountry = $benContents["Country"];
				$benCity = $benContents["City"];
				$benZip = $benContents["Zip"];
				$benLoginName = $benContents["username"];												
				$benEmail = $benContents["email"];
				$benPhone = $benContents["Phone"];
				
	
				$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentIDs ."'");			
				$AdmindLoginName = $AdmindContents["username"];
				$AdmindName = $AdmindContents["name"];
				$AdmindEmail = $AdmindContents["email"];
	
	
				$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $_SESSION["collectionPointID"] ."'");			
				$agentnamee = $cContents["cp_corresspondent_name"];
				$contactperson = $cContents["cp_corresspondent_name"];
				$company2 = $cContents["cp_branch_name"];
				$address = $cContents["cp_branch_address"];
				$country = $cContents["cp_country"];
				$city = $cContents["cp_city"];
				$phone = $cContents["cp_phone"];						
				$tran_date = date("Y-m-d");
				$tran_date = date("F j, Y", strtotime("$tran_date"));
				$subject = "New transaction created";
						
				/***********************************************************/
				$authoDate = $tran_date;
				include "transaction-mail.php";
				$transID = @mysql_insert_id();
						
				if($_SESSION["agentID_id"] != "")
				{		
					$entryDate = date("Y-m-d");
					insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
					VALUES ('". $_SESSION["agentID_id"]."', $transID, $entryDate,'INSERTION','".$_SESSION["customerID"]."')");		
				}
			
				if(trim($_POST["transType"]) == "Bank Transfer")
				{
					$querycnt = "select count(*) as ncount from ".TBL_CM_BANK_DETAILS." where benID = '".$_SESSION["benID"]."' and bankName = '".$_SESSION["bankName"]."'";
					$nResultcnt = mysql_query($querycnt)or die("Invalid query: " . mysql_error());
					$rstRowcnt = mysql_fetch_array($nResultcnt);
					$nRecods = $rstRowcnt["ncount"];										
					if($nRecods < 1 || CONFIG_IBAN_OR_BANK_TRANSFER=="1")
					{
						if(CONFIG_IBAN_OR_BANK_TRANSFER!="1"){
							$strQuery = "delete FROM ".TBL_CM_BANK_DETAILS." where benID = '".$_SESSION["benID"]."'";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
						}	
						
						$sqlBank = "insert into ".TBL_CM_BANK_DETAILS."
									( 
									benID, bankName, BankCode ,BranchName,
									accNo, branchCode, branchAddress,BranchCity ,
									ABACPF , IBAN,swiftCode , accountType
									)
									Values
									(
									'".$_SESSION["benID"]."',  '".$_SESSION["bankName"]."','".$_SESSION["BankCode"]."','".$_SESSION["BranchName"]."',
									'".$_SESSION["accNo"]."', '".$_SESSION["branchCode"]."', '".$_SESSION["branchAddress"]."','".$_SESSION["BranchCity"]."',
									'".$_SESSION["ABACPF"]."',	'".$_SESSION["IBAN"]."',	'".$_SESSION["swiftCode"]."', '".$_SESSION["accountType"]."'
									)
									";
						if(define("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
							$sqlBank = "insert into bankDetails
										( 
										benID, bankName, BankCode ,BranchName,
										accNo, branchCode, branchAddress,BranchCity ,
										ABACPF , IBAN,swiftCode , accountType
										)
										Values
										(
										'".$_SESSION["benID"]."',  '".$_SESSION["bankName"]."','".$_SESSION["BankCode"]."','".$_SESSION["BranchName"]."',
										'".$_SESSION["accNo"]."', '".$_SESSION["branchCode"]."', '".$_SESSION["branchAddress"]."','".$_SESSION["BranchCity"]."',
										'".$_SESSION["ABACPF"]."',	'".$_SESSION["IBAN"]."',	'".$_SESSION["swiftCode"]."', '".$_SESSION["accountType"]."'
										)
										";
										
						}
						insertInto($sqlBank);
						$bankDetailsInsertId = @mysql_insert_id();
						if($bankDetailsInsertId !="" && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
							$queryUpdate="update bankDetails set 
												 transID = '".$intLastInsertTransactionId."' 
												 where bankID='".$bankDetailsInsertId."'"; 
							update($queryUpdate);
						}
						$queryUpdate="update cm_beneficiary set 
											 transType = '".$_POST["transType"]."' 
											 where benID='".$_SESSION["benID"]."'"; 
						update($queryUpdate);
					}
				}
			/* #5028
				TO put service Type in transaction table
			*/
			if($countryBasedFlag && isset($_POST["serviceType"]))
			{
				update("update ".TBL_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where transID='".$intLastInsertTransactionId."'");
			}		  
				$_SESSION["collectionPointID"] = "";
				$_SESSION["transTypes"] = "";
				$_SESSION["transType"] 		= "";
				$_SESSION["benAgentID"] 	= "";
				$_SESSION["customerID"] 	= "";
				$_SESSION["benID"] 			= "";
				$_SESSION["moneyPaid"] 		= "";
				$_SESSION["transactionPurpose"] = "";
				$_SESSION["fundSources"] 	= "";
				$_SESSION["refNumber"] 		= "";
		
				$_SESSION["transAmount"] 	= "";
				$_SESSION["exchangeRate"] 	= "";
				$_SESSION["exchangeID"] 	= "";
				$_SESSION["localAmount"] 	= "";
				$_SESSION["totalAmount"] 	= "";
				$_SESSION["IMFee"] 			= "";
				$_SESSION["bankCharges"] = "";	
				// resetting Session vars for trans_type Bank Transfer
				$_SESSION["bankName"] 		= "";
				$_SESSION["branchCode"] 	= "";
				$_SESSION["branchAddress"] 	= "";
				$_SESSION["swiftCode"] 		= "";
				$_SESSION["accNo"] 			= "";
				$_SESSION["ABACPF"] 		= "";
				$_SESSION["IBAN"] 			= "";
				$_SESSION["accountType"] = "";
				
				$_SESSION["cardType"]= "";
				$_SESSION["cardNo"]= "";
				$_SESSION["expiryDate"]= "";
				$_SESSION["cvvNo"]	= "";
				$_SESSION["pincode"] = "";
				
				$_SESSION["question"] 			= "";
				$_SESSION["answer"]     = "";	
				$_SESSION["tip"]     = "";	
				$_SESSION["currencyCharge"] = "";
						
						insertError("Transaction has been added successfully for processing and ".$preFix." reference number is $imReferenceNumber");										
						redirect("thanks.php?msg1=Y&Status=$Status");
			}
			
			/******************************************* Transaction Working ***********************************************/
		}
						elseif($AttemptCode == "INVR")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);						
						}
						elseif($AttemptCode == "NORS")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);	
						}
						elseif($AttemptCode == "BADR")
						{
							insertError("$AttemptCode : There was an error while communicating with the Server, Please try again after some Time!");	
							redirect($backUrl);												
						}
						elseif($AttemptCode == "")
						{
							insertError("$AttemptCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "S000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "S009" || $ResultCode == "T003")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "T000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T001")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T002")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "T004")
						{
							insertError("System Unavailable, Please try again after in a few minutes!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R001")
						{
							insertError(" $ResultCode: The amount exceeds your per transaction limit, Please Enter Correct Amount!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R002")
						{
							insertError(" $ResultCode: The amount exceeds your credit limit, Please Enter Correct Amount!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R003")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R004")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "R005")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "P001" || $ResultCode == "P002" || $ResultCode == "P003" || $ResultCode == "S010")
						{
							insertError("Transaction has been Declined, Please try again after some Time!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X000")
						{
							insertError("$ResultCode: We are Sorry we are unable to Progress your Credit Card at this time. Please try again later.  If the Problem Persist Please Contact to the $company Customer Support!");		
							redirect($backUrl);						
						}
						elseif($ResultCode == "X001")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X002")
						{
							insertError(" $ResultCode: Ask from PayQuick!");	
							redirect($backUrl);						
						}
						elseif($ResultCode == "X003")
						{
							insertError("Please Contact to Customer Support office of $company!");	
							redirect($backUrl);						
						}					
						

/*************************         c r e d i t  c a r d   i n f o r m a t i o n ******************************/
		}
		else
		{
							/******************************************* Transactin  Working***********************************************/
							
									 $sql = "INSERT INTO transactions
																		( 
																		customerID, benID, custAgentID, refNumberIM,
																		exchangeID,  
																		transAmount, exchangeRate, 
																		localAmount, IMFee, totalAmount,benAgentID, 
																		transactionPurpose, 
																		transDate, transType, 											 
																		currencyFrom,   
																		currencyTo ,moneyPaid ,bankCharges ,PINCODE,createdBy,transStatus,toCountry,
																		fromCountry,collectionPointID  ,authoriseDate, question, answer, tip, outCurrCharges 
																		) 
																		VALUES     
																		( 
																		'".$_SESSION["customerID"]."', '".$_SESSION["benID"]."', '".$nRows["agentId"]."',
																		'$imReferenceNumber',										 
																		'".$_SESSION["exchangeID"]."', 
																		'".$_SESSION["transAmount"]."', '".$_SESSION["exchangeRate"]."', 
																		'".$_SESSION["localAmount"]."', '".$_SESSION["IMFee"]."', 
																		'".$_SESSION["totalAmount"]."', '".$benAgentIDs."',
																		'".checkValues($_SESSION["transactionPurpose"])."', 											 											 
																		NOW(), '".$_POST["transType"]."', 											 
																		'".$_SESSION["currencyFrom"]."', '".$_SESSION["currencyTo"]."', '".$_SESSION["moneyPaid"]."',
																		'".$_SESSION["bankCharges"]."','".$_POST["pincode"]."','CUSTOMER','$Status','".$_SESSION["benCountry"]."',
																		'".$customerCountry."','".$_SESSION["collectionPointID"]."','".$AuthorizationDate."',
																		'".$_SESSION["question"]."','".$_SESSION["answer"]."','".$_SESSION["tip"]."','".$_SESSION["currencyCharge"]."'
																								
																		) 
																		"; 
									//debug($sql, true);
									if(insertInto($sql))
									{
										$intLastInsertTransactionId = @mysql_insert_id();
										
										/* Updating the agent ledger */
										updateAgentAccount($nRows["agentId"], $_SESSION["totalAmount"], $intLastInsertTransactionId, "WITHDRAW", "Transaction Created", "Agent");
										
										$fromName = SUPPORT_NAME;
										$fromEmail = SUPPORT_EMAIL;
										// getting admin email address.
							
										//$benContents = selectFrom("select email from ". TBL_BENEFICIARY ." where benID= '". $_SESSION["benID"] ."'");
										//$benEmail = $benContents["email"];
										// Getting Customer Email
										$custContents = selectFrom("select * from cm_customer where c_id= '". $_SESSION["customerID"] ."'");
										$custEmail = $custContents["c_name"];
										$custTitle = $custContents["Title"];
										$custFirstName = $custContents["FirstName"];
										$custMiddleName = $custContents["MiddleName"];
										$custLastName = $custContents["LastName"];
										$custCountry = $custContents["c_country"];
										$custCity = $custContents["c_city"];
										$custZip = $custContents["c_zip"];
										$custLoginName = $custContents["username"];												
										$custEmail = $custContents["c_email"];
										$custPhone = $custContents["c_phone"];
							
							
										$benContents = selectFrom("select * from cm_beneficiary where benID= '". $_SESSION["benID"] ."'");			
										$benTitle = $benContents["Title"];
										$benFirstName = $benContents["firstName"];
										$benMiddleName = $benContents["middleName"];
										$benLastName = $benContents["lastName"];
										$benAddress  = $benContents["Address"];
										$benCountry = $benContents["Country"];
										$benCity = $benContents["City"];
										$benZip = $benContents["Zip"];
										$benLoginName = $benContents["username"];												
										$benEmail = $benContents["email"];
										$benPhone = $benContents["Phone"];
										
							
										$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentIDs ."'");			
										$AdmindLoginName = $AdmindContents["username"];
										$AdmindName = $AdmindContents["name"];
										$AdmindEmail = $AdmindContents["email"];
							
							
										$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $_SESSION["collectionPointID"] ."'");			
										$agentnamee = $cContents["cp_corresspondent_name"];
										$contactperson = $cContents["cp_corresspondent_name"];
										$company2 = $cContents["cp_branch_name"];
										$address = $cContents["cp_branch_address"];
										$country = $cContents["cp_country"];
										$city = $cContents["cp_city"];
										$phone = $cContents["cp_phone"];						
										$tran_date = date("Y-m-d");
										$tran_date = date("F j, Y", strtotime("$tran_date"));
										$subject = "New transaction created";
			/* #5028
				TO put service Type in transaction table
			*/
			if($countryBasedFlag)
			{
				update("update ".TBL_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where transID='".$intLastInsertTransactionId."'");
			}	
							/***********************************************************/
								$authoDate = "";
								include "transaction-mail.php";							
								
								$transID = @mysql_insert_id();
										
									if($_SESSION["agentID_id"] != "")
									{		
										$entryDate = date("Y-m-d");
										insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
										VALUES ('". $_SESSION["agentID_id"]."', $transID, $entryDate,'INSERTION','".$_SESSION["customerID"]."')");		
									}
							
										if(trim($_POST["transType"]) == "Bank Transfer")
										{
											$querycnt = "select count(*) as ncount from ".TBL_CM_BANK_DETAILS." where benID = '".$_SESSION["benID"]."' and bankName = '".$_SESSION["bankName"]."'";
											$nResultcnt = mysql_query($querycnt)or die("Invalid query: " . mysql_error());
											$rstRowcnt = mysql_fetch_array($nResultcnt);
											$nRecods = $rstRowcnt["ncount"];										
											if($nRecods < 1 || CONFIG_IBAN_OR_BANK_TRANSFER=="1")
											{
												if(CONFIG_IBAN_OR_BANK_TRANSFER!="1"){
													$strQuery = "delete FROM ".TBL_CM_BANK_DETAILS." where benID = '".$_SESSION["benID"]."'";
													$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			
												}	
												
												$sqlBank = "insert into ".TBL_CM_BANK_DETAILS."
															( 
															benID, bankName, BankCode ,BranchName,
															accNo, branchCode, branchAddress,BranchCity ,
															ABACPF , IBAN,swiftCode , accountType
															)
															Values
															(
															'".$_SESSION["benID"]."',  '".$_SESSION["bankName"]."','".$_SESSION["BankCode"]."','".$_SESSION["BranchName"]."',
															'".$_SESSION["accNo"]."', '".$_SESSION["branchCode"]."', '".$_SESSION["branchAddress"]."','".$_SESSION["BranchCity"]."',
															'".$_SESSION["ABACPF"]."',	'".$_SESSION["IBAN"]."',	'".$_SESSION["swiftCode"]."', '".$_SESSION["accountType"]."'
															)
															";
												if(defined("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
													$sqlBank = "insert into bankDetails
																( 
																benID, bankName, BankCode ,BranchName,
																accNo, branchCode, branchAddress,BranchCity ,
																ABACPF , IBAN,swiftCode , accountType
																)
																Values
																(
																'".$_SESSION["benID"]."',  '".$_SESSION["bankName"]."','".$_SESSION["BankCode"]."','".$_SESSION["BranchName"]."',
																'".$_SESSION["accNo"]."', '".$_SESSION["branchCode"]."', '".$_SESSION["branchAddress"]."','".$_SESSION["BranchCity"]."',
																'".$_SESSION["ABACPF"]."',	'".$_SESSION["IBAN"]."',	'".$_SESSION["swiftCode"]."', '".$_SESSION["accountType"]."'
																)
																";
												;
												}
												insertInto($sqlBank);
												$bankDetailsInsertId = @mysql_insert_id();
												$queryUpdate="update cm_beneficiary set 
																transType = '".$_POST["transType"]."'
																where benID='".$_SESSION["benID"]."'";
												if($bankDetailsInsertId !="" && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
													$queryUpdate="update bankDetails set 
																		 transID = '".$intLastInsertTransactionId."'
																		 where bankID='".$bankDetailsInsertId."'"; 
												}
												update($queryUpdate);
											}
										  }
										$_SESSION["collectionPointID"] = "";
										$_SESSION["transTypes"] = "";
										$_SESSION["transType"] 		= "";
										$_SESSION["benAgentID"] 	= "";
										$_SESSION["customerID"] 	= "";
										$_SESSION["benID"] 			= "";
										$_SESSION["moneyPaid"] 		= "";
										$_SESSION["transactionPurpose"] = "";
										$_SESSION["fundSources"] 	= "";
										$_SESSION["refNumber"] 		= "";
								
										$_SESSION["transAmount"] 	= "";
										$_SESSION["exchangeRate"] 	= "";
										$_SESSION["exchangeID"] 	= "";
										$_SESSION["localAmount"] 	= "";
										$_SESSION["totalAmount"] 	= "";
										$_SESSION["IMFee"] 			= "";
										$_SESSION["bankCharges"] = "";	
										// resetting Session vars for trans_type Bank Transfer
										$_SESSION["bankName"] 		= "";
										$_SESSION["branchCode"] 	= "";
										$_SESSION["branchAddress"] 	= "";
										$_SESSION["swiftCode"] 		= "";
										$_SESSION["accNo"] 			= "";
										$_SESSION["ABACPF"] 		= "";
										$_SESSION["IBAN"] 			= "";
										$_SESSION["accountType"] = "";
										$_SESSION["ibanorbank"] = "";
										
										
										$_SESSION["cardType"]= "";
										$_SESSION["cardNo"]= "";
										$_SESSION["expiryDate"]= "";
										$_SESSION["cvvNo"]	= "";
										$_SESSION["pincode"] = "";
										$_SESSION["question"] = "";
										$_SESSION["answer"] = "";	
										$_SESSION["tip"] = "";											
										$_SESSION["currencyCharge"] = "";
										insertError("Transaction has been added successfully for processing and ".$preFix." reference number is $imReferenceNumber");
										redirect("thanks.php?msg1=Y&Status=$Status");
								}
							
							/******************************************* Transaction Working ***********************************************/
		
		}
			
}
else
{
$queryUpdate = 	"update transactions set 
				customerID	='".$_SESSION["customerID"]."', 
				benID		='".$_SESSION["benID"]."',
				benAgentID	=$benAgentIDs,
				exchangeID	='".$_SESSION["exchangeID"]."',
				refNumber 	='".$_SESSION["refNumber"]."', 
				transAmount	='".$_SESSION["transAmount"]."', 
				exchangeRate='".$_SESSION["exchangeRate"]."', 
				localAmount	='".$_SESSION["localAmount"]."', 
				IMFee		='".$_SESSION["IMFee"]."',
				totalAmount	='".$_SESSION["totalAmount"]."', 
				transactionPurpose='".checkValues($_SESSION["transactionPurpose"])."',
				fundSources	='".checkValues($_SESSION["fundSources"])."',
				moneyPaid	='".$_SESSION["moneyPaid"]."', 
				transType	='".$_POST["transType"]."',
				toCountry= '".$_SESSION["benCountry"]."',
				fromCountry= '".$_SESSION["custCountry"]."',
				currencyFrom= '".$_SESSION["currencyFrom"]."', 
				currencyTo= '".$_SESSION["currencyTo"]."', 
				custAgentParentID= '".$_SESSION["custAgentParentID"]."', 
				benAgentParentID= '".$_SESSION["benAgentParentID"]."',
				question = '".$_SESSION["question"]."',
				answer = '".$_SESSION["answer"]."'	
				tip = '".$_SESSION["tip"]."',
				outCurrCharges = '".$_SESSION["currencyCharge"]."'	
				where transID='".$_POST["transID"]."'";
update($queryUpdate);

if($_SESSION["agentID_id"] != "")
{		
	$entryDate = date("Y-m-d");
	insertInto("insert into agent_logfile (agentID , transID  , entryDate , remarks,customerID ) 
	VALUES ('". $_SESSION["agentID_id"]."', '".$_POST["transID"]."', $entryDate,'UPDATION','".$_SESSION["customerID"]."')");		
}		
if(trim($_POST["transType"]) == "Bank Transfer")
{
	$queryUpdateBank = "update bankDetails set 
						benID 		= '".$_SESSION["benID"]."', 
						bankName 	= '".$_SESSION["bankName"]."', 
						accNo		= '".$_SESSION["accNo"]."',
						branchCode	= '".$_SESSION["branchCode"]."', 
						branchAddress='".$_SESSION["branchAddress"]."',
						ABACPF 		= '".$_SESSION["ABACPF"]."',
						IBAN		= 	'".$_SESSION["IBAN"]."'
						where transID='".$_SESSION["transID"]."'";
	if(define("CONFIG_IBAN_OR_BANK_TRANSFER") && CONFIG_IBAN_OR_BANK_TRANSFER=="1"){
		$getIbanOrBankInfoQuery = selectFrom("select * form ".TBL_CM_BANK_DETAILS."" );
		if($_SESSION["IBAN"]!=""){
			$queryUpdateBank = "update bankDetails set 
								benID 		= '', 
								bankName 	= '', 
								bankName 	= '', 
								BankCode 	= '', 
								BranchName 	= '', 
								BranchCity 	= '', 
								accountType = '', 
								accNo		= '',
								branchCode	= '', 
								branchAddress='',
								ABACPF 		= '',
								IBAN		= 	'".$_SESSION["IBAN"]."'
								where transID='".$_SESSION["transID"]."'";


		}
		else{
			$queryUpdateBank = "update bankDetails set 
								benID 		= '".$_SESSION["benID"]."', 
								bankName 	= '".$_SESSION["bankName"]."', 
								BankCode 	= '".$_SESSION["BankCode"]."', 
								BranchName 	= '".$_SESSION["BranchName"]."', 
								BranchCity 	= '".$_SESSION["BranchCity"]."', 
								accountType 	= '".$_SESSION["accountType"]."', 
								accNo		= '".$_SESSION["accNo"]."',
								branchCode	= '".$_SESSION["branchCode"]."', 
								branchAddress='".$_SESSION["branchAddress"]."',
								ABACPF 		= '".$_SESSION["ABACPF"]."',
								IBAN		= 	'".$_SESSION["IBAN"]."'
								where transID='".$_SESSION["transID"]."'";
		}
	}
	update($queryUpdateBank);
}
/*
	by Aslam Shahid. (traced while doing #4804)
*/
if($countryBasedFlag)
{
	update("update ".TBL_TRANSACTIONS." set serviceType='".$_POST["serviceType"]."' where transID='".$_POST["transID"]."'");
}
insertError("Transaction has been updated successfully.");
redirect("add-transaction.php?msg1=Y");
}
/*
transType : Bank Transfer
customerID : 2
benID : 2
benCountry : Pakistan

bankName : State BAnk
accNo : 123-132-123
branchCode : 123
branchAddress : asdafdasdasd
swiftCode : 
IBAN : 

refNumber : 123-123
transAmount : 500
exchangeRate : 58.8
exchangeID : 3
localAmount : 29400
IMFee : 20
totalAmount : 520
transactionPurpose : Family Assistant
fundSources : assad
moneyPaid : By Cash
Declaration : Y
*/

?>