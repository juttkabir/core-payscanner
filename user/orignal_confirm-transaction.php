<?
session_start();
require_once ("../include/config.php");
require_once ("./include/flags.php");
require_once ("security.php");

$date_time = date('d-m-Y  h:i:s A');


//$nAgentID = $_GET['agentID'];

if($_SESSION["benAgentID"] == '')
	$_SESSION["benAgentID"] = $_POST['agentID'];
	
	
	$_SESSION["BenTempcurrencyTo"] =  $_SESSION["currencyTo"];
	
	//echo $_SESSION["benAgentID"];

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["bankCharges"] = $_POST["bankCharges"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["currencyTo"] = $_POST['currencyTo'];	

$_SESSION["cardType"] = $_POST["cardType"];
$_SESSION["cardNo"] = $_POST["cardNo"];
$_SESSION["expiryDate"] = $_POST["expiryDate"] ;
$_SESSION["cvvNo"]	= $_POST["cvvNo"];

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y";
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}
if(trim($_POST["customerID"]) == "")
{
	if($_SESSION["customerID"] == "")
	{
		$_POST["customerID"] = $_SESSION["c_id"];
		$_SESSION["customerID"] = $_SESSION["c_id"];
	}
}
if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}


// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if(trim($_POST["bankName"]) == "")
	{
		insertError(TE14);	
		redirect($backUrl);
	}
	if(trim($_POST["accNo"]) == "")
	{
		insertError(TE18);	
		redirect($backUrl);
	}
/*
	if(trim($_POST["branchCode"]) == "")
	{
		insertError(TE15);	
		redirect($backUrl);
	}*/

	if(trim($_POST["branchAddress"]) == "")
	{
		insertError(TE16);	
		redirect($backUrl);
	}
/*
	if(trim($_POST["swiftCode"]) == "")
	{
		insertError(TE17);	
		redirect($backUrl);
	}
*/

	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "United States")
	{
		insertError(TE19);	
		redirect($backUrl);
	}
	
	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "Brazil")
	{
		insertError(TE20);	
		redirect($backUrl);
	}

	if(ibanCountries($_POST["benCountry"]) && trim($_POST["IBAN"]) == "")
	{
		insertError(TE21);	
		redirect($backUrl);
	}
}


if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{

}
else
{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}
$_SESSION["Declaration"] = $_POST["Declaration"];
if(trim($_POST["Declaration"]) != "Y")
{
	if(trim($_SESSION["Declaration"]) != "Y")
	{
		insertError(TE13);	
		redirect($backUrl);
	}
}
  
  if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
  {
  		if(trim($_POST["cardType"]) == "")
		{
			insertError(CR1);	
			redirect($backUrl);
		}
		if(trim($_POST["cardNo"]) == "")
		{
			insertError(CR2);	
			redirect($backUrl);
		}
		if(trim($_POST["expiryDate"]) == "")
		{
			insertError(CR3);	
			redirect($backUrl);
		}
		if(trim($_POST["cvvNo"]) == "")
		{
			insertError(CR4);	
			redirect($backUrl);
		}
}
require_once ("header.php");
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">

  
    <tr>
      <td align="center" valign="top"><br>
        <table width="700" border="0" bordercolor="#FF0000">
  <tr height="30">
    <td align="left" bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">&nbsp;Confirm Transaction.</font></b></td>
  </tr>		
          <tr>
            <td align="right"><a href="add-transaction.php" class="style2">Change Information</a></td>
          </tr>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <br>
            <?

	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo $width?>" height="<? echo $height?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table width="277" border="0" align="center">
              <tr>
                <td width="323" height="80" colspan="4" align="center" bgcolor="#D9D9FF"><img src="images/logo.jpg" width="238" height="61"></td>
              </tr>
            </table>
	<?
	}
	?>
            <br>
			<font color="#005b90">Transaction Date: </font><? echo date("F j, Y")?><br>
            </fieldset></td>
          </tr>

          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Customer Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
		
			$queryCust = "select c_id, c_name,  c_address, c_address2, c_city, c_state, c_zip,c_country, c_phone, c_email  from ".TBL_CUSTOMER." where c_id ='" . $_POST[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3" align="left"><? echo $customerContent["c_name"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3" align="left"><? echo $customerContent["c_address"] . " " . $customerContent["c_address2"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200" align="left"><? echo $customerContent["c_email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 

			
			{
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email from ".TBL_BENEFICIARY." where benID ='" . $_POST["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Beneficiary Name</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2" align="left"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200" align="left"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			  if($_POST["transType"] == "Bank Transfer")
			  {
		  			//$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
					?>
                      <tr>
                        <td colspan="2" align="left"><span class="style5">Beneficiary Bank Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" align="left"><? echo $_POST["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200" align="left"><? echo $_POST["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" align="left"><? echo $_POST["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				if($_POST["benCountry"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($_POST["benCountry"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                          </font></td>
                        <td width="200" align="left"><?
				if($_POST["benCountry"] == "United States" || $_POST["benCountry"] == "Brazil")
				{
				?>
                          <? echo $_POST["ABACPF"]; ?>
                          <?
				}
				?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" align="left"><? echo $_POST["branchAddress"]; ?>                        </td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Swift Code</font></td>
                        <td width="200" align="left"><? echo $_POST["swiftCode"]; ?>                        </td>
                        <td width="100" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" title="For european Countries only">&nbsp;</td>
                      </tr>
                      <?
  }
			  if($_POST["transType"] == "Pick up")
			  {
					$benAgentID = $_SESSION["benAgentID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $benAgentID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr align="left">
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Address</font></td>
                        <td width="200" rowspan="2" align="left" valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">&nbsp;
                          </font></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Compnay </font> </td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
                        <td width="100" height="20" align="right"><font color="#005b90">City</font></td>
                        <td width="200" height="20" align="left"><?  echo $senderAgentContent["cp_city"]; ?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Country</font></td>
                        <td width="200" height="20" align="left"><? echo $senderAgentContent["cp_country"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }
  ?>
            </table>
              </fieldset></td>
              </tr>
         
		<!-- 
		  <tr>
            <td><fieldset>
            <legend class="style2">Payment Collection point </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><font color="#005b90">Branch Name </font></td>
                <td><? //echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><?// echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? //echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Bank Name </font></td>
                <td width="200"><?// echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? //echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? //echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? //echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>-->
              <?
			  }
			  ?>
          <tr>
            <td></td>
          </tr>
		  
		 
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Exchange Rate</font></td>
                <td align="left"><? echo $_POST["exchangeRate"]?> </td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" align="left" ><? echo $_POST["transAmount"]?> in  <? echo $_POST["currencyFrom"]?></td>
              </tr>
			  <tr>
			    <td align="right"><font color="#005b90">IM Fee</font></td>
			    <td align="left"><? echo $_POST["IMFee"]?> </td>
                <td width="100" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200" align="left"><? echo $_POST["localAmount"]?>  in  <? echo $_SESSION["BenCurrencyName"];?>              </td>
			  </tr>
              <tr>
                <td align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td align="left"><? echo $_POST["transactionPurpose"]?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" align="left"><? echo $_POST["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td align="right"><font color="#005b90">Money Paid</font></td>
                <td align="left"><? echo $_POST["moneyPaid"]?> </td>
				<?
				if($_SESSION["moneyPaid"] == 'By Cash')
				{
				?>
                <td width="100" align="right"><font color="#005b90">Bank Charges</font></td>
                <td width="200" align="left"><? echo $_POST["bankCharges"]?>
                </td>
				<?
				}
				else
				{
				?>
                <td width="100" align="right">&nbsp;</td>
                <td width="200" align="left">&nbsp;</td>			
				<?
				}
				?>
              </tr>
              <tr>
                <td width="150" align="right">&nbsp;</td>
                <td width="200">&nbsp;</td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
  <?
  if($_SESSION["moneyPaid"] == 'By Credit Card' || $_SESSION["moneyPaid"] == 'By Debit Card')
  {
  ?>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>
					  <td colspan="4"><fieldset>
                    <legend class="style2">Credit/Debit Card Details </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["cardType"];?>
						</td>
                        <td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
                        <td width="183" height="20" align="left">
						<? echo $_SESSION["cardNo"];?>
						</td>
                      </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
                        <td width="196" height="20" align="left">
						<? echo $_SESSION["expiryDate"];?>
						</td>
                        <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $_SESSION["cvvNo"];?>
						</td>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>		
<?
}
?>					  			  

<!--//////////////////////////////////-->
<script>
function checkForm(theForm) {

	var strErr = "";
	strEmpty = /^\s*$/;
	strDigit = /^\d+$/;
	strEml = /^[A-Za-z0-9-_.]+\@[A-Za-z0-9-_]+.[A-Za-z0-9-_.]+$/;
	strLogin = /^[A-Za-z0-9-_]+$/
	
	if(theForm.pincode.value == "" || IsAllSpaces(theForm.pincode.value)){
    	alert("Please provide PINCODE.");
        theForm.pincode.focus();
        return false;
    }
	return true;
}	
</script>
  <form name="addTrans" action="add-transaction-conf.php" method="post" onSubmit="return checkForm(this);">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">			 
					  <td colspan="4"><fieldset>
                    <legend class="style2">PIN Code </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      
                      <tr>
                        <td  height="20" align="right"><font color="#005b90">PIN Code</font> </td>
                        <td  height="20" align="left" colspan="3">
						<input type="text" name="pincode" value="<?=$_SESSION["pincode"]; ?>" maxlength="5">
						</td>
 <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>		

<!--////////////////////////////-->

          <tr>
            <td align="center">
				
				  				  
				  
	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;
	  ?>


			<input type="submit" name="Submit" value="Confirm Order">
            <input type="button" name="Submit" value="Print this Receipt" onClick="print()">
                  </form>			
			</td>
          </tr>
          <tr>
            <td align="right"><a href="add-transaction.php?transID=<? echo $_POST["transID"]?>" class="style2">Change Information</a></td>
          </tr>
      </table></td>
			<td valign="top"><?
		include "exchange-rates-agent.php";
		?></td>		
    </tr>

</table>
</body>
</html>
<?

include "footer.php";
?>