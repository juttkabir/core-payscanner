<?
session_start();
include("../include/config.php");
dbConnect();
if (!$username=loggedUser())
{
?>
<form action="index.php" target="_top" method="get">
<input type="hidden" name="msg" value="out">
<input type="hidden" name="PHPSESSID" value="<?=$PHPSESSID;?>">
</form>
<script language="JavaScript">
	document.forms[0].submit();
</script>
<?
}
?>
<html>
	<head>
		<title><?=TITLE_ADMIN; ?></title>
<script language="javascript" src="../javascript/functions.js"></script>
<script language="JavaScript">
<!--
function submitForm() 
{
	if (isPassword() && isNewPassword() && isPasswordSame())
		return true;
	else
		return false;
}
/*
function isLoginName() 
{
	var str = document.forms[0].elements[0].value;
	if ( str=="" )
		{
			alert("\nUser-ID is blank, Please write your Login name.")
			document.forms[0].elements[0].focus();
			return false;
		}
return true;
}*/

function isPassword() 
{
	var str = document.forms[0].elements[1].value;
	if (str == "") 
		{
			alert("\nThe Password field is blank, Please enter Password Again.")
			document.forms[0].elements[1].focus();
			return false;
		}
return true;
}

function isNewPassword() 
{
	var str = document.forms[0].elements[2].value;
	if (str == "") 
		{
			alert("\nThe New password field is blank, Please write new password.")
			document.forms[0].elements[2].focus();
			return false;
		}
	if (str.length<8) 
		{
			alert("\nThe New password should be greater than 8 Characters")
			document.forms[0].elements[2].focus();
			return false;
		}
	
return true;
}
function isPasswordSame() 
{
	var str1 = document.forms[0].elements[2].value;
	var str2 = document.forms[0].elements[3].value;
	if (str1 != str2) 
		{
			alert("\nPassword mismatch, Please Retype same Passwords in both fields.")
			document.forms[0].elements[2].focus();
			return false;
		}
return true;
}
//-->
</script>
<link rel="stylesheet" href="images/interface.css" type="text/css">
</head>
<body onLoad="document.forms[0].OldPass.focus();">
<p>&nbsp;</p>
<table width=50% border="1" align="center" bordercolor="#006699">
  <tr align="center" bgcolor="#006699" valign="middle"> 
    <td height="20"><font color="#ffffff"><b>Change Password</b></font></td>
  </tr>
    <tr> 

      <td align="center" valign="middle" bordercolor="#FFFFFF"> 
        <form name="chpass" method=post action="smChangePass.php" onSubmit="return submitForm();">
	      <table width="90%" border="1" align="center">
            <tr bordercolor="#FFFFFF" bgcolor="#DBE3E1"> 
              <td colspan="2" align=center valign=bottom> <font color="#FF0000">&nbsp; 
                <? if ($msg == "Y")
					echo "Your old password has expired. Please keep changing your password after 30 days to make your account more and more secure.";
					if ($msg != "Y" && $msg != "")
				  		echo $msg; ?>
                </font></td>
            </tr>
            <tr bordercolor="#FFFFFF"> 
              <td width="167"  align=right 
                        valign=center>User Name:</td>
              <td width="174"  align=left 
                        valign=center> <?echo $username?>
                <input type="hidden" name="msg" size="17" value="<?echo $msg; ?>"> 
              </td>
            </tr>
            <tr bordercolor="#FFFFFF"> 
              <td width="167" align="right">Old Password:</td>
              <td width="174"  align="left"> 
                <input type="password" name="OldPass" size="17" class="flat"> 
              </td>
            </tr>
            <tr bordercolor="#FFFFFF"> 
              <td width="167"  align=right>New Password:</td>
              <td width="174"  align=left> 
                <input type="password" name="NewPass" size="17" class="flat"> 
              </td>
            </tr>
            <tr bordercolor="#FFFFFF"> 
              <td width="167" align=right>Confirm password:</td>
              <td width="174" align=left> 
                <input type="password" name="ConPass" size="17" class="flat"> 
              </td>
            </tr>
            <tr align="center" bordercolor="#FFFFFF"> 
              <td  colspan="2"> 
                <input type="hidden" name="sec" value="<?echo md5("04")?>"> 
                <input type="hidden" name="sid" value="<?echo $sid?>"><input type="hidden" name="UserID" size="17" value="<?echo $username?>" readOnly class="flat"> <input name="submit" type=submit class="flat" value="Change Password!"> 
              </td>
            </tr>
         
        </table> 
        </form> </td>
    </tr>
</table>
<div align="center"></div>
