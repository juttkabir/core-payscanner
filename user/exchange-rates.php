<?
//session_start();
require_once ("../include/config.php");
//require_once ("./include/flags.php");
$today = date("Y-m-d");
dbConnect();
//debug($_SESSION);
$logedUseCountry = $_SESSION["loggedCustomerData"]["c_country"];
$query = "select * from " . TBL_EXCHANGE_RATES." WHERE countryOrigin = '$logedUseCountry' order by country ";
$curencies = selectMultiRecords($query);
//debug($query);
//debug($curencies);
$contentAgent = selectFrom("select c_country  from cm_customer where username = '$username'");
$country = ($_GET["currency"] == "GBP" || $contentAgent["agentCountry"] == "" ? "United Kingdom" : $contentAgent["agentCountry"]);

//$logedUseCountry = $_GET["logedUseCountry"];

//$query = "select * from " . TBL_EXCHANGE_RATES . " where  countryOrigin ='$logedUseCountry'  order by country ";
//$contentRate = selectFrom($query);
//$countryRate =  $contentRate["primaryExchange"];
//$dated =  $contentRate["dated"];
//$dated = explode("-",$dated);
////$dated[2]."".$dated[1]."".$dated[0];
//$date2 = explode(" ",$dated[2]);
//$dated2 =  $date2[0]."/".$dated[1]. "/".$dated[0];
//$unixdate = strtotime ($dated2);
$ukdate = date("j F, Y");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="images/interface.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Today's Exchange Rates</title>
<script language="javascript" src="../javascript/functions.js"></script>
<style type="text/css">
<!--

.style2 {color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>

<body>


	
	
	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="13"><img src="images/tableleftUP.jpg" width="13" height="37"></td>
            <td width="983" valign="middle"  background="images/headerBACK.jpg"></td>
            <td width="13" valign="top"><img src="images/tablerightUP.jpg" width="13" height="37"></td>
          </tr>
          <tr>
            <td background="images/tableleftLINE.jpg" align="right"></td>
            <td align="center">
	
	
	
	
	<table width="695"  border="0">
        <tr>
          <td><fieldset>
            <legend class="style2" title="Exchange rates updated Last on <? echo $ukdate;?>">Exchange rates updated Last on <? echo $ukdate;?></legend>
            <br>
            <a href="#" onClick="javascript:window.close();" class="style2">Close</a>
            <br>
                        <table border="0" align="center" cellpadding="5" cellspacing="1">
			<tr>
			<td>
			<b><strong><font color="#000000"> [ The 
      display exchange rates are for guide only ]</font></strong></b>
			</td>
			</tr>
            </table>
            <table width="681" border="0"  cellpadding="5" cellspacing="1">
						<tr>
			<td width="352" bgcolor="#DFE6EA" align="left"><font color='#005b90'><strong>Date</strong></font></td>
			<td width="352" bgcolor="#DFE6EA" align="left"><font color='#005b90'><strong>Destination Country</strong></font></td>
			<td width="210"  bgcolor="#DFE6EA" align="left"><font color='#005b90'><strong>Destination Currency</strong></font></td>
	    <td width="85" align="left" bgcolor="#DFE6EA"><font color='#005b90'><strong>Rate</strong></font></td>
	    <td bgcolor="#DFE6EA"><font color="#005b90"><b>Based On</b></font></td>
      <td bgcolor="#DFE6EA"><font color="#005b90"><b>Value</b></font></td>
			</tr>
			<?
			for($i=0;$i<count($curencies);$i++)
			{
			$margin = $curencies[$i]["marginPercentage"];
			
			 ?>
              <tr>
             <td width="210" align="left" bgcolor="#DFE6EA"><font color='#005b90'><? echo date("M j, Y", strtotime($curencies[$i]["updationDate"])) ?></font></td>
               <td  width="352" bgcolor="#DFE6EA" align="left"><? echo displayFlag($curencies[$i]["country"], './'); echo " <font color='#005b90'>" . $curencies[$i]["country"] . "</font>"?> </td>
	
				<td width="210" align="left" bgcolor="#DFE6EA"><font color='#005b90'><? echo $curencies[$i]["currency"]?></font></td>
                <td width="85" align="left" bgcolor="#DFE6EA">
					<font color='#005b90'>
						<? 
							if($curencies[$i]["isDenomination"] == "Y")
								echo "<b>Denomination Based Exchange Rate</b>";
							else
								echo round($curencies[$i]["primaryExchange"] - (($curencies[$i]["primaryExchange"]) * $margin / 100 ) , 4);
						?>
					</font>
				</td>
	<td bgcolor="#DFE6EA"><font color='#005b90'><?=strtoupper(stripslashes($curencies[$i]["rateFor"])); ?></font></td>
            
            <td bgcolor="#DFE6EA"><font color='#005b90'><? 
            		if(!empty($curencies[$i]["rateFor"]))
            		{
            			$agentName = selectFrom("select userID,username, name from ".TBL_ADMIN_USERS." where userID = '".$curencies[$i]["rateValue"]."'");
            			echo($agentName["name"]." [".$agentName["username"]."]");
           			}
					else
            			echo "-";
						
         			?></font>
            </td>
              </tr>
			  
			  <?php
			  	if($curencies[$i]["isDenomination"] == "Y")
				{
			  		$arrDenominationBasedRate = selectMultiRecords("select * from denominationBasedRate where parentRateId = '".$curencies[$i]["erID"]."' order by id");
					//debug($arrDenominationBasedRate);
					
					$intAmountFrom = $arrDenominationBasedRate[0]["amountFrom"];
					$intAmountUpto = $arrDenominationBasedRate[count($arrDenominationBasedRate)-1]["amountTo"];
					$intInterval = $arrDenominationBasedRate[0]["amountTo"]-$arrDenominationBasedRate[0]["amountFrom"] + 1;
					if($arrDenominationBasedRate[0]["amountTo"] == "0")
						$intInterval = $intInterval + 1;
					
					for($xc=0; $xc<count($arrDenominationBasedRate); $xc++)
						$arrDenomIntervalRate[$xc] = $arrDenominationBasedRate[$xc]["exchangeRate"];
						
						
					$intTotalIntervals = ceil(($intAmountUpto - $intAmountFrom)/$intInterval);
			  ?>
			  <tr bgcolor="#DFE6EA" id="detailRow">
				<td align="left" colspan="6">
					<fieldset>
						<legend>Denomination Based Exchange Rate</legend>
						<table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<th width="30%" align="center">From</th>
								<th width="30%" align="center">To</th>
								<th width="40%" align="center">Exchange Rate</th>
							</tr>
							
							<? for($in = 0; $in < $intTotalIntervals; $in++) 
							   { 
							   		$upToAmountVal = ($intInterval*$in)+$intInterval+($intAmountFrom) - 0.01;
									if($upToAmountVal > $intAmountUpto)
										$upToAmountVal = $intAmountUpto;
							?>
								<tr bgcolor="#ededed">
									<td align="center"><?=$intAmountFrom+($intInterval*$in)?></td>
									<td align="center"><?=$upToAmountVal?></td>
									<td align="center">
										<?=$arrDenomIntervalRate[$in]?>
									</td>
								</tr>
							<? } ?>
						</table>
					</fieldset>		
	
				</td>
			</tr>
			  
			  <?
			  	}
			  }
			  ?>

          </table>
            <br>
          </fieldset></td>
        </tr>
</table>

	
	</td>
            <td background="images/tablerightLINE.jpg" align="left"></td>
          </tr>
          <tr>
            <td><img src="images/tableleftDOWN.jpg" width="13" height="18"></td>
            <td  background="images/tableDOWNLINE.jpg">&nbsp;</td>
            <td><img src="images/tablerightDOWN.jpg" width="13" height="18"></td>
          </tr>
</table>
</body>
</html>