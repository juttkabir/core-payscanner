<?
function debug( $variableToDebug = "", $shouldExitAfterDebug = false )
{
	if ( defined("DEBUG_ON")
		 && DEBUG_ON == true )
	{
		$arrayFileDetails = debug_backtrace();
		echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
	
		if ( !empty($variableToDebug)  )
		{
			echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
		}
		else
		{
			echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
		}
		echo "</pre>";
		if ($shouldExitAfterDebug)
			exit();
	}
}

function CalculateFromTotal($imFee1, $totalAmount1, $customerCountry, $benificiaryCountry, $dist, $senderAgentID, $payinBook, $feeTransType, $currencyFrom, $currencyTo)
{
	///Calculating Fee/////////
	$agentType = getAgentType();
	$imFee = 0;
	$totalAmount = $totalAmount1;

	if($imFee1 > 0 || (CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0))
	{
		$imFee = $imFee1;
		$amount = $totalAmount1 - $imFee;
		
		}else{
			
			$amount2= 0;///New calculated TransAmount
			$amount = $totalAmount;///TransAmount initially assumed as total Amount when Fee is Zero
			$fee2 = 0;////Fee is set to be Zero initially
			$count = 0;///To Avoid inifinit loop in situation where wrong amount entry and passing all conditions
				//while($amount != $amount2 && $count < 6)
				{////While Loop is commented to see the Total amount Based Commission
					//// Max. 6 Iterations 
					/// uptill last iteration amount is not equal 
					/// to current iteration amount
				
					
					$flag = false;///To check if Fee is not calculated in a particular iteration
					if(CONFIG_PAYIN_CUSTOMER != '1')
					{
						if(CONFIG_FEE_BASED_COLLECTION_POINT == 1)
						{
							$flag = true;
							
							$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
						
						}
						if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
						{
							$flag = true;
					
							$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
							
						}elseif((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
						{
							$flag = true;
					
						 $imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);	
						 
						}
						if(!$flag || $imFee <= 0)
						{
							$imFee = imFee($amount,  $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);	
						
						}
					}else
					{	
						if(CONFIG_FEE_BASED_COLLECTION_POINT == 1)
						{
							$flag = true;
							if($payinBook)
								$imFee = imFeeAgentPayin($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
							else
								$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
						
						}
						if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
						{ 
							$flag = true;
							if($payinBook)
								$imFee = imFeeAgentPayin($amount,  $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
							else
								$imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
						
						}elseif((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
						{
							$flag = true;
						
							if ($payinBook) {
								$imFee = imFeeAgentPayin($amount, $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);
								
							} else {
								$imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);
								
							}
								
						}
						if(!$flag || $imFee <= 0)
						{
								if($customerContent["payinBook"] != "" )
								{
									$imFee = imFeePayin($amount, $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);	
									
								}else
								{
									$imFee = imFee($amount, $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);
									
								}
						}
				}
				
				////If Both Fees calculated in consecutive iterations are not same
				if($imFee != $fee2)
				{
					$fee2 = $imFee;
				}
				$amount2 = $amount;
				$amount = $totalAmount - $imFee;
				$count++;
				
			}
			
		  }


	$feeData[] = $amount;
	$feeData[] = $imFee;
	return $feeData;
}

function UploadFile($Bext,$Uext)
{

	if ($BankcsvFile=='none' && $UsercsvFile=='none')
	{
		$msg = "Please select CSV files.";
		header("Location: import-excel-files.php?msg=$msg");
	}
	$uploadDir = '../uploads/';
	$uploadFile = $uploadDir . $_FILES['BankcsvFile']['name'];
	$uploadFile2 = $uploadDir . $_FILES['UsercsvFile']['name'];

	if($uploadFile == "./uploads/")
	{
	 return $BankcsvFile;
	}
	if($uploadFile == "./uploads/")
	{
	 return $UsercsvFile;
	}

		if (($Bext == ".csv"  ) && ( $Uext == ".csv"  ))
		{
				//$nTime=time();
				//$nTime=$nTime-1000000000;						
				$arrName=explode(".",$uploadFile);
				$arrName2=explode(".",$uploadFile2);			
				$uploadFile=$uploadDir."Bank_Report".".".$arrName[sizeof($arrName1)-1];
				$uploadFile2=$uploadDir."Daily_Activity_report".".".$arrName[sizeof($arrName2)-1];
				$reUploadFile = $nTime.".".$arrName[sizeof($arrName)-1];
			
						
				if (move_uploaded_file($_FILES['BankcsvFile']['tmp_name'], $uploadFile) && move_uploaded_file($_FILES['UsercsvFile']['tmp_name'], $uploadFile2))
				{
					//print "File is valid, and was successfully uploaded. ";//print "Here's some more debugging info:\n";//print_r($_FILES);
				}
				else
				{
					print "Possible file upload attack!  Here's some debugging info:\n";
					print_r($_FILES);
				}	
				
				//return $reUploadFile;
		}
		else
		{
					$msg = "File format must be  .csv";
					header("Location: import-excel-files.php?msg=$msg");		
		}
}



function ibanCountries($country)
{
	$listCountry[0] = "Austria";
	$listCountry[1] = "Belgium";
	$listCountry[2] = "Denmark";
	$listCountry[3] = "Finland";
	$listCountry[4] = "France";
	$listCountry[5] = "Germany";
	$listCountry[6] = "Greece";
	$listCountry[7] = "Iceland";
	$listCountry[8] = "Ireland";
	$listCountry[9] = "Italy";
	$listCountry[10] = "Luxembourg";
	$listCountry[11] = "Netherlands";
	$listCountry[12] = "Norway";
	$listCountry[13] = "Poland";
	$listCountry[14] = "Portugal";
	$listCountry[15] = "Spain";
	$listCountry[16] = "Sweden";
	$listCountry[17] = "Switzerland";
	$listCountry[18] = "United Kingdom";
	
	if(in_array($country, $listCountry))
		return true;
	else
		return false;
}

function getAgentType()
{
	global $username;
	if($username != "")
	{
		$users = selectFrom("select parentID, agentType, adminType, isCorrespondent, isMain from " . TBL_ADMIN_USERS . " where username='$username'");
		if(trim($users["parentID"]) == 0 && trim($users["isMain"]) == "Y")
		{
			return "admin";
		}
		elseif(trim($users["adminType"])!="Agent" && trim($users["isMain"]) == "N")
		{
			return trim($users["adminType"]);
		}
		elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Supper")
		{
			switch ($users["isCorrespondent"])
			{
				case "Y":
				{
					return "SUPAI"; // super-agent-IDA
					break;
				}
				case "N":
				{
					return "SUPA"; // super-agent
					break;
				}
				case "ONLY":
				{
					return "SUPI"; // super--IDA
					break;
				}
			}
		}
		elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Sub")
		{
			switch ($users["isCorrespondent"])
			{
				case "Y":
				{
					return "SUBAI"; // super-agent-IDA
					break;
				}
				case "N":
				{
					return "SUBA"; // super-agent
					break;
				}
				case "ONLY":
				{
					return "SUBI"; // super--IDA
					break;
				}
			}
		}
	}
	else
	{
		return false;
	}
}

function getCalculatorExchangeRates($custCountry, $benCountry,$dist,$amount)
{
	if ($custCountry != "" && $benCountry != "")
	{
		// add these queries with date functionality for ticket #349
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where countryOrigin='$custCountry' and dated =(select  Max(dated) from " . TBL_EXCHANGE_RATES . " where countryOrigin='$custCountry')");		
		
		if($dist != '' )
		{
			$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry' and rateFor = 'distributor' and rateValue = '$dist' and dated = (select Max(dated) from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry' and rateFor = 'distributor' and rateValue = '$dist')");///For Distributor
		}
		
		// comment these for add new functionality ticket #349
		//$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'"); 
		//$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  rateFor = 'distributor' and rateValue = '$dist'");///For Distributor
		//end
		
		/*if($exchangeRateTo["primaryExchange"] == "")
		{should be done for money....get all records with money and compare the amount range if find a match get that other wise discard.
		
			$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry' and rateFor = 'money'");///Generic One
			
		}*/
		if($exchangeRateTo["primaryExchange"] == "")
		{
			// add this query with date functionality for ticket #349
			$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where dated =(select  Max(dated) from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry' and (rateFor = '' OR rateFor = 'generic'))");///Generic One
			//$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  (rateFor = '' OR rateFor = 'generic')");///Generic One
			
			}
		
		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			 $rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
			 echo (" [line 180 ".$rate."]");
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
			echo (" [line 187 ".$rate."]");
			}
			else
			{
				$rate =round(1 - (1*$exchangeRateTo["marginPercentage"]) /  100 , 4);			
			echo (" [line 192 ".$rate."]");
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];
		
		return $data;
	}
}
function getMultipleExchangeRates($custCountry, $benCountry, $currencyTo, $agent, $amount, $currencyFrom)
{
	//debug_print_backtrace();
//	$fromCountry = selectFrom("select * from " . TBL_COUNTRY . " where countryName = '$custCountry' ");
	$custLoggedQ = "select agentId from ".TBL_CM_CUSTOMER. " where c_id = '".$_SESSION["loggedCustomerData"]["c_id"]."'";
	$custLogged = selectFrom( $custLoggedQ);
	if($custLogged['agentId']!="")
		$agent = $custLogged['agentId'];
	// flag to calculate exchange rate for denomination or simple
	$denomination = "1";
	if($custCountry != "" && $benCountry != "")
	{

// Agent Denomination rate
if($custLogged['agentId']!=""){
	$dj = "select * from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N'  and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount')";
	$exchangeRate = selectFrom($dj);

// Agent Simple rate
	if($exchangeRate == ""){
	$dj = "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isDenomination!='Y' and rateValue = '$agent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and isDenomination!='Y' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent')";
	$exchangeRate = selectFrom($dj);
	$denomination = "";
	}
}
// Generic Denomination rate
	if($exchangeRate == ""){
		$dj = "select * from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and rateValue = '' and currencyOrigin = '$currencyFrom' and isActive !='N'  and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount')";
		$exchangeRate = selectFrom($dj);
		$denomination = "1";
// Distributor Simple rate
/*	if($exchangeRate == ""){
	$dj="select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and isDenomination!='Y' and currencyOrigin = '$currencyFrom' and rateValue = '$dist' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and isDenomination!='Y'  and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent')";
		$exchangeRate = selectFrom($dj);
		$denomination = "";
		}*/
	if($exchangeRate == ""){
// Generic Simple rate
$dj="select * from " . TBL_EXCHANGE_RATES . " where  country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isDenomination = 'N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isDenomination = 'N')";
		$exchangeRate = selectFrom($dj);
		$denomination = "";
		}
	}	
			
		//debug($dj);
	
	if($denomination == "1")
		$rate =$exchangeRate["exchangeRate"];
	else
		$rate =round($exchangeRate["primaryExchange"] - ($exchangeRate["primaryExchange"] * $exchangeRate["marginPercentage"]) /  100 , 4);

		$data[] = $exchangeRate["erID"];
		$data[] = $rate;
		$data[] = $currencyFrom;
		$data[] = $exchangeRate["currency"];
		return $data;
		
	}
}
function getExchangeRate($custCountry, $benCountry, $currencyTo)
{
	if($custCountry != "" && $benCountry != "")
	{
	
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'");
		$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  currency = '$currencyTo'");

		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			$rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
			}
			else
			{
				 $rate =round(1 - (1 * $exchangeRateTo["marginPercentage"]) /  100 , 4);			
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];		
		
		return $data;
	}
}

function getExchangeRateTransaction($custCountry, $benCountry)
{
	if($custCountry != "" && $benCountry != "")
	{
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'");
		$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry'");

		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			$rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
			}
			else
			{
				$rate =round(1 - (1*$exchangeRateTo["marginPercentage"]) /  100 , 4);			
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];
		
		return $data;
	}
}




function imFeeAgent($amount, $origCountry, $destCountry, $agentID)
{
	$custLoggedQ = "select agentId from ".TBL_CM_CUSTOMER. " where c_id = '".$_SESSION["loggedCustomerData"]["c_id"]."'";
	$custLogged = selectFrom( $custLoggedQ);
	if($custLogged['agentId']!="")
		$agentID = $custLogged['agentId'];
	if($amount > 0 && $origCountry != "" && $destCountry != "")
	{
		$fee = 0;
		// agent Denomination based		
		$imFeeQ = "select Fee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and feeType='agentDenom'";
		$imFee = selectFrom($imFeeQ);
	if($imFee==""){
		// agent based
		$imFeeQ = "select Fee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and feeType='agentBased'";
		$imFee = selectFrom($imFeeQ);
	}
	if($imFee==""){
		// Denomination based
		$imFeeQ = "select Fee, feeType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and feeType='denominator'";
		$imFee = selectFrom($imFeeQ);
	}
	if($imFee==""){
		$imFeeQ = "select Fee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry'";
		$imFee = selectFrom($imFeeQ);
	}
		if($imFee["Fee"] != "" && $imFee["Fee"] != 0)
		{
			 if($imFee["feeType"]=="percent")
	     {
	  		$fee = ($amount * $imFee["Fee"])/100;
	  		}
	  	else{
	  		$fee = $imFee["Fee"];
	  		}
				return $fee;
		}
		else
		{
			return false;
		}
	}
}

function imFee($amount, $origCountry, $destCountry )
{
	if($amount > 0 && $origCountry != "" && destCountry)
	{
	
//	echo "select Fee from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry'";
		$imFee = selectFrom("select Fee, feeType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry'");
		if($imFee["Fee"] != "" && $imFee["Fee"] != 0)
		{
			if($imFee["feeType"]=="percent")
            	{
            		$fee = ($amount * $imFee["Fee"])/100;
            		}
            	else{
            		$fee = $imFee["Fee"];
            		}
			return $fee;
		}
		else
		{
			return false;
		}
	}
}

function sendMail($To,$Subject,$Message,$Name, $From)
{
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $Name <$From>\r\n";
		@mail($To,$Subject,$Message,$headers);
}
// create promotional code of any length
function createCode($length=8){
	$pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$promoCode = "";
	for ($i=0; $i < $length; $i++){
		$c = substr($pool, (rand()%(strlen($pool))), 1);
		$promoCode .= $c;
	}
	return $promoCode;
}
function checkValues($variable){
//	$variable = strip_tags($variable);
	$variable = addslashes($variable);
	$variable = trim($variable);
	return $variable;
}
// Email address validity
function validEmail($email) {
  $valid = 0;
  if(ereg("([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)", $email)) {
    $valid = 1;
  }
  return ($valid);
}
// Random number generation
function MyRandom($min,$max) {
  srand(time());
  $random = (rand()%$max) + $min;
  return ($random);
}

// Generates 14 digit random Id based on time, IP address and pid
function genRandomId()
{
  srand((double)microtime()*1000000);
  $abcdef = rand(10000,32000);
  $ip = getenv ("REMOTE_ADDR");
  $ip = substr($ip, 0, 8);
  $ip = preg_replace("/\./","",$ip);
  srand($ip);
  $ghij = rand(1000,9999);
  $pid = getmypid();
  srand($pid);
  $kl = rand(10,99);
  $number = $abcdef.$ghij.$kl;
  return $number;
}

// return image co-ordinates after resizing to the wanted 
function Resize($x,$y,$targetx,$targety) {
  $ratio = $x/$y;
  if ($x > $y) {
    $newx = $targetx;
    $newy = $newx / $ratio;
    if ($newy > $targety) {
      $newy = $targety;
      $newx = $newy * $ratio;
    }
  }
  else {
    $newy = $targety;
    $newx = $newy * $ratio;
    if ($newx > $targetx) {
      $newx = $targetx;
      $newy = $newx / $ratio;
    }
  }
  return array($newx,$newy);
}

// Returnbs name of the month
function Num2Month($month) {
if ($month=="1")  { return ("Jan"); }
if ($month=="2")  { return ("Feb"); }
if ($month=="3")  { return ("Mar"); }
if ($month=="4")  { return ("Apr"); }
if ($month=="5")  { return ("May"); }
if ($month=="6")  { return ("Jun"); }
if ($month=="7")  { return ("Jul"); }
if ($month=="8")  { return ("Aug"); }
if ($month=="9")  { return ("Sep"); }
if ($month=="10") { return ("Oct"); }
if ($month=="11") { return ("Nov"); }
if ($month=="12") { return ("Dec"); }

}
// return false on failure and array of records on success
function SelectMultiRecords($Querry_Sql)
{   
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
	    $count = 0;
		$data = array();
		while ( $row = mysql_fetch_array($result)) 
		{
			$data[$count] = $row;
			$count++;
		}
			return $data;
	}
}

function selectFrom($Querry_Sql)
{ //echo DEBUG . $Querry_Sql;
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}

function countRecords($Query)
{
        //monitor_sql($Query);
	
   	if((@$result = mysql_query ($Query))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Query);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))

   		{
      		return $check[0];
   		}
			return false;	
	}
/*
	$query="SELECT count(id) as cnt FROM " . TBL_MESSAGES . " where dest='$username'";
	$contents=selectFrom($query);*/
}

function isExist($Querry_Sql)
{//	echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}

function insertInto($Querry_Sql)
{	//echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}
function update($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
		echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;
   	}
}
function deleteFrom($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}

function mysql_message($Querry_Sql)
{
	$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>MySql Debugger:</font></strong><br>";
	$msg .= "Error in your Query: $Querry_Sql<BR>";
	$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
	$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
	echo $msg;
}

function fetchData($query)
{
	if((@$result = mysql_query ($query))==FALSE)
   	{
		return false;
	}   
	else
	{
		while ($row = mysql_fetch_array($result))
		{
			echo "<option value=$row[0]>$row[1]</option>\n\t";
		}
	}
}

function FillDateCombo($start, $end, $flag=0)
{
	if ($flag == 0){
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>$i</option>\n\t";
	} else {
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>". Num2Month($i)."</option>\n\t";
	}
}

function insertError($error) //insert error into session
{
	session_register("error");
	$_SESSION['error']  = $error;
}

function getIP() 
{
	if ($_SERVER) 
	{
		if ( $_SERVER["HTTP_X_FORWARDED_FOR"] ) 
		{
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} 
		elseif ( $_SERVER["HTTP_CLIENT_IP"] ) 
		{
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} 
		else 
		{
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} 
	else 
	{
 	if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) 
	{
		$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
	}
	elseif ( getenv( 'HTTP_CLIENT_IP' ) ) 
	{
		$realip = getenv( 'HTTP_CLIENT_IP' );
	} 
	else 
	{
		$realip = getenv( 'REMOTE_ADDR' );
	}
}
return $realip; 
}

function createSession($username)
{
        $ip=getIP();
		$myCustomerSession="";
		$myCustomerSession = md5(uniqid(rand()) . genRandomId());  // To Creat a unique SessionID
		setcookie ("Customercookie", $myCustomerSession, time()+7200,  '/');

        $now=date('Y-m-d H:i:s');        // Format of Session Start Date
		if (isExist("select username from ". TBL_SESSIONS . " where username='$username'"))
		{
			deleteFrom("delete from " . TBL_SESSIONS . " where username='$username'");
		}
		insertInto("INSERT INTO " . TBL_SESSIONS . "(session_id, username, session_time, ip) VALUES('$myCustomerSession','$username','$now', '$ip')");	

		//if ($is_admin == 'Y')
			$sessionCustomerTime = time();
			$_SESSION['sessionCustomerTime'] = $sessionCustomerTime;
			//$_SESSION['is_admin'] = $is_admin;
		//} 
		//if ($_SESSION["is_admin"] == "Y"){
			$loginContents = selectFrom("select * from " . TBL_CM_CUSTOMER . " where username='$username'");
						
			session_register("loggedCustomerData");
			$_SESSION["loggedCustomerData"] = $loginContents;
			session_register("c_id");
			$_SESSION["c_id"] = $loginContents["c_id"];
			session_register("cust_id");
			$_SESSION["cust_id"] = $loginContents["c_id"];
			session_register("DisplayName");
			$_SESSION["DisplayName"] = $loginContents["c_name"];
			session_register("changedPwd");
			$_SESSION["changedPwd"] = $loginContents["c_pass"];
		//}
		 $_SESSION["sCustomerId"]= $myCustomerSession;
}

function loggedUser() // returns the valid logged username
{
	global $chPassDate;
	$sessionID=$_SESSION["sCustomerId"];
	$now=date('Y-m-d H:i:s');
	//echo "SID: " . $sessionID;
	//echo "SELECT username from ". TBL_SESSIONS ." where session_id='$sid'";
	$sql4session="SELECT username from ". TBL_SESSIONS ." where session_id='$sessionID'";
    if (!isExist($sql4session) || !isset($sessionID)){
		return false;
	}
	update("update " . TBL_SESSIONS . " set session_time='$now' where session_id='$sessionID'");	
    $content=selectFrom($sql4session);
	if ($_SESSION["is_admin"] == "Y"){
		$loginContents = selectFrom("select c_id from ".TBL_CM_CUSTOMER." where username='".$content["username"]."'");
		//$chPassDate = date_diff($loginContents["changedPwd"], $now);
		session_register("customerID");
		$_SESSION["customerID"] = $loginContents["c_id"];
	}
	// check that a valid session exists, and has not timed out
	//echo "sad: " . $_SESSION["sessionCustomerTime"];
	//exit;
	if (($_SESSION["sessionCustomerTime"] == '') or ((time()-$_SESSION["sessionCustomerTime"]) > 7200)) {
		//session_unset();
		//header("Location: index.php?PHPSESSID=$PHPSESSID");
		//exit;
	} else {
		// on success, update the session time
		$_SESSION["sessionCustomerTime"] = time();
		setcookie ("Customercookie", $_SESSION["sCustomerId"], time()+7200,  '/');
	}

    return $content["username"];
}

function dateFormat($date, $pgFlag=1)
{
	if ($pgFlag != 1)
		return date("M j, Y", strtotime($date));
	else
		return date("m/d/Y", strtotime($date));
}

function extension($PicFile)
{
        $Ext = strtolower(strrchr($PicFile,"."));
		//return $ext;
        if ($Ext ==".gif" || $Ext ==".jpg" || $Ext ==".jpeg" || $Ext ==".png")
        {
            return 1;
        }
        else
        {
			return 0;
        }
}

function adminPicture($pic_name, $size="s")
{
	$image="../thumbs/bucket" . substr($pic_name, 12,2) . "/s_" . $pic_name;									
	return "<img src='$image' border=0>";
}

function display_error_message($message="")
{
	//global $_GET["error"];
	if (isset($_GET['error']) && $_GET['error'] == 'Y' && $message=="")
	{
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br><?    } 
	else
	{ if ($message !=""){
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br>
<?
	}}
}

function email_check($email)
{
	if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		return false;
	}
	else 
	{
		return true;
	}
}

function redirect($url)
{
	global $PHPSESSID;
	if(strstr($url, '?'))
	{
		header("location: $url&PHPSESSID=$PHPSESSID");
	}
	else
	{
		header("location: $url?PHPSESSID=$PHPSESSID");
	}
	exit();
}

function cuttext($tring, $cuton) 
{
	$space=" ";
	if (!strstr($tring,$space)) 
	{
		/* No space is found in the whole string: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	}
	
	if (substr($tring,$cuton,1)==$space) 
	{
		/* 21st character is a space: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	} 
	else 
	{
		/* 21st Character is NOT a space: return to last space and cut there */
		while ($teller <= $cuton) 
		{
			if (substr($tring,$cuton-$teller,1)==$space) 
			{
				$tring=substr($tring,0,$cuton-$teller);
				break;
			}
		$teller++;
		}
	}
	return $tring;
}

function date_diff($str_start, $str_end) 
{ 
	$str_start = strtotime($str_start); // The start date becomes a timestamp 
	$str_end = strtotime($str_end); // The end date becomes a timestamp 
	
	$nseconds = $str_end - $str_start; // Number of seconds between the two dates 
	$ndays = round($nseconds / 86400); // One day has 86400 seconds 
	return $ndays;
} 
function upload_picture($picture, $width, $height, $targetDir="../uploads/"){
//	global $picture;
	list($usec, $sec) = explode(" ",microtime());
	$createdID = eregi_replace(".","_", $sec + $usec);
	echo $picture_name;
	if (extension($picture_name)==1){
		$ext = strrchr($picture_name,".");
		if (strtolower($ext) == ".gif"){
			$ext = ".png"; 
		}
		if (@is_uploaded_file($picture)){ 
			// to further hide image name we add random number
			$picID = $createdID;
			$catPic = $targetDir. "c_".$picID . $ext;
			$catImage = "c_".$picID . $ext;

			$original = $targetDir. "o_c_" . $picID . $ext;
			@move_uploaded_file($picture, $original);
			@chmod ($original, 0755);   
			$size = getimagesize($original);
			if ($size[2]==1) { // if GIF, convert to png and then load 
	        	exec ("/usr/bin/gif2png $original");
       			$pngoriginal = $targetDir. "o_". $picID. ".png";
       			$im_in = imagecreatefrompng($pngoriginal); 
			}
			if ($size[2]==2) // if JPEG 
				$im_in = @imagecreatefromjpeg($original);
			if ($size[2]==3) // if PNG 
				$im_in = @imagecreatefrompng($original); 

			@imagegammacorrect($im_in, 1.0, 1.4);
			// Category Pictue
			list ($catx,$caty) = Resize($size[0],$size[1],$width,$height);
			
			$catImage = imagecreatetruecolor($catx,$caty);
			@imagecopyresized($catImage,$im_in,0,0,0,0,$catx,$caty,$size[0],$size[1]);
			if ($size[2]==1) { // if GIF it is not png
				@imagepng($catImage,$catPic);
			}
			if ($size[2]==2) { // if JPEG 
				@imagejpeg($catImage,$catPic);
			}
			if ($size[2]==3) { // if PNG 
				@imagepng($catImage,$catPic);
			}
		} else return ERR9;
	} else return ERR4;
	return $catImage;
}

function displayFlag($country, $flgPath = "")
{
	global $flags;
	if($country != "" && $flags[strtoupper($country)]!= "")
	{
		return  "<img src='" . $flgPath . "images/flags/". $flags[strtoupper($country)].".gif' alt='$country' border=0 align='absmiddle'>";
	}
}


	function updateAgentAccount($agentID, $amount, $transID, $type, $description, $actAs)
	{
		$today = date("Y-m-d");  
		$loginID  = $_SESSION["loggedUserData"]["userID"];
		
		$agentQuery = selectFrom("select isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
		$isCorresspondent = $agentQuery["isCorrespondent"];
		
		if($isCorresspondent != 'N')
		{
			$agentType = "Distributor";
			
			if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
			{
				$agentType = "Both";///AnD
			}	
		}elseif($isCorresspondent == 'N')
		{
			$agentType = "Agent";
		}
		
		if($agentType == 'Agent')
		{
			$accountQuery = "insert into agent_account 
			(agentID, dated, type, amount, modified_by, TransID, description) values
			('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description')";	
		}elseif($agentType == 'Distributor')
		{
			$accountQuery = "insert into bank_account
			(bankID, dated, type, amount, modified_by, TransID, description) values
			('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description')";
		}elseif($agentType == "Both")
		{
			$accountQuery = "insert into ".TBL_AnD_ACCOUNT." 
			(agentID, dated, type, amount, modified_by, TransID, description, actAs) values
			('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs')";
		}
		insertInto($accountQuery);
		agentSummaryAccount($agentID, $type, $amount); 
			
	}
	function agentSummaryAccount($agentID, $type, $amount)
	{
		 $today = date("Y-m-d"); 
			$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '$today'");
	
	
			if($agentContents["user_id"] != "")
			{
				
				$balance = $agentContents["closing_balance"];
				
				if($type == 'DEPOSIT')
				{
					$balance = $balance + $amount;	
				}else{
					$balance = $balance - $amount;	
				}
				
			 update("update ".TBL_ACCOUNT_SUMMARY." set closing_balance = '$balance' where id = '".$agentContents["id"]."'");			
		}else{
			$datesLast = selectFrom("select  Max(dated) as dated from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID'");
			 $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '".$datesLast["dated"]."'");
			
				
				$balance = 0;
				
				$balance += $agentContents["closing_balance"];
					
				
				
				$openingBalance = $balance;
				
				if($type == 'DEPOSIT')
				{
					$closingBalance = $balance + $amount;	
				}else{
					$closingBalance = $balance - $amount;	
				}
				
			 insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')");
			}	
			
				
	}

// Added by Niaz Ahmad at 14-04-2009 @4530	
//This function is used when the country is other than uk.	
function getCountryTime($countryCode)
{
	
	$row = selectFrom("select timeDiff from countryTime where countryCode = '$countryCode'");
	
	$timeDiff = $row[0];
	$temp = explode(":",$timeDiff);
	$year = date("Y");
	$mon = date("m");
	$day = date("d");
	$hour = date("H");
	$min = date("i");
	$sec = date("s");

		
	if ($temp[0].$temp[1] < 0)
	{
		$min = ($min - $temp[1]);
		if ($min < 0)
		{
			$min = 	(60 + ($min));
			$hour = ($hour - 1);
			if($hour < 0)
			{
				$hour = (24 + ($hour));
				$day = ($day - 1);
				if ($day == 0)
				{
					$mon = ($mon - 1);
					$day = (getTotalDays($mon,$year) + $day);
					if ($mon == 0)
					{
						$mon = (12 + $mon);	
						$year = ($year - 1);
					}
				}
			}
		}
		
		$hour = ($hour + ($temp[0]));
		if ($hour < 0)
		{
			$hour = (24 + ($hour));
			$day = ($day - 1);
			if ($day == 0)
			{
				$mon = ($mon - 1);
				$day = (getTotalDays($mon,$year) + $day);
				if ($mon == 0)
				{
					$mon = (12 + $mon);	
					$year = ($year - 1);
				}
			}
		}
	}
	
	else
	{
		$min = ($min + $temp[1]);
		if ($min >= 60)
		{
			$min = $min - 60;
			$hour = ($hour + 1);	
			if ($hour >= 24)
			{
				$hour = $hour - 24;
				$day = ($day + 1);
				if ($day > getTotalDays($mon,$year))
				{
					$day = "01";
					$mon = ($mon + 1);
					if ($mon > 12)
					{
						$mon = $mon - 12;
						$year = $year + 1;	
					}
				}
			}
		}
		
		$hour = ($hour + $temp[0]);
		if ($hour >= 24)
		{
			$hour = $hour - 24;
			$day = ($day + 1);
			if ($day > getTotalDays($mon,$year))
			{
				$day = "01";
				$mon = ($mon + 1);
				if ($mon > 12)
				{
					$mon = $mon - 12;
					$year = $year + 1;	
				}
			}
		}
	}

	if (strlen($mon) == 1)
	{
		$mon = "0" . $mon;	
	}
	if (strlen($day) == 1)
	{
		$day = "0" . $day;	
	}
	if (strlen($hour) == 1)
	{
		$hour = "0" . $hour;	
	}
	if (strlen($min) == 1)
	{
		$min = "0" . $min;	
	}
	
	$dateTime = $year . "-" . $mon . "-" . $day . " " . $hour . ":" . $min . ":" . $sec;
	
	return $dateTime;

}
	
// Added by Niaz Ahmad at 14-04-2009 @4530
function activities($login,$activity,$actionFor,$tableName,$descript)
{
		$activityQuery = "insert into ".TBL_LOGIN_ACTIVITIES." (login_history_id, activity, activity_time, action_for, table_name,description) values('$login','$activity','".getCountryTime(CONFIG_COUNTRY_CODE)."','$actionFor','$tableName','$descript')";
		insertInto($activityQuery);
		
}
	

function onlineCustomerAccountBalance($intCustomerId)
{
	/*$strRandomQuery = "SELECT 
							sum(ca1.amount) as WA, 
							sum(ca2.amount) as DA 
					   FROM 
							customer_account as ca1, 
							customer_account as ca2 
					   WHERE 
							ca1.customerID = '".$intCustomerId."' AND
							ca2.customerID = '".$intCustomerId."' AND
							ca1.Type = 'WITHDRAW' AND
							ca2.Type = 'DEPOSIT'
							";
	*/
	
	$strRandomQuery = "SELECT 
							sum(amount) as WA
					   FROM 
							customer_account
					   WHERE 
							customerID = '".$intCustomerId."' AND
							Type = 'WITHDRAW'";
	
	$nRandomResult = mysql_query($strRandomQuery) or die("Invalid query: " . mysql_error());
	$nRows = mysql_fetch_array($nRandomResult);
	
	$strRandomQuery = "SELECT 
							sum(amount) as DA
					   FROM 
							customer_account
					   WHERE 
							customerID = '".$intCustomerId."' AND
							Type = 'DEPOSIT'";
	
	$nRandomResult = mysql_query($strRandomQuery) or die("Invalid query: " . mysql_error());
	$nRowsDeposit = mysql_fetch_array($nRandomResult);
	
	return round($nRowsDeposit["DA"] - $nRows["WA"],2);
}

?>