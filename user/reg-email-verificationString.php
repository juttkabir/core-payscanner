<?
		// Polish Content
		$message = "
			Droga/i $Title $firtName $lastName
			<br /><br />
			Twoje konto ".COMPANY_NAME." zosta&#322;o aktywowane i mo&#380;esz teraz bez  przeszk&oacute;d korzysta&#263; z naszej us&#322;ugi transfer&oacute;w pieni&#281;&#380;nych.&nbsp; Prosz&#281; klikn&#261;&#263; na poni&#380;szy link, by uzyska&#263;  dost&#281;p do swojego konta ".COMPANY_NAME.".
			<br />
			<br />
			<a href='".$emailLogin."'>Login to your ".COMPANY_NAME." eWallet</a>
			Poprzez klikni&#281;cie na ten link zostaniesz automatycznie przeniesiony do  bezpiecznej strony ".COMPANY_NAME.", gdzie mo&#380;esz zalogowa&#263; si&#281; do swojego  konta
			<br />
			<br />
			Tw&oacute;j Login: ".$loginName."
			<br /><br />
			Tw&oacute;j  kod PIN: ".$PINCODE."<br />
			Tw&oacute;j  nr klienta: ".$customerCode."<br />
			<br />
			&nbsp;Kontaktuj&#261;c si&#281; z ".COMPANY_NAME." prosz&#281; u&#380;ywa&#263;  nr klienta ".$customerCode." w celu uzyskania jak najszybszej obs&#322;ugi.
			<br />
			<br />
			Uwaga:  Z powod&oacute;w bezpiecze&#324;stwa nie wysy&#322;amy hase&#322; w wiadomo&#347;ciach email, b&#261;d&#378; poczt&#261;.
			<br /><br />
			W  razie jakichkowliek pyta&#324; lub w&#261;tpliowo&#347;ci prosz&#281; kontaktowa&#263; sie z&nbsp; nami dzwoni&#261;c pod bezp&#322;atny numer 08082377777  lub wysy&#322;aj&#261;c email na adres <a href='".INQUIRY_EMAIL."'>".INQUIRY_EMAIL."</a> 
			<br />
			<br />
			Dzi&#281;kujemy za rejestracj&#281;,<br /><br />
			".COMPANY_NAME."
			<br /><br />
			<a href='".COMPANY_URL."'>".COMPANY_URL."</a><br />
			<hr />
		";
		
		// English Content
		$message .= "

  <Br> <Br>
     Dear $Title $firtName $lastName 
  
  <Br> <Br> <Br>
    
  
  
    Your ".COMPANY_NAME." account is activated and you can start using ".COMPANY_NAME."  <Br>
      Money Transfer service. Please click the link below to access your ".COMPANY_NAME."  <Br>
      account.
  
  <Br> <Br>
     <a href='".$emailLogin."'>Login to your ".COMPANY_NAME." eWallet </a>
  
  <Br>
    By clicking this link you will automatically be sent to the ".COMPANY_NAME." 
      Secure Site where you can login to your account.<Br>
  
  <Br> <Br>
    
  
  
    Your Login Name: ".$loginName."
    <Br> <Br>
	Your PIN code: ".$PINCODE."<br>
    Your Customer Number: ".$customerCode."
  <Br> <Br>
  
    Please use your Customer Number ".$customerCode." when you contact 
      ".COMPANY_NAME." customer service department to get quick response.
  
  
    <Br> <Br>
  
  
    Note: For security reason we do not send Password by email or by 
      post.
  
   <Br>
    <Br>
  
  
    If you have any question please contact us on Free Phone number 0808 2377777 
 or send email 
      to <a href='".INQUIRY_EMAIL."'>".INQUIRY_EMAIL."</a>
	  
  
  
    <Br> <Br>
  
  
    Thanks,
  <Br><Br>
  
    ".COMPANY_NAME." Team
  
  <Br> <Br>
    
      W: <a href='".COMPANY_URL."'>".COMPANY_URL."</a> 
	  <Br>	

	
						";				

?>