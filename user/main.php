<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$agentID = $_SESSION["loggedUserData"]["userID"];
//echo $agentType;

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../javascript/functions.js"></script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {
	color: #005b90;
	font-weight: bold;
}
.style4 {color: #005b90}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" bgcolor="#99c6e2"><span class="style3">Welcome To <? echo $company;?> Administration </span></td>
  </tr>
  <tr>
    <td>
	<table width="50%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Today's Tasks </legend>
    <br>
	<?
	switch ($agentType)
	{
		case "admin":
		case "Call":
		{
			$new = countRecords("select count(transID) as cnt from " . TBL_TRANSACTIONS . " where transStatus = 'Pending'");
			$Processing = countRecords("select count(transID) as cnt from " . TBL_TRANSACTIONS . " where transStatus = 'Processing'");
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="manage-transactions.php?action=verify"><font color="#005b90">New</font><font color="#005b90"> Transactions (<? echo $new?>)</font></a></td>
			  </tr>
<?
			  if($agentType != "Call")
{
?>
			<tr>
			    <td bgcolor="#DFE6EA"><a href="manage-transactions.php?action=authorize"><font color="#005b90">Pending For Authorisation (<? echo $Processing?>)</font></a></td>
		      </tr>
<?
}
?>
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="add-transaction.php?create=Y" target="mainFrame"><font color="#005b90">Create New Transaction</font></a></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="q-search.php" target="mainFrame"><font color="#005b90">Quick Search</font></a></td>
		      </tr>

			  <tr>
			    <td bgcolor="#DFE6EA"><a href="trans-status.php" target="mainFrame"><font color="#005b90">Transaction Status</font></a></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="daily-trans.php" target="mainFrame"><font color="#005b90">Daily Transaction</font></a></td>
		      </tr>
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><span class="child"><nobr><a HREF="view-complaint.php" target="mainFrame" class="tblItem style4">View All Complaints</a></nobr></span></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><span class="child"><a HREF="add-complaint.php" target="mainFrame" class="tblItem style4">Report a Complaint</a></span></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><span class="child"><a HREF="frm-fee-calculator.php" target="mainFrame" class="tblItem style4"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo (" Fee");}?> Calculator</a></span></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><span class="child"><a HREF="city-locator.php" target="mainFrame" class="tblItem style4">Agent Locator</a></span></td>
		      </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><span class="child"><a HREF="import-excel-files.php" target="mainFrame" class="tblItem style4">Excel Filse</a></span></td>
		      </tr>
			</table>
		<?
			break;
		}
		case "SUPAI": // AGENT + IDA
		{
			$query = "select count(transID) from ". TBL_TRANSACTIONS . " where (transStatus='Authorize') and (benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR benAgentID = '$agentID') ";
			//$query = "select count(transID) as cnt  from ". TBL_TRANSACTIONS . " as t where toCountry = '$agentCountry' and t.transStatus='Authorize'";
			$newTransforDel = countRecords($query);
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="release-trans.php"><font color="#005b90">New Transactions for Delivery (<? echo $newTransforDel?>)</font></a></td>
			  </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="add-transaction.php?create=Y" target="mainFrame"><font color="#005b90">Create New Transaction</font></a></td>
		      </tr>
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>

			</table>
		<?
			break;
		}
		case "SUPA":
		{?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="add-transaction.php?create=Y" target="mainFrame"><font color="#005b90">Create New Transaction</font></a></td>
		      </tr>
			 
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>
		 
			</table>
		<?
			break;
		}
		case "SUPI": // SUPER IDA
		{
			$query = "select count(transID) from ". TBL_TRANSACTIONS . " where (transStatus='Authorize') and (benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR benAgentID = '$agentID') ";
			//$query = "select count(transID) as cnt  from ". TBL_TRANSACTIONS . " as t where toCountry = '$agentCountry' and t.transStatus='Authorize'";
			$newTransforDel = countRecords($query);
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="release-trans.php"><font color="#005b90">New Transactions for Delivery (<? echo $newTransforDel?>)</font></a></td>
			  </tr>
			  <!--
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>
			  -->
			</table>
		<?
			break;
		}
		case "SUBAI": // Sub agent + IDA
		{
			$query = "select count(transID) as cnt  from ". TBL_TRANSACTIONS . " as t where  benAgentID = '$agentID' and t.transStatus='Authorize'";
			$newTransforDel = countRecords($query);
		
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="release-trans.php"><font color="#005b90">New Transactions for Delivery (<? echo $newTransforDel?>)</font></a></td>
			  </tr>
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="add-transaction.php?create=Y" target="mainFrame"><font color="#005b90">Create New Transaction</font></a></td>
		      </tr>
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>
			</table>
		<?
			break;
		}
		case "SUBA": //SUB AGENT
		{?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
			    <td bgcolor="#DFE6EA"><a href="add-transaction.php?create=Y" target="mainFrame"><font color="#005b90">Create New Transaction</font></a></td>
		      </tr>
			  <tr>
				<td bgcolor="#DFE6EA"><font color="#005b90">Today's Exchange Rates</font></td>
			  </tr>

			  <tr>
				<td bgcolor="#DFE6EA">&nbsp;</td>
			  </tr>
			</table>
		<?
			break;
		}
		case "SUBI": // SUB IDA
		{
			$query = "select count(transID) as cnt  from ". TBL_TRANSACTIONS . " as t where  benAgentID = '$agentID' and t.transStatus='Authorize'";
			$newTransforDel = countRecords($query);
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="release-trans.php"><font color="#005b90">New Transactions for Delivery (<? echo $newTransforDel?>)</font></a></td>
			  </tr>
			  <!--
			  
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>
			  -->
			</table>
		<?
			break;
		}
		default:
		{
		?>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
				<td bgcolor="#DFE6EA"><a href="exchange-rates-agent.php"><font color="#005b90">Today's Exchange Rates</font></a></td>
			  </tr>

			</table>
		<?
		}
	}
	?>
<br>
    </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>
