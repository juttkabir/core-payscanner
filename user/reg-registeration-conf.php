<?
// Session Handling
session_start();
require_once ("../include/config.php");
dbConnect();
$userIP = getIP();
$nId = $_POST["nId"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;
$date_time = date('Y-m-d');

	if($_POST["dMonth"] == '')
	{
		$backURL = 'registration.php?msg=Y';
		$_SESSION['error'] = "Please Select the month from the list";
		redirect($backURL);
	}
	if($_POST["dDay"] == '')
	{
		$backURL = 'registration.php?msg=Y';
		$_SESSION['error'] = "Please Select the day from the list";
		redirect($backURL);
	}
	if($_POST["dYear"] == '')
	{
		$backURL = 'registration.php?msg=Y';
		$_SESSION['error'] = "Please Select the year from the list";
		redirect($backURL);
	}		
	
$dDate = $_POST["dYear"]."-".$_POST["dMonth"]."-".$_POST["dDay"];


switch(trim($_POST["country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
case "United Kingdom": $ccode = "GB"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}

session_register("Title");
session_register("firtName");
session_register("middleName");
session_register("lastName");
session_register("loginName");
session_register("password");
session_register("dMonth");
session_register("dDay");
session_register("dYear");
session_register("email");
session_register("phone");
session_register("mobile");
session_register("address");
session_register("address1");
session_register("country");
session_register("state");
session_register("city");
session_register("zip");
session_register("question1");
session_register("answer1");
session_register("question2");
session_register("answer2");
session_register("uploadImage");
$_SESSION["DocumentProvided"] = (is_array($_POST["DocumentProvided"]) ? implode(",",$_POST["DocumentProvided"]) : "");
session_register("countryToSendMoney");

$Title = $_SESSION["Title"] = $_POST["Title"];
$firtName = $_SESSION["firtName"] = $_POST["firtName"];
$middleName = $_SESSION["middleName"] = $_POST["middleName"];
$lastName = $_SESSION["lastName"] = $_POST["lastName"];
$loginName = $_SESSION["loginName"] = $_POST["loginName"];
$password = $_SESSION["password"] = $_POST["password"];
$dMonth = $_SESSION["dMonth"] = $_POST["dMonth"];
$dDay = $_SESSION["dDay"] = $_POST["dDay"];
$dYear = $_SESSION["dYear"] = $_POST["dYear"];
$placeOfBirth = $_SESSION["placeOfBirth"] = $_POST["placeOfBirth"];
$email = $_SESSION["email"] = $_POST["email"];
$phone = $_SESSION["phone"] = $_POST["phone"];
$mobile = $_SESSION["mobile"] = $_POST["mobile"];
$address = $_SESSION["address"] = $_POST["address"];
$address1 = $_SESSION["address1"] = $_POST["address1"];
$country = $_SESSION["country"] = $_POST["country"];
$state = $_SESSION["state"] = $_POST["state"];
$city = $_SESSION["city"] = $_POST["city"];
$zip = $_SESSION["zip"] = $_POST["zip"];
$question1 = $_SESSION["question1"] = $_POST["question1"];
$answer1 = $_SESSION["answer1"] = $_POST["answer1"];
$question2 = $_SESSION["question2"] = $_POST["question2"];
$answer2 = $_SESSION["answer2"] = $_POST["answer2"];

$_SESSION["va"] = $_REQUEST["va"];

if($_REQUEST["uploadImage"] !=''){
$_SESSION["uploadImage"] = $_REQUEST["uploadImage"];
$uploadImage = $_SESSION["uploadImage"];
}

if($_REQUEST["mode"] !=''){
$_SESSION["mode"] = $_REQUEST["mode"];
$mode = $_SESSION["mode"];
}

//debug($_SESSION["va"]);

// This $_SESSION["DocumentProvided"] and $_POST["DocumentProvided"] as mixed and confused with the source of information at the front ent
$DocumentProvided = $_SESSION["DocumentProvided"] = (is_array($_POST["DocumentProvided"]) ? implode(",",$_POST["DocumentProvided"]) : "");
$countryToSendMoney = $_SESSION["countryToSendMoney"] = $_POST["countryToSendMoney"];

// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
{
	$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
}

// End of code against #3299: MasterPayex - Multiple ID Types

//if($nId == "")
//{
	$PINCODE = rand(10000, 99999);
	$strRandomQuery = "SELECT * FROM cm_customer where SecuriyCode = $PINCODE";
	$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
	$nRows = mysql_fetch_array($nRandomResult);
	$oldPINcode = $nRows["SecuriyCode"];
	
	if($oldPINcode)
	{		
		$strRandomQuery = "SELECT * FROM cm_customer";
		$nRandomResult = mysql_query($strRandomQuery)or die("Invalid query: " . mysql_error()); 
		while($nRows = mysql_fetch_array($nRandomResult))
		{		
			$PINCODE = rand(10000, 99999);
			$strRandomQuery2 = "SELECT * FROM cm_customer where SecuriyCode = $PINCODE";
			$nRandomResult2 = mysql_query($strRandomQuery2)or die("Invalid query: " . mysql_error()); 
			$nRows2 = mysql_fetch_array($nRandomResult2);
			$oldPINcode = $nRows2["SecuriyCode"];		
			if($oldPINcode == '')
				{
					break;
				}
		}
	}
	
	if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$loginName."'")) {
		$backURL = 'registration.php?msg=Y';
		insertError(CUS1);
		redirect($backURL);
	}
	if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$loginName."'")) {
		$backURL = 'registration.php?msg=Y';
		insertError(CUS1);
		redirect($backURL);
	}
	if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$loginName."'")) {
		$backURL = 'registration.php?msg=Y';
		insertError(CUS1);
		redirect($backURL);
	}
	if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$loginName."'")) {
		$backURL = 'registration.php?msg=Y';
		insertError(CUS1);
		redirect($backURL);
	}
	
	$bolIsAuth = true;
	/* Get virtual agent data */
	if(!empty($_REQUEST["va"]))
	{
		$strVirtualAgentSql = "select agentStatus, isOnline from admin where userID='".$_REQUEST["va"]."'";
		$arrVirtualAgentData = selectFrom($strVirtualAgentSql);
		if($arrVirtualAgentData["agentStatus"] != "Disabled" && $arrVirtualAgentData["isOnline"] == "Y")
			$bolIsAuth = false;
		else
			$bolIsAuth = true;
	}
	else
		$bolIsAuth = false;
		
	
	if($bolIsAuth)	
	{
		$backURL = 'registration.php?msg=Y&va='.$_REQUEST["va"];
		insertError("Your virtual agent is disable or does not have proper rights.");
		redirect($backURL);
	}

		if(CONFIG_MANUAL_ENABLE_ONLINE == '1')
		{
			$isActive = 'Disable';
		}else{
			$isActive = 'Active';
			}
			
	$customerNumber = selectFrom("select MAX(c_id) from ".TBL_CM_CUSTOMER." where c_country='".$_POST["country"]."'");
	//debug("select MAX(c_id) from ".TBL_CM_CUSTOMER." where c_country='".$_POST["country"]."'");
	$customerCode = $preFix."".$ccode.($customerNumber[0]+1);
	if($_SESSION["va"]==""){
	$strQuery = "insert into cm_customer( 
										 c_name , Title , FirstName , MiddleName ,
										 LastName , c_pass , c_date , c_email , c_phone , c_mobile , 
										 c_address , c_address2 , c_country , c_state , c_city , c_zip , 
										 c_p_question1 , c_answer1 , c_p_question2 , c_answer2 , c_info_source ,
										 c_benef_country , customerStatus ,  username , last_login , 
										 dateConfrimed , SecuriyCode , 
										 dateAccepted , dob ,placeOfBirth ,  limit1,accessFromIP, documentProvided 
										) values(
										'$customerCode','$Title','$firtName','$middleName','$lastName','$password',
										'$date_time','$email','$phone','$mobile','$address','$address1','$country',
										'$state','$city','$zip','$question1','$answer1','$question2','$answer2',
										'$DocumentProvided','$countryToSendMoney','".$isActive."','$loginName' ,'NOW()' ,
										'NOW()' , $PINCODE ,   'NOW()' , '$dDate' , '$placeOfBirth' ,'2000','$userIP', 'NO')";	
	}
	else{
	$strQuery = "insert into cm_customer( 
										 c_name , Title , FirstName , MiddleName ,
										 LastName , c_pass , c_date , c_email , c_phone , c_mobile , 
										 c_address , c_address2 , c_country , c_state , c_city , c_zip , 
										 c_p_question1 , c_answer1 , c_p_question2 , c_answer2 , c_info_source ,
										 c_benef_country , customerStatus ,  username , last_login , 
										 dateConfrimed , SecuriyCode , 
										 dateAccepted , dob ,placeOfBirth ,  limit1,accessFromIP, documentProvided , agentId
										) values(
										'$customerCode','$Title','$firtName','$middleName','$lastName','$password',
										'$date_time','$email','$phone','$mobile','$address','$address1','$country',
										'$state','$city','$zip','$question1','$answer1','$question2','$answer2',
										'$DocumentProvided','$countryToSendMoney','".$isActive."','$loginName' ,'NOW()' ,
										'NOW()' , $PINCODE ,   'NOW()' , '$dDate' , '$placeOfBirth' ,'2000','$userIP', 'NO', ".$_SESSION["va"]."
										)";	
	}
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
	//insertInto($strQuery);	
	
	$lastCustomerId = @mysql_insert_id();
	$customerID = $lastCustomerId;
	//debug($strQuery,true);
	//debug($lastCustomerId, true);
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				// If id is empty, its an insert request.
				// Insert new reocrd in this case.

				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				$insertQuery = "insert into cm_user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date 
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $lastCustomerId . "', 
											'C', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "', 
											'" . $expiryDate . "'
										)";
				insertInto( $insertQuery );
			}
		}
	}
	
	// End of code against #3299: MasterPayex - Multiple ID Types
	
	
	
	
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				$subject = "Welcome to $company Transfer Service";
				$securityAddress = md5("test");
				$securityPassword = md5($password);
				
				if(CONFIG_MANUAL_ENABLE_ONLINE == '1')
				{
					$emailLogin = USER_ACCOUNT_URL."/agent-login.php?username=$loginName&password=$securityPassword&sec=$securityAddress";
						require_once ("reg-email-verificationString.php");
				}else{
					$emailLogin = USER_ACCOUNT_URL;
						require_once ("reg-uk-email.php");
					}
				
				//if($country == "Canada")
				//	require_once ("reg-canada-email.php");
				//else
				
								
				sendMail($email, $subject, $message, $fromName, $fromEmail);
					
	$_SESSION["loginName"] = '';
	$_SESSION["password"] = '';
	$_SESSION["dMonth"] = '';
	$_SESSION["dDay"] = '';
	$_SESSION["dYear"] = '';
	$_SESSION["placeOfBirth"] = '';
	$_SESSION["email"] = '';
	$_SESSION["phone"] = '';
	$_SESSION["city"] = '';
	$_SESSION["mobile"] = '';
	$_SESSION["address"] = '';
	$_SESSION["email"] = '';
	$_SESSION["address1"] = '';
	$_SESSION["country"] = '';
	$_SESSION["state"] = '';
	$_SESSION["zip"] = '';
	$_SESSION["question1"] = '';
	$_SESSION["answer1"] = '';
	$_SESSION["question2"] = '';
	$_SESSION["answer2"] = '';
	$_SESSION["DocumentProvided"] = '';
	$_SESSION["countryToSendMoney"] ='';
	$_SESSION["Title"] = '';
	$_SESSION["firtName"] = '';
	$_SESSION["middleName"] = '';
	$_SESSION["lastName"] = '';	
	$_SESSION["va"] = '';	

	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] );


//} 
/*else
{
		  $strQuery = "update cm_customer set
		  										Title = '$Title',
												FirstName = '$firtName',
												MiddleName = '$middleName',
												LastName = '$lastName',		  
												c_name  ='$loginName',											
												c_date  = 'NOW()',
												c_email = '$email',
												c_phone  = '$phone',
												c_mobile = '$mobile',
												c_address = '$address',
												c_address2 = '$address1',
												c_country = '$country',
												c_state = '$state',
												c_city = '$city',
												c_zip  = '$zip',
												c_p_question1 = '$question1',
												c_answer1  = '$answer1',
												c_p_question2 = '$question2',
												c_answer2  = '$answer2',
												c_info_source = '$DocumentProvided',
												c_benef_country  = '$countryToSendMoney',
												username = 	'$loginName',	
												dob = '$dDate'									
											where c_id = $nId
											";
$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 											
$_SESSION["nId"] = '';


	$_SESSION["loginName"] = '';
	$_SESSION["password"] = '';
	$_SESSION["dMonth"] = '';
	$_SESSION["dDay"] = '';
	$_SESSION["dYear"] = '';
	$_SESSION["email"] = '';
	$_SESSION["city"] = '';
	$_SESSION["phone"] = '';
	$_SESSION["mobile"] = '';
	$_SESSION["address"] = '';
	$_SESSION["email"] = '';
	$_SESSION["address1"] = '';
	$_SESSION["country"] = '';
	$_SESSION["state"] = '';
	$_SESSION["zip"] = '';
	$_SESSION["question1"] = '';
	$_SESSION["answer1"] = '';
	$_SESSION["question2"] = '';
	$_SESSION["answer2"] = '';
	$_SESSION["DocumentProvided"] = '';
	$_SESSION["countryToSendMoney"] ='';
	$_SESSION["Title"] = '';
	$_SESSION["firtName"] = '';
	$_SESSION["middleName"] = '';
	$_SESSION["lastName"] = '';

//echo $strQuery = "SELECT distinct city    FROM  cities where country='$_SESSION['country']' order by city";
if($_POST["country"] != '' )
	$_SESSION["country"] = $_POST["country"];

$backURL = "home.php";

}*/

if($uploadImage == "Y"){		
		$backURL = "uploadOnlineDocuments.php?mode=".$mode;
		if(!empty($customerID)){
			$backURL .= "&customerID=".$customerID;
		}else{
			$backURL .= "&customerID=".$_REQUEST["customerID"];
		}
	
	}else{
	
	$backURL = "index.php?redirect=Y";
		}

//$backURL .= "&success=Y";
//redirect($backURL);
header("Location: $backURL");

?>