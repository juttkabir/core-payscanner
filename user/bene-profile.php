<?
session_start();

include "header.php";

$date_time = date('d-m-Y  h:i:s A');
session_register("Title");
session_register("firstName");
session_register("lastName");
session_register("middleName");
session_register("IDType");
session_register("IDNumber");
session_register("Address");
session_register("Address1");
session_register("Country");
session_register("City");
session_register("Zip");
session_register("State");
session_register("Phone");
session_register("Mobile");
session_register("bentEmail");
session_register("bankName");
session_register("branchCode");
session_register("branchAddress");
session_register("swiftCode");
session_register("accNo");
session_register("ABACPF");
session_register("IBAN");

$benIDs = $_GET["benIDs"];
{
    
    unset( $_SESSION["agentID2"] );
    unset( $_SESSION["FullName"] );
    unset( $_SESSION["IDType"] );
    unset( $_SESSION["IDNumber"] );
    unset( $_SESSION["Address"] );
    unset( $_SESSION["Address1"] );
    unset( $_SESSION["Country"] );
    unset( $_SESSION["City"] );
    unset( $_SESSION["Zip"] );
    unset( $_SESSION["State"] );
    unset( $_SESSION["Phone"] );
    unset( $_SESSION["Mobile"] );
    unset( $_SESSION["Email"] );
    unset( $_SESSION["Country"] );
    unset( $_SESSION["bankName"] );
    unset( $_SESSION["branchCode"] );
    unset( $_SESSION["branchAddress"] );
    unset( $_SESSION["swiftCode"] );
    unset( $_SESSION["accNo"] );
    unset( $_SESSION["ABACPF"] );
    unset( $_SESSION["IBAN"] );
    
    $strQuery = "SELECT * FROM cm_beneficiary where benID = $benIDs";
    $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());         
    $rstRow = mysql_fetch_array($nResult);                

    $_SESSION["agentID2"] = $rstRow["agentID"];
    $_SESSION["FullName"] = $rstRow["Title"]." ".$rstRow["firstName"]." ".$rstRow["middleName"]." ".$rstRow["lastName"];
    $_SESSION["IDType"] = $rstRow["IDType"];
    $_SESSION["IDNumber"] = $rstRow["NICNumber"];
    $_SESSION["Address"] = $rstRow["Address"];
    $_SESSION["Address1"] = $rstRow["Address1"];
    $_SESSION["Country"] = $rstRow["Country"];
    $_SESSION["City"] = $rstRow["City"];
    $_SESSION["Zip"] = $rstRow["Zip"];
    $_SESSION["State"] = $rstRow["State"];
    $_SESSION["Phone"] = $rstRow["Phone"];
    $_SESSION["Mobile"] = $rstRow["Mobile"];
    $_SESSION["Email"] = $rstRow["email"];

    $_SESSION["Country"] = ucfirst(strtolower($_SESSION["Country"]));

    $strQuery = "SELECT * FROM cm_bankdetails where benID = $benIDs";
    $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());         
    $rstRow = mysql_fetch_array($nResult);                
    
    $_SESSION["bankName"] = $rstRow["bankName"];
    $_SESSION["branchCode"] = $rstRow["branchCode"];
    $_SESSION["branchAddress"] = $rstRow["branchAddress"];
    $_SESSION["swiftCode"] = $rstRow["swiftCode"];
    $_SESSION["accNo"] = $rstRow["accNo"];
    $_SESSION["ABACPF"] = $rstRow["ABACPF"];
    $_SESSION["IBAN"] = $rstRow["IBAN"];
}
?>
    <table width="100%" border="0" cellspacing="1" cellpadding="5">
      <tr>
        <td align="center" valign="top">
          <table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
            <tr height="30">
              <td bgcolor="#6699cc" align="left" width="300"> 
                &nbsp;
                <strong><font color="#FFFFFF" size="2"><? echo $msg;?>&nbsp;</font></strong>              </td>
              <td bgcolor="#6699cc" align="right">
                <a href="./add-beneficiary.php?benIDs=<? echo $benIDs;?>">
                  <strong><font color="#FFFFFF" size="2">Edit Beneficiary Information&nbsp;&nbsp;</font></strong>                </a>              </td>
            </tr>

            <tr>
              <td bgcolor="#6699cc" colspan="2">
                <strong><font color="#FFFFFF" size="2" ><? echo $msg;?>&nbsp;</font></strong>              </td> 
            </tr>

            <tr>
              <td colspan="2" bgcolor="#000000">
                <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                  <tr>
                    <td align="right" bgcolor="#DFE6EA">
                      <font color="#000066" size="2"><strong>&nbsp;</strong></font>                    </td>
                  </tr>
                </table>              </td>
            </tr>

<? if ($_GET["msg"] != "")
    {
?>
            <tr bgcolor="#ededed">
              <td colspan="2" bgcolor="#FFFFCC">
                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                  <tr>
                    <td width="40" align="center">
                      <font size="5" color="#990000"><b><i>!</i></b></font>                    </td>
                    <td>
                      <font color='$990000'><b><? echo $_SESSION['error']; ?></b><br/><br/></font>                    </td>
                  </tr>
                </table>              </td>
            </tr>
<?
    }
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong>Name</strong></font></td>
              <td width="344" align="left">
                <?  echo $_SESSION["FullName"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong>Email</strong></font>              </td>
              <td align="left">
                <? echo $_SESSION["Email"];?>              </td>
            </tr>

<?
if ( defined("CONFIG_PHONES_AS_DROPDOWN") && CONFIG_PHONES_AS_DROPDOWN == 1 )
    {
    $phoneNumber = (!empty( $contentsBen[$i]["Phone"] ) ? $contentsBen[$i]["Phone"] : $contentsBen[$i]["Mobile"]);
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90">
                  <strong>Phone Number</strong>                </font>              </td>
              <td align="left">
                <?=!empty($_SESSION["Phone"]) ? $_SESSION["Phone"] : $_SESSION["Mobile"];?>              </td>
            </tr>
<?
    }
else
    {
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong>Home Phone Number</strong></font>              </td>
              <td align="left"><? echo $_SESSION["Phone"];?></td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong>Mobile Number</strong></font>              </td>
              <td align="left"><?  echo $_SESSION["Mobile"];?></td>
            </tr>
<?
    }
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong>Address</strong></font>              </td>
              <td align="left"><?  echo $_SESSION["Address"];?></td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">&nbsp;</td>
              <td align="left"><?  echo $_SESSION["Address1"];?></td>
            </tr>
            <tr bgcolor="#ededed">
              <td align="left">
                <font color="#005b90"><b>Country</b></font>              </td>
              <td align="left">
                <? echo $_SESSION["Country"];?>              </td>
            </tr>  
  
<?
//if ($_SESSION["Country"] == "United States")
  //  { 
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong>State</strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["State"];?>              </td>
            </tr>
<?
    //}
if ( $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom")
    {
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><b>City</b></font>              </td>
              <td align="left">
                <?  echo $_SESSION["City"];?>              </td>
            </tr> 
<?
    }
else
    {
?>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><b>City:</b></font>              </td>
              <td align="left">
                <?  echo $_SESSION["City"];?>              </td>
            </tr> 
<?
    }
?>
	<tr bgcolor="#ededed">
    	<td align="left" valign="top"><font color="#005b90"><b>Zip/Postal Code</b></font> </td>
    	<td align="left" valign="top"><?=$_SESSION["Zip"];?> </td>
	</tr>
<?
if ( CONFIG_PROVE_ADDRESS == "1" )
    {
?>
            <tr bgcolor="#ededed">
              <td align="left" valign="top">
                <font color="#005b90"><b>Prove of Address</b></font>              </td>
              <td align="left" valign="top">
                <?=($rstRow["proveAddress"] == 'Y' ? "Yes" : "No");?>              </td>
            </tr>
<?
    if (CONFIG_PROVE_ADDRESS_TYPE == "1" && !empty($rstRow["proveAddressType"]))
        {
        $types = explode(",", CONFIG_PROVE_ADDRESS_TYPE_VALUES);
?>
            <tr bgcolor="#ededed">
              <td>
                <font color="#005b90"><strong>Prove Address Type</strong></font>              </td>
              <td>
                <?=$rstRow["proveAddressType"];?>              </td>
            </tr>
<?
        }
    }
?>

            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong>ID Number</strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["IDNumber"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong>ID Type </strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["IDType"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong> IBAN Number</strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["IBAN"];?>              </td>
            </tr>
<?
if (trim($_SESSION["bankName"]) != "")
    {
?>
            <tr bgcolor="#ededed">
              <td colspan="2" align="center">&nbsp;</td>
            </tr> 
        
            <tr bgcolor="#ededed">
              <td colspan="2" align="left">
                <font color="#005b90"><strong>Bank Information </strong></font>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td align="left">
                <font color="#005b90"><strong>Bank Name </strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["bankName"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left" valign="top">
                <font color="#005b90"><strong> Account Number</strong></font>              </td>
              <td align="left">
                <p><?  echo $_SESSION["accNo"];?></p>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong> Branch Code </strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["branchCode"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong> Branch Address </strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["branchAddress"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong> <? if($_SESSION["Country"] == "United states") echo("ABA Number"); elseif($_SESSION["Country"] == "Brazil") echo("CPF Code");?>&nbsp;</strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["ABACPF"];?>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong>Swift Code </strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["swiftCode"];?>              </td>
            </tr>
<? if(CONFIG_IBAN_OR_BANK_TRANSFER != "1"){ ?>
            <tr bgcolor="#ededed">
              <td width="300" align="left">
                <font color="#005b90"><strong> IBAN Number</strong></font>              </td>
              <td align="left">
                <?  echo $_SESSION["IBAN"];?>              </td>
            </tr>
<? }?>
			
<?			
    }
?>


	<?
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY")
		&& CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1 )
	{
		// Fetch available ID Types
		$query = "select * from id_types
					where active='Y'
					and show_on_ben_page='Y'";
		
		$IDTypes = selectMultiRecords( $query );
		if ( !empty( $benIDs ) )
		{
			// Now fetch values against available id types.
			$numberOfIDTypes = count( $IDTypes );
			for( $i = 0; $i < $numberOfIDTypes; $i++ )
			{
				$IDTypeValuesQ = "select * from cm_user_id_types
										where
											id_type_id = '" . $IDTypes[$i]["id"] . "'
										and	user_id = '" . $benIDs . "'
										and user_type = 'B'";
				$IDTypeValues = selectFrom( $IDTypeValuesQ );
				
				if ( !empty( $IDTypeValues ) )
				{
					$IDTypes[$i]["values"] = $IDTypeValues;
				}
			}
		}

		
		
		if ( !empty( $IDTypes ) )
		{
		?>
			<tr bgcolor="#ededed">
				<td colspan="4">
					<table width="100%">
						<tr>
						<?
							$tdNumber = 1;
							$numberOfIDTypes = count( $IDTypes );

							$chkIDTypesJS = "";

							$serverDateArray["year"] = date("Y");
							$serverDateArray["month"] = date("m");
							$serverDateArray["day"] = date("d");

							for( $i = 0; $i < $numberOfIDTypes; $i++ )
							{
								$idNumber = "";
								$idTypeValuesSESS = "";
								$issuedBy = "";

								if( !empty( $_SESSION["IDTypesValuesData"] )
									&& !empty( $_SESSION["IDTypesValuesData"][$IDTypes[$i]["id"]] ) )
								{
									$idTypeValuesSESS = $_SESSION["IDTypesValuesData"];
									$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
									$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
								}
								else if (!empty($IDTypes[$i]["values"]))
								{
									$idNumber = $IDTypes[$i]["values"]["id_number"];
									$issuedBy = $IDTypes[$i]["values"]["issued_by"];
								}

								?>
									<td style="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:0px solid black;" : "");?> ">
										<table border="1" cellpadding="2" cellspacing="0" align="center">
										  <tr>
											<td colspan="2" style="border:2px solid black; font-weight:bold;"><?=$IDTypes[$i]["title"];?> <?=($IDTypes[$i]["mandatory"] == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?></td>
										  </tr>
										  <tr>
											<td width="80">ID Number:</td>
											<td>
												<?=$idNumber;?>
											</td>
										  </tr>
										  <tr>
											<td>Issued by:</td>
											<td><?=$issuedBy?></td>
										  </tr>
										  <tr>
											<td>Issue Date:</td>
											<td>
												<?
													$startYear = 1985;
													$currentYear = $serverDateArray["year"];

													$issueDay = 0;
													$issueMonth = 0;
													$issueYear = 0;

													if ( !empty($idTypeValuesSESS)
														&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
													{
														$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
														$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
														$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;
													}
													else if ( !empty($IDTypes[$i]["values"]) )
													{
														$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
														$issueDay = $dateArray[2] - 1;
														$issueMonth = $dateArray[1] - 1;
														$issueYear = $dateArray[0] - $startYear;
													}
												?>
												<select id="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
												</select>
												
												<select id="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
												</select>
												
												<select id="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
												</select>

												<script type="text/javascript">
													initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>', 'issue_date_year_<?=$IDTypes[$i]["id"];?>', 
																	'', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', <?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
												</script>
											</td>
										  </tr>
										  <tr>
											<td>Expiry Date:</td>
											<td>
												<?
													$expiryStartYear = $currentYear;
													$expiryDay = $serverDateArray["day"]; // Didn't do "less 1" here because atleast one day further from current has to be selected.
													$expiryMonth = $serverDateArray["month"] - 1;
													$expiryYear = $serverDateArray["year"] - $expiryStartYear;

													if ( !empty($idTypeValuesSESS)
														 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
													{
														$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
														$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
														$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;
													}
													else if ( !empty($IDTypes[$i]["values"]) )
													{
														$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );

														$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );

														$expiryDay = $dateArray[2] - 1;
														$expiryMonth = $dateArray[1] - 1;
														$expiryYear = $dateArray[0] - $expiryStartYear;
													}
												?>
												<select id="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
												</select>

												<select id="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
												</select>

												<select id="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
														name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
												</select>

												<script type="text/javascript">
													initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>', 'expiry_date_year_<?=$IDTypes[$i]["id"];?>', 
																	'', '1:12', '<?=$expiryStartYear;?>:2035', <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
												</script>
											</td>
										  </tr>
										</table>
									</td>
								<?
									if ( $tdNumber == 1 )
									{
										// Starting a new TR when two TDs are output.
										?>
											</tr><tr>
										<?
										$tdNumber = 1;
									}
									else
										$tdNumber++;
									

									$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
															id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

									if ( $IDTypes[$i]["mandatory"] == 'Y' )
									{
										$chkIDTypesJS .= "
															if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																id_number_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
															if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
														";
									}
									else
									{
										$chkIDTypesJS .= "
															if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																id_number_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
															if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
														";
									}

									$chkIDTypesJS .= "
														issueDate = new Date();
														todayDate = new Date( issueDate );
														expiryDate = new Date();

														todayDate.setFullYear( " . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " . $serverDateArray["day"] . " );
														issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
														expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );

														if ( issueDate >= todayDate )
														{
															alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
															document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
														
														if ( expiryDate <= todayDate )
														{
															alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
															document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
													";
							}
						?>
						</tr>
					</table>
					<script type="text/javascript">
						function checkIdTypes( theForm )
						{
							<?
								if ( !empty($chkIDTypesJS) )
									echo $chkIDTypesJS;
							?>
							return true;
						}
					</script>
				</td>
			</tr>
		<?
		}
	} // End of code against #3299: MasterPayex - Multiple ID Types
?>







            <tr bgcolor="#ededed">
              <td colspan="2" align="center">&nbsp;</td>
            </tr>  
          </table>
        </td>
        <td align='right' valign='top'>
<?
include "exchange-rates-agent.php";
?>
        </td>
      </tr>
    </table>

<?
include "footer.php";
?>
