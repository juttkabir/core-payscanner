<?
/* Compliance Controller File*/
define("DEBUG_ON",true);
//debugCompliance($_REQUEST);

function payexCompliance($arrguments)
{
	//debugCompliance($arrguments);
	$code = '';
	$firstName  = $arrguments["firstName"];
	$middleName = $arrguments["middleName"];
	$lastName   = $arrguments["lastName"];
	$match   = $arrguments["match"];
	$listName   = $arrguments["listName"];
	
	//debugCompliance($_REQUEST,true);
	$returnCode = validateInput($firstName,$middleName,$lastName);
	
	if($returnCode == 'EC01')
	{
	 echo $returnCode;
	die();
	}
	if($returnCode == 'SC01')
	{
		$_inputData = array(
							"firstName" =>$firstName,
							"middleName" => $middleName,
							"lastName"=>$lastName,
							"match"=>$match,
							"listName"=>$listName
							);
		/* Call Method*/		
		//debugCompliance($_inputData,true);
		// Call Method Exact Match 
		$response = userMatch($_inputData);
		//debugCompliance($response);
		$formattedResponse = arrFormat($response);
		//debugCompliance($formattedResponse);	
		
		/* End Method Calling*/			
	}
	

   $getXml = array_to_xml($formattedResponse);
   //echo htmlentities($getXml);
  // debugCompliance($getXml);
   
return $response;
}
$_arrErrorCode = array(
						"EC01" => "Mandatory Paramter is Missing",
						"EC02"  => "USER AUTHENTICATION ERROR",
						"EC03"  => "LIST NOT FOUND",
						"EC04"  => "INVALID MATCH TYPE"
);

function validateInput($firstName,$middleName,$lastName)
{
		/*
			EC01 = Input is not valid or require parameter is missing
			SC01 = Input is  Valid
		*/
		$code = '';
			
		if(empty($firstName) && empty($middleName) && empty($lastName))
			$code = 'EC01';
		else
			$code = 'SC01';
	
		return $code;
}












$strLastTag = '';

function array_to_xml($array, $level=1) {
	/*echo "<pre>";
	debug_print_backtrace();
	echo "</pre>";
	*/
	global $strLastTag;
        $xml = '';
    if ($level==1) {
        $xml .= '<?xml version="1.0" encoding="ISO-8859-1"?>'.
                "\n<COMPLIANCE>\n";
    }
    foreach ($array as $key=>$value) {
      // debugCompliance($key."-->".$value);
	    $key = strtoupper($key);
        if (is_array($value)) {
            $multi_tags = false;
		    foreach($value as $key2=>$value2) {
                 //debugCompliance($key2."-->".$value2);
				if (is_array($value2)) {
					if($key2 != $strLastTag)
					{
	                    $xml .= str_repeat("\t",$level)."<$key>\n";
						
					}
					$xml .= str_repeat("\t",$level)."<$key2>\n";
                    $xml .= array_to_xml($value2, $level+1);
					$xml .= str_repeat("\t",$level)."</$key2>\n";
					if($key2 != $strLastTag)
					{
						
						$xml .= str_repeat("\t",$level)."</$key>\n";
					}
                    $multi_tags = true;
                } else {
                    if (trim($value2)!='') {
                        if (htmlspecialchars($value2)!=$value2) {
                            $xml .= str_repeat("\t",$level).
                                    "<$key><![CDATA[$value2]]>".
                                    "</$key>\n";
                        } else {
                            $xml .= str_repeat("\t",$level).
                                    "<$key>$value2</$key>\n";
                        }
                    }
                    $multi_tags = true;
                }
				$strLastTag = $key2;
            }
            if (!$multi_tags and count($value)>0) {
                $xml .= str_repeat("\t",$level)."<$key>\n";
                $xml .= array_to_xml($value, $level+1);
                $xml .= str_repeat("\t",$level)."</$key>\n";
            }
        } else {
            if (trim($value)!='') {
                if (htmlspecialchars($value)!=$value) {
                    $xml .= str_repeat("\t",$level)."<$key>".
                            "<![CDATA[$value]]></$key>\n";
                } else {
                    $xml .= str_repeat("\t",$level).
                            "<$key>$value</$key>\n";
                }
            }
        }
    }
    if ($level==1) {
        $xml .= "</COMPLIANCE>\n";
    }
    return $xml;
}

function arrFormat($arrInput)
{
$date_time = date("Y-m-d h:i:s");
$stamp = strtotime($date_time);
//debugCompliance($date_time."-->".$stamp);
//debugCompliance(date('Y-m-d',$stamp));
$mapping = array(
	"OFAC" => array(
					'CompliancePersonID' => 'compliancePersonID',
					'EntNum' => 'ent_num',
					'SdnType' => 'sdn_type',
					'SdnTitle' => 'sdn_title',
					'FirstName' => 'firstName',
					'MiddleName' => 'middleName',
					'LastName' => 'lastName',
					'Address' => 'address',
					'Remarks' => 'remarks',
					'Dob' => 'dob',
					'AltId' => 'alt_id',
					'AltType' => 'alt_type',
					'AltName' => 'alt_name',
					'AltRemarks' => 'alt_remarks'
					),
   "PEP" => array(
   				  'CompPEPID' => 'comp_PEPID',
				  'PepTitle' => 'pep_title',
				  'FullName' => 'fullName',
				  'Position' => 'position',
				  'Portfolio' => 'portfolio'
				  ),
  "HM" =>  array(
				 'HmId' => 'hm_id',
				 'Title' => 'title',
				 'JobTitle' => 'job_title',
				 'FullName' => 'full_name',
				 'Name1' => 'name1',
				 'Name2' => 'name2',
				 'Name3' => 'name3',
				 'Name4' => 'name4',
				 'Name5' => 'name5',
				 'Name6' => 'name6',
				 'Name7' => 'name7',
				 'FullAddress' => 'full_address',
				 'Address1' => 'address1',
				 'Address2' => 'address2',
				 'Address3' => 'address3',
				 'Address4' => 'address4',
				 'Address5' => 'address5',
				 'Address6' => 'address6',
				 'AddRemarks' => 'add_remarks',
				 'ZipCode1' => 'zip_code1',
				 'ZipCode2' => 'zip_code2',
				 'ZipCode3' => 'zip_code3',
				 'Country1' => 'country1',
				 'Country2' => 'country2',
				 'Country3' => 'country3',
				 'Nationality1' => 'nationality1',
				 'Nationality2' => 'nationality2',
				 'Nationality3' => 'nationality3',
				 'Dob1' => 'dob1',
				 'Dob2' => 'dob2',
				 'DobTown' => 'dob_town',
				 'DobCountry' => 'dob_country',
				 'PassportNo' => 'passport_no',
				 'PassportDetail' => 'passport_detail',
				 'IdTitle' => 'id_title',
				 'IdNo' => 'id_no',
				 'OtherDetails' => 'other_details',
				 'GroupType' => 'group_type',
				 'GroupId' => 'group_id',
				 'AliasType' => 'alias_type',
				 'Regime' => 'regime',
				 'ListedOn' => 'listed_on',
				 'Remarks' => 'remarks'
	            )			  
   );

//debugCompliance($mapping);
$arrCloneInput = $arrInput;

foreach ($arrInput as $key=>$value)
{
	$arrCloneInput[$key] = $value;
	if(isset($mapping[$key]))
	{
		foreach($value as $key2 => $value2)
		{
			foreach($value2 as $key3 => $value3)
			{
				foreach($value3 as $key4 => $value4)
				{
					$arrFilped = array_flip($mapping[$key]);
					if(isset($arrFilped[$key4]))
					{
						if($key4 == 'compliancePersonID' || $key4 == 'comp_PEPID' || $key4 == 'hm_id'){
							$arrCloneInput[$key][$key2][$key3][$key4] = $value4."T".$stamp;
							//debugCompliance($key4);
							}
						$arrCloneInput[$key][$key2][$key3][$arrFilped[$key4]] = $arrCloneInput[$key][$key2][$key3][$key4];
						unset($arrCloneInput[$key][$key2][$key3][$key4]);
					}else{
						unset($arrCloneInput[$key][$key2][$key3][$key4]);
					}
				}
			}
		}
	}
}

return $arrCloneInput;
//debugCompliance($arrCloneInput);

}

 

?>