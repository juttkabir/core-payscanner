
<?
//session_start();

/***********************************************************************************************************************/
/*** Compliance Validation for user management Start***/
/***********************************************************************************************************************/


function gatewayCompliance($name, $arguments=false, $value=false)
{
	
	//while($record = mysql_fetch_assoc($result))
	$returnValue = '';
	
	if (is_callable($name)) {
	    
	    $ret = call_user_func($name, $arguments, $value);
	   
	   // $ret = call_user_func($cb3, $arg1, $arg2);
	  return $ret;  
	}else{
			echo("Call to undefined function as ".$record[$i]["name"]);
		}
		
	return $returnValue;	
}


function userMatch($arguments, $value="")
{
	 // debug_print_backtrace();
	  //echo error_reporting;
	  //debugCompliance($arguments);
	   $returnValue = array();
       $strMessage = "";
	   $date_time = date("Y-m-d h:i:s");
	   $firstName = $arguments["firstName"];
	   $middleName = $arguments["middleName"];
	   $lastName = $arguments["lastName"];
	   $list = $arguments["listName"];
	   $check_user = $arguments["check_user"];
	   $match = $arguments["match"];
	   $_arrListExact = $arguments["_arrListExact"];
	   $_arrListData = array();
		// compliance logic (Niaz)
	   
		/* trim name*/
		  $_arrName = trimName($firstName,$middleName,$lastName);
		 // debugCompliance($_arrName);
		/////////////exact match /////////////////////////////
				
				$getList = getListName($list);
				//debugCompliance($getList);

				/* if no list define in database then return back*/
				if($getList == 'CLE01')
				{
					// echo $getList;
					// die();
				}
			
			
			for($k =0; $k<count($getList);$k++)
			{
				$listName=strtoupper($getList[$k]["listName"]);
				$listID=$getList[$k]["listID"];
				if($listName == 'OFAC')
				{
					$_arrListData['OFAC'] = ofac_list($_arrName,$listName,$listID,$match);
				}elseif($listName == 'PEP'){
					$_arrListData['PEP'] = pep_list($_arrName,$listName,$listID,$match);
				}elseif($listName == 'HM TREASURY'){
					$_arrListData['HM'] = hm_treasury_list($_arrName,$listName,$listID,$match);
				}elseif($listName == 'PAYEX'){
					$_arrListData['PAYEX'] = payex_list($_arrName,$listName,$listID,$match);
				}
			}
			//debugCompliance($_arrListData);
	
		return $_arrListData;
	
		   /////////////End Exact Match /////////////////////////////
}



function getListName($list)
{
//debug_print_backtrace();
	$code = 'CLE01';
	
	if($list == 'all' || $list == '')
	
		$strListSql = SelectMultiRecordsCompliance("select listID,listName from complianceLists");
	else
		$strListSql = SelectMultiRecordsCompliance("select listID,listName from complianceLists where listName = '".$list."'");
		
		//debugCompliance($strListSql);
		
		if(empty($strListSql))
			return $code;
		else
			return $strListSql;
}
function trimName($firstName,$middleName,$lastName)
{
   //debug_print_backtrace();
   $_arrReturnData = '';
   $firstName =  trim($firstName);
   $middleName = trim($middleName);
   $lastName =   trim($lastName);

	$firstName = str_replace("'", "", $firstName);
	$middleName = str_replace("'", "", $middleName);
	$lastName = str_replace("'", "", $lastName);
	 
	$firstName = stripslashes($firstName);
	$middleName = stripslashes($middleName);
	$lastName = stripslashes($lastName);
	
	if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
		$full_name = $firstName.",".$middleName.",".$lastName;
	  }elseif($firstName !='' &&  $middleName!=''){
		$full_name = $firstName.",".$middleName;
	  }elseif($firstName !='' &&  $lastName!=''){
		$full_name = $firstName.",".$lastName;
	  }elseif($middleName!='' &&  $lastName){
		$full_name = $middleName.",".$lastName;
	  }elseif($firstName !=''){
		$full_name = $firstName;
	  }elseif($middleName!=''){
		$full_name = $middleName;
	  }elseif($lastName!=''){
	   $full_name = $lastName;
	 }	
	 
	$_arrReturnData = array("firstName"=>$firstName,"middleName"=>$middleName,"lastName"=>$lastName,"full_name"=>$full_name);
	
	return  $_arrReturnData;
		
}
function ofac_list($arguments,$listName,$listID,$match)
{
 //debug_print_backtrace();
 /*
 http://www.ustreas.gov/offices/enforcement/ofac/sdn/dat_spec.txt
 */
 $firstName = $arguments["firstName"];
 $middleName = $arguments["middleName"];
 $lastName = $arguments["lastName"];
 $full_name = $arguments["full_name"];
 $listName = trim($listName);
 $listID = trim($listID);
 $match = trim($match);
 $ofac_flag = false;
 $_arrReturnOFAC = array();
 $ofac_exact_counter = 0;
 $ofac_exact_alt_counter = 0;
 $ofac_partial_counter = 0;
 $ofac_partial_alt_counter = 0;
 $ofac_exact_id = array();
 $ofac_exact_alt_id = array();
 
 $strFieldsToFetchFromDb = "
							cp.compliancePersonID,
							cp.ent_num,
							cp.sdn_type,
							cp.sdn_title,
							cp.listID,
							cp.firstName,
							cp.middleName,
							cp.lastName,
							cp.PersonID,
							cp.LastModified,
							cp.address,
							cp.remarks,
							cp.dob,
							cp.isDisabled,
							cp.userDataBaseID
							";	
								
	                      /*  ca.add_id,
							ca.compliancePersonID as addressPID,
							ca.add_num,
							ca.address as AddressAlt,
							ca.city,
							ca.country,
							ca.add_remarks,
							ca.	listID 					
LEFT JOIN  comp_ofac_address as ca  ON cp.compliancePersonID = ca.compliancePersonID*/
 if(!empty($listID))
 {
 	$sqlExactOFAC = "select 
							$strFieldsToFetchFromDb
					 from 
					 		compliancePerson as cp
					where 
						cp.listID= '".$listID."' ";
	//debugCompliance($match,true); 
 /*Query Exact Match*/
	if($listName == 'OFAC' && ($match == 'exact' || $match == 'all' || $match == ''))
	 {
		if($firstName!='')
			$sqlExactOFAC .= " and (cp.firstName ='".$firstName."')"; 
		if($middleName!='')
			$sqlExactOFAC .= " and (cp.middleName ='".$middleName."')"; 
		if($lastName!='')
			$sqlExactOFAC .= " and (cp.lastName ='".$lastName."')"; 
			
		//debugCompliance($sqlExactOFAC);
		 $rs_exact_ofac = mysql_query($sqlExactOFAC)or die(__LINE__."Query Error:".mysql_error());
		 $subRecord = mysql_num_rows($rs_exact_ofac);
	
		 if($subRecord >= 1 ){
		 //debugCompliance($query,true);
		 
			 while($row_exact_ofac = mysql_fetch_array($rs_exact_ofac,MYSQL_ASSOC))
			 {
			  $_arrReturnOFAC['OFACEXACT'][$ofac_exact_counter] =  $row_exact_ofac;
			  $ofac_exact_id[$ofac_exact_counter] = $row_exact_ofac["compliancePersonID"];
			  $ofac_flag = true;
			 // debugCompliance($compliancePersonID);
			
			 $ofacAddressExact = getOfacAltAddress($row_exact_ofac["compliancePersonID"]);
			 $_arrReturnOFAC['OFACEXACT'][$ofac_exact_counter]['ADDRESSALT'] = $ofacAddressExact;
			 //debugCompliance($ofacAddressExact);
			  $ofac_exact_counter++;
			 //$_arrReturnOFAC['OFACEXACT']['OFACEXACTADDRESSALT'] = $ofacAddressExact;
			 }
		 }
		 // debugCompliance($sqlExactOFAC);
		   if($subRecord < 1 )
		   {
			// $query = "select alt_id,compliancePersonID as id,alt_name from comp_ofac_alt where listID= '".$listID."' "; 
			 $sqlExactAlt = "select * from comp_ofac_alt where listID= '".$listID."' "; 
		
			 if($firstName!='' || $middleName !='' || $lastName!=''){
			 $sqlExactAlt .= " and  alt_name = '".$full_name."'"; 
			 }
		//	debugCompliance(sqlExactAlt);
		   $rs_exact_alt = mysql_query($sqlExactAlt) or die(__LINE__."Query Error:".mysql_error());
			while($row_exact_alt = mysql_fetch_array($rs_exact_alt,MYSQL_ASSOC)){
			   $_arrReturnOFAC['OFACEXACTALTERNATE'][$ofac_exact_alt_counter] =  $row_exact_alt;
			   $ofac_exact_alt_id[$ofac_exact_alt_counter] = $row_exact_alt["alt_id"];
			   $ofac_flag = true;
			  $ofac_exact_alt_counter++;
			}
		}	
		//debugCompliance($_arrExactOFAC);
	  }
 
 } 
	/*Query Partial Match*/
 if(!empty($listID))
 {
 	$sqlPartialOFAC = "select 
							$strFieldsToFetchFromDb
					 from 
					 		compliancePerson as cp
					where 
						cp.listID= '".$listID."' ";
	//$sqlPartialOFAC = "select *  from compliancePerson where listID= '".$listID."' ";
	if($listName == 'OFAC' && ($match == 'partial' || $match == 'all' || $match == ''))
	{
	  if($firstName!='')
		$sqlPartialOFAC .= " and (cp.firstName ='".$firstName."' or cp.lastName ='".$firstName."')"; 
	  if($middleName!='')
		$sqlPartialOFAC .= " or(cp.middleName ='".$middleName."')"; 
	  if($lastName!='')
		$sqlPartialOFAC .= " or (cp.lastName ='".$lastName."' or cp.firstName ='".$lastName."')"; 
	
		//debugCompliance($sqlPartialOFAC);
		 $rs_partial_ofac = mysql_query($sqlPartialOFAC)or die(__LINE__."Query Error:".mysql_error());
		 $subRecord = mysql_num_rows($rs_partial_ofac);
		 if($subRecord >= 1 ){
			 while($row_partial_ofac = mysql_fetch_array($rs_partial_ofac,MYSQL_ASSOC))
			 {
			   if(!in_array($row_partial_ofac["compliancePersonID"],$ofac_exact_id))
			  	 $_arrReturnOFAC['OFACPARTIAL'][$ofac_partial_counter] =  $row_partial_ofac;
				  $ofac_flag = true;
				  $ofac_partial_counter++;
				 $ofacAddressPartial = getOfacAltAddress($row_partial_ofac["compliancePersonID"]);
				 //debugCompliance($ofacAddressPartial);
			 }
		 }
		   if($subRecord < 1 )
		   {
			 //$query = "select alt_id,compliancePersonID as id,alt_name from comp_ofac_alt where listID= '".$listID."' "; 
			 $sqlPartialAlt = "select * from comp_ofac_alt where listID= '".$listID."' "; 
		
			 if($firstName!='')
				$sqlPartialAlt .= "  and  (MATCH (alt_name) AGAINST ('".$firstName."')"; 
			 if($middleName!='')
				$sqlPartialAlt .= "  OR  MATCH(alt_name) AGAINST ('".$middleName."')"; 
			 if($lastName!='')
				$sqlPartialAlt .= "  OR MATCH(alt_name) AGAINST ('".$lastName."')"; 
			
			if(!empty($firstName) || !empty($middleName) || !empty($lastName))
				$sqlPartialAlt .= ")";
			 //debugCompliance($sqlPartialAlt); 	
			 
		   $rs_partial_alt = mysql_query($sqlPartialAlt) or die(__LINE__."Query Error:".mysql_error());
			while($row_partial_alt = mysql_fetch_array($rs_partial_alt,MYSQL_ASSOC)){
			   if(!in_array($row_partial_alt["alt_id"],$ofac_exact_alt_id))
			     $_arrReturnOFAC['OFACPARTIALALTERNATE'][$ofac_partial_alt_counter] =  $row_partial_alt;
			  //debugCompliance($_arrPartialOFAC);
			  $ofac_flag = true;
			  $ofac_partial_alt_counter++;
			}
		}
	 }
}
//debugCompliance($_arrReturnOFAC);
 return  $_arrReturnOFAC;

}
function getOfacAltAddress($id){
 if(!empty($id))
 {
 			$sqlAddress = "select 
			  						address as AddressAlt,
			  						city as City,
									compliancePersonID as addressPID,
									country as Country 
							 FROM 
							 		comp_ofac_address 
							 WHERE 
							 		compliancePersonID = '".$id."'";
					//debugCompliance($sqlPartialAddress);				
						$_arrReturnAddress = SelectMultiRecordsCompliance($sqlAddress);			
					 /* $rs_address_alt = mysql_query($sqlAddress) or die(__LINE__."Query Error:".mysql_error());
					  while($row_address_alt = mysql_fetch_array($rs_address_alt,MYSQL_ASSOC)){
						   //debugCompliance($row_partial_address_alt);
						   $_arrReturnAddress['OFACPARTIALADDRESSALT'] =  $row_address_alt;
					}*/
	}
	//debugCompliance($_arrReturnAddress);		
	return $_arrReturnAddress;
}
function hm_treasury_list($arguments,$listName,$listID,$match)
{
//debug_print_backtrace();
 $firstName = $arguments["firstName"];
 $middleName = $arguments["middleName"];
 $lastName = $arguments["lastName"];
 $full_name = trim($arguments["full_name"]);
 $listName = trim($listName);
 $listID = trim($listID);
 $hm_flag = false;
 $hm_exact_counter = 0;
 $hm_partial_counter = 0;
 $hm_exact_id = array();
 $_arrReturnHM = array();
 if(!empty($listID))
 {
 	$sqlExactHM = "select * from compliance_hm_treasury where list_id= '".$listID."' ";
	//debugCompliance($query,true);
	/*Query Exact Match*/
	 if($listName == 'HM TREASURY' && ($match == 'exact' || $match == 'all' || $match == ''))
	 {
		  $sqlExactHM = "select * from compliance_hm_treasury where list_id= '".$listID."' ";
		  if($firstName!='' || $middleName !='' || $lastName !='')
		  {
			 $sqlExactHM .= " and  full_name =('".$full_name."')"; 
			 $sqlExactHM .=" AND group_type = 'Individual' " ;
		  }	
		//debugCompliance($sqlExactHM); 	 
		  $rs_exact_hm = mysql_query($sqlExactHM) or die(__LINE__."Query Error:".mysql_error());
			  while($hm_exact_row = mysql_fetch_array($rs_exact_hm,MYSQL_ASSOC))
				 {
				  $_arrReturnHM['HMEXACT'][$hm_exact_counter] = $hm_exact_row; 
				  $hm_exact_id[$hm_exact_counter] =  $hm_exact_row[hm_id];
				  $hm_flag = true;
				  $hm_exact_counter++;
				 // debugCompliance($hm_exact_counter);
				 }  
		  
		   //debugCompliance($sqlExactHM); 	
	 }
 }
 /*Query Partial Match*/
 if(!empty($listID))
 {
 	$sqlPartialHM = "select * from compliance_hm_treasury where list_id= '".$listID."' ";
	 if($listName == 'HM TREASURY' && ($match == 'partial' || $match == 'all' || $match == ''))
	 {
		  if($firstName!='')
			 $sqlPartialHM .= "  and  (MATCH(full_name) AGAINST ('".$firstName."')"; 
		  if($middleName!='')
			 $sqlPartialHM .= "  OR  MATCH(full_name) AGAINST ('".$middleName."')"; 
		 if($lastName!='')
			$sqlPartialHM .= "  OR MATCH(full_name) AGAINST ('".$lastName."')"; 
		 
		  if(!empty($firstName) || !empty($middleName) || !empty($lastName))
		  $sqlPartialHM .= ")";  
		  
		  $sqlPartialHM .=" AND group_type = 'Individual' " ;
	//debugCompliance($hm_exact_id); 
	  //debugCompliance($sqlPartialHM); 	
	  $rs_partial_hm = mysql_query($sqlPartialHM) or die(__LINE__."Query Error:".mysql_error());
			  while($hm_partial_row = mysql_fetch_array($rs_partial_hm,MYSQL_ASSOC))
				 {
					if(!in_array($hm_partial_row["hm_id"],$hm_exact_id))
						$_arrReturnHM['HMPARTIAL'][$hm_partial_counter] = $hm_partial_row; 
				  $hm_flag = true;
				  $hm_partial_counter++;
				  //debugCompliance($_arrReturnHM);
				 }	
		}		   
 }	
//debugCompliance($_arrReturnHM);
return $_arrReturnHM;		
}

function pep_list($arguments,$listName,$listID,$match)
{
 $firstName = $arguments["firstName"];
 $middleName = $arguments["middleName"];
 $lastName = $arguments["lastName"];
 $full_name = $arguments["full_name"];
 $_arrReturnPEP = array();
 $pep_exact_counter = 0;
 $pep_partial_counter = 0;
 $pep_exact_id = array();
 $pep_flag = false;

	if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
	$full_name_pep = $firstName." ".$middleName." ".$lastName;
	}elseif($firstName !='' &&  $middleName!=''){
	$full_name_pep = $firstName." ".$middleName;
	}elseif($firstName !='' &&  $lastName!=''){
	$full_name_pep = $firstName." ".$lastName;
	}elseif($middleName!='' &&  $lastName){
	$full_name_pep = $middleName." ".$lastName;
	}elseif($firstName !=''){
	$full_name_pep = $firstName;
	}elseif($middleName!=''){
	$full_name_pep = $middleName;
	}elseif($lastName!=''){
	$full_name_pep = $lastName;
	}
	
	if(!empty($listID))
	{	
		$sqlExactPEP = "select * from compliancePEP where listID= '".$listID."' "; 
		/*query exact match*/
		if($listName == 'PEP' && ($match == 'exact' || $match == 'all' || $match == ''))
		{
			 if($firstName!='' || $middleName !='' || $lastName !='')
				 $sqlExactPEP .= " and  fullName =('".trim($full_name_pep)."')"; 
		
			//debugCompliance($sqlExactPEP);
			$rs_exact_pep = mysql_query($sqlExactPEP) or die(__LINE__."Query Error:".mysql_error());
			 while($row_exact_pep = mysql_fetch_array($rs_exact_pep,MYSQL_ASSOC))
			 {
			   $_arrReturnPEP['PEPEXACT'][$pep_exact_counter] = $row_exact_pep; 
			   $pep_exact_id[$pep_exact_counter] = $row_exact_pep["comp_PEPID"];
			  $pep_flag = true;
			  $pep_exact_counter++;
			  //debugCompliance($id);
			 }	
		}   
   }	
	/*query partial match*/
	if(!empty($listID))
	{	
		$sqlPartialPEP = "select * from compliancePEP where listID= '".$listID."' "; 
		
		if($listName == 'PEP' && ($match == 'partial' || $match == 'all' || $match == ''))
		{
			if($firstName!='')
				$sqlPartialPEP .= "  AND ( MATCH(fullName) AGAINST('".$firstName."')"; 			     
			if($middleName!='')
				$sqlPartialPEP .= "  OR  MATCH(fullName) AGAINST ('".$middleName."')"; 
			if($lastName!='')
				$sqlPartialPEP .= "  OR MATCH(fullName) AGAINST ('".$lastName."')"; 
			
			if(!empty($firstName) || !empty($middleName) || !empty($lastName))
			$sqlPartialPEP .= ")";
		
	  	  //debugCompliance($sqlPartialPEP);
	 	  $rs_partial_pep = mysql_query($sqlPartialPEP) or die(__LINE__."Query Error:".mysql_error());
			 while($row_partial_pep = mysql_fetch_array($rs_partial_pep,MYSQL_ASSOC))
			 {
			  if(!in_array($row_partial_pep["comp_PEPID"],$pep_exact_id))
			  	 $_arrReturnPEP['PEPPARTIAL'][$pep_partial_counter] = $row_partial_pep; 
			  
			   $pep_partial_counter++;
			   $pep_flag = true;
			 }	  
       }
  } 		
// debugCompliance($_arrReturnPEP);
return $_arrReturnPEP;		
 
}

function payex_list($arguments,$listName,$listID)
{
 $firstName = $arguments["firstName"];
 $middleName = $arguments["middleName"];
 $lastName = $arguments["lastName"];
 $full_name = $arguments["full_name"];
 $_arrReturnPAYEX = array();
 $payex_flag = false;
 $payex_exact_counter = 0;
 $payex_partial_counter = 0;
 $payex_exact_id = array();
 
if(!empty($listID)) 
{
 	$sqlExactPayex = "select *  from compliancePerson where listID= '".$listID."'"; 
 
	 /* query exact match*/
	 if($listName == 'PAYEX' && ($match == 'exact' || $match == 'all' || $match == ''))
	 {
		if($firstName!=''){
			$sqlExactPayex .= " and (firstName ='".$firstName."')"; 
		}
		if($middleName!=''){
			$sqlExactPayex .= " and (middleName ='".$middleName."')"; 
		}
		if($lastName!=''){
			$sqlExactPayex .= " and (lastName ='".$lastName."')"; 
		}	
		// debugCompliance($sqlExactPayex); 
		 $rs_exact_payex = mysql_query($sqlExactPayex) or die(__LINE__."Query Error:".mysql_error());
		 while($row_exact_payex = mysql_fetch_array($rs_exact_payex,MYSQL_ASSOC))
		 {
		    $_arrReturnPAYEX['PAYEXEXACT'][$payex_exact_counter] = $row_exact_payex; 
		    $payex_exact_id[$payex_exact_counter] =  $row_exact_payex["compliancePersonID"];
			$payex_exact_counter++;
			$payex_flag = true;
		 }	  	
	}
}	
/* query partial match*/
if(!empty($listID)) 
{
	$sqlPartialPayex = "select *  from compliancePerson where listID= '".$listID."'"; 
	
	if($listName == 'PAYEX' && ($match == 'partial' || $match == 'all' || $match == ''))	   
	{
		 if($firstName!='')
			 $sqlPartialPayex .= "  AND (MATCH(firstName,middleName,lastName) AGAINST('".$firstName."')"; 			     
		 if($middleName!='')
			 $sqlPartialPayex .= "  OR  MATCH(firstName,middleName,lastName) AGAINST ('".$middleName."')"; 
		 if($lastName!='')
			 $sqlPartialPayex .= "  OR MATCH(firstName,middleName,lastName) AGAINST ('".$lastName."')"; 
		
		if(!empty($firstName) || !empty($middleName) || !empty($lastName)) 
			 $sqlPartialPayex .= ")";
	}
	  // debugCompliance($sqlPartialPayex); 	
			$rs_partial_payex = mysql_query($sqlPartialPayex) or die(__LINE__."Query Error:".mysql_error());
			 while($row_partial_payex = mysql_fetch_array($rs_partial_payex,MYSQL_ASSOC))
			 {
				if(!in_array($row_partial_payex["compliancePersonID"],$payex_exact_id))
					$_arrReturnPAYEX['PAYEXPARTIAL'][$payex_partial_counter] = $row_partial_payex; 
			    
				$payex_partial_counter++;
				$payex_flag = true;
			 
			 }	  
		 	  
}	
//debugCompliance($_arrReturnPAYEX);	
return $payex;		
 
}


/***********************************************************************************************************************/
/*** Compliance Validation for user management End***/
/***********************************************************************************************************************/

function debugCompliance( $variableToDebug = "", $shouldExitAfterDebug = false )
{
	if ( defined("DEBUG_ON")
		 && DEBUG_ON == true )
	{
		$arrayFileDetails = debug_backtrace();
		echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
	
		if ( !empty($variableToDebug)  )
		{
			echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
		}
		else
		{
			echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
		}
		echo "</pre>";
		if ($shouldExitAfterDebug)
			exit();
	}
}

function SelectMultiRecordsCompliance($Querry_Sql)
{   
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
	
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
	    $count = 0;
		$data = array();
		while ( $row = mysql_fetch_array($result,MYSQL_ASSOC)) 
		{
			$data[$count] = $row;
			$count++;
		}
			return $data;
	}
}

function selectFromCompliance($Querry_Sql)
{ //echo DEBUG . $Querry_Sql;
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}
function insertIntoCompliance($Querry_Sql)
{	//echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}
		//******** Function modification by Usman Ghani *********/
		// Add the code to return false in case of query error.
		return false; 
		// End of modification code.
	}   
	else
 	{	
		return true;	
	}
}
function updateCompliance($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
		echo mysql_message($Querry_Sql);		
		}
		//******** Function modification by Usman Ghani *********/
		// Add the code to return false in case of query error.
		return false; 
		// End of modification code.
	}   
	else
 	{
		return true;
   	}
}
function deleteFromCompliance($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}


?>