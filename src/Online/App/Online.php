<?php
namespace Payex\Online\App;
use Payex\Database\Connection;
use Payex\Helper\UiHelper;
use Payex\Online\Helper\LoginHelper;
use Payex\Repository\AgentRepository;

/**
 * Created by PhpStorm.
 * Date: 18.03.19
 * Time: 0:42
 */

class Online
{
    /**
     * @var Connection
     */
    protected $db;

    /**
     * @var LoginHelper
     */
    protected $loginHelper;

    protected $agentRepository;

    protected $uiHelper;

    /**
     * @var Online
     */
    private static $instance;

    private function __construct()
    {
    }

    /**
     * @return Online
     */
    public static function getInstance()
    {
        if (empty(self::$instance)){
            self::$instance = new Online();
        }
        return self::$instance;
    }

    /**
     * @param $db Connection
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return Connection
     */
    public function getDb()
    {
        return $this->db;
    }

    public function getBaseUrl()
    {
        $protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
        $domain = $_SERVER['SERVER_NAME'];
        $baseUrl = "${protocol}://${domain}/core-payscanner/online_all/";
        return $baseUrl;
    }

    public function getBaseDir()
    {
        $baseDir = __DIR__ . '/../../../';
        return $baseDir;
    }

    /**
     * @return AgentRepository
     */
    public function getAgentRepository()
    {
        if (empty($this->agentRepository)){
            $this->agentRepository = new AgentRepository($this->db);
        }
        return $this->agentRepository;
    }

    /**
     * @return LoginHelper
     */
    public function getLoginHelper()
    {
        if (empty($this->loginHelper)){
            $this->loginHelper = new LoginHelper($this->getAgentRepository(), $this->getUiHelper());
        }
        return $this->loginHelper;
    }

    /**
     * @return UiHelper
     */
    public function getUiHelper()
    {
        if (empty($this->uiHelper)){
            $this->uiHelper = new UiHelper();
        }
        return $this->uiHelper;
    }
}
