<?php
namespace Payex\Online\Service;
use Payex\Database\Connection;
use Payex\Helper\DataHelper;
use Payex\Model\Customer;
use Payex\Repository\UserIdTypeRepository;

/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 16:25
 */

class CustomerService
{
    private $db;

    protected $dataHelper;

    public function __construct(Connection $db)
    {
        $this->db = $db;
        $this->dataHelper = new DataHelper();
    }

    public function getCustomerServiceType()
    {
        return false;
    }

    /**
     * @param Customer $customer
     * @param string $customerEmail
     * @param $password
     * @return bool
     */
    public function loginCustomer(Customer $customer, $customerEmail, $password)
    {
        if ($this->checkCustomerCredentials($customer, $customerEmail, $password)){
            if ($customer->getCustomerStatus() === Customer::STATUS_ENABLE && (!empty($customer->getParentId()) || !is_null($customer->getParentId()))){
                return true;
            }
        }
        return false;
    }

    /**
     * @param Customer $customer
     * @param string $customerEmail
     * @param $password
     * @return bool
     */
    public function checkCustomerCredentials(Customer $customer, $customerEmail, $password)
    {
        return $customerEmail === $customer->getEmail() && $password === $customer->getPassword();
    }

    /**
     * @param Customer $customer
     * @return bool
     * @throws \Exception
     */
    public function checkCustomerIdTypeExpireDate(Customer $customer)
    {
        $userIdTypeRepository = new UserIdTypeRepository($this->db);
        $currentDate = new \DateTime();
        $customerExpiryDate = $userIdTypeRepository->getCustomerExpireDate($customer->getId());
        if (!$customerExpiryDate = new \DateTime($customerExpiryDate['expiry_date'])){
            throw new \Exception('Not correct format for customer expiry date');
        }
        return $customerExpiryDate > $currentDate ? true : false;
    }

    /**
     * @param Customer $customer
     * @param $pageType
     * @return array
     */
    public function checkCustomerServiceType(Customer $customer, $pageType)
    {
        $data =[];
        if ($pageType === 'PC'){
            $data['serviceName'] = 'Prepaid';
        }else{
            $data['serviceName'] = 'Online Trading';
        }

//        $customerServices = $this->dataHelper->createArrayFromString($customer->getOnlineCustomerServices());
//        if (in_array($pageType, $customerServices)){
//            if (in_array('PC', $customerServices)){
//                $data['redirectPage'] = 'topup.php';
//            }else{
//                $data['redirectPage'] = 'make_payment-ii-new.php';
//            }
//        }else{
//            $data['error'] = 'This customer don\'t have ' . $data['serviceName'] . ' service';
//        }

        $customerServices = $this->dataHelper->createArrayFromString($customer->getOnlineCustomerServices());
        if (in_array($pageType, $customerServices)){
            if (in_array('PC', $customerServices)){
                $data['redirectPage'] = "verifyCustomer.php";
                $data['from'] = 'index';
                $data['type'] = 'PC';
                $data['customerID'] = $customer->getId();
//                $data['redirectPage'] = 'topup.php';
            }else{
                $data['redirectPage'] = "verifyCustomer.php";
                $data['from'] = 'index';
                $data['type'] = '';
                $data['customerID'] = $customer->getId();
//                $data['redirectPage'] = 'make_payment-ii-new.php';
            }
        }else{
            $data['error'] = 'This customer don\'t have ' . $data['serviceName'] . ' service';
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getCountryListForRegistrationPage()
    {
        $countryList = '';
        $query = 'SELECT DISTINCT countryName FROM countries';
        $result = $this->db->selectAll($query);
        foreach ($result as $row) {
            $countryList .= $row['countryName'] . ',';
        }

        return $countryList;
    }

    /**
     * @param $serviceArray
     * @return bool
     */
    public function hasPrepaidService($serviceArray)
    {
        return in_array('PC', $serviceArray);
    }

    public function getBeneficiariesListWithBankInfo(Customer $customer)
    {
        $query = 'SELECT beneficiary.benID, beneficiary.firstName AS benName, ben_banks.bankName FROM beneficiary
                    LEFT JOIN ben_banks ON ben_banks.benId = beneficiary.benID
                    WHERE beneficiary.customerID="' . $customer->getId(). '"
                    AND status = "Enabled"';
        return $this->db->selectAll($query);
    }
}
