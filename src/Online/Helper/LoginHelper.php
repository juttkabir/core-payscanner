<?php

namespace Payex\Online\Helper;

use Payex\Helper\UiHelper;
use Payex\Model\Agent;
use Payex\Repository\AgentRepository;

/**
 * Created by PhpStorm.
 * Date: 18.03.19
 * Time: 0:06
 */
class LoginHelper
{
    const MAIN_AGENT_URL = 'http://www.payscanner.com';
    /**
     * @var AgentRepository
     */
    protected $agentRepository;

    /**
     * @var UiHelper
     */
    protected $uiHelper;

    /**
     * LoginHelper constructor.
     * @param AgentRepository $agentRepository
     * @param UiHelper $uiHelper
     */
    public function __construct(AgentRepository $agentRepository, UiHelper $uiHelper)
    {
        $this->agentRepository = $agentRepository;
        $this->uiHelper = $uiHelper;
    }

    /**
     * @param $onlineModuleUrl
     * @param string $loggedCustomerAgent
     * @return array|bool
     */
    public function getAgentUiData($onlineModuleUrl, $loggedCustomerAgent = '')
    {
        if (!$loggedCustomerAgent){
            $agent = $this->agentRepository->getAgentByOnlineUrl($onlineModuleUrl);
        }else{
            $agent = $this->agentRepository->getById($loggedCustomerAgent);
        }
        /* @var $agent Agent */
        if ($agent) {
            $data['title'] = $agent->getCompanyName();
            $data['email'] = $agent->getEmail();
            $data['phone'] = $agent->getAgentPhoneNumber();
            $data['address'] = $agent->getAgentAddress();
            $data['agentId'] = $agent->getId();
            $data['logoHtml'] = $this->uiHelper->getLogoHtml($agent->getLogoPath(), true);
            $data['logoUrls'] = $this->uiHelper->getLogoUrls($agent->getLogoPath(), true);
        } else {
            $data['title'] = TITLE;
            $data['email'] = SUPPORT_EMAIL;
            $data['phone'] = COMPANY_PHONE;
            $data['address'] = COMPANY_ADDRESS;
            $data['logoHtml'] = $this->uiHelper->getLogoHtml('');
            $data['logoUrls'] = $this->uiHelper->getLogoUrls('');
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}