<?php
/**
 * Created by PhpStorm.
 * Date: 01.05.19
 * Time: 12:57
 */

namespace Payex\Online\Helper;


use Payex\Database\Connection;

class BalanceInquiryHelper
{
    /**
     * @var Connection
     */
    protected $db;

    /**
     * BalanceInquiryHelper constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param $customerId
     * @return array|bool
     */
    public function getReceivingCurrencies($customerId)
    {
        $currencies = [];
        if ($customerId){
            $query = 'SELECT `SAN`,`card_issue_currency` FROM `card_issue_response` WHERE `customerID` = "' . $customerId . '"';
            $currencies = $this->db->selectAll($query);
        }

        return $currencies;
    }
}