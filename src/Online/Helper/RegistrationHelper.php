<?php
namespace Payex\Online\Helper;
/**
 * Created by PhpStorm.
 * Date: 26.03.19
 * Time: 2:31
 */

class RegistrationHelper
{
    /**
     * @param $countryList
     * @return string
     */
    public function getCountryOptionListForRegistrationOnlinePage($countryList)
    {
        $list = '';
        if ($countryList){
            $countries = explode(",", $countryList);
            $countries = array_filter($countries);
            for ($k = 0; $k < count($countries); $k++) {
                $selected = '';
                if (trim($countries[$k]) == "United Kingdom") $selected = "selected";
                $list .= '<option value="' . trim($countries[$k]) . '"' . $selected . '>' . trim($countries[$k]) . '</option>';
            }
        }

        return $list;
    }
}