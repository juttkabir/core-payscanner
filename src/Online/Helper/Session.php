<?php
namespace Payex\Online\Helper;
use Exception;

/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 16:08
 */

class Session
{
    /**
     * Session Age.
     *
     * The number of seconds of inactivity before a session expires.
     *
     * @var integer
     */
    protected static $SESSION_AGE = 1800;

    /**
     * Writes a value to the current session data.
     *
     * @param string $key String identifier.
     * @param mixed $value Single value or array of values to be written.
     * @return mixed Value or array of values written.
     * @throws Exception
     */
    public static function write($key, $value)
    {
        if ( !is_string($key) )
            throw new Exception('Session key must be string value');
        self::_init();
        $_SESSION[$key] = $value;
        self::_age();
        return $value;
    }

    /**
     * Initializes a new secure session or resumes an existing session.
     *
     * @return boolean Returns true upon success and false upon failure.
     * @throws Exception
     */
    private static function _init()
    {
        if (function_exists('session_status'))
        {
            // PHP 5.4.0+
            if (session_status() == PHP_SESSION_DISABLED)
                throw new Exception('Sessions are disabled');
        }

        if ( '' === session_id() )
        {
            $secure = true;
            $httponly = true;
            // Disallow session passing as a GET parameter.
            // Requires PHP 4.3.0
            if (ini_set('session.use_only_cookies', 1) === false) {
                throw new Exception('Can\'t set session to use only cookies');
            }
            // Mark the cookie as accessible only through the HTTP protocol.
            // Requires PHP 5.2.0
            if (ini_set('session.cookie_httponly', 1) === false) {
                throw new Exception('Can\'t mark the cookie as accessible only through the HTTP protocol');
            }
            // Ensure that session cookies are only sent using SSL.
            // Requires a properly installed SSL certificate.
            // Requires PHP 4.0.4 and HTTPS
            //if (ini_set('session.cookie_secure', 1) === false) {
            //    throw new SessionCookieSecureException();
            //}
            $params = session_get_cookie_params();
            session_set_cookie_params($params['lifetime'],
                $params['path'], $params['domain'],
                $secure, $httponly
            );
            return session_start();
        }
        // Helps prevent hijacking by resetting the session ID at every request.
        // Might cause unnecessary file I/O overhead?
        // TODO: create config variable to control regenerate ID behavior
        return session_regenerate_id(true);
    }

    /**
     * Expires a session if it has been inactive for a specified amount of time.
     *
     * @return void
     * @throws Exception
     */
    private static function _age()
    {
        $last = isset($_SESSION['LAST_ACTIVE']) ? $_SESSION['LAST_ACTIVE'] : false ;

        if (false !== $last && (time() - $last > self::$SESSION_AGE))
        {
            self::destroy();
            throw new Exception('Can\'t read or write is attempted on an expired session');
        }
        $_SESSION['LAST_ACTIVE'] = time();
    }

    /**
     * Removes session data and destroys the current session.
     *
     * @return void
     */
    public static function destroy()
    {
        if ( '' !== session_id() )
        {
            $_SESSION = array();
            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }
            session_destroy();
        }
    }
}
