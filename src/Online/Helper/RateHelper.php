<?php
namespace Payex\Online\Helper;
use Payex\Database\Connection;

/**
 * Created by PhpStorm.
 * Date: 20.03.19
 * Time: 22:25
 */

class RateHelper
{
    protected $db;
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getRates($agentId = '')
    {
        $agentCondition = '';
        if ($agentId){
            $agentCondition = ' AND agentID = "' . $agentId . '"';
        }
        $rates = $this->db->selectAll('(SELECT quotedCurrency AS currencies FROM   fx_rates_margin  where statusType = \'enable\'' . $agentCondition .') UNION (SELECT baseCurrency AS currencies FROM fx_rates_margin  where statusType = \'Enable\'' . $agentCondition .')');
        return $rates ? $rates : [];
    }
}