<?php
namespace Payex\Repository;

use Payex\Database\Connection;
use Payex\Model\Agent;

/**
 * Created by PhpStorm.
 * Date: 17.03.19
 * Time: 16:40
 */

class AgentRepository
{
    const ADMIN_TABLE = 'admin';
    /**
     * @var Connection
     */
    protected $db;

    /**
     * AdminUserRepository constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Return agent by ID
     * @param $id
     * @return bool|Agent
     */
    public function getById($id)
    {
        $query = 'SELECT * FROM ' . self::ADMIN_TABLE . ' WHERE userID="' . $id . '"';
        $result = $this->db->select($query);
        if ($result){
            $agent = new Agent($result);
            return $agent;
        }else{
            return false;
        }
    }

    /**
     * @param $url
     * @return bool|Agent
     */
    public function getAgentByOnlineUrl($url)
    {
        $query = 'SELECT * FROM ' . self::ADMIN_TABLE . ' WHERE spi_online_module = "' . $url . '"';
        $result = $this->db->select($query);
        if ($result){
            $agent = new Agent($result);
            return $agent;
        }else{
            return false;
        }
    }
}