<?php
namespace Payex\Repository;
use Payex\Database\Connection;
use Payex\Model\Customer;

/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 15:27
 */

class CustomerRepository
{
    const CUSTOMER_TABLE = 'customer';

    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getById($id)
    {
        $query = 'SELECT * FROM ' . self::CUSTOMER_TABLE . ' WHERE customerID="' . $id . '"';
        $result = $this->db->select($query);
        if ($result){
            $agent = new Customer($result);
            return $agent;
        }else{
            return false;
        }
    }

    public function getByCustomerNumber($customerNumber)
    {
        $query = 'SELECT * FROM ' . self::CUSTOMER_TABLE . ' WHERE accountName="' . $customerNumber . '"';
        $result = $this->db->select($query);
        if ($result){
            $agent = new Customer($result);
            return $agent;
        }else{
            return false;
        }
    }

    /**
     * @param string $customerEmail
     * @return bool|Customer
     */
    public function getByCustomerEmail($customerEmail)
    {
        $query = 'SELECT * FROM ' . self::CUSTOMER_TABLE . ' WHERE email="' . $customerEmail . '"';
        $result = $this->db->select($query);
        if ($result){
            $agent = new Customer($result);
            return $agent;
        }else{
            return false;
        }
    }
}