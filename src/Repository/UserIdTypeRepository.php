<?php
namespace Payex\Repository;
use Payex\Database\Connection;

/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 16:46
 */

class UserIdTypeRepository
{
    const USER_ID_TYPES_TABLE = 'user_id_types';

    const CONFIG_ID_ID_TYPES = 1;

    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getCustomerExpireDate($customerId)
    {
        $query = 'SELECT expiry_date FROM ' . self::USER_ID_TYPES_TABLE . ' WHERE user_id = "' . $customerId . '" AND user_type="C" AND  id_type_id="' . self::CONFIG_ID_ID_TYPES . '"';
        $expiryDate = $this->db->select($query);
        return $expiryDate;
    }
}