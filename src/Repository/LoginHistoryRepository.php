<?php
namespace Payex\Repository;
use Payex\Database\Connection;
use Payex\Model\LoginHistory;

/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 17:51
 */

class LoginHistoryRepository
{
    const LOGIN_HISTORY_TABLE = 'login_history';

    /**
     * @var Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function save(LoginHistory $loginHistory)
    {
        $dbHandle = $this->db->getDbHandle();
        $statement = $dbHandle->prepare('INSERT INTO ' . self::LOGIN_HISTORY_TABLE .
            ' (login_time, login_name, access_ip, login_type, user_id, custID) VALUES 
            (:logintime, :loginname, :accessip, :logintype, :userid, :custid)');
        $statement->execute([
            'logintime' => $loginHistory->getLoginTime(),
            'loginname' => $loginHistory->getLoginName(),
            'accessip' => $loginHistory->getAccessIp(),
            'logintype' => $loginHistory->getLoginType(),
            'userid' => $loginHistory->getUserId(),
            'custid' => $loginHistory->getCustId()
        ]);
        return $statement ? $dbHandle->lastInsertId() : false;
    }
}