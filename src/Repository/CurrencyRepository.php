<?php
namespace Payex\Repository;
use Payex\Database\Connection;
use Payex\Model\Currency;

/**
 * Created by PhpStorm.
 * Date: 29.03.19
 * Time: 0:26
 */

class CurrencyRepository
{
    const CURRENCIES_TABLE = 'currencies';
    /**
     * @var Connection
     */
    protected $db;

    /**
     * AdminUserRepository constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param $countryList
     * @return array
     */
    public function getByCountryList($countryList)
    {
        $dbHandle = $this->db->getDbHandle();
        $list = [];
        foreach ($countryList as $item){
            $list[] = strtoupper($item);
        }
        $in  = str_repeat('?,', count($list) - 1) . '?';
        $query = 'SELECT * FROM ' . self::CURRENCIES_TABLE . ' WHERE country IN (' . $in . ')';
        $statement = $dbHandle->prepare($query);
        $statement->execute($list);
        $collection = [];
        $currencies = $statement->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($currencies as $currency){
            $collection[] = $this->createModel($currency);
        }

        return $collection;
    }

    /**
     * @param $data
     * @return Currency
     */
    private function createModel($data)
    {
        if ($data && is_array($data)){
            $currency = new Currency();
            $currency->setCID($data['cID']);
            $currency->setCountry($data['country']);
            $currency->setCurrencyName($data['currencyName']);
            $currency->setNumCode($data['numCode']);
            $currency->setDescription($data['description']);
            $currency->setCurrencyType($data['currencyType']);
            $currency->setCurrencyFor($data['currency_for']);

            return $currency;
        }
    }
}