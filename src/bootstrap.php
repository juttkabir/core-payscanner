<?php
/**
 * Created by PhpStorm.
 * Date: 17.03.19
 * Time: 15:19
 */

use Payex\App\App;
use Payex\Database\Connection;
use Payex\Online\App\Online;

include_once __DIR__ . '/../include/masterConfig.php';
require __DIR__ . '/../vendor/autoload.php';

/**
 * @var $db Connection
 */
$db = Connection::getInstance();
$db->setCredentials(SERVER_MASTER, USER, PASSWORD, DATABASE);
$db->createConnection();

/**
 * @var $app App
 */
$app = App::getInstance();
$app->setDb($db);

/**
 * @var $online Online
 */
$online = Online::getInstance();
$online->setDb($db);
