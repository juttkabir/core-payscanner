<?php
namespace Payex\App;

use Payex\Database\Connection;
use Payex\Helper\UiHelper;
use Payex\Repository\AgentRepository;

/**
 * Created by PhpStorm.
 * Date: 17.03.19
 * Time: 15:27
 */

class App
{
    /**
     * @var App
     */
    private static $instance;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var AgentRepository
     */
    public $agentRepository;

    public $uiHelper;

    private function __construct()
    {
    }

    public function getBaseDir()
    {
        $baseDir = __DIR__;
        return $baseDir;
    }

    public function getBaseUrl()
    {
        $protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
        $domain = $_SERVER['SERVER_NAME'];
        $baseUrl = "${protocol}://${domain}/";
        return $baseUrl;
    }

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (empty(self::$instance)){
            self::$instance = new App();
        }
        return self::$instance;
    }

    /**
     * @param $db Connection
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return Connection
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return AgentRepository
     */
    public function getAgentRepository()
    {
        if (empty($this->agentRepository)){
            $this->agentRepository = new AgentRepository($this->db);
        }
        return $this->agentRepository;
    }

    public function getUiHelper()
    {
        if (empty($this->uiHelper)){
            $this->uiHelper = new UiHelper();
        }
        return $this->uiHelper;
    }
}