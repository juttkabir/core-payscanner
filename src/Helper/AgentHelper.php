<?php
namespace Payex\Helper;
use Payex\Model\Agent;

/**
 * Created by PhpStorm.
 * Date: 28.03.19
 * Time: 10:12
 */

class AgentHelper
{
    /**
     * @param $data array
     * @return array
     */
    public function getAgentRelatedCountries($data)
    {
        $countries = [];
        if (is_array($data)){
            foreach ($data as $item){
                if (strpos($item, ',') !== false){
                    $arr = explode(',', $item);
                    if (is_array($arr)){
                        foreach ($arr as $element){
                            $countries[] = trim($element);
                        }
                    }
                }else{
                    $countries[] = trim($item);
                }
            }
        }

        return $countries;
    }

    /**
     * @param Agent $agent
     * @return bool
     */
    public function isAgent(Agent $agent)
    {
        return $agent->getAdminType() === Agent::ADMIN_TYPE_AGENT && (
            $agent->getAgentType() === Agent::AGENT_TYPE_SUPPER
            || $agent->getAgentType() === Agent::AGENT_TYPE_SUB);
    }
}