<?php

namespace Payex\Helper;
/**
 * Created by PhpStorm.
 * Date: 17.03.19
 * Time: 18:35
 */
class UiHelper
{
    /**
     * @param $logo
     * @param bool $isOnline
     * @return string
     */
    public function getLogoHtml($logo, $isOnline = false)
    {
        if ($logo) {
            if (!$isOnline) {
                $file = $logo['fullPath'];
                $link = $logo['relativePath'];
            } else {
                $file = $logo['fullOnlinePath'];
                $link = $logo['relativeOnlinePath'];
            }
            if ($file && file_exists($file)) {
                $arr = getimagesize($file);

                $width = ($arr[0] > 200 ? 200 : $arr[0]);
                $height = ($arr[1] > 100 ? 100 : $arr[1]);

                return '<img src="' . $link . '" width="' . $width . '" height="' . $height . '">';
            }
        }
        return '<img height="' . CONFIG_LOGO_HEIGHT . '" alt="" src="' . CONFIG_LOGO . '" width="' . CONFIG_LOGO_WIDTH . '">';
    }

    public function getLogoUrls($logo, $isOnline = false)
    {
        if ($logo){
            if (!$isOnline) {
                $file = $logo['fullPath'];
                $link = $logo['relativePath'];
            } else {
                $file = $logo['fullOnlinePath'];
                $link = $logo['relativeOnlinePath'];
            }
            if ($file && file_exists($file)) {
                return [
                    'fullPath' => $file,
                    'relativePath' => $link,
                    'width' => 200,
                    'height' => 70
                ];
            }
        }
        return ['relativePath' => CONFIG_LOGO];
    }

    /**
     * @param $agentName
     * @param $agentUrl
     * @return string
     */
    public function getShortcutHeaderLink($agentName, $agentUrl)
    {
        $str = 'Go to SPI ' . $agentName;
        $template = '<a href="' . $agentUrl . '" target="_blank"><b><font color="#264661">' . $str . '</font></b></a>';
        return $template;
    }

    public function getFooterCompanyRegNumberString($agentRegNum)
    {
        $str = 'Company Registration Number ' . $agentRegNum;
        return $str;
    }

    public function createListString($arr)
    {
        $result = '';
        if ($arr && is_array($arr)){
            foreach ($arr as $item){
                if (!empty($result)){
                    $result .= ',' . $item['countryName'];
                }else{
                    $result = $item['countryName'];
                }
            }
        }

        return $result;
    }

    /**
     * @param $url
     * @return mixed|string
     */
    public function getCurrentPageScriptName($url)
    {
        $result = '';
        if ($url && is_string($url)){
            $arr = explode('/', $url);
            if (strpos(end($arr), '.php') !== false){
                $result = end($arr);
            }
        }

        return $result;
    }

    /**
     * @param $url
     * @param $menuItemScriptName
     * @return bool
     */
    public function checkIfNeedSelect($url, $menuItemScriptName)
    {
        $result = false;
        if ($url && $menuItemScriptName){
            if ($url === $menuItemScriptName){
                $result = true;
            }
        }

        return $result;
    }
}