<?php
namespace Payex\Helper;
/**
 * Created by PhpStorm.
 * Date: 30.03.19
 * Time: 16:34
 */

class DataHelper
{
    public function createArrayFromString($str)
    {
        $result = [];
        if ($str && is_string($str)){
            if (strpos($str, ',') !== false){
                $arr = explode(',', $str);
                $result = array_map(function ($val) {
                    return trim($val);
                }, $arr);
            }else{
                $result[] = trim($str);
            }
        }

        return $result;
    }
}