<?php
namespace Payex\Model;
/**
 * Created by PhpStorm.
 * Date: 29.03.19
 * Time: 0:37
 */

class Currency
{
    private $cID;

    private $country;

    private $currencyName;

    private $numCode;

    private $description;

    private $currencyType;

    private $currencyFor;

    /**
     * @return mixed
     */
    public function getCID()
    {
        return $this->cID;
    }

    /**
     * @param mixed $cID
     */
    public function setCID($cID)
    {
        $this->cID = $cID;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    /**
     * @param mixed $currencyName
     */
    public function setCurrencyName($currencyName)
    {
        $this->currencyName = $currencyName;
    }

    /**
     * @return mixed
     */
    public function getNumCode()
    {
        return $this->numCode;
    }

    /**
     * @param mixed $numCode
     */
    public function setNumCode($numCode)
    {
        $this->numCode = $numCode;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCurrencyType()
    {
        return $this->currencyType;
    }

    /**
     * @param mixed $currencyType
     */
    public function setCurrencyType($currencyType)
    {
        $this->currencyType = $currencyType;
    }

    /**
     * @return mixed
     */
    public function getCurrencyFor()
    {
        return $this->currencyFor;
    }

    /**
     * @param mixed $currencyFor
     */
    public function setCurrencyFor($currencyFor)
    {
        $this->currencyFor = $currencyFor;
    }

}