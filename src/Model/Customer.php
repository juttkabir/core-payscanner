<?php
namespace Payex\Model;
/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 15:27
 */

class Customer
{
    const STATUS_ENABLE = 'Enable';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getId()
    {
        return $this->data['customerID'];
    }

    public function getFirstName()
    {
        return $this->data['firstName'];
    }

    public function getAccountName()
    {
        return $this->data['accountName'];
    }

    public function getPassword()
    {
        return $this->data['password'];
    }

    public function getEmail()
    {
        return $this->data['email'];
    }

    public function getAgentId()
    {
        return $this->data['agentID'];
    }

    public function getCustomerStatus()
    {
        return $this->data['customerStatus'];
    }

    public function getParentId()
    {
        return $this->data['parentID'];
    }

    public function getCardIssueCurrency()
    {
        return $this->data['card_issue_currency'];
    }

    public function getOnlineCustomerServices()
    {
        return $this->data['online_customer_services'];
    }
}