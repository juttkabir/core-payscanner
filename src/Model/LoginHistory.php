<?php
namespace Payex\Model;
/**
 * Created by PhpStorm.
 * Date: 23.03.19
 * Time: 18:50
 */

class LoginHistory
{
    private $loginTime;
    private $loginName;
    private $accessIp = '';
    private $loginType;
    private $userId = 0;
    private $custId = 0;

    /**
     * @return mixed
     */
    public function getLoginTime()
    {
        return $this->loginTime;
    }

    /**
     * @return mixed
     */
    public function getLoginName()
    {
        return $this->loginName;
    }

    /**
     * @return string
     */
    public function getAccessIp()
    {
        return $this->accessIp;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getCustId()
    {
        return $this->custId;
    }

    /**
     * @param mixed $loginTime
     */
    public function setLoginTime($loginTime)
    {
        $this->loginTime = $loginTime;
    }

    /**
     * @param mixed $loginName
     */
    public function setLoginName($loginName)
    {
        $this->loginName = $loginName;
    }

    /**
     * @param string $accessIp
     */
    public function setAccessIp($accessIp)
    {
        $this->accessIp = $accessIp;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param int $customerId
     */
    public function setCustId($customerId)
    {
        $this->custId = $customerId;
    }

}