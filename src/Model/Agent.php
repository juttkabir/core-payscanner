<?php
namespace Payex\Model;
/**
 * Created by PhpStorm.
 * Date: 17.03.19
 * Time: 17:26
 */

class Agent
{
    const ADMIN_TYPE_AGENT = 'Agent';
    const AGENT_TYPE_SUB = 'Sub';
    const AGENT_TYPE_SUPPER = 'Supper';
    /**
     * @var array
     */
    protected $data;

    /**
     * Agent constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getId()
    {
        return $this->data['userID'];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->data['name'];
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->data['agentCompany'];
    }

    /**
     * @return mixed
     */
    public function getAdminType()
    {
        return $this->data['adminType'];
    }

    /**
     * @return mixed
     */
    public function getAgentType()
    {
        return $this->data['agentType'];
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->data['email'];
    }

    /**
     * @return mixed
     */
    public function getLogoPath()
    {
        $logoUrl = '';
        if ($this->data['logo']){
            $logoUrl = [
                'fullPath' => $_SERVER['DOCUMENT_ROOT'] . '/logos/' . $this->data['logo'],
                'relativePath' => '../logos/' . $this->data['logo'],
                'fullOnlinePath' => $_SERVER['DOCUMENT_ROOT'] . '/images/logos/' . $this->data['logo'],
                'relativeOnlinePath' => './images/logos/' . $this->data['logo']
            ];
        }
        return $logoUrl;
    }

    /**
     * @return mixed
     */
    public function getAgentUrl()
    {
        return $this->data['agentURL'];
    }

    /**
     * @return mixed
     */
    public function getAgentOnlineUrl()
    {
        return $this->data['spi_online_module'];
    }

    /**
     * @return mixed
     */
    public function getAgentCompanyRegNumber()
    {
        return $this->data['agentCompRegNumber'];
    }

    /**
     * @return mixed
     */
    public function getAgentPhoneNumber()
    {
        return $this->data['agentPhone'];
    }

    /**
     * @return mixed
     */
    public function getAgentAddress()
    {
        return $this->data['agentAddress'];
    }

    /**
     * @return mixed
     */
    public function getIDACountry()
    {
        return $this->data['IDAcountry'];
    }

    /**
     * @return mixed
     */
    public function getCustCountries()
    {
        return $this->data['custCountries'];
    }
}