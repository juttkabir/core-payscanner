<?php
namespace Payex\Database;
/**
 * Created by Soloviov Denys.
 * Date: 08.03.19
 * Time: 22:39
 */

interface ConnectionInterface
{
    /**
     * @param $host
     * @param $dbUser
     * @param $dbPassword
     * @param $dbName
     * @return mixed
     */
    public function setCredentials($host, $dbUser, $dbPassword, $dbName);

    /**
     * @return mixed
     */
    public function createConnection();
}