<?php
namespace Payex\Database;
/**
 * Created by Soloviov Denys.
 * Date: 08.03.19
 * Time: 22:44
 */

class Connection implements ConnectionInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $dbUser;

    /**
     * @var string
     */
    private $dbPassword;

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var Connection
     */
    private static $instance;

    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * Connection constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (empty(self::$instance)){
            self::$instance = new Connection();
        }
        return self::$instance;
    }

    /**
     * @param $host
     * @param $dbUser
     * @param $dbPassword
     * @param $dbName
     * @return mixed|void
     */
    public function setCredentials($host, $dbUser, $dbPassword, $dbName)
    {
        $this->host = $host;
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPassword;
        $this->dbName = $dbName;
    }

    /**
     *
     * @throws \Exception
     */
    public function createConnection()
    {
        try {
            $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName . ';port=3306';
            $this->dbh = new \PDO($dsn, $this->dbUser, $this->dbPassword);
        } catch (\Exception $e) {
            throw new \Exception('Database Connection error: ' . $e->getMessage());
        }
    }

    /**
     * Unse database descriptor
     */
    public function closeConnection()
    {
        $this->dbh = null;
    }

    /**
     * @param $sqlQuery
     * @return bool|mixed
     */
    public function select($sqlQuery)
    {
        if ($sqlQuery){
            $sth = $this->dbh->prepare($sqlQuery);
            $sth->execute();
            $result = $sth->fetch(\PDO::FETCH_ASSOC);
            return $result ? $result : 0;
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return array|bool
     */
    public function selectAll($sqlQuery)
    {
        if ($sqlQuery){
            $sth = $this->dbh->prepare($sqlQuery);
            $sth->execute();
            $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            return $result ? $result : [];
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return bool
     */
    public function insert($sqlQuery)
    {
        if (!empty($sqlQuery)){
            $sth = $this->dbh->prepare($sqlQuery);
            $result = $sth->execute();
            $lastId = $this->dbh->lastInsertId();
            return $result ? $lastId : 0;
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return bool
     */
    public function delete($sqlQuery)
    {
        if (!empty($sqlQuery)){
            $sth = $this->dbh->prepare($sqlQuery);
            return $sth->execute() ? true : 0;
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return bool
     */
    public function isExist($sqlQuery)
    {
        if (!empty($sqlQuery)){
            $sth = $this->dbh->prepare($sqlQuery);
            $sth->execute();
            $result = $sth->fetch(\PDO::FETCH_ASSOC);
            return $result ? $result : 0;
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return bool|int|mixed
     */
    public function count($sqlQuery){
        if (!empty($sqlQuery)){
            $sth = $this->dbh->prepare($sqlQuery);
            $sth->execute();
            $result = $sth->fetchColumn();
            return $result ? $result : 0;
        }else{
            return false;
        }
    }

    /**
     * @param $sqlQuery
     * @return bool|int
     */
    public function update($sqlQuery)
    {
        if (!empty($sqlQuery)){
            $sth = $this->dbh->prepare($sqlQuery);
            $sth->execute();
            return $sth->execute() ? true : 0;
        }else{
            return false;
        }
    }

    /**
     * @return \PDO
     */
    public function getDbHandle()
    {
        return $this->dbh;
    }
}