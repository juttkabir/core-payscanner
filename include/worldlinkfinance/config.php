<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","worldlinkfinance");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/worldlinkfinance/logo.gif");
define("CONFIG_LOGO_WIDTH","150");
define("CONFIG_LOGO_HEIGHT","96");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");


/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");



/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I confirm that the source of funds for purposes of money transmission is legitimate and has no predicate offence motives.");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");



/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");



/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "4");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","1");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","WL");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","World Link Code");
define("COMPANY_NAME","World Link Finance");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "noreply@worldlinkfinance.co.uk");
define("INQUIRY_EMAIL", "enquiries@worldlinkfinance.co.uk");
define("CC_EMAIL", "info@worldlinkfinance.co.uk");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Company Support");


define("CONFIG_INVOICE_FOOTER", " 0800 433 133");
define("COMPANY_URL","www.worldlinkfinance.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- World Link");
define("TITLE_ADMIN", "Payex- World Link");


define("COMPANY_ADDRESS","4 Waldeck Road, Lution, LU1 1HG");
define("COMPANY_PHONE", "Tel: 0800 433 133 Fax: 01582 400992");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","1");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","1");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT","print-ex-confirmed-transaction.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");

/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
********************/
define("CONFIG_TELLER_ACCESS","0");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
********************/
define("CONFIG_POST_PAID","0");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");
//define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");


/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is
* CONFIG_COMPLIANCE_CUSTOM_COLUMN,
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "600");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "600");

/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","0");

/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "0");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,");
define("CONFIG_TRANS_ROUND_CURRENCY", "GHS,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

/********************
* Switch to Enable/Disable import bulk Transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");
/********************   
* Added by Niaz Ahmad against ticket #2202 at 19-10-2007                                                                      
* Show Global balance 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_GLOBAL_BALANCE", "1");


/******************** 
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");
/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
* 0 Disable
* 1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Admin Manager,Branch Manager,Support,SUPI Manager,MLRO,Call,COLLECTOR,");

/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1");
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php");
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");

/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

/********************
Added by Niaz Ahmad #2618 at 27-11-2007
* Switch to Enable/Disable compulsary fields and additional columns in Exchange Rate Earning Report
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPULSARY_FIELDS_COLUMNS", "1");


/********************
* Switch to Enable/Disable transaction Commission for multiple currencies
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FEE_CURRENCY", "1");




/********************
* Switch to Enable/Disable transaction Commission for multiple currencies
*       0 Disable
*       1 Enable
*define("CONFIG_CUSTOM_BANK_CHARGE_CURRENCY", "");its value should be either FROM or TO
*
********************/
define("CONFIG_CUSTOM_BANK_CHARGE", "1");
define("CONFIG_CUSTOM_BANK_CHARGE_CURRENCY", "FROM");
define("CONFIG_DEFAULT_PAYMENT_MODE","1");

/********************
* Added by Niaz Ahmad this work based on Hassan sb email "World Link Minutes of the Meeting" at 22-02-2008
* Ticket #2967
* To Remove Admin (bank) charges  
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REMOVE_ADMIN_CHARGES","0");
define("CONFIG_DELETE_BANK_CHARGES","1");

/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");
/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************    
* Added By Niaz Ahmad #2918 at 06-02-2008                                                                     
* Switch to Enable/Disable calculation of cumulative amount for specific period
* When Config ON calneder will be shown at Compliance Configurations At Create Transaction
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_COMPLIANCE_NUM_OF_DAYS", "1");
/********************    
* Added By Niaz Ahmad #2967 at 08-03-2008                                                                     
* To hide Cumulative Total from Distributor Account Statement
*	0 Enable                                                                                   
*	1 Disable                                                                                   
********************/ 
define("CONFIG_DISTRIBUTOR_STATEMENT_CUMULATIVE", "1");


/******************** 
*Enabling user commission management 
*       0 means Disable 
*       1 means Enable  

*CONFIG_USER_COMMISSION_MANAGEMENT shows whether it is to be for other users like super agenrt/distributor or for sub distributor only
*CONFIG_COMMISSION_MANAGEMENT_NAME defines title of the link which is displayed in left menu, it part of that link name, not whole one. some where in beacon crest its value is 'Agent' making its link as 'Add Agent Commission'
*CONFIG_COMM_MANAGEMENT_FOR_ALL describes either it is for super agents only or for all users(Agent, Distributor, AnD), it is active when  CONFIG_USER_COMMISSION_MANAGEMENT is 1

********************/ 
define("CONFIG_USER_COMMISSION_MANAGEMENT", "1"); 
define("CONFIG_COMMISSION_MANAGEMENT_NAME", ""); 
define("CONFIG_COMM_MANAGEMENT_FOR_ALL", "1"); 


/********************    
* Added By Javeria #3156 at 16-05-2008                                                                     
* To show only Local amount on reciept page at distributor end
*	0 Enable                                                                                   
*	1 Disable                                                                                   
********************/ 

define("CONFIG_SHOW_ONLY_LOCAL_AMOUNT","1");


/********************    
* Added By Javeria #3156 at 17-05-2008                                                                     
* To show /hide bank charges on distributor reports
*	0 Enable                                                                                   
*	1 Disable                                                                                   
********************/ 

define("CONFIG_BANK_CHARGES_DISTRIBUTOR_REPORT","1");


define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","1");

define("DEST_CURR_IN_ACC_STMNTS","1");

/********************    
* Added By Javeria #3156 at 21-05-2008                                                                     
* To show /hide currency on distributor commision summary and detail report
*	0 Enable                                                                                   
*	1 Disable                                                                                   
********************/ 
define("CONFIG_SHOW_CURRENCY_DIST_COMM_REPORT", "1");

/********************
* Switch to Enable/Disable show list,by default, of transactions 
from distributor end on release page when clicked on 
the link "new transactions for delievery" in main menu
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SHOW_TRANS_LIST","1");


define("CONFIG_AGENT_OWN_GENERIC_RATE","0");

/********************
* Added By Javeria #3569 
* To show /hide all types of rates on today's rate menu for agents
* to enable this config, following configs are to be disbled
* CONFIG_AGENT_OWN_GENERIC_RATE 
* CONFIG_GENERIC_RATE_FOR_AGENT 
* CONFIG_GENERIC_RATE_FOR_AnD
* 1 Enable
* 0 Disable
********************/
define("CONFIG_ALL_TYPES_OF_RATE","1");


/********************
Added by Niaz Ahmad #2810 at 16-01-2008
* Enable/Disable settlement currency dropdown
* 0 Disable
* 1 Enable
********************/
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "1");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "0");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");

/********************
* Switch to Enable/Disable settlement commission on create transaction
* 0 - enable
* 1 - disable
* i think it is to be removed from system as this development seem to be
craped now.... just to confirm from javeria first...
********************/
define("CONFIG_SETTLEMENT_COMMISSION", "1");


/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");

define("CONFIG_AGENT_LAST_30_DAY_TRANSACTION","1");
define("CONFIG_SHOW_BALANCE","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Branch Money Paid In");
define("CONFIG_TIP_ENABLED","1");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

* 0 means Enable
* 1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "1");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "0");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");
define("CONFIG_SETTLEMENT_COMMISSION", "1");

//Ticket 3594
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "1");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "0");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");
define("CONFIG_SETTLEMENT_COMMISSION", "1");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

?>