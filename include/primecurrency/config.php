<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Prime Currency Exchange Ltd");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/primecurrency/logo.JPG");

define("CONFIG_LOGO_WIDTH","168");
define("CONFIG_LOGO_HEIGHT","100");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I accept that I have not used any illegal means to earn the money being sent. I accept that I am not sending this money for any illegal act. I also accept the full Terms & Conditions of Prime Currency Exchange. Prime Currency Exchange Ltd will not be responsible for any delay of payment.");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","1");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Payin Book Sender");
//define("CONFIG_PAYING_BOOK_AGENTS","100221,"); //for express
//define("CONFIG_CONTACT_NUMBER_SEARCH_PHONE_MOBILE","1");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "2");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","PC");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Manual Receipt Code");
define("COMPANY_NAME","Prime Currency");
define("COMPANY_NAME_EXTENSION","Prime Currency");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "primecurrency@hotmail.com");
define("INQUIRY_EMAIL", "primecurrency@hotmail.com");
define("CC_EMAIL", "");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "");


define("CONFIG_INVOICE_FOOTER", "T: 0207 488 9078 / 0207 680 7680");
define("COMPANY_URL","www.primecurrencyexchangeltd.com");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- Prime Currency");
define("TITLE_ADMIN", "Payex- Prime Currency");


define("COMPANY_ADDRESS","20-22 Hessel Street, London, E1 2LP");
define("COMPANY_PHONE", "Telephone: 0207 488 9078 / 0207 680 7680 � Fax: 0207 481 794 / 0207 702 0415");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","confirmTransactionPrimeCurrency.php");
define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction-PrimeCurrency.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","0");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","1");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","1");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","1");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)
* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "1");
define("CONFIG_PAYIN_RESTRICT_LIST", "SUPA,SUPAI");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "Admin, SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","1"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "Prime Currency");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");



/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");




/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
//define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,");
//define("CONFIG_TRANS_ROUND_CURRENCY", "GBP , COP");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_FROM, CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "1");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
/********************
* Switch to Enable/Disable an agent associated with payin book customers
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_AGENT_NUMBER", "100221");
 	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "0"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "0"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "GHS");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add  Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "0");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "1");



/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","0");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","0");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");


/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");



/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

/********************
* Switch to Enable/Disable show list,by default, of transactions 
from distributor end on release page when clicked on 
the link "new transactions for delievery" in main menu
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/


define("CONFIG_SHOW_TRANS_LIST","1");

define("CONFIG_FILING_NO","1");
define("CONFIG_PROFESSION_ADD_SENDER","1");



/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");

/********************
* Switch to Disable Accepted Terms during Add Sender
*	1 means Disable 
********************/
define("CONFIG_DISABLE_ACCEPTED_TERMS", "0");

/********************
* Switch to Enable/Disable Drop Down of "ID Type" rather than radio buttons
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPE_DROPDOWN", "1");

/********************
* Switch to Enable/Disable ID Expiry with Calendar (not drop down)
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "1");

/********************
* Switch to Enable/Disable Free text ID fields
* user has to input dates like DD-MM-YYYY
* CONFIG_IDEXPIRY_YEAR_DELTA is the maximum year (from now) that user inputs
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_FREETEXT_IDFIELDS", "0");
define("CONFIG_IDEXPIRY_YEAR_DELTA", "10");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "0");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,UK Driving License,Home Office ID");

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");
/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");


/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");

/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "1");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");


/********************
* Switch to Enable/Disable functionality of default Distribuotr
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DEFAULT_DISTRIBUTOR", "0");

/********************
* This switch is to enable/disable Internal Remarks in transactions
* Only the company will see it internally..... Not distributor side will be allowed to view it
* Made for Monex and Opal transfer
* 0 Disable Internal remarks 
* 1 Enable Internal remarks 
********************/
define("CONFIG_INTERNAL_REMARKS","1");
define("CONFIG_LABEL_INTERNAL_REMARKS","Internal Remarks for the Transaction");

/********************
* Switch to Enable/Disable Address Line 2 for customer and Beneficiary
* Especially done for Opal
* If value is '1' its Disabled
* If value is '0' its Enabled
********************/
define("CONFIG_DISABLE_ADDRESS2", "1");

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");


/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","0");

/********************
*Created by Khola Rasheed against ticket #3234 on june 3,2008
* Enable or disbale create transation form to show id details
* i.e. ID type, ID number, ID issue date and ID Expiry date when ID type is not mandatory. 
* In case if the fields have empty data the msg CONFIG_ID_DETAILS_MSG will be shown.
* NOTE: CONFIG_ID_MANDATORY should be off since in case it is on it also shows id details 
* on create transaction page i.e. ID type, ID number, Other ID type and other ID number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SHOW_ID_DETAILS","1");
define("CONFIG_ID_DETAILS_MSG","Not provided");

/********************
* Created By Jahangir Ticket# 3202
* Enable or disbale  Currency Dinomination Count stroed in database, 

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CURRENCY_DENOMINATION_ON_CONFIRM","1");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY, 
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example, 
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","1");
define("CONFIG_BEN_COUNTRY","PAKISTAN");


/********************
* Created By Khola against Ticket# 3270
* Enable or disbale sending of email on releasing a transaction 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EMAIL_ON_RELEASE","1");


/********************
* Switch to use optimise reference number search--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMIZE_REFERENCE_NO","1");
define("CONFIG_OPTIMISE_DATE","1");



/********************
* Switch to Enable/Disable Distributor's Reference number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIST_REF_NUMBER", "1");
define("CONFIG_DIST_REF_NAME", "Distributor's Reference Number");
define("CONFIG_GENERATE_DIST_REF_ON_EXPORT", "0");
define("CONFIG_DIST_MANUAL_REF_NUMBER", "1");
define("CONFIG_UPDATE_DISTRIBUTOR_REFERENCE","0");

define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX","0");

/************************
* Config added by Usman Ghani against ticket# 3235
* To allow specific type of users to edit customer/beneficiary.
*************************/
define("CONFIG_DISABLE_EDIT_SENDER", "1"); // 1 => disabled		and		2 => no disabled
define("CONFIG_USERS_ALLOWED_TO_EDIT_SENDER", "admin"); // (CSV) It will hold user types which are allowed to edit senders in case
														// CONFIG_DISABLE_EDIT_SENDER is set to 1.
define("CONFIG_DISABLE_EDIT_BENEFICIARY", "1"); // 1 => disabled		and		2 => no disabled
define("CONFIG_USERS_ALLOWED_TO_EDIT_BENEFICIARY", "admin"); // (CSV) It will hold user types which are allowed to edit senders in case
															 // CONFIG_DISABLE_EDIT_BENEFICIARY is set to 1.
 
/******************** 
* created by Khola against ticket # 3264 on 10-06-2008 
* Switch to Enable/Disable opal daily cash report 
* If value is '0' its Disabled 
* If value is '1' its Enabled 
********************/ 
define("CONFIG_DAILY_CASH_REPORT","1"); 
/*Custom Currency Denomination Menu Caption*/ 
define("CONFIG_REPORT_CAPTION","Daily Cash Report"); 
 
/*Daily cash report to show agent's transaction amount*/ 
define("CONFIG_DAILY_CASH_FOR_AGENT","1"); 

/**
 * Introiducing the new column
 * @Ticket# 3256
 * @addedBy	jahangir
 */
define("CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT","1");


/***************
set focus ID for prime currency


****************/
define("CONFIG_SET_FOCUS_ID","1");
define("CONFIG_SET_FOCUS_ID_VALUE","transAmountID");

/***************
CONFIG  for transactions that are based on money paid, 
By cheque.if a transaction is by cheque type it will go to 
"un clear cheque payments" before going to pending status
done by javeria ticket # 3265
* If value is '0' its Disabled 
* If value is '1' its Enabled 
****************/
define("CONFIG_CHEQUE_BASED_TRANSACTIONS","1");

/********************
* Created By Khola against Ticket# 3381
* Enable or disbale import-sender-pc page which imports sender number from import file 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_IMPORT_PAGE_FOR_PC","1");

/**
 * config to hide/display Distributor Alias column in Distributor Locator Report
 * true  = Show
 * false = Hide
 */
define("CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN", false);

/**
 * To show/hide customized agent account statement for prime currency in user-functionality.php file.
 * Ticket #3642
 * 1 = On
 * 0 = Off
 */
define("CONFIG_CUSTOM_AGENT_ACCOUNT_STATEMENT", 1);


/**
 * To show "Part Cheque Part Cash" functionality on add transaction page.
 * Ticket #3642: Prime Currency - Agent Account Balance Modification
 * 1 = On
 * 0 = Off
 */
define("CONFIG_PART_CASH_PART_CHEQUE_TRANS", 1);



define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "SUPA,SUPAI,Admin,admin,");
define("CONFIG_AGENT_OWN_RATE", "1");

define("CONFIG_USE_EXTENDED_TRANSACTION", 1);

/**
 * Show the total of all the pending transactions 
 */
define("CONFIG_TOTAL_PENDING_TRANSACTIONS","1");

/**
 * Enables/disables mode of payment on agent account
 *
 * true = Enable
 * false = Disable
 */
define("CONFIG_ENABLE_PAYMENT_MODE_ON_AGENT_ACCOUNT", true);

/**
 * Includes/excludes uncleared cheques in the count of Banking Total column
 * in Agent Account Statement (New) Report
 *
 * true = Include
 * false = Exclude
 */
define("CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT", true);

/**
 * Includes/excludes uncleared cheques in the count of Today's total of mixed and cheque
 * in Agent Account Statement (New) Report
 *
 * true = Include
 * false = Exclude
 */
define("CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT_TODAY_TOTAL", true);

/**
 * Includes/excludes cheques in the count of Mixed Banking column
 * in Agent Account Statement (New) Report
 *
 * true = Include
 * false = Exclude
 */
define("CONFIG_INCLUDE_CHEQUES_IN_MIXED_ON_AGENT_ACCOUNT", false);

/**
 * Shows/Hides cheque number, status fields on agent account statement.
 *
 * true = Shows
 * false = Hides
 */
define("CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT", true);

/**
 * Enables/Disables filter for search by cheque number, on agent account statement.
 *
 * true = Enables
 * false = Disables
 */
define("CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM", true);

/**
 * Enables/Disables - To identify the logos 
 *
 * true = Enables
 * false = Disables
 */
define("CONFIG_PERSONAL_LOGO","../uploads/logos");

/**
 * Displays/Hides moneyPaid "By Cheque" option on add-transaction.php 
 *
 * true = Displays
 * false = Hides
 */
define("CONFIG_DISPLAY_MONEY_PAID_BY_CHEQUE", false);

/**
 * Saves received amount in database. This configuration is being used for 2 clients.
 * For GE: It saves received amount in database - reference Ticket# 3547
 * For PC: It saves received amount in case of payment made in parts (cheque and cash). However,
 *         this isn't a true case, it was an initial requirement, but wasn't being used in PC based on this config.
 */
define("CONFIG_SAVE_RECIEVED_AMOUNT", 1);

/**
 * Includes/excludes uncleared mixed banking in the count of Cheques To Be Posted column
 * in Agent Account Statement (New) Report
 *
 * true = Include
 * false = Exclude
 */
define("CONFIG_INCLUDE_UNCLEARED_MIXED_IN_CHEQUES_POSTED_ON_AGENT_ACCOUNT", false);

/**
 * Includes/excludes cleared mixed banking in the count of Mixed Banking column
 * in Agent Account Statement (New) Report
 *
 * true = Include
 * false = Exclude
 */
define("CONFIG_INCLUDE_CLEARED_MIXED_ON_AGENT_ACCOUNT", false);

define("CONFIG_AGENT_OWN_GENERIC_RATE", "1");


/**For PC customised report
 * Excludes cheque and mixed deposits from banking total column on agent account balance for PC
 * true = Excludes
 * false = Includes
 */
define("CONFIG_EXCLUDE_MIXED_AND_CHEQUE_ON_AGENT_ACCOUNT", true);

define("CONFIG_PROFESSION_FIELD_LABEL","Occupation");

/**
 * Displays links on main menu for New and Old Transaction Export (Distributor Output File).
 * This config is for PrimeCurrency Only. Other clients have their own configs.
 * 1 = Display Links
 * 0 = Hide Links
 */
define("CONFIG_LINK_TO_DISTRIBUTOR_OUTPUT_FOR_PRIME_CURRENCY", "1");

/**
 * Defines a comma separated list of distributors (IDs) to display links for.
 * This config is based on CONFIG_LINK_TO_DISTRIBUTOR_OUTPUT_FOR_PRIME_CURRENCY and works only if
 * the said config is set to 1.
 *
 * NOTE: List of IDs should be comma separated and each item should containt User ID.
 */
define("CONFIG_DISTRIBUTORS_LIST_FOR_OUTPUT", "100661,100659");

/**
 * Include the distributor IDs(should be comma separated) for which 
 * distributor refrence number should include '-' (DASH) within it.
 * @Ticket #4108
 */
define("CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO","100659");

/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", 1);

/** 
 * Manage Upload document Categories 
 * @Ticket #3594
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

/**
 * These configs have been added by Omair Anwer to remove the alert which comes up
 * when an agent tries to put in a manual exchnage rate greater then his own exchange 
 * rate. This work was added against the ticket # 4820
 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE",1);
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","PAKISTAN,");
define("CONFIG_UNLIMITED_EX_RATE_OPTION",1);

/**
 *  This work was done to add the S/O field on to the beneficiary page. This work was done
 *  against the ticket # 4821
 */
define("CONFIG_SO_BEN",1);
/*
Added by Niaz Ahmad at 16-04-2009 @4821
*/
define("CONFIG_SON_BEN_RENAME_FAMILY_RELATION","1");

define("CONFIG_EXPORT_TRANSACTION_CUSTOMIZE","1");

/**
 * Upload documents link for Agent
 * @Ticket #4961
 */
define("CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT", "1");
/**
 * Upload documents link for Sub Agent
 * @Ticket #4961
 * point #4 
 * by Aslam Shahid
 */
 define("CONFIG_UPLOAD_DOCUMENT_ADD_SUB_AGENT","1");
/**
 * Collection Point ID title can be changed
 * @Ticket #4961 point-5
 * by Aslam Shahid
 * Disable = "0"
 * Enable  = "Branch Code" (i.e. title to be displayed)
 */
 define("CONFIG_COLLECTION_LOCATION_ID_TITLE","Branch Code");
 
 define("CONFIG_SEARCH_EXISTING_DATA_CUSTOMIZE","1");

/********************
* Commission based on transaction type.
* CONFIG_FEE_BASED_DROPDOWN is used on add commission page and CONFIG_FEE_BASED_TRANSTYPE is used on create transaction 
* and Add commission page.
* Ticket #4961
* 0 Disable
* 1 Enable
********************/
define("CONFIG_FEE_BASED_DROPDOWN","1");
define("CONFIG_FEE_BASED_TRANSTYPE","1");

/**
 * If config is '1'  then Notes field will be displayed All user types.
 * If config is  '0' then will not display for any.
 * if config has user_types then will show for them only. i.e. SUPA,SUPAI,admin, Branch Manager, Admin Manager,Admin,Call,
 * @Ticket #4961
 */
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES", "1");

/**
 * View senders / customer docuements at create transaction
 * @Ticket# 4961
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION",1);

/**
 * User Management :: ID Number field is displayed on Add/Update frome for Super/Sub agent/dist
 * according to which config is ON
 * @Ticket# 4961 Point #4
 * by Aslam Shahid
 */
define("CONFIG_SUP_AGENT_DIST_ID_NUMBER","1"); // Super Agent/Dist
define("CONFIG_SUB_AGENT_DIST_ID_NUMBER","1"); // Sub Agent/Dist
define("CONFIG_VIEW_UPLOAD_DOCUMENT_SUPPER_AGENT","1");
/**
 * Transaction :: Amount button comes next to Local Amoun field and Local Amount
 * button comes next to Amount Field.
 * @Ticket# 4961 Point #16
 * by Aslam Shahid
 */
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT_BUTTONS_REVERSE","1");

/**
 * Transaction :: Alert is shown on Negatinve change amount.
 * @Ticket# 4961 Point # 1
 * by Aslam Shahid
 */
define("CONFIG_ALERT_OUTSTANDING_CHANGE_AMOUNT","1");

/**
 * Transaction :: Alert if SOF value is empty in selected Beneficiary.
 * @Ticket# 4961 Point # 14
 * by Aslam Shahid
 */
define("CONFIG_BEN_SOF_TYPE_COMP_TRANSACTION_TYPES","Pick up");
?>