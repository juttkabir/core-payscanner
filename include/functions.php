<?

function UploadFile($Bext,$Uext)
{

	if ($BankcsvFile=='none' && $UsercsvFile=='none')
	{
		$msg = "Please select CSV files.";
		header("Location: import-excel-files.php?msg=$msg");
	}
	$uploadDir = '../uploads/';
	$uploadFile = $uploadDir . $_FILES['BankcsvFile']['name'];
	$uploadFile2 = $uploadDir . $_FILES['UsercsvFile']['name'];

	if($uploadFile == "./uploads/")
	{
	 return $BankcsvFile;
	}
	if($uploadFile == "./uploads/")
	{
	 return $UsercsvFile;
	}

		if (($Bext == ".csv"  ) && ( $Uext == ".csv"  ))
		{
				//$nTime=time();
				//$nTime=$nTime-1000000000;						
				$arrName=explode(".",$uploadFile);
				$arrName2=explode(".",$uploadFile2);			
				$uploadFile=$uploadDir."Bank_Report".".".$arrName[sizeof($arrName1)-1];
				$uploadFile2=$uploadDir."Daily_Activity_report".".".$arrName[sizeof($arrName2)-1];
				$reUploadFile = $nTime.".".$arrName[sizeof($arrName)-1];
			
						
				if (move_uploaded_file($_FILES['BankcsvFile']['tmp_name'], $uploadFile) && move_uploaded_file($_FILES['UsercsvFile']['tmp_name'], $uploadFile2))
				{
					//print "File is valid, and was successfully uploaded. ";//print "Here's some more debugging info:\n";//print_r($_FILES);
				}
				else
				{
					print "Possible file upload attack!  Here's some debugging info:\n";
					print_r($_FILES);
				}	
				
				//return $reUploadFile;
		}
		else
		{
					$msg = "File format must be  .csv";
					header("Location: import-excel-files.php?msg=$msg");		
		}
}


function ibanCountries($country)
{
	$listCountry[0] = "Austria";
	$listCountry[1] = "Belgium";
	$listCountry[2] = "Denmark";
	$listCountry[3] = "Finland";
	$listCountry[4] = "France";
	$listCountry[5] = "Germany";
	$listCountry[6] = "Greece";
	$listCountry[7] = "Iceland";
	$listCountry[8] = "Ireland";
	$listCountry[9] = "Italy";
	$listCountry[10] = "Luxembourg";
	$listCountry[11] = "Netherlands";
	$listCountry[12] = "Norway";
	$listCountry[13] = "Poland";
	$listCountry[14] = "Portugal";
	$listCountry[15] = "Spain";
	$listCountry[16] = "Sweden";
	$listCountry[17] = "Switzerland";
	$listCountry[18] = "United Kingdom";
	
	if(in_array($country, $listCountry))
		return true;
	else
		return false;
}

function getAgentType()
{
	global $username;
	if($username != "")
	{
		$users = selectFrom("select parentID, agentType, adminType, isCorrespondent, isMain from " . TBL_ADMIN_USERS . " where username='$username'");
		if(trim($users["parentID"]) == 0 && trim($users["isMain"]) == "Y")
		{
			return "admin";
		}
		elseif(trim($users["adminType"])!="Agent" && trim($users["isMain"]) == "N")
		{
			return trim($users["adminType"]);
		}
		elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Supper")
		{
			switch ($users["isCorrespondent"])
			{
				case "Y":
				{
					return "SUPAI"; // super-agent-IDA
					break;
				}
				case "N":
				{
					return "SUPA"; // super-agent
					break;
				}
				case "ONLY":
				{
					return "SUPI"; // super--IDA
					break;
				}
			}
		}
		elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Sub")
		{
			switch ($users["isCorrespondent"])
			{
				case "Y":
				{
					return "SUBAI"; // super-agent-IDA
					break;
				}
				case "N":
				{
					return "SUBA"; // super-agent
					break;
				}
				case "ONLY":
				{
					return "SUBI"; // super--IDA
					break;
				}
			}
		}
	}
	else
	{
		return false;
	}
}

function getExchangeRate($custCountry, $benCountry)
{
	if($custCountry != "" && $benCountry != "")
	{
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'");
		$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry'");

		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			$rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
			}
			else
			{
				$rate =round(1 - (1*$exchangeRateTo["marginPercentage"]) /  100 , 4);			
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];
		
		return $data;
	}
}

function imFee($amount, $origCountry, $destCountry )
{
	if($amount > 0 && $origCountry != "" && destCountry)
	{
		$imFee = selectFrom("select Fee from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry'");
		if($imFee["Fee"] != "" && $imFee["Fee"] != 0)
		{
			return $imFee["Fee"];
		}
		else
		{
			return false;
		}
	}
}

function sendMail($To,$Subject,$Message,$Name, $From)
{
	//if(validEmail($To))
	//{
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $Name <$From>\r\n";
		@mail($To,$Subject,$Message,$headers);
	//}
}
// create promotional code of any length
function createCode($length=8){
	$pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$promoCode = "";
	for ($i=0; $i < $length; $i++){
		$c = substr($pool, (rand()%(strlen($pool))), 1);
		$promoCode .= $c;
	}
	return $promoCode;
}
function checkValues($variable){
//	$variable = strip_tags($variable);
	$variable = addslashes($variable);
	$variable = trim($variable);
	return $variable;
}
// Email address validity
function validEmail($email) {
  $valid = 0;
  if(ereg("([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)", $email)) {
    $valid = 1;
  }
  return ($valid);
}
// Random number generation
function MyRandom($min,$max) {
  srand(time());
  $random = (rand()%$max) + $min;
  return ($random);
}

// Generates 14 digit random Id based on time, IP address and pid
function genRandomId()
{
  srand((double)microtime()*1000000);
  $abcdef = rand(10000,32000);
  $ip = getenv ("REMOTE_ADDR");
  $ip = substr($ip, 0, 8);
  $ip = preg_replace("/\./","",$ip);
  srand($ip);
  $ghij = rand(1000,9999);
  $pid = getmypid();
  srand($pid);
  $kl = rand(10,99);
  $number = $abcdef.$ghij.$kl;
  return $number;
}

// return image co-ordinates after resizing to the wanted 
function Resize($x,$y,$targetx,$targety) {
  $ratio = $x/$y;
  if ($x > $y) {
    $newx = $targetx;
    $newy = $newx / $ratio;
    if ($newy > $targety) {
      $newy = $targety;
      $newx = $newy * $ratio;
    }
  }
  else {
    $newy = $targety;
    $newx = $newy * $ratio;
    if ($newx > $targetx) {
      $newx = $targetx;
      $newy = $newx / $ratio;
    }
  }
  return array($newx,$newy);
}

// Returnbs name of the month
function Num2Month($month) {
if ($month=="1")  { return ("Jan"); }
if ($month=="2")  { return ("Feb"); }
if ($month=="3")  { return ("Mar"); }
if ($month=="4")  { return ("Apr"); }
if ($month=="5")  { return ("May"); }
if ($month=="6")  { return ("Jun"); }
if ($month=="7")  { return ("Jul"); }
if ($month=="8")  { return ("Aug"); }
if ($month=="9")  { return ("Sep"); }
if ($month=="10") { return ("Oct"); }
if ($month=="11") { return ("Nov"); }
if ($month=="12") { return ("Dec"); }

}
// return false on failure and array of records on success
function SelectMultiRecords($Querry_Sql)
{   
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
	    $count = 0;
		$data = array();
		while ( $row = mysql_fetch_array($result)) 
		{
			$data[$count] = $row;
			$count++;
		}
			return $data;
	}
}

function selectFrom($Querry_Sql)
{ //echo DEBUG . $Querry_Sql;
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}


function checkExistence($fieldName,$fieldvalue,$table)
{
	
	$name = explode(",", $fieldName);
	$value = explode (",", $fieldvalue);
	
	$j = count($name);
	$query = "";
	
	for($i=0; $i< $j; $i++){
		
		if($query > 0)
		{
			$query .= " AND ";
			}
		$query .= $name[$i] ."=".  $value[$i];
		
		
		}
	return(isExist("select $fieldName from $table where $query"));
	
	}



function countRecords($Query)
{
        //monitor_sql($Query);
	
   	if((@$result = mysql_query ($Query))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Query);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check[0];
   		}
			return false;	
	}
/*
	$query="SELECT count(id) as cnt FROM " . TBL_MESSAGES . " where dest='$username'";
	$contents=selectFrom($query);*/
}

function isExist($Querry_Sql)
{//	echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}

function insertInto($Querry_Sql)
{	//echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}
function update($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
		echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;
   	}
}
function deleteFrom($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}

function mysql_message($Querry_Sql)
{
	$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>MySql Debugger:</font></strong><br>";
	$msg .= "Error in your Query: $Querry_Sql<BR>";
	$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
	$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
	echo $msg;
}

function fetchData($query)
{
	if((@$result = mysql_query ($query))==FALSE)
   	{
		return false;
	}   
	else
	{
		while ($row = mysql_fetch_array($result))
		{
			echo "<option value=$row[0]>$row[1]</option>\n\t";
		}
	}
}

function FillDateCombo($start, $end, $flag=0)
{
	if ($flag == 0){
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>$i</option>\n\t";
	} else {
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>". Num2Month($i)."</option>\n\t";
	}
}

function insertError($error) //insert error into session
{
	session_register("error");
	$_SESSION['error']  = $error;
}

function getIP() 
{
	if ($_SERVER) 
	{
		if ( $_SERVER["HTTP_X_FORWARDED_FOR"] ) 
		{
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} 
		elseif ( $_SERVER["HTTP_CLIENT_IP"] ) 
		{
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} 
		else 
		{
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} 
	else 
	{
 	if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) 
	{
		$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
	}
	elseif ( getenv( 'HTTP_CLIENT_IP' ) ) 
	{
		$realip = getenv( 'HTTP_CLIENT_IP' );
	} 
	else 
	{
		$realip = getenv( 'REMOTE_ADDR' );
	}
}
return $realip; 
}

function createSession($username, $is_admin='N')
{
        $ip=getIP();
		$mySession="";
		$mySession = md5(uniqid(rand()) . genRandomId());  // To Creat a unique SessionID
		setcookie ("Usercookie", $mySession, time()+7200,  '/');

        $now=date('Y-m-d H:i:s');        // Format of Session Start Date
		if (isExist("select username from ". TBL_SESSIONS . " where username='$username'"))
		{
			deleteFrom("delete from " . TBL_SESSIONS . " where username='$username'");
		}
		insertInto("INSERT INTO " . TBL_SESSIONS . "(session_id, username, session_time, ip) VALUES('$mySession','$username','$now', '$ip')");	

			$sessionTime = time();
			$_SESSION['sessionTime'] = $sessionTime;
			$_SESSION['is_admin'] = $is_admin;
			$loginContents = selectFrom("select * from " . TBL_ADMIN_USERS . " where username='$username'");
						
			session_register("loggedUserData");
			$_SESSION["loggedUserData"] = $loginContents;
			session_register("parentID");
			$_SESSION["parentID"] = $loginContents["userID"];
			session_register("changedPwd");
			$_SESSION["changedPwd"] = $loginContents["changedPwd"];
			session_register("isMain");
			$_SESSION["isMain"] = $loginContents["isMain"];
			session_register("rights");
			$_SESSION["rights"] = $loginContents["rights"];
			session_register("adminType");
			$_SESSION["adminType"] = $loginContents["adminType"];
			session_register("agentType");
			$_SESSION["agentType"] = $loginContents["agentType"];
		$_SESSION["sid"]= $mySession;
}

function loggedUser() // returns the valid logged username
{
	global $chPassDate;
	$sessionID=$_SESSION["sid"];
	$now=date('Y-m-d H:i:s');
	//echo "SID: " . $sessionID;
	//echo "SELECT username from ". TBL_SESSIONS ." where session_id='$sid'";
	$sql4session="SELECT username from ". TBL_SESSIONS ." where session_id='$sessionID'";
    if (!isExist($sql4session) || !isset($sessionID)){
		return false;
	}
	update("update " . TBL_SESSIONS . " set session_time='$now' where session_id='$sessionID'");	
    $content=selectFrom($sql4session);
	if ($_SESSION["is_admin"] == "Y"){
		$loginContents = selectFrom("select userID, changedPwd from ".TBL_ADMIN_USERS." where username='".$content["username"]."'");
		$chPassDate = date_diff($loginContents["changedPwd"], $now);
		session_register("parentID");
		$_SESSION["parentID"] = $loginContents["userID"];
	}
	// check that a valid session exists, and has not timed out
	//echo "sad: " . $_SESSION["sessionTime"];
	//exit;
	if (($_SESSION["sessionTime"] == '') or ((time()-$_SESSION["sessionTime"]) > 7200)) {
		//session_unset();
		//header("Location: index.php?PHPSESSID=$PHPSESSID");
		//exit;
	} else {
		// on success, update the session time
		$_SESSION["sessionTime"] = time();
		setcookie ("Usercookie", $_SESSION["sid"], time()+7200,  '/');
	}
    return $content["username"];
}

function dateFormat($date, $pgFlag=1)
{
	if ($pgFlag != 1)
		return date("M j, Y", strtotime($date));
	else
		return date("m/d/Y", strtotime($date));
}

function extension($PicFile)
{
        $Ext = strtolower(strrchr($PicFile,"."));
		//return $ext;
        if ($Ext ==".gif" || $Ext ==".jpg" || $Ext ==".jpeg" || $Ext ==".png")
        {
            return 1;
        }
        else
        {
			return 0;
        }
}

function adminPicture($pic_name, $size="s")
{
	$image="../thumbs/bucket" . substr($pic_name, 12,2) . "/s_" . $pic_name;									
	return "<img src='$image' border=0>";
}

function display_error_message($message="")
{
	//global $_GET["error"];
	if (isset($_GET['error']) && $_GET['error'] == 'Y' && $message=="")
	{
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br><?    } 
	else
	{ if ($message !=""){
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br>
<?
	}}
}

function email_check($email)
{
	if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		return false;
	}
	else 
	{
		return true;
	}
}

function redirect($url)
{
	global $PHPSESSID;
	if(strstr($url, '?'))
	{
		header("location: $url&PHPSESSID=$PHPSESSID");
	}
	else
	{
		header("location: $url?PHPSESSID=$PHPSESSID");
	}
	exit();
}

function cuttext($tring, $cuton) 
{
	$space=" ";
	if (!strstr($tring,$space)) 
	{
		/* No space is found in the whole string: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	}
	
	if (substr($tring,$cuton,1)==$space) 
	{
		/* 21st character is a space: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	} 
	else 
	{
		/* 21st Character is NOT a space: return to last space and cut there */
		while ($teller <= $cuton) 
		{
			if (substr($tring,$cuton-$teller,1)==$space) 
			{
				$tring=substr($tring,0,$cuton-$teller);
				break;
			}
		$teller++;
		}
	}
	return $tring;
}

/*
function date_diff($str_start, $str_end) 
{ 
	$str_start = strtotime($str_start); // The start date becomes a timestamp 
	$str_end = strtotime($str_end); // The end date becomes a timestamp 
	
	$nseconds = $str_end - $str_start; // Number of seconds between the two dates 
	$ndays = round($nseconds / 86400); // One day has 86400 seconds 
	return $ndays;
} 

*/
function upload_picture($picture, $width, $height, $targetDir="../uploads/"){
//	global $picture;
	list($usec, $sec) = explode(" ",microtime());
	$createdID = eregi_replace(".","_", $sec + $usec);
	echo $picture_name;
	if (extension($picture_name)==1){
		$ext = strrchr($picture_name,".");
		if (strtolower($ext) == ".gif"){
			$ext = ".png"; 
		}
		if (@is_uploaded_file($picture)){ 
			// to further hide image name we add random number
			$picID = $createdID;
			$catPic = $targetDir. "c_".$picID . $ext;
			$catImage = "c_".$picID . $ext;

			$original = $targetDir. "o_c_" . $picID . $ext;
			@move_uploaded_file($picture, $original);
			@chmod ($original, 0755);   
			$size = getimagesize($original);
			if ($size[2]==1) { // if GIF, convert to png and then load 
	        	exec ("/usr/bin/gif2png $original");
       			$pngoriginal = $targetDir. "o_". $picID. ".png";
       			$im_in = imagecreatefrompng($pngoriginal); 
			}
			if ($size[2]==2) // if JPEG 
				$im_in = @imagecreatefromjpeg($original);
			if ($size[2]==3) // if PNG 
				$im_in = @imagecreatefrompng($original); 

			@imagegammacorrect($im_in, 1.0, 1.4);
			// Category Pictue
			list ($catx,$caty) = Resize($size[0],$size[1],$width,$height);
			
			$catImage = imagecreatetruecolor($catx,$caty);
			@imagecopyresized($catImage,$im_in,0,0,0,0,$catx,$caty,$size[0],$size[1]);
			if ($size[2]==1) { // if GIF it is not png
				@imagepng($catImage,$catPic);
			}
			if ($size[2]==2) { // if JPEG 
				@imagejpeg($catImage,$catPic);
			}
			if ($size[2]==3) { // if PNG 
				@imagepng($catImage,$catPic);
			}
		} else return ERR9;
	} else return ERR4;
	return $catImage;
}

function displayFlag($country, $flgPath = "")
{
	global $flags;
	if($country != "" && $flags[strtoupper($country)]!= "")
	{
		return  "<img src='" . $flgPath . "images/flags/". $flags[strtoupper($country)].".gif' alt='$country' border=0 align='absmiddle'>";
	}
}

?>
