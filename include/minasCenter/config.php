<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","minascenter");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/minascenter/logo.JPG");
define("CONFIG_LOGO_WIDTH","200");
define("CONFIG_LOGO_HEIGHT","100");
define("CONFIG_OTHER_LOGO","../admin/images/minascenter/Logo CenterUnion.JPG");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","1");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I accept that I have not used any illegal means to earn the money being sent. I accept that I am not sending this money for any illegal act. I also accept the full Terms & Conditions of Minas Center / Center Union");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","1");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Payin Book Sender");
define("CONFIG_PAYING_BOOK_AGENTS","100221,"); //for express
//define("CONFIG_CONTACT_NUMBER_SEARCH_PHONE_MOBILE","1");
define("CONFIG_SENDER_NUMBER_GENERATOR", 1);

/* @Ticket #3792 */
//define("CONFIG_AUTO_PAYIN_NUMBER",'1');

/**
 * If this config is on than at add sender page, if sender is selected from the top than this sender will be saved against the sender
 * else if no sender is selected than default sender from CONFIG_PAYIN_AGENT_NUMBER will be assigned provided if payin book check box is 
 * selected from down.
 * else exception will be thorugh to select the agent
 * @Ticket #3790
 */
//define("CONFIG_USE_AGENT_AT_ADD_SENDER", "1");


/**
 * introducing the new const for handling the commission based on payment mode
 * If enabled then the commission will be based on the payment mode
 * @Ticket# 3319
 * @Author Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_ADD_PAYMENT_MODE_BASED_COMM","1");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","MC");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Minas Center Trans Code");
define("COMPANY_NAME","Minas Center / Center Union");
define("COMPANY_NAME_EXTENSION","Money Transfer");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "minascenter@centerunion.co.uk");
define("INQUIRY_EMAIL", "minascenter@centerunion.co.uk");
define("CC_EMAIL", "");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Center Union Support");


define("CONFIG_INVOICE_FOOTER", " 020 8965 6588 / 020 8965 0116");
define("COMPANY_URL","www.centerunion.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- Minas Center / Center Union");
define("TITLE_ADMIN", "Payex- Minas Center / Center Union");


define("COMPANY_ADDRESS","96 Tubbs Road, London, NW10 4SB");
define("COMPANY_PHONE", "Tel: 020 8965 6588 / 020 8965 0116");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");
define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","1");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","express-confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction-minasCenter.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","1");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","0");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)

* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "1");
define("CONFIG_PAYIN_RESTRICT_LIST", "SUPA,SUPAI");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Money In");
define("CONFIG_WITHDRAW","Money Out");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","1"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "express");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "0");




/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");



/**
 * Adding config to handle the bank details of the beneficiary
 * @Ticket# 3320
 * @Date 25th June, 08.
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_BEN_BANK_DETAILS","1");


/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,By Cash,By Cheque,");
define("CONFIG_TRANS_ROUND_CURRENCY", "BRL,GBP,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");
/******
* #5211 - Minas Center
* CONFIG_TRANS_ROUND_NUMBER is parent.
* if CONFIG_ROUND_NUM_FOR is defined like define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,By Cash,By Cheque,")
* then control should not enter into this block as disturbs trans amount and converts back to decimals.
* This work is done because it came from OPAL (there define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,")) from ticket #1772
* 0-  Disable
* 1-  Enable
* by Aslam Shahid
******/
define("CONFIG_BY_CASH_ROUND_NUM_FOR", "1");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
/********************
* Switch to Enable/Disable an agent associated with payin book customers
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_AGENT_NUMBER", "100221");
 	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "0"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "0"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "GHS");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "0");


/**
 * Adding a const to handle the appearance  of distributor at the add transaction case
 * Provideed if the the transfer type is bank selected
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 * @Ticket# 3321 / 3320
 */
define("CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS","1");


/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","0");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");


/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
//define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");



/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "admin,Branch Manager,Admin Manager,Admin,Call,");

/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");


/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "1");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Full UK Driving License");

/********************

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");
/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");


/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "1");


/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "0");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "0");

/**
 * Select the default Money Paid option to select One 
 * @Ticket# 3517
 * @AddedBy Jahangir
 */

define("CONFIG_MONEY_PAID_SELECT_ONE",1);

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

define("CONFIG_CPF_ENABLED","1");

define("CONFIG_ACCOUNT_TYPE_ENABLED","1");
define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");

/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");



/********************
* Switch to use optimise search for agent on view transactions page--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMISE_AGENT_SEARCH","1");



/********************
* Switch to use optimise reference number search--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMIZE_REFERENCE_NO","1");



define ("CONFIG_VIEW_CURRENCY_LABLES","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

/**
 * Adding a new field containg the client refrence number
 * @ticket 3321 / 3320
 */
define("CONFIG_CLIENT_REF", "1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
* 1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");

/********************
* Added By Khola Rasheed against ticket #3318: Minas Center - Bank Transfer Types
* Switch to Enable/Disable 24 hour banking module which addes 24 hour banking exchange rate 
* and create transaction has banking info whether normal or 24 hr
* 0 Disable
* 1 Enable
********************/
define("CONFIG_24HR_BANKING", "1");

/**
 * Setting the default transaction type to bank transfer
 * @Ticket# 3321 / 3320
 */
define("CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER","Bank Transfer");

/********************
* Added by Usman Ghani against #3321 minas center import sender
* To import a custom csv file.
* 0 Off
* 1 On
********************/
define("CONFIG_IMPORT_CUSTOM_SENDERS_DATA", 1);

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", 1);

/* Money paid as non mandatory*/
define("CONFIG_MONEY_PAID_NON_MANDATORY","0");

/**
 * To display custom list of banks in Bank Search Module.
 * @Ticket: #3538: Minas Center - Bank List Problem
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}
 */
define("CONFIG_CUSTOM_BANK_SEARCH_RESULTS", 1);


/**
 * To show a custom template file for minas center only.
 * @Ticket: #3526: Minas Center-Change the sample file of Import Beneficiary to the latest one.
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}.
 */
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE", 1);
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME", "sampleTempBen_minasCenter.xls");


/**************************************************************
* Added by Javeria against #3546:
add sending bank on create transaction page depending on money paid's value
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_ADD_SENDING_BANK_ON_CREATE_TRANSACTION", 1);


/**
 * Adding the bank based trasactions capability
 * this config will control the main.php entery against the bank based transactions
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT",1);


/**
 * View senders / customer docuements at create transaction
 * @Ticket# 3779
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION",1);

/** 
 * Manage Upload document Categories 
 * @Ticket #3917
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

/**
 * Sender refrence number at confirm transaction page
 * @Ticket #
 */
define("CONFIG_SHOW_REFRENCE_NO_AT_CONFIRM", 1);
/**
 * Displays sender bank details on reports.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_BANK_DETAILS","1");

/* added and enabled this config against ticket #3939 */
define("CONFIG_DUAL_ENTRY_IN_LEDGER","1");

define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "SUPA,SUPAI,admin, Branch Manager, Admin Manager,Admin,Call,");

/* unlimited exchange rate ticket #4136 */
define("CONFIG_UNLIMITED_EX_RATE_OPTION",'1');

/*user can enter unlimited manual exchange rate  #4553 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE",'1');
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","BRAZIL,");

/**
 * Agent Rate can be defined from the system 
 * @Ticket #4355
 */
define("CONFIG_AGENT_OWN_RATE","1");

/**
 * To retains the remarks about a customer/sender
 * at its creation and display at the add transaction
 * @Ticket #4230
 */
define("CONFIG_REMARKS_ABOUT_SENDER","1");

/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_TRANSFER_BY_BANK","1");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_REFERENCE_NUMBER","1");

//Ticket#4362 to display Money Paid drop down above on calculation fields.
//Just need to assign "TOP" in config if want to show above calculation fields.
define("CONFIG_MONEY_PAID_LOCATION","TOP");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * CURRENCY EXCHANGE MODULE
 * This config affects whole module.
 * if not enabled then you can't access page directly into url.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE","1");

/* @470
 Functionality to Enable/Disable Beneficiaries from Search Beneficiaries.
 For certain types of admins i.e admin,Supper,Admin Manager,Agent,Support,SUPI Manager,Call,Branch Manager,MLRO,Admin(Basic User)
 Also If Value is assigned as followed.
 0 = Disabled for all user groups
 1 = Enabled for all user Groups.
 OR give values like admin (super admin), Call(call center staff) comma separated...
 
 Also if CONFIG_SHOW_ENABLE_DISABLE_BENS is on or has certain User group assigned. They will not see Disabled Beneficiaries 
 for any Customer on Create Transaction.
*/
define("CONFIG_ENABLE_DISABLE_BENS","1"); // to give Enable/Disable beneficiary
define("CONFIG_SHOW_ENABLE_DISABLE_BENS","1"); // to show on create transaction.

/*#4580
	If transaction selected has Pending status then dont show Paid status in DropDown
	in Cheque Cahing Module.
	by ashahid
*/
define("CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_PENDING_CQ","1");

/*#5027
	Paid status is disabled on Cheque cashing Module report
	CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_PENDING_CQ will only be applicable
	for Pending status in Cheque Cahing Module. you may switch that off and use this this config.
	Enable : (for status in comma-separated list in Config.)
	"NG" => "Pending"
	"AR" => "Clear"
	"ID" => "Paid"
	"LD" => "Hold"
	"ST" => "Cancel Request"
	"EL" => "Cancel"
	"RN" => "Return"
	Disable : (assgin "0")
	by Aslam Shahid
	
*/
define("CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_STATUSES_CQ","LD,ST,EL,RN");

/*#4597
	On creating cheque cashing order estimated date was including saturdays and sundays from issue date after adding
 	no of days defined in database.Now holidays(saturday & sunday) are not dealt in no of days.
	by ashahid
*/
define("CONFIG_ADD_HOLIDAYS_IN_ESTIMATED_DATE_CQ","1");


/*#4594
	This functionality works if CONFIG_SHOW_ENABLE_DISABLE_BENS and CONFIG_SHOW_ENABLE_DISABLE_BENS is on
	If this config is on then Disabled beneficiaries will be displayed(i.e in red) but not selected
	on create transaction.
	by ashahid
*/
define("CONFIG_SHOW_ENABLE_DISABLE_BENS_COLORED","1");

/**
* #4802 
 * This config affects whole module on Search Sender form. 
 * To use Exact search. 
 * '1' For enable  and Config CONFIG_CUST_WILDCARD_SRCH should be '0' 
 * '0' For disable 
 */ 
 define("CONFIG_EXACT_MATCH_CUST_SEARCH","1"); 
 
 /** 
 * #4802 
 * This config affects whole module on Search Benificiary form. 
 * To use Exact search. 
 * '1' For enable 
 * '0' For disable 
 */ 
 define("CONFIG_EXACT_MATCH_BEN_SEARCH","1"); 
   
/** 
 * #4898
 * Item-Wise reconciliation of Bank Payments
 * '1' For enable 
 * '0' For disable 
 */ 
define("CONFIG_UNRES_PAY_BARCLAY_FILE_AJAX","1");
//define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT","1");


/** 
 * #4595
 * Currency exchange :: now customer in creating currency exchange transaction
 * becomes non-mandatory for specified User groups in config.
 * Enable  : 1 or User types..(Admin,SUPA, etc..)
 * Disable : 0
 */ 
define("CONFIG_CUST_NON_MANDATORY_USERS_CURR","0");

/** 
 * #4748
 * Print Summary of Sender transaction from Last transaction link on
 * Create transaction page
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_PRINT_SENDER_TRANS_SUMMARY","1");

/** 
 * #4776
 * CPF is displayed on Transaction status page
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_DISPLAY_CPF_TRANS_STATUS","1");


/** 
 * #4595
 * Notes field is made mandatory on cheque order
 * for status Cancel Request.
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_NOTE_STATUS_MANDATORY_CHEQUE_CASHING","1");


/** 
 * #4566
 * Search by sender code in cheque cashing
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_SEARCH_SENDER_BY_CODE_CQ","1");

/**
 * To deposit into the agent account, with the currency options
 * This config shouls be enabled with "1"
 * @Ticket #4895
 */
define("CURRENCY_AT_ACCOUNT_DEPOSIT", "1");
/** 
 * #4693
 * Cancel currency exchange transaction functionality.
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_CANCEL_TRANS_CURR_EXCH","1");

define("CONFIG_CREATE_ADMIN_STAFF_LEDGERS", "1");
$_arrAdminStaffForCreateTransactionLedgers = array(
													"Admin", "Admin Manager",
													"CALL"
												);
/** 
 * #5178 - Point 4 of Document
 * Use Round number and give value of decimal places to 
 * be rounded.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 											
define("CONFIG_ROUND_NUMBER_ENABLED_CURR_EXCH", "1");
define("CONFIG_ROUND_NUMBER_TO_CURR_EXCH", "2"); // Number of rounded decimals

/** 
 * #5178 - Point 3.1 of Document
 * Use Round number and give value of decimal places to 
 * be rounded in Cheque Cashing.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 											
define("CONFIG_ROUND_NUMBER_ENABLED_CHEQ_CASH", "1");
define("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH", "2"); // Number of rounded decimals
define("CONFIG_ROUND_FEE_ENABLED_CHEQ_CASH", "1");
define("CONFIG_ROUND_FEE_TO_CHEQ_CASH", "2"); // Number of rounded decimals

/** 
 * #5214 Minas Center
 * Beneficiary Agent details should not be shown
 * to basic users in any case.
 * Enable  : assign user groups (Admin,Call, etc...)
 * by Aslam Shahid
 */ 											
define("CONFIG_HIDE_DISTRIBUTOR_DETAILS_USERS", "Admin,");

/** 
 * #5213 Minas Center
 * Associated Beneficiaries to customer
 * given in dropdown can be searched.
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 											
define("CONFIG_SEARCH_CUST_ASSOCIATED_BEN", "1");
/** 
 * #5258 Minas Center
 * Shows Opening/Closing Balnaces on Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 											
define("CONFIG_ADMIN_STATEMENT_BALANCE", "1");
/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Currency Excange Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_ADMIN_LEDGER", "1");

/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Cheque Cashing Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_CASHING_ADMIN_LEDGER", "1");

/** 
 * #5255 Minas Center Point #7
 * Ledgers of all users are affected for User Denomination
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_USER_DENOMINATION_ADMIN_LEDGER", "1");

/** 
 * #5273 Minas Center Point
 * We can mention transaction stuses which we want to 
 * view inputter transactions.
 * Enable   : "1" for All transactions OR "Pending,Authorized" etc.. for specific.
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_SHOW_INPUTTER_TRANS_STATUS_ONLY", "1");
/** 
 * #5328 Minas Center
 * Inter-User Transfer labels changed.
 * if it is WITHDRAW then shown it as DEPOSIT (Cr.)
 * and vice versa.
 * (Originally DEPOSIT should be Dr. and WITHDRAW should be Cr.)
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_SENDER_DEPOSIT_RECEIVER_WITHDRAW", "1");
/** 
 * #5328 Minas Center
 * Showing labels as From OR To user for 
 * DEPOSIT OR WITHDRAW.
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_SHOW_DESCRIPTION_INTER_USER", "1");
/** 
 * #5367 Minas Center
 * Bank Transactions also added in User Denomination Report.
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_BANK_TRANS_USER_DENOMINATION", "1");
/** 
 * #5367 Minas Center
 * Cheque Transactions also added in User Denomination Report.
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_TRANS_USER_DENOMINATION", "1");
/** 
 * #5367 Minas Center
 * Total Calculated/Collected/OverShort amounts are
 * displayed in in 3 cards on user denomination report.
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CAL_COLL_OS_USER_DENOMINATION", "1");
/** 
 * #5367 Minas Center
 * Saving User Denomination Temporarily and it will be editable until 
 * saved permanently. Ledgers will be affected on saving permanently
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_SAVE_TEMP_USER_DENOMINATION", "1");
/** 
 * #5395 Minas Center
 * Hide total of Cash Transactions displayed on User Denomination Report
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_HIDE_CASH_TOTAL_USER_DENOMINATION", "1");

/** 
 * #5395 Minas Center
 * Show cheque created on user denomination report
 * Enable   : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_CREATED_USER_DENOMINATION", "1");
/**  
 * #5415 Minas Center 
 * Show search filter to users specified in config if defined 
 * else to super admin only .(Cash Book System)
 * Enable   : "ALL" for All users OR admin,Admin Manager,Admin, etc.. (Commas at end for User groups) 
 * Disable  : "0" 
 * by Aslam Shahid 
 */  
define("CONFIG_SHOW_FILTERS_CASH_BOOK_SYSTEM", "admin,Admin Manager,"); 


/**
 * #5420 This config is being turned on to enable the bank export links on the quick menu
 *  The config was turned off by niaz prior to release 1.74.1.
 *  The config is being inputted again by Omair Anwer
 **/

define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT","1");

/*	#5610 - Minas Centre
*	Member and Standard customer dropdown is added on form.
*	and M is prefixed to customerID for Membership customers.
*	by A.Shahid
*/
define("CONFIG_MEMBERSHIP_CUSTOMER_DROPDOWN","1");
?>