<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Pakistan Post Powered by Payex");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","images/pakpost/logo.gif");
define("CONFIG_LOGO_WIDTH","165");
define("CONFIG_LOGO_HEIGHT","40");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");


/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","1");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","0");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I confirm that the source of funds for purposes of money transmission is legitimate and has no predicate offence motives.");


/********************
* To show only the country where the services are set from Add/Update service. This will apply for adding beneficiary
* form and adding distributor form
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");



/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** 
* One branch manager can look into their agents transaction and manage them
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");



/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","1");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");


/********************
* If option is 1 then the Commission name will be used rather thne Fee. 
* If the option is 0 then the Fee will use by default
********************/
define("CONFIG_FEE_DEFINED","0");
define("CONFIG_FEE_NAME","Commission");

/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","0");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","0");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Walkin Customer");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "10");

/********************
* switch to generate auto payin number or not...
********************/
define("CONFIG_AUTO_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","PP");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Pak-Post Code");
define("COMPANY_NAME","Pakistan Post");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "noreply@horivert.co.uk");
define("INQUIRY_EMAIL", "enquiries@horivert.co.uk");
define("CC_EMAIL", "info@horivert.co.uk");
define("SUPPORT_NAME", "Pakistan Post");


define("CONFIG_INVOICE_FOOTER", "111 111 117");
define("COMPANY_URL","www.pakpost.gov.pk");




define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Pakistan Post");
define("TITLE_ADMIN", "Pakistan Post");


/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","PK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");


/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","0");


/********************
* Switch to Enable/Disable round function 
*	0 means Disable
*	1 means Enable
* it is used to convert float into integer
********************/
define("CONFIG_ROUND_NUMBER_ENABLED","0");

/********************
* Switch to Enable/Disable Bank Account Type 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_ACCOUNT_TYPE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","Province");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY, 
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example, 
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","0");

define("CONFIG_BEN_COUNTRY","BRAZIL");


/********************
* Switch to Enable/Disable Filteration of records in braclays  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("FILTER_BARCLAYS_RECORDS","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");


/********************
* Switch to Enable/Disable specific country of Canada while adding customer 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CANADIAN_COUNTRY_OPTION","0");

/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT","0");

/********************
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS","0");


/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");


/********************
* Switch to Enable/Disable to show Create Transaction Option for Teller
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TELLER_CREATE_TRANS","1");


/********************
* Switch to Enable/Disable Calculating Transaction Amounts from Total Amount
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CALCULATE_FROM_TOTAL","0");

/********************
* Switch to Enable/Disable Profit Earning Report Especialy for Bayba
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PROFIT_EARNING","0");

/********************
* Switch to Enable/Disable to show Country/Currency Filter
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CNTRY_CURR_FILTER", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable drop down of collection points instead of pop-up window
* during create transaction (if transaction type is 'Pick up')
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_COLLPOINTS_DROPDOWN", "0");

/********************
* Switch to Enable/Disable color scheme for header and footer
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUSTOM_COLOR_ENABLE", "1");
define("CONFIG_CUSTOM_COLOR1", "#E80026");
define("CONFIG_CUSTOM_COLOR2", "#2D2470");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");



/********************
* Switch to Enable/Disable Domestic Transaction System
* with the concept of No Exchange Rate and No Currency.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_DOMESTIC_TRNAS", "1");

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "0");

/********************
* Switch to Enable/Disable default Transaction Purpose
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Domestic Transactions
* with by default 1 exchange rate and no currency
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_ENABLE_DOMESTIC", "1");

?>
