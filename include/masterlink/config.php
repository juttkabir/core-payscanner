<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Master Link");
define("CLIENT","MasterLink");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/masterlink/logo.gif");
define("CONFIG_LOGO_WIDTH","157");
define("CONFIG_LOGO_HEIGHT","63");


/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","0");
define("UPDATE_TRANSACTIONS_AGENT","0");




/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");



/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");



/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/beaconcrest/Terms_and_conditions.htm");
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","MasterLink will pay the beneficiary set out above on production, by the beneficiary, of 
valid 
personal identification. MasterLink reserves the right to withhold any payment from any beneficiary who is unable to 
satisfy reasonable identification procedures. <br /> The rate of exchange established on the day on which the money transfer was received, will be the rate upon which the currency amount will be paid to the beneficiary.");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","1");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","1");



/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");



/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "1");



/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");


/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");

/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","1");

/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");


/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");




/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","1");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","B");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Master Link Code");
define("COMPANY_NAME","Master Link");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");




/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");



/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","1");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Company specific information to be used in the system
********************/

define("SUPPORT_EMAIL", "support@masterlink.com");
define("INQUIRY_EMAIL", "");
define("CC_EMAIL", "");
define("SUPPORT_NAME", "Master Link");



define("CONFIG_INVOICE_FOOTER", "+44(0)207000000");
define("COMPANY_URL","www.worldlink.com");
//define("PAYEX_URL","payex.global-link.co.uk");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","::.World Link.:: ....");
define("TITLE_ADMIN", "::...Payex- World Link Remittance System ....::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","1");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Receipt");
define("CONFIG_WITHDRAW","Payment");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");
/********************
* Quick Add Benificiary 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ELEMINATE_ADDRESS_QUICK_BENEFICIARY","0");

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable 6 digits(at last position) auto-generated transacation number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SIX_DIGITS_TRANS_NUM", "1") ;

/********************
* Switch to Enable/Disable Location ID Number column in Distributor Output File
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LOCIDNUM_ENABLE", "1");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
define("CONFIG_AMOUNT_CONTROLLER", "675");
define("CONFIG_AML_COLOR_SCHEME", "1");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Switch to Enable/Disable T&C at Beneficiary Reciept
* Specifically done for Beacon Crest
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BENEFICIARY_TERMS", "1") ;


/********************
* Switch to Enable/Disable distributor list on d'istributor output file'
* Specifically done for Beacon Crest
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DISTRIBUTOR_LIST", "1") ;


/********************                                                          
* to get client's name
********************/ 

define("CLIENT_NAME","worldlink");

/******************** 
*Enabling configure-distributor-commission.php commission page for beaconcrest 
*       0 means Disable 
*       1 means Enable  
********************/ 
define("CONFIG_PESHAWAR_REPORT", "1"); 

/******************** 
*Enabling user commission management 
*       0 means Disable 
*       1 means Enable  
********************/ 
define("CONFIG_USER_COMMISSION_MANAGEMENT", "1"); 
define("CONFIG_COMMISSION_MANAGEMENT_NAME", "Agent"); 


/******************** 
*Enable/Disable birth date and place of birth on add quick sender
*       0 means Disable 
*       1 means Enable  
********************/ 
define("CONFIG_CUST_BIRTH_DATE", "1"); 

/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************
* Switch to Enable/Disable id expiry with calendar
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "1");


/********************
* Switch to Enable/Disable Merchant Bank Export Report 
* Done Only for Beacon Crest
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_MERCHANT_FILE_BEACON", "1");

/********************
* Switch to Enable/Disable IDAcountry check for showing agent list when agent based fee type is selected on the add fee page. 
* Done Only for Beacon Crest
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IDACOUNTRY_CHECK", "1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*       0 Disable
*       1 Enable
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "0");



/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_LEVEL", "2");

/********************
* Switch to Enable/Disable Sender Address on distributor ouput file
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_SENDER_ADD_DIST_OUTPUT_FILE", "1");



/********************
* Switch to Enable/Disable ID card label on distributor ouput file
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_ID_TYPE_NATIONAL_ID", "1");



/********************
* Switch to Enable/Disable password field for beneficiary's ID type
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_IDTYPE_PASSWORD", "1");

/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");



/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");


/********************                                                                         
* Switch to Enable/Disable post code on quick sender
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_POST_CODE_QUICK_SENDER", "1");


/********************                                                                         
* Switch to Enable/Disable middle name on distributor ouput 1111
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MIDDLE_NAME_DOF", "1");



/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");

/********************
* Switch to Enable/Disable Drop Down of "ID Type" rather than radio buttons
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_IDTYPE_DROPDOWN", "1");


/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is 
* CONFIG_COMPLIANCE_CUSTOM_COLUMN, 
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable sender ID date alert
* 0 Disable
* 1 Enable
********************/
define("CONFIG_ALERT", "1");
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php");
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable non compulsory phone field
* 0 Disable
* 1 Enable
********************/
define("CONFIG_PHONE_NON_COMPULSORY", "1");

/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");

/********************
* To disable the button on click once on create transaction page. To
avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");

/********************
* Switch to Enable/Disable Documentation verification module
* 0 Disable
* 1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");
/********************
* Make Address line 1 mandatory
*	1 means Mandatory
* 0 means non mandatory
********************/
define("ADDRESS_LINE1_Mandatory", "1");

/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");
/********************
* Change Label on all receipts from Distributor to Paying Agent
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CHANGE_LABEL_RECEIPT", "1");

?>
