<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Opal Transfer");
define("CLIENT_NAME","opaltransfer");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/opaltransfer/logo.jpg");
define("CONFIG_LOGO_WIDTH","213");
define("CONFIG_LOGO_HEIGHT","97");
// These variables are used only in Opal Receipt pages to make dimensions of logo Bigger
define("RECEIPT_LOGO_WIDTH","300");
define("RECEIPT_LOGO_HEIGHT","150");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT", "1");
define("CONFIG_RECIEPT_NAME", "Receipt4Opal.php");
define("CONFIG_RECIEPT_PRINT", "print-ex-Receipt4Opal.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "Receipt4OpalBen.php");

define("CONFIG_CUSTOM_TRANSACTION", "1");
define("CONFIG_TRANSACTION_PAGE", "add-transaction_opaldesign.php");

/*
Citizenship Field on Add Beneficiary Form.
*/
define("CONFIG_CITIZENSHIP","0");
/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","0");
define("DISPLAY_TRANSACTIONS_LABEL","1");

/********************
* Switch to Enable/Disable Currency Denomination 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURR_DENOMINATION","1");

/*
Custom Currency Denomination Menu Caption
*/
define("CONFIG_CURR_CAPTION","Currency Denomination");



/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");



/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");



/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/glink/Terms_and_conditions.doc");
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","This Money Transfer is private financial support and it is not related to any 
commercial purposes.");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","External Remarks for the Transaction");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");



/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","1");


/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");



/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");



/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");


/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","0");

/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","0");

/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");


/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");




/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","OT");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Opal Transfer Code");
define("COMPANY_NAME","Opal Transfer Ltd.");

// Opal used this title in receipts.
define("COMPANY_INT_NAME", "International Money Transfers");

define("CONFIG_AGENT_STATMENT_GLINK","1");



/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");




/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");



/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","0");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Company specific information to be used in the system
********************/

define("SUPPORT_EMAIL", "noreply@opaltransfer.com");
define("INQUIRY_EMAIL", "payex@opaltransfer.com");
define("CC_EMAIL", "payex@opaltransfer.com");
define("SUPPORT_NAME", "Company Support");
define("COMPANY_ADDR","<br>43 Grosvenor Gardens<br>
London, SW1W 0BP. Tel: 0207 976 5445<br><br>
Mon-Sat 10:00-20:00 Sun 10:00-19:00
");


define("CONFIG_INVOICE_FOOTER", "+44(0)207 976 5445");
define("COMPANY_URL","www.opaltransfer.com");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","::.Opal Transfer.::");
define("TITLE_ADMIN", "::...Payex- Opal Transfer Ltd ...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","State/County");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","0");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","0");

/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");

/********************
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS", "1");

/********************
* Switch to Enable/Disable to manage export transactions a/c to opal
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_OPAL", "1");


/********************
* Switch to Enable/Disable to manage export transactions a/c to opal
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_OLD", "1");

/********************
* Through this variable, transactions exported will be for this country(receiving)
********************/
define("CONFIG_COUNTRY_EXPORT_TRANS", "Lithuania,Georgia");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","0");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","0");

/*
* Transaction refrence number format change
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_IMREF_FORMAT", "0");


/********************
* Switch to Enable/Disable to show Create Transaction Option for Teller
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TELLER_CREATE_TRANS","1");



/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
CONFIG_EXCHANGE_MONEY_PAID has to be disabled in order to enable this config
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","0");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,");
define("CONFIG_TRANS_ROUND_CURRENCY", "USD,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");
/********************
* Switch to Enable/Disable Defalut Sending Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY","0");
define("CONFIG_CURRENCY_NAME","USD");

/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "USD");

/********************
* Switch to Enable/Disable Filteration of records in braclays  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("FILTER_BARCLAYS_RECORDS","1");

/********************
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
*	0 means Disable
*	1 means Enable 
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;

/********************
* Switch to Enable/Disable other Title for "Middle Name"
* during add sender/beneficiary either quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUSTBEN_MIDNAME", "1");
define("CONFIG_CUSTBEN_MIDNAME_TITLE", "Patronyme");

/********************
* Switch to Disable Accepted Terms during Add Sender
*	1 means Disable 
********************/
define("CONFIG_DISABLE_ACCEPTED_TERMS", "1");

/********************
* Switch to Enable/Disable Drop Down of "ID Type" rather than radio buttons
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPE_DROPDOWN", "1");

/********************
* Switch to Enable/Disable ID Expiry with Calendar (not drop down)
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "0");

/********************
* Switch to Enable/Disable Free text ID fields
* user has to input dates like DD-MM-YYYY
* CONFIG_IDEXPIRY_YEAR_DELTA is the maximum year (from now) that user inputs
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_FREETEXT_IDFIELDS", "1");
define("CONFIG_IDEXPIRY_YEAR_DELTA", "20");

/********************
* Switch to Disable check box terms and conditions during create transaction
*	1 means Disable 
********************/
define("CONFIG_TnC_CHECKBOX_OFF", "1");

/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "1");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Driving License,UK Driving License,CIS Card,Home Office ID,Other ID");

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");

/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "1");


/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Admin Manager,");



/********************
* Switch to Enable/Disable 
* Option to switch off the tille in data entry forms
* By default it is not OFF
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TITLE_OFF", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is 
* CONFIG_COMPLIANCE_CUSTOM_COLUMN, 
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");


/********************
* Switch to Enable/Disable Search Beneficiary 
* and then ability to create transaction 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************
* Switch to Enable/Disable layout like 1- Last Name 2- First Name 3- Patronyme(Mid Name)
* during add/update sender/beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_NAME_SEQUENCE", "1");

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");


/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "1");

/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "1");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable mobile field OFF
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_MOBILE_FIELD_OFF", "1");

/********************
* Switch to Enable/Disable Calculating Transaction Amounts from Total Amount
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CALCULATE_FROM_TOTAL", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "1");

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");



/********************
* Switch to Enable/Disable to show Create Transaction Option for Teller
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TELLER_CREATE_TRANS","1");




// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");







/********************
* Switch to Enable/Disable default Transaction Purpose
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "0");

/********************
* Switch to Enable/Disable Swaping of Amount and Local Amount up and down
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT", "1");

/********************
* Switch to Enable/Disable Ref NumberIM either a/c to DDMM or auto
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUMIM_AC_TO_DDMM", "1");

/********************
* Switch to Enable/Disable Ref NumberIM either a/c to DDMMYY or auto
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUMIM_AC_TO_DDMMYY", "0");

/********************
* Switch to Enable/Disable not to update Ref NumberIM
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IMTRANSNUM_NOT_UPDATE", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");



/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Switch to Enable/Disable functionality of default Distribuotr
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DEFAULT_DISTRIBUTOR", "1");


/********************
* This switch is to enable/disable Internal Remarks in transactions
* Only the company will see it internally..... Not distributor side will be allowed to view it
* Made for Monex and Opal transfer
* 0 Disable Internal remarks 
* 1 Enable Internal remarks 
********************/
define("CONFIG_INTERNAL_REMARKS","1");
define("CONFIG_LABEL_INTERNAL_REMARKS","Internal Remarks for the Transaction");


/********************
* Switch to Enable/Disable Address Line 2 for customer and Beneficiary
* Especially done for Opal
* If value is '1' its Disabled
* If value is '0' its Enabled
********************/
define("CONFIG_DISABLE_ADDRESS2", "1");


/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/********************
* Switch to Enable or Disable to Skip Confirm Transaction Step
* while creating a transaction
* For Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_SKIP_CONFIRM","1");



/********************
* Switch to Enable/Disable Free text ID fields
* user has to input dates like DD-MM-YYYY
* CONFIG_IDEXPIRY_YEAR_DELTA is the maximum year (from now) that user inputs
* Expilictly made for opal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_FREETEXT_DATES", "1");



/********************
* Switch sender's search results to ascending or descending order
* For Opal
* 0-  Descending
* 1-  Ascending
********************/
define("CONFIG_SEARCH_RESULT_ORDER","1");


/********************
* Enable or disbale 'send to confirm' button
* For Opal
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SEND_ONLY_BUTTON","1");

/********************
* Enable or disbale 'edit total amount option' on editing the transaction

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EDIT_VALUES","0");


/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","1");




/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");

/********************
* city non-compulsory for opal 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CITY_NON_COMPULSORY","1");


/********************
* remove quick sender/beneficary link from add transaction 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMOVE_QUICK_LINK","1");

/********************
* search collection point via country  


* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CP_SEARCH_VIA_COUNTRY","1");




/********************
* at edit transaction and amend transaction system generates reciept 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CUSTOM_AMEND_RECIEPT","1");

/********************
* add exchange rate option to be based on money paid also for opal
* If value is '0' its Disabled
* If value is '1' its Enabled
CONFIG_EXCHNG_TRANS_TYPE  has to be 'Disabled' in order to hae this config to be 'Enabled'
********************/
define("CONFIG_EXCHANGE_MONEY_PAID","1");


/********************
* add remove customer reference number on update sender page

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_REF_NUMBER","1");

/********************
* add remove print option on print transaction page for opal

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SINGLE_PRINT","1");


/********************
* add remove print option on print transaction page for opal

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_LINK_SENDING_AGENT_COUNTRY","0");


/********************
* add remove labels on the report

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_LABEL_ON_REPORT","1");
define("CONFIG_REPORT_NAME","Transactions For Lithuania");

/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");

/********************
* Switch to Enable/Disable non compulsory phone field 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_PHONE_NON_COMPULSORY", "1");


/********************                                                                         
* Switch to Enable/Disable formating of email and ID type for opal on add customer 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FORMAT_EMAIL_ID_TYPE", "1");




/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");



/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTOM_SENDER", "1");
define("CONFIG_SENDER_PAGE", "addSenderOpal.php");

/********************
* add remove linking agent country and sender country

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_LINK_AGENT_SENDER_COUNTRY","1");

/********************
* add remove date format yyyymmdd

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DATE_FORMAT_YYYYMMDD","1");

/********************
* add remove by cheque option in default payment mode for opal

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMOVE_OPTION_PAYMENT_MODE","1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");

/********************
* Switch to Enable/Disable search transaction on create transaction page

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_DISABLE_SEARCH", "1");

/********************
* Switch to Enable/Disable red/yellow color on date link of view transaction 
* and reference code link on manage detail transaction for 
* transactions having enquiry/remarks. 
*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_COLOR_OF_LINK", "1");

/********************
* Switch to Enable/Disable calculations for transactions with to country United kingdom for opal
*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_TOCOUNTRY_UK", "1");

/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");

?>
