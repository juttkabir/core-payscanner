<?php

// Load the config from the database
include "../include/load_config.php";


/********************
* To define Client to use System.
********************/
define("SYSTEM","globalexchange");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/globalexchange/logo.JPG");
define("CONFIG_LOGO_WIDTH","142");
define("CONFIG_LOGO_HEIGHT","74");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");

/**
 * On the quick menu, to diaplay the multiple authorisation links
 * i.e. one link for Bank and Cash and other for Transaction with Cheque
 * To enable this functionality assign below mentioned link with "1"
 * If this config is marked as "0", than one link will be displayed for all the transaction types
 * @Ticket #4362
 */
define("CONFIG_MULTI_LINKS_AT_MAIN_MENU","0");

/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I/we hereby declare that the money paid to you by me/us was or is not
derived or obtained by any illegal means and Transaction including but
not limited to any unlawful drug dealings.");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","1");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Customer ");
define("CONFIG_PAYING_BOOK_AGENTS","100221");
// Config below is added to give payin book functionality functionality to every one.
define("CONFIG_PAYIN_BOOK_FUNCTIONALITY_TO_EVERY_ONE", 1);





/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","GE");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Global Exchange Funds Code");
define("COMPANY_NAME","Global Exchange");
define("COMPANY_NAME_EXTENSION","Money Transfer");
define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "");
define("INQUIRY_EMAIL", "");
define("CC_EMAIL", "");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Company Support");


define("CONFIG_INVOICE_FOOTER", " 0800 000 0000");
define("COMPANY_URL","");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- Global Exchange");
define("TITLE_ADMIN", "Payex- Global Exchange");


define("COMPANY_ADDRESS","54 Ealing Road, Wembley HA0 4TQ");
define("COMPANY_PHONE", "not provided");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","confirmTransactionGlobalExchangeReciept.php");
define("CONFIG_RECIEPT_PRINT","printConfirmTransactionGlobalExchangeReciept.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","0");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)
* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "0");
define("CONFIG_PAYIN_RESTRICT_LIST", "SUPA,SUPAI");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "Admin, SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");
define("CONFIG_USERS_FOR_MANDATORY_ADRESS","SUBA,SUBAI,SUPA,SUPAI");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_AGENT_STATEMENT_BALANCE","1");
define("CONFIG_DISTRIBUTOR_STATEMENT_BALANCE","1");



/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","0");



/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","0");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","1"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","0"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","0");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "express");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");



/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");

/**
 * TO show the remarks field at the add transaction page
 */
define("CONFIG_CUST_DOC_FUN","1");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");




/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "0");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,");
define("CONFIG_TRANS_ROUND_CURRENCY", "GHS,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "0") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
/********************
* Switch to Enable/Disable an agent associated with payin book customers
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_AGENT_NUMBER", "100221");
 	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "0"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "0"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
//*define("CONFIG_CURRENCY_NAME_BEN", "LKR");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","0");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
//define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","1");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "1");



/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");


/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/
define("CONFIG_OPTIMISE_NAME_SEARCH","1");



/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

/********************
* The logic in which transation will be delivered without the customer giving the money and then 
* those transactions will be checked through money paid by  on credit option.
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_MONEYPAID_ONCREDIT","0");

/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/
define("CONFIG_ENABLE_RECALCULATE_UNHOLD","1");

/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/
define("CONFIG_ENABLE_EX_RATE_LIMIT","1");


/********************
* Switch to Enable/Disable Usage of an extended table with transaction 
* to keep optional info related to a particular transaction
* specifically used for connect plus and global Exchange
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_USE_EXTENDED_TRANSACTION", "1");


/********************
* Switch to Enable/Disable Distributor's Reference number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIST_REF_NUMBER", "0");
define("CONFIG_DIST_REF_NAME", "Distributor's Reference Number");
define("CONFIG_GENERATE_DIST_REF_ON_EXPORT", "0");
define("CONFIG_DIST_REF_ON_EXPORT","0");
define("CONFIG_REUSE_DIST_REF_NUMBER", "0");
DEFINE("CONFIG_REF_IN_EXTRA_INFO","0");

define("CONFIG_CUSTOM_COLOR_ENABLE","0");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

/********************   
* Added by Niaz Ahmad against ticket #2202 at 19-10-2007                                                                      
* Show Global balance 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_GLOBAL_BALANCE", "0");

/********************
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
*	0 means Disable
*	1 means Enable 
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;


/********************
* Switch to Enable/Disable settlement commission on create transaction
*       0 - enable
*       1 - disable
* i think it is to be removed from system as this development seem to be craped now.... just to confirm from javeria 
first...
********************/
define("CONFIG_SETTLEMENT_COMMISSION", "1");



/********************
Added by Niaz Ahmad #2810 at 16-01-2008
*  Enable/Disable settlement currency dropdown
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "1");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "0");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");
//define("CONFIG_SHOW_MANUAL_CODE", "1");

/************************************************************************************************
Added by Usman Ghani against #3331: Global Exchange: Inter distributor transfer enhancement
To allow transfer even if it is greater then the amount of available balance.
* If the value is '1', it is on.
* If the value is '0', it is off.
************************************************************************************************/
define("CONFIG_ALLOW_INTER_DIST_TRANSFER_EVEN_LOW_BALANCE", 1);

/********************************************
This cancel return functionality is brought from opaltransfer.
* If the value is '1', it is on.
* If the value is '0', it is off.
********************************************/
define("CONFIG_RETURN_CANCEL", "0");

/**
 * Saving the amount recieved at the confirm trasaction page level to db
 * @Ticket# 3547
 */
define("CONFIG_SAVE_RECIEVED_AMOUNT",1);

/**
 * introducing the new const for handling the commission based on payment mode
 * If enabled then the commission will be based on the payment mode
 * @Ticket# 3319
 */
define("CONFIG_ADD_PAYMENT_MODE_BASED_COMM","1");

/**
 * Add new money paid option to create trasaction
 * and exchange rate module
 * @Ticket# 3564
 */
define("CONFIG_MONEY_PAID_OPTION_BY_CARD",1);

/**
 * Creating the new file documentCategories.php 
 * having control by this config
 * @Ticket# 3554
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);


/**
 * redirect the user to reciept when depositing manualy in customer's ledger
 * ticket no 3549
 * @Ticket# 3554
 */
define("CONFIG_CUSTOMER_LEDGER_PAGE",1);
define("CONFIG_REF_NO_ON_CUSTOMER_ACCOUNT",1);
define("CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT",1);


/**
 * Config to control the Agent Outstanding balance report
 * @Ticket# 3553
 */
define("CONFIG_AGENT_OUTSTANDING_REPORT",1);

/**
 * Config enable the sender reference number generator
 * @Ticket# 3549
 */
define("CONFIG_SENDER_NUMBER_GENERATOR",1);

/********************
* To make postal code field non compulsory for agent logins in add/update beneficiary
* during create transaction in case CONFIG_COMPUL_FOR_AGENTLOGINS is enabled.
* NOTE: Add this config only if CONFIG_COMPUL_FOR_AGENTLOGINS is on.  
* @Ticket #3639 Global Exchange - Beneficiary Postcode
* By khola
********************/
define("CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS", 1);


/**
 * For GE, normal sender will have payin book customer logic
 * This config will work as same as in express client but no will not display any label or field to clinet
 * @Ticket# 3549
 */
define("CONFIG__GE__USE_CUSTOMER_PAYIN_BOOK_LOGIC",1);

/**
 * At the creation of the transaction the fax number entry can be controled by following config
 * @Ticket #3638
 */
define("CONFIG_GE_ADD_FAX_NUMBER",1);

/**
 * Config added to route the distributor output file export action to Global Exchange's custom export file.
 * @Ticket #3636
 */
define("CUSTOM_EXPORT_DISTRIBUTOR_OUTPUT_FILE", 1);
define("CUSTOM_EXPORT_DISTRIBUTOR_OUTPUT_FILE_NAME", "export_distributor_output_global_exchange.php");
define("CUSTOM_EXPORT_DISTRIBUTOR_OUTPUT_FILE_NAME_WORD", "export_distributor_output_global_exchange_word.php");
/**
 * Config added to make the distributor field mandatory in search filters on distributor outpur file.
 * @Ticket #3636
 */
define("CONFIG_DISTRIBUTOR_MANDATORY_IN_SEARCH", 1);

/**
 * Add Bank account payment detail field mandatory
 * 
 */
define("CONFIG_ADD_BANK_ACCOUNT_PAYMENT_DETAIL_MANDATORY",1);

/* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types */
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY",1);

/**
 * By assigning this config '1', system will have Export functionality in distributor statement report
 * @Ticket #3799 
 */
define("CONFIG_EXPORT_DISTRIBUTOR_STATEMENT_REPORT",1);

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");
/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
*       1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");
/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
* 0 Disable
* 1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/* @Ticket #3792 */
define("CONFIG_AUTO_PAYIN_NUMBER",'1');
/**
 * If this config is on than at add sender page, if sender is selected from the top than this sender will be saved against the sender
 * else if no sender is selected than default sender from CONFIG_PAYIN_AGENT_NUMBER will be assigned provided if payin book check box is 
 * selected from down.
 * else exception will be thorugh to select the agent
 * @Ticket #3790
 */
define("CONFIG_USE_AGENT_AT_ADD_SENDER",1);

/**
 * Displays a column for received amount in Customer Account Statement.
 * true = Display
 * false = Hide
 */
define("CONFIG_SHOW_RECEIVED_AMOUNT_COLUMN", true);

/********************
* Agent Based Margin on Exchange Rate connect plus,#2782
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_AGENT_RATE_MARGIN","1");

/**
 * Saves full (total) amount in ledger, in case CONFIG_SAVE_RECIEVED_AMOUNT == 1 and totalAmount == receivedAmount
 * true = Saves full amount
 * false = Does nothing
 */
define("CONFIG_SAVE_FULL_AMOUNT_IN_LEDGER", true);

/**
* Saves total amount in distributor ledger rather than default transaction amount.
* true = Saves total amount
* false = Default to transaction amount
*/
define("CONFIG_SAVE_TOTAL_AMOUNT_IN_DISTRIBUTOR_LEDGER", true);

/**
* Impacts ledgers (customer and agent) with double entry
* true = impact with double entry
* false = default to normal
*/
define("CONFIG_EFFECT_LEDGERS_WITH_DOUBLE_ENTRY", "true");

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/**
 * To show the currency lables at the GE Distributor Transaction Statement 
 * @Ticket #4039
 */
define("CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT",0);

/********************
* Switch to Enable/Disable full name search filters on sender/beneficiary report
*       0 Disable
*       1 Enable
********************/
define("CONFIG_AGENT_OWN_RATE", "1");
define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "SUPA,SUPAI,Admin,admin,");

/******************** 
*Enabling user commission management 
*       0 means Disable 
*       1 means Enable  

*CONFIG_USER_COMMISSION_MANAGEMENT shows whether it is to be for other users like super agenrt/distributor or for sub distributor only
*CONFIG_COMMISSION_MANAGEMENT_NAME defines title of the link which is displayed in left menu, it part of that link name, not whole one. some where in beacon crest its value is 'Agent' making its link as 'Add Agent Commission'
*CONFIG_COMM_MANAGEMENT_FOR_ALL describes either it is for super agents only or for all users(Agent, Distributor, AnD), it is active when  CONFIG_USER_COMMISSION_MANAGEMENT is 1

********************/ 
define("CONFIG_USER_COMMISSION_MANAGEMENT", "1"); 
define("CONFIG_COMMISSION_MANAGEMENT_NAME", "Distributor"); 
define("CONFIG_COMM_MANAGEMENT_FOR_ALL", "0"); 
define("CONFIG_SHOW_ONLY_LOCAL_AMOUNT","1");

/**
 * To Hide the distributor commission column and compute the commission on the basis of commulative amount
 * @Ticket #4047
 */
define("CONFIG_DISTRIBUTOR_COMMISSION_REPORT_HIDE_COMMISSION_COLUMN","0");

/** 
 * Customer Customer's account name prefix
 * @Ticket #4305
 */
define("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX","");
/**
 * To store the modify transaction history
 * For other clients than global exchange, this need to be developed in the confirm receipt, so this will 
 * not work for other clients
 * @Ticket #3337
 */
define("CONFIG_STORE_MODIFY_TRANSACTION_HISTORY","1");

define("CONFIG_UNLIMITED_EX_RATE_OPTION","1");


/**
 * The below mentioned config display the user defined ID information on the DOF File.
 * Assign the below config the name of the Id type of beneficiary, e.g. Passport
 * NOTE: Please dont assign this config "0" to make it off, instead just comment this code.
 * @Ticket #4425
 */
define("CONFIG_USE_ID_TYPE_OF_BENEFICIARY","Passport");

/**
 * These are the configs which allow user to add unlimited manual exchange rate.
 * country should be in capital letters.
 * Add those countries in config where user has to make transactions with unlimited exchange rate.
 * Ticket #4462
 **/
define("CONFIG_UNLIMITED_EXCHANGE_RATE","1");
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","SRI LANKA,COLOMBIA,CANADA,PAKISTAN,");

/**
 * If client want to have full name or number of agent at the prefix, than this config should be enabled
 * @Ticket #3833
 */
define("CONFIG_USE_FULL_NAME_AT_REFRENCE_GENERATOR_PREFIX",1);

/**
 * If client want to see only Authorize Transactions and in ascending order at Distributor Output file
 * @Ticket #4488
 */
define("CONFIG_VIEW_AUTHORIZE_TRANS_AES",1);

define("CONFIG_CHECK_MANUAL_REFNUMBER_AT_TRANS",1);
/**
if this config is 1 then postal code search field will be shown at search customer page
@ Ticket 4424
*/
define("CONFIG_SEARCH_SENDER_BY_POSTCODE",1);
/*
This config added by Niaz Ahmad at 07-03-2009 @4630
At add sender page select agent in non mandatory for globalexchange
*/
define("CONFIG_ADD_CUSTOMER_AGENT_NO_MANDATORY","1");
/* @Ticket #4731
	* 	You can add transaction types for which Beneficiary ID Details should be necessary like as follows.
		ATM Card,Pick up,Bank Transfer,Home Delivery
	*	Or assign 0 (zero) to Disable
	
*/
define("CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES","Pick up,Home Delivery");

/*************
* If CONFIG_HIDE_TIP_FIELD is "1" then CONFIG_TIP_ENABLED should be "0"
**************/

define("CONFIG_HIDE_TIP_FIELD","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","0");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * To use the separate Distributor Output File for each client
 * feed the filename into the below menioned config
 * @Ticket #4362
 */
define("CONFIG_CUSTOMIZED_OUTPUT_FILE","distributor_output_file_ge.php");


/* This config is being added my Omair Anwer for Global exchange to provide them the 
 * functionality of "view documents" against a sender on the create transaction page.
 * The ticket numbher for this config is : 4899
 *
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION","1");


/* #4900
 * Quick Edit link shown at search beneficiary.
 * Config can have values '1' for all user types
 * OR can have user types in config like SUPA,Admin Manager etc.. (Link will be shown to those)
 * Config can have values '0' to disable functionality for all user types.
 */
define("CONFIG_SHOW_QUICK_EDIT_SEARCH_BEN","1");

/**
 * If customer provide the address, city, zip, place of birth, data of birth, id number and id expiration date
 * than do not put any compliance alert
 * @Ticket #3795
 */
define("CONFIG_COMPLIANCE_PROMPT_IDTYPE","1");

define("CONFIG_COMPLIANCE_PROMPT_FOR_LATEST_DATE", "0");
define("CONFIG_COMPLIANCE_PROMPT_FOR_MULTIPLE_DATE", "1");
define("CONFIG_DOCUMENTS_PROVIDED_MANDATORY","1");

//define("CONFIG_DCOUMNETS_PROVIDED_AS_DROPDOWN","1");
define("CONFIG_COMPLIANCE_PROMPT_FOR_DOCUMENT_CATEGORY","1");
$arrDocumentCategory = array(
	"SOURCE OF FUND" => "1",
	"Address Proof" => "2",
	"PHOTO" => "3"
	 );
/*
* Added by Niaz Ahmad at 03-07-2009 @5052-- GE
* Shown Document Proivded option at create transaction under sender details
*/	 
define("CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS","1");
/*
* Added by Niaz Ahmad at 21-07-2009 @5224-- GE
* Shown sender ref no  at view transaction page
* Enable = 1
* Disable = 0
*/	 
define("CONFIG_SENDER_REFERENCE_NUMBER","1");
?>