<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Connect Plus");
define("CLIENT_NAME","connectplus");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/connectplus/logo.jpg");
define("CONFIG_LOGO_WIDTH","213");
define("CONFIG_LOGO_HEIGHT","97");
define("CLIENT_NAME","connectplus");
/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT", "1");
define("CONFIG_RECIEPT_NAME", "confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT", "print-confirmed-transaction-connectPlus1.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "Receipt4OpalBen.php");

/*
Citizenship Field on Add Beneficiary Form.
*/
define("CONFIG_CITIZENSHIP","0");
/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","0");

/********************
* Switch to Enable/Disable Currency Denomination 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURR_DENOMINATION","1");

/*
Custom Currency Denomination Menu Caption
*/
define("CONFIG_CURR_CAPTION","Currency Denomination");



/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");



/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","1");



/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/glink/Terms_and_conditions.doc");
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I confirm that the information set above and the documentation
provided as evidence of my identity are true and correct. Accordingly,
I accept that Connect Plus Business Limited cannot be held responsible
for any delays or non-payments due to any error(s) that will be found
to exist in the above information and documentation. I understand that
Connect Plus Business Limited will refund to me only the payment, and I
accept that the administrator fee charged is non-refundable.");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","1");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","1");



/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");



/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");



/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");


/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");

/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");

/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","1");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");


/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "1");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");




/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","CP");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Manual Receipt Code");
define("COMPANY_NAME","Connect Plus Business Ltd");

// Opal used this title in receipts.
define("COMPANY_INT_NAME", "International Money Transfers");

define("CONFIG_AGENT_STATMENT_GLINK","1");



/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");




/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");



/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","0");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Company specific information to be used in the system
********************/

define("SUPPORT_EMAIL", "customerservices@connect-plus.co.uk");
define("INQUIRY_EMAIL", "customerservices@connect-plus.co.uk");
define("CC_EMAIL", "customerservices@connect-plus.co.uk");
define("SUPPORT_NAME", "Connect Plus Support");
define("COMPANY_ADDR","First Floor , 19 Denmark Street<br>
London, WC2H 8NA - London - UK
");

define("CONFIG_COMPANY_PHONE","020 7836 1172");
define("CONFIG_COMPANY_FAX","020 7836 1173");

define("CONFIG_INVOICE_FOOTER", "+44(0)800 093 7072");
define("COMPANY_URL","www.connect-plus.co.uk");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","::.Connect Plus Business Ltd.::");
define("TITLE_ADMIN", "::...Connect Plus Business Ltd ...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","1");

/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");

/********************
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS", "1");

/********************
* Switch to Enable/Disable to manage export transactions a/c to opal
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_OPAL", "1");

/********************
* Through this variable, transactions exported will be for this country(receiving)
********************/
define("CONFIG_COUNTRY_EXPORT_TRANS", "");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","1");
define("CONFIG_DISTRIBUTOR_STATEMENT_CUMULATIVE","1");

/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY, 
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example, 
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","0");

define("CONFIG_BEN_COUNTRY","BRAZIL");
/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","0");

/*
* Transaction refrence number format change
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_IMREF_FORMAT", "0");


/********************
* Switch to Enable/Disable to show Create Transaction Option for Teller
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TELLER_CREATE_TRANS","0");



/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,");
define("CONFIG_TRANS_ROUND_LEVEL","2");

/********************
* Switch to Enable/Disable Defalut Sending Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY","1");
define("CONFIG_CURRENCY_NAME","GBP");

/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "BRL");

/********************
* Switch to Enable/Disable Filteration of records in braclays  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("FILTER_BARCLAYS_RECORDS","1");

/********************
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
*	0 means Disable
*	1 means Enable 
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;

/********************
* Switch to Enable/Disable other Title for "Middle Name"
* during add sender/beneficiary either quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUSTBEN_MIDNAME", "1");
define("CONFIG_CUSTBEN_MIDNAME_TITLE", "Middle Name");

/********************
* Switch to Disable Accepted Terms during Add Sender
*	1 means Disable 
********************/
define("CONFIG_DISABLE_ACCEPTED_TERMS", "0");

/********************
* Switch to Enable/Disable Drop Down of "ID Type" rather than radio buttons
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPE_DROPDOWN", "1");

/********************
* Switch to Enable/Disable ID Expiry with Calendar (not drop down)
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "0");

/********************
* Switch to Enable/Disable Free text ID fields
* user has to input dates like DD-MM-YYYY
* CONFIG_IDEXPIRY_YEAR_DELTA is the maximum year (from now) that user inputs
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_FREETEXT_IDFIELDS", "0");
define("CONFIG_IDEXPIRY_YEAR_DELTA", "10");

/********************
* Switch to Disable check box terms and conditions during create transaction
*	1 means Disable 
********************/
define("CONFIG_TnC_CHECKBOX_OFF", "0");

/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "0");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Driving License,UK Driving License,Home Office ID");

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");

/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");


/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");



/********************
* Switch to Enable/Disable 
* Option to switch off the tille in data entry forms
* By default it is not OFF
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TITLE_OFF", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");


/********************
* Switch to Enable/Disable Search Beneficiary 
* and then ability to create transaction 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************
* Switch to Enable/Disable layout like 1- Last Name 2- First Name 3- Patronyme(Mid Name)
* during add/update sender/beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_NAME_SEQUENCE", "0");

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "0");

/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "0");

/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "1");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable mobile field OFF
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_MOBILE_FIELD_OFF", "0");

/********************
* Switch to Enable/Disable Calculating Transaction Amounts from Total Amount
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CALCULATE_FROM_TOTAL", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "1");

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "0");

/********************
* Switch to Enable/Disable default Transaction Purpose
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Swaping of Amount and Local Amount up and down
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT", "0");

/********************
* Switch to Enable/Disable Ref NumberIM either a/c to DDMM or auto
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUMIM_AC_TO_DDMM", "1");

/********************
* Switch to Enable/Disable Ref NumberIM either a/c to DDMMYY or auto
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUMIM_AC_TO_DDMMYY", "0");

/********************
* Switch to Enable/Disable not to update Ref NumberIM
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IMTRANSNUM_NOT_UPDATE", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");



/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "5");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Switch to Enable/Disable functionality of default Distribuotr
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DEFAULT_DISTRIBUTOR", "0");


/********************
* This switch is to enable/disable Internal Remarks in transactions
* Only the company will see it internally..... Not distributor side will be allowed to view it
* Made for Monex and Opal transfer
* 0 Disable Internal remarks 
* 1 Enable Internal remarks 
********************/
define("CONFIG_INTERNAL_REMARKS","1");
define("CONFIG_LABEL_INTERNAL_REMARKS","Internal Remarks for the Transaction");


/********************
* Switch to Enable/Disable Address Line 2 for customer and Beneficiary
* Especially done for Opal
* If value is '1' its Disabled
* If value is '0' its Enabled
********************/
define("CONFIG_DISABLE_ADDRESS2", "1");


/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is 
* CONFIG_COMPLIANCE_CUSTOM_COLUMN, 
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable sender ID date alert
* 0 Disable
* 1 Enable
********************/
define("CONFIG_ALERT", "1");
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php");
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
define("CONFIG_AMOUNT_CONTROLLER", "675");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");
/********************
* Added by Niaz Ahmad #1727 at 08-01-2008
* 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT","1");
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");


/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","1");

/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","1");

/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* define Enquiry ID
********************/
define("CONFIG_FORUM_ID","MSG-");

/********************
* define Enquiry ID
********************/
define("CONFIG_FORUM_QUICK_MENU","View Enquiry in Forum");
define("CONFIG_ENABLE_FORUM","1");



/********************
* reconcilation exchangerate
added by javeria razzaq-ticket #2609
********************/
define("CONFIG_CONNECT_PLUS_DIST_COM_REPORT","1");



/********************
* Switch to Enable/Disable Ledger on cancel transaction Paid back to sender
*       0 Disable
*       1 Enable
********************/
define("CONFIG_RETURN_CANCEL", "0");

/********************
* Switch to Enable/Disable Commission effect on transaction canceled or return of amount...
*       0 - Commission Effect on canceled
*       1 - Commission effect on amount return..
********************/
define("CONFIG_COMMISSION_ON_RETURN", "1");



/********************
* Switch to Enable/Disable settlement commission on create transaction
*       0 - enable
*       1 - disable
* i think it is to be removed from system as this development seem to be craped now.... just to confirm from javeria first...
********************/
define("CONFIG_SETTLEMENT_COMMISSION", "1");

/********************
* Switch to Enable/Disable Distributor's Reference number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIST_REF_NUMBER", "1");
define("CONFIG_DIST_REF_NAME", "Distributor's Reference Number");
define("CONFIG_GENERATE_DIST_REF_ON_EXPORT", "1");
define("CONFIG_DIST_REF_ON_EXPORT","1");
define("CONFIG_REUSE_DIST_REF_NUMBER", "0");
define("CONFIG_REF_IN_EXTRA_INFO","1");



/********************
* Switch to Enable/Disable columns on agent commission report
*       0 - enable
*       1 - disable
********************/
define("CONFIG_AGENT_COMMISSION_REPORT", "1");

/********************
* Switch to Enable/Disable Profit Earning Report Especialy for Bayba
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PROFIT_EARNING","1");


/********************
* Switch to Enable/Disable cancelation request link on main menu--added by javeria,#2759
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CANCEL_REQUEST_LINK","1");

/********************
* Switch to Enable/Disable agent filter on exchange rate earning report--added by javeria,#2759
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_FILTER","1");



/********************
* Switch to SHOW/HIDE already added exchage rate on add rate page to the agent--added by javeria,#2758
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_SHOW_EXCHANGE_RATE","1");
define("CONFIG_AGENT_OWN_RATE" , "1");



/********************
* Switch to exchangerate page page for connect plus,#2782
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_EXCHANGE_RATE_CONNECTPLUS","0");



/********************
* Agent Based Margin on Exchange Rate connect plus,#2782
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_AGENT_RATE_MARGIN","1");

/********************
* multiple exchange rates for connect plus,#2782
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_MULTI_RATE_PATTERN","0");

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");
/********************
Added by Niaz Ahmad #2810 at 16-01-2008
*  Enable/Disable settlement currency dropdown
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "1");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "1");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");
//define("CONFIG_SHOW_MANUAL_CODE", "1");

/********************
* Switch to Enable/Disable Exchange Rate used for the a particular transaction
* Developed for connect plus and used in Exchange Rate earning report and edit transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCH_RATE_USED","1");

/********************
* Switch to Enable/Disable Exchange Rate used for the a particular transaction
* I should not be edited once transaction is created, Developed for connect plus
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCH_RATE_NOT_EDIT","1");



/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report  
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
//define("CONFIG_AGENT_OWN_RATE", "0");
define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "SUPA,SUPAI,Admin,admin,");

define("CONFIG_MULTI_CURRENCY_BALANCE", "1");
/********************                                                                         
* Switch to Enable/Disable delete option on manage rate page
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REMOVE_DELETE_ON_MANAGE_RATE", "1");
/********************    
* Added By Niaz Ahmad #2918 at 06-02-2008                                                                     
* Switch to Enable/Disable calculation of cumulative amount for specific period
* When Config ON calneder will be shown at Compliance Configurations At Create Transaction
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_COMPLIANCE_NUM_OF_DAYS", "1");
define("CONFIG_EXPORT_TRANS_OLD","1");
define("CONFIG_ACCOUNT_TYPE_ENABLED","1");
define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");
/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Added by Niaz Ahmad #2936 at 14-02-2008
* Switch to Enable/Disable "Total Amount Sent" and "Cumulative Total of Amount" from Daily Distributor Report
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DAILY_DISTRIBUTOR_REPORT_TOTALS", "1");


/********************
* Switch to Enable/Disable to show Country/Currency Filter
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CNTRY_CURR_FILTER", "0");

/********************
* Switch to Enable/Disable to add whole commission in grand total 
* in Agent Commission Report view transaction detail
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_COMMISSION_REPORT_GRANDTOTAL", "1");

/********************
* Added by Niaz Ahmad #2962 at 26-02-2008
* Switch to Enable/Disable red/orange/green color.
*Pending trnsaction color = Dark Orange,
*Cancelled transaction color = Dark Red
*Authorize transaction color = Dark Green  
* 0 means Enable
*	1 means Disable 
********************/
define("CONFIG_SHOW_FLAG", "1");


/********************
* Added by Niaz Ahmad #2983 at 10-03-2008
* Client can select export file format
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_FILE_CUSTOMIZE", "1");


/********************
* Switch to Enable/Disable dual entry in agent's ledger for connectpluss
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DUAL_ENTRY_IN_LEDGER", "1");


/********************
* Switch to Enable/Disable negetive entry in case of deposit for connectplus

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NEGETIVE_ENTRY_IN_LEDGER", "0");



/********************
* Switch to Enable/Disable payment mode option on add agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_OPTION", "0");

/********************
* Added by Niaz Ahmad #2983 at 10-03-2008
* Client can select export file format
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_FILE_CUSTOMIZE", "1");

/********************
* Switch to Enable/Disable transaction manipulation before export
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TRANS_EXTRA_INFO", "1");


/********************
* Switch to Enable/Disable agent profit on agent commision report

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SHOW_AGENT_PROFIT", "1");



/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");


/********************
* Switch to Enable/Disable seconf middle name for sender/cstomer
* used for CP
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CUSTOMER_SECOND_MIDDLE", "1");


/********************
* Switch to Enable/Disable agent loss on agent commision report

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SHOW_AGENT_LOSS", "1");


/********************
* Switch to Enable/Disable seconf middle name for sender/cstomer
* used for CP
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BENEFICIARY_SECOND_MIDDLE", "1");


/********************
* Switch to Enable/Disable Clave(a range given for each transaction created)
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_CLAVE", "1");




/********************
* Switch to Enable/Disable Usage of an extended table with transaction 
* to keep optional info related to a particular transaction
* specifically used for connect plus
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_USE_EXTENDED_TRANSACTION", "1");

/********************
* Switch to Enable/Disable unlimited Agent own exchange rate
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_UNLIMITED_EXCHANGE_RATE", "0");
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","");
define("CONFIG_UNLIMITED_EX_RATE_OPTION", "1");


/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;


/********************
* Switch to Enable/Disable update Distributor Reference Number
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UPDATE_DISTRIBUTOR_REFERENCE", "1");

/********************
* Added by Niaz Ahmad #3096 at 17-04-2008
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1");
define("CONFIG_MANUAL_DISTRIBUTOR_NUMBER", "1");
define("CONFIG_CP_BRANCH_ADDRESS", "1");
define("CONFIG_DISTRIBUTOR_ALIAS", "1");

// Added by Usman Ghani #3379 and #3414. 
// Hide or show the distributor name from agent.
// 1 = Hidden , 0 = Not hidden
//define("CONFIG_HIDE_DISTRIBUTOR_NAME", 1);
define("CONFIG_HIDE_DISTRIBUTOR_CERTAIN_FIELDS", 1);
define("CONFIG_USERS_PROHIBITTED_TO_SEE_CERTAIN_FIELDS", "SUPA,SUBA");


/******************** 
*Enabling user commission management 
*       0 means Disable 
*       1 means Enable  
*CONFIG_USER_COMMISSION_MANAGEMENT shows whether it is to be for other users like super agenrt/distributor or for sub distributor only
*CONFIG_COMMISSION_MANAGEMENT_NAME defines title of the link which is displayed in left menu, it part of that link name, not whole one. some where in beacon crest its value is 'Agent' making its link as 'Add Agent Commission'
*CONFIG_COMM_MANAGEMENT_FOR_ALL describes either it is for super agents only or for all users(Agent, Distributor, AnD), it is active when  CONFIG_USER_COMMISSION_MANAGEMENT is 1

********************/ 
define("CONFIG_USER_COMMISSION_MANAGEMENT", "0"); 
define("CONFIG_COMMISSION_MANAGEMENT_NAME", ""); 
define("CONFIG_COMM_MANAGEMENT_FOR_ALL", "0"); 


/********************
* Added by Javeria #3115 
* to Show distributor name and alias on the collection points window while creating the transaction.
* if logged in by agent only Distributor's name is displayed otherwise both distributor and alias is displayed.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DIST_ON_COLLECTION_POINT_SERACH","1");

/********************
* Added by Javeria #3115 
* Notes field on add/update Agent and dostributor page
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NOTES_AGENT_DIST","1");

/********************
* Added by Javeria #3115 
* collection point data on transaction reciept for connect plus
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CP_DATA_ON_RECIEPT","1");


/********************
* Added by Javeria #3115 
* agent name filter and agent name column on manage fee pafe
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_MANAGE_FEE","1");


define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","1");



/********************
* Added by Khola #3115 
* forum message viewable by user for whom message has been added 
* CONFIG_FORUM_VIEW_USER specifices the user type for which this functionality is to be used e.g agent, branch manager etc
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_USER_SPECIFIC_FORUM_VIEW","1");
define("CONFIG_FORUM_VIEW_USER","SUPA,SUBA,SUPI,SUBI,SUPAI,SUBAI,");

/********************
* Added by JAVERIA #3224 
* update transaction to old or new transaction on export from DOF done for cp
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_UPDATE_EXPORTED_TRANS_ON_DOF","1");


/**
 * Implamenting the functionality in forum to only allow and view the enquiry between agents and head office staff
 * To make this functionality enable set below const value to 1
 * Or else by setting this value to 0 the functionality disables
 * @Ticket# 3376
 * @AddeddBy Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_FORUM_UPDATION_BETWEEN_AGENTS","1"); 


/********************
* Added by JAVERIA #3356
* transaction's local amount break up based on intermediate exchange rate
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_INTERMEDIATE_EXRATE_BREAK_UP","1");
define("CONFIG_INTERMEDIATE_EXRATE_BREAK_UP_LABEL","Intermediate Exchange rate Breakup");



/********************
* Config added by Usman Ghani against ticket "#3359: Connect Plus - Enhancement on Fee calculator" 
* To hide the exchange rate selection if a sub agent is signed.
* If the value is '1', it will hide the sxchange rate dropdown.
* If the value is '0', it will show the exchange rate dropdown.
*********************/

define("CONFIG_HIDE_EXCHANGE_RATE_DROPDOWN", 1);


/********************
* Added by JAVERIA #3356
* enable disable export module
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_MODULE","1");


/********************
* Added by JAVERIA #3388
* enable disable agentHouseNumber field on add agent page
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_HOUSE_NUMBER","1");

/********************
* Created By Khola against Ticket# 3270
* Enable or disbale sending of email on releasing a transaction 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EMAIL_ON_RELEASE","1");

/********************
* Added by Khola against ticket #3412
* enable disable remove some trans details from ben email for CP.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_EMAIL_CP","1");


/********************
* Added by JAVERIA #3322
* provide the reciept which is to be printed from quick menu

********************/
define("CONFIG_RECEIPT_PRINT_TRANSACTIONS_CUSTOMIZE_PAGE","1");
define("CONFIG_RECEIPT_PRINT_TRANSACTIONS","print-trans-details-connectPlus.php");

/**
 * Introiducing the new column
 * @Ticket# 3256
 * @addedBy     jahangir
 */
define("CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT","1");


/********************
* Added by JAVERIA #3430
* update all rates in the list 
* where sending and recieving currency matches with that of the updated rate

********************/
define("CONFIG_UPDATE_SAME_CURRENCY_RATE","1");

/************************************************************************************************
Added by Usman Ghani against "#3428: Connect Plus - Select Distributors for Agents"
* If the value is '1', it is on.
* If the value is '0', it is off.
It three pages.
	(a) add-agent.php
	(b) update-agent.php
	(c) add-agent-conf.php

Note:	If this config is on, it will show a distributor selection list box with
		label "select distributor".
************************************************************************************************/
define("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX", 1);


/********************
* Added by JAVERIA #3429
* change the priority of exchange rate from agent to distributor 

********************/
define("CONFIG_DIST_RATE_PRIORITY","1");

/**
 * Adding the config to handle the more filters in view exchange rate
 * @Ticket# 3426
 * @AddedBy Jahangir
 */
define("CONFIG_EXCHANGE_RATE_FILTERS",1);

/**
 * Adding the config constant to handle the default value of calculate by field
 * Set the default value to exclusive
 * This variable was already availble, so just use it
 * @Ticket# 3409
 */
define("CONFIG_DEFAULT_VALUE_FOR_CALCULATEBY", "1");
define("CONFIG_DEFAULT_VALUE_CHOICE", "exclusive");

/**
 * At exchange rate in distributor list sub distributors will also be present
 * as well as uper distributors
 * @Ticket# 3541
 */
define("CONFIG_SUB_DISTRIBUTOR_AT_EXCHANGE_RATE",1);

/**
 * At exchange rate in agent list sub agents will also be present
 * as well as super agents
 * @Ticket# 3541
 */
define("CONFIG_SUB_AGENT_AT_EXCHANGE_RATE",1);

/**
 * Config added to show agent->distributor exchange rates.
 * @Ticket: #3425: Connect Plus - Agent Exchange Rate Filters.
 * @Author: Usman Ghani.
 */
define("CONFIG_SHOW_AGENT_DISTRIBUTOR_RATE_TO_CERTAIN_USERS", 1);
define("CONFIG_USERS_TO_SEE_AGENT_DISTRIBUTOR_RATES", "SUPA, SUBA");

/**
 * config to hide/display Distributor Alias column in Distributor Locator Report
 * true  = Show
 * false = Hide
 */
define("CONFIG_SHOW_DISTRIBUTOR_ALIAS_COLUMN", false);

/**
 * Config to enable/disbale Distributor Commission based on WIHC logic.
 * See ticket #3603 for a reference.
 * 
 * true = Enable
 * false = Disable
 */
define("CONFIG_CALCULATE_WIHC", true);

/**
 * this cofig used to display distributor commission from the "settlementDistributorCommisson"
 * instead of "distributorComm" of tranaction table
 */
define("CONFIG_CP_DISTRIBUTOR_SETTLEMENT_COMM",1);
/**
 * Use this config, if you want to add the denomination based fee, agent and denomination based fee, * or agent base fee to sub type of fixed or percentage fee
 * @Ticket #3820
 */
define("CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE",1);


/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", 1);

/** 
 * Manage Upload document Categories 
 * @Ticket #3594
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);



/** 
 * Agent commission manager 
 * @Ticket #5304
 */
define("CONFIG_AGENT_COMMISSION_MANAGER",'1');

define("CONFIG_STORE_MODIFY_TRANSACTION_HISTORY","1");



/** 
 * Email on amendment of transaction 
 * @Ticket #5305
 */
define("CONFIG_EMAIL_ON_AMEND",'1');
define("CONFIG_EMAIL_ADDRESS_FOR_AMEND",SUPPORT_EMAIL);


/**
 * View senders / customer docuements at create transaction
 * @Ticket# 5322
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION",1);

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * CURRENCY EXCHANGE MODULE
 * This config affects whole module.
 * if not enabled then you can't access page directly into url.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE","1");

/*#5027
	Paid status is disabled on Cheque cashing Module report
	CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_PENDING_CQ will only be applicable
	for Pending status in Cheque Cahing Module. you may switch that off and use this this config.
	Enable : (for status in comma-separated list in Config.)
	"NG" => "Pending"
	"AR" => "Clear"
	"ID" => "Paid"
	"LD" => "Hold"
	"ST" => "Cancel Request"
	"EL" => "Cancel"
	"RN" => "Return"
	Disable : (assgin "0")
	by Aslam Shahid
	
*/
define("CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_STATUSES_CQ","LD,ST,EL,RN");

/*#4597
	On creating cheque cashing order estimated date was including saturdays and sundays from issue date after adding
 	no of days defined in database.Now holidays(saturday & sunday) are not dealt in no of days.
	by ashahid
*/
define("CONFIG_ADD_HOLIDAYS_IN_ESTIMATED_DATE_CQ","1");

define("CONFIG_CREATE_ADMIN_STAFF_LEDGERS", "1");
$_arrAdminStaffForCreateTransactionLedgers = array(
													"Admin", "Admin Manager",
													"CALL"
												);

/** 
 * #5258 Minas Center
 * Shows Opening/Closing Balnaces on Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 											
define("CONFIG_ADMIN_STATEMENT_BALANCE", "0");
/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Currency Excange Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_ADMIN_LEDGER", "1");

/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Cheque Cashing Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_CASHING_ADMIN_LEDGER", "1");

/** 
 * #5255 Minas Center Point #7
 * Ledgers of all users are affected for User Denomination
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_USER_DENOMINATION_ADMIN_LEDGER", "0");		

/*
* #4949
* Paypoint Transactions functionality added alongwith its services,account summary under this config.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_ADD_PAYPOINT_TRANS", "1");										
/** 
 * #5307 CONNECT PLUS
 * When partial match occur and not block from partial match window 
 * set a rule whether hold transaction of this user or not
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_COMPLIANCE_USERS_TRANS_HOLD", "1");												
/** 
 * #5323 CONNECT PLUS
 * when select other option from funds source dropdown  
 * then it will shown other funds sources input fields 
 * added new field (other_funds_source) in transaction table as well
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_OTHER_FUNDS_SOURCES","1");

/******************************************************************************************* 
 * #5186 (AMB)
 * Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
	More Importantly following config should be OFF.
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN
	
	And these sould be on... (Although settlement logic is not used here.)
	CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD

	NOTE :: All this scenario is set-up for
			1- System uses A&D functionality.
			2- There are no settlements currencies in A&D.
			3- Ledgers A&D as Distributor will be affected properly. (Created,Cancelled,Rejected,Failed,Credited etc)
			4- Account Summary and Balance of A&D as Distributor is affected as well.
 *****************************************************************************************
 * #5313 (Connect Plus)
 * For Agents/Admin Staff ledgers to affect properly in case of settlement functionality is workding.
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD", "1") ;
/** 
 * #5299 CONNECT PLUS
 * When CONFIG_UPDATE_SAME_CURRENCY_RATE is ON to Update Rates of Agent/Distributors
 * While updating Generic rate.
 * Now margin of rate is not updated under new config CONFIG_UPDATE_SAME_RATE_NOT_MARGIN
 * Enable  : "1"
 * Disabl  : "0"
 */ 
define("CONFIG_UPDATE_SAME_RATE_NOT_MARGIN", "1");


/**
 * User should not change margin values on update exchange rate
 * 1 = enable
 * 0 = disable
 */
define("CONFIG_NOT_EDIT_MARGIN", 1);

?>