<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Muthoot Global");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/muthoot/Muthoot_Global_logo.JPG");
define("CONFIG_LOGO_WIDTH","160");
define("CONFIG_LOGO_HEIGHT","80");
define("CONFIG_OTHER_LOGO","../admin/images/muthoot/Logo_CenterUnion.JPG");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I accept that I have not used any illegal means to earn the money being sent. I accept that I am not sending this money for any illegal act. I also accept the full Terms & Conditions of Muthoot Global Services");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","0");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Payin Book Sender");
define("CONFIG_PAYING_BOOK_AGENTS","100221,"); //for express
//define("CONFIG_CONTACT_NUMBER_SEARCH_PHONE_MOBILE","1");


/**
 * introducing the new const for handling the commission based on payment mode
 * If enabled then the commission will be based on the payment mode
 * @Ticket# 3319
 * @Author Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_ADD_PAYMENT_MODE_BASED_COMM","0");

#4730-Transaction type Fee
define("CONFIG_FEE_BASED_DROPDOWN","1");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","MGL");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Muthoot Global Trans Code");
define("COMPANY_NAME","Muthoot Global");
define("COMPANY_NAME_EXTENSION","Money Transfer");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "muthootglobal@yahoo.co.uk");
define("INQUIRY_EMAIL", "muthootglobal@yahoo.co.uk");
//define("CC_EMAIL", "");
//define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Muthoot Global Support");


define("CONFIG_INVOICE_FOOTER", "");
//define("COMPANY_URL","www.centerunion.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- Muthoot Global Service");
define("TITLE_ADMIN", "Payex- Muthoot Global Service");


define("COMPANY_ADDRESS","236 Merton High Street,Wimbledon,London SW19 1AU");
define("COMPANY_PHONE", "Tel. 02085430580 Fax. 02085430580");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","200");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");



/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");




/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","express-confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction-muthoot.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","0");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","1");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","0");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");


/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");




/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");




/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "0");

//#4726  This config is created to make Address Line 1 1 a mandatory field for All users.
define("CONFIG_BENEFICIARY_ADDRESS_MENDATORY","1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Money In");
define("CONFIG_WITHDRAW","Money Out");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");






/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 





/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");




/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "0");




/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");



/**
 * Adding config to handle the bank details of the beneficiary
 * @Ticket# 3320
 * @Date 25th June, 08.
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_BEN_BANK_DETAILS","0");


/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,By Cheque,");
define("CONFIG_TRANS_ROUND_CURRENCY", "BRL,GBP,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO,CURRENCY_FROM,");
define("CONFIG_TRANS_ROUND_LEVEL", "0");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable

* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "0") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	

 	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	





/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "BRL");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");






/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","0");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","0");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "0");


/**
 * Adding a const to handle the appearance  of distributor at the add transaction case
 * Provideed if the the transfer type is bank selected
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 * @Ticket# 3321 / 3320
 */
define("CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS","1");


/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","0");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");


/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");



/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");


/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "1");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Full UK Driving License");

/********************

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");
/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");


/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "1");


/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "1");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "0");

/**
 * Select the default Money Paid option to select One 
 * @Ticket# 3517
 * @AddedBy Jahangir
 */

define("CONFIG_MONEY_PAID_SELECT_ONE",1);

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

define("CONFIG_CPF_ENABLED","1");

define("CONFIG_ACCOUNT_TYPE_ENABLED","1");
define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");

/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");



/********************
* Switch to use optimise search for agent on view transactions page--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMISE_AGENT_SEARCH","1");



/********************
* Switch to use optimise reference number search--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMIZE_REFERENCE_NO","1");



define ("CONFIG_VIEW_CURRENCY_LABLES","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

/**
 * Adding a new field containg the client refrence number
 * @ticket 3321 / 3320
 */
define("CONFIG_CLIENT_REF", "1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
* 1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");

/********************
* Added By Khola Rasheed against ticket #3318: Minas Center - Bank Transfer Types
* Switch to Enable/Disable 24 hour banking module which addes 24 hour banking exchange rate 
* and create transaction has banking info whether normal or 24 hr
* 0 Disable
* 1 Enable
********************/
define("CONFIG_24HR_BANKING", "0");

/**
 * Setting the default transaction type to bank transfer
 * @Ticket# 3321 / 3320
 */
define("CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER","Bank Transfer");

/********************
* Added by Usman Ghani against #3321 minas center import sender
* To import a custom csv file.
* 0 Off
* 1 On
********************/
define("CONFIG_IMPORT_CUSTOM_SENDERS_DATA", 1);

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", "1");

/* Money paid as non mandatory*/
define("CONFIG_MONEY_PAID_NON_MANDATORY","0");

/**
 * To display custom list of banks in Bank Search Module.
 * @Ticket: #3538: Minas Center - Bank List Problem
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}
 */
define("CONFIG_CUSTOM_BANK_SEARCH_RESULTS", 1);


/**
 * To show a custom template file for minas center only.
 * @Ticket: #3526: Minas Center-Change the sample file of Import Beneficiary to the latest one.
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}.
 */
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE", 1);
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME", "sampleTempBen_minasCenter.xls");


/**************************************************************
* Added by Javeria against #3546:
add sending bank on create transaction page depending on money paid's value
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_ADD_SENDING_BANK_ON_CREATE_TRANSACTION", 0);


/**
 * Adding the bank based trasactions capability
 * this config will control the main.php entery against the bank based transactions
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT",0);


/**
 * View senders / customer docuements at create transaction
 * @Ticket# 3779
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION",1);

/** 
 * Manage Upload document Categories 
 * @Ticket #3917
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

/**
 * Sender refrence number at confirm transaction page
 * @Ticket #
 */
define("CONFIG_SHOW_REFRENCE_NO_AT_CONFIRM", 1);
/**
 * Displays sender bank details on reports.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_BANK_DETAILS","0");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_TRANSFER_BY_BANK","0");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_REFERENCE_NUMBER","0");

/* added and enabled this config against ticket #3939 */
define("CONFIG_DUAL_ENTRY_IN_LEDGER","0");

define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "SUPA,SUPAI,admin, Branch Manager, Admin Manager,Admin,Call,");

/* unlimited exchange rate ticket #4136 */
define("CONFIG_UNLIMITED_EX_RATE_OPTION",'1');

/*user can enter unlimited manual exchange rate  #4553 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE",'1');
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","BRAZIL,PAKISTAN,");

/**
 * Agent Rate can be defined from the system 
 * @Ticket #4355
 */
define("CONFIG_AGENT_OWN_RATE","1");

/**
 * To retains the remarks about a customer/sender
 * at its creation and display at the add transaction
 * @Ticket #4230
 */
define("CONFIG_REMARKS_ABOUT_SENDER","0");

/**
 * This is to implement logical calculation if Manual Rate is entered. so that for transaction 
 * if Agent of the sender has defined the exchage rate then it should be 
 * used rather than other rates. So there may be profit/lossfor agent after comparing the amount(amount*manula_exchage_rate)
 * to (amount*agent_exchage_rate) calculated.
 * by exchange rate
 * @Ticket #4574
 */
//define("CONFIG_AGENT_EXCHANGE_RATE_GET","1");
define("CONFIG_SHOW_AGENT_PROFIT","1");
define("CONFIG_SHOW_AGENT_LOSS","1");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","0");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * CURRENCY EXCHANGE MODULE
 * This config affects whole module.
 * if not enabled then you can't access page directly into url.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE","0");
/*
  Added by Niaz Ahmad at 11-03-2009 @4691
  This config use to insert margin in  transactionExtended table when manual exchange rate used.
*/
define("CONFIG_USE_EXTENDED_TRANSACTION","1");
define("CONFIG_INTERNAL_REMARKS","1");
define("CONFIG_LABEL_INTERNAL_REMARKS","Message");

//#4729-manual commission
define("CONFIG_MANUAL_FEE_RATE","1");
define("CONFIG_BEN_COUNTRY","INDIA,ITALY,");

//#4726-Make mobile field compulsary
define("CONFIG_MOBILE_MANDATORY","0");
/*
Added by Naiz Ahmad at 19-03-2009 @4722 Muthoot
*/
define("CONFIG_CITY_MANDATORY","1");
define("CONFIG_SUPI_MANAGER_ADMIN_STAFF","1");

/**
 * For the commission to be based on the transaction type
 * Assign CONFIG_FEE_BASED_TRANSTYPE config value "1"
 * @Ticket #4730
 */
define("CONFIG_FEE_BASED_TRANSTYPE", "1");


/**
 * The commission is calculated on the base of the amount, which is specified by followin g config
 * If this config is unavailable than the default value will be 'Amount' (transAmount)
 * @Ticket #4730
 */
define("CONFIG_COMMISSION_CALCULATED_ON", "transAmount");

define("CONFIG_AGENT_MANAGE_FEE", "1");

/**
 * If this config is '1' then localAmounts will be displayed otherwise transAmount for All user types.
 * If this config is given user_types like SUPA,SUPAI,admin, Branch Manager, Admin Manager,Admin,Call,
 * then for only for those localAmounts will be displayed otherwise transAmount on ReleaseTrans(currently)
 * @Ticket #4792
 */
define("CONFIG_SHOW_ONLY_LOCAL_AMOUNT_USER_TYPES", "SUPI,SUBI");
/**
 * If this config is '1' then localAmounts will be displayed otherwise transAmount for All user types.
 * @Ticket #4792
 */
define("CONFIG_SHOW_ONLY_LOCAL_AMOUNT", "1");

/**
 * If config is '1'  then Notes field will be displayed All user types.
 * If config is  '0' then will not display for any.
 * if config has user_types then will show for them only. i.e. SUPA,SUPAI,admin, Branch Manager, Admin Manager,Admin,Call,
 * @Ticket #4794
 */
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES", "1");
/*
Added by Niaz Ahmad at 16-04-2009 @4720

*/
define("CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION","1");

/**
 * Enables/disables mode of payment on agent account
 *
 * true = Enable
 * false = Disable
 */
define("CONFIG_ENABLE_PAYMENT_MODE_ON_AGENT_ACCOUNT", true);

/**
 * Shows/Hides cheque number, status fields on agent account statement.
 *
 * true = Shows
 * false = Hides
 */
define("CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT", true);

/**
 * Enables/Disables filter for search by cheque number, on agent account statement.
 *
 * true = Enables
 * false = Disables
 */
define("CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM", true);

/**
 * To send email to sender/beneficiary on releaseing the transaction.
 * Ticket #4947
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_EMAIL_ON_RELEASE","1");

/**
 *  Ticket #4990
 *	To send email to sender/beneficiary on status change of the transaction, when
 	the Ledger effects (Authorize,Cancelled,Cancelled - Returned,Failed,Picked up,Credited)
 *  NOTE:- For create transaction,release transaction etc.. mails are sent automatically. It can be stopped using 
    their config where mail function is utilized.
 * '1' For enable
 * '0' For disable
 *  by Aslam Shahid
 */
define("CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER","1");
define("CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER","1");


/**
 *  Ticket #4805
 *	It showns ciew document link resptive files agains transactions
 *	which were uploaded by sender.
 * '1' For enable
 * '0' For disable
 *  by Aslam Shahid
 */
define("CONFIG_VIEW_TRANSACTIONS_DOCUMENTS","1"); // on view transaction page
define("CONFIG_AML_AMOUNT_REPORT_DOCUMENTS","1"); // on AML Amount Report
define("CONFIG_TRANS_AML_AMOUNT_REPORT_DOCUMENTS","1"); // on Transaction AML Amount Report
define("CONFIG_SENDER_TRANSACTION_SUMMARY_DOCUMENTS","1"); // on Sender Transaction Summary


/**
 *  Ticket #4946
 *	This will show "Assign Manual Code" interface
 *	page in Left menu. Where by you can assign manually the 
 *	Manual code to the transactions which don't have 
 *	Manual codes yet.
 * '1' For enable
 * '0' For disable
 *  by Aslam Shahid
 */
define("CONFIG_ASSIGN_MANUAL_CODE","1");

/**
 *  Ticket #4948
 * Export transaction links added on Quick menu.
 * '1' For enable
 * '0' For disable
 *  by Aslam Shahid
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT_MUTHOOT","1");

/**
 *  Ticket #5097 
 * Occupation field for Sender as per ticket 4948 requirement.
 * '1' For enable
 * '0' For disable
 *  by Farhan Sethi
 */
define("CONFIG_PROFESSION_ADD_SENDER","1");
define("CONFIG_PROFESSION_FIELD_LABEL","Occupation");

?>