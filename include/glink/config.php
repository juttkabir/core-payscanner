<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","glink");
define("CLIENT_NAME", "glink");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/glink/logo.gif");
define("CONFIG_LOGO_WIDTH","157");
define("CONFIG_LOGO_HEIGHT","63");


/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","0");




/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");



/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");



/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/glink/Terms_and_conditions.doc");
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I accept that I haven't used any illegal ways to earn money. I also accept that I am not going to send this money for any illegal act. I also accept the full terms and conditions of Global Link International Ltd");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");



/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");
define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","1");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");



/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");


/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","0");

/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","0");

/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");


/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");




/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","GL");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Global Code");
define("COMPANY_NAME","Global Link");


define("CONFIG_AGENT_STATMENT_GLINK","1");



/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");




/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");



/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","1");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Company specific information to be used in the system
********************/

define("SUPPORT_EMAIL", "customersupport@global-link.co.uk");
define("INQUIRY_EMAIL", "inquiries@global-link.co.uk");
define("CC_EMAIL", "payex@global-link.co.uk");
define("SUPPORT_NAME", "Company Support");



define("CONFIG_INVOICE_FOOTER", "+44(0)203 031 9030");
define("COMPANY_URL","www.global-link.co.uk");
//define("PAYEX_URL","payex.global-link.co.uk");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");
define("COMPANY_PHONE", "Tel: +44(0)203 031 9030");


define("TITLE","::.Global Link.:: The Fastest Money Transfer....");
define("TITLE_ADMIN", "::...Payex- Global Link ...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Transaction Reference Number patteren with no dash between agent username and transaction counter
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
* if CONFIG_TRANS_REF = 1 and this config is 0 then reference can be like GLUK-110 other wise GLUK110
********************/
define("CONFIG_NO_DASH_REF","1");



/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");


/********************
* Switch to Enable or Disable GLSuper
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SUPER_AGENT","0");


/********************
* Switch to Enable or Disable Pending/Processing Transactions in Ddaily Distributor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DISTRIBUTOR_PENDING","1");


/********************
* Switch to Enable or Disable Branch Code of Collection Point in Distributor Output File
* 0-  Disable
* 1-  Enable
********************/
define(CONFIG_DIST_FIELDS, '0');


/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","DIST_COMM, DAILY_CASHIER, TELLER_ACCOUNT, DAILY_AGENT, OUSTSTANDING_AGENT, RATE_EARNING");



/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Agents Associated to online customers
* Functionality made for GLink
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ONLINE_AGENT","0");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

/********************
* Variable indicating that ID type is mandatory while creating beneficiary/sender
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ID_MANDATORY","0");

/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");   


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");


/********************
* Enable or disbale Editing option for amount and local amount
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EDIT_VALUES","1");

/********************
* Enable or disbale Mobile Compulsion for express & glink
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* Mobile Number format

********************/
define("CONFIG_MOBILE_FORMAT","");



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");

/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","0");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");


/********************
* Switch to Enable/Disable to show the beneficiary verification id distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");

/********************
* Switch to Enable/Disable to show consolidated sender registration report link in left_Express.php
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT", "1");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");



/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "1");

/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is
* CONFIG_COMPLIANCE_CUSTOM_COLUMN,
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "1"); 	 	

/******************** 
* Switch to Enable/Disable Documentation verification module 
* 0 Disable 
* 1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");



/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 	 	
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 	 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");


/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "1");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "1");

define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

/********************
* Added by Niaz Ahmad againist ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad againist ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");

/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");
/********************
* Added by Niaz Ahmad againist ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

/********************
* Switch to Enable/Disable PP type drop down on create transaction page 

* If value is '0' its Disabled
* If value is '1' its Enabled
Added by- javeria ticket #2512-world link
********************/
define("CONFIG_PP_TYPE_ON_TRANSACTION","1");


/********************************************
* added by Niaz Ahmad against #1727 at 08-01-2008
* compliance for transactions greater than a configurable variable for the amount and number of days.
*       0 Disable
*       1 Enable
******************************************/
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");

/********************
* Added by Khola against ticket # 2726 at 10-01-2008
* Additional fields to be shown on quick search when search type is by sender number. 
* Fields are Sender Number, Registration date, ID Type, ID Number 
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FIELDS_FOR_SENDER_NUMBER_SEARCH", "1");


define("CONFIG_TRANS_ROUND_LEVEL", "2");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");



/********************
* fetch sub dist's transactions on DOF
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_SUB_DIST_TRANS","1");

/********************
* Show/Hide CPF number on DOF
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_SHOW_CPF_NUMBER", "1");



/******************** * Variable to carrry ABA and CPF Countaining countries 
* Switch to Enable/Disable the option *
********************/ 
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");




define("CONFIG_GENERIC_RATE_FOR_AGENT","1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_REFNUM_GENERATOR", "0");

/********************
* Switch to Enable/Disable agent's last 30 day transaction for nco global
*       0 Disable
*       1 Enable
********************/
define("CONFIG_AGENT_LAST_30_DAY_TRANSACTION", "1");


/********************
* Enable or Disable
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SHOW_BALANCE","1");

/******************** * Remove currency filter from daily cashier report 
* 1- Hide 
* 0- Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");


/********************
* Switch to SHOW/HIDE company drop down on Distibutor output file for express and glink--added by javeria,#3200
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_SHOW_COMPANY_DOF","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "GHS");

?>
