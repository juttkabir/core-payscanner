<?
define("IMG_PATH_EDITOR","images/Images/");
// Others
define("ADD_ATTRIBUTES", 10);
define("PASSWORD_CHANGE", 30);
if(CONFIG_FEE_DEFINED == '1'){$fee=CONFIG_FEE_NAME;}else{ $fee="Fee";}

//******************** TABLE NAMES *************
define("TBL_ADMIN_USERS", DATABASE. ".admin");
define("TBL_BENEFICIARY", DATABASE. ".beneficiary");
define("TBL_CONTACTS", DATABASE. ".contacts");
define("TBL_FAQS", DATABASE. ".faqs");
define("TBL_CUSTOMER", DATABASE. ".customer");
define("TBL_EXCHANGE_RATES", DATABASE. ".exchangerate");
define("TBL_RELATED_EXCHANGE_RATES", DATABASE. ".relatedExchangeRate");
define("TBL_HISTORICAL_EXCHANGE_RATES", DATABASE. ".historicalExchangeRate");
define("TBL_TRANSACTIONS", DATABASE. ".transactions");
define("TBL_TRANSACTION_EXTENDED", DATABASE. ".transactionExtended");
define("TBL_USER_GROUPS", DATABASE. ".usergroups");
define("TBL_USERS", DATABASE. ".users"); 
define("TBL_SESSIONS", DATABASE.".sessions");
define("TBL_CITIES", DATABASE.".cities");
define("TBL_BANK_DETAILS", DATABASE.".bankDetails");
define("TBL_COLLECTION", DATABASE.".cm_collection_point");
define("TBL_FUNCTIONAL", DATABASE.".user_functionality");
define("TBL_CAMPAIGN_TYPE", DATABASE.".Campaign_Type");
define("TBL_USER", DATABASE.".user");
define("TBL_USERTYPE", DATABASE.".userType");
define("TBL_COUNTRIES", DATABASE.".countries");
define("TBL_BANK_CHARGES", DATABASE.".bankCharges");
define("TBL_COMPLIANCE_MATCH_LIST", DATABASE.".complianceMatchList");
define("TBL_COMPLIANCE_LIST", DATABASE.".complianceLists");
define("TBL_COMPLIANCE_PERSON", DATABASE.".compliancePerson");
define("TBL_COMPLIANCE_LOGIC", DATABASE.".complianceLogic");
define("TBL_COMPLIANCE_QUERY", DATABASE.".complianceQuery");
define("TBL_COMPLIANCE_QUERY_LIST", DATABASE.".complianceQueryList");
define("TBL_COMPLIANCE_OFAC_ADDRESS", DATABASE.".comp_ofac_address");
define("TBL_COMPLIANCE_OFAC_ALT", DATABASE.".comp_ofac_alt");

define("TBL_SETTLEMENT_AMOUNT", DATABASE.".settlementAmount");
define("TBL_EXCH_RATE_USED", DATABASE. ".exRateUsed");
define("TBL_REUSED_DIST_REF", DATABASE. ".reusedDistRef");
define("TBL_IMPORT_CONNECTPLUS_TRANS", DATABASE. ".importConnectplusTrans");
define("TBL_CUSTOMER_CATEGORY", DATABASE. ".customer_category");



//////Accounts/////////agent_Dist_account
define("TBL_SUB_AnD_ACCOUNT", DATABASE.".sub_agent_Dist_account");
define("TBL_SUB_ACCOUNT_SUMMARY", DATABASE.".sub_account_summary");
define("TBL_SUB_AGENT_ACCOUNT", DATABASE.".sub_agent_account");
define("TBL_SUB_DISTRIBUTOR_ACCOUNT", DATABASE.".sub_bank_account");


define("TBL_AnD_ACCOUNT", DATABASE.".agent_Dist_account");
define("TBL_ACCOUNT_SUMMARY", DATABASE.".account_summary");
define("TBL_AGENT_ACCOUNT", DATABASE.".agent_account");
define("TBL_DISTRIBUTOR_ACCOUNT", DATABASE.".bank_account");

/////Agent's Receipt Book Ranges table
define("TBL_RECEIPT_RANGE", DATABASE.".receipt_range");

/////Distributor's Clave Book Ranges table
define("TBL_CLAVE_RANGE", DATABASE.".clave_range");

//////// Teller ////
define("TBL_TELLER", DATABASE.".teller");
define("TBL_TELLER_LIMIT", DATABASE.".teller_account_limit");
define("TBL_TELLER_ACCOUNT", DATABASE.".teller_account");
                                                                
///Services////
define("TBL_COUNTRY", DATABASE. ".countries");                                                                     
define("TBL_SERVICE", DATABASE. ".services");
define("TBL_SERVICE_NEW", DATABASE . ".servicesNew");

///Currency////
define("TBL_CURRENCY", DATABASE. ".currencies");                                                                     
define("TBL_CURRENCY_STOCK_LIST", DATABASE. ".currency_stock");
define("TBL_CURRENCY_STOCK_HISTORY", DATABASE. ".currency_stock_history");

///SeviceNotAvaialbe///
define("TBL_SRV_NOT_AVAILABLE", DATABASE.".homeDeliveryService");
                                                                                                                                  
///////Fee
define("TBL_IMFEE", DATABASE. ".imfee");
define("TBL_MANAGE_FEE", DATABASE. ".manageFee");


//////To maintain the History
define("TBL_AMENDED_TRANSACTIONS", DATABASE. ".amended_transactions");
define("TBL_LOGIN_HISTORY", DATABASE.".login_history");
define("TBL_LOGIN_ACTIVITIES", DATABASE.".login_activities");

// Complaints
define("TBL_COMPLAINTS", DATABASE.".complaints");
define("TBL_COMPLAINTS_INTER", DATABASE.".complaintInteracts");
define("TBL_AGENT_CUSTOMER", DATABASE.".agents_customer_account");
define("TBL_ENQUIRY", DATABASE.".forum");
define("TBL_ENQUIRY_THREAD", DATABASE.".forum_thread");

////Demonination
define("TBL_CURRENCYNOTES", DATABASE.".currencyDenomination");

////cash report tables
define("TBL_CASHIER_CURRENCY", DATABASE.".cashierCurrency");
define("TBL_CASHIER_COLLECT_AMOUNT", DATABASE.".cashierCollectAmount");

// Debuggers On and Off Parameters
define("DEBUG","True");
define("LOG","False");

//// Export File 
define("TBL_EXPORT_TEMPLATE", DATABASE.".tbl_export_template");
define("TBL_EXPORT_FIELDS", DATABASE.".tbl_export_fields");
define("TBL_EXPORT_FILE", DATABASE.".tbl_export_file");
define("TBL_EXPORT_LABELS", DATABASE.".tbl_export_labels");
define("TBL_EXPORT_DEFAULT", DATABASE.".tbl_export_default");

//// Shortcut table
define("TBL_SHORTCUTS", DATABASE . ".shortcuts");


/////////////////Profit Earning Calculation
define("TBL_PROFIT_EARNING", DATABASE.".profit_earning");


/////////////////Profit Sharing Configuration for Spinzar particularly
define("TBL_SHARE_CONFIG", DATABASE.".shareConfig");
define("TBL_SHARE_PROFIT", DATABASE.".shareProfit");
define("TBL_DISTRIBUTOR_COMM", DATABASE.".configure_commission");
define("TBL_SHARE_COUNTRIES", DATABASE.".shareCountries");


/////////////////Profit Earning Calculation
define("TBL_TRANSACTION_LIMIT", DATABASE.".trans_limit");

// Trading / Double Entry
define("TBL_ACCOUNTS", DATABASE.".accounts");
define("TBL_ACCOUNTS_CHART", DATABASE.".accounts_chart");
define("TBL_ACCOUNT_DETAILS", DATABASE.".account_details");
define("TBL_ACCOUNT_LEDGERS", DATABASE.".account_ledgers");

define("TBL_CURR_EXCHANGE_ACCOUNT", DATABASE.".curr_exchange_account");
define("TBL_TT_TRANSACTIONS", DATABASE.".tt_transactions");

// Pictues Size definition
//define ("PIC_SIZE","1000000");

// URL definition parameters
define("URL" , dirname("http://" . $_SERVER['SERVER_NAME']  . $_SERVER['REQUEST_URI'] ) . "/");

// LIVE URL
define("LURL", "http://www.horivert.co.uk/");



/////////////////Config Table to set client configurations
define("TBL_CONFIG", DATABASE . ".config");
define("TBL_JOINTCLIENT", DATABASE . ".jointClients");
define("TBL_SHAREDUSERS", DATABASE . ".sharedUsers");
define("TBL_SHARED_TRANSACTIONS", DATABASE . ".sharedTransactions");
define("TBL_SHARED_FEE", DATABASE . ".SharedFee");
define("TBL_SHARED_EXCHANGE", DATABASE . ".SharedExchangeRate");
// Pictues Size definition
//define ("PIC_SIZE","1000000");

// ERRORS and MESSAGES
define("ERR1","Please log in to access this page.");
define("ERR4","You must upload a valid JPG, GIF, or PNG file!");
define("ERR19","Invalid username or password.");

// Agents
define("AG1","Please provide company name.");
define("AG2","Please provide contact person name.");
define("AG3","Please provide company address line 1.");
define("AG4","Please select country.");
define("AG5","Please select city.");
define("AG6","Please provide zip/postal code.");
define("AG7","Please provide phone number.");
define("AG8","Please provide email address.");
define("AG9","Please provide company's director's name.");
define("AG10","Please provide bank name.");
define("AG11","Please provide account name.");
define("AG12","Please provide account number.");
define("AG13","Please provide currency.");
define("AG14","Please provide account limit.");
define("AG15","Please provide commission percentage.");
define("AG16","Super Agent added successfully.");
define("AG17","Record updated successfully.");
define("AG18","Super Agent suspended successfully.");
define("AG19","Super Agent activated successfully.");
define("AG20","Super Agent disabled successfully.");

$payingBookLabel = "Payin Book";

if(CONFIG_PAYIN_CUSTOMER == '1')
{
	if ( defined("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW")
		 && CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == 1
		 && defined("CONFIG_PAYIN_CUSTOMER_LABEL") )
	{
		$payingBookLabel = CONFIG_PAYIN_CUSTOMER_LABEL;
	}
}

define("AG46","Please Provide " . $payingBookLabel . " Number.");
define("AG47","Please Check " . $payingBookLabel . " No.");
define("AG48","Provide Numeric " . $payingBookLabel . " Number.");
define("AG49","Please provide the " . $payingBookLabel . " number not less than");
define("AG50","Please provide the " . $payingBookLabel . " number not greater than");
define("AG51","Please provide the " . $payingBookLabel . " number");
define("AG52",$payingBookLabel . " No all ready exists");
define("AG53","Agent '");
define("AG54","' for the oiginating country all ready exists");

//Sub Agent
define("AG21","Please select Super Agent from the list.");
define("AG22","Sub Agent added successfully.");
define("AG23","Sub Agent updated successfully.");
//Admin
define("AG24","Please Provide Login Name.");
define("AG25","Please Provide Password.");
define("AG26","Please Confirm Password.");
define("AG27","Password fields do not match.");
define("AG28","Please assign at least one Right to the Admin.");
define("AG29","Please Provide email address.");
define("AG30","Admin User Added successfully.");
define("AG31","Admin User Updated successfully.");
define("AG32","Admin with this Login already exists. Please provide another Login Name.");
//Branch Manager
define("AG33","Branch Manager updated successfully.");
define("AG34","Branch Manager added successfully.");
//Payin Book Customers
define("AG35","" . $payingBookLabel . " Number already exists. Please provide another Payin Book Number.");
//Collection Point
define("AG36","Collection Point deleted successfully.");
//Super Admin
define("AG37","Super Admin disabled successfully.");
define("AG38","Selected Super Admin is activated successfully.");
define("AG39","Super Admin suspended successfully.");
//Create Transaction Message
define("AG40","Your daily account limit has exceeded, Please contact your admin.");
define("AG41","Please provide country to send money.");
define("AG42","Please provide agent's Login Name.");
define("AG43","Login Name already exists.");
define("AG44","ID Expiry date should be greater than current date.");
define("AG45","Please select atleast one country for customers.");
define("AG55","ID issue date should be less than current date.");


// Off-line Customers
define("CU1","Please select agent from the list to add customer.");
define("CU2","Please provide customer's first name.");
define("CU3","Please provide customer's last name.");
define("CU4","Please provide customer's ID number.");
define("CU5","Please provide customer's address.");
define("CU6","Please select customer's country.");
define("CU7","Please provide customer's city.");
define("CU8","Please provide customer's contact number.");
define("CU9","Please provide customer's email.");
define("CU10","Sender added successfully.");
define("CU11","Sender updated successfully.");
define("CU12","Please provide Numeric value for " . $payingBookLabel . ".");
define("CU13","Please select the " . $payingBookLabel . " check box.");
define("CU14","Please provide the " . $payingBookLabel . " number.");
define("CU15","Please provide the " . $payingBookLabel . " number not less than");
define("CU16","Please provide the " . $payingBookLabel . " number not greater than");
define("CU17","Please provide correct Customer/Sender Number to Deposit Money.");
define("CU18","Please provide correct Online Customer/Sender Number to Deposit Money.");
define("CU19","Please Select ID Type.");
define("CU20","Please provide Customer Mobile Number.");
define("CU21","Please provide Mobile Number in correct format.");
define("CU22","Customer");
define("CU23","Already exists.");
define("CU24","Please check prove Address Check Box.");
define("CU25","Please check Documents Provided Check Box.");


// Beneficiary
define("BE1","Please provide Beneficiary's first name.");
define("BE2","Please provide Beneficiary's last name.");
define("BE3","Please provide Beneficiary's ID number.");
define("BE4","Please provide Beneficiary's Address.");
define("BE5","Please select Beneficiary's country.");
define("BE6","Please provide Beneficiary's city.");
define("BE7","Please provide Beneficiary's contact number.");
define("BE8","Please provide Beneficiary's email.");
define("BE9","Beneficiary Added successfully.");
define("BE10","Beneficiary updated successfully.");
define("BE11","Please provide the correct CPF number.");
define("BE12","ID Expiry date should be greater than current date.");
define("BE13","Please provide Beneficiary ID Type.");
define("BE14","Customer");
define("BE15","already exists.");
define("BE16","ID issue date should not be greater than current date.");
define("BE17","Please provide Beneficiary's mobile number.");
define("BE18","CPF Number is not Valid.");
define("BE19","CPF Number is Valid.");

// IM Fee
define("IMF1", "Please select Originating Country.");
define("IMF2", "Please provide Lower range of fee.");
define("IMF3", "Please provide Upper range of fee.");
define("IMF4", "Please provide valid value for Upper range that must be greater than Lower range.");
define("IMF5", "Please provide  $fee for specified range of money transfer.");
define("IMF6", " $fee for selected country and specified range of amount has been added successfully.");
define("IMF7", " $fee for selected country and specified range of amount has been updated successfully.");
define("IMF8", "Please select destination Country.");
define("IMF9", "Please select $fee Type.");
define("IMF10", "Please select Agent from the List.");
define("IMF11", "This range of $fee already exist.");
define("IMF12", "Please provide the interval.");
define("IMF13", "Please select Sending currency.");
define("IMF14", "Please select Receiving currency.");
define("IMF15", "Please select Collection Point from the list.");
define("IMF16", "Please select Distributor from the list.");

/////////Manage Bank Charges
define("BC1", "Please select Originating Country.");
define("BC2", "Please provide Lower range of Amount.");
define("BC3", "Please provide Upper range of Amount.");
define("BC4", "Please provide valid value for Upper range that must be greater than Lower range.");
define("BC5", "Please provide  Bank Charges for specified range of Amount.");
define("BC6", " Bank Charges for selected country and specified range of amount has been added successfully.");
define("BC7", " Bank Charges for selected country and specified range of amount has been updated successfully.");
define("BC8", "Please select destination Country.");
define("BC9", "Please select Charges Type.");
define("BC10", "Please select Distributor from the List.");
define("BC11", "This range of Charges already exist.");
define("BC12", "Please select Sending currency.");
define("BC13", "Please select Receiving currency.");


//add bank 
define("B1", "The bank id already exists in the database please provide new bank id.");


// Exchange Rates
define("ER1", "Please select Orignating Country.");
define("ER2", "Please select Destination Country.");
define("ER4", "Please provide primary exchange rate.");
define("ER5", "Please provide margin exchange rate.");
define("ER6", "Please provide net exchange rate.");
define("ER7", "Exchange rate added successfully.");
define("ER8", "Exchange rate updated successfully.");
define("ER9", "Please select a Currency.");
define("ER10", "Please Select an Option to apply Exchange Rate.");
define("ER11", "Please select a Distributor Name from the List.");
define("ER12", "Please provide the amount range to apply exchange rate.");
define("ER13", "Please Select distributor.");
define("ER14", "Please provide primary intermediate exchange rate.");
define("ER15", "Please provide secondary intermediate exchange rate.");
define("ER16", "Please select intermediate currency.");
define("ER17", "Please select customer category.");

// General Errors and messages
define("MSG1", "Please provide either your login name or email to retreive your password.");


// FAQs
define("FA1", "Please select FAQ type.");
define("FA2", "Please enter question.");
define("FA3", "Please enter answer.");
define("FA4", "FAQ added successfully.");
define("FA5", "FAQ updated successfully.");
define("FA6", "FAQ(s) removed successfully.");

// TRANSACTIONS ERROR MESSAGES
define("TE1","Please enter a valid amount.");
/**
 * Against ticket no 3319
 */
if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1")
	define("TE2","The  $fee range for amount you entered is invalid or your commission does not based on Payment Mode/Money Paid.");
else
	define("TE2","Please enter correct Commission. Zero as commission is not allowed.");
define("TE3","Please select transaction type.");
define("TE4","Please select a cutomer/sender to send money.");
define("TE5","Please select Distributor for beneficiary to collect funds.");
define("TE6","Please select beneficiary from the list.");
define("TE7","Invalid reference number.");
define("TE8","Please write amount in the Amount field.");
define("TE9","Amount to transfer is not valid.");
define("TE10","Please select transaction purpose.");
define("TE11","Please select fund sources.");
define("TE12","Please select money paid in the form of.");
define("TE13","Please accept the Terms & Conditions.");
define("TE14","Please provide the Bank Name.");
define("TE15","Please provide the Branch Code/Number.");
define("TE16","Please provide the Branch Address.");
define("TE17","Please provide the Swift Code.");
define("TE18","Please provide the bank account number.");
define("TE19","Please provide ABA Number.");
define("TE20","Please provide CPF Number.");
define("TE21","Please provide IBAN Number.");
define("TE22","Home Delivery Service is not Available for this city. Please change city or transaction type.");

//Online Customers
define("TE23","Please provide your secret question.");
define("TE24","Please provide  Answer of your secret question.");
////////////
define("TE25","Please provide Discounted Charges less than or equal to Original Charges.");
define("TE26","Exchange Rate Margin is More than Agent Commission.");
define("TE27","Please provide ".MANUAL_CODE.".");

if(CONFIG_CLIENT_NAME == 'glink')
{
	define("TE28","Agent's Account Limit acceeded. Please call credit control on 02030319032");
}else{
	define("TE28","Agent's Account Limit does not allow to create transaction.");
}
define("TE29","Please select a sender bank from the list.");
define("TE30", "Please select a Fund Source from the list");
define("TE31","Provide Beneficiary Verification ID.");
define("TE32","Please Provide Value date of transaction.");
define("TBL_CM_CUSTOMER", DATABASE. ".cm_customer");
define("TBL_CM_BENEFICIARY", DATABASE. ".cm_beneficiary");
define("TBL_CM_BANK_DETAILS", DATABASE.".cm_bankdetails");

define("TBL_BEN_BANK_DETAILS", DATABASE.".ben_banks");
define("TBL_BANKS", DATABASE.".banks");

//Agent Receipt Range Messages for express
define("ARR1", "Range is saved successfully");
define("ARR4", "Range is updated successfully");
if(CONFIG_DIST_REF_NUMBER == "1"){
	$value = "Distributor";
}else{
	$value = "Agent";
}

define("ARR0", "Prefix for the selected Distributor already exists");
define("ARR2", "This range already exists against selected ".$value);

define("ARR3", "This range already exists For selected Distributor");
define("ARR5", "Records are Disabled Successfully");
define("ARR6", "Record is Activated Successfully");
define("ARR7", "Record is saved successfully");
//Online Customer Registration
define("CUS1","Please provide another Login, User with this Login already Exists");

//Credit Card Validation
define("CR1","Please provide Card Type.");
define("CR2","Please provide Card No.");
define("CR3","Please provide Expiry Date.");
define("CR4","Please provide Card CVV or CVV2.");
define("CR5","Please provide First Name.");
define("CR6","Please provide Last Name.");
define("CR7","Please provide Address1.");
define("CR8","Please provide Address2.");
define("CR9","Please provide City Name.");
define("CR10","Please provide State Name.");
define("CR11","Please provide Postcode/ZIP.");
define("CR12","Please provide Country.");

define("PIN1","Please provide Valid PIN Code No.");

// Configurations Messages
define("CONFMSG1", "Record updated successfully.");
define("CONFMSG2", "Default configurtions have been set successfully.");


///// Feedback Email Address. Means to whom feedback should be sent
define("FEEDBACK_EMAIL_ADDRESS", "payex-feedback@support.horivert.com");

/////Tool Tips for Reports

/////Agent Commission Report
define("agentcomid","Agent Login");
define("agentcomn","Agent Full Name");
define("agentcomtrans","Amount To be sent Exclusive of Fee");
define("agentcomcomf","Fee charged apart from the transaction amount");
define("agentcomcom","Commission for the agent exclusive of the fee and transaction amount");
define("agentcomc","The commission of the company after deduction of agent");
define("agentcomtotal","Sum of the transaction amount, fee and commission");
define("agentcomtranscount","Total Transactions done by the Agent in the time filter");

/////A&D Account Statement
define("adate","Date");
define("amodify","The person who authorised or deposit the money");
define("amoneyin","Money Deposited");
define("amoneyout","Money withdrawn/paid");
define("atrans","The transaction reference number");
define("ades","Reason for money paid or deposited.");
define("astatus","Status of the relevant transaction(s)");

/////Daily Distributor Report
define("dbankn","Name of the Distributor");
define("damntsent","The amount collected by beneficiary");
define("datrizcont","Number of authorised transaction");

/////Distributor Statement
define("dsdate","Date");
define("dsmodby","The person who authorised the transaction");
define("dsmonin","Money Deposited");
define("dsmonout","Money withdrawn/paid");

////Payout Center Report
define("ppaycentercity","City containing the collection points");
define("pamntsent","The amount sent by sender in sending currency");
define("pforgnamnt","The amount picked up by beneficiary in local currency");
define("ptrnscount","Total Transactions done by the agent in the time filter");

/////Daily Transaction Summary
define("dadate","Date");
define("datotaltrns","Total transactions on the DATE");
define("datrnsamnt","Amount being sent");
define("dacomfee","Company fee to be changed");

////Transaction Transfer Report
define("ttrefcode","Transaction reference number");
define("ttcomcode","Company Code for the agent");
define("ttsendname","Sender Name");
define("ttbnfname","Recipient Name");
define("ttamntsent","Amount on the sendig end");
define("ttexhrate","Exchange Rate for Receiving Country");
define("ttfrgnamnt","Amount Received");
define("ttbankname","Name of the Distributor");
define("ttpikloc","Collection Point for the Distributor");
define("ttagntname","The Agent Performing the Transaction for the sender");

/////Sender/Beneficiary Report
define("sbrefcode","Transaction Reference Number");
define("sbcomcode","Company Code for the Agent");
define("sbsendname","Sender Name");
define("sbbnfname","Recipient Name");
define("sbamntsent","Amount on the Sending end");
define("sbexhrate","Exchange Rate for Receiving Country");
define("sbfrgamnt","Amount Received");
define("sbbnkname","Name of the Distributor");

////Sender Registration Report
define("srcustid","Sender Number");
define("srcustname","Sender Name");
define("srcustcity","Sender City");
define("srcountry","Sender Country");
define("srregdate","Registration Date with the Company");
define("sremail","Sender Email");

////Agent Account Balance
define("aaagntname","Login and Name of the Agent");
define("aagrstransamnt","Total Amount Including fee and Commission");
define("aacshpayrec","The Cash Received from Agent");
define("aabnkpayrec","The Money Deposited in the Bank Account from Agent");
define("aacantrns","The Total of the Cancelled Transaction Amount During the Date Range Selected");
define("aabalance","The Balance of Ballance=Gross Transaction Amount-(Cash Payment Amount+Bank Payment Amount + Cancelled Transaction Amount)");

////Agent Country Base Transactions
define("accntname","Name of the Country");
define("acdistname","Name of the Distributor");
define("acnotrns","Total Number of Transactions for that country");
define("acincome","Commission Earned by the Distributor");

/////Agent Statement
define("asdate","Date");
define("asopnbalance","The previous closing day balance become opening balance");
define("asmoneyin","The money deposited via bank payment file or manual");
define("asmoneyout","The transaction amount in sending currency");
define("astrns","Reference number with the hyperlink");
define("asdes","The pre-defined description i.e Transaction created or Transaction Failed or Manual Deposit");
define("asnote","Any note written when some one deposit the cash manual in agent account");
define("asclosbalance","Closing balance=Money In-Money Out");

/////Agent Account Statement
define("a1","Date");
define("a2","The person who authorised the transaction or deposit the money");
define("a3","The previous closing day balance become opening balance");
define("a4","Money Deposited");
define("a5","Money Withdrawn/Transaction amount in sending currency");
define("a6","Tramsaction Reference Number");
define("a7","Company Code for the transaction");
define("a8","Description of the transaction");
define("a9","Reason for withdraw/deposit of money");
define("a10","Status of the transaction");
define("a11","Closing balance=Money In-Money Out");

////Sub A&D Account Statement
define("subdate","Date");
define("submodby","The person who authorised the transaction or deposit the money");
define("submonin","Money deposited");
define("submonout","Money Withdrawn/Paid");
define("subrefcode","Transaction reference number");
define("subdes","Description of the transaction");

////Bank Account Statement
define("bnkdate","Date");
define("bnkmodby","The person who authorised the transaction or deposit the money");
define("bnkmonin","Money Deposited");
define("bnkmonout","Money Withdrawn/Paid");

/////Account Summary Report
define("accagent","Agent");
define("accamntused","In case of agent it will calculate the Transaction amount(in sending currency)+Transaction Fees+Admin Charges. In Case of Distributor it will show the amount of transaction only.");
define("accamntpaid","When you deposit the money in agent or distributor account through agent account statement. That amount will be shown.");
define("accopnbalance","For new agent or distributor the opening balance will be 0 for current date. For regular agent or distributor opening balance will be the closing balance of previous date.");
define("accrunbalance","The calculation behind is following. Amount Paid(Cr)-Amount Used(Dr)=Running Balance");
define("accclosebalance","Opening Balance + Runing Balance");

/////Audit Trial Window
define("audlogintime","Time Stamp of user login");
define("audusername","Name of the user");
define("audusertype","User Type");
define("audipadd","IP address of accessing machine");

////Transactions Audit Window
define("trnscreat","Date of creation of transaction");
define("trnsref","Transaction Reference Number");
define("trnscom","Company Transaction Code");
define("trnsinputt","Transaction Created By");



////Commision summary Report tables
define("TBL_EMAIL", DATABASE.".email");
define("TBL_EMAIL_REMINDER", DATABASE.".emailReminder");
define("TBL_REMINDER_REPORT", DATABASE.".reminderReport");
define("TBL_EMAIL_ALERT", DATABASE.".emailAlert");


///document uploading tables////
define("TBL_DOCUMENT_NAMES", DATABASE. ".document_names");                                                                     
define("TBL_DOCUMENT_CATEGORY", DATABASE. ".document_category");
define("TBL_DOCUMENT_UPLOAD", DATABASE. ".document_upload");
define("TBL_CATEGORY", DATABASE. ".category");
define("TBL_USER_ID_TYPES", DATABASE. ".cm_user_id_types");

// Multiple ID Type Tables //

define("TBL_MULTIPLE_USER_ID_TYPES", DATABASE. ".user_id_types");
define("TBL_ID_TYPES", DATABASE. ". id_types");

///view////
define("VW_TRANSACTION_SUMMARY", DATABASE. ".transactionSummary");
define("VW_BENFICIARY_AGENT", DATABASE. ".benAgent");

///error for fax output report

define("r01","Select collection point to search");




?>
