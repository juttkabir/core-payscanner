<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Spinzar Ltd");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/spinzar/spinzarlogo.png");
define("CONFIG_LOGO_WIDTH","240");
define("CONFIG_LOGO_HEIGHT","89");


/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");




/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");

/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");


/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");



/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/glink/Terms_and_conditions.doc");
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I accept that I haven't used any illegal ways to earn money. I also accept that I am not going to send this money for any illegal act. I also accept the full terms and conditions of Global Link International Ltd");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","1");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","1");



/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","1");


/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE", "0");


/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");



/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");



/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "1");



/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");


/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");

/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","0");

/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER", "0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT", "0");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW", "0");
define("CONFIG_PAYIN_CUSTOMER_LABEL", "Walkin Sender");

/********************
* This variable will make every customer as PayinBook Customer 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AUTO_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");


/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "0");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");




/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","1");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","S");
define("SYSTEM_CODE","Ref Code");
define("MANUAL_CODE","Spinzar Manual Code");
define("COMPANY_NAME","Spinzar Ltd");
define("CLIENT_NAME", "Spinzar");
define("CONFIG_AGENT_STATMENT_GLINK","0");

/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");




/********************
* Maximum Number of Transactions viewed per page
********************/
define("CONFIG_MAX_TRANSACTIONS","50");



/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","0");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Company specific information to be used in the system
********************/

define("SUPPORT_EMAIL", "noreply@companydomain.com");
define("INQUIRY_EMAIL", "spinzarltd@yahoo.co.uk");
define("CC_EMAIL", "spinzarltd@yahoo.co.uk");
define("SUPPORT_NAME", "Spinzar");



define("CONFIG_INVOICE_FOOTER", "+44(0)208 993 4236");
define("COMPANY_URL","www.spinzar.co.uk");
//define("PAYEX_URL","payex.global-link.co.uk");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","::.Spinzar Ltd.::...");
define("TITLE_ADMIN", "::...Payex- Spinzar Ltd ...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","Province");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","1");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","0");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");


/********************
* Switch to Enable or Disable GLSuper
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SUPER_AGENT","0");


/********************
* Switch to Enable or Disable Pending/Processing Transactions in daily Distributor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DISTRIBUTOR_PENDING","0");


/********************
* Switch to Enable or Disable Branch Code of Collection Point in Distributor Output File
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIST_FIELDS", "0");


/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","DIST_COMM, DAILY_CASHIER, TELLER_ACCOUNT, DAILY_AGENT, OUSTSTANDING_AGENT, RATE_EARNING");



/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");



/******************** 
* Switch to Enable or Disable Zero(0) Commission(Fee) 
* 0- Disable 
* 1- Enables
********************/ 
define("CONFIG_ZERO_FEE","1");

/********************
* Switch to Enable/Disable Calculating Transaction Amounts from Total Amount
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CALCULATE_FROM_TOTAL","1");

/********************
* Switch to Enable/Disable Spinzar Transaction Number
* It will be like this "SUK000001".
* S for Spinzar, UK for sending counrty code, 000001 for auto generated number
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_SPINZAR_TRANS_NUM", "1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/******************** 
* Peshwar Commission report especialy for spinzar  
*       0 means Disable 
*       1 means Enable  
********************/ 
define("CONFIG_PESHAWAR_REPORT", "1"); 
 
 
/******************** 
* Switch to Enable/Disable Search Beneficiary  
* and then ability to create transaction  
*       0 Disable 
*       1 Enable  
********************/ 
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Sub Account Statement Reports
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUB_ACCOUNT_REPORT", "1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark_green.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#00CC00");
define("CAUTION_COLOR", "#CC0000");

/********************
* Switch to Enable/Disable multiple links to Release Transactions 
*	according to Sub Distributors
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUB_DISTRIBUTOR_BASE","1");


/********************
* Switch to Enable/Disable Exchange Rate Margin Type 
*	built for Spinzar
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");


/********************
* Switch to Enable/Disable Exchange Rate Calculatio Report for 
* Agents and Company
* Built for Spinzar
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_SHARE","1");

/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");

/********************
* Switch to Enable/Disable non compulsory phone field
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PHONE_NON_COMPULSORY", "1");

/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT", "0");
define("CONFIG_RECIEPT_NAME", "confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT", "print-confirmed-transaction-spinzar.php");
define("CONFIG_RECEIPT_FORM", "reciept_form.php");
/********************
* Switch to Enable or Disable to Skip Confirm Transaction Step
* while creating a transaction
* For Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_SKIP_CONFIRM","0");

/********************
* Switch to Enable or Disable to show standard payex reciept
* while creating a transaction
* For Opal
* 0-  Disable
* 1-  Enables
********************/

define("CONFIG_SHOW_DEFAULT_RECIEPT","1");

define("CONFIG_SO_BEN","1");
/********************
* Added by Niaz Ahmad #1727 at 08-01-2008
* 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT","1");
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");
define("CONFIG_TRANS_ROUND_LEVEL", "2");

/********************
* Added by Niaz Ahmad #2879 at 18-03-2008
* 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTOM_TRANSACTION", "0");
define("CONFIG_TRANSACTION_PAGE", "add-transaction_opaldesign.php");
//define("CONFIG_MONEY_PAID_NON_MANDATORY", "1");

/********************
* Added by Niaz Ahmad #2831 at 24-03-2008
* 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_BENEFICIARY_CITY_DROPDOWN", "1");

/********************
* Added by Niaz Ahmad #2879 at 26-03-2008 at create transaction page
* to  make Transaction Purpose drop down non mandatory
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");
/********************
* Added by Niaz Ahmad at 26-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "0");


/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");

/********************
* Switch to Enable/Disable sender ID date alert
* 0 Disable
* 1 Enable
********************/
define("CONFIG_ALERT", "1");
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php");
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");

/********************
* Added by Niaz Ahmad #3023  at 26-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************    
* Added By Niaz Ahmad #2918 at 06-02-2008                                                                     
* Switch to Enable/Disable calculation of cumulative amount for specific period
* When Config ON calneder will be shown at Compliance Configurations At Create Transaction
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_COMPLIANCE_NUM_OF_DAYS", "1");
/********************
* Added by Niaz Ahmad Spinzar Meeting Minutes  at 31-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_AGENT_ACCOUNT_STATEMENT", "1");
define("CONFIG_AnD_ACCOUNT_STATEMENT", "1");
define("CONFIG_BANK_ACCOUNT_STATEMENT", "1");
define("CONFIG_SINGLE_PRINT", "1");
/********************
* Added by Niaz Ahmad Spinzar Meeting Minutes  at 31-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADD_SHORTCUT", "1");
/********************
* Added by Niaz Ahmad Spinzar Meeting Minutes  at 31-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_VIEW_CURRENCY_LABLES", "1");
define("CONFIG_ADD_AMOUNT_COLUMN", "1");
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");
define("CONFIG_AGENT_STATEMENT_CUMULATIVE", "1"); // to hide comulative total from top of super A&D Report
define("CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT", "1");
/********************
* Added by Niaz Ahmad #2829  at 16-04-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_BRANCH_NAME", "1");

/********************
* Added by Niaz Ahmad #3085  at 22-04-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADD_FIELDS_RECEIVING_RECEIPTS", "1");
define("CONFIG_NONCOMPUL_FUNDSOURCES", "1");

/********************
* show hide Quick Search on main menu
added by javeria on 07 April 2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ENABLE_QUICK_SEARCH", "1");

/********************
* Give ability to user to import transaction into Payex 
*	0 Disable
*	1 Enable 
********************/

define("CONFIG_IMPORT_TRANSACTION","1");

?>
