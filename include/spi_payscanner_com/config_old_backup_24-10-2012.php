<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Premier FX");

define("CONFIG_SYSTEM_WEBSITE_URL", "Go to Demo FX |http://demofx.com/");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/premier/logo.jpg?v=2");
define("CONFIG_LOGO_WIDTH","170");
define("CONFIG_LOGO_HEIGHT","60");
define("CONFIG_OTHER_LOGO","../admin/images/premier/logo.jpg?v=2");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Config is being turned off by Omair Anwer under ticket # 5539
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","1");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
//define("CONFIG_TRANS_COND","I accept that I have not used any illegal means to earn the money being sent. I accept that I am not sending this money for any illegal act. I also accept the full Terms & Conditions of Premier Fx");

/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","1");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");


/**
 * introducing the new const for handling the commission based on payment mode
 * If enabled then the commission will be based on the payment mode
 * @Ticket# 3319
 * @Author Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_ADD_PAYMENT_MODE_BASED_COMM","0");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","PE");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Manual Reference Number");
define("COMPANY_NAME","Premier FX Currency Exchange");
define("COMPANY_NAME_EXTENSION","Premier Exchange Money Transfer");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "info@premfx.com");
define("INQUIRY_EMAIL", "info@premfx.com");
define("CC_EMAIL", "");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Payex Support");


define("CONFIG_INVOICE_FOOTER", "");
define("COMPANY_URL","horivert.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Premier Exchange");
define("TITLE_ADMIN", "Premier Exchange");


define("COMPANY_ADDRESS","London");
define("COMPANY_PHONE", "");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");
define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","0");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
//define("CONFIG_CUSTOM_RECIEPT","1");
//define("CONFIG_RECIEPT_NAME","confirm-transaction.php");
//define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction.php");
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","confirmTransactionPremierReceipt.php");
define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction-premier.php");
/*
* #5292 - AMB Exchange
* If transaction type is Bank Transfer then this Receipt will be displayed 
* only if CONFIG_CUSTOM_RECIEPT in ON.
* Enable  = Receipt File Name like "abc.php"
* Disable = "0"
* by Aslam Shahid
*/
//define("CONFIG_RECIEPT_PRINT_BANK","print-confirmed-transaction-ambExchange-Bank.php");
define("CONFIG_RECIEPT_PRINT_BANK","print-confirmed-transaction-premier.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","0");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","1");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","0");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","0");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)

* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "1");
define("CONFIG_PAYIN_RESTRICT_LIST", "");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Money In");
define("CONFIG_WITHDRAW","Money Out");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
//define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","0");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","1"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "premierexchange");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");




/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");



/**
 * Adding config to handle the bank details of the beneficiary
 * @Ticket# 3320
 * @Date 25th June, 08.
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_BEN_BANK_DETAILS","0");


/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
//define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,By Cash,By Cheque,");
define("CONFIG_TRANS_ROUND_CURRENCY", "EUR,GBP,USD,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_FROM,");
define("CONFIG_TRANS_ROUND_LEVEL", "2");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "0"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "0"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertControllerNew.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "GHS");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "0");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "0");


/**
 * Adding a const to handle the appearance  of distributor at the add transaction case
 * Provideed if the the transfer type is bank selected
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 * @Ticket# 3321 / 3320
 */
define("CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS","0");


/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
*	#5001
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");

/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");


/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "1");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Full UK Driving License");

/********************

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");
/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "0");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");


/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "1");


/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable and Config CONFIG_EXACT_MATCH_CUST_SEARCH should be '0'
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
//define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "0");

/**
 * Select the default Money Paid option to select One 
 * @Ticket# 3517
 * @AddedBy Jahangir
 */

define("CONFIG_MONEY_PAID_SELECT_ONE",1);

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

define("CONFIG_ACCOUNT_TYPE_ENABLED","0");
//define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");

/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","0");

/********************
* Switch to use optimise search for agent on view transactions page--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMISE_AGENT_SEARCH","1");



/********************
* Switch to use optimise reference number search--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMIZE_REFERENCE_NO","1");



define ("CONFIG_VIEW_CURRENCY_LABLES","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

/**
 * Adding a new field containg the client refrence number
 * @ticket 3321 / 3320
 */
define("CONFIG_CLIENT_REF", "0");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
* 1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");

/********************
* Added By Khola Rasheed against ticket #3318: Minas Center - Bank Transfer Types
* Switch to Enable/Disable 24 hour banking module which addes 24 hour banking exchange rate 
* and create transaction has banking info whether normal or 24 hr
* 0 Disable
* 1 Enable
********************/
define("CONFIG_24HR_BANKING", "0");

/**
 * Setting the default transaction type to bank transfer
 * @Ticket# 3321 / 3320
 */
define("CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER","Bank Transfer");

/********************
* Added by Usman Ghani against #3321 minas center import sender
* To import a custom csv file.
* 0 Off
* 1 On
********************/
define("CONFIG_IMPORT_CUSTOM_SENDERS_DATA", "1");

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", "1");

/* Money paid as non mandatory*/
define("CONFIG_MONEY_PAID_NON_MANDATORY","1");

/**
 * To display custom list of banks in Bank Search Module.
 * @Ticket: #3538: Minas Center - Bank List Problem
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}
 */
define("CONFIG_CUSTOM_BANK_SEARCH_RESULTS", "1");


/**
 * To show a custom template file for minas center only.
 * @Ticket: #3526: Minas Center-Change the sample file of Import Beneficiary to the latest one.
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}.
 */
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE", "1");
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME", "sampleTempBen_minasCenter.xls");


/**************************************************************
* Added by Javeria against #3546:
add sending bank on create transaction page depending on money paid's value
* 0 Off
* 1 On
***************************************************************/
//define("CONFIG_ADD_SENDING_BANK_ON_CREATE_TRANSACTION", "0");


/**
 * Adding the bank based trasactions capability
 * this config will control the main.php entery against the bank based transactions
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT","0");


/**
 * View senders / customer docuements at create transaction
 * @Ticket# 3779
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION","1");

/** 
 * Manage Upload document Categories 
 * @Ticket #3917
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES","1");

/**
 * Sender refrence number at confirm transaction page
 * @Ticket #
 */
define("CONFIG_SHOW_REFRENCE_NO_AT_CONFIRM", "1");
/**
 * Displays sender bank details on reports.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_BANK_DETAILS","1");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_TRANSFER_BY_BANK","1");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_REFERENCE_NUMBER","1");

/* added and enabled this config against ticket #3939 */
define("CONFIG_DUAL_ENTRY_IN_LEDGER","0");

define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "admin, Branch Manager, Admin Manager,Admin,Call,");

/* unlimited exchange rate ticket #4136 */
define("CONFIG_UNLIMITED_EX_RATE_OPTION","1");

/*user can enter unlimited manual exchange rate  #4553 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE","1");
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","0");
//define("CONFIG_UNLIMITED_EXRATE_COUNTRY","1"); // 1 for all countries or specific countries can be mentioned i.e "PAKISTAN,INDIA,"

/********************
* Switch to Enable/Disable settlement commission on create transaction
*       0 - enable
*       1 - disable
* i think it is to be removed from system as this development seem to be craped now.... just to confirm from javeria 
first...
********************/
define("CONFIG_SETTLEMENT_COMMISSION", "0");

/**
 * Agent Rate can be defined from the system 
 * @Ticket #4355
 */
define("CONFIG_AGENT_OWN_RATE","1");

/**
 * To retains the remarks about a customer/sender
 * at its creation and display at the add transaction
 * @Ticket #4230
 */
define("CONFIG_REMARKS_ABOUT_SENDER","1");

/********************
Added by Niaz Ahmad #2810 at 16-01-2008
*  Enable/Disable settlement currency dropdown
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "0");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "0");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "0");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * CURRENCY EXCHANGE MODULE
 * This config affects whole module.
 * if not enabled then you can't access page directly into url.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE","0");


/**
 * #4802
 * This config affects whole module on Search Sender form.
 * To use Exact search.
 * '1' For enable  and Config CONFIG_CUST_WILDCARD_SRCH should be '0'
 * '0' For disable
 */
define("CONFIG_EXACT_MATCH_CUST_SEARCH","1");

/**
 * #4802
 * This config affects whole module on Search Benificiary form.
 * To use Exact search.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_EXACT_MATCH_BEN_SEARCH","1");
/**
 * Added By Niaz Ahmad at 26-05-2009
 * #4851
 * This config is used to validate passport number.
 * To use Exact search.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_PASSPORT_VALIDATION","0");
/**
 * Enables/disables mode of payment on agent account
 * Ticket#4950
 * true = Enable
 * false = Disable
 */
define("CONFIG_ENABLE_PAYMENT_MODE_ON_AGENT_ACCOUNT", "true");

/**
 * Shows/Hides cheque number, status fields on agent account statement.
 * Ticket#4950
 * true = Shows
 * false = Hides
 */
define("CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT", "true");

/**
 * Enables/Disables filter for search by cheque number, on agent account statement.
 * Ticket#4950
 * true = Enables
 * false = Disables
 */
define("CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM", "true");

 
/********************
* #5003	 START
* This functionality has following specs
* 1- All Agents or Specified Agent is assigned and its Ledgers are affected
* 2- Customers Ledgers also get affected.
* 3- Report can also be viewed on Depositing money.
* 4- On transaction Reciept there is a field of Payin amount.
*********************/
/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","1");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Customer ");
// Config below is added to give payin book functionality functionality to every one.
define("CONFIG_PAYIN_BOOK_FUNCTIONALITY_TO_EVERY_ONE", "1");
define("CONFIG_AUTO_PAYIN_NUMBER","1");	

/********************

* Switch to Enable/Disable an agent associated with payin book customers
* If CONFIG_PAYIN_AGENT_NUMBER has value then Don't Use CONFIG_PAYIN_CUST_AGENT_ALL
* Also either CONFIG_PAYIN_CUST_AGENT_ALL OR CONFIG_PAYIN_AGENT_NUMBER is mandatory
* to use with CONFIG_PAYIN_CUST_AGENT
* by Aslam Shahid
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_CUST_AGENT_ALL", "1");
//define("CONFIG_PAYIN_AGENT_NUMBER", "100221");


/**
 * redirect the user to reciept when depositing manualy in customer's ledger
 * ticket no 3549
 * @Ticket# 3554
 */
/*define("CONFIG_CUSTOMER_LEDGER_PAGE","1");
define("CONFIG_REF_NO_ON_CUSTOMER_ACCOUNT","1");
*/

/**
 * Config enable the sender reference number generator
 * @Ticket# 3549
 */
define("CONFIG_SENDER_NUMBER_GENERATOR","1");

/**
 * For GE, normal sender will have payin book customer logic
 * This config will work as same as in express client but no will not display any label or field to clinet
 * @Ticket# 3549
 */
define("CONFIG__GE__USE_CUSTOMER_PAYIN_BOOK_LOGIC","1"); // Associated Payin Senders can be searched on Transaction

/* This config is being added by Omair Anwer against ticket # 5000
 * The config brings up the functionality of putting a limit(uppper and lower limit) for a transactions 
 * such that incas the exchange rate changes drastically then at the time of the release or paying out an the
 * admin user can see and tell if this transaction can be payed out or not.
 * CONFIG_USE_EXTENDED_TRANSACTION should also be ON.
 */
define("CONFIG_ENABLE_EX_RATE_LIMIT","0");

/********************
* Switch to Enable/Disable Usage of an extended table with transaction 
* to keep optional info related to a particular transaction
* specifically used for connect plus and global Exchange
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_USE_EXTENDED_TRANSACTION", "1");


/********************
* #5003	 END
*********************/
/*
* Added by Niaz Ahmad
* @4997
* In case of companies it will show just one field instead of first/mid and last name
* Dated: 01-06-2009
*/
define("CONFIG_BENEFICIARY_FULLNAME_FOR_COMPANIES", "1");
define("CONFIG_SENDER_FULLNAME_FOR_COMPANIES", "0");
/*
* Added by Niaz Ahmad at 11-06-2009
* #5004 AMB Clinet
* This config is used to show transaction status alert queue at quick menu
* 2nd Config shows defult  hours it will shows all transaction that over 48 hours
*/
define("CONFIG_TRANSACTION_STATUS_ALERT_LIMIT_IN_HOURS","1");
/*
* Added by Niaz Ahmad at 22-06-2009
* #4999 AMB Clinet
* This config is used to show customer list alert at quick menu
* this config check the days limit of customer account if customer limit over the show alert
*/
define("CONFIG_CUSTOMER_ACCOUNT_DAYS_LIMIT","1");
define("CONFIG_MANAGE_USER_CONFIGURATION","1");


/*
* #5082
* Manual exchange rate can be used in currency exchange module
* Enable  = "1" (for all) OR admin, Branch Manager, Admin Manager,Admin,Call... etc
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_MANUAL_RATE_USERS_CURR", "1");

/*
* #4996 point 1
* Company type dropdown for MSB/3rd Party like companies on agents/AnD forms.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_COMPANY_TYPE_DROPDOWN", "1");

/*
* #4996 point 3
* Trading Name on agents form.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_TRADING_COMPANY_NAME", "1");


/*
* #4996 point 2
* Upload document functionality for AMB AnD's
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT", "1");
define("CONFIG_VIEW_UPLOAD_DOCUMENT_SUPPER_AGENT","1");
define("CONFIG_UPLOAD_DOCUMENT_ADD_SUB_AGENT","1");
define("CONFIG_VIEW_UPLOAD_DOCUMENT_SUB_AGENT","1");
/**************************************************************
* Added by Niaz Ahmad against #5085:
this config will control the main.php entery against the bank based transactions export
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT","0");
define("CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT","1");
define("CONFIG_MT103_EXPORT_FILE_NAME","MT103");
/***************************************************************
* Added by Niaz Ahmad against #4950:
* this config use to show option of cheque at customer account statement
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT","1");
/***************************************************************
* Added by Niaz Ahmad against #4998:AMB
* this config use to show Alert at create transaction,cheque cashing and currecny exchange
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT","1");  
/** 
 * #5084
 * Item-Wise reconciliation of Bank Payments
 * '1' For enable 
 * '0' For disable 
 */ 
define("CONFIG_UNRES_PAY_BARCLAY_FILE_AJAX","1");
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");
/**
* Added by Niaz at 30-06-2009
* @4997 AMB
* when edit sender then in case of company it show add-company.php link at edit
*/
define("CONFIG_SENDER_AS_COMPANY","1");
/**
 * #5091
 * To stop Agent profit/Loss entry in ledgers
 * Also turn OFF the config CONFIG_DUAL_ENTRY_IN_LEDGER ,to stop the company Gain/ Loss entries in ledgers
 */
define("CONFIG_AT_CREATE_TRANSACTION_STOP_PROFIT_LOSS_ENTRY","1");

/* Config to display Currency filter on A&D account statement.
   Ticket#5001
*/
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");

/* Being added against ticket 4992 by Omair Anwer
 *
 *
 */
define("CONFIG_STORE_MODIFY_TRANSACTION_HISTORY","1");

/*
* #4949
* Paypoint Transactions functionality added alongwith its services,account summary under this config.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_ADD_PAYPOINT_TRANS", "1");

/********************
*	#5186
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
	More Importantly following config should be OFF.
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN
	
	And these sould be on... (Although settlement logic is not used here.)
	CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD

	NOTE :: All this scenario is set-up for
			1- System uses A&D functionality.
			2- There are no settlements currencies in A&D.
			3- Ledgers A&D as Distributor will be affected properly. (Created,Cancelled,Rejected,Failed,Credited etc)
			4- Account Summary and Balance of A&D as Distributor is affected as well.
*	0 means Disable
*	1 means Enable 
*	by Aslam Shahid
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD", "1") ;
/**********************
* This config is used to remove extra text in the Add/Update Agent/Distributor on the label for MSB Number
* @5275
* Disable =0 
* Enable =0
* Niaz Ahmad at 13-08-2009
***********************/
define("CONFIG_REMOVE_MSB_TEXT_FROM_ADD_AGENT_DISTRIBUTOR","1");
/**********************
* This config is used to make Bank Name non mandatory at add/update collection point
* @5281
* Disable =0 
* Enable =0
* Niaz Ahmad at 13-08-2009
***********************/
define("CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT","1");
/**********************
* This config is used to Add beneficary as company
* @5279
* Disable =0 
* Enable =0
* Niaz Ahmad at 17-08-2009
***********************/
define("CONFIG_BENEFICIARY_AS_COMPANY","1");
/*
* #5336
* On Printing Transactions dropdown is added in case 
* if there are different Receipts for Pick and Bank Transfer then
* user can only print multiple receipts but of same type. 
* CONFIG_RECIEPT_PRINT_BANK should be assigned file name value for Bank Transfer Receipt.
* CONFIG_RECIEPT_PRINT should have Pick up format file name value.
* Enable  = "1"
* Disable = "0"
* by A.Shahid
*/
define("CONFIG_SHOW_TRANS_TYPE_RECEIPTS_PRINT","1");
/**********************
* This config is used to Add ben bank fields at add transaction page
* when this config is ON then IBAN configs will be OFF
* e.g CONFIG_IBAN_OR_BANK_TRANSFER and CONFIG_EURO_TRANS_IBAN will be OFF
* @5282
* Disable =0 
* Enable =0
* Niaz Ahmad at 18-08-2009
***********************/
define("CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION","1");

//#5283-manual commission
define("CONFIG_MANUAL_FEE_RATE","0");
define("CONFIG_BEN_COUNTRY","INDIA,PAKISTAN,PORTUGAL,");
define("CONFIG_USE_MANUAL_COMMISSION_AJAX","1");

/* Dear all, This config is being added under ticket 5294.
 * This config makes sure that all transactions that are to be viewed via the Quick Menu
 * export link for Western Union only shows transactions that have been authorised.
 * Config Added by Omair Anwer
 */
define("CONFIG_EXPORT_TRANSACTION_CUSTOMIZE","1");

//5286-hide ben verification id on create transaction
define("CONFIG_HIDE_TIP_FIELD","0");

//5289-User account statement
define("CONFIG_CREATE_ADMIN_STAFF_LEDGERS", "1");
$_arrAdminStaffForCreateTransactionLedgers = array(
													"Admin", "Admin Manager",
													"CALL"
												);
												
/** 
 * #5258 Minas Center
 * Shows Opening/Closing Balnaces on Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 											
define("CONFIG_ADMIN_STATEMENT_BALANCE", "0");
/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Currency Excange Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_ADMIN_LEDGER", "1");

/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Cheque Cashing Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_CASHING_ADMIN_LEDGER", "1");

/** 
 * #5255 Minas Center Point #7
 * Ledgers of all users are affected for User Denomination
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_USER_DENOMINATION_ADMIN_LEDGER", "0");
												
/** 
 * #5365 AMB Exchange
 * Shown Note input box at add transaction page
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad at 21-08-2009
 */ 
define("CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION", "0");
define("CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION_LABELS","Transaction Notes");
/** 
 * #5364 AMB Exchange
 * customize bank base transaction page
 * shown pick up transaction , remove ID column, exclude western union distributor transaction...etc
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad at 22-08-2009
 */ 
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION","1");
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION_EXCLUDE_DISTRIBUTOR","100226");
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION_QUICK_MENUE_LABEL","Pick up and Bank Based Transactions");

/** 
 * #5178 - Point 4 of Document
 * Use Round number and give value of decimal places to 
 * be rounded.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 											
define("CONFIG_ROUND_NUMBER_ENABLED_CURR_EXCH", "1");
define("CONFIG_ROUND_NUMBER_TO_CURR_EXCH", "0"); // Number of rounded decimals

define("CONFIG_RELEASE_BULK_TRANSACTIONS_FILE","releaseBulkTransactions.php");

/***************
CONFIG  for transactions that are based on money paid,
By cheque.if a transaction is by cheque type it will go to
"un clear cheque payments" before going to pending status
done by javeria ticket # 3265
* If value is '0' its Disabled
* If value is '1' its Enabled
****************/
define("CONFIG_CHEQUE_BASED_TRANSACTIONS","0");
/** 
 * #5441
 * Operator Code is added in AnD form.
 * to be displayed in Western Union Output export file.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */  
define("CONFIG_OPERATOR_ID_AnD","1");
/** 
 * #5441
 * Additional Fields for AMB enhancement of
 * export file to be displayed in Western Union Output export file.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_CUSTOMIZED_FIELDS_AMB_EXCHANGE","1");

/** 
 * #5544
 * Dropdown for agents is displayed showing all Agents.
 * Also ledger will be affected accordingly.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_USE_PAYPOINT_AGENT_DROPDOWN","1");

/** 
 * #5544
 * Default agent for paypint transactions.
 * CONFIG_USE_PAYPOINT_AGENT_DROPDOWN should also be ON for this.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_PAYPOINT_DEFAULT_AGENT_ID","100230");

/** 
 * #5287 Minas Center
 * Currency stock account is mainted and currency stock
 * amounts are deducted/added
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_STOCK_ACCOUNT", "1");

/** 
 * #4595
 * Currency exchange :: now customer in creating currency exchange transaction
 * becomes non-mandatory for specified User groups in config.
 * Enable  : 1 or User types..(Admin,SUPA, etc..)
 * Disable : 0
 */ 
define("CONFIG_CUST_NON_MANDATORY_USERS_CURR","1");

/** 
 * #4693
 * Cancel currency exchange transaction functionality.
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_CANCEL_TRANS_CURR_EXCH","1");

/** 
 * #5751
 * Currency exchange stock list in left menu
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_CURRENCY_STOCK","1");

/** 
 * #5750
 * Manula Super AnD number (Login name) can be entered 
 * in AnD forms (super and sub AnD)
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONFIG_MANUAL_SUPER_AnD_NUMBER","1");

/** 
 * #5750
 * Manula Sub AnD number (Login name) can be entered 
 * in AnD forms (super and sub AnD)
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONFIG_MANUAL_SUB_AnD_NUMBER","1");

/** 
 * #5747
 * Currency filter and Total Amount is added 
 * on currency exchange reports page
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE","1");
/** 
 * #5957:Premierexchange
 * Added new customer category
 * Enable  : "1"
 * Disable : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_CUSTOMER_CATEGORY","0");
define("CONFIG_ADMIN_STAFF_LABEL","Account Manager");
define("CONFIG_FEE_BASED_DROPDOWN",'1');
/** 
 * #5966:Premierexchange
 * Added enquiry type dropdown
 * Enable  : "1"
 * Disable : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_ENQUIRY_TYPE","1");
/** 
 * #5955:Premierexchange
 * Added custome sending & receiving currencies
 * Enable  : "1"
 * Disable : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES","1");
/** 
 * #5950:Premierexchange
 * compliance for all users
 * Enable  : "1"
 * Disable : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_COMPLIANCE_FOR_ALL_USERS","0");

$_arrLabels = array(
					"Agent"=>"Introducer",
					"Sender"=>"Customer",
					"Created By"=>"Introducer"
					);
/** 
 * #5893:Premierexchange
 * forex trading
 * Enable  : "1"
 * Disable : "0"
 * by Niaz Ahmad
 */ 
define("CONFIG_FOREX_TRADING_TRANS_STATUS","1");

/**  
 * #5893:AMB Exchange
 * added new module for trading
 * Enable  : "1"
 * Disable  : "0"
 * by Niaz Ahmad 
 */  
define("CONFIG_ADD_TRADING_MODULE","1");

/********************
* #6240
* Switch to Enable/Disable mail Generator in case of creation of user and updation of info or password
* also checkbox for send email should be checked on add customer page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_MAIL_GENERATOR", "1");
/********************
* #5951 premier fx
* Address on MT103 export file
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_MT103_ADDRESS", "Premier Fx
20 SMITH ST 
BOURNEMOUTH
GB");

//define("CONFIG_IBAN_BEN_BANK_DETAILS","1");

//Ticket #6269 editable customer number on update customer
define("CONFIG_ADD_REF_NUMBER", "1");
define("CONFIG_UPDATE_CUSTOMER_NUMBER", "1");

/* #6272 - PremierExchange
 * when we enter IBAN value on create transaction page then it also
 * updates the same value in Account Number
 * by Aslam Shahid.
 */
define("CONFIG_UPDATE_ACCOUNT_WITH_IBAN", "0");
/* #6439+7027 - masterpayex
 * This config is used to stop sending emails for whole of the system and it over-rides all configs.
 * Enable  = 1
 * Disable = 0
 * by Aslam Shahid.
 */
 define("CONFIG_STOP_WHOLE_EMAIL_SYSTEM", "1");
 /* #6468 
 * Documents uploaded are shown on sender page
 * Enable  = "1"
 * Disable = "0"
 * by Aslam Shahid.
 */
 define("CONFIG_SHOW_DOCUMENTS_SENDER_FORM", "1");
  /* #6221 
 * By Default manual exchange is selected.
 * Enable  = "1"
 * Disable = "0"
 * by Aslam Shahid.
 */
 define("CONFIG_DEFAULT_MANUAL_EXCHANGE_RATE", "1");
 
 /**  
 * #5088:AMB Exchange
 * added new module for deal sheet 
 * Enable  : "1"
 * Disable  : "0"
 * by Niaz Ahmad 
 */  
define("CONFIG_DISTRIBUTION_DEAL_SHEET","0");
define("CONFIG_DEAL_SHEET_USER_TYPE","Customer");
define("CONFIG_DEAL_SHEET_VIEW_USERS","SUPI,SUBI,SUPAI,SUBAI,admin");

define("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT", "1");
define("CONFIG_DEFAULT_DISTRIBUTOR", "1");
/**  
 * #6752:Premier Exchange
 * Value Date can be assigned and then transaction goes in Value Date Transactions Queue in left menu.
 * to Display link in left menu under Transaction module you need to add right from user functionality for
 * "Validate Value Dated Transactions"
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_VALUE_DATE_TRANSACTIONS", "1");
 /**  
 * #6752:Premier Exchange
 * Dropdown is displayed for Bank Details Yes/No. and also link is added in left menu
 * by user functionality "Incomplete Transactions"
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_USE_TRANSACTION_DETAILS", "1");
  /**  
 * #6752:Premier Exchange
 * Transaction purpose is made non-compulsory
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");
  /**  
 * #6752:Premier Exchange
 * Funds source is hidden from transaction page
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_REMOVE_FUNDS_SOURCES", "1");
  /**  
 * #6752:Premier Exchange
 * Admin charges is hidden from transaction page
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_REMOVE_ADMIN_CHARGES", "1");
  /**  
 * Money paid is taken above amounts on transaction page.
 * Enable  : "1"
 * Disable  : "0"
 */
 define("CONFIG_MONEY_PAID_LOCATION", "TOP");
 /**  
 * #6752:Premier Exchange
 * New layout for agent/customer/beneficiary/transaction
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_MVC_LAYOUT", "1");
 /**  
 * #6752:Premier Exchange
 * System has single distributor. and transaction uese that distributor
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_SINGLE_DISTRIBUTOR_SYSTEM", "1");
 /**  
 * #6752:Premier Exchange
 * Branch address is made non-mandatory on transaction page.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_BRANCH_ADDRESS_TRANS_NON_MANDATORY", "1");
 /**  
 * #6752:Premier Exchange
 * Commission/fee is hidden from transaction page.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_NORMAL_FEE_TRANS_HIDE", "1");
 /**  
 * #6752:Premier Exchange
 * Agent User type is displayed on agent form.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_AGENT_USER_TYPE", "1");
 /**  
 * #7021:Premier Exchange
 * Take off terms and conditions and tickebox from transaction page
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
 define("CONFIG_TnC_CHECKBOX_OFF", "1");
 /*
 # 7014 - Premier FX
 * Rename label of "Add New Regular" from "Sender" to any required.
 * Enable  :  any string
 * Disable  : "0"
 * Module : Transaction
 * Client: Premier FX
 * Control No: 1133
 * by Aslam Shahid
*/
 define("CONFIG_ADD_SENDER_TRANS_LABEL", "Add New");
/*
 * 7014 - Premier FX
 * Search sender name is in surname formate "surname,firstname"
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * Control No: 1134
 * by Aslam Shahid
 */
 define("CONFIG_SURNAME_SENDER_SEARCH", "1");
/*
 * 7014 - Premier FX
 * No of transactions is displayed on search sender.
 * And also linked to view all transactions
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_IMPORTED_TOTAL_TRANSACTIONS", "1");
 /*
 * 7015 - Premier FX
 * hide msb number and expiry date from agent/introducer details
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1135
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_MSB_AGENT", "1");
/*
 * 7015 - Premier FX
 * hide agent/introducer company logo upload
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1136
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_AGENT_COMPANY_LOGO", "1");
/*
 * 7015 - Premier FX
 * hide agent authorized services
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1137
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_AGENT_AUTHO_SERVICE", "1");
/*
 * 7015 - Premier FX
 * hide agent access ip
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1138
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_AGENT_ACCESS_IP", "1");
/*
 * 7020 - Premier FX
 * hide money paid values in dropdown on transaction page.
 * Enable  : names of money paid comma separated
 * Disable : "0"
 * Module : Transaction
 * Control No: 1139
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_MONEY_PAID_HIDE_VALUES", "By Cash,");
/*
 * 7023 - Premier FX
 * value date on transaction is mandatory.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1140
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_VALUE_DATE_MANDATORY", "1");
/*
 * 7055 - Premier FX
 * a dropdown is populated with users in the system.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1141
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_POPULATE_USERS_DROPDOWN", "1");
/*
 *  - Premier FX
 * a .
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 
 * Client: Premier FX
 * by HAroon
 */
 define("CONFIG_POPULATE_ADMINSTAFF_DROPDOWN", "'Admin Manager','Branch Manager'"); 
 /*
 * 7083 - Premier FX
 * Currency dropdown is displayed on agent account statement.
 * also currency is attached to amount.
 * Enable  : "1"
 * Disable : "0"
 * Module : Report
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CURRENCY_AT_ACCOUNT_DEPOSIT", "1");
 /**
 * Define exchange rate in term of denomination based
 * @Ticket #4269
 */
 define("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE","1");
 /*
 * 6596 - Premier FX
 * this config enables to send the mail to customer when he reigisters. 
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 */
 define("CONFIG_MAIL_CUSTOMER_REGISTRATION","1");
/*
 * 7132 - Premier FX
 * this config hides the agent's commission column on the Introducer summary report. 
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 */ 
define("CONFIG_HIDE_COMMISSION_REPORT_COLUMNS","1"); 

define("CONFIG_COMMISSION_REPORT_RENAME_SENDER","Client Name");

 /*
 * 6276 - Premier FX
 * Agent commission report has agent's account currenty commission 
 * when transaction is created, rate is picked and converts commission.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_AGENT_CURRENCY_COMMISSION","1");
 
 /*
 * 7132 - Premier FX
 * Transaction purposes can be added on transaction page.
 * Enable  : comma separated values
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_TANSACTION_PURPOSES","Leisure,Business,Bank Account Top-up,Mortgage Payments,Pension,Property Purchase,Property Maintenance");
 /*
 * 7132 - Premier FX
 * Added account manager dropdown on add/udate sender page
 * CONFIG_POPULATE_USERS_DROPDOWN is required for this functionality.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_POPULATE_USERS_DROPDOWN_SENDER","1");
 /*
 * 7148 - Premier FX
 * Agent selection and details is hidden on transaction page. When sender is searched then its agent is used
 * in transaction.
 * Enable  : user type comma separated or "1" for all user types.
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_AGENT_ON_TRANSACTION","1");
 /*
 * 7166 - Premier FX
 * Now you can re-label Amount and Local Amount labels alongwith buttons using config 
 * define("CONFIG_BUTTON_LABELS_AT_TRANSACTION","Client Selling,Client Buying");
 * Enable  : i.e "lable1,lable2".
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_BUTTON_LABELS_AT_TRANSACTION","Client Selling,Client Buying");
 /*
 * 7166 - Premier FX
 * Reverse calculation can be done after enering amount and manual exchange rate
 * on tranaction page. There is a checkbox on transaction page.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_REVERSE_TRANS_AMOUNT_OPERATORS","1");
 /*
 * 7247 - Premier FX
 * change the order of cols on manager super introducer page 
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Client: Premier FX
 * control number: 1153
 * by Haroon ur Rasheed
 */
 define ("CONFIG_SUPA_COL","1");
/*
 * 7248 - Premier FX
 * hides cheque cashing limit on add/update admin staff page
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Client: Premier FX
 * control number: 1154
 * by Haroon ur Rasheed
 */ 
define("CONFIG_HIDE_CHEQUE_CASHING_LIMIT","0");
/*
 * 7248 - Premier FX
 * hides CURRENCY EXCHANGE LIMIT on add/update admin staff page
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Client: Premier FX
 * control number: 1155
 * by Haroon ur Rasheed
 */ 
define("CONFIG_HIDE_CURRENCY_EXCHANGE_LIMIT","0");
/*
 * 7248 - Premier FX
 * hides DAILY EXPENSE LIMIT on add/update admin staff page
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Client: Premier FX
 * control number: 1156
 * by Haroon ur Rasheed
 */ 
define("CONFIG_HIDE_DAILY_EXPENSE_LIMIT","0");

 /*
 * 7249 - Premier FX
 * Account manager dropdown is displayed on some reports.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_ACCOUNT_MANAGER_COLUMN","Account Manager");

/*
 * 7254 - Premier FX
 * Enable the functionality of Email to both sender and Account Manager.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 1157
 * by Haroon ur Rasheed
 */ 

define("CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR","1");

/*
 * 7340 - Premier FX
 * Specific Countries in sender and beneficiary forms.
 * Enable  : "1" for all or comma separated list.
 * Disable : "0"
 * Module : Customer/Beneficiary
 * Client: Premier FX
 * control number:
 * by A.Shahid
 */ 

define("CONFIG_CUST_COUNTRIES_LIST","Australia, Belgium, Brazil, Bulgaria, Canada, Cyprus, Finland, France,Greece, Hong Kong, Ireland, Italy, Luxembourg, Malaysia, New Zealand,Norway, Poland, Portugal, Singapore, South Africa, Spain, Sweden,Switzerland, Turkey, United Kingdom, United States,");
define("CONFIG_BEN_COUNTRIES_LIST","Australia, Belgium, Brazil, Bulgaria, Canada, China, Cyprus, Finland, France,Greece,Gibraltar, Hong Kong, Ireland, Italy, Luxembourg, Malaysia, New Zealand,Norway, Poland, Portugal, Singapore, South Africa, Spain, Sweden,Switzerland, Turkey, United Kingdom, United States,");

/*
 * 7251 - Premier FX
 * this config change the order of customers Address info.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 1157
 * by Haroon
 */ 

define("CONFIG_ADD_CUST_GHANGE_ADDRESSINFO_ORDER","1");

/*
 * 7251 - Premier FX
 * this config enbales the insertion of comments with current date.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 1158
 * by Haroon
 */ 

define("CONFIG_INSERT_DATE_IN_COMMENTS","1");

/*
 * 7258 - Premier FX
 * Specific Countries in sender and beneficiary forms.
 * Enable  : "1"
 * Disable : "0"
 * Module : Report
 * Client: Premier FX
 * control number:
 * by A.Shahid
 */ 
define("CONFIG_HIDE_COMP_CUST_CITY_LIST","1");
define("CONFIG_HIDE_COMP_CUST_COUNT_LIST","1");

/*
 * 7251 - Premier FX
 * this config enbales the insertion of comments with current date.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 1158
 * by Haroon
 */ 
define("CONFIG_INSERT_COMP_CONTACT_PERSON","1");

/*
 * 7286 - Premier FX
 * Source field added in customer form
 * Enable  : "1"
 * Disable : "0"
 * Module : Customer
 * Client: Premier FX
 * control number:
 * by A.Shahid
 */ 
define("CONFIG_SOURCE_FIELD_CUSTOMER","1");

/*
 * 7253 - Premier FX
 * this config sorts the records on the surname of customer
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 
 * by Haroon
 */ 
define("CONFIG_CUST_SURNAME_ARRANGEDBY_SURNAME","1");

/*
 * 7361 - Premier FX
 * this config enbales the iinsertion of docs for Introducer.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 1158
 * by Haroon
 */ 
define("CONFIG_ADD_INTRO_UPLOAD_DOCS","1");

/*
 * 7337 - Premier FX
 * Both types of Currencies on transaction page come from trading accounts
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 
 * by A.Shahid
 */ 
define("CONFIG_TRANSACTION_CURRENCIES_TRADING","1");

/*
 * 7337 - Premier FX
 * IBAN is displayed on add account form in trading module.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Trading
 * Client: Premier FX
 * control number: 
 * by A.Shahid
 */ 
define("CONFIG_IBAN_TRADING_ACCOUNT","1");
define("CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT","1");

/*
 * 7361 - Premier FX
 * this config enbales the iinsertion of docs for Introducer.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 
 * by Haroon
 */ 
define("CONFIG_HIDE_LINKS_FOR_SUPERINTRO","SUPA,");

/*
 * 7417 - Premier FX
 * this config enbales the iinsertion of docs for Introducer.
 * Enable  : "1" 
 * Disable : "0"
 * Module : Transaction
 * Client: Premier FX
 * control number: 
 * by Haroon
 */ 
define("CONFIG_SHOW_CUST_BY_TITLE_MR","0");

/**  
 * #7463:Premier Exchange
 * Account Number is made non-mandatory on transaction page.
 * Enable  : "1"
 * Disable  : "0"
 * by Haroon
 */
 define("CONFIG_ACCNO_TRANS_NON_MANDATORY", "1");
 
/**  
 * #7463:Premier Exchange
 * Bank name is renamed on transaction page.
 * Enable  : "1"
 * Disable  : "0"
 * by Haroon
 */
 define("CONFIG_BANKNAME_TRANS_RENAME", "1");
 
/**  
 * #7404:Premier Exchange
 * Agent company records are shown on customer registration report.
 * Enable  : "1"
 * Disable  : "0"
 * by Haroon
 */
 define("CONFIG_SHOW_INTRODUCER_COMPANY_NAME", "1");

/**  
 * #7420:Premier Exchange
 * Source field is shown if the config on.
 * Enable  : "1" or User types..(Admin,SUPA, etc..)
 * Disable  : "0"
 * by Haroon
 */ 
 define("CONFIG_SHOW_INTRODUCER_FIELD","1");
 
/**  
 * #7415:Premier Exchange
 * this config changes the order of left menu.
 * Enable  : "1" 
 * Disable  : "0"
 * by Haroon
 */ 
 define("CONFIG_CHANGE_ORDER_OF_LEFTMENU","1"); 
 
  /*
 * 7405 - Premier FX
 * hide msb number on add company page.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No: 1135
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_HIDE_MSB_CUST", "1");
  /*
 * 7419 - Premier FX
 * Default sender reference number counter sarting value with C-
 * Enable  : "1"
 * Disable : "0"
 * Module : Customer
 * Control No:
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_CUSTOM_SENDER_COUNTER_START", "5737");
  /*
 * 7400 - Premier FX
 * Client Account Detail dropdown added on customer page displaying all
 * trading accounts. these details are sent to customer within email.
 * Enable  : "1"
 * Disable : "0"
 * Module : Customer
 * Control No:
 * Client: Premier FX
 * by Aslam Shahid
 */
 define("CONFIG_TRADING_ACCOUNT_CUSTOMER", "1");
 /*
 * 7575 - Premier FX
 * this config enables Phone field non-mandatory.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by Haroon
 */
 define("CONFIG_PHONE_NON_COMPULSORY", "1");
 /*
 * 7575 - Premier FX
 * this config enables city field non-mandatory.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by Haroon
 */
 define("CONFIG_CITY_NON_COMPULSORY", "1");

  /*
 * 7688 - Premier FX
 * stop the automatic email on creation
 * Enable  : "0"
 * Disable : "1"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by Moin
 */
 define("CONFIG_TRANSACTION_EMAIL", "1");

 /*
 * 7645 - Premier FX
 * This config enables transaction creation without 
 * beneficiary selected.
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_TRANS_WITHOUT_BENEFICIARY", "1");


 /*
 * 7591 - Premier FX
 * New commission report for customers/clients
 * Enable  : "1"
 * Disable : "0"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_AGENT_COMMISSION_REPORT_FOR_CLIENT", '1');
 
 /*
 * 7897 - Premier FX
 * Rename 'Create Transaction' text in the system.
 * Enable  : string
 * Disable : "0"
 * Module : Transaction
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_SYSTEM_TRANSACTION_LABEL", 'Deal Contract');

 /*
 * 8008 - Premier FX
 * Hide Agent Account Statement links from Quick Menu
 * Enable  : user types comma separated.
 * Disable : "0"
 * Module : User Management
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_HIDE_QUICK_AGENT_ACCOUNT_USERS", 'SUPA,');
 /*
 * 8008 - Premier FX
 * Introducer/agent login changes label which is shown under 
 * CONFIG_HIDE_LINKS_FOR_SUPERINTRO for own profile/form for updation
 * Enable  : text for link
 * Disable : "0"
 * Module : User Management
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_MANAGE_OWN_PROFILE_AGENT_LABEL", 'Manage Profile');
 /*
 * 8008 - Premier FX
 * Certain fields hidden from update super agent/introducer page with agent login.
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_UPDATE_AGENT_INTRODUCER_FIELDS", '1');
 /*
 * 8105 - Premier FX
 * hide all quick menu for Call center staff login 
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_HIDE_QUICK_MENU_CALL_CENTER", '1');
 
 
 
 define("CONFIG_PASSPORT_VALIDATION_MANDATORY", '0');
 
 /*
 * 8254 - Premier FX
 * Service email to customer and company
 * Enable  : "1"
 * Disable : "0"
 * Module : User Management
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL", '1');
 /*

 */
define("CONFIG_PHONE_NUMBER_NUMERIC", '1');
  /*
 * 8397 - Premier FX
 * Contact Person name field mandatory
 * Enable  : "1"
 * Disable : "0"
 * Client: Premier FX
 * by Nimra
 */
 
 define("CONFIG_CONTACT_PERSON_MANDATORY", '1');
 
 
  /*
 * #9328: PremeirFx: Association of introducer with account manager
 * 
 * Enable  : "1"
 * Disable : "0"
 * Client: Premier FX
 * by Nimra
 */
 
 define("CONFIG_NEW_INTRODUCER_ASSOCIATION", '1');

 /*
 * 9398 - Premier FX
 * Regulation laundering message in system footer.
 * Enable  : text to be displayed
 * Disable : "0"
 * Module : System
 * Control No:
 * Client: Premier FX
 * by A.Shahid
 */
 define("CONFIG_ADD_REGULATIONS_COMPANY", "Premier FX are regulated and authorised by the FSA under the payment services directive number: 530712 and are regulated by HMRC under money laundering regulations.");
/*
 * #8899: Premier FX: Cancellation not working properly
 * 
 * Enable  : "1"
 * Disable : "0"
 * Client: Premier FX
 * by Nimra
 */
 
 define("CONFIG_CANCELLATION_LEDGER_CURRENCY", '1');
 
 /*
 * #7888: Premier FX- new report for the account manager performance RTS
 * 
 * Enable  : "1"
 * Disable : "0"
 * Client: Premier FX
 * by Nimra 
 */
define("CONFIG_SHOW_FLAG", '1');


/*
 * #7888: Premier FX- new report for the account manager performance RTS 
 * Admin Staff List
 * Get ASSOCIATE INTRODUCER (Agents) for logged in Admin Staff
 * By Kalim
 */
define("CONFIG_ADMIN_STAFF_LIST", "Admin,Admin Manager,Branch Manager,SUPI Manager,Call,Collector,COLLECTOR,Support,MLRO,");
define("CONFIG_DISPLAY_ASSOCIATE_INTRODUCER","1");
/*
 * #9484: Premier Fx: Sender ID Expiary
 * 
 * Enable  : "1"
 * Disable : "0" 
 * Client: Premier FX
 * by Nimra 
 */ 
define("CONFIG_SENDER_ID_EXPIRED_WAVIER", '1');

/*
 * #9728: Premier Fx: issues
 * 
 * Enable  : "1"
 * Disable : "0" 
 * Client: Premier FX
 * by Nimra 
 */ 
define("CONFIG_BENEFICIARY_COMPANY_RETURN", '1');
//9788 in MB
define("CONFIG_LIMIT_FILE_SIZE", '2');

/*
 * #9703: Premier Exchange: Alert for Sender ID Expiry
 * 
 * Enable  : "1"
 * Disable : "0" 
 * Client: Premier FX
 * by Kalim 
 */ 
define("CONFIG_DUP_CHECK_ON_ADD_ALERT", "1");
define("CONFIG_NEW_ALERT_CONTROLLER_PAGE", "1");
define("CONFIG_DB_ID_TYPES","1");
define("CONFIG_ALERT_TYPE_ON_ALERT_CONTROLLER","1");

/**
 * #9900 :Premier Exchange : Add Amount Functionality Define
 * Short Description
 * This Config Disable the Through out the Table Add Amount
 */
define("CONFIG_ADD_AMOUNT_COLUMN", "0");

/**
 * # 7888: Premeir FX - Admin Staff and Correspoding Agents
 * Short Description
 * This Config Enable / Disable the Agent Staff Field and Corresponding Field
 */
define("CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS", "1");

/**
 *  #9808: PremierFx: Import and Export of MT103
 *  Short Description
 *  This config enables/disables the functionality of import mt103 transactions
 *  Enable  : 1
 *  Disable : 0
 */
define("CONFIG_IMPORT_MT103_TRANSACTIONS","1");
 
 /**
  *#9897: Premier Exchange:Sender registration through website
  * Short Description
  * This config is for the functionality of new Senders enable/disable
  */
 define("CONFIG_NEW_SENDERS_ENABLE_DISABLE","1");
 
 /**
  *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements
  *  Short Description
  *  This config is used to display only the associated sender's transactions of 
  *  the logged in admin manager or branch manager when they view/search transactions.
  *  Enable  : the users
  *  Disable : 0
  */
 define("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS","Branch Manager,Admin Manager");
define("CONFIG_SENDER_BANK_BRANCH_DEPOSIT","1");

define("CONFIG_IMPORT_PAYMENT_AUDIT", "1");
/* #7454 Opal
 * Exported date is displayed on Export old transaction report [Latvia]
 * Enable  : "1" for all countries or LATVIA, i.e comma separated distribution countries
 * Disable  : "0"
 * by Aslam Shahid
 */
define("CONFIG_EXPORTED_FILE_DATE_COUNTRIES", "1");
/* #7451 Opal
 * Transaction can be edited from unresolved payments.
 * Also displays transactions without distributor as well
 * Enable  : "1" for all users or "admin,Admin," i.e comma separated users
 * Disable  : "0"
 * by Aslam Shahid
 */
define("CONFIG_EDIT_TRANSACTION_UNRESOLVED", "1");

/* #7459 Opal
 * Transaction can be cancelled from unresolved payments.
 * Also displays transactions without distributor as well
	* Currently transaction is cancelled with refund fee.
 * Enable  : "1" for all users or "admin,Admin," i.e comma separated users
 * Disable  : "0"
 * by Aslam Shahid
 */
define("CONFIG_CANCEL_TRANSACTION_UNRESOLVED", "1");

/* #8057 Opal
 * This stops automatic reconciliation when bib format file is imported [uploaded]
 * from "Reconcile Online Bank Statement"
 * Enable  : "1"
 * Disable : "0"
 * by Aslam Shahid
 */
define("CONFIG_STOP_AUTAMATIC_RECONCILE_BIB_FILE", "1");

/* #8058 Opal
 * This stops automatic reconciliation when normal barclays format file is imported [uploaded]
 * from "Reconcile Online Bank Statement"
 * Enable  : "1"
 * Disable : "0"
 * by Aslam Shahid
 */
define("CONFIG_STOP_AUTOMATIC_RECONCILE_BARCLAYS_FILE", "1");

/* #8058 Opal
 * This stops automatic reconciliation when barclays evening format file is imported [uploaded]
 * from "Reconcile Online Bank Statement"
 * Enable  : "1"
 * Disable : "0"
 * by Aslam Shahid
 */
define("CONFIG_STOP_AUTOMATIC_RECONCILE_EVENING_FILE", "1");

/* #8058 Opal
 * This config imports every type of tlaCode payment.
 * Enable  : "1"
 * Disable : "0"
 * by Aslam Shahid
 */
define("CONFIG_IMPORT_ALL_CODES", "1");

/**
 *	#9706: Master Payex:copy of premier fx sandbox
 *	Short Description
 *  This config is defined to deal with the rounding 
 *	off of amount and exchange rate in trading module
 *	Enable  : 1
 *  Disable : 0
 */
define("CONFIG_ROUND_NUMBER_TRADING","1");
/**
 *	#9808: PremierFx: Import and Export of MT103
 *	Short Description
 *	This config is used to set the format of the 
 *	refNumber in mt103
 *	Enable  : 1
 *  Disable : 0
 */
define("CONFIG_REF_NUMBER_FORMAT_MT103","1");

/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS
 * short description
 * this config is used to remove (Add New Quick Beneficiary/Add Beneficiary As Company) links from deal contract
 * "0" for OFF and remove the links and "1" for ON display them back
 */
define("CONFIG_FOR_BEN_LINKS_ON_DEAL_CONTRACT", "0");
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS
 * short description
 * this config is used to show default the bank section
 * "0" for OFF and  "1" for ON.
 */
define("CONFIG_DISPLAY_DEFAULT_BENEFICIARY_BANK_DETAIL", "1");
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS 
 * Short description
 * This config is for removel of details (address 1, address 2  etc) and making fields non-mandatory on add beneficiary page
 */
define("CONFIG_FOR_ADD_BENEFICIARY_CHANGES","1");
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS 
 * short description
 * This config is defined for making fields non mandatory on deal contract page.
 * */
define("CONFIG_FIELDS_NON_MANDATORY", "1");
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS 
 * short description
 * This config makes the beneficiary mandatory for making deal
 */
define("CONFIG_DEAL_WITHOUT_BENEFICIARY", "0");
/**
 * #9983: Premier Exchange: BENEFICIARY DETAILS ON DEAL CONTRACTS 
 * short description
 * this config is used for making transaction incomplete.
 */
define("CONFIG_FOR_INCOMPLETE_TRANSACTION","1");
/**
 * #9835: PremierFX: GB Group Integration
 * short description
 * for online customer id check
 */
//define("CHECK_ID_INFO_FROM_DB","0");
/**
 * #10232: Premier Exchange: Module to set Country based rules on Bank Transfer 
 * short description
 * this config is used to active module on new deal contract and pdf attachment
 */
define("CONFIG_CHK_RULE_BANK_DETAILS","1");

/**
 * #10232: Premier Exchange: Module to set Country based rules on Bank Transfer 
 * short description
 * for addition of fields in benbanks table and store information in ben bank table
 */
define("CONFIG_USE_BEN_BANKS_DETAILS", "1");
/**
  * #10289: Premier Exchange: Deal Confirmation Page should be as same PDF
  * change the layout of the print confirm deal and confirm deal page
  */
define("CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT","1");
