<?php

function textpattern($array,$seperator='',$pattern='[KEY]="[VALUE]"',$enablelast=false){
	$PatternString ='';
	$keys= array_keys($array);
	for($i=0;$i<sizeof($array);$i++){
		if($i == sizeof($array)-1){if(!$enablelast){$seperator='';}}
		$key = $keys[$i];
		$value = $array[$keys[$i]];
			$subject = $pattern;
			$subject = str_replace('[KEY]',$key,$pattern);
			$subject = str_replace('[VALUE]',$value,$subject).$seperator;
			$PatternString .= $subject;
	}
	return $PatternString;
}
function select_MySql($table,$data='*',$where=false, $showquery=false){
	if(is_array($data)){
		$data = array_string($data,$seperator=',',$enablelast=false);	
	}
	if($where){
	$query = 'Select '.$data.' FROM '.$table.' WHERE '.array_string($where,' AND ',false,'`[KEY]`="[VALUE]"');
	}else{
	$query = 'Select '.$data.' FROM '.$table;
	}
	if($showquery){
	echo "\n\n".$query."\n\n";
	}
	
	$result = mysql_query($query); 
	while(($resultArray[] = mysql_fetch_assoc($result)) || array_pop($resultArray));
	return $resultArray;
} 
//Set returntype as resource/boolean
function databaseConnect($host,$user,$password,$database,$returntype = 'resource'){
	$boolean = true;
	$con = mysql_connect($host,$user,$password);
	if (!$con){
		$boolean = false;
	  die('Could not connect: ' . mysql_error());
	  }
	if($returntype == 'boolean'){$return = $boolean;}else{$return = $con;}
		mysql_select_db($database, $con);
		
return $return;
}
//pattern [KEY]='[VALUE]'
//array_string(array [Input array],seperator [Value sperator], enablelast [Enable / Disable last sperator], pattern [Pattern for desired output with [KEY] and [VALUE]])
function array_string($array,$seperator='',$enablelast=true,$pattern=false){
	$keys= array_keys($array);
	$String = '';
	for($i=0;$i<sizeof($array);$i++){
		if($i == sizeof($array)-1){if(!$enablelast){$seperator='';}}
		$key = $keys[$i];
		$value = $array[$keys[$i]];
		$Values .= $value.$seperator;
		if($pattern){
			$subject = $pattern;
			$subject = str_replace('[KEY]',$key,$pattern);
			$subject = str_replace('[VALUE]',$value,$subject).$seperator;
			$PatternString .= $subject;
		}
	}
	$String = $Values;
		if($pattern){$String = $PatternString;}
	$return = $String;
	return $return;
}

function ifexists_mysql($array,$table, $like= false){
	$return = false;
	$arraykeys = array_keys($array);
	$fields = array_string($arraykeys,', ',false);
	if($like){$pat = "`[KEY]` LIKE '[VALUE]'";}else{$pat = "`[KEY]`='[VALUE]'";}
	$fieldvalue = array_string($array,' AND ',false,$pat);
	$query = "SELECT ".$fields." FROM `".$table."` WHERE ".$fieldvalue;
	//echo $query."\n\n";
	$result = mysql_query($query);
	
	if (!$result) {
		echo 'Could not run query: ' . mysql_error();
	}else{
		$row = mysql_fetch_row($result);
		if($row){$return = true;}    
	}
	return $return;
}
 function array_compare($array1, $array2) {
    $diff = false;
    // Left-to-right
    foreach ($array1 as $key => $value) {
        if (!array_key_exists($key,$array2)) {
            $diff[0][$key] = $value;
        } elseif (is_array($value)) {
             if (!is_array($array2[$key])) {
                    $diff[0][$key] = $value;
                    $diff[1][$key] = $array2[$key];
             } else {
                    $new = array_compare($value, $array2[$key]);
                    if ($new !== false) {
                         if (isset($new[0])) $diff[0][$key] = $new[0];
                         if (isset($new[1])) $diff[1][$key] = $new[1];
                    };
             };
        } elseif ($array2[$key] !== $value) {
             $diff[0][$key] = $value;
             $diff[1][$key] = $array2[$key];
        };
 };
 // Right-to-left
 foreach ($array2 as $key => $value) {
        if (!array_key_exists($key,$array1)) {
             $diff[1][$key] = $value;
        };
        // No direct comparsion because matching keys were compared in the
        // left-to-right loop earlier, recursively.
 };
 return $diff;
}; 

//File upload function Upload($_FILES['filefield']   File Name, UploadPath[ target path] , $ReplaceFile = false);
function Upload($File, $UploadPath, $Name, $ReplaceFile = false){
	$File["targetpath"] = $UploadPath.$Name;
  if ($File["error"] > 0){
	$Status = $File["error"];
  }else{
	if(!$ReplaceFile){
		if (file_exists($UploadPath. $File["name"])){
			$Status = "AlreadyExists";
		}else{
	      move_uploaded_file($File["tmp_name"],$UploadPath.$Name);
		}
    }else{
      move_uploaded_file($File["tmp_name"],$UploadPath.$Name);
      $Status = "UpdateSucessful";
    }
	if($Status != "AlreadyExists"){
		if($Status != "UpdateSucessful"){
			if (file_exists($UploadPath.$Name)) {
				$Status = "Stored";
			}else{
				$Status = "Unsucessful";
			}
		}
	}
  }
  $File['status'] = $Status;
  return $File;
}

//Write file
function fwrite_stream($filename, $string) {
		$fp = fopen($filename,'w');
		
    for ($written = 0; $written < strlen($string); $written += $fwrite) {
        $fwrite = fwrite($fp, substr($string, $written));
        if (!$fwrite) {
            return $fwrite;
        }
    }
	fclose($fp);
}
//$data = array(field=>array(a,b,c)) OR $data = array(field=>a) 
function insert_MySql($table, $data, $printstatus=false){
	$return = true;
	if(!is_array($data)){$msg = "Invalid input data type";}	
	$field_names = array();
	$field_names = array_keys($data);
	$values = array_values($data);
	$field_names = array_string($field_names,',',false,"`[VALUE]`");
	$values = array_string($values,',',false,"'[VALUE]'");
	$values = str_replace("'NULL'",'NULL',$values);
	$query = "INSERT INTO `".$table."` (".$field_names.") VALUES(".$values.");";

	$result = mysql_query($query);
	//echo $query;
	if($printstatus){
		if (!$result) {
			die(mysql_error());
			$return = false;
		}
	}
	return $return;
}
function update_MySql($table, $data, $Where,$printstatus=false,$printquery=false){
	echo "Hello World";
	$return = true;
	if(is_array($data)){
			$setvalues = textpattern($data,", ","`[KEY]`='[VALUE]'");
	}
	$keys = array_keys($Where);
	$key = $keys[0];
	$value = $Where[$keys[0]];
	$query= "UPDATE `".$table."` SET  ".$setvalues." WHERE  `".$key."` ='".$value."' ";
	$result = mysql_query($query);
	if($printquery){var_dump($query);}
	if($printstatus){
		if (!$result) {
			die(mysql_error());
			$return = false;
		}
	}
	return $return;
	
}
// returns data in spcific text pattern [table name [String], fields[array], text pattern [String], Where Clause [String]]
function text_patternMysql($table,$fields,$pattern,$Where=false){
	$array = select_MySql($table,$fields,$Where);
	$return_string = "";
	
	$new_array = array();
	for($i=0;$i<sizeof($array);$i++){
	if($i == sizeof($array)-1){if(!$enablelast){$seperator='';}}
		$temp_array=$array[$i];
		$temp_keys = array_keys($temp_array);
		$temp_pattern = $pattern;
	 	for($n=0;$n<sizeof($temp_keys);$n++){
			$key = $temp_keys[$n];
			$value = $temp_array[$key];
			$pat = '['.$n.']';
			$temp_pattern = str_replace($pat,$value,$temp_pattern);
		}
		$return_string .=$temp_pattern.$seperator;
	}
	return $return_string;
}
function format_label($array){
	$array_keys = array_keys($array);
	$temp = array();
	for($i=0;$i<sizeof($array);$i++){
		$key = $array_keys[$i];
		$tkey = $key;
		$key = ucwords(str_replace('_',' ',$key));
		$temp[$key] = $array[$tkey];
	}
	return $temp;
}
function ProcessConfigFile($clientName, $filename, $tablename, $host,$user,$password,$db, $logfile = false){

	$con = mysql_connect($host,$user,$password);
	if (!$con){die('Could not connect: ' . mysql_error());}
	mysql_select_db($db, $con);
	
	$count = 0;
	$ConfigCollection = array();
	$data = array();
	$Config = array();
	$Duplicate = 0;
	
		

	if(!mysql_num_rows( mysql_query("SHOW TABLES LIKE '".$tablename."'"))){
		$tblQuery = "CREATE TABLE IF NOT EXISTS `".$tablename."` (
		  `id` int(11) NOT NULL auto_increment,
		  `varName` varchar(100) NOT NULL,
		  `value` varchar(256) NOT NULL,
		  `oldValue` varchar(256) NOT NULL,
		  `description` varchar(100) NOT NULL,
		  `detailDesc` text NOT NULL,
		  `isFlag` enum('Y','N') NOT NULL default 'N',
		  `client` varchar(50) NOT NULL,
		  PRIMARY KEY  (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;
		";




		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		fclose($handle);
		$subject = $contents;
		$UnProcessedSubject = $subject;
		$pattern = '/define.*?\(.*?"(.*?)".*?,.*?"(.*?)".*?\).*?;/s';
		$match = array();
		preg_match_all($pattern,$subject,$match);			
		$matchedCompletes = $match[0];
		$TotalVariables = sizeof($matchedCompletes);
		for($i=0;$i<sizeof($matchedCompletes);$i++){$subject = str_replace($matchedCompletes[$i],"XXX",$subject);}
		$count += sizeof($match[1]);
		$tmp = array_combine($match[1],$match[2]);
		$Config[$tablename] = $tmp;
		$d = sizeof($ConfigCollection);
		$ConfigCollection = array_merge($ConfigCollection,$tmp);
		$a = sizeof($ConfigCollection);
		$b = sizeof($tmp);
		$c = $a-$d;
		$data['a'][] = sizeof($ConfigCollection);
		$data['b'][] = sizeof($tmp);
		$data['c'][] = $c;

		$Log .= $subject;

		$config_keys = array_keys($Config);
		for($i=0;$i<sizeof($config_keys);$i++){
			$tablename = $config_keys[$i];
			$client = $clientName;
			$cdata = $Config[$tablename];
			$cdata_keys = array_keys($cdata);
			mysql_query($tblQuery);
			for($n=0;$n<=sizeof($cdata_keys);$n++){
				$value = $cdata[$cdata_keys[$n]];
				$name = $cdata_keys[$n];
				$array = array();
				$array['varName'] = $name;
					if(!ifexists_mysql($array,$tablename)){
				$insrtQuery=  "INSERT INTO `".$tablename."` (`id`, `varName`, `value`, `oldValue`, `description`, `detailDesc`, `isFlag`, `client`) VALUES 
(null, '".$name."', '".$value."', '0', '', '".$comment."', 'N', '".$client."')";
					mysql_query($insrtQuery);
					}else{
					 //echo "Duplication \n\n";
					 $Duplicate++;
					}
			}
		}

		/// Write Log file
		if($logfile){fwrite_stream($logfile, $Log);}

		// Result calibration
		$match = array();
		$pattern = '/define.*?\(.*?"/i';
		preg_match_all($pattern,$Log,$match);
		$remains = sizeof($match[0]);
		$percent = (($TotalVariables - $remains) / $TotalVariables) * 100;

		$Result = array();
		$Result['CaptureSuccessful'] = $TotalVariables;
		$Result['CaptureUnsuccessful'] = $remains;
		$Result['Accuracy'] = $percent;
		$Result['Processed'] = $subject;
		$Result['UnProcessed'] = $UnProcessedSubject;
		$Result['Duplicate'] = $Duplicate;
		$Result['Status'] = "Sucessful";
		if(sizeof($Result['CaptureSuccessful']) == sizeof($Result['Duplicate'])){
			$Result['Status'] = "Failed";
		}
	}else{$Result['Status'] = "DuplicateRequestedTable";}
	
	return $Result;
}
?>