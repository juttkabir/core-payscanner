<?

# Client specific variables
error_reporting(E_ALL - E_NOTICE);

# Path to smarty template files
define('TEMPLATE_DIR', "$APP_ROOT/tpl/");

# Set magic quotes based on database type.  If using either of these and using odbc, 
# you will need to set this by hand.  
# You should be able to safely comment this out if your system is setup right. 
ini_set('magic_quotes_sybase', (int)($dbType == 'mssql' || $dbType == 'sybase'));

# Include pear database handler, auth object (remove if not used), and smarty
require_once 'DB.php';
require_once 'Smarty/Smarty.class.php';
require_once "auto_session.php";

# Change error handling as necessary
# PEAR_ERROR_RETURN, PEAR_ERROR_PRINT, PEAR_ERROR_TRIGGER, PEAR_ERROR_DIE or PEAR_ERROR_CALLBACK
# PEAR::setErrorHandling(PEAR_ERROR_PRINT);
PEAR::setErrorHandling(PEAR_ERROR_CALLBACK, 'errhndl');
function errhndl ($err) {
    echo '<pre>' . $err->message;
    die();
    print_r($err);  # print_r is very useful during development but will display the db uid/pwd on query failure.
    die();
} 


# Connect to the database
$db = DB::connect($DSN);
$db->setFetchMode(DB_FETCHMODE_ASSOC);  # Other modes possible; I find assoc best.
$db->setOption('optimize', 'portability');  # This is useful for apps supporting multiple backends such as mysql & oracle


# Store and load DataObjects Configuration
	
define("DB_DATAOBJECT_NO_OVERLOAD",true);	# This is needed for some buggy versions of PHP4

$options = &PEAR::getStaticProperty('DB_DataObject','options');
$options = array(
    'database'         => "$DSN",
    'schema_location'  => "$APP_ROOT/db/dataobjects",
    'class_location'   => "$APP_ROOT/db/dataobjects",
    'require_prefix'   => 'db/dataobjects',
    'class_prefix'     => 'DataObject_',
    'proxy'			   => 'full',
);


# Setup template object - NOTE:  in this example, 'smarty' is a symlink to 
# the smarty directory.  This allows you to upgrade Smarty without changing code.
$smarty = new smarty;
$smarty->left_delimiter = '{{';
$smarty->right_delimiter = '}}';

$smarty->template_dir = TEMPLATE_DIR;
# For other compile and cache directory options, see the comment by Pablo Veliz at the bottom of this article.
$smarty->compile_dir = $APP_ROOT . '/compile';
$smarty->cache_dir = $APP_ROOT . '/cache';
# Because you should never touch smarty files, store your custom smarty functions, modifiers, etc. in /include
$smarty->plugins_dir = array($APP_ROOT . '/include', $APP_ROOT . '/smarty/plugins');

if ($SMARTY_CACHING) {
    $smarty->caching = true;
} else {
    $smarty->force_compile = true;
}

session_start();

# Assign any global smarty values here.
$smarty->assign('web_root', $WEB_ROOT);

#
# Set all the auto session variables
#
autoSession();

$smarty->assign_by_ref('session',$_SESSION);
?>
