<?php

/********************
* To define Client to use System.
********************/
define("SYSTEM","Monex Transfer Ltd");
define("CLIENT_NAME", "monextransfer");
define("CONFIG_SECOND_LOGO_ENABLE","1");
define("CONFIG_SECOND_LOGO","../admin/images/monextransfer/emts1.gif");
define("CONFIG_MONEX_LOGO_WIDTH","133"); 
define("CONFIG_MONEX_LOGO_HEIGHT","93");
define("CONFIG_MONEX_IMAGES","1");
//define("CONFIG_CUSTOM_COLOR_ENABLE","1");
/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/monextransfer/logo.jpg");
define("CONFIG_LOGO_WIDTH","133");
define("CONFIG_LOGO_HEIGHT","93");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","0");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","1");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I confirm that the source of funds for purposes of money transmission is legitimate and has no predicate offence motives.");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");

/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","1");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","1");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","0");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","0");



/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Payin Book Sender");
define("CONFIG_CONTACT_NUMBER_SEARCH_PHONE_MOBILE","1");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "4");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","1");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","MT");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","MT Code");
define("COMPANY_NAME","Monex Transfer Ltd");
//define("COMPANY_NAME_EXTENSION","Money Transfer");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "admin@monextransfer.com");
define("SENT_ADDRESS", "admin@monextransfer.com");
define("INQUIRY_EMAIL", "admin@monextransfer.com");
define("CC_EMAIL", "admin@monextransfer.com");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Monex Transfer Support");


define("CONFIG_INVOICE_FOOTER", "0800 8620142 (UK ONLY) 
Email: customerservice@monextransfer.co.uk ");
define("COMPANY_URL","www.monextransfer.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Monex Payex");
define("TITLE_ADMIN", "Monex Payex");


define("COMPANY_ADDRESS","");
define("COMPANY_PHONE", " ");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","0");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","0");
define("CONFIG_RECIEPT_NAME","express-confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT","print-ex-confirmed-transaction.php");

//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","0");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","0");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");
/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
********************/
define("CONFIG_TELLER_ACCESS","0");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","0");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","1");


/********************
* This switch is to enable/disable 
* Auto generating reference number
* 0 Disable auto( User will rather manually input the reference number, while creation of transaction)
* 1 Enable auto( System will itself genrate the ID)
********************/
define("CONFIG_AUTO_TRANS_REFRENCE","1");


/********************
* This switch is to enable/disable Internal Remarks in transactions
* Only the company will see it internally..... Not distributor side will be allowed to view it
* 0 Disable Internal remarks 
* 1 Enable Internal remarks 
********************/
define("CONFIG_INTERNAL_REMARKS","1");

/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");

/********************
* Two exchange rates for a country based on amount
* 0-  Disable
* 1-  Enable
 ********************/
define("CONFIG_DIVIDE_RATE","1");
define("CONFIG_DIVIDE_FOR_COUNTRY","NIGERIA");

/********************
* Transaction Type at add beneficiary 
* 0-  Enable
* 1-  Disable
 ********************/
define("CONFIG_TRANS_TYPE_BENE","0");

/********************
* Transaction Visible Answer for Secret Question
* while adding a transaction
* 0-  Disable
* 1-  Enable
 ********************/
define("CONFIG_TRANS_SECRET_ANS","0");

/********************
* Online User Managment(Client's defined) .... Sender Admin(Core payex default)
* It is a switch to show Label from above at the link, where admin or admin staff can view and access Online Customer
* and can do cetain operations like creation of transaction on customer's behalf
* 0-  Sender Admin
* 1-  Any other specified like 'Online User Management'
 ********************/
define("CONFIG_LABEL_ONLINE_SENDER","1");
define("CONFIG_LABEL_VALUE","Online User Management");

/********************
* To show Super Agent color. This variable is used in following Pages
* Add Super Agent, Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_COLOR","1");

/********************
* This variable is used to show Distributor_Output_File_Monex.php page for monex
* It is used in left.php
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MONEX_DIST_OUTPUT_FILE","1");

/********************
* It will enable the option for "IP Access"
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCESS_FROM_IP","0");

/********************
* It will enable to add/update messages for collection points
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CP_MESSAGE_ENABLED","1");

/********************                                                          
* It will enable to show the Manual code or system generated code at time of create transaction
* 0-  System generated reference number                                                                 
* 1-  Company Code manual transaction code                                                                   
********************/ 
define("CONFIG_TRANS_REFERENCE_SHOW","1");  

/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","1");	

/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");

/********************
* Switch to Enable/Disable specific country fee calender (other than super admin) 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_COUNTRY_BASE_FEE","1");

/********************
* Switch to Enable/Disable agent can edit its own transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EDIT_TRANS","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","0");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* This switch is to enable/disable 
* Auto generating reference number
* 0 Disable auto( User will rather manually input the reference number, while creation of transaction)
* 1 Enable auto( System will itself genrate the ID)
********************/
define("CONFIG_AUTO_TRANS_REFRENCE","1");

/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","0");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)
* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "0");
define("CONFIG_PAYIN_RESTRICT_LIST", "SUPA,SUPAI");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "Admin, SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");
define("CONFIG_USERS_FOR_MANDATORY_ADRESS","SUBA,SUBAI,SUPA,SUPAI");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","1");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","0"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/

define("CONFIG_CUSTBEN_OLD_TRANS", "1");



/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "0");




/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,");
define("CONFIG_TRANS_ROUND_CURRENCY", "GHS,GBP,EURO,USD");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "0");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
/********************
* Switch to Enable/Disable an agent associated with payin book customers
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_AGENT_NUMBER", "100221");
 	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "1"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "1"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "0");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "PLN");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "1");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than �700 require that you have an ID on file � if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo � e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*

*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "0");



/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","1");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","0");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","1");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");

/********************
* Switch to Enable/Disable Drop Down of "ID Type" rather than radio buttons
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_IDTYPE_DROPDOWN", "1");

/********************
* Switch to Enable/Disable ID Expiry with Calendar (not drop down)
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "0");

/********************
* Switch to Enable/Disable Free text ID fields
* user has to input dates like DD-MM-YYYY
* CONFIG_IDEXPIRY_YEAR_DELTA is the maximum year (from now) that user inputs
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_FREETEXT_IDFIELDS", "0");
define("CONFIG_IDEXPIRY_YEAR_DELTA", "53");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "0");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Driving License,UK Driving License");

/********************
* Switch to Disable check box terms and conditions during create transaction
* 1 means Disable 
********************/
define("CONFIG_TnC_CHECKBOX_OFF", "0");

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
* 0 Disable
* 1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable import bulk Transactions 
* 0 Disable
* 1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable layout like 1- Last Name 2- First Name 3- Patronyme(Mid Name)
* during add/update sender/beneficiary
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_NAME_SEQUENCE", "0");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable mobile field OFF
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_MOBILE_FIELD_OFF", "0");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "1");

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "0");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable Swaping of Amount and Local Amount up and down
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

/********************
* Switch to Enable/Disable functionality of default Distribuotr
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DEFAULT_DISTRIBUTOR", "1");


/********************
* This switch is to enable/disable Internal Remarks in transactions
* Only the company will see it internally..... Not distributor side will be allowed to view it
* Made for Monex and Opal transfer
* 0 Disable Internal remarks
* 1 Enable Internal remarks
********************/
define("CONFIG_INTERNAL_REMARKS","1");
define("CONFIG_LABEL_INTERNAL_REMARKS","Internal Remarks for the Transaction");

/********************
* Switch to Enable/Disable Address Line 2 for customer and Beneficiary
* Especially done for Opal
* If value is '1' its Disabled
* If value is '0' its Enabled
********************/
define("CONFIG_DISABLE_ADDRESS2", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Enable or Disable
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SHOW_BALANCE","1");

/******************** * Remove currency filter from daily cashier report 
* 1- Hide 
* 0- Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");


/********************
* Switch to Enable/Disable agent's last 30 day transaction for nco global
*       0 Disable
*       1 Enable
********************/
define("CONFIG_AGENT_LAST_30_DAY_TRANSACTION", "1");

/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");


/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");

/********************    
* Added By Niaz Ahmad #2918 at 06-02-2008                                                                     
* Switch to Enable/Disable calculation of cumulative amount for specific period
* When Config ON calneder will be shown at Compliance Configurations At Create Transaction
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_COMPLIANCE_NUM_OF_DAYS", "1");
define("CONFIG_EXPORT_TRANS_OLD","1");
define("CONFIG_ACCOUNT_TYPE_ENABLED","1");
define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Switch to Enable/Disable show list,by default, of transactions 
from distributor end on release page when clicked on 
the link "new transactions for delievery" in main menu
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/


define("CONFIG_SHOW_TRANS_LIST","1");

/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");

/********************
* Switch to Enable/Disable prove address mandatory
* Created by Yameen Adnan on 14-05-2008
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS_MANDATORY", "1");


/********************
* Switch to Enable/Disable Documents provided mandatory
* Created by Yameen Adnan on 14-05-2008
* 0 means Disable
* 1 means Enable 
********************/
define("CONFIG_DOCUMENTS_PROVIDED_MANDATORY", "1");


/********************
* Switch to check/uncheck Documents Provided checkbox
* Created by Yameen Adnan on 14-05-2008
* 1 means uncheck
* 0 means check 
********************/
define("CONFIG_UNCHECK_DOCUMENTS_PROVIDED", "1");


/********************
* Switch to change option value from By Bank Transfer to Inclusive of Fee and By Cash to Exclusive of Fee on add-transaction.php page
* Created by Yameen Adnan on 14-05-2008
* 1 means modify
* 0 means do not modify 
********************/
define("CONFIG_MODIFY_BYBANKTRANSFER_TO_INCLUSIVEOFFEE", "Inclusive of Fee");


/********************
* Switch to change option value from By Bank Transfer to Inclusive of Fee and By Cash to Exclusive of Fee on add-transaction.php page
* 1 means modify
* 0 means do not modify 
********************/
define("CONFIG_MODIFY_BYCASH_TO_EXCLUSIVEOFFEE", "Exclusive of Fee");


/********************
* Switch to change option value from Pick up (Default) to Bank transfer on add-transaction.php page
* 1 means modify
* 0 means do not modify 
********************/
define("CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER", "Bank Transfer");

/********************
* remove quick sender/beneficary link from add transaction 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMOVE_QUICK_LINK","0");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable                                                                                   
*	1 Enable 
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");


/********************
* With this config the distributor can only view its own transaction in DOF

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/



define("CONFIG_DISTRIBUTOR_LIST","1");

/********************
* Added by khola aginst ticket #3224
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS", "1");

/********************
* Added by khola aginst ticket #3224
* Switch to Enable/Disable to manage export transactions a/c to opal
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_OPAL", "1");

/********************
* created by Khola against ticket # 3264 on 10-06-2008
* Switch to Enable/Disable opal daily cash report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DAILY_CASH_REPORT","1");
/*Custom Currency Denomination Menu Caption*/
define("CONFIG_REPORT_CAPTION","Daily Cash Report");

/*Daily cash report to show agent's transaction amount*/
define("CONFIG_DAILY_CASH_FOR_AGENT","1");




/**
 * Defining the config to use the IBAN number at quick beneficiary page
 * @Ticket# 3474
 * This config gneeds to be off as now
 * IBAN field is Radio button based.
 * So either on CONFIG_IBAN_OR_BANK_TRANSFER or this config(NT).
 */
define("CONFIG_IBAN_ON_QUICK_BEN","0");




/********************
* Added by khola aginst ticket #3463
* Switch to Enable/Disable default value for calculate by option.
* The config CONFIG_DEFAULT_VALUE_CHOICE holds the value which is 
* required as default by the client .i.e either inclusive or exclusive.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DEFAULT_VALUE_FOR_CALCULATEBY", "1");
define("CONFIG_DEFAULT_VALUE_CHOICE", "exclusive");


/********************
* created by javeria against ticket # 3466 on 28-06-2008
* to show/hide prove address type against prove of address check box
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PROVE_ADDRESS_TYPE","1");
define("CONFIG_PROVE_ADDRESS_TYPE_VALUES","utility bill,drivers licence");

/********************
* Added by Hasan Tariq aginst ticket #3456
* Switch to Enable/Disable to show other as transction purpose default value
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_OTHER_TRANSACTION_PURPOSE", "1");


/*********************************************
* Added by Usman Ghani aginst ticket #3467 
* Config to make the post code necessary for only Monex
* If value is '0' its not mendatory
* If value is '1' its mendatory
*********************************************/
define("CONFIG_POST_CODE_MENDATORY", 1);


/*********************************************
* Added by Usman Ghani aginst ticket #3469 
* This config is added to select the poland as a default country for Monex
* If value is '0' its off
* If value is '1' its on
*********************************************/
/* Commented the configs to meet requirement @Ticket #4500 
	define("CONFIG_DEFAULT_COUNTRY", 1);
	define("CONFIG_DEFAULT_COUNTRY_VALUE", "Poland");
*/


/*********************************************
* Added by Usman Ghani aginst ticket #3471 
* This config is added to make the benefeciary address line necessary for now transfer on add benefeciary page.
* If value is '0' its off
* If value is '1' its on
*********************************************/
define("CONFIG_BENEFICIARY_ADDRESS_MENDATORY", 1);

/*********************************************
* Added by Usman Ghani aginst ticket #3473  Phone/Mobile validation
* To validate phone and mobile numbers only to numeric values 0123456789
* If value is '0' its off
* If value is '1' its on
*********************************************/
define("CONFIG_PHONE_NUMBER_NUMERIC", 1);

/*********************************************
* Added by Usman Ghani aginst ticket #3472:  Phone/Mobile
* If this config is on, the phone and mobile number will be shown as dropdown where one could be selected at a time.
* Also the user provide a phone number if this config is on.
* If value is '0' its off
* If value is '1' its on
*********************************************/
define("CONFIG_PHONES_AS_DROPDOWN", 1);

/*********************************************
* Added by Usman Ghani aginst ticket #3459: 
* If this config is on, the other purpose option will 
* not be shown in transaction purpose dropdown box on create transaction page.
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_DO_NOT_SHOW_OTHER_PURPOSE", 0);

/*********************************************
* Added by Usman Ghani aginst ticket #3459: 
* If this config is on, the "Fee discount" check box will not be shown on create transaction page.
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_DO_NOT_SHOW_FEE_DISCOUNT", 1);

/*********************************************
* Added by javeria aginst ticket #3471: 
* show states on add beneficiary form instead of countries
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_SHOW_STATES", 1);


/*********************************************
* Added by Usman Ghani aginst ticket #3468: 
* If it is on, a custom list upload files will be implemented.
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_CUSTOM_UPLOAD_FILE_LIST", 1);

/*********************************************
* Added by Khola aginst ticket #3465:
* If this config is on, documents provided option will be shown as drop down with values 
* yes and no instead of check box.
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_DCOUMNETS_PROVIDED_AS_DROPDOWN", "1");

/*********************************************
* Added by Khola aginst ticket #3459:  
* If this config is on, Tip field will not be shown on create transction page. 
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_HIDE_TIP_FIELD", "1");

/*********************************************
* Added by javeria aginst ticket #3456:  
* If value is '0' its off.
* If value is '1' its on.
*********************************************/
define("CONFIG_HIDE_OTHER_PURPOSE", "1");

/**
 * Config added to show funds source dropdown on online sender module's add-tranasction page for nowtransfer.
 * 1  It is on
 * 0  It is off
 * @Author: Usman Ghani
 */
define("CONFIG_SHOW_FUNDS_SOURCE_ON_ONLINE_MODULE", 1);

/**
 * Config added to show funds source dropdown on online sender module's add-tranasction page for nowtransfer.
 * 1  It is on
 * 0  It is off
 * @Author: Usman Ghani
 */
define("CONFIG_SHOW_DEFAULT_FUND_SOURCE", 1);
define("CONFIG_DEFAULT_FUND_SOURCE_VALUE", "Salary");


/********************
* Switch to show Remarks field in Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");



/********************
* Variable used to define value of the
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");

/**********************
* Config Added against #3544 - online sender module enhancements.
* 
* 
***********************/
define( "CONFIG_DONT_SHOW_TRANS_TYPE_ON_BEN_PAGE", 1 );
define( "CONFIG_DONT_SHOW_BANK_DETAILS_ON_ADD_TRANS_PAGE", 1 );

/********************
* Agents Associated to online customers
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ONLINE_AGENT","1");

/**
 * Added against ticket #3544
 * Config added to attach custom transaction page with online modules's transactions. *
 */
define("CONFIG_CUSTOM_CUSTOMER_TRANSACTION", '1');
define("CONFIG_CUSTOMER_TRANSACTION_PAGE", "add-transaction.php");

define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",'1');

/******************
* This config allow user to have multiple ID type and their information
* on regular sender and beneficiary form
*       0 Disable
*       1 Enable
******************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", "1");

/**
 * Assign online sender on adding agent page of Payex.
 * @Ticket #4138
 */
define("CONFIG_USE_VIRTUAL_AGENT","1");
//define("CONFIG_VIRTUAL_AGENT_URL_PREFIX","http://payex.niaz.horivert.com");
define("CONFIG_VIRTUAL_AGENT_URL_PREFIX","http://online.monextransfer.com");

/********************
* Switch to SHOW/HIDE already added exchage rate on add rate page to the agent--added by javeria,#2758
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_SHOW_EXCHANGE_RATE","1");
define("CONFIG_AGENT_OWN_RATE" , "1");


define("CONFIG_UNLIMITED_EXCHANGE_RATE","1");
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","POLAND,");

/**
 * Define exchange rate in term of denomination based
 * @Ticket #4269
 */
//define("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE","1");

define("CONFIG_MANUAL_FEE_RATE","1");
define("CONFIG_BEN_COUNTRY","NIGERIA");


/* Added against Ticket #4319 */
define("CONFIG_AGENT_OWN_MANUAL_RATE","1");
define("CONFIG_MANUAL_RATE_USERS","admin,SUPA");
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX","1");


define("CONFIG_CUSTOMIZED_OUTPUT_FILE","Distributor_Output_File_Monex.php");

/********************
* Switch to Enable/Disable multiple links to authorize/verify Transactions 
*	according to sending countries.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_COUNTRY_BASE_LINKS","1");

/**
 * At the quick menu, to show the pending or authorization links , country wise
 * Assign this config "1"
 */
define("CONFIG_COUNTRY_WISE_TRANSACTION_COUNT_AT_QUICK","1");

/* Search Filters at the Exchange Rate List */
define("CONFIG_EXCHANGE_RATE_FILTERS","1");
?>