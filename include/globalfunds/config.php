<?php

// Load the config from the database
if(!defined("LOCAL_DIR"))
	include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","globalfunds");
define("CLIENT_NAME","globalfunds");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/globalfunds/logo.jpg");
define("CONFIG_LOGO_WIDTH","227");
define("CONFIG_LOGO_HEIGHT","40");


/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","0");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");


/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");

/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");

/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_CONDITIONS","../admin/images/moneytalks/Terms_and_conditions.doc");
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I accept that I haven't used any illegal ways to earn money. I also accept that I am not going to send this money for any illegal act. I also accept the full terms and conditions of Global Funds Limited");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","0");

/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");

/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");

/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");

/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");

/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");

/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");
/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");
define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","1");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");

/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","0");

/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");

/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");
/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");
/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");

/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");

/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "0");

/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");

/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");

/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");

/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","GF");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Global Funds Code");
define("COMPANY_NAME","Global Funds");
define("CONFIG_AGENT_STATMENT_GLINK","1");

/********************
* I can't remeber why it is created...perhaps Imran did it
********************/
define("PAGE_LENTH","1");

/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

/********************
* Switch to Enable/Disable Admin to login into other user's data
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ENABLE_LOGIN","0");

/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");

/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");

/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "reyomsglobal@yahoo.com");
define("INQUIRY_EMAIL", "reyomsglobal@yahoo.com");
define("CC_EMAIL", "reyomsglobal@yahoo.com");
define("SUPPORT_NAME", "Global Funds Ltd");
define("CONFIG_INVOICE_FOOTER", "(Tel)<br>(Mob) ");
define("COMPANY_URL","");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");
define("TITLE","::.Global Funds Ltd.::....");
define("TITLE_ADMIN", "::...Payex- Global Funds Ltd ...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report:
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");


/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");


/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");


/********************
* Switch to Enable or Disable GLSuper
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SUPER_AGENT","0");


/********************
* Switch to Enable or Disable Pending/Processing Transactions in Ddaily Distributor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DISTRIBUTOR_PENDING","1");


/********************
* Switch to Enable or Disable Branch Code of Collection Point in Distributor Output File
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIST_FIELDS", '0');


/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","DIST_COMM, DAILY_CASHIER, TELLER_ACCOUNT, DAILY_AGENT, OUSTSTANDING_AGENT,");

/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");

/********************
* Agents Associated to online customers
* Functionality made for GLink
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ONLINE_AGENT","1");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

/********************
* Variable indicating that ID type is mandatory while creating beneficiary/sender
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ID_MANDATORY","1");

/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");   

/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************
* Enable or disbale Editing option for amount and local amount
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EDIT_VALUES","1");

/********************
* Enable or disbale Mobile Compulsion for express & glink
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_COMPULSORY_MOBILE","0");

/********************
* Mobile Number format

********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Switch to Enable/Disable Exchange Rate Margin Type
*       built for Spinzar
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");


/********************
* Switch to Enable/Disable Exchange Rate Calculatio Report for
* Agents and Company
* Built for Spinzar
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_SHARE","1");

/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");

/********************
* Enabling discount report for money talks for the case where discount request is auto authorized for user who creates transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_FEE_DISCOUNT_REPORT","1");

/********************                                                                         
* Switch to Enable/Disable full name search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");

/********************
* Add or remove search filter mobile number on export customer output file
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MOBILE_FILTER","1");


/********************
* Enable or Disable
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SHOW_BALANCE","1");

/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");

/********************
* Add or remove email alert for moneytalks ticket 1974
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ALERT","1");
define("CONFIG_ALERT_MESSAGE","Weekly Report Update ");
define("CONFIG_ADD_ALERT_PAGE", "weeklyReportController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageWeeklyReportController.php"); 		 	


/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;


/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");

/********************
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS", "1");

/********************
* Switch to Enable/Disable to manage export transactions 
* Old and New link 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANS_OLD", "1");

/********************                                                                         
* Switch to Enable/Disable count for new/old transactions to be exported   
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_EXPORT_TRANSACTION_COUNT", "1");


/********************
* Switch to Enable/Disable Export Commission Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_COMMISSION", "1") ;


/********************
* Switch to Enable/Disable Export Commission Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_COMMISSION_EMAIL", "1") ;

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");

define("CONFIG_CUSTOM_TRANSACTION", "1");
define("CONFIG_TRANSACTION_PAGE", "add-transaction_opaldesign.php");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* link variable is 
* CONFIG_COMPLIANCE_CUSTOM_COLUMN, 
* and CONFIG_COMPLIANCE_COUNTRY_REMOVE, for opal
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");


/********************
* Enable or disbale inclusive or exclusive calculation

* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CALCULATE_BY","1");

/********************
* Switch to Enable/Disable Swaping of Amount and Local Amount up and down
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT", "1");

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,");
define("CONFIG_TRANS_ROUND_CURRENCY", "USD,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");

/********************
* Enable or disbale 'send to confirm' button
* For Opal
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SEND_ONLY_BUTTON","1");

/********************
* Switch to Enable or Disable to Skip Confirm Transaction Step
* while creating a transaction
* For Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_SKIP_CONFIRM","1");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");

/********************                                                                         
* Switch to Enable/Disable rights to view a report in user functionality
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_REPORT_ALL", "1");

/********************     
* Add by Niaz Ahmad #2736 at 10-12-2007                                                                    
* Switch to Enable/Disable INPUT Fields for supper/sub distributors
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_VIEW_TRANSACTIONS_DISTRIBUTORS", "1");
define("CONFIG_FEE_CURRENCY","1");

/********************     
* Add by Khola Rasheed against #2735 at 26-12-2007                                                                    
* Switch to Enable/Disable collection point option on add/update fee/commission
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_FEE_BASED_COLLECTION_POINT", "1");
/********************
* Added by Niaz Ahmad #1727 at 08-01-2008
* 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT","1");
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");
/********************
* Added by Niaz Ahmad against ticket #2876 at 25-01-2008
* Switch to Enable/Disable View of all transactions on Distributor Output file
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_LIST", "1");

/********************
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
*	0 means Disable
*	1 means Enable 
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");


/********************
* Switch to Enable/Disable generic exchange rate option on agent and AnD's login
*       0 Disable
*       1 Enable
********************/
define("CONFIG_GENERIC_RATE_FOR_AnD","1");

/********************
* Added by Niaz Ahmad Spinzar Meeting Minutes  at 31-03-2008 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_VIEW_CURRENCY_LABLES","1");
define("CONFIG_ADD_AMOUNT_COLUMN", "1");
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");
define("CONFIG_AGENT_STATEMENT_CUMULATIVE", "1"); // to hide comulative total from top of super A&D Report
define("CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT", "1");


/********************
* Switch to Enable/Disable Sub Account Statement Reports
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUB_ACCOUNT_REPORT", "1");

/********************
* Switch to show Remarks field on Cancel Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");
define("CONFIG_REMARKS","Remarks");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

//define("CONFIG_OPTIMISE_NAME_SEARCH","1");


/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
*       1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");


/********************
* Distributor based Fee/Commission. it works only when the agent logged in or selected then their fee used
* Switch to Enable/Disable to put agent based fee
* 0 Disable
* 1 Enable
********************/

define("CONFIG_FEE_DISTRIBUTOR","1");


/********************
* Added by Javeria #3115
* agent name filter and agent name column on manage fee pafe
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_MANAGE_FEE","1");


define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","1");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY,
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example,
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","0");

define("CONFIG_BEN_COUNTRY","BRAZIL");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
* inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled
* and CONFIG_COMMISSION_STATUS_ON_CANCELATION should be enabled so that the payment mode 
* value is entered in transaction table in commissionMode field.
*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");

/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");

/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");

/********************
* Switch to SHOW/HIDE already added exchage rate on add rate page to the agent--added by javeria,#2758
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_SHOW_EXCHANGE_RATE","1");
define("CONFIG_AGENT_OWN_RATE" , "1");

/*******************
Distributor based exchange rate. Functionality already exists but is working fo connectplus exchange rates.
* If value is '0' its Disabled
* If value is '1' its Enabled
Added by Khola @ticket #3712
*******************/
define("CONFIG_EXCHNG_DIST_BASED","1");



define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/**
 * If client want to have full name or number of agent at the prefix, than this config should be enabled
 * @Ticket #3833
 */
define("CONFIG_USE_FULL_NAME_AT_REFRENCE_GENERATOR_PREFIX",1);

/**
 * If to show full agent number in the postfix of the Transaction refrence number
 * @Ticket #4089
 */
define("CONFIG_SHOW_FULL_AGENT_NUMBER_IN_POSTFIX_REFNO","1");

/**
 * Confid to show separate export transaction link for both Pick Up and Bank Transfer
 * @Ticket #4130
 */
define("CONFIG_SEPARATE_LINK_FOR_BOTH_TRANSACTIONS","1");

/**
 * In the A&D Account Statement, if below defined config is "1" than 
 * Admin Manager can deposit into the A&D Account
 * @Ticket #4422
 */
define("CONFIG_ADMIN_MANAGER_DEPOSIT", "0");
/**
 * In the A&D Account Statement, if below defined config is "1" than 
 * Account Manager can deposit into the A&D Account
 * @Ticket #4345
 */
define("CONFIG_ACCOUNT_MANAGER_DEPOSIT", "1");

/**
 * On the quick menu, to diaplay the multiple authorisation links
 * i.e. one link for Bank and Cash and other for Transaction with Cheque
 * To enable this functionality assign below mentioned link with "1"
 * If this config is marked as "0", than one link will be displayed for all the transaction types
 * @Ticket #4362
 */
//define("CONFIG_MULTI_LINKS_AT_MAIN_MENU","0");

/********************
* Added by Khola #3617
* dist name filter and dist name column on manage fee page
* If value is '0' its Disabled
* If value is '1' its Enabled
*******************
commented in result of merginig branch 1.46 to trunk
define("CONFIG_DIST_MANAGE_FEE","1");
*/
/********************
* Added by Khola #3617
* dist list and collection point list agaisnt collection point fee as search filters on manage fee page
* If value is '0' its Disabled
* If value is '1' its Enabled
*******************
commented in result of merginig branch 1.46 to trunk
define("CONFIG_COLLPOINT_MANAGE_FEE","1");
*/

/**  
 * Adding the "Account Manager" User, which will use the basic user functionality  
 * The basic user will have report view access along with whatever they have the rights. 
 * @Ticket #4123 
 */ 
define("CONFIG_LABEL_BASIC_USER","1"); 
define("CONFIG_BASIC_USER_NAME","Account Manager"); 

/**
 * Just enabling or disabling the functionality to update the agent legder in case of agent own rate 
 * 
 */
define("CONFIG_DISABLE_AGENT_OWN_RATE_LEDGER_ENTRY", "1");
/**
 * @Ticket #4634
 * Config displays Page Total and Runnig Total Balance on AnD account statement report.
 */
define("CONFIG_DISPLAY_TOTAL_RUNNING_AnD_STATEMENT","0");
/**
 * @Ticket #4541
 * Bank Based Transactions are viewd and also can be exported. Export Bank Based Transactions
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT","1");

/**
 * To export the A&D Account Statement,
 * Mark this onfig as "1"
 * By this a export button will be displayed at the bottom of the A&D Report, which export all the Transactions
 * @Ticket #4636
 */
define("CONFIG_EXPORT_AnD_ACCOUNT_STATEMENT", "1");

/**
 * To make "Exchange Rate Margin is More than Agent Commission." alert off,
 * following two configs are being added.
 * @Ticket #4557
 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE","1");
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","NIGERIA,POLAND,");

/**
 * With admin staff associate the super agents
 * @Ticket #4557
 */
define("CONFIG_ADMIN_ASSOCIATE_AGENT","1");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/* # 4635 - To show opening balances of same currencies. and swaping closing of previous date
 to opening of next date otherwise keeping same(ashahid)
 To On this config this config:CONFIG_ADD_CURRENCY_DROPDOWN should also be ON
 Enable  = 1
 Disable = 0
 */
define("CONFIG_SWAP_CLOSING_TO_OPENING","1");


/*	@Ticket #4616 Point # 3
	This Config enables disables assigned rights like (Create trannsaction) Also user-functionality
	
*/
define("CONFIG_ADD_USER_RIGHT","1");

/**
 * To use the separate Distributor Output File for each client
 * feed the filename into the below menioned config
 * @Ticket #4362
 */
define("CONFIG_CUSTOMIZED_OUTPUT_FILE","Distributor_Output_File_MT.php");

/** 
 * Manage Upload document Categories 
 * @Ticket #3594
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

?>