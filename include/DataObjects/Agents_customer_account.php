<?php
/**
 * Table Definition for agents_customer_account
 */
require_once 'DB/DataObject.php';

class DataObjects_Agents_customer_account extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'agents_customer_account';         // table name
    public $caID;                            // int(20)  not_null primary_key unsigned auto_increment
    public $customerID;                      // int(20)  not_null
    public $Date;                            // date(10)  not_null binary
    public $tranRefNo;                       // string(30)  not_null
    public $payment_mode;                    // string(50)  not_null
    public $Type;                            // string(10)  not_null
    public $amount;                          // real(12)  not_null
    public $modifiedBy;                      // string(35)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Agents_customer_account',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
