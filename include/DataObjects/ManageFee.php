<?php
/**
 * Table Definition for manageFee
 */
require_once 'DB/DataObject.php';

class DataObjects_ManageFee extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'manageFee';                       // table name
    public $manageFeeID;                     // int(20)  not_null primary_key unsigned auto_increment
    public $origCountry;                     // string(100)  not_null
    public $destCountry;                     // string(100)  not_null
    public $amountRangeFrom;                 // real(12)  not_null
    public $amountRangeTo;                   // real(12)  not_null
    public $regularFee;                      // real(12)  not_null
    public $feeType;                         // string(15)  not_null
    public $payinFee;                        // real(12)  not_null
    public $agentNo;                         // int(20)  not_null
    public $intervalUsed;                    // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_ManageFee',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
