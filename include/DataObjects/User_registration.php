<?php
/**
 * Table Definition for user_registration
 */
require_once 'DB/DataObject.php';

class DataObjects_User_registration extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'user_registration';               // table name
    public $id;                              // int(20)  not_null primary_key
    public $user_id;                         // string(30)  not_null
    public $password;                        // string(30)  not_null
    public $Company_name;                    // string(50)  not_null
    public $address;                         // string(200)  not_null
    public $country;                         // string(40)  not_null
    public $email;                           // string(40)  not_null
    public $post_code;                       // string(10)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_User_registration',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
