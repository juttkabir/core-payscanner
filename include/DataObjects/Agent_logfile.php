<?php
/**
 * Table Definition for agent_logfile
 */
require_once 'DB/DataObject.php';

class DataObjects_Agent_logfile extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'agent_logfile';                   // table name
    public $logID;                           // int(20)  not_null primary_key unsigned auto_increment
    public $customerID;                      // int(20)  not_null
    public $agentID;                         // int(20)  not_null
    public $transID;                         // int(20)  not_null
    public $benID;                           // int(20)  not_null
    public $compID;                          // int(200)  not_null
    public $entryDate;                       // date(10)  not_null binary
    public $remarks;                         // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Agent_logfile',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
