<?php
/**
 * Table Definition for cities
 */
require_once 'DB/DataObject.php';

class DataObjects_Cities extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cities';                          // table name
    public $country;                         // string(50)  not_null multiple_key
    public $city;                            // string(255)  not_null
    public $countryCode;                     // string(4)  not_null
    public $Currency;                        // string(3)  not_null
    public $currDesc;                        // string(32)  not_null
    public $deliveryTime;                    // string(100)  not_null
    public $currencyRec;                     // string(255)  not_null
    public $serviceAvailable;                // string(255)  not_null
    public $isoCode;                         // string(2)  not_null
    public $countryType;                     // string(15)  not_null
    public $bankCharges;                     // real(12)  not_null
    public $outCurrCharges;                  // real(12)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cities',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
