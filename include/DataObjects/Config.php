<?php
/**
 * Table Definition for config
 */
require_once 'DB/DataObject.php';

class DataObjects_Config extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'config';                          // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $category;                        // string(32)  
    public $name;                            // string(32)  
    public $value;                           // blob(-1)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Config',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    #
    # Return the index of the table
    #
    function keys() {
        return array('id');
    }
    
    #
    # Turn off generation of a separate key sequence since we have an auto-increment index
    #
    function sequenceKey() {
        return array(false,false);
    }

    #
    # Get an associative array of categories
    #
    # e.g.
    # $config_logo = &DataObjects_Config::category_array('logo');
    # echo "width = ". $config_logo['width'];
    #
    public static function category_array($category) {
        $ret_array = array();
        $obj = new DataObjects_Config;
        $obj->category = $category;
        $obj->find();
        while ($obj->fetch()) {
            $ret_array[$obj->name] = $obj->value;
        }
        return $ret_array;
    }
}
