<?php
/**
 * Table Definition for complaints
 */
require_once 'DB/DataObject.php';

class DataObjects_Complaints extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'complaints';                      // table name
    public $cmpID;                           // real(22)  not_null primary_key auto_increment
    public $transID;                         // real(22)  not_null
    public $agent;                           // string(32)  not_null
    public $subject;                         // string(255)  not_null
    public $details;                         // blob(65535)  not_null blob
    public $status;                          // string(15)  not_null
    public $dated;                           // datetime(19)  not_null binary
    public $logedUserID;                     // int(20)  not_null
    public $logedUserName;                   // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Complaints',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
