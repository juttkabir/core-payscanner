<?php
/**
 * Table Definition for complaintinteracts
 */
require_once 'DB/DataObject.php';

class DataObjects_Complaintinteracts extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'complaintinteracts';              // table name
    public $id;                              // real(22)  not_null primary_key auto_increment
    public $cmpID;                           // real(22)  not_null
    public $agent;                           // string(32)  not_null
    public $dated;                           // datetime(19)  not_null binary
    public $comments;                        // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Complaintinteracts',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
