<?php
/**
 * Table Definition for login_activities
 */
require_once 'DB/DataObject.php';

class DataObjects_Login_activities extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'login_activities';                // table name
    public $activity_id;                     // int(20)  not_null primary_key auto_increment
    public $login_history_id;                // int(20)  not_null
    public $activity;                        // string(254)  not_null
    public $activity_time;                   // datetime(19)  not_null binary
    public $action_for;                      // string(45)  not_null
    public $table_name;                      // string(60)  not_null
    public $description;                     // string(254)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Login_activities',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
