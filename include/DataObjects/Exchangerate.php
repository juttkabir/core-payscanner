<?php
/**
 * Table Definition for exchangerate
 */
require_once 'DB/DataObject.php';

class DataObjects_Exchangerate extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'exchangerate';                    // table name
    public $erID;                            // int(20)  not_null primary_key unsigned auto_increment
    public $country;                         // string(100)  not_null
    public $primaryExchange;                 // real(12)  not_null
    public $sProvider;                       // string(150)  not_null
    public $dated;                           // date(10)  not_null binary
    public $currency;                        // string(10)  not_null
    public $marginPercentage;                // int(11)  not_null
    public $rateFor;                         // string(40)  not_null
    public $rateValue;                       // string(75)  not_null
    public $updationDate;                    // datetime(19)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Exchangerate',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
