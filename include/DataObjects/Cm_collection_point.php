<?php
/**
 * Table Definition for cm_collection_point
 */
require_once 'DB/DataObject.php';

class DataObjects_Cm_collection_point extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cm_collection_point';             // table name
    public $cp_id;                           // int(20)  not_null primary_key auto_increment
    public $isAdmin;                         // string(10)  not_null
    public $cp_corresspondent_name;          // string(50)  
    public $cp_ria_branch_code;              // string(20)  
    public $cp_ida_id;                       // string(20)  
    public $cp_branch_name;                  // string(70)  
    public $cp_branch_no;                    // string(20)  
    public $cp_branch_address;               // string(255)  
    public $cp_city;                         // string(50)  
    public $cp_state;                        // string(50)  
    public $cp_country;                      // string(50)  
    public $cp_phone;                        // string(50)  
    public $cp_active;                       // string(10)  not_null
    public $disabledDate;                    // datetime(19)  not_null binary
    public $disabledBy;                      // string(100)  not_null
    public $disableReason;                   // string(256)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cm_collection_point',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
