<?php
/**
 * Table Definition for natwestpayments
 */
require_once 'DB/DataObject.php';

class DataObjects_Natwestpayments extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'natwestpayments';                 // table name
    public $id;                              // int(9)  not_null primary_key auto_increment
    public $importedOn;                      // datetime(19)  not_null binary
    public $paymentFrom;                     // string(8)  not_null enum
    public $username;                        // string(30)  not_null
    public $currency;                        // string(10)  not_null
    public $amount;                          // int(11)  not_null
    public $entryDate;                       // date(10)  not_null binary
    public $isResolved;                      // string(1)  not_null enum
    public $isProcessed;                     // string(1)  not_null enum
    public $unResReason;                     // string(255)  not_null
    public $description;                     // string(255)  not_null
    public $tlaCode;                         // string(5)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Natwestpayments',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
