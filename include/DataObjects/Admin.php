<?php
/**
 * Table Definition for admin
 */
require_once 'DB/DataObject.php';

class DataObjects_Admin extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'admin';                           // table name
    public $userID;                          // int(20)  not_null primary_key auto_increment
    public $username;                        // string(32)  not_null
    public $password;                        // string(16)  not_null
    public $changedPwd;                      // date(10)  not_null binary
    public $name;                            // string(25)  not_null
    public $last_login;                      // datetime(19)  not_null binary
    public $isMain;                          // string(1)  not_null enum
    public $email;                           // string(100)  not_null
    public $role;                            // blob(65535)  not_null blob
    public $rights;                          // string(255)  not_null
    public $adminType;                       // string(14)  not_null enum
    public $created;                         // datetime(19)  not_null binary
    public $parentID;                        // int(20)  not_null unsigned
    public $agentType;                       // string(6)  not_null multiple_key enum
    public $isCorrespondent;                 // string(4)  not_null enum
    public $IDAcountry;                      // string(255)  not_null
    public $agentNumber;                     // int(10)  not_null unsigned
    public $subagentNum;                     // int(10)  not_null unsigned
    public $agentCompany;                    // string(255)  not_null
    public $agentContactPerson;              // string(100)  not_null
    public $agentAddress;                    // string(255)  not_null
    public $agentAddress2;                   // string(255)  not_null
    public $agentCity;                       // string(100)  not_null
    public $agentZip;                        // string(16)  not_null
    public $agentCountry;                    // string(100)  not_null
    public $agentCountryCode;                // string(3)  not_null
    public $agentPhone;                      // string(32)  not_null
    public $mobile;                          // string(32)  not_null
    public $agentFax;                        // string(32)  not_null
    public $agentURL;                        // string(255)  not_null
    public $agentMSBNumber;                  // string(255)  not_null
    public $agentMCBExpiry;                  // datetime(19)  not_null binary
    public $agentCompRegNumber;              // string(100)  not_null
    public $agentCompDirector;               // string(100)  not_null
    public $designation;                     // string(255)  not_null
    public $agentDirectorAdd;                // string(255)  not_null
    public $agentProofID;                    // string(100)  not_null
    public $agentIDExpiry;                   // datetime(19)  not_null binary
    public $agentDocumentProvided;           // string(100)  not_null
    public $agentBank;                       // string(255)  not_null
    public $agentAccountName;                // string(255)  not_null
    public $agentAccounNumber;               // string(255)  not_null
    public $agentBranchCode;                 // string(255)  not_null
    public $agentAccountType;                // string(255)  not_null
    public $agentCurrency;                   // string(255)  not_null
    public $agentAccountLimit;               // real(12)  not_null
    public $limitUsed;                       // real(12)  not_null
    public $commPackage;                     // string(3)  not_null enum
    public $agentCommission;                 // real(12)  not_null
    public $agentStatus;                     // string(9)  not_null enum
    public $suspensionReason;                // blob(65535)  not_null blob
    public $suspendedBy;                     // string(32)  not_null
    public $activatedBy;                     // string(32)  not_null
    public $disableReason;                   // blob(65535)  not_null blob
    public $disabledBy;                      // string(32)  not_null
    public $logo;                            // string(30)  not_null
    public $accessFromIP;                    // string(255)  not_null
    public $swiftCode;                       // string(100)  not_null
    public $authorizedFor;                   // string(200)  not_null
    public $balance;                         // real(22)  not_null
    public $postCode;                        // string(10)  
    public $payinBook;                       // string(25)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Admin',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
