<?php
/**
 * Table Definition for faqs
 */
require_once 'DB/DataObject.php';

class DataObjects_Faqs extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'faqs';                            // table name
    public $faqID;                           // int(20)  not_null primary_key auto_increment
    public $question;                        // blob(65535)  not_null blob
    public $answer;                          // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Faqs',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
