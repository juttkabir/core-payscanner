<?php
/**
 * Table Definition for beneficiary
 */
require_once 'DB/DataObject.php';

class DataObjects_Beneficiary extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'beneficiary';                     // table name
    public $benID;                           // int(20)  not_null primary_key unsigned auto_increment
    public $customerID;                      // int(20)  not_null unsigned
    public $created;                         // date(10)  not_null binary
    public $Title;                           // string(10)  not_null
    public $firstName;                       // string(25)  not_null
    public $middleName;                      // string(25)  not_null
    public $lastName;                        // string(25)  not_null
    public $benAccount;                      // string(32)  not_null
    public $Password;                        // string(32)  not_null
    public $Address;                         // string(255)  not_null
    public $Address1;                        // string(255)  not_null
    public $City;                            // string(100)  not_null
    public $State;                           // string(50)  not_null
    public $Zip;                             // string(16)  not_null
    public $Country;                         // string(100)  not_null
    public $Phone;                           // string(32)  not_null
    public $Mobile;                          // string(32)  not_null
    public $email;                           // string(100)  not_null
    public $IDType;                          // string(100)  not_null
    public $IDNumber;                        // string(32)  not_null
    public $isTransfered;                    // string(1)  not_null enum
    public $agentID;                         // int(11)  not_null
    public $other_title;                     // string(30)  not_null
    public $otherId;                         // string(30)  not_null
    public $otherId_name;                    // string(30)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Beneficiary',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
