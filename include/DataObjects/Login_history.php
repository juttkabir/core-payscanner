<?php
/**
 * Table Definition for login_history
 */
require_once 'DB/DataObject.php';

class DataObjects_Login_history extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'login_history';                   // table name
    public $history_id;                      // int(20)  not_null primary_key auto_increment
    public $login_time;                      // datetime(19)  not_null binary
    public $login_name;                      // string(45)  not_null
    public $access_ip;                       // string(140)  not_null
    public $login_type;                      // string(60)  not_null
    public $user_id;                         // int(20)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Login_history',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
