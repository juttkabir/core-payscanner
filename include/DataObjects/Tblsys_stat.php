<?php
/**
 * Table Definition for tblsys_stat
 */
require_once 'DB/DataObject.php';

class DataObjects_Tblsys_stat extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblsys_stat';                     // table name
    public $BUSY;                            // int(10)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tblsys_stat',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
