<?php
/**
 * Table Definition for tbl_transaction_staus
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_transaction_staus extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_transaction_staus';           // table name
    public $TSTATUSID;                       // int(20)  not_null
    public $DESCRIPTION;                     // string(250)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_transaction_staus',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
