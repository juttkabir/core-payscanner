<?php
/**
 * Table Definition for cm_questions
 */
require_once 'DB/DataObject.php';

class DataObjects_Cm_questions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cm_questions';                    // table name
    public $qID;                             // int(20)  not_null primary_key auto_increment
    public $question;                        // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cm_questions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
