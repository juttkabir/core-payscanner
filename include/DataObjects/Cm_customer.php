<?php
/**
 * Table Definition for cm_customer
 */
require_once 'DB/DataObject.php';

class DataObjects_Cm_customer extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cm_customer';                     // table name
    public $c_id;                            // int(6)  not_null primary_key unsigned auto_increment
    public $c_name;                          // string(50)  
    public $Title;                           // string(10)  not_null
    public $FirstName;                       // string(20)  not_null
    public $MiddleName;                      // string(20)  not_null
    public $LastName;                        // string(20)  not_null
    public $c_pass;                          // string(50)  
    public $c_date;                          // date(10)  binary
    public $c_email;                         // string(30)  
    public $c_phone;                         // string(40)  
    public $c_mobile;                        // string(40)  
    public $c_address;                       // string(255)  
    public $c_address2;                      // string(255)  
    public $c_country;                       // string(30)  
    public $c_state;                         // string(30)  
    public $c_city;                          // string(30)  
    public $c_zip;                           // string(30)  
    public $c_p_question1;                   // int(6)  
    public $c_answer1;                       // string(50)  
    public $c_p_question2;                   // int(6)  
    public $c_answer2;                       // string(50)  
    public $c_info_source;                   // string(50)  
    public $c_benef_country;                 // string(30)  
    public $customerStatus;                  // string(20)  not_null
    public $accessFromIP;                    // string(50)  not_null
    public $username;                        // string(50)  not_null
    public $last_login;                      // date(10)  binary
    public $dateConfrimed;                   // date(10)  not_null binary
    public $SecuriyCode;                     // int(5)  not_null
    public $dateDisabled;                    // date(10)  not_null binary
    public $reason;                          // string(200)  not_null
    public $holdAccountID;                   // int(20)  not_null
    public $accountType;                     // string(20)  not_null
    public $dateAccepted;                    // date(10)  not_null binary
    public $dob;                             // date(10)  not_null binary
    public $limit1;                          // real(12)  not_null
    public $Balance;                         // real(12)  not_null
    public $limit3;                          // real(12)  not_null
    public $determind;                       // string(50)  not_null
    public $changedPwd;                      // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cm_customer',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
