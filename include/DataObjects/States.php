<?php
/**
 * Table Definition for states
 */
require_once 'DB/DataObject.php';

class DataObjects_States extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'states';                          // table name
    public $country;                         // string(50)  not_null multiple_key
    public $state;                           // string(30)  not_null
    public $code;                            // string(10)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_States',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
