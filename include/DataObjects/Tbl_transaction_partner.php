<?php
/**
 * Table Definition for tbl_transaction_partner
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_transaction_partner extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_transaction_partner';         // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $TRANSID;                         // int(20)  not_null
    public $TDISPLAYID;                      // string(50)  not_null
    public $TTYPE;                           // int(10)  not_null
    public $TRANS_DATE;                      // date(10)  not_null binary
    public $SEND_PTID;                       // string(20)  not_null
    public $TRANS_RECV_DATE;                 // date(10)  not_null binary
    public $AUTH_CODE;                       // string(100)  not_null
    public $PASSWORD;                        // string(100)  not_null
    public $POUT_CURRENCYID;                 // string(20)  not_null
    public $EXRATE;                          // real(12)  not_null
    public $PAYOUT_AMT;                      // real(12)  not_null
    public $STL_CURRENCYID;                  // string(50)  not_null
    public $STL_AMT;                         // real(12)  not_null
    public $STL_CHARGE;                      // real(12)  not_null
    public $SCOUNTRY;                        // string(50)  not_null
    public $SEND_POINT;                      // string(50)  not_null
    public $RCOUNTRY;                        // string(50)  not_null
    public $RCITY;                           // string(50)  not_null
    public $PICKUP_POINT;                    // string(50)  not_null
    public $PICKUP_PT_DETAILS;               // string(50)  not_null
    public $SENDER_FNAME;                    // string(50)  not_null
    public $SENDER_LNAME;                    // string(50)  not_null
    public $RECV_FNAME;                      // string(50)  not_null
    public $RECV_LNAME;                      // string(50)  not_null
    public $RECV_ID;                         // string(100)  not_null
    public $RECV_ADDRESS1;                   // string(200)  not_null
    public $RECV_ADDRESS2;                   // string(200)  not_null
    public $RECV_PHONE;                      // string(50)  not_null
    public $RECV_BANKINFO1;                  // string(50)  not_null
    public $RECV_BANKINFO2;                  // string(50)  not_null
    public $RECV_BANKINFO3;                  // string(50)  not_null
    public $RECV_BANKINFO4;                  // string(50)  not_null
    public $RECV_BANKINFO5;                  // string(50)  not_null
    public $TSTATUSID;                       // int(10)  not_null
    public $PARTNER_READ;                    // int(10)  not_null
    public $TC_READ;                         // int(10)  not_null
    public $Error_Code;                      // int(10)  not_null
    public $Remarks;                         // string(250)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_transaction_partner',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
