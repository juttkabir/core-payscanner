<?php
/**
 * Table Definition for customer
 */
require_once 'DB/DataObject.php';

class DataObjects_Customer extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'customer';                        // table name
    public $customerID;                      // int(20)  not_null primary_key unsigned auto_increment
    public $agentID;                         // int(20)  not_null unsigned
    public $created;                         // date(10)  not_null binary
    public $Title;                           // string(10)  not_null
    public $firstName;                       // string(25)  not_null
    public $middleName;                      // string(25)  not_null
    public $lastName;                        // string(25)  not_null
    public $accountName;                     // string(32)  not_null
    public $password;                        // string(32)  not_null
    public $customerNumber;                  // int(10)  not_null unsigned
    public $Address;                         // string(255)  not_null
    public $Address1;                        // string(255)  not_null
    public $City;                            // string(100)  not_null
    public $State;                           // string(50)  not_null
    public $Zip;                             // string(16)  not_null
    public $Country;                         // string(100)  not_null
    public $Phone;                           // string(32)  not_null
    public $Mobile;                          // string(32)  not_null
    public $email;                           // string(100)  not_null
    public $dob;                             // date(10)  not_null binary
    public $acceptedTerms;                   // string(1)  not_null enum
    public $IDType;                          // string(100)  not_null
    public $IDNumber;                        // string(32)  not_null
    public $IDExpiry;                        // datetime(19)  not_null binary
    public $issuedBy;                        // string(255)  not_null
    public $documentProvided;                // string(100)  not_null
    public $fundsSources;                    // string(100)  not_null
    public $transactionPurpose;              // string(100)  not_null
    public $other_title;                     // string(30)  not_null
    public $otherId;                         // string(30)  not_null
    public $otherId_name;                    // string(30)  not_null
    public $payinBook;                       // string(35)  
    public $balance;                         // real(12)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Customer',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
