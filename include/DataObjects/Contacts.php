<?php
/**
 * Table Definition for contacts
 */
require_once 'DB/DataObject.php';

class DataObjects_Contacts extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'contacts';                        // table name
    public $contactID;                       // int(20)  not_null primary_key auto_increment
    public $officeName;                      // string(255)  not_null
    public $address;                         // blob(65535)  not_null blob
    public $phone;                           // string(255)  not_null
    public $fax;                             // string(255)  not_null
    public $email;                           // string(255)  not_null
    public $image;                           // string(32)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contacts',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
