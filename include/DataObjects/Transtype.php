<?php
/**
 * Table Definition for transtype
 */
require_once 'DB/DataObject.php';

class DataObjects_Transtype extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'transtype';                       // table name
    public $faqID;                           // int(20)  not_null primary_key auto_increment
    public $question;                        // blob(65535)  not_null blob
    public $answer;                          // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Transtype',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
