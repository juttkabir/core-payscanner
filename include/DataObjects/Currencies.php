<?php
/**
 * Table Definition for currencies
 */
require_once 'DB/DataObject.php';

class DataObjects_Currencies extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'currencies';                      // table name
    public $cID;                             // int(20)  not_null primary_key auto_increment
    public $country;                         // string(45)  not_null
    public $currencyName;                    // string(200)  not_null
    public $numCode;                         // int(11)  not_null
    public $description;                     // string(60)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Currencies',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
