<?php
/**
 * Table Definition for imbanks
 */
require_once 'DB/DataObject.php';

class DataObjects_Imbanks extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'imbanks';                         // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $bankName;                        // string(150)  not_null
    public $country;                         // string(100)  not_null
    public $bank_address;                    // string(255)  not_null
    public $dd_code;                         // string(20)  not_null
    public $dd_locations;                    // string(50)  not_null
    public $bankCode;                        // string(15)  not_null
    public $branchName;                      // string(50)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Imbanks',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
