<?php
/**
 * Table Definition for cm_credit_trans
 */
require_once 'DB/DataObject.php';

class DataObjects_Cm_credit_trans extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cm_credit_trans';                 // table name
    public $cID;                             // int(20)  not_null primary_key multiple_key auto_increment
    public $transID;                         // string(20)  not_null
    public $cardType;                        // string(15)  not_null
    public $cardNo;                          // string(16)  not_null
    public $expiryDate;                      // string(6)  
    public $Card_CVV;                        // string(4)  
    public $TransactionTime;                 // datetime(19)  not_null binary
    public $AttemptCode;                     // string(4)  
    public $PQTransID;                       // string(17)  
    public $ApprovalCode;                    // string(6)  
    public $ResultCode;                      // string(4)  
    public $ResultText;                      // string(100)  
    public $IPCountryCode;                   // string(3)  
    public $SenderOFAC;                      // string(25)  
    public $SenderOFACRecord;                // string(255)  
    public $BenOFAC;                         // string(25)  
    public $BenOFACRecord;                   // string(255)  
    public $First_Name;                      // string(25)  not_null
    public $Last_Name;                       // string(25)  not_null
    public $Address_1;                       // string(30)  not_null
    public $Address_2;                       // string(30)  not_null
    public $City;                            // string(25)  not_null
    public $State;                           // string(2)  not_null
    public $Postal_Code;                     // string(9)  not_null
    public $Country_Code;                    // string(3)  not_null
    public $IP_Address;                      // string(15)  not_null
    public $Telephone;                       // string(15)  not_null
    public $Email;                           // string(60)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cm_credit_trans',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
