<?php
/**
 * Table Definition for customer_docs
 */
require_once 'DB/DataObject.php';

class DataObjects_Customer_docs extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'customer_docs';                   // table name
    public $ID;                              // int(20)  not_null primary_key auto_increment
    public $docName;                         // string(100)  not_null
    public $details;                         // string(200)  not_null
    public $filepath;                        // string(100)  not_null
    public $customerID;                      // int(20)  not_null
    public $docDate;                         // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Customer_docs',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
