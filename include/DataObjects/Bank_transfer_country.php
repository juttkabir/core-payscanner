<?php
/**
 * Table Definition for bank-transfer-country
 */
require_once 'DB/DataObject.php';

class DataObjects_Bank_transfer_country extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'bank-transfer-country';           // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $country;                         // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Bank_transfer_country',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
