<?php
/**
 * Table Definition for bankDetails
 */
require_once 'DB/DataObject.php';

class DataObjects_BankDetails extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'bankDetails';                     // table name
    public $bankID;                          // int(11)  not_null primary_key auto_increment
    public $benID;                           // int(11)  not_null
    public $transID;                         // int(11)  not_null
    public $bankName;                        // string(150)  not_null
    public $accNo;                           // string(100)  not_null
    public $branchCode;                      // string(100)  not_null
    public $branchAddress;                   // string(255)  not_null
    public $ABACPF;                          // string(100)  not_null
    public $IBAN;                            // string(100)  not_null
    public $swiftCode;                       // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_BankDetails',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
