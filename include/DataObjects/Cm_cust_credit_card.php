<?php
/**
 * Table Definition for cm_cust_credit_card
 */
require_once 'DB/DataObject.php';

class DataObjects_Cm_cust_credit_card extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cm_cust_credit_card';             // table name
    public $cID;                             // int(20)  not_null primary_key multiple_key auto_increment
    public $customerID;                      // int(20)  not_null
    public $cardNo;                          // string(20)  not_null
    public $cardName;                        // string(40)  not_null
    public $cardCVV;                         // string(4)  not_null
    public $expiryDate;                      // string(10)  not_null
    public $firstName;                       // string(20)  not_null
    public $lastName;                        // string(20)  not_null
    public $address;                         // string(255)  
    public $address2;                        // string(255)  
    public $city;                            // string(40)  
    public $state;                           // string(40)  
    public $country;                         // string(40)  
    public $postalCode;                      // string(20)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cm_cust_credit_card',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
