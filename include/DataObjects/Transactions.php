<?php
/**
 * Table Definition for transactions
 */
require_once 'DB/DataObject.php';

class DataObjects_Transactions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'transactions';                    // table name
    public $transID;                         // int(20)  not_null primary_key unsigned auto_increment
    public $transType;                       // string(13)  not_null enum
    public $customerID;                      // int(20)  not_null unsigned
    public $benID;                           // int(20)  not_null unsigned
    public $custAgentID;                     // int(20)  not_null unsigned
    public $benAgentID;                      // int(20)  not_null
    public $exchangeID;                      // int(20)  not_null unsigned
    public $refNumber;                       // string(32)  not_null
    public $refNumberIM;                     // string(50)  not_null
    public $transAmount;                     // real(12)  not_null
    public $exchangeRate;                    // real(12)  not_null
    public $localAmount;                     // real(22)  not_null
    public $IMFee;                           // real(12)  not_null
    public $bankCharges;                     // real(12)  not_null
    public $totalAmount;                     // real(12)  not_null
    public $transactionPurpose;              // string(100)  not_null
    public $fundSources;                     // string(100)  not_null
    public $moneyPaid;                       // string(100)  not_null
    public $Declaration;                     // string(100)  not_null
    public $transStatus;                     // string(20)  not_null
    public $addedBy;                         // string(32)  not_null
    public $transDate;                       // datetime(19)  not_null binary
    public $verifiedBy;                      // string(30)  not_null
    public $authorisedBy;                    // string(30)  not_null
    public $cancelledBy;                     // string(30)  not_null
    public $recalledBy;                      // string(20)  not_null
    public $isSent;                          // string(1)  not_null enum
    public $trackingNum;                     // string(30)  not_null
    public $trans4Country;                   // string(30)  not_null
    public $remarks;                         // string(255)  not_null
    public $authoriseDate;                   // datetime(19)  not_null binary
    public $deliveryOutDate;                 // datetime(19)  not_null binary
    public $deliveryDate;                    // datetime(19)  not_null binary
    public $cancelDate;                      // datetime(19)  not_null binary
    public $rejectDate;                      // datetime(19)  not_null binary
    public $failedDate;                      // datetime(19)  not_null binary
    public $suspeciousDate;                  // datetime(19)  not_null binary
    public $recalledDate;                    // datetime(19)  not_null binary
    public $fromCountry;                     // string(30)  not_null
    public $toCountry;                       // string(30)  not_null
    public $currencyFrom;                    // string(10)  not_null
    public $currencyTo;                      // string(10)  not_null
    public $custAgentParentID;               // int(11)  not_null
    public $benAgentParentID;                // int(11)  not_null
    public $is_exported;                     // string(10)  not_null
    public $PINCODE;                         // int(5)  not_null
    public $createdBy;                       // string(10)  not_null
    public $collectionPointID;               // int(20)  not_null
    public $transRefID;                      // string(30)  not_null
    public $AgentComm;                       // real(12)  not_null
    public $CommType;                        // string(30)  not_null
    public $other_pur;                       // string(35)  not_null
    public $admincharges;                    // string(30)  not_null
    public $cashCharges;                     // int(11)  not_null
    public $refundFee;                       // string(10)  not_null
    public $question;                        // string(100)  not_null
    public $answer;                          // string(200)  not_null
    public $tip;                             // blob(65535)  blob
    public $holdedBy;                        // string(20)  not_null
    public $outCurrCharges;                  // real(12)  not_null
    public $distributorComm;                 // real(12)  not_null
    public $discountRequest;                 // string(255)  not_null
    public $discountType;                    // string(255)  not_null
    public $discounted_amount;               // real(12)  not_null
    public $creation_date_used;              // date(10)  not_null binary
    public $holdDate;                        // datetime(19)  not_null binary
    public $verificationDate;                // datetime(19)  not_null binary
    public $unholdBy;                        // string(40)  not_null
    public $unholdDate;                      // datetime(19)  not_null binary
    public $deliveredBy;                     // string(45)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Transactions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
