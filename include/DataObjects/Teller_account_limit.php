<?php
/**
 * Table Definition for teller_account_limit
 */
require_once 'DB/DataObject.php';

class DataObjects_Teller_account_limit extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'teller_account_limit';            // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $teller_id;                       // int(11)  not_null
    public $limit_date;                      // date(10)  not_null binary
    public $account_limit;                   // string(30)  not_null
    public $used_limit;                      // string(30)  not_null
    public $limit_alert;                     // string(30)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Teller_account_limit',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
