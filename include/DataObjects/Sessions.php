<?php
/**
 * Table Definition for sessions
 */
require_once 'DB/DataObject.php';

class DataObjects_Sessions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'sessions';                        // table name
    public $username;                        // string(15)  not_null
    public $session_id;                      // string(32)  not_null primary_key
    public $session_time;                    // datetime(19)  not_null binary
    public $ip;                              // string(20)  not_null
    public $cookie_remember;                 // string(1)  not_null enum
    public $is_online;                       // string(1)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Sessions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
