<?php
/**
 * Table Definition for tbl_error_code
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_error_code extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_error_code';                  // table name
    public $ERROR_CODE;                      // int(20)  not_null
    public $ERROR_NAME;                      // string(250)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_error_code',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
