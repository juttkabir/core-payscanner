<?php
/**
 * Table Definition for teller
 */
require_once 'DB/DataObject.php';

class DataObjects_Teller extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'teller';                          // table name
    public $tellerID;                        // int(20)  not_null primary_key auto_increment
    public $collection_point;                // int(20)  not_null
    public $loginName;                       // string(50)  not_null
    public $password;                        // string(30)  not_null
    public $name;                            // string(50)  not_null
    public $is_active;                       // string(10)  not_null
    public $isMain;                          // string(5)  not_null
    public $changedPwd;                      // datetime(19)  not_null binary
    public $account_limit;                   // string(30)  not_null
    public $limit_alert;                     // string(30)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Teller',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
