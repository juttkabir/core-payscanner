<?php
/**
 * Table Definition for agent_account
 */
require_once 'DB/DataObject.php';

class DataObjects_Agent_account extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'agent_account';                   // table name
    public $aaID;                            // int(20)  not_null primary_key auto_increment
    public $agentID;                         // int(20)  not_null
    public $dated;                           // date(10)  not_null binary
    public $type;                            // string(15)  not_null
    public $amount;                          // real(22)  not_null
    public $modified_by;                     // int(20)  not_null
    public $TransID;                         // string(20)  not_null
    public $description;                     // string(60)  not_null
    public $status;                          // string(30)  not_null
    public $note;                            // string(256)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Agent_account',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
