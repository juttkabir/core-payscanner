<?php

// Load the config from the database
include "../load_config.php";

define("SYSTEM","abl");

define("CONFIG_LOGO","../admin/images/abl/logo.gif");
define("CONFIG_LOGO_WIDTH","194");
define("CONFIG_LOGO_HEIGHT","48");

define("DISPLAY_EDIT_TRANSACTIONS","0");
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_CUST_AGENT_ENABLED","0");
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_SECRET_QUEST_ENABLED","0");


define("PIN_CODE", "0");
define("CONFIG_FEE_ON_REFUND", "5");


define("CONFIG_INVOICE_FOOTER", "+44(0)00 000 000");
define("COMPANY_URL","www.abl.com.pk");
define("USER_ACCOUNT_URL","http://abl.test.horivert.com/user");
define("ADMIN_ACCOUNT_URL","http://abl.test..horivert.com/admin");

define("SYSTEM_PRE","ABL");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","ABL Code");
define("COMPANY_NAME","Allied Bank Of Pakistan");


define("CONFIG_MAX_TRANSACTIONS","50");

define("SUPPORT_EMAIL", "noreply@horivert.co.uk");
define("INQUIRY_EMAIL", "inquiries@horivert.co.uk");
define("CC_EMAIL", "payex@horivert.co.uk");
define("SUPPORT_NAME", "Company Support");


define("TITLE","::.ABL.:: The Fastest Money Transfer....");
define("TITLE_ADMIN", "::...ABL Agent Application...::");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","Province");

/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


?>
