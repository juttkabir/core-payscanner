<?php
// Load the config from the database
//include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","AMB Exchange");


/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/AMB/AMB_logo.JPG");
define("CONFIG_LOGO_WIDTH","100");
define("CONFIG_LOGO_HEIGHT","90");
define("CONFIG_OTHER_LOGO","../admin/images/Horivert.PNG");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");
define("UPDATE_TRANSACTIONS_AGENT","1");

/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","1");




/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","1");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Config is being turned off by Omair Anwer under ticket # 5539
********************/
//define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
/*define("CONFIG_TRANS_COND","I accept that I have not used any illegal means to earn the money being sent. I accept that I am not sending this money for any illegal act. I also accept the full Terms & Conditions of AMB Exchange");*/


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");


/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");


/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","0");




/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");


/**
 * introducing the new const for handling the commission based on payment mode
 * If enabled then the commission will be based on the payment mode
 * @Ticket# 3319
 * @Author Jahangir <jahangir.alam@horivert.co.uk>
 */
define("CONFIG_ADD_PAYMENT_MODE_BASED_COMM","0");


/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","0");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","AMB");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Western Union MTCN Number");
define("COMPANY_NAME","AMB Exchange");
define("COMPANY_NAME_EXTENSION","Money Transfer");

define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "info@horivert.com");
define("INQUIRY_EMAIL", "info@horivert.com");
define("CC_EMAIL", "");
define("CONFIG_CC_EMAIL", "0");



define("SUPPORT_NAME", "Payex Support");


define("CONFIG_INVOICE_FOOTER", "+442032864226");
define("COMPANY_URL","horivert.co.uk");



//define("PAYEX_URL","83.138.154.207");
define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex");
define("TITLE_ADMIN", "Payex");


define("COMPANY_ADDRESS","London");
define("COMPANY_PHONE", "Tel:+442032864226");
/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","0");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");
define("CONFIG_AGENT_LIMIT_ON_CREATE_TRANSACTION","1");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","1");
define("CONFIG_RECIEPT_NAME","confirm-transaction.php");
define("CONFIG_RECIEPT_PRINT","print-confirmed-transaction-ambExchange.php");
/*
* #5292 - AMB Exchange
* If transaction type is Bank Transfer then this Receipt will be displayed 
* only if CONFIG_CUSTOM_RECIEPT in ON.
* Enable  = Receipt File Name like "abc.php"
* Disable = "0"
* by Aslam Shahid
*/
define("CONFIG_RECIEPT_PRINT_BANK","print-confirmed-transaction-ambExchange-Bank.php");
//Receipt Form for Beneficiary End
define("CONFIG_RECEIPT_FORM", "reciept_form.php");




/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");


/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","1");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* It is to enable either post paid for the distributor 
* or prepaid distributor for his transactions to be released.
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_POST_PAID","1");


/********************
* This switch is to enable the address on POS reciept
* 1 for head office info
* 0 for agent of the transaction
********************/
define("CONFIG_POS_ADDRESS","0");


/********************
* This switch is to enable/disable Agent commission in Ledger
* 1 Agent Commission is excluded
* 0 Agent Commission is included
* Its value was one...i changed to make it logically equal to glink for debtor report
**inorder to turn this config on CONFIG_AGENT_PAYMENT_MODE config should be disabled
********************/
define("CONFIG_EXCLUDE_COMMISSION","0");


/********************
* This switch is to enable/disable The Release Options for Tellers
* 0 Teller Can only Pickup or Credit
* 1 Teller can Fail or Reject as well
********************/
define("CONFIG_RELEASE_TRANS","0");


/********************
* Transaction Reference Number patteren with 
* 0-  COMPANY_PRE-COUNT
* 1-  AGENT-COUNT
********************/
define("CONFIG_TRANS_REF","0");
define("CUSTOM_AGENT_TRANS_REF","MS,MSB,MSC,MSD,MST");


/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* Daily Sales Report
* 0-  Destination Currency
* 1-  Originating Currency
* Its value was one...i changed to make it logically equal to glink for debtor report
********************/
define("CONFIG_SALES_REPORT","1");


/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");


/********************
* Switch to Enale/Disable Daily Commission Report, It is done especialy for Express 
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DIALY_COMMISSION","1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* To Name to Basic User as per Client Requirement, 
* As Express want it to be ADMIN OFFICER
* 0-  Basic User
* 1-  Client Defined
********************/
define("CONFIG_LABEL_BASIC_USER","0");
define("CONFIG_BASIC_USER_NAME","Admin Officer");

/********************
* Switch to Enale/Disable Report Suspicious Transaction It is being done as currently It is not fully functional
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_SUSPICIOUS","0");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LIMIT_TRANS_AMOUNT","1");



/********************
* Switch to Enale/Disable Agent's Receipt Book Ranges
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_RECEIPT_RANGE", "1");

/********************
* Switch to Enale/Disable appending manual code with agent's receipt range
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_CODE_WITH_RANGE", "1");

/********************
* Switch to Enale/Disable Back Dating of Payments (agent payments)
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BACKDATING_PAYMENTS", "0");

/********************
* Switch to Enale/Disable Payin Book Customers from list below (CONFIG_PAYIN_RESTRICT_LIST)

* CONFIG_PAYIN_RESTRICT_LIST should be comma separated
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_PAYIN_RESTRICT", "1");
define("CONFIG_PAYIN_RESTRICT_LIST", "SUPA,SUPAI");


/********************
* Using Cancelled Transactions in Dailly commission report
* specifically done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_COMM","1");



/********************
* Usage of reverse entry of the cancelled transactions 
* after it is cancelled in dailly commission for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM_COMM","0");



/********************
* Excluding Cancelled Transactions in Dailly commission report
* specifically done for express
* was 1...i changed so to make logically equal to GLINK
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXCLUDE_CANCEL_DAILY_COMM","1");

/********************
* Export of Daily Cashier Report
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_CASHIER","1");

/********************
* adding a dropdown of unclaimed transactions in daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_UNCLAIMED_DAILY_MANAGE","1");


/********************
* Export of daily manage transactions
* Especially done for express
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_EXPORT_DAILY_MANAGE","1");


/********************
* To disable the edit option of Sender and Beneficiaries
* If value is 1 then disable else enable
********************/
define("CONFIG_DISABLE_EDIT_CUSTBEN", "0");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "SUPAI,");

/********************
* To make some compulsory fields for agent logins in add/update customer/beneficiary
* during create transaction
* Postcode, Address Line 1
********************/
define("CONFIG_COMPUL_FOR_AGENTLOGINS", "1");


///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");


/********************
* Account Summary Report  Came From GLINK for Debtor Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");


/********************
* Using Daily Sales Report as Agent Balance Report
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","0");


/********************
* Using Opening and Closing Balance in Agent Account Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


/********************
* Using Cumulative Balances in Agent Statement
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");


/********************
* Labels for the deposit and withdraw
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Money In");
define("CONFIG_WITHDRAW","Money Out");


/********************
* Using Cancelled Transactions in daily transaction
* Came from GLINK for Debtor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");



/********************
* Reports to Disable for this client
* 
* 
********************/
//define("CONFIG_DISABLE_REPORT","RATE_EARNING");

/********************
* Mobile phone seperator
********************/
define("CONFIG_MOBILE_FORMAT","");


/********************
* Mobile Number compulsion for express &glink
enable 1
disable 0
********************/
define("CONFIG_COMPULSORY_MOBILE","0");

/********************
* To Enable fields in sender/beneficiary report
enable 1
disable 0
********************/
define("CONFIG_SEARCH_FIELDS","1");


/********************
* Pick up center column displayed for express for authorized transaction and agent/distributor commission report 
enable 1
disable 0
********************/
define("CONFIG_PICKUP_INFO","1");

/********************
* To Enable duplicated data in customer table for express
enable 1
disable 0
********************/
define("CONFIG_SEARCH_EXISTING_DATA","1");


/********************                                                           
* to Enable/disable Sameday option for Express Receipt
*       0 Disable                                                               
*       1 Enable  
********************/  
define("CONFIG_isSAMEDAY_OPTION","1"); 




/********************
* To disable the button on click once on create transaction page. To avoid duplication of transaction.
enable 1
disable 0
********************/
define("CONFIG_DISABLE_BUTTON","1");




/******************** 
* Enable or disbale Editing option for amount and local amount 
* 1-  Disable 
* 0-  Enable 
********************/ 
define("CONFIG_EDIT_VALUES","1"); 

/******************** 
* Enable or disbale Advertisement option for express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AVERT_CAMPAIGN","1"); 



/********************
* Switch to Enable/Disable Exchange Rate Margin Type 

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_MARGIN_TYPE","1");



/********************
* Switch to Enable/Disable time filter on view transactions

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TIME_FILTER","1");




/********************
* Switch to Enable/Disable activate option on update sender

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ACTIVATE_OPTION","0");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "amb");


/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");



/********************                                                                         
* Switch to Enable/Disable search filters on sender/beneficiary report 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_REFNUM_GENERATOR", "1");


/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");




/********************                                                                         
* Switch to Enable/Disable Manual Enable of Online Users 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/ 
define("CONFIG_MANUAL_ENABLE_ONLINE", "0");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");



/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "700");


/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "700");

/********************
* Switch to Enable/Disable to show the beneficiary verification id in distributor output file
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_VERIFICATION_ID", "1");



/**
 * Adding config to handle the bank details of the beneficiary
 * @Ticket# 3320
 * @Date 25th June, 08.
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 */
//define("CONFIG_BEN_BANK_DETAILS","1");


/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
//define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer, By Cash,By Cheque,");
define("CONFIG_TRANS_ROUND_CURRENCY", "BRL,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_FROM,");
define("CONFIG_TRANS_ROUND_LEVEL", "2");


/********************
* Switch to Enable/Disable New Currency Column for GHS
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_GHS_DAILY_CASHIER", "1");

/********************
* Switch to Enable/Disable Manual Agent Login Name Entry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_AGENT_NUMBER", "1") ;

/********************
* Switch to Enable/Disable default Transaction Purpose
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");

/********************
* Switch to Enable/Disable Making Fund Sources non compulsory
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NONCOMPUL_FUNDSOURCES", "0");

/******************** 
* Using Status and Modified By fields in Agent Statement 
* Fields are used by Express 
* 0-  Disable 
* 1-  Enable 
********************/ 
define("CONFIG_AGENT_STATEMENT_FIELDS","1"); 


/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");
 	 	
	 	
/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 	 	



/********************
* Switch to Enable/Disable shared network API as inter-payex transactions
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHARE_OTHER_NETWORK", "0"); 	 	
define("CONFIG_SHARE_FROM_TOTAL", "0"); 	
define("CONFIG_CONTROL_SENDING_OWN_SHARE", "0"); 

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	 	


/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php"); 	

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");


/********************
* Switch to Enable/Disable Defalut Receiving Currency
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DEFAULT_CURRENCY_BEN", "1");
define("CONFIG_CURRENCY_NAME_BEN", "GHS");



/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");

/********************
* Switch to Enable/Disable Calcualtion of balance using Transactions
* Temporarily done for express to solve urgent issue.
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_BALANCE_FROM_TRANSACTION","0");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");




/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");


/********************
* Switch to Enable/Disable backdated fees in edit/amend trans.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BACKDATED_FEE","0");




/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");



/********************
* Switch to Show/Hide Add Quick sender link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_SENDER_LINK", "0");

define("CONFIG_PAYIN_AGENT","1");
define("CONFIG_AGENT_PAYIN_NUMBER","6");


/********************
* Switch to Show/Hide Add Quick beneficiary link on add transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_REMOVE_QUICK_BENEFICIARY_LINK", "0");

//define("CONFIG_COMPLIANCE_MAX_AMOUNT", "10000");
//define("CONFIG_NO_OF_DAYS", "30");
define("CONFIG_CONFRIM_MESSAGE_FOR_EXPRESS", "Reminder - Transfers of greater than £700 require that you have an ID on file  if you do not please take an appropriate ID and keep a copy on file (appropriate ID requires a photo  e.g. photo page of passport, full driving licence, national ID card)");



/********************
* Switch to add collection point on updating distributor
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UPDATE_COLLECTION_POINT_ON_ADD_DIST", "1");





/********************
* Switch to Enable/Disable Ledgers at creation for remote Agents
* and Switch to Enable/Disable Remote distributors as Post Paid
* It will work only if CONFIG_SHARE_OTHER_NETWORK is Enabled
* 
* If value is '0' its Disabled
* If value is '1' its Enabled
*
*Here comes the dependency for the Ledger at creation at Local(Originating system) with REMOTE_LEDGER_AT CREATION
* If REMOTE_LEDGER_AT_CREATION is 1, ledger at creation(CONFIG_LEDGER_AT_CREATION) must be 1 and vice versa in not must 
*
*on the other hand if CONFIG_POST_PAID is 1 on originating end, CONFIG_REMOTE_DISTRIBUTOR_POST_PAID, must also be 1,  
*and vice versa is not must
*
********************/
define("CONFIG_REMOTE_LEDGER_AT_CREATE","0");

define("CONFIG_REMOTE_DISTRIBUTOR_POST_PAID","1");

define("CONFIG_DISABLE_BACK_DATED_TRANS_FOR_REMOTE_DIST","1");


/********************
* Switch to Enable/Disable removing the totals field from agent commision report
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TOTALS_FIELD_OFF","0");

define("CONFIG_FULLNAME_SEARCH_FILTER","0");


/********************
* Switch to Enable/Disable back dated ledgers as per transaction date selected 
* while creating a transaction.
* If value is '0' current Date in Ledgers
* If value is '1' selected date is sent to ledgers
********************/
define("CONFIG_BACK_LEDGER_DATES","0");
/********************
* Added by Niaz Ahmad against ticket # 2533 at 09-10-2007
* To show boxed for transaction Reference Code
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_REFERENCE_CODE_BOX", "1");
/********************
* Added by Niaz Ahmad against ticket # 2531 at 09-10-2007
* Add Collection Points in view transaction page
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SHOW_COLLECTIONPOINTS", "1");
/********************
* Added by Niaz Ahmad against ticket # 2532 at 12-10-2007
* Add Sender Reference # in create transaction page under Sender Details Section
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISPLAY_SENDER_REFERENCE_NUMBER", "1");
/********************
* Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
* FORMAT FOR AGNET COMMISSION REPORT EXPORT
*       0 Disable
*       1 Enable
********************/
define("CONFIG_FORMAT_AGENT_COMMISSION_REPORT_EXPORT", "1");

define("CONFIG_SHOW_MANUAL_CODE", "1");
define("CONFIG_ID_DETAILS_AML_REPORT", "1");

/********************
* Added by Javeria against ticket # 2663 at 19-11-2007
* check commission status from transaction's table on cancelation
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMMISSION_STATUS_ON_CANCELATION", "1");


/********************
* Added by Javeria against ticket # 2663 at 23-11-2007
* United kingdom is selected by default on add sender on express
*       0 Disable
*       1 Enable
********************/
define("CONFIG_UK_DEFAULT_ON_ADD_SENDER", "1");

/********************
* Added by Javeria against ticket # 2861 at 21-01-2008
* Distributor to be linked to distributor management
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT", "0");


/**
 * Adding a const to handle the appearance  of distributor at the add transaction case
 * Provideed if the the transfer type is bank selected
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 * @Ticket# 3321 / 3320
 */
define("CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS","0");


/********************
* Switch to Enable/Disable Suspend Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SUSPEND_TRANSACTION_ENABLED","0");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");



/********************
* Agent Commission Report to add Commission Value in total
* for agents with End of Month Commission payemt mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_MONTH_END_COMMISSION", "1");


/********************
*	#5001
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");

/********************
* on agent's login generic rates are to be displayed
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_GENERIC_RATE_FOR_AGENT","1");


/********************
* Exclulde payin bok customers from generic sender search
* 0-  Disable
* 1-  Enable
********************/

define("CONFIG_REMOVE_PAYING_BOOK_AGENT","0");



/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_CURRENCY_FILTER","1");

/********************
* Switch to Enable/Disable version2 of 
* natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE_VERSION2","1");

/********************
* Switch to Enable/Disable Exchange Rates Based on TransType
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCHNG_TRANS_TYPE","1");


/********************
* Switch to Enable/Disable Customer Countries selection on add admin staff page
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CUST_COUNTRIES", "1");

/********************
* Switch to Enable/Disable Getting ID Types from below list CONFIG_IDTYPES_LIST
* Add more id type in list by comma separated
* It should be noted that no space is allowed in comma separation
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_IDTYPES_FROM_LIST", "1");
define("CONFIG_IDTYPES_LIST", "Passport,ID Card,Full UK Driving License");

/********************

/********************
* Switch to Enable/Disable Prove Address option during add sender quick/normal
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_PROVE_ADDRESS", "1");
/********************
* Switch to Enable/Disable only IBAN Number (in bank transfer) 
* during European Union Transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EURO_TRANS_IBAN", "0");

/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");

/********************
* Switch to Enable/Disable import bulk Transactions 
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_IMPORT_TRANSACTION", "1");

/********************
* Switch to Enable/Disable 30 days expiry OFF
*	0 Disable
*	1 Enable 
* if 1, then there will be NO 30 days expiry of password
********************/
define("CONFIG_PWD_EXPIRY_OFF", "1");


/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Request");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUST_DOC_FUN", "1");


/********************
* Switch to Enable/Disable wild card search of customer
*	0 Disable
*	1 Enable and Config CONFIG_EXACT_MATCH_CUST_SEARCH should be '0'
********************/
define("CONFIG_CUST_WILDCARD_SRCH", "0");

/********************
* Switch to Enable/Disable sender search format according to Opal Requirement
* Two text boxes; sender name, sender number (respectively)
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Remove currency filter from daily cashier report
* 1-  Hide
* 0-  Show
********************/

define("CONFIG_OPTIMISE_NAME_SEARCH","1");
//define("CONFIG_MINIMUM_CHARACTERS_TO_SEARCH","3");

/********************
* Switch to Enable/Disable link for Default Payment Mode
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_DEFAULT_PAYMENT_MODE", "0");

/**
 * Select the default Money Paid option to select One 
 * @Ticket# 3517
 * @AddedBy Jahangir
 */

define("CONFIG_MONEY_PAID_SELECT_ONE",1);

/********************
* Switch to Enable/Disable displaying buttons "Amount, Local Amount"
* in front of their respective fields in create transaction page.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FRONT_BUTTONS", "1");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE", "1");

/********************
* Switch to Enable/Disable filter for Payment Mode e.g. By Cash ...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_FILTER", "1");

/********************
* Switch to Enable/Disable Add Shortcut link [by JAMSHED]
* Also, this functionality is given to those who use left_Express.php (user functionality)
* So, if you ON the variable CONFIG_ADD_SHORTCUT, then you should ON CONFIG_LEFT_FUNCTION
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADD_SHORTCUT", "1");

// This variable is used to check how many shortcuts can a user Add.
// It is limit for Shortcuts to add.
define("CONFIG_TOTAL_SHORTCUTS", "10");

define("CONFIG_ACCOUNT_TYPE_ENABLED","1");
define("CONFIG_REGULAR_ACCOUNT_TYPE_LABEL","Current");

/********************
* Variable to carrry ABA and CPF Countaining countries                                               
*	Switch to Enable/Disable the option                                               
*	                                               
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","0");

/********************
* Switch to use optimise search for agent on view transactions page--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMISE_AGENT_SEARCH","1");



/********************
* Switch to use optimise reference number search--added by javeria,#3187
* If value is '0' its Show
* If value is '1' its Hide
********************/
define("CONFIG_OPTIMIZE_REFERENCE_NO","1");



define ("CONFIG_VIEW_CURRENCY_LABLES","1");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/

define("CONFIG_TIP","Beneficiary Verification ID");
define("CONFIG_TIP_ENABLED","1");

/**
 * Adding a new field containg the client refrence number
 * @ticket 3321 / 3320
 */
define("CONFIG_CLIENT_REF", "1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* add/update admin staff opal pages 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_STAFF_OPAL_PAGES","1");

/********************
* Added By Khola Rasheed against ticket #3219 on 31-05-2008
* Switch to Enable/Disable editable admin type in manage admin staff page.
* 0 Disable
* 1 Enable
********************/
define("CONFIG_EDIT_ADMIN_TYPE", "1");

/********************
* Added By Khola Rasheed against ticket #3318: Minas Center - Bank Transfer Types
* Switch to Enable/Disable 24 hour banking module which addes 24 hour banking exchange rate 
* and create transaction has banking info whether normal or 24 hr
* 0 Disable
* 1 Enable
********************/
define("CONFIG_24HR_BANKING", "0");

/**
 * Setting the default transaction type to bank transfer
 * @Ticket# 3321 / 3320
 */
define("CONFIG_MODIFY_DEFAULT_PICKUP_TO_BANKTRANSFER","Bank Transfer");

/********************
* Added by Usman Ghani against #3321 minas center import sender
* To import a custom csv file.
* 0 Off
* 1 On
********************/
define("CONFIG_IMPORT_CUSTOM_SENDERS_DATA", 1);

/********************
* Switch to Enable or Disable Zero(0) Commission(Fee)
* For Beacon Crest and Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_ZERO_FEE","1");

/**************************************************************
* Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
* Turn this config on to show the Multiple ID Types functionlaity.
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY", 1);

/* Money paid as non mandatory*/
define("CONFIG_MONEY_PAID_NON_MANDATORY","0");

/**
 * To display custom list of banks in Bank Search Module.
 * @Ticket: #3538: Minas Center - Bank List Problem
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}
 */
define("CONFIG_CUSTOM_BANK_SEARCH_RESULTS", 1);


/**
 * To show a custom template file for minas center only.
 * @Ticket: #3526: Minas Center-Change the sample file of Import Beneficiary to the latest one.
 * @Author: Usman Ghani{usman.ghani@horivert.co.uk}.
 */
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE", 1);
define("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME", "sampleTempBen_minasCenter.xls");


/**************************************************************
* Added by Javeria against #3546:
add sending bank on create transaction page depending on money paid's value
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_ADD_SENDING_BANK_ON_CREATE_TRANSACTION", 1);


/**
 * Adding the bank based trasactions capability
 * this config will control the main.php entery against the bank based transactions
 */
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT","1");


/**
 * View senders / customer docuements at create transaction
 * @Ticket# 3779
 */
define("CONFIG_VIEW_SENDER_DOCS_AT_ADD_TRANSACTION",1);

/** 
 * Manage Upload document Categories 
 * @Ticket #3917
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

/**
 * Sender refrence number at confirm transaction page
 * @Ticket #
 */
define("CONFIG_SHOW_REFRENCE_NO_AT_CONFIRM", 1);
/**
 * Displays sender bank details on reports.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_BANK_DETAILS","1");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_TRANSFER_BY_BANK","1");
/**
 * Displays for check the cleint minasCenter.
 * 1 = Shows
 * 0 = Hides
 */
define("CONFIG_SENDER_REFERENCE_NUMBER","1");

/* added and enabled this config against ticket #3939 */
define("CONFIG_DUAL_ENTRY_IN_LEDGER","0");

define("CONFIG_AGENT_OWN_MANUAL_RATE", "1");
define("CONFIG_MANUAL_RATE_USERS", "admin, Branch Manager, Admin Manager,Admin,Call,");

/* unlimited exchange rate ticket #4136 */
define("CONFIG_UNLIMITED_EX_RATE_OPTION",'1');

/*user can enter unlimited manual exchange rate  #4553 */
define("CONFIG_UNLIMITED_EXCHANGE_RATE",'1');
define("CONFIG_UNLIMITED_EXRATE_COUNTRY","BRAZIL,PAKISTAN,POLAND,SPAIN,GEORGIA,CANADA,INDIA,");

/********************
* Switch to Enable/Disable settlement commission on create transaction
*       0 - enable
*       1 - disable
* i think it is to be removed from system as this development seem to be craped now.... just to confirm from javeria 
first...
********************/
define("CONFIG_SETTLEMENT_COMMISSION", "0");

/**
 * Agent Rate can be defined from the system 
 * @Ticket #4355
 */
define("CONFIG_AGENT_OWN_RATE","1");

/**
 * To retains the remarks about a customer/sender
 * at its creation and display at the add transaction
 * @Ticket #4230
 */
define("CONFIG_REMARKS_ABOUT_SENDER","1");

/********************
Added by Niaz Ahmad #2810 at 16-01-2008
*  Enable/Disable settlement currency dropdown
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN", "0");
define("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT", "1");
define("CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT", "1");

/**
 * CHEQUE CASHING MODULE
 * by assinging the value "1" to below config, cheque cashing module will be enabled
 * DONT FORGET to copy th below mentioned two arrays to other clients config file for enabling this module
 */
define("CONFIG_ENABLE_CHEQUE_CASHING_MODULE","1");
/**
 * Cheque Status
 * NG :		Pending
 * AR :		Clear
 * ID :		Paid
 * LD :		Hold
 * ST :		Cancel Request
 * EL :		Cancel
 * RN :		Return
 */
$arrChequeStatues = array(
	"NG" => "Pending",
	"AR" => "Clear",
	"ID" => "Paid",
	"LD" => "Hold",
	"ST" => "Cancel Request",
	"EL" => "Cancel",
	"RN" => "Return"
);

$arrChequeStatusRight = array(
	"NG" => "admin,Admin Manager",
	"AR" => "admin,Admin Manager",
	"ID" => "admin,AdminS,Call,Admin Manager",
	"LD" => "admin,Call,AdminS,Admin Manager",
	"ST" => "admin,AdminS,Call,Admin Manager",
	"EL" => "admin,Admin Manager",
	"RN" => "admin,Admin Manager"
);
/* Cheque Cashing MOdule Defination Ends */

/**
 * CURRENCY EXCHANGE MODULE
 * This config affects whole module.
 * if not enabled then you can't access page directly into url.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE","1");


/**
 * #4802
 * This config affects whole module on Search Sender form.
 * To use Exact search.
 * '1' For enable  and Config CONFIG_CUST_WILDCARD_SRCH should be '0'
 * '0' For disable
 */
define("CONFIG_EXACT_MATCH_CUST_SEARCH","1");

/**
 * #4802
 * This config affects whole module on Search Benificiary form.
 * To use Exact search.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_EXACT_MATCH_BEN_SEARCH","1");
/**
 * Added By Niaz Ahmad at 26-05-2009
 * #4851
 * This config is used to validate passport number.
 * To use Exact search.
 * '1' For enable
 * '0' For disable
 */
define("CONFIG_PASSPORT_VALIDATION","1");
/**
 * Enables/disables mode of payment on agent account
 * Ticket#4950
 * true = Enable
 * false = Disable
 */
define("CONFIG_ENABLE_PAYMENT_MODE_ON_AGENT_ACCOUNT", true);

/**
 * Shows/Hides cheque number, status fields on agent account statement.
 * Ticket#4950
 * true = Shows
 * false = Hides
 */
define("CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT", true);

/**
 * Enables/Disables filter for search by cheque number, on agent account statement.
 * Ticket#4950
 * true = Enables
 * false = Disables
 */
define("CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM", true);

 
/********************
* #5003	 START
* This functionality has following specs
* 1- All Agents or Specified Agent is assigned and its Ledgers are affected
* 2- Customers Ledgers also get affected.
* 3- Report can also be viewed on Depositing money.
* 4- On transaction Reciept there is a field of Payin amount.
*********************/
/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","1");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Customer ");
// Config below is added to give payin book functionality functionality to every one.
define("CONFIG_PAYIN_BOOK_FUNCTIONALITY_TO_EVERY_ONE", 1);
define("CONFIG_AUTO_PAYIN_NUMBER",'1');	

/********************

* Switch to Enable/Disable an agent associated with payin book customers
* If CONFIG_PAYIN_AGENT_NUMBER has value then Don't Use CONFIG_PAYIN_CUST_AGENT_ALL
* Also either CONFIG_PAYIN_CUST_AGENT_ALL OR CONFIG_PAYIN_AGENT_NUMBER is mandatory
* to use with CONFIG_PAYIN_CUST_AGENT
* by Aslam Shahid
*       0 Disable
*       1 Enable
********************/
define("CONFIG_PAYIN_CUST_AGENT", "1");
define("CONFIG_PAYIN_CUST_AGENT_ALL", "1");
//define("CONFIG_PAYIN_AGENT_NUMBER", "100221");


/**
 * redirect the user to reciept when depositing manualy in customer's ledger
 * ticket no 3549
 * @Ticket# 3554
 */
/*define("CONFIG_CUSTOMER_LEDGER_PAGE",1);
define("CONFIG_REF_NO_ON_CUSTOMER_ACCOUNT",1);
define("CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT",1);
*/

/**
 * Config enable the sender reference number generator
 * @Ticket# 3549
 */
define("CONFIG_SENDER_NUMBER_GENERATOR",1);

/**
 * For GE, normal sender will have payin book customer logic
 * This config will work as same as in express client but no will not display any label or field to clinet
 * @Ticket# 3549
 */
define("CONFIG__GE__USE_CUSTOMER_PAYIN_BOOK_LOGIC",1); // Associated Payin Senders can be searched on Transaction

/* This config is being added by Omair Anwer against ticket # 5000
 * The config brings up the functionality of putting a limit(uppper and lower limit) for a transactions 
 * such that incas the exchange rate changes drastically then at the time of the release or paying out an the
 * admin user can see and tell if this transaction can be payed out or not.
 * CONFIG_USE_EXTENDED_TRANSACTION should also be ON.
 */
define("CONFIG_ENABLE_EX_RATE_LIMIT","1");

/********************
* Switch to Enable/Disable Usage of an extended table with transaction 
* to keep optional info related to a particular transaction
* specifically used for connect plus and global Exchange
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_USE_EXTENDED_TRANSACTION", "1");


/********************
* #5003	 END
*********************/
/*
* Added by Niaz Ahmad
* @4997
* In case of companies it will show just one field instead of first/mid and last name
* Dated: 01-06-2009
*/
define("CONFIG_BENEFICIARY_FULLNAME_FOR_COMPANIES", "1");
define("CONFIG_SENDER_FULLNAME_FOR_COMPANIES", "0");
/*
* Added by Niaz Ahmad at 11-06-2009
* #5004 AMB Clinet
* This config is used to show transaction status alert queue at quick menu
* 2nd Config shows defult  hours it will shows all transaction that over 48 hours
*/
define("CONFIG_TRANSACTION_STATUS_ALERT_LIMIT_IN_HOURS","1");
/*
* Added by Niaz Ahmad at 22-06-2009
* #4999 AMB Clinet
* This config is used to show customer list alert at quick menu
* this config check the days limit of customer account if customer limit over the show alert
*/
define("CONFIG_CUSTOMER_ACCOUNT_DAYS_LIMIT","1");
define("CONFIG_MANAGE_USER_CONFIGURATION","1");


/*
* #5082
* Manual exchange rate can be used in currency exchange module
* Enable  = "1" (for all) OR admin, Branch Manager, Admin Manager,Admin,Call... etc
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_MANUAL_RATE_USERS_CURR", "1");

/*
* #4996 point 1
* Company type dropdown for MSB/3rd Party like companies on agents/AnD forms.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_COMPANY_TYPE_DROPDOWN", "1");

/*
* #4996 point 3
* Trading Name on agents form.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_TRADING_COMPANY_NAME", "1");


/*
* #4996 point 2
* Upload document functionality for AMB AnD's
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT", "1");
define("CONFIG_VIEW_UPLOAD_DOCUMENT_SUPPER_AGENT","1");
define("CONFIG_UPLOAD_DOCUMENT_ADD_SUB_AGENT","1");
define("CONFIG_VIEW_UPLOAD_DOCUMENT_SUB_AGENT","1");
/**************************************************************
* Added by Niaz Ahmad against #5085:
this config will control the main.php entery against the bank based transactions export
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT","0");
define("CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT","1");
/***************************************************************
* Added by Niaz Ahmad against #4950:
* this config use to show option of cheque at customer account statement
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT",1);
/***************************************************************
* Added by Niaz Ahmad against #4998:AMB
* this config use to show Alert at create transaction,cheque cashing and currecny exchange
* 0 Off
* 1 On
***************************************************************/
define("CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT",1);  
/** 
 * #5084
 * Item-Wise reconciliation of Bank Payments
 * '1' For enable 
 * '0' For disable 
 */ 
define("CONFIG_UNRES_PAY_BARCLAY_FILE_AJAX","1");
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");
/**
* Added by Niaz at 30-06-2009
* @4997 AMB
* when edit sender then in case of company it show add-company.php link at edit
*/
define("CONFIG_SENDER_AS_COMPANY",1);
/**
 * #5091
 * To stop Agent profit/Loss entry in ledgers
 * Also turn OFF the config CONFIG_DUAL_ENTRY_IN_LEDGER ,to stop the company Gain/ Loss entries in ledgers
 */
define("CONFIG_AT_CREATE_TRANSACTION_STOP_PROFIT_LOSS_ENTRY","1");

/* Config to display Currency filter on A&D account statement.
   Ticket#5001
*/
define("CONFIG_ADD_CURRENCY_DROPDOWN", "1");

/* Being added against ticket 4992 by Omair Anwer
 *
 *
 */
define("CONFIG_STORE_MODIFY_TRANSACTION_HISTORY","1");

/*
* #4949
* Paypoint Transactions functionality added alongwith its services,account summary under this config.
* Enable  = "1"
* Disable = "0"
* by Aslam Shahid
*/

define("CONFIG_ADD_PAYPOINT_TRANS", "1");

/********************
*	#5186
* Switch to Enable/Disable Filteration and Showing Receiving Currency
	with Money Out in Bank/Distributor Account Statements.
	More Importantly following config should be OFF.
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN
	
	And these sould be on... (Although settlement logic is not used here.)
	CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT
	CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD

	NOTE :: All this scenario is set-up for
			1- System uses A&D functionality.
			2- There are no settlements currencies in A&D.
			3- Ledgers A&D as Distributor will be affected properly. (Created,Cancelled,Rejected,Failed,Credited etc)
			4- Account Summary and Balance of A&D as Distributor is affected as well.
*	0 means Disable
*	1 means Enable 
*	by Aslam Shahid
********************/
define("DEST_CURR_IN_ACC_STMNTS", "1") ;
define("CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD", "1") ;
/**********************
* This config is used to remove extra text in the Add/Update Agent/Distributor on the label for MSB Number
* @5275
* Disable =0 
* Enable =0
* Niaz Ahmad at 13-08-2009
***********************/
define("CONFIG_REMOVE_MSB_TEXT_FROM_ADD_AGENT_DISTRIBUTOR","1");
/**********************
* This config is used to make Bank Name non mandatory at add/update collection point
* @5281
* Disable =0 
* Enable =0
* Niaz Ahmad at 13-08-2009
***********************/
define("CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT","1");
/**********************
* This config is used to Add beneficary as company
* @5279
* Disable =0 
* Enable =0
* Niaz Ahmad at 17-08-2009
***********************/
define("CONFIG_BENEFICIARY_AS_COMPANY","1");
/*
* #5336
* On Printing Transactions dropdown is added in case 
* if there are different Receipts for Pick and Bank Transfer then
* user can only print multiple receipts but of same type. 
* CONFIG_RECIEPT_PRINT_BANK should be assigned file name value for Bank Transfer Receipt.
* CONFIG_RECIEPT_PRINT should have Pick up format file name value.
* Enable  = "1"
* Disable = "0"
* by A.Shahid
*/
define("CONFIG_SHOW_TRANS_TYPE_RECEIPTS_PRINT","1");
/**********************
* This config is used to Add ben bank fields at add transaction page
* when this config is ON then IBAN configs will be OFF
* e.g CONFIG_IBAN_OR_BANK_TRANSFER and CONFIG_EURO_TRANS_IBAN will be OFF
* @5282
* Disable =0 
* Enable =0
* Niaz Ahmad at 18-08-2009
***********************/
define("CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION","1");

//#5283-manual commission
define("CONFIG_MANUAL_FEE_RATE","1");
define("CONFIG_BEN_COUNTRY","INDIA,PAKISTAN,");
define("CONFIG_USE_MANUAL_COMMISSION_AJAX","1");

/* Dear all, This config is being added under ticket 5294.
 * This config makes sure that all transactions that are to be viewed via the Quick Menu
 * export link for Western Union only shows transactions that have been authorised.
 * Config Added by Omair Anwer
 */
define("CONFIG_EXPORT_TRANSACTION_CUSTOMIZE","1");

//5286-hide ben verification id on create transaction
define("CONFIG_HIDE_TIP_FIELD","1");

//5289-User account statement
define("CONFIG_CREATE_ADMIN_STAFF_LEDGERS", "1");
$_arrAdminStaffForCreateTransactionLedgers = array(
													"Admin", "Admin Manager",
													"CALL"
												);
												
/** 
 * #5258 Minas Center
 * Shows Opening/Closing Balnaces on Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 											
define("CONFIG_ADMIN_STATEMENT_BALANCE", "0");
/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Currency Excange Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_ADMIN_LEDGER", "1");

/** 
 * #5258 Minas Center
 * Ledgers of all users are affected for Cheque Cashing Module
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CHEQUE_CASHING_ADMIN_LEDGER", "1");

/** 
 * #5255 Minas Center Point #7
 * Ledgers of all users are affected for User Denomination
 * and utilized in Admin Account Statement
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_USER_DENOMINATION_ADMIN_LEDGER", "0");
												
/** 
 * #5365 AMB Exchange
 * Shown Note input box at add transaction page
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad at 21-08-2009
 */ 
define("CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION", "1");
define("CONFIG_TRANSACTION_NOTE_AT_ADD_TRANSACTION_LABELS","Transaction Notes");
/** 
 * #5364 AMB Exchange
 * customize bank base transaction page
 * shown pick up transaction , remove ID column, exclude western union distributor transaction...etc
 * Enable  : "1"
 * Disabl  : "0"
 * by Niaz Ahmad at 22-08-2009
 */ 
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION","1");
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION_EXCLUDE_DISTRIBUTOR","100226");
define("CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION_QUICK_MENUE_LABEL","Pick up and Bank Based Transactions");

/** 
 * #5178 - Point 4 of Document
 * Use Round number and give value of decimal places to 
 * be rounded.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 											
define("CONFIG_ROUND_NUMBER_ENABLED_CURR_EXCH", "1");
define("CONFIG_ROUND_NUMBER_TO_CURR_EXCH", "0"); // Number of rounded decimals

define("CONFIG_RELEASE_BULK_TRANSACTIONS_FILE","releaseBulkTransactions.php");

/***************
CONFIG  for transactions that are based on money paid,
By cheque.if a transaction is by cheque type it will go to
"un clear cheque payments" before going to pending status
done by javeria ticket # 3265
* If value is '0' its Disabled
* If value is '1' its Enabled
****************/
define("CONFIG_CHEQUE_BASED_TRANSACTIONS","1");
/** 
 * #5441
 * Operator Code is added in AnD form.
 * to be displayed in Western Union Output export file.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */  
define("CONFIG_OPERATOR_ID_AnD","1");
/** 
 * #5441
 * Additional Fields for AMB enhancement of
 * export file to be displayed in Western Union Output export file.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_CUSTOMIZED_FIELDS_AMB_EXCHANGE","1");

/** 
 * #5544
 * Dropdown for agents is displayed showing all Agents.
 * Also ledger will be affected accordingly.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_USE_PAYPOINT_AGENT_DROPDOWN","1");

/** 
 * #5544
 * Default agent for paypint transactions.
 * CONFIG_USE_PAYPOINT_AGENT_DROPDOWN should also be ON for this.
 * Enable  : 1
 * Disable : 0
 * by Aslam Shahid
 */ 
define("CONFIG_PAYPOINT_DEFAULT_AGENT_ID","100230");

/** 
 * #5287 Minas Center
 * Currency stock account is mainted and currency stock
 * amounts are deducted/added
 * Enable  : "1"
 * Disabl  : "0"
 * by Aslam Shahid
 */ 
define("CONFIG_CURR_EXCH_STOCK_ACCOUNT", "1");

/** 
 * #4595
 * Currency exchange :: now customer in creating currency exchange transaction
 * becomes non-mandatory for specified User groups in config.
 * Enable  : 1 or User types..(Admin,SUPA, etc..)
 * Disable : 0
 */ 
define("CONFIG_CUST_NON_MANDATORY_USERS_CURR","1");

/** 
 * #4693
 * Cancel currency exchange transaction functionality.
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_CANCEL_TRANS_CURR_EXCH","1");

/** 
 * #5751
 * Currency exchange stock list in left menu
 * Enable  : 1
 * Disable : 0
 */ 
define("CONFIG_CURRENCY_STOCK","1");

/** 
 * #5750
 * Manula Super AnD number (Login name) can be entered 
 * in AnD forms (super and sub AnD)
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONFIG_MANUAL_SUPER_AnD_NUMBER","1");

/** 
 * #5750
 * Manula Sub AnD number (Login name) can be entered 
 * in AnD forms (super and sub AnD)
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONFIG_MANUAL_SUB_AnD_NUMBER","1");

/** 
 * #5747
 * Currency filter and Total Amount is added 
 * on currency exchange reports page
 * Enable  : "1"
 * Disable : "0"
 * by A.Shahid
 */ 
define("CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE","1");
/**  
 * #5088:AMB Exchange
 * added new module for deal sheet 
 * Enable  : "1"
 * Disable  : "0"
 * by Niaz Ahmad 
 */  
define("CONFIG_DISTRIBUTION_DEAL_SHEET","1");
define("CONFIG_DEAL_SHEET_USER_TYPE","Distributor");
define("CONFIG_DEAL_SHEET_VIEW_USERS","SUPI,SUBI,SUPAI,SUBAI,admin");

/**  
 * #5995:AMB Exchange
 * Modified functionality under this previously existing config
 * such that opening/closing balances are correct [calculated on runtime from ledger, not summary]
 * Opening for same date remains same and closing is swapped as opening for next date.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */  
define("CONFIG_SWAP_CLOSING_TO_OPENING","1");

/* #5781 - AMB Exchange
 * Currency Exchange Fee module added
 * Also works for manual fee. Currency Report is added in left menu as well.
 * Enable  : "1" for all users or User types..(Admin,SUPA, etc..)
 * Disable : "0"
 * by Aslam Shahid.
*/
define("CONFIG_CURRENCY_EXCHANGE_FEE_USERS","1");
define("CONFIG_CURRENCY_EXCHANGE_MANUAL_FEE_USERS","1");

/* AMB Exchange
 * Double entry system. this was existing in configurator.
 * Enable  : "1"
 * Disable : "0"
*/
define("CONFIG_DOUBLE_ENTRY","1");

/* #7106 - AMB Exchange
 * Currency Exchange Fee is non-mandatory
 * Enable  : "1" for all users or User types..(Admin,SUPA, etc..)
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 1142
 * by Aslam Shahid
*/
define("CONFIG_FEE_NON_MANDATORY_USERS_CURR","1");

/* #7106 - AMB Exchange
 * Currency Exchange Stock is affected in reverse on creation 
 * of currency exchange.
 * Enable  : "1"
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 1143
 * by Aslam Shahid
*/
define("CONFIG_REVERSE_STOCK_ACCOUNT","1");
/* #7138 - AMB Exchange
 * When currency exchange transaction is created then GBP [operational currency account is affected in view accounts of
   Double Entry / Trading]
 * Enable  : account number of currency exchange.
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_CURRENCY_EXCHANGE_ACCOUNT","11223344");

/* #7825 - AMB Exchange
 * Saved currency exchange reports can be displayed
 * Enable  : account number of currency exchange.
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS",'1');

/* #8597 - AMB Exchange
 * Allow Zero balance in account
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_ALLOW_ZERO_BALANCE_ACCOUNT",'1');


/* #8597 - AMB Exchange
 * When sender amount is typed then it is copied in receiving amount fields as well.
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_SENDING_COPY_TO_RECEIVING_INTER_ACCOUNT",'1');

/* #8794 - AMB Exchange
 * Transaction ID format on the basis of currency exchange [CE-1]and 
 * inter fund transfer [FT-1]. also it shows the search filter on view accounts page.
 * also account number is displayed which is affected against selected account for fund transfer
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_ACCOUNT_LEDGER_TRANS_TYPE",'1');
define("CONFIG_ACCOUNT_LEDGER_TRANS_TYPE_TT",'1');

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

define("CONFIG_TRANS_TYPE_TT",'1');

/* #9457 - AMB Exchange
 * Hide agent select selection from sender form
 * Enable  : 1
 * Disable : 0 
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_HIDE_AGENT_CUSTOMER_PAGE","1");

/********************
* Switch to Enable/Disable non compulsory phone field
* 0 Disable
* 1 Enable
********************/
define("CONFIG_PHONE_NON_COMPULSORY", "1");
/********************
*#9463: AMB Exchange: Sender and beneficiary as Company
* 0 Disable
* 1 Enable
********************/
define("CONFIG_COMPANY_CUSTOMIZE", "1");
//define("CONFIG_MAIL_GENERATOR", "1");

/* #9363 - AMB Exchange
 * Foreign Currency Stock History
 * Enable  : 1
 * Disable : 0 
 * Client: AMB Exchange
 * Control No: 
 * by Kalim ul Haq
*/
define("CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY","1");
// #9618: AMB Exchange: Decimal point Configuration
define("CONFIG_DECIMAL_POINT_CURRENCY_EX_STOCK","1");
?>
