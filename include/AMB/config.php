<?php
include_once('functions.lib.php');
$dbresource = databaseConnect(SERVER_MASTER,USER,PASSWORD,DATABASE);

$ConfigCollection = select_MySql('form',array('varName','value'));
for($i=0;$i<sizeof($ConfigCollection);$i++){
	$Var = $ConfigCollection[$i]['varName'];
	$Value = $ConfigCollection[$i]['value'];
	define($Var,$Value);
}

/* AMB Exchange
 * Double entry system. this was existing in configurator.
 * Enable  : "1"
 * Disable : "0"
*/
define("CONFIG_DOUBLE_ENTRY","1");
/* #7106 - AMB Exchange
 * Currency Exchange Fee is non-mandatory
 * Enable  : "1" for all users or User types..(Admin,SUPA, etc..)
 * Disable  : "0" 
 * Transaction
 * Client: speedfast
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_FEE_NON_MANDATORY_USERS_CURR","1");

/* #7106 - AMB Exchange
 * Currency Exchange Stock is affected in reverse on creation 
 * of currency exchange.
 * Enable  : "1"
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 1143
 * by Aslam Shahid
*/
define("CONFIG_REVERSE_STOCK_ACCOUNT","1");
/* #7138 - AMB Exchange
 * When currency exchange transaction is created then GBP [operational currency account is affected in view accounts of
   Double Entry / Trading]
 * Enable  : account number of currency exchange.
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_CURRENCY_EXCHANGE_ACCOUNT","11223344");

/* #7825 - AMB Exchange
 * Saved currency exchange reports can be displayed
 * Enable  : account number of currency exchange.
 * Disable  : "0" 
 * Transaction
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS",'1');

/* #8597 - AMB Exchange
 * Allow Zero balance in account
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_ALLOW_ZERO_BALANCE_ACCOUNT",'1');


/* #8597 - AMB Exchange
 * When sender amount is typed then it is copied in receiving amount fields as well.
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_SENDING_COPY_TO_RECEIVING_INTER_ACCOUNT",'1');

/* #8794 - AMB Exchange
 * Transaction ID format on the basis of currency exchange [CE-1]and 
 * inter fund transfer [FT-1]. also it shows the search filter on view accounts page.
 * also account number is displayed which is affected against selected account for fund transfer
 * Enable  : 1
 * Disable : 0 
 * Double Entry / Trading
 * Client: AMB Exchange
 * Control No: 
 * by Aslam Shahid
*/
define("CONFIG_ACCOUNT_LEDGER_TRANS_TYPE",'1');
define("CONFIG_ACCOUNT_LEDGER_TRANS_TYPE_TT",'1');

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

define("CONFIG_TRANS_TYPE_TT",'1');

?>