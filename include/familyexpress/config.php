<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","familyexpress");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","images/familyexpress/logo.jpg");
define("CONFIG_LOGO_WIDTH","157");
define("CONFIG_LOGO_HEIGHT","63");

/********************
* Switch to Enable/Disable Edit Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");


/********************
* Switch to Enable/Disable Hold/Unhold Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Batch Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Verify Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");


/********************
* Switch to apply charges on Cash payements for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","5");


/********************
* Switch to show Terms & Conditions for Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","0");
define("CONFIG_TRANS_COND","I confirm that the source of funds for purposes of money transmission is legitimate and has no predicate offence motives.");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");



/********************
* Switch to show Remarks field in Transactions  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/******************** Imran is to Confirm this thing also as he worked for this logic
* Switch to show whether there is a logic for Branch Manager being used or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","0");



/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent  
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent 
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at 
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");


/********************
* Perhaps Imran Added this Variable
* so he is to add its comments
********************/
define("CONFIG_FEE_DEFINED","1");


/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at 
* Cancel transaction if customer 
********************/
define("CONFIG_FEE_ON_REFUND", "5");

/********************
* Switch to Enable/Disable Fee on the basis of denominator 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","1");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable Fee based on Distributor 
* If value is '0' its Disabled
* If value is '1' its Enabled
* If this is enabled...
* Agent Based fee would be automatically Disabled
********************/
define("CONFIG_FEE_DISTRIBUTOR","1");


/********************
* Switch to Enable/Disable PayinBook Customers 
* If value is '0' its Disabled
* If value is '1' its Enabled but with label Payin Book Customer
* if value is '2' its Enabled but with label Out City Customer
********************/
define("CONFIG_PAYIN_CUSTOMER","2");



/********************
* Switch to Enable/Disable PayinBook Limit 
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","1");



/********************
* Switch to control the charges applied on issueing of new 
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "4");
define("CONFIG_PAYIN_NUMBER", "6");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at 
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","1");



/********************
* Switch to Enable/Disable access of distributor output file to other users 
*   e.g. Distributor 
* and second Variable is to whether we are going to show 
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","1");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","1");


/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","1");




/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","FX");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Family Express Code");
define("COMPANY_NAME","Family Express");
define("CONFIG_FEE_NAME","Commission");

/********************
* Variable used to define value of the 
* Label at Transactions
********************/
//This config show Message field label on release transaction page and on DOF.
define("CONFIG_REMARKS","Message");

/********************
* Switch to show Remarks field on create Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
// This config shows Message on create/confirm/view transction page and on receipt page as well.
define("CONFIG_TIP","Message");
define("CONFIG_TIP_ENABLED","1");

/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "noreply@familyexpress.co.ca");
define("INQUIRY_EMAIL", "enquiries@familyexpress.co.ca");
define("CC_EMAIL", "info@familyexpress.co.ca");
define("SUPPORT_NAME", "Company Support");


define("CONFIG_INVOICE_FOOTER", " ");
define("COMPANY_URL","www.familyexpress.co.ca");




define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex- Family Express ");
define("TITLE_ADMIN", "Payex- Family Express ");


/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");

define("CONFIG_MAX_TRANSACTIONS_FOR_PRINT","20");


define("CONFIG_AGENT_STATMENT_GLINK","0");


/********************
* Maximum Number of Transactions
********************/

define("CONFIG_MAX_LIMIT_FOR_PAYOUT_REPORT","20");

define("CONFIG_MAX_LIMIT_FOR_OUTSTANDING_PAYOUT_REPORT","20");

/**********************************
* Auto Authorization of the Transaction
* For Agent.
* This rule is generic and applies to 
* all agents in the system
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/********************
* Switch to Enable/Disable Facility
* for customization of autoAuthorization 
* for the selected agents who are 
* configured as custom agents
********************/
define("CONFIG_CUSTOM_AUTHORIZE","1");
define("CONFIG_CUSTOM_AGENTS","MS");


/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the 
* system at the time of transaction 
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","0");


/**********************************
* Lable Selection for the Exchange Rate
***********************************/
define("CONFIG_ENABLE_RATE","1");
define("CONFIG_PIMARY_RATE","Upper Exchange*");
define("CONFIG_NET_RATE","Lower Exchange");


/**********************************
* Switch to check exact equal Balance
* for Authorization or more balance can
* also work
***********************************/
define("CONFIG_EXACT_BALANCE","1");

/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT","0");
define("CONFIG_RECIEPT_NAME","express-confirm-transaction.php");


/********************
* Switch to Enable/Disable countries 
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");


/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");


/********************
* For View enquiries page 
* Whether to show 'New' and 'Close' in drop down menu
* or 'Unresolved' and 'Resolved' and so on...
* Edited by Jamshed........
********************/
define("CONFIG_ENQUIRY_VARIABLE","1");
define("CONFIG_ENQUIRY_VAR_FOR_NEW","Unresolved");
define("CONFIG_ENQUIRY_VAR_FOR_CLOSE","Resolved");

/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor 
* 0 Teller is limited to its own collection point not all 
* transactions linked to distributor of that collection point
********************/
define("CONFIG_TELLER_ACCESS","0");

/********************
* This variable is used to enter the 
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","CA");

/********************
* Switch to Enable/Disable City Service such as Home Delivery 
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated 
* 1 means Ledgers will be updated 
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County 
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");


/********************
* Reports Based on Originating Currency or Destination Currency 
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");


/********************
* Special Fields for Family express Distributor output file. i.e Branch Code and Branch Name 
* 0-  Don't show extra fields
* 1-  Show extra fields
********************/
define("CONFIG_DIST_FIELDS","1");


/********************
* To Enable Home Delivery in Distributor Out put File
********************/
define("CONFIG_DIST_HOME_DELIVERY","1");

/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
define("CONFIG_AMOUNT_CONTROLLER", "3000");

/********************
* Switch to Enable/Disable Company MANUAL_CODE (e.g., Family Express Code) in Reports
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_SHOW_MANUAL_CODE", "1");

/********************
* Switch to Enable/Disable to show Country/Currency Filter
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CNTRY_CURR_FILTER", "1");

/********************
* Switch to Enable/Disable to show Remarks Column in Distributor Output File
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTOUTFILE_REMARKS_ENABLE", "1");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#0000FF");
define("CAUTION_COLOR", "#990000");

/********************
* Switch to Enable/Disable display of last transaction (upto transaction type and its details)
* between Customer and Beneficiary
* Done for opal Transfer and Family Express
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");


/********************
* switch to enable/disable search options in sender/beneficiary report
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_SENDER_BENIFICIARY_REPORT", "1");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");

/******************** 
* Switch to Enable/Disable Association between 
* Agent and Admin staff for transactions 
* 0 Disable
* 1 Enable ********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,");

// It is for Opal, number will be completely rounded
define("CONFIG_OPAL_ROUND_NUM", "1");

/********************
* To show link Distributor Locator on Main Menu for following user types
********************/
define("CONFIG_AGENTTYPES_DISTLOCATOR", "Admin, SUPAI,");

/********************
* Switch to Enable/Disable default Transaction Purpose
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_DEFAULT_TRANSACTION_PURPOSE", "1");


/********************
* Switch to Enable/Disable to cancel reverse effect of cancelled transactions
*	0 means Disable
*	1 means Enable 
********************/
define("CONFIG_CANCEL_REVERSE_COMM", "1");



/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");
/********************
[ADDED BY NIAZ AGAINST #2331 AT 08-10-2007]
* Enable Sending/Receiving Currency and Country 
* 0-  Disable
* 1-  Enable
********************/  
define("CONFIG_SENDING_RECEIVING_COUNTRY_CURRENCY","1");

/********************
[ADDED BY NIAZ AGAINST #2331 AT 08-10-2007]
* Enable Export Beneficiary
* 0-  Disable
* 1-  Enable
********************/  
define("CONFIG_EXPORT_BENEFICIARY","1");

/********************
* Enable/Disable Agent Based Exchange Rate
* 0-  Disable
* 1-  Enable
********************/ 
define("CONFIG_AGENT_OWN_RATE","0");

/*
* Create Transaction Amount 2 digit Round number
* 0-  Disable
* 1-  Enable
*/
define("CONFIG_TRANS_ROUND_NUMBER", "1");
define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,");
define("CONFIG_TRANS_ROUND_CURRENCY", "USD,");
define("CONFIG_TRANS_ROUND_CURRENCY_TYPE", "CURRENCY_TO");
define("CONFIG_TRANS_ROUND_LEVEL", "2");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY, 
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example, 
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","0");

define("CONFIG_BEN_COUNTRY","UNITED KINGDOM");

/********************
* Switch to Enable/Disable full discount of Fee
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_TOTAL_FEE_DISCOUNT", "1");
define("CONFIG_DISCOUNT_EXCEPT_USER", "");
define("CONFIG_DISCOUNT_LINK_LABLES", "Fee Discount Report");
/********************
* Added by Niaz Ahmad against ticket #2538 at 26-10-2007
* Switch to Enable/Disable  Fee Base on Trans type
* Switch to Enable/Disable  AJAX Functionality
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_FEE_BASED_TRANSTYPE", "1");
define("CONFIG_FEE_BASED_DROPDOWN", "1");
define("CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX", "1");

/********************
* Added by Niaz Ahmad against ticket #2605 at 24-10-2007
* Switch to Enable/Disable  Commission for Distributor Output File
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMMISSION_DISTRIBUTOR_FILE", "1");
/********************
* Added by Niaz Ahmad againist ticket # 2331 at 06-11-2007
*  Agent Based Exchange Rate,Agent can view own and Generic exchange rate  
* 0-  Disable
* 1-  Enable
********************/  
define("CONFIG_AGENT_OWN_GENERIC_RATE","0");

/********************
* Switch to Enable/Disable settlement commission on create transaction
*       0 - enable
*       1 - disable
********************/
define("CONFIG_ATM_CARD_OPTION", "1");

/********************
* Added by Niaz Ahmad againist ticket # 1727 at 08-01-2008
*  show popup at create transaction
* 0-  Disable
* 1-  Enable
********************/  
define("CONFIG_COMPLIANCE_PROMPT", "1");
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM","1");
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");
/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");
define("CONFIG_AGENT_STATEMENT_BALANCE","1");


///remove it 
//define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");
//define("CONFIG_MONEY_PAID_NON_MANDATORY", "1");
//define("CONFIG_CALCULATE_BY", "1");

/********************
* Add manual reference number on view enquiry
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_MANUAL_REFNUM_IN_ENQUIRY","1");

/********************
* This config shows Message on create/confirm/view transaction page and on receipt page as well.
********************/

/********************
* Show Associated agent transaction on vie transaction for admin staff
* 0-  Disable
* 1-  Enable
* Added by Khola @ticket #3655
********************/
define("CONFIG_VIEW_ASSOCIATED_AGENT_TRANS", "1");

define("CONFIG_TIP_ENABLED","1");
define("CONFIG_TIP","Message");

/**
 * Config added to sort the sender list by name in sender search page.
 * Ticker: #3761: Search Senders
 */
define( "CONFIG_SENDER_SEARCH_ORDER_BY_NAME", "1" );

/********************
* Switch to Enable/Disable field on add sender page; profession
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PROFESSION_ADD_SENDER","1");
define("CONFIG_PROFESSION_FIELD_LABEL", "Occupation");

/********************
* Defines order of search results, either Ascending or Descending
* If value is '0' its Descending
* If value is '1' its Ascending
********************/
define("CONFIG_SEARCH_RESULT_ORDER", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1");
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php");
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php");

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1");

/**
 * Creating the new file documentCategories.php 
 * having control by this config
 * @Ticket# 3554
 */
define("CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES",1);

/**
 * The commission is calculated on the base of the amount, which is specified by followin g config
 * If this config is unavailable than the default value will be 'Amount' (transAmount)
 * @Ticket #4730
 */
define("CONFIG_COMMISSION_CALCULATED_ON", "localAmount");
/**
Added by Niaz Ahamd at 09-04-2009 for @4757
DOB is compulsory for ATM_VISA distributor at create transaction
***/
define("CONFIG_BEN_DOB_COMP_ATM_VISA_DISTRIBUTOR","1");
define("CONFIG_BEN_ATM_VISA_DISTRIBUTOR","1002640065,");

/*	@Ticket #4766
	This Config enables disables assigned rights like (Create trannsaction) Also user-functionality
	
*/
define("CONFIG_ADD_USER_RIGHT","1");

/**
 * To use the separate Distributor Output File for each client
 * feed the filename into the below menioned config
 * @Ticket #4362
 */
define("CONFIG_CUSTOMIZED_OUTPUT_FILE","Distributor_Output_File_Familyexpress.php");
/**
 * Added by Niaz Ahmad at 22-05-2009
 * To remove auto response email to sender at create transaction
 * ahutorize transaction
 * @Ticket #4979 family express
 */
define("CONFIG_CUSTOMER_EMAIL_ADD_TRANS_DISABLED","1");

/* This config is being added to enable the Zero commission in the system
 * while creating the transaction.;
 * Ticket #4987
 */
define("CONFIG_ZERO_FEE","1");

?>