<?php

// Load the config from the database
include "../include/load_config.php";

/********************
* To define Client to use System.
********************/
define("SYSTEM","Bayba MT Powered by Payex");

/********************
* Logo Information of the particular sandbox in
* terms of its path and its size to be viewed.
********************/
define("CONFIG_LOGO","../admin/images/bayba/logo.jpg");
define("CONFIG_LOGO_WIDTH","331");
define("CONFIG_LOGO_HEIGHT","47");

/********************
* Switch to Enable/Disable Edit Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("DISPLAY_EDIT_TRANSACTIONS","1");


/********************
* Switch to Enable/Disable Hold/Unhold Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_HOLD_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Batch Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BATCH_TRANSACTION_ENABLED","0");

/********************
* Switch to Enable/Disable Verify Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_VERIFY_TRANSACTION_ENABLED","0");
define("CONFIG_DEFAULT_DISTRIBUTOR", "1");

/********************
* Switch to apply charges on Cash payements for Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
* Second line is to determine Amount being charged
********************/
define("CONFIG_CASH_PAID_CHARGES_ENABLED","0");
define("CONFIG_CASH_PAID_CHARGES","0");


/********************
* Switch to show Terms & Conditions for Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
* If Enabled then system will show client specific Terms & Conditions
*     these Terms & Conditions are defined in the very nex line
*   elseif Disabled then Payex generic Terms and Conditions would be used
********************/
define("CONFIG_TRANS_CONDITION_ENABLED","1");
define("CONFIG_TRANS_COND","I confirm that the source of funds for purposes of money transmission is legitimate and has no predicate offence motives.");


/********************
* To show only the country where the services are set from Add/Update service. This will apply for adding beneficiary
* form and adding distributor form
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_COUNTRY_SERVICES_ENABLED","1");



/********************
* Switch to show Remarks field in Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_REMARKS_ENABLED","1");


/********************
* One branch manager can look into their agents transaction and manage them
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("IS_BRANCH_MANAGER","1");



/********************
* Switch to Enable/Disable Email being sent during Transactions
* to the Distributor or Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED","0");
define("CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED","0");


/********************
* Switch to Enable/Disable either a customer
* is strongly linked to only one agent or
* all agents can view all customers
* If value is '0' its Disabled ... All Agents and All Customers
* If value is '1' its Enabled ... A customer is limited to only agent
*   where it is registered.
********************/
define("CONFIG_CUST_AGENT_ENABLED","0");


/********************
* Switch to Enable/Disable Secret Questions at
* Create Transactions that will be asked to beneficairy
* at the Distributor's End to release Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_SECRET_QUEST_ENABLED","1");


/********************
* If option is 1 then the Commission name will be used rather thne Fee.
* If the option is 0 then the Fee will use by default
********************/
define("CONFIG_FEE_DEFINED","1");
define("CONFIG_FEE_NAME","Commission");

/********************
* It is to Control that if one of the Admin staff is
* going to create transaction on behalf of an online customer
*  whether he need to input PIN Code of online customer or not.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("PIN_CODE", "0");


/********************
* Switch to Define Amount to be charged to customer at
* Cancel transaction if customer
********************/
define("CONFIG_FEE_ON_REFUND", "0");

/********************
* Switch to Enable/Disable Fee on the basis of denominator
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_DENOMINATOR","1");


/********************
* Switch to Enable/Disable Fee on the Basis of Agent
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_FEE_AGENT_DENOMINATOR","0");
define("CONFIG_FEE_AGENT","1");



/********************
* Switch to Enable/Disable PayinBook Customers
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_CUSTOMER","0");
define("CONFIG_PAYIN_CUSTOMER_ACCOUNT","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL_SHOW","1");
define("CONFIG_PAYIN_CUSTOMER_LABEL","Walkin Customer");


/********************
* Switch to Enable/Disable PayinBook Limit
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_LIMET","0");



/********************
* Switch to control the charges applied on issueing of new
*  PayinBook to any customer
* second Variable is to specify the length of Payin Book
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYIN_FEE", "0");
define("CONFIG_PAYIN_NUMBER", "10");

/********************
* switch to generate auto payin number or not...
********************/
define("CONFIG_AUTO_PAYIN_NUMBER", "1");


/********************
* Switch to Enable/Disable Foriegn Currency Charges at
* Create Transactions
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CURRENCY_CHARGES","0");



/********************
* Switch to Enable/Disable access of distributor output file to other users
*   e.g. Distributor
* and second Variable is to whether we are going to show
*   Department Information on that File or not
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_DISTRIBUTOR_FILE_TO_DISTRIBUTOR","1");
define("CONFIG_DISTRIBUTOR_FILE_DEPT_INFO","0");


/********************
* Switch to Enable/Disable natwest Bank Transaction
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_NATWEST_FILE","0");



/********************
* Switch to Enable/Disable Defalut
* agent where agent is not selected
********************/
define("CONFIG_ENABLE_DEFAULT_AGENT","0");



/********************
* Variable used Client Secific to show
* Company's Lables for the specific things
********************/
define("SYSTEM_PRE","BG");
define("SYSTEM_CODE","Reference Code");
define("MANUAL_CODE","Bayba Code");
define("COMPANY_NAME","The Bayba Group");
define("CLIENT_NAME","Bayba");


/********************
* Variable used to define value of the
* Label at Transactions
********************/
define("CONFIG_REMARKS","Remarks");


/********************
* Company specific information to be used in the system
********************/
define("SUPPORT_EMAIL", "noreply@bayba.co.uk");
define("INQUIRY_EMAIL", "enquiries@bayba.co.uk");
define("CC_EMAIL", "info@bayba.co.uk");
define("SUPPORT_NAME", "Bayba Group");


define("CONFIG_INVOICE_FOOTER", "020 770 29595");
define("COMPANY_URL","www.bayba.co.uk");




define("USER_ACCOUNT_URL","http://".PAYEX_URL."/user");
define("ADMIN_ACCOUNT_URL","http://".PAYEX_URL."/admin/");


define("TITLE","Payex - The Bayba Group");
define("TITLE_ADMIN", "Payex - The Bayba Group");


/********************
* Maximum Number of Transactions
********************/
define("CONFIG_MAX_TRANSACTIONS","50");


define("CONFIG_AGENT_STATMENT_GLINK","1");



/**********************************
* Auto Authorization of the Transaction
* For Agent.
***********************************/
define("CONFIG_AUTO_AHTHORIZE","0");


/**********************************
* Record of the transaction in the
* system at the time of transaction
* creation
***********************************/
define("CONFIG_LEDGER_AT_CREATION","1");

/**********************************
* Record of the transaction in the
* system at the time of transaction
* creation
***********************************/
define("CONFIG_AGENT_LIMIT","1");


/********************
* Switch to Enable/Disable countries
* other than United Kingdom for originaing
* transactions
********************/
define("CONFIG_ENABLE_ORIGIN","1");

/********************
* This variable is used to enter the
*	current date & time of specific country in database
********************/
define("CONFIG_COUNTRY_CODE","UK");

/********************
* Switch to Enable/Disable City Service such as Home Delivery
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CITY_SERVICE_ENABLED","0");


/********************
* Variable to carrry ABA and CPF Countaining countries
*	Switch to Enable/Disable the option
*
********************/
define("CONFIG_CPF_COUNTRY","Brazil");
define("CONFIG_CPF_ENABLED","1");


/********************
* Switch to Enable/Disable round function
*	0 means Disable
*	1 means Enable
* it is used to convert float into integer
********************/
define("CONFIG_ROUND_NUMBER_ENABLED","0");
define("CONFIG_TRANS_ROUND_LEVEL", "4");


/********************
* Switch to Enable/Disable Bank Account Type
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_ACCOUNT_TYPE_ENABLED","1");

/********************
* If transaction is suspended...
* 0 means Ledgers will not be updated
* 1 means Ledgers will be updated
********************/
define("CONFIG_LEDGER_SUSPEND","0");

/********************
* This variable is used when we add sender,beneficiary,etc...
* So that instead of County, it will show State OR Province OR County
* according to Client's country.
********************/
define("CONFIG_STATE_NAME","County");

/********************
* This variable is used to enter FEE and Exchange Rate manually during create transaction
* If value is 1, then if Beneficiary Country is a/c to variable CONFIG_BEN_COUNTRY,
* then user can enter manually.
* If you want to add another country in CONFIG_BEN_COUNTRY, then you should write
* another country name(all upper) seperated by comma and spaces. For example,
* if you add PAKISTN, then define("CONFIG_BEN_COUNTRY","BRAZIL , PAKISTAN");
********************/
define("CONFIG_MANUAL_FEE_RATE","1");

define("CONFIG_BEN_COUNTRY","BRAZIL");


/********************
* Switch to Enable/Disable Filteration of records in braclays
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("FILTER_BARCLAYS_RECORDS","1");


/********************
* Reports Based on Originating Currency or Destination Currency
* 0-  Destination Currency
* 1-  Originating Currency
********************/
define("CONFIG_CURRENCY_BASED_REPORTS","1");

/********************
* New Account for Agent and Distributor
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AnD_ENABLE","1");


/********************
* Switch to Enable/Disable specific country of Canada while adding customer
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CANADIAN_COUNTRY_OPTION","1");

/********************
* Switch to Enable/Disable to manage the field names from trasaction table
* that are shown during export transactions.
* also for file format...
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TABLE_MGT", "1");

/********************
* Switch to Enable/Disable to show the link for export transactions.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TRANSACTIONS","1");

/********************
* Switch to Enable/Disable to show the link Export Templates.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_TEMPLATE", "0");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");


/********************
* Switch to Enable/Disable to show Create Transaction Option for Teller
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TELLER_CREATE_TRANS","1");


/********************
* Switch to Enable/Disable Calculating Transaction Amounts from Total Amount
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CALCULATE_FROM_TOTAL","1");

/********************
* Switch to Enable/Disable Profit Earning Report Especialy for Bayba
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PROFIT_EARNING","1");

/********************
* Switch to Enable/Disable to show Country/Currency Filter
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CNTRY_CURR_FILTER", "1");

/********************
* Switch to Enable/Disable Edit Option for Sender during create transaction
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_EDIT_SENDER", "1");

/********************
* Switch to Enable/Disable drop down of collection points instead of pop-up window
* during create transaction (if transaction type is 'Pick up')
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_COLLPOINTS_DROPDOWN", "1");

/********************
* Switch to Enable/Disable link for Reference Number Generator
* This will generate client's refrence numbers as they want.
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_REFNUM_GENERATOR", "1");

///// Sign for Success and Caution [by Jamshed]
define("SUCCESS_MARK","<img src='images/tickmark-darkgreen.png'>");
define("CAUTION_MARK","!");

///// Color scheme for Success and Caution Messages [by Jamshed]
define("SUCCESS_COLOR", "#009800");
define("CAUTION_COLOR", "#990000");



/********************
* Switch to Enable/Disable Association between
* Agent and Admin staff for transactions
* Done for Opal and need to test to apply for glink as well
*	0 Disable
*	1 Enable
********************/
define("CONFIG_ADMIN_ASSOCIATE_AGENT", "1");
define("CONFIG_ASSOCIATED_ADMIN_TYPE", "Admin,Call,Admin Manager,");



/********************
* Switch to Enable/Disable Facility
* for creation of Back Dated Transactions
********************/
define("CONFIG_BACK_DATED","1");

/********************
* Switch to Enable/Disable ID Expiry with Calendar (not drop down)
*       0 means Disable
*       1 means Enable
********************/
define("CONFIG_IDEXPIRY_WITH_CALENDAR", "1");


/********************
* Switch to fix teller's access for a transaction
* 1 Teller can view all the transactions of its Distributor
* 0 Teller is limited to its own collection point not all
* transactions linked to distributor of that collection point
* In other words, a good pickup scenario
********************/
define("CONFIG_TELLER_ACCESS","1");


/********************
* Switch to Enable/Disable edit transaction and amend tarnsaction labels
*       0 means Disable
*       1 means Enable
********************/
define("DISPLAY_TRANSACTIONS_LABEL","1");

/********************
* Switch to Enable/Disable display of last transaction (upto transactiontype and its details)
* between Customer and Beneficiary
* 0 Disable
* 1 Enable
********************/
define("CONFIG_CUSTBEN_OLD_TRANS", "1");

/********************
* Switch to Enable/Disable sender search format according to Opal
Requirement
* Two text boxes; sender name, sender number (respectively)
* 0 Disable
* 1 Enable
********************/
define("CONFIG_SENDER_SEARCH_FORMAT", "1");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
*	0 Disable
*	1 Enable
********************/
define("CONFIG_CUST_DOC_FUN", "1");

/********************
* Switch to Enable/Disable document provided(or not provided) function for customer
* plus message prompt as per requirement of richalmond.
* and works with 'CONFIG_CUST_DOC_FUN' variable.
*	0 Disable
*	1 Enable
********************/
define("CONFIG_COMPLIANCE_PROMPT", "1");
//define("CONFIG_COMPLIANCE_MIN_AMOUNT", "600");

/********************
* Enable or disbale 'send to confirm' button
* For Opal
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SEND_ONLY_BUTTON","1");

/********************
* Show sender city on create transaction page and reciept
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SHOW_CITY","1");

/********************
* Switch to Enable/Disable mobile field OFF
*	0 Disable
*	1 Enable
********************/
define("CONFIG_MOBILE_FIELD_OFF", "1");

/********************
* Switch to Enable/Disable check if id type selected from the radio button options the id number is also entered
*	0 Disable
*	1 Enable
********************/
define("CONFIG_RADIOIDTYPE_IDNUMBER_CHECK", "1");


/**********************************
* Switch to show user Reciept
* for the customer on create Transaction
***********************************/
define("CONFIG_CUSTOM_RECIEPT", "1");
define("CONFIG_RECIEPT_NAME", "confirmTransactionBaybaReciept.php");
define("CONFIG_RECIEPT_PRINT", "confirmTransactionBaybaReciept.php");
define("CONFIG_RECEIPT_FORM", "reciept_form.php");

/********************
* Switch to Enable/Disable non compulsory phone field
*	0 Disable
*	1 Enable
********************/
define("CONFIG_PHONE_NON_COMPULSORY", "1");

/********************
* Switch to Enable or Disable to Skip Confirm Transaction Step
* while creating a transaction
* For Opal
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_SKIP_CONFIRM","0");

/********************
* at edit transaction and amend transaction system generates reciept

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CUSTOM_AMEND_RECIEPT","1");


/********************
* Switch to Enable/Disable full name search filters on teller search
*	0 Disable
*	1 Enable
********************/
define("CONFIG_FULLNAME_SEARCH_FILTER", "1");



/********************
* Switch to Enable/Disable Customer AML (Anti Money Laundering) Report
*	0 means Disable
*	1 means Enable
********************/
define("CONFIG_CUST_AML_REPORT", "1");
//define("CONFIG_AMOUNT_CONTROLLER", "600");

/********************
* to make manual code compulsory  in transaction details on create transaction page

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_TRANS_MANUAL_CODE_COMP","0");



/********************
* to enable mobile number on teller report

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_BEN_MOBILE_TELLER","0");

/********************
* to enable mobile number on teller report

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CUSTOMER_REF_NUMBER_TELLER","1");



/********************
* to disable agent code on teller search transaction report for bayba

* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_AGENT_CODE","1");


/********************
* Switch to Enable/Disable to show consolidated sender registration report link in left_Express.php
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT", "1");

/********************
* Switch to Enable/Disable to show export all transactions link in left.php under reports module
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXPORT_ALL_TRANS", "1");

/********************
* Left File with Functionality
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEFT_FUNCTION","1");
define("CONFIG_LEFT_PATH","left_Express.php");

/********************
* Switch to Enable/Disable making transaction purpose non compulsory
*	0 Disable
*	1 Enable
********************/
define("CONFIG_NON_COMP_TRANS_PURPOSE", "1");


/********************
* Switch to Enale/Disable Reports for all users
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_REPORT_ALL","1");

/********************
* Switch to Enable/Disable Search Beneficiary
* and then ability to create transaction
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SEARCH_BENEFICIARY", "1");

/********************
* Switch to Enable/Disable MLRO option on add admin Type
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_ADMIN_TYPE_MLRO","1");

/********************
* Switch to Enable/Disable changes on transactions if the sender is disabled/enabled.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER","1");

/********************
* Switch to Enable/Disable to show the page for Payment Mode Report.
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_PAYMENT_MODE_REPORT_ENABLE","1");

/********************
* Switch to Enable/Disable payment mode options on add super/sub agents
*inorder to turn this config on CONFIG_EXCLUDE_COMMISSION config should be disabled

*	0 means Enable
*	1 means Disable 
********************/
define("CONFIG_AGENT_PAYMENT_MODE", "1");

/********************
* Switch to Enable/Disable calculation of sender accumulative amount and inserting into db
*       0 Disable
*       1 Enable
********************/
define("CONFIG_SENDER_ACCUMULATIVE_AMOUNT", "1");

/********************
* Switch to Enable/Disable sender ID date alert
*       0 Disable
*       1 Enable
********************/
define("CONFIG_ALERT", "1"); 
define("CONFIG_ADD_ALERT_PAGE", "AlertController.php"); 		 	
define("CONFIG_MANAGE_ALERT_PAGE", "manageAlertController.php");

/********************
* Switch to Enable/Disable Documentation verification module
*       0 Disable
*       1 Enable
********************/
define("CONFIG_DOCUMENT_VERIFICATION", "1"); 	

/********************
* Switch to Enable/Disable target page for compliance mode
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE", "1"); 

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Country of Customer Removed by Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_COUNTRY_REMOVE", "1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
* Custom Colums Added By Opal
*       0 Disable
*       1 Enable
********************/
define("CONFIG_COMPLIANCE_CUSTOM_COLUMN", "1");

/********************
* Account Summary Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_ACCOUNT_SUMMARY","1");

/********************
* Using Daily Sales Report as Agent Balance Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_SALES_AS_BALANCE","1");

/********************
* Using Opening and Closing Balance in Agent Account Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_BALANCE","1");

/********************
* Using Status and Modified By fields in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_FIELDS","0");

/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_AGENT_STATEMENT_CUMULATIVE","0");

/********************
* Using Cumulative Balances in Agent Statement
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_REVERSE_COMM","1");

/********************
* Labels for the deposit and withdraw
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_LEDGER_LABEL","1");
define("CONFIG_DEPOSIT","Credit");
define("CONFIG_WITHDRAW","Debit");

/********************
* Using Cancelled Transactions in daily transaction
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_CANCEL_DAILY_TRANS","1");

/********************
* Switch to Enable or Disable Pending/Processing Transactions in Ddaily Distributor Report
* 0-  Disable
* 1-  Enable
********************/
define("CONFIG_DISTRIBUTOR_PENDING","1");

/********************
* Reports to Disable for this client
* 
* 
********************/
define("CONFIG_DISABLE_REPORT","DIST_COMM, DAILY_AGENT, OUSTSTANDING_AGENT, RATE_EARNING");

/********************
* Transaction Surcharges in reports
* 0-  Disable
* 1-  Enables
********************/
define("CONFIG_REPORTS_SURCHARG","1");

/********************
* Switch to Enable/Disable Compliance for customer transaction
*	0 Disable
*	1 Enable 
********************/
define("CONFIG_COMPLIANCE_SENDER_TRANS", "1");

define("CONFIG_CURR_DENOMINATION", "1");


define("CONFIG_CURR_CAPTION","Denom Report");



/********************************************
* added by Niaz Ahmad against #1727 at 08-01-2008
* compliance for transactions greater than a configurable variable for the amount and number of days.
*       0 Disable
*       1 Enable
******************************************/
define("CONFIG_COMPLIANCE_PROMPT_CONFIRM", "1");

/********************
* Switch to Enable/Disable Exchange Rate used for the a particular
transaction
* I should not be edited once transaction is created, Developed for
connect plus
* If value is '0' its Disabled
* If value is '1' its Enabled
********************/
define("CONFIG_EXCH_RATE_NOT_EDIT","1");


/********************                                                                         
* Switch to Enable/Disable link "export customer mobile number"
* Done for Express                                                 
*	0 Disable                                                                                   
*	1 Enable                                                                                    
********************/                                                                         
define("CONFIG_CUSTOMER_MOBILE_NUMBER", "1");  
define("CLIENT_NAME", "bayba");

?>
