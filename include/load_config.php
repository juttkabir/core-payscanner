<?php

function log_exit2($message, $line, $file) 
{ 
    echo "<font color=red><u>Error:</u></font><br>" . $message .  " on line: ". $line. " in ". $file ; 
    exit; 
} 
 	 
function dbConnect2($SERVER=SERVER_MASTER) 
{
//echo "<pre>";
//debug_print_backtrace(); 
//echo "</pre>";
    if ((@$db=mysql_connect($SERVER, USER, PASSWORD)) == FALSE) 
        log_exit2( "<b>Sorry:</b> Could not connect to MySQL Database server $SERVER ", __LINE__, __FILE__); 
 		 
    if(!mysql_select_db(DATABASE, $db)) 
        log_exit2( "<b>Sorry:</b> Could not connect to database DATABASE", __LINE__, __FILE__); 
 		   
    return $db; 
} 
 		 
function dbClose2($db) 
{ 
    mysql_close($db); 
} 
 		 
$conn = dbConnect2(SERVER_MASTER); 
 		 
$query = "SELECT `varName`, `value` FROM `config`";
$config_res = mysql_query($query) or die("Query Error: " . $query . "<br>" . mysql_error()); 
while ($confRow = mysql_fetch_array($config_res)) { 
    define($confRow['varName'], $confRow['value']); 
} 
 		 
dbClose2($conn); 
?>
