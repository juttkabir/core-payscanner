<?
define("USER","payex");
define("PASSWORD","payex12");
define("DATABASE","payex");

define("IMG_PATH_EDITOR","images/Images/");
// Others
define("ADD_ATTRIBUTES", 10);
define("PASSWORD_CHANGE", 30);
define("SUPPORT_EMAIL", "noreply@horivert.co.uk");
define("SUPPORT_NAME", "Company  Support");

//******************** TABLE NAMES *************
define("TBL_CM_CUSTOMER", DATABASE. ".cm_customer");
define("TBL_BENEFICIARY", DATABASE. ".cm_beneficiary");
define("TBL_CONTACTS", DATABASE. ".contacts");
define("TBL_FAQS", DATABASE. ".faqs");
define("TBL_CUSTOMER", DATABASE. ".cm_customer");
define("TBL_EXCHANGE_RATES", DATABASE. ".exchangerate");
define("TBL_IMFEE", DATABASE. ".imfee");
define("TBL_TRANSACTIONS", DATABASE. ".transactions");
define("TBL_USER_GROUPS", DATABASE. ".usergroups");
define("TBL_USERS", DATABASE. ".users"); 
define("TBL_SESSIONS", DATABASE.".sessions");
define("TBL_CITIES", DATABASE.".cities");
define("TBL_BANK_DETAILS", DATABASE.".cm_bankdetails");
// Complaints
define("TBL_COMPLAINTS", DATABASE.".complaints");
define("TBL_COMPLAINTS_INTER", DATABASE.".complaintInteracts");

define("TITLE","::.Payex.:: Best Money Transfer Solution....");
define("TITLE_ADMIN", "::...Payex Admin...::");

// Debuggers On and Off Parameters
define("DEBUG","True");
define("LOG","False");

// Pictues Size definition
//define ("PIC_SIZE","1000000");

// URL definition parameters
define("URL" , dirname("http://" . $_SERVER['SERVER_NAME']  . $_SERVER['REQUEST_URI'] ) . "/");

// LIVE URL
define("LURL", "http://www.horivert.co.uk/");

// ERRORS and MESSAGES
define("ERR1","Please log in to access this page.");
define("ERR4","You must upload a JPG, GIF, or PNG file!");
define("ERR19","Invalid username or password.");

define("CUS1","Please some other Name, User with this name already Exist");
// Agents
define("AG1","Please provide company name.");
define("AG2","Please provide supper contact person name.");
define("AG3","Please provide supper agents company address line 1.");
define("AG4","Please select supper agent's country.");
define("AG5","Please select supper agent's city.");
define("AG6","Please provide supper agent's zip/postal code.");
define("AG7","Please provide supper agent's phone number.");
define("AG8","Please provide supper agent's email address.");
define("AG9","Please provide supper agent company's director name.");
define("AG10","Please provide supper agent's bank name.");
define("AG11","Please provide supper agent's account name.");
define("AG12","Please provide supper agent's account number.");
define("AG13","Please provide supper agent's currency.");
define("AG14","Please provide supper agent's account limit.");
define("AG15","Please provide supper agent's commission percentage.");
define("AG16","Supper Agent added successfully in database.");
define("AG17","Supper Agent updated successfully in database.");
define("AG18","Supper Agent suspended successfully in database.");
define("AG19","Selected SupperAgent is activated successfully.");
define("AG20","Supper Agent disabled successfully in database.");
define("AG21","Please select supper agent from the list.");
define("AG22","Sub Agent added successfully in database.");
define("AG23","Sub Agent updated successfully in database.");
define("AG24","Please provide Login Name for the Admin.");
define("AG25","Please provide Password for the Admin.");
define("AG26","Please provide Confirm Password for the Admin.");
define("AG27","Both Password fields must poses the same value.");
define("AG28","Please assign at least one right to the Admin.");
define("AG29","Please provide email address of the Admin.");
define("AG30","Admin User created successfully.");
define("AG31","Admin User updated successfully.");
define("AG32","Admin with this login already exists. Please provide some other login.");
// Customers
define("CU1","Please select agent from the list to add customer againt.");
define("CU2","Please provide customer's first name.");
define("CU3","Please provide customer's last name.");
define("CU4","Please provide customer's ID number.");
define("CU5","Please provide customer's address line one.");
define("CU6","Please select customer's country.");
define("CU7","Please provide customer's city.");
define("CU8","Please provide customer's phone number.");
define("CU9","Please provide customer's email.");
define("CU10","Customer Added successfully.");
define("CU11","Customer updated successfully.");
// Benificiary
define("BE1","Please provide Benificiary's first name.");
define("BE2","Please provide Benificiary's last name.");
define("BE3","Please provide Benificiary's ID number.");
define("BE4","Please provide Benificiary's address line one.");
define("BE5","Please select Benificiary's country.");
define("BE6","Please provide Benificiary's city.");
define("BE7","Please provide Benificiary's phone number.");
define("BE8","Please provide Benificiary's email.");
define("BE9","Benificiary Added successfully.");
define("BE10","Benificiary updated successfully.");

// IM Fee
define("IMF1", "Please select Originating Country.");
define("IMF2", "Please provide Lower range of money.");
define("IMF3", "Please provide Upper range of money.");
define("IMF4", "Please provide valid value for Upper range that must be greater than Lower range.");
define("IMF5", "Please provide IM Fee for specified range of money transfer.");
define("IMF6", "IM Fee for selected country and specified range of amount has been added successfully.");
define("IMF7", "IM Fee for selected country and specified range of amount has been updated successfully.");
define("IMF8", "Please select destination Country.");
// Exchange Rates
define("ER1", "Please select Country A.");
define("ER2", "Please select Country B.");
define("ER3", "Please select different country than Country A.");
define("ER4", "Please provide primary exchange rate.");
define("ER5", "Please provide margin exchange rate.");
define("ER6", "Please provide net exchange rate.");
define("ER7", "Exchange rate added successfully.");
define("ER8", "Exchange rate updated successfully.");

// General Errors and messages
define("MSG1", "Please provide either your login name or email to retreive your password.");

// NEWS/Evnets
define("NE1", "Please enter title of news/event.");
define("NE2", "Please enter description of news/event.");
define("NE4", "News/Event added successfully.");
define("NE5", "News/Event updated successfully.");
define("NE6", "News/Event(s) removed successfully.");

// FAQs
define("FA1", "Please select FAQ type.");
define("FA2", "Please enter question.");
define("FA3", "Please enter answer.");
define("FA4", "FAQ added successfully.");
define("FA5", "FAQ updated successfully.");
define("FA6", "FAQ(s) removed successfully.");

// Promo Rate Plans
define("PR1", "Please select country.");
define("PR2", "Please provide country code.");
define("PR3", "Please provide city or mobilre code.");
define("PR4", "Promo Rate Plan added successfully.");
define("PR5", "Promo Rate Plan updated successfully.");
define("PR6", "Promo Rate Plan(s) removed successfully.");
define("PR7", "Please provide country name.");

// Contact valisdatin Errors
define("CT1", "Please enter the name of the office.");
define("CT2", "Please enter the address of the office.");
define("CT3", "Please enter a valid email address.");
define("CT4", "Contact added successfully.");
define("CT5", "Selected Contacts removed successfully.");
define("CT6", "Selected Contact updated successfully.");

// Product Validation ERRORS
define("PE1", "Product category added successfully.");
define("PE2", "Please enter category master ID.");
define("PE3", "Please enter category name.");
define("PE4", "Please select product category.");
define("PE5", "Please enter product name.");
define("PE6", "Please enter product code.");
define("PE7", "Product added successfully.");
define("PE8", "Category updated successfully.");
define("PE9", "Product updated successfully.");
define("PE10", "Product Attributes added successfully.");
define("PE11", "Product Attributes updated successfully.");
define("PE12", "Product Rate Plan added successfully.");
define("PE13", "Product Rate Plan updated successfully.");
define("PE14", "Please enter title of product rate plan.");
define("PE15", "Please enter name of product rate plan.");
define("PE16", "Product Rate Plan updated successfully.");
define("PE17", "Product Rate Plan removed successfully.");
define("PE18", "Please enter tour title.");
define("PE19", "Product tour add successfully.");
define("PE20", "Product tour updated successfully.");
define("PE21", "Product tour(s) removed successfully.");
// Events Handling Messages
define("EVE1","Please enter a valid Title.");

// TRANSACTIONS ERROR MESSAGES
define("TE1","Please enter a valid amount.");
define("TE2","The IM FEE range for amount you entered is unavailable.");
define("TE3","Please select transaction type.");
define("TE4","Please select any cutomer/sender to send money.");
define("TE5","Please select agent, where benificiary will collect the funds.");
define("TE6","Please select benificiary from the list.");
define("TE7","Invalid reference number.");
define("TE8","Please write amount to transfer.");
define("TE9","Amount to transfer is not valid.");
define("TE10","Please select transaction purpose.");
define("TE11","Please select fund sources.");
define("TE12","Please select money paid in the form of.");
define("TE13","You must accept the money transfer declaration statement at the bottom of form.");
define("TE14","You must Provide your Documents to do this transaction. Your Limit Without Depositing Documents is 2000");

define("TE14","Please provide the Bank name of Beneficiary.");
define("TE15","Please provide the Branch Code.");
define("TE16","Please provide the Branch Address.");
define("TE17","Please provide the Swift Code.");
define("TE18","Please provie the benificiary's bank account number.");
define("TE19","Please provide ABA Number.");
define("TE20","Please provide CPF Number.");
define("TE21","Please provide IBAN Number.");
define("TE22","Home Delivery Service is not Available for beneficiary city. Please change city or transaction type.");

define("CR1","Please provide Card Type.");
define("CR2","Please provide Card No.");
define("CR3","Please provide Expiry Date.");
define("CR4","Please provide Card CVV or CV2.");
define("CR5","Please provide First Name in Credit Card Information Form.");
define("CR6","Please provide Last Name in Credit Card Information Form.");
define("CR7","Please provide Address1 in Credit Card Information Form.");
define("CR8","Please provide Address2 in Credit Card Information Form.");
define("CR9","Please provide City Name in Credit Card Information Form.");
define("CR10","Please provide State Name in Credit Card Information Form.");
define("CR11","Please provide Postcode in Credit Card Information Form.");
define("CR12","Please provide Country in Credit Card Information Form.");

define("PIN1","Please provide Valid PIN Code No.");

?>
