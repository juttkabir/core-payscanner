<?php
//no direct access
/*
if(!isset($_SERVER['HTTP_REFERER']))
	die("No direct access.");
*/		
	
	
class PayexLocal {

	static $_lang = NULL;
	var $_lang_file = NULL;
	var $_strings = array();
	var $_debug = false;
	var $_localDir = NULL;
	var $_rawData = NULL;
	var $_ext = NULL;
	
	function __construct($selectedLanguage = "")
	{
		$this->_localDir = $_SERVER['DOCUMENT_ROOT']."/localization/resource/";
		/*
		if(defined("DEBUG_ON"))
			$this->_debug = true;
		*/	
		$this->_ext = ".po";
		
		$this->changeLang($selectedLanguage);
	}
	
	function getCurrentLang()
	{
		return $this->_lang;
	}
	
	function changeLang($selectedLanguage)
	{
		//debug_print_backtrace();
		if(!empty($selectedLanguage))
		{
			//session_register($_SESSION["lang"]);
			$this->_lang = $selectedLanguage;
			
			$_SESSION["lang"] = $selectedLanguage;
			
			$this->getLanguageFile($this->_lang);
			$this->_parse();
			
			return true;

		}else{
			
			if(isset($_SESSION["lang"]))
			{
				$this->_lang = $_SESSION["lang"];
				$this->getLanguageFile($this->_lang);
				$this->_parse();
				
				return true;
			}
		}
	}
	
	function getLanguageFile($lang)
	{
		//debug_print_backtrace();
		
		$strFilePath = $this->_localDir.$lang.$this->_ext;
		//debug($strFilePath);
		if(!$this->fileFound($strFilePath))
			return false;
		
		$pt = fopen($strFilePath, "r");
		
		if(!$pt)
			return false;

		$data = "";
		stream_set_blocking( $pt, true );
		while( $buf = fread($pt,4096) ){
			$data .= $buf;
		}
		fclose($pt);

		$this->_rawData = $data;
		//debug($data);
		return true;
		
	}
	
	function fileFound($strFile)
	{
		if(is_file($strFile))
			return true;
		else
			return false;
	}
	
	function _parse()
	{
		$toDorawData = $this->_rawData;
		$arrLines = explode("\r\n", $toDorawData);
	
		$arrParsedValuePare = array();
		
		$i = 0;
		foreach($arrLines as $lineNo => $lineData)
		{
			$lineData = trim($lineData);
			// TODO: Check for varios new comments style
			$chrFirst = substr($lineData, 0, 1);
			
			
			
			if($chrFirst == "#" || empty($chrFirst))
				continue;
		
			if(substr($lineData, 0, strlen("msgid")) == "msgid")
			{
				//debug($lineData);
				$secondLine = trim($arrLines[$lineNo+1]);
				$str = preg_match('/\"[^*]+\"$/', $lineData, $matchesStr);
				$msg = preg_match('/\"[^*]+\"$/', $secondLine, $matchesMsg);
				
				$str = substr($matchesStr[0], 1, -1);
				$msg = substr($matchesMsg[0], 1, -1);

				//debug("[".$str ."] = ".$msg);
				$arrParsedValuePare[$str] = $msg;
			}
			
			$i++;
		}
		$this->_strings = $arrParsedValuePare;
	}
	
	
	function _($string, $jsSafe = false)
	{
		//$key = strtoupper($string);
		$key = trim($string);
		$key = substr($key, 0, 1) == '_' ? substr($key, 1) : $key;

		if (isset ($this->_strings[$key]))
		{
			$string = $this->_debug ? "&bull;".$this->_strings[$key]."&bull;" : $this->_strings[$key];

		}
		else
		{
			if (defined($string))
				$string = $this->_debug ? '!!'.constant($string).'!!' : constant($string);
		}

		
		/* if do not find the combine string as key than try to find as seperate with SPACE
		if($key == $string)
		{
			$arrKeys = explode(" ", $key);
			
			foreach($arrKeys as $k)
			{
				
			}
		}
		*/
		if ($jsSafe) {
			$string = addslashes($string);
		}

		return $string;
	}

	function sprintf($string)
	{
		$args = func_get_args();
		if (count($args) > 0) {
			$args[0] = $lang->_($args[0]);
			return call_user_func_array('sprintf', $args);
		}
		return '';
	}

	function printf($string)
	{
		$args = func_get_args();
		if (count($args) > 0) {
			$args[0] = $lang->_($args[0]);
			return call_user_func_array('printf', $args);
		}
		return '';
	}
	
}
