<?php

require_once 'masterConfig.php';
require_once 'DB/DataObject.php';

$conf_file = 'dbconf.txt';
$handle = fopen($conf_file, 'w');

fwrite($handle,"[DB_DataObject]\n\n");

fwrite($handle,"database        = $DSN\n");
fwrite($handle,"schema_location = {$APP_ROOT}schema/DataObjects\n");
fwrite($handle,"class_location  = {$APP_ROOT}include/DataObjects\n");
fwrite($handle,"require_prefix  = DataObjects/\n");
fwrite($handle,"class_prefix    = DataObjects_\n");
fwrite($handle,"debug           = 1\n");

fclose($handle);

$_SERVER['argv'][1] = $conf_file;
require_once("DB/DataObject/createTables.php");
?>
