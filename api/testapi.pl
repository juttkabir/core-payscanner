

use strict;
use warnings;

use LWP;
use HTTP::Request;
use HTTP::Cookies;
use HTTP::Headers;
use URI;
use Data::Dumper;



my $send_xml = <<"END_XML";
<?xml version="1.0" encoding="utf-8"?>
<telegiros_payex>
  <transaction ref_id="OPC0024333">
    <customer_agent_id>2001</customer_agent_id>
    <date>2006-11-22</date>
    <ref_number>53662</ref_number>
    <ref_number_payex>BA1707000007</ref_number_payex>
    <type>banktransfer</type>
    <status>pending</status>
    <funds>
      <amount>300.12</amount>
      <exchange_rate>122.90</exchange_rate>
      <agent_fee>5.10</agent_fee>
      <total_amount>305.22</total_amount>
      <currency_to>GMD</currency_to>
      <amount_to>36884.75</amount_to>
    </funds>
    <comments>Sample Transaction</comments>
    <sender>
      <firstname>Carles</firstname>
      <lastname>Baguda</lastname>
      <authentication>
        <document_type>Passport</document_type>
        <document_id>SE-12221</document_id>
      </authentication>
    </sender>
    <beneficiary>
      <firstname>Suman</firstname>
      <lastname>Goha</lastname>
      <address>
        <line_1>15 The Street</line_1>
        <line_2></line_2>
        <line_3></line_3>
        <city>Banjul</city>
        <state>Banjul</state>
        <postcode></postcode>
        <country>Gambia</country>
        <phone>012345678</phone>
        <bank_details>
          <bank_name>Big Bank</bank_name>
          <branch_code>2322</branch_code>
          <account>
            <type>current</type>
            <number>2300021</number>
          </account>
        </bank_details>
      </address>
    </beneficiary>
  </transaction>
</telegiros_payex>
END_XML


my $url = 'http://test-api.test.horivert.com/api/telegiros.php';
my ($response, $body) = makePostRequest($url, $send_xml);



print "####\n\n".Dumper($response)."\n\n###\n";
print "###$body###\n";


#############################################

sub makeRequest {
    if (wantarray) {
        my @ret = _makeRequest('GET', @_);
        return @ret;
    }
    return _makeRequest('GET', @_);
}

sub makeGetRequest {
    if (wantarray) {
        my @ret = _makeRequest('GET', @_);
        return @ret;
    }
    return _makeRequest('GET', @_);
}

sub makePostRequest {
	my ($url, $body) = @_;

	my $u = URI->new( $url );

	my $h = HTTP::Headers->new;
	$h->header('Content-Type' => 'application/x-www-form-urlencoded');

	my $request = HTTP::Request->new( 'POST', $url, $h, $body);

	my $lwp = LWP::UserAgent->new(
		requests_redirectable   => [],
	);
	my $response = $lwp->request( $request );
	my $content = $response->content();
	return wantarray ? ($response, $content) : $content;
}

sub _makeRequest {
	my ($method, $url, $body) = @_;

	my $u = URI->new( $url );

	my $h = HTTP::Headers->new;
	$h->header('Content-Type' => 'text/xml');

	my $request = HTTP::Request->new( $method, $url, $h, $body);

	my $lwp = LWP::UserAgent->new(
		requests_redirectable   => [],
	);
	my $response = $lwp->request( $request );
	my $content = $response->content();
	return wantarray ? ($response, $content) : $content;
}
