<?php
/**
 * @pacakage API Address Look UP
 * short description
 * This page returns the addressed against the post codes
 * @author Mirza Arslan Baig
 */
//i_set("display_errors","on");
//ror_reporting(E_ALL);
//echo 'there';exit();

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

if ( isset($_POST['postcode'])) {
	
include_once("__user.php");

//Address Lookup
$objParam ->address = new stdClass();
//$country = $_POST['country'];

$postcode = trim($_POST['postcode']);
$buildingNumber = trim($_POST['buildingNumber']);
$street = trim($_POST['street']);
$town = trim($_POST['town']);
if(!empty($postcode))
	$objParam ->address->Postcode = $postcode;
else
	die('<i style="color:#FF3A3A">Please Enter Post Code to Search the Address.</i>');
if(!empty($buildingNumber))
	$objParam ->address->BuildingNo = $buildingNumber;
if(!empty($street))
	$objParam ->address->Street = $street;
if(!empty($town))
	$objParam ->address->Town = $town;
/*	$arrAddressMapping = array(
		'Postcode' => array('United Kingdom'),
        'BuildingName' => array('United Kingdom'),
        'BuildingNo' => array('United Kingdom'),
        'SubBuilding' => array('United Kingdom'),
        'Organisation' => array('United Kingdom'),
        'Street' => array('United Kingdom','USA'),
        'SubStreet' => array('United Kingdom'),
        'Town' => array('United Kingdom'),
        'District' => array('United Kingdom'),
        'Building1' => 
          <Building2>string</Building2>
          <Building3>string</Building3>
          <Building4>string</Building4>
          <ZipPCode>string</ZipPCode>
          <Street>string</Street>
          <CityTown>string</CityTown>
          <StateDistrict>string</StateDistrict>
	);*/

// PILOT PLATFORM URL
//$soap = new SoapClient(WSDL_TEST_URL, array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));


// Live Platform Url
//$soap = new SoapClient(WSDL_LIVE_URL, array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
//echo WSDL_LIVE_URL;
//exit();
$soap = new SoapClient(WSDL_LIVE_URL,array("trace"=>true));

if (is_soap_fault($soap)) 
{
	throw new Exception(" {$soap->faultcode}: {$soap->faultstring} ");
}
try {
		//if($country == 'United Kingdom')
			$objRet = $soap->AddressLookupUK($objParam);
		/* elseif($country == 'USA')
			$objRet = $soap->AddressLookupUS($objParam); */
			
		print "<select name='tx_addresslist' id='tx_addresslist' size='5'  onchange='getAddressData(this);' style='overflow:scroll;max-height:80px;padding:2px;height:auto;'>";
		   foreach($objRet->AddressLookupUKResult->URUAddressFixed as $message)
		   {
				/*print "<option value=\"
				".($message->BuildingNo).'&nbsp;'.($message->BuildingName).'&nbsp;'.($message->SubBuilding).'&nbsp;'.($message->Street).'&nbsp;'.($message->SubStreet).'&nbsp;'.($message->Town).'&nbsp;'.($message->District).'&nbsp;'.($message->Postcode).'&nbsp;'.($message->Organisation)."\">*/
				
				if(!((empty($message->BuildingNo) && empty($message->BuildingName) && empty($message->SubBuilding)) && (empty($message->Street) && empty($message->SubStreet)) && empty($message->Town) && empty($message->District) && empty($message->Postcode) && empty($message->Organisation))){
					if($message->BuildingNo != '')
						$value = $message->BuildingNo;
					if($message->BuildingName != '')
						$value .= '/'.$message->BuildingName;
					else
						$value .= ' /';
					if($message->SubBuilding != '')
						$value .= '/'.$message->SubBuilding;
					else
						$value .= ' /';
					if($message->Street != '')
						$value .= '/'.$message->Street;
					else
						$value .= ' /';
					if($message->SubStreet)
						$value .= '/'.$message->SubStreet;
					else
						$value .= ' /';
					//if($country == 'United Kingdom'){
						if($message->Town != '')
							$value .= '/'.$message->Town;
						else
							$value .= ' /';
					/* }elseif($country == 'USA'){
						if($message->CityTown != '')
							$value .= $message->CityTown;
					} */
						
					$value .= '/'.$message->Postcode;
					if($message->Organisation != '')
						$value .= '/'.$message->Organisation;
					else
						$value .= ' ';
					print "<option value=\"".$value."\">".($message->BuildingNo).'&nbsp;'.($message->BuildingName).'&nbsp;'.($message->SubBuilding).'&nbsp;'.($message->Street).'&nbsp;'.($message->Town)."</option>";
				}
			}
		print "</select>"; 
	
//	foreach ($objRet->AddressLookupUKResult->URUAddressFixed as $message) { 
//	echo '<pre>';
//	print" ".($message->Postcode)." ";
//	print" ".($message->BuildingNo)." ";
//	print" ".($message->Street)."";
//	print" ".($message->Town)."";
//	echo '</pre>';
//}
}
catch (Exception $e) {
	//var_dump($objRet);
	//	exit('PHPcode check failed');
	echo $e->getMessage();
}
/*
if (is_soap_fault($objRet)) {
	throw new Expception(" {$objRet->faultcode}: {$objRet->faultstring} ");
}
*/
}
 ?>
