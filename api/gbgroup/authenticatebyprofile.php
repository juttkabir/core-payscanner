<?PHP
ini_set("display_errors","1");
error_reporting(1);
include_once("__user.php");
//$objParam ->ProfileId			= "c088fc62-34c7-4317-ba25-c1a332f8493f";
$objParam ->ProfileId			= "2d7b8dc3-bd08-4b5b-8364-5fd5a8c0d16a";
$objParam ->CustomerRef 		= "your own reference";
//$objParam ->IPAddress 		= "";

//Personal Details
$objParam ->UserData = new stdClass();
$objParam ->UserData->Basic->Title 				= "mr";				// required 
$objParam ->UserData->Basic->Forename 			= "ali";				// required
$objParam ->UserData->Basic->Surname 			= "mohammad";				// required
$objParam ->UserData->Basic->MiddleName			= "";				// 
$objParam ->UserData->Basic->Gender 			= "Male";				// optional / required for driving licence check
$dob = "1989-07-01";
//$arrDob = explode($_POST['dob'],"-");
$arrDob = explode("-", $dob);
$objParam ->UserData->Basic->DOBDay 			= $arrDob[2];				// optional / required for driving licence check
$objParam ->UserData->Basic->DOBMonth 			= $arrDob[1];				// optional / required for driving licence check
$objParam ->UserData->Basic->DOBYear 			= $arrDob[0];				// optional / required for driving licence check


//Address #1
$objParam ->UserData->UKData = new stdClass();
$objParam ->UserData->UKData->Address1 = new stdClass();
$objParam ->UserData->UKData->Address1->FixedFormat = new stdClass();
$objParam ->UserData->UKData->Address1->FixedFormat->Postcode 		= "W11 2BQ";				// required
$objParam ->UserData->UKData->Address1->FixedFormat->BuildingName 	= "";				// required / optional if BuildingNo supplied
$objParam ->UserData->UKData->Address1->FixedFormat->BuildingNo 	= "14";				// required / optional if BuildingName supplied
$objParam ->UserData->UKData->Address1->FixedFormat->SubBuilding 	= "";				// optional
$objParam ->UserData->UKData->Address1->FixedFormat->Organisation 	= "";				// optional
$objParam ->UserData->UKData->Address1->FixedFormat->Street 		= "";				// required
$objParam ->UserData->UKData->Address1->FixedFormat->SubStreet 		= "";				// optional
$objParam ->UserData->UKData->Address1->FixedFormat->Town 			= "";				// optional
$objParam ->UserData->UKData->Address1->FixedFormat->District 		= "";				// optional

/*
// UK Passport
$objParam ->UserData->UKData->Passport = new stdClass();
$objParam ->UserData->UKData->Passport->Number1 = "123456789";
$objParam ->UserData->UKData->Passport->Number2 = "GBR";
$objParam ->UserData->UKData->Passport->Number3 = "19890701";
$objParam ->UserData->UKData->Passport->Number4 = "M";
$objParam ->UserData->UKData->Passport->Number5 = "0810050";
$objParam ->UserData->UKData->Passport->Number6 = "12";
//$arrExpiryDate = explode($_POST['passportExpiry'],"-");
$expiry = "2013-07-01";
$arrExpiryDate = explode("-",$expiry);
$objParam ->UserData->UKData->Passport->ExpiryDay = $arrExpiryDate[2];
$objParam ->UserData->UKData->Passport->ExpiryMonth = $arrExpiryDate[1];
$objParam ->UserData->UKData->Passport->ExpiryYear = $arrExpiryDate[0];

// 
*/
//Drivers Licence
// $objParam ->UserData->UKData->Driver = new stdClass();
// $objParam ->UserData->UKData->Driver->Number1						= "MARR9";			//	5 digits
// $objParam ->UserData->UKData->Driver->Number2						= "608232";			//	6 digits
// $objParam ->UserData->UKData->Driver->Number3						= "HB9";			//	3 digits
// $objParam ->UserData->UKData->Driver->Number4						= "00";				//	2 digits
// $objParam ->UserData->UKData->Driver->Postcode						= "S61 2NL";		//	postcode capture from Address#1
//$objParam ->UserData->UKData->Driver->Microfiche					= "";				//	14 digits


//Landline Telephone
// $objParam ->UserData->UKData->Telephone = new stdClass();
// $objParam ->UserData->UKData->Telephone->ExDirectory 				= "TRUE";			// Set to TRUE if no number is supplied
// $objParam ->UserData->UKData->Telephone->Number 					= "";				// optional can be blank if ExDirectory Flag set to True


//InternationalPassport
$objParam ->UserData->InternationalPassport = new stdClass();
$objParam ->UserData->InternationalPassport->Number1				= "012345678";				// 9	digits
$objParam ->UserData->InternationalPassport->Number2				= "1";				// 1	digits
$objParam ->UserData->InternationalPassport->Number3				= "GBR";				// 3	digits
$objParam ->UserData->InternationalPassport->Number4				= "5011025";				// 7	digits
$objParam ->UserData->InternationalPassport->Number5				= "M";				// 1	digits
$objParam ->UserData->InternationalPassport->Number6				= "0810050";		// 7	digits
$objParam ->UserData->InternationalPassport->Number7				= "<<<<<<<<<<<<<<";	// 14	digits
$objParam ->UserData->InternationalPassport->Number8				= "2";				// 1	digits
$objParam ->UserData->InternationalPassport->Number9				= "9";				// 1	digits
$objParam ->UserData->InternationalPassport->ExpiryDay				= "11";				// 2	digits - optional but recommended for enhanced matching
$objParam ->UserData->InternationalPassport->ExpiryMonth			= "11";				// 2	digits - optional but recommended for enhanced matching
$objParam ->UserData->InternationalPassport->ExpiryYear				= "2015";				// 4	digits - optional but recommended for enhanced matching
$objParam ->UserData->InternationalPassport->CountryOfOrigin		= "";				// GetPassportCountries  methosd - optional but recommended for enhanced matching 



//LIVE PLATFORM URL	
//$soap = new SoapClient("https://www.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
echo "<pre>";
print_r($objParam);
echo "</pre>";
//PILOT PLATFORM URL
$soap = new SoapClient("https://pilot.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl");

if (is_soap_fault($soap)) 
	{
	throw new Exception(" {$soap->faultcode}: {$soap->faultstring} ");
	}
try {
	$objRet = $soap->AuthenticateByProfile($objParam);
	echo '<pre>';
	print"Authentication ID : ".($objRet->AuthenticateByProfileResult->AuthenticationId)."<br>";
	print"Score             : ".($objRet->AuthenticateByProfileResult->Score)."<br>";
	print"Band Text         : ".($objRet->AuthenticateByProfileResult->BandText)."<br>";
	echo '</pre>';
//	exit('PHPcode check ok');
}
catch (Exception $e) {
	var_dump($ibjRet);
//	exit('PHPcode check failed');
	throw new Exception($e->getMessage());
}
if (is_soap_fault($objRet)) {
	throw new Expception(" {$objRet->faultcode}: {$objRet->faultstring} ");
}

?>
