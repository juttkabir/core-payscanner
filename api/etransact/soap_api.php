<?php

$_arreTransactErrorCodes = array(
/* CASE 1 */
	"00" => "Approved or completed successfully",
	"01" => "Refer to card issuer",
	"02" => "Refer to card issuer, special condition",
	"03" => "Invalid merchant",
	"04" => "Pick-up card",
	"05" => "Do not honor",
	"06" => "Error",
	"07" => "Pick-up card, special condition",
	"08" => "Honor with identification",
	"09" => "Request in progress",
	"10" => "Approved, partial",
	"11" => "Approved, VIP",
	"12" => "Invalid transaction",
	"13" => "Invalid amount",
	"14" => "Invalid card number",
	"15" => "No such issuer",
	"16" => "Approved, update track 3",
	"17" => "Customer cancellation",
	"18" => "Customer dispute",
	"19" => "Re-enter transaction",
	"20" => "Invalid response",
	"21" => "No action taken",
	"22" => "Suspected malfunction",
	"23" => "Unacceptable transaction fee",
	"24" => "File update not supported",
	"25" => "Unable to locate record",
	"26" => "Duplicate record",
	"27" => "File update edit error",
	"28" => "File update file locked",
	"29" => "File update failed",
	"30" => "Format error",
	"31" => "Bank not supported",
	"32" => "Completed partially",
	"33" => "Expired card, pick-up",
	"34" => "Suspected fraud, pick-up",
	"35" => "Contact acquirer, pick-up",
	"36" => "Restricted card, pick-up",
	"37" => "Call acquirer security, pick-up",
	"38" => "PIN tries exceeded, pick-up",
	"39" => "No credit account",
	"40" => "Function not supported",
	"41" => "Lost card",
	"42" => "No universal account",
	"43" => "Stolen card",
	"44" => "No investment account",
	"51" => "Not sufficient funds",
	"52" => "No check account",
	"53" => "No savings account",
	"54" => "Expired card",
	"55" => "Incorrect PIN",
	"56" => "No card record",
	"57" => "Transaction not permitted to cardholder",
	"58" => "Transaction not permitted on terminal",
	"59" => "Suspected fraud",
	"60" => "Contact acquirer",
	"61" => "Exceeds withdrawal limit",
	"62" => "Restricted card",
	"63" => "Security violation",
	"64" => "Original amount incorrect",
	"65" => "Exceeds withdrawal frequency",
	"66" => "Call acquirer security",
	"67" => "Hard capture",
	"68" => "Response received too late",
	"75" => "PIN tries exceeded",
	"77" => "Intervene, bank approval required ",
	"78" => "Intervene, bank approval required for partial amount",
	"90" => "Cut-off in progress",
	"91" => "Issuer or switch inoperative",
	"92" => "Routing error",
	"93" => "Violation of law",
	"94" => "Duplicate transaction",
	"95" => "Reconcile error",
	"96" => "System malfunction",
	"98" => "Exceeds cash limit",
	
/* CASE 2 */
	"000" => "Financial transaction has been approved",
	"114" => "Invalid account number",
	"115" => "Requested function not supported (First two digits of processing code or Function code is invalid)",
	"116" => "Insufficient funds",
	"119" => "Transaction not permitted to card holder",
	"121" => "Withdrawal amount limit exceeded.",
	"180" => "Transfer Limit Exceeded",
	"182" => "Not all Cheques could be stopped",
	"183" => "Cheque not issued to this account",
	"800" => "Network message was accepted",
	"902" => "Invalid transaction (Invalid function code within network management messages)",
	"904" => "Format Error (Any format related errors etc)",
	"906" => "Cut-over in progress (When DC is generating PBF, it will give this result code)",
	"907" => "Card issuer inoperative (Sent when BancsConnect cannot contact DC and PBF holder or DC has logged off or it is in fallback mode and financial transactions are not allowed)",
	"909" => "System malfunction (Sent by BancsConnect or DC for errors like database corrupted)",
	"911" => "Card issuer timed out (BancsConnect contacts DC and DC times out or BancsConnect itself times out",
	"913" => "Duplicate transaction Id"
);



function sendApiTransaction($intTransactionId)
{
	//debug_print_backtrace();
	if(!empty($intTransactionId))
	{
		
		$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
		if(!mysql_select_db(MTDB, $mtlink))
			die("Could not connect to ".MTDB." database.");

		$payex = mysql_connect(SERVER_MASTER, USER, PASSWORD);
		if(!mysql_select_db(DATABASE, $payex))
			die("Could not connect to ".DATABASE." database.");

		$strDistributorSql = "select 
								c.associate_distributor, 
								a.associated_id 
							from 
								client as c 
							LEFT JOIN api_services as a 
								ON c.payex_client_id = a.client_id 
							where 
								payex_client_id = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'
								and a.service_type = 'D'
								";
		
		$arrDistributorId = selectSql($strDistributorSql, $mtlink);
		
		$intDistributorId = trim($arrDistributorId["associated_id"]);
		if(empty($intDistributorId))
			$intDistributorId = trim($arrDistributorId["associate_distributor"]);
			
		
		$strSql = "select 
						t.transid,
						t.transAmount, 
						t.localAmount, 
						t.currencyTo, 
						t.toCountry, 
						t.refNumber, 
						t.refNumberIM, 
						t.tip,
						t.transType,
						ben.title,
						ben.firstName, 
						ben.lastName, 
						ben.middleName,
						ben.Address,
						ben.City,
						ben.Country,
						ben.phone,
						ben.email,
						tbd.bankName,
						tbd.accNo,
						tbd.branchCode,
						tbd.swiftCode,
						cur.numCode
					from 
						transactions as t
						LEFT JOIN beneficiary as ben ON t.benID = ben.benID
						LEFT JOIN bankDetails as tbd ON t.transID = tbd.transID
						LEFT JOIN currencies as cur ON t.currencyTo = cur.currencyName
					where
						t.transid = '$intTransactionId'
						AND t.benAgentId = '$intDistributorId'
						  ";

		$arrTransactionData = selectSql($strSql, $payex);
		//debug($arrTransactionData);
		
		/* Check if same Reference Number and same Manual code transaction are exisits in MT Db or not */
		$strDuplicateSql = "select trans_id from ".MTDB.".transaction where tmt_number = '".$arrTransactionData["refNumberIM"]."' and agent_id = '".$arrTransactionData["refNumber"]."'";
		$arrDuplicateData = selectSql($strDuplicateSql, $mtlink);

		if(!empty($arrTransactionData) && empty($arrDuplicateData["trans_id"]))
		{
			/* Insert the transaction data into the middle tier */
			
			//$strBeneficiaryName = (!empty($arrTransactionData["title"])? $arrTransactionData["title"]." ":"").$arrTransactionData["firstName"]." ".(!empty($arrTransactionData["middleName"])? $arrTransactionData["middleName"]." ":"").$arrTransactionData["lastName"];
			
			$strBeneficiaryName = (!empty($arrTransactionData["middleName"]) ? $arrTransactionData["middleName"]." ":"").$arrTransactionData["lastName"];
			
			$strAddress2 = $arrTransactionData["City"].", ".$arrTransactionData["Country"];
			
			$strAccountType = $arrTransactionData["accountType"];
			if($arrTransactionData["transType"] == "ATM Card")
				$strAccountType = "ATM";
			
			$sqlBeneficiaryData = "insert into ".MTDB.".beneficiary 
					(first_name, last_name, address, address2, phone, account_number, bank_name, branch_code, branch_address, account_type, swift, email)
					values
					('".strtoupper($arrTransactionData["firstName"])."', '".strtoupper($strBeneficiaryName)."', '".$arrTransactionData["Address"]."', '".$strAddress2."', '".$arrTransactionData["phone"]."', '".$arrTransactionData["accNo"]."', '".$arrTransactionData["bankName"]."', '".$arrTransactionData["branchCode"]."', '".$arrTransactionData["branchAddress"]."', '".$strAccountType."', '".$arrTransactionData["swiftCode"]."', '".$arrTransactionData["email"]."' )";

			if(insertSql($sqlBeneficiaryData, $mtlink))
			{
				$intBeneficiaryId = mysql_insert_id($mtlink);
			
				$sqlTransaction = "insert into ".MTDB.".transaction 
											   (tmt_number, agent_id, country_from, currency_from, country_to, currency_to, local_amount, created_on, foreign_amount,  middle_tier_client_id, mt_beneficiary_id, store_timestamp, extra1)
											   values
											   ('".$arrTransactionData["refNumberIM"]."', '".$arrTransactionData["refNumber"]."', '".$arrTransactionData["fromCountry"]."', '".$arrTransactionData["currencyFrom"]."', '".$arrTransactionData["toCountry"]."', '".$arrTransactionData["numCode"]."', '".$arrTransactionData["localAmount"]."', '".$transactionDateTime."', '".$arrTransactionData["transAmount"]."',  '".CONFIG_MIDDLE_TIER_CLIENT_ID."', '".$intBeneficiaryId."', '".time()."', '".$arrTransactionData["tip"]."')";
				
				//debug(date("H:i:s")." % ".$sqlTransaction);
				if(insertSql($sqlTransaction, $mtlink))
				{
					$arrClientMiddleTierData = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);
					
					$strUpdatePayexTransaction = "update transactions set from_server = '".$arrClientMiddleTierData["partner_name"]."', is_parsed = 'Y' where transid = '$intTransactionId'";
					updateSql($strUpdatePayexTransaction, $payex);
				}
			}
			
		}
		mysql_close($mtlink);
		//mysql_close($payex);
	}
}

function cancelApiTransaction($intTransactionId)
{
	//debug_print_backtrace();

	if(!empty($intTransactionId))
	{
		$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
		if(!mysql_select_db(MTDB, $mtlink))
			die("Could not connect to ".MTDB." database.");

		$payex = mysql_connect(SERVER_MASTER, USER, PASSWORD);
		if(!mysql_select_db(DATABASE, $payex))
			die("Could not connect to ".DATABASE." database.");

		$strDistributorSql = "select 
								c.associate_distributor, 
								a.associated_id 
							from 
								client as c 
							LEFT JOIN api_services as a 
								ON c.payex_client_id = a.client_id 
							where 
								payex_client_id = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'
								and a.service_type = 'D'
								";
		
		$arrDistributorId = selectSql($strDistributorSql, $mtlink);
		$intDistributorId = trim($arrDistributorId["associated_id"]);
		if(empty($intDistributorId))
			$intDistributorId = trim($arrDistributorId["associate_distributor"]);
			
		
		$strSql = "select 
						transid,
						partner_id, 
						sending_agent
					from 
						transactions as t
					where
						transid = '$intTransactionId'
						AND t.benAgentId = '$intDistributorId'
						AND transStatus = 'AwaitingCancellation'
					  ";

		$arrTransactionData = selectSql($strSql, $payex);
		debug($arrTransactionData,true);
		
		if(!empty($arrTransactionData))
		{
			/* Insert the transaction data into the middle tier */
			
			$client = new SoapClient(SOAP_URL);
			
			if(!empty($intBatchId))
			{
				try {
					
					//cancelOutletPayment(string $batchno, string $seq_no, string $reason)
					$objResponse = $client->cancelOutletPayment($arrTransactionData["partner_id"], $arrTransactionData["sending_agent"], "For internal");
				}
				catch(SoapFault $e){
					//debugVar($e, true);
					echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
				}
				debugVar(htmlentities($objResponse));
				$arrResponse = xmlToArray($objResponse);
				debugVar($arrResponse);

			}					

			if($arrResponse["sucess"])
			{
					$arrClientMiddleTierData = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);

					updateSql($strUpdatePayexTransaction, $payex);
			}
		}
		mysql_close($mtlink);
		//mysql_close($payex);
	}
}

function selectSql($sql, $objDbLink)
{ 
	if((@$result = mysql_query ($sql, $objDbLink)) == false)
	{
		if(defined("DEBUG_ON"))
			echo mysql_sql_message($sql, $objDbLink);		
	}   
	else
	{	
		if ($check=mysql_fetch_array($result, MYSQL_BOTH))
			return $check;

		return false;	
	}
}

function insertSql($sql, $objDbLink)
{
	if((@$result = mysql_query($sql, $objDbLink)) == false)
	{
		if(defined("DEBUG_ON"))
			echo mysql_sql_message($sql, $objDbLink);
		return false; 
	}   
	else
		return true;	
}

function updateSql($sql, $objDbLink)
{
	/*echo "<pre>";
	debug_print_backtrace();
	echo "<pre>"; */
	if((@$result = mysql_query($sql, $objDbLink)) == false)
	{
		if(defined("DEBUG_ON"))
			echo mysql_sql_message($sql, $objDbLink);		
		return false; 
	}   
	else
		return true;
}

function SelectMultiRecordsSql($sql, $objDbLink)
{   
	if((@$result = mysql_query ($sql, $objDbLink)) == false)
	{
		if(defined("DEBUG_ON"))
			echo mysql_sql_message($sql, $objDbLink);
		return false;	
	}   
	else
	{	
		$count = 0;
		$data = array();
		while ( $row = mysql_fetch_array($result)) 
		{
			$data[$count] = $row;
			$count++;
		}
		return $data;
	}
}

function mysql_sql_message($sql, $objDbLink)
{
	$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>$company MySql Debugger:</font></strong><br>";
	$msg .= "Error in your Query: $sql<BR>";
	$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
	$msg .= mysql_errno($objDbLink) . " " . mysql_error($objDbLink) . "</div><HR>";
	echo $msg;
}

function xmlToArray($strXml, $get_attributes = 1, $priority = 'tag')
{
	$contents = $strXml;
	if (!function_exists('xml_parser_create'))
	{
		return array ();
	}
	$parser = xml_parser_create('');
	xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
	xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	xml_parse_into_struct($parser, trim($contents), $xml_values);
	xml_parser_free($parser);
	if (!$xml_values)
		return;
	$xml_array = array ();
	$parents = array ();
	$opened_tags = array ();
	$arr = array ();
	$current = & $xml_array;
	$repeated_tag_index = array ();
	foreach ($xml_values as $data)
	{
		unset ($attributes, $value);
		extract($data);
		$result = array ();
		$attributes_data = array ();
		if (isset ($value))
		{
			if ($priority == 'tag')
				$result = $value;
			else
				$result['value'] = $value;
		}
		if (isset ($attributes) and $get_attributes)
		{
			foreach ($attributes as $attr => $val)
			{
				if ($priority == 'tag')
					$attributes_data[$attr] = $val;
				else
					$result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
			}
		}
		if ($type == "open")
		{
			$parent[$level -1] = & $current;
			if (!is_array($current) or (!in_array($tag, array_keys($current))))
			{
				$current[$tag] = $result;
				if ($attributes_data)
					$current[$tag . '_attr'] = $attributes_data;
				$repeated_tag_index[$tag . '_' . $level] = 1;
				$current = & $current[$tag];
			}
			else
			{
				if (isset ($current[$tag][0]))
				{
					$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
					$repeated_tag_index[$tag . '_' . $level]++;
				}
				else
				{
					$current[$tag] = array (
						$current[$tag],
						$result
					);
					$repeated_tag_index[$tag . '_' . $level] = 2;
					if (isset ($current[$tag . '_attr']))
					{
						$current[$tag]['0_attr'] = $current[$tag . '_attr'];
						unset ($current[$tag . '_attr']);
					}
				}
				$last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
				$current = & $current[$tag][$last_item_index];
			}
		}
		elseif ($type == "complete")
		{
			if (!isset ($current[$tag]))
			{
				$current[$tag] = $result;
				$repeated_tag_index[$tag . '_' . $level] = 1;
				if ($priority == 'tag' and $attributes_data)
					$current[$tag . '_attr'] = $attributes_data;
			}
			else
			{
				if (isset ($current[$tag][0]) and is_array($current[$tag]))
				{
					$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
					if ($priority == 'tag' and $get_attributes and $attributes_data)
					{
						$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
					}
					$repeated_tag_index[$tag . '_' . $level]++;
				}
				else
				{
					$current[$tag] = array (
						$current[$tag],
						$result
					);
					$repeated_tag_index[$tag . '_' . $level] = 1;
					if ($priority == 'tag' and $get_attributes)
					{
						if (isset ($current[$tag . '_attr']))
						{
							$current[$tag]['0_attr'] = $current[$tag . '_attr'];
							unset ($current[$tag . '_attr']);
						}
						if ($attributes_data)
						{
							$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
						}
					}
					$repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
				}
			}
		}
		elseif ($type == 'close')
		{
			$current = & $parent[$level -1];
		}
	}
	return ($xml_array);

}

function object_to_array($data)
{
	if(is_array($data) || is_object($data))
	{
		$result = array(); 
		foreach($data as $key => $value)
		{ 
		  $result[$key] = object_to_array($value); 
		}
		return $result;
	}
	return $data;
}

function debugVar( $variableToDebug = "", $shouldExitAfterDebug = false)
{
	if ( defined("DEBUG_ON")
		 && DEBUG_ON == true )
	{
		$arrayFileDetails = debug_backtrace();
		echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
	
		if ( !empty($variableToDebug)  )
		{
			echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
		}
		else
		{
			echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
		}
		echo "</pre>";
		if ($shouldExitAfterDebug)
			exit();
	}
}

function encrypt_3DES($strData, $iv, $kA)
{
	$enc = "";
	$ivv = "";
	$ivv2 = "";
	
	//$iv = array(51,51,52,54,54,55,56,58);
	//$kA = array(0xA2, 0x15, 0x37, 0x07, 0xCB, 0x62,0xC1, 0xD3, 0xF8, 0xF1, 0x97, 0xDF,0xD0, 0x13, 0x4F, 0x79, 0x01, 0x67,0x7A, 0x85, 0x94, 0x16, 0x31, 0x92);	
	foreach($kA as $arr)
		$ivv .= chr($arr);

	foreach($iv as $arr)
		$ivv2 .= chr($arr);
	
	$buffer = "01230000"; 
	// get the amount of bytes to pad
	$extra = 8 - (strlen($buffer) % 8);
	// add the zero padding
	if($extra > 0)
		for($i = 0; $i < $extra; $i++)
			$buffer .= "\0";

	// very simple ASCII key and IV
	$key = $ivv;
	$iv = $ivv2;
	//debug($key);
	//debug($iv);
	// hex encode the return value
	return  bin2hex(mcrypt_cbc(MCRYPT_3DES, $key, $strData, MCRYPT_ENCRYPT, $iv));
}


function in_multi_array($value, $array)
{   
	foreach ($array as $key => $item)
	{       
		if (!is_array($item))
		{
			if ($item == $value) 
				return true;
		}
		else
		{
			// See if the array name matches our value
			if ($key == $value) 
				return true;
		  
			// See if this array matches our value
			if (in_array($value, $item)) 
				return true;
			else if (in_multi_array($value, $item)) 
				return true;
		}
	}
	return false;
}

?>