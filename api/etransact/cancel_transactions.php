<?php
	define("LOCAL_DIR", "/var/sandbox/jalam/payex/");
	include(LOCAL_DIR."include/masterConfig.php");
	//include(LOCAL_DIR."api/country_codes_class.php");
	require("soap_api.php");
	
	$_arrResponseCodes = array(
		"0" => "Transaction Successful",
		"1" => "Destination Card Not Found",
		"2" => "Card Number Not Found",
		"3" => "Invalid Card PIN",
		"4" => "Card Expiration Incorrect",
		"5" => "Insufficient balance",
		"6" => "Spending Limit Exceeded",
		"7" => "Internal System Error Occured please contact the service provider",
		"8" => "Financial Institution Cannot authorise transaction Please try later",
		"9" => "PIN tries Exceeded",
		"10" => "Card has been locked",
		"11" => "Invalid Terminal Id",
		"12" => "Payment Timeout",
		"13" => "Destination card has been locked",
		"14" => "Card has expired",
		"15" => "PIN change required",
		"16" => "Invalid Amount",
		"17" => "Card has been disabled",
		"18" => "Unable to credit destination account request will be rolled back",
		"19" => "Transaction not permitted on terminal",
		"20" => "Exceeds withdrawal frequency",
		"21" => "Destination Card has Expired",
		"22" => "Destination Card Disabled",
		"23" => "Source Card Disabled",
		"24" => "Invalid Bank Account",
		"25" => "Insufficient Balance"
	);
	
	
	$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");
	
	
	/* making connection with Payex DB */
	$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);
	//debugVar($arrDbConnection);
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
		die("Could not connect to ".$arrDbConnection["client_database"]." database.");
	
	$payex_db = $arrDbConnection["client_database"];
	
	
	/* Select transaction from which parsed count is less than the total count */
	$strNewTransactionMiddleTier = "select * from ".MTDB.".transaction where middle_tier_client_id = '".CONFIG_MIDDLE_TIER_CLIENT_ID."' and is_parsed = 'N'";
	debugVar($strNewTransactionMiddleTier);
	$arrNewTransactionMiddleTier = SelectMultiRecordsSql($strNewTransactionMiddleTier, $mtlink);
	
	//debugVar($arrNewTransactionMiddleTier);
	
	/* Making the SOAP Client to forward the transactions */
	$client = new SoapClient(SOAP_URL);

	$intRejectedTransCount = 0;
	$transIds = "";
	$sequenceNo = 1;
	$bachNumber = "1231111100000001";
	
	$arrTransactionSrlToRefNo = array();
	$batchNo = time();
	
	if(count($arrNewTransactionMiddleTier) > 0)
	{
	
		
		$strXmlString = '
			<?xml version="1.0" encoding="UTF-8"?>
			<FundGateData >
				<Request batchno="'.$batchNo.'" totaldstaccount="1">
					<SourceAccountData>
						<AccountType>C</AccountType>
						<AccountNumber>0331324872345712</AccountNumber>
						<Currency>566</Currency>
						<Expiration>566</Expiration>
						<PIN>'.encrypt_3DES(PIN_CODE, $_arrIV, $_arrUserKey).'</PIN>
					</SourceAccountData>
			';

		for($j=0; $j < count($arrNewTransactionMiddleTier); $j++)
		{
			$intTotalTransactionTransferToPayex = 0;
			$arrTransIdsField = $arrNewTransactionMiddleTier[$j]["trans_id"];
			$transIds .= $arrNewTransactionMiddleTier[$j]["tmt_number"].",";
			//debugVar($arrTransIdsField);
			if(!empty($arrTransIdsField))
			{
				$arrBeneficiaryData = selectSql("select * from ".MTDB.".beneficiary where beneficiary_id = '".$arrNewTransactionMiddleTier[$j]["mt_beneficiary_id"]."'", $mtlink);
				
				/* Calculation for Account Type (Transaction type)*/
				$chrAccountType = "";
				if(!empty($arrBeneficiaryData["bank_name"]) && !empty($arrBeneficiaryData["account_number"]))
				{
					$chrAccountType = "A"; // Bank Account
					$strAccountNo = $arrBeneficiaryData["account_number"];
					
					$arrBankCodes = array_flip($_arrBankCodes);
					$intBankCode = $arrBankCodes[$arrBeneficiaryData["bank_name"]];

					$strBankCodeXml = '<BankCode>'.$intBankCode.'</BankCode>';
				}
				else
				{
					$chrAccountType = "O"; // Counter Deliver (Pick Up)
					$strAccountNo = "REFID:".$arrNewTransactionMiddleTier[$j]["tmt_number"];
					$strBankCodeXml = '';
				}
	
				$strAccountNo = $arrBeneficiaryData["account_number"];
				if(empty($strAccountNo))
					$strAccountNo = $arrNewTransactionMiddleTier[$j]["tmt_number"];
				
				/*
					<FirstName></FirstName>
					<LastName></LastName>
					<Email></Email>
					<PayeeId></PayeeId>
					
					<Sequence>'.str_pad($sequenceNo,3,"0", STR_PAD_LEFT).'</Sequence>

					
				*/
				$strXmlString .= '
								<DestinationAccountData>
									<Sequence>'.$sequenceNo.'</Sequence>
									<AccountType>'.$chrAccountType.'</AccountType>
									<AccountNumber>'.$strAccountNo.'</AccountNumber>
									'.$strBankCodeXml.'
									<AccountAccessCode>'.$arrNewTransactionMiddleTier[$j]["extra1"].'</AccountAccessCode>
									<Fullname>'.$arrBeneficiaryData["first_name"].'</Fullname>
									<Address1>'.$arrBeneficiaryData["address"].'</Address1>
									<Address2>'.$arrBeneficiaryData["address2"].'</Address2>
									<Phone>'.$arrBeneficiaryData["phone"].'</Phone>
									<TotalAmount>'.$arrNewTransactionMiddleTier[$j]["local_amount"].'</TotalAmount>
									<Currency>'.$arrNewTransactionMiddleTier[$j]["currency_to"].'</Currency>
									<Description>REFID:'.$arrNewTransactionMiddleTier[$j]["tmt_number"].'</Description>
								</DestinationAccountData>
								';

				$arrTransactionSrlToRefNo[$arrNewTransactionMiddleTier[$j]["tmt_number"]] = $sequenceNo;
				$sequenceNo++;
			}
			
		}
	
		$strXmlString .= '
							</Request>
						</FundGateData>
						';
		
		debugVar(htmlentities($strXmlString));
		try {
			$objResponse = $client->cancelOutletPayment("1242828792",2,"this is test to see cancel");
			//$objResponse = $client->cancelOutletBatch("1241520014");
			debugVar($objResponse);
		}
		catch(SoapFault $e){
			//debugVar($e, true);
			echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
		}
		debugVar($arrTransactionSrlToRefNo);
		$arrResponse = xmlToArray($objResponse);
		debugVar($arrResponse);
		debugVar(htmlentities($objResponse));
		
		/*
		if(!empty($arrResponse["FundGateData"]["Response"]["Exception"]))
		{
			
			if(in_multi_array())
			{
				
			}
		}
		*/
/*		if($arrResponse["FundGateData"]["Response"]["Transaction"]["Count"] > 0)
		{
			$arrTransactions = explode(",",$transIds);
			debugVar($arrTransactions);
			foreach($arrTransactions as $strKey => $strVal)
			{
						
				if(!empty($strVal))
				{
					$strBatchNo = $arrResponse["FundGateData"]["Response_attr"]["batchno"];
					
					$strUpdatePayexTrans = "update 
												$payex_db.transactions 
											set 
												partner_id = '".$strBatchNo."', 
												sending_agent = '".$arrTransactionSrlToRefNo[$strVal]."'
											where 
												refNumberIM = '".$strVal."'";
					debugVar($strUpdatePayexTrans);
					if(!updateSql($strUpdatePayexTrans, $payexLink))
						echo "Unable to update the PAYEX transaction!<br />";*/
					/*	
					$strRemoveTransactionData = "delete from ".MTDB.".transaction where tmt_number = '".$arrTransactions[$c]."'";
					if(!updateSql($strRemoveTransactionData, $mtlink))
						echo "Unable to remove transaction data from Middle Tier!<br />";
					*/
				}
				//else /* Error In the transaction processing from the Remote server */
				//	$intRejectedTransCount++;
	/*		}
		}*/

		/* Update the external response table, with all the transactios as parsed */
/*		$strInsertExternalResponseTable = "insert into ".MTDB.".external_reponse_code 
										   (client_id, total_transactions, parsed, rejected, datetime, interaction_step, trans_ids)
										   values 
										   ('".CONFIG_MIDDLE_TIER_CLIENT_ID."', '".($j+1)."', '".($j+1-$intRejectedTransCount)."', '".$intRejectedTransCount."', '".time()."', '3', '".$transIds."')";
	
		if(!insertSql($strInsertExternalResponseTable, $mtlink))
			echo "Error in updating the response code table summary.";*/
		
	//}	
	/* Close the payex DB connection */	
	debugVar($client->__getFunctions());
	mysql_close($payexLink);
	mysql_close($mtlink);

	function in_multi_array($value, $array)
	{   
		foreach ($array as $key => $item)
		{       
			// Item is not an array
			if (!is_array($item))
			{
				// Is this item our value?
				if ($item == $value) return true;
			}
		  
			// Item is an array
			else
			{
				// See if the array name matches our value
				if ($key == $value) return true;
			  
				// See if this array matches our value
				if (in_array($value, $item)) return true;
			  
				// Search this array
				else if (in_multi_array($value, $item)) return true;
			}
		}
	  
		// Couldn't find the value in array
		return false;
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>eTransact Transactions</title>
</head>
<body>
<h1 align="center">Summary of Cancellation </h1>
<h2>
<?php	


	$strCommunicationSummary = 
		"<hr />
		Time for the Cancellation: ".date("Y-m-d H:i:s")."<br />
		No. of Transactions for Cancellation: ".($j)."<br />
		Not sent For cancellation: ".$intRejectedTransCount;
	
	echo $strCommunicationSummary;
?>	
</h2>
</body>
</html>
