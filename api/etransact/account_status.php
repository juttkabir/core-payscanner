<?php
	define("LOCAL_DIR", "/var/sandbox/jalam/payex/");
	include(LOCAL_DIR."include/masterConfig.php");

	require("soap_api.php");
	
	$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");
	
	
	/* making connection with Payex DB */
	$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);
	//debugVar($arrDbConnection);
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);


	if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
		die("Could not connect to ".$arrDbConnection["client_database"]." database.");
	
	$payex_db = $arrDbConnection["client_database"];
	
	
	/* Select transaction from which parsed count is less than the total count */
	$strNewTransactionMiddleTier = "
									select 
										distinct partner_id
									from 
										$payex_db.transactions 
									where 
										from_server = 'ETRANSACT' AND 
										is_parsed = 'Y' AND 
										transStatus = 'Pending'"; 
										
	debugVar($strNewTransactionMiddleTier);
	$arrNewTransactionMiddleTier = SelectMultiRecordsSql($strNewTransactionMiddleTier, $payexLink);
	
	debugVar($arrNewTransactionMiddleTier);
		
	/* Making the SOAP Client to forward the transactions */
	$client = new SoapClient(SOAP_URL);

	$_intTransactionRejected = 0;
	$_intTransactionWithProblem = 0;
	
	//$arrNewTransactionMiddleTier = array( "0" => array( "partner_id" => "1243002776" ), "1" => array( "partner_id" => "1243434898" )  ); 
	
	if(count($arrNewTransactionMiddleTier) > 0)
	{
	
		for($j=0; $j < count($arrNewTransactionMiddleTier); $j++)
		{
			$intTotalTransactionTransferToPayex = 0;
			$intBatchId = $arrNewTransactionMiddleTier[$j]["partner_id"];

			if(!empty($intBatchId))
			{
				try {
					$objResponseAccountStatus = $client->fetchBatchResponse($intBatchId);
				}
				catch(SoapFault $e){
					echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
				}

/*
				$objResponseAccountStatus = '
				<?xml version="1.0" encoding="UTF-8"?>
		<FundGateData>
			<Response batchno="1243002776">
				<Transaction>
					<Sequence>1</Sequence>
					<ResponseCode>57   </ResponseCode>
					<Description>null</Description>
				</Transaction>
				<Transaction>
					<Sequence>2</Sequence>
					<ResponseCode>96   </ResponseCode>
					<Description>null</Description>
				</Transaction>
			</Response>
		</FundGateData>
'; */

				debugVar(htmlentities($objResponseAccountStatus));
				$arrResponse = xmlToArray($objResponseAccountStatus);
				debugVar($arrResponse);
			
				/* No response from the eTransact */
				if(!empty($arrResponse["FundGateData"]["Response"]["Transaction"]))
				{
				
					$_arrTransactions = $arrResponse["FundGateData"]["Response"];
					if(is_array($arrResponse["FundGateData"]["Response"]["Transaction"]["0"]))
						$_arrTransactions = $arrResponse["FundGateData"]["Response"]["Transaction"];
					
					$_strResBatchNumber = $arrResponse["FundGateData"]["Response_attr"]["batchno"];
					
					
					foreach($_arrTransactions as $_key => $_val)
					{
						debugVar($_key);
						debugVar($_val);
						
						$_intResSequenceNumber = $_val["Sequence"];
						$_strResDescription = $_val["Description"];
						$_strResCode = trim($_val["ResponseCode"]);
						
						// Transaction authorized 
						if($_strResCode == "00" && !empty($_intResSequenceNumber))
						{
							$strUpdatePayexTrans = "update 
										$payex_db.transactions 
									set 
										transStatus = 'Authorize',
										authoriseDate = '".date("Y-m-d H:i:s")."',
										authorisedBy = 'eTransact'
									where 
										partner_id = '".$_strResBatchNumber."' AND
										sending_agent = '".$_intResSequenceNumber."'";


						} /* Response other than the Authorized */
						elseif($_strResCode != "00" && $_strResCode != "-1")
						{
							$strUpdatePayexTrans = "update 
									$payex_db.transactions 
								set 
									transStatus = 'Failed',
									failedDate = '".date("Y-m-d H:i:s")."',
									remarks = '".$_arreTransactErrorCodes[$_strResCode]."'
								where 
									partner_id = '".$_strResBatchNumber."' AND
									sending_agent = '".$_intResSequenceNumber."'";
						
						}

						debugVar($strUpdatePayexTrans);
						/* Execute the DB query to update the transaction */
						if(!updateSql($strUpdatePayexTrans, $payexLink))
							echo "Unable to update the payex transaction.";

					} // end of for loop --for each transaction in a batch

				
				}
				
				
			} // end of empty batch condition check
			
		} // end of for loop
	
	} // end of transaction count check
	
	/* Close the payex DB connection */	
	mysql_close($payexLink);
	mysql_close($mtlink);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>eTransact Transactions - Update Account Status</title>
</head>
<body>
<h1 align="center">Summary of Communication</h1>
<h3>
<?php	

	echo "<hr />
		Time for the Summary: ".date("Y-m-d H:i:s")."<br />";
	
	if(!empty($arrNewTransactionMiddleTier))
	{
		echo "Total batched updated: <br />";
		
		foreach($arrNewTransactionMiddleTier as $key => $val)
			echo $val["partner_id"] ."<br />"; 
	}		
?>	
</h3>
</body>
</html>