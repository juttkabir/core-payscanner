<?php
	define("LOCAL_DIR", "/var/sandbox/jalam/payex/");
	include(LOCAL_DIR."include/masterConfig.php");
	//include(LOCAL_DIR."api/country_codes_class.php");
	require("soap_api.php");
	
	$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");
	
	
	/* making connection with Payex DB */
	$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);
	//debugVar($arrDbConnection);
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);


	if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
		die("Could not connect to ".$arrDbConnection["client_database"]." database.");
	
	$payex_db = $arrDbConnection["client_database"];
	
	
	/* Select transaction from which parsed count is less than the total count */
	$strNewTransactionMiddleTier = "
									select 
										transid, refNumberIM, partner_id, sending_agent, transStatus
									from 
										$payex_db.transactions 
									where 
										from_server = 'ETRANSACT' AND 
										partner_id != '' AND 
										sending_agent != '' AND
										is_parsed = 'Y' AND 
										transStatus = 'Authorize'"; 
										
	debugVar($strNewTransactionMiddleTier);
	$arrNewTransactionMiddleTier = SelectMultiRecordsSql($strNewTransactionMiddleTier, $payexLink);
	
	debugVar($arrNewTransactionMiddleTier);
	
	/* Making the SOAP Client to forward the transactions */
	$client = new SoapClient(SOAP_URL);

	//$arrNewTransactionMiddleTier = array( "0" => array( "partner_id" => "1242828792", "sending_agent" => "2" )/*, "1" => array( "partner_id" => "1243434898" ) */ ); 
	//$arrNewTransactionMiddleTier = array( "0" => array( "partner_id" => "1243002776" ), "1" => array( "partner_id" => "1243434898" )  ); 
	
	
	$_intTransactionRejected = 0;
	$_intTransactionCredited = 0;
	$_intTransactionWithProblem = 0;
	$_arrTransactionNotCredited = array();
	
	if(count($arrNewTransactionMiddleTier) > 0)
	{
	
		for($j=0; $j < count($arrNewTransactionMiddleTier); $j++)
		{
			$intTotalTransactionTransferToPayex = 0;
			$intBatchId = $arrNewTransactionMiddleTier[$j]["partner_id"];
			$intSerialNo = $arrNewTransactionMiddleTier[$j]["sending_agent"];
			$intTransactionId = $arrNewTransactionMiddleTier[$j]["transid"];
			

			if(!empty($intBatchId))
			{
				try {

					$objResponse = $client->fetchPaymentResponse($intBatchId, $intSerialNo);
				}
				catch(SoapFault $e){
					//debugVar($e, true);
					echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
				}
				//debugVar($intBatchId ." - ". $intSerialNo);
				debugVar(htmlentities($objResponse));
				$arrResponse = xmlToArray($objResponse);
				debugVar($arrResponse);
				
				/* No response from the eTransact */
				if(empty($arrResponse["FundGateData"]))
				{
					$strUpdatePayexTrans = "update 
								$payex_db.transactions 
							set 
								remarks = 'NO RESPONSE FROM eTransact',
								transStatus = 'Rejected',
								rejectDate = '".date("Y-m-d H:i:s")."'
							where 
								transId = '".$intTransactionId."'";

					$_arrTransactionNotCredited[] = $arrNewTransactionMiddleTier[$j]["refNumberIM"];
					$_intTransactionRejected++;

				}
				else
				{
					$_strResBatchNumber = $arrResponse["FundGateData"]["Response_attr"]["batchno"];
					$_intResSequenceNumber = $arrResponse["FundGateData"]["Response"]["Transaction"]["Sequence"];
				
					$_strResDescription = $arrResponse["FundGateData"]["Response"]["Transaction"]["Description"];
					$_strResCode = trim($arrResponse["FundGateData"]["Response"]["Transaction"]["ResponseCode"]);
				
					if($_strResBatchNumber == $intBatchId && $_intResSequenceNumber == $intSerialNo)
					{
						// Transaction approved or completed sucessfully 
						if($_strResCode == "00")
						{
							$strUpdatePayexTrans = "update 
										$payex_db.transactions 
									set 
										transStatus = 'Credited',
										remarks = '".$_strResDescription."',
										deliveryDate = '".date("Y-m-d H:i:s")."'
									where 
										transId = '".$intTransactionId."'";
										
										
							$_intTransactionCredited++;

						}
						else//if($_strResCode != "-1")
						{
							$strUpdatePayexTrans = "update 
										$payex_db.transactions 
									set 
										remarks = '".$_arreTransactErrorCodes[$_strResCode]."'
									where 
										transId = '".$intTransactionId."'";

							$_arrTransactionNotCredited[] = $arrNewTransactionMiddleTier[$j]["refNumberIM"];
							$_intTransactionWithProblem++;

						}
					}
				}
				
				debugVar($strUpdatePayexTrans);
				/* Execute the DB query to update the transaction */
				if(!updateSql($strUpdatePayexTrans, $payexLink))
					echo "Unable to update the payex transaction.";
				
			} // end of empty batch condition check
			
		} // end of for loop
	
		debugVar($_arrTransactionNotCredited);
		
		/* Entry into the transaction communication summary */
		$strInsertExternalResponseTable = "insert into ".MTDB.".external_reponse_code 
										   (client_id, total_transactions, parsed, rejected, datetime, interaction_step, trans_ids)
										   values 
										   ('".CONFIG_MIDDLE_TIER_CLIENT_ID."', 
										    '".count($arrNewTransactionMiddleTier)."', 
											'".$_intTransactionCredited."', 
											'".$_intTransactionWithProblem."', 
											'".time()."', 
											'3', 
											'".serialize($_arrTransactionNotCredited)."')";
		debugVar($strInsertExternalResponseTable);
		if(!insertSql($strInsertExternalResponseTable, $mtlink))
			echo "Error in updating the response code table summary.";		

	} // end of transaction count check
	
	/* Close the payex DB connection */	
	mysql_close($payexLink);
	mysql_close($mtlink);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>eTransact Transactions - Update Status</title>
</head>
<body>
<h1 align="center">Summary of Communication</h1>
<h3>
<?php	

	echo "<hr />
		Time for the Summary: ".date("Y-m-d H:i:s")."<br />";
	
	if(!empty($arrNewTransactionMiddleTier))
	{
		echo "Total batche(s) with serial number updated: <br />";
		
		foreach($arrNewTransactionMiddleTier as $key => $val)
			echo $val["partner_id"] ." => ". $val["sending_agent"]."<br />"; 
	}		
?>	
</h3>
</body>
</html>