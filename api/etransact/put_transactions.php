<?php
	define("LOCAL_DIR", "/var/sandbox/jalam/payex/");
	include(LOCAL_DIR."include/masterConfig.php");
	//include(LOCAL_DIR."api/country_codes_class.php");
	require("soap_api.php");
	
	$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");
	
	/* making connection with Payex DB */
	$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '".CONFIG_MIDDLE_TIER_CLIENT_ID."'", $mtlink);
	//debugVar($arrDbConnection);
	$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
	if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
		die("Could not connect to ".$arrDbConnection["client_database"]." database.");
	
	$payex_db = $arrDbConnection["client_database"];
	
		/* Select transaction from which parsed count is less than the total count */
	$strNewTransactionMiddleTier = "select * from ".MTDB.".transaction where middle_tier_client_id = '".CONFIG_MIDDLE_TIER_CLIENT_ID."' and is_parsed = 'N'";
	$arrNewTransactionMiddleTier = SelectMultiRecordsSql($strNewTransactionMiddleTier, $mtlink);
	
	//debugVar($arrNewTransactionMiddleTier);
	
	/* Making the SOAP Client to forward the transactions */
	$client = new SoapClient(SOAP_URL);
	debugVar(SOAP_URL);
	
	$intRejectedTransCount = 0;
	$transIds = "";
	$sequenceNo = 1;
	
	$arrTransactionSrlToRefNo = array();
	$batchNo = time();
	
	
	if(count($arrNewTransactionMiddleTier) > 0)
	{
	
		$strXmlString = '
			<?xml version="1.0" encoding="UTF-8"?>
			<FundGateData>
				<Request batchno="'.$batchNo.'" totaldstaccount="1">
					<SourceAccountData>
						<AccountType>C</AccountType>
						<AccountNumber>'.CARD_NO_ET.'</AccountNumber>
						<Currency>566</Currency>
						<Expiration>03-2011</Expiration>
						<PIN>'.encrypt_3DES(PIN_CODE, $_arrIV, $_arrUserKey).'</PIN>
					</SourceAccountData>
			';

		for($j=0; $j < count($arrNewTransactionMiddleTier); $j++)
		{
			$intTotalTransactionTransferToPayex = 0;
			$arrTransIdsField = $arrNewTransactionMiddleTier[$j]["trans_id"];
			$transIds .= $arrNewTransactionMiddleTier[$j]["tmt_number"].",";
			//debugVar($arrTransIdsField);
			if(!empty($arrTransIdsField))
			{
				$arrBeneficiaryData = selectSql("select * from ".MTDB.".beneficiary where beneficiary_id = '".$arrNewTransactionMiddleTier[$j]["mt_beneficiary_id"]."'", $mtlink);
				
				/* Calculation for Account Type (Transaction type)*/
				$chrAccountType = "";
				
				if($arrBeneficiaryData["account_type"] == "ATM")
				{
					/**
						<DestinationAccountData>
							<Sequence>001</Sequence>
							<AccountType>C</AccountType>
							<FirstName>Osahon</FirstName>
							<LastName>Osula</LastName>
							<Email>osaosula@yahoo.com</Email>
							<PayeeId>1011</PayeeId>
							<BankCode>069</BankCode>
							<AccountNumber>0690220708240006</AccountNumber>
							<Currency>566</Currency>
							<TotalAmount>1.00</TotalAmount>
							<Description>Test FundGate : 001</Description>
						</DestinationAccountData>
					
					
						PayeeId, First and Lastname, email, description are not  are not compulsory
					 */
				
					$chrAccountType = "C"; // Card Deposit
					$strAccountNo = $arrBeneficiaryData["account_type"];

					$arrBankCodes = array_flip($_arrBankCodes);
					$intBankCode = $arrBankCodes[$arrBeneficiaryData["bank_name"]];
					$strBankCodeXml = '<BankCode>'.$intBankCode.'</BankCode>';
				}
				elseif(!empty($arrBeneficiaryData["bank_name"]) && !empty($arrBeneficiaryData["account_number"]))
				{
					
					/**
						<DestinationAccountData>
							<Sequence>001</Sequence>
							<AccountType>A</AccountType>
							<FirstName>Osahon</FirstName>
							<LastName>Osula</LastName>
							<Email>osaosula@yahoo.com</Email>
							<PayeeId>1011</PayeeId>
							<BankCode>058</BankCode>
							<AccountNumber>4124007721590</AccountNumber>
							<Currency>566</Currency>
							<TotalAmount>1.00</TotalAmount>
							<Description>Test FundGate : 001</Description>
						</DestinationAccountData>
					*/

					$chrAccountType = "A"; // Bank Account
					$strAccountNo = $arrBeneficiaryData["account_number"];
					
					$arrBankCodes = array_flip($_arrBankCodes);
					$intBankCode = $arrBankCodes[$arrBeneficiaryData["bank_name"]];
					$strBankCodeXml = '<BankCode>'.$intBankCode.'</BankCode>';
				}
				else
				{
					/* Not working yet now from eTransact side */
				
					$chrAccountType = "O"; // Counter Deliver (Pick Up)
					$strAccountNo = "REFID:".$arrNewTransactionMiddleTier[$j]["tmt_number"];
					$strBankCodeXml = '
										<BankCode>000</BankCode>
										<Address1>'.$arrBeneficiaryData["address"].'</Address1>
										<Address2>'.$arrBeneficiaryData["address2"].'</Address2>
										<AccountAccessCode>'.$arrNewTransactionMiddleTier[$j]["extra1"].'</AccountAccessCode>
									';
				}
	
				$strAccountNo = $arrBeneficiaryData["account_number"];
				
				$strXmlString .= '
								<DestinationAccountData>
									<Sequence>'.$sequenceNo.'</Sequence>
									<AccountType>'.$chrAccountType.'</AccountType>
									<AccountNumber>'.$strAccountNo.'</AccountNumber>
									'.$strBankCodeXml.'
									<FirstName>'.$arrBeneficiaryData["first_name"].'</FirstName>
									<LastName>'.$arrBeneficiaryData["last_name"].'</LastName>
									<Email>'.CONFIG_EMAIL_ADDRESS_AT_API_PAYOUT.'</Email>
									<PayeeId>'.CONFIG_PAYEE_ID_AT_API_PAYOUT.'</PayeeId>
									<TotalAmount>'.$arrNewTransactionMiddleTier[$j]["local_amount"].'</TotalAmount>
									<Currency>'.$arrNewTransactionMiddleTier[$j]["currency_to"].'</Currency>
									<Description>REFID:'.$arrNewTransactionMiddleTier[$j]["tmt_number"].'</Description>
								</DestinationAccountData>
								';

				$arrTransactionSrlToRefNo[$arrNewTransactionMiddleTier[$j]["tmt_number"]] = $sequenceNo;
				$sequenceNo++;
			}
			
		}
	
		$strXmlString .= '
							</Request>
						</FundGateData>
						';
		
		debugVar(htmlentities($strXmlString));
		
		/*
		try {
			$objResponse = $client->sendFundsRequest($strXmlString, TERMINAL_ID);
		}
		catch(SoapFault $e){
			//debugVar($e, true);
			echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
		}
		debugVar($arrTransactionSrlToRefNo);
		$arrResponse = xmlToArray($objResponse);
		debugVar($arrResponse);
		debugVar(htmlentities($objResponse));
		*/
		
		/* TESTING DATA 
		$objResponse = '<?xml version="1.0" encoding="UTF-8"?>
<FundGateData>
  <Response batchno="1243434898">
    <Transaction>
     <Sequence>1</Sequence>
     <ResponseCode>-1</ResponseCode>
     <Description>Awaiting Validation</Description>
    </Transaction>
   <Transaction>
    <Sequence>2</Sequence>
    <ResponseCode>-1</ResponseCode>
    <Description>Awaiting Validation</Description>
   </Transaction>
  </Response>
</FundGateData>

						';
		$arrResponse = xmlToArray($objResponse);
		debugVar($arrResponse);
		debugVar(htmlentities($objResponse), true);
*/
				
		$strBatchNo = $arrResponse["FundGateData"]["Response_attr"]["batchno"];
		
		$intRejectedTransactionsCount = 0;	
		$strRejectedTransactionManualCode = "";	
		$arrExceptionList = array();

		
		/* If transansaction has been rejected by the eTransact , than record proper response code at the middle tier */
		if(is_array($arrResponse["FundGateData"]["Response"]["Exception"]["Item"]))
		{
			if(is_array($arrResponse["FundGateData"]["Response"]["Exception"]["Item"][0]))
				$arrItemException = $arrResponse["FundGateData"]["Response"]["Exception"]["Item"];
			else
				$arrItemException = $arrResponse["FundGateData"]["Response"]["Exception"];

			$_arrReverseSerialToManual = array_flip($arrTransactionSrlToRefNo);
			
			foreach($arrItemException as $ieKey => $ieVal)
			{
				//debugVar($ieKey);
				//debugVar($ieVal);
				
				$intSequenceNumber = $ieVal["Sequence"];
				$strDescription = $ieVal["Description"];
				
				$strRejectedTransactionManualCode .= $_arrReverseSerialToManual[$intSequenceNumber].",";
				$arrExceptionList[$intSequenceNumber] = $strDescription;
				
				$intRejectedTransactionsCount++;
			}
		}
		
		
		$intTotalAcceptedTransactions = $arrResponse["FundGateData"]["Response"]["Transaction"]["Count"];
		
		/* Update the external response table, with all the transactios as parsed */
		$strInsertExternalResponseTable = "insert into ".MTDB.".external_reponse_code 
										   (client_id, total_transactions, parsed, rejected, datetime, interaction_step, trans_ids)
										   values 
										   ('".CONFIG_MIDDLE_TIER_CLIENT_ID."', 
										    '".count($arrNewTransactionMiddleTier)."', 
											'".$intTotalAcceptedTransactions."', 
											'".$intRejectedTransactionsCount."', 
											'".time()."', 
											'1', 
											'".$strRejectedTransactionManualCode."')";
	
		debugVar($strInsertExternalResponseTable);
		//if(!insertSql($strInsertExternalResponseTable, $mtlink))
			echo "Error in updating the response code table summary.";		


		$arrTransactions = explode(",",$transIds);
		//debugVar($arrTransactions);
		//debugVar($arrExceptionList);
		foreach($arrTransactions as $strKey => $strVal)
		{
			if(!empty($strVal))
			{
				/* If the transaction is rejected from the eTransact, update txn as rejected */
				if(strpos($strRejectedTransactionManualCode, $strVal) !== false)
					$strUpdatePayexTrans = "update 
												$payex_db.transactions 
											set 
												partner_id = '".$strBatchNo."', 
												sending_agent = '".$arrTransactionSrlToRefNo[$strVal]."',
												remarks = '".$arrExceptionList[$arrTransactionSrlToRefNo[$strVal]]."',
												is_parsed = 'N',
												transStatus = 'Rejected'
											where 
												refNumberIM = '".$strVal."'";
					
				else
					$strUpdatePayexTrans = "update 
												$payex_db.transactions 
											set 
												partner_id = '".$strBatchNo."', 
												sending_agent = '".$arrTransactionSrlToRefNo[$strVal]."',
												is_parsed = 'Y'
											where 
												refNumberIM = '".$strVal."'";
				debugVar($strUpdatePayexTrans);

				if(1)//if(updateSql($strUpdatePayexTrans, $payexLink))
				{
					$strRemoveTransactionData = "delete from ".MTDB.".transaction where tmt_number = '".$strVal."'";
					debugVar($strRemoveTransactionData);
					//if(!updateSql($strRemoveTransactionData, $mtlink))
						echo "Unable to remove transaction data from Middle Tier!<br />";
				}
				else
					echo "Unable to update the PAYEX transaction!<br />";
			}
		}


		////////////////////////////////////////////////////////////////////////////////////
		////////////////////   UPDATE TRANSACTION WITH 'fetchAccountStatus'   //////////////
		////////////////////////////////////////////////////////////////////////////////////
		
		/*		
		try {
			//$objResponse = $client->fetchBatchResponse($intBatchId);
			//$objResponseAccountStatus = $client->fetchAccountStatus("1243002776");
			$objResponseAccountStatus = $client->fetchAccountStatus("1243434898");
		}
		catch(SoapFault $e){
			echo ($e->faultcode." <br /> ".$e->faultstring." <br /> ".$e->faultactor." <br /> ".$e->detail .$e->E_USER_ERROR);
		}
		
		//$doc = DOMDocument::loadXML($objResponseAccountStatus);
		//debugVar($doc->saveXML(), true);

		$objResponseAccountStatus = '<?xml version="1.0" encoding="UTF-8"?>
					<FundGateData>
					  <Response batchno="1243434898">
						<Transaction>
						 <Sequence>1</Sequence>
						 <ResponseCode>-1</ResponseCode>
						 <Description>Awaiting Validation</Description>
						</Transaction>
					   <Transaction>
						<Sequence>2</Sequence>
						<ResponseCode>-1</ResponseCode>
						<Description>Awaiting Validation</Description>
					   </Transaction>
					  </Response>
					</FundGateData>

						';
		
		debugVar(htmlentities($objResponseAccountStatus));
		$arrResponse = xmlToArray($objResponseAccountStatus);
		debugVar($arrResponse);

		*/
		/////////////////////////////////////////////////////////////////////////////////////


	}	
	
	
	
	/* Close the DB connections */	
	mysql_close($payexLink);
	mysql_close($mtlink);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>eTransact Transactions</title>
</head>
<body>
<h1 align="center">Summary of Communication</h1>
<h2>
<?php	
	$strCommunicationSummary = 
		"<hr />
		Time for the Summary: ".date("Y-m-d H:i:s")."<br />
		Total Transaction Received From Payex: ".count($arrNewTransactionMiddleTier)."<br />
		Total Transaction Rejected: ".$intRejectedTransactionsCount."<br />
		BATCH ID: ".$strBatchNo;
	
	echo $strCommunicationSummary;
?>	
</h2>
</body>
</html>
