<?php

$login = 'wajiha@payscanner.com';

$apiKey = 'c2c25846c9692ea7d539fc29a2f9c79fadf5b6489bab68d4bcc5e5a107071a4d';

$baseUrl = 'https://devapi.currencycloud.com';

$basicRatesUrl = '/v2/rates/find';

$detailedRatesUrl = '/v2/rates/detailed';

$getTokenUrl = '/v2/authenticate/api';

// $baseCurrency = $_POST['baseCurrency'] ? $_POST['baseCurrency']; : '';

// $quotedCurrency = $_POST['quotedCurrency'] ? $_POST['quotedCurrency']; : '';

function getToken($baseUrl, $getTokenUrl, $apiKey, $login) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl . $getTokenUrl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key=' . $apiKey . '&login_id=' . $login);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);

	if (strpos($response, 'auth_token') !== false) {
		$response = json_decode($response);
		return $response->auth_token;
	} else {
		echo json_encode(['error' => 'Authentication error']);
		die();
	}
}

function getBasicRates($baseUrl, $basicRatesUrl, $authToken, $currencyPair) {
	$ch = curl_init();
	$headers = [
		'X-Auth-Token: ' . $authToken,
	];
	curl_setopt($ch, CURLOPT_URL, $baseUrl . $basicRatesUrl . '?currency_pair=' . $currencyPair);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);

	$response = json_decode($response, true);

	if ($response['rates']) {
		$rates = $response['rates'];
		var_dump($rates);
	} else {
		echo '<br />' . $response . '<br />';
		echo json_encode(['error' => 'Error']);
		die();
	}
}

$baseCurrency = 'EUR';
$quotedCurrency = 'GBP';

if (!empty($baseCurrency) && !empty($quotedCurrency)) {
	$currencyPair = $baseCurrency . $quotedCurrency;
	$authToken = getToken($baseUrl, $getTokenUrl, $apiKey, $login);

	$rates = getBasicRates($baseUrl, $basicRatesUrl, $authToken, $currencyPair);

	print_r($rates);
} else {
	echo 'Wrong request';
	die();
}

?>