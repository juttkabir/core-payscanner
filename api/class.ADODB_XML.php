<?php require("class.xml.php"); ?>
<?php
/**
* Class of Manipulation of files XML and SGBD
*
* @author    			Olavo Alexandrino <oalexandrino@yahoo.com.br> - 2004
* @originalAuthor		Ricardo Costa <ricardo.community@globo.com>   - 2002
* @based				MySQL to XML - XML to MySQL - <http://www.phpclasses.org/browse/package/782.html>
* @require				Class XMLFile <Olavo Alexandrino version> - <orignal version: http://www.phpclasses.org/browse/package/79.html>
		- function commentary to strtoupper of the methods:
			1. add_attribute
			2. set_name
* @require				ADOdb Database Library for PHP <http://php.weblogs.com/adodb#downloads>
*/

class ADODB_XML
{
	/**
	*	Object representation:  File XML
	*
	*	@type		objcet
	*	@access		public
	*/
	var $xml = null;

	/**
	*	Creating the members
	*
	*	@param		string		Version of file XML
	*	@param		string		Codification to be used
	*	@access		public
	*/
	function ADODB_XML($version = "", $encoding = "")
	{
	  $this->xml = new XMLFile($version, $encoding);
	}

	/**
	*	It converts Table of the SGBD into file XML
	*
	*	@param		object 			Connection of ADOdb Database Library
	*	@param		string			Query SQL
	*	@param		string			Name of existing file XML
	*	@access		public
	*	@return 	void
	*/
	function ConvertToXML($dbConnection, $strSQL, $filename)
	{
	 	 //$dbConnection->SetFetchMode(ADODB_FETCH_ASSOC);
	 	 //$rs = $dbConnection->Execute($strSQL);
	  $rs = mysql_query($strSQL) or die("Query failed : " . mysql_error());

	  $this->xml->create_root();
	  $this->xml->roottag->name = "ROOT";


	   	$i= mysql_num_fields($rs);
		for($count=0; $count < $i; $count++)
		{
			//echo("Count is $count");
			$fieldNames = mysql_field_name($rs, $count);
			$fields[] = $fieldNames;
		}


		//echo("total is".$i);

		while($row = mysql_fetch_array($rs))
		{
		//echo("Entered loop for row");
		$this->xml->roottag->add_subtag("ROW", array());
		$tag = &$this->xml->roottag->curtag;

			$j = 0;
			while($j < $i)
			{
				$fieldName = $fields[$j];
				$fieldValue = $row[$j];

				$tag->add_subtag($fieldName);
				$tag->curtag->cdata = $fieldValue;
			$j++;
			}


		}
		//$xml_file = fopen($filename, "w" );
		//$this->xml->write_file_handle( $xml_file );
		$this->xml->download_file_handle( );

	}

	/**
	*	It inserts XML in table of the SGBD
	*
	*	@param		object 			Connection of ADOdb Database Library
	*	@param		string			Name of to be created file XML
	*	@param		string			Table of BD
	*	@access		public
	*	@return 	void
	*/
	function InsertIntoDB($dbConnection, $filename, $tablename)
	{

	  $xml_file = fopen($filename, "r");
	  $this->xml->read_file_handle($xml_file);

//	echo("number of rows are");
	  $numRows = $this->xml->roottag->num_subtags();

	  for ($i = 0; $i < $numRows; $i++)
	  {
		  $arrFields = null;
		  $arrValues = null;

		  
		   $row = $this->xml->roottag->tags[$i];
		   $numFields = $row->num_subtags();

		   for ($ii = 0; $ii < $numFields; $ii++)
		   {
			  $field = $row->tags[$ii];
			  
			  $innerFields = $field->num_subtags();
			  if($innerFields > 0){
			  	
			  	
			  	for($inner = 0; $inner < $innerFields; $inner++)
			  	{
			  		$innerField = $field->tags[$inner];
			  		$count = $innerField->num_subtags();
			  		if($count > 0)
			  		{
			  			for($inner2 = 0; $inner2 < $count; $inner2++)
			  			{
			  				$innerField2 = $innerField->tags[$inner2];	
			  				$arrFields[] = $field->name."-".$innerField->name."-".$innerField2->name;
			  				$arrValues[] = "\"".$innerField2->cdata."\"";	
			  			}
			  			
			  		}else
			  		{
			  			$arrFields[] = $field->name."-".$innerField->name;
			  			$arrValues[] = "\"".$innerField->cdata."\"";	
			  		}
			  	}
			  	
			  	
			}else{
				$arrFields[] = $field->name;
			  	$arrValues[] = "\"".$field->cdata."\"";
				
				}
			  
			  //$arrFields[] = $field->name;
			  //$arrValues[] = "\"".$field->cdata."\"";
			  
		   }
		   
		 /****
		 *This is used when need to return values in an array
		 *return $finalArray = array_combine($arrFields, $arrValues);
		 ****/
		 
		 
		  /*****
		  * It is custom private method defined below to be 
		  * used in a particular scenario for insertion in DB
		  *$fieldNames = $this->insertTransactions($arrFields, $arrValues);	
		  ******/
		  $errorReturn = $this->verifyData($arrFields, $arrValues);
		  if($errorReturn[0])
		  {
		  	//echo($errorReturn[0]."Error Condition".$errorReturn[1]);
		  	return $errorReturn[1];	
	
		  }else{
		  	//echo("Other");
		 	 $transIDReturn = $this->insertTransactions($arrFields, $arrValues);	
		 	 return $transIDReturn;
		}
		
		 /*  $fields = join($arrFields, ", ");
		   $values = join($arrValues, ", ");
		echo("query is");
		 echo  $strSQL = "INSERT INTO $tablename ($fields) VALUES ($values)";

		 echo $rs = mysql_query($strSQL) or die("Query failed : " . mysql_error());
			*/
			
		  // $dbConnection->Execute($strSQL);
	  }

	}
	
	function verifyData($dataFields, $dataValues)
	{
		$error = false;
		
		$dataArray = array_combine($dataFields, $dataValues);	
		
		if(str_replace("\"","",$dataArray['CUSTOMER_AGENT_ID']) == '')
		{
			////Customer Agent Not Found
			$error = true;	
			$errList[] = "E101";
		}
		
		if(str_replace("\"","",$dataArray['REF_NUMBER']) == '')
		{
			//Invalid Reference Number
			$error = true;	
			$errList[] = "E102";
		}
		$amount = (float)str_replace("\"","",$dataArray['FUNDS-AMOUNT']);
		if($amount <= 0)
		{
			//Invalid Amount
			$error = true;	
			$errList[] = "E104";
		}
		$exchange = (float)str_replace("\"","",$dataArray['FUNDS-EXCHANGE_RATE']);
		if($exchange <= 0)
		{
			//Invalid Exchange Rate
			$error = true;	
			$errList[] = "E105";
		}
		$fee = (float)str_replace("\"","",$dataArray['FUNDS-AGENT_FEE']);
		if($fee < 0)
		{
			//Invalid Agent Fee
			$error = true;	
			$errList[] = "E106";
		}
		
		if(str_replace("\"","",$dataArray['SENDER-FIRSTNAME']) == '')
		{
			//Empty Sender Name
			$error = true;	
			$errList[] = "E107";
		}
		
		/*if(str_replace("\"","",$finalArray['CUSTOMER_AGENT_ID']) == '')
		{
			//Empty Sender Country
			$error = true;	
			$errList[] = "E108";
		}
		
		if(str_replace("\"","",$finalArray['CUSTOMER_AGENT_ID']) == '')
		{
			//Beneficiary Name is Empty
			$error = true;	
			$errList[] = "E109";
		}*/
		$errorData =array($error, $errList); 
		return $errorData;
	}
	
	
	
	function insertTransactions($fields, $values)
	{
		///Mapping fields with the DB field names
		
		
		////Merging of fields and values array
		
		$finalArray = array_combine($fields, $values);
		/*foreach($finalArray as $key => $val)
		{
			echo("Key is  $key  and Value is  $val");	
		}*/
		
		
		//isset($a['test'])
		if(str_replace("\"","",$finalArray['SENDER-PAYEX_REF']) != "")
		{
			$selectCustID = selectFrom("select customerID from customer where accountName = '".str_replace("\"","",$finalArray['SENDER-PAYEX_REF'])."'");
			$senderID = $selectCustID["customerID"];
			$updateSender = "update customer set
			firstName = '".str_replace("\"","",$finalArray['SENDER-FIRSTNAME'])."',
			lastName = '".str_replace("\"","",$finalArray['SENDER-LASTNAME'])."',
			IDType = '".str_replace("\"","",$finalArray['SENDER-AUTHENTICATION-DOCUMENT_TYPE'])."',
			IDNumber = '".str_replace("\"","",$finalArray['SENDER-AUTHENTICATION-DOCUMENT_ID'])."',
			Country = '".str_replace("\"","",$finalArray['SENDER-COUNTRY'])."'
			where customerID = $senderID";
			update($updateSender);
		}else{
			$insertSender = "insert into customer (firstName, lastName, IDType, IDNumber, Country)
		 	values('".str_replace("\"","",$finalArray['SENDER-FIRSTNAME'])."', '".str_replace("\"","",$finalArray['SENDER-LASTNAME'])."', '".str_replace("\"","",$finalArray['SENDER-AUTHENTICATION-DOCUMENT_TYPE'])."','".str_replace("\"","",$finalArray['SENDER-AUTHENTICATION-DOCUMENT_ID'])."','".str_replace("\"","",$finalArray['SENDER-COUNTRY'])."')"; 
				 	
			insertInto($insertSender);
			$senderID = mysql_insert_id();
			update("update customer set accountName = 'C-$senderID' where customerID = '$senderID'");
		}
		
		$insertBeneficiary = "insert into beneficiary (customerID, firstName, lastName, Address, Address1, City, State, Zip, Country, Phone)
		values('".$senderID."', '".str_replace("\"","",$finalArray['BENEFICIARY-FIRSTNAME'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-LASTNAME'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-LINE_1'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-LINE_2'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-CITY'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-STATE'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-POSTCODE'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-COUNTRY'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-PHONE'])."')";
		
		
		 insertInto($insertBeneficiary);
		 $benID = mysql_insert_id();
		 
		
		 $insertTransaction = "insert into transactions(customerID, benID, custAgentID, refNumber, refNumberIM, transAmount, exchangeRate, localAmount, IMFee, totalAmount,
		 moneyPaid, transStatus, transDate, remarks, fromCountry, toCountry, currencyFrom, currencyTo, AgentComm )values
		 ('".$senderID."', '".$benID."', '".str_replace("\"","",$finalArray['CUSTOMER_AGENT_ID'])."', '".str_replace("\"","",$finalArray['REF_NUMBER'])."', '".str_replace("\"","",$finalArray['REF_NUMBER_PAYEX'])."', '".str_replace("\"","",$finalArray['FUNDS-AMOUNT'])."', '".str_replace("\"","",$finalArray['FUNDS-EXCHANGE_RATE'])."', '".str_replace("\"","",$finalArray['FUNDS-AMOUNT_TO'])."', '".str_replace("\"","",$finalArray['FUNDS-TOTAL_FEE'])."', '".str_replace("\"","",$finalArray['FUNDS-TOTAL_AMOUNT'])."',
		 '".$PaidBy."', '".str_replace("\"","",$finalArray['STATUS'])."', '".str_replace("\"","",$finalArray['DATE'])."', '".str_replace("\"","",$finalArray['COMMENTS'])."', '".str_replace("\"","",$finalArray['SENDER-COUNTRY'])."', '".str_replace("\"","",$finalArray['BENEFICIARY-ADDRESS-COUNTRY'])."','".str_replace("\"","",$finalArray['FUNDS-CURRENCY_FROM'])."','".str_replace("\"","",$finalArray['FUNDS-CURRENCY_TO'])."','".str_replace("\"","",$finalArray['FUNDS-AGENT_FEE'])."')";
		 
		 
			 insertInto($insertTransaction);	
		 $transID = mysql_insert_id();
		 
		 return $transID;
		 //echo $insertBank = "insert into bankDetails(benID, transID, bankName, branchCode, accountType, accNo) 
		 //values('".$benID."', '".$transID."', '".$finalArray[bank_name]."', '".$finalArray[branch_code]."', '".$finalArray[type]."', '".$finalArray[number]."')";
		 
		 
	}
	
	function CustomTransXML($dbConnection, $strSQL, $filename, $err)
	{
		if(!$err)
		{
			
				$xmlFields = array( 't_custAgentID' => 'CUSTOMER_AGENT_ID','t_transDate' => 'DATE' , 't_refNumber' => 'REF_NUMBER', 't_refNumberIM' => 'REF_NUMBER_PAYEX',
		't_transType' => 'TYPE', 't_transStatus' => 'STATUS', 't_transAmount' => 'FUNDS-AMOUNT', 't_exchangeRate' => 'FUNDS-EXCHANGE_RATE', 't_AgentComm' => 'FUNDS-AGENT_FEE', 't_totalAmount' => 'FUNDS-TOTAL_AMOUNT',
		't_currencyTo' => 'FUNDS-CURRENCY_TO', 't_localAmount' => 'FUNDS-AMOUNT_TO', 't_remarks' => 'COMMENTS', 'c_firstName' => 'SENDER-FIRSTNAME', 'c_lastName' => 'SENDER-LASTNAME', 'c_IDType' => 'SENDER-AUTHENTICATION-DOCUMENT_TYPE', 'c_IDNumber' => 'SENDER-AUTHENTICATION-DOCUMENT_ID','c_Country' => 'SENDER-COUNTRY',
		'b_firstName' => 'BENEFICIARY-FIRSTNAME', 'b_lastName' => 'BENEFICIARY-LASTNAME', 'b_Address' => 'BENEFICIARY-ADDRESS-LINE_1', 'b_Address1' => 'BENEFICIARY-ADDRESS-LINE_2',
		'b_City' => 'BENEFICIARY-ADDRESS-CITY', 'b_State' => 'BENEFICIARY_ADDRESS_STATE', 'b_Zip' => 'BENEFICIARY-ADDRESS-POSTCODE', 'b_Country' => 'BENEFICIARY-ADDRESS-COUNTRY', 'b_Phone' => 'BENEFICIARY-ADDRESS-PHONE');

	}


		$rs = mysql_query($strSQL) or die("Query failed : " . mysql_error());
		
	  $this->xml->create_root();
	  $this->xml->roottag->name = "telegiros_payex";

//echo(" Total Count is ");
	   	$i= mysql_num_fields($rs);
		for($count=0; $count < $i; $count++)
		{
			//echo("Count is $count < $i");
			$fieldNames = mysql_field_name($rs, $count);
			$fields[$count] = $fieldNames;
		}


		//foreach ($xmlFields as $key => $value) {
		   //echo "Key: $key; Value: $value<br />\n";
		//}


		//echo("total is".$i);

		while($row = mysql_fetch_array($rs))
		{
			$senderInfo = selectFrom("select accountName from customer where customerID = '".$row[t_customerID]."'");
		//echo("Entered loop for row");
		$this->xml->roottag->add_subtag("transaction", array('REF_NUMBER' => $row['t_refNumber'] ));
		$tag = &$this->xml->roottag->curtag;
		$tag->add_subtag("sender");
		$tag->curtag->cdata=($senderInfo["accountName"]);
			
			$j = 0;
		/*	while($j < $i)
			{
			echo("   For each Field $j of $i");	
				$fieldFind = $fields[$j];
				$fieldName = $xmlFields[$fieldFind];
				
				
				$fieldValue = $row[$j];
			echo("Field is $fieldFind  XML Name is $fieldName and  Value is $fieldValue");
				$parameters = explode("-", $fieldName);
				if(count($parameters) > 0)
				{
					$innerTag = $tag;
					for($inCount = 0; $inCount < count($parameters); $inCount++)
					{
						echo("Name is $parameters[$inCount]");
						$innerTag2 = $innerTag->find_subtags_by_name($parameters[$inCount]);	
						if(!$innerTag2)
						{
							$innerTag->add_subtag($parameters[$inCount]);	
							
							if($inCount == (count($parameters)-1) )
							{
								$innerTag->curtag->cdata = $fieldValue;	
							}
						}else{
							$innerTag = $innerTag2;
						}
						
						
						
					}
				}else{

					$tag->add_subtag($fieldName);
					$tag->curtag->cdata = $fieldValue;
				}
			$j++;
			}*/


		}
		$xml_file = fopen($filename, "w" );
		$this->xml->write_file_handle( $xml_file );
		$this->xml->download_file_handle( );

		
	}
	

	function ExportTransXML($dbConnection, $strSQL, $filename, $err)
	{
		if(!$err)
		{
			
				$xmlFields = array( 't_custAgentID' => 'CUSTOMER_AGENT_ID', 't_refNumberIM' => 'REF_NUMBER_PAYEX', 't_transAmount' => 'SENDING_AMOUNT', 
														'c_created' => 'DATE' , 'c_firstName' => 'SENDER-FIRSTNAME', 'c_lastName' => 'SENDER-LASTNAME', 'c_accountName' => 'SENDER_NUMBER',
														'b_firstName' => 'BENEFICIARY-FIRSTNAME', 'b_lastName' => 'BENEFICIARY-LASTNAME',	'b_Address' => 'BENEFICIARY-ADDRESS', 
														'bd_IBAN' => 'BENEFICIARY-IBAN', 'bd_bankName' => 'BENEFICIARY-BANK-NAME', 'bd_swiftCode' => 'BENEFICIARY-SWIFTCODE');

	}


		$rs = mysql_query($strSQL) or die("Query failed : " . mysql_error());
		
	  $this->xml->create_root();
	  $this->xml->roottag->name = "FIDAVISTA ";

//echo(" Total Count is ");
	   	$i= mysql_num_fields($rs);
		for($count=0; $count < $i; $count++)
		{
			//echo("Count is $count < $i");
			$fieldNames = mysql_field_name($rs, $count);
			$fields[$count] = $fieldNames;
		}


		//foreach ($xmlFields as $key => $value) {
		   //echo "Key: $key; Value: $value<br />\n";
		//}


		//echo("total is".$i);

		while($row = mysql_fetch_array($rs))
		{
			$this->xml->roottag->add_subtag("Header");
			$tag = &$this->xml->roottag->curtag;
			$tag->add_subtag("Timestamp");
			$tag->curtag->cdata=("Not confirm");
			$tag->add_subtag("From");
			$tag->curtag->cdata=("OPAL TRANSFER LIMITED");
			$this->xml->roottag->add_subtag("Payment");
			$tag = &$this->xml->roottag->curtag;
			$tag->add_subtag("ExtId");
			$tag->curtag->cdata=("Not confirm");
			$tag->add_subtag("DocNo");
			$tag->curtag->cdata=("Not confirm");
			$tag->add_subtag("RegDate");
			$tag->curtag->cdata=($row["c_created"]);
			$tag->add_subtag("TaxPmtFlg");
			$tag->curtag->cdata=("N");
			$tag->add_subtag("Ccy");
			$tag->curtag->cdata=("LVL");		
			$tag->add_subtag("PmtInfo");
			$tag->curtag->cdata=($row["c_firstName"]." ".$row["c_lastName"].", ".$row["c_accountName"]);
			$tag->add_subtag("PayLegalId");
			$tag->curtag->cdata=($row["t_refNumberIM"]);
			$tag->add_subtag("PayAccNo");
			$tag->curtag->cdata=("LV71PARX0007782390001");
			$tag->add_subtag("DebitCcy");
			$tag->curtag->cdata=("LVL");
			$tag->add_subtag("BenSet");
			$subTag = $tag->curtag;
			$subTag->add_subtag("BenExtId"); 
			$subTag->curtag->cdata=("Not confirm");
			$subTag->add_subtag("Priority"); 
			$subTag->curtag->cdata=("N");
			$subTag->add_subtag("Comm"); 
			$subTag->curtag->cdata=("OUR");	
			$subTag->add_subtag("Amt"); 
			$subTag->curtag->cdata=($row["t_transAmount"]);	
			$subTag->add_subtag("BenAccNo"); 
			$subTag->curtag->cdata=($row["bd_IBAN"]);	
			$subTag->add_subtag("BenName"); 
			$subTag->curtag->cdata=($row["b_firstName"]." ".$row["b_lastName"]);	
			$subTag->add_subtag("BenLegalId"); 
			$subTag->curtag->cdata=("Not confirm");	
			$subTag->add_subtag("BenAddress"); 
			$subTag->curtag->cdata=($row["b_Address"]);	
			$subTag->add_subtag("BenCountry"); 
			$subTag->curtag->cdata=("LV");	
			$subTag->add_subtag("BBName"); 
			$subTag->curtag->cdata=($row["bd_bankName"]);	
			$subTag->add_subtag("BBSwift"); 
			$subTag->curtag->cdata=($row["bd_swiftCode"]);	
			$subTag->add_subtag("AmkSet"); 
			$subTag1 = $subTag->curtag;
			$subTag1->add_subtag("Opc"); 
			$subTag1->curtag->cdata=("379");
			
			$j = 0;
		/*	while($j < $i)
			{
			echo("   For each Field $j of $i");	
				$fieldFind = $fields[$j];
				$fieldName = $xmlFields[$fieldFind];
				
				
				$fieldValue = $row[$j];
			echo("Field is $fieldFind  XML Name is $fieldName and  Value is $fieldValue");
				$parameters = explode("-", $fieldName);
				if(count($parameters) > 0)
				{
					$innerTag = $tag;
					for($inCount = 0; $inCount < count($parameters); $inCount++)
					{
						echo("Name is $parameters[$inCount]");
						$innerTag2 = $innerTag->find_subtags_by_name($parameters[$inCount]);	
						if(!$innerTag2)
						{
							$innerTag->add_subtag($parameters[$inCount]);	
							
							if($inCount == (count($parameters)-1) )
							{
								$innerTag->curtag->cdata = $fieldValue;	
							}
						}else{
							$innerTag = $innerTag2;
						}
						
						
						
					}
				}else{

					$tag->add_subtag($fieldName);
					$tag->curtag->cdata = $fieldValue;
				}
			$j++;
			}*/


		}
		$xml_file = fopen($filename, "w" );
		$this->xml->write_file_handle( $xml_file );
		$this->xml->download_file_handle( );

		
	}
	
	
		function CustomErrorXML($dbConnection, $strSQL, $filename, $err)
	{

		$rs = mysql_query($strSQL) or die("Query failed : " . mysql_error());

	  $this->xml->create_root();
	  $this->xml->roottag->name = "telegiros_payex";

//echo(" Total Count is ");
	   	$i= mysql_num_fields($rs);
		for($count=0; $count < $i; $count++)
		{
			//echo("Count is $count < $i");
			$fieldNames = mysql_field_name($rs, $count);
			$fields[$count] = $fieldNames;
		}


		//echo("total is".$i);

		while($row = mysql_fetch_array($rs))
		{
		//echo("Entered loop for row");
		$this->xml->roottag->add_subtag("Error", array('CODE' => $row['code'] ));
		$tag = &$this->xml->roottag->curtag;
		$tag->cdata = $row['description'];	
			
			//$j = 0;
	
//$xml->roottag->add_subtag( 'INNER', array( 'name' => 'value' ) );
//$xml->roottag->curtag->cdata = "Hello!";

		}
		$xml_file = fopen($filename, "w" );
		$this->xml->write_file_handle( $xml_file );
		$this->xml->download_file_handle( );

		
	}

}// end class
?>
