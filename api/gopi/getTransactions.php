<?php
	session_start();

	define("LOCAL_DIR", "/var/sandbox/cmtapi/");	

	include(LOCAL_DIR."include/masterConfig.php");
	require("api_functions.php");
	
	$mtlink = mysql_connect(MTHOST ,MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");

	
	//$srtFilesWildCard = "*_TMTPOT_".date("dmYH",$intLastDays)."*";
	$srtFilesWildCard = "*_TMTPOT_*";
	/**
	 * If successfully read out some files on the remote server, than process,
	 * This will help in the cron job process
	 */
	if($allFiles = getFileList($sftp, $srtFilesWildCard))
	{
		//die("Error in reading remote directory! ".$srtFilesWildCard);
	
		//debug($allFiles);
		//$arrFilesForSummary = array();
		
		if(!copyAllFilesOnLocalMachine($allFiles))
			die("Could not copy the remote files into local directory.");
		
		/**
		 * After copying the files from remote machine, get the list of all the copied files 
		 */
		/*
		if(!$allLocalFiles = getFileListOnLocalOutputDir($srtFilesWildCard))
			die("Error in reading local directory! ".$srtFilesWildCard);
		*/
		//debug($allLocalFiles);
	
		$intTransactionSaved = 0;
		$intTransactionFailedValidation = 0;
		
		$strMiddleTierTransIds = "";
		$i = 0;
		foreach($allFiles as $fK => $fV)
		{
			$arrayFileContents = readFileSftp($fV);
			
			if($arrayFileContents) /* If data is read successfully */
			{
			
				//debug($arrayFileContents);
		
				/* Validate the xml file */
				//validateXmlFromFile($arrayFileContents);
				
				$arrayFromXml = xmlToArray($arrayFileContents);
				
				//debugVar($arrayFromXml);
				//$intTransactionFailedValidation = 0;
				
				$arrayOfXmlFromFile = $arrayFromXml["TMTPayoutTrans"]["Transaction"];
				if(!is_array($arrayOfXmlFromFile[0]))
					$arrayOfXmlFromFile = $arrayFromXml["TMTPayoutTrans"];
				/*else {
					$arrayOfXmlFromFile = $arrayFromXml["TMTPayoutTrans"]["Transaction"];
				} */
					
				foreach($arrayOfXmlFromFile as $av => $ak)
				{
					//debugVar($av);
					//debugVar($ak);
					if(strlen($ak["tmtTransactionNumber"]) > 5) /* A validation check to ensure TMT numbers are valid */
					{
						$customerId = false;
						$beneficiaryId = false;
			
						$arrBenficiaryDOB = explode("T", $ak["dateOfBirth"]);
						$beneficiaryDOB = $arrBenficiaryDOB[0];
						if(empty($beneficiaryDOB))
							$beneficiaryDOB = "0000-00-00";
						
						/* Checking if customer data already found in customer table 
						$arrCheckCustomer = selectSql("select 
												customer_id 
											from 
												".MTDB.".customer
											where
												first_name = '".strtoupper($ak["firstName"])."' and
												last_name = '".strtoupper($ak["lastName"])."' and
												city = '".$ak["city"]."' and
												phone_no = '".$ak["phoneNumber"]."'
											", $mtlink);

						if(empty($arrCheckCustomer["customer_id"]))
						{
						}
						else 
						// Customer data already in database 
						$customerId = $arrCheckCustomer["customer_id"];

						*/
						/* If no data found for transaction customer, than add it */
							
						$arrDateOfissue = explode("T", $ak["dateOfIssue"]);
						$arrDateOfExpiry = explode("T", $ak["dateOfExpiry"]);
						
						$sqlCustomerData = "insert into ".MTDB.".customer 
											(first_name, last_name, address, city, country, postcode, phone_no, place_of_birth, nationality, gender, id_type, id_provided, date_of_expiry, date_of_issue)
											values
											('".strtoupper($ak["firstName"])."', '".strtoupper($ak["lastName"])."', '".$ak["address1"]."', '".$ak["city"]."', '".$ak["country"]."', '".$ak["postCode"]."', '".$ak["phoneNumber"]."', '".$ak["placeOfBirth"]."', '".$ak["nationality"]."', '".$ak["gender"]."', '".$ak["ID1Type"]."', '".$ak["ID1Provided"]."', '".$arrDateOfExpiry[0]."', '".$arrDateOfissue[0]."' )";
						
						if(insertSql($sqlCustomerData, $mtlink))
							$customerId = mysql_insert_id($mtlink);
							
						
						/* Checking if beneficiary data already found in customer table 
						$arrCheckBeneficiary = selectSql("select 
												beneficiary_id 
											from 
												".MTDB.".beneficiary
											where
												first_name = '".strtoupper($ak["recv-firstName"])."' and
												last_name = '".strtoupper($ak["recv-lastName"])."' and
												city = '".$ak["recv-city"]."' and
												date_of_birth = '".$beneficiaryDOB."'
											", $mtlink);	
			
						//debugVar("select beneficiary_id from ".MTDB.".beneficiary where first_name = '".strtoupper($ak["recv-firstName"])."' and last_name = '".strtoupper($ak["recv-lastName"])."' and city = '".$ak["recv-city"]."' and date_of_birth = '".$beneficiaryDOB."'");	
						if(empty($arrCheckBeneficiary["beneficiary_id"]))
						{
						}
						else
							$beneficiaryId = $arrCheckBeneficiary["beneficiary_id"];
						*/
			
						
						$sqlBeneficiaryData = "insert into ".MTDB.".beneficiary 
											(first_name, last_name, address, address2, city, region, country, postcode, date_of_birth, gender, moth_maiden_name)								
											values
											('".strtoupper($ak["recv-firstName"])."', '".strtoupper($ak["recv-lastName"])."', '".$ak["recv-address1"]."', '".$ak["recv-address2"]."', '".$ak["recv-city"]."', '".$ak["recv-region"]."', '".$ak["recv-country"]."', '".$ak["recv-postCode"]."', '".$beneficiaryDOB."', '".$ak["gender"]."', '".$ak["mothMaidenName"]."' )";
					
						if(insertSql($sqlBeneficiaryData, $mtlink))
							$beneficiaryId = mysql_insert_id($mtlink);
						
						
						//debug($sqlCustomerData);
						//debug($sqlBeneficiaryData);
						
						$sqlAlreadyAvailable = "select count(trans_id) from ".MTDB.".transaction where tmt_number = '".$ak["tmtTransactionNumber"]."'";
						$arrAlreadyAvailable = selectSql($sqlAlreadyAvailable, $mtlink);
						
						//debugVar($sqlAlreadyAvailable);
						//debugVar($arrAlreadyAvailable);	
						if($arrAlreadyAvailable[0] < 1 && !empty($customerId) && !empty($beneficiaryId))
						{
							$transactionDateTime = str_replace("T"," ",$ak["transactionDateTime"]);
							$messageDateTime = str_replace("T"," ",$ak["messageDateTime"]);
							
							$sqlTransaction = "insert into ".MTDB.".transaction 
											   (tmt_number, agent_id, transaction_type, country_from, currency_from, country_to, currency_to, product_id, local_amount, exchange_rate, commission, created_on, foreign_amount, message_date, partner_id, middle_tier_client_id, mt_customer_id, mt_beneficiary_id, store_timestamp, test_question, test_answer, payment_reason, service_data, payout_agent_id)
											   values
											   ('".$ak["tmtTransactionNumber"]."', '".$ak["agentId"]."', '".$ak["transactionType"]."', '".$ak["countryFrom"]."', '".$ak["currencyFrom"]."', '".$ak["countryTo"]."', '".$ak["currencyTo"]."', '".$ak["productId"]."', '".$ak["localAmount"]."', '".$ak["exchangeRate"]."', '".$ak["totalCustomerCharge"]."', '".$transactionDateTime."', '".$ak["payoutAmount"]."', '".$messageDateTime."', '".$ak["partnerId"]."', '1', '".$customerId."', '".$beneficiaryId."', '".time()."', '".$ak["testQuestion"]."', '".$ak["testAnswer"]."', '".$ak["reasonForPayment"]."', '".$ak["dataForServices"]."', '".$ak["partnerId"]."')
											   ";
							//debug($sqlTransaction);
							
							if(insertSql($sqlTransaction, $mtlink))
							{
								$strMiddleTierTransIds .= mysql_insert_id($mtlink).", ";
								$intTransactionSaved++;
								//debugVar($intTransactionSaved);
							}	
								
						}
						else
						{ 	/* Rejected Transaction */
							$intTransactionFailedValidation++;
						}
					}
					else
					{
						/* If validation fails, than store it into the for further refrence */
						$intTransactionFailedValidation++;
						//debugVar($fV, true);
					}
					
					$i++;
				}//end of foreach
				
				if(defined("CONFIG_REMOVE_OR_PUT_FILES_WITH_REMOTE_SERVER") && CONFIG_REMOVE_OR_PUT_FILES_WITH_REMOTE_SERVER == "1")
				{
					/* Remove the read file , on the remote server  */
					if(unlink($fV))
						echo "File Removed ($fV)<br />";
					else
						echo "Unable to Removed File ($fV)<br />";
				}
			}
			
		}
		
		
		/* Get The total count from , insert into the summary table for API transactions */
		$strExistingDataSql = "insert into ".MTDB.".external_reponse_code 
								(client_id, total_transactions, rejected, files, datetime, trans_ids)
								values
								('1', '".$intTransactionSaved."', '".$intTransactionFailedValidation."', '".serialize($allFiles)."', '".time()."', '".$strMiddleTierTransIds."')	";
		
		//debug($strExistingDataSql);
		if(!insertSql($strExistingDataSql, $mtlink))
			echo "Error in summary tale entry.";
		
		/** 
		 * Entry in the financial table 
		 *
		 * Will decide about it later *
		 *
		 */
		
		
		
		/**********************************************************************
		 *   Move the data from the middle tier db to the Client Db in Payex  *
		 **********************************************************************/
		
		$sqlTotalCountSql = "select sum(total_transactions) as stt, sum(parsed) as sp, sum(rejected) as sr from ".MTDB.".external_reponse_code where client_id = '1' and interaction_step = '1'";
		//debug($sqlTotalCountSql);
		$arrTransferOrders = selectSql($sqlTotalCountSql, $mtlink);
		
		//debug($arrTransferOrders);
		
		/* Difference beteen new and parsed transactions */
		//$notParsedTrans = $arrTransferOrders["stt"] - $arrTransferOrders["sp"]  - $arrTransferOrders["sr"];
		$notParsedTrans = $arrTransferOrders["stt"] - $arrTransferOrders["sp"];
		
		$intTotalTransactionTransferToPayex = 0;
		
		/**
		 * If total transaction that are transfer to the middle tier, are greater than 10 , 
		 * than transfer those transactions to the Client DB 
		 * After transferring to Payex, remove all those transactions from middle tier
		 */
		//debug($notParsedTrans);
		if($notParsedTrans >= 1)
		{
		
			/* making connection with Payex DB */
			$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '1'", $mtlink);
			//debugVar($arrDbConnection);
			$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
			if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
				die("Could not connect to ".$arrDbConnection["client_database"]." database.");
			
			$payex_db = $arrDbConnection["client_database"];
			$intAgentId = $arrDbConnection["associate_agent"];
			
			
			/* Select transaction from which parsed count is less than the total count */
			$strNewTransactionMiddleTier = "select * from ".MTDB.".external_reponse_code where parsed < total_transactions and client_id = '1' and interaction_step = '1'";
			$arrNewTransactionMiddleTier = SelectMultiRecordsSql($strNewTransactionMiddleTier, $mtlink);
			
			//debug($arrNewTransactionMiddleTier);
			for($j=0; $j<count($arrNewTransactionMiddleTier); $j++)
			{
				$intTotalTransactionTransferToPayex = 0;
				$arrTransIdsField = $arrNewTransactionMiddleTier[$j]["trans_ids"];
				//debug($arrTransIdsField);
				$arrTransIds = explode(", ", $arrTransIdsField);
				foreach($arrTransIds as $atik => $ativ)
				{
					//debug($ativ);
					if(!empty($ativ))
					{
						/**
						 * Now move the transactions, customers and the beneficiary associated with each transaction 
						 * to the Payex's own database, i.e. client database
						 */
						$arrTransactionData = selectSql("select * from ".MTDB.".transaction where trans_id = '".$ativ."' and is_parsed = 'N'", $mtlink);
						$arrCustomerData = selectSql("select * from ".MTDB.".customer where customer_id = '".$arrTransactionData["mt_customer_id"]."'", $mtlink);
						$arrBeneficiaryData = selectSql("select * from ".MTDB.".beneficiary where beneficiary_id = '".$arrTransactionData["mt_beneficiary_id"]."'", $mtlink);
						
						/**
						 * Insertion into the Payex DB, via Following steps
						 * 1- Check If the customer Already in Payex Customer Table
						 *	  Field: First_name, last_name, city, phone_number
						 * 2- If found than use the id, else insert and then use the customer id
						 * 3- Check If the beneficiary already in the payex beneficiary table
						 *	  Field: firse_name, last_name, city, data_of_birth
						 * 4- If beneficiary found than use the Id else insert and use the newly generated id
						 * 5- Insert the transaction data into the Payex table
						 * 6- Delete the transaction from the Middle tier DB
						 * 7- Update external_response_code that all transaction parsed
						 */
						
						$strIsAlreadyCustomerSql = "select 
														customerID 
													from 
														$payex_db.customer
													where
														firstName = '".$arrCustomerData["first_name"]."'  and 
														lastName = '".$arrCustomerData["last_name"]."'  and 
														City = '".$arrCustomerData["city"]."'  and 
														Phone = '".$arrCustomerData["phone_no"]."'";
						//debug($strIsAlreadyCustomerSql);
						
						$arrIsAlreadyCustomerSql = selectSql($strIsAlreadyCustomerSql, $payexLink);
						//debug($arrIsAlreadyCustomerSql);
						if(empty($arrIsAlreadyCustomerSql[0]))
						{
							
							$_arrIdTypes = array(
								"1" => "Passport",
								"2" => "National ID",
								"3" => "Driver\�s License",
								"4" => "Other Government ID",
								"6" => "Other"
							);
	
							
							$strInsertCustomerData = "insert into $payex_db.customer
													  (agentID, created, firstName, middleName, lastName, Address, Address1, City, State, Country, Zip, Phone, senderRemarks, IDType, IDNumber, IDExpiry, IDissuedate, placeOfBirth)
													  values
													  ('".$intAgentId."', '".date("Y-m-d H:i:s")."', '".$arrCustomerData["first_name"]."', '".$arrCustomerData["second_name"]."', '".$arrCustomerData["last_name"]."', '".$arrCustomerData["address"]."', '".$arrCustomerData["address2"]."', '".$arrCustomerData["city"]."', '".$arrCustomerData["statecode"]."', '".$arrCustomerData["country"]."', '".$arrCustomerData["postcode"]."', '".$arrCustomerData["phone_no"]."', 'CMT - Nationality:".$arrCustomerData["nationality"]."', '".$_arrIdTypes[$arrCustomerData["id_type"]]."', '".$arrCustomerData["id_provided"]."', '".$arrCustomerData["date_of_expiry"]."', '".$arrCustomerData["date_of_issue"]."', '".$arrCustomerData["place_of_birth"]."')";
							if(insertSql($strInsertCustomerData, $payexLink))
								$intPayexCustomerId = mysql_insert_id($payexLink);
						}
						else
							$intPayexCustomerId = $arrIsAlreadyCustomerSql["customerID"];
							
							
							
						$strIsAlreadyBeneficiarySql = "select 
														benID 
													from 
														$payex_db.beneficiary
													where
														firstName = '".$arrBeneficiaryData["first_name"]."'  and 
														lastName = '".$arrBeneficiaryData["last_name"]."'  and 
														City = '".$arrBeneficiaryData["city"]."'  and 
														dob = '".$arrBeneficiaryData["date_of_birth"]."'";
	
						//debug($strIsAlreadyBeneficiarySql);
						$arrIsAlreadyBeneficiarySql = selectSql($strIsAlreadyBeneficiarySql, $payexLink);
						//debug($arrIsAlreadyBeneficiarySql);
						if(empty($arrIsAlreadyBeneficiarySql[0]))
						{
							$beneficiaryCountry = explode(" (",$arrBeneficiaryData["country"]);
								
							$strInsertBeneficiaryData = "insert into $payex_db.beneficiary
													  (customerID, firstName, middleName, lastName, Address, Address1, City, State, Country, Zip, dob, CPF, created, moth_maiden_name, status)
													  values
													  ('".$intPayexCustomerId."', '".$arrBeneficiaryData["first_name"]."', '".$arrBeneficiaryData["second_name"]."', '".$arrBeneficiaryData["last_name"]."', '".$arrBeneficiaryData["address"]."', '".$arrBeneficiaryData["address2"]."', '".$arrBeneficiaryData["city"]."', '".$arrBeneficiaryData["statecode"]."', '".$beneficiaryCountry[0]."', '".$arrBeneficiaryData["postcode"]."', '".$arrBeneficiaryData["date_of_birth"]."', '".$arrBeneficiaryData["cpf_number"]."', '".date("Y-m-d")."', '".$arrBeneficiaryData["moth_maiden_name"]."', 'Disabled')";
							if(insertSql($strInsertBeneficiaryData, $payexLink))
								$intPayexBeneficiaryId = mysql_insert_id($payexLink);
						}
						else
							$intPayexBeneficiaryId = $arrIsAlreadyBeneficiarySql["benID"];
							
						$createdOn = $arrTransactionData["created_on"];
						if($createdOn == "0000-00-00 00:00:00")
							$createdOn = date("Y-m-d H:i:s");
						
						$arrProductType = array(
							"1" => "Pick up",
							"22" => "Bank Transfer",
							"25" => "Home Delivery"
							);
						
						
						$sqlAlreadyAvailable = "select count(transID) from $payex_db.transactions where refNumber = '".$arrTransactionData["tmt_number"]."'";
						$arrAlreadyAvailable = selectSql($sqlAlreadyAvailable, $payexLink);
						//debugVar($sqlAlreadyAvailable." * ".$arrAlreadyAvailable[0]);

						if($arrAlreadyAvailable[0] < 1 && !empty($intPayexCustomerId) && !empty($intPayexBeneficiaryId))
						{
						
							$strTransactionRefNo = $arrDbConnection["partner_name"]."-".$arrTransactionData["tmt_number"];
							
							$intBenAgentId = $_arrPayoutBanks[$arrTransactionData["payout_agent_id"]];
							
							
							$fltTransAmount = $arrTransactionData["local_amount"];
							$fltTotalAmount = $arrTransactionData["commission"];
							$fltLocalAmount = $arrTransactionData["foreign_amount"];
							$fltTotalFee = $fltTotalAmount - $fltTransAmount;
							//$totalAmount = $arrTransactionData["foreign_amount"] + $arrTransactionData["commission"];
	
															
							$sqlInsertIntoTransaction = "insert into $payex_db.transactions
														 (customerID, benID, custAgentID, benAgentID, refNumber, refNumberIM, exchangeRate, localAmount, IMFee, totalAmount, transAmount, transDate, addedBy, authorisedBy, authoriseDate, fromCountry, toCountry, currencyFrom, currencyTo, transStatus, moneyPaid, transType, from_server, sending_agent, question, answer, transactionPurpose, tip, partner_id)
														 values
														 ('".$intPayexCustomerId."', '".$intPayexBeneficiaryId."', '".$intAgentId."', '".$intBenAgentId."', '".$arrTransactionData["tmt_number"]."', '".$strTransactionRefNo."', '".$arrTransactionData["exchange_rate"]."', '".$fltLocalAmount."', '".$fltTotalFee."', '".$fltTotalAmount."', '".$fltTransAmount."', '".$createdOn."', 'CMT-".$arrTransactionData["intAgentId"]."', 'CMT-".$arrTransactionData["intAgentId"]."', '".date("Y-m-d H:i:s")."', '".$_arrCountries[$arrTransactionData["country_from"]]."', '".$_arrCountries[$arrTransactionData["country_to"]]."', '".$arrTransactionData["currency_from"]."', '".$arrTransactionData["currency_to"]."', 'Authorize', 'By Cash', '".$arrProductType[$arrTransactionData["product_id"]]."', '".$arrDbConnection["partner_name"]."', '".$arrTransactionData["agent_id"]."', '".$arrTransactionData["test_question"]."', '".$arrTransactionData["test_answer"]."', '".$arrTransactionData["payment_reason"]."', '".$arrTransactionData["service_data"]."', '".$arrTransactionData["payout_agent_id"]."')";
							
							if(insertSql($sqlInsertIntoTransaction, $payexLink))
							{
								$intTransactionId = mysql_insert_id($payexLink);
								
								//$strUpdateMiddleTierTrans = "delete from ".MTDB.".transaction where trans_id = '".$ativ."'";
								$strRemoveMiddleTierTrans = "delete from ".MTDB.".beneficiary where beneficiary_id = '".$arrTransactionData["mt_beneficiary_id"]."'";
								if(!updateSql($strRemoveMiddleTierTrans, $mtlink))
									echo "Beneficiary (".$arrTransactionData["mt_beneficiary_id"].") in Middle Tier could not deleted.";

								$strRemoveMiddleTierTrans = "delete from ".MTDB.".customer where customer_id = '".$arrTransactionData["mt_customer_id"]."'";
								if(!updateSql($strRemoveMiddleTierTrans, $mtlink))
									echo "Customer (".$arrTransactionData["mt_customer_id"].") in Middle Tier could not deleted.";
								
								/* Agent Ledger */
								$type = "WITHDRAW";
								$description = "Transaction Received";
								$actAs = "Agent";
								$note = "From CMT";
								$currencyFrom = $arrTransactionData["currency_from"];
								$agentAmount = $fltTransAmount + ($fltTotalFee * 0.25);   /* TransAmount + companycommission * 25 % */
								updateAgentAccountApi($intAgentId, $agentAmount, $intTransactionId, $type, $description, $actAs, $note, $currencyFrom, $payexLink, $payex_db);
								
								/* Distributor Ledger */
								$type = "DEPOSIT";
								$description = "Transaction Received";
								$actAs = "Distributor";
								$note = "From CMT";
								$currencyFrom = $arrTransactionData["currency_to"];
								updateAgentAccountApi($intBenAgentId, $fltLocalAmount, $intTransactionId, $type, $description, $actAs, $note, $currencyFrom, $payexLink, $payex_db);
								
								$intTotalTransactionTransferToPayex++;
							}
						}
					}
				}
				
				/* Update the external response table, with all the transactios as parsed */
				$strUpdateExternalResponseTable = "update 
														".MTDB.".external_reponse_code 
												   set
														parsed = '".$intTotalTransactionTransferToPayex."'
												   where
														id = '".$arrNewTransactionMiddleTier[$j]["id"]."'";
	
				if(!updateSql($strUpdateExternalResponseTable, $mtlink))
					echo "Error in updating the response code table summary.";
			}
			
			
			
			
			
			/**
			 * Making the XML output file for the response of the parsed files
			 * Get the transaction Upto 10, and composed into the file to SEND
			 */
			do{ 
				$bolToWriteFile = false;
				$sqlGetTransactionForResponse = SelectMultiRecordsSql("select transID, benAgentID, refNumber, refNumberIM, partner_id from $payex_db.transactions where from_server = '".$arrDbConnection["partner_name"]."' and is_parsed = 'N' limit 10 ", $payexLink);
				
				$strXmlTransactionResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
												<TMTPayoutTransResponse xmlns=\"tmt\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
				foreach($sqlGetTransactionForResponse as $trk => $trv)
				{
					if(!empty($trv["partner_id"]))
					{
						$strXmlTransactionResponse .= "
													   <Transaction>
														<partnerId>".$trv["partner_id"]."</partnerId>
														<tmtTransactionNumber>".$trv["refNumber"]."</tmtTransactionNumber>
														<partnerTransactionNumber></partnerTransactionNumber>
														<processDateTime>".date("Y-m-d")."T".date("H:i:s")."</processDateTime>
														<responseCode>10000</responseCode>
													   </Transaction>
													   ";
						
						updateSql("update $payex_db.transactions set is_parsed = 'Y' where transID = '".$trv["transID"]."'", $payexLink);
						$bolToWriteFile = true;
					}
				}
				$strXmlTransactionResponse .= "</TMTPayoutTransResponse>";
				
				$strOutputFileName = OUT_FILE.date("dmYHis").".xml";
				
				
				//do{	
					if($bolFileNotWritten);
					{
						if($bolToWriteFile)
						{
							if(!writeFileOnSftp($sftp, $strOutputFileName, $strXmlTransactionResponse))
							{	
								/* If file not written than try it again with a sllep time of 10 seconds */
								$bolFileNotWritten = true;
								echo "Unable to write '$strOutputFileName' file, on client system, trying it again in next 10 seconds.<br />";
								sleep(10);
							}
						}
					}
				//}while($bolFileNotWritten);
				
				
				
				
				/* Select transacion to see if any un parsed transaction left in table */
				$arrUnParsedTransactions = selectSql("select count(transId) as cti from $payex_db.transactions where from_server = '".$arrDbConnection["partner_name"]."' and is_parsed = 'N'", $payexLink);
				
				$bolNoMoreUnparsedTransactions = false;
				if($arrUnParsedTransactions["cti"] > 0)
				{
					$bolNoMoreUnparsedTransactions = true;
					sleep(3);				
				}
				
			}while($bolNoMoreUnparsedTransactions);
			/* XML Building response End */
			
			
			
			/* Close the payex DB connection */	
			mysql_close($payexLink);
		}
	} // closin of the files succesfully read by the sftp
	mysql_close($mtlink);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>CMT - GOPI Transactions</title>
</head>
<body>
<h1 align="center">Summary of Communication</h1>
<h2>
<?php	

	//$_SESSION["currentTime"] = $_SESSION["currentTime"] + 1;

	$strCommunicationSummary = 
	"<hr />
	Time for the Summary: ".date("Y-m-d H:i:s")."<br />
	Total Transaction Received From CMT: ".$i."<br />
	Total Transaction Saved In Middle Tier: ".$intTransactionSaved."<br />
	Total Files Parsed: ".count($allFiles)."<br />
	Total Transaction Saved IN Payex: ".$intTotalTransactionTransferToPayex;
	
	//$_SESSION["summary"] = $strCommunicationSummary. $_SESSION["summary"];
	
	echo $strCommunicationSummary;
?>	
</h2>
</body>
</html>
