<?php
	
	//$strLocalDirectory = "/var/sandbox/jalam/payex/api/gopi/remote_files/";
	
/**
 * horivert  /  Q8w$Ex2B  / 65.161.112.96
 * cpexpress /  hh)081?7  / sftp.cmtpartnernetwork.com
 */
 	//error_reporting(E_ALL);
	$connection = ssh2_connect(SFTPHOST, 22);
	if(!$connection)
		die("CON FAILED");
	
	if(!ssh2_auth_password($connection, SFTPUSER, SFTPPASSWORD))
		die("ERROR!     ...".SFTPUSER);

	if(!$sftp = ssh2_sftp($connection))
		die("Could Not Connected with SFTP!<br />");

	
	$_arrCountries = array(
			'AFG' => 'Afghanistan',
			'ALB' => 'Albania',
			'DZA' => 'Algeria',
			'ASM' => 'American Samoa',
			'AND' => 'Andorra',
			'AGO' => 'Angola',
			'AIA' => 'Anguilla',
			'ATG' => 'Antigua and Barbuda',
			'ARG' => 'Argentina',
			'ARM' => 'Armenia',
			'ABW' => 'Aruba',
			'AUS' => 'Australia',
			'AUT' => 'Austria',
			'AZE' => 'Azerbaijan',
			'BHS' => 'Bahamas',
			'BHR' => 'Bahrain',
			'BGD' => 'Bangladesh',
			'BRB' => 'Barbados',
			'BLR' => 'Belarus',
			'BEL' => 'Belgium',
			'BLZ' => 'Belize',
			'BEN' => 'Benin',
			'BMU' => 'Bermuda',
			'BTN' => 'Bhutan',
			'BOL' => 'Bolivia',
			'BIH' => 'Bosnia and Herzegovina',
			'BWA' => 'Botswana',
			'BRA' => 'Brazil',
			'VGB' => 'British Virgin Islands',
			'BRN' => 'Brunei Darussalam',
			'BGR' => 'Bulgaria',
			'BFA' => 'Burkina Faso',
			'BDI' => 'Burundi',
			'KHM' => 'Cambodia',
			'CMR' => 'Cameroon',
			'CAN' => 'Canada',
			'CPV' => 'Cape Verde',
			'CYM' => 'Cayman Islands',
			'CAF' => 'Central African Republic',
			'TCD' => 'Chad',
			'CHL' => 'Chile',
			'CHN' => 'China',
			'COL' => 'Colombia',
			'COM' => 'Comoros',
			'COG' => 'Congo',
			'COK' => 'Cook Islands',
			'CRI' => 'Costa Rica',
			'CIV' => 'Cote d\'Ivoire',
			'HRV' => 'Croatia',
			'CUB' => 'Cuba',
			'CYP' => 'Cyprus',
			'CZE' => 'Czech Republic',
			'PRK' => 'Democratic People\'s Republic of Korea',
			'COD' => 'Democratic Republic of the Congo',
			'DNK' => 'Denmark',
			'DJI' => 'Djibouti',
			'DMA' => 'Dominica',
			'DOM' => 'Dominican Republic',
			'ECU' => 'Ecuador',
			'EGY' => 'Egypt',
			'SLV' => 'El Salvador',
			'GNQ' => 'Equatorial Guinea',
			'ERI' => 'Eritrea',
			'EST' => 'Estonia',
			'ETH' => 'Ethiopia',
			'EUR' => 'Europe',
			'FRO' => 'Faeroe Islands',
			'FLK' => 'Falkland Islands (Malvinas)',
			'FJI' => 'Fiji',
			'FIN' => 'Finland',
			'FRA' => 'France',
			'GUF' => 'French Guiana',
			'PYF' => 'French Polynesia',
			'GAB' => 'Gabon',
			'GMB' => 'Gambia',
			'GEO' => 'Georgia',
			'DEU' => 'Germany',
			'GHA' => 'Ghana',
			'GIB' => 'Gibraltar',
			'GRC' => 'Greece',
			'GRL' => 'Greenland',
			'GRD' => 'Grenada',
			'GLP' => 'Guadeloupe',
			'GUM' => 'Guam',
			'GTM' => 'Guatemala',
			'GIN' => 'Guinea',
			'GNB' => 'Guinea-Bissau',
			'GUY' => 'Guyana',
			'HTI' => 'Haiti',
			'VAT' => 'Holy See',
			'HND' => 'Honduras',
			'HKG' => 'Hong Kong',
			'HUN' => 'Hungary',
			'ISL' => 'Iceland',
			'IND' => 'India',
			'IDN' => 'Indonesia',
			'IRN' => 'Iran',
			'IRQ' => 'Iraq',
			'IRL' => 'Ireland',
			'ISR' => 'Israel',
			'ITA' => 'Italy',
			'JAM' => 'Jamaica',
			'JPN' => 'Japan',
			'JOR' => 'Jordan',
			'KAZ' => 'Kazakhstan',
			'KEN' => 'Kenya',
			'KIR' => 'Kiribati',
			'KWT' => 'Kuwait',
			'KGZ' => 'Kyrgyzstan',
			'LAO' => 'Lao People\'s Democratic Republic',
			'LVA' => 'Latvia',
			'LBN' => 'Lebanon',
			'LSO' => 'Lesotho',
			'LBR' => 'Liberia',
			'LBY' => 'Libyan Arab Jamahiriya',
			'LIE' => 'Liechtenstein',
			'LTU' => 'Lithuania',
			'LUX' => 'Luxembourg',
			'MAC' => 'Macao Special Administrative Region of China',
			'MDG' => 'Madagascar',
			'MWI' => 'Malawi',
			'MYS' => 'Malaysia',
			'MDV' => 'Maldives',
			'MLI' => 'Mali',
			'MLT' => 'Malta',
			'MHL' => 'Marshall Islands',
			'MTQ' => 'Martinique',
			'MRT' => 'Mauritania',
			'MUS' => 'Mauritius',
			'MEX' => 'Mexico',
			'FSM' => 'Micronesia Federated States of,',
			'MCO' => 'Monaco',
			'MNG' => 'Mongolia',
			'MSR' => 'Montserrat',
			'MAR' => 'Morocco',
			'MOZ' => 'Mozambique',
			'MMR' => 'Myanmar',
			'NAM' => 'Namibia',
			'NRU' => 'Nauru',
			'NPL' => 'Nepal',
			'NLD' => 'Netherlands',
			'ANT' => 'Netherlands Antilles',
			'NCL' => 'New Caledonia',
			'NZL' => 'New Zealand',
			'NIC' => 'Nicaragua',
			'NER' => 'Niger',
			'NGA' => 'Nigeria',
			'NIU' => 'Niue',
			'NFK' => 'Norfolk Island',
			'MNP' => 'Northern Mariana Islands',
			'NOR' => 'Norway',
			'PSE' => 'Occupied Palestinian Territory',
			'OMN' => 'Oman',
			'PAK' => 'Pakistan',
			'PLW' => 'Palau',
			'PAN' => 'Panama',
			'PNG' => 'Papua New Guinea',
			'PRY' => 'Paraguay',
			'PER' => 'Peru',
			'PHL' => 'Philippines',
			'PCN' => 'Pitcairn',
			'POL' => 'Poland',
			'PRT' => 'Portugal',
			'PRI' => 'Puerto Rico',
			'QAT' => 'Qatar',
			'KOR' => 'Republic of Korea',
			'MDA' => 'Republic of Moldova',
			'REU' => 'Runion',
			'ROU' => 'Romania',
			'RUS' => 'Russian Federation',
			'RWA' => 'Rwanda',
			'SHN' => 'Saint Helena',
			'KNA' => 'Saint Kitts and Nevis',
			'LCA' => 'Saint Lucia',
			'SPM' => 'Saint Pierre and Miquelon',
			'VCT' => 'Saint Vincent and the Grenadines',
			'WSM' => 'Samoa',
			'SMR' => 'San Marino',
			'STP' => 'Sao Tome and Principe',
			'SAU' => 'Saudi Arabia',
			'SEN' => 'Senegal',
			'YUG' => 'Serbia and Montenegro',
			'SYC' => 'Seychelles',
			'SLE' => 'Sierra Leone',
			'SGP' => 'Singapore',
			'SVK' => 'Slovakia',
			'SVN' => 'Slovenia',
			'SLB' => 'Solomon Islands',
			'SOM' => 'Somalia',
			'ZAF' => 'South Africa',
			'ESP' => 'Spain',
			'LKA' => 'Sri Lanka',
			'SDN' => 'Sudan',
			'SUR' => 'Suriname',
			'SJM' => 'Svalbard and Jan Mayen Islands',
			'SWZ' => 'Swaziland',
			'SWE' => 'Sweden',
			'CHE' => 'Switzerland',
			'SYR' => 'Syrian Arab Republic',
			'TWN' => 'Taiwan',
			'TJK' => 'Tajikistan',
			'THA' => 'Thailand',
			'MKD' => 'The former Yugoslav Republic of Macedonia',
			'TLS' => 'Timor-Leste',
			'TGO' => 'Togo',
			'TKL' => 'Tokelau',
			'TON' => 'Tonga',
			'TTO' => 'Trinidad and Tobago',
			'TUN' => 'Tunisia',
			'TUR' => 'Turkey',
			'TKM' => 'Turkmenistan',
			'TCA' => 'Turks and Caicos Islands',
			'TUV' => 'Tuvalu',
			'UGA' => 'Uganda',
			'UKR' => 'Ukraine',
			'ARE' => 'United Arab Emirates',
			'GBR' => 'United Kingdom',
			'TZA' => 'United Republic of Tanzania',
			'USA' => 'United States',
			'VIR' => 'United States Virgin Islands',
			'XXX' => 'Unknown',
			'URY' => 'Uruguay',
			'UZB' => 'Uzbekistan',
			'VUT' => 'Vanuatu',
			'VEN' => 'Venezuela',
			'VNM' => 'Viet Nam',
			'WLF' => 'Wallis and Futuna Islands',
			'ESH' => 'Western Sahara',
			'YEM' => 'Yemen',
			'ZMB' => 'Zambia',
			'ZWE' => 'Zimbabwe',
			);	
	
	class multidi_array2xml {
		/**
		 * Parse multidimentional array to XML.
		 *
		 * @param array $array
		 * @return string    XML
		 */
		var $XMLtext;
		
		public function array2xml($array, $output=true) {
			//star and end the XML document
			$this->XMLtext="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<array>\n";
			$this->array_transform($array);
			$this->XMLtext .="</array>";
			if($output) return $this->XMLtext;
		}
		public function SaveXml($src){
			$myFile = "testFile.txt";
			$fh = @fopen($src, 'w');
			if($fh){
				fwrite($fh, $this->XMLtext);
				fclose($fh);
				return true;
			}else {
				return false;
			}
			
		}
		private function array_transform($array){
			static $Depth;
	
			foreach($array as $key => $value){
				if(!is_array($value)){
					unset($Tabs);
					for($i=1;$i<=$Depth+1;$i++) $Tabs .= "\t";
					if(preg_match("/^[0-9]\$/",$key)) $key = "n$key";
					$this->XMLtext .= "$Tabs<$key>$value</$key>\n";
				} else {
					$Depth += 1;
					unset($Tabs);
					for($i=1;$i<=$Depth;$i++) $Tabs .= "\t";
					//search for atribut like [name]-ATTR to put atributs to some object
					if(!preg_match("/(-ATTR)\$/", $key)) {
						if(preg_match("/^[0-9]\$/",$key)) $keyval = "n$key"; else $keyval = $key;
						$closekey = $keyval;
						if(is_array($array[$key."-ATTR"])){
							foreach ($array[$key."-ATTR"] as $atrkey => $atrval ) $keyval .= " ".$atrkey."=\"$atrval\"";
						} 
						$this->XMLtext.="$Tabs<$keyval>\n";
						$this->array_transform($value);
						$this->XMLtext.="$Tabs</$closekey>\n";
						$Depth -= 1;
						
					}
				}
			}
			return true;
		}
	}
	


	function connect_to_sftp($host, $user, $passwd, $port = 22)
	{
		//debug_print_backtrace();
		$sftp = "";
		$connection = ssh2_connect($host, $port);
		if(!$connection)
			die("Could not establishe connection with host $host.");
		if(!ssh2_auth_password($connection, $user, $passwd))
			die("Authentication failed for user $user.");
	
		if($sftp = ssh2_sftp($connection) == false)
			die("Could Not Connected with SFTP.");
		//debug($sftp);	
		return $sftp;
	}
	
	function getFileContents($sftp, $fileName)
	{
		$data = "";
		if(!($stream = fopen("ssh2.sftp://$sftp/Output/$fileName", 'r')))
		{
			echo "fail: unable to execute command\n";
			return false;
		} 
		else
		{
			stream_set_blocking( $stream, true );
			while( $buf = fread($stream,4096) ){
				$data .= $buf;
			}
			fclose($stream);
		}
		return $data;
	}
	
	function receiveFile($remote_file, $local_file)
    {
        $stream = @fopen($remote_file, 'r');
        if (! $stream)
            throw new Exception("Could not open file: $remote_file");
        $size = getFileSize($remote_file);           
        $contents = '';
        $read = 0;
        $len = $size;
        while ($read < $len && ($buf = fread($stream, $len - $read))) {
          $read += strlen($buf);
          $contents .= $buf;
        }       
        file_put_contents($local_file, $contents);
        @fclose($stream);
    }
	
	function getFileSize($file){
        return filesize($file);
    }
	
	function getFileList($sftp, $filesWithWildCard = "")
	{
		//debug($sftp);
		$directory = "ssh2.sftp://$sftp/Output/";
		
		$data = "";
		
		if(empty($filesWithWildCard))
			$filesWithWildCard = "130_TMTPOT_".date("dmYh")."*";
		
		//debug($directory.$filesWithWildCard);
		
		if($handle = opendir($directory.$filesWithWildCard)) 
		{
		   $files = array();
		   while(($file = @readdir($handle)) !== false) 
		   {
			   	if($file != "." && $file != "..")
			   		$files[] = $directory.$file; 
				  //receiveFile($sftp, $directory.$file, $strLocalDirectory.$file);
		   }
			
		   closedir($handle);
		   return $files;
		}
		else
			return false;
	}
	
	
	function copyAllFilesOnLocalMachine($arrFiles)
	{
		foreach($arrFiles as $fK => $fV)
		{
			//$localFile = substr($fV,strpos($fV,"_TMT"),strlen($fV));
			$localFile = substr(strrchr($fV, "/"), 1);
			receiveFile($fV, LOCAL_OUTPUT_DIR.$localFile);
		}
		return true;
	}
	
	function readFileSftp($strFullFileName)
	{

		if(!($stream = fopen($strFullFileName, 'r'))){
			return false;//echo "fail: unable to execute command\n";
		} else{
			stream_set_blocking( $stream, true );
			while( $buf = fread($stream,4096) ){
				$data .= $buf;
			}
			fclose($stream);
			//debug($data);
		}
		return $data;
	}
	
	
	function getFileListOnLocalOutputDir($filesWithWildCard = "")
	{
		$directory = LOCAL_OUTPUT_DIR;
		$data = "";
		
		if(empty($filesWithWildCard))
			$filesWithWildCard = "130_TMTPOT_".date("dmYh")."*";
		
		if($handle = opendir($directory.$filesWithWildCard)) 
		{
		   $files = array();
		   while(($file = @readdir($handle)) !== false) 
		   {
			   	if($file != "." && $file != "..")
			   		$files[] = $directory.$file; 
		   }
			
		   closedir($handle);
		   return $files;
		}
		else
		{
			echo $directory.$filesWithWildCard;
			return false;
		}
	}
		
	function readFileLocalMachine()
	{
		if(!($stream = fopen($strFullFileName, 'r')))
			echo "fail: unable to execute command\n";
		else
		{
			stream_set_blocking( $stream, true );
			while( $buf = fread($stream,4096) ){
				$data .= $buf;
			}
			fclose($stream);
		}
		return $data;
	}
	
	function writeFileOnSftp($sftp, $strFullFileName, $strXml)
	{
		$ret = false;
		/* Also write it on local machine */
		writeOnLocalMachine($strFullFileName, $strXml);
		
		$portocolWithFileName = "ssh2.sftp://$sftp/Input/".$strFullFileName;
		//debug($portocolWithFileName);
		
		if(CONFIG_REMOVE_OR_PUT_FILES_WITH_REMOTE_SERVER == "0")
			return true;
		
		if($fw = fopen($portocolWithFileName, 'w'))
		{
			if(fwrite($fw,$strXml))
			{
				$ret = true; 
				//echo "File Written! (".$portocolWithFileName.")<br />";
			}
			else
			{
				$ret = false; 
				//echo "Error in write! (".$portocolWithFileName.")<br />";
			}
			fclose($fw);
		}
		else
			die("Could not write files on remote server.");
		//debug("ER!");
		//debug("",true);
		return $ret;
	}
	
	
	function writeOnLocalMachine($strFullFileName, $strXml)
	{
		$ret = false;
		$portocolWithFileName = LOCAL_INPUT_DIR.$strFullFileName;
		if($fw = fopen($portocolWithFileName, 'w+'))
		{
			if(fwrite($fw,$strXml))
				$ret = true;//echo "File local Written! (".$portocolWithFileName.")<br />";
			else
				$ret = false;//echo "Error in local write! (".$portocolWithFileName.")<br />";
				
			fclose($fw);
		}
		return $ret;
	}
	
	function xmlToArray($strXml, $get_attributes = 1, $priority = 'tag')
	{
		$contents = $strXml;
		if (!function_exists('xml_parser_create'))
		{
			return array ();
		}
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);
		if (!$xml_values)
			return;
		$xml_array = array ();
		$parents = array ();
		$opened_tags = array ();
		$arr = array ();
		$current = & $xml_array;
		$repeated_tag_index = array ();
		foreach ($xml_values as $data)
		{
			unset ($attributes, $value);
			extract($data);
			$result = array ();
			$attributes_data = array ();
			if (isset ($value))
			{
				if ($priority == 'tag')
					$result = $value;
				else
					$result['value'] = $value;
			}
			if (isset ($attributes) and $get_attributes)
			{
				foreach ($attributes as $attr => $val)
				{
					if ($priority == 'tag')
						$attributes_data[$attr] = $val;
					else
						$result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}
			if ($type == "open")
			{
				$parent[$level -1] = & $current;
				if (!is_array($current) or (!in_array($tag, array_keys($current))))
				{
					$current[$tag] = $result;
					if ($attributes_data)
						$current[$tag . '_attr'] = $attributes_data;
					$repeated_tag_index[$tag . '_' . $level] = 1;
					$current = & $current[$tag];
				}
				else
				{
					if (isset ($current[$tag][0]))
					{
						$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
						$repeated_tag_index[$tag . '_' . $level]++;
					}
					else
					{
						$current[$tag] = array (
							$current[$tag],
							$result
						);
						$repeated_tag_index[$tag . '_' . $level] = 2;
						if (isset ($current[$tag . '_attr']))
						{
							$current[$tag]['0_attr'] = $current[$tag . '_attr'];
							unset ($current[$tag . '_attr']);
						}
					}
					$last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
					$current = & $current[$tag][$last_item_index];
				}
			}
			elseif ($type == "complete")
			{
				if (!isset ($current[$tag]))
				{
					$current[$tag] = $result;
					$repeated_tag_index[$tag . '_' . $level] = 1;
					if ($priority == 'tag' and $attributes_data)
						$current[$tag . '_attr'] = $attributes_data;
				}
				else
				{
					if (isset ($current[$tag][0]) and is_array($current[$tag]))
					{
						$current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
						if ($priority == 'tag' and $get_attributes and $attributes_data)
						{
							$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag . '_' . $level]++;
					}
					else
					{
						$current[$tag] = array (
							$current[$tag],
							$result
						);
						$repeated_tag_index[$tag . '_' . $level] = 1;
						if ($priority == 'tag' and $get_attributes)
						{
							if (isset ($current[$tag . '_attr']))
							{
								$current[$tag]['0_attr'] = $current[$tag . '_attr'];
								unset ($current[$tag . '_attr']);
							}
							if ($attributes_data)
							{
								$current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag . '_' . $level]++; //0 and 1 index is already taken
					}
				}
			}
			elseif ($type == 'close')
			{
				$current = & $parent[$level -1];
			}
		}
		return ($xml_array);

	}
	
	function validateXmlFromFile($strFullXml, $strXsdPath)
	{
		$xml = new DOMDocument();
		$xml->LoadXml($strFullXml);
		
		//$doc = simplexml_load_string($strFullfilePath);
		//$xmlErr = explode("\n", $xmlstr);
		
		if (!$xml->schemaValidate($strXsdPath))
		{
			/*
			$errors = libxml_get_errors();
			
			foreach ($errors as $error) {
    		   echo display_xml_error($error, $xmlErr);
			}
		    libxml_clear_errors();
			*/
			return false;
		}
		else
			return true;
		
	}

	function display_xml_error($error, $xml)
	{
	   $return  = $xml[$error->line - 1] . "\n";
	   $return .= str_repeat('-', $error->column) . "^\n";
	
	   switch ($error->level) {
		   case LIBXML_ERR_WARNING:
			   $return .= "Warning $error->code: ";
			   break;
			case LIBXML_ERR_ERROR:
			   $return .= "Error $error->code: ";
			   break;
		   case LIBXML_ERR_FATAL:
			   $return .= "Fatal Error $error->code: ";
			   break;
	   }
	
	   $return .= trim($error->message) .
				  "\n  Line: $error->line" .
				  "\n  Column: $error->column";
	
	   if ($error->file) {
		   $return .= "\n  File: $error->file";
	   }
	
	   return "$return\n\n--------------------------------------------\n\n";
	}


	function selectSql($sql, $objDbLink)
	{ 
		/*echo "<pre>";
		debug_print_backtrace();
		echo "<pre>"; */
		if((@$result = mysql_query ($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);		
		}   
		else
		{	
			if ($check=mysql_fetch_array($result, MYSQL_BOTH))
				return $check;

			return false;	
		}
	}

	function insertSql($sql, $objDbLink)
	{
		if((@$result = mysql_query($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);
			return false; 
		}   
		else
			return true;	
	}

	function updateSql($sql, $objDbLink)
	{
		/*echo "<pre>";
		debug_print_backtrace();
		echo "<pre>"; */
		if((@$result = mysql_query($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);		
			return false; 
		}   
		else
			return true;
	}

	function SelectMultiRecordsSql($sql, $objDbLink)
	{   
		if((@$result = mysql_query ($sql, $objDbLink))==FALSE)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);
			return false;	
		}   
		else
		{	
			$count = 0;
			$data = array();
			while ( $row = mysql_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
			return $data;
		}
	}
	
	function mysql_sql_message($sql)
	{
		/*echo "<pre>";
		debug_print_backtrace();
		echo "<pre>"; */
		$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>$company MySql Debugger:</font></strong><br>";
		$msg .= "Error in your Query: $sql<BR>";
		$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
		$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
		echo $msg;
	}

	function debugVar( $variableToDebug = "", $shouldExitAfterDebug = false)
	{
		if ( defined("DEBUG_ON")
			 && DEBUG_ON == true )
		{
			$arrayFileDetails = debug_backtrace();
			echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
		
			if ( !empty($variableToDebug)  )
			{
				echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
			}
			else
			{
				echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
			}
			echo "</pre>";
			if ($shouldExitAfterDebug)
				exit();
		}
	}
	
	
	
	function updateAgentAccountApi($agentID, $amount, $transID, $type, $description, $actAs,$note,$currencyFrom, $dbLink, $db, $today='0000-00-00')
	{
		//debug_print_backtrace(); 
		
		$agentQuery = selectSql("select balance, isCorrespondent from $db.admin where userID = '".$agentID."'", $dbLink);
		$isCorresspondent = $agentQuery["isCorrespondent"];
		
		if($isCorresspondent != 'N')
		{
			$agentType = "Distributor";
			
			if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
				$agentType = "Both";///AnD
		}
		elseif($isCorresspondent == 'N')
			$agentType = "Agent";
		
		if($today == '0000-00-00')
			$today = date("Y-m-d"); 
			
		$loginID  = $agentID;
		$currencyLedger = $currencyFrom;

		$transData = selectSql("select settlementCurrency,settlementValue from $db.transactions where transID = '".$transID."'", $dbLink);
		
		if($agentType == 'Agent')
		{
			$accountQuery = "insert into $db.agent_account 
			(agentID, dated, type, amount, modified_by, TransID, description,note,currency) values
			('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description','$note','".$currencyLedger."')";	
		}
		elseif($agentType == 'Distributor')
		{
			$accountQuery = "insert into $db.bank_account
			(bankID, dated, type, amount, modified_by, TransID, description,currency) values
			('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description','".$currencyLedger."')";
		}
		elseif($agentType == "Both")
		{
			$accountQuery = "insert into $db.agent_Dist_account 
			(agentID, dated, type, amount, modified_by, TransID, description, actAs,note,currency) values
			('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs','$note','".$currencyLedger."')";
		}
		
		if(insertSql($accountQuery, $dbLink))
		{	
			agentSummaryAccountApi($agentID, $type, $amount, $dbLink, $db, $currencyLedger, $settlementAmount);
			
			$currentBalance = $agentQuery["balance"];
			if($type == 'DEPOSIT')
				$currentBalance += $amount;
			else
				$currentBalance -= $amount;
	
			updateSql("update $db.admin set balance = $currentBalance where userID = '".$agentID."'", $dbLink);

			return true;
		}
		return false;
	}
	
	
	function agentSummaryAccountApi($agentID, $type, $amount, $dbLink, $db, $currency="", $settlementAmount="")
	{
		//debug_print_backtrace();
		$today = date("Y-m-d"); 
		$agentContents = selectSql("select id, user_id, dated, opening_balance, closing_balance from $db.account_summary where user_id = '$agentID' and dated = '$today' ", $dbLink);
				
		if($agentContents["user_id"] != "")
		{
			$balance = $agentContents["closing_balance"];
			if($type == 'DEPOSIT')
				$balance = $balance + $amount;	
			else
				$balance = $balance - $amount;	

			if($currency!="" && $getAnDActAsRecord!="" )
				selectSql("insert into $db.account_summary (dated, user_id, opening_balance, closing_balance,currency) values('$today', '$agentID', '".$agentContents["closing_balance"]."', '".$balance."','".$currency."')", $dbLink);
			else
				updateSql("update $db.account_summary set closing_balance = '$balance',currency = '$currency' where id = '".$agentContents["id"]."'", $dbLink);			
		 
		}
		else
		{
			if($currency!="" && $getAnDActAsRecord!="" )
			{
				$summaryLast = selectSql("select  Max(id) as lastID from $db.account_summary where user_id = '$agentID'  and currency = '".$currency."' order by id desc", $dbLink);	
				$agentContents = selectSql("select user_id, dated, opening_balance, closing_balance,currency  from $db.account_summary where user_id = '$agentID' and id = '".$summaryLast["lastID"]."'", $dbLink);
			}
			else
			{
				$datesLast = selectSql("select  Max(dated) as dated from $db.account_summary where user_id = '$agentID' ", $dbLink);	
				$agentContents = selectSql("select user_id, dated, opening_balance, closing_balance,currency  from $db.account_summary where user_id = '$agentID' and dated = '".$datesLast["dated"]."'", $dbLink);
				
			} 
			$balance = 0;
			
			$balance += $agentContents["closing_balance"];
			
			$openingBalance = $balance;
			
			if($type == 'DEPOSIT')
				$closingBalance = $balance + $amount;	
			else
				$closingBalance = $balance - $amount;	

			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") 
			{
				insertSql("insert into $db.account_summary (dated, user_id, opening_balance, closing_balance,currency ) values('$today', '$agentID', '$openingBalance', '$closingBalance','$currency')", $dbLink);
			}
			else if($currency!="" && $getAnDActAsRecord!="" )
			{
				insertSql("insert into $db.account_summary (dated, user_id, opening_balance, closing_balance,currency) values('$today', '$agentID', '$openingBalance', '$closingBalance','".$currency."')", $dbLink);
			}
			else
			{
				insertSql("insert into $db.account_summary (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')", $dbLink);
			}
			
		}	
	}
	
?>
