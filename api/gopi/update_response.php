<?php
	//session_start();
	
	define("LOCAL_DIR", "/var/sandbox/cmtapi/");
	include(LOCAL_DIR."include/masterConfig.php");
	require("api_functions.php");

	$mtlink = mysql_connect(MTHOST, MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");
	
	//$srtFilesWildCard = "*_TMTUR_".date("dmYH").substr(date("i"),0,1)."*";
	$srtFilesWildCard = "*_TMTUR_*";
	
	$arrFilesForSummary = array();
	$totalTransactionInput = 0;
	$totalRequestReturn = 0;
	$totalRequestRejected = 0;
	$tmtIds = "";
	
	
	if($allFiles = getFileList($sftp, $srtFilesWildCard))
	{
		//die("Error in reading remote directory! ".$srtFilesWildCard);
		//debug($allFiles);
		
		if(!copyAllFilesOnLocalMachine($allFiles))
			die("Could not copy the remote files into local directory.");
		
		$i = 0;
		
		/* making connection with Payex DB */
		$arrDbConnection = selectSql("select * from ".MTDB.".client where payex_client_id  = '1'", $mtlink);
		$payexLink = mysql_connect($arrDbConnection["client_host"], $arrDbConnection["client_user"], $arrDbConnection["client_pwd"]);
		if(!mysql_select_db($arrDbConnection["client_database"], $payexLink))
			die("Could not connect to ".$arrDbConnection["client_database"]." database.");
		
		$payex_db = $arrDbConnection["client_database"];
		
		$strXmlTransactionResponse = "";
		
		foreach($allFiles as $fK => $fV)
		{
			$arrayFileContents = readFileSftp($fV);
			
			if($arrayFileContents) /* If data is read successfully */
			{
			
				//debug($arrayFileContents);
		
				/* Validate the xml file 
				if(!validateXmlFromFile($arrayFileContents, TMTUR))
					die(debugVar($arrayFileContents));
				*/
				$arrayFromXml = xmlToArray($arrayFileContents);
				//debugVar($arrayFromXml, true);
				
				$arrayOfXmlFromFile = $arrayFromXml["TMTPayoutUpdateRequest"]["Transaction"];
				if(!is_array($arrayOfXmlFromFile[0]))
					$arrayOfXmlFromFile = $arrayFromXml["TMTPayoutUpdateRequest"];
				
				foreach($arrayOfXmlFromFile as $av => $ak)
				{
						$sqlGetTransactionForResponse = selectSql("select transStatus, transType, transID, refNumber, refNumberIM, custAgentID, benAgentID, partner_id, localAmount, totalAmount, currencyFrom, currencyTo from $payex_db.transactions where from_server = '".$arrDbConnection["partner_name"]."' and is_parsed = 'Y' and refNumber='".$ak["tmtTransactionNumber"]."' limit 1 ", $payexLink);
						
						$tmtIds .= $ak["tmtTransactionNumber"].",";
							
						if(!empty($sqlGetTransactionForResponse["transID"]))
						{
							
							$strTransStatus = $sqlGetTransactionForResponse["transStatus"];
							$intResposeCode = "";
							
							$intPartnerId = $sqlGetTransactionForResponse["partner_id"];
							if(empty($intPartnerId))
								$intPartnerId = CONFIG_API_PARTNER_ID;
							
							/**
							 * Making the XML output file for the response of the parsed files
							 
								30000 Update successfully carried out
								30001 Update refused - transaction paid
								30002 Update refused - transaction cancelled
								30003 Update refused - void transaction
								30004 Update refused - transaction rejected
								30005 Update refused - unable to cancel (Update type 02 only)
								39000 Unable to process - please contact
								39001 Invalid CMT number
								39002 Invalid partner transaction number
								39003 Invalid request type
							*/
							if($sqlGetTransactionForResponse["transType"] == "Bank Transfer")
								$intResposeCode = "30005";
							else
							{
								if($strTransStatus == "Authorize")
									$intResposeCode = "30000";
								elseif($strTransStatus == "Picked up")
									$intResposeCode = "30001";
								elseif($strTransStatus == "AwaitingCancellation")
									$intResposeCode = "30000";
								elseif($strTransStatus == "Cancelled")
									$intResposeCode = "30002";
								elseif($strTransStatus == "Rejected")
									$intResposeCode = "30004";
								elseif($strTransStatus == "Cancelled - Returned")
									$intResposeCode = "30002";
								elseif($strTransStatus == "Suspicious")
									$intResposeCode = "39000";
							}			
									
							if($intResposeCode == "30000")
							{
								//, cancelDate = '".date("Y-m-d H:i:s")."'
								updateSql("update $payex_db.transactions set transStatus = 'AwaitingCancellation', holdedBy = '".$arrDbConnection["partner_name"]."' where transID = '".$sqlGetTransactionForResponse["transID"]."'", $payexLink);
								
								/*
								$description = "Transaction Cancelled Remotly";
								
								// Agent Ledger 
								$type = "DEPOSIT";
								$actAs = "Agent";
								$note = "By CMT";
								$currencyFrom = $sqlGetTransactionForResponse["currencyFrom"];
								$amount = $sqlGetTransactionForResponse["totalAmount"];
								updateAgentAccountApi($sqlGetTransactionForResponse["custAgentID"], $amount, $sqlGetTransactionForResponse["transID"], $type, $description, $actAs, $note, $currencyFrom, $payexLink, $payex_db);
								
								// Distributor Ledger 
								$type = "WITHDRAW";
								$actAs = "Distributor";
								$note = "By CMT";
								$currencyFrom = $sqlGetTransactionForResponse["currencyTo"];
								$amount = $sqlGetTransactionForResponse["localAmount"];
								updateAgentAccountApi($sqlGetTransactionForResponse["benAgentID"], $amount, $sqlGetTransactionForResponse["transID"], $type, $description, $actAs, $note, $currencyFrom, $payexLink, $payex_db);
								*/
							}
							else
							{
								$strXmlTransactionResponse .= "
									<Transaction>
										<tmtTransactionNumber>".$sqlGetTransactionForResponse["refNumber"]."</tmtTransactionNumber>
										<updateType>01</updateType>
										<responseCode>".$intResposeCode."</responseCode>
										<processDateTime>".date("Y-m-d\TH:i:s")."</processDateTime>
										<partnerId>".$intPartnerId."</partnerId>
									</Transaction>
									";							
							}
				
							$totalRequestReturn++;
						}
						else
						{
							$strXmlTransactionResponse .= "
									<Transaction>
										<tmtTransactionNumber>".$ak["tmtTransactionNumber"]."</tmtTransactionNumber>
										<updateType>01</updateType>
										<responseCode>39001</responseCode>
										<processDateTime>".date("Y-m-d\TH:i:s")."</processDateTime>
										<partnerId>".CONFIG_API_PARTNER_ID."</partnerId>
									</Transaction>
									";
							$totalRequestRejected++;
						}
	
					$totalTransactionInput++;
				}
				
				if(defined("CONFIG_REMOVE_OR_PUT_FILES_WITH_REMOTE_SERVER") && CONFIG_REMOVE_OR_PUT_FILES_WITH_REMOTE_SERVER == "1")
				{
					/* Remove the read file , on the remote server  */
					if(unlink($fV))
						echo "File Removed ($fV)<br />";
					else
						echo "Unable to Removed File ($fV)<br />"; 
				}
			}
			
		}
		
		if(!empty($strXmlTransactionResponse))
		{
					
			$strXmlTransactionResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
												<TMTPayoutUpdateRequestResponse xmlns=\"tmt\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
													".$strXmlTransactionResponse."
												</TMTPayoutUpdateRequestResponse>";
					
			$strOutputFileName = PID."TMTURR_".date("dmYHis").".xml";

			//if(!validateXmlFromFile($strXmlTransactionResponse, TMTURR))
				//die(debugVar($strXmlTransactionResponse));
			//do{	
				$bolFileNotWritten = false;
				if(!writeFileOnSftp($sftp, $strOutputFileName, $strXmlTransactionResponse))
				{	
					// If file not written than try it again with a sllep time of 10 seconds 
					$bolFileNotWritten = true;
					echo "Unable to write '$strOutputFileName' file, on client system, trying it again in next 5 seconds.<br />";
					sleep(5);
				}
				$strXmlTransactionResponse = "";
			//}while($bolFileNotWritten);
		}

		/* XML Building response End */	
	
		
		
		/* Feeding the middle tier table for Summary */
		$strSummaryInsert = "insert into ".MTDB.".external_reponse_code
							(client_id, total_transactions, parsed, rejected, files, datetime, trans_ids, interaction_step)
							values
							('1', '".$totalTransactionInput."', '".$totalRequestReturn."', '".$totalRequestRejected."', '".serialize($allFiles)."', '".time()."', '".$tmtIds."', '5')	";
		
		if(!insertSql($strSummaryInsert, $mtlink))
			echo "Error in summary tale entry.";
		
		
		/* Close the payex DB connection */	
		mysql_close($payexLink);
	}
	mysql_close($mtlink);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>CMT - Transactions Update Response</title>
</head>
<body>
<h1 align="center">Summary of Communication</h1>
<h2>
<?php	

	$strCommunicationSummary = 
	"<hr />
	Time for the Summary: ".date("Y-m-d H:i:s")."<br />
	Total Update Requests Received From CMT: ".$i."<br />
	Total Files Parsed: ".count($allFiles)."<br />";
	
	echo $strCommunicationSummary;
?>	
</h2>
</body>
</html>