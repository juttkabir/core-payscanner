<?php
	function selectSql($sql, $objDbLink)
	{ 
		/*echo "<pre>";
		debug_print_backtrace();
		echo "<pre>"; */
		if((@$result = mysql_query ($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);		
		}   
		else
		{	
			if ($check=mysql_fetch_array($result, MYSQL_BOTH))
				return $check;

			return false;	
		}
	}

	function insertSql($sql, $objDbLink)
	{
		if((@$result = mysql_query($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);
			return false; 
		}   
		else
			return true;	
	}

	function updateSql($sql, $objDbLink)
	{
		/*echo "<pre>";
		debug_print_backtrace();
		echo "<pre>"; */
		if((@$result = mysql_query($sql, $objDbLink)) == false)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);		
			return false; 
		}   
		else
			return true;
	}

	function SelectMultiRecordsSql($sql, $objDbLink)
	{   
		if((@$result = mysql_query ($sql, $objDbLink))==FALSE)
		{
			if(defined("DEBUG_ON"))
				echo mysql_sql_message($sql);
			return false;	
		}   
		else
		{	
			$count = 0;
			$data = array();
			while ( $row = mysql_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
			return $data;
		}
	}
	
	function mysql_sql_message($sql)
	{
		$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>$company MySql Debugger:</font></strong><br>";
		$msg .= "Error in your Query: $sql<BR>";
		$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
		$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
		echo $msg;
	}

	function debugVar( $variableToDebug = "", $shouldExitAfterDebug = false)
	{
		if ( defined("DEBUG_ON")
			 && DEBUG_ON == true )
		{
			$arrayFileDetails = debug_backtrace();
			echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
		
			if ( !empty($variableToDebug)  )
			{
				echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
			}
			else
			{
				echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
			}
			echo "</pre>";
			if ($shouldExitAfterDebug)
				exit();
		}
	}
?>