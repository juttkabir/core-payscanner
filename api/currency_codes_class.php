<?php

/* UPDATED: 28 Aug 2011 */

$iso = array(
	'currencies'=>array(
		'aed'=>array(
			'name'=>'UAE Dirham'),
		'afn'=>array(
			'name'=>'Afghani',
			'symbol'=>'?'),
		'all'=>array(
			'name'=>'Lek',
			'symbol'=>'Lek'),
		'amd'=>array(
			'name'=>'Armenian Dram',
			'symbol'=>'?'),
		'ang'=>array(
			'name'=>'Netherlands Antillian Guilder',
			'symbol'=>'�'),
		'aoa'=>array(
			'name'=>'Kwanza',
			'symbol'=>'Kz'),
		'ars'=>array(
			'name'=>'Argentine Peso',
			'symbol'=>'$'),
		'aud'=>array(
			'name'=>'Australian Dollar',
			'symbol'=>'$',
			'country'=>'au'),
		'awg'=>array(
			'name'=>'Aruban Guilder',
			'symbol'=>'�'),
		'azn'=>array(
			'name'=>'Azerbaijanian Manat',
			'symbol'=>'???'),
		'bam'=>array(
			'name'=>'Convertible Marks',
			'symbol'=>'KM'),
		'bbd'=>array(
			'name'=>'Barbados Dollar',
			'symbol'=>'$'),
		'bdt'=>array(
			'name'=>'Taka'),
		'bgn'=>array(
			'name'=>'Bulgarian Lev',
			'symbol'=>'??'),
		'bhd'=>array(
			'name'=>'Bahraini Dinar'),
		'bif'=>array(
			'name'=>'Burundi Franc',
			'symbol'=>'FBu'),
		'bmd'=>array(
			'name'=>'Bermudian Dollar',
			'symbol'=>'$'),
		'bnd'=>array(
			'name'=>'Brunei Dollar',
			'symbol'=>'$'),
		'bob'=>array(
			'name'=>'Boliviano',
			'symbol'=>'$b'),
		'bov'=>array(
			'name'=>'Mvdol'),
		'brl'=>array(
			'name'=>'Brazilian Real',
			'symbol'=>'R$'),
		'bsd'=>array(
			'name'=>'Bahamian Dollar',
			'symbol'=>'$'),
		'btn'=>array(
			'name'=>'Ngultrum',
			'symbol'=>'Nu.'),
		'bwp'=>array(
			'name'=>'Pula',
			'symbol'=>'P'),
		'byr'=>array(
			'name'=>'Belarussian Ruble',
			'symbol'=>'p.'),
		'bzd'=>array(
			'name'=>'Belize Dollar',
			'symbol'=>'BZ$'),
		'cad'=>array(
			'name'=>'Canadian Dollar',
			'symbol'=>'$'),
		'cdf'=>array(
			'name'=>'Congolese Franc',
			'symbol'=>'FC'),
		'che'=>array(
			'name'=>'WIR Euro'),
		'chf'=>array(
			'name'=>'Swiss Franc',
			'country'=>'ch'),
		'chw'=>array(
			'name'=>'WIR Franc'),
		'clf'=>array(
			'name'=>'Unidades de fomento',
			'symbol'=>'UF'),
		'clp'=>array(
			'name'=>'Chilean Peso',
			'symbol'=>'$'),
		'cny'=>array(
			'name'=>'Yuan Renminbi',
			'symbol'=>'�'),
		'cop'=>array(
			'name'=>'Colombian Peso',
			'symbol'=>'$'),
		'cou'=>array(
			'name'=>'Unidad de Valor Real'),
		'crc'=>array(
			'name'=>'Costa Rican Colon',
			'symbol'=>'�'),
		'cuc'=>array(
			'name'=>'Peso Convertible',
			'symbol'=>'$'),
		'cup'=>array(
			'name'=>'Cuban Peso',
			'symbol'=>'?'),
		'cve'=>array(
			'name'=>'Cape Verde Escudo',
			'symbol'=>'$'),
		'czk'=>array(
			'name'=>'Czech Koruna',
			'symbol'=>'Kc'),
		'djf'=>array(
			'name'=>'Djibouti Franc',
			'symbol'=>'Fdj'),
		'dkk'=>array(
			'name'=>'Danish Krone',
			'symbol'=>'kr',
			'country'=>'dk'),
		'dop'=>array(
			'name'=>'Dominican Peso',
			'symbol'=>'RD$'),
		'dzd'=>array(
			'name'=>'Algerian Dinar',
			'symbol'=>'??'),
		'eek'=>array(
			'name'=>'Kroon'),
		'egp'=>array(
			'name'=>'Egyptian Pound',
			'symbol'=>'�'),
		'ern'=>array(
			'name'=>'Nakfa',
			'symbol'=>'Nfk'),
		'etb'=>array(
			'name'=>'Ethiopian Birr',
			'symbol'=>'Br'),
		'eur'=>array(
			'name'=>'Euro',
			'symbol'=>'�',
			'country'=>'eu'),
		'fjd'=>array(
			'name'=>'Fiji Dollar',
			'symbol'=>'$'),
		'fkp'=>array(
			'name'=>'Falkland Islands Pound',
			'symbol'=>'�'),
		'gbp'=>array(
			'name'=>'Pound Sterling',
			'symbol'=>'�',
			'country'=>'gb'),
		'gel'=>array(
			'name'=>'Lari'),
		'ggp'=>array(
			'symbol'=>'�'),
		'ghc'=>array(
			'symbol'=>'�'),
		'ghs'=>array(
			'name'=>'Cedi',
			'symbol'=>'GH?'),
		'gip'=>array(
			'name'=>'Gibraltar Pound',
			'symbol'=>'�'),
		'gmd'=>array(
			'name'=>'Dalasi',
			'symbol'=>'D'),
		'gnf'=>array(
			'name'=>'Guinea Franc',
			'symbol'=>'FG'),
		'gtq'=>array(
			'name'=>'Quetzal',
			'symbol'=>'Q'),
		'gyd'=>array(
			'name'=>'Guyana Dollar',
			'symbol'=>'$'),
		'hkd'=>array(
			'name'=>'Hong Kong Dollar',
			'symbol'=>'$'),
		'hnl'=>array(
			'name'=>'Lempira',
			'symbol'=>'L'),
		'hrk'=>array(
			'name'=>'Croatian Kuna',
			'symbol'=>'kn'),
		'htg'=>array(
			'name'=>'Gourde',
			'symbol'=>'G'),
		'huf'=>array(
			'name'=>'Forint',
			'symbol'=>'Ft'),
		'idr'=>array(
			'name'=>'Rupiah',
			'symbol'=>'Rp'),
		'ils'=>array(
			'name'=>'New Israeli Sheqel',
			'symbol'=>'?'),
		'imp'=>array(
			'symbol'=>'�'),
		'inr'=>array(
			'name'=>'Indian Rupee',
			'country'=>'in'),
		'iqd'=>array(
			'name'=>'Iraqi Dinar',
			'symbol'=>'?.?'),
		'irr'=>array(
			'name'=>'Iranian Rial',
			'symbol'=>'?'),
		'isk'=>array(
			'name'=>'Iceland Krona',
			'symbol'=>'kr'),
		'jep'=>array(
			'symbol'=>'�'),
		'jmd'=>array(
			'name'=>'Jamaican Dollar',
			'symbol'=>'J$'),
		'jod'=>array(
			'name'=>'Jordanian Dinar'),
		'jpy'=>array(
			'name'=>'Yen',
			'symbol'=>'�'),
		'kes'=>array(
			'name'=>'Kenyan Shilling',
			'symbol'=>'KSh'),
		'kgs'=>array(
			'name'=>'Som',
			'symbol'=>'??'),
		'khr'=>array(
			'name'=>'Riel',
			'symbol'=>'?'),
		'kmf'=>array(
			'name'=>'Comoro Franc',
			'symbol'=>'CF'),
		'kpw'=>array(
			'name'=>'North Korean Won',
			'symbol'=>'?'),
		'krw'=>array(
			'name'=>'Won',
			'symbol'=>'?'),
		'kwd'=>array(
			'name'=>'Kuwaiti Dinar',
			'symbol'=>'K.D.'),
		'kyd'=>array(
			'name'=>'Cayman Islands Dollar',
			'symbol'=>'$'),
		'kzt'=>array(
			'name'=>'Tenge',
			'symbol'=>'??'),
		'lak'=>array(
			'name'=>'Kip',
			'symbol'=>'?'),
		'lbp'=>array(
			'name'=>'Lebanese Pound',
			'symbol'=>'�'),
		'lkr'=>array(
			'name'=>'Sri Lanka Rupee',
			'symbol'=>'?'),
		'lrd'=>array(
			'name'=>'Liberian Dollar',
			'symbol'=>'$'),
		'lsl'=>array(
			'name'=>'Loti',
			'symbol'=>'L'),
		'ltl'=>array(
			'name'=>'Lithuanian Litas',
			'symbol'=>'Lt'),
		'lvl'=>array(
			'name'=>'Latvian Lats',
			'symbol'=>'Ls'),
		'lyd'=>array(
			'name'=>'Libyan Dinar',
			'symbol'=>'LD'),
		'mad'=>array(
			'name'=>'Moroccan Dirham',
			'symbol'=>'?.',
			'country'=>'ma'),
		'mdl'=>array(
			'name'=>'Moldovan Leu'),
		'mga'=>array(
			'name'=>'Malagasy Ariary',
			'symbol'=>'Ar'),
		'mkd'=>array(
			'name'=>'Denar',
			'symbol'=>'???'),
		'mmk'=>array(
			'name'=>'Kyat',
			'symbol'=>'K'),
		'mnt'=>array(
			'name'=>'Tugrik',
			'symbol'=>'?'),
		'mop'=>array(
			'name'=>'Pataca',
			'symbol'=>'MOP$'),
		'mro'=>array(
			'name'=>'Ouguiya',
			'symbol'=>'UM'),
		'mur'=>array(
			'name'=>'Mauritius Rupee',
			'symbol'=>'?'),
		'mvr'=>array(
			'name'=>'Rufiyaa',
			'symbol'=>'Rf'),
		'mwk'=>array(
			'name'=>'Kwacha',
			'symbol'=>'MK'),
		'mxn'=>array(
			'name'=>'Mexican Peso',
			'symbol'=>'$'),
		'mxv'=>array(
			'name'=>'Mexican Unidad de Inversion (UDI)'),
		'myr'=>array(
			'name'=>'Malaysian Ringgit',
			'symbol'=>'RM'),
		'mzn'=>array(
			'name'=>'Metical',
			'symbol'=>'MT'),
		'nad'=>array(
			'name'=>'Namibia Dollar',
			'symbol'=>'$'),
		'ngn'=>array(
			'name'=>'Naira',
			'symbol'=>'?'),
		'nio'=>array(
			'name'=>'Cordoba Oro',
			'symbol'=>'C$'),
		'nok'=>array(
			'name'=>'Norwegian Krone',
			'symbol'=>'kr',
			'country'=>'no'),
		'npr'=>array(
			'name'=>'Nepalese Rupee',
			'symbol'=>'?'),
		'nzd'=>array(
			'name'=>'New Zealand Dollar',
			'symbol'=>'$',
			'country'=>'nz'),
		'omr'=>array(
			'name'=>'Rial Omani',
			'symbol'=>'?'),
		'pab'=>array(
			'name'=>'Balboa',
			'symbol'=>'B/.'),
		'pen'=>array(
			'name'=>'Nuevo Sol',
			'symbol'=>'S/.'),
		'pgk'=>array(
			'name'=>'Kina',
			'symbol'=>'K'),
		'php'=>array(
			'name'=>'Philippine Peso',
			'symbol'=>'Php'),
		'pkr'=>array(
			'name'=>'Pakistan Rupee',
			'symbol'=>'?'),
		'pln'=>array(
			'name'=>'Zloty',
			'symbol'=>'zl'),
		'pyg'=>array(
			'name'=>'Guarani',
			'symbol'=>'Gs'),
		'qar'=>array(
			'name'=>'Qatari Rial',
			'symbol'=>'?'),
		'ron'=>array(
			'name'=>'New Leu',
			'symbol'=>'lei'),
		'rsd'=>array(
			'name'=>'Serbian Dinar',
			'symbol'=>'???.'),
		'rub'=>array(
			'name'=>'Russian Ruble',
			'symbol'=>'???'),
		'rwf'=>array(
			'name'=>'Rwanda Franc',
			'symbol'=>'FRw'),
		'sar'=>array(
			'name'=>'Saudi Riyal',
			'symbol'=>'?'),
		'sbd'=>array(
			'name'=>'Solomon Islands Dollar',
			'symbol'=>'$'),
		'scr'=>array(
			'name'=>'Seychelles Rupee',
			'symbol'=>'?'),
		'sdg'=>array(
			'name'=>'Sudanese Pound'),
		'sek'=>array(
			'name'=>'Swedish Krona',
			'symbol'=>'kr'),
		'sgd'=>array(
			'name'=>'Singapore Dollar',
			'symbol'=>'$'),
		'shp'=>array(
			'name'=>'Saint Helena Pound',
			'symbol'=>'�'),
		'sll'=>array(
			'name'=>'Leone',
			'symbol'=>'Le'),
		'sos'=>array(
			'name'=>'Somali Shilling',
			'symbol'=>'S'),
		'srd'=>array(
			'name'=>'Surinam Dollar',
			'symbol'=>'$'),
		'std'=>array(
			'name'=>'Dobra',
			'symbol'=>'Db'),
		'svc'=>array(
			'name'=>'El Salvador Colon',
			'symbol'=>'$'),
		'syp'=>array(
			'name'=>'Syrian Pound',
			'symbol'=>'�'),
		'szl'=>array(
			'name'=>'Lilangeni',
			'symbol'=>'L'),
		'thb'=>array(
			'name'=>'Baht',
			'symbol'=>'?'),
		'tjs'=>array(
			'name'=>'Somoni'),
		'tmt'=>array(
			'name'=>'Manat',
			'symbol'=>'m'),
		'tnd'=>array(
			'name'=>'Tunisian Dinar',
			'symbol'=>'DT'),
		'top'=>array(
			'name'=>'Pa\'anga',
			'symbol'=>'DT'),
		'try'=>array(
			'name'=>'Turkish Lira',
			'symbol'=>'TL'),
		'ttd'=>array(
			'name'=>'Trinidad and Tobago Dollar',
			'symbol'=>'TT$'),
		'tvd'=>array(
			'symbol'=>'$'),
		'twd'=>array(
			'name'=>'New Taiwan Dollar',
			'symbol'=>'NT$'),
		'tzs'=>array(
			'name'=>'Tanzanian Shilling'),
		'uah'=>array(
			'name'=>'Hryvnia',
			'symbol'=>'?'),
		'ugx'=>array(
			'name'=>'Uganda Shilling',
			'symbol'=>'USh'),
		'usd'=>array(
			'name'=>'US Dollar',
			'symbol'=>'$',
			'country'=>'us'),
		'usn'=>array(
			'name'=>'US Dollar (Next day)'),
		'uss'=>array(
			'name'=>'US Dollar (Same day)'),
		'uyi'=>array(
			'name'=>'Uruguay Peso en Unidades Indexadas'),
		'uyu'=>array(
			'name'=>'Peso Uruguayo',
			'symbol'=>'$U'),
		'uzs'=>array(
			'name'=>'Uzbekistan Sum',
			'symbol'=>'??'),
		'vef'=>array(
			'name'=>'Bolivar Fuerte',
			'symbol'=>'Bs'),
		'vnd'=>array(
			'name'=>'Dong',
			'symbol'=>'?'),
		'vuv'=>array(
			'name'=>'Vatu',
			'symbol'=>'VT'),
		'wst'=>array(
			'name'=>'Tala'),
		'xaf'=>array(
			'name'=>'CFA Franc BEAC',
			'symbol'=>'FCFA',
			'country'=>'cm'),
		'xag'=>array(
			'name'=>'Silver'),
		'xau'=>array(
			'name'=>'Gold'),
		'xba'=>array(
			'name'=>'Bond Markets Units European Composite Unit (EURCO)'),
		'xbb'=>array(
			'name'=>'European Monetary Unit (E.M.U.-6)'),
		'xbc'=>array(
			'name'=>'European Unit of Account 9(E.U.A.-9)'),
		'xbd'=>array(
			'name'=>'European Unit of Account 17(E.U.A.-17)'),
		'xcd'=>array(
			'name'=>'East Caribbean Dollar',
			'symbol'=>'$',
			'country'=>'kn'),
		'xdr'=>array(
			'name'=>'SDR'),
		'xfu'=>array(
			'name'=>'UIC-Franc'),
		'xof'=>array(
			'name'=>'CFA Franc BCEAO',
			'symbol'=>'CFA',
			'country'=>'sn'),
		'xpd'=>array(
			'name'=>'Palladium'),
		'xpf'=>array(
			'name'=>'CFP Franc',
			'country'=>'pf'),
		'xpt'=>array(
			'name'=>'Platinum'),
		'xts'=>array(
			'name'=>'Codes specifically reserved for testing purposes'),
		'xxx'=>array(
			'name'=>'The codes assigned for transactions where no currency is involved are:'),
		'yer'=>array(
			'name'=>'Yemeni Rial',
			'symbol'=>'?'),
		'zar'=>array(
			'name'=>'Rand',
			'symbol'=>'R',
			'country'=>'za'),
		'zmk'=>array(
			'name'=>'Zambian Kwacha',
			'symbol'=>'ZK'),
		'zwd'=>array(
			'symbol'=>'Z$'),
		'zwl'=>array(
			'name'=>'Zimbabwe Dollar',
			'symbol'=>'$'),
	),
	'country_currencies'=>array(
		'ad'=>'eur',
		'ae'=>'aed',
		'af'=>'afn',
		'ag'=>'xcd',
		'ai'=>'xcd',
		'al'=>'all',
		'am'=>'amd',
		'an'=>'ang',
		'ao'=>'aoa',
		'ar'=>'ars',
		'as'=>'usd',
		'at'=>'eur',
		'au'=>'aud',
		'aw'=>'awg',
		'ax'=>'eur',
		'az'=>'azn',
		'ba'=>'bam',
		'bb'=>'bbd',
		'bd'=>'bdt',
		'be'=>'eur',
		'bf'=>'xof',
		'bg'=>'bgn',
		'bh'=>'bhd',
		'bi'=>'bif',
		'bj'=>'xof',
		'bl'=>'eur',
		'bm'=>'bmd',
		'bn'=>'bnd',
		'bo'=>'bob', #bob,bov
		'bq'=>'usd',
		'br'=>'brl',
		'bs'=>'bsd',
		'bt'=>'btn', #inr,btn
		'bv'=>'nok',
		'bw'=>'bwp',
		'by'=>'byr',
		'bz'=>'bzd',
		'ca'=>'cad',
		'cc'=>'aud',
		'cd'=>'cdf',
		'cf'=>'xaf',
		'cg'=>'xaf',
		'ch'=>'chf', #chf,chw,che
		'ci'=>'xof',
		'ck'=>'nzd',
		'cl'=>'clp', #clp,clf
		'cm'=>'xaf',
		'cn'=>'cny',
		'co'=>'cop', #cop,cou
		'cr'=>'crc',
		'cu'=>'cup', #cup,cuc
		'cv'=>'cve',
		'cw'=>'ang',
		'cx'=>'aud',
		'cy'=>'eur',
		'cz'=>'czk',
		'de'=>'eur',
		'dj'=>'djf',
		'dk'=>'dkk',
		'dm'=>'xcd',
		'do'=>'dop',
		'dz'=>'dzd',
		'ec'=>'usd',
		'ee'=>'eek',
		'eg'=>'egp',
		'eh'=>'mad',
		'er'=>'ern',
		'es'=>'eur',
		'et'=>'etb',
		'eu'=>'eur',
		'fi'=>'eur',
		'fj'=>'fjd',
		'fk'=>'fkp',
		'fm'=>'usd',
		'fo'=>'dkk',
		'fr'=>'eur',
		'ga'=>'xaf',
		'gb'=>'gbp',
		'gd'=>'xcd',
		'ge'=>'gel',
		'gf'=>'eur',
		'gg'=>'gbp',
		'gh'=>'ghs',
		'gi'=>'gip',
		'gl'=>'dkk',
		'gm'=>'gmd',
		'gn'=>'gnf',
		'gp'=>'eur',
		'gq'=>'xaf',
		'gr'=>'eur',
		'gt'=>'gtq',
		'gu'=>'usd',
		'gw'=>'xof',
		'gy'=>'gyd',
		'hk'=>'hkd',
		'hm'=>'aud',
		'hn'=>'hnl',
		'hr'=>'hrk',
		'ht'=>'htg', #htg,usd
		'hu'=>'huf',
		'id'=>'idr',
		'ie'=>'eur',
		'il'=>'ils',
		'im'=>'gbp',
		'in'=>'inr',
		'io'=>'usd',
		'iq'=>'iqd',
		'ir'=>'irr',
		'is'=>'isk',
		'it'=>'eur',
		'je'=>'gbp',
		'jm'=>'jmd',
		'jo'=>'jod',
		'jp'=>'jpy',
		'ke'=>'kes',
		'kg'=>'kgs',
		'kh'=>'khr',
		'ki'=>'aud',
		'km'=>'kmf',
		'kn'=>'xcd',
		'kp'=>'kpw',
		'kr'=>'krw',
		'kw'=>'kwd',
		'ky'=>'kyd',
		'kz'=>'kzt',
		'la'=>'lak',
		'lb'=>'lbp',
		'lc'=>'xcd',
		'li'=>'chf',
		'lk'=>'lkr',
		'lr'=>'lrd',
		'ls'=>'lsl', #zar,lsl
		'lt'=>'ltl',
		'lu'=>'eur',
		'lv'=>'lvl',
		'ly'=>'lyd',
		'ma'=>'mad',
		'mc'=>'eur',
		'md'=>'mdl',
		'me'=>'eur',
		'mf'=>'eur',
		'mg'=>'mga',
		'mh'=>'usd',
		'mk'=>'mkd',
		'ml'=>'xof',
		'mm'=>'mmk',
		'mn'=>'mnt',
		'mo'=>'mop',
		'mp'=>'usd',
		'mq'=>'eur',
		'mr'=>'mro',
		'ms'=>'xcd',
		'mt'=>'eur',
		'mu'=>'mur',
		'mv'=>'mvr',
		'mw'=>'mwk',
		'mx'=>'mxn', #mxn,mxv
		'my'=>'myr',
		'mz'=>'mzn',
		'na'=>'nad', #zar,nad
		'nc'=>'xpf',
		'ne'=>'xof',
		'nf'=>'aud',
		'ng'=>'ngn',
		'ni'=>'nio',
		'nl'=>'eur',
		'no'=>'nok',
		'np'=>'npr',
		'nr'=>'aud',
		'nu'=>'nzd',
		'nz'=>'nzd',
		'om'=>'omr',
		'pa'=>'pab', #pab,usd
		'pe'=>'pen',
		'pf'=>'xpf',
		'pg'=>'pgk',
		'ph'=>'php',
		'pk'=>'pkr',
		'pl'=>'pln',
		'pm'=>'eur',
		'pn'=>'nzd',
		'pr'=>'usd',
		'pt'=>'eur',
		'pw'=>'usd',
		'py'=>'pyg',
		'qa'=>'qar',
		're'=>'eur',
		'ro'=>'ron',
		'rs'=>'rsd',
		'ru'=>'rub',
		'rw'=>'rwf',
		'sa'=>'sar',
		'sb'=>'sbd',
		'sc'=>'scr',
		'sd'=>'sdg',
		'se'=>'sek',
		'sg'=>'sgd',
		'sh'=>'shp',
		'si'=>'eur',
		'sj'=>'nok',
		'sk'=>'eur',
		'sl'=>'sll',
		'sm'=>'eur',
		'sn'=>'xof',
		'so'=>'sos',
		'sr'=>'srd',
		'st'=>'std',
		'sv'=>'usd', #svc,usd
		'sx'=>'ang',
		'sy'=>'syp',
		'sz'=>'szl',
		'tc'=>'usd',
		'td'=>'xaf',
		'tf'=>'eur',
		'tg'=>'xof',
		'th'=>'thb',
		'tj'=>'tjs',
		'tk'=>'nzd',
		'tl'=>'usd',
		'tm'=>'tmt',
		'tn'=>'tnd',
		'to'=>'top',
		'tr'=>'try',
		'tt'=>'ttd',
		'tv'=>'aud',
		'tw'=>'twd',
		'tz'=>'tzs',
		'ua'=>'uah',
		'ug'=>'ugx',
		'um'=>'usd',
		'us'=>'usd', #usd,uss,usn
		'uy'=>'uyu', #uyu,uyi
		'uz'=>'uzs',
		'va'=>'eur',
		'vc'=>'xcd',
		've'=>'vef',
		'vg'=>'usd',
		'vi'=>'usd',
		'vn'=>'vnd',
		'vu'=>'vuv',
		'wf'=>'xpf',
		'ws'=>'wst',
		'ye'=>'yer',
		'yt'=>'eur',
		'za'=>'zar',
		'zm'=>'zmk',
		'zw'=>'zwl'
	),
	'countries'=>array(
		'ad'=>'Andorra',
		'ae'=>'United Arab Emirates',
		'af'=>'Afghanistan',
		'ag'=>'Antigua and Barbuda',
		'ai'=>'Anguilla',
		'al'=>'Albania',
		'am'=>'Armenia',
		'an'=>'Netherlands Antilles',
		'ao'=>'Angola',
		'aq'=>'Antarctica',
		'ar'=>'Argentina',
		'as'=>'American Samoa',
		'at'=>'Austria',
		'au'=>'Australia',
		'aw'=>'Aruba',
		'ax'=>'�land Islands',
		'az'=>'Azerbaijan',
		'ba'=>'Bosnia and Herzegovina',
		'bb'=>'Barbados',
		'bd'=>'Bangladesh',
		'be'=>'Belgium',
		'bf'=>'Burkina Faso',
		'bg'=>'Bulgaria',
		'bh'=>'Bahrain',
		'bi'=>'Burundi',
		'bj'=>'Benin',
		'bl'=>'Saint Barth�lemy',
		'bm'=>'Bermuda',
		'bn'=>'Brunei', #'Brunei Darussalam',
		'bo'=>'Bolivia', #'Bolivia, Plurinational State of',
		'bq'=>'Caribbean Netherlands', #'Bonaire, Sint Eustatius and Saba',
		'br'=>'Brazil',
		'bs'=>'Bahamas',
		'bt'=>'Bhutan',
		'bv'=>'Bouvet Island',
		'bw'=>'Botswana',
		'by'=>'Belarus',
		'bz'=>'Belize',
		'ca'=>'Canada',
		'cc'=>'Cocos (Keeling) Islands',
		'cd'=>'DR Congo', #'Congo, the Democratic Republic of the',
		'cf'=>'Central African Republic',
		'cg'=>'Congo',
		'ch'=>'Switzerland',
		'ci'=>'C�te d\'Ivoire',
		'ck'=>'Cook Islands',
		'cl'=>'Chile',
		'cm'=>'Cameroon',
		'cn'=>'China',
		'co'=>'Colombia',
		'cr'=>'Costa Rica',
		'cu'=>'Cuba',
		'cv'=>'Cape Verde',
		'cw'=>'Cura�ao',
		'cx'=>'Christmas Island',
		'cy'=>'Cyprus',
		'cz'=>'Czech Republic',
		'de'=>'Germany',
		'dj'=>'Djibouti',
		'dk'=>'Denmark',
		'dm'=>'Dominica',
		'do'=>'Dominican Republic',
		'dz'=>'Algeria',
		'ec'=>'Ecuador',
		'ee'=>'Estonia',
		'eg'=>'Egypt',
		'eh'=>'Western Sahara',
		'en'=>'England',
		'er'=>'Eritrea',
		'es'=>'Spain',
		'et'=>'Ethiopia',
		'eu'=>'European Union',
		'fi'=>'Finland',
		'fj'=>'Fiji',
		'fk'=>'Falkland Islands', #'Falkland Islands (Malvinas)',
		'fm'=>'Micronesia', #'Micronesia, Federated States of',
		'fo'=>'Faroe Islands',
		'fr'=>'France',
		'ga'=>'Gabon',
		'gb'=>'United Kingdom',
		'gd'=>'Grenada',
		'ge'=>'Georgia',
		'gf'=>'French Guiana',
		'gg'=>'Guernsey',
		'gh'=>'Ghana',
		'gi'=>'Gibraltar',
		'gl'=>'Greenland',
		'gm'=>'Gambia',
		'gn'=>'Guinea',
		'gp'=>'Guadeloupe',
		'gq'=>'Equatorial Guinea',
		'gr'=>'Greece',
		'gs'=>'South Georgia and the South Sandwich Islands',
		'gt'=>'Guatemala',
		'gu'=>'Guam',
		'gw'=>'Guinea-Bissau',
		'gy'=>'Guyana',
		'hk'=>'Hong Kong',
		'hm'=>'Heard Island and McDonald Islands',
		'hn'=>'Honduras',
		'hr'=>'Croatia',
		'ht'=>'Haiti',
		'hu'=>'Hungary',
		'id'=>'Indonesia',
		'ie'=>'Ireland',
		'il'=>'Israel',
		'im'=>'Isle of Man',
		'in'=>'India',
		'io'=>'British Indian Ocean Territory',
		'iq'=>'Iraq',
		'ir'=>'Iran', #'Iran, Islamic Republic of',
		'is'=>'Iceland',
		'it'=>'Italy',
		'je'=>'Jersey',
		'jm'=>'Jamaica',
		'jo'=>'Jordan',
		'jp'=>'Japan',
		'ke'=>'Kenya',
		'kg'=>'Kyrgyzstan',
		'kh'=>'Cambodia',
		'ki'=>'Kiribati',
		'km'=>'Comoros',
		'kn'=>'Saint Kitts and Nevis',
		'kp'=>'Korea DPR', #Korea, Democratic People's Republic of
		'kr'=>'Korea Republic', #Korea, Republic of
		'kw'=>'Kuwait',
		'ky'=>'Cayman Islands',
		'kz'=>'Kazakhstan',
		'la'=>'Laos', #'Lao People\'s Democratic Republic',
		'lb'=>'Lebanon',
		'lc'=>'Saint Lucia',
		'li'=>'Liechtenstein',
		'lk'=>'Sri Lanka',
		'lr'=>'Liberia',
		'ls'=>'Lesotho',
		'lt'=>'Lithuania',
		'lu'=>'Luxembourg',
		'lv'=>'Latvia',
		'ly'=>'Libya', #'Libyan Arab Jamahiriya',
		'ma'=>'Morocco',
		'mc'=>'Monaco',
		'md'=>'Moldova', #'Moldova, Republic of',
		'me'=>'Montenegro',
		'mf'=>'Saint Martin', #'Saint Martin (French part)',
		'mg'=>'Madagascar',
		'mh'=>'Marshall Islands',
		'mk'=>'Macedonia', #'Macedonia, the former Yugoslav Republic of',
		'ml'=>'Mali',
		'mm'=>'Myanmar', #'Myanmar (Burma)',
		'mn'=>'Mongolia',
		'mo'=>'Macao',
		'mp'=>'Northern Mariana Islands',
		'mq'=>'Martinique',
		'mr'=>'Mauritania',
		'ms'=>'Montserrat',
		'mt'=>'Malta',
		'mu'=>'Mauritius',
		'mv'=>'Maldives',
		'mw'=>'Malawi',
		'mx'=>'Mexico',
		'my'=>'Malaysia',
		'mz'=>'Mozambique',
		'na'=>'Namibia',
		'nc'=>'New Caledonia',
		'ne'=>'Niger',
		'nf'=>'Norfolk Island',
		'ng'=>'Nigeria',
		'ni'=>'Nicaragua',
		'nl'=>'Netherlands',
		'no'=>'Norway',
		'np'=>'Nepal',
		'nr'=>'Nauru',
		'nu'=>'Niue',
		'nz'=>'New Zealand',
		'om'=>'Oman',
		'pa'=>'Panama',
		'pe'=>'Peru',
		'pf'=>'French Polynesia',
		'pg'=>'Papua New Guinea',
		'ph'=>'Philippines',
		'pk'=>'Pakistan',
		'pl'=>'Poland',
		'pm'=>'Saint Pierre and Miquelon',
		'pn'=>'Pitcairn',
		'pr'=>'Puerto Rico',
		'ps'=>'Palestine', #'Palestinian Territory, Occupied',
		'pt'=>'Portugal',
		'pw'=>'Palau',
		'py'=>'Paraguay',
		'qa'=>'Qatar',
		're'=>'R�union',
		'ro'=>'Romania',
		'rs'=>'Serbia',
		'ru'=>'Russia', #'Russian Federation',
		'rw'=>'Rwanda',
		'sa'=>'Saudi Arabia',
		'sb'=>'Solomon Islands',
		'sc'=>'Seychelles',
		'sd'=>'Sudan',
		'se'=>'Sweden',
		'sg'=>'Singapore',
		'sh'=>'Saint Helena, Ascension and Tristan da Cunha',
		'si'=>'Slovenia',
		'sj'=>'Svalbard and Jan Mayen',
		'sk'=>'Slovakia',
		'sl'=>'Sierra Leone',
		'sm'=>'San Marino',
		'sn'=>'Senegal',
		'so'=>'Somalia',
		'sr'=>'Suriname',
		'ss'=>'South Sudan',
		'st'=>'Sao Tome and Principe',
		'sv'=>'El Salvador',
		'sx'=>'Sint Maarten',
		'sy'=>'Syria', #'Syrian Arab Republic',
		'sz'=>'Swaziland',
		'tc'=>'Turks and Caicos Islands',
		'td'=>'Chad',
		'tf'=>'French Southern Territories',
		'tg'=>'Togo',
		'th'=>'Thailand',
		'tj'=>'Tajikistan',
		'tk'=>'Tokelau',
		'tl'=>'Timor-Leste',
		'tm'=>'Turkmenistan',
		'tn'=>'Tunisia',
		'to'=>'Tonga',
		'tr'=>'Turkey',
		'tt'=>'Trinidad and Tobago',
		'tv'=>'Tuvalu',
		'tw'=>'Taiwan', #'Taiwan, Province of China',
		'tz'=>'Tanzania', #'Tanzania, United Republic of',
		'ua'=>'Ukraine',
		'ug'=>'Uganda',
		'um'=>'U.S. Minor Outlying Islands', #'United States Minor Outlying Islands',
		'us'=>'United States',
		'uy'=>'Uruguay',
		'uz'=>'Uzbekistan',
		'va'=>'Vatican City', #'Holy See (Vatican City State)',
		'vc'=>'Saint Vincent and the Grenadines',
		've'=>'Venezuela', #'Venezuela, Bolivarian Republic of',
		'vg'=>'British Virgin Islands', #'Virgin Islands, British',
		'vi'=>'U.S. Virgin Islands', #'Virgin Islands, U.S.',
		'vn'=>'Vietnam', #'Viet Nam',
		'vu'=>'Vanuatu',
		'wf'=>'Wallis and Futuna',
		'ws'=>'Samoa',
		'ye'=>'Yemen',
		'yt'=>'Mayotte',
		'za'=>'South Africa',
		'zm'=>'Zambia',
		'zw'=>'Zimbabwe'
	)
);

class Currency {	
	var $id;
	var $name;
	var $symbol;
	var $countries;
	var $rate;
	
	var $amt;
	var $amt_f;
	
	var $rates;
	var $mtime;
	
	var $now;
	
	var $xml_path = 'rates.xml';
	var $txt_path = 'rates.txt';
	var $iso_path = 'iso.inc.php';
	
	var $iso;
	
	public function __construct($value='USD') {
		# Get and define $iso Array containing currency-to-country pairs
		//require($this->iso_path);
		global $iso;
		$this->iso = $iso;
		
		# Set global class time
		$this->now = time();
		
		$value = $this->lowercase_trim($value);
		$value = preg_replace('/\s+/', ' ', $value);
		
		if($rates = $this->rates_array()) {
			$this->rates = $rates;
		}
		
		if($currency = $this->find_currency($value)) {
			$this->id = $currency['id'];
			$this->name = $currency['name'];
			$this->symbol = $currency['symbol'];
			$this->countries = $currency['countries'];
			$this->rate = $currency['rate'];
		}
	}
	########################
	########################
	########################
	private function countries_by_currency_code($value) {
		$iso = $this->iso;
		
		$value = trim(strtolower($value));
		
		$countries = array_keys($iso['country_currencies'], $value);
		if(!empty($countries)) {
			sort($countries);
			if($parent_country = $iso['currencies'][$value]['country']) {
				if($key = array_search($parent_country, $countries)) {
					unset($countries[$key]);
					array_unshift($countries, $parent_country);
				}
			}
		}
		
		return $countries;
	}
	 function find_currency($value) {
		$value = $this->lowercase_trim($value);
		
		if($currency = $this->currency_by_code($value)) {
		} elseif($currency = $this->currency_by_name($value)) {
		} elseif($currency = $this->currency_by_country_code($value)) {
		} elseif($currency = $this->currency_by_country_name($value)) {
		}
		if(!empty($currency) && is_array($currency)) {
			$id = $this->lowercase_trim($currency['id']);
			/*$rate = (float) $this->rates[$id];
			if(!empty($rate)) {
				$currency['rate'] = $rate;
			}*/
			$currency['id'] = strtoupper($currency['id']);
			//$currency['countries'] = $this->countries_by_currency_code($currency['id']);
			//debugVar($currency,true);
			return $currency;
		}
		return FALSE;
	}
	# Find currency info by currency code/name or country code/name defined in iso.inc.php
	private function currency_by_code($value) {
		$iso = $this->iso;
		$currency = $iso['currencies'][$value];
		if(!empty($currency)) {
			$currency['id'] = $value;
			return $currency;
		}
		return FALSE;
	}
	private function currency_by_name($value) {
		$iso = $this->iso;
		foreach($iso['currencies'] as $currency_code=>$currency) {
			if($value == $this->lowercase_trim($currency['name'])) {
				$currency['id'] = $currency_code;
				return $currency;
			}
		}
		return FALSE;
	}
	private function currency_by_country_code($value) {
		$iso = $this->iso;
		
		$currency_code = $iso['country_currencies'][$value];
		$currency = $iso['currencies'][$currency_code];
		
		if(!empty($currency)) {
			$currency['id'] = $currency_code;
			return $currency;
		}
		return FALSE;
	}
	private function currency_by_country_name($value) {
		$iso = $this->iso;
		
		if($country_code = array_search($value, array_map(array(&$this, 'lowercase_trim'), $iso['countries']))) {
			if($currency = $this->currency_by_country_code($country_code)) {
				return $currency;
			}
		}
		return FALSE;
	}
	########################
	
	########################
	private function lowercase_trim($value) {
		$value = strtolower($value);
		$value = trim($value);
		$value = preg_replace('/\s+/', ' ', $value);
		return $value;
	}
	########################
	########################
	########################
	
	
	public function convert($new_currency, $amt=1) {
		$amt = (float) $amt;
		
		$this->amt = $amt;
		$this->amt_f = number_format($amt, 2);
		
		$new_currency = $this->find_currency($new_currency);
		
		$from_rate = (float) $this->rate;
		$to_rate = (float) $new_currency['rate'];
		
		if(empty($from_rate) || empty($to_rate)) {
			return FALSE;
		}
		
		$to = (float) $to_rate/$from_rate;
		$from = (float) $from_rate/$to_rate;
		$rates = array('to'=>$to, 'from'=>$from);
		
		$amt *= $rates['to'];
		
		
		$converted = $new_currency;
		$converted['amt'] = $amt;
		$converted['amt_f'] = number_format($amt, 2);
		$converted['rates'] = $rates;
		
		return $converted;
	}
	
	private function get_xml($url = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml') {
		$now = $this->now;
		
		$pathinfo = pathinfo($url);
		
		$xml_path = $this->xml_path;
		$txt_path = $this->txt_path;
		
		# Check if if error bookmark (txt) exists and valid or xml exists and valid
		$files = array('txt'=>$txt_path, 'xml'=>$xml_path);
		foreach($files as $ext=>$file) {
			if(file_exists($file) && is_file($file)) {
				$cache_time = ($ext=='txt') ? 1200 : 57600; # 20M TXT / 16H XML
				$filemtime = filemtime($file);
				$exp_time = $filemtime+$cache_time;
				if($exp_time > $now) { # File still valid (not expired)
					$this->mtime = $filemtime;
					if($ext == 'txt') {
						return FALSE;
					} elseif($xml = @simplexml_load_file($xml_path)) {
						return $xml;
					}
				}
			}
		}
		
		# New file
		if($file_string = @file_get_contents($url)) {
			if(file_put_contents($xml_path, $file_string)) {
				if($xml = @simplexml_load_file($xml_path)) {
					$this->mtime = $now;
					return $xml;
				}
			}
		}
		
		# All else failed. Bookmark error and return FALSE
		file_put_contents($txt_path, 'Error occured');
		return FALSE;
	}
	
	private function rates_array() {
		$xml = $this->get_xml();
		if(empty($xml)) {
			return FALSE;
		}
		
		$rates = array();
		
		foreach($xml->Cube->Cube->children() as $value) {
			unset($currency, $rate);
			$attributes = $value->attributes();
			$currency = (string) strtolower($attributes->currency);
			$rate = (string) $attributes->rate;
			$rates[$currency] = $rate;
		}
		
		if(!empty($rates) && is_array($rates)) {
			$rates['eur'] = 1;
			ksort($rates);
			return $rates;
		}
		
		return FALSE;
	}
}
