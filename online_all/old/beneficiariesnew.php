<?php
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";
include_once "includes/functions.php";
$benCountries = "select countryId,countryName,isocode,ibanlength from countries";
$strSendingCurrency = $_POST['sendingCurrency'];
$strReceivingCurrency = $_POST['receivingCurrency'];
$fltExchangeRate = $_POST['exchangeRate'];
if ($_POST["accept"] == "ACCEPT QUOTE") {
	activities($_SESSION["loggedInUser"]["userID"], "Accept Quote", "", "", "Quote accepted for " . $strSendingCurrency . " - " . $strReceivingCurrency . " = " . $fltExchangeRate . "");
}

$benCountriesResult = selectMultiRecords($benCountries);
$intValid = false;
$recievingCurrency = $_POST['receivingCurrency'];
if (!empty($_POST['receivingAmount']) && $_POST['receivingAmount'] > '0' && !empty($_POST['sendingAmount']) && $_POST['sendingAmount'] > '0' && !empty($_POST['receivingCurrency']) && !empty($_POST['sendingCurrency']) && !empty($_POST['exchangeRate'])) {
	$intValid = true;
	$_SESSION['receivingAmount'] = $_POST['receivingAmount'];
	$_SESSION['sendingAmount'] = $_POST['sendingAmount'];
	$_SESSION['receivingCurrency'] = $_POST['receivingCurrency'];
	$_SESSION['sendingCurrency'] = $_POST['sendingCurrency'];
	$_SESSION['exchangeRate'] = $_POST['exchangeRate'];

	$strQueryInsertAccept_Quote = "INSERT INTO acceptQuoteHistory (custID,buy_currency,sell_currency,buy_amount,sell_amount,rate) VALUES('" . $_SESSION['loggedInUser']['userID'] . "','" . $_POST['sendingCurrency'] . "','" . $_POST['receivingCurrency'] . "','" . $_POST['sendingAmount'] . "','" . $_POST['receivingAmount'] . "','" . $_POST['exchangeRate'] . "')";
	insertInto($strQueryInsertAccept_Quote);
}
if (!empty($_SESSION['receivingAmount']) && !empty($_SESSION['sendingAmount']) && !empty($_SESSION['receivingCurrency']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['exchangeRate'])) {
	$intValid = true;
	$strReceivingAmount = $_SESSION['receivingAmount'];
	$strSendingAmount = $_SESSION['sendingAmount'];
	$strReceivingCurrency = $_SESSION['receivingCurrency'];
	$strSendingCurrency = $_SESSION['sendingCurrency'];
	$strExchangeRate = $_SESSION['exchangeRate'];
}
if (!empty($_SESSION['errorIBAN'])) {
	$intValid = true;
	$intError = true;
	$strBenName = $_SESSION['benName'];
	$strBenCountry = $_SESSION['benCountry'];
	$strBenCurrency = $_SESSION['benCurrency'];
	$strSwift = $_SESSION['swift'];
	$strReference = $_SESSION['refernce'];
	$strExistBen = $_SESSION['existBen'];
	$strErrorIBAN = $_SESSION['errorIBAN'];

	unset($_SESSION['benCountry']);
	unset($_SESSION['benCurrency']);
	unset($_SESSION['swift']);
	unset($_SESSION['refernce']);
	unset($_SESSION['existBen']);
	unset($_SESSION['errorIBAN']);
}
$url = $_POST['url'];

$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];

?>

<!DOCTYPE HTML >
<html>
<head>
<title><?php echo $title; ?></title>
<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
    <meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="imagetoolbar" content="no">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/jquery.validate.js"></script>
<script type="text/javascript" src="javascript/date.js"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="javascript/ajaxupload.js"></script>


<script src="scripts/jquery-1.7.2.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script src="scripts/bootstrap-twipsy.js"></script>
<script src="scripts/bootstrap-popover.js"></script>
<script type='text/javascript'></script>
<style>
.myspanclass {
color:red;
font-size:13px;
padding: 0 7px 0 14px;
}
</style>
</head>

	<?php
if (empty($strUserId)) {
	$strUserId = $_SESSION[loggedInUser][userID];
}

$strAccountName = $_SESSION[loggedInUser][accountName];

if (empty($strEmail)) {
	$strEmail = $_SESSION[loggedInUser][email];
}
?>
<body>
<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
<img src="images/premier/formImages/loading.gif" alt=""/>
<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
</div>
</div>
<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Send Money</h2>
<?php include 'top-menu.php';?>
<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Send Money</p>
<p class="content_subheading">Choose a beneficiary account to receive the money, if you have not sent money to this
account before please create a new beneficiary using the form below.</p>
</div>
<!-- content area left-->
<div class="content_area_left">
<form id="benForTransaction" action="transactionnew.php" method="post" onSubmit="return checkAgree();" autocomplete="off" >

<div class="field_wrapper">
<h1 class="heading_beneficiary">Beneficiary information</h1>
<label class="form_label">Existing Beneficiary:<span style="color:red">*</span></label><br>

<select name="existBen" id="existBen" class="select_field" onChange="this.form.action = '<?php echo $_SERVER['PHP_SELF']; ?>'; this.form.submit();">
<option value="">- Select beneficiary for transaction -</option>
<?php
//$strQueryBen = "SELECT benID, beneficiaryName AS benName FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
$strQueryBen = "SELECT benID, firstName AS benName FROM " . TBL_BENEFICIARY . " WHERE customerID = '" . $_SESSION['loggedInUser']['userID'] . "' AND status = 'Enabled'";
$arrBens = selectMultiRecords($strQueryBen);

for ($x = 0; $x < count($arrBens); $x++) {
	$strQueryBen1 = "SELECT bankName FROM  ben_banks WHERE benId = '" . $arrBens[$x]['benID'] . "'";
	$arrBens1 = selectFrom($strQueryBen1);

	echo "<option value='" . $arrBens[$x]['benID'] . "'>" . $arrBens1['bankName'] . "</option>";
}
?>
</select>

<?php
if ($intError) {
	?>
<div>

<div><?php echo $strErrorIBAN; ?></div>

</div>
<?php
}
?>

<?php
$intBenExist = false;
if (!empty($_POST['existBen']) || isset($_SESSION['ben'])) {
	if (!empty($_POST['existBen'])) {
		$intBenID = trim($_POST['existBen']);
	} elseif (!empty($_SESSION['ben'])) {
		$intBenID = $_SESSION['ben'];
	}

	$intBenExist = true;

	$strQueryBenInfo = "SELECT countries.countryId, Country, firstName AS benName, benBankDetails.IBAN AS IBAN, benBankDetails.swiftCode AS swiftCode,benBankDetails.sortCode AS sortCode,benBankDetails.bankName AS accountName,benBankDetails.branchAddress  AS branchAddress,benBankDetails.accountNo AS accountNumber,benBankDetails.routingNumber AS routingNumber,reference,benBankDetails.sendCurrency as sendCurrency,benBankDetails.branchCode as branchCode  FROM " . TBL_BENEFICIARY . " AS ben LEFT JOIN " . TBL_BEN_BANK_DETAILS . " AS benBankDetails ON ben.benID = benBankDetails.benID left join countries on ben.Country=countries.countryName WHERE ben.benID = '$intBenID'";
	$arrBenInfo = selectFrom($strQueryBenInfo);

	$strSendCurrency = $arrBenInfo['sendCurrency'];
//debug($arrBenInfo);
}
?>





</div>

<div class="field_wrapper">
<label class="form_label">OR</label>
<br>
<?php
if (!$intBenExist) {
	?>
<label id="selben_cont_head" class="form_label">Add a new beneficiary</label>
<?php
}
?>
</div>

<div class="field_wrapper">
<h1 id="selben_info" class="heading_beneficiary">Beneficiary Information</h1>

<label id="selben_benC" class="form_label">Beneficiary Country:</label><br>
<span style="position:relative"><div style="position:absolute; top:-50px; left:4px; width:200px; font-size:13px; color:#666666; font-weight:bold;display:inline-block;text-align:center;	background-color:#E6A028;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;margin-right:5px;" id="countrymsgnote" class="input-medium"></div></span>

<select name="benCountry" class="select_field" id="benCountry" >
<option value="">- Select country for beneficiary -</option>
<?php
$strQueryCountries = "select distinct(countryName),countryId  from countries where 1";
$arrCountry = selectMultiRecords($strQueryCountries);
for ($x = 0; $x < count($arrCountry); $x++) {
	echo "<option value='" . $arrCountry[$x]['countryId'] . "'>" . $arrCountry[$x]['countryName'] . "</option>";
}
?>
</select>
<span class="myspanclass" id="countrySpan">Select the country please! </span>
</div>

<div class="field_wrapper" id="currency">
</select>

<label id="accname" class="form_label">Currency To Send:</label><br>

<input name="sendingCurrency" id="sendingCurrency" value ="<?=$_SESSION["receivingCurrency"];?>" maxlength = "30" type="text" class="form_fields" />
</div>

<div class="field_wrapper" id="accountNameCont">
<label id="accname" class="form_label">Beneficiary Account Name:<span style="color:red">*</span></label><br>

<input name="accountName" id="accountName" maxlength = "30" type="text" class="form_fields" />
<span class ="myspanclass" id ="spanAccountName">Enter the Beneficiary Account Name please!</span>
</div>

<div class="field_wrapper" id="accountNumberCont">
<label class="form_label" id="selben_accnum">Account Number:<span style="color:red">*</span></label><br>

<input name="accountNumber" id="accountNumber" type="text" class="form_fields" maxlength="40" onblur="check_field(this.id);" />
<span class ="myspanclass" id ="spanAccountNumber">Enter the Account Number please!</span>
</div>

<div class="field_wrapper" id="branchNameNumberCont">
<label id="selben_bname" class="form_label">Branch Name / Number:<span style="color:red">*</span></label><br>

<input name="branchNameNumber" id="branchNameNumber" type="text" class="form_fields" maxlength="40"/>
<span class="myspanclass" id="spanBranchNumber">Enter the Branch Number/Name please!</span>
</div>

<div class="field_wrapper" id="branchAddressCont">
<label class="form_label" id="selben_badd">Branch Address:<span style="color:red">*</span></label><br>

<input name="branchAddress" id="branchAddress" type="text" class="form_fields" maxlength="40"/>
<span class="myspanclass" id="spanBranchAddress">Enter the Branch Address please!</span>

</div>





<!--<div class="field_wrapper margin_400">
<input name="back" id="back" type="button" value="Go back" class="submit_btn" onClick="this.form.action='make_paymentnew.php';this.form.submit();"  />
<input name="goBack" value="back" type="hidden"/>

<input name="clear" id="clear" type="reset"  class="submit_btn" value="Clear details">
<input name="proceed" id="proceed" type="submit" value="Continue" class="submit_btn" onclick="return checkValidations();"/>

<input name="receivingAmount" type="hidden" value="<?php echo $strReceivingAmount; ?>"/>
<input name="sendingAmount" type="hidden" value="<?php echo $strSendingAmount; ?>"/>
<input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; ?>"/>

<input name="exchangeRate" type="hidden" value="<?php echo $strExchangeRate; ?>"/>
<input name="strSendCurrency" type="hidden" value="<?php //echo $strSendCurrency; ?>"/><input name="sendBenValue" id = "sendBenValue" type="hidden" />

</div>-->

 <div class="field_wrapper">
<input  id="agreeBenInfo" name="agreeBenInfo" type="checkbox" value="agreeBenInfo" class="bottom"/ style="text-align:right;">
<label class="form_label resp_label" style="font-weight:normal;font-size:13px;">I confirm that the details above are correct and I
understand that a fee of £30 will be charged if a
payment is to be returned.</label>
<br>
<span class="myspanclass" id="spanagreebenInfo">Please check the confirmation of beneficiary details correction!</span>
</div>

</div>
<div class="content_area_right margin_225" >
<div class="field_wrapper" id="routingNumberCont">
<label class="form_label" id="selben_rtnum">Routing Number:<span style="color:red">*</span></label><br>

<input name="routingNumber" id="routingNumber" type="text" class="form_fields" maxlength="40"/>
<span class="myspanclass" id="spanRouting">Enter the Routing Number please!</span>
</div>

<div class="field_wrapper" id="sortCodeCont">
<label class="form_label" id="selben_sortcode">Sort Code:<span style="color:red">*</span></label><br>

<input name="sortCode" id="sortCode" type="text" class="form_fields" maxlength="40"/>
<span class="myspanclass" id="spanSortCode">Enter the sort code please!</span>
</div>

<div class="field_wrapper" id="ibanCont">
<label class="form_label" id="selben_ibnnum">IBAN Number:<span style="color:red">*</span></label><br>

<input name="iban" id="iban" type="text" class="form_fields" maxlength="40" onBlur = "checkIBANvalid()"/>
<span class="myspanclass" id="spanIbanCorrect">Enter the correct IBAN Number please!</span>
<input name="isiban" id = "isiban" type="hidden" />
<span class="myspanclass" id="spanIban">Enter the Iban Number please!</span>
</div>

<div class="field_wrapper" id="swiftCont">
<label class="form_label" id="selben_swf">SWIFT Code:<span style="color:red">*</span></label><br>

<input name="swift" id="swift" type="text" class="form_fields" maxlength="16"/>
<span class="myspanclass" id="spanSwift">Enter the Swift Code please!</span>
<span class="please_do" style="color:red;font-size:13px;"></span>

</div>
<div class="field_wrapper">
<label class="form_label" id="selben_ref">Your Reference:</label><br>

<input name="reference" id="reference" type="text" maxlength ="15" class="form_fields"/>
</div>
</div>
<div class="button_wrapper margin_400">
<input name="back" id="back" type="button" value="Go back" class="submit_btn" onClick="this.form.action='make_paymentnew.php';this.form.submit();"  />
<input name="goBack" value="back" type="hidden"/>

<input name="clear" id="clear" type="reset"  class="submit_btn" value="Clear details">
<input name="proceed" id="proceed" type="submit" value="Continue" class="submit_btn" onclick="return checkValidations();"/>

<input name="receivingAmount" type="hidden" value="<?php echo $strReceivingAmount; ?>"/>
<input name="sendingAmount" type="hidden" value="<?php echo $strSendingAmount; ?>"/>
<input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; ?>"/>

<input name="exchangeRate" type="hidden" value="<?php echo $strExchangeRate; ?>"/>
<input name="strSendCurrency" type="hidden" value="<?php //echo $strSendCurrency; ?>"/><input name="sendBenValue" id = "sendBenValue" type="hidden" />

</div>


<div class="l">

<?
if(isset($arrBenInfo['countryId']))
{

$arrBenBankRules = benBankDetailRule($arrBenInfo['countryId']);
$strBenName = "Name: ".$arrBenInfo["benName"];
$accountName = $arrBenBankRules["accountName"];
$accNo = $arrBenBankRules["accNo"];
$branchNameNumber = $arrBenBankRules["branchNameNumber"];
$branchAddress = $arrBenBankRules["branchAddress"];
$swiftCode = $arrBenBankRules["swiftCode"];
$IBAN = $arrBenBankRules["iban"];
$routingNumber = $arrBenBankRules["routingNumber"];
$sortCode = $arrBenBankRules["sortCode"];
$strBenDetailFirst = "";

$strBenDetailSecond = "";


if($accountName == "Y")
{

if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Account Name: ".$arrBenInfo["accountName"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Account Name: ".$arrBenInfo["accountName"];

}
}
if($accNo == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Account No: ".$arrBenInfo["accountNumber"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Account No: ".$arrBenInfo["accountNumber"];

}
}
if($branchNameNumber == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Branch Name: ".$arrBenInfo["branchName"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Branch Name: ".$arrBenInfo["branchName"];

}
}
if($branchAddress == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Branch Address: ".$arrBenInfo["branchAddress"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Branch Address: ".$arrBenInfo["branchAddress"];

}
}
if($swiftCode == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Swift Code: ".$arrBenInfo["swiftCode"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Swift Code: ".$arrBenInfo["swiftCode"];

}
}
if($IBAN == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "IBAN: ".$arrBenInfo["IBAN"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "IBAN: ".$arrBenInfo["IBAN"];

}
}
if($routingNumber == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Routing Number: ".$arrBenInfo["routingNumber"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Routing Number: ".$arrBenInfo["routingNumber"];

}
}
if($sortCode == "Y")
{
if($strBenDetailFirst == "")
{
$strBenDetailFirst = "Sort Code: ".$arrBenInfo["sortCode"];

}
else if($strBenDetailSecond == "")
{
$strBenDetailSecond = "Sort Code: ".$arrBenInfo["sortCode"];

}
}

$_SESSION["benBame"] = $strBenName;
$_SESSION["firstBen"] = $strBenDetailFirst;
$_SESSION["secondBen"] = $strBenDetailSecond;
}
?>
</div>
</form>
</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include 'footer.php';?>
</div>
</div>
</body>

<script>
<?php

/*$strCountries = "{";
for($ii=0;$ii<count($benCountriesResult);$ii++)
{
$arrCount[] = "'".$benCountriesResult[$ii]["countryName"]."':{'c':'".$benCountriesResult[$ii]["countryName"]."','i':'".$benCountriesResult[$ii]["isocode"]."','g':'".$benCountriesResult[$ii]["ibanlength"]."'}";
//$strCountries .=
}
$strCountries .= implode(",",$arrCount);
$strCountries .= "}";*/
$strCountries = "Array(";
for ($ii = 0; $ii < count($benCountriesResult); $ii++) {
	$arrCount[] = "Array('" . strtoupper($benCountriesResult[$ii]["countryName"]) . "','" . strtoupper($benCountriesResult[$ii]["isocode"]) . "','" . $benCountriesResult[$ii]["ibanlength"] . "','" . $benCountriesResult[$ii]["countryId"] . "')";
//$strCountries .=
}
$strCountries .= implode(",", $arrCount);
$strCountries .= ")";

echo 'var arrCountrues =' . $strCountries . ';';

?>

function isValidIBAN($v){ //This function check if the checksum if correct
$v = $v.replace(/^(.{4})(.*)$/,"$2$1"); //Move the first 4 chars from left to the right
$v = $v.replace(/[A-Z]/g,function($e){return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10}); //Convert A-Z to 10-25
var $sum = 0;
var $ei = 1; //First exponent
for(var $i = $v.length - 1; $i >= 0; $i--){
$sum += $ei * parseInt($v.charAt($i),10); //multiply the digit by it's exponent
$ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
};
return $sum % 97 == 1;
}

var $patterns = { //Map automatic generated by IBAN-Patterns Online Tool
IBAN: function($v){
$v = $v.replace(/[ ]/g,'');
return /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/.test($v) && isValidIBAN($v);
},
};



function SelectOption(OptionListName, ListVal){
for (i=0; i < OptionListName.length; i++)
{
if (OptionListName.options[i].value == ListVal)
{
OptionListName.selectedIndex = i;
break;
}
}
}

$('#countryTip').popover({
trigger: 'hover',
offset: 5
});
$('#currencyTip').popover({
trigger: 'hover',
offset: 5
});
$('#benTip').popover({
trigger: 'hover',
offset: 5
});
$('#benNameTip').popover({
trigger: 'hover',
offset: 5
});
$('#accountNameTip').popover({
trigger: 'hover',
offset: 5
});
$('#accountNumberTip').popover({
trigger: 'hover',
offset: 5
});
$('#branchNameNumberTip').popover({
trigger: 'hover',
offset: 5
});

$('#branchAddressTip').popover({
trigger: 'hover',
offset: 5
});
$('#routingNumberTip').popover({
trigger: 'hover',
offset: 5
});
$('#sortCodeTip').popover({
trigger: 'hover',
offset: 5
});
$('#ibanTip').popover({
trigger: 'hover',
offset: 5
});
$('#swiftTip').popover({
trigger: 'hover',
offset: 5
});
$('#refTip').popover({
trigger: 'hover',
offset: 5
});

<?php
if ($intBenExist) {
	?>
SelectOption(document.forms[0].existBen, '<?php echo $intBenID; ?>');
SelectOption(document.forms[0].benCountry, '<?php echo $arrBenInfo['countryId']; ?>');
<? $_SESSION["ben"] = $intBenID;?>
//$('#sendingCurrency').val('<?php echo $strSendCurrency; ?>');
$("#sendBenValue").val($("#benCountry option:selected").val());
$('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
$('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
$('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
$('#routingNumber').val('<?php echo $arrBenInfo["routingNumber"]; ?>');
$('#branchNameNumber').val('<?php echo $arrBenInfo["branchCode"]; ?>');
$('#branchAddress').val('<?php echo $arrBenInfo["branchAddress"]; ?>');

//$('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
$('#iban').val('<?php echo $arrBenInfo['IBAN']; ?>');
$('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
$('#sortCode').val('<?php echo trim($arrBenInfo['sortCode']); ?>');
$('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
<?php
}
if ($intError) {
	?>
SelectOption(document.forms[0].existBen, '<?php echo $strExistBen; ?>');
SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountry; ?>');
$('#sendingCurrency').val('<?php echo $strSendCurrency; ?>');
$('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
$('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
$('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
$('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
$('#branchNameNumber').val('<?php echo $arrBenInfo["branchCode"]; ?>');
$('#iban').focus();
$('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
$('#sortCode').val('<?php echo $arrBenInfo['sortCode']; ?>');
$('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
$('#branchAddress').val('<?php echo $arrBenInfo["branchAddress"]; ?>');
<?php
}
?>
function checkAgree(){
var iban = $('#iban').val();
accountNumber = $('#accountNumber').val();
sortCode = $('#sortCode').val();
var country=$('#benCountry').val();
var checkLength=checkIBANLength(country,iban);
var isiban = $('#isiban').val();

   curr = $("#sendingCurrency").val();
      if (curr == 'GBP')
      {

	   if ($("#accountNumber").val() != '' && $("#sortCode").val() != ''){
	        return true;
	      }
		if(!($('#agreeBenInfo').is(':checked'))){
             hideDisplay();
             $("#spanagreebenInfo").show();
             return false;
           }

	  }
if(checkLength==false && $('#iban').is(':visible')){
        hideDisplay();
       $("#spanIbanCorrect").show();
      return false;
/* alert("Please enter the correct IBAN number for your beneficiary country");
//$('#iban').focus();
return false; */
}

//CY17 0020 0128 0000 0012 0052 7600
if(!$patterns.IBAN(iban) && $('#iban').is(':visible'))
{
       hideDisplay();
       $("#spanIbanCorrect").show();
      return false;
/* alert("Please enter the correct IBAN number for your beneficiary country");
return false; */
}
else
{

return true;
}

}

function checkIBANvalid(){

var iban = $('#iban').val();
var country=$('#sendBenValue').val();
		var checkLength=checkIBANLength(country,iban);
	console.log(checkLength);
		if(checkLength==false  && $('#iban').is(':visible')){
		console.log("first in");
		 hideDisplay();
       $("#spanIbanCorrect").show();
      return false;
		/* alert("Please enter the correct IBAN number for your beneficiary country 1");
		return false; */
		}

		if(!$patterns.IBAN(iban)  && $('#iban').is(':visible')){
	console.log("second in");
       hideDisplay();
       $("#spanIbanCorrect").show();
      return false;

		//alert("Please enter the correct IBAN number for your beneficiary country 2");
		//return false;
		}
		else
		{
console.log("success");
		return true;
		}

}

function checkIBANLength(country,iban){

for (var m=0;m<arrCountrues.length;m++) {
if(country==arrCountrues[m][3]){
if(iban.length !=arrCountrues[m][2]){

return false;
break;

}
if(iban.substr(0,2) !=arrCountrues[m][1]){

return false;
break;

}
}

}

return true;

}

$(document).ready(function() {

 document.oncut=new Function("return false");
document.oncopy=new Function("return false");
document.onpaste=new Function("return false");

 $(".myspanclass").hide();
     getBenRule();
 });
 function getBenRule(currency){


currency = $("#sendingCurrency").val();
$("#sendingCurrency").prop( "disabled", true );
$.ajax({
url: "ben_currency_base_rule.php",
type: "POST",
data: {
currency:currency,
},
cache: false,
dataType: 'json',
success: function(data){
if(!data){
errorTxt = "<p style='font-weight:bold;text-align:justify;'>We are sorry for the inconvenience, we are not allowing online deals in this currency. Please call support on the following numbers and place your order by phone.</p><br /><div style='font-weight:bold;'></div>";
//$('#overlay').fadeIn('slow');
$('#errorRule').fadeIn('slow');
$('#errorRule').html(errorTxt);
}else{

$('#errorRule').hide();
accountName = data.accountName;
accNo = data.accNo;
branchNameNumber = data.branchNameNumber;
branchAddress = data.branchAddress;
swiftCode = data.swiftCode;
IBAN = data.iban;
routingNumber = data.routingNumber;
sortCode = data.sortCode;
//$('#sendingCurrency').val("2");
var getrulesval=accountName+"|"+accNo+"|"+branchNameNumber+"|"+branchAddress+"|"+swiftCode+"|"+IBAN+"|"+routingNumber+"|"+sortCode;
//if(country != 'United Kingdom')
{
if(accountName == 'N'){
$('#accountName').attr('disabled', 'disabled');

$('#accountNameCont').fadeOut('fast');

}else if(accountName == 'Y'){
$('#accountName').removeAttr('disabled');
$('#accountNameCont').fadeIn('fast');

}
}
if(accNo == 'N'){
$('#accountNumber').attr('disabled', 'disabled');
$('#accountNumberCont').fadeOut('fast');
}else if(accNo == 'Y'){
$('#accountNumber').removeAttr('disabled');
$('#accountNumberCont').fadeIn('fast');
}

if(branchNameNumber == 'N'){
$('#branchNameNumber').attr('disabled', 'disabled');
$('#branchNameNumberCont').fadeOut('fast');
}else if(branchNameNumber == 'Y'){
$('#branchNameNumber').removeAttr('disabled');
$('#branchNameNumberCont').fadeIn('fast');
}

if(branchAddress == 'N'){
$('#branchAddress').attr('disabled', 'disabled');
$('#branchAddressCont').fadeOut('fast');
}else if(branchAddress == 'Y'){
$('#branchAddress').removeAttr('disabled');
$('#branchAddressCont').fadeIn('fast');
}
//alert(country);
//if(country != 'United Kingdom')

{
if(swiftCode == 'N'){
//$('#swift').attr('disabled', 'disabled');
$('#swiftCont').fadeOut('fast');
}else if(swiftCode == 'Y'){
$('#swift').removeAttr('disabled');
$('#swiftCont').fadeIn('fast');
}
}
//alert(country);
//if(country != 1)
{
if(IBAN == 'N'){

//	$('#iban').attr('disabled', 'disabled');
$('#ibanCont').fadeOut('fast');
$('#isiban').val('NO');
}else if(IBAN == 'Y'){

$('#iban').removeAttr('disabled');
$('#ibanCont').fadeIn('fast');
$('#isiban').val('YES');
}
}
if(routingNumber == 'N'){
//	$('#routingNumber').attr('disabled', 'disabled');
$('#routingNumberCont').fadeOut('fast');
}else if(routingNumber == 'Y'){
$('#routingNumber').removeAttr('disabled');
$('#routingNumberCont').fadeIn('fast');
}

if(sortCode == 'N'){
//if(country != 'United Kingdom')
{
//	$('#sortCode').attr('disabled', 'disabled');
}
$('#sortCodeCont').fadeOut('fast');
}else if(sortCode == 'Y'){
$('#sortCode').removeAttr('disabled');
$('#sortCodeCont').fadeIn('fast');
}

/*	CheckValidation(getrulesval);   */
}

}
});
/* if(field == 'country'){
onChangeCurrency(country);
} */
}

$('#benForTransaction').validate({
rules: {
benCountry:{
required: true
}
}
});

function check_field(id) {
var field = $( "#accountNumber" ).val();

if (isNaN(field)) {
alert('Please enter a numeric Account number');
$( "#accountNumber" ).val('');
}
}

function CheckValidation(val){

}
function checkGbpRule(){
}
// changed against 12882 by ghazanfar
function checkValidations(){
curr = $("#sendingCurrency").val();
      if (curr == 'GBP')
      {
	   if ($("#iban").val() != '' && !($('#agreeBenInfo').is(':checked'))){
		   hideDisplay();
           $("#spanagreebenInfo").show();
		   return false;
		  }
		   if ($("#iban").val() == '' && !($('#agreeBenInfo').is(':checked'))){
		    hideDisplay();
           $("#spanagreebenInfo").show();
		   return false;
		  }
      return true;

	  }
else if($('#benCountry').is(':visible') && $.trim($('#benCountry').val()) == ''){
hideDisplay();
$("#countrySpan").show();
return false;
}

else if($('#sendingCurrency').is(':visible') && $.trim($('#sendingCurrency').val()) == ''){
alert('Select the currency please!');
return false;
}

else if($('#benName').is(':visible') && $.trim($('#benName').val()) == ''){
alert('Select the Account Name please!');
return false;
}

else if($('#accountName').is(':visible') && $.trim($('#accountName').val()) == ''){
hideDisplay();
$("#spanAccountName").show();
return false;
}
else if($('#accountNumber').is(':visible') && $.trim($('#accountNumber').val()) == ''){
hideDisplay();
$("#spanAccountNumber").show();
return false;
}
else if($('#branchNameNumber').is(':visible') && $.trim($('#branchNameNumber').val()) == ''){
hideDisplay();
$("#spanBranchNumber").show();
return false;
}

else if($('#branchAddress').is(':visible') && $.trim($('#branchAddress').val()) == ''){
hideDisplay();
$("#spanBranchAddress").show();
return false;
}

else if($('#routingNumber').is(':visible') && $.trim($('#routingNumber').val()) == ''){
hideDisplay();
$("#spanRouting").show();
return false;
}
else if($('#sortCode').is(':visible') && $.trim($('#sortCode').val()) == ''){
hideDisplay();
$("#spanSortCode").show();
return false;
}
else if($('#iban').is(':visible') && $.trim($('#iban').val()) == ''){
hideDisplay();
$("#spanIban").show();
return false;
}


else if($('#swift').is(':visible') && $.trim($('#swift').val()) == ''){
hideDisplay();
$("#spanSwift").show();
return false;
}
else if(!($('#agreeBenInfo').is(':checked'))){
hideDisplay();
$("#spanagreebenInfo").show();
return false;
}

else if(!$('#accountNumber').is(':visible')){
$('#accountNumber').val('');
}

else if(!$('#sortCode').is(':visible')){
$('#sortCode').val('');
}

else if(!$('#swift').is(':visible')){
$('#swift').val('');
}

else if(!$('#routingNumber').is(':visible')){
$('#routingNumber').val('');
}
else {
return true;
}

}

//function to hide all validtions text
function hideDisplay(){

$(".myspanclass").each(function(){
$(this).hide();

});
}




</script>
</html>
