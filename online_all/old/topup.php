<?php 
	/**
	 * @package: Online Module	
	 * @Ticket : 12671
	 * @author : Imran Naqvi	
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	
?>

	
	<!DOCTYPE HTML>
<html>
	<head>
		<title><?= $title; ?></title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
        <meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		
		<!--<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />-->
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		
	<script src="scripts/jquery-1.7.2.min.js"></script>	
		<!--<script type="text/javascript" src="javascript/jquery.js"></script>-->
		
<!-- TICKET NUMBER 13105 ADDING A SLIDER BAR BY ARSLAN ZAFAR  -->	
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">-->

<style>
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
border: 1px solid #d3d3d3;
background: #98b2d1;;
font-weight: normal;
color: #555555;

}

  </style>
		
		<script type="text/javascript" src="./javascript/jquery.validate.js"></script>
	 
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
		
		
		<script src="scripts/bootstrap-twipsy.js"></script>

  
  
  <script>
 /* ========== TICKET NUMBER 13105 ADDING A SLIDER BAR BY ARSLAN ZAFAR */ 
  // $(function() {

// $( "#slider-range" ).slider({
// range: "min",
// value: 0,
// min: 0,
// max: 30000,
// slide: function( event, ui ) {
// $( "#sendingAmount" ).val( ui.value );
// }
// });
// $( "#sendingAmount" ).val( $( "#slider-range" ).slider( "value" ) ); 
// $("#sendingAmount").change(function () {
// var value = this.value;
// $( "#slider-range" ).slider( "value", parseInt(value) );
// });



// //TO DEFINE SLIDER AND EVENTS
// $('#slider-range').slider().bind({
// slidestop   : function(event,ui) {
// $( "#sendingAmount" ).blur();
// },
// });

// });	

  </script>
  
  
  
		<!--<script src="scripts/bootstrap-popover.js"></script>-->
		<script type='text/javascript'>
		
		
		
		
	$(function(){
	
	$("#topup").click(function(event){
	
		
	$.ajax({
	url: './ajaxCalls.php',
	
	method: 'POST',
	async: false,
	data:{		rtypeMode : "dailyTransLimit",
				customerID: $("#customerID").val()	
		},		
	success: function(data){
	//alert(data);
		if(data == 2 || data > 2){
		
		
		alert('You have reached your daily transaction limit .');
		//$("#topup").attr("disabled","disabled");
		
		event.preventDefault();
		return false;
		}
		
	}
	});
	
	});
	
	
	$("#topupForm").validate({
		
			rules:
			{
				receivingCurrency :{ required : true },
				sendingAmount 	  :{ required: true, number: true },
				receivingAmount 	  :{ required: true, number: true }
			}
		});
		
	
		
		 $('#sendingAmount').on('blur', function() {
		 getRate();
		 });
		 
		 $('#receivingCurrency').on('change', function() {
		 checkvalues();
		 });

		 $('#receivingAmount').on('blur', function() {
		 getinverserate();
		 });	
		 
		function checkvalues()
		{
			receivingAmount 	= $("#receivingAmount").val();
			sendingAmount 		= $("#sendingAmount").val();
			
			if(receivingAmount!="" && sendingAmount!="")
			{
				$("#receivingAmount").val("");
				$("#sendingAmount").val("");
				$("#xRate").text("");
				//alert("Enter value.");
			}
						
		}	
		function getRate()
		{

			receivingCurrency	= $("#receivingCurrency").val();
			sendingCurrency 	= $("#sendingCurrency").val();
			receivingAmount 	= $("#receivingAmount").val();
			sendingAmount 		= $("#sendingAmount").val();
			
			// if(receivingCurrency ==  '' ){
			// alert('Please select receiving currency');
			// return false;
			// }
			
			$.ajax({
			url:"./ajaxCalls.php",
			data: 
			{
			rtypeMode : "getExchangeRate",
			receivingCurrency: 	receivingCurrency,
			sendingCurrency : 	sendingCurrency,
			receivingAmount : 	receivingAmount,
			sendingAmount 	: 	sendingAmount,
			},
			method: "POST",
			success : function (data)
				{
					 
					dataSplit = data.split("|");
					 
					receivingAmount = $.trim(dataSplit[0]);	
					
					xRate = $.trim(dataSplit[1]);	
					
					preciseAmount=receivingAmount.replace(/\,/g,''); 

					//preciseAmount=parseInt(preciseAmount,10);
					
					checkLimits = validateAmountLimit(receivingCurrency,preciseAmount);

					if(checkLimits == false )
					{
						return false;
					}
					 
					if( isNaN(preciseAmount) == false){

					$(".content_area_right").css("display","none");	
					if(receivingCurrency != sendingCurrency)
						{
						$("#receivingAmount").val(preciseAmount);
						
						
						//$("#xRate").html('1 '+ sendingCurrency + ' = '+ xRate +' '+ receivingCurrency);
							
							$("#xRate").text('Rate = '+ xRate);
							
						}
					}else{
					if(receivingCurrency != ''){
					$("#receivingAmount").val("");
					$(".content_area_right").css("display","block");
					$("#invalid_amount p").html(receivingAmount);
					}
					}

				}
			});
		}
		
		function getinverserate()
		{

			receivingCurrency	= $("#receivingCurrency").val();
			sendingCurrency 	= $("#sendingCurrency").val();
			receivingAmount 	= $("#receivingAmount").val();
			sendingAmount 		= $("#sendingAmount").val();
			
			// if(receivingCurrency ==  '' ){
			// alert('Please select receiving currency');
			// return false;
			// }
			
			$.ajax({
			url:"./ajaxCalls.php",
			data: 
			{
			rtypeMode : "getExchangeRate",
			receivingCurrency: 	receivingCurrency,
			sendingCurrency : 	sendingCurrency,
			receivingAmount : 	receivingAmount,
			sendingAmount 	: 	sendingAmount,
			},
			method: "POST",
			success : function (data)
				{
					 
					dataSplit = data.split("|");
					 
					//receivingAmount = $.trim(dataSplit[0]);	
					
					xRate = $.trim(dataSplit[1]);	
					
					inverseRate = $.trim(dataSplit[2]);	
					if(receivingAmount!="")
					{
						sendingAmount=receivingAmount/xRate;
						sendingAmount=sendingAmount.toFixed(2);
					}
					//alert(receivingAmount);
					preciseAmount=receivingAmount.replace(/\,/g,''); 

					//preciseAmount=parseInt(preciseAmount,10);
					
					checkLimits = validateAmountLimit(receivingCurrency,preciseAmount);

					if(checkLimits == false )
					{
						return false;
					}
					
					
					 
					if( isNaN(preciseAmount) == false && preciseAmount != 0){

					$(".content_area_right").css("display","none");	
					if(receivingCurrency != sendingCurrency)
						{
						$("#sendingAmount").val(sendingAmount);
						
						
						//$("#xRate").html('1 '+ sendingCurrency + ' = '+ xRate +' '+ receivingCurrency);
							
							$("#xRate").text('Rate = '+ xRate);
							
						}
					}else{
					if(receivingCurrency != ''){
					$("#sendingAmount").val("");
					$(".content_area_right").css("display","block");
					$("#invalid_amount p").html(receivingAmount);
					}
					}

				}
			});
		}
		
		
		function validateAmountLimit(receivingCurrency,receivingAmount){

		var returnAmount = true;
		
		returnAmount = amountLimitPerTransaction(receivingCurrency,receivingAmount);

		if(returnAmount != false){
		
		 amountLimitPerMonth(receivingCurrency,function(receivingAmount){
		returnAmount = receivingAmount ;
		}); 

		}
		 
		 return returnAmount;
		}
		
		function amountLimitPerTransaction(receivingCurrency,receivingAmount)
		{
		
			if(receivingCurrency == 'EUR')
			{
			if(receivingAmount < 10 && receivingAmount != 0)
				{
					alert('you can not send money less than 10');
					$('#receivingAmount').val('');
					return false ;
				}else if(receivingAmount > 5000){
					alert('You can not send money more than 5,000');
					$('#receivingAmount').val('');
					return false ;
				}
			}else if(receivingCurrency == 'USD' && receivingAmount != 0)
			{
		
	
				if(receivingAmount < 10)
				{
					alert('you can not send money less than 10');
					$('#receivingAmount').val('');
					return false ;
				}else if(receivingAmount > 6000){
					alert('You can not send money more than 6,000');
					$('#receivingAmount').val('');
					return false ;
				}
			}else if(receivingCurrency == 'GBP' && receivingAmount != 0)
			{
				if(receivingAmount < 10)
				{
					alert('you can not send money less than 10');
					$('#receivingAmount').val('');
					return false ;
				}else if(receivingAmount > 4000){
					alert('You can not send money more than 4,000');
					$('#receivingAmount').val('');
					return false ;
				}
			}
			return true;
		}
		
		
		function amountLimitPerMonth(receivingCurrency,receivingAmount)
		{
		var returnValue = true;
		
		$.ajax({
		url: './ajaxCalls.php',
		data:{	rtypeMode: 'amountLimitPerMonth'	,
				customerID: $("#customerID").val()
		},
		async : false,
		success: function(data){
		
		amount = parseFloat($.trim(data)) + parseFloat($("#sendingAmount").val());
		 
				if(receivingCurrency == 'GBP')
				{
					if($.trim(amount) > 20000 )
					{
						alert('Monthy limit 20,000 cross for sending amount');
						$('#receivingAmount').val('');
						returnValue = false;
					}
				}else if(receivingCurrency == 'EUR')
				{
					if($.trim(amount) > 20000 )
					{
						alert('Monthy limit 20,000 cross for sending amount');
						$('#receivingAmount').val('');
						returnValue = false;
					}
				}else if(receivingCurrency == 'USD')
				{
					if($.trim(amount) > 20000 )
					{
						alert('Monthy limit 20,000 cross for sending amount');
						$('#receivingAmount').val('');
						returnValue = false;
					}
				}
			receivingAmount(returnValue);
			}
			
		});
		
	  }  
		
	});
		
	</script>
		<style>
		#xRate{
		font-style:italic;
		font-weight:bold;
		color:#555;
		font-size:12px;
		position: absolute;
		margin-top: 18px;
		color: #678dbb;
		font-weight:bold;
		font-size:18px;
		font-style:normal;
		}
		.field_wrapper{
		 width: 357px !important; 
		}
		</style>
	</head>
	<body>
	
<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
				<div class="logout_area">
<h2 class="heading">Top Up</h2>
<?php include('top-menu.php');?>


<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Top Up Card</p>
<p class="content_subheading">Enter the currency and amount to Top Up.</p>
</div>

<!-- content area left-->
<div class="content_area_left topup_custom">

    <form action="debit_card.php" method="post" id="topupForm">
        <input type="hidden" id="customerID" name="customerID" value="<?= $_SESSION['loggedInUser']['userID'] ?>"/>

        <div class="field_wrapper">
        <div id="slider-range"></div>

</div>
    <div class="field_wrapper topup_custom_field">
        <label class="form_label">I have <span>GBP</span></label>

        <input name="sendingAmount" id="sendingAmount" maxlength="20" class="form_fields " autocomplete="off"
               type="text">
        <br/>
        <label class="form_label">Buy/Top Up: <span></span></label>


        <!--<input name="receivingCurrency" id="receivingCurrency" maxlength="20" class="form_fields " autocomplete="off" value="<?= $_SESSION["loggedInUser"]["card_issue_currency"] ?>" type="text" readOnly />-->

        <select name="receivingCurrency" id="receivingCurrency" class="select_field ">
            <option value="USD">USD</option>
            <option value="EUR">EUR</option>

        </select>


        <input type="hidden" id="sendingCurrency" name="sendingCurrency" value="GBP" readOnly maxlength="20"
               class="select_field" autocomplete="off"/>
        &nbsp;<span id="xRate" class="xRate"></span>
        <br/><label class="form_label">Receiving Amount<span></span>:</label>

        <input name="receivingAmount" id="receivingAmount" maxlength="20" class="form_fields " autocomplete="off"
               type="text">
    </div>

</div>


                    <!-- content area right-->
                    <div class="content_area_right" style="display:none;">
                        <div class="info_change_password" id="invalid_amount"><p></p></div>
                    </div>

                    <div class="button_wrapper">
                        <input name="topup" id="topup" type="submit" value="Continue" class="submit_btn"/>
                        <input type="reset" value="Clear" class="clear_btn"/>
                        <a href="#" class="tip" id="topupTip" data-placement="above" rel="popover"
                           data-content="Click the get rate button to get the rate according to the currency selection"
                           title="Get Rate Button"> [?] </a>
                    </div>
                    </form>

<!-- Error message shown here -->
<?php 
if(isset($strError) && !empty($strError)){
?>
<div class="error_msg"><?php echo $strError; ?></div>

<?php 
}
if(isset($strMsg) && !empty($strMsg)){
?>
<div class="error_msg"><?php echo $strMsg; ?></div>

<?php 
}
?> 
<!-- Error message shown here ends area -->
</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
    <?php include('footer.php');?>
</div>
</div>
</body>
</html>
<script type='text/javascript'>
    $(function(){
        $("#topupForm").validate({
            rules:{
                sendingAmount : {
                    required: true,
                    number: true
                }
                },
            messages:{
                sendingAmount: {
                    required:'Plz provide amount to send',
                    number: 'Only integers allowed',
                }

            }
        });
    });
</script>
