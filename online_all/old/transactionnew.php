<?php
/**
* @package: Online Module	
* @subpackage: Confirm Page
* @author: Awais Umer
*/
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "javascript/audit-functions.php";
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

$strUserId=$_SESSION['loggedInUser']['accountName'];
$strEmail=$_SESSION['loggedInUser']['email'];

if($_POST["existBen"] != ""){
$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = ".$_POST["existBen"]." ";
$fetPreviousArr = selectFrom($getPreviousArr);	
/* $updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
`sendCurrency` = '".$_POST["sendingCurrency"]."',
accountNo	=	'".$_POST["accountNumber"]."',
sortCode	=	'".$_POST["sortCode"]."',
iban		=	'".$_POST["iban"]."'
WHERE benId = 	".$_POST["existBen"]."
"; */
// changed as send currecny is now changed in recieving currency

$updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
`sendCurrency` = '".$_POST["receivingCurrency"]."',
accountNo	=	'".$_POST["accountNumber"]."',
sortCode	=	'".$_POST["sortCode"]."',
iban		=	'".$_POST["iban"]."'
WHERE benId = 	".$_POST["existBen"]."
";
if(update($updateBenBank)){

activities($_SESSION["loginHistoryID"],"UPDATED",$_SESSION["loggedInUser"]["accountName"],"ben_banks","ben_banks updated successfully");

logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$_POST["existBen"],$_SESSION["loggedInUser"]["accountName"]);

}
}
?>

<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $title; ?></title>
    <meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
    <meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->

<script src="scripts/jquery-1.7.2.min.js"></script>
<script src="scripts/jquery.validate.js"></script>
<script type="text/javascript">



$(document).ready(function() {
$("#proceed").click(function(){
window.btn_clicked = true;
}
);

$(window).bind("beforeunload",function(event) {
if(!window.btn_clicked){

if(/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
if(confirm("You will not get the rate as good as this for your transaction. Are you sure you want to lose it?")) {
history.go();
} else {
window.setTimeout(function() {
window.stop();
}, 1);
}
} else {
return "You will not get the rate as good as this for your transaction. Are you sure you want to lose it?";
}
}
}
);

}
);


</script>
</head>
<body>
<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>
				</div>
				
<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Send Money</h2>
<?php include('top-menu.php');?>



<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Send Money</p>
<p class="content_subheading">Please check and confirm the transaction details below are correct.</p>
</div>
<!-- content area left-->
<div class="content_area_left">


<h1 class="heading_beneficiary">Transaction details</h1>
<div class="field_wrapper">
<label class="form_label">Exchange rate:</label>
<span class="rate_info" style="font-size:17px;"> <? echo number_format($_SESSION['exchangeRate'],4,".",","); ?></span>
</div>

<div class="field_wrapper">
<label class="form_label ">You are selling:</label>
<span class="rate_info" style="font-size:17px;"> <? echo number_format($_SESSION['sendingAmount'],2,".",",")." ". $_SESSION['sendingCurrency'] ?></span>
</div>

<div class="field_wrapper">
<label class="form_label ">You are buying:</label>
<span class="rate_info" style="font-size:17px;"> <? echo number_format($_SESSION['receivingAmount'],2,".",",") ." ". $_SESSION['receivingCurrency']; ?></span>
</div>
<div class="field_wrapper">
<p class="info_para custom_width">Once your funds have been received we will credit your chosen account within 48 hours</p>
</div>

<h1 class="heading_beneficiary">Beneficiary details</h1>

<?php if(isset($_POST["accountName"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Beneficiary name:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['accountName']?></span>
</div>

<? } ?>
<?php if(isset($_POST["accountNumber"])  && !empty($_POST["accountNumber"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Account Number:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['accountNumber']?></span>
</div>

<? } ?>
<?php if(isset($_POST["branchNameNumber"])  && !empty($_POST["branchNameNumber"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Branch Name/Number:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['branchNameNumber'] ?></span>
</div>


<? } ?>
<?php if(isset($_POST["branchAddress"])  && !empty($_POST["branchAddress"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Branch Address:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['branchAddress']?></span>
</div>
<? } ?>

<?php if(isset($_POST["routingNumber"])  && !empty($_POST["routingNumber"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Routing Number:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['routingNumber']?></span>
</div>
<? } ?>

<?php if(isset($_POST["sortCode"])  && !empty($_POST["sortCode"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Sort Code:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['sortCode']?></span>
</div>
<? } ?>

<?php if(isset($_POST["iban"]) && !empty($_POST["iban"]))
{
?>
<div class="field_wrapper iban_field">
<label class="form_label">IBAN Number:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['iban']?></span>
</div>
<? } ?>

<?php if(isset($_POST["swift"]) && !empty($_POST["swift"]))
{
?>
<div class="field_wrapper">
<label class="form_label">Swift:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['swift']?></span>
</div>
<? } ?>

<div class="field_wrapper">
<label class="form_label">Reference:</label>
<span class="rate_info" style="font-size:17px;"> <? echo $_POST['reference']?> </span>
</div>

<h1 class="heading_beneficiary">Deal Confirmation</h1>
<div class="field_wrapper custom_width">
<p class="info_para">I accept that by clicking Confirm button I am entering into a legally binding contract and a cost may be Charged to change these details.</p>
</div>

<form name="confirmTrans" id="confirmTrans" method="post" action="">
<input id="benName" name="benName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
<input id="benCountry" name="benCountry" type="hidden" value="<? echo $_POST['benCountry'] ?>" />
<input id="benCurrency" name="benCurrency" type="hidden" value="<? echo $_POST['benCurrency'] ?>" />
<input id="accountName" name="accountName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
<input id="accountNumber" name="accountNumber" type="hidden" value="<? echo $_POST['accountNumber'] ?>" />
<input id="branchNameNumber" name="branchNameNumber" type="hidden" value="<? echo $_POST['branchNameNumber'] ?>" />
<input id="branchAddress" name="branchAddress" type="hidden" value="<? echo $_POST['branchAddress'] ?>" />
<input id="routingNumber" name="routingNumber" type="hidden" value="<? echo $_POST['routingNumber'] ?>" />
<input id="sortCode" name="sortCode" type="hidden" value="<? echo $_POST['sortCode'] ?>" />
<input id="swift" name="swift" type="hidden" value="<? echo $_POST['swift'] ?>" />
<input id="reference" name="reference" type="hidden" value="<? echo $_POST['reference'] ?>" />
<input id="existBen" name="existBen" type="hidden" value="<? echo $_POST['existBen'] ?>" />
<input id="iban" name="iban" type="hidden" value="<? echo $_POST['iban'] ?>" />
<input type="hidden" name="transType" value="Bank Transfer" />
<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? echo $_POST['sendingCurrency'] ?>" />

<!--<div class="field_wrapper margin_400">
<input id="back" class="submit_btn" type="button" onClick="this.form.action='beneficiariesnew.php';this.form.submit();" value="Go back" name="back"/>

<? if($_SESSION['sendingCurrency'] != "GBP"){
?>
<input name="proceed" id="proceed" type="submit" onClick="this.form.action='payment_detailnew.php';this.form.submit();" value="Confirm Transaction" class="submit_btn"/>
<?}
else 
{?>
<input name="proceed" id="proceed" type="submit" onClick="this.form.action='debit_card.php';this.form.submit();" value="Confirm Transaction" class="submit_btn"/>
<?}
?>

</div>-->



</div>

<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password margin_less">
<p>Check all the details of your trade and press confirm to complete your transaction

Click go back if you wish to change or amend any details
</p>
</div>-->
</div>

<div class="button_wrapper margin_400">
<input id="back" class="submit_btn" type="button" onClick="this.form.action='beneficiariesnew.php';this.form.submit();" value="Go back" name="back"/>

<? if($_SESSION['sendingCurrency'] != "GBP"){?>
<input name="proceed" id="proceed" type="submit" onClick="this.form.action='payment_detailnew.php';this.form.submit();" value="Confirm Transaction" class="submit_btn"/>
<?} else {?>
<input name="proceed" id="proceed" type="submit" onClick="this.form.action='debit_card.php';this.form.submit();" value="Confirm Transaction" class="submit_btn"/>
<? } ?>

</div>
</form>

</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>
</html>
