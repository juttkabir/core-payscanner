<?php
session_start();
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "includes/configs.php";
include_once "includes/database_connection.php";
include_once "includes/functions.php";
//ini_set("display_errors","on");
$db = dbConnect();
//debug($_POST);
require_once "../admin/lib/phpmailer/class.phpmailer.php";
if (!empty($_POST['customerNumber']) && !empty($_POST['day']) && !empty($_POST['month']) && !empty($_POST['year']) && !empty($_POST['email'])) {

	$strCustomerNumber = trim($_POST['customerNumber']);
	$strBirthDate = $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day'];
	$strQueryVerification = "SELECT customerName,accountName, email, customerID, customerStatus FROM " . TBL_CUSTOMER . " WHERE accountName = '$strCustomerNumber' AND dob = '$strBirthDate' AND email = '" . $_POST['email'] . "' ";
	$arrCustomer = selectFrom($strQueryVerification);
	//print_r($arrCustomer);
	$intFlagVerify = false;
	if (!empty($arrCustomer['customerID'])) {
		$password = generateRandomString();
		// debug($password);
		$intFlagVerify = true;
		$_SESSION['issuePassCustomerID'] = $arrCustomer['customerID'];
		$strUpdatePassword = "UPDATE " . TBL_CUSTOMER . " SET `password` = '$password' WHERE customerID ='" . $arrCustomer['customerID'] . "'";
		update($strUpdatePassword);
		$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='6' and email_templates.status = 'Enable' ");
		$logoCompany = "<img border='0' src='http://" . $_SERVER['HTTP_HOST'] . "/admin/images/premier/logo.jpg?v=1' />";
		$message1 = $emailContent["body"];
		$varEmail = array("{customer}", "{email}", "{password}", "{customernumber}", "{logo}");
		$contentEmail = array($arrCustomer['customerName'], $arrCustomer['email'], $password, $arrCustomer["accountName"], $logoCompany);

		$messageT = str_replace($varEmail, $contentEmail, $message1);
		//debug($messageT);
		//Adedd by Waleed Nadeem  against Ticket #10825 Email Template Maanger
		$toField = explode('/', $emailContent['to']);

		for ($j = 0; $j < count($toField); $j++) {

			if ($toField[$j] == 'Sender' || $toField[$j] == 'Beneficiary') {
				//do Nothing
			} else {

				$getEmail = selectFrom("select email from admin where userID='" . $toField[$j] . "'");

				if (!empty($getEmail)) {
					$sendermailer->AddBCC($getEmail['email'], '');
				}

			}
		}

		$subject = $emailContent["subject"];
		$customerMailer = new PHPMailer();
		$strBody = $messageT;
		$fromName = $emailContent["fromName"];
		$fromEmail = $emailContent["fromEmail"];
		$customerMailer->FromName = $fromName;
		$customerMailer->From = $fromEmail;

		//die($emailContent['cc']);

		//Add BCC
		//if($emailContent['bcc'])
		//debug(CONFIG_SEND_EMAIL_ON_TEST);

		if (CONFIG_SEND_EMAIL_ON_TEST == "1") {
			$customerMailer->AddAddress(CONFIG_TEST_EMAIL, '');
		} else {
			$customerMailer->AddAddress($arrCustomer['email'], '');
			if ($emailContent['bcc']) {
				$customerMailer->AddBCC($emailContent['bcc'], '');
			}

			if ($emailContent['cc']) {
				$customerMailer->AddAddress($emailContent['cc'], '');
			}

		}
		$customerMailer->Subject = $subject;
		$customerMailer->IsHTML(true);
		$customerMailer->Body = $strBody;
		//		debug($customerMailer);
		//debug(CONFIG_EMAIL_STATUS);

		$custEmailFlag = $customerMailer->Send();

		activities($_POST['customerNumber'], "UPDATION", $_POST['customerNumber'], "customer", "Customer changed his password through forgot password");

		$strSuccess = "Password reset mail sent to the customer";

		/*
			Mailer
			$arrCustomer = selectFrom($strQueryCustomer);
				$subject = "Issue Password";
				$customerMailer = new PHPMailer();
				$strBody = "Dear ".$arrCustomer[$x]['customerName']."\r\n<br />";
				$strBody .= "Your Trading Account password has been set.\n<br />
				Your username is: ".$arrCustomer[$x]['email']."\r\n<br />
				Your new password is: [ ".$password." ]\r\n\n\r<br /><br />
				\n<br />".$arrCustomer[$x]["adminName"]."<br />\n
				Follow this link to login: http://clients.premfx.com/private_registration.php\n<br />
				<table>
					<tr>
						<td align='left' width='30%'>
						<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='20%'>&nbsp;</td>
						<td align='left'>
							<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
							55 Old Broad Street,London, EC2M 1RX.\n<br>
							<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
							<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
							Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
							<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
							<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
							<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
							C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
							<font color='#F7A30B'>Tel:</font> +34 971 576 724
							 \n <br>
							 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
						</td>
					</tr>
				</table>
				";
				$fromName  = SYSTEM;
				$customerMailer->FromName =  $fromName;
				$customerMailer->From =  $arrCustomer[$x]['adminEmail'];
				$customerMailer->AddAddress($arrCustomer[$x]['email'],'');
				$customerMailer->Subject = $subject;
				$customerMailer->IsHTML(true);
				$customerMailer->Body = $strBody;
				$custEmailFlag = $customerMailer->Send();
		 */
		//		debug($strSuccess);
	} else {
		$strError = "The details you entered are not correct.";

	}
}
//	debug($_POST);

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
        <meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward tradingm" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
        <meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
        <!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
        <link href="css/style_new.css" rel="stylesheet" type="text/css">
        <link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		<script type="text/javascript" src="javascript/jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
		<script type='text/javascript'></script>

		<!-- Google Analytics -->
		<?php include '_ga.inc.php';?>

	</head>
	<body>
<!-- main container-->
<div class="container">

<?php
if (!empty($_SESSION['loggedInUser']['userID'])){
    include ('menu.php');
}else{
    echo '
    <div class="upper_container">
        <div class="header">
            <div class="logo">
                ' . $logoHtml . '
            </div>
        </div>
    </div>';
}
?>

<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Change Password</h2>
<?php include 'top-menu.php';?>
<!-- content area -->
<div class="content_area">
<!-- content area left-->
<div class="content_area_left">
<p class="content_heading">Change Your Password</p>
<p class="content_subheading">You may change your password using the form below</p>
<form id="verifyIdentityFrom" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<div class="field_wrapper">
<label class="form_label">Customer Number:<span style="color:red">*</span></label><br>

<input name="customerNumber" id="customerNumber" class="form_fields"  type="text" maxlength="8"/>
</div>

    <div class="field_wrapper">
        <label class="form_label">Date of birth:<span style="color:red">*</span></label><br>

        <select id="day" name="day" class="select_mini">
            <option value="">Day</option>
            <?php
            for ($x = 1; $x <= 31; $x++) {
                $strDay = str_pad($x, 2, "0", STR_PAD_LEFT);
                echo "<option value='$strDay'>$strDay</option>";
            } ?>
        </select>

        <select  id="month" name="month" class="select_mini">
            <option value="">Month</option>
            <?php
            for ($x = 1; $x <= 12; $x++) {
                $strMonth = str_pad($x, 2, "0", STR_PAD_LEFT);
                echo "<option value='$strMonth'>$strMonth</option>";
            } ?>
        </select>

        <select id="year" name="year"class="select_mini">
            <option value="">Year</option>
            <?php
            for ($y = 1900; $y <= 1996; $y++) {
                echo "<option value='$y'>$y</option>";
            } ?>
        </select>
    </div>

    <div class="field_wrapper">
        <label class="form_label">Email:<span style="color:red">*</span></label><br>
        <input name="email" id="email" type="text" class="form_fields" maxlength="32" class="input1" />
    </div>

<!--<div class="field_wrapper">
<input name="changePassword" id="changePassword" type="submit" value="Submit" class="submit_btn"/>
<input id="Back" class="submit_btn" type="button" onClick="window.location.href='index.php'" value="Back" name="back">

</div>-->


</div>

<!-- content area right-->
    <div class="content_area_right">

    </div>

<div class="button_wrapper">
<input name="changePassword" id="changePassword" type="submit" value="Submit" class="submit_btn"/>
<input id="Back" class="submit_btn" type="button" onClick="window.location.href='index.php'" value="Back" name="back">

</div>
</form>

    <!-- Error message shown here -->
    <?php
    if (!empty($strError)) {
        echo '<div class="error_msg">' . $strError . '</div>';
    } else if (!empty($strSuccess)) {
        echo '<div class="error_msg">' . $strSuccess . '</div>';
    } ?>
    <!-- Error message shown here ends area -->

</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include 'footer.php';?>
</div>
</div>
</body>
	<script type="text/javascript">
        function checkDob(){
            if($('#day').val() == ''){
                alert('Enter the day of your date of birth');
                $(this).focus();
                return false;
            }
            if($('#month').val() == ''){
                alert('Enter the month of your date of birth');
                $(this).focus();
                return false;
            }
            if($('#year').val() == ''){
                alert('Enter the year of your date of birth');
                $(this).focus();
                return false;
            }
            return true;
        }

        $('#verifyIdentityFrom').submit(
            function(){
                return checkDob();
            }
        );

        $('#verifyIdentityFrom').validate({
            rules: {
                customerNumber: {
                    required: true,
                    maxlength: 8
                },
                email: {
                    required: true,
                    email: true
                }
            }
        });
    </script>
</html>