<?php
/**
 * @package: Online Module
 * @subpackage: view beneficiaries
 * @author: Mirza Arslan Baig
 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php");
}
$limit = 10;
if (!empty($_GET['offset']))
    $offset = $_GET['offset'];
else
    $offset = 0;
$next = $offset + $limit;
$prev = $offset - $limit;

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];


if (($_REQUEST["benCountry"] == '') && ($_REQUEST["benName"] != '')) {
    $name = $_REQUEST["benName"];
    $query = " (firstName like '%" . $name . "%' OR middleName like '%" . $name . "%' AND lastName like '%" . $name . "%')AND ";
} else if (($_REQUEST["benCountry"] != '') && ($_REQUEST["benName"] == '')) {
    $strbenCountry = $_REQUEST["benCountry"];
    $query = " (Country like '%" . $strbenCountry . "%')AND ";
} else if (($_REQUEST["benCountry"] != '') && ($_REQUEST["benName"] != '')) {
    $name = $_REQUEST["benName"];
    $strbenCountry = $_REQUEST["benCountry"];
    $query = " (firstName like '%" . $name . "%' OR middleName like '%" . $name . "%' AND lastName like '%" . $name . "%')AND (Country like '%" . $strbenCountry . "%') AND ";
} else {
    $query = "";
}


/**Ticket 10651**/
//$strQueryTotalBens = "SELECT COUNT(benID) AS records FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
$strQueryTotalBens = " SELECT   COUNT(benID)AS records FROM " . TBL_BENEFICIARY . " WHERE  " . $query . " `customerID` = '" . $_SESSION['loggedInUser']['userID'] . "'  AND status = 'Enabled'";
//debug($strQueryTotalBens);
/* $strQueryBen = "SELECT benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' ORDER BY benID DESC status = 'Enabled' LIMIT $offset, $limit";
*/

$strQueryBen = " SELECT  benID, CONCAT(firstName, middleName, lastName) AS name, Country, DATE_FORMAT(created, '%d-%m-%Y') AS created FROM " . TBL_BENEFICIARY . " WHERE  " . $query . " `customerID` = '" . $_SESSION['loggedInUser']['userID'] . "'  AND status = 'Enabled' ORDER BY `benID`DESC LIMIT " . $offset . " , " . $limit . " ";
//debug($strQueryBen);

$arrTotalBens = selectFrom($strQueryTotalBens);

$arrBens = selectMultiRecords($strQueryBen);
$intTotalBens = count($arrBens);
$intAllBens = $arrTotalBens['records'];
$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title; ?></title>
    <meta name="description"
          content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property."/>
    <meta name="keywords"
          content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading"/>
    <meta http-equiv="Content-Type" content="text/htm, charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
    <!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
    <link href="css/style_new.css" rel="stylesheet" type="text/css">
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css">
    <link href="css/datePicker.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap1.css" type="text/css" rel="stylesheet"/>
    <!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="javascript/jquery.validate.js"></script>
    <script type="text/javascript" src="javascript/date.js"></script>
    <script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
    <script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="javascript/ajaxupload.js"></script>
</head>
<body>

<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
    <!-- lower container -->
    <div class="lower_container">
        <div class="body_area">
            <div class="logout_area">
                <h2 class="heading">Beneficiaries</h2>
                <?php include('top-menu.php'); ?>
                <!-- content area -->
                <div class="content_area">
                    <!-- content area left-->
                    <div class="content_area_left">
                        <p class="content_heading">Search your beneficiaries</p>
                        <!--<p class="content_subheading">You may change your password using the form below</p>-->
                        <form action="" name="benSearch" method="post">
                            <div class="field_wrapper">
                                <label class="form_label">Beneficiary Name:</label><br>
                                <input type="text" value="<?= $name; ?>" class="form_fields" name="benName" id="benName"
                                       maxlength="30"/>
                            </div>

                            <!--<div class="field_wrapper">
                            <input id="searchBen"  type="submit" value="Search" class="submit_btn">
                            <input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();" value="Create Beneficiary">
                            </div>-->
                    </div>
                    <!-- content area right-->
                    <div class="content_area_right">
                        <div class="field_wrapper margin_min">
                            <label class="form_label">Beneficiary Country:</label><br>
                            <select id="benCountry" class="select_field" name="benCountry">
                                <option value="">-Select Country-</option>
                                <?php $strQueryCountries = "select distinct(c.countryName),s.toCountryId from services as s inner join countries as c on c.countryId = s.toCountryId";
                                $arrCountry = selectMultiRecords($strQueryCountries);
                                for ($j = 0; $j < count($arrCountry); $j++)
                                    echo "<option value='" . $arrCountry[$j]['countryName'] . "'>" . $arrCountry[$j]['countryName'] . "</option>";
                                ?>
                            </select>
                            <!--<div class="field_wrapper">
                            <input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();" value="Create Beneficiary">
                            </div>-->
                        </div>
                    </div>
                    <div class="button_wrapper">
                        <input id="searchBen" type="submit" value="Search" class="submit_btn">
                        <input id="searchAddress2" class="submit_btn" type="button" onClick="createBen();"
                               value="Create Beneficiary">
                    </div>
                    </form>
                </div>
                <!-- content area ends here-->
                <!-- beneficiary content area -->

                <div class="content_area">
                    <div class="table_area">
                        <!-- pagination-->
                        <?php if ($intTotalBens > 0): ?>
                            <table class="table" width="100%">
                                <tr>
                                    <td class="showing_pag">
                                        Showing <?php echo ($offset + 1) . "-" . ($intTotalBens + $offset) . " of " . $intAllBens; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align="right" class="showing_pag">
                                        <?php if ($prev >= 0) { ?>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0">First</a>&nbsp;
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev; ?>">Previous</a>&nbsp;
                                            <?php }
                                            if ($next > 0 && $next < $intAllBens) {
                                            $intLastOffset = (ceil($intAllBens / $limit) - 1) * $limit;
                                            ?>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>">Next</a>&nbsp;
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset; ?>">Last</a>
                                            <?php } ?>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <br>
                                <!-- table head-->
                                <tr>
                                    <th align="left" class="head_td"><strong>Creation Date</strong></th>
                                    <th align="left" class="head_td"><strong>Name</strong></th>
                                    <th align="left" class="head_td"><strong>Country</strong></th>
                                    <th align="left" class="head_td"><strong>Operations</strong></th>
                                    <th class="head_td"></th>
                                </tr>
                                <?php for ($x = 0; $x < $intTotalBens; $x++) { ?>
                                    <!-- table body-->
                                    <tr class="desc_tr">
                                        <td class="desc_td"><?php echo $arrBens[$x]['created']; ?></td>
                                        <td class="desc_td"><?php echo $arrBens[$x]['name']; ?></td>
                                        <td class="desc_td"><?php echo $arrBens[$x]['Country']; ?></td>

                                        <form action="" method="post">
                                            <td class="desc_td">
                                                <a href="javascript:void(0);"
                                                   onClick="document.forms[<?php echo $x + 1; ?>].action='add_beneficiary_new.php';document.forms[<?php echo $x + 1; ?>].submit();">Edit</a>
                                                <input name="ben" type="hidden"
                                                       value="<?php echo $arrBens[$x]['benID']; ?>"/>
                                            </td>
                                            <td class="desc_td">
                                                <a href="javascript:void(0);"
                                                   onClick="document.forms[<?php echo $x + 1; ?>].action='make_payment-ii-new.php';document.forms[<?php echo $x + 1; ?>].submit();">Create
                                                    Transaction</a>
                                        </form>
                                    </tr>
                                <?php } ?>
                            </table>
                        <?php else: ?>
                            <table width="100%">
                                <tr class="desc_tr">
                                    <td class="desc_td" style="text-align: center;">
                                        There are no beneficiaries to display.
                                    </td>
                                </tr>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- footer area -->
            <?php include('footer.php'); ?>
        </div>
    </div>
</body>
<script>
    function createBen() {
        window.location = 'add_beneficiary_new.php';
    }
</script>
</html>
