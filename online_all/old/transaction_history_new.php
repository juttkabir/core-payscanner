<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Transaction History
	 * @author: Mirza Arslan Baig
	 */
require_once '../src/bootstrap.php';
	
	session_start();

	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	$custIDi = $_SESSION['loggedInUser']['userID'];
    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
    $title = $agentUiData['title'];
    $logoHtml = $agentUiData['logoHtml'];
	//checking user type to be logged in
	$userLoggedinType ="";

if ($_SESSION['loggedInUser']['online_customer_services'] == "TC") {
    $userLoggedinType = "AND transType = 'Bank Transfer'";
} else {
    $userLoggedinType = "AND transType = 'Topup'";
}
		
		
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	include_once "includes/functions.php";
	
	if(isset($_GET['limit']) && $_GET['limit'] > 0)
		$limit = intval(trim($_GET['limit']));
	else
		$limit = 5;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	elseif(!empty($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;

	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	
	dbConnect();
	//print_r($_REQUEST);
	$intSearchFlag = false;
	if(!empty($_POST['search']) || !empty($_GET['search'])){
		$intSearchFlag = true;
		
		
		if(!empty($_POST['search'])){
		
			
			
			$fromDate=$_POST['fromDate'];
			sscanf($fromDate, '%d-%d-%d', $d, $m, $Y);
			$strFromDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			debug ($strFromDate);
			
			$strFromDate=$strFromDate." 00:00:00";
			$toDate=$_POST['toDate'];
			
			sscanf($toDate, '%d-%d-%d', $d, $m, $Y);
			$strToDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			$strToDate=$strToDate." 23:59:59";
			debug ($strToDate);
			
			// $strSearchText = trim($_POST['searchText']);
			// $strSearchType = $_POST['searchType'];
			// $strAmountSearchCriteria = $_POST['amountSearch'];
			// $strAmount = trim($_POST['amount']);
			// $strCurrency = $_POST['currency'];
			// $strTransStatus = $_POST['status'];
			// $strApplyOnAmount = $_POST['applyOnAmount'];
			// $strCondition = " ";
		}else{
			//$strFromDate=$_GET['fromDate']." 00:00:00";
			$strFromDate=2008-12-11;
			//$strToDate = $_GET['toDate']." 23:59:59";
			$strToDate = date("Y-m-d")." 23:59:59";
			$strSearchText = trim($_GET['searchText']);
			
			// $strSearchType = $_GET['searchType'];
			// $strAmountSearchCriteria = $_GET['amountSearch'];
			// $strAmount = trim($_GET['amount']);
			// $strCurrency = $_GET['currency'];
			// $strTransStatus = $_GET['status'];
			// $strApplyOnAmount = $_GET['applyOnAmount'];
			// $strCondition = " ";
		}
		// if(!empty($strSearchText) && $strSearchType == 'transRefNumber')
			// $strCondition .= " AND refNumberIM = '$strSearchText'";
		// elseif(!empty($strSearchText) && $strSearchType == 'beneficiaryName')
			// $strCondition .= " AND (ben.firstName LIKE '$strSearchText%' OR ben.middleName LIKE '$strSearchText%' OR ben.lastName LIKE '$strSearchText%' OR ben.beneficiaryName LIKE '$strSearchText%') ";
		
		// if(!empty($strApplyOnAmount)){
			// if($strApplyOnAmount == 'sendingAmount')
				// $strApplyAmount = "transAmount";
			// elseif($strApplyOnAmount == 'receivingAmount')
				// $strApplyAmount = "localAmount";
			// else
				// $strApplyAmount = "transAmount";
		// }
		
		// if($strAmountSearchCriteria != 'all' && !empty($strAmountSearchCriteria)){
			// switch($strAmountSearchCriteria){
				// case ">":
					// $strCondition .= " AND $strApplyAmount > '$strAmount' ";
					// break;
				// case "<":
					// $strCondition .= " AND $strApplyAmount < '$strAmount' ";
					// break;
				// case "=":
					// $strCondition .= " AND $strApplyAmount = '$strAmount'";
					// break;
				// case ">=":
					// $strAmount .= " AND $strApplyAmount >= '$strAmount' ";
					// break;
				// case "<=":
					// $strCondition .= " AND $strApplyAmount <= '$strAmount' ";
					// break;
			// }
		// }
		
		// if(!empty($strCurrency) && $strCurrency != 'all'){
			// $strCondition .= " AND currencyFrom = '$strCurrency' ";
		// }
		
		// if($strTransStatus != 'all' && !empty($strTransStatus))
			// $strCondition .= " AND transStatus = '$strTransStatus' ";
		$strCondition .= " AND (transDate >= '$strFromDate' AND transDate <= '$strToDate')";
		
		
		//$strCondition .= " AND trans_source = 'O'";
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, transType,DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, transType,DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		
		
		$strQueryGroupBy = " group by transDate DESC";
		$strQueryPagination = " LIMIT $offset, $limit";
		//$strQuerySearch .= $strCondition;
		$strQuerySearch .= $strQueryGroupBy;
		$exportQuery = $strQuerySearch;
		$strQuerySearch .= $strQueryPagination;
		$advanced_Search= "search";
		
		
	}else{
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus,transType, DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType  group by transDate DESC";

		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType";
		
		$strQueryPagination = " LIMIT $offset, $limit";
		
		$exportQuery = $strQuerySearch;
		$strQuerySearch .= $strQueryPagination;
		
		$advanced_Search= "";
		
	}
	
		$arrTotalRecords = selectFrom($strQueryCountTotal);
		$intTotalRecords = $arrTotalRecords['records'];
		$arrTrans = selectMultiRecords($strQuerySearch);
		$intTotalTrans = count($arrTrans);
	//print_r($strQuerySearch);
	//echo $userLoggedinType;
	/* debug($intTotalRecords);
	debug($strQuerySearch);
	debug($strQueryCountTotal); */
	
		$strUserId=$_SESSION['loggedInUser']['accountName'];
		$strEmail=$_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title; ?>Transaction History</title>
    <meta name="description"
          content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property."/>
    <meta name="keywords"
          content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading"/>
    <meta http-equiv="Content-Type" content="text/html, charset=utf-8">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
    <!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
    <link href="css/style_new.css" rel="stylesheet" type="text/css">
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/datePicker.css"/>

    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/jquery.datePicker.pi.js"></script>
    <script type="text/javascript" src="javascript/date.js"></script>
    <script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    Date.format = 'dd-mm-yyyy';
    $("#fromDate").datePicker({
        startDate: '01-01-2000',
        clickInput: true,
    });

    $("#toDate").datePicker({
        startDate: '01-01-2000',
        clickInput: true
    });

    $("#printReport").click(function () {
        // Maintain report print logs
        $.ajax({
            url: "maintainReportsLogs.php",
            type: "post",
            data: {
                maintainLogsAjax: "Yes",
                pageUrl: "<?=$_SERVER['PHP_SELF']?>",
                action: "P"
            }
        });

        $("#searchTable").hide();
        $("#pagination").hide();
        $("#actionBtn").hide();
        $(".actionField").hide();
        print();
        $("#searchTable").show();
        $("#pagination").show();
        $("#actionBtn").show();
        $(".actionField").show();
    });

	
	//************ajax call for ref number************//
	
		$("#searchText").keyup(function(){
		var searchType = $("#searchType").val();
		var searchText = $("#searchText").val();
		var custIDi	   = $("#userIDi").val();
		var htmlData='';
		
		$.ajax({
			url: "../admin/registration_form_conf.php",
			type: "post",
			async:false,
			error: function( xhr, status )
			{
            console.log("xhr : "+xhr+" Status: "+status);
			},
			data:{
				action: "showRefNumber",
				searchType 	:	searchType, 
				searchText	:	searchText,
				custIDi 	:	custIDi
			},
			success: function(data){
			htmlData = data ;
			}
		});
		
		$("#showRefNumber").html(htmlData);
		$("#_showRefNumber").show();
			
	});	
	
	});

function selectRefNumber(getid){
    var ids = $("#searchText").val(getid);
    $("#_showRefNumber").hide();
}
</script>
</head>
<body>
<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
    <!-- lower container -->
    <div class="lower_container">
        <div class="body_area">
            <div class="logout_area">
                <h2 class="heading">Transaction History</h2>
                <?php include('top-menu.php');?>
                <!-- content area -->
                <!-- content area -->
                <div class="content_area">
                    <div class="content_area_center">
<p class="content_heading">Search Your Transactions</p>
<p class="content_subheading">All transactions you make through <?= $title; ?> are logged for your reference, you may search your transaction history using the form
below.</p>
</div>
<!-- content area left-->
<div class="content_area_left">
 <form id="filters" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

<div class="field_wrapper">
<label class="form_label">Date from:</label><br>
<input name="fromDate" id="fromDate" type="text" class="form_fields calender_bg" autocomplete="off" value="<?=$fromDate?>" />

</div>

<!--<div class="field_wrapper">
<label class="form_label">Search criteria:</label><br>
<select name="searchType" class="select_field" id="searchType">
<option value="" <?php if($strSearchType ==''){echo 'selected';}?>>- Select Search Criteria -</option>
<option value="transRefNumber" <?php if($strSearchType =='transRefNumber'){echo 'selected';} ?>>Transaction Reference Number</option>
<option value="beneficiaryName" <?php if($strSearchType =='beneficiaryName'){echo 'selected';}?>>Beneficiary Name</option>
</select>
</div>-->

<!--<div class="field_wrapper">
<label class="form_label">Amount criteria:</label><br>
<select id="amountSearch" class="select_field" name="amountSearch">

<option value="" <?php if($strAmountSearchCriteria=='')  {echo 'selected';}?> >- Select Amount Criteria -</option>
<option value=">" <?php if($strAmountSearchCriteria=='>'){echo 'selected';}?>>Greater Than</option>
<option value="<" <?php if($strAmountSearchCriteria=='<'){echo 'selected';}?> >Less Than</option>
<option value="=" <?php if($strAmountSearchCriteria=='='){echo 'selected';}?>>Equal to</option>
<option value=">=" <?php if($strAmountSearchCriteria=='>='){echo 'selected';}?>>Greater than and equal to</option>
<option value="<=" <?php if($strAmountSearchCriteria=='<='){echo 'selected';}?>>Less than and equal to</option>
</select>
</div>-->
<!--<div class="field_wrapper">
<label class="form_label">Apply on:</label><br>
<select name="applyOnAmount" class="select_field" id="applyOnAmount">
<option value="transAmount"   <?php if($strApplyOnAmount =='transAmount'){echo 'selected';}?>>- Select Amount to apply on -</option>
<option value="sendingAmount" <?php if($strApplyOnAmount=='sendingAmount'){echo 'selected';}?>>Sending Amount</option>
<option value="receivingAmount" <?php if($strApplyOnAmount=='receivingAmount'){echo 'selected';}?> >Receving Amount</option>
</select>
</div>-->

<!--<div class="field_wrapper">
<input type='hidden' name='userIDi' id='userIDi' value='<?echo $custIDi ?>' />
<input name="search" id="search" class="submit_btn" type="submit" value="Search">

</div>-->
</div>

<!-- content area right-->
<div class="content_area_right margin_10">
<br>

<div class="field_wrapper">
<label class="form_label">Date to:</label><br>
<input name="toDate" id="toDate" type="text" class="form_fields calender_bg" autocomplete="off" value="<?=$toDate?>" />

</div>

<!--<div class="field_wrapper">
<label class="form_label">Search for:</label><br>

<input name="searchText" value="<?=$strSearchText?>" id="searchText" type="text"  class="form_fields" />
</div>-->

<!--<div id="_showRefNumber"  style="display:none" class="field_wrapper">
<div id="showRefNumber" name="showRefNumber" > 
</div>
</div>-->



<!--<div class="field_wrapper">
<label class="form_label">Amount:</label><br>
<input name="amount" value="<?=$strAmount?>" id="amount" type="text" class="form_fields" />
</div>-->

<!--<div id="_showExpectedAmount"  style="display:none" class="field_wrapper">
<div id="showExpectedAmount" name="showExpectedAmount" > 
	
</div>
</div>-->


<!--<div class="field_wrapper">
<label class="form_label">Currency:</label><br>
<select name="currency" class="select_field" id="currency">
<option value="all">All</option>
<?php 

// $strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_ONLINE_EXCHANGE_RATES." ORDER BY quotedCurrency ASC";
// $arrCurrency = selectMultiRecords($strQueryCurrency);
// for($x = 0;$x < count($arrCurrency);$x++){

// if($strCurrency == ''){
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."'>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// else if(!empty($strCurrency))
// {
// if($strCurrency == $arrCurrency[$x]['quotedCurrency']){
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."' selected>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// else {
// echo "<option value='".$arrCurrency[$x]['quotedCurrency']."'>".$arrCurrency[$x]['quotedCurrency']."</option>";
// }
// }
// }
?>
</select>
</div>-->

</div>
    <div class="button_wrapper">
        <input type='hidden' name='userIDi' id='userIDi' value='<?echo $custIDi ?>' />
        <input name="search" id="search" class="submit_btn" type="submit" value="Search">
    </div>
    </form>
    <div class="position_btn">
        <form action="export_transaction.php?query=<? echo base64_encode(urlencode($exportQuery)); ?>&querycount=<? echo $intTotalRecords; ?>" method="post" style="display:inline-block;">
            <input id="export" class="submit_btn" type="submit" value="Export to Excel" >
        </form>
    </div>
</div>

<!-- table area -->
                    <?php if($intTotalTrans > 0){ ?>
                        <div class="content_area">
                            <div class="table_area">
                                <div>
                                    <table width="100%" border="0">
                                        <tr class="desc_tr">
							<td class="desc_td">Showing <?php echo ($offset+1)." - ".($intTotalTrans + $offset)." of ".$intTotalRecords ?></td>
							<td class="desc_td">
								Records Per Page:
								<select name="limit" id="limit" class="input-small" onChange="javascript:document.location.href='<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&search=search&limit='+this.value;" >
									<option value="5" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '5'){ echo ' selected="selected"';}?>>5</option>
									<option value="10" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '10'){ echo ' selected="selected"';}?>>10</option>
									<option value="20" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '20'){ echo ' selected="selected"';}?>>20</option>
									<option value="30" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '30'){ echo ' selected="selected"';}?>>30</option>
									<option value="50" <?php if(!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '50'){ echo ' selected="selected"';}?>>50</option>
								</select>
							</td>
							<td class="desc_td" >
								<?php 
									if($prev >= 0){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search;?>">First</a>&nbsp;
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev;?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search;?>">Previous</a>&nbsp;
								<?php 
									}
									if($next > 0 && $next < $intTotalRecords){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search;?>">Next</a>&nbsp;
									<?php 
										$intLastOffset = (ceil($intTotalRecords / $limit) - 1) * $limit;
									?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset;?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType;?>&status=<?php echo $strStatus;?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search;?>">Last</a>
								<?php 
									}
								?>
							</td>
						</tr>
					</table>
					<table width="100%" border="1">
						<tr class="desc_tr">
							<th class="desc_td" width="170px;">
								Transaction Reference No.
								
							</th>
							<th class="desc_td">
								Transaction Date
							</th>
							<th class="desc_td">
								Beneficiary Name
							</th>
							<th class="desc_td">
								Sending Amount
							</th>
							<th class="desc_td">
								Sending Currency
							</th>
							<th class="desc_td">
								Receiving Amount
							</th>
							<th class="desc_td">
								Receiving Currency
							</th>
							<th width="50px" class="desc_td">
								Rate
							</th>
						</tr>
						<?php for($x = 0;$x < $intTotalTrans;$x++){ ?>
						<tr class="desc_tr">
							<td class="desc_td" width="80px;">
								<?php echo $arrTrans[$x]['refNumber']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['transDate']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['benName']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['sendingAmount']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['sendingCurrency']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['recievingAmount']; ?>
							</td>
							<td class="desc_td">
								<?php echo $arrTrans[$x]['recievingCurrency']; ?>
							</td>
							<td class="desc_td" width="50px">
								<?php echo $arrTrans[$x]['rate']; ?>
							</td>
							<!--<td width="50px">
								<?php echo $arrTrans[$x]['transStatus']; ?>
							</td>-->
						</tr>
						<?php } ?>
                    </table>
                                </div>
                            </div>
                        </div>
					<?php }elseif(isset($_REQUEST['search'])){ ?>
                        <div class="content_area">
                            <div class="table_area">
                                <div>
                                    <table width="100%">
                                        <tr class="desc_tr">
                                            <td class="desc_td" style="  text-align: center;">There are no transaction(s) to display.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
					<?php } ?>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>
</div>
</body>
</html>
