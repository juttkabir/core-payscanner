<?php
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$address = $agentUiData['address'];
$logoHtml = $agentUiData['logoHtml'];


$strUserId = $_SESSION['loggedInUser']['userID'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Contact us</title>
    <meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
    <!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
    <link href="../css/style_new.css" rel="stylesheet" type="text/css">
    <link href="../css/style_responsive.css" rel="stylesheet" type="text/css">
    <link href="../css/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="../javascript/jquery.validate.js"></script>
    <script type="text/javascript" src="../javascript/date.js"></script>
    <script type="text/javascript" src="../javascript/jquery.datePicker.js"></script>
    <script type="text/javascript" src="../javascript/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="../javascript/ajaxupload.js"></script>
    <script src="../scripts/jquery-1.7.2.min.js"></script>
    <script src="../scripts/jquery.validate.js"></script>
    <script src="../scripts/bootstrap-twipsy.js"></script>
    <script src="../scripts/bootstrap-popover.js"></script>
    <script type="text/javascript" src="../../admin/javascript/jquery.maskedinput.min.js"></script>
</head>
<body>
<!-- main container-->
<div class="container">
    <?php include 'menu.php'; ?>
    <!-- lower container -->
    <div class="lower_container">
        <div class="body_area">
            <div class="logout_area">
                <h2 class="heading">Contact us</h2>
                <?php include 'top-menu.php';?>
                <!-- content area -->
                <!-- content area -->
                <div class="content_area">
                    <div class="content_area_center">
                        <p class="content_heading font_style">Contact Us</p>
                        <p class="content_subheading font_style">If you have any questions about your account or any aspect of the <?= $title; ?> Online system please get in touch and we will assist you.  </p>
                    </div>
                    <!-- content area left-->
                    <div class="content_area_left">
                        <p class="contact_desc font_style" style="font-size:16px;">Talk to us</p>
                        <div class="field_wrapper">
                            <p class="contact_head font_style">Call our Office on</p>
                            <p class="contact_desc font_style"><?= $phone; ?></p>
                        </div>
                        <div class="field_wrapper">
                            <p class="contact_desc font_style">Email Us</p>
                            <p class="contact_head font_style"><?= $email; ?></p>
                        </div>
                    </div>
                    <!-- content area right-->
                    <div class="content_area_right" style="margin:27px;">
                        <p class="contact_desc font_style" style="font-size:16px;">Office addresses</p>
                        <div class="field_wrapper">
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" data-src="holder.js/64x64" alt="55x55" src="../images/uk_flag.png" >
                                    </a>
                                </div>
                                <div class="media-body contact_head">
                                    <h4 class="media-heading contact_desc font_style">Office</h4>
                                    <?= $address; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content area ends here-->
            </div>
            <!-- footer area -->
            <?php include 'footer.php';?>
        </div>
    </div>
</body>
</html>
