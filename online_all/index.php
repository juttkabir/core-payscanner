<?php

use Payex\Model\LoginHistory;
use Payex\Online\Service\CustomerService;
use Payex\Repository\CustomerRepository;
use Payex\Repository\LoginHistoryRepository;

//ini_set("display_errors","On");
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];
$userIp = $online->getLoginHelper()->getUserIpAddr();

/* Checking Get params for distinguish customer type */
$type = $_GET["type"];

if (empty($type)) {
	$type = $_POST["type"];
}

$currentType = base64_decode($type);

if (empty($currentType)) {
	$currentType = 'TC';
}
/* End of checking Get params for distinguish customer type */

$customerNumber = trim($_POST['customerNumber']);
$customerEmail = trim($_POST['customerEmail']);
$password = $_POST['password'];
if (isset($customerNumber) && isset($password)){
    /* Start session logic */
    $lifetime = 60000;
    session_start();
    setcookie(session_name(), session_id(), time() + $lifetime);
    /* End session logic */
    include_once "includes/functions.php";
    $customerRepository = new CustomerRepository($online->getDb());
    $customer = $customerRepository->getByCustomerEmail($customerEmail);
    $errorMessage = '';
    if ($customer){
        $customerService = new CustomerService($online->getDb());
        if ($customerService->loginCustomer($customer, $customerEmail, $password)){
            if ($customerService->checkCustomerIdTypeExpireDate($customer)){
                if ($agentUiData['agentId'] && $customer->getAgentId() == $agentUiData['agentId']){
                    $checkType = $customerService->checkCustomerServiceType($customer, $currentType);

//                    $_SESSION['loggedInUser']['userID'] = $customer->getId();
//                    $_SESSION['loggedInUser']['agentID'] = $customer->getAgentId();
//                    $_SESSION['loggedInUser']['email'] = $customer->getEmail();
//                    $_SESSION['loggedInUser']['accountName'] = $customer->getAccountName();
//                    $_SESSION['loggedInUser']['online_customer_services'] = $customer->getOnlineCustomerServices();
//                    $_SESSION['loggedInUser']['card_issue_currency'] = $customer->getCardIssueCurrency();
//
//                    $loginHistory = new LoginHistory();
//                    $loginHistory->setLoginTime(getCountryTime(CONFIG_COUNTRY_CODE));
//                    $loginHistory->setLoginName($customer->getFirstName());
//                    $loginHistory->setAccessIp($userIp);
//                    $loginHistory->setLoginType('customer');
//                    $loginHistory->setCustId($customer->getId());
//                    $loginHistoryRepository = new LoginHistoryRepository($online->getDb());
//                    $insertId = $loginHistoryRepository->save($loginHistory);
//
//                    session_register("loginHistoryID");
//                    $_SESSION["loginHistoryID"] = $insertId;

                    if (isset($checkType['redirectPage']) && !isset($checkType['error'])){
                        $_SESSION['loggedInUser']['agentID'] = $customer->getAgentId();
                        ?>
                        <form id="verifyCustomerForm" method="post" action="<?=$checkType['redirectPage']?>" style="display: none">
                            <input type="hidden" name="customerEmail" value="<?=$customerEmail?>" />
                            <input type="hidden" name="password" value="<?=$password?>" />
                            <input type="hidden" name="from" value="<?=$checkType['from']?>" />
                            <input type="hidden" name="type" value="<?=$checkType['type']?>" />
                            <input type="hidden" name="customerID" value="<?=$checkType['customerID']?>" />
                        </form>
                        <script type="text/javascript">
                            document.getElementById("verifyCustomerForm").submit();
                        </script>
                        <?php
//                        header("LOCATION: " . $checkType['redirectPage']);
                    }else{
                        $errorMessage = $checkType['error'];
                    }
                }else{
                    $errorMessage = 'Some of data used is invalid.';
                }
            }else{
                $errorMessage = 'Your Passport expiry is less than to date. Please contact administrator.';
            }
        }else{
            $errorMessage = 'Customer email or password is not valid or customer is not active';
        }
    }else{
        $errorMessage = 'Customer email or password is not valid or customer is not active';
    }
}

if (isset($_GET['logout']) && !empty($_GET['logout'])) {
	$strMsg = "You have logged out!!";
}

?>

<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Private Client Login <?php echo $title; ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include '_ga.inc.php';?>

</head>
<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="login-section" class="fullheight">
    <div class="vcenter-container">
        <div class="vcenter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-3 col-lg-4 col-lg-push-4">
                        <h1 class="logo text-center">
                            <?= $logoHtml; ?>
                        </h1>
                        <div class="form-wrapper">
                            <h2 class="h4 title-underblock custom mb30">Login to <?= $title; ?> Online</h2>
                            <?php if ($errorMessage):?>
                                <div class="alert alert-danger error_msg">
                                    <?php echo $errorMessage; ?>
                                </div>
                            <?php endif; ?>
                            <form method="post" action="<?= $online->getBaseUrl() . 'index.php'; ?>">
                                <div class="form-group">
                                    <label for="customerEmail" class="input-desc">Customer Email</label>
                                    <input type="text" class="form-control" id="customerEmail" name="customerEmail" placeholder="Customer Email" required>
                                </div><!-- End .from-group -->
                                <div class="form-group mb10">
                                    <label for="password" class="input-desc">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" maxlength="32" required>
                                </div><!-- End .from-group -->
                                <div class="form-group text-right clear-margin helper-group">
                                    <a href="<?php echo $online->getBaseUrl() . 'forgot_passwordnew.php'; ?>" class="add-tooltip" data-placement="top" title="Recover your password">Forget your password?</a>
                                </div><!-- End .form-group -->
                                <div class="form-group mt15-r">
                                    <a href="<?php echo $online->getBaseUrl() . 'registration_form.php'; ?>" class="add-tooltip" data-placement="top" title="Register as private client">Not registered yet</a>
                                </div><!-- End .form-group -->
                                <div class="form-group mb5">
                                    <input type="submit" name="submit" class="btn btn-custom" value="Login">
                                    <input type="hidden" name="type" id="type" value="<?=$type?>" />
                                </div><!-- End .from-group -->
                            </form>
                        </div><!-- End .form-wrapper -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .vcenter -->
    </div><!-- End .vcenter-container -->
</div><!-- End .fullheight -->


<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->

<script src="<?= $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>

</body>

</html>
