<?php
/**
 * @package: Online Module
 * @subpackage: Add Beneficiary
 * @author: Mirza Arslan Baig
 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php?notLogin=1");
}
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();

include_once "includes/functions.php";
include_once "javascript/audit-functions.php";
$benCountries = "select countryId,countryName,isocode,ibanlength from countries";
//debug($benCountries);
$benCountriesResult = selectMultiRecords($benCountries);
//debug($benCountriesResult);

if (!empty($_POST['addBen']) || !empty($_POST['updateBen'])) {

    $patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
    $strIBAN = mysql_real_escape_string(trim($_POST['iban']));
    $strBenName = mysql_real_escape_string(trim($_POST['benName']));

    $strBenCountry = mysql_real_escape_string($_POST['benCountry']);
    $strBenCountryId = $strBenCountry;
    $strBenCurrency = mysql_real_escape_string(trim($_POST['benCurrency']));
    $strSwiftCode = mysql_real_escape_string(trim($_POST['swift']));
    $strSortCode = mysql_real_escape_string(trim($_POST['sortCode']));
    $strReference = mysql_real_escape_string(trim($_POST['reference']));
    $strAccountName = mysql_real_escape_string(trim($_POST['accountName']));
    $strRoutingNumber = mysql_real_escape_string(trim($_POST['routingNumber']));
    $strAccountNumber = mysql_real_escape_string(trim($_POST['accountNumber']));
    $strSendingCurrency = mysql_real_escape_string(trim($_POST['sendingCurrency']));
    $strBranchNameNumber = mysql_real_escape_string(trim($_POST['branchNameNumber']));
    $strBranchAddress = mysql_real_escape_string(trim($_POST['branchAddress']));
    $strBranchAddress = cleanString($strBranchAddress);
    $intCustomerID = $_SESSION['loggedInUser']['userID'];
    if (!empty($_POST['benID'])) {
        $intBenID = mysql_real_escape_string(trim($_POST['benID']));
    }
    $strCurrentDate = date("Y-m-d");

    if (!empty($intBenID)) {
        $benCountry = selectFrom("select countryName from countries where countryId = '" . $strBenCountry . "' ");
        $strBenCountry = $benCountry["countryName"];

        $strQueryUpdateBen = "UPDATE " . TBL_BENEFICIARY . " SET `firstName` = '$strBenName', `beneficiaryName` = '$strBenName', `Country` = '$strBenCountry', `reference` = '$strReference' WHERE benID = '$intBenID'";
        $getPreviousArr = "SELECT * FROM ben_banks WHERE benID = '$intBenID'";
        $fetPreviousArr = selectFrom($getPreviousArr);

        $getPreviouBensArr = "SELECT * FROM beneficiary WHERE benID = '$intBenID'";
        $fetPreviousBenArr = selectFrom($getPreviouBensArr);
        $benName = $fetPreviousBenArr["firstName"] . " " . $fetPreviousBenArr["middleName"] . " " . $fetPreviousBenArr["lastName"];

        if (update($strQueryUpdateBen)) {
            $strMsg = "Beneficiary " . $benName . "updated successfully";

            activities($_SESSION["loginHistoryID"], "UPDATED", $intBenID, "beneficiary", "beneficiary updated successfully");

            logChangeSet($fetPreviousArr, $_SESSION["loggedInUser"]["accountName"], "ben_banks", "benId", $intBenID, $_SESSION["loggedInUser"]["accountName"]);

            logChangeSet($fetPreviousBenArr, $_SESSION["loggedInUser"]["accountName"], "beneficiary", "benId", $intBenID, $_SESSION["loggedInUser"]["accountName"]);

        } else {
            $strError = "Beneficiary cannot be updated";
        }
    } else {
        $benCountry = selectFrom("select countryName from countries where countryId = '" . $strBenCountry . "' ");
        $strBenCountry = $benCountry["countryName"];
        $strQueryInsertBen = "INSERT INTO " . TBL_BENEFICIARY . " (`firstName`, `middleName`, `lastName`, `beneficiaryName`, `Country`, `reference`, `customerID`, `created`) VALUES('$strBenName', '', '', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID', '$strCurrentDate')";
        if (insertInto($strQueryInsertBen)) {
            $BenID = mysql_insert_id();

            $getPreviouBensArr = "SELECT * FROM beneficiary WHERE benID = '$BenID'";
            $fetPreviousBenArr = selectFrom($getPreviouBensArr);

            $benName = $fetPreviousBenArr["firstName"] . " " . $fetPreviousBenArr["middleName"] . " " . $fetPreviousBenArr["lastName"];

            activities($_SESSION["loginHistoryID"], "INSERTION", $BenID, "beneficiary", "beneficiary " . $benName . " added successfully");
            $strMsg = "Beneficiary " . $benName . " added successfully";
            //$intBenID = mysql_insert_id();
        } else {
            $strError = "Beneficiary cannot be added";
        }
    }
    if ($BenID != "") {
        $intBenID = $BenID;
    }

    if (!empty($intBenID)) {
        $strQueryChkBankInfo = "SELECT count(id) AS record FROM " . TBL_BEN_BANK_DETAILS . " WHERE benID = '$intBenID'";
        debug($strQueryChkBankInfo);
        $arrBenBankInfo = selectFrom($strQueryChkBankInfo);

        if ($arrBenBankInfo['record'] == 0) {

            $strQueryInsertBenBank = "INSERT INTO " . TBL_BEN_BANK_DETAILS . "
					(	benId,
						IBAN,
						swiftCode,
						sortCode,
						bankName,
						accountNo,
						routingNumber,
						branchCode,
						branchAddress,
						sendCurrency
					)
					VALUES
					(	'$intBenID',
						'$strIBAN',
						'$strSwiftCode',
						'$strSortCode',
						'$strBenName',
						'$strAccountNumber',
						'$strRoutingNumber',
						'$strBranchNameNumber',
						'$strBranchAddress',
						'$strSendingCurrency'
					)";

            if (!insertInto($strQueryInsertBenBank)) {
                $strError .= " and beneficiary bank info cannot added.";
            } else {
                activities($_SESSION["loginHistoryID"], "INSERTION", $_SESSION["loggedInUser"]["accountName"], "bankDetails", "bankDetails added successfully");
            }
        } else {

            $strQueryUpdateBenBank = "UPDATE " . TBL_BEN_BANK_DETAILS . " SET";
            if (!empty($strIBAN)) {
                $strQueryUpdateBenBank .= "`IBAN` = '$strIBAN',";
            }
            if (!empty($strSwiftCode)) {
                $strQueryUpdateBenBank .= "`swiftCode` = '$strSwiftCode',";
            }
            /*
            if(!empty($strBenAccountName)){
            $strQueryUpdateBenBank .="`bankName` = '$strBenAccountName',";
            }
             */
            if (!empty($strBenName)) {
                $strQueryUpdateBenBank .= "`bankName` = '$strBenName',";
            }
            //    if(!empty($strAccountNumber)){
            $strQueryUpdateBenBank .= "`accountNo` = '$strAccountNumber',";
            //}
            if (!empty($strBranchNameNumber)) {
                $strQueryUpdateBenBank .= "`branchCode` = '$strBranchNameNumber',";
            }
            if (!empty($strBranchAddress)) {
                $strQueryUpdateBenBank .= "`branchAddress` = '$strBranchAddress',";
            }
            $strQueryUpdateBenBank .= "`sortCode` = '$strSortCode',";
            if (!empty($strRoutingNumber)) {
                $strQueryUpdateBenBank .= "`routingNumber` = '$strRoutingNumber',";
            }
            if (!empty($strSendingCurrency)) {
                $strQueryUpdateBenBank .= "`sendCurrency` = '$strSendingCurrency',";
            }
            $strQueryUpdateBenBank = substr($strQueryUpdateBenBank, 0, strlen($strQueryUpdateBenBank) - 1);
            $strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";

            if (update($strQueryUpdateBenBank)) {
                $strMsg .= " and beneficiary bank info is updated";

                activities($_SESSION["loginHistoryID"], "UPDATED", $_SESSION["loggedInUser"]["accountName"], "bankDetails", "ben_banks added successfully");

                $getPreviousArr2 = "SELECT * FROM ben_banks WHERE benID = '$intBenID'";
                $fetPreviousArr2 = selectFrom($getPreviousArr2);

                logChangeSet($fetPreviousArr, $_SESSION["loggedInUser"]["accountName"], "ben_banks", "benId", $intBenID, $_SESSION["loggedInUser"]["accountName"]);
            } else {
                $strError .= " and beneficiary bank info cannot be updated";
            }

        }
    }

}
//debug($intBenID);
if (!empty($_POST['ben'])) {
    $intBenID = trim($_POST['ben']);
    $strQueryBen = "SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName,bd.routingNumber, bd.accountNo,bd.branchName,bd.branchAddress,bd.sendCurrency,bd. 	branchCode FROM " . TBL_BENEFICIARY . " AS ben LEFT JOIN " . TBL_BEN_BANK_DETAILS . " AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName WHERE ben.customerID = '{$_SESSION['loggedInUser']['userID']}' AND ben.status = 'Enabled' AND ben.benID = '$intBenID'";
    $arrBen = selectFrom($strQueryBen);

    $strIBAN = $arrBen['IBAN'];
    $strBenName = $arrBen['benName'];
    $strSendingCurrency = $arrBen['sendCurrency'];
    $strBenCountry = $arrBen['Country'];
    $strBenCountryId = $arrBen['countryId'];
    $strSwiftCode = $arrBen['swiftCode'];
    $strSortCode = $arrBen['sortCode'];
    $strReference = $arrBen['reference'];
    $intCustomerID = $arrBen['customerID'];
    $strAccountName = $arrBen['bankName'];
    $strRoutingNumber = $arrBen['routingNumber'];
    $strAccountNumber = $arrBen['accountNo'];
    $strBranchNameNumber = $arrBen['branchCode'];
    $strBranchAddress = $arrBen['branchAddress'];

}
$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>



<?php 
	/**
	 * @package: Online Module
	 * @Ticket : 12671
	 * @author : Imran Naqvi
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}

    require_once '../src/bootstrap.php';
    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
    $title = $agentUiData['title'];
    $logoHtml = $agentUiData['logoHtml'];

	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";

?><!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title><?php if(!empty($intBenID)) echo "Edit"; else echo "Add"; ?> Beneficiary -  Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>

</head>

<body>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'sendmoney';?>
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?=$online->getBaseUrl();?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Add beneficiary</h1>
                        <p class="page-header-desc">Add new Beneficiary</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Add beneficiary</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">


            <div class="row">
                <div class="col-sm-8">

                        <?php
                        if (isset($strError) && !empty($strError)) {
                        ?>
                        <div class="alert alert-danger error_msg">
                            <?php echo $strError; ?>
                        </div>

                        <?php
                        }
                        if (isset($strMsg) && !empty($strMsg)) {
                        ?>
                        <div class="alert alert-success error_msg">
                            <?php echo $strMsg; ?>
                        </div>

                        <?php
                        }
                        ?>

                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Add beneficiary</h2>
                        <p>Add beneficiary details below:</p>
                        <form id="addBenForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" autocomplete="off">

                            <div class="row">
                                <!-- col 1 -->
                                <div class="col-sm-6 content_area_left">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="benCountry" class="input-desc">Beneficiary Country:</label>
                                                <select name="benCountry" id="benCountry" class="form-control select_field">
                                                    <option value="">- Select country for beneficiary -</option>
                                                    <?php
                                                        /*
                                                        $strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
                                                        */

                                                        /**
                                                        Below mention Query added on recomandation of Wajiha
                                                        */
                                                        $strQueryCountries = "SELECT distinct(countries.countryName),countryId
                                                                FROM countries
                                                                INNER JOIN services
                                                                ON countries.countryId=services.toCountryId
                                                                ORDER BY countries.countryName";

                                                        $arrCountry = selectMultiRecords($strQueryCountries);

                                                        for ($x = 0; $x < count($arrCountry); $x++) {

                                                            echo "<option value='" . $arrCountry[$x]['countryId'] . "'>" . $arrCountry[$x]['countryName'] . "</option>";
                                                        }

                                                        /**
                                                        $strQueryCountries = "SELECT DISTINCT countryName AS countryName, countryCode, countryId FROM ".TBL_COUNTRIES." WHERE countryCode != '' GROUP BY countryName ORDER BY countryName ASC";
                                                        $arrCountry = selectMultiRecords($strQueryCountries);
                                                        for($x = 0;$x < count($arrCountry);$x++){
                                                        echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']." - ".$arrCountry[$x]['countryCode']."</option>";
                                                        }
                                                        */
                                                        ?>
                                                </select>
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb10">
                                                <label for="sendingCurrency" class="input-desc">Currency You Wish To Send:</label>
                                                <select name="sendingCurrency" id="sendingCurrency" type="text" class="form-control select_field">
                                                    <option value="">- Select Currency -</option>
                                                    <?php
                                                    $strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM   (SELECT quotedCurrency AS currencies FROM " . TBL_FX_RATES_MARGIN . " UNION ALL SELECT baseCurrency AS currencies FROM " . TBL_FX_RATES_MARGIN . ") AS tg GROUP BY currencies;";

                                                    $arrCurrency = selectMultiRecords($strQueryCurrency);

                                                    for ($x = 0; $x < count($arrCurrency); $x++) {
                                                        echo "<option value='" . $arrCurrency[$x]['currencies'] . "'>" . $arrCurrency[$x]['currencies'] . "</option>";
                                                    }

                                                    ?>
                                                </select>
                                            </div><!-- End .from-group -->

                                            <div class="form-group" id="accountNameCont">
                                                <label for="benName" class="input-desc">Beneficiary Account Name:</label>
                                                <input name="benName" id="benName" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="accountNumberCont">
                                                <label for="accountNumber" class="input-desc">Account Number:</label>
                                                <input name="accountNumber" id="accountNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div class="form-group" id="branchNameNumberCont">
                                                <label for="branchNameNumber" class="input-desc">Branch Name / Number:</label>
                                                <input name="branchNameNumber" id="branchNameNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="branchAddressCont">
                                                <label for="accountNumber" class="input-desc">Branch Address:</label>
                                                <input name="accountNumber" id="accountNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->



                                        </div>

                                    </div>
                                </div>

                                <!-- col 2 -->
                                <div class="col-sm-6 content_area_right">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group" id="routingNumberCont">
                                                <label for="routingNumber" class="input-desc">Routing Number:</label>
                                                <input name="routingNumber" id="routingNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb10" id="sortCodeCont">
                                                <label for="sortCode" class="input-desc">Sort Code:</label>
                                                <input name="sortCode" id="sortCode" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div id="chackboxnew" style="display:none;" class="field_wrapper">
                                                <input id="chackbox_new" name="chackbox_new" type="checkbox" value="chackbox_new"
                                                    onclick="CheckValidation(this)" class="bottom" />
                                                <span class="customer_rn_vl" style="font:size:11px;">Please mark the check box if u
                                                    want to add IBAN instead of SortCode and Account No.</span>
                                            </div>

                                            <div class="form-group" id="ibanCont">
                                                <label for="iban" class="input-desc">IBAN Number:</label>
                                                <input name="iban" id="iban" type="text" maxlength="40" onBlur="checkAgree();" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <input name="isiban" id="isiban" type="hidden" />
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="swiftCont">
                                                <label for="accountNumber" class="input-desc">SWIFT Code:</label>
                                                <input name="accountNumber" id="accountNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->

                                            <div class="form-group">
                                                <label for="reference" class="input-desc">Your Reference:</label>
                                                <input name="reference" id="reference" type="text" maxlength="15" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                            </div><!-- End .from-group -->



                                        </div>

                                    </div>
                                </div>


                                
                                <div class="col-sm-12">
                                    <div class="form-group mb5">

                                        <?php
                                            if (empty($intBenID)) {
                                        ?>
                                        <!--<input name="addBen" id="addBen" type="submit" value="Add" class="submit_btn" onclick="return checkValidations();"/>-->
                                        <input name="addBen" id="addBen" type="submit" value="Add" class="btn btn-custom mr10 submit_btn" onclick="return CheckValidation();" />

                                        <?php
                                            } else {
                                        ?>
                                        <input name="updateBen" id="updateBen" type="submit" value="Update" class="btn btn-custom mr10 submit_btn"
                                            onclick="return checkValidations();" />
                                        <input name="benID" type="hidden" value="<?php echo $intBenID; ?>">
                                        <input name="sendCurrencyVal" type="hidden" value="">
                                        <?php
                                            }
                                        ?>
                                        <input name="clear" id="clear" type="reset" value="Clear Details" class="btn btn-default clear_btn">

                                    </div><!-- End .from-group -->
                                </div>

                            </div>


                        </form>
                    </div><!-- End .form-wrapper -->

                </div><!-- End .col-sm-6 -->

                <div class="mb40 visible-xs"></div><!-- space -->

                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?=$title;?></h2>

                    <p>Send money safely and securely with <?=$title;?> Online.</p>

                    <div class="mb10"></div><!-- space -->

                    <a href="<?=$online->getBaseUrl();?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->

            </div><!-- End .container -->

            <div class="mb40 mb20-xs"></div><!-- space -->
            <footer id="footer" class="footer-inverse" role="contentinfo">
                <?php include 'templates/footer.php';?>
            </footer>
            <!-- End #footer -->

        </div>
        <!-- End #content -->
    </div>
    <!-- End #wrapper -->

    <a href="#top" id="scroll-top" title="Back to Top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- END -->
    <script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>
</html>
