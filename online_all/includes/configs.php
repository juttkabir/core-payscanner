<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

include './includes/functions.php';

define("DEBUG", false);
define("DEBUG_ON", false);
// text PFX online title;
define("CLIENT_NAME_ONLINE", "Payscanner Online");
//die("Website being update");
/*
$Ip=$_SERVER['REMOTE_ADDR'];
if($Ip !='210.2.146.50')
{
die("Website being update");
die("A release in process");
die("PROCESSING");

}
 */

// Database credentials
define("SERVER_MASTER", "localhost");
define("USER", "dev_payscanner");
define("PASSWORD", "");
define("DATABASE", "dev_payscanner");

/********************
 * This variable is used to enter the
 *	current date & time of specific country in database
 */
define("CONFIG_COUNTRY_CODE", "UK");
// Database Tables
define("TBL_BENEFICIARY", DATABASE . ".beneficiary");
define("TBL_LOGIN_ACTIVITIES", "login_activities");
define("TBL_AUDIT_MODIFY_HISTORY", DATABASE . ".audit_modify_history");
define("TBL_CUSTOMER", DATABASE . ".customer");
define("TBL_TRANSACTIONS", DATABASE . ".transactions");
define("TBL_COUNTRIES", DATABASE . ".countries");
define("TBL_CURRENCY", DATABASE . ".currencies");
define("TBL_ONLINE_EXCHANGE_RATE", DATABASE . ".online_exchange_rates");
define("TBL_AGENT_COMPANY_ACCOUNT", DATABASE . ".agents_company_account");
define("TBL_BEN_BANK_DETAILS", DATABASE . ".ben_banks");
define("TBL_ONLINE_EXCHANGE_RATES", DATABASE . ".online_exchange_rates");
// define("TBL_TRANSACTIONS", DATABASE . ".transactions");
define("TBL_ADMIN_USERS", DATABASE . ".admin");
define("TBL_BANK_DETAILS", DATABASE . ".bankDetails");
define("TBL_SESSIONS", DATABASE . ".sessions");
define("TBL_FX_RATES_MARGIN", DATABASE . ".fx_rates_margin");
// Rules for Bank Details Country Based
define("TBL_BEN_BANK_DETAILS_RULE", DATABASE . ".ben_country_rule_bank_details");
define("TBL_COLLECTION", DATABASE . ".cm_collection_point");
/*** Maturity date (Guzatted Holidays) Ticket # 10838****/
define("TBL_GUZATTED_HOLIDAY", DATABASE . ".guzatted_holidays");
// Ref Number
define("SYSTEM_PRE", "PE");
define("CONFIG_ONLINE_AGENT", "100474");
define("TBL_LOGIN_HISTORY", DATABASE . ".login_history");
/**Added by Umair ticket 11956**/
define("TBL_CUST_CREDIT_CARD", DATABASE . ".cm_cust_credit_card");
//LIVE DEBIT CARD URL
define("CONFIG_URL_DEBIT_CARD", "https://payments.epdq.co.uk/ncol/prod/orderdirect.asp");

/* define("CONFIG_INTERCASH_WSDL","https://wdcs.prepaidgate.com/wswc/Service1.asmx?wsdl");
define("CONFIG_INTERCASH_WSDL_USERNAME","9R3MFx");
define("CONFIG_INTERCASH_WSDL_PASS","pre^^7878ierk"); */

//Live information
define("CONFIG_INTERCASH_WSDL", "https://wdcs.prepaidgate.com/wswc/Service1.asmx?wsdl");

//define("CONFIG_INTERCASH_WSDL","includes/Service1.asmx?wsdl");
define("CONFIG_INTERCASH_USERNAME", "9R3MFx");
define("CONFIG_INTERCASH_PASSWORD", "pre^^7878ierk");

define("TBL_MULTIPLE_USER_ID_TYPES", DATABASE . ".user_id_types");

define('CONFIG_ID_ID_TYPES', '1');

define("CONFIG_CUST_COUNTRIES_LIST", getCountries(SERVER_MASTER, USER, PASSWORD, DATABASE));

?>
