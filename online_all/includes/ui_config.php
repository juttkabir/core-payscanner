<?php
define('CLIENT_TITLE', 'Payscanner');

//Logo configuration
define('LOGO_IMAGE', 'images/payscanner/payscanner_green_logo.png');
define('LOGO_URL', 'http://payscanner.com');
define('LOGO_WIDTH', 170);
define('LOGO_HEIGHT', 60);

define('LINKEDIN_URL', 'https://www.linkedin.com/in/pay-scanner-8a9772161/');
define('FACEBOOK_URL', 'https://www.facebook.com');
