function validateFields (customerID) {
    var senderRegistrationForm = $('#senderRegistrationForm');

    var mobile = $('#mobile');

    var mobileValidator = $('#mobileValidator');

    if (mobile.val().length === 0 || mobile.val().length === 8 || mobile.val().length === 9 || mobile.val().length === 10) {
        senderRegistrationForm.submit();
        return true;
    } else {
        mobileValidator.html("<small>Mobile number is invalid</small>").css('display', 'inline');
        mobile.focus();
        return false;
    }
}