$(function(){
    $("#infoMessages").hide();
    $("#topupForm").validate({
        rules:{
            sendingAmount : {
                required: true,
                number: true
            },
            receivingCurrency: {required: true},
            receivingAmount: {required: true, number: true}
        },
        messages:{
            sendingAmount: {
                required:'Plz provide amount to send',
                number: 'Only integers allowed',
            }

        }
    });
});

$(function () {
    $("#topup").click(function (event) {
        $.ajax({
            url: './ajaxCalls.php',
            method: 'POST',
            async: false,
            data: {
                rtypeMode: "dailyTransLimit",
                customerID: $("#customerID").val()
            },
            success: function (data) {
                //alert(data);
                if (data == 2 || data > 2) {
                    alert('You have reached your daily transaction limit .');
                    //$("#topup").attr("disabled","disabled");
                    event.preventDefault();
                    return false;
                }
            }
        });

    });


    $("#topupForm").validate({

        rules:
            {
                receivingCurrency: {required: true},
                sendingAmount: {required: true, number: true},
                receivingAmount: {required: true, number: true}
            }
    });


    $('#sendingAmount').on('blur', function () {
        $("#infoMessages").hide();
        getRate();
    });

    $('#receivingCurrency').on('change', function () {
        checkvalues();
    });

    $('#receivingAmount').on('blur', function () {
        getinverserate();
    });

    function checkvalues() {
        receivingAmount = $("#receivingAmount").val();
        sendingAmount = $("#sendingAmount").val();

        if (receivingAmount != "" && sendingAmount != "") {
            $("#receivingAmount").val("");
            $("#sendingAmount").val("");
            $("#xRate").text("");
            //alert("Enter value.");
        }

    }

    function getRate() {

        receivingCurrency = $("#receivingCurrency").val();
        sendingCurrency = $("#sendingCurrency").val();
        receivingAmount = $("#receivingAmount").val();
        sendingAmount = $("#sendingAmount").val();

        // if(receivingCurrency ==  '' ){
        // alert('Please select receiving currency');
        // return false;
        // }

        $.ajax({
            url: "./ajaxCalls.php",
            data:
                {
                    rtypeMode: "getExchangeRate",
                    receivingCurrency: receivingCurrency,
                    sendingCurrency: sendingCurrency,
                    receivingAmount: receivingAmount,
                    sendingAmount: sendingAmount,
                },
            method: "POST",
            success: function (data) {

                dataSplit = data.split("|");

                receivingAmount = $.trim(dataSplit[0]);

                xRate = $.trim(dataSplit[1]);

                preciseAmount = receivingAmount.replace(/\,/g, '');

                //preciseAmount=parseInt(preciseAmount,10);

                checkLimits = validateAmountLimit(receivingCurrency, preciseAmount);

                if (checkLimits == false) {
                    return false;
                }

                if (isNaN(preciseAmount) == false) {

                    $(".content_area_right").css("display", "none");
                    if (receivingCurrency != sendingCurrency) {
                        $("#receivingAmount").val(preciseAmount);


                        //$("#xRate").html('1 '+ sendingCurrency + ' = '+ xRate +' '+ receivingCurrency);

                        $("#xRate").text('Rate = ' + xRate);

                    }
                } else {
                    if (receivingCurrency != '') {
                        $("#receivingAmount").val("");
                        $("#infoMessages").css("display", "block");
                        $("#infoMessages").html(receivingAmount);
                    }
                }

            }
        });
    }

    function getinverserate() {

        receivingCurrency = $("#receivingCurrency").val();
        sendingCurrency = $("#sendingCurrency").val();
        receivingAmount = $("#receivingAmount").val();
        sendingAmount = $("#sendingAmount").val();

        // if(receivingCurrency ==  '' ){
        // alert('Please select receiving currency');
        // return false;
        // }

        $.ajax({
            url: "./ajaxCalls.php",
            data:
                {
                    rtypeMode: "getExchangeRate",
                    receivingCurrency: receivingCurrency,
                    sendingCurrency: sendingCurrency,
                    receivingAmount: receivingAmount,
                    sendingAmount: sendingAmount,
                },
            method: "POST",
            success: function (data) {

                dataSplit = data.split("|");

                //receivingAmount = $.trim(dataSplit[0]);

                xRate = $.trim(dataSplit[1]);

                inverseRate = $.trim(dataSplit[2]);
                if (receivingAmount != "") {
                    sendingAmount = receivingAmount / xRate;
                    sendingAmount = sendingAmount.toFixed(2);
                }
                //alert(receivingAmount);
                preciseAmount = receivingAmount.replace(/\,/g, '');

                //preciseAmount=parseInt(preciseAmount,10);

                checkLimits = validateAmountLimit(receivingCurrency, preciseAmount);

                if (checkLimits == false) {
                    return false;
                }

                if (isNaN(preciseAmount) == false && preciseAmount != 0) {

                    $(".content_area_right").css("display", "none");
                    if (receivingCurrency != sendingCurrency) {
                        $("#sendingAmount").val(sendingAmount);
                        //$("#xRate").html('1 '+ sendingCurrency + ' = '+ xRate +' '+ receivingCurrency);
                        $("#xRate").text('Rate = ' + xRate);
                    }
                } else {
                    if (receivingCurrency != '') {
                        $("#sendingAmount").val("");
                        $(".content_area_right").css("display", "block");
                        $("#infoMessages").html(receivingAmount);
                    }
                }
            }
        });
    }


    function validateAmountLimit(receivingCurrency, receivingAmount) {
        var returnAmount = true;
        returnAmount = amountLimitPerTransaction(receivingCurrency, receivingAmount);

        if (returnAmount != false) {

            amountLimitPerMonth(receivingCurrency, function (receivingAmount) {
                returnAmount = receivingAmount;
            });

        }

        return returnAmount;
    }

    function amountLimitPerTransaction(receivingCurrency, receivingAmount) {

        if (receivingCurrency == 'EUR') {
            if (receivingAmount < 10 && receivingAmount != 0) {
                alert('you can not send money less than 10');
                $('#receivingAmount').val('');
                return false;
            } else if (receivingAmount > 5000) {
                alert('You can not send money more than 5,000');
                $('#receivingAmount').val('');
                return false;
            }
        } else if (receivingCurrency == 'USD' && receivingAmount != 0) {


            if (receivingAmount < 10) {
                alert('you can not send money less than 10');
                $('#receivingAmount').val('');
                return false;
            } else if (receivingAmount > 6000) {
                alert('You can not send money more than 6,000');
                $('#receivingAmount').val('');
                return false;
            }
        } else if (receivingCurrency == 'GBP' && receivingAmount != 0) {
            if (receivingAmount < 10) {
                alert('you can not send money less than 10');
                $('#receivingAmount').val('');
                return false;
            } else if (receivingAmount > 4000) {
                alert('You can not send money more than 4,000');
                $('#receivingAmount').val('');
                return false;
            }
        }
        return true;
    }


    function amountLimitPerMonth(receivingCurrency, receivingAmount) {
        var returnValue = true;

        $.ajax({
            url: './ajaxCalls.php',
            data: {
                rtypeMode: 'amountLimitPerMonth',
                customerID: $("#customerID").val()
            },
            async: false,
            success: function (data) {

                amount = parseFloat($.trim(data)) + parseFloat($("#sendingAmount").val());

                if (receivingCurrency == 'GBP') {
                    if ($.trim(amount) > 20000) {
                        alert('Monthy limit 20,000 cross for sending amount');
                        $('#receivingAmount').val('');
                        returnValue = false;
                    }
                } else if (receivingCurrency == 'EUR') {
                    if ($.trim(amount) > 20000) {
                        alert('Monthy limit 20,000 cross for sending amount');
                        $('#receivingAmount').val('');
                        returnValue = false;
                    }
                } else if (receivingCurrency == 'USD') {
                    if ($.trim(amount) > 20000) {
                        alert('Monthy limit 20,000 cross for sending amount');
                        $('#receivingAmount').val('');
                        returnValue = false;
                    }
                }
                receivingAmount(returnValue);
            }

        });

    }

});