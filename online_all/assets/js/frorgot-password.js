function checkDob(){
    if($('#day').val() == ''){
        alert('Enter the day of your date of birth');
        $(this).focus();
        return false;
    }
    if($('#month').val() == ''){
        alert('Enter the month of your date of birth');
        $(this).focus();
        return false;
    }
    if($('#year').val() == ''){
        alert('Enter the year of your date of birth');
        $(this).focus();
        return false;
    }
    return true;
}

$('#verifyIdentityFrom').submit(
    function(){
        return checkDob();
    }
);

$('#verifyIdentityFrom').validate({
    rules: {
        customerNumber: {
            required: true,
            maxlength: 8
        },
        email: {
            required: true,
            email: true
        }
    }
});
