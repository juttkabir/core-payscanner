function removerules(){}

var succssMsg = '';
var fileValidateFlag = 0;
$(function() {
    $('#addressContainer').hide();
    var button = $('#file');
    var fileUpload = new AjaxUpload(button, {
        action: 'registration_form_conf.php',
        name: 'passportDoc',
        // for single upload this should have 0 (zero appended)
        autoSubmit: false,
        allowedExtensions: ['.jpg', '.png', '.gif', '.jpeg', '.pdf'],
        // max size
        onChange: function(file, ext) {
            if ((ext && /^(jpg|png|jpeg|gif|pdf)$/i.test(ext))) {
                var fileSizeBytes = this._input.files[0].size;
                if(fileSizeBytes < 2097152){
                    fileValidateFlag = 1;
                    //button.text("Selected");
                    button.css({
                        "background": "#CCCCCC",
                        "font-weight": "bold"
                    });
                    button.html('Document Selected');
                }else{
                    alert("file is too large, maximum file size is 2MB");
                }
            } else {
                alert('Please choose a standard image file to upload, JPG or JPEG or PNG or GIF or PDF');
                button.css({
                    "background": "#ECB83A",
                    "font-weight": "normal"
                });
                button.html('Select Document');
            }
        },
        onSubmit: function(file, ext) {
            // If you want to allow uploading only 1 file at time,
            // you can disable upload button
            this.disable();

            $('#wait').fadeOut("slow");
            $('#wait').fadeIn("slow");
            $('#fileUploader').fadeIn("slow");

        },

        onComplete: function(file, response) {
            // enable upload button
            this.enable();
            //this.disable();
            $('#emailValidator').html(' ');
            $('#wait').fadeOut("fast");
            $('#fileUploader').fadeOut("slow");

            alert(response);
            $('#msg').show();
            $('#msg').html(succssMsg);
        }
    });


    $(document).ready(function() {
        $('#register').click(function(){
            if($( "#initial_load_row" ).is(":visible") == false){
                $("#card_issue_currency").rules("remove");
                $("#initial_load").rules("remove");
            }
        });

        // Date Picker
        Date.format = 'dd/mm/yyyy';
        $('#passportExpiry').datePicker({
            clickInput: true,
            createButton: false
        });

        $('#passportIssue').datePicker({
            clickInput: true,
            createButton: false,
            endDate: (new Date()).toString()
        });

        $('#idExpiry').datePicker({
            clickInput: true,
            createButton: false
        });

        $('#passportIssue').dpSetStartDate(' ');

        // Masking
        $('passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');

        //$('#line1').mask('**-***-*********-9-***************');
        //$('#line2').mask('999999-9-*-999999-9-aaa-***********');

        // Form Validation
        $("#senderRegistrationForm").validate({
            rules: {
                forename: {
                    required: true
                },
                /*
                 password: {
                 required: true, minlength: 6
                 },
                 cpassword: {
                 required: true, equalTo: "#password", minlength: 6
                 },*/
                surname: {
                    required: true
                },
                postcode: {
                    required: true
                },
                // passportNumber: {
                //     required: function() {
                //         if ($('#line1').val() == '') return true;
                //         else return false;
                //     }
                //     //,minlength: 37
                // },
                /* line1: {
                 required: function() {
                 if ($('#passportNumber').val() == '') return true;
                 else return false;
                 }
                 }, */
                /* line2: {
                 required: function() {
                 if ($('#line1').val() == '') return false;
                 else return true;
                 }
                 },
                 line3: {
                 required: function() {
                 if ($('#line2').val() == '' || $('#line1').val() == '') return false;
                 else return true;
                 }
                 },
                 idExpiry: {
                 required: function() {
                 if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;
                 else return false;
                 }
                 }, */
                // passportExpiry: {
                //     required: true
                //     // required: function() {
                //     // if ($('#passportNumber').val() != '') return true;
                //     // else return false;
                //     // }
                // },
                initial_load: {
                    required: false
                },
                card_issue_currency: {
                    required: false
                },
            },

            messages: {
                // passportNumber: "Please provide your Passport or National Identity card number",
                /* line1: "Please provide your Passport or National Identity card number",
                 line2: "Please enter the remaining part of your National Identity card number",
                 line3: "Please enter the remaining part of your National Identity card number",*/
                // passportExpiry: "Please enter the expiry date of your Passport",
                password: "Password should be of atleast 6 characters",
                cpassword: "Confirm password does not match",
            },

            submitHandler: function() {
                register();
                return false;
            }
        });

        // Validating Email availability checks

        $("#email").blur(
            function() {
                var email = $('#email').val();

                if (email != '') {
                    $.ajax({
                        url: "registration_form_conf.php",
                        data: {
                            email: email,
                            chkEmailID: '1'
                        },
                        type: "POST",
                        cache: false,
                        success: function(data) {
                            existingCurr = [];
                            Currencies = [];
                            emaildup = data.split('|');

                            if(emaildup[2] != undefined){
                                prepaidCurrency = emaildup[2].split(',');
                                ExistingCurrency =  document.getElementById("card_issue_currency");
                                for(i=0;i<ExistingCurrency.length; i++){
                                    existingCurr.push(ExistingCurrency.options[i].text);
                                }
                                $("#card_issue_currency").empty();
                                for(j=0;j<existingCurr.length;j++){
                                    if($.inArray(existingCurr[j],prepaidCurrency) == -1){
                                        $("#card_issue_currency").append('<option value="'+existingCurr[j]+'">'+existingCurr[j]+'</option>');
                                    }
                                }
                                $('#msg').show();
                                $('#msg').html(emaildup[0]);
                            }else{
                                $("#card_issue_currency").empty();
                                $("#card_issue_currency").append('<option value="---SELECT---">---SELECT---</option><option value="USD">USD</option><option value="EUR">EUR</option>');
                            }

                            //console.log(Currencies);
                            if($.trim(emaildup[0]) != ''){
                                $('.hideDivOnDuplicateEmail').css('display','none');
                                //$("#passportNumber").rules("remove", "required");
                                //$(".hideOnDuplicateEmail").rules("remove", "required");
                                //$("#passportExpiry").rules("remove", "required");

                                /* 		$("#postcode").rules("remove", "required");

                                 $("#line1").rules("remove", "required"); */
                                $('#senderRegistrationForm').find('.hideOnDuplicateEmail').each( function (index,element) {
                                    $(element).rules('remove', 'required');
                                });

                                if($.trim(emaildup[1])  == 'PC'){
                                    $("#currency_row").css("display","none");
                                    $("#initial_load_row").css("display","none");
                                    $("#card_issue_currency").rules("remove", "required");
                                    $("#initial_load").rules("remove", "required");
                                }else if($.trim(emaildup[1])  == 'TC'){
                                    /* 		$("#currency_row").css("display","block");

                                     $("#initial_load_row").css("display","block");

                                     $("#card_issue_currency").rules("add", "required");

                                     $("#initial_load").rules("add", "required"); */
                                }
                            }else{
                                if($('#register_type').val()  == 'prepaid'){
                                    $("#currency_row").css("display","block");
                                    $("#initial_load_row").css("display","block");
                                    //$("#card_issue_currency").rules("add", "required");
                                    $("#initial_load").rules("add", "required");
                                }

                                $('.hideDivOnDuplicateEmail').css('display','block');
                                // $('#senderRegistrationForm').find('.hideOnDuplicateEmail').each(function (index,element) {
                                // console.log(element.id);
                                // if(element.id !="passportExpiry"){
                                // $(element).rules('remove', 'required');
                                // }

                                // });
                            }

                            $('#emailValidator').html($.trim(emaildup[0]) );
                        }
                    });
                }
            });

        // Check if the passport availability checks
        $('#passportNumber').blur(
            function() {
                setTimeout(function() {
                    passportAvailabilty();
                }, 100);
            });

        function passportAvailabilty() {
            var passport = $('#passportNumber').val();
            //alert(passport);

            if (passport != '') {
                $.ajax({
                    url: "registration_form_conf.php",
                    data: {
                        passportNum: passport,
                        chkPassport: '1'
                    },
                    type: "POST",
                    cache: false,
                    success: function(data) {
                        $('#passportAvailability').html(data);
                    }
                });
            }
        }

        // ID card availability checks
        $('#line3').blur(
            function() {
                var idLine1 = $('#line1').val();
                var idLine2 = $('#line2').val();
                var idLine3 = $('#line3').val();

                if (idLine1 != '' && idLine2 != '' && idLine3 != '') {
                    var idCard = idLine1 + idLine2 + idLine3;
                } else return false;

                $.ajax({
                    url: "registration_form_conf.php",
                    data: {
                        idCardNumber: idCard,
                        chkIDCard: '1'
                    },
                    type: "POST",
                    cache: false,
                    success: function(data) {
                        $('#NICAvailability').html(data);
                    }
                });
            });

        /**
         * function checkForServices()
         * against ticket 12761
         * To ask for the other services
         */
        function checkForServices(msg){
            //alert($.trim(msg)+"   "+ $("#initial_load").val());
            var confirmResult = false;

            if($.trim(msg) == 'PC'){
                confirmResult = confirm("You are already registered as Prepaid customer. Do you want to register as Trading Customer ?");
                //	confirmResult = true;
            }else if($.trim(msg) == 'TC'){
                confirmResult = confirm("You are already registered as Trading customer. Do you want to register as Prepaid Customer?");
                // if( (confirmResult == true )  == false ){

                // //$("#currency_row").css("display","block");

                // //$("#initial_load_row").css("display","block");

                // // $("#card_issue_currency").rules("add", "required");
                // //$("#initial_load").rules("add", "required");
                // //$("#initial_load").val('');
                // confirmResult = false;
                // }
            }else if ($.trim(msg) == 'PC,TC'){
                // alert("here both pc and tc");
                // return false;
                // if($('#card_issue_currency option').size()>1){

                confirmResult = confirm('Do you want to proceed?');

                //$("#currency_row").css("display","block");
                // if($('#card_issue_currency').val() =='---SELECT---'){
                // //alert("Please choose currency.");
                // return false;
                // }else{
                // confirmResult = confirm('Do you want another currency for prepaid?');
                // }

                // }else{
                // alert("You have registered with all currencies.");
                // return false;
                // }

                $('.hideDivOnDuplicateEmail').css('display','block');
                $(".hideOnDuplicateEmail").rules("add","required");
            }
            // else if ($.trim(msg) == ''){
            //     confirmResult = true;
            // }
            return confirmResult;
        }

        function initialLoadLimit(){
            // if($("#initial_load_row").is("visible") == false){
            // return true;
            // }

            var initialLoad = $("#initial_load").val(),
                receivingCurrency = $("#card_issue_currency").val();

            if(receivingCurrency == 'EUR') {
                if(initialLoad < 20) {
                    alert('You can not send money less than 20');
                    return false ;
                }else if(initialLoad > 500){
                    alert('You can not send money more than500');
                    return false ;
                }
            }else if(receivingCurrency == 'USD'){
                if(initialLoad < 20) {
                    alert('You can not send money less than 20');
                    return false ;
                }else if(initialLoad > 500){
                    alert('You can not send money more than 500');
                    return false ;
                }
            }else if(receivingCurrency == 'GBP'){
                if(initialLoad < 20) {
                    alert('You can not send money less than 20');
                    return false ;
                }else if(initialLoad > 400){
                    alert('You can not send money more than 400');
                    return false ;
                }
            }
            return true;
        }
        // Ajax Registration of Sender

        function register() {
            // if(initialLoadLimit() == false){
            // return false;
            // }

            //Passport Issue and Expiry Date
            var passportIssue = new Date($('#passportIssue').val());
            var passportExpiry = new Date($('#idExpiry').val());

            if (passportIssue != '') {
                if (passportIssue >= passportExpiry) {
                    alert("Your Passport issued date must be before the expiry date.");
                    return false;
                }
            }

            // Registration Request

            var data = $('#senderRegistrationForm').serialize();
            var formData = $('#senderRegistrationForm').serialize();

            data += "&register=Register";
            data += "&slog=yes";
            data +="&cardType=<? echo $register_type; ?>";

            $('#wait').fadeIn("fast");
            $("#loadingMessage").text('Submitting your registration');
            $.ajax({
                url: "registration_form_conf.php",
                data: data,
                type: "POST",
                cache: false,
                success: function(msg) {
                    servicesFlag = checkForServices(msg);
                    //alert(servicesFlag);

                    //console.log("in registration= "+$.trim(msg));
                    if(servicesFlag == true){
                        updateRegisterType(formData);
                        // if($.trim(msg) == 'PC,TC'){
                        // addOtherCurrency(formData);
                        // }else{
                        // updateRegisterType(formData);
                        // }
                    }

                    if (msg.search(/Sender is registered successfully/i) >= 0) {
                        if (fileValidateFlag != 0) {
                            succssMsg = msg;
                            $("#loadingMessage").text('Please wait for the confirmation of your passport upload.');
                            var plog = "yes";
                            var chk = "1";

                            $.ajax({
                                url: "registration_form_conf.php",
                                data: {
                                    plog: plog,
                                    chk: chk
                                },
                                type: "POST",
                                cache: false,
                                success: function(msg) {
                                }});


                            fileValidateFlag = 0;
                            fileUpload.submit();
                        }else{
                            $('#msg').show();
                            $('#msg').html(msg);
                            $('#wait').fadeOut("fast");
                        }
                        resetFormData();
                    }else{
                        if($.trim(msg) != "TC" && $.trim(msg) != "PC" && $.trim(msg) != "PC,TC"){
                            $('#msg').show();
                            $('#msg').html(msg);
                            $('#wait').fadeOut("fast");
                        }else{
                            $('#msg').show();
                            $('#msg').html("");
                            $('#wait').fadeOut("fast");
                        }
                    }
                }
            });
        }

        function addOtherCurrency(formData){
            var data = formData;
            data += '&rtype=addOtherCurrency';

            $.ajax({
                url: "registration_form_conf.php",
                data: data,
                type: "POST",
                cache: false,
                success: function(msg){
                    $('#msg').html('Currency Updated.');
                    resetFormData();
                }
            });
        }

        function  updateRegisterType(formData){
            var data = formData;

            data += '&rtype=updateRegisterType';

            $.ajax({
                url: "registration_form_conf.php",

                data: data,

                type: "POST",

                cache: false,

                success: function(msg){
                    $('#msg').css("color","green")
                    $('#msg').css("font-style","italic")
                    $('#msg').html(msg);
                    window.location.replace = "online_all/verifyCustomer.php?from=registration";
                    resetFormData();
                }
            });
        }


        function resetFormData() {
            $('#addressContainer').fadeOut('fast');

            document.forms[0].reset();
            $('#file').css({
                "background-color": "#ECB83A",
                "border": "1px solid #E5A000",
                "font-weight": "normal"
            });

            $('#file').html('Select Document');
            //$('#senderRegistrationForm')[0].reset();
        }
        // Trigger the search address function
        $('#searchAddress').click(
            function() {
                searchAddress();
            });
    });

});

// Populate the Address in the fields
function getAddressData(ele) {
    var value = ele.value;
    var arrAddress = value.split('/');
    var buildingNumber = $.trim(arrAddress[0]);
    var buildingName = $.trim(arrAddress[1]);
    var subBuilding = $.trim(arrAddress[2]);
    var street = $.trim(arrAddress[3]);
    var subStreet = $.trim(arrAddress[4]);
    var town = $.trim(arrAddress[5]);

    //var postcode = $.trim(arrAddress[6]);

    var organization = $.trim(arrAddress[7]);
    var buildingNumberVal = '';
    var buildingNameVal = '';
    var streetValue = '';
    var postCode = $('#postCodeSearch').val();
    if (buildingNumber != '') buildingNumberVal += buildingNumber;
    if (buildingName != '') buildingNameVal += buildingName;
    if (subBuilding != '') buildingNameVal += ' ' + subBuilding;
    if (street != '') streetValue += street;
    if (subStreet != '') streetValue += ' ' + subStreet;

    $('#buildingNumber').val(buildingNumberVal);
    $('#buildingName').val(buildingNameVal);
    $('#street').val(streetValue);
    $('#town').val(town);
    $('#postcode').val(postCode);

}

// if Press Enter on any field in the address area trigger the search address function
function enterToSearch(e) {
    if (e.which) {
        keyCode = e.which;
        if (keyCode == 13) {
            e.preventDefault();
            searchAddress();
        }
    }
}

// Calls the API for suggessted address

function removeValidation(id){
    var texts =$('#'+id).next('.error').html('');
}

function searchAddress() {
    $('#residenceCountry').val('United Kingdom');
    $('#addressContainer').fadeOut('fast');
    postcode = $.trim($('#postCodeSearch').val());
    buildingNumber = $.trim($('#buildingNumber').val());
    street = $.trim($('#street').val());
    town = $.trim($('#town').val());

    if (postcode == '') {
        alert("Enter a postcode to search for your address");
        $('#postCodeSearch').focus();
        return;
    }
    $("#loadingMessage").text('Searching Address...');
    $('#wait').fadeIn("fast");

    $.ajax({
        url: "https://core.payscanner.com/api/gbgroup/addresslookupCus.php",
        data: {
            postcode: postcode,
            buildingNumber: buildingNumber,
            street: street,
            town: town
        },
        type: "POST",
        cache: false,
        success: function(data) {
            //alert(data.match(/option/i));
            $('#wait').fadeOut("slow");
            if (data.search(/option/i) >= 0) {
                $('#addressContainer').fadeIn('fast');
                $('#suggesstions').html(data);
            } else {
                $('#addressContainer').fadeIn('fast');
                $('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');
            }
        }
    });
}

// Clear the address section
function clearAddress() {
    $('#buildingNumber').val('');
    $('#buildingName').val('');
    $('#street').val('');
    $('#town').val('');
    $('#province').val('');
}

function passportMask() {
    var passportCountry = $('#passportCountry').val(),
        passportNumber = $('#passportNumber');
    //alert(passportCountry);
    switch (passportCountry) {
        case 'United Kingdom':
            passportNumber.unmask().mask('*********-9-***-9999999-a-9999999-<<<<<<<<<<<<<<-*-9');
            passportNumber.removeAttr("disabled");
            break;
        case 'Portugal':
            passportNumber.unmask().mask('*******<<-9-***-9999999-a-9999999-**********<<<<-*-9');
            passportNumber.removeAttr("disabled");
            break;
        case 'USA':
            passportNumber.unmask().mask('*********-9-***-9999999-a-9999999-*********<****-*-9');
            passportNumber.removeAttr("disabled");
            break;
        case 'Australia':
            passportNumber.unmask().mask('********<-9-***-9999999-a-9999999-<*********<<<<-*-9');
            passportNumber.removeAttr("disabled");
            break;
        case 'Spain':
            passportNumber.unmask().mask('********<-9-aaa-9999999-a-9999999-***********<<<-*-9');
            passportNumber.removeAttr("disabled");
            break;
        case 'Ireland':
            /* 	$('#passportNumber').unmask().mask('a999999<<9aaa9999999a9999999<<<<<<<<<<<<<<<9'); */
            passportNumber.unmask().mask('aa99999999aaa9999999a9999999<<<<<<<<<<<<<<<9');
            passportNumber.removeAttr("disabled");
            break;
        case 'Netherlands':
            /* $('#passportNumber').unmask().mask('a999aa9a99a<<9999999a9999999<<<<<<<<<<<<<<<9'); */
            passportNumber.unmask().mask('aa9999a999aaa9999999a9999999999999999<<<<<99');
            passportNumber.removeAttr("disabled");
            break;
        case 'Canada':
            /* $('#passportNumber').unmask().mask('a999aa9a99a<<9999999a9999999<<<<<<<<<<<<<<<9'); */
            passportNumber.unmask().mask('aa999999<9aaa9999999a9999999<<<<<<<<<<<<<<99');
            passportNumber.removeAttr("disabled");
            break;
        default:
            alert('Please select a country and enter your passport number');
            break;
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
