$('#changePasswordForm').validate({
    rules: {
        oldPass: {
            required: true,
            minlength: 6
        },
        pass1: {
            required: true,
            minlength: 6
        },
        pass2: {
            required: true,
            equalTo: '#pass1'
        },
        messages: {
            oldPass: 'Enter Old Password to change the password.',
            pass1: 'Enter new password',
            pass2: {
                required: 'Re-enter your new password'
            }
        }
    }
});
