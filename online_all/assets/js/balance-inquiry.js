$(function () {
    $('#availableBalance').hide();
    var cd = $('#countdown');
    var c = parseInt(cd.text(), 10);
    var interv = setInterval(function () {
        c--;
        cd.html(c);
        if (c == 0) {
            window.location.reload(false);
            location.reload(true);
            clearInterval(interv);
        }
    }, 1000);
});

$("#getBalance").click(function (event) {
    event.preventDefault();
    var currency = $("#receivingCurrency").val();
    $.ajax({
        url: 'ajaxCalls.php',
        method: 'POST',
        async: false,
        data: {
            rtypeMode: 'prepaidBalanceInquiry',
            san: currency,
            customerID: $("#customerID").val()
        },
        success: function (data) {
            var balanceInquiryResponse = data.split(',');
            var balance = balanceInquiryResponse[0];
            var balanceTwoDecimal = parseFloat(balance).toFixed(2);

            if (balanceInquiryResponse[2] == 0) {
                $('#availableBalance strong').html(balanceTwoDecimal + ' ' + balanceInquiryResponse[1]);
                $('#availableBalance').show();
            }
        }
    });
});
