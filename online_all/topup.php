<?php
/**
 * @package: Online Module
 * @Ticket : 12671
 * @author : Imran Naqvi
 */
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
    header("LOCATION: logout.php");
}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

?>
<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Top Up Card - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>
    <style>
        .xRate{
            margin-top:10px;
        }
    </style>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'topup'; ?>
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Top Up</h1>
                        <p class="page-header-desc">Top Up Card</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Top Up</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <?php if ((isset($strError) && !empty($strError)) || (isset($strMsg) && !empty($strMsg))):?>
                        <div class="alert alert-danger error_msg">
                            <?php
                            echo !empty($strError) ? $strError . '<br>' : '';
                            echo !empty($strMsg) ? $strMsg . '<br>' : '';
                            ?>
                        </div>
                    <?php else: ?>
                    <div class="alert alert-danger error_msg" id="infoMessages"></div>
                    <?php endif; ?>

                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Top Up Card</h2>
                        <p>Enter the currency and amount to Top Up.</p>
                        <form action="debit_card.php" method="post" id="topupForm">
                            <input type="hidden" id="customerID" name="customerID" value="<?= $_SESSION['loggedInUser']['userID']; ?>" />
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="sendingAmount" class="input-desc">I have GBP:</label>
                                        <input name="sendingAmount" id="sendingAmount" type="text" maxlength="20" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                    </div><!-- End .from-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="receivingCurrency" class="input-desc">Buy/Top Up:</label>
                                        <select name="receivingCurrency" id="receivingCurrency" class="form-control select_field">
                                            <option value="USD">USD</option>
                                            <option value="EUR">EUR</option>
                                        </select>
                                    </div><!-- End .from-group -->
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb40">
                                        <label class="input-desc btn-block">Rate:</label>
                                        <input type="hidden" id="sendingCurrency" name="sendingCurrency" value="GBP" readonly="" maxlength="20" class="select_field" autocomplete="off">
                                        <div id="xRate" class="xRate">0.0000</div>
                                    </div><!-- End .from-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="receivingAmount" class="input-desc">Receiving Amount:</label>
                                        <input name="receivingAmount" id="receivingAmount" type="text" maxlength="20" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                    </div><!-- End .from-group -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group mb5">
                                        <input name="topup" id="topup" type="submit" class="btn btn-custom mr10" value="Continue">
                                        <input type="reset" class="btn btn-default" value="Clear">
                                    </div><!-- End .from-group -->
                                </div>
                            </div>


                        </form>
                    </div><!-- End .form-wrapper -->

                </div><!-- End .col-sm-6 -->

                <div class="mb40 visible-xs"></div><!-- space -->

                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>

                    <p>Send money safely and securely with <?= $title; ?> Online.</p>

                    <div class="mb10"></div><!-- space -->

                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->


        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/topup.js"></script>
</body>
</html>
