<?php
/**
 * @package: Online Module
 * @subpackage: Send Transaction

 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$logoHtml = $agentUiData['logoHtml'];

//ini_set("display_errors","on");
//error_reporting(E_ALL);
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";
$intBackFlag = false;

// print_r($_SESSION['loggedUserData']);
$agentIDQ = "SELECT agentID FROM " . TBL_CUSTOMER . " WHERE customerID='" . $_SESSION['loggedInUser']['userID'] . "'  ";
$agentIDResult = selectFrom($agentIDQ);
// print_r($agentIDResult['agentID']);
if (isset($_SESSION['ben'])) {
	$strBenNameCont = selectFrom("select firstName from beneficiary where benID = '" . $_SESSION['ben'] . "' ");
	$strBenName = $strBenNameCont["firstName"];
}
//debug($_REQUEST);
if (!empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') || ($_REQUEST['sendingAmount'] > '0' && !empty($_REQUEST['sendingAmount']))) {
	//debug($_REQUEST);
	$strBaseCurrency1 = "";
	$strBaseCurrency2 = "";
	$queryBaseCurrencies = "select distinct(baseCurrency) from online_exchange_rates";
	$arrBaseCurrencies = selectMultiRecords($queryBaseCurrencies);

	$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
	$strReceivingAmount = number_format(floatval(trim($_REQUEST['receivingAmount'])), 4, ".", "");
	$strSendingAmount = number_format(floatval(trim($_REQUEST['sendingAmount'])), 4, ".", "");
	//debug($strReceivingAmount);
	//debug($strSendingAmount);
	if ($strReceivingAmount != 0 && $strSendingAmount != 0) {
		$strReceivingAmount = 0;
	}
	if ($strReceivingAmount != 0) {
		$strBaseAmountCurrency = $strReceivingCurrency;
		$strQuotedAmountCurrency = $strSendingCurrency;
		$fltAmountBase = $strReceivingAmount;
		$fltAmountIsSending = false;

	} elseif ($strSendingAmount != 0) {
		$strBaseAmountCurrency = $strSendingCurrency;
		$strQuotedAmountCurrency = $strReceivingCurrency;
		$fltAmountBase = $strSendingAmount;
		$fltAmountIsSending = true;
	}
	for ($i = 0; $i < count($arrBaseCurrencies); $i++) {
		if ($arrBaseCurrencies[$i]["baseCurrency"] == $strSendingCurrency) {
			$strBaseCurrency1 = $strSendingCurrency;
			$strFinalBaseCurrency = $strSendingCurrency;
			$strFinalQuotedCurrency = $strReceivingCurrency;
			$flagBaseIsSending = true;
		} elseif ($arrBaseCurrencies[$i]["baseCurrency"] == $strReceivingCurrency) {
			$strBaseCurrency2 = $strReceivingCurrency;
			$strFinalBaseCurrency = $strReceivingCurrency;
			$strFinalQuotedCurrency = $strSendingCurrency;
			$flagBaseIsSending = false;
		}
	}

	//debug($fltAmountIsSending);
	//debug($flagBaseIsSending);

	/*if($strBaseAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "RATE";

			}
			elseif($strQuotedAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "INVERSE";
			}*/

	if ($strBaseCurrency1 != "" && $strBaseCurrency2 != "") {
		$flagBaseCurrency = "Both";

	} elseif ($strBaseCurrency1 == "" && $strBaseCurrency2 == "") {
		$flagBaseCurrency = "None";

	} else {
		$flagBaseCurrency = "One";

	}

	//debug($flagBaseCurrency);
	if ($flagBaseCurrency == "Both") {
		if ($strBaseAmountCurrency == 'GBP') {
		} else if ($strQuotedAmountCurrency == 'GBP') {
			$strFirstCurr = $strBaseAmountCurrency;
			$strBaseAmountCurrency = $strQuotedAmountCurrency;
			$strQuotedAmountCurrency = $strFirstCurr;
		} else if ($strQuotedAmountCurrency == 'EUR') {
			$strFirstCurr = $strBaseAmountCurrency;
			$strBaseAmountCurrency = $strQuotedAmountCurrency;
			$strQuotedAmountCurrency = $strFirstCurr;
		}
		if ($strSendingCurrency == "GBP") {
			$flgExtendMargin = "Sub";
		} else if ($strReceivingCurrency == "GBP") {
			$flgExtendMargin = "Add";
		} else if ($strSendingCurrency == "EUR") {
			$flgExtendMargin = "Sub";
		} else if ($strReceivingCurrency == "EUR") {
			$flgExtendMargin = "Add";
		}
		$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' order by id DESC LIMIT 1";
		$arrExchangeRate = selectFrom($strQueryRate);
		$descript = "Get rate called";

		$customerID = $_SESSION['loggedUserData']['userID'];

//print_r($_SESSION["loginHistoryID"]);
		activities($_SESSION["loginHistoryID"], "SELECTION", $customerID, TBL_ONLINE_EXCHANGE_RATE, $descript);

		//debug($arrExchangeRate);
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "' AND  baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";

		$arrExchangeMargin = selectFrom($strQueryMargin);

		$fltExchangeRate = $arrExchangeRate['exchangeRate'];
		$fltInverseRate = $arrExchangeRate['inverseRate'];
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000, 4);
		//debug($_SESSION['exchangeRateError']);
		if (empty($arrExchangeMargin['margin'])) {

			if ($_REQUEST['sendingAmount'] != '') {
				$convertedCurrency = $_REQUEST['receivingCurrency'];
				if ($_REQUEST['sendingCurrency'] == 'GBP') {
					if ($_REQUEST['sendingAmount'] < 100 && $_REQUEST['sendingAmount'] > 50000) {
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strSendingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case18";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				} else {
					$sendingCurrency = $_REQUEST['sendingCurrency'];
					$sendingAmount = $_REQUEST['sendingAmount'];
					$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP = $sendingAmount * $getExchangeRateResult[0]['inverseRate']; //exit;
					if ($totalAmountInGBP < 100 && $totalAmountInGBP > 50000) {

						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case17";
						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				}
			} else {
				$convertedCurrency = $_REQUEST['sendingCurrency'];
				if ($_REQUEST['receivingCurrency'] == 'GBP') {

					if ($_REQUEST['receivingAmount'] < 100 && $_REQUEST['receivingAmount'] > 50000) {
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case16";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				} else {
					$recCurrency = $_REQUEST['receivingCurrency'];
					$recAmount = $_REQUEST['receivingAmount'];
					$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP = $recAmount * $getExchangeRateResult[0]['inverseRate'];
					if ($totalAmountInGBP < 100 && $totalAmountInGBP > 50000) {

						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case15";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				}

			}

			/* $_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				//$_SESSION['exchangeRateError1'] = "Test Case15";

				 if($_REQUEST['sendingCurrency'] == 'GBP' && ($_SESSION['sendingAmount']!=''  && $_SESSION['sendingAmount']!= '0.0000' && $_SESSION['sendingAmount']<100 && $_SESSION['sendingAmount']>50000))
				 $option=1;
				 if($_REQUEST['sendingCurrency'] != 'GBP' && ($_SESSION['sendingAmount']!=''  && $_SESSION['sendingAmount']!= '0.0000'))
				 $option=2;
				 if($_REQUEST['receivingCurrency'] == 'GBP' && ($_SESSION['receivingAmount']!=''  && $_SESSION['receivingAmount']!= '0.0000' && $_SESSION['receivingCurrency']<100 && $_SESSION['receivingCurrency']>50000))
				 $option=3;
				 if($_REQUEST['receivingCurrency'] != 'GBP' && ($_SESSION['receivingAmount']!=''  && $_SESSION['receivingAmount']!= '0.0000'))
				 $option=4;

				switch ($option){
				case 1:
				$_SESSION['totalAmountInGBP'] = $strSendingAmount;
				$_SESSION['exchangeRateError1'] = "Test Case15a";
				break;

				case 2:
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate'];

				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				$_SESSION['exchangeRateError1'] = "Test Case15b";
				break;

				case 3:
				$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
				$_SESSION['exchangeRateError1'] = "Test Case15c";
				break;
				case 4:
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];

				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				$_SESSION['exchangeRateError1'] = "Test Case15d";
				break;

				default:
				echo "NULL";
				}

				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-ii-new.php");
				exit(); */
		}
		if ($strMarginType == "FIXED") {
			if ($flgExtendMargin == "Add") {
				$fltExchangeRate = $fltExchangeRate + $fltMargin;
			} else {
				$fltExchangeRate = $fltExchangeRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($flgExtendMargin == "Add") {
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				} else {
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				//debug($fltMargin);
				if ($flgExtendMargin == "Add") {
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				} else {
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($flgExtendMargin == "Add") {
				$fltExchangeRate = $fltExchangeRate + $fltMargin;
			} else {
				$fltExchangeRate = $fltExchangeRate - $fltMargin;
			}
		}
		//$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
		if ($strSendingCurrency == 'GBP' && $strSendingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
		} else if ($strSendingCurrency == 'GBP' && $strReceivingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase / $fltExchangeRate;
		} else if ($strReceivingCurrency == 'GBP' && $strReceivingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
		} else if ($strReceivingCurrency == 'GBP' && $strSendingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase / $fltExchangeRate;
		} else if ($strSendingCurrency == 'EUR' && $strSendingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
		} else if ($strSendingCurrency == 'EUR' && $strReceivingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase / $fltExchangeRate;
		} else if ($strReceivingCurrency == 'EUR' && $strReceivingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
		} else if ($strReceivingCurrency == 'EUR' && $strSendingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase / $fltExchangeRate;
		}
		$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100, 2);
		////debug($arrExchangeRate,true);
	} elseif ($flagBaseCurrency == "One") {
		if ($strBaseCurrency1 != "") {
			$flgExtendMargin = "Sub";
		} else {
			$flgExtendMargin = "Add";
		}
		//debug($flagRateOrInverse);
		$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' order by id DESC LIMIT 1";

		$arrExchangeRate = selectFrom($strQueryRate);

		if ($strBaseCurrency1 != "" && $strSendingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase;
		} else if ($strBaseCurrency2 != "" && $strReceivingAmount != 0) {
			$fltConvertedAmnt = $fltAmountBase;
		} else {
			$fltConvertedAmnt = $fltAmountBase / $arrExchangeRate['exchangeRate'];
		}

		//debug($arrExchangeRate);
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "'  AND  baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' and $fltConvertedAmnt between fromAmount AND toAmount and statusType='Enable' order by id DESC LIMIT 1";
		//debug($strQueryMargin);
		$arrExchangeMargin = selectFrom($strQueryMargin);

		if (empty($arrExchangeMargin['margin'])) {

			if ($_REQUEST['sendingAmount'] != '') {
				$convertedCurrency = $_REQUEST['receivingCurrency'];
				if ($_REQUEST['sendingCurrency'] == 'GBP') {
					if ($_REQUEST['sendingAmount'] < 100 || $_REQUEST['sendingAmount'] > 50000) {
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strSendingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case14";
						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				} else {
					$sendingCurrency = $_REQUEST['sendingCurrency'];
					$sendingAmount = $_REQUEST['sendingAmount'];
					$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP = $sendingAmount * $getExchangeRateResult[0]['inverseRate']; //exit;
					if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case13";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				}
			} else {
				$convertedCurrency = $_REQUEST['sendingCurrency'];
				if ($_REQUEST['receivingCurrency'] == 'GBP') {

					if ($_REQUEST['receivingAmount'] < 100 || $_REQUEST['receivingAmount'] > 50000) {
						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
						//$_SESSION['exchangeRateError1'] = "Test Case12";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				} else {
					$recCurrency = $_REQUEST['receivingCurrency'];
					$recAmount = $_REQUEST['receivingAmount'];
					$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
					$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
					$totalAmountInGBP = $recAmount / $getExchangeRateResult[0]['inverseRate'];
					if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

						$_SESSION['receivingAmount'] = $strReceivingAmount;
						$_SESSION['sendingAmount'] = $strSendingAmount;
						$_SESSION['sendingCurrency'] = $strSendingCurrency;
						$_SESSION['receivingCurrency'] = $strReceivingCurrency;
						$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
						//$_SESSION['exchangeRateError1'] = "Test Case11";

						$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

						header("LOCATION: make_payment-ii-new.php");
						exit();

					}

				}

			}

		}

		//debug($arrExchangeMargin);

		//debug($arrExchangeRate);
		if (!empty($arrExchangeMargin)) {
			$fltExchangeRate = $arrExchangeRate['exchangeRate'];
			$fltInverseRate = $arrExchangeRate['inverseRate'];
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			//debug($strMarginType);
			//debug($flagRateOrInverse);
			$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000, 4);
			//debug($fltExchangeRate);
			if ($strMarginType == "FIXED") {
				if ($flgExtendMargin == 'Add') {
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				} else {
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			} elseif ($strMarginType == "PIPS") {
				if ($strQuotedAmountCurrency == "JPY") {
					$fltMargin = $fltMargin * 0.01;
					if ($flgExtendMargin == 'Add') {
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					} else {
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				} else {
					$fltMargin = $fltMargin * 0.0001;
					//debug($fltMargin);
					if ($flgExtendMargin == 'Add') {
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					} else {
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
			} elseif ($strMarginType == "PERCENTAGE") {
				$fltMargin = $fltMargin * 0.01;
				if ($flgExtendMargin == 'Add') {
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				} else {
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}

			if ($strBaseCurrency1 != "" && $strSendingAmount != 0) {
				$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
			} else if ($strBaseCurrency2 != "" && $strReceivingAmount != 0) {
				$fltConvertedAmnt = $fltAmountBase * $fltExchangeRate;
			} else {
				$fltConvertedAmnt = $fltAmountBase / $fltExchangeRate;
			}
			$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100, 2);
		}

		if (empty($fltExchangeRate)) {
			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			$_SESSION['re'] = "N";
			$_SESSION['exchangeRateError'] = "We are sorry for the inconvenience, currently we are not dealing in this currency";
			//header("LOCATION: make_payment-ii-new.php");
		}
	} elseif ($flagBaseCurrency == "None") {

		if ($_REQUEST['sendingAmount'] != '') {
			$convertedCurrency = $_REQUEST['receivingCurrency'];
			if ($_REQUEST['sendingCurrency'] == 'GBP') {
				if ($_REQUEST['sendingAmount'] < 100 || $_REQUEST['sendingAmount'] > 50000) {
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case10";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
					header("LOCATION: make_payment-ii-new.php");
					exit();

				}

			} else {
				$sendingCurrency = $_REQUEST['sendingCurrency'];
				$sendingAmount = $_REQUEST['sendingAmount'];
				$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP = $sendingAmount * $getExchangeRateResult[0]['inverseRate']; //exit;
				if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case9";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
					header("LOCATION: make_payment-ii-new.php");
					exit();

				}

			}
		} else {
			$convertedCurrency = $_REQUEST['sendingCurrency'];
			if ($_REQUEST['receivingCurrency'] == 'GBP') {

				if ($_REQUEST['receivingAmount'] < 100 || $_REQUEST['receivingAmount'] > 50000) {
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case8";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

					header("LOCATION: make_payment-ii-new.php");
					exit();

				}

			} else {
				$recCurrency = $_REQUEST['receivingCurrency'];
				$recAmount = $_REQUEST['receivingAmount'];
				$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP = $recAmount * $getExchangeRateResult[0]['inverseRate'];
				if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case7";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
					header("LOCATION: make_payment-ii-new.php");
					exit();

				}

			}

		}

		if ($strBaseAmountCurrency == $strSendingCurrency) {
			$fromCurrency = $strSendingCurrency;
			$toCurrency = $strReceivingCurrency;

		} else {
			$fromCurrency = $strReceivingCurrency;
			$toCurrency = $strSendingCurrency;
		}
		//debug($strSendingCurrency);
		//debug($strReceivingCurrency);
		$queryUSDFrom = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateFrom = selectFrom($queryUSDFrom);
		$fromRate = $arrExchangeRateFrom["exchangeRate"];
		//Calculate margin of From for USD
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "'  AND  baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";

		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			if ($strSendingAmount != 0) {
				$fromRate = $fromRate + $fltMargin;
			} else {
				$fromRate = $fromRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$fromRate = $fromRate + $fltMargin;
			} else {
				$fromRate = $fromRate - $fltMargin;
			}
		}
		//Margin END From for USD

		$queryUSDTo = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateTo = selectFrom($queryUSDTo);

		$toRate = $arrExchangeRateTo["exchangeRate"];

		//Calculate margin of From for USD
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "' AND   baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		}
		//Margin END to for USD
		$fltConvertedAmnt1 = $fltAmountBase / $fromRate * $toRate;
		//USD END
		//start GBP
		$queryGBPFrom = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateFrom = selectFrom($queryGBPFrom);
		$fromRate = $arrExchangeRateFrom["exchangeRate"];

		//Calculate margin of From for GBP
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "'  AND   baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			$fromRate = $fromRate - $fltMargin;
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$fromRate = $fromRate + $fltMargin;
			} else {
				$fromRate = $fromRate - $fltMargin;
			}
		}
		//Margin END From for GBP

		$queryGBPTo = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateTo = selectFrom($queryGBPTo);

		$toRate = $arrExchangeRateTo["exchangeRate"];

		//Calculate margin of From for GBP
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "' AND  baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		}
		//Margin END to for GBP
		$fltConvertedAmnt2 = $fltAmountBase / $fromRate * $toRate;
		//GBP END

		//start EUR
		$queryEURFrom = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateFrom = selectFrom($queryEURFrom);
		$fromRate = $arrExchangeRateFrom["exchangeRate"];
		//Calculate margin of From for EUR
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "' AND  baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			if ($strSendingAmount != 0) {
				$fromRate = $fromRate + $fltMargin;
			} else {
				$fromRate = $fromRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}

			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$fromRate = $fromRate + $fltMargin;
				} else {
					$fromRate = $fromRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$fromRate = $fromRate + $fltMargin;
			} else {
				$fromRate = $fromRate - $fltMargin;
			}
		}
		//Margin END From for EUR

		$queryEURTo = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM " . TBL_ONLINE_EXCHANGE_RATE . " WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
		$arrExchangeRateTo = selectFrom($queryEURTo);

		$toRate = $arrExchangeRateTo["exchangeRate"];

		//Calculate margin of From for EUR
		$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE agentID='" . $agentIDResult['agentID'] . "' AND   baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
		$arrExchangeMargin = selectFrom($strQueryMargin);
		$strMarginType = $arrExchangeMargin['marginType'];
		$fltMargin = $arrExchangeMargin['margin'];
		if ($strMarginType == "FIXED") {
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		} elseif ($strMarginType == "PIPS") {
			if ($strQuotedAmountCurrency == "JPY") {
				$fltMargin = $fltMargin * 0.01;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			} else {
				$fltMargin = $fltMargin * 0.0001;
				if ($strSendingAmount != 0) {
					$toRate = $toRate + $fltMargin;
				} else {
					$toRate = $toRate - $fltMargin;
				}
			}
		} elseif ($strMarginType == "PERCENTAGE") {
			$fltMargin = $fltMargin * 0.01;
			if ($strSendingAmount != 0) {
				$toRate = $toRate + $fltMargin;
			} else {
				$toRate = $toRate - $fltMargin;
			}
		}
		//Margin END to for EUR
		$fltConvertedAmnt3 = $fltAmountBase / $fromRate * $toRate;
		//EUR END

		//debug($fltConvertedAmnt1);
		//debug($fltConvertedAmnt2);
		//debug($fltConvertedAmnt3);
		if ($fltConvertedAmnt1 != "" && $fltConvertedAmnt2 != "" && $fltConvertedAmnt3 != "") {
			$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2, $fltConvertedAmnt3));
		} elseif ($fltConvertedAmnt1 != "" && $fltConvertedAmnt2 != "") {
			$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2));
		} elseif ($fltConvertedAmnt1 != "" && $fltConvertedAmnt3 != "") {
			$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt3));
		} elseif ($fltConvertedAmnt2 != "" && $fltConvertedAmnt3 != "") {
			$fltConvertedAmnt = min(array($fltConvertedAmnt2, $fltConvertedAmnt3));
		} elseif ($fltConvertedAmnt1 != "") {
			$fltConvertedAmnt = $fltConvertedAmnt1;
		} elseif ($fltConvertedAmnt2 != "") {
			$fltConvertedAmnt = $fltConvertedAmnt2;
		} elseif ($fltConvertedAmnt3 != "") {
			$fltConvertedAmnt = $fltConvertedAmnt3;
		}
		if ($fltConvertedAmnt1 != "" || $fltConvertedAmnt2 != "" || $fltConvertedAmnt3 != "") {
			$arrExchangeRate = $fromRate;
			$fltExchangeRate = $toRate / $fromRate;
			$fltInverseRate = $fromRate / $toRate;
		}

	}

	if ($_REQUEST['sendingAmount'] != '') {
		$convertedCurrency = $_REQUEST['receivingCurrency'];
		if ($_REQUEST['sendingCurrency'] == 'GBP') {
			if ($_REQUEST['sendingAmount'] < 100 || $_REQUEST['sendingAmount'] > 50000) {
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['totalAmountInGBP'] = $strSendingAmount;
				//$_SESSION['exchangeRateError1'] = "Test Case6";
				$_SESSION['re'] = "y";
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
				header("LOCATION: make_payment-ii-new.php");
				exit();

			}

		} else {
			$sendingCurrency = $_REQUEST['sendingCurrency'];
			$sendingAmount = $_REQUEST['sendingAmount'];
			$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
			$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
			$totalAmountInGBP = $sendingAmount * $getExchangeRateResult[0]['inverseRate']; //exit;
			if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				//$_SESSION['exchangeRateError1'] = "Test Case5";
				$_SESSION['re'] = "y";
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
				header("LOCATION: make_payment-ii-new.php");
				exit();

			}

		}
	} else {
		$convertedCurrency = $_REQUEST['sendingCurrency'];
		if ($_REQUEST['receivingCurrency'] == 'GBP') {

			if ($_REQUEST['receivingAmount'] < 100 || $_REQUEST['receivingAmount'] > 50000) {
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
				//$_SESSION['exchangeRateError1'] = "Test Case4";
				$_SESSION['re'] = "y";
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;

				header("LOCATION: make_payment-ii-new.php");
				exit();

			}

		} else {
			$recCurrency = $_REQUEST['receivingCurrency'];
			$recAmount = $_REQUEST['receivingAmount'];
			$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
			$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
			$totalAmountInGBP = $recAmount * $getExchangeRateResult[0]['inverseRate'];
			if ($totalAmountInGBP < 100 || $totalAmountInGBP > 50000) {

				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				//$_SESSION['exchangeRateError1'] = "Test Case3";
				$_SESSION['re'] = "y";
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
				header("LOCATION: make_payment-ii-new.php");
				exit();

			}

		}

	}
	if ($convertedCurrency == 'GBP') {
		if ($fltConvertedAmnt < 100 && $fltConvertedAmnt > 50000) {
			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			//$_SESSION['exchangeRateError1'] = "Test Case2";
			$_SESSION['totalAmountInGBP'] = $strSendingAmount;
			$_SESSION['re'] = "y";
			$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
			header("LOCATION: make_payment-ii-new.php");
			exit();

		}
	} else {
		$getExchangeRateQuery = "select exchangeRate,inverseRate from " . TBL_ONLINE_EXCHANGE_RATE . " where baseCurrency='GBP' and quotedCurrency='$convertedCurrency' order by id DESC limit 1 ";
		$getExchangeRateResult = selectMultiRecords($getExchangeRateQuery);
		$totalAmountInGBP = $fltConvertedAmnt * $getExchangeRateResult[0]['inverseRate'];
		if ($totalAmountInGBP < 100 && $totalAmountInGBP > 50000) {

			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
			//$_SESSION['exchangeRateError1'] = "Test Case1";
			$_SESSION['re'] = "y";
			$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - " . $phone;
			header("LOCATION: make_payment-ii-new.php");
			exit();

		}

	}
	//ebug($strReceivingAmount);
	//debug($fltConvertedAmnt);
	//debug($strSendingAmount);
	//debug($strReceivingAmount);
	//$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
	////debug($strQueryRate,true);
	/* $strExchangeRate = number_format($arrExchangeRate['exchangeRate'], 2, ".", "");
		$strInverseRate = number_format($arrExchangeRate['inverseRate'], 2, ".", ""); */
	//$arrExchangeRate = selectFrom($strQueryRate);
	//$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
	//$arrExchangeMargin = selectFrom($strQueryMargin);
	//$strExchangeRate = $arrExchangeRate['exchangeRate'];
	//$strInverseRate = $arrExchangeRate['inverseRate'];
	//$strMarginType = $arrExchangeMargin['marginType'];
	//$fltMargin = $arrExchangeMargin['margin'];

	/*
		if($strMarginType == "PIPS")
		{
			$fltMargin = $fltMargin*0.0001;
			$strExchangeRate = $strExchangeRate-$fltMargin;

		}

		elseif($strMarginType == "PERCENTAGE")
		{
			$fltMargin = $fltMargin * 0.01;
			$strExchangeRate = $strExchangeRate-$fltMargin;

		}
		elseif($strMarginType == "FIXED")
		{
			$strExchangeRate = $strExchangeRate-$fltMargin;

		}
		*/
	////debug("test",true);
	//$strLowerLimit = number_format("0", 2, ".", "");
	if (empty($arrExchangeRate)) {
		$_SESSION['receivingAmount'] = $strReceivingAmount;
		$_SESSION['sendingAmount'] = $strSendingAmount;
		$_SESSION['sendingCurrency'] = $strSendingCurrency;
		$_SESSION['receivingCurrency'] = $strReceivingCurrency;
		$_SESSION['re'] = "N";
		$_SESSION['exchangeRateError'] = "We are sorry for the inconvenience, currently we are not dealing in this currency";
		header("LOCATION: make_payment-ii-new.php");
	}
} elseif (!empty($_REQUEST['goBack']) && $_REQUEST['goBack'] == 'Back') {
	$intBackFlag = true;
	$strReceivingAmount = $_SESSION['receivingAmount'];
	$strSendingAmount = $_SESSION['sendingAmount'];
	$strReceivingCurrency = $_SESSION['receivingCurrency'];
	$strSendingCurrency = $_SESSION['sendingCurrency'];
	$strExchangeRate = $_SESSION['exchangeRate'];
	$strInverseRate = $arrExchangeRate['inverseRate'];
} else {
	header("LOCATION: make_payment-ii-new.php");
}

$url = $_SERVER['REQUEST_URI'];
$url = explode('/', $url);
$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];

activities($_SESSION["loggedInUser"]["userID"], "Get Rate", "", "", "Got Rate for " . $strSendingCurrency . " - " . $strReceivingCurrency . " = " . $fltExchangeRate . "");
?>
<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
	<meta charset="utf-8">
	<title>Top Up Card - <?= $title; ?> Online Currency Transfers</title>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php include 'templates/script_header.php';?>

	<?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
	<header id="header" role="banner">
		<?php include 'templates/header.php'?>
	</header>
	<!-- End #header -->

	<div id="content" class="pb0" role="main" style="padding-bottom:0;">
		<div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
			<div class="container" data-0="opacity:1;" data-top="opacity:0;">
				<div class="row">
					<div class="col-md-6">
						<h1>Send Money</h1>
						<p class="page-header-desc">Make payment</p>
					</div><!-- End .col-md-6 -->
					<div class="col-md-6">
						<ol class="breadcrumb">
							<li><a href="#">Home</a></li>
							<li class="active">Transactions</li>
						</ol>
					</div><!-- End .col-md-6 -->
				</div><!-- End .row -->
			</div><!-- End .container -->
		</div><!-- End .page-header -->

		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="form-wrapper">
						<h2 class="title-underblock custom mb30">Spot quote</h2>
						<p style="font-size:18px;">The spot quote rate is refreshed every minute, this quote will be updated in
							<strong><span id="tmin" class="timer">0 : <span id="countdown">34</span></span> s</strong>
						</p>

						<form id="processTrans" action="beneficiariesnew.php" method="post">
							<!-- ROW -->
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="fromDate" class="input-desc mt10">Current rate:</label>
										<input type='hidden' name='url' value="<?= $url[2]; ?>">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div id="numfo" class="form-control"><?php echo number_format($fltExchangeRate, 4, ".", ","); ?></div>
										<input name="exchangeRate" type="hidden" value="<?php echo $fltExchangeRate; ?>"/>
										<input name="inverseRate" type="hidden" value="<?php echo $fltInverseRate; ?>"/>
									</div>
								</div>
							</div>
							<!-- ROW -->
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="fromDate" class="input-desc mt10">You pay:</label>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div id="youpayind" class="form-control"><?php
											echo $strSendingCurrency . " ";
											$_SESSION["strSendingCurrency"] = "";
											$_SESSION["strSendingCurrency"] = $strSendingCurrency;
											if ($strSendingAmount != 0) {
												echo number_format($strSendingAmount, 2, ".", ",");
											} else {
												echo number_format($fltConvertedAmnt, 2, ".", ",");
											}
											?></div>
										<input name="sendingAmount" type="hidden" value="<?php if ($strSendingAmount != 0) {
											echo $strSendingAmount;
											$_SESSION["strSendingAmount"] = "";
											$_SESSION["strSendingAmount"] = $strSendingAmount;
										} else {
											echo $fltConvertedAmnt;
											$_SESSION["fltConvertedAmnt"] = "";
											$_SESSION["fltConvertedAmnt"] = $fltConvertedAmnt;
										}
										?>"/>
										<input name="sendingCurrency" type="hidden" value="<?php echo $strSendingCurrency; ?>"/>
									</div>
								</div>
							</div>
							<!-- ROW -->
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label for="fromDate" class="input-desc mt10">You receive:</label>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<div id="youbuyd" class="form-control"><?php
											echo $strReceivingCurrency . " ";
											$_SESSION["strReceivingCurrency"] = "";
											$_SESSION["strReceivingCurrency"] = $strReceivingCurrency;
											if ($strReceivingAmount != 0) {
												echo number_format($strReceivingAmount, 2, ".", ",");
											} else {
												echo number_format($fltConvertedAmnt, 2, ".", ",");
											}
											?></div>
										<input name="receivingAmount" type="hidden" value="<?php if ($strReceivingAmount != 0) {
											echo $strReceivingAmount;
											$_SESSION["strReceivingAmount"] = "";
											$_SESSION["strReceivingAmount"] = $strReceivingAmount;
										} else {
											echo $fltConvertedAmnt;
											$_SESSION["fltConvertedAmnt"] = "";
											$_SESSION["fltConvertedAmnt"] = $fltConvertedAmnt;
										}
										?>"/>
										<input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency;
										?>"/>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-sm-12">
									<div class="form-group mb5">
										<input name="reject" id="reject" type="button" class="btn btn-default" value="Go back" onClick="window.location.href='make_payment-ii-new.php?reject&sendingCurrency=<?= $strSendingCurrency ?>&receivingCurrency=<?= $strReceivingCurrency ?>&fltExchangeRate=<?= $fltExchangeRate ?>'">
										<input id="changeTransaction" name="changeTransaction" type="hidden" value="changeTransaction"/>
										<input name="changeTrans" id="changeTrans" type="button" class="btn btn-custom" value="CHANGE QUOTE" onClick="this.form.action='make_payment_change_quote.php';this.form.submit();">
										<input name="accept" id="accept" type="submit" class="btn btn-custom" value="ACCEPT QUOTE">
									</div>
								</div>
							</div>

							<p class="mt20 mb30">For amounts over £50,000 or if you have any other questions, please contact us <?= $email; ?></p>
						</form>
					</div><!-- End .form-wrapper -->
				</div><!-- End .col-sm-6 -->
				<div class="mb40 visible-xs"></div><!-- space -->
				<div class="col-sm-4">
					<h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
					<p>Send money safely and securely with <?= $title; ?> Online.</p>
					<div class="mb10"></div><!-- space -->
					<a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
				</div><!-- End .col-sm-6 -->
			</div><!-- End .row -->


		</div><!-- End .container -->

		<div class="mb40 mb20-xs"></div><!-- space -->
		<footer id="footer" class="footer-inverse" role="contentinfo">
			<?php include 'templates/footer.php'; ?>
		</footer>
		<!-- End #footer -->

	</div>
	<!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
	<i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/make-paymentnew.js"></script>
</body>
</html>

