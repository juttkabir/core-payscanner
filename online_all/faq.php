<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: FAQs page
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
?>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
	<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
		<link href="css/stylenew.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
        	<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
        <script type="text/javascript" src="../admin/javascript/jquery.maskedinput.min.js"></script>
	</head>
	<body>
				<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>
				</div>
				<div id="body">
					<div id="main">
						<div id="page">
							<div id="cntnt">
								<div id="hdr">
				<div id="login">
					<a href="#nl"><img src="images/shim.gif" style="height:33px; width:140px;"></a>
				
				<ul>
					<li><a href="http://www.premfx.com/index.php" target="_parent">Home</a></li>
					<li><a href="http://www.premfx.com/news.php" target="_parent">News</a></li>
					<li><a href="http://www.premfx.com/testimonials.php" target="_parent">Testimonials</a></li>
					<li><a href="http://www.premfx.com/casestudies.php" target="_parent">Case studies</a></li>
					<li><a href="http://www.premfx.com/faqs.php" target="_parent">FAQs</a></li>
                    <li><a href="http://www.premfx.com" target="_parent"><img src="images/pfx-online-icon.png"></a></li>
				</ul>
				</div>
			</div>
            <div id="logo_nav">
				<img id="logo" src="images/ui/logo_def.png">
				<ul id="nav">
					<li class="l"></li>
					<li class="btn">
						<div>
							<a href="http://www.premfx.com/index.php" target="_parent"><img src="images/ui/btn_home.gif"></a>
						</div>
					</li><li><a href="make_payment-ii-new.php" target="_parent">Send Money</a>
						
					<li><a href="transaction_history_new.php" target="_parent">Transaction History</a>
						
					<li><a href="view_beneficiariesnew.php" target="_parent">Beneficiaries</a><ul id="sub_nav">
							<div></div>
							
						</ul></li>
					<li><a href="change_passwordnew.php" target="_parent">Change Password</a>
                    <li><a href="contact_us.php" target="_parent">CONTACT US</a>
						
					<li><a href="faqs.php" target="_parent">FAQs</a><ul id="sub_nav">
							<div></div>
							
						</ul></li>
					<li class="r"></li>
				</ul>
			</div>	
	
			<div id="container"  class="body_text l">
				<div id="content_one">
					<div class="body_text l">
						<div class="banner_cont">
							<div class="banner">
								<div style="margin:0px 0px 30px 0px; width:680px;">
									   
									      <div style="width:800px; margin-left:30px">
									        <div id="box" class="l">
									       
										<div >
									<h1>Frequently Asked Questions</h1>
									<h3 class="gry">Answers to questions our customers often ask us. If the answer to your question is not here email or call us and we'll be happy to help you.</h3>
								
							
						
					  <div style="width:440px;" class="l">
						  <h1 id="cont_head">FAQs</h1>
						  <ul>
								<li><a id="l1" href="#1">How does Premier FX save me money?</a></li>
								<li><a id="l2" href="#2">What costs / fees can I expect to pay?</a></li>
								<li><a id="l3" href="#3">How do I know my money is safe?</a></li>
								<li><a id="l4" href="#4">How long do you take to transfer funds?</a></li>
								<li><a id="l5" href="#5">Why shouldn't I just use my bank?</a></li>
								<li><a id="l6" href="#6">What happens if I am unable to pay for my trade on the day it falls due?</a></li>
								<li><a id="l7" href="#7">What is a foreign exchange company?</a></li>
								<li><a id="l8" href="#8">How does Premier FX make its money if there are no fees or commissions?</a></li>
						  </ul>
						  <a name="1"></a>
						  <hr>
							<h1 id="h1">How does Premier FX save me money?</h1>
							<p id="p1">As a commercial client of Barclays Bank we transact a high volume of deals. This means we can access very competitive exchange rates from the foreign exchange markets. Coupled with our low overheads this enables us to pass on great savings to our clients. We do not charge our clients for transfers.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="2"></a>
							<p>&nbsp;</p>
							<h1 id="h2">What costs / fees can I expect to pay?</h1>
							<p id="p2">Premier FX do not charge any fees for any of their services, so if you are worried that somewhere down the process you will be hit with a bill, dont be! Our account opening is free of charge, as are our transfers and even the one-to-one market trading advice given by our Account Executives. If you transfer your funds to our client account via on-line banking, in the majority of cases you will find the whole process completely free from charges. </p>
							<p ><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="3"></a>
							<p>&nbsp;</p>
							<h1 id="h3">How do I know my money is safe?</h1>
							<p id="p3">Premier FX hold secure clients accounts with Barclays Bank plc, London, which are totally separate from our day-to-day business accounts. These accounts are very similar to how Lawyer / Solicitor clients accounts work. We are also registered with HM Customs &amp; Excise, registration number 1223621400000.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="4"></a>
							<p>&nbsp;</p>
							<h1 id="h4">How long do you take to transfer funds?</h1>
							<p id="p4">As soon as we receive your payment we transfer your currency straight away. It should take no longer than 48 hours to reach the designated bank account anywhere in the world. In most cases it is even quicker.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="5"></a>
							<p>&nbsp;</p>
							<h1 id="h5">Why shouldn't I just use my bank?</h1>
							<p id="p5">Banks are usually 4 - 8 % more expensive on their exchange rates, sometimes a little less, often a lot more!  They do not have the expertise on a local level to provide any guidance on Currency Exchange nor do they monitor the markets. We have our rates in front of us when you talk to us and we make sure we are up to date with the latest market movements. </p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="6"></a>
							<p>&nbsp;</p>
							<h1 id="h6">What happens if I am unable to pay for my trade on the day it falls due?</h1>
							<p id="p6">Contact us immediately and your Account Manager can help you in this situation.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="7"></a>
							<p>&nbsp;</p>
							<h1 id="h7">What is a foreign exchange company?</h1>
							<p id="p7">A foreign exchange company is a non-bank organisation that services the currency needs of both private and corporate clients. The company's role is to achieve the best currency exchange rates for their clients and to be as cost effective as possible. The company is able to source the currency direct from the foreign exchange (FX) market and therefore pass savings onto their clients.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<a name="8"></a>
							<p>&nbsp;</p>
							<h1 id="h8">How does Premier FX make its money if there are no fees or commissions?</h1>
							<p id="p8">We make a small profit between the price we sell foreign currency to our customers at and the price we pay for the currency on the international market.</p>
							<p><strong class="ylw"><a href="#top">Back to top</a></strong></p>
							<p>&nbsp;</p>
						</div>
						<div style="width:245px;" class="r">
							<div class="r" id="box_ylw">
								<div class="top"></div>
								<div class="cntnt">
									<h1 style="margin:0px 0px 10px 0px;" class="wht nm">Service summary</h1>
									<ul class="wht">
										<li>Competitive exchange rates (superior to banks)</li>
										<li>No minimum amounts, fees or commissions</li>
										<li>A dedicated account manager to give you timely advice</li>
										<li>Fast, free money transfers</li>
										<li>Fixed-rate, forward contracts up to 2 years</li>
									</ul>
								</div>
								<div class="btm"></div>
							</div>
							<div class="r" id="box_ylw">
								<div class="top"></div>
								<div class="cntnt">
									<h2 style="font-size:14px; line-height:20px; margin:0px 0px 10px 0px;" class="wht nm"><strong>Other services we provide to private individuals:</strong></h2>
									<ul class="wht">
										<li><a href="private_regpayment.php">Regular payment plans</a></li>
										<li><a href="private_emigration.php">Emigration</a></li>
									</ul>
								</div>
								<div class="btm"></div>
							</div>
							<div class="r" id="box_ylw">
								<div class="top"></div>
								<div class="cntnt">
									<h2 style="font-size:14px; line-height:20px; margin:0px 0px 10px 0px;" class="wht nm"><strong>Types of contract we offer for currency purchase</strong></h2>
									<ul class="wht">
										<li><a href="private_typeofcontract.php">Spot contract</a></li>
										<li><a href="private_typeofcontract.php">Forward contract</a></li>
										<li><a href="private_typeofcontract.php">Limit order</a></li>
										<li><a href="private_typeofcontract.php">Stop loss</a></li>
									</ul>
									<p class="wht">
									We always send you a deal contract by e-mail, fax or post. There are different <a href="private_typeofcontract.php"><strong>types of contract</strong></a> to suit your needs.										</p>
								</div>
								<div class="btm"></div>
							</div>
						</div>
						<div style="margin:0px 0px 40px 0px; width:680px;" class="l">
							<hr>
							<img style="margin:8px -10px 0px 20px;" class="r" src="images/faq/ico_phone_r.png">
							<h1 style="line-height:25px; margin:0px 0px 10px 0px;">Talk to one of our advisers today to find out how we can help<br>
							your business save time and money:</h1>
							<div>
								<h3 style="line-height:25px;" class="gry">please email us on <a class="ylw" href="mailto:info@premfx.com"><strong>info@premfx.com</strong></a> or call<br>
								<strong class="ylw">UK 0845 021 2370</strong> / <strong class="ylw">Portugal +351 289 358 511</strong> / <strong class="ylw">Spain +34 971 576 724</strong></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<script type="text/javascript">
			function changeMySize(myvalue,iden)
// this function is called by the user clicking on a text size choice
{
if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
	var ch = document.getElementById("cont_head");
	var l1 = document.getElementById("l1");
	var l2 = document.getElementById("l2");
	var l3 = document.getElementById("l3");
	var l4 = document.getElementById("l4");
	var l5 = document.getElementById("l5");
	var l6 = document.getElementById("l6");
	var l7 = document.getElementById("l7");
	var l8 = document.getElementById("l8");
	var h1 = document.getElementById("h1");
	var h2 = document.getElementById("h2");
	var h3 = document.getElementById("h3");
	var h4 = document.getElementById("h4");
	var h5 = document.getElementById("h5");
	var h6 = document.getElementById("h6");
	var h7 = document.getElementById("h7");
	var h8 = document.getElementById("h8");
	var p1 = document.getElementById("p1");
	var p2 = document.getElementById("p2");
	var p3 = document.getElementById("p3");
	var p4 = document.getElementById("p4");
	var p5 = document.getElementById("p5");
	var p6 = document.getElementById("p6");
	var p7 = document.getElementById("p7");
	var p8 = document.getElementById("p8");
	
	ch.style.fontSize = myvalue + "px";
	l1.style.fontSize = myvalue + "px";
	l2.style.fontSize = myvalue + "px";
	l3.style.fontSize = myvalue + "px";
	l4.style.fontSize = myvalue + "px";
	l5.style.fontSize = myvalue + "px";
	
	l6.style.fontSize = myvalue + "px";
	l7.style.fontSize = myvalue + "px";
	l8.style.fontSize = myvalue + "px";
	h1.style.fontSize = myvalue + "px";
	h2.style.fontSize = myvalue + "px";
	h3.style.fontSize = myvalue + "px";
	
	h4.style.fontSize = myvalue + "px";
	h5.style.fontSize = myvalue + "px";
	h6.style.fontSize = myvalue + "px";
	h7.style.fontSize = myvalue + "px";
	
	h8.style.fontSize = myvalue + "px";
	p1.style.fontSize = myvalue + "px";
	p2.style.fontSize = myvalue + "px";
	p3.style.fontSize = myvalue + "px";
	p4.style.fontSize = myvalue + "px";
	p5.style.fontSize = myvalue + "px";
	p6.style.fontSize = myvalue + "px";
	p7.style.fontSize = myvalue + "px";
	p8.style.fontSize = myvalue + "px";
	p9.style.fontSize = myvalue + "px";
}
		
		</script>
	</body>
</html>