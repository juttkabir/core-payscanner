<?php
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
    header("LOCATION: logout.php");
}

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

require_once '../src/bootstrap.php';

$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];
$customerID = $_SESSION['loggedInUser']['userID'];;

$customer = selectFrom("SELECT Title as title, firstName, lastName, gender, dob, Phone as phone, Mobile as mobile, email, Zip as zip, Country as country, id_number as IDNumber, countryOfIssue, issue_date as issueDate, expiry_date as expiryDate FROM customer, user_id_types WHERE customer.customerID = user_id_types.user_id AND customer.customerID = $customerID");
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Profile - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>
    <?php include 'templates/_ga.inc.php'; ?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'profile'; ?>
<!--        --><?php //include 'templates/header.php'; ?>
    </header>
    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Profile</h1>
                        <p class="page-header-desc">Profile details</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Profile</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <h1 class="logo text-center mt30">
                        <a href="http://payscanner.com" title="PAYscanner">
                            <img src="./assets_online/images/logo.png" alt="PAYscanner">
                        </a>
                    </h1>
                    <div class="form-wrapper">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2 class="h4 title-underblock custom mb30">REMITM</h2>
                            </div>
                            <div class="col-sm-4">
                                <div class="text-right"><a href="#link_here" class="btn btn-custom btn-sm btn-border no-radius">Edit my profile</a></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Personal informations</small></span></h2>
                                <div class="row">

                                    <div class="col-sm-5">
                                        <strong>Title</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["title"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Gender</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["gender"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Firstname</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["firstName"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Lastname</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["lastName"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Gender</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["gender"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Birthdate</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["dob"]; ?>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="col-sm-6">
                                <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Address</small></span></h2>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <strong >Building Number:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            here buiding number
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Street:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            here street text
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Town</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            town text here
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Region</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            region text here
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Postal Code</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["zip"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Country</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["country"]; ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="mb20"></div><!-- space -->


                            <div class="col-sm-6">
                                <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Contact informations</small></span></h2>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <strong>Landline:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["phone"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Mobile:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["mobile"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Email</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["email"]; ?>
                                        </div>
                                    </div>
                                </div>
                                <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Profile Picture</small></span></h2>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <img src="assets_online/images/user_img.jpg" alt="User Profile Picture" class="img-responsive mt15 mb15 pull-left" style="border:1px solid #009688" width="200">
                                    </div>
                                </div>

                            </div>
                            <div class="mb20"></div><!-- space -->


                            <div class="col-sm-6">
                                <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Passport Details</small></span></h2>
                                <div class="row">

                                    <div class="col-sm-5">
                                        <strong>Passport number:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["IDNumber"]; ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <strong>Country of issue:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["countryOfIssue"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Issue date:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["issueDate"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Expiry date:</strong>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <?php echo $customer["expiryDate"]; ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <strong>Passport:</strong>
                                    </div>
                                    <div class="col-sm-12">
                                        <img src="assets_online/images/uk_passport@2x.jpg" alt="UK Specimin Passport" class="img-responsive mt15 mb15 pull-left" width="342">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="mb20"></div><!-- space -->


                    </div><!-- End .form-wrapper -->
                    <div class="mb20"></div><!-- space -->
                    <div class="mb20"></div><!-- space -->
                </div>
            </div><!-- End .row -->

        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>
</html>
