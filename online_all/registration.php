<?php
/**
 * Created by PhpStorm.
 * Date: 26.03.19
 * Time: 1:02
 */

require_once '../src/bootstrap.php';

$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$logoHtml = $agentUiData['logoHtml'];

$customerService = new \Payex\Online\Service\CustomerService($online->getDb());
$registrationHelper = new \Payex\Online\Helper\RegistrationHelper();
$countryList = $customerService->getCountryListForRegistrationPage();
$countryOptionList = $registrationHelper->getCountryOptionListForRegistrationOnlinePage($countryList);

if (isset($_GET['type'])) {
    $register_type = base64_decode(urldecode($_GET['type']));
} else {
    $register_type = "";
}

if (isset($_GET['cur_type'])) {
    $cust_type = $_GET['cur_type'];
} else {
    $cust_type = "USD";
}
include_once 'includes/configs.php';

?>

<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Private Client Register - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/styles00.min.css">
    <!-- Favicon and Apple Icons -->
    <link rel="icon" type="image/png" href="./assets/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="./assets/images/icons/faviconx57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./assets/images/icons/faviconx72.png">
    <script src="./assets/js/scriptjs.min.js"></script>
    <style>
        .normal {
            font-weight: normal !important;
        }
    </style>
    <style type="text/css">
        .passport-filedroppable {
            background: #e6e6e6;
            color: #000;
            /* padding: 28px 0; */
            text-align: center;
            line-height: 88px;
            height: 88px;
            border: dashed 2px #343434;
            margin-bottom: 20px;
            border-radius: 4px;
        }

        .passport-filedroppable.dragover {
            background: #009688;
            color: #fff;
        }

        .file_preview {
            border: solid 5px #d3d3d3;
            padding: 10px;
            text-align: center;
        }
    </style>
    <link rel="stylesheet" href="./assets/css/bootstrap-datetimepicker.min.css">
    <!-- Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131811966-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-131811966-3');
    </script>
</head>
<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="login-section" class="fu llheight">
    <div class="vcen ter-container">
        <div class="vcent er">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="logo text-center mt30">
                            <?= $logoHtml; ?>
                        </h1>
                        <div class="form-wrapper">
                            <h2 class="h4 title-underblock custom mb30">Open an account: Private client</h2>
                            <p>Please complete the form below and upload one form of identification to comply with UK
                                money laundering regulations, we require:</p>
                            <form name="senderRegistrationForm" id="senderRegistrationForm" action="" method="post">
                                <input type="hidden" value="" id="register_type" name="register_type"/>
                                <input type="hidden" name="ipAddress" value="95.210.228.178"/>
                                <input type="hidden" name="initial_load" id="initial_load" class="form_fields"
                                       value="0.00" style="text-transform:uppercase"/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Personal informations</small></span>
                                        </h2>
                                    </div>
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="title" class="input-desc">Title</label>
                                                    <select name="title" id="title" class="form-control">
                                                        <option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs</option>
                                                        <option value="Miss">Miss.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>
                                                        <option value="Eng">Eng.</option>
                                                        <option value="Herr">Herr</option>
                                                        <option value="Frau">Frau</option>
                                                        <option value="Ing">Ing.</option>
                                                        <option value="Mag">Mag.</option>
                                                        <option value="Sr">Sr.</option>
                                                        <option value="Sra">Sra.</option>
                                                        <option value="Srta">Srta.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <label for="forename" class="input-desc">Forename</label>
                                                    <input type="text" class="form-control" id="forename"
                                                           name="forename"
                                                           placeholder="Your Forename" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="surname" class="input-desc">Surname</label>
                                                    <input type="text" class="form-control" id="surname" name="surname"
                                                           placeholder="Company Surname" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="email" class="input-desc">Email</label>
                                                    <input type="email" class="form-control" id="email" name="email"
                                                           placeholder="Your Email" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="landline" class="input-desc">Telephone -
                                                        Landline</label>
                                                    <input type="text" class="form-control" id="landline"
                                                           name="landline"
                                                           placeholder="Your Landline phone number" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="mobile" class="input-desc">Telephone - Mobile</label>
                                                    <input type="text" class="form-control" id="mobile" name="mobile"
                                                           placeholder="Your Mobile phone number" required>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="dob" class="input-desc">Birth date</label>
                                                    <div class="input-group date form-date" data-date=""
                                                         data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                                         data-link-format="dd-mm-yyyy">
                                                        <input class="form-control" id="dob" name="dob" size="16"
                                                               type="text" value=""
                                                               readonly="">
                                                        <span class="input-group-addon"><span
                                                                    class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for="birthCountry" class="input-desc">Country of
                                                        Birth</label>
                                                    <select name="birthCountry" class="form-control" id="birthCountry">
                                                        <?php echo $countryOptionList; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div><!-- End .row -->
                                        <div class="row">

                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="gender" class="input-desc">Gender</label>
                                                    <select name="gender" id="gender" class="form-control">
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Automatic Address Lookup</small></span>
                                        </h2>
                                    </div>
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="surname" class="input-desc">Post code:</label>
                                            <input type="text" class="form-control" id="surname" name="surname"
                                                   placeholder="Company Surname" required>
                                        </div>

                                        <div class="form-group mb5">
                                            <button type="button" class="btn btn-custom">Search for address</button>
                                        </div>
                                    </div>
                                </div><!-- End .row -->


                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Your Address</small></span>
                                        </h2>
                                    </div>
                                </div><!-- End .row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="buildingNumber" class="input-desc">Building Number:</label>
                                            <input type="text" class="form-control" id="buildingNumber"
                                                   name="buildingNumber"
                                                   placeholder="Building Number" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street" class="input-desc">Street:</label>
                                            <input type="text" class="form-control" id="street" name="street"
                                                   placeholder="Street" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="town" class="input-desc">Town:</label>
                                            <input type="text" class="form-control" id="town" name="town"
                                                   placeholder="Town" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="region" class="input-desc">Region:</label>
                                            <input type="text" class="form-control" id="region" name="region"
                                                   placeholder="Region" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="postcode" class="input-desc">Postal Code</label>
                                            <input type="text" class="form-control" id="postcode" name="postcode"
                                                   placeholder="Company Post Code" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="country" class="input-desc">Country</label>
                                            <select name="residenceCountry" class="form-control" id="residenceCountry">
                                                <?php echo $countryOptionList; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- End .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Passport Details</small></span>
                                        </h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportCountry" class="input-desc">Country of issue</label>
                                            <select name="passportCountry" id="passportCountry" class="form-control">
                                                <option value="" selected='selected'>-- Select Country of Issue --
                                                </option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="USA">USA</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Canada">Canada</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="passportIssue" class="input-desc">Issue date</label>
                                            <div class="input-group date form-date" data-date=""
                                                 data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                                 data-link-format="dd-mm-yyyy">
                                                <input class="form-control" id="passportIssue" name="passportIssue"
                                                       size="16" type="text" value="" readonly>
                                                <span class="input-group-addon"><span
                                                            class="fa fa-calendar"></span></span>
                                            </div><!-- End .input-group -->
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="passportexpirydate" class="input-desc">Expiry date</label>
                                            <div class="input-group date form-date" data-date=""
                                                 data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                                 data-link-format="dd-mm-yyyy">
                                                <input class="form-control" id="passportexpirydate"
                                                       name="passportexpirydate"
                                                       size="16" type="text" value="" readonly>
                                                <span class="input-group-addon"><span
                                                            class="fa fa-calendar"></span></span>
                                            </div><!-- End .input-group -->
                                        </div>
                                    </div>
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportupload" class="input-desc">Passport Upload</label>
                                            <div class="passport-filedroppable">
                                                <div>Drag file here or click to upload</div>
                                            </div>
                                            <div class="output"></div>

                                            <span class="help-block mt10"><strong>Hint:</strong>
                                                        <small>Please
                                                            upload a scanned copy of your passport, the maximum file
                                                            size is 2Mb.<br>If you are having difficulty uploading the
                                                            file please post a copy or e-mail us at
                                                            <?= $email; ?> as soon as possible.
                                                        </small>
                                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportNumber" class="input-desc">Passport Number</label>
                                            <input type="text" class="form-control" id="passportNumber"
                                                   name="passportNumber"
                                                   placeholder="Passport Number" required>
                                            <span class="help-block mt10"><strong>Hint:</strong> <small>Please
                                                            enter your passport number as highlighted in the picture</small></span>
                                            <img src="./assets/images/uk_passport.jpg"
                                                 alt="UK Specimin Passport" class="img-responsive mt15 mb15 pull-right">
                                        </div>
                                    </div>

                                </div><!-- End .row -->

                                <div class="mb20"></div><!-- space -->
                                <div class="form-group mb5">
                                    <button type="button" class="btn btn-custom">Register Now</button>
                                </div>
                            </form>
                        </div><!-- End .form-wrapper -->
                        <div class="mb20"></div><!-- space -->
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="copyright text-center">
                                    <?= $title; ?> Online is a registered trademark of <?= $title; ?> Ltd.<br>
                                    Copyright (c) 2019 <?= $title; ?> Ltd. All rights reserved.
                                    <a href="<?= $online->getBaseUrl(); ?>"><?= $currentUrl; ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="mb20"></div><!-- space -->
                    </div>
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .vcenter -->
    </div><!-- End .vcenter-container -->
</div><!-- End .fullheight -->
<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="./javascript/jquery.js"></script>
<script src="./assets/js/registration/jquery.mask.min.js"></script>
<script src="./assets/js/registration/registration.js"></script>
<script src="./assets/js/script00.min.js"></script>
<script src="./assets/js/bootstrap-datetimepicker.js"></script>

<script type="text/javascript">
    $(function () {
        $('.form-datetime').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    });
    $(function () {
        $('.form-date').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    });
    $(function () {
        $('.form-time').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0
        });
    });


</script>
<script type="text/javascript">
    (function (window) {
        function triggerCallback(e, callback) {
            if (!callback || typeof callback !== 'function') {
                return;
            }
            var files;
            if (e.dataTransfer) {
                files = e.dataTransfer.files;
            } else if (e.target) {
                files = e.target.files;
            }
            callback.call(null, files);
        }

        function makeDroppable(ele, callback) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            //input.setAttribute('multiple', false);
            input.setAttribute('class', 'form-control');
            input.setAttribute('id', 'passportupload');
            input.setAttribute('name', 'passportupload');
            input.setAttribute('required', 'required');
            input.style.display = 'none';
            input.addEventListener('change', function (e) {
                triggerCallback(e, callback);
            });
            ele.appendChild(input);

            ele.addEventListener('dragover', function (e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.add('dragover');
            });

            ele.addEventListener('dragleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.remove('dragover');
            });

            ele.addEventListener('drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.remove('dragover');
                triggerCallback(e, callback);
            });

            ele.addEventListener('click', function () {
                input.value = null;
                input.click();
            });
        }

        window.makeDroppable = makeDroppable;
    })(this);
    (function (window) {
        makeDroppable(window.document.querySelector('.passport-filedroppable'), function (files) {
            console.log(files);
            var output = document.querySelector('.output');
            output.innerHTML = '';
            for (var i = 0; i < files.length; i++) {
                if (files[i].type.indexOf('image/') === 0) {
                    output.innerHTML += '<img width="200" class="file_preview" src="' + URL.createObjectURL(files[i]) + '" />';
                }
                output.innerHTML += '<p>' + files[i].name + '</p>';
            }
        });
    })(this);
</script>
</body>
</html>
