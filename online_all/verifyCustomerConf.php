<?php
    use Payex\Model\LoginHistory;
    use Payex\Online\Service\CustomerService;
    use Payex\Repository\CustomerRepository;
    use Payex\Repository\LoginHistoryRepository;

    include_once "includes/configs.php";
    include_once "includes/database_connection.php";
    dbConnect();
    include_once "includes/functions.php";

    $OTPNumber = $_POST["OTPNumber"];

    $querCust = "SELECT OTPNumber FROM " . TBL_CUSTOMER . " WHERE customerID=" . $_GET['customerID'];
    $getCust = selectFrom($querCust);

    if (isset($_GET["from"]) && $_GET["from"] == 'registration') {
        if ($OTPNumber == $getCust["OTPNumber"])
            echo "Mobile verified";
        else
            echo "Invalid OTP number";
    } else if (isset($_GET["from"]) && $_GET["from"] == 'index') {
        if ($OTPNumber == $getCust["OTPNumber"]) {
            ////////////////////////////////////////////////////////////////

//            ini_set("display_errors","On");
            require_once '../src/bootstrap.php';
            $currentUrl = $_SERVER['HTTP_HOST'];
            $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl);
            $title = $agentUiData['title'];
            $logoHtml = $agentUiData['logoHtml'];
            $userIp = $online->getLoginHelper()->getUserIpAddr();

            /* Checking Get params for distinguish customer type */
            $type = $_GET["type"];

            if (empty($type)) {
                $type = $_POST["type"];
            }

            $currentType = base64_decode($type);

            if (empty($currentType)) {
                $currentType = 'TC';
            }
            /* End of checking Get params for distinguish customer type */

            $customerEmail = trim($_POST['customerEmail']);
            $password = $_POST['password'];
            echo $customerEmail.$password;
            if (isset($customerEmail) && isset($password)){
                /* Start session logic */
                $lifetime = 60000;
                session_start();
                setcookie(session_name(), session_id(), time() + $lifetime);
                /* End session logic */
                include_once "includes/functions.php";
                $customerRepository = new CustomerRepository($online->getDb());
                $customer = $customerRepository->getByCustomerEmail($customerEmail);
                $errorMessage = '';
                if ($customer){
                    $customerService = new CustomerService($online->getDb());
                    if ($customerService->loginCustomer($customer, $customerEmail, $password)){
                        if ($customerService->checkCustomerIdTypeExpireDate($customer)){
                            if ($agentUiData['agentId'] && $customer->getAgentId() == $agentUiData['agentId']){
                                $checkType = $customerService->checkCustomerServiceType($customer, $currentType);

                                $_SESSION['loggedInUser']['userID'] = $customer->getId();
                                $_SESSION['loggedInUser']['agentID'] = $customer->getAgentId();
                                $_SESSION['loggedInUser']['email'] = $customer->getEmail();
                                $_SESSION['loggedInUser']['accountName'] = $customer->getAccountName();
                                $_SESSION['loggedInUser']['online_customer_services'] = $customer->getOnlineCustomerServices();
                                $_SESSION['loggedInUser']['card_issue_currency'] = $customer->getCardIssueCurrency();

                                $loginHistory = new LoginHistory();
                                $loginHistory->setLoginTime(getCountryTime(CONFIG_COUNTRY_CODE));
                                $loginHistory->setLoginName($customer->getFirstName());
                                $loginHistory->setAccessIp($userIp);
                                $loginHistory->setLoginType('customer');
                                $loginHistory->setCustId($customer->getId());
                                $loginHistoryRepository = new LoginHistoryRepository($online->getDb());
                                $insertId = $loginHistoryRepository->save($loginHistory);

                                session_register("loginHistoryID");
                                $_SESSION["loginHistoryID"] = $insertId;

                                if (isset($checkType['redirectPage']) && !isset($checkType['error'])){
                                    $_SESSION['loggedInUser']['agentID'] = $customer->getAgentId();
            //                        header("LOCATION: " . $checkType['redirectPage']);
                                }else{
                                    $errorMessage = $checkType['error'];
                                }
                            }else{
                                $errorMessage = 'Some of data used is invalid.';
                            }
                        }else{
                            $errorMessage = 'Your Passport expiry is less than to date. Please contact administrator.';
                        }
                    }else{
                        $errorMessage = 'Customer email or password is not valid or customer is not active';
                    }
                }else{
                    $errorMessage = 'Customer email or password is not valid or customer is not active';
                }
            }
            ////////////////////////////////////////////////////////////////
            if ($_GET["type"] == 'PC')
                header("LOCATION:  topup.php");
            else
                header("LOCATION:  make_payment-ii-new.php");
        } else
            header("LOCATION:  index.php");
    } else
        header("LOCATION:  index.php");
?>