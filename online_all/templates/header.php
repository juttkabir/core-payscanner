<?php
/**
 * Created by PhpStorm.
 * Date: 02.04.19
 * Time: 0:26
 */

use Payex\Repository\CustomerRepository;

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$address = $agentUiData['address'];
$logoUrls = $agentUiData['logoUrls'];

$customerRepository = new CustomerRepository($online->getDb());
$customer = $customerRepository->getById($_SESSION['loggedInUser']['userID']);

?>
<div class="collapse navbar-white special-for-mobile" id="header-search-form">
    <div class="container">
        <form class="navbar-form animated fadeInDown">
            <input type="search" id="s" name="s" class="form-control" placeholder="Search in here...">
            <button type="submit" class="btn-circle" title="Search"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- End .container -->
</div>
<!-- End #header-search-form -->
<?php /*if( $this->router->fetch_class()=='home'){$v_class=' navbar-transparent';} else {$v_class='';}
<nav class="navbar navbar-white animated-dropdown<?=$v_class?> ttb-dropdown" role="navigation"> */?>
<nav class="navbar navbar-white animated-dropdown ttb-dropdown" role="navigation">

    <div class="navbar-top clearfix">
        <div class="container">
            <div class="pull-left">
                <ul class="navbar-top-nav clearfix hidden-sm hidden-xs">
                    <li><strong>Logged as:</strong> <span class="normal"><?php echo $customer->getEmail(); ?></span></li>
                    <li>Customer Reference Number: <span class="normal"><?php echo $customer->getAccountName(); ?></span></li>
                </ul>
                <div class="dropdown account-dropdown visible-sm visible-xs">
                    <a class="dropdown-toggle" href="#" id="account-dropdown" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-user"></i>Customer
                        <span class="angle"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="account-dropdown">
                        <li role="presentation"><strong>Logged as:</strong> <span class="normal"><?php echo $customer->getEmail(); ?></span></li>
                        <li role="presentation">Customer Reference Number: <span class="normal"><?php echo $customer->getAccountName(); ?></span></li>
                    </ul>
                </div>
                <!-- End .account-dropdown -->
            </div>
            <!-- End .pull-left -->

            <div class="pull-right">


                <div class="dropdown language-dropdown pull-right">
                    <a class="dropdown-toggle" href="#" id="language-dropdown" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-user"></i> Account
                        <span class="angle"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="language-dropdown">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $online->getBaseUrl(). 'change_passwordnew.php'; ?>"><i class="fa fa-lock"></i> Change password</a></li>
                        <li role="separator" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $online->getBaseUrl(). 'logout.php'; ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                    </ul>
                </div><!-- End .curreny-dropdown -->



                <ul class="navbar-top-nav clearfix hidden-sm hidden-xs">
                    <div class="social-icons pull-right hidden -xs">
                    <a href="#" class="social-icon icon-facebook" title="Find us at Facebook" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="social-icon icon-twitter" title="Find us at Twitter" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#" class="social-icon icon-linkedin" title="Find us at Linkedin" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </div>
                    <!-- End .social-icons --></li>
                </ul>
                <!-- End. dropdowns-container -->

            </div>
            <!-- End .pull-right -->
        </div>
        <!-- End .container -->
    </div>
    <!-- End .navbar-top -->

    <div class="navbar-inner sticky-menu">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle btn-circle pull-right collapsed" data-toggle="collapse" data-target="#main-navbar-container">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand text-uppercase" href="<?php echo $online->getBaseUrl(); ?>">
                    <img src="<?php echo $online->getBaseUrl() . $logoUrls['relativePath']; ?>" alt="<?= $title; ?>" width="<?= $logoUrls['width']; ?>" height="<?= $logoUrls['height']; ?>" class="img-responsive center-block">
                </a>

            </div>
            <!-- End .navbar-header -->
            <?php include 'templates/menu.php'; ?>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </div>
    <!-- End .navbar-inner -->
</nav>

