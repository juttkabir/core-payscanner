<?php
use Payex\Helper\DataHelper;
use Payex\Online\Service\CustomerService;

/**
 * Created by PhpStorm.
 * Date: 02.04.19
 * Time: 2:04
 */

$customerService = new CustomerService($online->getDb());
$dataHelper = new DataHelper();
if ($customer) {
    $customerServices = $dataHelper->createArrayFromString($customer->getOnlineCustomerServices());
}

?>

<div class="collapse navbar-collapse" id="main-navbar-container">
    <ul class="nav navbar-nav navbar-left pull-right">
        <?php if ($customerServices && $customerService->hasPrepaidService($customerServices)):?>
        <li <?php if($currentPage == 'topup') echo "class='active'"; ?>><a href="<?= $online->getBaseUrl() . 'topup.php'?>" hreflang="en">Top Up</a></li>
        <li <?php if($currentPage == 'balance_inquiry') echo "class='active'"; ?>><a href="<?= $online->getBaseUrl() . 'balance_inquiry.php'?>" hreflang="en">Balance Inquiry</a></li>
        <?php endif; ?>
        <li <?php if($currentPage == 'sendmoney') echo "class='active'"; ?>><a href="<?= $online->getBaseUrl() . 'make_payment-ii-new.php'?>" hreflang="en">Send Money</a></li>
        <li <?php if($currentPage == 'transactions') echo "class='active'"; ?>><a href="<?= $online->getBaseUrl() . 'transaction_history_new.php'?>" hreflang="en">Transaction History</a></li>
        <li <?php if($currentPage == 'beneficiaries') echo "class='active'"; ?>><a href="<?= $online->getBaseUrl() . 'view_beneficiariesnew.php'?>" hreflang="en">Beneficiaries</a></li>
        <li <?php if($currentPage == 'help') echo "class='active'"; ?> class="dropdown">
            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help<span class="angle"></span></a>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="<?= $online->getBaseUrl() . 'faqs.php'?>" hreflang="en">FAQs</a></li>
                <li><a href="<?= $online->getBaseUrl() . 'contact_us.php'?>" hreflang="en">Contact Us</a></li>
            </ul>
        </li>
    </ul>
</div>
