<?php
/**
 * Created by PhpStorm.
 * Date: 02.04.19
 * Time: 1:34
 */

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentId = $_SESSION['loggedInUser']['agentID'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $agentId);
$logoUrls = $agentUiData['logoUrls'];
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$address = $agentUiData['address'];
?>
<div id="footer-inner" class="no-padding-bt-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="widget">
                    <div class="corporate-widget">
                        <p>
                            <?= $title; ?> Online is a registered trademark of <?= $title; ?> Ltd.<br>
                            Copyright &copy; 2019 <?= $title; ?> Ltd. All rights reserved.
                        </p>

                        <span class="social-icons-label">Find Us at:</span>
                        <div class="social-icons">
                            <a href="#" class="social-icon icon-facebook" title="Find us at Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="social-icon icon-twitter" title="Find us at Twitter" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon icon-linkedin" title="Find us at Linkedin" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </div>
                        <!-- End .social-icons -->

                    </div>
                    <!-- End corporate-widget -->
                </div>
                <!-- End .widget -->
            </div>
            <!-- End .col-md-4 -->
            <div class="col-md-2 col-sm-4">
                <div class="widget">
                    <h4>Links</h4>
                    <ul class="links">
                        <li><a href="#"><i class="fa fa-angle-right"></i>Home</a></li>
                        <li><a href="<?= $online->getBaseUrl() . 'make_payment-ii-new.php'?>"><i class="fa fa-angle-right"></i>Send money</a></li>
                        <li><a href="<?= $online->getBaseUrl() . 'transaction_history_new.php'?>"><i class="fa fa-angle-right"></i>Transactions history</a></li>
                        <li><a href="<?= $online->getBaseUrl() . 'view_beneficiariesnew.php'?>"><i class="fa fa-angle-right"></i>Beneficiaries</a></li>
                    </ul>

                </div>
                <!-- End .widget -->

            </div>
            <!-- End .col-md-4 -->

            <div class="col-md-2 col-sm-4">
                <div class="widget">
                    <h4>Links</h4>
                    <ul class="links">
                        <li><a href="#"><i class="fa fa-angle-right"></i>Terms and Conditions</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>Privacy Policy</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>Compliance Policy</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i>Cookies</a></li>
                    </ul>
                </div>
                <!-- End .widget -->

            </div>
            <!-- End .col-md-4 -->

            <div class="col-md-3 col-sm-4">
                <div class="widget">
                    <h4><?= $title; ?></h4>
                    <p><?= $address; ?><br>
                        <?= $email; ?>
                        <br><a href="<?= $online->getBaseUrl() . 'contact_us.php'?>">Contact us</a>
                        <br><strong>Landline:</strong> <a href="tel:<?= $phone; ?>"><?= $phone; ?></a></p>
                </div>
                <!-- End .widget -->
            </div>
            <!-- End .col-md-4 -->
        </div>
        <!-- End .row -->
    </div>
    <!-- End .container -->
</div>
<!-- End #footer-inner -->
<div id="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-6">
                <ul class="footer-menu">
                    <li><a href="#">Home</a></li>
                    <li><a href="<?= $online->getBaseUrl() . 'make_payment-ii-new.php'?>">Send money</a></li>
                    <li><a href="<?= $online->getBaseUrl() . 'transaction_history_new.php'?>">Transactions history</a></li>
                    <li><a href="<?= $online->getBaseUrl() . 'view_beneficiariesnew.php'?>">Beneficiaries</a></li>
                </ul>
            </div>
            <!-- End .col-md-6 -->
            <div class="col-md-6 col-md-pull-6">
                <p class="copyright"><?= $title; ?>. All rights reserved. &copy;
                    <?php echo date('Y') ?>
                    <a href="<?= $online->getBaseUrl(); ?>"><?= $title; ?></a>
                </p>
            </div>
            <!-- End .col-md-6 -->
        </div>
        <!-- End .row -->
    </div>
    <!-- End .container -->
</div>
<!-- End #footer-bottom -->

