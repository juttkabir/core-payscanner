<link rel="stylesheet" href="<?= $online->getBaseUrl(); ?>assets/css/styles00.min.css">

<!-- Favicon and Apple Icons -->
<link rel="icon" type="image/png" href="<?= $online->getBaseUrl(); ?>assets/images/icons/favicon.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?= $online->getBaseUrl(); ?>assets/images/icons/faviconx57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?= $online->getBaseUrl(); ?>assets/images/icons/faviconx72.png">

<script src="<?= $online->getBaseUrl(); ?>assets/js/scriptjs.min.js"></script>

<style>
    .normal {
        font-weight: normal !important;
    }
</style>