<?php 
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
    header("LOCATION: logout.php");
}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$address = $agentUiData['address'];
$logoHtml = $agentUiData['logoHtml'];


$strUserId = $_SESSION['loggedInUser']['userID'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Contact us - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>
    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'contact-us'; ?>
        <?php include 'templates/header.php'; ?>
    </header>
    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>FAQs</h1>
                        <!--                        <p class="page-header-desc">Talk to us</p>-->
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">FAQs</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container">

            <div class="logout_area">
                <h2 class="heading">Frequently Asked Questions</h2>
                <!-- content area -->
                <div class="content_area">
                    <div class="content_area_center">
                        <p class="content_heading">FAQs</p>
                        <p class="content_subheading">Answers to questions our customers often ask us. If the answer to
                            your question is not here email or call us and we'll be happy to help you. </p>
                    </div>
                    <?php /*
<!--faqs questions links -->
<ul class="faqs_list">
							  		<li><a href="#1">How does Premier FX save me money?</a></li>
									<li><a href="#2">What costs / fees can I expect to pay?</a></li>
									<li><a href="#3">How do I know my money is safe?</a></li>
									<li><a href="#4">How long do you take to transfer funds?</a></li>
									<li><a href="#5">Why shouldn't I just use my bank?</a></li>
									<li><a href="#6">What happens if I am unable to pay for my trade on the day it falls due?</a></li>
									<li><a href="#7">What is a foreign exchange company?</a></li>
									<li><a href="#8">How does Premier FX make its money if there are no fees or commissions?</a></li>
							  </ul>
							  
<div class="questions_area">
<a name="1"></a>
<p class="question_heading">How does Premier FX save me money?</p>
<p class="question_answer">As a commercial client of a major international bank we transact a high volume of deals. This means we can access very competitive exchange rates from the foreign exchange markets. Coupled with our low overheads this enables us to pass on great savings to our clients. We do not charge our clients for transfers.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="2"></a>
<p class="question_heading">What costs / fees can I expect to pay?</p>
<p class="question_answer">Premier FX do not charge any fees for any of their services, so if you are worried that somewhere down the process you will be hit with a bill, dont be! Our account opening is free of charge, as are our transfers and even the one-to-one market trading advice given by our Account Executives. If you transfer your funds to our client account via on-line banking, in the majority of cases you will find the whole process completely free from charges. </p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>
<div class="questions_area">
<a name="3"></a>
<p class="question_heading">How do I know my money is safe?</p>
<p class="question_answer">Premier FX hold secure clients accounts with a major international bank in London, which are totally separate from our day-to-day business accounts. These accounts are very similar to how Lawyer / Solicitor clients accounts work. We are also registered with HM Customs & Excise, registration number 1223621400000.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="4"></a>
<p class="question_heading">How long do you take to transfer funds?</p>
<p class="question_answer">As soon as we receive your payment we transfer your currency straight away. It should take no longer than 48 hours to reach the designated bank account anywhere in the world. In most cases it is even quicker.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="5"></a>
<p class="question_heading">Why shouldn't I just use my bank?</p>
<p class="question_answer">Banks are usually 4 - 8 % more expensive on their exchange rates, sometimes a little less, often a lot more!  They do not have the expertise on a local level to provide any guidance on Currency Exchange nor do they monitor the markets. We have our rates in front of us when you talk to us and we make sure we are up to date with the latest market movements. </p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="6"></a>
<p class="question_heading">What happens if I am unable to pay for my trade on the day it falls due?</p>
<p class="question_answer">Contact us immediately and your Account Manager can help you in this situation.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="7"></a>
<p class="question_heading">What is a foreign exchange company?</p>
<p class="question_answer">A foreign exchange company is a non-bank organisation that services the currency needs of both private and corporate clients. The company's role is to achieve the best currency exchange rates for their clients and to be as cost effective as possible. The company is able to source the currency direct from the foreign exchange (FX) market and therefore pass savings onto their clients.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

<div class="questions_area">
<a name="8"></a>
<p class="question_heading">How does Premier FX make its money if there are no fees or commissions?</p>
<p class="question_answer">We make a small profit between the price we sell foreign currency to our customers at and the price we pay for the currency on the international market.</p>
<p><strong class="back_top"><a href="#top">Back to top</a></strong></p>
</div>

*/ ?>
                </div>
                <!-- content area ends here-->
            </div>

        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>
</html>