<?php
include_once 'includes/ui_config.php';
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentId = $_SESSION['loggedInUser']['agentID'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $agentId);
$logoHtml = $agentUiData['logoHtml'];
$title = $agentUiData['title'];
?>
<div class="footer_area">
	<div class="upper_footer">
		<div class="upper_footer_wrapper">
			<div class="footer_left">
				<?= $logoHtml; ?>
			</div>
			<div class="footer_right">
				<div class="footer_social_wrapper">
					<a href="<?=LINKEDIN_URL?>" target="_blank"><img class="social_logos" src="images/in_logo.png"></a>
					<a href="<?=FACEBOOK_URL?>" target="_blank"><img class="social_logos" src="images/fb_logo.png"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="lower_footer">
		<div class="lower_footer_wrapper">
			<div class="footer_left">
				<span class="footer_address"><?= $title; ?> Online is a registered trademark of <?= $title;?> Ltd.
					Copyright (c) <?php echo date("Y"); ?> <?= $title; ?> Ltd. All rights reserved.
				</span>
			</div>
			<div class="footer_right">
				<ul class="footer_menu">
					<li class="footer_menu_list"><a href="#">Terms & Conditions</a></li><span class="footer_sep">|</span>
					<li class="footer_menu_list"><a href="#">Privacy Policy</a></li><span class="footer_sep">|</span>
					<li class="footer_menu_list"><a href="#">Cookie Policy</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>