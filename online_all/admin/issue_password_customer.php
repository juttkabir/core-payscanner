<?php 
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();
	$strQuery = "SELECT accountName, password, email, customerName,  customerID FROM ".." ";

?>
<html>
	<head>
		<title>Issue password to customer</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="css/style_admin.css"/>
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/bootstrap.js"></script>
		<script>
			$(document).ready(
				function(){
					function checkAll(){
						$('input:checkbox').each(
							function(){
								$('input:checkbox').attr('checked', true);
							}
						);
					}
					
					function uncheckAll(){
						$('input:checkbox').each(
							function(){
								$('input:checkbox').attr('checked', false);
							}
						);
					}
					
					$('#all').change(
						function(){
							if($('#all').is(':checked'))
								checkAll();
							else
								uncheckAll();
						}
					);
				}
			);
		</script>
	</head>
	<body>
		<div class="container">
			<?php 
				require_once "includes/header.php";
			?>
			<h3 class="well-small align_center well">Issue password to customer</h3>
			<div class="form_cont">
				<form action="" method="post">
					<table class="table table-striped">
						<tr>
							<th>From Date:</th>
							<td>
								<select name="fDay" id="fDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="fMonth" id="fMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="fYear" id="fYear" class="input-small">
									<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>To Date:</th>
							<td>
								<select name="tDay" id="tDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="tMonth" id="tMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="tYear" id="tYear" class="input-small">
									<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>Customer Name:</th>
							<td>
								<input name="customerName" id="customerName" type="text" class="input-medium"/>
							</td>
						</tr>
						<tr>
							<th>Customer Number: </th>
							<td><input name="customerNumber" id="customerNumber" type="text" class="input-medium"/></td>
						</tr>
						<tr>
							<td colspan="2" class="align_center">
								<input name="search" id="search" type="submit" class="btn btn-primary" value="Search"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<table class="table table-striped table-bordered cust_size2">
				<tr>
					<th><input name="all" id="all" type="checkbox"/></th>
					<th>Customer Number</th>
					<th>Name</th>
					<th>Email</th>
					<th>Issue Password</th>
				</tr>
				<tr>
					<td><input name="" id="" type="checkbox"/></td>
					<td>C-123</td>
					<td>test name</td>
					<td>abc@xyz.com</td>
					<td><a href="#">issue Password</a></td>
				</tr>
				<tr>
					<td colspan="5" class="align_center"><input name="" id="" type="button" class="btn-danger btn" value="Send"/></td>
				</tr>
			</table>
		</div>
	</body>
</html>