<?php
require '../../src/bootstrap.php';

session_start();
include_once "../includes/configs.php";
include_once "../includes/database_connection.php";
dbConnect();
include_once "../includes/functions.php";
$username=loggedUser();
if ($username=="")
{
    header("location: ../../admin/index.php");
    exit();

}
	
$userID  = $_SESSION["loggedUserData"]["userID"];

$agentRepository = new \Payex\Repository\AgentRepository($app->getDb());
$currencyRepository = new \Payex\Repository\CurrencyRepository($app->getDb());
$agentHelper = new \Payex\Helper\AgentHelper();
$agent = $agentRepository->getById($userID);

//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{

 $where_sql="   AND agentID='".	$userID."' ";
}


	foreach($_REQUEST as $key => $val)
			$$key = $val;
	if(isset($_GET['limit']) && $_GET['limit'] > 0)
		$limit = intval(trim($_GET['limit']));
	else
		$limit = 5;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	elseif(!empty($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;
		
	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	$fromDate = $fYear."-".$fMonth."-".$fDay." 00:00:00";
	$toDate = $tYear."-".$tMonth."-".$tDay." 23:59:59";
	
	if($_REQUEST["search"] == "Search" || $_REQUEST["search"] == "search")
	{
		
		
	$query = "select * from fx_rates_margin where 1  $where_sql";
	//debug($fYear);
	if(isset($fYear) && isset($fMonth) && isset($fDay) && isset($tYear) && isset($tMonth) && isset($tDay))
	{
		$query .= " and addedOn between '".$fromDate."' and '".$toDate."' ";
	
	}
	if(isset($baseCurrency) && $baseCurrency != "")
	{
		$query .= " and baseCurrency = '".$baseCurrency."' ";
	
	}
	if(isset($quotedCurrency) && $quotedCurrency != "")
	{
		$query .= " and quotedCurrency = '".$quotedCurrency."' ";
	
	}

	if(isset($_REQUEST["fxStatus"]) && $_REQUEST["fxStatus"] != "")
	{
		$query .= " and statusType = '".$_REQUEST["fxStatus"]."' ";
	
	}
	

	
	$query .= " group by addedOn DESC ORDER BY baseCurrency, fromAmount, toAmount ASC";
	//debug($query);
	
	//print_r($query);
	$allRecords = selectMultiRecords($query);
	$totalCount = count($allRecords);
	$strQueryPagination = " LIMIT $offset, $limit";
	$query .= $strQueryPagination;
	//debug($query);
	$arrContentMargin = selectMultiRecords($query);
	$intCountLimit = count($arrContentMargin);
	}
?>

<html>
	<head>
		<title>Manage FX Rate</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="css/style_admin.css"/>
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/bootstrap.js"></script>
		<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			SelectOption(document.forms[0].tDay, "<?php if(isset($tDay)) echo $tDay; else echo date('d'); ?>");
			SelectOption(document.forms[0].tMonth, "<?php if(isset($tMonth)) echo $tMonth; else echo date('m');?>");
			SelectOption(document.forms[0].tYear, "<?php if(isset($tYear)) echo $tYear; else echo date('Y');?>");
			<?php 
				if(isset($fDay) && isset($fMonth) && isset($fYear)){
			?>
			SelectOption(document.forms[0].fDay, "<?php echo $fDay; ?>");
			SelectOption(document.forms[0].fMonth, "<?php echo $fMonth; ?>");
			SelectOption(document.forms[0].fYear, "<?php echo $fYear; ?>");
			<?php 
				}
			?>
		</script>
	</head>
	<body>
		<div class="container">
			<?php 
				require_once "includes/header.php";
			?>
			<h3 class="well-small align_center well">Manage FX Rates</h3>
			<div class="form_cont">
				<form action="" method="post">
					<table class="table table-striped">
						<tr>
							<th>From Date:</th>
							<td>
								<select name="fDay" id="fDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="fMonth" id="fMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="fYear" id="fYear" class="input-small">
									<!--<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>-->	<?php 
$currentYear = date("Y");
$years = range ($currentYear, 2005);
foreach ($years as $value) {
echo "<option value=\"$value\">$value</option>\n";
}

?>
								</select>
							</td>
							<th>To Date:</th>
							<td>
								<select name="tDay" id="tDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="tMonth" id="tMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="tYear" id="tYear" class="input-small">
									<!--<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>-->	<?php 
$currentYear = date("Y");
$years = range ($currentYear, 2005);
foreach ($years as $value) {
echo "<option value=\"$value\">$value</option>\n";
}

?>					</select>
							</td>
						</tr>
						<tr>
							<th>Base Currency:</th>
							<td>
								<select name="baseCurrency" id="baseCurrency" class="input-small">
									<option value="">All</option>
									<?php
                                    if ($agent && $agentHelper->isAgent($agent)){
                                        $countryCondition = [0 => $agent->getCustCountries()];
                                        $baseCountryList = $agentHelper->getAgentRelatedCountries($countryCondition);
                                        $baseCurrenciesCollection = $currencyRepository->getByCountryList($baseCountryList);
                                        /* @var $item \Payex\Model\Currency */
                                        foreach ($baseCurrenciesCollection as $item){
                                            echo '<option value="' . $item->getCurrencyName() . '">' . $item->getCurrencyName() . '</option>';
                                        }
                                    }else{
                                        $queryBase = "select distinct(baseCurrency) from fx_rates_margin order by baseCurrency ASC";
                                        $arrBaseCurrencies = selectMultiRecords($queryBase);
                                        for ($i=0;$i<count($arrBaseCurrencies);$i++){
                                            echo '<option value="' . $arrBaseCurrencies[$i]["baseCurrency"] .'">' . $arrBaseCurrencies[$i]["baseCurrency"] . '</option>';
                                        }
                                    }
                                    ?>
								</select>
							</td>
							<th>Quoted Currency:</th>
							<td>
								<select name="quotedCurrency" id="quotedCurrency" class="input-small">
									<option value="">All</option>
									<?php
                                    if ($agent && $agentHelper->isAgent($agent)){
                                        $countryCondition = [0 => $agent->getIDACountry()];
                                        $quotedCountryList = $agentHelper->getAgentRelatedCountries($countryCondition);
                                        $quotedCurrenciesCollection = $currencyRepository->getByCountryList($quotedCountryList);
                                        /* @var $item \Payex\Model\Currency */
                                        foreach ($quotedCurrenciesCollection as $item){
                                            echo '<option value="' . $item->getCurrencyName() . '">' . $item->getCurrencyName() . '</option>';
                                        }
                                    }else{
                                        $queryQuoted = "select distinct(quotedCurrency) from fx_rates_margin order by quotedCurrency ASC";
                                        $arrQuotedCurrencies = selectMultiRecords($queryQuoted);
                                        for ($i=0;$i<count($arrQuotedCurrencies);$i++){
                                            echo '<option value="' . $arrQuotedCurrencies[$i]["quotedCurrency"] . '">' . $arrQuotedCurrencies[$i]["quotedCurrency"] . '</option>';
                                        }
                                    }
									?>
								</select>
								
								
							<!-- ticket 12704 starts here -->
							<label class="premierfx_status" style="">Status:</label>
								<select name="fxStatus" id="fxStatus" class="input-small">
							<option value="">All</option>
							<option value="Enable" selected="selected">Enabled</option>
							<option value="Disable">Disabled</option>
							</select>
							<!-- ticket 12704 ends here -->
							</td>
							
							
							
							
						</tr>
						<!--<tr>
							<th colspan="1">User Group</th>
							<td colspan="3">
								<select name="userGroup" id="userGroup" class="input-small">
									<option value="">All</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="5" class="control-group error align_center">
								<label>Error Msg</label>
							</td>
						</tr>
						<tr>
							<td colspan="5" class="control-group success align_center">
								<label>Success Msg</label>
							</td>
						</tr>-->
						<tr>
							<td colspan="4" class="align_center">
								<input name="search" id="search" type="submit" class="btn btn-primary" value="Search"/>
								<input name="reset" id="reset" type="reset" class="offset1 btn btn-danger" value="Clear"/>
							</td>
						</tr>
					</table>
				</form>
				<?php 
						if($totalCount > 0){
					?>
					<table class="table cust_size">
						<tr>
							<td class="bold">Showing <?php echo ($offset+1)." - ".($intCountLimit + $offset)." of ".$totalCount ?></td>
							<td class="bold align_right" align = "right">
								Records Per Page:
								<select name="limit" id="limit" class="input-small" >
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="50">50</option>
								</select>
							</td>
							<td class="align_right" >
								<?php 
								//debug($next);
									if($prev >= 0){
									
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fDay=<?php echo $fDay; ?>&fMonth=<?php echo $fMonth;?>&fYear=<?php echo $fYear; ?>&tDay=<?php echo $tDay; ?>&tMonth=<?php echo $tMonth; ?>&tYear=<?php echo $tYear; ?>&baseCurrency=<?php echo $baseCurrency; ?>&quotedCurrency=<?php echo $quotedCurrency;?>&fxStatus=<?php echo $_REQUEST["fxStatus"];?>&limit=<?php echo $limit; ?>&search=search">First</a>&nbsp;
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev;?>&fDay=<?php echo $fDay; ?>&fMonth=<?php echo $fMonth;?>&fYear=<?php echo $fYear; ?>&tDay=<?php echo $tDay; ?>&tMonth=<?php echo $tMonth; ?>&tYear=<?php echo $tYear; ?>&baseCurrency=<?php echo $baseCurrency; ?>&quotedCurrency=<?php echo $quotedCurrency;?>&fxStatus=<?php echo $_REQUEST["fxStatus"];?>&limit=<?php echo $limit; ?>&search=search">Previous</a>&nbsp;
								<?php 
									}
									if($next > 0 && $next < $totalCount){
								?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>&fDay=<?php echo $fDay; ?>&fMonth=<?php echo $fMonth;?>&fYear=<?php echo $fYear; ?>&tDay=<?php echo $tDay; ?>&tMonth=<?php echo $tMonth; ?>&tYear=<?php echo $tYear; ?>&baseCurrency=<?php echo $baseCurrency; ?>&quotedCurrency=<?php echo $quotedCurrency;?>&fxStatus=<?php echo $_REQUEST["fxStatus"];?>&limit=<?php echo $limit; ?>&search=search">Next</a>&nbsp;
									<?php 
										$intLastOffset = (ceil($totalCount / $limit) - 1) * $limit;
									?>
									<a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset;?>&fDay=<?php echo $fDay; ?>&fMonth=<?php echo $fMonth;?>&fYear=<?php echo $fYear; ?>&tDay=<?php echo $tDay; ?>&tMonth=<?php echo $tMonth; ?>&tYear=<?php echo $tYear; ?>&baseCurrency=<?php echo $baseCurrency; ?>&quotedCurrency=<?php echo $quotedCurrency;?>&fxStatus=<?php echo $_REQUEST["fxStatus"];?>&limit=<?php echo $limit; ?>&search=search">Last</a>
								<?php 
									}
									
								?>
							</td>
						</tr>
					</table>
				<table class="table table-striped table-bordered cust_size">
					<tr>
						<th>Date Created</th>
						
						<th>Base Currency</th>
						<th>Quoted Currency</th>
						
						<th>From Amount</th>
						<th>To Amount</th>
						
						<th>Margin Type</th>
						<th>Margin</th>
						<th>Status</th>
						<th>Operation</th>
						
					</tr>
					
					<?php
					for($i=0;$i<count($arrContentMargin);$i++)
					{ 
						$dateCreated = explode(" ",$arrContentMargin[$i]["addedOn"]);
						$dateCreatedOrder = explode("-",$dateCreated[0]);
					?>
						<tr>
						<td><? echo $dateCreatedOrder[2]."-".$dateCreatedOrder[1]."-".$dateCreatedOrder[0]; ?></td>
						
						<td><? echo $arrContentMargin[$i]["baseCurrency"]; ?></td>
						<td><? echo $arrContentMargin[$i]["quotedCurrency"]; ?></td>
						
						<td><? echo $arrContentMargin[$i]["fromAmount"]; ?></td>
						<td><? echo $arrContentMargin[$i]["toAmount"]; ?></td>
						
						<td><? echo $arrContentMargin[$i]["marginType"]; ?></td>
						<td><? echo $arrContentMargin[$i]["margin"]; ?></td>
						<td><? echo $arrContentMargin[$i]["statusType"]; ?></td>
						<td><a href = "create_fx_rate.php?status=edit&id=<?=$arrContentMargin[$i]['id']?>">Edit</a></td>
					
						</tr>
					<? } 
					} ?>
					
						
					
				</table>
			</div>
		</div>
	</body>
	<script type="text/javascript">
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			SelectOption(document.forms[0].tDay, "<?php if(isset($tDay)) echo $tDay; else echo date('d'); ?>");
			SelectOption(document.forms[0].tMonth, "<?php if(isset($tMonth)) echo $tMonth; else echo date('m');?>");
			SelectOption(document.forms[0].tYear, "<?php if(isset($tYear)) echo $tYear; else echo date('Y');?>");
			<?php 
				if(isset($fDay) && isset($fMonth) && isset($fYear)){
			?>
			SelectOption(document.forms[0].fDay, "<?php echo $fDay; ?>");
			SelectOption(document.forms[0].fMonth, "<?php echo $fMonth; ?>");
			SelectOption(document.forms[0].fYear, "<?php echo $fYear; ?>");
			<?php 
				}
				if(isset($baseCurrency))
				{
			?>
			SelectOption(document.forms[0].baseCurrency, "<?php echo $baseCurrency;?>");
			<? }

			if(isset($quotedCurrency))
			{		?>
			
			SelectOption(document.forms[0].quotedCurrency, "<?php echo $quotedCurrency;?>");
			<? } 
			
			if(isset($_REQUEST["fxStatus"]))
			{		?>
			
			SelectOption(document.forms[0].fxStatus, "<?php echo $_REQUEST["fxStatus"];?>");
			<? } ?>
			
			SelectOption(document.getElementById('limit'), '<?php echo $limit; ?>');
			$('#limit').change(
				function(){
					limit = $(this).val();
					window.location.href = '<?php echo $_SERVER['PHP_SELF'];?>?offset=0&fDay=<?php echo $fDay; ?>&fMonth=<?php echo $fMonth;?>&fYear=<?php echo $fYear; ?>&tDay=<?php echo $tDay; ?>&tMonth=<?php echo $tMonth; ?>&tYear=<?php echo $tYear; ?>&baseCurrency=<?php echo $baseCurrency; ?>&quotedCurrency=<?php echo $quotedCurrency;?>&fxStatus=<?php echo $_REQUEST["fxStatus"];?>&limit='+limit+'&search=search';
				}
			);
		</script>
</html>
