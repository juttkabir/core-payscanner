<?php 
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Send Password</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<style>
			.container{
				padding:10px;
				background-color:#fff;
				border:1px solid #999;
			}
			.short{
				width:500px !important;
				margin:0 auto !important;
			}
			.table tr th{
				color:#000 !important;
			}
			.align_center{
				text-align:center !important;
			}
			h1{
				font-size:24px;
			}
		</style>
		<script src="scripts/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h1 class="well well-small align_center">Send Password</h1>
			<form action="" method="post">
				<table class="table short">
					<tr>
						<th>
							Enter Email:
						</th>
						<td>
							<input name="email" id="email" class="input-medium" type="text"/>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="align_center">
							<input name="sendPassword" id="sendPassword" class="btn actv_btn" type="submit" value="Send Password"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>