<?php
/**
 * @package: Online Module
 * @subpackage: logout page
 * @author: Mirza Arslan Baig
 */
session_start();

header("LOCATION: index.php");

//if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC" || $_SESSION["loggedInUser"]["online_customer_services"] == "TC,PC" || $_SESSION["loggedInUser"]["online_customer_services"] == "PC,TC") {
//    header("LOCATION: signup_login.php?typesign=UEM=&type=cHJlcGFpZA%3D%3D#login");
//
//} else {
//
//    header("LOCATION: signup_login.php?type=UEM=#login");
//}

session_destroy();

?>