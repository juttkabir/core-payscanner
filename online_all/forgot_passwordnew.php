<?php
session_start();
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoUrls = $agentUiData['logoUrls'];
$logoHtml = $agentUiData['logoHtml'];

include_once "includes/configs.php";
include_once "includes/database_connection.php";
include_once "includes/functions.php";
//ini_set("display_errors","on");
$db = dbConnect();
//debug($_POST);
require_once "../admin/lib/phpmailer/class.phpmailer.php";
if (!empty($_POST['customerNumber']) && !empty($_POST['day']) && !empty($_POST['month']) && !empty($_POST['year']) && !empty($_POST['email'])) {

	$strCustomerNumber = trim($_POST['customerNumber']);
	$strBirthDate = $_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day'];
	$strQueryVerification = "SELECT customerName,accountName, email, customerID, customerStatus,Phone,Address,agentID FROM " . TBL_CUSTOMER . " WHERE accountName = '$strCustomerNumber' AND dob = '$strBirthDate' AND email = '" . $_POST['email'] . "' ";
	$arrCustomer = selectFrom($strQueryVerification);
	//print_r($arrCustomer);
	$intFlagVerify = false;
	if (!empty($arrCustomer['customerID'])) {
		$password = generateRandomString();
		// debug($password);
		$intFlagVerify = true;
		$_SESSION['issuePassCustomerID'] = $arrCustomer['customerID'];
		$strUpdatePassword = "UPDATE " . TBL_CUSTOMER . " SET `password` = '$password' WHERE customerID ='" . $arrCustomer['customerID'] . "'";
		update($strUpdatePassword);
		$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='6' and email_templates.status = 'Enable' AND agentID='".$arrCustomer["agentID"]."' ");
		$logoCompany = "<img border='0' src='http://" . $_SERVER['HTTP_HOST'] . $logoUrls['relativePath'] . "/>";
		$message1 = $emailContent["body"];
		$varEmail = array("{customer}", "{email}", "{password}", "{customernumber}", "{logo}", "{accountName}", "{phone}", "{address}");
		$contentEmail = array($arrCustomer['customerName'], $arrCustomer['email'], $password, $arrCustomer["accountName"], $logoCompany, $arrCustomer["accountName"], $arrCustomer["Phone"], $arrCustomer["Address"]);

		$messageT = str_replace($varEmail, $contentEmail, $message1);
		//debug($messageT);
		//Adedd by Waleed Nadeem  against Ticket #10825 Email Template Maanger
		$toField = explode('/', $emailContent['to']);

		for ($j = 0; $j < count($toField); $j++) {

			if ($toField[$j] == 'Sender' || $toField[$j] == 'Beneficiary') {
				//do Nothing
			} else {

				$getEmail = selectFrom("select email from admin where userID='" . $toField[$j] . "'");

				if (!empty($getEmail)) {
					$sendermailer->AddBCC($getEmail['email'], '');
				}

			}
		}

		$subject = $emailContent["subject"];
		$customerMailer = new PHPMailer();
		$strBody = $messageT;
		$fromName = $emailContent["fromName"];
		$fromEmail = $emailContent["fromEmail"];
		$customerMailer->FromName = $fromName;
		$customerMailer->From = $fromEmail;

		//die($emailContent['cc']);

		//Add BCC
		//if($emailContent['bcc'])
		//debug(CONFIG_SEND_EMAIL_ON_TEST);

		if (CONFIG_SEND_EMAIL_ON_TEST == "1") {
			$customerMailer->AddAddress(CONFIG_TEST_EMAIL, '');
		} else {
			$customerMailer->AddAddress($arrCustomer['email'], '');
			if ($emailContent['bcc']) {
				$customerMailer->AddBCC($emailContent['bcc'], '');
			}

			if ($emailContent['cc']) {
				$customerMailer->AddAddress($emailContent['cc'], '');
			}

		}
		$customerMailer->Subject = $subject;
		$customerMailer->IsHTML(true);
		$customerMailer->Body = $strBody;
		//		debug($customerMailer);
		//debug(CONFIG_EMAIL_STATUS);

		$custEmailFlag = $customerMailer->Send();

		activities($_POST['customerNumber'], "UPDATION", $_POST['customerNumber'], "customer", "Customer changed his password through forgot password");

		$strSuccess = "Password reset mail sent to the customer";

		/*
			Mailer
			$arrCustomer = selectFrom($strQueryCustomer);
				$subject = "Issue Password";
				$customerMailer = new PHPMailer();
				$strBody = "Dear ".$arrCustomer[$x]['customerName']."\r\n<br />";
				$strBody .= "Your Trading Account password has been set.\n<br />
				Your username is: ".$arrCustomer[$x]['email']."\r\n<br />
				Your new password is: [ ".$password." ]\r\n\n\r<br /><br />
				\n<br />".$arrCustomer[$x]["adminName"]."<br />\n
				Follow this link to login: http://clients.premfx.com/private_registration.php\n<br />
				<table>
					<tr>
						<td align='left' width='30%'>
						<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width='20%'>&nbsp;</td>
						<td align='left'>
							<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
							55 Old Broad Street,London, EC2M 1RX.\n<br>
							<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
							<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
							Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
							<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
							<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
							<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
							C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
							<font color='#F7A30B'>Tel:</font> +34 971 576 724
							 \n <br>
							 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
						</td>
					</tr>
				</table>
				";
				$fromName  = SYSTEM;
				$customerMailer->FromName =  $fromName;
				$customerMailer->From =  $arrCustomer[$x]['adminEmail'];
				$customerMailer->AddAddress($arrCustomer[$x]['email'],'');
				$customerMailer->Subject = $subject;
				$customerMailer->IsHTML(true);
				$customerMailer->Body = $strBody;
				$custEmailFlag = $customerMailer->Send();
		 */
		//		debug($strSuccess);
	} else {
		$strError = "The details you entered are not correct.";

	}
}
//	debug($_POST);

?>
<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Private Client Forgot Password <?php echo $title; ?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>

</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="login-section" class="fullheight">
    <div class="vcenter-container">
        <div class="vcenter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-3 col-lg-4 col-lg-push-4">
                        <h1 class="logo text-center">
                            <?= $logoHtml; ?>
                        </h1>
                        <div class="form-wrapper">
                            <h2 class="h4 title-underblock custom mb30">Recover your password</h2>
                            <!-- Error message shown here -->
                            <?php
                            if (!empty($strError)) {
                                echo '<div class="alert alert-danger error_msg">' . $strError . '</div>';
                            } else if (!empty($strSuccess)) {
                                echo '<div class="alert alert-danger error_msg">' . $strSuccess . '</div>';
                            } ?>
                            <!-- Error message shown here ends area -->
                            <form id="verifyIdentityFrom" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                <div class="form-group">
                                    <label for="customerNumber" class="input-desc">Customer Number</label>
                                    <input type="text" class="form-control" id="customerNumber" name="customerNumber"
                                           placeholder="Customer Number" maxlength="8" required>
                                </div><!-- End .from-group -->
                                <div class="form-group mb10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="day" class="input-desc">Day</label>
                                            <select name="day" id="day" type="text" class="form-control select_field">
                                                <option value="">- Select day -</option>
                                                <?php
                                                for ($x = 1; $x <= 31; $x++) {
                                                    $strDay = str_pad($x, 2, "0", STR_PAD_LEFT);
                                                    echo "<option value='$strDay'>$strDay</option>";
                                                } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="month" class="input-desc">Month</label>
                                            <select name="month" id="month" type="text"
                                                    class="form-control select_field">
                                                <option value="">- Select month -</option>
                                                <?php
                                                for ($x = 1; $x <= 12; $x++) {
                                                    $strMonth = str_pad($x, 2, "0", STR_PAD_LEFT);
                                                    echo "<option value='$strMonth'>$strMonth</option>";
                                                } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="year" class="input-desc">Year</label>
                                            <select name="year" id="year" type="text" class="form-control select_field">
                                                <option value="">- Select Year -</option>
                                                <?php
                                                for ($y = 1900; $y <= 1996; $y++) {
                                                    echo "<option value='$y'>$y</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- End .from-group -->
                                <div class="form-group mb10">
                                    <label for="email" class="input-desc">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                           maxlength="32" required>
                                </div><!-- End .from-group -->
                                <div class="form-group ">
                                    <a href="<?php echo $online->getBaseUrl(); ?>" class="add-tooltip"
                                       data-placement="top" title="Login as private client">Already registered</a>
                                </div><!-- End .form-group -->
                                <div class="form-group mt15-r">
                                    <a href="<?php echo $online->getBaseUrl() . 'registration_form.php'; ?>" class="add-tooltip"
                                       data-placement="top" title="Register as private client">Not registered yet</a>
                                </div><!-- End .form-group -->
                                <div class="form-group mb5">
                                    <input name="changePassword" id="changePassword" type="submit"
                                           class="btn btn-custom" value="Submit">
                                </div><!-- End .from-group -->
                            </form>
                        </div><!-- End .form-wrapper -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->

            </div>
            <!-- content area ends here-->
        </div><!-- End .container -->
    </div><!-- End .vcenter -->
</div><!-- End .vcenter-container -->
</div><!-- End .fullheight -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->

<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script type="text/javascript" src="./assets/js/frorgot-password.js"></script>
</body>
</html>


