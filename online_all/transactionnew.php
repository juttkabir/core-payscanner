<?php
/**
* @package: Online Module	
* @subpackage: Confirm Page
* @author: Awais Umer
*/
session_start();
if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$logoHtml = $agentUiData['logoHtml'];

include_once "javascript/audit-functions.php";
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

$strUserId=$_SESSION['loggedInUser']['accountName'];
$strEmail=$_SESSION['loggedInUser']['email'];

if($_POST["existBen"] != ""){
$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = ".$_POST["existBen"]." ";
$fetPreviousArr = selectFrom($getPreviousArr);	
/* $updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
`sendCurrency` = '".$_POST["sendingCurrency"]."',
accountNo	=	'".$_POST["accountNumber"]."',
sortCode	=	'".$_POST["sortCode"]."',
iban		=	'".$_POST["iban"]."'
WHERE benId = 	".$_POST["existBen"]."
"; */
// changed as send currecny is now changed in recieving currency

$updateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET
`sendCurrency` = '".$_POST["receivingCurrency"]."',
accountNo	=	'".$_POST["accountNumber"]."',
sortCode	=	'".$_POST["sortCode"]."',
iban		=	'".$_POST["iban"]."'
WHERE benId = 	".$_POST["existBen"]."
";
if(update($updateBenBank)){

activities($_SESSION["loginHistoryID"],"UPDATED",$_SESSION["loggedInUser"]["accountName"],"ben_banks","ben_banks updated successfully");

logChangeSet($fetPreviousArr,$_SESSION["loggedInUser"]["accountName"],"ben_banks","benId",$_POST["existBen"],$_SESSION["loggedInUser"]["accountName"]);

}
}
?>
<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Confirm Make payment - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Send Money</h1>
                        <p class="page-header-desc">Make payment</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Transactions</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Spot quote</h2>
                        <p style="font-size:18px;">Please check and confirm the transaction details below are correct.</p>

                        <form name="confirmTrans" id="confirmTrans" method="post" action="">
                            <input id="benName" name="benName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
                            <input id="benCountry" name="benCountry" type="hidden" value="<? echo $_POST['benCountry'] ?>" />
                            <input id="benCurrency" name="benCurrency" type="hidden" value="<? echo $_POST['benCurrency'] ?>" />
                            <input id="accountName" name="accountName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
                            <input id="accountNumber" name="accountNumber" type="hidden" value="<? echo $_POST['accountNumber'] ?>" />
                            <input id="branchNameNumber" name="branchNameNumber" type="hidden" value="<? echo $_POST['branchNameNumber'] ?>" />
                            <input id="branchAddress" name="branchAddress" type="hidden" value="<? echo $_POST['branchAddress'] ?>" />
                            <input id="routingNumber" name="routingNumber" type="hidden" value="<? echo $_POST['routingNumber'] ?>" />
                            <input id="sortCode" name="sortCode" type="hidden" value="<? echo $_POST['sortCode'] ?>" />
                            <input id="swift" name="swift" type="hidden" value="<? echo $_POST['swift'] ?>" />
                            <input id="reference" name="reference" type="hidden" value="<? echo $_POST['reference'] ?>" />
                            <input id="existBen" name="existBen" type="hidden" value="<? echo $_POST['existBen'] ?>" />
                            <input id="iban" name="iban" type="hidden" value="<? echo $_POST['iban'] ?>" />
                            <input type="hidden" name="transType" value="Bank Transfer" />
                            <input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? echo $_POST['sendingCurrency'] ?>" />

                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h3>Transaction details</h3>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Exchange rate:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-control"><?php echo number_format($_SESSION['exchangeRate'],4,".",","); ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">You are Sending:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-control"><?php echo number_format($_SESSION['sendingAmount'],2,".",",")." ". $_SESSION['sendingCurrency']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Beneficiary will Receive:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-control"><?php echo number_format($_SESSION['receivingAmount'],2,".",",") ." ". $_SESSION['receivingCurrency']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group ml10">
                                        <p>Once your funds have been received we will credit your chosen account within 48 hours</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h3>Beneficiary details</h3>
                                    </div>
                                </div>
                                <?php if(isset($_POST["accountName"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Beneficiary name:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['accountName'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["accountNumber"])  && !empty($_POST["accountNumber"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Account Number:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['accountNumber'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["branchNameNumber"])  && !empty($_POST["branchNameNumber"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Branch Name/Number:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['branchNameNumber'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["branchAddress"])  && !empty($_POST["branchAddress"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Branch Address:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['branchAddress'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["routingNumber"])  && !empty($_POST["routingNumber"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Routing Number:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['routingNumber'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["sortCode"])  && !empty($_POST["sortCode"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Sort Code:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['sortCode'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST["iban"]) && !empty($_POST["iban"])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">IBAN Number:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['iban'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($_POST['swift']) && !empty($_POST['swift'])): ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-desc mt10">Swift:</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control"><?php echo $_POST['swift'];?></div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Reference:</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-control"><?php echo $_POST['reference']; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h3>Deal Confirmation</h3>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <p>I accept that by clicking Confirm button I am entering into a legally binding contract and a cost may be Charged to change these details.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group mb5">
                                        <input name="back" id="back" type="button" class="btn btn-default" value="Go back" onClick="this.form.action='beneficiariesnew.php';this.form.submit();" />
                                        <? if($_SESSION['sendingCurrency'] != "GBP"){?>
                                            <input name="proceed" id="proceed" class="btn btn-custom" type="submit" value="Confirm Transaction" onClick="this.form.action='payment_detailnew.php';this.form.submit();"/>
                                        <?} else {?>
                                            <input name="proceed" id="proceed" class="btn btn-custom" type="submit"  value="Confirm Transaction" onClick="this.form.action='debit_card.php';this.form.submit();"/>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>

                            <p class="mt20 mb30">For amounts over £50,000 or if you have any other questions, please contact us <?= $email; ?></p>
                        </form>
                    </div><!-- End .form-wrapper -->
                </div><!-- End .col-sm-6 -->
                <div class="mb40 visible-xs"></div><!-- space -->
                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
                    <p>Send money safely and securely with <?= $title; ?> Online.</p>
                    <div class="mb10"></div><!-- space -->
                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->


</div><!-- End .container -->

    <div class="mb40 mb20-xs"></div><!-- space -->
    <footer id="footer" class="footer-inverse" role="contentinfo">
        <?php include 'templates/footer.php'; ?>
    </footer>
    <!-- End #footer -->

</div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            $("#proceed").click(function(){
                    window.btn_clicked = true;
                }
            );

            $(window).bind("beforeunload",function(event) {
                    if(!window.btn_clicked){

                        if(/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
                            if(confirm("You will not get the rate as good as this for your transaction. Are you sure you want to lose it?")) {
                                history.go();
                            } else {
                                window.setTimeout(function() {
                                    window.stop();
                                }, 1);
                            }
                        } else {
                            return "You will not get the rate as good as this for your transaction. Are you sure you want to lose it?";
                        }
                    }
                }
            );

        }
    );
</script>
</body>
</html>
