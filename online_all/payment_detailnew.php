<?php
/**
 * @package: Online Module
 * @subpackage: Payment Details

 */

session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";
$PaymentMode = $_SESSION["test"];

$intTransDataFlag = false;
if (!empty($_SESSION['exchangeRate']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['receivingCurrency']) && (!empty($_SESSION['sendingAmount']) && $_SESSION['sendingAmount'] > '0') && (!empty($_SESSION['receivingAmount']) && $_SESSION['receivingAmount'] > '0')) {
	if (isset($_POST['iban'])) {
		$patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
		$strIBAN = trim($_POST['iban']);

		$_SESSION['branchNameNumber'] = trim($_POST['branchNameNumber']);

		$_SESSION['branchAddress'] = trim($_POST['branchAddress']);
/*if(!preg_match($patternIBAN, $strIBAN)){
//debug('match', true);
$_SESSION['benName'] = trim($_POST['benName']);
$_SESSION['benCountry'] = $_POST['benCountry'];
$_SESSION['benCurrency'] = trim($_POST['benCurrency']);
$_SESSION['accountName'] = trim($_POST['accountName']);
$_SESSION['accountNumber'] = trim($_POST['accountNumber']);
$_SESSION['branchNameNumber'] = trim($_POST['branchNameNumber']);
$_SESSION['branchAddress'] = trim($_POST['branchAddress']);
$_SESSION['routingNumber'] = trim($_POST['routingNumber']);
$_SESSION['sortCode'] = trim($_POST['sortCode']);
$_SESSION['swift'] = trim($_POST['swift']);
$_SESSION['refernce'] = trim($_POST['reference']);
$_SESSION['existBen'] = $_POST['existBen'];
$_SESSION['errorIBAN'] = "IBAN number is not valid";
header("LOCATION: select_beneficiary_trans.php");
}*/
	}

	{
//debug('false', true);
		/**************Geting Guzatted Holidays****************/
		$holidayQuery = "select date from " . TBL_GUZATTED_HOLIDAY . " order by date";
		$holidayResult = selectMultiRecords($holidayQuery);

		$allCount = count($holidayResult);

		$intTransDataFlag = true;
		$strBenName = trim($_POST['benName']);
		$strBenCountryID = $_POST['benCountry'];
		$strQueryCountry = "SELECT countryName FROM " . TBL_COUNTRIES . " WHERE countryId = '$strBenCountryID'";
		$arrCountry = selectFrom($strQueryCountry);
		$strBenCountry = $arrCountry['countryName'];
		$strBenCurrency = trim($_POST['benCurrency']);
		if (isset($_POST['accountName'])) {
			$strBenAccountName = trim($_POST['accountName']);
		} else {
			$strBenAccountName = '';
		}

		if (isset($_POST['accountNumber'])) {
			$strBenAccountNumber = trim($_POST['accountNumber']);
		} else {
			$strBenAccountNumber = '';
		}

		if (isset($_SESSION['branchNameNumber'])) {
			$strBranchNameNumber = trim($_SESSION['branchNameNumber']);
		} else {
			$strBranchNameNumber = '';
		}

		if (isset($_SESSION['branchAddress'])) {
			$strBranchAddress = trim($_SESSION['branchAddress']);
		} else {
			$strBranchAddress = '';
		}

		if (isset($_POST['sortCode'])) {
			$strSortCode = trim($_POST['sortCode']);
		} else {
			$strSortCode = '';
		}

		if (isset($_POST['routingNumber'])) {
			$strRoutingNumber = trim($_POST['routingNumber']);
		} else {
			$strRoutingNumber = '';
		}

		if (isset($_POST['swift'])) {
			$strSwiftCode = trim($_POST['swift']);
		} else {
			$strSwiftCode = '';
		}

		$strTip = trim($_POST['tip']);
		$strReference = trim($_POST['reference']);
		$intBenID = $_POST['existBen'];
		$intCustomerID = $_SESSION['loggedInUser']['userID'];
		$intExchangeRate = $_SESSION['exchangeRate'];
		$strSendCurrency = $_POST['sendingCurrency'];
		$strSendingCurrency = $_SESSION['sendingCurrency'];
		$strReceivingCurrency = $_SESSION['receivingCurrency'];
		$strSendingAmount = $_SESSION['sendingAmount'];
		$strReceivingAmount = $_SESSION['receivingAmount'];
		$_SESSION["benBame"];
		$_SESSION["firstBen"];
		$_SESSION["secondBen"];
		$strCurrentTimeStamp = date("Y-m-d h:i:s");
		$valueDate = date('Y-m-d H:i:s', strtotime($strCurrentTimeStamp . ' + 2 day'));
		$valueDateWithoutTime = date('Y-m-d', strtotime($valueDate));
		$dateflag = true;
		while ($dateflag != false) {
			$dateflag = false;
			for ($i = 0; $i < $allCount; $i++) {
				if ($valueDateWithoutTime == $holidayResult[$i]['date']) {
					$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 1 day'));
					$valueDateWithoutTime = date('Y-m-d', strtotime($valueDate));
					$dateflag = true;

				}

			}

			$valueDateDay = date('l', strtotime($valueDate));
			if ($valueDateDay == "Saturday") {
				$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 2 day'));
				$dateflag = true;

			} else if ($valueDateDay == "Sunday") {
				$valueDate = date('Y-m-d H:i:s', strtotime($valueDate . ' + 1 day'));
				$dateflag = true;

			}
		}

		$strCurrentDate = date("Y-m-d");
		$strIP = $_SERVER['REMOTE_ADDR'];
		$strMsg = '';
		$strError = '';

		if (!empty($_POST['existBen'])) {

			$strQueryUpdateBen = "UPDATE " . TBL_BENEFICIARY . " SET ";
			if (!empty($strBenName)) {
				$strQueryUpdateBen .= "`firstName` = '$strBenName',";
			}
			if (!empty($strBenName)) {
				$strQueryUpdateBen .= "`beneficiaryName` = '$strBenName',";
			}
			if (!empty($strBenCountry)) {
				$strQueryUpdateBen .= "`Country` = '$strBenCountry',";
			}

			if (!empty($strReference)) {
				$strQueryUpdateBen .= "`reference` = '$strReference',";
			}
			$strQueryUpdateBen = substr($strQueryUpdateBen, 0, strlen($strQueryUpdateBen) - 1);

			$strQueryUpdateBen .= " WHERE benID = '$intBenID' AND customerID = '$intCustomerID'";
			if (update($strQueryUpdateBen)) {
				$strMsg = "Beneficiary updated successfully";
				$intinsertedID = "";
				$strInsertionquery = "";
				$strInsertionquery = urlencode(base64_encode($strQueryUpdateBen));
				onlineactivities($intinsertedID, $strInsertionquery);
			} else {
				$strError = "Beneficiary cannot be updated. please try again.";
			}

		} else {

			//beneficiary should not be added for topup

			if ($_SESSION["loggedInUser"]["online_customer_services"] == "TC") {

				$strQueryInsertBen = "INSERT INTO " . TBL_BENEFICIARY . " (`firstName`, `middleName`, `lastName`,`created`, `beneficiaryName`, `Country`, `reference`, `customerID`) VALUES('$strBenName', '', '','$strCurrentDate', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID')";

				$benAded = insertInto($strQueryInsertBen);

				if ($benAded) {
					$strMsg = "Beneficiary added successfully";
					$intBenID = mysql_insert_id();

					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($strQueryInsertBen));
					onlineactivities($intBenID, $strInsertionquery);
				} else {
					$strError = "Beneficiary cannot be added successfully";
				}
			}
		}
		if ($_SESSION["loggedInUser"]["online_customer_services"] != "TC") {
			$intBenID = -1;
		}

		if (!empty($intBenID)) {
			$strQueryChkBankInfo = "SELECT count(id) AS record FROM " . TBL_BEN_BANK_DETAILS . " WHERE benID = '$intBenID'";
			$arrBenBankInfo = selectFrom($strQueryChkBankInfo);
			if ($arrBenBankInfo['record'] == 0) {

				$strQueryInsertBenBank = "INSERT INTO " . TBL_BEN_BANK_DETAILS . " (`benId`, `IBAN`, `swiftCode`, `bankName`, `accountNo`, `branchCode`, `branchAddress`, `sortCode`, `routingNumber`,sendCurrency) VALUES('$intBenID', '$strIBAN', '$strSwiftCode', '$strBenAccountName', '$strBenAccountNumber', '$strBranchNameNumber', '$strBranchAddress', '$strSortCode', '$strRoutingNumber','$strSendCurrency')";
				if (insertInto($strQueryInsertBenBank)) {
					$strMsg .= " and beneficiary bank details are added against the beneficiary";
					$intinsertedID = mysql_insert_id();
					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($strQueryInsertBenBank));
					onlineactivities($intinsertedID, $strInsertionquery);
				} else {
					$strError .= " and beneficiary bank info cannot added.";
				}

			} else {
				$strQueryUpdateBenBank = "UPDATE " . TBL_BEN_BANK_DETAILS . " SET";
				if (!empty($strIBAN)) {
					$strQueryUpdateBenBank .= "`IBAN` = '$strIBAN',";
				}
				if (!empty($strSwiftCode)) {
					$strQueryUpdateBenBank .= "`swiftCode` = '$strSwiftCode',";
				}
				if (!empty($strBenAccountName)) {
					$strQueryUpdateBenBank .= "`bankName` = '$strBenAccountName',";
				}
				if (!empty($strBenAccountNumber)) {
					$strQueryUpdateBenBank .= "`accountNo` = '$strBenAccountNumber',";
				}
				if (!empty($strBranchNameNumber)) {
					$strQueryUpdateBenBank .= "`branchCode` = '$strBranchNameNumber',";
				}
				if (!empty($strBranchAddress)) {
					$strQueryUpdateBenBank .= "`branchAddress` = '$strBranchAddress',";
				}
				if (!empty($strSortCode)) {
					$strQueryUpdateBenBank .= "`sortCode` = '$strSortCode',";
				}
				if (!empty($strRoutingNumber)) {
					$strQueryUpdateBenBank .= "`routingNumber` = '$strRoutingNumber',";
				}
				if (!empty($strSendCurrency)) {
					$strQueryUpdateBenBank .= "`sendCurrency` = '$strSendCurrency',";
				}
				$strQueryUpdateBenBank = substr($strQueryUpdateBenBank, 0, strlen($strQueryUpdateBenBank) - 1);
				$strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";
				if (update($strQueryUpdateBenBank)) {
					$strMsg .= " and beneficiary bank details are updated";
					$intinsertedID = "";
					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($strQueryUpdateBenBank));
					onlineactivities($intinsertedID, $strInsertionquery);
				} else {
					$strError .= " and beneficiary bank info cannot be updated";
				}

			}
			$getCustAgent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='" . $intCustomerID . "'");
			$name = $getCustAgent["firstName"] . " " . $getCustAgent["lastName"];
			$strRefNumber = generateRefNumber();

			$transTypeStatus = "";
			if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC") {
				$transTypeStatus = 'Topup';

			} else {
				$transTypeStatus = "Bank Transfer";
			}
// balance ledger for online customer
			$querycustBalance = selectFrom("select balance from " . TBL_CUSTOMER . " where customerID = '" . $intCustomerID . "'");
			if ($querycustBalance['balance'] == 0) {
				$addBalance = "-" . $strSendingAmount;
				$updateBalance = update("update " . TBL_CUSTOMER . " set balance= '" . $addBalance . "' where customerID = '" . $intCustomerID . "'");
			} else {
				$newBalance = $querycustBalance['balance'] - $strSendingAmount;
				$updateBalance = update("update " . TBL_CUSTOMER . " set balance= '" . $newBalance . "' where customerID = '" . $intCustomerID . "'");
			}

			$strQueryTrans = "INSERT INTO " . TBL_TRANSACTIONS . " (`transType`, `customerID`, `benID`, `custAgentID`, `benAgentID`, `refNumberIM`, `transAmount`, `exchangeRate`, `localAmount`, `totalAmount`, `transStatus`, `transDate`, `valueDate`,`validValue`, `currencyFrom`, `currencyTo`, `transDetails`, `agentExchangeRate`, `trans_source`, `transIP`,`tip`,`remTransAmount`,addedBy,modeOfPayment) VALUES('$transTypeStatus', '$intCustomerID', '$intBenID', '" . $getCustAgent['agentID'] . "', '', '$strRefNumber', '$strSendingAmount', '$intExchangeRate', '$strReceivingAmount', '$strSendingAmount', 'Pending', '$strCurrentTimeStamp', '$valueDate','N', '$strSendingCurrency', '$strReceivingCurrency', 'Y', 'N', 'O', '$strIP','$strReference', '$strSendingAmount','" . $name . "','$PaymentMode')";
			if (insertInto($strQueryTrans)) {
				$intTransID = mysql_insert_id();
				$strInsertionquery = "";
				$strInsertionquery = urlencode(base64_encode($strQueryTrans));
				onlineactivities($intTransID, $strInsertionquery);

				$querNewlyAddedTrans = "SELECT * FROM " . TBL_TRANSACTIONS . " WHERE 1 ORDER By transID DESC LIMIT 1 ";
				$getNewlyAddedTrans = selectFrom($querNewlyAddedTrans);

				if ($_POST["proceed"] = "Confirm Transaction") {
					activities($_SESSION["loginHistoryID"], "INSERTION", $getNewlyAddedTrans["transID"], "transactions", "transaction " . $getNewlyAddedTrans["refNumberIM"] . " is added");
				}

				$querAgentCustAct = "INSERT INTO agents_customer_account  (`customerID`,`Date`,`tranRefNo`,`payment_mode`,`Type`,`amount`,`currencytitle`) VALUE ('" . $_SESSION["loggedInUser"]["userID"] . "',CURDATE(),'" . $intTransID . "','Transaction is Created','WITHDRAW','" . $_SESSION["sendingAmount"] . "','" . $_SESSION["sendingCurrency"] . "')";

				if (insertInto($querAgentCustAct) or die("something wrong with insert query")) {
					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($querAgentCustAct));
					$intinsertedID = mysql_insert_id();
					onlineactivities($intinsertedID, $strInsertionquery);
				}

/************agent_company_account************/
				$querAgentComtAct = "INSERT INTO " . TBL_AGENT_COMPANY_ACCOUNT . "  ( `Date`,`tranRefNo`,`payment_mode`,`Type`,`amount`,`currencytitle`) VALUE ( CURDATE(),'" . $intTransID . "','Transaction is Created','DEPOSIT','" . $_SESSION["sendingAmount"] . "','" . $_SESSION["sendingCurrency"] . "')";
				if (insertInto($querAgentComtAct) or die("something wrong with insert query")) {
					$intinsertedID = mysql_insert_id();
					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($querAgentComtAct));
					onlineactivities($intinsertedID, $strInsertionquery);
				}

				$strQueryBankDetails = "INSERT INTO " . TBL_BANK_DETAILS . " (`benID`, `transID`, `bankName`, `accNo`, `IBAN`, `swiftCode`, `branchCode`, `branchAddress`, `routingNumber`, `sortCode`) VALUES('$intBenID', '$intTransID', '$strBenAccountName', '$strBenAccountNumber', '$strIBAN', '$strSwiftCode', '$strBranchNameNumber', '$strBranchAddress', '$strRoutingNumber', '$strSortCode')";
				if (insertInto($strQueryBankDetails)) {
					$intinsertedID = mysql_insert_id();
					$strInsertionquery = "";
					$strInsertionquery = urlencode(base64_encode($strQueryBankDetails));
					onlineactivities($intinsertedID, $strInsertionquery);
				}
				update("UPDATE " . TBL_CUSTOMER . " SET no_of_transactions= no_of_transactions+1 WHERE customerID=$intCustomerID ");
				$strMsg = "Transaction Created Successfully. Reference Number: $strRefNumber";
//$strMsg = "Transaction Created Successfully. Reference Number: $strTip";
				update("update transactions set moneyPaid='By Bank Transfer' where transID='" . $intTransID . "' ");
				include_once 'sendmail-premier-online-pdf.php';

				unset($_SESSION['exchangeRate']);
				unset($_SESSION['sendingCurrency']);
				unset($_SESSION['receivingCurrency']);
				unset($_SESSION['sendingAmount']);
				unset($_SESSION['receivingAmount']);

			}
		}
	}
} else {
//header("LOCATION: make_payment-ii-new.php");

	if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC" || $_SESSION["loggedInUser"]["online_customer_services"] == "TC,PC") {
		header("LOCATION: topup.php");

	} else {

		header("LOCATION: make_payment-ii-new.php");
	}
}

// $queryBankDetails = "select * from accounts where currency = '" . $strSendingCurrency . "' ";
// $arrBankRecord = selectFrom($queryBankDetails);

$queryBankDetails = "select * from accounts where 1=1  ANd userID='".$getCustAgent['agentID']."'  AND currency = '" . $strSendingCurrency . "'  ANd showOnDeal='Y'  ORDER by id DESC LIMIT 1 ";



$arrBankRecord = selectFrom($queryBankDetails);
if($arrBankRecord['showOnDeal']!='Y')
{
$queryBankDetails = "select * from accounts where 1=1  ANd userID='".$getCustAgent['agentID']."'  AND currency = '" . $strSendingCurrency . "'    ORDER by id DESC LIMIT 1 ";
$arrBankRecord = selectFrom($queryBankDetails); 

}


$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];

?>
<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>New Transaction - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Send Money</h1>
                        <p class="page-header-desc">Make payment</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Transactions</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Send money</h2>
                        <p class="mt5">Your transaction has been successfully created reference number <?= $strRefNumber; ?>.</p>
                        <p class="mt5">You will shortly receive a confirmation of your trade via e-mail with our bank details.</p></p>
                        <p class="mt5">We will confirm the credit on your card once we receive your funds.</p>
                        <?php
                            if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC") {
                                echo '<script type="text/javascript">emailTopup("' . $strRefNumber . '");</script>';
                            }
                            ?>

                        <?php if ($PaymentMode != 'card'): ?>
                            <p class="alert alert-danger">
                                You must now instruct your bank to send the funds to us, using the details below.<br>We do not take the funds from your account, you must send them to us.
                            </p>
                        <?php endif; ?>

                        <form action="" method="post">
                            <?php if ($PaymentMode != 'card'): ?>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h3>Payment details</h3>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Bank name:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["bankName"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Account name:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["accountName"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Account number:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["accounNumber"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">Sort code:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["sortCode"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">IBAN number:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["IBAN"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc mt10">SWIFT Code:</label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-control"><?= $arrBankRecord["swiftCode"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <!-- ROW -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <p>This deal has now been booked into the market and no further amendments can be made.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <p>This deal has now been booked into the market and no further amendments can be made.</p>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group mb5">
                                        <? if ($_SESSION["loggedInUser"]["online_customer_services"] == "PC") { ?>
                                            <input class="btn btn-custom" name="proceed" id="proceed" type="button" value="New Transaction"
                                                   onClick="window.location.href = 'topup.php';"/>
                                        <? } else { ?>
                                            <input class="btn btn-custom" name="proceed" id="proceed" type="button" value="New Transaction"
                                                   onClick="window.location.href = 'make_payment-ii-new.php';"/>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div><!-- End .form-wrapper -->
                </div><!-- End .col-sm-6 -->
                <div class="mb40 visible-xs"></div><!-- space -->
                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
                    <p>Send money safely and securely with <?= $title; ?> Online.</p>
                    <div class="mb10"></div><!-- space -->
                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->

        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script type="text/javascript">
    /* WORK DONE AGAINST 13137 EMAIL TOP UP  ###### ARSLAN ZAFAR */

    function emailTopup(transID) {
        $.ajax({
            url: './ajaxCalls.php',
            data: {
                rtypeMode: 'emailTopup',
                transID: transID,
            },
            success: function (data) {
            }
        });
    }
</script>
</body>
</html>