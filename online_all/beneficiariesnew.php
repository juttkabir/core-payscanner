<?php
use Payex\Online\Service\CustomerService;
use Payex\Repository\CustomerRepository;

session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php");
}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];
$customerService = new CustomerService($online->getDb());
$customerRepository = new CustomerRepository($online->getDb());
$customer = $customerRepository->getById($_SESSION['loggedInUser']['userID']);
$beneficiariesList = $customerService->getBeneficiariesListWithBankInfo($customer);
//end of new logic

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";
include_once "includes/functions.php";
$benCountries = "select countryId,countryName,isocode,ibanlength from countries";
$strSendingCurrency = $_POST['sendingCurrency'];
$strReceivingCurrency = $_POST['receivingCurrency'];
$fltExchangeRate = $_POST['exchangeRate'];
if ($_POST["accept"] == "ACCEPT QUOTE") {
    activities($_SESSION["loggedInUser"]["userID"], "Accept Quote", "", "", "Quote accepted for " . $strSendingCurrency . " - " . $strReceivingCurrency . " = " . $fltExchangeRate . "");
}

$benCountriesResult = selectMultiRecords($benCountries);
$intValid = false;
$recievingCurrency = $_POST['receivingCurrency'];
if (!empty($_POST['receivingAmount']) && $_POST['receivingAmount'] > '0' && !empty($_POST['sendingAmount']) && $_POST['sendingAmount'] > '0' && !empty($_POST['receivingCurrency']) && !empty($_POST['sendingCurrency']) && !empty($_POST['exchangeRate'])) {
    $intValid = true;
    $_SESSION['receivingAmount'] = $_POST['receivingAmount'];
    $_SESSION['sendingAmount'] = $_POST['sendingAmount'];
    $_SESSION['receivingCurrency'] = $_POST['receivingCurrency'];
    $_SESSION['sendingCurrency'] = $_POST['sendingCurrency'];
    $_SESSION['exchangeRate'] = $_POST['exchangeRate'];
    $dateTime = date("Y-m-d h:i:s");
    $strQueryInsertAccept_Quote = "INSERT INTO acceptQuoteHistory (custID,buy_currency,sell_currency,buy_amount,sell_amount,rate,dateTime) VALUES('" . $_SESSION['loggedInUser']['userID'] . "','" . $_POST['sendingCurrency'] . "','" . $_POST['receivingCurrency'] . "','" . $_POST['sendingAmount'] . "','" . $_POST['receivingAmount'] . "','" . $_POST['exchangeRate'] . "','$dateTime')";
    insertInto($strQueryInsertAccept_Quote);
}
if (!empty($_SESSION['receivingAmount']) && !empty($_SESSION['sendingAmount']) && !empty($_SESSION['receivingCurrency']) && !empty($_SESSION['sendingCurrency']) && !empty($_SESSION['exchangeRate'])) {
    $intValid = true;
    $strReceivingAmount = $_SESSION['receivingAmount'];
    $strSendingAmount = $_SESSION['sendingAmount'];
    $strReceivingCurrency = $_SESSION['receivingCurrency'];
    $strSendingCurrency = $_SESSION['sendingCurrency'];
    $strExchangeRate = $_SESSION['exchangeRate'];
}
if (!empty($_SESSION['errorIBAN'])) {
    $intValid = true;
    $intError = true;
    $strBenName = $_SESSION['benName'];
    $strBenCountry = $_SESSION['benCountry'];
    $strBenCurrency = $_SESSION['benCurrency'];
    $strSwift = $_SESSION['swift'];
    $strReference = $_SESSION['refernce'];
    $strExistBen = $_SESSION['existBen'];
    $strErrorIBAN = $_SESSION['errorIBAN'];

    unset($_SESSION['benCountry']);
    unset($_SESSION['benCurrency']);
    unset($_SESSION['swift']);
    unset($_SESSION['refernce']);
    unset($_SESSION['existBen']);
    unset($_SESSION['errorIBAN']);
}
$url = $_POST['url'];

$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];

?>
<?php
if (empty($strUserId)) {
    $strUserId = $_SESSION[loggedInUser][userID];
}

$strAccountName = $_SESSION[loggedInUser][accountName];

if (empty($strEmail)) {
    $strEmail = $_SESSION[loggedInUser][email];
}
?>

<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Make payment - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Send Money</h1>
                        <p class="page-header-desc">Make payment</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Transactions</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">



            <div class="row">
                <div class="col-sm-8">
                    <?php if ($intError):?>
                        <div class="alert alert-danger error_msg">
                            <?php echo $strErrorIBAN; ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Send Money</h2>
                        <p>Choose a beneficiary account to receive the money, if you have not sent money to this account before please create a new beneficiary using the form below.</p>
                        <form id="benForTransaction" action="transactionnew.php" method="post" onSubmit="return checkAgree();" autocomplete="off">

                            <div class="row">
                                <!-- col 0 -->
                                <div class="col-sm-12">

                                    <div class="row">
                                        <div class="col-sm-6">

                                            <div class="form-group">
                                                <h4>Beneficiary information</h4>
                                            </div>

                                            <div class="form-group">
                                                <label for="benCountry" class="input-desc">Existing Beneficiary:</label>
                                                <select name="existBen" id="existBen" class="form-control select_field" onChange="this.form.action = '<?php echo $_SERVER['PHP_SELF']; ?>'; this.form.submit();">
                                                    <option value="">- Select beneficiary for transaction -</option>
                                                    <?php
                                                    foreach ($beneficiariesList as $item){
                                                        echo '<option value="' . $item['benID'] . '">' . $item['bankName'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div><!-- End .from-group -->

                                            <?php
                                            $intBenExist = false;
                                            if (!empty($_POST['existBen']) || isset($_SESSION['ben'])) {
                                                if (!empty($_POST['existBen'])) {
                                                    $intBenID = trim($_POST['existBen']);
                                                } elseif (!empty($_SESSION['ben'])) {
                                                    $intBenID = $_SESSION['ben'];
                                                }

                                                $intBenExist = true;

                                                $strQueryBenInfo = "SELECT countries.countryId, Country, firstName AS benName, benBankDetails.IBAN AS IBAN, benBankDetails.swiftCode AS swiftCode,benBankDetails.sortCode AS sortCode,benBankDetails.bankName AS accountName,benBankDetails.branchAddress  AS branchAddress,benBankDetails.accountNo AS accountNumber,benBankDetails.routingNumber AS routingNumber,reference,benBankDetails.sendCurrency as sendCurrency,benBankDetails.branchCode as branchCode  FROM " . TBL_BENEFICIARY . " AS ben LEFT JOIN " . TBL_BEN_BANK_DETAILS . " AS benBankDetails ON ben.benID = benBankDetails.benID left join countries on ben.Country=countries.countryName WHERE ben.benID = '$intBenID'";
                                                $arrBenInfo = selectFrom($strQueryBenInfo);

                                                $strSendCurrency = $arrBenInfo['sendCurrency'];
                                            }
                                            ?>

                                            <div class="form-group">
                                                <?php if (!$intBenExist): ?>
                                                    <label>OR<br>Add a new beneficiary</label>
                                                <?php else: ?>
                                                    <label>OR<br></label>
                                                <?php endif; ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <!-- col 1 -->
                                <div class="col-sm-6 content_area_left">
                                    <div class="row">
                                        <div class="col-sm-12">


                                            <div class="form-group">
                                                <h4>Beneficiary information</h4>
                                            </div>

                                            <div class="form-group">
                                                <label for="benCountry" class="input-desc">Beneficiary Country:</label>
                                                <select name="benCountry" id="benCountry" class="form-control select_field">
                                                    <option value="">- Select country for beneficiary -</option>
                                                    <?php
                                                    $strQueryCountries = "select distinct(countryName),countryId  from countries where 1";
                                                    $arrCountry = selectMultiRecords($strQueryCountries);
                                                    for ($x = 0; $x < count($arrCountry); $x++) {
                                                        echo "<option value='" . $arrCountry[$x]['countryId'] . "'>" . $arrCountry[$x]['countryName'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <small id="spanBenCountry" style="display: none"><font color="red">This field is required.</font></small>
                                            </div>

                                            <div class="form-group mb10">
                                                <label for="sendingCurrency" class="input-desc">Currency You Wish To Send:</label>
                                                <input name="sendingCurrency" id="sendingCurrency" type="text" maxlength="40" value="<?= $_SESSION["receivingCurrency"]; ?>" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanSendingCurrency" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group" id="accountNameCont">
                                                <label for="benName" class="input-desc">Beneficiary Account Name:</label>
                                                <input name="accountName" id="accountName" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanAccountName" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="accountNumberCont">
                                                <label for="accountNumber" class="input-desc">Account Number:</label>
                                                <input name="accountNumber" id="accountNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanAccountNumber" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group" id="branchNameNumberCont">
                                                <label for="branchNameNumber" class="input-desc">Branch Name / Number:</label>
                                                <input name="branchNameNumber" id="branchNameNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanBranchNameNumber" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="branchAddressCont">
                                                <label for="branchAddress" class="input-desc">Branch Address:</label>
                                                <input name="branchAddress" id="branchAddress" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanBranchAddress" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->



                                        </div>

                                    </div>
                                </div>

                                <!-- col 2 -->
                                <div class="col-sm-6 content_area_right">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <h4>&nbsp;</h4>
                                            </div>


                                            <div class="form-group" id="routingNumberCont">
                                                <label for="routingNumber" class="input-desc">Routing Number:</label>
                                                <input name="routingNumber" id="routingNumber" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanRoutingNumber" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb10" id="sortCodeCont">
                                                <label for="sortCode" class="input-desc">Sort Code:</label>
                                                <input name="sortCode" id="sortCode" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanSortCode" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div id="chackboxnew" style="display:none;" class="field_wrapper">
                                                <input id="chackbox_new" name="chackbox_new" type="checkbox" value="chackbox_new"
                                                       onclick="CheckValidation(this)" class="bottom" />
                                                <span class="customer_rn_vl" style="font:size:11px;">Please mark the check box if u
                                                            want to add IBAN instead of SortCode and Account No.</span>
                                            </div>

                                            <div class="form-group" id="ibanCont">
                                                <label for="iban" class="input-desc">IBAN Number:</label>
                                                <input name="iban" id="iban" type="text" maxlength="40" onBlur = "checkIBANvalid()" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanIBAN" style="display: none"><font color="red">This field is required.</font></small>
                                                <input name="isiban" id = "isiban" type="hidden" />
                                            </div><!-- End .from-group -->

                                            <div class="form-group mb40" id="swiftCont">
                                                <label for="swift" class="input-desc">SWIFT Code:</label>
                                                <input name="swift" id="swift" type="text" maxlength="40" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanSwift" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->

                                            <div class="form-group">
                                                <label for="reference" class="input-desc">Your Reference:</label>
                                                <input name="reference" id="reference" type="text" maxlength="15" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                                <small id="spanReference" style="display: none"><font color="red">This field is required.</font></small>
                                            </div><!-- End .from-group -->



                                        </div>

                                    </div>
                                </div>



                                <div class="col-sm-12">

                                    <div class="form-group mt15-r">
                                        <div class="checkbox">
                                            <label class="custom-checkbox-wrapper">
                                                    <span class="custom-checkbox-container">
                                                        <input type="checkbox" id="agreeBenInfo" name="agreeBenInfo" value="agreeBenInfo">
                                                        <span class="custom-checkbox-icon"></span>
                                                    </span>
                                                <span> I confirm that the details above are correct and I understand that a fee of £30 will be charged if a payment is to be returned.</span>
                                            </label>
                                            <span class="myspanclass" id="spanagreebenInfo"><font color="red">Please check the confirmation of beneficiary details correction!</font></span>
                                        </div><!-- End .checkbox -->
                                    </div>


                                    <div class="form-group mb5">

                                        <input name="back" id="back" type="button" value="Go back" class="btn btn-custom mr10 submit_btn" onClick="this.form.action='make_paymentnew.php';this.form.submit();" />
                                        <input name="goBack" value="back" type="hidden"/>

                                        <input name="clear" id="clear" type="reset" value="Clear details" class="btn btn-default mr10 submit_btn" />

                                        <input name="proceed" id="proceed" type="submit" value="Continue" class="btn btn-custom mr10 submit_btn" onclick="return checkValidations();" />

                                        <input name="receivingAmount" type="hidden" value="<?php echo $strReceivingAmount; ?>"/>
                                        <input name="sendingAmount" type="hidden" value="<?php echo $strSendingAmount; ?>"/>
                                        <input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; ?>"/>

                                        <input name="exchangeRate" type="hidden" value="<?php echo $strExchangeRate; ?>"/>
                                        <input name="strSendCurrency" type="hidden" value="<?php //echo $strSendCurrency; ?>"/>
                                        <input name="sendBenValue" id = "sendBenValue" type="hidden" />
                                    </div><!-- End .from-group -->
                                </div>

                            </div>
                        </form>

                    </div><!-- End .form-wrapper -->
                </div><!-- End .col-sm-6 -->
                <div class="mb40 visible-xs"></div><!-- space -->
                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
                    <p>Send money safely and securely with <?= $title; ?> Online.</p>
                    <div class="mb10"></div><!-- space -->
                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->




            <!--  START OLD PAGE -->

            <?php
            if (isset($arrBenInfo['countryId'])) {

                $arrBenBankRules = benBankDetailRule($arrBenInfo['countryId']);
                $strBenName = "Name: " . $arrBenInfo["benName"];
                $accountName = $arrBenBankRules["accountName"];
                $accNo = $arrBenBankRules["accNo"];
                $branchNameNumber = $arrBenBankRules["branchNameNumber"];
                $branchAddress = $arrBenBankRules["branchAddress"];
                $swiftCode = $arrBenBankRules["swiftCode"];
                $IBAN = $arrBenBankRules["iban"];
                $routingNumber = $arrBenBankRules["routingNumber"];
                $sortCode = $arrBenBankRules["sortCode"];
                $strBenDetailFirst = "";

                $strBenDetailSecond = "";


                if ($accountName == "Y") {

                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Account Name: " . $arrBenInfo["accountName"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Account Name: " . $arrBenInfo["accountName"];

                    }
                }
                if ($accNo == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Account No: " . $arrBenInfo["accountNumber"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Account No: " . $arrBenInfo["accountNumber"];

                    }
                }
                if ($branchNameNumber == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Branch Name: " . $arrBenInfo["branchName"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Branch Name: " . $arrBenInfo["branchName"];

                    }
                }
                if ($branchAddress == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Branch Address: " . $arrBenInfo["branchAddress"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Branch Address: " . $arrBenInfo["branchAddress"];

                    }
                }
                if ($swiftCode == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Swift Code: " . $arrBenInfo["swiftCode"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Swift Code: " . $arrBenInfo["swiftCode"];

                    }
                }
                if ($IBAN == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "IBAN: " . $arrBenInfo["IBAN"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "IBAN: " . $arrBenInfo["IBAN"];

                    }
                }
                if ($routingNumber == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Routing Number: " . $arrBenInfo["routingNumber"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Routing Number: " . $arrBenInfo["routingNumber"];

                    }
                }
                if ($sortCode == "Y") {
                    if ($strBenDetailFirst == "") {
                        $strBenDetailFirst = "Sort Code: " . $arrBenInfo["sortCode"];

                    } else if ($strBenDetailSecond == "") {
                        $strBenDetailSecond = "Sort Code: " . $arrBenInfo["sortCode"];

                    }
                }

                $_SESSION["benBame"] = $strBenName;
                $_SESSION["firstBen"] = $strBenDetailFirst;
                $_SESSION["secondBen"] = $strBenDetailSecond;
            }
            ?>
            </form>





        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>

<script>
    <?php

    /*$strCountries = "{";
    for($ii=0;$ii<count($benCountriesResult);$ii++)
    {
    $arrCount[] = "'".$benCountriesResult[$ii]["countryName"]."':{'c':'".$benCountriesResult[$ii]["countryName"]."','i':'".$benCountriesResult[$ii]["isocode"]."','g':'".$benCountriesResult[$ii]["ibanlength"]."'}";
    //$strCountries .=
    }
    $strCountries .= implode(",",$arrCount);
    $strCountries .= "}";*/
    $strCountries = "Array(";
    for ($ii = 0; $ii < count($benCountriesResult); $ii++) {
        $arrCount[] = "Array('" . strtoupper($benCountriesResult[$ii]["countryName"]) . "','" . strtoupper($benCountriesResult[$ii]["isocode"]) . "','" . $benCountriesResult[$ii]["ibanlength"] . "','" . $benCountriesResult[$ii]["countryId"] . "')";
//$strCountries .=
    }
    $strCountries .= implode(",", $arrCount);
    $strCountries .= ")";

    echo 'var arrCountrues =' . $strCountries . ';';

    ?>

    function isValidIBAN($v){ //This function check if the checksum if correct
        $v = $v.replace(/^(.{4})(.*)$/,"$2$1"); //Move the first 4 chars from left to the right
        $v = $v.replace(/[A-Z]/g,function($e){return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10}); //Convert A-Z to 10-25
        var $sum = 0;
        var $ei = 1; //First exponent
        for(var $i = $v.length - 1; $i >= 0; $i--){
            $sum += $ei * parseInt($v.charAt($i),10); //multiply the digit by it's exponent
            $ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
        };
        return $sum % 97 == 1;
    }

    var $patterns = { //Map automatic generated by IBAN-Patterns Online Tool
        IBAN: function($v){
            $v = $v.replace(/[ ]/g,'');
            return /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/.test($v) && isValidIBAN($v);
        },
    };


    function SelectOption(OptionListName, ListVal) {
        for (i = 0; i < OptionListName.length; i++) {
            if (OptionListName.options[i].value == ListVal) {
                OptionListName.selectedIndex = i;
                break;
            }
        }
    }

    $('#countryTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#currencyTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#benTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#benNameTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#accountNameTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#accountNumberTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#branchNameNumberTip').popover({
        trigger: 'hover',
        offset: 5
    });

    $('#branchAddressTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#routingNumberTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#sortCodeTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#ibanTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#swiftTip').popover({
        trigger: 'hover',
        offset: 5
    });
    $('#refTip').popover({
        trigger: 'hover',
        offset: 5
    });

    <?php
    if ($intBenExist) {
    ?>
    SelectOption($("#benForTransaction")[0].existBen, '<?php echo $intBenID; ?>');
    SelectOption($("#benForTransaction")[0].benCountry, '<?php echo $arrBenInfo['countryId']; ?>');
    <? $_SESSION["ben"] = $intBenID;?>
    //$('#sendingCurrency').val('<?php echo $strSendCurrency; ?>');
    $("#sendBenValue").val($("#benCountry option:selected").val());
    $('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
    $('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
    $('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
    $('#routingNumber').val('<?php echo $arrBenInfo["routingNumber"]; ?>');
    $('#branchNameNumber').val('<?php echo $arrBenInfo["branchCode"]; ?>');
    $('#branchAddress').val('<?php echo $arrBenInfo["branchAddress"]; ?>');

    //$('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
    $('#iban').val('<?php echo $arrBenInfo['IBAN']; ?>');
    $('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
    $('#sortCode').val('<?php echo trim($arrBenInfo['sortCode']); ?>');
    $('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
    <?php
    }
    if ($intError) {
    ?>
    SelectOption($("#benForTransaction")[0].existBen, '<?php echo $strExistBen; ?>');
    SelectOption($("#benForTransaction")[0].benCountry, '<?php echo $strBenCountry; ?>');
    $('#sendingCurrency').val('<?php echo $strSendCurrency; ?>');
    $('#benName').val('<?php echo $arrBenInfo['benName']; ?>');
    $('#accountName').val('<?php echo $arrBenInfo['accountName']; ?>');
    $('#accountNumber').val('<?php echo $arrBenInfo['accountNumber']; ?>');
    $('#benCurrency').val('<?php echo $_SESSION['sendingCurrency']; ?>');
    $('#branchNameNumber').val('<?php echo $arrBenInfo["branchCode"]; ?>');
    $('#iban').focus();
    $('#swift').val('<?php echo $arrBenInfo['swiftCode']; ?>');
    $('#sortCode').val('<?php echo $arrBenInfo['sortCode']; ?>');
    $('#reference').val('<?php echo $arrBenInfo['reference']; ?>');
    $('#branchAddress').val('<?php echo $arrBenInfo["branchAddress"]; ?>');
    <?php
    }
    ?>
    function checkAgree(){
        var iban = $('#iban').val();
        accountNumber = $('#accountNumber').val();
        sortCode = $('#sortCode').val();
        var country=$('#benCountry').val();
        var checkLength=checkIBANLength(country,iban);
        var isiban = $('#isiban').val();

        curr = $("#sendingCurrency").val();
        if (curr == 'GBP')
        {

            if ($("#accountNumber").val() != '' && $("#sortCode").val() != ''){
                return true;
            }
            if(!($('#agreeBenInfo').is(':checked'))){
                hideDisplay();
                $("#spanagreebenInfo").show();
                return false;
            }

        }
        if(checkLength==false && $('#iban').is(':visible')){
            hideDisplay();
            $("#spanIbanCorrect").show();
            return false;
            /* alert("Please enter the correct IBAN number for your beneficiary country");
             //$('#iban').focus();
             return false; */
        }

//CY17 0020 0128 0000 0012 0052 7600
        if(!$patterns.IBAN(iban) && $('#iban').is(':visible'))
        {
            hideDisplay();
            $("#spanIbanCorrect").show();
            return false;
            /* alert("Please enter the correct IBAN number for your beneficiary country");
             return false; */
        }
        else
        {

            return true;
        }

    }

    function checkIBANvalid() {
        var iban = $('#iban').val();
        var country = $('#sendBenValue').val();
        var checkLength = checkIBANLength(country, iban);
        console.log(checkLength);
        if (checkLength == false && $('#iban').is(':visible')) {
            console.log("first in");
            hideDisplay();
            $("#spanIbanCorrect").show();
            return false;
            /* alert("Please enter the correct IBAN number for your beneficiary country 1");
             return false; */
        }

        if (!$patterns.IBAN(iban) && $('#iban').is(':visible')) {
            console.log("second in");
            hideDisplay();
            $("#spanIbanCorrect").show();
            return false;

            //alert("Please enter the correct IBAN number for your beneficiary country 2");
            //return false;
        } else {
            console.log("success");
            return true;
        }
    }

    function checkIBANLength(country, iban) {
        for (var m = 0; m < arrCountrues.length; m++) {
            if (country == arrCountrues[m][3]) {
                if (iban.length != arrCountrues[m][2]) {
                    return false;
                    break;
                }
                if (iban.substr(0, 2) != arrCountrues[m][1]) {
                    return false;
                    break;
                }
            }
        }
        return true;
    }
    $(document).ready(function () {

        document.oncut = new Function("return false");
        document.oncopy = new Function("return false");
        document.onpaste = new Function("return false");

        $(".myspanclass").hide();
        getBenRule();
    });

    function getBenRule(currency) {


        currency = $("#sendingCurrency").val();
        $("#sendingCurrency").prop("disabled", true);
        $.ajax({
            url: "ben_currency_base_rule.php",
            type: "POST",
            data: {
                currency: currency,
            },
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (!data) {
                    errorTxt = "<p style='font-weight:bold;text-align:justify;'>We are sorry for the inconvenience, we are not allowing online deals in this currency. Please call support on the following numbers and place your order by phone.</p><br /><div style='font-weight:bold;'></div>";
//$('#overlay').fadeIn('slow');
                    $('#errorRule').fadeIn('slow');
                    $('#errorRule').html(errorTxt);
                } else {

                    $('#errorRule').hide();
                    accountName = data.accountName;
                    accNo = data.accNo;
                    branchNameNumber = data.branchNameNumber;
                    branchAddress = data.branchAddress;
                    swiftCode = data.swiftCode;
                    IBAN = data.iban;
                    routingNumber = data.routingNumber;
                    sortCode = data.sortCode;
//$('#sendingCurrency').val("2");
                    var getrulesval = accountName + "|" + accNo + "|" + branchNameNumber + "|" + branchAddress + "|" + swiftCode + "|" + IBAN + "|" + routingNumber + "|" + sortCode;
//if(country != 'United Kingdom')
                    {
                        if (accountName == 'N') {
                            $('#accountName').attr('disabled', 'disabled');

                            $('#accountNameCont').fadeOut('fast');

                        } else if (accountName == 'Y') {
                            $('#accountName').removeAttr('disabled');
                            $('#accountNameCont').fadeIn('fast');

                        }
                    }
                    if (accNo == 'N') {
                        $('#accountNumber').attr('disabled', 'disabled');
                        $('#accountNumberCont').fadeOut('fast');
                    } else if (accNo == 'Y') {
                        $('#accountNumber').removeAttr('disabled');
                        $('#accountNumberCont').fadeIn('fast');
                    }

                    if (branchNameNumber == 'N') {
                        $('#branchNameNumber').attr('disabled', 'disabled');
                        $('#branchNameNumberCont').fadeOut('fast');
                    } else if (branchNameNumber == 'Y') {
                        $('#branchNameNumber').removeAttr('disabled');
                        $('#branchNameNumberCont').fadeIn('fast');
                    }

                    if (branchAddress == 'N') {
                        $('#branchAddress').attr('disabled', 'disabled');
                        $('#branchAddressCont').fadeOut('fast');
                    } else if (branchAddress == 'Y') {
                        $('#branchAddress').removeAttr('disabled');
                        $('#branchAddressCont').fadeIn('fast');
                    }
//alert(country);
//if(country != 'United Kingdom')

                    {
                        if (swiftCode == 'N') {
//$('#swift').attr('disabled', 'disabled');
                            $('#swiftCont').fadeOut('fast');
                        } else if (swiftCode == 'Y') {
                            $('#swift').removeAttr('disabled');
                            $('#swiftCont').fadeIn('fast');
                        }
                    }
//alert(country);
//if(country != 1)
                    {
                        if (IBAN == 'N') {

//	$('#iban').attr('disabled', 'disabled');
                            $('#ibanCont').fadeOut('fast');
                            $('#isiban').val('NO');
                        } else if (IBAN == 'Y') {

                            $('#iban').removeAttr('disabled');
                            $('#ibanCont').fadeIn('fast');
                            $('#isiban').val('YES');
                        }
                    }
                    if (routingNumber == 'N') {
//	$('#routingNumber').attr('disabled', 'disabled');
                        $('#routingNumberCont').fadeOut('fast');
                    } else if (routingNumber == 'Y') {
                        $('#routingNumber').removeAttr('disabled');
                        $('#routingNumberCont').fadeIn('fast');
                    }

                    if (sortCode == 'N') {
//if(country != 'United Kingdom')
                        {
//	$('#sortCode').attr('disabled', 'disabled');
                        }
                        $('#sortCodeCont').fadeOut('fast');
                    } else if (sortCode == 'Y') {
                        $('#sortCode').removeAttr('disabled');
                        $('#sortCodeCont').fadeIn('fast');
                    }

                    /*	CheckValidation(getrulesval);   */
                }

            }
        });
        /* if(field == 'country'){
         onChangeCurrency(country);
         } */
    }

    $('#benForTransaction').validate({
        rules: {
            benCountry: {
                required: true
            }
        }
    });

    function check_field(id) {
        var field = $("#accountNumber").val();

        if (isNaN(field)) {
            alert('Please enter a numeric Account number');
            $("#accountNumber").val('');
        }
    }

    function CheckValidation(val){

    }
    function checkGbpRule(){
    }
    // changed against 12882 by ghazanfar
    function checkValidations() {
        curr = $("#sendingCurrency").val();
        if (curr == 'GBP') {
            if ($("#iban").val() != '' && !($('#agreeBenInfo').is(':checked'))) {
//                hideDisplay();
                $("#spanagreebenInfo").show();
                return false;
            }
            if ($("#iban").val() == '' && !($('#agreeBenInfo').is(':checked'))) {
//                hideDisplay();
                $("#spanagreebenInfo").show();
                return false;
            }
            return true;

        } else if ($('#benCountry').is(':visible') && $.trim($('#benCountry').val()) == '') {
//            hideDisplay();
//            $("#countrySpan").show();
            $('#spanBenCountry').css('display', 'inline');
            return false;
        } else if ($('#sendingCurrency').is(':visible') && $.trim($('#sendingCurrency').val()) == '') {
//            alert('Select the currency please!');
            $('#spanSendingCurrency').css('display', 'inline');
            return false;
        }
//          else if ($('#benName').is(':visible') && $.trim($('#benName').val()) == '') {
////            alert('Select the Account Name please!');
//            alert("Hello3");
//            return false;
//        }
            else if ($('#accountName').is(':visible') && $.trim($('#accountName').val()) == '') {
//            hideDisplay();
//            $("#spanAccountName").show();
            $('#spanAccountName').css('display', 'inline');
            return false;
        } else if ($('#accountNumber').is(':visible') && $.trim($('#accountNumber').val()) == '') {
//            hideDisplay();
//            $("#spanAccountNumber").show();
            $('#spanAccountNumber').css('display', 'inline');
            return false;
        } else if ($('#branchNameNumber').is(':visible') && $.trim($('#branchNameNumber').val()) == '') {
//            hideDisplay();
//            $("#spanBranchNumber").show();
            $('#spanBranchNameNumber').css('display', 'inline');
            return false;
        } else if ($('#branchAddress').is(':visible') && $.trim($('#branchAddress').val()) == '') {
//            hideDisplay();
//            $("#spanBranchAddress").show();
            $('#spanBranchAddress').css('display', 'inline');
            return false;
        } else if ($('#routingNumber').is(':visible') && $.trim($('#routingNumber').val()) == '') {
//            hideDisplay();
//            $("#spanRouting").show();
            $('#spanRoutingNumber').css('display', 'inline');
            return false;
        } else if ($('#sortCode').is(':visible') && $.trim($('#sortCode').val()) == '') {
//            hideDisplay();
//            $("#spanSortCode").show();
            $('#spanSortCode').css('display', 'inline');
            return false;
        } else if ($('#iban').is(':visible') && $.trim($('#iban').val()) == '') {
//            hideDisplay();
//            $("#spanIban").show();
            $('#spanIBAN').css('display', 'inline');
            return false;
        } else if ($('#swift').is(':visible') && $.trim($('#swift').val()) == '') {
//            hideDisplay();
//            $("#spanSwift").show();
            $('#spanSwift').css('display', 'inline');
            return false;
        } else if (!($('#agreeBenInfo').is(':checked'))) {
//            hideDisplay();
            $("#spanagreebenInfo").show();
            return false;
        } else if (!$('#accountNumber').is(':visible')) {
            $('#accountNumber').val('');
        } else if (!$('#sortCode').is(':visible')) {
            $('#sortCode').val('');
        } else if (!$('#swift').is(':visible')) {
            $('#swift').val('');
        } else if (!$('#routingNumber').is(':visible')) {
            $('#routingNumber').val('');
        } else {
            alert("Hello13");
            return true;
        }
    }

    //function to hide all validtions text
    function hideDisplay() {
        $(".myspanclass").each(function () {
            $(this).hide();
        });
    }
</script>
</html>
