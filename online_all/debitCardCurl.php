<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Payment Details
	
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php"; 
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
        $PaymentMode  =  $_SESSION["test"];
		
$log_time = date('Y-m-d  h:i:s A');

$encodedCardNo = base64_encode($_POST["CARDNO"]);

$AMOUNT =$_POST["AMOUNT"];	
$CARDNO =$_POST["CARDNO"];	
$CURRENCY =$_POST["CURRENCY"];	
$CVC =$_POST["CVC"];	
$ECI =$_POST["ECI"];	
$ED =$_POST["ED"];	
$EMAIL =$_POST["EMAIL"];	
$OPERATION =$_POST["OPERATION"];	
$ORDERID =$_POST["ORDERID"];	
$PSPID =$_POST["PSPID"];	
$PSWD =$_POST["PSWD"];	
$SHASIGN =$_POST["SHASIGN"];	
$USERID =$_POST["USERID"];

/*echo $AMOUNT ;
echo $CARDNO ;
echo $CURRENCY;
echo $CVC ;
echo $ECI ;
echo $ED ;
echo $EMAIL ;
echo $OPERATION ;
echo $ORDERID ;
echo $PSPID ;
echo $PSWD ;
echo $SHASIGN ;
echo $USERID ;
echo "<br><br>"
*/	
?>

<html>
	<head>
		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
		<link href="css/stylenew.css" rel="stylesheet" type="text/css">
		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
		
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		
		<script type="text/javascript" src="javascript/jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
	</head>

<body>

<script language="php">

$query= "select Address,Zip from customer where email='$EMAIL'";
$result= selectFrom($query);
$result[0]=str_replace(' ', '', $result[0]);
$result[1]=str_replace(' ', '', $result[1]);
//$reqValue = "ACCEPTURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_success_page.php&AMOUNT=$AMOUNT&CARDNO=$CARDNO&CURRENCY=$CURRENCY&CVC=$CVC&DESCLINEURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_error_page.php&ECI=$ECI&ED=$ED&EMAIL=$EMAIL&EXCEPTIONURL=http://premierfx.live.hbstech.co.uk/premier_fx_online_module/debit_error_page.php&FLAG3D=Y&OPERATION=$OPERATION&ORDERID=$ORDERID&OWNERADDRESS=$result[0]&OWNERZIP=$result[1]&PSPID=$PSPID&PSWD=$PSWD&SHASIGN=$SHASIGN&USERID=$USERID";
$reqValue = "AMOUNT=$AMOUNT&CARDNO=$CARDNO&CURRENCY=$CURRENCY&CVC=$CVC&ECI=$ECI&ED=$ED&EMAIL=$EMAIL&FLAG3D=Y&OPERATION=$OPERATION&ORDERID=$ORDERID&OWNERADDRESS=$result[0]&OWNERZIP=$result[1]&PSPID=$PSPID&PSWD=$PSWD&SHASIGN=$SHASIGN&USERID=$USERID";

//before
//$reqValue = "AMOUNT=$AMOUNT&CARDNO=$encodedCardNo&CURRENCY=$CURRENCY&CVC=$CVC&ECI=$ECI&ED=$ED&EMAIL=$EMAIL&OPERATION=$OPERATION&ORDERID=$ORDERID&PSPID=$PSPID&PSWD=$PSWD&SHASIGN=$SHASIGN&USERID=$USERID";

$ch = curl_init();                    // initiate curl
// Test URL 
$url = CONFIG_URL_DEBIT_CARD; 
// where you want to post data
// Live URL
//$url = "https://payments.epdq.co.uk/ncol/prod/orderdirect.asp"; // where you want to post data
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
curl_setopt($ch, CURLOPT_POSTFIELDS, $reqValue); // define what you want to post
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
//session
/*define("COOKIE_FILE", "cookie.txt");
curl_setopt($ch, CURLOPT_COOKIESESSION, true);
curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE);  //could be empty, but cause problems on some hosts
curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);*/
$output = curl_exec ($ch); // execute
$xml = simplexml_load_string($output);
//print($xml);
$order = stristr($output,"orderID");
$PAYID = stristr($output,"PAYID");
$positionendstartbodytag = strpos($PAYID);
//print($positionendstartbodytag);
$aceptance = stristr($output,"ACCEPTANCE");
$position =strpos($output,"orderID");
$PAYID =strpos($output,"PAYID");
//print($position +=9);
//echo "<br>";
//print($PAYID);

$aceptance1=substr($output,50,9);
$amount = stristr($output,"amount");
//print"<br>";
//print($aceptance1);

/*Please DONOT change this Section unless You understand This Part care fully
This Part is to get The starting index of All the response parameters and add the response parameter's length in integer in original value to get the Response Parameter actual value .

If their is any change or increase in Acceptance parameter in Barclays system MUST change this section to set the starting and Ending position of request parameters . 
*/

$orderIdPosition = strpos($output,"orderID");
$orderIdStart =$orderIdPosition+9;
$PAYIDPosition = strpos($output,"PAYID");
$PAYIDStart =$PAYIDPosition+7 ;
$NCSTATUSPosition = strpos($output,"NCSTATUS"); 
$NCSTATUSStart=$NCSTATUSPosition+10;
$NCERRORPosition = strpos($output,"NCERROR"); 
$NCERRORStart = $NCERRORPosition +9;
$ACCEPTANCEPosition = strpos($output,"ACCEPTANCE"); 
$ACCEPTANCEStart =$ACCEPTANCEPosition + 12; 
$STATUSPosition = strpos($output,"STATUS");
$STATUSStart =$STATUSPosition + 8;
$amountPosition = strpos($output,"amount");
$amountStart =$amountPosition +8;
$currencyPosition = strpos($output,"currency");
$currencyStart =$currencyPosition +10; 
$PMPosition = strpos($output,"PM"); 
$PMStart =$PMPosition + 4;
$BRANDPosition = strpos($output,"BRAND"); 
$BRANDStart =$BRANDPosition + 7;
$NCERRORPLUSPosition = strpos($output,"NCERRORPLUS"); 
$NCERRORPLUSStart =$NCERRORPLUSPosition + 13;
$responseEnd =strpos($output,"</ncresponse>");


/**print($NCERRORPosition);
echo "<br>";
print($STATUSStart);
echo "<br>";**/
$orderIdEnd =$PAYIDPosition-($orderIdStart+3); 
$PAYIDEnd =$NCSTATUSPosition-($PAYIDStart +3);
$NCSTATUSEnd = $NCERRORPosition-($NCSTATUSStart+3);
$NCERROREnd =$ACCEPTANCEPosition-($NCERRORStart+3);
$ACCEPTANCEEnd = ($amountPosition -12) -($ACCEPTANCEStart+3);
$STATUSEnd =1;//$amountPosition -($STATUSStart+3); 
$amountEnd = $currencyPosition -($amountStart+3);
$currencyEnd = $PMPosition -($currencyStart+3);
$PMEnd = $BRANDPosition-($PMStart +3);
$BRANDEnd =$NCERRORPLUSPosition-($BRANDStart+3); 
$NCERRORPLUSEnd = $responseEnd;

// for status 

$STATUSStart = $amountPosition-4;//$ACCEPTANCEPosition+12+$ACCEPTANCEEnd+9;

/*echo "<br>";
print($STATUSStart);
echo "<br>";
print($amountPosition);
echo "<br>";

echo "Ending <br>";
print($orderIdEnd);
echo "<br>";
print($PAYIDEnd);
echo "<br>";
print($NCSTATUSEnd);
echo "<br>";
print($NCERROREnd);
echo "<br>";
print($ACCEPTANCEEnd);
echo "<br>";
print($STATUSEnd);
echo "<br>";
print($amountEnd);
echo "<br>";
print($currencyEnd);
echo "<br>";
print($PMEnd);
echo "<br>";
print($BRANDEnd);
echo "<br>";
print($NCERRORPLUSEnd);
echo "<br>";*/

// to get Values

$orderId = substr($output,$orderIdStart,$orderIdEnd);
$PAYID = substr($output,$PAYIDStart,$PAYIDEnd);
$NCSTATUS = substr($output,$NCSTATUSStart,$NCSTATUSEnd);
$NCERROR = substr($output,$NCERRORStart,$NCERROREnd);
$ACCEPTANCE = substr($output,$ACCEPTANCEStart,$ACCEPTANCEEnd);
$STATUS = substr($output,$STATUSStart,$STATUSEnd);
$amount = substr($output,$amountStart,$amountEnd);
$currency = substr($output,$currencyStart,$currencyEnd);
$PM = substr($output,$PMStart,$PMEnd);
$BRAND = substr($output,$BRANDStart,$BRANDEnd);
$NCERRORPLUS = substr($output,$NCERRORPLUSStart,$NCERRORPLUSEnd);


/*echo "Starting <br>";
print($orderIdStart);
echo "<br>";
print($PAYIDStart);
echo "<br>";
print($NCSTATUSStart);
echo "<br>";
print($NCERRORStart);
echo "<br>";
print($ACCEPTANCEStart);
echo "<br>";
print($STATUSStart);
echo "<br>";
print($amountStart);
echo "<br>";
print($currencyStart);
echo "<br>";
print($PMStart);
echo "<br>";
print($BRANDStart);
echo "<br>";
print($NCERRORPLUSStart);
echo "<br>";


echo "Values <br>";
print($orderId);
echo "<br>";
print($PAYID);
echo "<br>";
print($NCSTATUS);
echo "<br>";
print($NCERROR);
echo "<br>";
print($ACCEPTANCE);
echo "<br> ";
print($STATUS);
echo "<br>";
print($amount);
echo "<br>";
print($currency);
echo "<br>";
print($PM);
echo "<br>";
print($BRAND);
echo "<br>";
print($NCERRORPLUS);
echo "<br>";



/*
End of This Section 

*/
// Conditions To do after successful response

// logs Maintain in Db soap_Api_Logs table.

$resValue = "orderId=$orderId&payId=$PAYID&NCSTATUS=$NCSTATUS&NCERROR=$NCERROR&ACCEPTANCE=$ACCEPTANCE&STATUS=$STATUS&amount=$amount&currency=$currency&PM=$PM&BRAND=$BRAND&NCERRORPLUS=$NCERRORPLUS";

$logsQuery ="insert into soap_Api_Logs (transID, method, request, response, responseCode ,dated, remarks) VALUES ('".$orderId."','SAL','".$reqValue."','".$resValue."','".$STATUS."','".$log_time."','".$NCERRORPLUS."')";

insertInto($logsQuery);

$htmlDecoded = str_replace("Identification requested\"><HTML_ANSWER>","",$NCERRORPLUS);
$htmlDecoded = str_replace("</HTML_ANSWER></ncresponse>","",$htmlDecoded);

//echo "<br>";
//print($output);
curl_close ($ch); // close curl handle

if($STATUS == "6" ){
$updateTransaction = "update transactions set DebitCardTrans='Y',debitPayId='".$PAYID."',acceptance ='".$ACCEPTANCE."' where refNumberIM ='".$orderId."' ";
update($updateTransaction);
echo base64_decode($htmlDecoded);
exit(0);
}

if($STATUS == "9" || $STATUS == "5")
{
echo "Success";
$updateTransaction = "update transactions set moneyPaid='By Debit Card',DebitCardTrans='Y',debitPayId='".$PAYID."',acceptance ='".$ACCEPTANCE."' where refNumberIM ='".$orderId."' ";
update($updateTransaction);

</script>


<script>
window.location.assign("debit_success_page.php")
</script>

<?

}
if($STATUS == "" ||$STATUS == "0" || $STATUS == "2")
{
echo "Error";
if(!empty($orderId))
{
$updateTransaction = "update transactions set moneyPaid='Payment Failed' where refNumberIM ='".$orderId."' ";
update($updateTransaction);
}
?>
<script>
window.location.assign("debit_error_page.php")
</script>
<?
}

?>

</body></html>