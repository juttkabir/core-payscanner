<?php
/**
 * @package: Online Module
 * @subpackage: view beneficiaries
 * @author: Mirza Arslan Baig
 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php");
}
$limit = 10;
if (!empty($_GET['offset']))
    $offset = $_GET['offset'];
else
    $offset = 0;
$next = $offset + $limit;
$prev = $offset - $limit;

include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];


if (($_REQUEST["benCountry"] == '') && ($_REQUEST["benName"] != '')) {
    $name = $_REQUEST["benName"];
    $query = " (firstName like '%" . $name . "%' OR middleName like '%" . $name . "%' AND lastName like '%" . $name . "%')AND ";
} else if (($_REQUEST["benCountry"] != '') && ($_REQUEST["benName"] == '')) {
    $strbenCountry = $_REQUEST["benCountry"];
    $query = " (Country like '%" . $strbenCountry . "%')AND ";
} else if (($_REQUEST["benCountry"] != '') && ($_REQUEST["benName"] != '')) {
    $name = $_REQUEST["benName"];
    $strbenCountry = $_REQUEST["benCountry"];
    $query = " (firstName like '%" . $name . "%' OR middleName like '%" . $name . "%' AND lastName like '%" . $name . "%')AND (Country like '%" . $strbenCountry . "%') AND ";
} else {
    $query = "";
}


/**Ticket 10651**/
//$strQueryTotalBens = "SELECT COUNT(benID) AS records FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' AND status = 'Enabled'";
$strQueryTotalBens = " SELECT   COUNT(benID)AS records FROM " . TBL_BENEFICIARY . " WHERE  " . $query . " `customerID` = '" . $_SESSION['loggedInUser']['userID'] . "'  AND status = 'Enabled'";
//debug($strQueryTotalBens);
/* $strQueryBen = "SELECT benID, CONCAT(firstName, middleName, lastName) AS name, Country, created FROM ".TBL_BENEFICIARY." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."' ORDER BY benID DESC status = 'Enabled' LIMIT $offset, $limit";
*/

$strQueryBen = " SELECT  benID, CONCAT(firstName, middleName, lastName) AS name, Country, DATE_FORMAT(created, '%d-%m-%Y') AS created FROM " . TBL_BENEFICIARY . " WHERE  " . $query . " `customerID` = '" . $_SESSION['loggedInUser']['userID'] . "'  AND status = 'Enabled' ORDER BY `benID`DESC LIMIT " . $offset . " , " . $limit . " ";
//debug($strQueryBen);

$arrTotalBens = selectFrom($strQueryTotalBens);

$arrBens = selectMultiRecords($strQueryBen);
$intTotalBens = count($arrBens);
$intAllBens = $arrTotalBens['records'];
$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>View beneficiaries - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>

    <?php include 'templates/_ga.inc.php'; ?>
    <script>
        function createBen() {
            window.location = 'add_beneficiary_new.php';
        }
    </script>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'beneficiaries'; ?>
        <?php include 'templates/header.php' ?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Beneficiaries</h1>
                        <p class="page-header-desc">Search your beneficiaries</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Beneficiaries</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->


        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Search your beneficiaries</h2>
                        <p>Enter the Beneficiary name or/and select the country of currency you wish to search for.</p>
                        <form action="" name="benSearch" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="benName" class="input-desc">Beneficiary Name:</label>
                                        <input name="benName"
                                               value="<?= $name; ?>"
                                               type="text" maxlength="20"
                                               placeholder=""
                                               class="form-control form_fields min_field"
                                               autocomplete="off">
                                    </div><!-- End .from-group -->
                                </div>
                                <div class="col-sm-6">
                                    <label for="benCountry" class="input-desc">Beneficiary Country:</label>
                                    <select name="benCountry" type="text" class="form-control select_field">
                                        <option value="">- Select Country -</option>
                                        <?php $strQueryCountries = "select distinct(c.countryName),s.toCountryId from services as s inner join countries as c on c.countryId = s.toCountryId";
                                        $arrCountry = selectMultiRecords($strQueryCountries);
                                        for ($j = 0; $j < count($arrCountry); $j++)
                                            echo "<option value='" . $arrCountry[$j]['countryName'] . "'>" . $arrCountry[$j]['countryName'] . "</option>";
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-right clear-margin helper-group">
                                    </div><!-- End .form-group -->
                                    <div class="form-group mb5">
                                        <input type="submit" id="searchBen" class="btn btn-custom" value="Search">
                                        <input type="reset" class="btn btn-default" value="Clear">
                                        <a href="add_beneficiary_new.php"
                                           class="btn btn-custom pull-right add-tooltip"
                                           data-placement="top"
                                           title="Click here to add a new beneficiary"
                                        >
                                            Create beneficiary
                                        </a>
                                    </div><!-- End .from-group -->
                                </div>
                            </div>
                        </form>
                    </div><!-- End .form-wrapper -->

                </div><!-- End .col-sm-6 -->

                <div class="mb40 visible-xs"></div><!-- space -->

                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>

                    <p>Send money safely and securely with <?= $title; ?> Online You can send any amount from £100 up to
                        £50,000 or currency equivalent.</p>

                    <div class="mb10"></div><!-- space -->

                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->


            <div class="row">
                <span class="col-sm-12">
                    <h2 class="title-border custom mt40 mb40">List of beneficiaries</h2>
                    <?php if ($intTotalBens > 0): ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-condensed">
                                <tr>
                                    <td class="showing_pag">
                                        Showing <?php echo ($offset + 1) . "-" . ($intTotalBens + $offset) . " of " . $intAllBens; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align="right" class="showing_pag">
                                        <?php if ($prev >= 0) { ?>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0">First</a>&nbsp;
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev; ?>">Previous</a>&nbsp;
                                        <?php }
                                        if ($next > 0 && $next < $intAllBens) {
                                            $intLastOffset = (ceil($intAllBens / $limit) - 1) * $limit;
                                            ?>
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>">Next</a>&nbsp;
                                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset; ?>">Last</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Creation Date</th>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Operations</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $rows = '';
                                for ($x = 0; $x < $intTotalBens; $x++) {
                                    $rows .= '<tr>';
                                    $rows .= '<td>' . $arrBens[$x]['created'] . '</td>';
                                    $rows .= '<td>' . $arrBens[$x]['name'] . '</td>';
                                    $rows .= '<td>' . $arrBens[$x]['Country'] . '</td>';
                                    $rows .= '<td><form name="editBen" action="add_beneficiary_new.php" method="post">
                                             <input type="hidden" name="ben" value="' . $arrBens[$x]['benID'] . '">
                                             <input class="btn btn-custom" type="submit" value="Edit">
                                             </form></td>';
                                    $rows .= '<td><form name="createTrn" action="make_payment-ii-new.php" method="post">
                                             <input type="hidden" name="ben" value="' . $arrBens[$x]['benID'] . '">
                                             <input class="btn btn-custom" type="submit" value="Create Transaction">
                                             </form></td>';
                                    $rows .= '</tr>';
                                }
                                echo $rows;
                                ?>
                                </tbody>
                            </table>
                        </div><!-- End .table-responsive -->
                    <?php else: ?>
                        <span>There are no beneficiaries to display.</span>
                    <?php endif; ?>
                </div>
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php' ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->

<script src="<?= $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>

</body>

</html>
