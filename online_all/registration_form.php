<?php
require_once '../src/bootstrap.php';

$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$logoHtml = $agentUiData['logoHtml'];
$agentId = $agentUiData['agentId'] ? $agentUiData['agentId'] : '';
$customerService = new \Payex\Online\Service\CustomerService($online->getDb());
$registrationHelper = new \Payex\Online\Helper\RegistrationHelper();
$countryList = $customerService->getCountryListForRegistrationPage();
$countryOptionList = $registrationHelper->getCountryOptionListForRegistrationOnlinePage($countryList);

if(isset($_GET['type'])){
    $register_type =base64_decode( urldecode( $_GET['type'] ) );
}else{
    $register_type="";
}

if(isset($_GET['cur_type'])){
    $cust_type = $_GET['cur_type'];
}else{
    $cust_type = "USD";
}
include_once 'includes/configs.php';
// include($_SERVER['DOCUMENT_ROOT']."/include/config.php");
$senderPage = selectMultiRecords("SELECT id, title FROM id_types WHERE active='Y' AND show_on_sender_page='Y'");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Register - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
    <meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="imagetoolbar" content="no">
    <link href="css/register/style_new.css" rel="stylesheet" type="text/css">
    <link href="css/register/style_responsive.css" rel="stylesheet" type="text/css">
    <link href="css/register/stylenew.css" rel="stylesheet" type="text/css">
    <link href="css/datePicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/js/registration/build/css/intlTelInput.css">
    <script type="text/javascript" src="./javascript/jquery.js"></script>
    <script type="text/javascript" src="./javascript/jquery.validate.js"></script>
    <script type="text/javascript" src="./javascript/date.js"></script>
    <script type="text/javascript" src="./javascript/jquery.datePicker.js"></script>
    <script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="./javascript/ajaxupload.js"></script>
</head>
<body>
<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">
    <div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
        <img src="images/formImages/loading.gif" alt=""/>
        <span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
    </div>
</div>
<!-- main container-->
<div class="container">
    <div class="upper_container">
        <div class="header">
            <div class="logo">
                <?= $logoHtml; ?>
            </div>
        </div>
    </div>
    <!-- lower container -->
    <div class="lower_container">
        <div class="body_area">
            <br>
            <form name="senderRegistrationForm" id="senderRegistrationForm" action="verifyCustomer.php" method="post" style="overflow:hidden;">
                <input type="hidden" value="<?= $register_type; ?>" id="register_type" name="register_type" />
                <input type="hidden" value="<?= $agentId; ?>" name="agent_id" />
                <div class="">
                    <div class="content_area_center">
                        <h1 class="content_heading">Register</h1>
                        <p class="content_subheading">Please complete the form below and upload one form of identification to comply with UK money laundering regulations, we require </p>
                        <ul class="faqs_list">
                            <li><a href="#" style="text-decoration:none;cursor:default">a valid Passport</a></li>
                            <!--<li><a href="#" style="text-decoration:none;cursor:default">or a valid National Identity Document</a></li>-->
                        </ul>
                        <!-- table area for input fields ends  -->
                    </div>
                    <!-- content area left-->
                    <div id="emailDiv" class="content_area_left">
                        <div class="field_wrapper reg_title">
                            <label class="form_label ">Title :</label><br>
                            <select name="title" class="select_mini " >
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss.</option>
                                <option value="Dr">Dr.</option>
                                <option value="Prof">Prof.</option>
                                <option value="Eng">Eng.</option>
                                <option value="Herr">Herr</option>
                                <option value="Frau">Frau</option>
                                <option value="Ing">Ing.</option>
                                <option value="Mag">Mag.</option>
                                <option value="Sr">Sr.</option>
                                <option value="Sra">Sra.</option>
                                <option value="Srta">Srta.</option>
                            </select>
                        </div>
                        <div class="field_wrapper reg_forname">
                            <label class="form_label ">Forename:<span style="color:red">*</span></label><br>
                            <input name="forename" class="form_fields" type="text" id="forename" />
                            <span id="forenameValidator" class="please_do"></span>
                        </div>
                        <div class="field_wrapper">
                            <label class="form_label">Email:<span style="color:red">*</span></label><br>
                            <input name="email" type="text" id="email" class="required email error form_fields"/>
                            <span id="emailValidator" class="please_do"></span>
                            <input type="hidden" name="ipAddress" value="95.210.228.178"/>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmail">
                            <label class="form_label">Country of Birth :<span style="color:red"></span></label><br>
                            <select name="birthCountry" id="birthCountry" class="select_field hideOnDuplicateEmail">
                                <?php echo $countryOptionList; ?>
                            </select>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmail">
                            <label class="form_label  hideOnDuplicateEmail">Telephone - Landline:<span style="color:red"></span></label><br>
                            <input name="landline" type="text" class="form_fields  hideOnDuplicateEmail" id="landline"  onkeypress="return isNumber(event)" />
                        </div>
                    </div>
                    <!-- content area right-->
                    <div class="content_area_right">
                        <div class="field_wrapper" >
                            <label class="form_label">Surname:<span style="color:red">*</span></label><br>
                            <input name="surname" type="text" id="surname" class="form_fields">
                            <span id="surnameValidator" class="please_do"></span>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmail">
                            <label class="form_label">Date of Birth:<span style="color:red"></span></label><br>
                            <select name="dobDay" class="select_mini hideOnDuplicateEmail" id="dobDay">
                                <?php
                                $str = '';
                                for($i=1; $i<=31; $i++){
                                    $str = $i < 10 ? '0' . $i : $i;
                                    echo '<option value=' . $str . '>' . $str . '</option>';
                                }
                                ?>
                            </select>
                            <select class="select_mini hideOnDuplicateEmail" name="dobMonth" id="dobMonth">
                                <?php
                                for ($i=1; $i<=12; $i++){
                                    $number = $i < 10 ? '0'.$i : $i;
                                    $monthName = DateTime::createFromFormat('m', $i)->format('M');
                                    $row = '<option value="' . $number .'">' . $monthName . '</option>';
                                    echo $row;
                                }
                                ?>
                            </select>
                            <select name="dobYear" class="select_mini" id="dobYear">
                                <?php
                                $currentYear = 2000;
                                for ($i=1929; $i<=$currentYear; $i++){
                                    $selectedYear = $i === 1970 ? ' selected' : '';
                                    $row = '<option value="' . $i . '"' . $selectedYear . '>' . $i . '</option>';
                                    echo $row;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmail">
                            <label class="form_label">Gender:</label><br>
                            <select class="select_mini hideOnDuplicateEmail"  name="gender">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="hideDivOnDuplicateEmail">
                            <!--                                <div class="field_wrapper reg_title">-->
                            <!--                                    <label class="form_label ">Code :</label>-->
                            <!--                                    <select name="code" class="select_mini">-->
                            <!--                                        --><?php
                            //                                            $queryCountryCodeList = selectMultiRecords("SELECT countryCodeNumber FROM countries");
                            //                                            foreach ($queryCountryCodeList as $row) {
                            //                                                $row = explode(', ', $row["countryCodeNumber"]);
                            //                                                foreach ($row as $item) {
                            //                                                    echo "<option value='$item'>$item</option>";
                            //                                                }
                            //                                            }
                            //                                        ?>
                            <!--                                    </select>-->
                            <!--                                </div>-->
                            <div class="field_wrapper reg_forname" style="margin-top: 0px">
                                <label class="form_label" style="line-height: 30px">Telephone - Mobile:<span style="color:red"></span></label><br>
                                <input name="mobile" type="text" id="mobile" class="form_fields  hideOnDuplicateEmail"  onkeypress="return isNumber(event)" style="width: 339px;height: 35px"/>
                                <span id="mobileValidator" class="please_do"></span>
                                <input type="hidden" id="code" name="code">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content area ends here

                    <?php /*
                    <!-- content area 2-->
                    <div class="content_area hideDivOnDuplicateEmail" >
                        <div class="content_area_center">
                            <h2 class="heading_beneficiary">Automatic Address Lookup</h2>
                            <p class="content_subheading">Enter your United Kingdom postcode and click the 'Search for address' button</p>
                        </div>
                        <!-- content area left-->
                        <div class="content_area_left hideDivOnDuplicateEmail">
                            <div class="field_wrapper">
                                <label class="form_label">Post code:<span style="color:red">*</span></label><br>
                                <input id="postCodeSearch"  class="form_fields  hideOnDuplicateEmail" type="text" onKeyPress='enterToSearch(event);' onFocus="clearAddress();"/>
                            </div>
                            <div class="field_wrapper">
                                <input type="button" id="searchAddress" value="Search for address" class="submit_btn" />
                            </div>
                            <div class="field_wrapper" id="addressContainer">
                                <label class="form_label">Suggested addresses<br /><small>please choose your address from the list below</small></label>
                                <div id="suggesstions"></div>
                            </div>
                        </div>
                        <!-- content area right-->
                        <div class="content_area_right">

                        </div>
                    </div>
                    <!-- content area ends here 2-->
                    */ ?>

                    <!-- content area 3-->
                <div class="content_area hideDivOnDuplicateEmail">
                    <div class="content_area_center">
                        <p class="content_heading">Address</p>
                        <p class="content_subheading">If your address cannot be found using the automatic lookup above, please enter your address here.</p>
                    </div>
                    <!-- content area left-->
                    <div class="content_area_left">
                        <div class="field_wrapper">
                            <label class="form_label">Building Number:</label><br>
                            <input name="buildingNumber" id="buildingNumber" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
                        </div>
                        <div class="field_wrapper">
                            <label class="form_label">Street:</label><br>
                            <input name="street" id="street" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
                        </div>
                        <div class="field_wrapper">
                            <label class="form_label">Town:</label><br>
                            <input name="town" id="town" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
                        </div>
                        <div class="field_wrapper">
                            <label class="form_label">Region:</label><br>
                            <input name="province" id="province" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
                        </div>
                    </div>

                    <!-- content area right-->
                    <div class="content_area_right">

                        <div class="field_wrapper">
                            <label class="form_label">Postcode:<span style="color:red">*</span></label><br>
                            <input  name="postcode" type="text" id="postcode"  class="form_fields  hideOnDuplicateEmail" />
                            <span id="postcodeValidator" class="please_do"></span>
                        </div>

                        <div class="field_wrapper">
                            <label class="form_label">Country:</label><br>
                            <select name="residenceCountry" class="select_field" id="residenceCountry">
                                <?php echo $countryOptionList; ?>
                            </select>
                        </div>

                        <div class="field_wrapper">
                            <label class="form_label">Building Name:</label><br>
                            <input name="buildingName" id="buildingName" class="form_fields  hideOnDuplicateEmail" type="text" onKeyPress='enterToSearch(event);'/>
                        </div>
                    </div>
                </div>
                <!-- content area ends here 3-->



                <!-- content area 4-->
                <div class="content_area  hideDivOnDuplicateEmail">
                    <div class="content_area_center">
                        <h2 class="heading_beneficiary">Passport Details
                        </h2>
                    </div>

                    <!-- content area left-->
                    <div id="passportDiv" class="content_area_left hideDivOnDuplicateEmail">

                        <div class="field_wrapper">
                            <label class="form_label">Country of issue :<span style="color:red"></span></label><br>
                            <select name="passportCountry" id="passportCountry" class="select_field" onChange="passportMask();">
                                <? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
                                $custCountries = array_filter($custCountries);
                                for ($k = 0; $k < count($custCountries); $k++) {
                                    $selected="";
                                    if (trim($custCountries[$k]) == "United Kingdom"){
                                        $selected = "selected";         }    ?>
                                    <option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
                                        <?
                                        echo trim($custCountries[$k]) ?>
                                    </option>
                                    <?
                                } ?>

                                <!--        <option value="" selected='selected'>-- Select Country of Issue --</option>-->
                                <!--        <option value="United Kingdom">United Kingdom</option>-->
                                <!--        <option value="Portugal">Portugal</option>-->
                                <!--        <option value="USA">USA</option>-->
                                <!--        <option value="Australia">Australia</option>-->
                                <!--        <option value="Spain">Spain</option>-->
                                <!--        <option value="Ireland">Ireland</option>-->
                                <!--        <option value="Netherlands">Netherlands</option>-->
                                <!--        <option value="Canada">Canada</option>-->
                            </select>
                        </div>
                        <div class="field_wrapper">
                            <img src="images/formImages/uk_passport.jpg" alt="UK Specimin Passport" width="280px"/>
                        </div>
                        <!--                        <div class="field_wrapper">-->
                        <!--                            <label class="form_label">Passport Number:</label><br>-->
                        <!--                            <input name="passportNumber" type="text" size="62" class="form_fields  hideOnDuplicateEmail passport_fields" id="passportNumber"/>-->
                        <!--                            <span class="please_do" style="color:grey">Please enter your passport number as highlighted above.</span>-->
                        <!--                            <span id="passportNumberValidator" class="please_do"></span>-->
                        <!--                        </div>-->
                        <!--                        <div class="field_wrapper">-->
                        <!--                            <label class="form_label   ">Issue Date:</label><br>-->
                        <!--                            <input name="passportIssue" id="passportIssue" class="form_fields  hideOnDuplicateEmail" type="text" readonly />-->
                        <!--                        </div>-->
                        <!---->
                        <!--                        <div class="field_wrapper">-->
                        <!--                            <label class="form_label">Passport Expiry Date:</label><br>-->
                        <!--                            <input name="passportExpiry" id="passportExpiry" class="hideOnDuplicateEmail form_fields" value="" readonly />-->
                        <!--                            <span id="passportExpiryValidator" class="please_do"></span>-->
                        <!--                        </div>-->

                    </div>

                    <!-- content area right-->
                    <!--                    <div class="content_area_right ">-->
                    <!---->
                    <!---->
                    <!---->
                    <!--                        <div class="field_wrapper hideDivOnDuplicateEmail">-->
                    <!--                            <label class="form_label ">Passport Upload </label><br><br>-->
                    <!--                            <!--<a href="javascript:void(0);" class="submit_btn" id="file" class="button" >Select file</a>-->
                    <!--                            <button class="submit_btn" id="file">Select file</button>-->
                    <!--                            <br>-->
                    <!---->
                    <!--                            <input type="file" class="submit_btn" name="passportDoc" id="passportDoc" style="visibility:hidden;" />-->
                    <!--                            <span class="please_do" style="color:grey;display:inline-block;margin-top: -33px;">Please upload a scanned copy of your passport, the maximum file size is 2Mb. If you are having difficulty uploading the file please post a copy or e-mail us at <a href="mailto:--><?//= $email; ?><!--">--><?//= $email; ?><!--</a> as soon as possible</span><br>-->
                    <!--                            <div class="please_do" id="fileUploader">-->
                    <!--                                <img src="images/formImages/loading.gif"/> Uploading ...-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div>
                <!--                    Arif changes starts-->
                <?php
                for( $i = 0; $i < count($senderPage); $i++ )
                {
                    ?>
                    <div class="content_area  hideDivOnDuplicateEmail">
                        <div class="content_area_center">
                            <h1 class="heading_beneficiary"><?php echo $senderPage[$i]['title'];?>
                            </h1>
                        </div>
                        <div id="passportDiv" class="content_area_left hideDivOnDuplicateEmail">
                            <div class="field_wrapper">
                                <input type="hidden" name="IDTypeID[]" value="<?=$senderPage[$i]["id"];?>" />
                                <label class="form_label">ID Number:</label><br>
                                <input id="id_number_<?=$senderPage[$i]["id"];?>" name="IDNumber[]" value="<?=$idNumber;?>" type="text" size="62" class="form_fields  hideOnDuplicateEmail passport_fields"/>
                            </div>
                            <div class="field_wrapper">
                                <label class="form_label">Issue Date:</label><br>
                                <input type="hidden" id="issue_date_text_<?=$senderPage[$i]["id"];?>" name="issueDate[]" />
                                <input id="issue_date_<?=$senderPage[$i]["id"];?>" class="form_fields  hideOnDuplicateEmail" type="date" onchange="document.getElementById('issue_date_text_<?=$senderPage[$i]["id"];?>').value=document.getElementById('issue_date_<?=$senderPage[$i]["id"];?>').value" />
                            </div>
                            <div class="field_wrapper">
                                <label class="form_label">Expiry Date:</label><br>
                                <input type="hidden" id="expiry_date_text_<?=$senderPage[$i]["id"];?>" name="expiryDate[]"/>
                                <input id="expiry_date_<?=$senderPage[$i]["id"];?>" class="hideOnDuplicateEmail form_fields" type="date" onchange="document.getElementById('expiry_date_text_<?=$senderPage[$i]["id"];?>').value=document.getElementById('expiry_date_<?=$senderPage[$i]["id"];?>').value" />
                            </div>
                        </div>
                        <div class="content_area_right ">
                            <div class="field_wrapper hideDivOnDuplicateEmail">
                                <label class="form_label "><?php echo $senderPage[$i]['title'];?> Upload </label><br><br>
                                <!--<a href="javascript:void(0);" class="submit_btn" id="file" class="button" >Select file</a>-->
                                <button class="submit_btn" id="file">Select file</button>
                                <br>
                                <input type="file" class="submit_btn" name="passportDoc" id="passportDoc" style="opacity: 0; margin-top: -27px" />
                                <span class="please_do" style="color:grey;display:inline-block;margin-top: 0px;">Please upload a scanned copy of your <?php echo $senderPage[$i]['title'];?>, the maximum file size is 2Mb. If you are having difficulty uploading the file please post a copy or e-mail us at <a href="mailto:<?= $email; ?>"><?= $email; ?></a> as soon as possible</span><br>
                                <div class="please_do" id="fileUploader">
                                    <img src="images/premier/formImages/loading.gif"/> Uploading ...
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                }
                ?>
                <!--                    Arif changes ends-->
                <!-- content area ends here 5-->
                <?php
                if (!empty($register_type) && strtolower($register_type) == 'prepaid') {
                    $display = "display:block";
                } else {
                    $display = "display:none";
                }
                ?>
                <!-- content area 2-->
                <div class="content_area ">
                    <div class="content_area_center hideDivOnDuplicateEmailh" style="<?=$display?>">
                        <!--<h2 class="heading_beneficiary">National Identity Card Details</h2>-->
                        <!--<h2 class="heading_beneficiary">Initial Load for Prepaid Card</h2>-->
                    </div>
                    <!-- content area left-->
                    <div class="content_area_left">
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Line1:</label><br>
                            <input name="line1" type="text" id="line1" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase" />
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Line3:</label><br>
                            <input name="line3" id="line3" type="text" maxlength="30" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase;" /><br>
                            <div class="please_do" id="NICAvailability"></div>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Country of nationality:</label><br>
                            <select name="nationalityCountry" class="select_field  hideOnDuplicateEmail" id="nationalityCountry">
                                <?php echo $countryOptionList; ?>
                            </select>
                        </div>
                        <div class="field_wrapper" id="initial_load_row" style="<?=$display?>">
                            <!--<label class="form_label">Initial Load:<span style="color:red">*</span></label><br>-->
                            <input name="initial_load" type="hidden" id="initial_load" class="form_fields" value="0.00" style="text-transform:uppercase" />
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmail">
                            <label class="form_label">How did you hear about us?</label>
                            <textarea name="heardAboutUs" class="form_fields  hideOnDuplicateEmail" cols="50" rows="2" style="height:60px;"></textarea>
                        </div>
                    </div>

                    <!-- content area right-->
                    <div class="content_area_right ">
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Line2:</label><br>
                            <input name="line2" id="line2" type="text" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase" />
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Expiry Date:<span style="color:red">*</span></label><br>
                            <input name="idExpiry" id="idExpiry" class="form_fields  hideOnDuplicateEmail" type="text" readonly/>
                        </div>
                        <div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
                            <label class="form_label">Country of issue:</label><br>
                            <select name="issueCountry" class="select_field  hideOnDuplicateEmail" id="issueCountry">
                                <?php echo $countryOptionList; ?>
                            </select>
                        </div>
                        <?php
                        //if(!empty($register_type) && strtolower($register_type) == 'prepaid')
                        { ?>
                            <div class="field_wrapper" id="currency_row" style="<?=$display?>">
                                <label class="form_label"><span style="color:red"></span></label><br>
                                <input type="hidden" name="card_issue_currency" class="select_field" id="card_issue_currency" value="<?php echo $cust_type; ?>">
                                <!--<select name="card_issue_currency" class="select_field" id="card_issue_currency" >
                                    <option value="">---SELECT---</option>

                                    <option value="EUR">EUR</option>
                                    <option value="USD">USD</option>
                                </select>-->
                            </div>
                        <?php } ?>
                        <div class="field_wrapper">
                            <input type="button" name="register" value="Register" id="register" class="submit_btn" onclick="return validateFields();" />

                            <br>
                            <br>
                            <br>
                            <div class="form_label">Already registered? <a href="<?php echo $online->getBaseUrl();?>">Log in</a></div>
                            <br>


                        </div>
                    </div>
                    <div class="error_msg" id="msg" style="display:none;"></div>
                </div>
                <!-- content area ends here 5-->

            </form>
        </div>
        <!-- footer area -->
        <?php include 'footer.php';?>
    </div>
</div>
<script type="text/javascript" src="assets/js/registration/registration.js"></script>
<!-- <script src="script_cookie.js"></script> -->
<script type="text/javascript" src="assets/js/registrationFieldsValidator.js"></script>
<script type="text/javascript" src="assets/js/registration/build/js/intlTelInput.js"></script>
<script>
    var input = document.querySelector("#mobile");
    window.intlTelInput(input, {
        // allowDropdown: false,
        // autoHideDialCode: false,
        // autoPlaceholder: "off",
        // dropdownContainer: document.body,
        // excludeCountries: ["us"],
        // formatOnDisplay: false,
        // geoIpLookup: function(callback) {
        //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //     var countryCode = (resp && resp.country) ? resp.country : "";
        //     callback(countryCode);
        //   });
        // },
        hiddenInput: "full_number",
        // initialCountry: "auto",
        // localizedCountries: { 'de': 'Deutschland' },
        // nationalMode: false,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: true,
        utilsScript: "assets/js/registration/build/js/utils.js"
    });
</script>
</body>
</html>