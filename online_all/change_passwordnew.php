<?php
/**
 * @package: Online Module
 * @subpackage: Change Password
 * @author: Mirza Arslan Baig
 */
ini_set("display_errors", "on");
error_reporting(1);
session_start();

if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php");
}
include_once "includes/configs.php";
include_once "includes/database_connection.php";
dbConnect();
include_once "includes/functions.php";
include_once "javascript/audit-functions.php";

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];


if (isset($_POST['changePassword']) && !empty($_POST['oldPass']) && !empty($_POST['pass1'])) {
    $strQueryChkOldPass = "SELECT password FROM " . TBL_CUSTOMER . " WHERE customerID = '" . $_SESSION['loggedInUser']['userID'] . "'";

    $arrChkOldPass = selectFrom($strQueryChkOldPass);

    $strOldPassword = $_POST['oldPass'];
    $strNewPass1 = mysql_real_escape_string($_POST['pass1']);
    $strNewPass2 = mysql_real_escape_string($_POST['pass2']);
    if ($arrChkOldPass['password'] == $strOldPassword && $strNewPass1 == $strNewPass2) {
        $strQueryChangePass = "UPDATE " . TBL_CUSTOMER . " SET `password` = '$strNewPass1' WHERE customerID = '" . $_SESSION['loggedInUser']['userID'] . "'";
        if (update($strQueryChangePass)) {
            $strMsg = "Password changed successfully!!";

            activities($_SESSION["loggedInUser"]["userID"], "UPDATION", $_SESSION["loggedInUser"]["userID"], "customer", "Customer changed his password");
            $id_activity_id = @mysql_insert_id();

            logChangeSet($arrChkOldPass, $_SESSION["loggedInUser"]["userID"], "customer", "customerID", $_SESSION["loggedInUser"]["userID"], $_SESSION["loginHistoryID"]);
            $auditModifyId = @mysql_insert_id();
            $querInsertActivityId = "UPDATE " . TBL_AUDIT_MODIFY_HISTORY . " SET id_activity_id = " . $id_activity_id . " WHERE id = " . $auditModifyId . "";
            update($querInsertActivityId);
        } else {
            $strError = "Password can not be changed.Please try again";
        }
    } elseif ($arrChkOldPass['password'] != $strOldPassword) {
        $strError = "Enter valid old password!!";
    } else {
        $strError = "Your passwords did not matched!!";
    }
}
$strUserId = $_SESSION['loggedInUser']['userID'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Change password - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>
    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'contact-us'; ?>
        <?php include 'templates/header.php'; ?>
    </header>
    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Change Your Password</h1>
                        <!--                        <p class="page-header-desc">Talk to us</p>-->
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Change Your Password</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container">


            <div class="logout_area">
                <h2 class="heading">Change Password</h2>
                <!-- content area -->
                <div class="content_area">
                    <!-- content area left-->
                    <div class="content_area_left">
                        <p class="content_heading">Change Your Password</p>
                        <p class="content_subheading">You may change your password using the form below</p>
                        <form id="changePasswordForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                            <div class="field_wrapper">
                                <label class="form_label">Old Password:<span style="color:red">*</span></label><br>
                                <input name="oldPass" id="oldPass" type="password" class="form_fields"/>
                            </div>
                            <div class="field_wrapper">
                                <label class="form_label">New Password:<span style="color:red">*</span></label><br>
                                <input name="pass1" id="pass1" type="password" class="form_fields"/>
                            </div>
                            <div class="field_wrapper">
                                <label class="form_label">Confirm Password:<span style="color:red">*</span></label><br>
                                <input name="pass2" id="pass2" type="password" maxlength="32" class="form_fields"/>
                            </div>
                            <!--<div class="field_wrapper">
                            <input name="changePassword" id="changePassword" type="submit" value="Submit" class="submit_btn"/>
                            <input type="reset" value="Clear" class="clear_btn"/>
                            </div>-->
                    </div>

                    <div class="button_wrapper">
                        <input name="changePassword" id="changePassword" type="submit" value="Submit"
                               class="submit_btn"/>
                        <input type="reset" value="Clear" class="clear_btn"/>
                    </div>
                    </form>

                    <!-- Error message shown here -->
                    <?php
                    if (isset($strError) && !empty($strError)) {
                        echo '<div class="error_msg">' . $strError . '</div>';
                    }
                    if (isset($strMsg) && !empty($strMsg)) {
                        echo '<div class="error_msg">' . $strMsg . '</div>';
                    }
                    ?>
                    <!-- Error message shown here ends area -->
                </div>
                <!-- content area ends here-->
            </div>
            <!-- footer area -->




        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script type="text/javascript" src="./assets/js/changepass.js"></script>
</body>
</html>
