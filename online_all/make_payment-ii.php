<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Send Transaction
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	//ini_set("display_errors","on");
	//error_reporting(E_ALL);
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	$intBackFlag = false;
	
	if(isset($_SESSION['ben']))
	{
	$strBenNameCont = selectFrom("select firstName from beneficiary where benID = '".$_SESSION['ben']."' ");
		$strBenName = $strBenNameCont["firstName"];
	}
	if(!empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') || ($_REQUEST['sendingAmount'] > '0'&& !empty($_REQUEST['sendingAmount']))){
		//debug($_REQUEST);
		$strBaseCurrency1 = "";
		$strBaseCurrency2 = "";
		$queryBaseCurrencies = "select distinct(baseCurrency) from online_exchange_rates";
		$arrBaseCurrencies = selectMultiRecords($queryBaseCurrencies);
		
		$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
		$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
		$strReceivingAmount = number_format((trim($_REQUEST['receivingAmount'])), 4, ".", "");
		$strSendingAmount = number_format((trim($_REQUEST['sendingAmount'])), 4, ".", "");
		//debug($strReceivingAmount);
		//debug($strSendingAmount);
		if($strReceivingAmount != 0 && $strSendingAmount != 0)
		{
			$strReceivingAmount = 0;
		}
		if($strReceivingAmount != 0)
		{
			$strBaseAmountCurrency = $strReceivingCurrency;
			$strQuotedAmountCurrency = $strSendingCurrency;
			$fltAmountBase = $strReceivingAmount;
			$fltAmountIsSending = false; 
			
		}
		elseif($strSendingAmount != 0)
		{
			$strBaseAmountCurrency = $strSendingCurrency;
			$strQuotedAmountCurrency = $strReceivingCurrency;
			$fltAmountBase = $strSendingAmount;
			$fltAmountIsSending = true; 
		}
		for($i=0;$i<count($arrBaseCurrencies);$i++)
		{
			if($arrBaseCurrencies[$i]["baseCurrency"] == $strSendingCurrency)
			{
				$strBaseCurrency1 = $strSendingCurrency;
				$strFinalBaseCurrency = $strSendingCurrency;
				$strFinalQuotedCurrency = $strReceivingCurrency;
				$flagBaseIsSending = true;
			}
			elseif($arrBaseCurrencies[$i]["baseCurrency"] == $strReceivingCurrency)
			{
				$strBaseCurrency2 = $strReceivingCurrency;
				$strFinalBaseCurrency = $strReceivingCurrency;
				$strFinalQuotedCurrency = $strSendingCurrency;
				$flagBaseIsSending = false;
			}
		}
		
		//debug($fltAmountIsSending);
		//debug($flagBaseIsSending);
			
			/*if($strBaseAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "RATE";
			
			}
			elseif($strQuotedAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "INVERSE";
			}*/
		
		
		if($strBaseCurrency1 !="" && $strBaseCurrency2 != "")
		{
			$flagBaseCurrency = "Both";
		
		}
		elseif($strBaseCurrency1 == "" && $strBaseCurrency2 == "")
		{
			$flagBaseCurrency = "None";
		
		}
		else
		{
			$flagBaseCurrency = "One";
		
		}
		
		
		
		//debug($flagBaseCurrency);
		if($flagBaseCurrency == "Both")
		{
			if($strBaseAmountCurrency == 'GBP')
			{}
			else if($strQuotedAmountCurrency == 'GBP')
			{
				$strFirstCurr = $strBaseAmountCurrency;
				$strBaseAmountCurrency = $strQuotedAmountCurrency;
				$strQuotedAmountCurrency = $strFirstCurr;
			}
			else if($strQuotedAmountCurrency == 'EUR')
			{
				$strFirstCurr = $strBaseAmountCurrency;
				$strBaseAmountCurrency = $strQuotedAmountCurrency;
				$strQuotedAmountCurrency = $strFirstCurr;
			}
			if($strSendingCurrency == "GBP")
			{
				$flgExtendMargin = "Sub";
			}
			else if($strReceivingCurrency == "GBP")
			{
				$flgExtendMargin = "Add";
			}
			else if($strSendingCurrency == "EUR")
			{
				$flgExtendMargin = "Sub";
			}
			else if($strReceivingCurrency == "EUR")
			{
				$flgExtendMargin = "Add";
			}
			$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' order by id DESC LIMIT 1";
			$arrExchangeRate = selectFrom($strQueryRate);
			//debug($arrExchangeRate);
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			//debug($arrExchangeMargin);
			$fltExchangeRate = $arrExchangeRate['exchangeRate'];
			$fltInverseRate = $arrExchangeRate['inverseRate'];
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
			
			if(empty($arrExchangeMargin['margin'])){
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-i.php");
				exit();
			}
			if($strMarginType == "FIXED")
			{
				if($flgExtendMargin == "Add")
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($flgExtendMargin == "Add")
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					//debug($fltMargin);
					if($flgExtendMargin == "Add")
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($flgExtendMargin == "Add")
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			//$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			if($strSendingCurrency =='GBP' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strSendingCurrency =='GBP' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'GBP' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'GBP' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strSendingCurrency =='EUR' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strSendingCurrency =='EUR' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'EUR' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'EUR' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,2);
			////debug($arrExchangeRate,true);
		}
		elseif($flagBaseCurrency == "One")
		{
			if($strBaseCurrency1 != "")
			{
				$flgExtendMargin = "Sub";
			}
			else
			{
				$flgExtendMargin = "Add";
			}
			//debug($flagRateOrInverse);
			$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' order by id DESC LIMIT 1";
			$arrExchangeRate = selectFrom($strQueryRate);
			//debug($arrExchangeRate);
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			//debug($strQueryMargin);
			$arrExchangeMargin = selectFrom($strQueryMargin);
			//debug($arrExchangeMargin);
			if(empty($arrExchangeMargin['margin'])){
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-i.php");
				exit();
			}
			//debug($arrExchangeMargin);
			
			//debug($arrExchangeRate);
			$fltExchangeRate = $arrExchangeRate['exchangeRate'];
			$fltInverseRate = $arrExchangeRate['inverseRate'];
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			//debug($strMarginType);
			//debug($flagRateOrInverse);
			$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
			//debug($fltExchangeRate);		
			if($strMarginType == "FIXED")
			{
				if($flgExtendMargin =='Add')
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($flgExtendMargin =='Add')
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					//debug($fltMargin);
					if($flgExtendMargin =='Add')
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($flgExtendMargin =='Add')
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}

			if($strBaseCurrency1 != "" && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
			}
			else if($strBaseCurrency2 != "" && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
			}
			else
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;
			}
			$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,2);
			
		}
		elseif($flagBaseCurrency == "None")
		{
			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			$_SESSION['exchangeRateError'] = "Call us direct to obtain this quote.";
			header("LOCATION: make_payment-i.php");
			exit();
			if($strBaseAmountCurrency == $strSendingCurrency)
			{
				$fromCurrency = $strSendingCurrency;
				$toCurrency = $strReceivingCurrency;
			
			}
			else
			{
				$fromCurrency = $strReceivingCurrency;
				$toCurrency = $strSendingCurrency;
			}
			//debug($strSendingCurrency);
			//debug($strReceivingCurrency);
			$queryUSDFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryUSDFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for USD
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for USD
			
			$queryUSDTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryUSDTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for USD
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for USD
			$fltConvertedAmnt1 = $fltAmountBase/$fromRate*$toRate;
			//USD END
			//start GBP
			$queryGBPFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryGBPFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for GBP
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				$fromRate = $fromRate-$fltMargin;
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for GBP
			
			$queryGBPTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryGBPTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for GBP
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for GBP
			$fltConvertedAmnt2 = $fltAmountBase/$fromRate*$toRate;
			//GBP END
			
			//start EUR
			$queryEURFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryEURFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for EUR
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
					
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}				
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for EUR
			
			$queryEURTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryEURTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for EUR
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}				
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for EUR
			$fltConvertedAmnt3 = $fltAmountBase/$fromRate*$toRate;
			//EUR END
			
			//debug($fltConvertedAmnt1);
			//debug($fltConvertedAmnt2);
			//debug($fltConvertedAmnt3);
			if($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "" && $fltConvertedAmnt3 !="")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2));
			}
			elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt3 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt2!="" && $fltConvertedAmnt3 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt2, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt1!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt1;
			}
			elseif($fltConvertedAmnt2!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt2;
			}
			elseif($fltConvertedAmnt3!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt3;
			}
			if($fltConvertedAmnt1!="" || $fltConvertedAmnt2 != "" || $fltConvertedAmnt3 !="")
			{
				$arrExchangeRate = $fromRate;
				$fltExchangeRate = $toRate/$fromRate;
				$fltInverseRate =  $fromRate/$toRate;
			}
			//debug($fltConvertedAmnt);
		
		
		}
		if($_REQUEST['sendingAmount']!=''){
			$convertedCurrency=$_REQUEST['receivingCurrency'];
		if($_REQUEST['sendingCurrency']=='GBP'){
				if($_REQUEST['sendingAmount']>30000){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-i.php");
					exit();
					
				}
			
			}
			else
			{	
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP>30000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-i.php");
					exit();
				
				
				}
			
			}
		}
		else{
			$convertedCurrency=$_REQUEST['sendingCurrency'];
			if($_REQUEST['receivingCurrency']=='GBP'){
				if($_REQUEST['receivingAmount']>30000){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-i.php");
					exit();
					
				}
			
			}
			else
			{	
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP>30000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-i.php");
					exit();
				
				
				}
			
			}
		
		
		
		
		}
		if($convertedCurrency=='GBP'){
		if($fltConvertedAmnt>30000){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-i.php");
					exit();
		
		
		}
		}
		else{
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$convertedCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$fltConvertedAmnt*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP>30000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-i.php");
					exit();
				
				
				}
		
		
		}
		//ebug($strReceivingAmount);
		//debug($fltConvertedAmnt);
		//debug($strSendingAmount);
		//debug($strReceivingAmount);
		//$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
		////debug($strQueryRate,true);
		/* $strExchangeRate = number_format($arrExchangeRate['exchangeRate'], 2, ".", "");
		$strInverseRate = number_format($arrExchangeRate['inverseRate'], 2, ".", ""); */
		//$arrExchangeRate = selectFrom($strQueryRate);
		//$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
		//$arrExchangeMargin = selectFrom($strQueryMargin);
		//$strExchangeRate = $arrExchangeRate['exchangeRate'];
		//$strInverseRate = $arrExchangeRate['inverseRate'];
		//$strMarginType = $arrExchangeMargin['marginType'];
		//$fltMargin = $arrExchangeMargin['margin'];

		/*
		if($strMarginType == "PIPS")
		{
			$fltMargin = $fltMargin*0.0001;
			$strExchangeRate = $strExchangeRate-$fltMargin;
			
		
		}
		
		elseif($strMarginType == "PERCENTAGE")
		{
			$fltMargin = $fltMargin * 0.01;
			$strExchangeRate = $strExchangeRate-$fltMargin;
		
		
		}
		elseif($strMarginType == "FIXED")
		{
			$strExchangeRate = $strExchangeRate-$fltMargin;
		
		}
		*/
		////debug("test",true);
		//$strLowerLimit = number_format("0", 2, ".", "");
		if(empty($arrExchangeRate)){
			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			$_SESSION['exchangeRateError'] = "We are sorry for the inconvenience, currently we are not dealing in this currency";
			header("LOCATION: make_payment-i.php");
		}
	}elseif(!empty($_REQUEST['goBack']) && $_REQUEST['goBack'] == 'Back'){
		$intBackFlag = true;
		$strReceivingAmount = $_SESSION['receivingAmount'];
		$strSendingAmount = $_SESSION['sendingAmount'];
		$strReceivingCurrency = $_SESSION['receivingCurrency'];
		$strSendingCurrency = $_SESSION['sendingCurrency'];
		$strExchangeRate = $_SESSION['exchangeRate'];
		$strInverseRate = $arrExchangeRate['inverseRate'];
	}else{
		header("LOCATION: make_payment-i.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Transaction Details Page</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
		<style type="text/css">
			#content{
				padding-left:228px;
				width:574px;
				position:relative;
			}
			#container1{
				position:relative;
			}
			#imgContainer{
				position:absolute;
				left:-6px;
				top:28px;
				
				
				z-index:800;
			}
		</style>
        <script>
		    $(function() {
        var cd = $('#countdown');
        var c = parseInt(cd.text(),10);
        var interv = setInterval(function() {
            c--;
            cd.html(c);
            if (c == 0) {
                window.location.reload(false);
				//window.location.href = 'make_payment-ii.php';
				 //$('#changeTrans').trigger('click');
				 //window.location.href = window.location.protocol +'//'+ window.location.host + window.location.pathname;
				 location.reload(true);
                clearInterval(interv);
            }
        }, 1000);
    });
		</script>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				$currentPage = 'send money';
				require_once "includes/header.php";
			?>
			<div id="container1" >
            
			  <div class = "xRatesTmr">
            
            <div class="xRatesTmrBx">
            
       	  <div class="xRatesTmrBxRt">
                	<div class="xRctrSec">
            			<span id="tmin" class = "timer">0 :&nbsp;<span id='countdown'>60</span></span>
                </div>
                
                </div>
            </div>
            </div>
            	<span id="msg" class="msg">Exchange rate is refreshed after 60 seconds.
Please accept and proceed to continue your transaction. &nbsp;&nbsp;&nbsp;  </span>
				
				<div id="content" class="pull-left">
                <div id="imgContainer"><img src="images/timer_dollar.png" alt="some_text"> </div>
					<div class="short">
						<form action="select_beneficiary_trans.php" method="post" id="processTrans">
							<table class="table table-striped table-bordered">
								<tr>
									<td>
										<label id="upay"class="bold">You Pay :</label>
                                        <br />
										<label id="pcur" class="bold"><?php echo $strSendingCurrency; ?></label>
									</td>
									<td>
										<label id="youpayind" class="bold">
											<?php
												echo $strSendingCurrency." ";
												if($strSendingAmount != 0)
												{
													echo number_format($strSendingAmount,2,".",",");
												}
												else
												{
													////debug($fltConvertedAmnt);
													echo number_format($fltConvertedAmnt,2,".",",");
												}
												
											?>
										</label>
                                        
										<input name="sendingAmount" type="hidden" value="<?php if($strSendingAmount != 0)
												{ echo $strSendingAmount;
}else
{ echo $fltConvertedAmnt;
}
												?>" />
										<input name="sendingCurrency" type="hidden" value="<?php echo $strSendingCurrency; ?>" />
										<!--<div>You pay us by 06 December 2012</div>-->
									</td>
								</tr>
								<tr>
									<td>
										<label id="ubuy" class="bold">You Buy :</label>
                                        <br />
										<label id="bcur" class="bold"><?php echo $strReceivingCurrency; ?></label>
									</td>
									<td>
										<label id="youbuyd" class="bold">
											<?php 
												echo $strReceivingCurrency." ";
												
												if($strReceivingAmount != 0)
												{
													echo number_format($strReceivingAmount,2,".",",");
												}
												else
												{
													echo number_format($fltConvertedAmnt,2,".",",");
													//debug($fltConvertedAmnt);
												
												}
												
											?>
										</label>
										<input name="receivingAmount" type="hidden" value="<?php if($strReceivingAmount != 0)
												{ echo $strReceivingAmount;
}else
{ echo $fltConvertedAmnt;
}
												?>"/>
										<input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; ?>"/>
										<!--<div>We pay you on 05 December 2012</div>-->
									</td>
								</tr>
								<tr>
									<td>
										<label id="exrt" class="bold">Exchange Rate:</label>
									</td>
									<td>
										<label id="numfo" class="bold exchange-rate"><?php echo number_format($fltExchangeRate,4,".",","); ?></label> <!--<em id="inverse" class="inverse-rate">Inverse <?php echo number_format($fltInverseRate,4,".",","); ?></em>-->
									</td>
									<input name="exchangeRate" type="hidden" value="<?php echo $fltExchangeRate; ?>"/>
									<input name="inverseRate" type="hidden" value="<?php echo $fltInverseRate; ?>"/>
								</tr>
								<tr>
									<td colspan="2" class="align_center">
										<input name="changeTrans" id="changeTrans" type="button" class="btn actv_btn" value="Change Quote" onClick="this.form.action='make_payment-i.php';this.form.submit();"/>
										<input name="changeTransaction" type="hidden" value="changeTransaction" />
									</td>
								</tr>
							</table>
						</div>
						<!--<div class="row align_center">
							<p class="bold">
								<input name="agreement" id="agreement" type="checkbox"/> I accept that by clicking accept and proceed i am entering into a legally binding contract and it will cost to change this contract if required.
							</p>
						</div>-->
						<div class="row align_right">
						<!--<div style="text-align:center; margin-bottom:10px">I accept that by clicking ACCEPT QUOTE, I am entering legally binding contract and a cost could be involved to change this.</div>-->
							<input name="reject" id="reject" type="button" class="btn simple-btn" value="REJECT" onClick="window.location.href='make_payment-i.php?reject'"/>
							<input name="accept" id="accept" type="submit" class="btn actv_btn" value="ACCEPT QUOTE"/>
							<a href="#" class="tip" id="acceptTip" data-placement="above" data-content="If you agree the above transaction details then click on the ACCEPT QUOTE button if not click REJECT" rel="popover" title="Agree or Reject">
								[?]
							</a>
						</div>
					</form>
				</div>
				<!--<div id="sidebar">
					<div class="step_head">
						<h2>STEP 1 TRANSACTION DETAILS</h2>
					</div>
					<p class="inner_step">Buying <?php echo number_format($strReceivingAmount, 4, ".", ",")." ".$arrExchangeRate['baseCurrency']; ?></p>
					<p class="inner_step">&#64; <?php echo $strExchangeRate; ?></p>
					<p class="inner_step">Selling <?php echo number_format($strSendingAmount, 4, ".", ",")." ".$arrExchangeRate['quotedCurrency']; ?></p>
					<div class="step_head">
						<h2>STEP 2 BENEFICIARY DETAILS</h2>
					</div>
					<p class="inner_step">&nbsp;<? echo $_SESSION["benBame"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["firstBen"]; ?></p>
                    <p class="inner_step">&nbsp;<? echo $_SESSION["secondBen"]; ?></p>
					<div class="step_head">
						<h2>STEP 3 PAYMENT DETAILS</h2>
					</div>
					<p class="inner_step">&nbsp;</p>
					<p class="inner_step">&nbsp;</p>
					<p class="inner_step">&nbsp;</p>
				</div>-->
			</div>
			<?php 
				include_once "includes/footer.php";
			?>
			</div>
		</div>
		<script type="text/javascript">
			$('#acceptTip').popover({
				trigger: 'hover',
				offset: 5
			});
			/* function checkAgree(){
				if(!($('#agreement').is(':checked'))){
					alert('Please click the confirmation of your details correction');
					return false;
				}
				else
					return true;
			} */
			$('#processTrans').submit(
				function(){
					if($(this).attr('action') == 'select_beneficiary_trans.php'){
						return checkAgree();
					}else{
						return true;
					}
				}
			);
			
function changeMySize(myvalue,iden)
{

if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
var tmin = document.getElementById("tmin");
var ctdwn = document.getElementById("countdown");
var msg= document.getElementById("msg");
var upay = document.getElementById("upay");
var pcur = document.getElementById("pcur");
var ubuy = document.getElementById("ubuy");
var bcur = document.getElementById("bcur");
var upayd = document.getElementById("youpayind");
var byd = document.getElementById("youbuyd");
var stcd = document.getElementById("exrt");
var ibnn= document.getElementById("numfo");
var swf = document.getElementById("inverse");
//var ref = document.getElementById("ref");

//tmin.style.fontSize = myvalue + "px";
//ctdwn.style.fontSize = myvalue + "px";
msg.style.fontSize = myvalue + "px";
upay.style.fontSize = myvalue + "px";
pcur.style.fontSize = myvalue + "px";
ubuy.style.fontSize = myvalue + "px";
bcur.style.fontSize = myvalue + "px";
upayd.style.fontSize = myvalue + "px";
byd.style.fontSize = myvalue + "px";
stcd.style.fontSize = myvalue + "px";
ibnn.style.fontSize = myvalue + "px";
swf.style.fontSize = myvalue + "px";

}

		</script>
	</body>
</html>