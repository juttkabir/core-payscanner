<?php
    session_start();
    include_once "includes/configs.php";
    include_once "includes/database_connection.php";
    dbConnect();
    include_once "includes/functions.php";

    require_once '../src/bootstrap.php';
    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl);
    $logoHtml = $agentUiData['logoHtml'];

    if ($_GET["from"] == 'registration'){
        $from = 'registration';
        $type = '';
        $customerID = $_SESSION["customerID"];
    } else if ($_POST["from"] == 'index') {
        $from = $_POST["from"];
        $type = $_POST["type"];
        $customerID = $_POST['customerID'];
    } else
        header("LOCATION:  index.php");

    $querCust = "SELECT Mobile FROM " . TBL_CUSTOMER . " WHERE customerID=$customerID";
    $getCust = selectFrom($querCust);

    $mobile = $getCust["Mobile"];
    $OTP = genRandomOTP();
    update("UPDATE customer SET OTPNumber = $OTP WHERE customerID=$customerID");
    $message = "Your OTP Number is " . $OTP;
    if ($mobile != '' && strlen($mobile) == 13 && substr($mobile, 0, 1) == '+')
        sendSMS($message, $mobile);
    else
        header("LOCATION:  index.php");
?>
<html>
<head>
    <title>Private Client Verification - Payscanner Online Currency Transfers</title>
    <?php include 'templates/script_header.php';?>
</head>
<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="login-section" class="fullheight">
    <div class="vcenter-container">
        <div class="vcenter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-3 col-lg-4 col-lg-push-4">
                        <h1 class="logo text-center">
                            <?= $logoHtml; ?>
                        </h1>
                        <div class="form-wrapper">
                            <h2 class="h4 title-underblock custom mb30">Verify to Online</h2>
                            <form method="post" action="verifyCustomerConf.php?from=<?php echo $from; ?>&type=<?php echo $type; ?>&customerID=<?php echo $customerID; ?>">
                                <div class="form-group">
                                    <label for="customerEmail" class="input-desc">OTP Number</label>
                                    <input type="text" id="OTPNumber" name="OTPNumber" placeholder="OTP Number" class="form-control" autocomplete="off" autofocus />
                                    <span id="OTPNumberValidator" class="please_do"></span>
                                </div><!-- End .from-group -->
                                <div class="form-group text-right clear-margin helper-group">
                                    <a href="verifyCustomer.php?from=<?php echo $from; ?>&type=<?php echo $type; ?>&customerID=<?php echo $customerID; ?>" class="add-tooltip" data-placement="top" title="Resend OTP Number">Resend OTP Number</a>
                                </div><!-- End .form-group -->
                                <div class="form-group mb5">
                                    <input type="submit" name="submit" value="Verify" class="btn btn-custom" onclick="return checkOTPNumber();" />
                                    <input type="hidden" name="type" id="type" value="<?=$type?>" />
                                    <input type="hidden" name="customerEmail" id="customerEmail" value="<?=$_POST["customerEmail"]?>" />
                                    <input type="hidden" name="password" id="password" value="<?=$_POST["password"]?>" />
                                </div><!-- End .from-group -->
                            </form>
                        </div><!-- End .form-wrapper -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .vcenter -->
    </div><!-- End .vcenter-container -->
</div><!-- End .fullheight -->

<script src="<?= $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script type="text/javascript">
    function checkOTPNumber() {
        if (document.getElementById("OTPNumber").value.length !== 6){
            document.getElementById("OTPNumberValidator").innerHTML = "<small style='color:red'>OTP Number is Invalid</small>";
            return false;
        }
        return true;
    }
</script>
<!--<div>-->
<!--    <form method="post" action="verifyCustomerConf.php?from=--><?php //echo $from; ?><!--&type=--><?php //echo $type; ?><!--&customerID=--><?php //echo $customerID; ?><!--">-->
<!--        <input type="text" name="OTPNumber" placeholder="Enter OTP Number" autofocus />-->
<!--        <input type="submit" value="Verify" />-->
<!--        <a href="verifyCustomer.php?from=--><?php //echo $from; ?><!--&type=--><?php //echo $type; ?><!--&customerID=--><?php //echo $customerID; ?><!--">Resend OTP Number</a>-->
<!--    </form>-->
<!--</div>-->

</body>
</html>