<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Send Transaction
	 * @author: M.Rizwan
	 */
	 $lifetime=60000;
  session_start();
  setcookie(session_name(),session_id(),time()+$lifetime);
	
	if(!isset($_SESSION['loggedInUser']['userID']) || empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
    require_once '../src/bootstrap.php';
    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentInfo = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
    $rateHelper = new \Payex\Online\Helper\RateHelper($online->getDb());
    $rates = $rateHelper->getRates();//$agentInfo['agentId'] ? $rateHelper->getRates($agentInfo['agentId']) : 

	//debug($_SESSION);
	//print_r($_SESSION);
	//debug($_SESSION['loggedInUser']['userID']);  
	
	$strQueryCustomer = "select * from user_id_types where user_id = '".$_SESSION['loggedInUser']['userID']."'";
	$strAccountName = selectMultiRecords($strQueryCustomer);

    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
    $title = $agentUiData['title'];
    $phone = $agentUiData['phone'];
    $logoHtml = $agentUiData['logoHtml'];

	//debug($strQueryCustomer);
	$strCountRecords = count($strAccountName);
	//debug($strCountRecords);
	//debug($strAccountName[0]["expiry_date"]);
	$TodayDate  = date('Y-m-d');   
	//debug($TodayDate);
	
	//debug($_SESSION);
		if($strCountRecords > 1){
		for($i=1;$i < $strCountRecords;$i++){
		
			if($strAccountName[$i]["expiry_date"] == $TodayDate || $strAccountName[$i]["expiry_date"] < $TodayDate){
			
			$var = 1;
		//	debug($var);
			
			}
		}
	}
	
	
	if(!empty($_REQUEST['ben'])){
		$_SESSION['ben'] = trim($_REQUEST['ben']);
		$strBenCont = selectFrom("SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName, bd.accountNo, bd.branchName, bd.branchAddress, bd.routingNumber FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName where ben.benID = '".$_SESSION['ben']."' ");
		$strBenCountryId = $strBenCont["countryId"];
		$arrBenBankRules = benBankDetailRule($strBenCountryId);
		$strBenName = "Name: ".$strBenCont["benName"];
		$accountName = $arrBenBankRules["accountName"];
		$accNo = $arrBenBankRules["accNo"];
		$branchNameNumber = $arrBenBankRules["branchNameNumber"];
		$branchAddress = $arrBenBankRules["branchAddress"];
		$swiftCode = $arrBenBankRules["swiftCode"];
		$IBAN = $arrBenBankRules["iban"];
		$routingNumber = $arrBenBankRules["routingNumber"];
		$sortCode = $arrBenBankRules["sortCode"];
		$strBenDetailFirst = "";
		$strBenDetailSecond = "";

			
			if($accountName == "Y")
			{
				
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Bank Name: ".$strBenCont["bankName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Bank Name: ".$strBenCont["bankName"];
					
				}
			}
			if($accNo == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Account No: ".$strBenCont["accountNo"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Account No: ".$strBenCont["accountNo"];
					
				}
			}
			if($branchNameNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Name: ".$strBenCont["branchName"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Name: ".$strBenCont["branchName"];
					
				}
			}
			if($branchAddress == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Branch Address: ".$strBenCont["branchAddress"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Branch Address: ".$strBenCont["branchAddress"];
					
				}
			}
			if($swiftCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Swift Code: ".$strBenCont["swiftCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Swift Code: ".$strBenCont["swiftCode"];
					
				}
			}
			if($IBAN == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "IBAN: ".$strBenCont["IBAN"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "IBAN: ".$strBenCont["IBAN"];
					
				}
			}
			if($routingNumber == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Routing Number: ".$strBenCont["routingNumber"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Routing Number: ".$strBenCont["routingNumber"];
					
				}
			}
			if($sortCode == "Y")
			{
				if($strBenDetailFirst == "")
				{
					$strBenDetailFirst = "Sort Code: ".$strBenCont["sortCode"]; 
					
				}
				else if($strBenDetailSecond == "")
				{
					$strBenDetailSecond = "Sort Code: ".$strBenCont["sortCode"];
					
				}
			}
			
			$_SESSION["benBame"] = $strBenName;
			$_SESSION["firstBen"] = $strBenDetailFirst;
			$_SESSION["secondBen"] = $strBenDetailSecond;
		
	}
	else
	{
			unset($_SESSION["benBame"]);
			unset($_SESSION["firstBen"]);
			unset($_SESSION["secondBen"]);
			unset($_SESSION['ben']);
		 
	}
	$intChangeTrans = false;
	$intReject = false;
	if(isset($_GET['reject'])){
		$intReject = true;
	}
	if(!empty($_REQUEST['changeTransaction']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') && (!empty($_REQUEST['sendingAmount']) && $_REQUEST['sendingAmount'] > '0') && !empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency'])){
		$intChangeTrans = true;
		$strReceivingAmount = trim($_REQUEST['receivingAmount']);
		$strSendingAmount = trim($_REQUEST['sendingAmount']);
		$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
		$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	}
	 
	$strQueryName = "SELECT CONCAT(firstName, ' ', middleName, ' ', lastName) AS customerName FROM ".TBL_CUSTOMER." WHERE accountName = '".$_SESSION['loggedInUser']['userID']."' ";
	$strCustomer = selectFrom($strQueryName);
	if(empty($_REQUEST['reject'])){
	 
	$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
	$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
	
	$fltExchangeRate = $_REQUEST["fltExchangeRate"];
	if($fltExchangeRate == ""){
	$fltExchangeRate= $_REQUEST["exchangeRate"];
	}
	if($_REQUEST["changeTransaction"] == "changeTransaction"){
	activities($_SESSION["loggedInUser"]["userID"],"Reject Quote","","","Quote rejected for ".$strSendingCurrency." - ".$strReceivingCurrency." = ".$fltExchangeRate."");
	}
	}
	
	unset($_SESSION["accountName"]);
	unset($_SESSION["accountNumber"]);
	unset($_SESSION["branchNameNumber"]);
	unset($_SESSION["branchAddress"]);
	unset($_SESSION["routingNumber"]);
	unset($_SESSION["sortCode"]);
	unset($_SESSION["swift"]);
	unset($_SESSION["reference"]);
	unset($_SESSION["existBen"]);
	unset($_SESSION["iban"]);
	unset($_SESSION["benCountry"]);
	unset($_SESSION["benName"]);
	unset($_SESSION["benCurrency"]);
   
	?>

<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Send Money - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>

</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'sendmoney'; ?>
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Send money</h1>
                        <p class="page-header-desc">Get a spot quote</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Send money</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">


            <div class="row">
                <div class="col-sm-8">
                    <?php if (isset($_SESSION['exchangeRateError'])):?>
                        <div class="alert alert-danger error_msg">
                            <?php if (isset($_SESSION['exchangeRateError'])) {
                                $strError = "class='error align_center'";
                                    if ($_SESSION['exchangeRateError'] == 'balance') {
                                        echo "You are not allowed to create transaction due to insufficient balance. Please contact " . $title . " support team.";
                                        unset($_SESSION['totalAmountInGBP']);
                                        unset($_SESSION['exchangeRateError']);
                                        unset($_SESSION['re']);
                                    } else if (isset($_SESSION['exchangeRateError'])) {
                                        $AmountInGBP = number_format($_SESSION['totalAmountInGBP'], 2);

                                        $alert_val = "'" . 'You can only send amount from £100 up to £50,000 or currency equivalent.' . '\n' . 'Your current amount in GBP = ' . $AmountInGBP . "'";

                                        if ($_SESSION['re'] == "N") {
                                            $alert_val = "'" . $_SESSION['exchangeRateError'] . "'";
                                        }
                                        echo "<script>alert($alert_val);</script>";
                                        echo $_SESSION['exchangeRateError'];

                                        unset($_SESSION['totalAmountInGBP']);
                                        unset($_SESSION['exchangeRateError']);
                                        unset($_SESSION['re']);

                                    } else {
                                        echo "&nbsp;";
                                    }
                            } ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Get a spot quote</h2>
                        <p>Enter the amount of currency you wish to send, or the amount of currency you wish to receive.</p>
                        <form action="make_paymentnew.php" method="get" id="getRate">

                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <?php
//                                            echo count($rates);
                                        ?>
                                        <label for="sendingCurrency" class="input-desc">Currency to send:</label>
                                        <select name="sendingCurrency" id="sendingCurrency" class="form-control select_field">
                                            <option value="">- Select Currency -</option>
                                            <?php
                                            sort($rates);
                                            for ($x = 0; $x < count($rates); $x++) {
                                                echo "<option value='" . $rates[$x]['currencies'] . "'>" . $rates[$x]['currencies'] . "</option>";
                                            } ?>
                                        </select>
                                    </div><!-- End .from-group -->
                                    <div class="form-group mb10">
                                        <label for="receivingCurrency" class="input-desc">Currency to receive:</label>
                                        <select name="receivingCurrency" id="receivingCurrency" class="form-control select_field">
                                            <option value="">- Select Currency -</option>
                                            <?php
                                            sort($rates);
                                            for ($x = 0; $x < count($rates); $x++) {
                                                echo "<option value='" . $rates[$x]['currencies'] . "'>" . $rates[$x]['currencies'] . "</option>";
                                            } ?>
                                        </select>
                                    </div><!-- End .from-group -->


                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="sendingAmount" class="input-desc">Amount:</label>
                                        <input name="sendingAmount" id="sendingAmount" type="text" maxlength="20" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                    </div><!-- End .from-group -->
                                    <div class="form-group mb40">
                                        <label for="receivingAmount" class="input-desc">Amount:</label>
                                        <input name="receivingAmount" id="receivingAmount" type="text" maxlength="20" placeholder="" class="form-control form_fields min_field" autocomplete="off">
                                    </div><!-- End .from-group -->
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group mb5">
                                        <input type="submit" class="btn btn-custom mr10" value="Get rate">
                                        <input type="reset" class="btn btn-default" value="Clear">
                                    </div><!-- End .from-group -->
                                </div>

                            </div>


                        </form>
                    </div><!-- End .form-wrapper -->

                </div><!-- End .col-sm-6 -->

                <div class="mb40 visible-xs"></div><!-- space -->

                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>

                    <p>Send money safely and securely with <?= $title; ?> Online.</p>

                    <div class="mb10"></div><!-- space -->

                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->

            </div><!-- End .container -->

            <div class="mb40 mb20-xs"></div><!-- space -->
            <footer id="footer" class="footer-inverse" role="contentinfo">
                <?php include 'templates/footer.php'; ?>
            </footer>
            <!-- End #footer -->

        </div>
        <!-- End #content -->
    </div>
    <!-- End #wrapper -->

    <a href="#top" id="scroll-top" title="Back to Top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- END -->
    <script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        ToadayDate = '<?php echo $TodayDate;?>';
        ExpiryDate = '<?php echo $strAccountName[0]["expiry_date"];?>';
        VarMultiRecords = '<?php echo $var; ?>';

        if (ExpiryDate == ToadayDate || ExpiryDate < ToadayDate || VarMultiRecords != '') {
            var elem = document.getElementById("test");
            elem.style.display = "none";
            alert("You have an expired ID, Please contact <?= $title; ?> support");
        }
    });
</script>
<script type="text/javascript">
    function SelectOption(OptionListName, ListVal) {
        for (i = 0; i < OptionListName.length; i++) {
            if (OptionListName.options[i].value == ListVal) {
                OptionListName.selectedIndex = i;
                break;
            }
        }
    }

    function checkAmounts() {
        sendingAmount = $.trim($('#sendingAmount').val());
        receivingAmount = $.trim($('#receivingAmount').val());
        //receivingAmount = parseFloat(receivingAmount).toFixed(2);
        //sendingAmount = parseFloat(sendingAmount).toFixed(2);
        if (receivingAmount == '' && sendingAmount != '') {
            $('#receivingAmount').attr('disabled', 'disabled');
            return;
        } else if (receivingAmount != '' && sendingAmount == '') {
            $('#sendingAmount').attr('disabled', 'disabled');
            return;
        }
    }

    function clearAll() {
        $('#receivingAmount').removeAttr('disabled');
        $('#sendingAmount').removeAttr('disabled');

    }
			
			<?php 
				if($intChangeTrans){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $strReceivingCurrency; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $strSendingCurrency; ?>');
				$('#sendingAmount').focus(
					function(){
						$('#receivingAmount').attr('disabled', 'disabled');
					}
				);
				$('#receivingAmount').focus(
					function(){
						$('#sendingAmount').attr('disabled', 'disabled');
					}
				);
			<?php 
				}elseif((isset($_SESSION['sendingCurrency']) || isset($_SESSION['receivingCurrency'])) && !$intReject){
			?>
				SelectOption(document.forms[0].receivingCurrency, '<?php echo $_SESSION['receivingCurrency']; ?>');
				SelectOption(document.forms[0].sendingCurrency, '<?php echo $_SESSION['sendingCurrency']; ?>');
			<?php 
				unset($_SESSION['receivingCurrency']);
				unset($_SESSION['sendingCurrency']);
				}
			?>
    $('#getRateTip').popover({
        trigger: 'hover',
        offset: 5
    });

    $('#sendingAmount').blur(
        function () {
            sendingAmount = $.trim($(this).val());
            if (sendingAmount != '') {
                //$('#receivingAmount').attr('readonly', 'readonly');
                $('#receivingAmount').attr('disabled', 'disabled');
                //$(this).removeAttr('readonly');
                $(this).removeAttr('disabled');
            } else {
                //$('#receivingAmount').removeAttr('readonly');
                $('#receivingAmount').removeAttr('disabled');
            }
        }
    );

    $('#receivingAmount').blur(
        function () {
            receivingAmount = $.trim($(this).val());
            if (receivingAmount != '') {
                receivingAmount = parseFloat(receivingAmount).toFixed(2);
                if (receivingAmount < 0) {
                    $('#error').addClass('error');
                    $('#error').parent().addClass('error');
                    $('#error').addClass('align_center');
                    $('#error').text('Receiveing amount cannot be negative.');
                } else {
                    $('#error').removeClass('error');
                    $('#error').parent().removeClass('error');
                    $('#error').text('');
                    //$('#sendingAmount').attr('readonly', 'readonly');
                    $('#sendingAmount').attr('disabled', 'disabled');
                    //$(this).removeAttr('readonly');
                    $(this).removeAttr('disabled');
                }
            } else {
                //$('#sendingAmount').removeAttr('readonly');
                $('#sendingAmount').removeAttr('disabled');
            }
        }
    );
    <?php
    if ($intChangeTrans) {
        echo "$('#sendingAmount').val('" . round(floor($strSendingAmount * 100) / 100, 2) . "');";
        echo "$('#receivingAmount').val('" . round(floor($strReceivingAmount * 100) / 100, 2) . "');";
    } elseif ((isset($_SESSION['sendingAmount']) || isset($_SESSION['receivingAmount'])) && !$intReject) {
        if (isset($_SESSION['sendingAmount'])) {
            echo "$('#receivingAmount').val('" . number_format($_SESSION['sendingAmount'], 2, ".", ",") . "');";
            unset($_SESSION['sendingAmount']);
        }
        if (isset($_SESSION['receivingAmount'])) {
            echo "$('#receivingAmount').val('" . round(floor($_SESSION['receivingAmount'] * 100) / 100, 2) . "');";
            unset($_SESSION['receivingAmount']);
        }
    }
    ?>
    $('#getRate').validate({
        rules: {
            receivingCurrency: {
                required: true
            },
            receivingAmount: {
                required: function () {
                    sendingAmount = $.trim($('#sendingAmount').val());
                    if (sendingAmount == '')
                        return true;
                    else
                        return false;
                },
                number: true
            },
            sendingAmount: {
                required: function () {
                    receivingAmount = $.trim($('#receivingAmount').val());
                    if (receivingAmount == '')
                        return true;
                    else
                        return false;
                },
                number: true
            },
            sendingCurrency: {
                required: true
            }
        },
        messages: {
            sendingAmount: {
                required: '<br><font style="color:#FF0000">Enter amount to send.</font>',
                number: '<br><font style="color:#FF0000">Amount can only be numbers</font>'
            },
            receivingAmount: {
                required: '<br><font style="color:#FF0000">Enter amount to receive.</font>',
                number: '<br><font style="color:#FF0000">Amount can only be numbers</font>'
            }
        }
    });
</script>
</html>
