<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Change Password
	 * @author: Mirza Arslan Baig
	 */
	ini_set("display_errors","on");
	error_reporting(1);
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	if(isset($_POST['changePassword']) && !empty($_POST['oldPass']) && !empty($_POST['pass1'])){
		$strQueryChkOldPass = "SELECT password FROM ".TBL_CUSTOMER." WHERE customerID = '".$_SESSION['loggedInUser']['userID']."'";
		//debug($strQueryChkOldPass);
		$arrChkOldPass = selectFrom($strQueryChkOldPass);
		//debug($arrChkOldPass);
		$strOldPassword = $_POST['oldPass'];
		$strNewPass1 = mysql_real_escape_string($_POST['pass1']);
		$strNewPass2 = mysql_real_escape_string($_POST['pass2']);
		if($arrChkOldPass['password'] == $strOldPassword && $strNewPass1 == $strNewPass2){
			$strQueryChangePass = "UPDATE ".TBL_CUSTOMER." SET `password` = '$strNewPass1' WHERE customerID = '".$_SESSION['loggedInUser']['userID']."'";
			if(update($strQueryChangePass)){
				$strMsg = "Password changed successfully!!";
			}
			else{
				$strError = "Password can not be changed.Please try again";
			}
		}elseif($arrChkOldPass['password'] == $strOldPassword){
			$strError = "Enter valid old password!!";
		}else{
			$strError = "Your passwords did not matched!!";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Change Password</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="scripts/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="scripts/jquery.validate.js"></script>
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				$currentPage = 'change password';
				require_once "includes/header.php";
			?>
			<div id="container" >
				<div id="content_one">
					<h1 id="cont_head" class="cont_head">Change Password</h1>
					<form id="changePasswordForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
						<table border="0" class="center">
							<?php 
								if(isset($strError) && !empty($strError)){
							?>
							<tr class="error">
								<td class="error align_center" colspan="2"><?php echo $strError; ?></td>
							</tr>
							<?php 
								}
								if(isset($strMsg) && !empty($strMsg)){
							?>
							<tr class="success align_center">
								<td class="success" colspan="2"><?php echo $strMsg; ?></td>
							</tr>
							<?php 
								}
							?>
							<tr>
								<td class="bold">
									<label id="old_pass">Old Password <em>*</em> : </label>
								</td>
								<td>
									<input name="oldPass" id="oldPass" type="password" class="input-large" />
								</td>
							</tr>
							<tr>
								<td class="bold">
									<label id="new_pass">New Password <em>*</em> :</label>
								</td>
								<td>
									<input name="pass1" id="pass1" type="password" class="input-large" maxlength="32"/>
								</td>
							</tr>
							<tr>
								<td class="bold">
									<label id="c_new_pass">Confirm new password <em>*</em> : </label>
								</td>
								<td>
									<input name="pass2" id="pass2" type="password" class="input-large" maxlength="32"/>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="bold"><p id="mand"><em>*</em> Mandatory Fields</p></td>
							</tr>
							<tr>
								<td class="align_center" colspan="4">
									<input name="changePassword" id="changePassword" type="submit" value="Submit" class="btn actv_btn input-medium"/>
									<input type="reset" value="Clear" class="btn simple-btn input-small"/>
								</td>
							</tr>
						</table>
				</div>
			</div>
			<?php 
				require_once "includes/footer.php";
			?>
		</div>
		<script type="text/javascript">
			
		
			$('#changePasswordForm').validate({
				rules: {
					oldPass: {
						required: true,
						minlength: 6
					},
					pass1: {
						required: true,
						minlength: 6
					},
					pass2: {
						required: true,
						equalTo: '#pass1'
					},
					messages:{
						oldPass: 'Enter Old Password to change the password.',
						pass1: 'Enter new password',
						pass2: { required: 'Re-enter your new password'
						}
					}
				}
			});
			function changeMySize(myvalue,iden)
// this function is called by the user clicking on a text size choice
{

	if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
// find the div to apply the text resizing to
		var ch = document.getElementById("cont_head");
		var op= document.getElementById("old_pass");
		var np = document.getElementById("new_pass");
		var cnp = document.getElementById("c_new_pass");
		var man = document.getElementById("mand");		
		ch.style.fontSize = myvalue + "px";
		op.style.fontSize = myvalue + "px";
		np.style.fontSize = myvalue + "px";
		cnp.style.fontSize = myvalue + "px";
		man.style.fontSize = myvalue + "px";
		
}
		</script>
	</body>
</html>