<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Transaction History
	 * @author: Mirza Arslan Baig
	 */
require_once '../src/bootstrap.php';
	
	session_start();

	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	$custIDi = $_SESSION['loggedInUser']['userID'];
    $currentUrl = $_SERVER['HTTP_HOST'];
    $agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
    $title = $agentUiData['title'];
    $logoHtml = $agentUiData['logoHtml'];
	//checking user type to be logged in
	$userLoggedinType ="";

if ($_SESSION['loggedInUser']['online_customer_services'] == "TC") {
    $userLoggedinType = "AND transType = 'Bank Transfer'";
} else {
    $userLoggedinType = "AND transType = 'Topup'";
}
		
		
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	include_once "includes/functions.php";
	
	if(isset($_GET['limit']) && $_GET['limit'] > 0)
		$limit = intval(trim($_GET['limit']));
	else
		$limit = 5;
	if(!empty($_GET['offset']))
		$offset = $_GET['offset'];
	elseif(!empty($_POST['offset']))
		$offset = $_POST['offset'];
	else
		$offset = 0;

	$next = $offset + $limit;
	$prev = $offset - $limit;
	
	
	dbConnect();
	//print_r($_REQUEST);
	$intSearchFlag = false;
	if(!empty($_POST['search']) || !empty($_GET['search'])){
		$intSearchFlag = true;
		
		
		if(!empty($_POST['search'])){
		
			
			
			$fromDate=$_POST['fromDate'];
			sscanf($fromDate, '%d-%d-%d', $d, $m, $Y);
			$strFromDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			debug ($strFromDate);
			
			$strFromDate=$strFromDate." 00:00:00";
			$toDate=$_POST['toDate'];
			
			sscanf($toDate, '%d-%d-%d', $d, $m, $Y);
			$strToDate = date("Y-m-d", strtotime("$Y-$m-$d"));
			$strToDate=$strToDate." 23:59:59";
			debug ($strToDate);
			
			// $strSearchText = trim($_POST['searchText']);
			// $strSearchType = $_POST['searchType'];
			// $strAmountSearchCriteria = $_POST['amountSearch'];
			// $strAmount = trim($_POST['amount']);
			// $strCurrency = $_POST['currency'];
			// $strTransStatus = $_POST['status'];
			// $strApplyOnAmount = $_POST['applyOnAmount'];
			// $strCondition = " ";
		}else{
			//$strFromDate=$_GET['fromDate']." 00:00:00";
			$strFromDate=2008-12-11;
			//$strToDate = $_GET['toDate']." 23:59:59";
			$strToDate = date("Y-m-d")." 23:59:59";
			$strSearchText = trim($_GET['searchText']);
			
			// $strSearchType = $_GET['searchType'];
			// $strAmountSearchCriteria = $_GET['amountSearch'];
			// $strAmount = trim($_GET['amount']);
			// $strCurrency = $_GET['currency'];
			// $strTransStatus = $_GET['status'];
			// $strApplyOnAmount = $_GET['applyOnAmount'];
			// $strCondition = " ";
		}
		// if(!empty($strSearchText) && $strSearchType == 'transRefNumber')
			// $strCondition .= " AND refNumberIM = '$strSearchText'";
		// elseif(!empty($strSearchText) && $strSearchType == 'beneficiaryName')
			// $strCondition .= " AND (ben.firstName LIKE '$strSearchText%' OR ben.middleName LIKE '$strSearchText%' OR ben.lastName LIKE '$strSearchText%' OR ben.beneficiaryName LIKE '$strSearchText%') ";
		
		// if(!empty($strApplyOnAmount)){
			// if($strApplyOnAmount == 'sendingAmount')
				// $strApplyAmount = "transAmount";
			// elseif($strApplyOnAmount == 'receivingAmount')
				// $strApplyAmount = "localAmount";
			// else
				// $strApplyAmount = "transAmount";
		// }
		
		// if($strAmountSearchCriteria != 'all' && !empty($strAmountSearchCriteria)){
			// switch($strAmountSearchCriteria){
				// case ">":
					// $strCondition .= " AND $strApplyAmount > '$strAmount' ";
					// break;
				// case "<":
					// $strCondition .= " AND $strApplyAmount < '$strAmount' ";
					// break;
				// case "=":
					// $strCondition .= " AND $strApplyAmount = '$strAmount'";
					// break;
				// case ">=":
					// $strAmount .= " AND $strApplyAmount >= '$strAmount' ";
					// break;
				// case "<=":
					// $strCondition .= " AND $strApplyAmount <= '$strAmount' ";
					// break;
			// }
		// }
		
		// if(!empty($strCurrency) && $strCurrency != 'all'){
			// $strCondition .= " AND currencyFrom = '$strCurrency' ";
		// }
		
		// if($strTransStatus != 'all' && !empty($strTransStatus))
			// $strCondition .= " AND transStatus = '$strTransStatus' ";
		$strCondition .= " AND (transDate >= '$strFromDate' AND transDate <= '$strToDate')";
		
		
		//$strCondition .= " AND trans_source = 'O'";
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, transType,DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus, transType,DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType ".$strCondition;
		
		
		$strQueryGroupBy = " group by transDate DESC";
		$strQueryPagination = " LIMIT $offset, $limit";
		//$strQuerySearch .= $strCondition;
		$strQuerySearch .= $strQueryGroupBy;
		$exportQuery = $strQuerySearch;
		$strQuerySearch .= $strQueryPagination;
		$advanced_Search= "search";
		
		
	}else{
		$strQuerySearch = "SELECT refNumberIM AS refNumber, transStatus,transType, DATE_FORMAT(transDate, '%d-%m-%Y') AS transDate, FORMAT(transAmount, 2) AS sendingAmount, FORMAT(localAmount, 2) AS recievingAmount, FORMAT(exchangeRate, 4) AS rate,currencyFrom AS sendingCurrency, currencyTo AS recievingCurrency, CONCAT(ben.firstName, ben.middleName, ben.lastName) AS benName FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType  group by transDate DESC";

		$strQueryCountTotal = "SELECT COUNT(transID) AS records FROM ".TBL_TRANSACTIONS." AS trans INNER JOIN ".TBL_BENEFICIARY." AS ben ON ben.benID = trans.benID WHERE trans.customerID = '".$_SESSION['loggedInUser']['userID']."' $userLoggedinType";
		
		$strQueryPagination = " LIMIT $offset, $limit";
		
		$exportQuery = $strQuerySearch;
		$strQuerySearch .= $strQueryPagination;
		
		$advanced_Search= "";
		
	}
	
		$arrTotalRecords = selectFrom($strQueryCountTotal);
		$intTotalRecords = $arrTotalRecords['records'];
		$arrTrans = selectMultiRecords($strQuerySearch);
		$intTotalTrans = count($arrTrans);
	//print_r($strQuerySearch);
	//echo $userLoggedinType;
	/* debug($intTotalRecords);
	debug($strQuerySearch);
	debug($strQueryCountTotal); */
	
		$strUserId=$_SESSION['loggedInUser']['accountName'];
		$strEmail=$_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>View transactions - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>
    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'transactions'; ?>
        <?php include 'templates/header.php'; ?>
    </header>
    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Transactions</h1>
                        <p class="page-header-desc">Search your transactions</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Transactions</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Search your transactions</h2>
                        <p>Please select date range.</p>

                        <div class="row">
                            <form id="filters" action="<?php echo $_SERVER['PHP_SELF']; ?>#nav_list01" method="post">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fromDate" class="input-desc">Date from</label>
                                        <div class="input-group date form-date" data-date=""
                                             data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                             data-link-format="dd-mm-yyyy">
                                            <input class="form-control" id="fromDate" name="fromDate" size="10" type="text" value="" readonly>
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div><!-- End .input-group -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="toDate" class="input-desc">Date to</label>
                                        <div class="input-group date form-date" data-date=""
                                             data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                             data-link-format="dd-mm-yyyy">
                                            <input class="form-control" id="toDate" name="toDate" size="10" type="text" value="" readonly>
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div><!-- End .input-group -->
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group mb5">
                                        <input type='hidden' name='userIDi' id='userIDi' value='<?echo $custIDi ?>' />
                                        <input name="search" id="search" type="submit" class="btn btn-custom" value="Search">
                                        <input type="reset" class="btn btn-default" value="Clear">

                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb5 pull-right">
                                    <form action="export_transaction.php?query=<?echo base64_encode(urlencode($exportQuery)); ?>&querycount=<?echo $intTotalRecords; ?>" method="post" style="display:inline-block;">
                                        <input id="export" class="btn btn-custom pull-right add-tooltip" data-placement="top" title="Click here to download as excel file" type="submit" value="Export to Excel" >
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!-- End .form-wrapper -->
                </div><!-- End .col-sm-6 -->
                <div class="mb40 visible-xs"></div><!-- space -->
                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
                    <p>Send money safely and securely with <?= $title; ?> Online.</p>
                    <div class="mb10"></div><!-- space -->
                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->
            <div class="row" id="nav_list01">
                <div class="col-sm-12">
                    <h2 class="title-border custom mt40 mb40">List of transactions</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-condensed">
                            <tr>
                                <td>Showing <?php echo ($offset + 1) . " - " . ($intTotalTrans + $offset) . " of " . $intTotalRecords ?></td>
                                <td>
                                    <div class="pull-left mt10">Records Per Page:</div>
                                    <select name="limit" id="limit" class="input-small form-control pull-right mb0" style="width:auto;" onChange="javascript:document.location.href='<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType; ?>&status=<?php echo $strStatus; ?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&search=search&limit='+this.value;+'#nav_list01'" >
                                        <option value="5" <?php if (!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '5') {echo ' selected="selected"';}?>>5</option>
                                        <option value="10" <?php if (!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '10') {echo ' selected="selected"';}?>>10</option>
                                        <option value="20" <?php if (!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '20') {echo ' selected="selected"';}?>>20</option>
                                        <option value="30" <?php if (!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '30') {echo ' selected="selected"';}?>>30</option>
                                        <option value="50" <?php if (!empty($_REQUEST['limit']) && $_REQUEST['limit'] == '50') {echo ' selected="selected"';}?>>50</option>
                                    </select>
                                </td>
                                <td >
                                    <?php
                                    if ($prev >= 0) { ?>
                                        <a class="btn btn-default" href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=0&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType; ?>&status=<?php echo $strStatus; ?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search; ?>#nav_list01"><< First</a>

                                        <a class="btn btn-default" href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $prev; ?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType; ?>&status=<?php echo $strStatus; ?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search; ?>#nav_list01">< Previous</a>

                                        <?php
                                    }
                                    if ($next > 0 && $next < $intTotalRecords) {
                                        ?>
                                        <a class="btn btn-default" href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $next; ?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType; ?>&status=<?php echo $strStatus; ?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search; ?>#nav_list01">Next ></a>

                                        <?php
                                        $intLastOffset = (ceil($intTotalRecords / $limit) - 1) * $limit;
                                        ?>
                                        <a class="btn btn-default" href="<?php echo $_SERVER['PHP_SELF']; ?>?offset=<?php echo $intLastOffset; ?>&fromDate=<?php echo $strFromDate; ?>&toDate=<?php echo $strToDate; ?>&searchText=<?php echo $strSearchText; ?>&searchType=<?php echo $strSearchType; ?>&status=<?php echo $strStatus; ?>&amountSearch=<?php echo $strAmountSearchCriteria; ?>&amount=<?php echo $strAmount; ?>&currency=<?php echo $strCurrency; ?>&limit=<?php echo $limit; ?>&search=<?php echo $advanced_Search; ?>#nav_list01">Last >></a>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>

                        <?php if ($intTotalTrans > 0) {?>

                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>
                                        Transaction Reference No.
                                    </th>
                                    <th>
                                        Transaction Date
                                    </th>
                                    <th>
                                        Beneficiary Name
                                    </th>
                                    <th>
                                        Sending Amount
                                    </th>
                                    <th>
                                        Sending Currency
                                    </th>
                                    <th>
                                        Receiving Amount
                                    </th>
                                    <th>
                                        Receiving Currency
                                    </th>
                                    <th width="50px">
                                        Rate
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  for ($x = 0; $x < $intTotalTrans; $x++) { ?>
                                    <tr>
                                        <td width="80px;">
                                            <?php echo $arrTrans[$x]['refNumber']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['transDate']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['benName']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['sendingAmount']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['sendingCurrency']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['recievingAmount']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrTrans[$x]['recievingCurrency']; ?>
                                        </td>
                                        <td width="50px">
                                            <?php echo $arrTrans[$x]['rate']; ?>
                                        </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                            <?php
                        } elseif (isset($_REQUEST['search'])) {
                            ?>
                            <div class="alert alert-info">
                                There are no transaction(s) to display. <a href="">View all transactions</a>
                            </div>
                        <?php }?>

                    </div><!-- End .table-responsive -->
                </div>
            </div><!-- End .row -->

        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('.form-date').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    });


    $(document).ready(function () {
        Date.format = 'dd-mm-yyyy';

        $("#printReport").click(function () {
            // Maintain report print logs
            $.ajax({
                url: "maintainReportsLogs.php",
                type: "post",
                data: {
                    maintainLogsAjax: "Yes",
                    pageUrl: "<?=$_SERVER['PHP_SELF']?>",
                    action: "P"
                }
            });

            $("#searchTable").hide();
            $("#pagination").hide();
            $("#actionBtn").hide();
            $(".actionField").hide();
            print();
            $("#searchTable").show();
            $("#pagination").show();
            $("#actionBtn").show();
            $(".actionField").show();
        });


        //************ajax call for ref number************//

        $("#searchText").keyup(function () {
            var searchType = $("#searchType").val();
            var searchText = $("#searchText").val();
            var custIDi = $("#userIDi").val();
            var htmlData = '';

            $.ajax({
                url: "../registration_form_conf.php",
                type: "post",
                async: false,
                error: function (xhr, status) {
                    console.log("xhr : " + xhr + " Status: " + status);
                },
                data: {
                    action: "showRefNumber",
                    searchType: searchType,
                    searchText: searchText,
                    custIDi: custIDi
                },
                success: function (data) {
                    htmlData = data;
                }
            });

            $("#showRefNumber").html(htmlData);
            $("#_showRefNumber").show();

        });

    });

    function selectRefNumber(getid) {
        var ids = $("#searchText").val(getid);
        $("#_showRefNumber").hide();
    }
</script>
</body>
</html>
