<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Add Beneficiary
	 * @author: Mirza Arslan Baig
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php?notLogin=1");
	}
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	
	
	include_once "includes/functions.php";
	$benCountries="select countryId,countryName,isocode,ibanlength from countries";
		//debug($benCountries);
		$benCountriesResult=selectMultiRecords($benCountries);
		//debug($benCountriesResult);
	
	if(!empty($_POST['addBen']) || !empty($_POST['updateBen'])){
		$patternIBAN = '/((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/';
		$strIBAN = mysql_real_escape_string(trim($_POST['iban']));
		$strBenName = mysql_real_escape_string(trim($_POST['benName']));
		
		$strBenCountry = mysql_real_escape_string($_POST['benCountry']);
		$strBenCountryId = $strBenCountry;
		$strBenCurrency = mysql_real_escape_string(trim($_POST['benCurrency']));
		$strSwiftCode = mysql_real_escape_string(trim($_POST['swift']));
		$strSortCode = mysql_real_escape_string(trim($_POST['sortCode']));
		$strReference = mysql_real_escape_string(trim($_POST['reference']));
		$strAccountName = mysql_real_escape_string(trim($_POST['accountName']));
		$strRoutingNumber = mysql_real_escape_string(trim($_POST['routingNumber']));
		$strAccountNumber = mysql_real_escape_string(trim($_POST['accountNumber']));
		$strSendingCurrency = mysql_real_escape_string(trim($_POST['sendingCurrency']));
		
		//debug($strBenCountry);
		$intCustomerID = $_SESSION['loggedInUser']['userID'];
		if(!empty($_POST['benID'])){
			$intBenID = mysql_real_escape_string(trim($_POST['benID']));
		}
		$strCurrentDate = date("Y-m-d");
		//if(preg_match($patternIBAN, $strIBAN)){
			if(!empty($intBenID)){
				$benCountry = selectFrom("select countryName from countries where countryId = '".$strBenCountry."' ");
				$strBenCountry = $benCountry["countryName"];
				
				$strQueryUpdateBen = "UPDATE ".TBL_BENEFICIARY." SET `firstName` = '$strBenName', `beneficiaryName` = '$strBenName', `Country` = '$strBenCountry', `reference` = '$strReference' WHERE benID = '$intBenID'";
				/*$strConditionBenUpdate = "";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ,";
				if(!empty($strSwiftCode))
					$strConditionBenUpdate .= " `swiftCode` = '$strSwiftCode', ";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ";
				if(!empty($strIBAN))
					$strConditionBenUpdate .= " `IBAN` = '$strIBAN' ";*/
				
				
				
				
				if(update($strQueryUpdateBen)){
					$strMsg = "Beneficiary updated successfully";
				}else{
					$strError = "Beneficiary cannot be updated";
				}
			}else{
				$benCountry = selectFrom("select countryName from countries where countryId = '".$strBenCountry."' ");
				$strBenCountry = $benCountry["countryName"];
				$strQueryInsertBen = "INSERT INTO ".TBL_BENEFICIARY." (`firstName`, `middleName`, `lastName`, `beneficiaryName`, `Country`, `reference`, `customerID`, `created`) VALUES('$strBenName', '', '', '$strBenName', '$strBenCountry', '$strReference', '$intCustomerID', '$strCurrentDate')";
				if(insertInto($strQueryInsertBen)){
					$strMsg = "Beneficiary added successfully";
					$intBenID = mysql_insert_id();
				}else{
					$strError = "Beneficiary cannot be added";
				}
			}
			
			if(!empty($intBenID)){
				$strQueryChkBankInfo = "SELECT count(id) AS record FROM ".TBL_BEN_BANK_DETAILS." WHERE benID = '$intBenID'";
				$arrBenBankInfo = selectFrom($strQueryChkBankInfo);
				if($arrBenBankInfo['record'] == 0){
					$strQueryInsertBenBank = "INSERT INTO ".TBL_BEN_BANK_DETAILS." (`benId`, `IBAN`, `swiftCode`,`sortCode`, `bankName`,`accountNo`,`routingNumber`,sendCurrency) VALUES('$intBenID', '$strIBAN', '$strSwiftCode','$strSortCode', '$strBenName','$strAccountNumber','$strRoutingNumber','$strSendingCurrency')";
					//debug($strQueryInsertBenBank);
					if(!insertInto($strQueryInsertBenBank))
						$strError .= " and beneficiary bank info cannot added.";
				}else{
					$strQueryUpdateBenBank = "UPDATE ".TBL_BEN_BANK_DETAILS." SET";
					if(!empty($strIBAN)){ 
					$strQueryUpdateBenBank .="`IBAN` = '$strIBAN',"; 
					} 
					if(!empty($strSwiftCode)){ 
					$strQueryUpdateBenBank .="`swiftCode` = '$strSwiftCode',"; 
					} 
					/*
					if(!empty($strBenAccountName)){ 
					$strQueryUpdateBenBank .="`bankName` = '$strBenAccountName',"; 
					} 
					*/
					if(!empty($strBenName)){ 
					$strQueryUpdateBenBank .="`bankName` = '$strBenName',"; 
					}
					if(!empty($strAccountNumber)){
					$strQueryUpdateBenBank .="`accountNo` = '$strAccountNumber',"; 
					} 
					if(!empty($strBranchNameNumber)){ 
					$strQueryUpdateBenBank .="`branchCode` = '$strBranchNameNumber',"; 
					} 
					 if(!empty($strBranchAddress)){
					$strQueryUpdateBenBank .="`branchAddress` = '$strBranchAddress',"; 
					} 
					 if(!empty($strSortCode)){
					$strQueryUpdateBenBank .="`sortCode` = '$strSortCode',"; 
					} 
					 if(!empty($strRoutingNumber)){ 
					$strQueryUpdateBenBank .="`routingNumber` = '$strRoutingNumber',"; 
					} 
					if(!empty($strSendCurrency)){
					$strQueryUpdateBenBank .="`sendCurrency` = '$strSendCurrency',"; 
					} 
					$strQueryUpdateBenBank = substr($strQueryUpdateBenBank,0,strlen($strQueryUpdateBenBank )-1);
					$strQueryUpdateBenBank .= " WHERE benId = '$intBenID'";
					//debug($strQueryUpdateBenBank);
					if(update($strQueryUpdateBenBank))
						$strMsg .= " and beneficiary bank info is updated";
						//$strMsg .= " and beneficiary iban and swift code is updated";
					else
						$strError .= " and beneficiary bank info cannot be updated";
				}
			}
		/*}else{
			$strError = "IBAN number is invalid";
		}*/
		
	}
	
	if(!empty($_POST['ben'])){
		$intBenID = trim($_POST['ben']);
		$strQueryBen = "SELECT countries.countryId, ben.benID, ben.firstName AS benName, ben.Country, ben.reference, bd.swiftCode,bd.sortCode, bd.IBAN, bd.bankName,bd.routingNumber, bd.accountNo FROM ".TBL_BENEFICIARY." AS ben LEFT JOIN ".TBL_BEN_BANK_DETAILS." AS bd ON bd.benId = ben.benID left join countries on ben.Country=countries.countryName WHERE ben.customerID = '{$_SESSION['loggedInUser']['userID']}' AND ben.status = 'Enabled' AND ben.benID = '$intBenID'";
		//debug($strQueryBen);
		$arrBen = selectFrom($strQueryBen);
	
		$strIBAN = $arrBen['IBAN'];
		$strBenName = $arrBen['benName'];
		$strBenCountry = $arrBen['Country'];
		$strBenCountryId = $arrBen['countryId'];
		//$strBenCurrency = $arrBen['benCurrency'];
		$strSwiftCode = $arrBen['swiftCode'];
		$strSortCode = $arrBen['sortCode'];
		$strReference = $arrBen['reference'];
		$intCustomerID = $arrBen['customerID'];
		$strAccountName = $arrBen['bankName'];
		$strRoutingNumber=$arrBen['routingNumber'];
		$strAccountNumber = $arrBen['accountNo'];
		
	}

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php if(!empty($intBenID)) echo "Edit"; else echo "Add"; ?> Beneficiary</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
		<script src="scripts/jquery-1.7.2.min.js"></script>
		<script src="scripts/jquery.validate.js"></script>
		<script src="scripts/bootstrap-twipsy.js"></script>
		<script src="scripts/bootstrap-popover.js"></script>
        <script type="text/javascript" src="../admin/javascript/jquery.maskedinput.min.js"></script>
		
		
		
	</head>
	<body>
		<div id="wrapper" >
			<?php 
				require_once "includes/header.php";
			?>
			<div id="container" >
				<h1 class="cont_head"><?php if(!empty($intBenID)) echo "Edit"; else echo "Add";?> Beneficiary Details</h1>
				<div id="content" class="left">
					<form id="addBenForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onSubmit="return checkAgree();">
					<input name="sendBenValue" id = "sendBenValue" type="hidden" />
						<h2>Add Beneficiary Information</h2>
						<?php 
							if(!empty($strError) || !empty($strMsg)){
								if(!empty($strError)){
									$strText = $strError;
									$strClass = "error";
								}else{
									$strClass = "success";
									$strText = $strMsg;
								}
						?>
						<table class="table clear-top">
							<tr class="<?php echo $strClass; ?>">
								<td class="<?php echo $strClass.' align_center'; ?>"><?php echo $strText; ?></td>
							</tr>
						</table>
						<?php 
							}
						?>
						<div class="inner_highlight">
							<table class="table">
								<tr>
									<td>
										<label>Beneficiary Country<em>*</em> : </label>
									</td>
									<td>
                                    <span style="position:relative"><div style="position:absolute; top:-50px; left:4px; width:200px; font-size:13px; color:#666666; font-weight:bold;display:inline-block;text-align:center;	background-color:#E6A028;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;margin-right:5px;" id="countrymsgnote" class="input-medium"></div></span>
										<select name="benCountry" id="benCountry" class="input-xlarge">
											<option value="">- Select country for beneficiary -</option>
												<?php 
													$strQueryCountries = "select countries.countryId, countries.countryName from countries right join ben_country_rule_bank_details on countries.countryId=ben_country_rule_bank_details.benCountry where ben_country_rule_bank_details.status = 'Enable' GROUP BY countries.countryName ORDER BY countries.countryName ASC";
												
												
													$arrCountry = selectMultiRecords($strQueryCountries);
													//debug($strQueryCountries);
													
													
													for($x = 0;$x < count($arrCountry);$x++){
														echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']."</option>";
													}
												?>
											<?php 
												/*
												 * $strQueryCountries = "SELECT DISTINCT countryName AS countryName, countryCode, countryId FROM ".TBL_COUNTRIES." WHERE countryCode != '' GROUP BY countryName ORDER BY countryName ASC";
												$arrCountry = selectMultiRecords($strQueryCountries);
												for($x = 0;$x < count($arrCountry);$x++){
													echo "<option value='".$arrCountry[$x]['countryId']."'>".$arrCountry[$x]['countryName']." - ".$arrCountry[$x]['countryCode']."</option>";
												}
												*/
											?>
										</select>
									</td>
									<td>
										<a href="#" id="countryTip" class="tip" data-placement="above" rel="popover" data-content="To change beneficiary country, please click the drop down option." title="Beneficiary Country">
											[?]
										</a>
									</td>
								</tr>
								<tr id="currency">
										<td>
											<label>Currency You Wish To Send:</label>
										</td>
										<td>
										<select name="sendingCurrency" id="sendingCurrency" type="text" class="input-medium">
											<option value="">- Select Currency -</option>
											<?php 
												//$strQueryCurrency = "SELECT currencyName, baseCurrency FROM ".TBL_CURRENCY." INNER JOIN ".TBL_ONLINE_EXCHANGE_RATES." ON cID = quotedCurrency GROUP BY currencyName ORDER BY currencyName ASC";
												//$strQueryCurrency = "SELECT DISTINCT quotedCurrency FROM ".TBL_FX_RATES_MARGIN." ORDER BY quotedCurrency ASC";
												$strQueryCurrency = "SELECT currencies, COUNT(*) AS c FROM
             (SELECT quotedCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN." UNION ALL
              SELECT baseCurrency AS currencies FROM ".TBL_FX_RATES_MARGIN.")
       AS tg GROUP BY currencies;";			
//debug($strQueryCurrency);
												$arrCurrency = selectMultiRecords($strQueryCurrency);
												
												for($x = 0;$x < count($arrCurrency);$x++)
													echo "<option value='".$arrCurrency[$x]['currencies']."'>".$arrCurrency[$x]['currencies']."</option>";
											?>
										</select>
									</td>
										<td>
											<a href="#" class="tip" id="currencyTip" data-placement="above" rel="popover" data-content="Select Currency You Want To Send" title="Select Currency">
												[?]
											</a>
										</td>
									</tr>
								<tr>
										<td>
											<label>Name of Beneficiary<em>*</em> : </label>
										</td>
										<td>
											<input name="benName" id="benName" type="text" class="input-medium" />
										</td>
										<td>
											<a href="#" class="tip" id="benNameTip" data-placement="above" rel="popover" data-content="Enter the beneficiary complete name" title="Beneficiary Name">
												[?]
											</a>
										</td>
									</tr>
									
									<!--<tr id="accountNameCont">
										<td>
											<label>Account Name<em>*</em> :</label>
										</td>
										<td>
											<input name="accountName" id="accountName" type="text" class="input-medium" maxlength="40" />
										</td>
										<td>
											<a href="#" class="tip" id="accountNameTip" data-placement="above" rel="popover" data-content="Enter beneficiary account name" title="Beneficiary Account Name">
												[?]
											</a>
										</td>
									</tr>-->
									<tr id="accountNumberCont">
										<td>
											<label>Account Number<em>*</em> :</label>
										</td>
										<td>
											<input onblur="changeStausValidation();" name="accountNumber" id="accountNumber" type="text" class="input-medium" maxlength="40" />
										</td>
										<td>
											<a href="#" class="tip" id="accountNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary account number" title="Beneficiary Account Number">
												[?]
											</a>
										</td>
									</tr>
									<tr id="branchNameNumberCont">
										<td>
											<label>Branch Name / Number<em>*</em> :</label>
										</td>
										<td>
											<input name="branchNameNumber" id="branchNameNumber" type="text" class="input-medium" maxlength="40"/>
										</td>
										<td>
											<a href="#" class="tip" id="branchNameNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch name or number" title="Beneficiary Bank Branch Name / Number">
												[?]
											</a>
										</td>
									</tr>
									<tr id="branchAddressCont">
										<td>
											<label>Branch Address<em>*</em> :</label>
										</td>
										<td>
											<input name="branchAddress" id="branchAddress" type="text" class="input-medium" maxlength="40"/>
										</td>
										<td>
											<a href="#" class="tip" id="branchAddressTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch address" title="Beneficiary Bank Branch Address">
												[?]
											</a>
										</td>
									</tr>
									<tr id="routingNumberCont">
										<td>
											<label>Routing Number<em>*</em> :</label>
										</td>
										<td>
											<input name="routingNumber" id="routingNumber" type="text" class="input-medium" maxlength="40"/>
										</td>
										<td>
											<a href="#" class="tip" id="routingNumberTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank branch routing number" title="Beneficiary Bank Branch Routing Number">
												[?]
											</a>
										</td>
									</tr>
									<tr id="sortCodeCont">
										<td>
											<label>Sort Code<em>*</em> :</label>
										</td>
										<td>
											<input onblur="changeStausValidation();" name="sortCode" id="sortCode" type="text" class="input-medium" maxlength="40"/>
										</td>
										<td>
											<a href="#" class="tip" id="sortCodeTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank sort code" title="Beneficiary Bank Sort Code">
												[?]
											</a>
										</td>
									</tr>
									<tr id="ibanCont">
										<td>
											<label>IBAN Number<em>*</em> :</label>
										</td>
										<td>
											<input name="iban" id="iban" type="text" class="input-medium" maxlength="40" onBlur = "changeStausValidation();checkAgree();"/>
                                            <input name="isiban" id = "isiban" type="hidden" />
											
										</td>
										<td>
											<a href="#" class="tip" id="ibanTip" data-placement="above" rel="popover" data-content="Please enter your full IBAN number" title="Beneficiary IBAN">
												[?]
											</a>
										</td>
									</tr>
									<tr id="swiftCont">
										<td>
											<label>SWIFT Code<em>*</em> :</label>
										</td>
										<td>
											<input name="swift" id="swift" type="text" class="input-medium" maxlength="16"/>
										</td>
										<td>
											<a href="#" class="tip" id="swiftTip" data-placement="above" rel="popover" data-content="Enter beneficiary bank swift code" title="Swift Code">
												[?]
											</a>
										</td>
									</tr>
									<tr>
										<td>
											<label>Your Reference :</label>
										</td>
										<td>
											<input name="reference" id="reference" type="text" class="input-medium"/>
										</td>
										<td>
											<a href="#" class="tip" id="refTip" data-placement="above" rel="popover" data-content="Please enter some character used for the reference of the beneficiary" title="Reference for beneficiary">
												[?]
											</a>
										</td>
									</tr>
								</table>
							<!--<div class="row align_right">
								<input name="agreement" id="agreement" type="checkbox" class="bottom"/>
								I confirm the above beneficiary details are correct. I understand that if a payment is returned there will be a &pound;30 fee.
							</div>-->
						</div>
						<p><em>*</em> Mandatory Fields</p>
						<div class="row align_center">
							<input name="clear" id="clear" type="reset" value="Clear Details" class="btn simple-btn input-small"/>
							<?php 
								if(empty($intBenID)){
							?>
							<input name="addBen" id="addBen" type="submit" value="Add" class="btn actv_btn input-small"/>
							<?php 
								}else{
							?>
							<input name="updateBen" id="updateBen" type="submit" value="Update" class="btn actv_btn input-small"/>
							<input name="benID" type="hidden" value="<?php echo $intBenID; ?>" />
							<input name="sendCurrencyVal" type="hidden" value="" />
							<?php 
								}
							?>
						</div>
					</form>
				</div>
			</div>
			<?php 
			
				require_once "includes/footer.php";
			?>
			
		</div>
		
		<script>
		<?php
			
		/*$strCountries = "{";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "'".$benCountriesResult[$ii]["countryName"]."':{'c':'".$benCountriesResult[$ii]["countryName"]."','i':'".$benCountriesResult[$ii]["isocode"]."','g':'".$benCountriesResult[$ii]["ibanlength"]."'}";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= "}";*/
		$strCountries = "Array(";
		for($ii=0;$ii<count($benCountriesResult);$ii++)
		{
			$arrCount[] = "Array('".strtoupper($benCountriesResult[$ii]["countryName"])."','".strtoupper($benCountriesResult[$ii]["isocode"])."','".$benCountriesResult[$ii]["ibanlength"]."','".$benCountriesResult[$ii]["countryId"]."')";
			//$strCountries .= 
		}
		$strCountries .= implode(",",$arrCount);
		$strCountries .= ")";
		
		echo 'var arrCountrues ='.$strCountries.';'; 
		?>
		
		
		/*$(document).ready(
			function() {
					$('#iban').mask('aa99 **** 9999 9999 9999 9999 9999');

					
			});*/
		
		
		function isValidIBAN($v){ //This function check if the checksum if correct
			
			$v = $v.replace(/^(.{4})(.*)$/,"$2$1"); //Move the first 4 chars from left to the right
			$v = $v.replace(/[A-Z]/g,function($e){return $e.charCodeAt(0) - 'A'.charCodeAt(0) + 10}); //Convert A-Z to 10-25
			var $sum = 0;
			var $ei = 1; //First exponent 
			for(var $i = $v.length - 1; $i >= 0; $i--){
				$sum += $ei * parseInt($v.charAt($i),10); //multiply the digit by it's exponent 
				$ei = ($ei * 10) % 97; //compute next base 10 exponent  in modulus 97
			}; 
			return $sum % 97 == 1;
		}
		
		var $patterns = { //Map automatic generated by IBAN-Patterns Online Tool
				IBAN: function($v){
					//alert($v);
					//alert($v.length);
					$v = $v.replace(/[ ]/g,''); 
					return /^AL\d{10}[0-9A-Z]{16}$|^AD\d{10}[0-9A-Z]{12}$|^AT\d{18}$|^BH\d{2}[A-Z]{4}[0-9A-Z]{14}$|^BE\d{14}$|^BA\d{18}$|^BG\d{2}[A-Z]{4}\d{6}[0-9A-Z]{8}$|^HR\d{19}$|^CY\d{10}[0-9A-Z]{16}$|^CZ\d{22}$|^DK\d{16}$|^FO\d{16}$|^GL\d{16}$|^DO\d{2}[0-9A-Z]{4}\d{20}$|^EE\d{18}$|^FI\d{16}$|^FR\d{12}[0-9A-Z]{11}\d{2}$|^GE\d{2}[A-Z]{2}\d{16}$|^DE\d{20}$|^GI\d{2}[A-Z]{4}[0-9A-Z]{15}$|^GR\d{9}[0-9A-Z]{16}$|^HU\d{26}$|^IS\d{24}$|^IE\d{2}[A-Z]{4}\d{14}$|^IL\d{21}$|^IT\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^[A-Z]{2}\d{5}[0-9A-Z]{13}$|^KW\d{2}[A-Z]{4}22!$|^LV\d{2}[A-Z]{4}[0-9A-Z]{13}$|^LB\d{6}[0-9A-Z]{20}$|^LI\d{7}[0-9A-Z]{12}$|^LT\d{18}$|^LU\d{5}[0-9A-Z]{13}$|^MK\d{5}[0-9A-Z]{10}\d{2}$|^MT\d{2}[A-Z]{4}\d{5}[0-9A-Z]{18}$|^MR13\d{23}$|^MU\d{2}[A-Z]{4}\d{19}[A-Z]{3}$|^MC\d{12}[0-9A-Z]{11}\d{2}$|^ME\d{20}$|^NL\d{2}[A-Z]{4}\d{10}$|^NO\d{13}$|^PL\d{10}[0-9A-Z]{,16}n$|^PT\d{23}$|^RO\d{2}[A-Z]{4}[0-9A-Z]{16}$|^SM\d{2}[A-Z]\d{10}[0-9A-Z]{12}$|^SA\d{4}[0-9A-Z]{18}$|^RS\d{20}$|^SK\d{22}$|^SI\d{17}$|^ES\d{22}$|^SE\d{22}$|^CH\d{7}[0-9A-Z]{12}$|^TN59\d{20}$|^TR\d{7}[0-9A-Z]{17}$|^AE\d{21}$|^GB\d{2}[A-Z]{4}\d{14}$/.test($v) && isValidIBAN($v);		
				},
		};
		//alert($patterns.IBAN('CY17 0020 0128 0000 0012 0052 7600'));
		
		
		
		
		
			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
			$('#countryTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#currencyTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#benNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNameTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#accountNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#branchNameNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#branchAddressTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#routingNumberTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#sortCodeTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#ibanTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#swiftTip').popover({
				trigger: 'hover',
				offset: 5
			});
			$('#refTip').popover({
				trigger: 'hover',
				offset: 5
			});
			<?php
				if($strError){
					
					//debug($arrBenInfo);
			?>
				SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountryId; ?>');
				//$('#benCountry').val('<?php echo $arrBenInfo['Country']; ?>');
				$('#benName').val('<?php echo $strAccountName; ?>');
				$('#accountName').val('<?php echo $strAccountName; ?>');
				$('#routingNumber').val('<?php echo $strRoutingNumber; ?>');
				
				//$('#benCurrency').val('<?php echo $arrBenInfo['currency']; ?>');
				$('#iban').focus();
				//$('#swift').val('<?php echo $strSwiftCode; ?>');
				$('#sortCode').val('<?php echo $strSortCode; ?>');
				$('#accountNumber').val('<?php echo $strAccountNumber ?>');
				$('#reference').val('<?php echo $strReference; ?>');
			<?php 
				}
				
			
				if(!empty($intBenID)){
					
					//debug($arrBenInfo);
			?>
				SelectOption(document.forms[0].benCountry, '<?php echo $strBenCountryId; ?>');
				$('#benName').val('<?php echo $strAccountName; ?>');
				$('#accountName').val('<?php echo $strAccountName; ?>');
				$('#routingNumber').val('<?php echo $strRoutingNumber; ?>');
				//$('#benCurrency').val('<?php echo $arrBenInfo['currency']; ?>');
				$('#iban').val('<?php echo $strIBAN; ?>');
				$('#swift').val('<?php echo $strSwiftCode; ?>');
				$('#sortCode').val('<?php echo $strSortCode; ?>');
				$('#accountNumber').val('<?php echo $strAccountNumber ?>');
				$('#reference').val('<?php echo $strReference; ?>');
			<?php 
				}
			?>
			
			var changeStausFlag='YES';
	function changeStausValidation()
		{
		   
		//var countryVal2=$("#sendBenValue").val();
		var countryVal2= $("#benCountry option:selected").text();
		
		
		//alert(countryVal2);
		
		/**1=Uk*/
		
		if((countryVal2=='United Kingdom'))
		{
			
		$('#countrymsgnote').html("<b>Enter an account no. or IBAN</b>");
		if($('#accountNumber').val() !='')
		{
		changeStausFlag='no';
		$('#isiban').val('NO');
		$("#iban").rules("remove");
		//$("#accountNumber").rules("add");
		//$("#sortCode").rules("add");
		}else if($('#iban').val() !='')
		{
		//$("#iban").rules("add");
		$("#accountNumber").rules("remove");
		$("#sortCode").rules("remove");
		
		changeStausFlag='YES';
		$('#isiban').val('YES');
		
		}
		
		}else if(countryVal2=='United States')
		{
		$('#countrymsgnote').html("");	
		changeStausFlag='YES';
		if($('#accountNumber').val() !='')
		{
		$("#swift").rules("remove");
		$("#routingNumber").rules("remove");
		}
		}else {
		$('#countrymsgnote').html("");
		changeStausFlag='YES';
		//$("#routingNumber").rules("add");
		//$("#swift").rules("add");
		}
		
		}
		
			function checkAgree(){
					var iban = $('#iban').val();
					var country=$('#sendBenValue').val();
					var checkLength=checkIBANLength(country,iban);
					if(checkLength==false && isiban == "YES"){
						$('#iban').focus();
						alert("Please enter the correct IBAN number for your beneficiary country");
						$('#iban').focus();
						return false;
					}
					//alert(checkLength);
					var isiban = $('#isiban').val();
					
					if(!$patterns.IBAN(iban) && isiban == "YES" && changeStausFlag =="YES")
					{
						$('#iban').focus();
						alert("Please enter the correct IBAN number for your beneficiary country");
						$('#iban').focus();
						return false;
						
						//return false;
						
					}
					else
					{
						/*if($("#sendBenValue").val().toUpperCase() == 'AUSTRALIA')
						{
							if(iban.substr(0,2) != 'AU')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val() == 'BELGIUM')
						{
							if(iban.substr(0,2).toUpperCase() != 'BE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'FINLAND')
						{
							if(iban.substr(0,2) != 'FI')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'FRANCE')
						{
							if(iban.substr(0,2) != 'FR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'GERMANY')
						{
							if(iban.substr(0,2) != 'DE')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'GREECE')
						{
							if(iban.substr(0,2) != 'GR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'IRELAND')
						{
							if(iban.substr(0,2) != 'IR')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'ITALY')
						{
							if(iban.substr(0,2) != 'IT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'NETHERLANDS')
						{
							if(iban.substr(0,2) != 'NL')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'PORTUGAL')
						{
							if(iban.substr(0,2) != 'PT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'SPAIN')
						{
							if(iban.substr(0,2) != 'ES')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'ANDORRA')
						{
							if(iban.substr(0,2) != 'AD')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'SLOVAKIA')
						{
							if(iban.substr(0,2) != 'SK')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'FRENCH GUIANA')
						{
							if(iban.substr(0,2) != 'GF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'FRENCH SOUTHERN TERRITORIES')
						{
							if(iban.substr(0,2) != 'TF')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'GUADELOUPE')
						{
							if(iban.substr(0,2) != 'GP')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'MARTINIQUE')
						{
							if(iban.substr(0,2) != 'MQ')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else if($("#sendBenValue").val().toUpperCase() == 'MAYOTTE')
						{
							if(iban.substr(0,2) != 'YT')
							{
								alert("Please enter the correct IBAN number for your beneficiary country");
								return false;
							}
						}
						else
						{
							return true;
						}*/
						return true;
					}
				
				/*if(!($('#agreement').is(':checked'))){
					alert('Please click the confirmation of your details correction');
					return false;
				}
				else
					return true;*/
					
			}
			
			function onChangeCurrency(country){
					//alert(country);
				$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_currency.php"?>",
					type:"POST",
					data: {
						country: country,
						
					},
					cache:false,
					dataType: 'json',
					success: function(data){
						//alert(data['currency']);
						$('#sendingCurrency').val(data['currency']);
						changeStausValidation();
					}
			});
		}
		function checkIBANLength(country,iban){
			//alert(arrCountrues.length);
			
			for (var m=0;m<arrCountrues.length;m++) {
				if(country==arrCountrues[m][3]){
				if(iban.length !=arrCountrues[m][2]){
					
					return false;
					break;
				
				}
				if(iban.substr(0,2) !=arrCountrues[m][1]){
					
					return false;
					break;
				
				}
				}
			
			}
			return true;
		
		}
		
		function getCountry(currency){
				$.ajax({
					url:"http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/get_country.php"?>",
					type:"POST",
					data: {
						currency: currency,
						
					},
					cache:false,
					dataType: 'json',
					success: function(dataFun){
						//$('#benCountry').val(dataFun['c']);
						$('#sendBenValue').val(dataFun['c']);
						getBenRule(dataFun['c']);
					
					}
			});
		}
			
			
			function getBenRule(country){
			
				$.ajax({
					url: "http://<?php echo $_SERVER['HTTP_HOST']."/premier_fx_online_module/ben_country_base_rule.php"; ?>",
					type: "POST",
					data: {
						country: country
					},
					cache: false,
					dataType: 'json',
					success: function(data){
						if(!data){
							errorTxt = "<p style='font-weight:bold;text-align:justify;'>We are sorry for the inconvenience, we are not allowing online deals in this country. Please call support on the following numbers and place your order by phone.</p><br /><div style='font-weight:bold;'>United Kingdom : +44 (0)845 021 2370 Portugal: +351 289 358 511 Spain:+34 971 576 724</div>";
							//$('#overlay').fadeIn('slow');
							$('#errorRule').fadeIn('slow');
							$('#errorRule').html(errorTxt);
						}else{
							//alert(country);
							$('#errorRule').hide();
							accountName = data.accountName;
							accNo = data.accNo;
							branchNameNumber = data.branchNameNumber;
							branchAddress = data.branchAddress;
							swiftCode = data.swiftCode;
							IBAN = data.iban;
							routingNumber = data.routingNumber;
							sortCode = data.sortCode;
							//$('#sendingCurrency').val("2");
							if(accountName == 'N'){
								$('#accountName').attr('disabled', 'disabled');
								$('#accountNameCont').fadeOut('fast');
								
							}else if(accountName == 'Y'){
								$('#accountName').removeAttr('disabled');
								$('#accountNameCont').fadeIn('fast');
																
							}
							
							if(accNo == 'N'){
								$('#accountNumber').attr('disabled', 'disabled');
								$('#accountNumberCont').fadeOut('fast');
							}else if(accNo == 'Y'){
								$('#accountNumber').removeAttr('disabled');
								$('#accountNumberCont').fadeIn('fast');
							}
							
							if(branchNameNumber == 'N'){
								$('#branchNameNumber').attr('disabled', 'disabled');
								$('#branchNameNumberCont').fadeOut('fast');
							}else if(branchNameNumber == 'Y'){
								$('#branchNameNumber').removeAttr('disabled');
								$('#branchNameNumberCont').fadeIn('fast');
							}
							
							if(branchAddress == 'N'){
								$('#branchAddress').attr('disabled', 'disabled');
								$('#branchAddressCont').fadeOut('fast');
							}else if(branchAddress == 'Y'){
								$('#branchAddress').removeAttr('disabled');
								$('#branchAddressCont').fadeIn('fast');
							}
							
							if(swiftCode == 'N'){
								$('#swift').attr('disabled', 'disabled');
								$('#swiftCont').fadeOut('fast');
							}else if(swiftCode == 'Y'){
								$('#swift').removeAttr('disabled');
								$('#swiftCont').fadeIn('fast');
							}
							
							if(IBAN == 'N'){
								
								$('#iban').attr('disabled', 'disabled');
								$('#ibanCont').fadeOut('fast');
								$('#isiban').val('NO');
							}else if(IBAN == 'Y'){
								
								$('#iban').removeAttr('disabled');
								$('#ibanCont').fadeIn('fast');
								$('#isiban').val('YES');
							}
							
							if(routingNumber == 'N'){
								$('#routingNumber').attr('disabled', 'disabled');
								$('#routingNumberCont').fadeOut('fast');
							}else if(routingNumber == 'Y'){
								$('#routingNumber').removeAttr('disabled');
								$('#routingNumberCont').fadeIn('fast');
							}
							
							if(sortCode == 'N'){
								$('#sortCode').attr('disabled', 'disabled');
								$('#sortCodeCont').fadeOut('fast');
							}else if(sortCode == 'Y'){
								$('#sortCode').removeAttr('disabled');
								$('#sortCodeCont').fadeIn('fast');
							}
							onChangeCurrency(country);
						}
						
					}
				});
				//onChangeCurrency();
			}
			
			if($('#benCountry').val())
				getBenRule($('#benCountry').val());
				
				$('#benCountry').change(
				function(){
					$('#errorRule').hide();
					if($(this).val() != ''){
						$('#iban').val('');
						$('#sendBenValue').val($(this).val());
						getBenRule($(this).val());
					}
				//countryVal=$(this).attr('');
						
					 	 
				}
			);
			$('#sendingCurrency').change(
				function(){
					//alert($(this).val());
					/*if($(this).val() != '')
					
					var benCountryVal = $('#benCountry').val();
						getCountry($(this).val());
						$('#benCountry').val(benCountryVal)
				//countryVal=$(this).attr('');
						*/
						
				}
			);
			
			
			/*$.validator.addMethod("ibanValidate", function(value, element) {
				//alert('test');
				pattern = /^((NO)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(NO)[0-9A-Z]{15}|(BE)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BE)[0-9A-Z]{16}|(DK|FO|FI|GL|NL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(DK|FO|FI|GL|NL)[0-9A-Z]{18}|(MK|SI)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MK|SI)[0-9A-Z]{19}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(BA|EE|KZ|LT|LU|AT)[0-9A-Z]{20}|(HR|LI|LV|CH)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(HR|LI|LV|CH)[0-9A-Z]{21}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(BG|DE|IE|ME|RS|GB)[0-9A-Z]{22}|(GI|IL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(GI|IL)[0-9A-Z]{23}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AD|CZ|SA|RO|SK|ES|SE|TN)[0-9A-Z]{24}|(PT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{1}|(PT)[0-9A-Z]{25}|(IS|TR)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(IS|TR)[0-9A-Z]{26}|(FR|GR|IT|MC|SM)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(FR|GR|IT|MC|SM)[0-9A-Z]{27}|(AL|CY|HU|LB|PL)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}|(AL|CY|HU|LB|PL)[0-9A-Z]{28}|(MU)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{2}|(MU)[0-9A-Z]{30}|(MT)[0-9A-Z]{2}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{4}[ ][0-9A-Z]{3}|(MT)[0-9A-Z]{31})$/;
			  return this.optional(element) || pattern.test(value);
			}, "Please enter correct IBAN number");*/
			
			$('#addBenForm').validate({
				rules: {
					benName: {
						required: true
					},
					accountName: {
						required: true
						
					},
					accountNumber: {
						required: true
						
					},
					branchNameNumber: {
						required: true
						
					},
					branchAddress: {
						required: true
						
					},
					routingNumber: {
						required: true
						
					},
					sortCode: {
						required: true
						
					},
					
					iban: {
						required: true
						//},
						//ibanValidate: true
					},
					swift: {
						required: true
					}
				}
			});
		</script>
		<script type="text/javascript">
		</script>
	</body>
</html>