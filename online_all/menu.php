<?php

use Payex\Helper\DataHelper;
use Payex\Online\Service\CustomerService;
use Payex\Repository\CustomerRepository;

require '../src/bootstrap.php';

$customerService = new CustomerService($online->getDb());
$customerRepository = new CustomerRepository($online->getDb());
$dataHelper = new DataHelper();
$uiHelper = $online->getUiHelper();
$currentUrl = $_SERVER['HTTP_HOST'];
$loggedUserId = $_SESSION['loggedInUser']['userID'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$logoHtml = $agentUiData['logoHtml'];

$customer = $customerRepository->getById($loggedUserId);
if ($customer) {
    $customerServices = $dataHelper->createArrayFromString($customer->getOnlineCustomerServices());
}

$currentRequestUri = $uiHelper->getCurrentPageScriptName($_SERVER['REQUEST_URI']);

?>
<div class="upper_container">
    <div class="header">
        <div class="logo">
            <?= $logoHtml; ?>
        </div>
        <div class="menu">
            <ul class="menu_ul">
                <?php if ($customerServices && $customerService->hasPrepaidService($customerServices)): ?>
                    <li class="menu_li">
                        <a href="topup.php"
                           class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'topup.php') ? ' menu_selected' : ''; ?>"
                           target="_parent">Top Up</a>
                    </li>
                    <li class="menu_li">
                        <a href="balance_inquiry.php"
                           class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'balance_inquiry.php') ? ' menu_selected' : ''; ?>"
                           target="_parent">Balance Inquiry</a>
                    </li>
                <?php endif; ?>
                <li class="menu_li">
                    <a href="make_payment-ii-new.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'make_payment-ii-new.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">Send Money</a>
                </li>
                <li class="menu_li">
                    <a href="transaction_history_new.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'transaction_history_new.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">Transaction History</a>
                </li>
                <li class="menu_li">
                    <a href="view_beneficiariesnew.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'view_beneficiariesnew.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">Beneficiaries</a>
                </li>
                <li class="menu_li">
                    <a href="change_passwordnew.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'change_passwordnew.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">Change Password</a>
                </li>
                <li class="menu_li">
                    <a href="contact_us.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'contact_us.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">Contact Us</a>
                </li>
                <li class="menu_li">
                    <a href="faqs.php"
                       class="menu_link <?php echo $uiHelper->checkIfNeedSelect($currentRequestUri, 'faqs.php') ? ' menu_selected' : ''; ?>"
                       target="_parent">FAQs</a>
                </li>
            </ul>
        </div>
    </div>
</div>