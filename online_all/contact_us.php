<?php
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
    header("LOCATION: logout.php");
}

require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$email = $agentUiData['email'];
$phone = $agentUiData['phone'];
$address = $agentUiData['address'];
$logoHtml = $agentUiData['logoHtml'];


$strUserId = $_SESSION['loggedInUser']['userID'];
$strEmail = $_SESSION['loggedInUser']['email'];
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Contact us - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php'; ?>
    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php $currentPage = 'contact-us'; ?>
        <?php include 'templates/header.php'; ?>
    </header>
    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc"
             data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg"
             data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Contact us</h1>
<!--                        <p class="page-header-desc">Talk to us</p>-->
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Contact us</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->
        <div class="container">


            <div class="logout_area">
                <h2 class="heading">Contact us</h2>
                <!-- content area -->
                <!-- content area -->
                <div class="content_area">
                    <div class="content_area_center">
                        <p class="content_heading font_style">Contact Us</p>
                        <p class="content_subheading font_style">If you have any questions about your account or any
                            aspect of the <?= $title; ?> Online system please get in touch and we will assist you. </p>
                    </div>
                    <!-- content area left-->
                    <div class="content_area_left">
                        <p class="contact_desc font_style" style="font-size:16px;">Talk to us</p>
                        <div class="field_wrapper">
                            <p class="contact_head font_style">Call our Office on</p>
                            <p class="contact_desc font_style"><?= $phone; ?></p>
                        </div>
                        <div class="field_wrapper">
                            <p class="contact_desc font_style">Email Us</p>
                            <p class="contact_head font_style"><?= $email; ?></p>
                        </div>
                    </div>
                    <!-- content area right-->
                    <div class="content_area_right" style="margin:27px;">
                        <p class="contact_desc font_style" style="font-size:16px;">Office addresses</p>
                        <div class="field_wrapper">
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" data-src="holder.js/64x64" alt="55x55"
                                             src="images/uk_flag.png">
                                    </a>
                                </div>
                                <div class="media-body contact_head">
                                    <h4 class="media-heading contact_desc font_style">Office</h4>
                                    <?= $address; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content area ends here-->
            </div>


        </div><!-- End .container -->

        <div class="mb40 mb20-xs"></div><!-- space -->
        <footer id="footer" class="footer-inverse" role="contentinfo">
            <?php include 'templates/footer.php'; ?>
        </footer>
        <!-- End #footer -->

    </div>
    <!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
</body>
</html>
