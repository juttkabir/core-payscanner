<?php
/**
 * @package: Online Module
 * @subpackage: Confirm Page
 * @author: Awais Umer
 */
session_start();
if (!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])) {
	header("LOCATION: logout.php");
}
require_once '../src/bootstrap.php';
$currentUrl = $_SERVER['HTTP_HOST'];
$agentUiData = $online->getLoginHelper()->getAgentUiData($currentUrl, $_SESSION['loggedInUser']['agentID']);
$title = $agentUiData['title'];
$logoHtml = $agentUiData['logoHtml'];

include_once "javascript/audit-functions.php";
include_once "includes/configs.php";
include_once "includes/database_connection.php";

dbConnect();
include_once "includes/functions.php";
$currency = $_SESSION['sendingCurrency'];

//debug($_SESSION);

if ($_POST['benName']) {
	$_SESSION['benName'] = $_POST['benName'];
}

if ($_POST['benCountry']) {
	$_SESSION['benCountry'] = $_POST['benCountry'];
}

if ($_POST['benCurrency']) {
	$_SESSION['benCurrency'] = $_POST['benCurrency'];
}

if ($_POST['accountName']) {
	$_SESSION['accountName'] = $_POST['accountName'];
}

if ($_POST['accountNumber']) {
	$_SESSION['accountNumber'] = $_POST['accountNumber'];
}

if ($_POST['branchNameNumber']) {
	$_SESSION['branchNameNumber'] = $_POST['branchNameNumber'];
}

if ($_POST['branchAddress']) {
	$_SESSION['branchAddress'] = $_POST['branchAddress'];
}

if ($_POST['routingNumber']) {
	$_SESSION['routingNumber'] = $_POST['routingNumber'];
}

if ($_POST['sortCode']) {
	$_SESSION['sortCode'] = $_POST['sortCode'];
}

if ($_POST['swift']) {
	$_SESSION['swift'] = $_POST['swift'];
}

if ($_POST['reference']) {
	$_SESSION['reference'] = $_POST['reference'];
}

if ($_POST['existBen']) {
	$_SESSION['existBen'] = $_POST['existBen'];
}

if ($_POST['iban']) {
	$_SESSION['iban'] = $_POST['iban'];
}

$test = $_POST["test"];
$_SESSION["test"] = $_POST["test"];

$strUserId = $_SESSION['loggedInUser']['accountName'];
$strEmail = $_SESSION['loggedInUser']['email'];

if ($_POST["existBen"] != "") {
	$getPreviousArr = "SELECT * FROM ben_banks WHERE benID = " . $_POST["existBen"] . " ";
	$fetPreviousArr = selectFrom($getPreviousArr);

	$updateBenBank = "UPDATE " . TBL_BEN_BANK_DETAILS . " SET

	accountNo	=	'" . $_POST["accountNumber"] . "',
	sortCode	=	'" . $_POST["sortCode"] . "',
	iban		=	'" . $_POST["iban"] . "'
	WHERE benId = 	" . $_POST["existBen"] . "
	";
	if (update($updateBenBank)) {

		activities($_SESSION["loginHistoryID"], "UPDATED", $_SESSION["loggedInUser"]["accountName"], "ben_banks", "ben_banks updated successfully");

		logChangeSet($fetPreviousArr, $_SESSION["loggedInUser"]["accountName"], "ben_banks", "benId", $_POST["existBen"], $_SESSION["loggedInUser"]["accountName"]);

	}
}
?>

<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en" >

<head>
    <meta charset="utf-8">
    <title>Payment Method - <?= $title; ?> Online Currency Transfers</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include 'templates/script_header.php';?>

    <?php include 'templates/_ga.inc.php';?>
</head>

<body>
<div class="boss-loader-overlay"></div>
<!-- End .boss-loader-overlay -->
<div id="wrapper">
    <header id="header" role="banner">
        <?php include 'templates/header.php'?>
    </header>
    <!-- End #header -->

    <div id="content" class="pb0" role="main" style="padding-bottom:0;">
        <div class="page-header parallax larger2x larger-desc" data-bgattach="<?= $online->getBaseUrl(); ?>assets/images/backgrounds/online_bg.jpg" data-0="background-position:50% 0px;" data-500="background-position:50% -100%">
            <div class="container" data-0="opacity:1;" data-top="opacity:0;">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Payment Method</h1>
                        <p class="page-header-desc">Select the mode of payment</p>
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Payment Method</li>
                        </ol>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .page-header -->

        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-wrapper">
                        <h2 class="title-underblock custom mb30">Payment Method</h2>
                        <p style="font-size:16px;">Please select the method of payment you wish to choose.<br>Bank transfer or secure debit card payment.</p>

                            <!-- ROW -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <form name="continue" id="continue" method="post">
                                        <div class="radio">
                                            <label class="custom-radio-wrapper">
                                                      <span class="custom-radio-container">
                                                          <input type="radio"
                                                                 id="test"
                                                                 name="test"
                                                                 value="bank"
                                                                 onClick="changeModeOfPayment(this);">
                                                          <span class="custom-radio-icon"></span>
                                                      </span>
                                                <span>Bank Transfer </span>
                                            </label>
                                        </div><!-- End .radio -->

<!--                                        <div class="radio">-->
<!--                                            <label class="custom-radio-wrapper">-->
<!--                                                        <span class="custom-radio-container">-->
<!--                                                            <input type="radio"-->
<!--                                                                   id="test1"-->
<!--                                                                   name="test"-->
<!--                                                                   value="card"-->
<!--                                                                   onClick="changeModeOfPayment(this);">-->
<!--                                                            <span class="custom-radio-icon"></span>-->
<!--                                                        </span>-->
<!--                                                <span>Debit Cards</span>-->
<!--                                            </label>-->
<!--                                        </div>--><!-- End .radio -->
                                    </form>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-12">
                                    <form name="confirmTrans" id="confirmTrans" method="post" action="">
                                        <input id="benName" name="benName" type="hidden" value="<? echo $_SESSION['accountName'] ?>"/>
                                        <input id="benCountry" name="benCountry" type="hidden"
                                               value="<? echo $_SESSION['benCountry'] ?>"/>
                                        <input id="benCurrency" name="benCurrency" type="hidden"
                                               value="<? echo $_SESSION['benCurrency'] ?>"/>
                                        <input id="accountName" name="accountName" type="hidden"
                                               value="<? echo $_SESSION['accountName'] ?>"/>
                                        <input id="accountNumber" name="accountNumber" type="hidden"
                                               value="<? echo $_SESSION['accountNumber'] ?>"/>
                                        <input id="branchNameNumber" name="branchNameNumber" type="hidden"
                                               value="<? echo $_SESSION['branchNameNumber'] ?>"/>
                                        <input id="branchAddress" name="branchAddress" type="hidden"
                                               value="<? echo $_SESSION['branchAddress'] ?>"/>
                                        <input id="routingNumber" name="routingNumber" type="hidden"
                                               value="<? echo $_SESSION['routingNumber'] ?>"/>
                                        <input id="sortCode" name="sortCode" type="hidden" value="<? echo $_SESSION['sortCode'] ?>"/>
                                        <input id="swift" name="swift" type="hidden" value="<? echo $_SESSION['swift'] ?>"/>
                                        <input id="reference" name="reference" type="hidden" value="<? echo $_SESSION['reference'] ?>"/>
                                        <input id="existBen" name="existBen" type="hidden" value="<? echo $_SESSION['existBen'] ?>"/>
                                        <input id="iban" name="iban" type="hidden" value="<? echo $_SESSION['iban'] ?>"/>
                                        <input type="hidden" name="transType" value="Bank Transfer"/>
                                        <!--<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? //echo $_SESSION['sendingCurrency'] ?>" /> -->
                                        <!-- changed against @ 12703 -->
                                        <input id="sendingCurrency" name="sendingCurrency" type="hidden"
                                               value="<? echo $_POST['sendingCurrency'] ?>"/>


                                        <div id="bankConfirm" class="form-group mt40" style="display:none;">
                                            <input name="go_back" id="go_back" type="button" class="btn btn-default" value="Go back">

                                            <input class="btn btn-custom"
                                                   name="bankConfirmb"
                                                   id="bankConfirmb"
                                                   type="submit"
                                                   onClick="formSubmit('payment_detailnew.php')"
                                                   value="Continue">
                                        </div>
                                        <div id="debitCardConfirm" class="form-group mt40" style="display:none;">
                                            <input name="go_back" id="go_back" type="button" class="btn btn-default" value="Go back">

                                            <input class="btn btn-custom"
                                                   name="debitCardConfirmb"
                                                   id="debitCardConfirmb"
                                                   type="submit"
                                                   onClick="formSubmit('debit_card_bank.php')"
                                                   value="Continue">
                                        </div>
                                    </form>
                                </div>
                            </div>

                    </div><!-- End .form-wrapper -->
                </div><!-- End .col-sm-6 -->
                <div class="mb40 visible-xs"></div><!-- space -->
                <div class="col-sm-4">
                    <h2 class="title-underblock custom mb40">With <?= $title; ?></h2>
                    <p>Send money safely and securely with <?= $title; ?> Online You can send any amount from £100 up to £50,000 or currency equivalent.<br>There are NO transfer fees and NO commissions to pay.</p>
                    <div class="mb10"></div><!-- space -->
                    <a href="<?= $online->getBaseUrl(); ?>faqs.php" class="btn btn-dark">Need help?</a>
                </div><!-- End .col-sm-6 -->
            </div><!-- End .row -->


    </div><!-- End .container -->

    <div class="mb40 mb20-xs"></div><!-- space -->
    <footer id="footer" class="footer-inverse" role="contentinfo">
        <?php include 'templates/footer.php'; ?>
    </footer>
    <!-- End #footer -->

</div>
<!-- End #content -->
</div>
<!-- End #wrapper -->

<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->
<script src="<?php echo $online->getBaseUrl(); ?>assets/js/script00.min.js"></script>
<script>
    function changeModeOfPayment(val) {

        var Value = val.value;

        if(Value=="Bank" || Value=="bank")
        {
            document.getElementById('debitCardConfirm').style.display="none";
            document.getElementById('bankConfirm').style.display="block";
        }

        if(Value=="Card" || Value=="card")
        {
            document.getElementById('bankConfirm').style.display="none";
            document.getElementById('debitCardConfirm').style.display="block";

        }

        //alert(val.value);
    }

    function formSubmit(action) {
        var form = document.getElementById('confirmTrans');

        form.action=action;
        form.submit();
    }
</script>
</body>
</html>
