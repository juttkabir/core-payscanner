<?php
	/**
	 * @package: Online Module	
	 * @subpackage: Confirm Page
	 * @author: Awais Umer
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	//debug($_POST['accountNumber']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <script src="scripts/jquery-1.7.2.min.js"></script>
	<script src="scripts/jquery.validate.js"></script>
    <script type="text/javascript">
	 $(document).ready(function() {
   $("#proceed").click(function(){
								window.btn_clicked = true;
								}
								);
   
   $(window).bind("beforeunload",function(event) {
    if(!window.btn_clicked){
        
		if(/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
        if(confirm("You will not get the rate as good as this for your transaction. Are you sure you want to lose it?")) {
            history.go();
        } else {
            window.setTimeout(function() {
                window.stop();
            }, 1);
        }
    } else {
        return "You will not get the rate as good as this for your transaction. Are you sure you want to lose it?";
    }
    }
										  }
	);

								}
								);
    

</script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Confirm Transaction</title>
		<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />
		<link href="css/style.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<div id="wrapper" >
			<?php 
			//debug($_POST);
				require_once "includes/header.php";
				if(!empty($strMsg) || !empty($strError)){
			?>
			<table class="table">
				<?php 
					if(!empty($strMsg)){
				?>
				<tr class="success">
					<td class="align_center"><?php echo $strMsg; ?></td>
				</tr>
				<?php 
					}
					if(!empty($strError)){
				?>
				<tr class="error">
					<td class="align_center"><?php echo $strError; ?></td>
				</tr>
				<?php
					}
				?>
			</table>
			<?php 
				}
			?>
            
			<div id="container" >
				<p  style="font-size:14px; font-weight: bold; margin-bottom:10px; padding-left:69px; color:#E6A028">Please check details of the trade below. Please select confirm to continue</p>
				<div id="content" class="left">
						<fieldset class="inner_highlight short">
						<legend>Transaction Details</legend>
						<br />
                        <table class="table">
								<tr>
									<td width="3%" class="bold" style="text-align:left">
										Currency You Buy: 
									</td>
                                    <td width="10%" class="bold" style="text-align:left">
										<? echo $_SESSION['receivingCurrency'] ?>
									</td>
                                    
                                    </tr>
                                    <tr>
									<td class="bold" >
										Rate: 
									</td>
                                    <td class="bold" >
										<? echo number_format($_SESSION['exchangeRate'],4,".",","); ?>
									</td>
                                    
                                    </tr>
                                    <tr>
									<td class="bold" >
										 Amount You Buy:  
									</td>
                                    <td class="bold" >
										<? echo number_format($_SESSION['receivingAmount'],2,".",","); ?>
									</td>
                                    
                                    </tr>
                                    <tr>
									<td  class="bold" >
										Currency You Sell: 
									</td>
                                    <td  class="bold" >
										<? echo $_SESSION['sendingCurrency'] ?>
									</td>
                                    
                                    </tr>
                                    <tr>
									<td  class="bold" >
										Amount You Sell: 
									</td>
                                    <td  class="bold" >
										<? echo number_format($_SESSION['sendingAmount'],2,".",",") ?>
									</td>
                                    
                                    </tr>
                                    </table>
						
                        </fieldset>
                        <br />
                       <fieldset class="inner_highlight short">
						<legend>Beneficiary Details</legend>
                        <br />
                        
                        
                        <table class="table">
								<!--<tr>
									<td width="3%" class="bold" style="text-align:left">
										Beneficiary Name: 
									</td>
                                    <td width="10%" class="bold" style="text-align:left">
										<? echo $_POST['benName']?>
									</td>
                                    
                                    </tr>-->
                                    <?php if(isset($_POST["accountName"]))
									{
										?>
                                    <tr>
									<td width="3%" class="bold" style="text-align:left">
										Beneficiary Name: 
									</td>
                                    <td width="10%" class="bold" style="text-align:left">
										<? echo $_POST['accountName']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                    <?php if(isset($_POST["accountNumber"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Account Number: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['accountNumber']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                     <?php if(isset($_POST["branchNameNumber"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Branch Name/Number: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['branchNameNumber']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                     <?php if(isset($_POST["branchAddress"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Branch Address: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['branchAddress']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                     <?php if(isset($_POST["routingNumber"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Routing Number: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['routingNumber']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                    
                                     <?php if(isset($_POST["sortCode"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Sort Code: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['sortCode']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                    
                                     <?php if(isset($_POST["iban"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										IBAN Number: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['iban']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                    
                                     <?php if(isset($_POST["swift"]))
									{
										?>
                                    <tr>
									<td class="bold" >
										Swift: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['swift']?>
									</td>
                                    
                                    </tr>
                                    <? } ?>
                                    
                                    
                                   
                                    <tr>
									<td  class="bold" >
										Reference: 
									</td>
                                    <td class="bold" >
										<? echo $_POST['reference']?>
									</td>
                                                                        
                                    </tr>
                                    </table>
                        
                        </fieldset>
                        
						<div class="row align_center">
						<fieldset class="inner_highlight short" style=" padding-bottom:14px;" >
						<legend>Deal Confirmation</legend>
						<p  style="font-size:12px; font-weight: bold; margin-bottom:13px;">I accept that by clicking Confirm Transaction I am entering into a legally binding contract and a cost may be involved to change this.</p>
                        <form name="confirmTrans" id="confirmTrans" method="post" action="">
                        <input id="benName" name="benName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
						<input id="benCountry" name="benCountry" type="hidden" value="<? echo $_POST['benCountry'] ?>" />
                        <input id="benCurrency" name="benCurrency" type="hidden" value="<? echo $_POST['benCurrency'] ?>" />
						<input id="accountName" name="accountName" type="hidden" value="<? echo $_POST['accountName'] ?>" />
                        <input id="accountNumber" name="accountNumber" type="hidden" value="<? echo $_POST['accountNumber'] ?>" />
						<input id="branchNameNumber" name="branchNameNumber" type="hidden" value="<? echo $_POST['branchNameNumber'] ?>" />
                        <input id="branchAddress" name="branchAddress" type="hidden" value="<? echo $_POST['branchAddress'] ?>" />
						<input id="routingNumber" name="routingNumber" type="hidden" value="<? echo $_POST['routingNumber'] ?>" />
                        <input id="sortCode" name="sortCode" type="hidden" value="<? echo $_POST['sortCode'] ?>" />
                        <input id="swift" name="swift" type="hidden" value="<? echo $_POST['swift'] ?>" />
                        <input id="reference" name="reference" type="hidden" value="<? echo $_POST['reference'] ?>" />
                        <input id="existBen" name="existBen" type="hidden" value="<? echo $_POST['existBen'] ?>" />
                        <input id="iban" name="iban" type="hidden" value="<? echo $_POST['iban'] ?>" />
                        <input type="hidden" name="transType" value="Bank Transfer" />
	<input id="sendingCurrency" name="sendingCurrency" type="hidden" value="<? echo $_POST['sendingCurrency'] ?>" />
                       
                        
                        
							<input id="back" class="btn simple-btn" type="button" onclick="this.form.action='select_beneficiary_trans.php';this.form.submit();" value="Back" name="back"/>
                            <input name="proceed" id="proceed" type="submit" onclick="this.form.action='payment_details.php';this.form.submit();" value="Confirm Transaction" class="btn actv_btn"/>
                          </fieldset>  
						</div>
				</div>
				<!--
				<div id="sidebar">
					<div class="step_head">
						<h2>STEP 1 TRANSACTION DETAILS</h2>
					</div>
					<p class="inner_step">Buying <?php echo number_format($strReceivingAmount, 2, ".", ",")." ".$strReceivingCurrency; ?></p>
					<p class="inner_step">&#64; <?php echo $intExchangeRate; ?></p>
					<p class="inner_step">Selling <?php echo number_format($strSendingAmount, 2, ".", ",")." ".$strSendingCurrency; ?></p>
					<div class="step_head">
						<h2>STEP 2 BENEFICIARY DETAILS</h2>
					</div>
                    
                    
                    <p class="inner_step"><? echo $_SESSION["benBame"]; ?></p>
                    <p class="inner_step"><? echo $_SESSION["firstBen"]; ?></p>
                    <p class="inner_step"><? echo $_SESSION["secondBen"]; ?></p>
                    
					<div class="step_head">
						<h2>STEP 3 PAYMENT DETAILS</h2>
					</div>
					<p class="inner_step">Bank Transfer</p>
					<p class="inner_step">Premier FX Ltd.</p>
					<p class="inner_step">Account Number</p>
					<p class="inner_step">Swift Code</p>
				</div>-->
			</div>
			
		</div>
	</body>
</html>