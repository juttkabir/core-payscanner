<?
	if(isset($_REQUEST["_startSession"]))
	{
		$_startSession = $_REQUEST["_startSession"];
	} else {
		$_startSession = false;
	}

	include("../include/config.php");

	if(isset($_SESSION))
	{
        	include("../admin/security.php");
	} else {
		dbConnect();
	}

        include("../admin/lib/phpmailer/class.phpmailer.php");
	
	$outputFileDateTime = date("YmdHis", time());
	$logFileDateTime = "[".date("Y-m-d H:i:s", time())."] ";
	
	$sql = "SELECT
				*
			FROM
				distributor_email
			";
	$result = mysql_query($sql);
	$numrows = mysql_num_rows($result);
	
	if($numrows > 0)
	{
		while($rs = mysql_fetch_array($result))
		{
			$distributor	= $rs["distributorId"];
			$emailTo		= $rs["emailTo"];
			$emailCC		= $rs["emailCC"];
			$emailBCC		= $rs["emailBCC"];
		
			$sql2 = "SELECT
						t.transID, t.refNumberIM, t.transDate, t.customerID, t.IMFee, t.AgentComm, t.benID, t.totalAmount, t.exchangeRate, t.localAmount, t.collectionPointID, t.tip,
						concat(c.firstName, ' ', c.middleName, ' ', c.lastName) as senderName, concat(c.Address, ', ', c.City, ' - ', c.Zip, ', ', c.state, ', ', c.Country) as senderAddress, IF(c.Phone <> NULL, c.Phone, c.Mobile) as senderContact,
						concat(b.firstName, ' ', b.middleName, ' ', b.lastName) as beneficiaryName, b.IDNumber, b.IDType, IF(b.Phone <> '', b.Phone, b.Mobile) as beneficiaryContact,
						concat(p.cp_corresspondent_name, ', ', p.cp_city, ', ', p.cp_country) as collectionPointName, p.cp_branch_no
					FROM
						transactions t,
						customer c,
						beneficiary b,
						cm_collection_point p
					WHERE
						t.customerID = c.customerID
						AND t.benID = b.benID
						AND t.collectionPointID = p.cp_id
						AND t.customerID = b.customerID
						AND t.is_exported <> 'Y'
						AND t.transStatus IN ('Authorize', 'Amended')
						AND t.benAgentID = ".$distributor."
					";
			$result2 = mysql_query($sql2);
			$numrows2 = mysql_num_rows($result2);
			$output = "";
			$transactionsList = "";
			$i = 1;
			$outputFileName = $distributor."_TransactionSummary_".$outputFileDateTime.".xls";
			
			if($numrows2 > 0)
			{
				$output .= "<table border='1' width='100%' cellspacing='0' cellpadding='5'>";
				$output .= "<tr>";
				$output .= "<td>ITEM</td>";
				$output .= "<td>CODE</td>";
				$output .= "<td>DATE</td>";
				$output .= "<td>SENDER</td>";
				$output .= "<td>SENDER ADDRESS</td>";
				$output .= "<td>SENDER CONTACT NUMBER</td>";
				$output .= "<td>TOTAL COMMISION</td>";
				$output .= "<td>AGENT COMM SHARE</td>";
				$output .= "<td>BENEFICIARIES</td>";
				$output .= "<td>PASSWORD / BENEFIARY ID</td>";
				$output .= "<td>BENEFICIARY CONTACT</td>";
				$output .= "<td>SOURCE AMT(FCY)</td>";
				$output .= "<td>RATE</td>";
				$output .= "<td>CEDI AMT - GHC</td>";
				$output .= "<td>PAYMENT LOCATION</td>";
				$output .= "<td>LOCATION ID NUMBER</td>";
				$output .= "</tr>";
				
				while($rs2 = mysql_fetch_array($result2))
				{
					$transID				= $rs2["transID"];
					$refNumberIM			= $rs2["refNumberIM"];
					$transDate				= $rs2["transDate"];
					$customerID				= $rs2["customerID"];
					$IMFee					= $rs2["IMFee"];
					$AgentComm				= $rs2["AgentComm"];
					$benID					= $rs2["benID"];
					$totalAmount			= $rs2["totalAmount"];
					$exchangeRate			= $rs2["exchangeRate"];
					$localAmount			= $rs2["localAmount"];
					$collectionPointID		= $rs2["collectionPointID"];
					$tip					= $rs2["tip"];
					$senderName				= $rs2["senderName"];
					$senderAddress			= $rs2["senderAddress"];
					$senderContact			= $rs2["senderContact"];
					$beneficiaryName		= $rs2["beneficiaryName"];
					$IDNumber				= $rs2["IDNumber"];
					$IDType					= $rs2["IDType"];
					$beneficiaryContact		= $rs2["beneficiaryContact"];
					$collectionPointName	= $rs2["collectionPointName"];
					$cp_branch_no			= $rs2["cp_branch_no"];
					$transactionsList		.= $transID.",";
					
					$output .= "<tr>";
					$output .= "<td>".$i."</td>";
					$output .= "<td>".$refNumberIM."</td>";
					$output .= "<td>".$transDate."</td>";
					$output .= "<td>".$senderName."</td>";
					$output .= "<td>".$senderAddress."</td>";
					$output .= "<td>".$senderContact."</td>";
					$output .= "<td>".$IMFee."</td>";
					$output .= "<td>".$AgentComm."</td>";
					$output .= "<td>".$beneficiaryName."</td>";
					$output .= "<td>".$tip."</td>";
					$output .= "<td>".$beneficiaryContact."</td>";
					$output .= "<td>".$totalAmount."</td>";
					$output .= "<td>".$exchangeRate."</td>";
					$output .= "<td>".$localAmount."</td>";
					$output .= "<td>".$collectionPointName."</td>";
					$output .= "<td>".$cp_branch_no."</td>";
					$output .= "</tr>";
					
					$i++;
				}
				
				$output .= "</table>";
				$transactionsList = substr($transactionsList, 0, strlen($transactionsList) - 1);
				
				$fp = fopen($outputFileName, "w");
				fwrite($fp, $output);
				fclose($fp);
				
				$mail = new PHPMailer();
				
				$mail->From     = SUPPORT_EMAIL;
				$mail->FromName = SYSTEM;
				$mail->AddReplyTo(SUPPORT_EMAIL, SYSTEM);
				$mail->AddAddress($emailTo);
				
				$tmpCC = explode(",", $emailCC);
				
				for($j=0; $j<sizeof($tmpCC); $j++)
				{
					$mail->AddCC(trim($tmpCC[$j]));
				}
				
				$tmpBCC = explode(",", $emailBCC);
				
				for($j=0; $j<sizeof($tmpBCC); $j++)
				{
					$mail->AddBCC(trim($tmpBCC[$j]));
				}
				
				
				$mail->Subject = "Exported Transactions from Payex";
				$mail->Body    = "-- Auto Generated - Do Not Reply --\r\nSee attachement.";
				$mail->AddAttachment($outputFileName, $outputFileName);
				  
				if($mail->Send())
				{  
				   $sql3 = "UPDATE
				   				transactions
							SET
								is_exported = 'Y'
							WHERE
								transID IN (".$transactionsList.")
				   			";
					$result3 = mysql_query($sql3);
					
					unlink($outputFileName);
					
					echo $logFileDateTime."Email sent for $distributor.<br />\n";
				} else {
					echo $logFileDateTime."Error sending email for $distributor.<br />\n";
				}
				
				unset($mail);
			} else {
				echo $logFileDateTime."Nothing to send for $distributor.<br />\n";
			}
		}
	} else {
		echo $logFileDateTime."No recipient found.";
	}
?>
