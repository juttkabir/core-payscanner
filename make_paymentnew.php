<?php 
	/**
	 * @package: Online Module	
	 * @subpackage: Send Transaction
	
	 */
	session_start();
	if(!isset($_SESSION['loggedInUser']['userID']) && empty($_SESSION['loggedInUser']['userID'])){
		header("LOCATION: logout.php");
	}
	//ini_set("display_errors","on");
	//error_reporting(E_ALL);
	include_once "includes/configs.php";
	include_once "includes/database_connection.php";
	dbConnect();
	include_once "includes/functions.php";
	$intBackFlag = false;
	
	if(isset($_SESSION['ben']))
	{
	$strBenNameCont = selectFrom("select firstName from beneficiary where benID = '".$_SESSION['ben']."' ");
		$strBenName = $strBenNameCont["firstName"];
	}
	//debug($_REQUEST);
	if(!empty($_REQUEST['receivingCurrency']) && !empty($_REQUEST['sendingCurrency']) && (!empty($_REQUEST['receivingAmount']) && $_REQUEST['receivingAmount'] > '0') || ($_REQUEST['sendingAmount'] > '0'&& !empty($_REQUEST['sendingAmount'])))
	{
		//debug($_REQUEST);
		$strBaseCurrency1 = "";
		$strBaseCurrency2 = "";
		$queryBaseCurrencies = "select distinct(baseCurrency) from online_exchange_rates";
		$arrBaseCurrencies = selectMultiRecords($queryBaseCurrencies);
		
		$strSendingCurrency = trim($_REQUEST['sendingCurrency']);
		$strReceivingCurrency = trim($_REQUEST['receivingCurrency']);
		$strReceivingAmount = number_format((trim($_REQUEST['receivingAmount'])), 4, ".", "");
		$strSendingAmount = number_format((trim($_REQUEST['sendingAmount'])), 4, ".", "");
		//debug($strReceivingAmount);
		//debug($strSendingAmount);
		if($strReceivingAmount != 0 && $strSendingAmount != 0)
		{
			$strReceivingAmount = 0;
		}
		if($strReceivingAmount != 0)
		{
			$strBaseAmountCurrency = $strReceivingCurrency;
			$strQuotedAmountCurrency = $strSendingCurrency;
			$fltAmountBase = $strReceivingAmount;
			$fltAmountIsSending = false; 
			
		}
		elseif($strSendingAmount != 0)
		{
			$strBaseAmountCurrency = $strSendingCurrency;
			$strQuotedAmountCurrency = $strReceivingCurrency;
			$fltAmountBase = $strSendingAmount;
			$fltAmountIsSending = true; 
		}
		for($i=0;$i<count($arrBaseCurrencies);$i++)
		{
			if($arrBaseCurrencies[$i]["baseCurrency"] == $strSendingCurrency)
			{
				$strBaseCurrency1 = $strSendingCurrency;
				$strFinalBaseCurrency = $strSendingCurrency;
				$strFinalQuotedCurrency = $strReceivingCurrency;
				$flagBaseIsSending = true;
			}
			elseif($arrBaseCurrencies[$i]["baseCurrency"] == $strReceivingCurrency)
			{
				$strBaseCurrency2 = $strReceivingCurrency;
				$strFinalBaseCurrency = $strReceivingCurrency;
				$strFinalQuotedCurrency = $strSendingCurrency;
				$flagBaseIsSending = false;
			}
		}
		
		//debug($fltAmountIsSending);
		//debug($flagBaseIsSending);
			
			/*if($strBaseAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "RATE";
			
			}
			elseif($strQuotedAmountCurrency == $arrBaseCurrencies[$i]["baseCurrency"])
			{
				$flagRateOrInverse = "INVERSE";
			}*/
		
		
		if($strBaseCurrency1 !="" && $strBaseCurrency2 != "")
		{
			$flagBaseCurrency = "Both";
		
		}
		elseif($strBaseCurrency1 == "" && $strBaseCurrency2 == "")
		{
			$flagBaseCurrency = "None";
		
		}
		else
		{
			$flagBaseCurrency = "One";
		
		}
		
		
		
		//debug($flagBaseCurrency);
		if($flagBaseCurrency == "Both")
		{
			if($strBaseAmountCurrency == 'GBP')
			{
			}
			else if($strQuotedAmountCurrency == 'GBP')
			{
				$strFirstCurr = $strBaseAmountCurrency;
				$strBaseAmountCurrency = $strQuotedAmountCurrency;
				$strQuotedAmountCurrency = $strFirstCurr;
			}
			else if($strQuotedAmountCurrency == 'EUR')
			{
				$strFirstCurr = $strBaseAmountCurrency;
				$strBaseAmountCurrency = $strQuotedAmountCurrency;
				$strQuotedAmountCurrency = $strFirstCurr;
			}
			if($strSendingCurrency == "GBP")
			{
				$flgExtendMargin = "Sub";
			}
			else if($strReceivingCurrency == "GBP")
			{
				$flgExtendMargin = "Add";
			}
			else if($strSendingCurrency == "EUR")
			{
				$flgExtendMargin = "Sub";
			}
			else if($strReceivingCurrency == "EUR")
			{
				$flgExtendMargin = "Add";
			}
			$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' order by id DESC LIMIT 1";
			$arrExchangeRate = selectFrom($strQueryRate);
		$descript = "Get rate called"; 
	
		$customerID=$_SESSION['loggedUserData']['userID'];
	
//print_r($_SESSION["loginHistoryID"]);
			activities($_SESSION["loginHistoryID"],"SELECTION",$customerID,TBL_ONLINE_EXCHANGE_RATE,$descript);
	
			//debug($arrExchangeRate);
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strBaseAmountCurrency' AND quotedCurrency = '$strQuotedAmountCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			//debug($arrExchangeMargin);
			$fltExchangeRate = $arrExchangeRate['exchangeRate'];
			$fltInverseRate = $arrExchangeRate['inverseRate'];
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
			//debug($_SESSION['exchangeRateError']);
			if(empty($arrExchangeMargin['margin']))
			{
			
			if($_REQUEST['sendingAmount']!='')
				{
				$convertedCurrency=$_REQUEST['receivingCurrency'];
				if($_REQUEST['sendingCurrency']=='GBP')
				{
					if($_REQUEST['sendingAmount']<100 && $_REQUEST['sendingAmount']>50000)
					{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case18";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
					}
			
				}
			
			else
			{	
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
				if($totalAmountInGBP<100 && $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case17";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
					}
			
				}
			}
			
			
			
		else{
			$convertedCurrency=$_REQUEST['sendingCurrency'];
			if($_REQUEST['receivingCurrency']=='GBP')
			{

				if($_REQUEST['receivingAmount']<100 && $_REQUEST['receivingAmount']>50000)
				{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case16";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			else
			{
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 && $totalAmountInGBP>50000){

					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case15";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();


				}

			}

		}
			
			
				/* $_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				//$_SESSION['exchangeRateError1'] = "Test Case15";

				 if($_REQUEST['sendingCurrency'] == 'GBP' && ($_SESSION['sendingAmount']!=''  && $_SESSION['sendingAmount']!= '0.0000' && $_SESSION['sendingAmount']<100 && $_SESSION['sendingAmount']>50000)) 
				 $option=1;
				 if($_REQUEST['sendingCurrency'] != 'GBP' && ($_SESSION['sendingAmount']!=''  && $_SESSION['sendingAmount']!= '0.0000')) 
				 $option=2;
				 if($_REQUEST['receivingCurrency'] == 'GBP' && ($_SESSION['receivingAmount']!=''  && $_SESSION['receivingAmount']!= '0.0000' && $_SESSION['receivingCurrency']<100 && $_SESSION['receivingCurrency']>50000)) 
				 $option=3;
				 if($_REQUEST['receivingCurrency'] != 'GBP' && ($_SESSION['receivingAmount']!=''  && $_SESSION['receivingAmount']!= '0.0000')) 
				 $option=4;
				
				
				switch ($option){
				case 1:
				$_SESSION['totalAmountInGBP'] = $strSendingAmount;
				$_SESSION['exchangeRateError1'] = "Test Case15a";
				break;
				
				case 2:
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; 
				
				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				$_SESSION['exchangeRateError1'] = "Test Case15b";
				break;
				
				case 3:
				$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
				$_SESSION['exchangeRateError1'] = "Test Case15c";
				break;
				case 4:
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				
				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				$_SESSION['exchangeRateError1'] = "Test Case15d";
				break;
				
				default:
				echo "NULL";
				}
				
				
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-ii-new.php");
				exit(); */
			}
			if($strMarginType == "FIXED")
		{
			if($flgExtendMargin == "Add")
			{
				$fltExchangeRate = $fltExchangeRate + $fltMargin;
			}
			else
			{
				$fltExchangeRate = $fltExchangeRate - $fltMargin;
			}
		}
		elseif($strMarginType == "PIPS")
		{
			if($strQuotedAmountCurrency == "JPY")
			{
				$fltMargin = $fltMargin*0.01;
				if($flgExtendMargin == "Add")
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			else
			{
				$fltMargin = $fltMargin*0.0001;
				//debug($fltMargin);
				if($flgExtendMargin == "Add")
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
		}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($flgExtendMargin == "Add")
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			//$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			if($strSendingCurrency =='GBP' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strSendingCurrency =='GBP' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'GBP' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'GBP' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strSendingCurrency =='EUR' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strSendingCurrency =='EUR' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'EUR' && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;	
			}
			else if($strReceivingCurrency == 'EUR' && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;	
			}
			$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,2);
			////debug($arrExchangeRate,true);
		}
		elseif($flagBaseCurrency == "One")
		{
			if($strBaseCurrency1 != "")
			{
				$flgExtendMargin = "Sub";
			}
			else
			{
				$flgExtendMargin = "Add";
			}
			//debug($flagRateOrInverse);
			$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' order by id DESC LIMIT 1";
			$arrExchangeRate = selectFrom($strQueryRate);
			//debug($arrExchangeRate);
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strFinalBaseCurrency' AND quotedCurrency = '$strFinalQuotedCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			//debug($strQueryMargin);
			$arrExchangeMargin = selectFrom($strQueryMargin);
			//debug($arrExchangeMargin);
			if(empty($arrExchangeMargin['margin'])){
			
				if($_REQUEST['sendingAmount']!='')
				{
				$convertedCurrency=$_REQUEST['receivingCurrency'];
				if($_REQUEST['sendingCurrency']=='GBP')
				{
					if($_REQUEST['sendingAmount']<100 || $_REQUEST['sendingAmount']>50000)
					{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case14";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
					}
			
			}
			
			else
			{	
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case13";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
					}
			
				}
			}
			
			
			
		else{
			$convertedCurrency=$_REQUEST['sendingCurrency'];
			if($_REQUEST['receivingCurrency']=='GBP')
			{

				if($_REQUEST['receivingAmount']<100 || $_REQUEST['receivingAmount']>50000)
				{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case12";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			else
			{	
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case11";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
				
				}
			
			}
		
		
		}
				 
	}  
			
			
			//debug($arrExchangeMargin);
			
			//debug($arrExchangeRate);
			$fltExchangeRate = $arrExchangeRate['exchangeRate'];
			$fltInverseRate = $arrExchangeRate['inverseRate'];
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			//debug($strMarginType);
			//debug($flagRateOrInverse);
			$fltExchangeRate = round(floor($fltExchangeRate * 10000) / 10000,4);
			//debug($fltExchangeRate);		
			if($strMarginType == "FIXED")
			{
				if($flgExtendMargin =='Add')
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($flgExtendMargin =='Add')
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					//debug($fltMargin);
					if($flgExtendMargin =='Add')
					{
						$fltExchangeRate = $fltExchangeRate + $fltMargin;
					}
					else
					{
						$fltExchangeRate = $fltExchangeRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($flgExtendMargin =='Add')
				{
					$fltExchangeRate = $fltExchangeRate + $fltMargin;
				}
				else
				{
					$fltExchangeRate = $fltExchangeRate - $fltMargin;
				}
			}

			if($strBaseCurrency1 != "" && $strSendingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
			}
			else if($strBaseCurrency2 != "" && $strReceivingAmount != 0)
			{
				$fltConvertedAmnt = $fltAmountBase*$fltExchangeRate;
			}
			else
			{
				$fltConvertedAmnt = $fltAmountBase/$fltExchangeRate;
			}
			$fltConvertedAmnt = round(floor($fltConvertedAmnt * 100) / 100,2);
			
		}
		elseif($flagBaseCurrency == "None")
		{
		
		if($_REQUEST['sendingAmount']!='')
		{
		$convertedCurrency=$_REQUEST['receivingCurrency'];
			if($_REQUEST['sendingCurrency']=='GBP')
			{
				if($_REQUEST['sendingAmount']<100 || $_REQUEST['sendingAmount']>50000)
				{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case10";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			
			else
			{	
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case9";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
				}
			
			}
		}
		
		
		
		else{
			$convertedCurrency=$_REQUEST['sendingCurrency'];
			if($_REQUEST['receivingCurrency']=='GBP')
			{

				if($_REQUEST['receivingAmount']<100 || $_REQUEST['receivingAmount']>50000)
				{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case8";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			else
			{	
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case7";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
				
				}
			
			}
		
		
		}
		
			if($strBaseAmountCurrency == $strSendingCurrency)
			{
				$fromCurrency = $strSendingCurrency;
				$toCurrency = $strReceivingCurrency;
			
			}
			else
			{
				$fromCurrency = $strReceivingCurrency;
				$toCurrency = $strSendingCurrency;
			}
			//debug($strSendingCurrency);
			//debug($strReceivingCurrency);
			$queryUSDFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryUSDFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for USD
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for USD
			
			$queryUSDTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryUSDTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for USD
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'USD' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for USD
			$fltConvertedAmnt1 = $fltAmountBase/$fromRate*$toRate;
			//USD END
			//start GBP
			$queryGBPFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryGBPFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for GBP
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$fromCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				$fromRate = $fromRate-$fltMargin;
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for GBP
			
			$queryGBPTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryGBPTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for GBP
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'GBP' AND quotedCurrency = '$toCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for GBP
			$fltConvertedAmnt2 = $fltAmountBase/$fromRate*$toRate;
			//GBP END
			
			//start EUR
			$queryEURFrom ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateFrom = selectFrom($queryEURFrom);
			$fromRate = $arrExchangeRateFrom["exchangeRate"];
			//Calculate margin of From for EUR
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$fromCurrency'  and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}
					
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$fromRate = $fromRate + $fltMargin;
					}
					else
					{
						$fromRate = $fromRate - $fltMargin;
					}				
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$fromRate = $fromRate + $fltMargin;
				}
				else
				{
					$fromRate = $fromRate - $fltMargin;
				}
			}
			//Margin END From for EUR
			
			$queryEURTo ="SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' order by id DESC LIMIT 1";
			$arrExchangeRateTo = selectFrom($queryEURTo);
			
			$toRate = $arrExchangeRateTo["exchangeRate"];
			
			//Calculate margin of From for EUR
			$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = 'EUR' AND quotedCurrency = '$toCurrency' and $fltAmountBase between fromAmount AND toAmount order by id DESC LIMIT 1";
			$arrExchangeMargin = selectFrom($strQueryMargin);
			$strMarginType = $arrExchangeMargin['marginType'];
			$fltMargin = $arrExchangeMargin['margin'];
			if($strMarginType == "FIXED")
			{
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			elseif($strMarginType == "PIPS")
			{
				if($strQuotedAmountCurrency == "JPY")
				{
					$fltMargin = $fltMargin*0.01;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}
				}
				else
				{
					$fltMargin = $fltMargin*0.0001;
					if($strSendingAmount !=0)
					{
						$toRate = $toRate + $fltMargin;
					}
					else
					{
						$toRate = $toRate - $fltMargin;
					}				
				}
			}
			elseif($strMarginType == "PERCENTAGE")
			{
				$fltMargin = $fltMargin*0.01;
				if($strSendingAmount !=0)
				{
					$toRate = $toRate + $fltMargin;
				}
				else
				{
					$toRate = $toRate - $fltMargin;
				}
			}
			//Margin END to for EUR
			$fltConvertedAmnt3 = $fltAmountBase/$fromRate*$toRate;
			//EUR END
			
			//debug($fltConvertedAmnt1);
			//debug($fltConvertedAmnt2);
			//debug($fltConvertedAmnt3);
			if($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "" && $fltConvertedAmnt3 !="")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt2 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt2));
			}
			elseif($fltConvertedAmnt1!="" && $fltConvertedAmnt3 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt1, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt2!="" && $fltConvertedAmnt3 != "")
			{
				$fltConvertedAmnt = min(array($fltConvertedAmnt2, $fltConvertedAmnt3));
			}
			elseif($fltConvertedAmnt1!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt1;
			}
			elseif($fltConvertedAmnt2!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt2;
			}
			elseif($fltConvertedAmnt3!="")
			{
				$fltConvertedAmnt = $fltConvertedAmnt3;
			}
			if($fltConvertedAmnt1!="" || $fltConvertedAmnt2 != "" || $fltConvertedAmnt3 !="")
			{
				$arrExchangeRate = $fromRate;
				$fltExchangeRate = $toRate/$fromRate;
				$fltInverseRate =  $fromRate/$toRate;
			}
			//debug($fltConvertedAmnt);
		
		
		}
		if($_REQUEST['sendingAmount']!='')
		{
			$convertedCurrency=$_REQUEST['receivingCurrency'];
		if($_REQUEST['sendingCurrency']=='GBP'){
				if($_REQUEST['sendingAmount']<100 || $_REQUEST['sendingAmount']>50000){
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strSendingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case6";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			
			else
			{	
				$sendingCurrency=$_REQUEST['sendingCurrency'];
				$sendingAmount=$_REQUEST['sendingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$sendingCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				 $totalAmountInGBP=$sendingAmount*$getExchangeRateResult[0]['inverseRate']; //exit;
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case5";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
				}
			
			}
		}
		else{
			$convertedCurrency=$_REQUEST['sendingCurrency'];
			if($_REQUEST['receivingCurrency']=='GBP'){

				if($_REQUEST['receivingAmount']<100 || $_REQUEST['receivingAmount']>50000)
				{
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $strReceivingAmount;
					//$_SESSION['exchangeRateError1'] = "Test Case4";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					
					header("LOCATION: make_payment-ii-new.php");
					exit();
					
				}
			
			}
			else
			{	
				$recCurrency=$_REQUEST['receivingCurrency'];
				$recAmount=$_REQUEST['receivingAmount'];
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$recCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$recAmount*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 || $totalAmountInGBP>50000){
				
					$_SESSION['receivingAmount'] = $strReceivingAmount;
					$_SESSION['sendingAmount'] = $strSendingAmount;
					$_SESSION['sendingCurrency'] = $strSendingCurrency;
					$_SESSION['receivingCurrency'] = $strReceivingCurrency;
					$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
					//$_SESSION['exchangeRateError1'] = "Test Case3";
					$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
					header("LOCATION: make_payment-ii-new.php");
					exit();
				
				
				}
			
			}
		
		
		}
		if($convertedCurrency=='GBP'){
		if($fltConvertedAmnt<100 && $fltConvertedAmnt>50000){
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				//$_SESSION['exchangeRateError1'] = "Test Case2";
				$_SESSION['totalAmountInGBP'] = $strSendingAmount;
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-ii-new.php");
				exit();
		
			}
		}
		else{
				$getExchangeRateQuery="select exchangeRate,inverseRate from ".TBL_ONLINE_EXCHANGE_RATE." where baseCurrency='GBP' and quotedCurrency='$convertedCurrency' order by id DESC limit 1 ";
				$getExchangeRateResult=selectMultiRecords($getExchangeRateQuery);
				$totalAmountInGBP=$fltConvertedAmnt*$getExchangeRateResult[0]['inverseRate'];
				if($totalAmountInGBP<100 && $totalAmountInGBP>50000){
				
				$_SESSION['receivingAmount'] = $strReceivingAmount;
				$_SESSION['sendingAmount'] = $strSendingAmount;
				$_SESSION['sendingCurrency'] = $strSendingCurrency;
				$_SESSION['receivingCurrency'] = $strReceivingCurrency;
				$_SESSION['totalAmountInGBP'] = $totalAmountInGBP;
				//$_SESSION['exchangeRateError1'] = "Test Case1";
				$_SESSION['exchangeRateError'] = "Please contact us for an exchange rate - 0845 021 2370";
				header("LOCATION: make_payment-ii-new.php");
				exit();
				
				}
		
		}
		//ebug($strReceivingAmount);
		//debug($fltConvertedAmnt);
		//debug($strSendingAmount);
		//debug($strReceivingAmount);
		//$strQueryRate = "SELECT *, FORMAT(exchangeRate, 4) AS exchangeRate, FORMAT(inverseRate, 4) AS inverseRate FROM ".TBL_ONLINE_EXCHANGE_RATE." WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
		////debug($strQueryRate,true);
		/* $strExchangeRate = number_format($arrExchangeRate['exchangeRate'], 2, ".", "");
		$strInverseRate = number_format($arrExchangeRate['inverseRate'], 2, ".", ""); */
		//$arrExchangeRate = selectFrom($strQueryRate);
		//$strQueryMargin = "SELECT marginType,margin FROM `fx_rates_margin` WHERE baseCurrency = '$strReceivingCurrency' AND quotedCurrency = '$strSendingCurrency' LIMIT 1";
		//$arrExchangeMargin = selectFrom($strQueryMargin);
		//$strExchangeRate = $arrExchangeRate['exchangeRate'];
		//$strInverseRate = $arrExchangeRate['inverseRate'];
		//$strMarginType = $arrExchangeMargin['marginType'];
		//$fltMargin = $arrExchangeMargin['margin'];

		/*
		if($strMarginType == "PIPS")
		{
			$fltMargin = $fltMargin*0.0001;
			$strExchangeRate = $strExchangeRate-$fltMargin;
			
		
		}
		
		elseif($strMarginType == "PERCENTAGE")
		{
			$fltMargin = $fltMargin * 0.01;
			$strExchangeRate = $strExchangeRate-$fltMargin;
		
		
		}
		elseif($strMarginType == "FIXED")
		{
			$strExchangeRate = $strExchangeRate-$fltMargin;
		
		}
		*/
		////debug("test",true);
		//$strLowerLimit = number_format("0", 2, ".", "");
		if(empty($arrExchangeRate)){
			$_SESSION['receivingAmount'] = $strReceivingAmount;
			$_SESSION['sendingAmount'] = $strSendingAmount;
			$_SESSION['sendingCurrency'] = $strSendingCurrency;
			$_SESSION['receivingCurrency'] = $strReceivingCurrency;
			$_SESSION['exchangeRateError'] = "We are sorry for the inconvenience, currently we are not dealing in this currency";
			header("LOCATION: make_payment-ii-new.php");
		}
	}elseif(!empty($_REQUEST['goBack']) && $_REQUEST['goBack'] == 'Back'){
		$intBackFlag = true;
		$strReceivingAmount = $_SESSION['receivingAmount'];
		$strSendingAmount = $_SESSION['sendingAmount'];
		$strReceivingCurrency = $_SESSION['receivingCurrency'];
		$strSendingCurrency = $_SESSION['sendingCurrency'];
		$strExchangeRate = $_SESSION['exchangeRate'];
		$strInverseRate = $arrExchangeRate['inverseRate'];
	}else{
		header("LOCATION: make_payment-ii-new.php");
	}
	
	$url=$_SERVER['REQUEST_URI'];
	$url=explode('/',$url);
		$strUserId=$_SESSION['loggedInUser']['accountName'];
	$strEmail=$_SESSION['loggedInUser']['email'];
	
	activities($_SESSION["loggedInUser"]["userID"],"Get Rate","","","Got Rate for ".$strSendingCurrency." - ".$strReceivingCurrency." = ".$fltExchangeRate."");
?>

<!DOCTYPE HTML >
<html>
		<head>
		<title>Premier FX</title>
		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." />
		<meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="imagetoolbar" content="no">
			<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
<!--<link href="css/stylenew.css" rel="stylesheet" type="text/css">-->
<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="javascript/jquery.js"></script>
		<script type="text/javascript" src="javascript/jquery.validate.js"></script>
		<script type="text/javascript" src="javascript/date.js"></script>
		<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
		<script type="text/javascript" src="javascript/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="javascript/ajaxupload.js"></script>
		<link href="css/bootstrap1.css" type="text/css" rel="stylesheet" />
		<!--<link href="css/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" />-->
		<script>
		    $(function() {
        var cd = $('#countdown');
        var c = parseInt(cd.text(),10);
        var interv = setInterval(function() {
            c--;
            cd.html(c);
            if (c == 0) {
                window.location.reload(false);
				//window.location.href = 'make_payment-ii.php';
				 //$('#changeTrans').trigger('click');
				 //window.location.href = window.location.protocol +'//'+ window.location.host + window.location.pathname;
				 location.reload(true);
                clearInterval(interv);
            }
        }, 1000);
    });
		</script>
		</head>
				
		<body>
<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://www.premfx.com"><img id="logo" src="images/logo_png.png"></a>
</div>
<div class="menu">
<ul class="menu_ul">
<li class="menu_li"><a href="make_payment-ii-new.php" class="menu_link menu_selected" target="_parent">Send Money</a></li>
<li class="menu_li"><a href="transaction_history_new.php" class="menu_link" target="_parent">Transaction History</a></li>
<li class="menu_li"><a href="view_beneficiariesnew.php" class="menu_link" target="_parent">Beneficiaries</a></li>
<li class="menu_li"><a href="change_passwordnew.php" class="menu_link " target="_parent">Change Password</a></li>
<li class="menu_li"><a href="contact_us.php" class="menu_link" target="_parent">Contact Us</a></li>
<li class="menu_li"><a href="faqs.php" class="menu_link" target="_parent">FAQs</a></li>
</ul>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Send Money</h2>
<?php include('top-menu.php');?>

<!-- content area -->
<div class="content_area">
<div class="content_area_center">
<p class="content_heading">Spot quote</p>
<p class="content_subheading">
The spot quote rate is refreshed every minute, this quote will be updated in
<span id="tmin" class = "timer" style="font-size:20px;">0 :&nbsp;<span id='countdown' style="font-size:20px;">60</span></span> seconds
</p>
</div>
<!-- content area left-->
<div class="content_area_left">

<form action="beneficiariesnew.php" method="post" id="processTrans">

<div class="field_wrapper">
<label class="form_label rate_info_label">Current rate:</label>
<input type='hidden' name='url' value="<?echo $url[2]; ?>">

                                      <span id="numfo" class="rate_info" ><?php echo number_format($fltExchangeRate,4,".",","); ?></span>
                                      <!--<em id="inverse" class="inverse-rate">Inverse <?php echo number_format($fltInverseRate,4,".",","); ?></em>-->
                                      <input name="exchangeRate" type="hidden" value="<?php echo $fltExchangeRate; ?>"/>
                                      <input name="inverseRate" type="hidden" value="<?php echo $fltInverseRate; ?>"/>

</div>
<br>
<div class="field_wrapper">
<label class="form_label rate_info_label">You pay:</label>
<span id="youpayind" class="rate_info">
<?php
echo $strSendingCurrency." ";
if($strSendingAmount != 0)
{
echo number_format($strSendingAmount,2,".",",");
}
else
{
////debug($fltConvertedAmnt);
echo number_format($fltConvertedAmnt,2,".",",");
}
?>
</span>
<input name="sendingAmount" type="hidden" value="<?php if($strSendingAmount != 0)
{ echo $strSendingAmount;
}else
{ echo $fltConvertedAmnt;
}
?>" />
<input name="sendingCurrency" type="hidden" value="<?php echo $strSendingCurrency; ?>" />
</div>
<br>
<div class="field_wrapper">
<label class="form_label rate_info_label">You receive:</label>
<span id="youbuyd" class="rate_info">
<?php 
echo $strReceivingCurrency." ";
if($strReceivingAmount != 0)
{
echo number_format($strReceivingAmount,2,".",",");
}
else
{
echo number_format($fltConvertedAmnt,2,".",",");
}
?>
</span>
<input name="receivingAmount" type="hidden" value="<?php if($strReceivingAmount != 0)
{ echo $strReceivingAmount;
}else
{ echo $fltConvertedAmnt;
}
?>"/>
<input name="receivingCurrency" type="hidden" value="<?php echo $strReceivingCurrency; 
?>"/>
</div>
<br>

<!--<div class="field_wrapper margin_400">
<input name="reject" id="reject" class="submit_btn" type="button" value="Go back" onClick="window.location.href='make_payment-ii-new.php?reject&sendingCurrency=<?=$strSendingCurrency?>&receivingCurrency=<?=$strReceivingCurrency?>&fltExchangeRate=<?=$fltExchangeRate?>'"/>
<input id="changeTransaction" name="changeTransaction" type="hidden" value="changeTransaction" />
<input name="changeTrans" id="changeTrans" type="button" class="submit_btn" value="CHANGE QUOTE" onClick="this.form.action='make_payment-ii-new.php';this.form.submit();"/>
<input name="accept" id="accept" class="submit_btn" type="submit" value="ACCEPT QUOTE"/>
<a href="#" class="tip" id="acceptTip" data-placement="above" data-content="If you agree the above transaction details then click on the ACCEPT QUOTE button if not click REJECT" rel="popover" title="Agree or Reject"> [?] </a>
</div>-->



</div>

<!-- content area right-->
<div class="content_area_right">
<!--<div class="info_change_password margin_less">
<p><b>The rate is refreshed every 60 seconds</b><br>
If you are happy with the exchange rate click accept quote.
The rate you see is the rate you get.
</p>
</div>-->
</div>

<div class="button_wrapper margin_400">
<input name="reject" id="reject" class="submit_btn" type="button" value="Go back" onClick="window.location.href='make_payment-ii-new.php?reject&sendingCurrency=<?=$strSendingCurrency?>&receivingCurrency=<?=$strReceivingCurrency?>&fltExchangeRate=<?=$fltExchangeRate?>'"/>
<input id="changeTransaction" name="changeTransaction" type="hidden" value="changeTransaction" />
<input name="changeTrans" id="changeTrans" type="button" class="submit_btn" value="CHANGE QUOTE" onClick="this.form.action='make_payment-ii-new.php';this.form.submit();"/>
<input name="accept" id="accept" class="submit_btn" type="submit" value="ACCEPT QUOTE"/>
<a href="#" class="tip" id="acceptTip" data-placement="above" data-content="If you agree the above transaction details then click on the ACCEPT QUOTE button if not click REJECT" rel="popover" title="Agree or Reject"> [?] </a>
</div>
</form>

<p class="bottom_info">For amounts over £50,000 or if you have any other questions, please contact us 0845 021 2370</p>
</div>
<!-- content area ends here-->
</div>
<!-- footer area -->
<?php include('footer.php');?>
</div>



</div>
</body>

<script type="text/javascript">
			$('#acceptTip').popover({
				trigger: 'hover',
				offset: 5
			});
			/* function checkAgree(){
				if(!($('#agreement').is(':checked'))){
					alert('Please click the confirmation of your details correction');
					return false;
				}
				else
					return true;
			} */
			$('#processTrans').submit(
				function(){
					if($(this).attr('action') == 'select_beneficiary_trans.php'){
						return checkAgree();
					}else{
						return true;
					}
				}
			);
			
function changeMySize(myvalue,iden)
{

if(iden==2){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			
			textSize.style.color="red";
			textSize1.style.color="#0088CC";
			textSize2.style.color="#0088CC";
		}
		else if(iden==4){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="red";
			textSize2.style.color="#0088CC";
		}
		else if(iden==5){
			
			var textSize = document.getElementById("sizeTwo");
			var textSize1 = document.getElementById("sizeFour");
			var textSize2 = document.getElementById("sizeFive");
			textSize.style.color="#0088CC";
			textSize1.style.color="#0088CC";
			textSize2.style.color="red";
		}
var tmin = document.getElementById("tmin");
var ctdwn = document.getElementById("countdown");
var msg= document.getElementById("msg");
var upay = document.getElementById("upay");
var pcur = document.getElementById("pcur");
var ubuy = document.getElementById("ubuy");
var bcur = document.getElementById("bcur");
var upayd = document.getElementById("youpayind");
var byd = document.getElementById("youbuyd");
var stcd = document.getElementById("exrt");
var ibnn= document.getElementById("numfo");
var swf = document.getElementById("inverse");
//var ref = document.getElementById("ref");

//tmin.style.fontSize = myvalue + "px";
//ctdwn.style.fontSize = myvalue + "px";
msg.style.fontSize = myvalue + "px";
upay.style.fontSize = myvalue + "px";
pcur.style.fontSize = myvalue + "px";
ubuy.style.fontSize = myvalue + "px";
bcur.style.fontSize = myvalue + "px";
upayd.style.fontSize = myvalue + "px";
byd.style.fontSize = myvalue + "px";
stcd.style.fontSize = myvalue + "px";
ibnn.style.fontSize = myvalue + "px";
swf.style.fontSize = myvalue + "px";

}

		</script>
</html>
