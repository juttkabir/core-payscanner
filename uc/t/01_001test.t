use strict;
use warnings;

use lib qw(./tlib ./lib);
use Test::More tests => 2;
use Test::Exception;
use Data::Dumper;
use UC::HTTPTest;
use XML::LibXML;

ok(1);

use_ok 'UC::Payex::Schema';

#
# Do a test on the XML schema
#

my $schema = XML::LibXML::RelaxNG->new(location => 'data/schema.xml');

my $url = '';

my ($response, $content) = makeGetRequest($url);

# Parse content to XML object

my $dom = XML::LibXML->new()->parse_string($content);

ok( eval { $schema->validate( $dom ); 1; }, "Validating schema" );



1;
