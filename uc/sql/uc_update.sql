--
-- Update script to create Universal Connector
--

CREATE TABLE uc_job_queue (
    id          int(11) NOT NULL auto_increment,
    action      varchar(30) NOT NULL,
    ident       varchar(30) NOT NULL,
    status      varchar(20) NOT NULL,
    timestamp   timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;
