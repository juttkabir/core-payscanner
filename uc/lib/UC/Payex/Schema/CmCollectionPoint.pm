package UC::Payex::Schema::CmCollectionPoint;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('cm_collection_point');
__PACKAGE__->add_columns(qw(
    cp_id isAdmin cp_corresspondent_name cp_ria_branch_code cp_ida_id
    cp_branch_name cp_branch_no cp_branch_address cp_city cp_state cp_country
    cp_phone cp_active disabledDate disabledBy disableReason workingDays cp_fax
    sameday openingTime closingTime operatesWeekend weekendWorkingDays
    weekendOpeningTime weekendClosingTime cp_contact_person_name t_weekDayTime
    t_weekEndTime
    ));
__PACKAGE__->set_primary_key('cp_id');

1;
