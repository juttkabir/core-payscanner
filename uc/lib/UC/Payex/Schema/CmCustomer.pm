package UC::Payex::Schema::CmCustomer;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('cm_customer');
__PACKAGE__->add_columns(qw(
    c_id c_name Title FirstName MiddleName LastName c_pass c_date c_email
    c_mobile c_address c_address2 proveAddress c_country c_state c_city c_zip
    c_p_question1 c_answer1 c_p_question2 c_answer2 c_info_source
    c_benef_country customerStatus accessFromIP username last_login
    dateConfrimed SecuriyCode dateDisabled reason holdAccountID accountType
    dateAccepted dob limit1 Balance limit3 determind changedPwd
));
__PACKAGE__->set_primary_key('c_id');

1;
