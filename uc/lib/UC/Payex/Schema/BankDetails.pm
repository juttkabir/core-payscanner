package UC::Payex::Schema::BankDetails;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('bankDetails');
__PACKAGE__->add_columns(qw(
    bankID benID transID bankName accNo branchCode branchAddress ABACPF IBAN swiftCode accountType Remarks
));
__PACKAGE__->set_primary_key('bankID');

1;
