package UC::Payex::Schema::Beneficiary;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('beneficiary');
__PACKAGE__->add_columns(qw(
    benID customerID created Title firstName middleName lastName benAccount
    Password Address Address1 City State Zip Citizenship Country Phone Mobile
    email IDissuedate IDexpirydate IDType IDNumber CPF isTransfered agentID
    other_title otherId otherId_name
));
__PACKAGE__->set_primary_key('benID');

1;
