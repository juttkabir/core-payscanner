package UC::Payex::Schema::Admin;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('admin');
__PACKAGE__->add_columns(qw(
userID username password changedPwd name last_login isMain email role rights adminType created parentID agentType isCorrespondent IDAcountry agentNumber subagentNum agentCompany agentContactPerson agentAddress agentAddress2 agentCity agentZip agentCountry agentCountryCode agentPhone mobile agentFax agentURL agentMSBNumber agentMCBExpiry agentCompRegNumber agentCompDirector designation agentDirectorAdd agentProofID agentIDExpiry agentDocumentProvided agentBank agentAccountName agentAccounNumber agentBranchCode agentAccountType agentCurrency agentAccountLimit limitUsed commPackage agentCommission agentStatus suspensionReason suspendedBy activatedBy disableReason disabledBy logo accessFromIP swiftCode authorizedFor balance postCode payinBook ));
__PACKAGE__->set_primary_key('userID');

1;


