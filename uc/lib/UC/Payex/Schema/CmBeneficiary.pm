package UC::Payex::Schema::CmBeneficiary;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('cm_beneficiary');
__PACKAGE__->add_columns(qw(
    benID customerID created DateDisabled DateLastPaid Title firstName
    middleName lastName benAccount Password Address Address1 address2 City
    State Zip Country Phone Mobile email IDType NICNumber oldNICNumber CPF
    bankDetailsID transType loginID editedBy editDate other_title otherId
    otherid_name
));
__PACKAGE__->set_primary_key('benID');

1;
