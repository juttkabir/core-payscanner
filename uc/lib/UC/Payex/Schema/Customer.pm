package UC::Payex::Schema::Customer;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('customer');
__PACKAGE__->add_columns(
 qw(customerID agentID created Title firstName middleName lastName accountName
    password customerNumber Address Address1 proveAddress City State Zip Country
    Phone Mobile email dob acceptedTerms IDType IDNumber IDExpiry issuedBy
    documentProvided fundsSources transactionPurpose other_title otherId
    otherId_name payinBook balance
));
__PACKAGE__->set_primary_key('customerID');

1;
