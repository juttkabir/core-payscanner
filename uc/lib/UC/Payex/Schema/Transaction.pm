package UC::Payex::Schema::Transaction;
#
# $HeadURL: svn+ssh://dev.horivert.com/var/svn/payex/vertex/trunk/lib/Horivert/Vertex/Schema/User.pm $
# $LastChangedRevision: 1349 $
# $LastChangedDate: 2007-05-01 13:01:14 +0100 (Tue, 01 May 2007) $
# $LastChangedBy: icydee $
#

use strict;
use base qw(DBIx::Class);

__PACKAGE__->load_components(qw(PK::Auto Core));
__PACKAGE__->table('transactions');
__PACKAGE__->add_columns(qw(transID transType customerID benID custAgentID
    benAgentID exchangeID refNumber refNumberIM transAmount exchangeRate localAmount
    IMFee bankCharges totalAmount transactionPurpose fundSources moneyPaid
    Declaration transStatus addedBy transDate verifiedBy authorisedBy cancelledBy
    recalledBy isSent trackingNum trans4Country remarks authoriseDate
    deliveryOutDate deliveryDate cancelDate suspendDate rejectDate failedDate
    suspeciousDate recalledDate fromCountry toCountry currencyFrom currencyTo
    custAgentParentID benAgentParentID is_exported PINCODE createdBy
    collectionPointID transRefID AgentComm CommType other_pur admincharges
    cashCharges refundFee question answer tip holdedBy outCurrCharges
    distributorComm discountRequest discountType discounted_amount
    creation_date_used holdDate verificationDate unholdBy unholdDate deliveredBy));
__PACKAGE__->set_primary_key('transID');

1;
