package UC::Payex::Schema;
use base qw(DBIx::Class::Schema);

__PACKAGE__->load_classes(qw(
    Admin
    BankDetails
    Beneficiary
    CmBeneficiary
    CmCollectionPoint
    CmCustomer
    Country
    Customer
    Transaction
    UCJobQueue
));

1;
