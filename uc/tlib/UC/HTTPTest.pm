package UC::HTTPTest;

use strict;
use warnings;
require  Exporter;
use vars qw( @EXPORT @ISA  );
@ISA=qw( Exporter );
@EXPORT = qw( makeRequest makeGetRequest makePostRequest);

use LWP;
use Apache::TestRequest;
use HTTP::Request;
use HTTP::Cookies;
use HTTP::Headers;
use URI;

sub makeRequest {
    if (wantarray) {
        my @ret = _makeRequest('GET', @_);
        return @ret;
    }
    return _makeRequest('GET', @_);
}

sub makeGetRequest {
    if (wantarray) {
        my @ret = _makeRequest('GET', @_);
        return @ret;
    }
    return _makeRequest('GET', @_);
}

sub makePostRequest {
	my ($url, $body) = @_;
	$url = Apache::TestRequest::resolve_url( $url );
	my $u = URI->new( $url );

	my $h = HTTP::Headers->new;
	$h->header('Content-Type' => 'application/x-www-form-urlencoded');

	my $request = HTTP::Request->new( 'POST', $url, $h, $body);

	my $lwp = LWP::UserAgent->new(
		requests_redirectable   => [],
	);
	my $response = $lwp->request( $request );
	my $content = $response->content();
	return wantarray ? ($response, $content) : $content;
}

sub _makeRequest {
	my ($method, $url, $body) = @_;
	$url = Apache::TestRequest::resolve_url( $url );
	my $u = URI->new( $url );

	my $h = HTTP::Headers->new;
	$h->header('Content-Type' => 'text/xml');

	my $request = HTTP::Request->new( $method, $url, $h, $body);

	my $lwp = LWP::UserAgent->new(
		requests_redirectable   => [],
	);
	my $response = $lwp->request( $request );
	my $content = $response->content();
	return wantarray ? ($response, $content) : $content;
}

1;
