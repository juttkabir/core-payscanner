<?php
	session_start();
	include ("../include/config.php");
	$date_time = date('Y-m-d  h:i:s A');
	dbConnect();
	$username=loggedUser();
	$agentType = getAgentType();
	$parentID = $_SESSION["loggedUserData"]["userID"];
	
	$benID			= $_REQUEST["benID"];
	$benName		= $_REQUEST["benName"];
	$fullName		= $_REQUEST["fullName"];
	$benStatus		= $_REQUEST["benStatus"];
	$benSearch		= $_REQUEST["benSearch"];
	$newOffset		= $_REQUEST["offset"];
	$contactNumber	= $_REQUEST["contactNumber"];
	

	if($benStatus == "Enabled")
	{
		$actionBtnVal	= "Disable";
		$action			= "D";
	}
	elseif($benStatus == "Disabled")
	{
		$actionBtnVal	= "Enable";
		$action			= "E";
	}
	
	$arrReason	= selectFrom("SELECT disableReason FROM ".TBL_BENEFICIARY." WHERE benID='".$benID."'");
	$strReason	= $arrReason["disableReason"];
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Enable/Disable Beneficiary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
<!--

function checkForm(theForm) {
	if(theForm.changeStatusReason.value == "" || IsAllSpaces(theForm.changeStatusReason.value)){
    	alert("Please enter change status reason.");
        theForm.changeStatusReason.focus();
        return false;
    }
	return true;
-->
}
function IsAllSpaces(myStr){
	while (myStr.substring(0,1) == " "){
			myStr = myStr.substring(1, myStr.length);
	}
	if (myStr == ""){
			return true;
	}
	return false;
}
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>

<body>
	<h3 style="padding:5px 5px">Change <?php echo $fullName;?>'s Status</h3>
	<form name="changeStatus" action="benEnableDisable.php" method="post" onSubmit="return checkForm(this);">
		<input type="hidden" name="benID" value="<?php echo $benID; ?>" />
		<input type="hidden" name="benName" value="<?php echo $benName; ?>" />
		<input type="hidden" name="benSearch" value="<?php echo $benSearch; ?>" />
		<input type="hidden" name="action" value="<?php echo $action; ?>" />
		<input type="hidden" name="newOffset" value="<?php echo $newOffset; ?>" />
		<input type="hidden" name="contactNumber" value="<?php echo $contactNumber; ?>" />
		<input type="hidden" name="from" value="enableDisableBen" />
		<table width="450" border="0" style="margin:40 auto"> 
			<tr bgcolor="#DFE6EA">
				<th colspan="2" style="border:2px solid #333333">
					<font color="#000066" size="2"><strong><?php echo ($actionBtnVal." ".$fullName);?></strong></font>
				</th>
			</tr>
			<tr bgcolor="#DDDDDD">
				<td valign="top" width="150"><font color="#000066"><strong><?php echo $actionBtnVal;?> Reason * </strong></font></td>
				<td width="300"><textarea name="changeStatusReason" id="changeStatusReason" cols="54" rows="6"><?=$strReason;?></textarea></td>
			</tr>
			<tr bgcolor="#DDDDDD">
				<td>&nbsp;</td>
				<td><input type="submit" name="actionBtn" id="actionBtn" value="<?php echo $actionBtnVal; ?>" /></td>
			</tr>
		</table>
	</form>
</body>
</html>
