<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
session_register("type");

//session_register["agentID2"];

if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
	$_SESSION["type"]="";		
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
	$_SESSION["type"]=$_POST["type"];
	
	
		
}

if($_POST["agentID"]!="")
	$_SESSION["agentID2"] =$_POST["agentID"];
	elseif($_GET["agentID"]!="")
		$_SESSION["agentID2"]=$_GET["agentID"];
	else 
		$_SESSION["agentID2"] = "";


if ($_SESSION["type"]== 'Deposited'){
		if(CONFIG_LEDGER_LABEL != '1')
		{
			$transData = " and TransID = ''";
		}
		$transtype='DEPOSIT';
		
		//$transtype='Manually Deposited';
		//$transtype_second='Barclays Payment';		 	
	}
	else if ($_SESSION["type"]== 'Refund'){
		
		$transData = "  and TransID != ''";
		$transtype='DEPOSIT';
	//	$transtype='Transaction Cancelled';
		//$transtype_second='Transaction Rejected';	
	}
	else {
		$transtype='WITHDRAW';
		//$transtype_second='Transaction Amended';	
		}		
	
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Agent Account</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(Search) {

if(Search.agentID.options.selectedIndex == 0){
    	alert("Please select the agent from the list.");
        Search.agentID.focus();
        return false;
    }
}


function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	  <tr>
    <td class="topbar">Agent Statement</td>
  </tr>
	<tr>
      <td width="563">
      	<table width="100%" border="0" cellspacing="1">
      		<tr>
      			<td colspan="2">
	<form action="agent_Account_List.php" method="post" name="Search" onSubmit="return checkForm(this);">
  		<div align="center">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day < 32;$Day++)
			{
				if ($Day < 10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
         </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                </td>
            </tr>
            <tr>
            	<td align="center">
            		Select Agent 
            	
                 <?    if($agentType == "admin" || $agentType == "Admin Manager" || $agentType == "COLLECTOR" || $agentType == "Branch Manager")//101
								  {
								  ?>
                <select name="agentID" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select Agent-</option>
                  <?
                  $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$changedBy'";				
									
								}
								$agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                  <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                                </script>
                 <? } ?>
                 
               </td>
            </tr>
            <tr>
            	<td align="center">
            		Select Type  
            		                
                 <select name="type" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select All-</option>
                  <?
                  if(CONFIG_LEDGER_LABEL == '1')
                  {
                   ?>
                   <option value="Deposited"><? echo(CONFIG_DEPOSIT); ?></option>
                   <option value="Withdraw"><? echo(CONFIG_WITHDRAW); ?></option>
                   <?
                  }else{
                  ?>
                   <option value="Deposited">Deposited</option>
                   <option value="Refund">Refund</option>
                   <option value="Withdraw">Withdraw</option>                  
                  <?
                	}
                  ?>
                </select>
                  <script language="JavaScript">
         	SelectOption(document.forms[0].type, "<?=$_SESSION["type"]; ?>");
                                </script>
                  </td>
            </tr>
            <tr>
            	<td   align="center" colspan="2">          
                <input type="submit" name="Submit" value="Search">
              </div>
        	              </script>
                  </td>
            </tr>
        </table>
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["search"]=="search")
			$slctID=$_GET["agentID"];
		else
			{
				$slctID=$_POST["agentID"];	
			}
			
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			 
		if ($slctID !=""){
			$accountQuery="Select * from agent_account where agentID = '$slctID'";
				
				 $accountQuery .=  " and $queryDate";									
				$accountQuery .= " and status != 'Provisional' ";
				if ($_SESSION["type"]!=""){
					if ($_SESSION["type"]=="Withdraw"){
					$accountQuery .=  " and type like '$transtype'";
					}
					else{
				 $accountQuery .=  $transData;
				 $accountQuery .=  " and type like '$transtype'";
				}
			}
			//echo $accountQuery;	
			$contentsAcc = selectMultiRecords($accountQuery);
	
			$allCount = count($contentsAcc);
			//echo $accountQuery;
			  $accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}else{
			$slctID = $changedBy;
			$accountQuery="Select * from agent_account where agentID = $changedBy";
					 $accountQuery .=  " and $queryDate";							
				 $accountQuery .= " and status != 'Provisional' ";
				if ($_SESSION["type"]!=""){
					if ($_SESSION["type"]=="Withdraw"){
					$accountQuery .=  " and type like '$transtype'";
					}
					else{
				 $accountQuery .=  $transData;
				 $accountQuery .=  " and type like '$transtype'";
				}
			}
			//echo $accountQuery;		
			$contentsAcc = selectMultiRecords($accountQuery);
		
			$allCount = count($contentsAcc);
			//echo $accountQuery;
			$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
			}									
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
				if ($_SESSION["type"]== 'Withdraw' || $_SESSION["type"]== 'Refund' ){
			?>
			<tr>
			<td>&nbsp;
				<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
				<table>
					<tr>
						<td width="300">
							Cumulative Total <? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate);  
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>
			</td>
			<td>
				<? if ($allAmount < 0 ){ ?>
				<font color="red"><strong>The agent has to pay the money </strong></font>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<strong> Agent Credit Balance  </strong>
			<? } ?>
			</td>
		</tr>
			</table>
			
			<?
		}
		?>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <td align="left" ><span class="style1"><?=$systemCode ?></span></td>
					  <td align="left" ><span class="style1"><?=$manualCode ?></span></td>					  
					  <td align="left" ><span class="style1">Local Amount</span></td>
						<td align="left" ><span class="style1">Foreign Amount</span></td>
						<td align="left" ><span class="style1">Exchange Rate</span></td>
						<td align="left" ><span class="style1"><?=$systemPre ?>Fee </span></td>
						<td align="left" ><span class="style1">Description</span></td>
						
				</tr>
				<? for($i=0;$i<count($contentsAcc);$i++)
				{
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td width="200" align="left"><strong><font color="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>					
					</td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM,refNumber,transAmount,localAmount,exchangeRate,IMFee FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				
				<td width="56">
					&nbsp;<? echo $trans["refNumber"]; ?>
				</td>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["transAmount"]); ?>
				</td>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["localAmount"]); ?>
				</td>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["exchangeRate"]); ?>
				</td>
				<td width="56">
					&nbsp;<? echo customNumberFormat($trans["IMFee"]); ?>
				</td>
				
									
					<!--td width="200" align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_COLLECTION." WHERE cp_id ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td-->
					<!--td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?><td-->
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["description"]; ?>
				</td>
				
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
					 <td>&nbsp;</td>
					  <td>&nbsp;</td>
					 
				</tr>
				
				<!--tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); ?></td>
				  	<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						
						
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					echo number_format($_SESSION["grandTotal"],2,'.',',');
					?>
					  </td>
				  		<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>
						
						
						
						
                </tr -->
            <tr bgcolor="#FFFFFF">
              <td colspan="6">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
			</table>		
            <?
			}
			
			else {
				
//////// for Deposit only

?>
				
				
	<tr>
			<td>&nbsp;
			<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
				<table>
					<tr>
						<td width="300">
							Cumulative Total <? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate);  
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>
						</td>
						<td>
							<? if ($allAmount < 0 ){ ?>
							<font color="red"><strong>The agent has to pay the money </strong></font>
							<? 
						}		
						if ($allAmount > 0 )
						{		
						?>
						<strong> Agent Credit Balance  </strong>
						<? } ?>
						</td>
					</tr>
				</table>
			<?
			}
			?>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <?
					  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
					  {
					  ?>
					  	<td align="left" ><span class="style1">Modified By</span></td>
					  	<?
					  }
					  ?>
					  <?
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  ?>
					  <td align="left" ><span class="style1">Opening Balance</span></td>
					  <?
						}
					  ?>
					  <td align="left" ><span class="style1">
					  	<?
					  	if(CONFIG_LEDGER_LABEL == '1')
					  	{
					  		echo(CONFIG_DEPOSIT);
					  	}else{
					  		echo("Money In");
					  		}
					  	?></span></td>
					  <td align="left" ><span class="style1">
					  	<?
					  	if(CONFIG_LEDGER_LABEL == '1')
					  	{
					  		echo(CONFIG_WITHDRAW);
					  	}else{
					  		echo("Money Out");
					  		}
					  	?></span></td>
						<td align="left" ><span class="style1">Transaction</span></td>
						<td align="left" ><span class="style1">Description</span></td>
						<td align="left" ><span class="style1">Note</span></td>	
						<?
					  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
					  {
					  ?>
						<td align="left" ><span class="style1">Status</span></td>
						<?
					  }
					  ?>
						<?
						if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
						  {
						  ?>
						  <td align="left" ><span class="style1">Closing Balance</span></td>
						  <?
							}
						  ?>
											
				</tr>
				<? 
					$preDate = "";
					//$preDate = $fromDate;
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i<count($contentsAcc);$i++)
				{
					
					///////////////////Opening and Closing////////////////
					
						if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
						{
						
							$currDate = $contentsAcc[$i]["dated"];
							//echo $currDate ."!=". $preDate."<br>"; 
							if($currDate != $preDate)
							{
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								//echo "--> ".$FirstDated; 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
									$account1 .= " and  user_id = '".$slctID."' ";								
									$contents1 = selectFrom($account1);
									if($currDate == $FirstDated)
										$openingBalance =  $contents1["opening_balance"];
									else
										$openingBalance =  $contents1["closing_balance"];
								}
								
								$closingBalance = $openingBalance;
								 $preDate = $currDate;
								$_SESSION["openingBalance"] = $openingBalance;
							}
								
						}
						$_SESSION["currDate"] = $currDate;
					/////////////////////////Opening and Closing///////////
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td width="200" align="left"><strong><font color="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
				  <?
				  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
				  {
				  ?>
					<td width="200" align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_COLLECTION." WHERE cp_id ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					<?
				  } 
				  ?>
					<?
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  ?>
					  <td width="200" align="left"><?=customNumberFormat($openingBalance)?></td>
					  
					  <?
					}
					?>
					
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo customNumberFormat($contentsAcc[$i]["amount"]);
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?></td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["description"]; ?>
				</td>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["note"]; ?>
				</td>
				<?
				 if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
				 {
				 ?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["status"]; ?>
				</td>
				<?
				}
				?>
				<?
					  if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					  {
					  	if(!empty($_REQUEST["closingBalance"]))
							$closingBalance = $_REQUEST["closingBalance"];
					  	if($contentsAcc[$i]["type"] == "WITHDRAW")
						{
							$closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
						}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
						{
							$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
						}
						$_SESSION["closingBalance"] = $closingBalance;
					  ?>
					  <td width="200" align="left"><?=customNumberFormat($closingBalance)?></td>
					  
					  <?
					}
					?>
				
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
					 <td>&nbsp;</td>
				</tr>
			<?		
		if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
		{
		?>	
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); ?></td>
				  	<td>&nbsp;</td>
						<td>&nbsp;</td>
						
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					echo number_format($_SESSION["grandTotal"],2,'.',',');
					?>
					  </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>	
						
                </tr>
                <?
            }
            ?>
            <tr bgcolor="#FFFFFF">
              <td colspan="6">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
			</table>			
				
				
				
				
				
				
				
				
				
				
				
				
		<?	
			}} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table width="781" border="0" bordercolor="#EFEFEF">
            <tr bgcolor="#FFFFFF">
				<td width="93" align="left"><span class="style1">Date</span></td>
                 
                <td width="215" align="left"><span class="style1">Modified By</span></td>
                      
                 <td width="174" align="left"><span class="style1">Money In</span></td>
                    
                 <td width="281" align="left">Money Out</td>
				 <td width="120"  align="left"><span class="style1">Transaction</span></td>
						<td width="96" align="left"><span class="style1">Description</span></td>
						<td align="left" ><span class="style1">Status</span></td>
            </tr>	
			<tr bgcolor="#FFFFFF">
				<td>&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
      	<td align="left">&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
      	<td>&nbsp;</td>
     	</tr>
			</table>
		   </td>
		  </tr>
			<?
			}
			if($agentType == "admin" || $agentType == "Admin Manager")
			{
			if($slctID!=''){
			?>
			
				
			<tr>
		          <!--td height="15" bgcolor="#c0c0c0"><span class="child"><a HREF="add_Agent_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit 
                    Money</font></a></span> </td-->
			 
  	</tr>
	<? } 
}
	?>
       </table>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>