<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$add = $_SERVER['REMOTE_ADDR'];
$modifyby = $_SESSION["loggedUserData"]["userID"];
$loggedUserID = $_SESSION["loggedUserData"]["userID"];
$agentType = getAgentType();
$dealFor = strtoupper(CONFIG_DEAL_SHEET_USER_TYPE);
$root_path = $_SERVER['DOCUMENT_ROOT'];
$url_host = $_SERVER['HTTP_HOST'];
$server_protocol = $_SERVER['SERVER_PROTOCOL'];
if(strstr($server_protocol,"HTTP")){
	$url_address = "HTTP://".$url_host;
}elseif(strstr($server_protocol,"HTTPS")){
$url_address = "HTTPS://".$url_host;
}	
?>
<html>
<head>
<title>Distributor Deal Sheet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.smartmodal.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>

<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 10;
		var userType = '';
		var dealType = '<?=$dealFor?>';
	
		if(dealType == 'CUSTOMER')
		{
		   	 fieldName = 'Customer No';
			 userType =  'CUSTOMER';
		}	  
		else
		{
			fieldName = 'Distributor';	
			userType = 'DISTRIBUTOR';
		}	
		$("#newDeal").validate({
			rules:
			{
				currency_origin:"required",
				currency_destination:"required",
				value_origin:"required",
				deal_ref:"required",
				distributor:"required",
				rate:"required"
			},
			messageg:
			{
				currency_origin: "<br /> Please Originating Currency",
				currency_destination: "<br /> Please Destination Currency",
				value_origin: "<br /> Please Enter Originating Value",
				deal_ref: "<br /> Please Enter Deal Reference No. that should be unique",
				distributor: "<br /> Please select distrbutor",
				rate: "<br /> Please Enter Deal Rate"
			}
			
		});
		jQuery("#payList").jqGrid({
			url:'new_deal_sheet.php?getGrid=payList&userType='+userType+'&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Date',
				'Purchase From',
				'Selling Currency',
				'Amount',
				'Buying Currency', 
				'Local Amount',
				'Balance',
				'Rate',
				'Deal Ref',
				 fieldName,
				'Trans Sent',
				'Trans Amount',
				'Profit/Loss'

			],
			colModel:[
				{name:'deal_date',index:'deal_date', width:70, height:30, sorttype:'date', datefmt:'Y-m-d'},
				{name:'purchase_from',index:'purchase_from', width:100, align:"right"},
				{name:'currencySale',index:'currencySale', width:90, align:"left"},
				{name:'foreign_amount',index:'foreign_amount', width:70, align:"right"},
				{name:'currencyBuy',index:'currencyBuy', width:90, align:"left"},
				{name:'local_amount',index:'local_amount', width:90, align:"right"},
				{name:'balance',index:'balance', width:70, align:"left"},
				{name:'rate',index:'rate', width:50, align:"left",editable:true, editrules:{required:true, number:true}},
				{name:'deal_ref', index:'deal_ref', width:50, align:"left"},
				{name:'distributor',index:'distributor', width:100, align:"right"},
				{name:'trans',index:'trans', width:80, align:"left"},
				{name:'trans_amount',index:'trans_amount', width:90, align:"right"},
				{name:'profit_loss',index:'profit_loss', width:70, align:"left"}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50],
			pager: jQuery('#pagernav'),
			viewrecords: true,
			multiselect:true,
			sortname: 'deal_date',
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			cellEdit: true,
			cellurl: '<?=$url_address?>/deal_sheet/server.php?action=editRate',
			caption: "New Deal Sheet",
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
			gridReload();
			}		
		});

$("#_sf").click(function(){
	  var deal_date = document.getElementById("deal_date").value;
	  var deal_ref = document.getElementById("deal_ref").value;
	  if(document.getElementById("customer_no"))
	 	 var customer_no = document.getElementById("customer_no").value;
	  if(document.getElementById("distributor"))
	 	 var distributor = document.getElementById("distributor").value;	 
	  var purchase_from = document.getElementById("purchase_from").value;
	  var currency_origin = document.getElementById("currency_origin").value;
	  var value_origin = document.getElementById("value_origin").value;
	  var rate = document.getElementById("rate").value;
	  var currency_destination = document.getElementById("currency_destination").value;
	  var insertNewDeal = document.getElementById("insertNewDeal").value; 
	  var agentType = '<?=$agentType?>';
	  var loggedUserID = '<?=$loggedUserID?>';
	  var userType = '<?=$dealFor?>';
	 	 
	  	$.ajax({
		   type: "GET",
		   url: "<?=$url_address?>/deal_sheet/server.php?action=add",
		   data: "deal_date="+deal_date+"&deal_ref="+deal_ref+"&distributor="+distributor+"&purchase_from="+purchase_from+"&currency_origin="+currency_origin+"&value_origin="+value_origin+"&rate="+rate+"&currency_destination="+currency_destination+"&insertNewDeal="+insertNewDeal+"&agentType="+agentType+"&loggedUserID="+loggedUserID+"&customer_no="+customer_no+"&userType="+userType,
		   async: false,
		   beforeSend: function(XMLHttpRequest){
		   	$("#msg").html("");
		   },
		   error: function(XMLHttpRequest, textStatus, errorThrow)
		   {
		   		return false;
		   },
		   success: function(msg){
				 if(msg =="E")
				{
					$("#msg").html("<strong><font color='red' size='2'> New Deal Not Added Reference Should be Unique</font></strong>");
					 return false;
				}
				else if(msg == "M")
				{
					$("#msg").html("<strong><font color='red' size='2'> Some Mandatory Fields are missing</font></strong>");
					 return false;
				}
				else if(msg == "C")
				{
					$("#msg").html("<strong><font color='red' size='2'> Customer Number is Invalid or Not Exist in Data Base</font></strong>");
					 return false;
				}
				else if(msg == "S")
				{
					$("#msg").html("<strong><font color='green' size='2'> New Deal Added Successfully</font></strong>");
					 clearFields();
					 gridReload();
					 return true;
				}
		   }
		 });		
	return true;		
			
			
		});
		
	jQuery("#btnActionDel").click( function(){
	  var agentType = '<?=$agentType?>';
	  var loggedUserID = '<?=$loggedUserID?>';
			if($(this).val()=="Delete"){
				var dealIDs = jQuery("#payList").getGridParam('selrow');
				if(dealIDs!=''){
					dealIDs = dealIDs.toString();
					var confirmDel = confirm("Are you sure to delete selected Deal Sheet Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allDealIdsArr = dealIDs.split(",");
					$("#dealListDeleteData").load("<?=$url_address?>/deal_sheet/server.php?type=", {'dealIDs[]': allDealIdsArr,'btnAction': 'Delete', 'agentType': agentType, 'loggedUserID': loggedUserID},
						 function(){
						alert($("#dealListDelete").val());
						gridReload();
					});
				}	
				else{
					alert("Please Select Deal Sheet to delete");
					$("#allDealIdsV").val('');
					return false;
				}
				$("#allDealIdsV").val('');
				gridReload();
			}
		});	
	
	$('.rmtSt').smart_modal(); {hide_on_overlay_click:false}
});

function gridReload()
	{
	  var userType = '<?=$dealFor?>';
	  var theUrl = 'new_deal_sheet.php?getGrid=payList&userType='+userType+'&nd='+new Date().getTime();
		
		
			jQuery("#payList").setGridParam({
				url: theUrl,
				page:1
			}).trigger("reloadGrid");
	}	
function clearFields()
{
	 $("#deal_date").val("<?=date("Y-m-d")?>");
	 $("#deal_ref").val("");
	 $("#value_origin").val("");
	 $("#purchase_from").val('');
	 $("#rate").val("");
	 $("#currency_origin").val("");
	 $("#currency_destination").val("");
}		
</script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>

<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }
.rmtSt {
	color: 6699CC;
	width:800px;
	height:600px;
}
.error {
	color:red;
	font-weight:bold;
}
-->
</style>
<link rel="stylesheet" href="css/smartmodal.css" type="text/css" media="screen" charset="utf-8" />
</head>
<body>
<table width="70%" border="0" align="center" cellpadding="5" cellspacing="1">
   <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF"> Deal Sheet</font></strong></b></td>
  </tr>
  <tr><td><div id="msg" align="center"></div></td></tr>
  <tr><td align="center"><font class="error">red mark fields are mandatory</font></td></tr>
   <tr>
	<td valign="top"><br /><br />
	  <form name="newDeal" id="newDeal" action="add_deal_sheet.php" method="post">
	  <table align="center" width="100%" cellpadding="2" cellspacing="0" >
        <tr>
          <td nowrap bgcolor="#6699cc" colspan="9"><strong class="tab-u" style=" line-height:25px">&nbsp;&nbsp;Add New Deal</strong> </td>
        </tr>
        <tr align="left">
          <th>Date</th>
		  <th>Purchase From</th>
		  <th>Orginating Currency<span class="error">*</span></th>
		  <th>Orginating Value<span class="error">*</span></th>
		  <th>Destination Currency<span class="error">*</span></th>
		  <th>Rate<span class="error">*</span></th>
          <th>Reference<span class="error">*</span></th>
		  <? if($dealFor == 'CUSTOMER'){ ?>
		  <th>Customer Number</th>
          <? }  if($agentType == 'admin' && $dealFor == 'DISTRIBUTOR'){ ?>
		  <th>Distributor<span class="error">*</span></th>
		  <? } ?>
          
        
          <!--<th>Destination Value</th>-->
        </tr>
		<?
		$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
		$rs = mysql_query($strQuery) or die('invalid query'.mysql_error());
	   	while($row2 = mysql_fetch_array($rs))
		{
			$currency2[] = $row2["currencyName"];
		}
		 
		  	$distQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and (agentType='Supper' OR agentType='Sub') and isCorrespondent != 'N' ";
			$distRs = mysql_query($distQuery) or die('invalid query'.mysql_error());
		?>
        <tr valign="top">
          <td><input type="text" name="deal_date" id="deal_date" size="15" title="YYYY-MM-DD" value="<?=date("Y-m-d")?>" /></td>
		   <td><input type="text" name="purchase_from" id="purchase_from" size="20" /></td>
		    <td>
		   <select name="currency_origin" id="currency_origin">
		   <option value="">--select currency--</option>
		   <?
		   	foreach($currency2 as $currency)
				echo "<option value='$currency'>".$currency."</option>";
		   ?>
		   </select>
		  </td>
		   <td><input type="text" name="value_origin" id="value_origin" size="20" /></td>
		    <td>
		   <select name="currency_destination" id="currency_destination">
		   <option value="">--select currency--</option>
		   <?
		   
		   	foreach($currency2 as $currency)
				echo "<option value='$currency'>".$currency."</option>";
		   ?>
		   </select>
		  </td>
		  <td><input type="text" name="rate" id="rate" size="10" /></td>
		  <td><input type="text" name="deal_ref" id="deal_ref" size="10" /></td>
		    <? if($dealFor == 'CUSTOMER'){?>
		  <td><input type="text" name="customer_no" id="customer_no" size="20" /></td>
		  <? } if($agentType == 'admin' && $dealFor == 'DISTRIBUTOR'){ ?>
		  <td>
		  <select name="distributor" id="distributor">
		  <!--<option value="generic">None</option>-->
		 <option value="">--select distributor--</option>
		 <?
		    while($row = mysql_fetch_array($distRs))
			{
			 $userID = $row["userID"];
			 $username = $row["username"];
			 echo "<option value='$userID'>".$username."</option>";
			}
			?>
		  </select>
		  </td>
		  <? } ?>
       
        
        </tr>
        <tr>
          <td colspan="9" align="center"><br />
		  <input type="button" value="Submit" id="_sf" value="Add Value" >
          <!--<input name="button" type="button" id="_sf" value="Add Value" />-->
          <input type="hidden" name="insertNewDeal" id="insertNewDeal" value="ADD" />
              <br />          </td>
        </tr>
		</form>
      </table></td>
   </tr>
   <form name="frmSearch" id="frmSearch">
		<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
			<div id="pagernav" class="scroll" style="text-align:center;"></div>
	<tr align="center">
		  <td>
		 		<input type="hidden" name="allDealIdsV" id="allDealIdsV" value="">
				<input name="btnActionDel" id="btnActionDel" type="button"  value="Delete">&nbsp;&nbsp;&nbsp;
				<? if(CLIENT_NAME == "nowtransfer") {?>
				<a class="rmtSt" href="deal_insufficient_funds.php?action='resolve'">Customer Holded due to Insufficient Funds</a>
				<? } ?>
				
		  </td>
	</tr>	
	<tr bgcolor="#FFFFFF">
					<td align="center" colspan="2">
						<div id="dealListDeleteData" style="display:none"></div>
					</td>
				</tr>
</form>
</table>		
</body>
</html>