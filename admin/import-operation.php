<?php
	include ("../include/config.php");
	include ("security.php");
	
	error_reporting(E_ALL ^ E_NOTICE);
	$arrFileNames = array(
		'file1' => 'Customer Data with Reference Number',
	);

	$cmd = $_REQUEST['cmd'];
	$arrErrorLog = array();
	if($cmd == 'do_post')
	{
/*		echo '<pre>';
		print_r($_FILES);
		echo '</pre>';
*/		
		require 'cm_xls/Excel/excel_reader2.php';
		//require_once 'cm_xls/Excel/reader.php';

		
		if(empty($_FILES['file1']['tmp_name']))
		{
			$arrErrorLog[] = $arrFileNames['file1'].' file is not selected.';
		}

		if(!empty($_FILES['file1']['tmp_name']))
		{
			
			$objRegionsData = new Spreadsheet_Excel_Reader();
			$objRegionsData->setOutputEncoding('CP1251');
			$objRegionsData->read($_FILES['file1']['tmp_name']);

			$intRowsCount1 = $objRegionsData->rowcount(0);
			
			//$intMaxColumnCount = $objRegionsData->colcount(0);
			
			$intFile1TotalImportCount = 0;
			
			for($i = 2; $i < $intRowsCount1; $i++)
			{
				$intAgentId = $objRegionsData->val($i, 2);
				if(!empty($intAgentId))
				{
					$strUpdateSql = 'UPDATE customer 
										set 
											accountName = "'.$intAgentId.'", 
											customerNumber = "'.$intAgentId.'"
										WHERE
											customerID = "'.$objRegionsData->val($i, 1).'"		
									';

					//debug($strUpdateSql);
					
					if(update($strUpdateSql))
						$intFile1TotalImportCount++;
				}
				
			}
			
			$arrErrorLog[] = $intFile1TotalImportCount.'/'.$intRowsCount1.' records updated into customers against file '.$_FILES['file1']['name'].'.';
			
		}
		/* Table Data Import - End */

	}
	

	$strTitle = "Excel Sheet - Import / Update Operation";
?>
<html>
<head>
	<title><?=$strTitle?></title>
	<style>
	body {
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;
	}
	</style>
</head>
<body>
  	<form name="reference_data" action="" method="post" enctype="multipart/form-data"> 

	<table width="60%" border="0" align="center" cellpadding="5" cellspacing="1">
		<tr>
			<td colspan="2">
				<h2><?=$strTitle?></h2>
				<br />
				<em>
					All files should be in .xls, excel (1997-2003) format to import.<br />
					Import might take time, depend upon import file(s) size.
				</em>
				
			</td>
		</tr>
		
		<?php if($cmd == 'do_post') { ?>
		<tr>
			<td colspan="2" align="left" style="padding-left:5px; background-color:#CCCCCC">
				<br />
				<?php 
					foreach($arrErrorLog as $strError)
						echo '<b>'.$strError.'<b><br />';
				?>
				<br />
			</td>
		</tr>
		<? } ?>
		
		<tr>
			<td align="right" bgcolor="#DFE6EA">
				Select File of <b><?=$arrFileNames['file1']?></b>
			</td>
			<td bgcolor="#DFE6EA">
				<input name="file1" id="file1" type="file" />
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#DFE6EA" colspan="2">
				<input name="Submit" type="submit" class="flat" value="Submit" />
				<input type="hidden" name="cmd" value="do_post" />
			</td>
		</tr>
	</table>

	</form>

</body>
</html>