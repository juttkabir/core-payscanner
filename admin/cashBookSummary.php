<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	/**
	 * Variables from Query String
	 */
	$agentID		    = $_REQUEST["agentID"];
	
	/**
	 * Some pre data preparation.
	 */
	
	$userId			= (!empty($agentID))?$agentID:$_SESSION["loggedUserData"]["userID"];
	
	
	/**
	 * Check for data validity
	 */
		
	$sql = "SELECT 
				c.userId,
				c.created,
				c.openingBalance,
				c.closingBalance,
				c.banks,
				c.overShort,
				a.name
			FROM
				cash_book c,
				admin a
			WHERE
				c.userId = a.userID
			";
	if(($agentType == "admin" && !empty($agentID)) || $agentType != "admin" )
	{
		$sql .=" AND c.userId = $userId";
	}
	$result = SelectMultiRecords($sql);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cash Book History List</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
</head>

<body>

<? 
if($agentType =="admin")
{
?>
	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<tr>
			<td align="right">Agents</td>
			<?
	//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
			$agentSql = "SELECT 
							userID,
							username, 
							name, 
							agentStatus
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
							AND agentStatus = 'Active'
						ORDER BY
							username
						";
			}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
			$agentSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
						ORDER BY
							username
						";
		}
//work end by Mudassar Ticket #11425	
			$agentResult = SelectMultiRecords($agentSql);
				
			?>
			<td align="center">
				<select name="agentID" id="agentID">
					<option value="">-- All Agents --</option>
					
					<?
					for($i=0; $i<sizeof($agentResult); $i++)
					{
					?>
						<option value="<?=$agentResult[$i]["userID"]?>"><?=$agentResult[$i]["username"]."[".$agentResult[$i]["name"]."]" ?></option>
					<?
					}
					?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].agentID, "<?=$agentID; ?>");
			   </script>
			</td>
		</tr>
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2"><input type="submit" name="Submit" value="Submit" /></td>
		</tr>
	</table>
	</form>
<?
}
?>
<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader">Cash Book History List</td>
				</tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="15%" class="columnTitle">Date</td>
					<td width="15%" class="columnTitle">User Name </td>
					<td width="20%" align="right" class="columnTitle">Opening Balance</td>
					<td width="20%" align="right" class="columnTitle">Closing Balance</td>
					<td width="12%" align="right" class="columnTitle">Bank</td>
					<td width="12%" align="right" class="columnTitle">Over/Short</td>
					<td width="6%" align="right" class="columnTitle">&nbsp;</td>
				</tr>
				<?
					for($i=0; $i<sizeof($result); $i++)
					{
						$date			 		= date('F d, Y', strtotime($result[$i]["created"]));
						$dateQueryString 		= date('Y-m-d', strtotime($result[$i]["created"]));
						$userName				= $result[$i]["name"];
						$openingBalance			= $result[$i]["openingBalance"];
						$closingBalance			= $result[$i]["closingBalance"];
						$bank					= $result[$i]["banks"];
						$overShort				= $result[$i]["overShort"];
				?>
						<tr>
							<td><?=$date?></td>
							<td><?=$userName?></td>
							<td align="right"><?=number_format($openingBalance, 2, ".", ",")?></td>
							<td align="right"><?=number_format($closingBalance, 2, ".", ",")?></td>
							<td align="right"><?=number_format($bank, 2, ".", ",")?></td>
							<td align="right"><?=number_format($overShort, 2, ".", ",")?></td>
							<td align="right"><a href="cashBook.php?userId=<?=$result[$i]["userId"]?>&currentDate=<?=$dateQueryString?>">View</a></td>
						</tr>
				<?
					}
				?>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				
			</table>
			</form>
		</td>
	</tr>
</table>

</body>
</html>
