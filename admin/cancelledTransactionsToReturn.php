<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("ledgersUpdation.php");
$agentType = getAgentType();

$userID  = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


if($_POST["transType"]!="")
{
		$transType2 = $_POST["transType"];
		
	}
	elseif($_GET["transType"]!="") 
	{
		$transType2=$_GET["transType"];
		
	}

if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
	$transID = "";			
	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];

if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];

$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];

//$ttrans = $_POST["totTrans"];
if($_POST["btnAction"] != "")
{
	if(count($trnsid) >= 1)
	{
		for ($i=0;$i< count($trnsid);$i++)
		{
			if($_POST["btnAction"] == "Return")
			{
				$tranStat = selectFrom("select transID, totalAmount, custAgentID, benAgentID,refundFee,transAmount from ". TBL_TRANSACTIONS." where transID='". $trnsid[$i] ."' and transStatus ='Cancelled'");
				$id=$tranStat["transID"];
				if($id != ""){
//####THIS LEDGER PART WAS FOR THE RESTORE CANCELLETION PAGE SO COMENTED IT NEEDS TO CHANGED ACCORDING TO THE CANCELLED TRANSACTION TO RETURN
					
						if($tranStat["refundFee"] == 'No'){
							$refundFees = "No";
						}else{
							$refundFees = "Yes";
						}
	
						$agent = $tranStat["custAgentID"];
						$today = date("Y-m-d");
						
						/////Ledger Start
						transStatusChange($tranStat["transID"], "Cancelled - Returned", $username, $_POST["remarks"], $refundFees);
							
							if(CONFIG_SHARE_OTHER_NETWORK == '1')
							{
								
								
								$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$tranStat["transID"]."'");
														
								if($remoteTransInfo["remoteTrans"] != '')
								{
									
									$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
									if($jointClient["clientId"] != '')
									{
										$otherClient = new connectOtherDataBase();
										$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
										
										
										$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and transStatus != 'Cancelled'");		
										
										if($otherTransaction["transID"] != '')
										{						
											$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
															transStatus = 'Cancelled - Returned', 
															returnedBy = '".$username."',
															returnDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'
															
															where transID = '".$otherTransaction["transID"]."'
															");	
												
											$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
										
										
											if($remoteCustAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
											}else{
												$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
											}				
											if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
											{
												$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
													
													if($remoteBenAgent["agentType"] == 'Sub')
													{
														$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}else{
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled - Returned', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}	
											}
										}
										
									$otherClient->closeConnection();		
									
									dbConnect();
									}
									
								}
									
									
								
							}
						/////Ledger End					
					//	update("update ".TBL_TRANSACTIONS." SET transStatus = 'Cancelled - Returned', returnDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', returnedBy = '$username' where transID = ".$id."");
						
						
					
				}
			}
		}
	}
//redirect("request_cancellation.php?msg=Y&action=". $_GET["action"]);

}

//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType2");
$query = "select * from ". TBL_TRANSACTIONS . " as t where 1 ";
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1 ";

if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{		
				$query = "select * from ". TBL_TRANSACTIONS . " as t where 1 ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1 ";
				
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
		
				$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
								
				$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				
				$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";		
				$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
				
				
				$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
			}
}
	if($transType2 != "")
	{
		$query .= " and (t.transType='".$transType2."')";
		$queryCnt .= " and (t.transType='".$transType2."')";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType2."')";
		  $queryonlineCnt .= " and (t.transType='".$transType2."')";
			}
		
	}

	$query .= " and t.transStatus ='Cancelled'";
	$queryCnt .= " and t.transStatus ='Cancelled'";
	$queryonline .= " and t.transStatus ='Cancelled'";
	$queryonlineCnt .= " and t.transStatus ='Cancelled'";

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	
	break;
	case "Admin Manager":
		break;
	case "admin":
		break;
	case "TELLER":
		$query .= " and t.addedBy = '".$_SESSION["loggedUserData"]["loginName"]."' ";
		$queryCnt .= " and t.addedBy = '".$_SESSION["loggedUserData"]["loginName"]."' ";
		break;
	default:
		$query .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
		$queryCnt .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
		
}
 $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
  $query .= " LIMIT $offset , $limit";
 
  $contentsTrans = selectMultiRecords($query);
 
  $allCount = countRecords($queryCnt);


		
		if($transID != "" && $by != 0)
		{	
			
			
			$onlinecustomerCount = count(selectMultiRecords($queryonlineCnt ));
				
			$allCount = $allCount + $onlinecustomerCount;
			
			
			
					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
			  $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
		
?>
<html>
<head>
	<title>Cancelled Transactions To Return</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Cancelled Transactions To Return</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="cancelledTransactionsToReturn.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="transID" type="text" id="transID" value="<?=$transID?>">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Cash Collection</option>
          <option value="Bank Transfer">To Bank Account</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType2?>");</script>
        	<br>
		<select name="searchBy" >
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
			<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
		
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="cancelledTransactionsToReturn.php" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> Cancelled Transactions to be Returned.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center"><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
		    <td width="74" align="center">&nbsp;</td>
		 </tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  //$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($contentsTrans[$i]["addedBy"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?></td>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			
			<? for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["totalAmount"] .  " " . $onlinecustomer[$j]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? $tempTransId = $onlinecustomer[$j]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?></td>
				   <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$j]["transID"]?>" class="style2">Enquiry</a></td>
					  
				</tr>
				<?
			}
			?>
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><!--input type='hidden' name='totTrans' value='<?echo $i?>'-->
			    <input name="btnAction" type="submit"  value="Return"></td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
	  	</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType2&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>