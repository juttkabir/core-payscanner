<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();

$searchString = trim($_REQUEST["searchString"]);
$searchType = $_REQUEST["searchType"];

$searchKeywordsArray = explode(" ", $searchString);
$searchKeywordsClause = "";

if(!empty($_REQUEST["from"]))
	$transactionPage = $_REQUEST["from"];

if($_REQUEST["manage"] == 'y')
	$_REQUEST["country"] = "United Kingdom";

$qryClause = "select * from ".TBL_BANKS." where 1 ";

switch($searchType)
{
	case "BRANCH_ADDRESS":
		for($i=0; $i<sizeof($searchKeywordsArray); $i++)
		{
			$searchKeywordsClause .= "branchAddress like '%".$searchKeywordsArray[$i]."%' OR ";
		}
		
		$searchKeywordsClause = substr($searchKeywordsClause, 0, strlen($searchKeywordsClause) - 4);
		$qryClause .= "and ".$searchKeywordsClause;
		break;
		
	case "BANK_NAME":
		for($i=0; $i<sizeof($searchKeywordsArray); $i++)
		{
			$searchKeywordsClause .= "name like '%".$searchKeywordsArray[$i]."%' OR ";
		}
		
		$searchKeywordsClause = substr($searchKeywordsClause, 0, strlen($searchKeywordsClause) - 4);
		$qryClause .= "and ".$searchKeywordsClause;
		break;
		
	case "BANK_NAME_ADDRESS":
		for($i=0; $i<sizeof($searchKeywordsArray); $i++)
		{
			$searchKeywordsClause .= "name like '%".$searchKeywordsArray[$i]."%' OR branchAddress like '%".$searchKeywordsArray[$i]."%' OR ";
		}
		
		$searchKeywordsClause = substr($searchKeywordsClause, 0, strlen($searchKeywordsClause) - 4);
		$qryClause .= "and ".$searchKeywordsClause;
		break;
		
	case "SORT_CODE":
		$qryClause .= "and branchCode like '%".$searchString."%'";
		break;
		
}


if($_REQUEST["Submit"] || !empty($_REQUEST["Submit"]) || $_REQUEST["search"] == 'Y' || $_REQUEST["manage"] == 'y')
{
	$qryClause .= " order by name";
	$contentsCust = selectMultiRecords($qryClause);
	$allCount = sizeof($contentsCust);
}

?>
<html>
<head>
<title>Search Banks</title>
<script language="javascript">
<!--
function searchSenderBank()
{
	var searchString = document.getElementById("searchString").value;
	var searchType = document.getElementById("searchType").value;
	if(searchString == '' || IsAllSpaces(searchString))
	{ 
		alert ('Please provide search string.');
		document.getElementById("searchString").focus;
	} 
	
	return true;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function IsAllSpaces(myStr)
{
	while (myStr.substring(0,1) == " ")
		myStr = myStr.substring(1, myStr.length);
	if (myStr == "")
		return true;
	return false;
}
-->
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Search Results Banks </font></strong></td>
  </tr>
  
  <tr>
    <td align="center">
		<table border="1" cellpadding="5" bordercolor="#666666">
			<tr>
            	<td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>
				
				Search <?=__("Sender");?> Banks 
				
				</strong></span></td>
			</tr>
        	<tr>
				<td nowrap>
					<form action="search-sender-bank.php?agentID=<? echo $_GET[agentID]?>" method="post" name="Search" onSubmit="return searchSenderBank();">
					 	Search For <input type="text" size="15" id="searchString" name="searchString" value="<?=$searchString?>" />
						&nbsp;&nbsp;
						On
						<select name="searchType" id="searchType">
							<?
									$typeLable[] = "Branch Address";
									$typeLable[] = "Bank Name";
									$typeLable[] = "Bank Name and Branch Address";
									$typeLable[] = "Sort Code";
									
									$typeValue[] = "BRANCH_ADDRESS";
									$typeValue[] = "BANK_NAME";
									$typeValue[] = "BANK_NAME_ADDRESS";
									$typeValue[] = "SORT_CODE";
								
								for($i=0; $i<sizeof($typeLable); $i++)
								{
									if($typeValue[$i] == $searchType)
									{
										echo "<option value=\"".$typeValue[$i]."\" selected>".$typeLable[$i]."</option>";
									} else {
										echo "<option value=\"".$typeValue[$i]."\">".$typeLable[$i]."</option>";
									}
								}
							?>
						</select>
						<input type="submit" name="Submit" value="Search">
						<input type="hidden" name="country" value="<?=$_REQUEST["country"]?>" />
						<input type="hidden" name="transID" value="<?=$_REQUEST["transID"]?>" />
						<input type="hidden" name="from" value="<?=$_REQUEST["from"]?>" />
						<input type="hidden" name="manage" value="<?=$_REQUEST["manage"]?>" />
					</form>
		   		</td>
      		</tr>
    	</table>
		<br>
		<br>
		<br>
      	<table width="700" border="1" cellpadding="0" bordercolor="#666666">
			<tr>
            	<td height="25" nowrap bgcolor="333333">
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
						<tr>
							<td>
								<?
									if (count($contentsCust) > 0)
									{
										?>
										Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsCust));?></b> of <?=$allCount; ?>
								 		<?
									}
								?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<?
		if(count($contentsCust) > 0)
		{
		?>
			<tr>
				<td nowrap bgcolor="#EFEFEF">
					<table width="700" border="0" bordercolor="#EFEFEF">
						<tr bgcolor="#FFFFFF">
							<td><span class="style1">Bank Id</span></td>
							<td><span class="style1">Bank Name</span></td>
							<td><span class="style1">Sort Code</span></td>
							<td><span class="style1">Branch Name</span></td>
							<td><span class="style1">Country</span></td>
							<td width="124" align="center">&nbsp;</td>
						</tr>
						<?
							for($i=0;$i<count($contentsCust);$i++)
							{
								?>
									<tr bgcolor="#FFFFFF">
										<td width="109"><strong><font color="#006699">
											<? echo $contentsCust[$i]["bankId"]?></font></strong>
										</td>
										<td width="228"><? echo $contentsCust[$i]["name"]; ?></td>
										<td width="228"><? echo $contentsCust[$i]["branchCode"]; ?></td>
										
										<td width="220"><? echo $contentsCust[$i]["branchAddress"]?></td>
										<td width="179"><? echo $contentsCust[$i]["country"]?></td>
										<td align="center">
										
										<?  if($_REQUEST["manage"] == 'y'){ ?>
											<a href="#" onClick="javascript:document.location = 'edit-bank.php?id=<? echo $contentsCust[$i]["id"]?>';" class="style3">Edit</a>									
										<? } ?>
										
										<?  if($_REQUEST["manage"] != 'y'){ ?>
											<!--  #5149
												  This logic was put because beneficiary forms were not retaining values. 
												  so it was needed to submit other than request from transaction page.
												  by Aslam Shahid
											-->
											<? if(strstr($transactionPage,"add-trans")){?>
												<a href="#" onClick="javascript:opener.document.addTrans.action = '<?=$transactionPage?>?transID=<? echo $transID?>&ben_bank_id=<? echo $contentsCust[$i]["id"]?>&notEndSessions=not&from=popUp&benID=<? echo $_GET["benID"]?>&custCountry=<?=$_GET["custCountry"]?>&senderBank=<? echo $contentsCust[$i]["bankId"]?>';  opener.document.addTrans.submit();window.close();" class="style3">Select</a>
											<? }else{?>
												<a href="#" onClick="javascript:opener.document.location = '<?=$transactionPage?>?transID=<? echo $transID?>&ben_bank_id=<? echo $contentsCust[$i]["id"]?>&notEndSessions=not&from=popUp&benID=<? echo $_GET["benID"]?>&custCountry=<?=$_GET["custCountry"]?>&senderBank=<? echo $contentsCust[$i]["bankId"]?>'; window.close();" class="style3">Select</a>
											<? }?>
											<!--<a href="#" onClick="javascript:opener.document.location = '<?=$transactionPage?>?transID=<? echo $transID?>&ben_bank_id=<? echo $contentsCust[$i]["id"]?>&notEndSessions=not&from=popUp&benID=<? echo $_GET["benID"]?>'; window.close();" class="style3">Select</a>-->
										<?  } ?>
										</td>
									</tr>
								<?
							}
						?>
						<tr bgcolor="#FFFFFF">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		<?
		}
		else
		{
			?>
			<tr>
				<td align="center">
					Select proper filters
				</td>
			</tr>	
			<?
		}
		?>
			<tr>
				<td align="center">
					<?  if($_REQUEST["manage"] != 'y'){ ?>
						<input type="button" value="CLOSE" onClick="window.close();" class="style3">
					<? } ?>
				</td>
			</tr>
		</table>
	  </td>
	</tr>
</table>
</body>
</html>