<?php  
session_start();
include ("../include/config.php");
include ("security.php");
//if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "csv")	
//{
	/* Export version is csv */
/*	if(!empty($_REQUEST["distributors"]))
		$strDistSql = selectFrom("select username from ".TBL_ADMIN_USERS." WHERE userID = '".$_REQUEST["distributors"]."'");
		
	$username = $strDistSql["username"];*/
	$customerCode = "MTUK";
	$intDispatchNumber = $customerCode.$_REQUEST["recId"].date("Ymdhis");
	
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
	header('Content-type: application/octet-stream');
	header("Content-disposition: filename=MINASCENTER".$intDispatchNumber.".TXT");
	
	
	if(!empty($_REQUEST["recId"]))
	{
		$strSql = "
				select 
					tr.transID, 
					tr.refNumberIM, 
					tr.refNumber, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.transAmount,
					tr.benAgentID,
					tr.remarks,
					tr.refNumber, 
					tr.dispatchNumber,
					tr.transType, 
					tr.exchangeRate,
					tr.faxNumber,
					tr.transStatus, 
					tr.currencyFrom,
					tr.currencyTo,
					tr.senderBank,
					ben.firstName as bf,
					ben.middleName as bm,
					ben.lastName as bl,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					ben.Phone as bp,
					ben.Address as ba,
					ben.Address1 as ba1,
					ben.City as bc,
					ben.State as bs,
					ben.Zip as bz,
					ben.Mobile as bmobile,
					cust.firstName,
					cust.middleName,
					cust.lastName,
					cust.IDNumber,
					cust.Address,
					cust.Address1, 
					cust.city,
					cust.state,
					cust.zip,
					cust.country,
					cust.dob as sdob,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode,
					tbd.bankName,
					tbd.branchAddress,
					tbd.originalBankID
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
	}
	
	if(strtolower($_REQUEST["searchTrans"]) == "old" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					tr.transID, 
					tr.refNumberIM, 
					tr.refNumber, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.transAmount,
					tr.benAgentID,
					tr.remarks,
					tr.refNumber, 
					tr.dispatchNumber,
					tr.transType,
					tr.faxNumber,
					tr.transStatus, 
					tr.currencyFrom,
					tr.currencyTo,
					tr.exchangeRate,
					tr.senderBank,
					ben.firstName as bf,
					ben.middleName as bm,
					ben.lastName as bl,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					ben.Phone as bp,
					ben.Address as ba,
					ben.Address1 as ba1,
					ben.City as bc,
					ben.State as bs,
					ben.Zip as bz,
					ben.Mobile as bmobile,
					cust.firstName,
					cust.middleName,
					cust.lastName,
					cust.IDNumber,
					cust.Address,
					cust.Address1, 
					cust.city,
					cust.state,
					cust.zip,
					cust.country,
					cust.dob as sdob,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode,
					tbd.bankName,
					tbd.branchAddress,
					tbd.originalBankID
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.transID IN (".$arrAllTransactions.") and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
		
		
	}
	
	if(!empty($strSql))
		$fullRS = SelectMultiRecords($strSql);
	//debug($strSql);
	//debug($fullRS);
	
	 $_arrCountries = array(
            'AF' => 'Afghanistan',
            'AX' => '�land Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CD' => 'Congo, Democratic Republic of',
            'CG' => 'Congo, People\'s Republic of',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire, Ivory Coast',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'TL' => 'Timor-Leste',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FO' => 'Faeroe Islands',
            'FK' => 'Falkland Islands (Malvinas)',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadaloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard and McDonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HR' => 'Hrvatska (Croatia)',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyz Republic',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'AN' => 'Netherlands Antilles',
            'NL' => 'Netherlands',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn Island',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'SH' => 'St. Helena',
            'KN' => 'St. Kitts and Nevis',
            'LC' => 'St. Lucia',
            'PM' => 'St. Pierre and Miquelon',
            'VC' => 'St. Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard & Jan Mayen Islands',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TG' => 'Togo',
            'TK' => 'Tokelau (Tokelau Islands)',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'VI' => 'US Virgin Islands',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'UM' => 'United States Minor Outlying Islands',
            'US' => 'United States of America',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'WF' => 'Wallis and Futuna Islands',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'CS' => 'Serbia and Montenegro',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        );
	$_arrCountriesFlip = array_flip($_arrCountries);
	$strCsvOutput = "";
	$strCsvSeparator = "|";
		
	$cpfseperator = array("-","/",".");
	$benCountry = "";
	for($i=0; $i < count($fullRS); $i++)
	{
		
		$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"];
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"];
		$strTransactionType = strpos($fullRS[$i]["transType"], "Pick");
		
		if(strpos($fullRS[$i]["transType"], "Pick") !== false)
			$strPaymentType = "B"; // Cash 
		elseif(strpos($fullRS[$i]["transType"], "Bank") !== false)
			$strPaymentType = "D"; // Bank Transfer 
		
		foreach($_arrCountriesFlip as $key => $val){
			if(strtoupper($fullRS[$i]["Country"]) == strtoupper($key))
				$benCountry = $val;
		}
		
		/* To Get Beneficairy BANK CODE */
			$benBanksDataQry = selectFrom("select bankId from ".TBL_BEN_BANK_DETAILS." where benID= '".$fullRS[$i]["benID"]."' ");
			$benBankID = $benBanksDataQry["bankId"];
			if(!empty($benBankID)){
				$benBanksData = selectFrom("select bankId from ".TBL_BANKS." where id='".$benBankID."' ");
				$benBankCode = $benBanksData["bankId"];
			}
		
		/**
		 * I suggest you guys to mask it and use just the number 
		 * of our transactions not considering the letters (1234 for MC-1234HEAD,for example)
		 */
		$strReferenceNumber = $fullRS[$i]["refNumberIM"];
		preg_match("/\-[0-9]+/",$strReferenceNumber,$arrMatches);
		$intReferenceNumberOnlyNumeric = substr($arrMatches[0],1);

		/**
		 * In Transaction Type, there are two settings, N=new and V=void. N would be for all new transactions (authorized ones) 
		 * and V would be for a cancel request. It can occur that we send an order for payment and we decide to cancel it before 
		 * it has been paid out in Brazil. We would send a new file for Banco Rendimento with the status of this transaction set
		 * as Void (V).
		 */
		if(strpos($fullRS[$i]["transStatus"], "AwaitingCancellation") !== false)
			$charTransTypeShortCode = "V";
		else
			$charTransTypeShortCode = "N";		
		 
		//$strCsvOutput .= "[ ".$fullRS[$i]["transID"]." ] ";
		$strCsvOutput .= substr($i+1,0,10).$strCsvSeparator; //ORDER_SEQUENCE
		$strCsvOutput .= substr($fullRS[$i]["refNumberIM"],0,14).$strCsvSeparator; // ORDER_NUMBER
		$strCsvOutput .= substr(date("Y-m-d",strtotime($fullRS[$i]["transDate"])),0,10).$strCsvSeparator; //TRANSACTION_DATE
		/*sender fields*/
		$strCsvOutput .= trim(substr($senderFullName,0,40)).$strCsvSeparator; //SENDER_NAME
		$strCsvOutput .= trim(substr($fullRS[$i]["lastName"],0,20)).$strCsvSeparator; //SENDER_LASTNAME
		$strCsvOutput .= substr($fullRS[$i]["city"],0,20).$strCsvSeparator; //SENDER_CITY
		$strCsvOutput .= substr($fullRS[$i]["IDNumber"],0,40).$strCsvSeparator; //SENDER_IDENT1
		$strCsvOutput .= substr(date("Y-m-d",strtotime($fullRS[$i]["sdob"])),0,10).$strCsvSeparator;//SENDER_DOB
		/*receiver fields*/
		$strCsvOutput .= trim(substr($beneficiaryFullName,0,40)).$strCsvSeparator; // RECEIVER_NAME
		$strCsvOutput .= substr($fullRS[$i]["bl"],0,20).$strCsvSeparator; // RECEIVER_LASTNAME
		$strCsvOutput .= substr($fullRS[$i]["bc"],0,30).$strCsvSeparator; // RECEIVER_CITY
		$strCsvOutput .= substr(str_replace($cpfseperator,"",$fullRS[$i]["CPF"]),0,40).$strCsvSeparator; // CPF number
		$strCsvOutput .= substr($fullRS[$i]["bp"],0,30).$strCsvSeparator; // beneficiary phone
		$strCsvOutput .= substr($fullRS[$i]["bmobile"],0,30).$strCsvSeparator; // beneficiary mobile
		$strCsvOutput .= substr($fullRS[$i]["ba"],0,60).$strCsvSeparator; // Beneficiary's address
		$strCsvOutput .= substr($fullRS[$i]["ba1"],0,60).$strCsvSeparator; // complement of beneficiary's address(2)
		$strCsvOutput .= substr($fullRS[$i]["bs"],0,3).$strCsvSeparator; // beneficiary state
		$strCsvOutput .= substr($fullRS[$i]["bz"],0,10).$strCsvSeparator; // bene zip
		$strCsvOutput .= substr($benCountry,0,2).$strCsvSeparator; // bene country
		
		/*Bank fields*/
		$strCsvOutput .= substr($benBankCode,0,10).$strCsvSeparator; //BANK_CODE Bank number 
		$strCsvOutput .= substr($fullRS[$i]["bankName"],0,40).$strCsvSeparator; // Bank name
		$strCsvOutput .= substr($fullRS[$i]["branchCode"],0,30).$strCsvSeparator; // Branch number/code
		$strCsvOutput .= substr($fullRS[$i]["accNo"],0,30).$strCsvSeparator; // Account number
		$strCsvOutput .= ($fullRS[$i]["accountType"] == "Savings"?"SA":"CH").$strCsvSeparator; // Account type (SA:savings, CH:checking)
		/*Transaction fields*/
		$strCsvOutput .= substr($fullRS[$i]["currencyFrom"],0,3).$strCsvSeparator; // ISO code of the local currency
		$strCsvOutput .= substr(number_format($fullRS[$i]["transAmount"], 2, ".",""),0,10).$strCsvSeparator; // Amount due in local currency (2 decimal places)
		$strCsvOutput .= substr(number_format($fullRS[$i]["exchangeRate"], 3, ".",""),0,10).$strCsvSeparator; // xchange rate (3 decimal places)
		$strCsvOutput .= substr($fullRS[$i]["currencyTo"],0,3).$strCsvSeparator; // ISO code of the foreign currency	
		$strCsvOutput .= substr(number_format($fullRS[$i]["localAmount"], 2, ".",""),0,10).$strCsvSeparator; // AConverted amount in foreign currency (2 decimal places)
		$strCsvOutput .= substr($fullRS[$i]["remarks"],0,50).$strCsvSeparator; // msg from sender to beneficiary
			
		//$strCsvOutput .= substr(date("h:i:s",strtotime($fullRS[$i]["transDate"])),0,20).$strCsvSeparator; // time
		//$strCsvOutput .= substr($fullRS[$i]["faxNumber"],0,9).$strCsvSeparator; // dispatch number 
		//$strCsvOutput .= substr($fullRS[$i]["refNumberIM"],0,9).$strCsvSeparator; // updated to Transaction number
		//$strCsvOutput .= substr($intReferenceNumberOnlyNumeric,0,9).$strCsvSeparator; // updated to Transaction Number  only numeric part as in #4107
		/*sender fields*/
		/*$strCsvOutput .= trim(substr($senderFullName,0,40)).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["Address"]." ".$fullRS[$i]["Address1"],0,70).$strCsvSeparator; // sender address
		$strCsvOutput .= substr($fullRS[$i]["state"],0,2).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["zip"],0,15).$strCsvSeparator;
		$strCsvOutput .= substr($fullRS[$i]["country"],0,18).$strCsvSeparator;*/
		//$strCsvOutput .= substr(number_format($fullRS[$i]["localAmount"], 2, ".",""),0,13).$strCsvSeparator; // local amount 
		//$strCsvOutput .= substr("L",0,1).$strCsvSeparator; //Currency to use: D = Dollar, L = Local (for now only L) L by default
		//$strCsvOutput .= $charTransTypeShortCode.$strCsvSeparator; // transaction type
		//$strCsvOutput .= substr($fullRS[$i]["refNumber"],0,20).$strCsvSeparator;   //Remitance internal codeRemitance internal code -> Minas Center Code
		//$strCsvOutput .= $strPaymentType.$strCsvSeparator; // payment type
		//$strCsvOutput .= substr("",0,25); // folio number
		
		$strCsvOutput .= "\r\n";
	}
	
	echo $strCsvOutput;
	/* End of csv version */
//}
?>