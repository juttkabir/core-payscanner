<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//$agentType = getAgentType();
///////////////////////History is maintained via method named 'activities'
/*	#4949
	Paypoint Transaction Service functionality.
	by Aslam Shahid.
*/

/*
	This is called on Ajax call to load Caegoris for selected Paypoint Service.
*/
if(isset($_REQUEST["sID"])){
	if(!empty($_REQUEST["sID"])){
		$strQuerySer = "SELECT serviceName FROM  servicesPaypoint where serviceID = '".$_REQUEST["sID"]."'";
		$strQuerySerR = selectFrom($strQuerySer);
		
		$strQueryCat = "SELECT categoryID,categoryName FROM  categoriesPaypoint where serviceID = '".$_REQUEST["sID"]."'  ORDER BY categoryName";
		$strQueryCatR = selectMultiRecords($strQueryCat);
		
		$strCAT = "<td align='right' valign='top' bgcolor='#dfe6cc'>Selected Categories for Paypoint Service <strong>".$strQuerySerR["serviceName"]."</strong></td>";
		$strCAT .="<td bgcolor='#dfe6cc' valign='top'>";
		$strCAT .="<select name='selectedCategories[]' id='selectedCategories' multiple='multiple' size='5'>";
		if(count($strQueryCatR)>0){
			for($cr=0;$cr<count($strQueryCatR);$cr++){
				$strCAT .= "<option  value ='".$strQueryCatR[$cr]["categoryID"]."'> ".$strQueryCatR[$cr]["categoryName"]."</option>";
			}
			$strCAT .= "</select>&nbsp;&nbsp;";
			$strCAT .= "<input type='submit' name='removeCategoryBtn' id='removeCategoryBtn' value='Remove Selected Categories' onClick='return checkService(selectedCategories);'>	</td>";
		}
		else{
		$strCAT = "<td align='center' valign='top' bgcolor='#999999' colspan='2'>
			<strong><font size='2' color='#990000'>No categories defined for Selected Paypoint Service ".$strQuerySerR["serviceName"]."</strong></font></td>";
		}
	}else{
		$strCAT = "<td align='center' valign='top' bgcolor='#999999' colspan='2'>&nbsp;</td>";
	}
	echo $strCAT;
	exit;
}
// Add new Paypoint Service
if($_POST["addNewServiceBtn"] == "Add New Paypoint Service"){
	$newServiceV = trim(strtoupper($_POST["newService"]));
	$getNewServiceQ = "select serviceID from servicesPaypoint where upper(serviceName) ='".$newServiceV."'";
	$getNewServiceR = selectFrom($getNewServiceQ);
	if($getNewServiceR["serviceID"] != ""){
		$msg = " The Service '".ucwords($newServiceV)."' already exists.";
	}else{
		
		insertInto("insert into servicesPaypoint (serviceName,createdBy,created_at) VALUES
		('".$newServiceV."','".$username."','".$created_at."')");
		$descript = "New Paypoint Service Added";
		$insertLastIdRS = selectFrom("SELECT LAST_INSERT_ID() as li");
		$serviceIdds = $insertLastIdRS["li"];
		activities($_SESSION["loginHistoryID"],"INSERTION",$serviceIdds,"servicesPaypoint",$descript);
		$msg = SUCCESS_MARK ." Paypoint Service has been added successfully.";
	}
}
// Add new Paypoint Service Category like For Bills -> Electricity Bill,Gas Bill etc..
if($_POST["addNewCategoryBtn"] == "Add New Category"){
	$newCategoryV = trim(strtoupper($_POST["newCategory"]));
	$useServiceV = $_POST["useService"];
	$commissionV = $_POST["commission"];
	$getNewServiceQ = "select categoryID from categoriesPaypoint where upper(categoryName) ='".$newCategoryV."'";
	$getNewServiceR = selectFrom($getNewServiceQ);
	if($getNewServiceR["categoryID"] != ""){
		$msg = " The Service '".ucwords($newCategoryV)."' already exists.";
	}else{
		
		insertInto("insert into categoriesPaypoint (serviceID,categoryName,commission,createdBy,created_at) VALUES('".$useServiceV."','".$newCategoryV."','".$commissionV."','".$username."','".$created_at."')");
		$descript = "New Paypoint Category Added";
		$insertLastIdRS = selectFrom("SELECT LAST_INSERT_ID() as li");
		$serviceIdds = $insertLastIdRS["li"];
		activities($_SESSION["loginHistoryID"],"INSERTION",$serviceIdds,"categoriesPaypoint",$descript);
		$msg = SUCCESS_MARK ." Paypoint Service Category has been added successfully.";
	}
}
// Removing selected Paypoint Service
if($_POST["removeServiceBtn"] == "Remove Selected Service"){
	$selectedServicesV= $_POST["useService"];
	update("delete from servicesPaypoint where serviceID ='".$selectedServicesV."'");
	update("delete from categoriesPaypoint where serviceID ='".$selectedServicesV."'");
	$msg = "Selected Service and its Categories deleted successfully.";
	
}
// Remove selected categories.
if($_POST["removeCategoryBtn"] == "Remove Selected Categories" && is_array($_POST["selectedCategories"])){
	$selectedCategoriesArr = $_POST["selectedCategories"];
	$getCatValuesArr = implode(",",$selectedCategoriesArr);
	if($getCatValuesArr!=""){
		update("delete from categoriesPaypoint where categoryID IN(".$getCatValuesArr.")");
		$msg = "Selected Categories deleted successfully.";
	}
}
?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript" src="jquery.js"></script>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

function checkService(obj){
	var ServiceV = obj.value;
	if(obj.id=="newService" && (ServiceV=="" || IsAllSpaces(ServiceV))){
        alert("Please Provide New Paypoint Service Name.");
		obj.focus();
		return false;
    }
	if(obj.id=="newCategory" && (ServiceV=="" || IsAllSpaces(ServiceV))){
        alert("Please Provide New Paypoint Category Name.");
		obj.focus();
		return false;
    }
	if(obj.id=="newCategory" && document.getElementById("useService")!=null){
		if(document.getElementById("useService").value==""){
			alert("Please select Paypoint Service to assign New Category to that.");
			document.getElementById("useService").focus();
			return false;
		}
	}
	if((obj.id=="useService" || obj.id=="selectedCategories") && ServiceV!=""){
		obj.focus();
		var extranInfo="";
		if(obj.id=="useService"){
			extranInfo = "This will Remove its Categories Also. "
		}
		return confirm(extranInfo+"Are you sure to Remove ?");
	}
	if((obj.id=="useService" || obj.id=="selectedCategories") && ServiceV==""){
		obj.focus();
		alert("Please select Option(s) to remove.");
		return false;
	}
}
$(document).ready(function(){
	$("#loading").ajaxStart(function(){
		$(this).show();
		$("#selectedCurrRow").html('<td colspan="2" align="center" bgcolor="#999999" height="80" valign="middle"><strong>Loading Currencies Please Wait....</strong></td>');
	});
	$("#loading").ajaxComplete(function(request, settings){
		$(this).hide();
	});
});
function selectRespectiveCat(theForm,ssId){
	//var selectedOpt = getChoices(theForm);
	//var selectedOptArr = selectedOpt.split(",");
	//var ssId = document.getElementById("selectedServices").value;
	var ssIdQS = '?sID='+ssId;

	if(ssId!=""){
		$("#selectedCurrRow").load("add-services-paypoint.php"+ssIdQS);
	}
}

function getChoices(theForm) 
{ 
  var values, options, option, k=0; 
  values  = new Array(); 
  options = theForm.selectedCategories.options; 
  while(option=options[k++]) 
    if(option.selected) 
      values[values.length] = option.value; 
  return values.join(','); 
} 

</script>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex">Add/Update Paypoint Services</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Add/Update Paypoint Services </legend>
    <br>
			<form name="addComplaint" action="add-services-paypoint.php" method="post">
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
            <td width="37%" colspan="2" class="tab-r"><? echo $msg?><br></td>
			    </tr>
			  <?
			  }
			  
			  ?>
		    <tr>
			 	<td colspan="2" align="right" bgcolor="#dfe6cc">&nbsp;</td>
			    </tr>
			    
			  <tr>
				<td width="37%" align="right" valign="top" bgcolor="#dfe6cc">Paypoint Service</td>
			    <td width="59%" bgcolor="#dfe6cc">
<? 
	$contentsService = selectMultiRecords("select serviceID,serviceName from servicesPaypoint order by serviceName");	
	$serviceIdFirst = $contentsService[0]["serviceID"];
	$serviceNameFirst = $contentsService[0]["serviceName"];
?>
<select name="useService" id="useService" onChange="selectRespectiveCat(addComplaint,this.value);">
	<option value=""> Select Service </option>
<? for($sA=0;$sA<count($contentsService);$sA++){?>
	<option value="<?=$contentsService[$sA]["serviceID"]?>"><?=$contentsService[$sA]["serviceName"]?></option>
<? }?>
</select>
<input type="submit" name="removeServiceBtn" id="removeServiceBtn" value="Remove Selected Service"  onClick="return checkService(useService)"></td>
			  </tr>
				<tr>
					<td colspan="2" align="right" valign="top" bgcolor="#dfe6cc">&nbsp;</td>
					<tr>
	<td valign="top" bgcolor="#dfe6cc" align="right">Add New Paypoint Service </td>
	<td valign="top" bgcolor="#dfe6cc" align="left"><input type="text" name="newService" id="newService" value="" size="30">
		&nbsp;&nbsp;&nbsp;
		<input type="submit" name="addNewServiceBtn" id="addNewServiceBtn" value="Add New Paypoint Service" onClick="return checkService(newService)">	</td>
					<tr>
						<td colspan="2" align="right" valign="top" bgcolor="#dfe6cc">&nbsp;</td>
						</tr>
					<tr>
		<td valign="top" bgcolor="#dfe6cc" align="right">Add New Category for Selected Paypoint Service </td>
		<td valign="top" bgcolor="#dfe6cc" align="left">
			<input type="text" name="newCategory" id="newCategory" value="" size="30">&nbsp;&nbsp;&nbsp;
			<input type="submit" name="addNewCategoryBtn" id="addNewCategoryBtn" value="Add New Category" onClick="return checkService(newCategory)">		</td>
	</tr>
	<tr>
		  <td align="right" valign="top" bgcolor="#dfe6cc">&nbsp;</td>
		<td bgcolor="#dfe6cc">&nbsp;</td>
	 </tr>
	<tr id="selectedCurrRow">
<? 
	$strQueryCat = "SELECT categoryID,categoryName FROM  categoriesPaypoint where serviceID = '".$serviceIdFirst."'  ORDER BY categoryName";
	$strQueryCatR = selectMultiRecords($strQueryCat);
	$showMsgCur = "No categories defined for Selected Paypoint Service ".$serviceNameFirst;
	if($strQueryCatR[0]["categoryID"]!=""){ 
	?>
	
			  <td align="right" valign="top" bgcolor="#dfe6cc">Selected Categories for Paypoint Service <strong><?=$serviceNameFirst?></strong></td>
			    <td bgcolor="#dfe6cc" valign="top">
				<select name="selectedCategories[]" id="selectedCategories" multiple="multiple" size="5">
					  <?
					  for($cr=0;$cr<count($strQueryCatR);$cr++){
							echo "<option  value ='".$strQueryCatR[$cr]["categoryID"]."'> ".$strQueryCatR[$cr]["categoryName"]."</option>";
					  }
					  ?>				
				</select>
				&nbsp;&nbsp;
				<input type="submit" name="removeCategoryBtn" id="removeCategoryBtn" value="Remove Selected Categories" onClick="return checkService(selectedCategories);">
			</td>
<? }else{?>
				  <td align="center" valign="top" bgcolor="#999999" colspan="2"><font size="2" color="#990000"><strong>&nbsp;</strong></font></td>
<? }?>
	</tr>
			</table>
			</form>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>