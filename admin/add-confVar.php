<?
session_start();
include ("../include/config.php");
include ("security.php");

if ($_POST['Submit'] == "Save") {
	
	$qryIns = "INSERT INTO `config` (`varName`, `value`, `oldValue`, `description`, `detailDesc`, `isFlag`, `client`)
						VALUES ('" . trim($_POST['varName']) . "', '" . trim($_POST['value']) . "', '" . trim($_POST['oldValue']) . "', '" . trim($_POST['description']) . "', '" . trim($_POST['detailDesc']) . "', '" . $_POST['isFlag'] . "', '" . CONFIG_CLIENT_NAME . "')";
	if ( insertInto($qryIns) ) {
		insertError("Record saved successfully.");
		redirect("add-confVar.php?msg=Y&success=Y");
	} else {
		insertError("Record is not saved.");
		redirect("add-confVar.php?msg=Y");
	}
}

?>
<html>
<head>
	<title>Add Config Variable</title>
<script language="javascript" src="./javascript/functions.js"></script>
	
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function checkForm() {
	
	if (document.getElementById('varName').value == "" || IsAllSpaces(document.getElementById('varName').value)) {
  	alert("Please provide variable name.");
    document.getElementById('varName').focus();
    return false;
  }
	if (document.getElementById('value').value == "" || IsAllSpaces(document.getElementById('value').value)) {
  	alert("Please provide value.");
    document.getElementById('value').focus();
    return false;
  }
	if (document.getElementById('oldValue').value == "" || IsAllSpaces(document.getElementById('oldValue').value)) {
  	alert("Please provide default value.");
    document.getElementById('oldValue').focus();
    return false;
  }
	if (document.getElementById('description').value == "" || IsAllSpaces(document.getElementById('description').value)) {
  	alert("Please provide small description for the variable.");
    document.getElementById('description').focus();
    return false;
  }
	
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <form action="add-confVar.php" method="post" onSubmit="return checkForm();" name="addVariable" id="addVariable">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add Config Variable</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != "" && isset($_SESSION["error"])){ ?>
		  <tr bgcolor="EEEEEE"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td></tr></table></td></tr>
    <? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2"><a href="config.php" class="style2">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font></td>
		</tr>
				<tr bgcolor="#ededed">
					<td width="200" align="right"><font color="#005b90"><strong>Variable Name<font color="#ff0000">*</font></strong></font></td>
					<td width="300"><input name="varName" type="text" id="varName"></td>
				</tr>
				<tr bgcolor="#ededed">
					<td align="right"><font color="#005b90"><strong>Value<font color="#ff0000">*</font></strong></font></td>
					<td><input name="value" type="text" id="value"></td>
				</tr>
				<tr bgcolor="#ededed">
					<td align="right"><font color="#005b90"><strong>Default Value<font color="#ff0000">*</font></strong></font></td>
					<td><input name="oldValue" type="text" id="oldValue"></td>
				</tr>
				<tr bgcolor="#ededed">
					<td align="right"><font color="#005b90"><strong>Description<font color="#ff0000">*</font></strong></font></td>
					<td><input name="description" type="text" id="description"></td>
				</tr>
				<tr bgcolor="#ededed">
					<td align="right"><font color="#005b90"><strong>Detail Description</strong></font></td>
					<td><textarea name="detailDesc" id="detailDesc" rows="10" cols="60"></textarea></td>
				</tr>
				<tr bgcolor="#ededed">
					<td align="right"><font color="#005b90"><strong>Is Flag</strong></font></td>
					<td>
						<select name="isFlag" id="isFlag">
							<option value="N">No</option>
							<option value="Y">Yes</option>
						</select>	
					</td>
				</tr>
        
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="Submit" value="Save">&nbsp;&nbsp; <input type="reset" value="Clear">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>