<?

/* This file created by Naiz Ahmad @4513 at 20-02-2009 */
$date_time = date('Y-m-d H:i:s');
if($_POST["Submit"] != "")
{


	if ($_FILES["csvFile"]["name"]=='none')
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	}
	$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));
	if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" || $ext == ".del" )
	{
		if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
		{
		   	/*define("DATABASE_COMPLIANCE","dev_compliance");
			$compliance = mysql_connect(SERVER_MASTER, USER, PASSWORD);
		    if(!mysql_select_db(DATABASE_COMPLIANCE, $compliance))
			die("Could not connect to ".DATABASE_COMPLIANCE." database.");*/
		  //$listID = selectFrom("select list_id,list_name from compliance_lists where  list_name = 'HM Treasury'");
		  
		 $listID = selectFrom("select listID,listName,Description from ".TBL_COMPLIANCE_LIST." where  listName = 'HM Treasury'"); 
		  
			$filename = $_FILES["csvFile"]["tmp_name"];
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('CP1251');
			$data->read($filename);
			error_reporting(E_ALL ^ E_NOTICE);
			$counterInsert = 0;
			$counterUpdate = 0;
		  
		  	function useDate($strDate){
					$arrDate= explode("/",$strDate);
						if(count($arrDate) > 0 )
							{
							  $day   = $arrDate[0];
							  $month = $arrDate[1];
							  $year  = $arrDate[2];
							}
						$return_date = $year."-".$month."-".$day;	
						return 	$return_date;
				}
					
			for ($i = 3; $i <= $data->sheets[0]['numRows']; $i++)
			{ 
				$data1[1]   = addslashes($data->sheets[0]['cells'][$i][1]);  //name6
				$data1[2]   = addslashes($data->sheets[0]['cells'][$i][2]);  //name1
				$data1[3]   = addslashes($data->sheets[0]['cells'][$i][3]);  //name2
				$data1[4]   = addslashes($data->sheets[0]['cells'][$i][4]);	//name3
				$data1[5]   = addslashes($data->sheets[0]['cells'][$i][5]);	//name4
				$data1[6]   = addslashes($data->sheets[0]['cells'][$i][6]);	//name5
				$data1[7]   = addslashes($data->sheets[0]['cells'][$i][7]);	//title
				$data1[8]   = addslashes($data->sheets[0]['cells'][$i][8]);	//dob
				$data1[9]   = addslashes($data->sheets[0]['cells'][$i][9]);	//Town of Birth
				$data1[10]  = addslashes($data->sheets[0]['cells'][$i][10]);//Country of Birth
				$data1[11]  = addslashes($data->sheets[0]['cells'][$i][11]);//Nationality
				$data1[12]  = addslashes($data->sheets[0]['cells'][$i][12]);//Passport Details
				$data1[13]  = addslashes($data->sheets[0]['cells'][$i][13]);//NI Number
				$data1[14]  = addslashes($data->sheets[0]['cells'][$i][14]);//Position
				$data1[15]  = addslashes($data->sheets[0]['cells'][$i][15]);//Address1
				$data1[16]  = addslashes($data->sheets[0]['cells'][$i][16]);//Address2
				$data1[17]  = addslashes($data->sheets[0]['cells'][$i][17]);//Address3
				$data1[18]  = addslashes($data->sheets[0]['cells'][$i][18]);//Address4
				$data1[19]  = addslashes($data->sheets[0]['cells'][$i][19]);//Address5
				$data1[20]  = addslashes($data->sheets[0]['cells'][$i][20]);//Address6
				$data1[21]  = addslashes($data->sheets[0]['cells'][$i][21]);//Zip
				$data1[22]  = addslashes($data->sheets[0]['cells'][$i][22]);//Country
				$data1[23]  = addslashes($data->sheets[0]['cells'][$i][23]);//Other Information
				$data1[24]  = addslashes($data->sheets[0]['cells'][$i][24]);//Group Type
				$data1[25]  = addslashes($data->sheets[0]['cells'][$i][25]);//Alias Type
				$data1[26]  = addslashes($data->sheets[0]['cells'][$i][26]);//Regime
				$data1[27]  = addslashes($data->sheets[0]['cells'][$i][27]);//Listed On
				$data1[28]  = addslashes($data->sheets[0]['cells'][$i][28]);//Last Updated
				$data1[29]  = addslashes($data->sheets[0]['cells'][$i][29]);//Group ID
				 
				if(!empty($data1[8]))	
				   $data1[8] = useDate($data1[8]);
				if(!empty($data1[27]))	
				   $data1[27] = useDate($data1[27]);
				if(!empty($data1[28]))	
				   $data1[28] = useDate($data1[28]);  
											
				if(empty($data1[8])) // DOB
					$data1[8] ="0000-00-00";
					
				if( empty($data1[27]))  // Listed On
					$data1[27] ="0000-00-00"; // LAst Updated
					
				if(empty($data1[28]))
					$data1[28] ="0000-00-00";
				
				if(!empty($data1[1]))
					$full_name = $data1[1];
				if(!empty($data1[2]))
					$full_name .= ",".$data1[2];		
				if(!empty($data1[3]))
					$full_name .= ",".$data1[3];
				if(!empty($data1[4]))
					$full_name .= ",".$data1[4];	
				if(!empty($data1[5]))
					$full_name .= ",".$data1[5];
				if(!empty($data1[6]))
					$full_name .= ",".$data1[6];	
						
				//debug($full_name);
			  
			    if(!empty($data1[15]))
					$full_address = $data1[15];
				if(!empty($data1[16]))
					$full_address .= ",".$data1[16];		
				if(!empty($data1[17]))
					$full_address .= ",".$data1[17];
				if(!empty($data1[18]))
					$full_address .= ",".$data1[18];	
				if(!empty($data1[19]))
					$full_address .= ",".$data1[19];
				if(!empty($data1[20]))
					$full_address .= ",".$data1[20];	
			  	
				//debug($full_address);
				//$full_name    =    $data1[1].",".$data1[2].",".$data1[3].",".$data1[4].",".$data1[5].",".$data1[6];
				//$full_address =    $data1[15].",".$data1[16].",".$data1[17].",".$data1[18].",".$data1[19].",".$data1[20];
													
				$strExistingRecord = selectFrom("
										select 
											hm_id
										from 
											compliance_hm_treasury 
										where 
					                       name6           = '".$data1[1] ."' AND 
										   name1           = '".$data1[2] ."' AND 
										   name2           = '".$data1[3] ."' AND 
										   name3           = '".$data1[4] ."' AND 
										   name4           = '".$data1[5] ."' AND
										   name5           = '".$data1[6] ."' AND
										   dob1            = '".$data1[8] ."' AND
										   dob_town        = '".$data1[9] ."' AND
										   dob_country     = '".$data1[10] ."' AND
										   nationality1    = '".$data1[11] ."' AND
										   passport_detail = '".$data1[12] ."' AND
										   id_no           = '".$data1[13] ."' AND 
										   job_title       = '".$data1[14] ."' AND 
										   address1        = '".$data1[15] ."' AND
										   address2        = '".$data1[16] ."' AND
										   address3        = '".$data1[17] ."' AND
										   address4        = '".$data1[18] ."' AND
										   address5        = '".$data1[19] ."' AND
										   address6        = '".$data1[20] ."' AND
										   zip_code1       = '".$data1[21] ."' AND  
										   country1        = '".$data1[22] ."' AND 
										   other_details   = '".$data1[23] ."' AND 
										   list_id         = '".$listID["listID"] ."' AND 
										   group_type      = '".$data1[24] ."' AND 
										   alias_type      = '".$data1[25] ."' AND 
										   regime          = '".$data1[26] ."' AND 
										   listed_on       = '".$data1[27] ."' AND 
										   last_updated    = '".$data1[28] ."' AND
										   group_id        = '".$data1[29] ."'  
										   
									 ");
									 
					//debug($strExistingRecord);
					if($strExistingRecord["hm_id"] !='' ){
						
						 $strUpdatHMSql= "update
						 				 compliance_hm_treasury 
									 set 
									 	 dated       = '".$date_time."'
									 where 
									 	hm_id='".$strExistingRecord["hm_id"]."' ";
				
					update($strUpdatHMSql);
					$counterUpdate ++;
					//debug($strUpdatHMSql);
					$msgUpdate = " $counterUpdate HM Treasury Records are Updated Successfully ";
						}else{
					
				    $strInsertSql = " INSERT INTO 
											compliance_hm_treasury
				   						(
											name6,
											name1,
											name2,
											name3,
											name4,
											name5,
											full_name,
											title,
											dob1,
											dob_town,
											dob_country,
											nationality1,
											passport_detail,
											id_no,
											job_title,
											address1,
											address2,
											address3,
											address4,
											address5,
											address6,
											full_address,
											zip_code1,
											country1,
											other_details,
											group_type,
											alias_type,
											regime,
											listed_on,
											last_updated,
											group_id,
											list_id,
											dated
							   		   ) 
									VALUES 
				  					 	(
									 		'". $data1[1] ."',
											'". $data1[2] ."',
											'". $data1[3] ."',
											'". $data1[4] ."',
				   							'". $data1[5] ."',
											'". $data1[6] ."',
											'". $full_name ."',
											'". $data1[7] ."',
											'". $data1[8] ."',
											'". $data1[9] ."',
											'". $data1[10] ."',
											'". $data1[11] ."',
				   							'". $data1[12] ."',
											'". $data1[13] ."',
											'". $data1[14] ."',
											'". $data1[15] ."',
											'". $data1[16] ."',
				   							'". $data1[17] ."',
											'". $data1[18] ."',
											'". $data1[19] ."',
											'". $data1[20] ."',
											'". $full_address ."',
											'". $data1[21] ."',
											'". $data1[22] ."',
				   							'". $data1[23] ."',
											'". $data1[24] ."',
											'". $data1[25] ."',
											'". $data1[26] ."',
											'". $data1[27] ."',
											'". $data1[28] ."',
											'". $data1[29] ."',
											'".$listID["listID"]."',
											'".$date_time."'
										)";
				     
				 
			     insertInto($strInsertSql);
				 $counterInsert ++;
				// debug($strInsertSql);
				$msgInsert = " $counterInsert HM Treasury Records are Imported Successfully ";
			  }
			
	    } //end  for loop
		$msg = $msgInsert." ".$msgUpdate;  
		//mysql_close($compliance);	
	}
		else
		{
			$msg = CAUTION_MARK . " Your file is not uploaded due to some error.";
		}
	}
	else
	{
		$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
	}	
}


?>
