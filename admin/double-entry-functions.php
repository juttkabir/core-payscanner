<?php


function updateLedger($_arrInput)
{
	//debug_print_backtrace(); 
	//debug($_arrInput);
	/*
	Action
	MANUAL_DEPOSIT = Manual Deposit by Agent 
	*/
 
	if($_arrInput["action"] == 'MANUAL_DEPOSIT')
		$response = manualDeposit($_arrInput);
	
	if($_arrInput["action"] == 'CREATE_TRANSACTION')
	{
		$response = createTrans($_arrInput);	
		//	$responseComm = feeLedger($_arrInput);			
	} 
	
	if($_arrInput["action"] == 'CANCELLED_TRANSACTION')
	{
		$response = transCancel($_arrInput);	
	} 
	
	
	if($_arrInput["action"] == 'AMENDED_TRANSACTION')
	{
		$response = transAmended($_arrInput);
		/*$responseComm = feeLedger($_arrInput);*/
	} 
	
	if($_arrInput["action"] == 'GET_MAX_DATE_CLOSING_BALANCE')
	{
		$response = getClosingBalance($_arrInput);
		/*$responseComm = feeLedger($_arrInput);*/
	} 

	//debug($_arrInput,true);
}

function manualDeposit($arguments)
{
	//debug_print_backtrace();	
	//debug($arguments);
	$moneyPaid = $arguments["moneyPaid"];
	$accountNumber = $arguments["accountNumber"];
	$crAmount = '';
	$drAmount ='';
	$crAccount = '';
	$drAccount = '';
	$type = '';
	
 if($accountNumber != '')
 {
			if($arguments["type"] == 'DEPOSIT'){
				$drAmount = $arguments["amount"];
				$drAccount = $accountNumber;  
				$type = 'Dr';
			}else{
				$crAmount = $arguments["amount"];
				$crAccount = $accountNumber; 
				$type = 'Cr';
				}
					
			$strSql = "INSERT INTO account_ledgers
							(
							userID,
							crAmount,
							drAmount,
							crAccount,
							drAccount,
							currency,
							description,
							created,
							createdBy,
							tblName,
							fid
							)
							VALUES
							(
							'".$arguments["userID"]."',
							'".$crAmount."',
							'".$drAmount."',
							'".$crAccount."',
							'".$drAccount."',
							'".$arguments["currency"]."',
							'".$arguments["description"]."',
							'".$arguments["created"]."',
							'".$arguments["createdBy"]."',
							'".$arguments["tableName"]."',
							'".$arguments["id"]."'
							)";
			//debug($strSql);
			$execute = insertInto($strSql);
			
		  /* update account sumamry balance */
		  $_arrInput = array(
			  "accountNumber" => $accountNumber,
			  "currency" => $arguments["currency"],
			  "amount" =>$arguments["amount"],
			  "type" => $type
		  );
			ledgerSummary($_arrInput);
		}
return $execute;
}

function createTrans($arguments)
{
	//debug_print_backtrace();	
	//debug($arguments);
	$currencyTo  = $arguments["currencyTo"];
	$currencyFrom  = $arguments["currencyFrom"];
	$commission = $arguments["commission"];
	$crAmount = '';
	$drAmount ='';
	$crAccount = '';
	$drAccount = '';
	$status = '';
	$type = '';
	$execute = false;
	
	/* Start
	 When transaction created then following action will be performed
	 1- Local Amount will be credited in Currency a/c 
	 2- Agent account will be debited with total amount (this work already done )
	 3- fee will be debited in Commission
	 4- second entry of fee go in agent a/c. logic remain same just split the total amount in two parts transAmount + Fee = totalAmount 
	*/
	
	/* Insert entry in currency ledger Start*/
	if(!empty($currencyTo))
		 $strAc = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$currencyTo."'");
	
	if($arguments["action"] == 'CREATE_TRANSACTION')
	{
		$crAmount = $arguments["amount"];
		$crAccount = $strAc["accounNumber"];
		$type = 'Cr';
	}
			
	if($strAc["id"] != '')
	{
		$strSql = "INSERT INTO account_ledgers
					(
						userID,
						transID,
						crAmount,
						drAmount,
						crAccount,
						drAccount,
						currency,
						description,
						status,
						created,
						createdBy
					)
					VALUES
					(
						'".$arguments["userID"]."',
						'".$arguments["transID"]."',
						'".$crAmount."',
						'".$drAmount."',
						'".$crAccount."',
						'".$drAccount."',
						'".$arguments["currencyTo"]."',
						'".$arguments["description"]."',
						'".$status."',
						'".$arguments["created"]."',
						'".$arguments["createdBy"]."'
					)";

		//debug($strSql);
		$execute = insertInto($strSql);
		
		/* update account sumamry balance */
		$_arrInput = array(
			"accountNumber" => $crAccount,
			"currency" => $arguments["currencyTo"],
			"amount" =>$crAmount,
			"type" => $type
		);

		ledgerSummary($_arrInput);
	}	
	
	/*Insert entry in currency ledger end  */	
	
	/*insert entry in commission  a/c start*/
	
	/*
	chart of account
	CORRESPONDENTS/ LOCAL BANKS = 1001
	Commission / Charges Account = 450
	*/
	/*$bankChartAccount = '1001';*/
	$commChartAccount = '420';

	/*if(!empty($currencyFrom))
		 $strBank = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$currencyFrom."' AND accounType = '".$bankChartAccount."'");*/
		 
	if(!empty($commission))	 
		 $strFee = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$currencyFrom."' AND accounType='".$commChartAccount."'");	

	if($arguments["action"] == 'CREATE_TRANSACTION')
	{
		$crAmount = '';
		//$drAmount = '';
		$crAccount = '';
		$type = '';
		//$drAccount = '';
		$crAmount = $arguments["commission"];
		//$drAmount = $arguments["commission"];
		$crAccount = $strFee["accounNumber"];
		$type = 'Cr';
		//$drAccount = $strBank["accounNumber"];
		$dated = $arguments["created"];
		
	   if($strFee["id"] != '')
	   {
		$strSql = "INSERT INTO account_ledgers
					   (
						userID,
						transID,
						crAmount,
						crAccount,
						currency,
						description,
						status, 
						created,
						createdBy
						)
					    VALUES
						(
						'".$arguments["userID"]."',
						'".$arguments["transID"]."',
						'".$crAmount."',
						'".$crAccount."',
						'".$arguments["currencyFrom"]."',
						'".$description."',
						'".$status."',
						'".$dated."',
						'".$arguments["createdBy"]."'
						)";
		//debug($strSql);
		$execute = insertInto($strSql);
		  /* update account sumamry balance */
		  $_arrInput = array(
			  "accountNumber" => $crAccount,
			  "amount" =>$crAmount,
			  "currency" => $arguments["currencyFrom"],
			  "type" => $type
		  );
		ledgerSummary($_arrInput);
	}		
 }
		
/* insert entry in commission and bank a/c end */
	
//debug($execute,true);	
return $execute;
}


function transAmended($arguments)
{
	//debug_print_backtrace();	
	$currencyTo  = $arguments["currencyTo"];
	$currencyFrom  = $arguments["currencyFrom"];
	$commission = $arguments["commission"];
	$transID = $arguments["transID"];
	$crAmount = '';
	$drAmount ='';
	$crAccount = '';
	$drAccount = '';
	$status = '';
	$currentdate = date("Y-m-d h:i:s");
	$createddBy = $_SESSION["loggedUserData"]["userID"];	
	$execute = false;
	
	/* Start First do revers Entry*/
	if(!empty($transID))
	{
	
	  $transData = selectFrom("SELECT transID,localAmount,custAgentID,currencyFrom,currencyTo,transDate FROM ". TBL_TRANSACTIONS." WHERE transID = '".$transID."'");
	  if(!empty($transData["currencyTo"]))
	   	 $strTransAc = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$transData["currencyTo"]."'");
		 
	if($arguments["action"] == 'AMENDED_TRANSACTION' &&  $strTransAc["id"] != '')
	{
		$drAmount = $transData["localAmount"];
		$drAccount = $strTransAc["accounNumber"];
		
		$strSql = "INSERT INTO account_ledgers
						(
						userID,
						transID,
						drAmount,
						drAccount,
						currency,
						description,
						status,
						created,
						updated
						)
					    VALUES
						(
						'".$transData["custAgentID"]."',
						'".$transData["transID"]."',
						'".$drAmount."',
						'".$drAccount."',
						'".$transData["currencyTo"]."',
						'".$arguments["description"]."',
						'E',
						'".$transData["transDate"]."',
						'".$currentdate."'
						)";
		//debug($strSql);
		$execute = insertInto($strSql);
	}	 
  }
	
	/*End First do revers Entry*/
	
	/* Start Insert New Entry in Ledger*/
	if(!empty($currencyTo))
		 $strAc = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$currencyTo."'");
	
	
	if($arguments["action"] == 'AMENDED_TRANSACTION' && $strAc["id"] != '')
	{
		$crAmount = $arguments["amount"];
		$crAccount = $strAc["accounNumber"];
		$des = "Transaction Amended";
	
		$strSql = "INSERT INTO account_ledgers
						(
						userID,
						transID,
						crAmount,
						crAccount,
						currency,
						description,
						created,
						createdBy
						)
					    VALUES
						(
						'".$arguments["userID"]."',
						'".$arguments["transID"]."',
						'".$crAmount."',
						'".$crAccount."',
						'".$arguments["currencyTo"]."',
						'".$des."',
						'".$currentdate."',
						'".$createddBy."'
						)";
		//debug($strSql);
		$execute = insertInto($strSql);
	}
	
/* End Insert New Entry in Ledger*/
//debug($execute,true);	
return $execute;
}

function transCancel($arguments)
{
//debug_print_backtrace();	
	
	$transID = $arguments["transID"];
	$commission = $arguments["commission"];
	$drAmount ='';
	$drAccount = '';
	$status = '';
	$type ='';
	$currentdate = date("Y-m-d h:i:s");
	$createddBy = $_SESSION["loggedUserData"]["userID"];	
	$execute = false;
	

	/* Start First do revers Entry in currency account*/
	if(!empty($transID))
	{
	
	$transData = selectFrom("SELECT transID,localAmount,custAgentID,currencyFrom,currencyTo,IMFee,transDate FROM ". TBL_TRANSACTIONS." WHERE transID = '".$transID."'");
	  
	  if(!empty($transData["currencyTo"]))
	   	 $strTransAc = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$transData["currencyTo"]."'");

		//debug("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$transData["currencyTo"]."'"); 
		
	if($arguments["action"] == 'CANCELLED_TRANSACTION' &&  $strTransAc["id"] != '')
	{
		$drAmount = $transData["localAmount"];
		$drAccount = $strTransAc["accounNumber"];
		$type = 'Dr';
		$strSql = "INSERT INTO account_ledgers
						(
						userID,
						transID,
						drAmount,
						drAccount,
						currency,
						description,
						status,
						created,
						updated
						)
					    VALUES
						(
						'".$transData["custAgentID"]."',
						'".$transData["transID"]."',
						'".$drAmount."',
						'".$drAccount."',
						'".$transData["currencyTo"]."',
						'".$arguments["description"]."',
						'C',
						'".$transData["transDate"]."',
						'".$currentdate."'
						)";
		//debug($strSql);
		$execute = insertInto($strSql);
		$_arrInput = array(
			  "accountNumber" =>$drAccount,
			  "amount" =>$drAmount,
			  "currency" => $transData["currencyTo"],
			  "type" => $type
		  );
		ledgerSummary($_arrInput);
	}	 

	/*End First do revers Entry*/
	
  /* Reverse Commission Start system do revers entry of commission in currency account*/
/*  if($arguments["refund"] == 'Y')
  {*/
   	/*
	chart of account
	CORRESPONDENTS/ LOCAL BANKS = 1001
	Commission / Charges Account = 450
	*/
	//$bankChartAccount = '1001';
	/* when create transaction in case of commission greater than 1 then commission a/c will be Cr and Bank a/c will Dr*/
	//if(!empty($currencyFrom))
		/* $strBank = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$transData["currencyFrom"]."' AND accounType = '".$bankChartAccount."'");*/
		
		$commChartAccount = '420';
	 
		 $strFee = selectFrom("SELECT id,accounNumber FROM accounts WHERE status = 'AC' AND transFlag = 'Y' AND currency ='".$transData["currencyFrom"]."' AND accounType='".$commChartAccount."'");
  
  if($strFee["id"] != '')
	{
		$type ='';
		$drAmount = $commission;
		$drAccount = $strFee["accounNumber"];
		$type = 'Dr';
		
		$strSql = "INSERT INTO account_ledgers
					   (
						userID,
						transID,
						drAmount,
						drAccount,
						currency,
						description,
						status, 
						created,
						updated
						)
					    VALUES
						(
						'".$transData["custAgentID"]."',
						'".$transData["transID"]."',
						'".$drAmount."',
						'".$drAccount."',
						'".$transData["currencyFrom"]."',
						'".$arguments["description"]."',
						'C',
						'".$transData["transDate"]."',
						'".$currentdate."'
						)";
		//debug($strSql);
		$execute = insertInto($strSql);
		$_arrInput = array(
			  "accountNumber" => $drAccount,
			  "amount" =>$drAccount,
			  "currency" => $transData["currencyFrom"],
			  "type" => $type
		  );
		ledgerSummary($_arrInput);
	}	
  
  //}
  
  /* Reverse Commission End*/
 }
}

function ledgerSummary($arguments)
{
//debug_print_backtrace();
$accountNumber = $arguments["accountNumber"];
$currency = $arguments["currency"];
$amount = $arguments["amount"];
$type = $arguments["type"];
$balance = 0;
$today = date("Y-m-d"); 
//debug($arguments);
/* Insert entry of opening balance for new created account Dr (+) and Cr (-)*/
		
		 $strStartSql		= "SELECT 
		 						id,
								accountNumber,
								opening_balance,
								closing_balance,
								currency 
							 FROM  
							 	".TBL_ACCOUNT_SUMMARY."
							 WHERE
							 	 accountNumber = '".$accountNumber."' AND
								 currency = '".$currency."'";
				
							$accountNew = selectFrom($strStartSql);
							//debug($strStartSql);	
							//debug($accountNew);
			if($accountNew["id"] == '')
			{
				
				$openingBalance = 0;
				/*if($accountsRS['balance']>0){
					if($accountsRS['balanceType']=='Dr')
						$openingBalance = -1*$accountsRS['balance']; // 100-500 = -400
					elseif($accountsRS['balanceType']=='Cr')
						$openingBalance = $accountsRS['balance']; // 100+500 = 600
				}*/
				if($accountNew['opening_balance'] != ''){
					if($type == 'Dr')
						$openingBalance = -1*$amount + $openingBalance;// -(-400) = 400
					else
						$openingBalance = $amount + $openingBalance;// 600
				}
				
				if($type == 'Dr'){
					$closingBalance = $openingBalance - $amount;
				}
				else{
					$closingBalance = $openingBalance + $amount;
				}
				$strNewInsert	= "INSERT INTO
						   			".TBL_ACCOUNT_SUMMARY."
									(
									
									  dated,
									  accountNumber,
									  currency,
									  opening_balance, 
									  closing_balance
									)
								VALUES
									(
									 '".$today."',
									 '".$accountNumber."',
									 '".$currency."',
									 '".$openingBalance."',
									 '".$closingBalance."'
									)";	
						   
						   //debug($strNewInsert);
					       insertInto($strNewInsert);	
			}
			else
			{
	/* if account arleady exit then system will pick today closing balance if met then update it*/						 
		 $strSql		= "SELECT 
		 						id,
								dated,
								opening_balance,
								closing_balance,
								accountNumber,
								currency 
							 FROM  
							 	".TBL_ACCOUNT_SUMMARY."
							 WHERE
							 	 accountNumber = '".$accountNumber."' AND
								 currency = '".$currency."' AND
								 dated = '".$today."' ";
							
							//debug($strSql);	 
							$accountContents = selectFrom($strSql);
		
		if($accountContents["id"] != "")
		{
			/* when  new account is created then first time amount is added or subtracted from opening balance after that updated closing balance amount */
			/*if($accountContents["closing_balance"] == 0)
				$balance = $accountContents["opening_balance"];
			else*/
				$balance = $accountContents["closing_balance"];
			
			if($type == 'Dr')
				$balance = $balance - $amount;	
			else
				$balance = $balance + $amount;	
				
			$strUpdate  = " UPDATE
								".TBL_ACCOUNT_SUMMARY."
								SET
								   closing_balance = '".$balance."',
								   currency = '".$currency."'
								WHERE
									id =  '".$accountContents["id"]."'";	
			//debug($strUpdate);
			update($strUpdate);
				
		}
		else
		{
		
		/* if today  balance not found then system will pick max date closing balance and assign to opening balance*/				
			$strLastSql		= "SELECT 
								Max(dated) as dated
								
							 FROM  
							 	".TBL_ACCOUNT_SUMMARY."
							 WHERE
							 	 accountNumber = '".$accountNumber."' AND
								 currency = '".$currency."' ORDER BY id DESC";

							//debug($strLastSql);	 
							$lastDate = selectFrom($strLastSql);
		  
		  if($strLastSql["dated"] !='')
		  {
		  $strSql		= "SELECT 
		 						id,
								dated,
								opening_balance,
								closing_balance,
								accountNumber,
								currency 
							 FROM  
							 	".TBL_ACCOUNT_SUMMARY."
							 WHERE
							 	 accountNumber = '".$accountNumber."' AND
								 currency = '".$currency."' AND
								 dated = '".$lastDate["dated"]."' ";
							
							//debug($strSql);	 
							$accountContents = selectFrom($strSql);		
							
							$balance = 0;
				
							$balance = $accountContents["closing_balance"];
									
							$openingBalance = $balance;
								
							if($type == 'Dr')
							{
								$closingBalance = $balance - $amount;	
								
							}else{
								$closingBalance = $balance + $amount;	
								
							}	
							
		$strInsertSql	= "INSERT INTO
						   			".TBL_ACCOUNT_SUMMARY."
									(
									  dated,
									  accountNumber,
									  currency,
									  opening_balance,
									  closing_balance
									)
								VALUES
									(
									 '".$today."',
									 '".$accountNumber."',
									 '".$currency."',
									 '".$openingBalance."',
									 '".$closingBalance."'
									)";	
						   
						   //debug($strInsertSql);
					       insertInto($strInsertSql);	
		
		}
     }
   }
}	
function getClosingBalance($arguments)
{

//debug_print_backtrace();	
$accountNumber = $arguments["accountNumber"];
$currency = $arguments["currency"];
$today = date("Y-m-d"); 

$closingBalance = selectFrom("SELECT 
									id,
									closing_balance,
									dated,
									accountNumber 
								FROM 
									".TBL_ACCOUNT_SUMMARY." 
								WHERE
									accountNumber = '".$accountNumber."' AND
									dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
									WHERE accountNumber = '".$accountNumber."')
								 ");
							$closingBalance = 	$closingBalance["closing_balance"]; 
return $closingBalance; 				
}
function calculateTradeProfitLoss($arguments){
	$returnComm = 0;
	$returnCommTotal = 0;
	if(is_array($arguments)){
		$tradeRate = $arguments["tradeRates"];
		$transRateArr = $arguments["transRates"];
		$transAmountArr = $arguments["transAmounts"];
		$transIDsArr = $arguments["transIDs"];
		for($i=0;$i<count($transAmountArr);$i++){
			$returnComm = ($tradeRate - $transRateArr[0]) * $transAmountArr[$i];
			$returnCommTotal += $returnComm;
			update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = '".$returnComm."' WHERE transID='".$transIDsArr[$i]."' ");
		}
		$returnComm = number_format($returnComm,"4",".","");
	}
	return $returnCommTotal;
}


function newAccount($arguments)
{
	//debug_print_backtrace();	
	//debug($arguments);
	$accountNumber = $arguments["accountNumber"];
	$crAmount = '';
	$drAmount ='';
	$crAccount = '';
	$drAccount = '';
	$type = $arguments["type"];
	
 if($accountNumber != '')
 {
			if($type == 'Dr'){
				$drAmount = $arguments["amount"];
				$drAccount = $accountNumber;  
				//$type = 'Dr';
			}else{
				$crAmount = $arguments["amount"];
				$crAccount = $accountNumber; 
				$type = 'Cr';
			}
					
			$strSql = "INSERT INTO account_ledgers
							(
							userID,
							crAmount,
							drAmount,
							crAccount,
							drAccount,
							currency,
							description,
							created,
							createdBy,
							tblName,
							fid
							)
							VALUES
							(
							'".$arguments["userID"]."',
							'".$crAmount."',
							'".$drAmount."',
							'".$crAccount."',
							'".$drAccount."',
							'".$arguments["currency"]."',
							'".$arguments["description"]."',
							'".$arguments["created"]."',
							'".$arguments["createdBy"]."',
							'".$arguments["tableName"]."',
							'".$arguments["id"]."'
							)";
			//debug($strSql);
			$execute = insertInto($strSql);
			
		  /* update account sumamry balance */
		  $_arrInput = array(
			  "accountNumber" => $accountNumber,
			  "currency" => $arguments["currency"],
			  "amount" => $arguments["amount"],
			  "type" => $type
		  );
			ledgerSummary($_arrInput);
		}
return $execute;
}

?>
