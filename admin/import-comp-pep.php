<?

/* This file created by Naiz Ahmad @4513 at 20-02-2009 */

$date_time = date('Y-m-d h:i:s');
if($_POST["Submit"] != "")
{


	if ($_FILES["csvFile"]["name"]=='none')
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	}
	$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));
	if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" || $ext == ".del" )
	{
		if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
		{
		   	define("DATABASE","dev_compliance");
			$compliance = mysql_connect(SERVER_MASTER, USER, PASSWORD);
		    if(!mysql_select_db(DATABASE, $compliance))
			die("Could not connect to ".DATABASE." database.");
			
		 	$listID = selectFrom("select listID,listName,Description from ".TBL_COMPLIANCE_LIST." where  listName = 'PEP'"); 
			
			$Pict="$username-" . date("Y-m-d") . $ext;
		
			$filename = $_FILES["csvFile"]["tmp_name"];

			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('CP1251');
			$data->read($filename);
			error_reporting(E_ALL ^ E_NOTICE);
			$counter = 0;
			//debug($data->sheets[0]['cells'],true);
			for ($i = 4; $i <= $data->sheets[0]['numRows']; $i++)
			{ 

		            $data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //sno
					$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //full_name*
					$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //position
					$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);	// portfolio*

	                 // To remove title from PEP list
					 
					   $title_array1 = array("Dr",
					   						"Dr.",
											"Mr",
											"Mr.",
											"Mrs",
											"Mrs.",
											"Prof",
											"Prof.",
											"Chief",
											"Alh",
											"Alh.",
											"Engr",
											"Engr.",
											"Senator",
											"Senator.",
											"Ambassador",
											"General",
											"Prince",
											"RTD"   
											);
					   	$title_array = array();
						// convert tilte in upper case
						foreach($title_array1 as $ta)
						{
							$title_array[] = strtoupper($ta);
						}
						
						$fullName = explode(" ",$data1[2]);	
						$strFullName = "";	
						
					  if(in_array(strtoupper($fullName[0]),$title_array)){
					     $strTitle = $fullName[0]; 
					     for($j=1; $j<count($fullName); $j++)
						 	$strFullName .= $fullName[$j]." ";
							
					  }
					   else
					   	$strFullName = $data1[2];
																				   					   
					 /// end tilte remove code			 
	 			 
					$pepData = selectFrom("select comp_PEPID,listID from compliancePEP where 
					                       fullName='".$strFullName ."' ");
					
					if($pepData["comp_PEPID"] !='' ){
						
						 $updatPep= "update compliancePEP set sno = '". $data1[1] ."', fullName =  '". $strFullName ."', 
				         listID = '".$listID["listID"]."',position = '". $data1[3] ."',portfolio ='". $data1[4] ."',
						 pep_title='".$strTitle."',LastModified = '".$date_time."'
						 where comp_PEPID='".$pepData["comp_PEPID"]."' ";
				
					update($updatPep);
					$counter ++;
					$msg = " $counter PEP Records are Updated Successfully ";
						}else{
					
				    $Querry_Sqls = "INSERT INTO compliancePEP
				   (sno,fullName,pep_title,listID,position,portfolio,LastModified) VALUES 
				   ('". $data1[1] ."','". $strFullName ."','".$strTitle."','".$listID["listID"]."','". $data1[3] ."',
				   '". $data1[4] ."','".$date_time."')";
				     
				 
			     insertInto($Querry_Sqls);
			 	$pepID = @mysql_insert_id();
				
				$counter ++;
				$msg = " $counter PEP List is Imported Successfully ";
			  }
			
	    }
		mysql_close($compliance);	
	}
		else
		{
			$msg = CAUTION_MARK . " Your file is not uploaded due to some error.";
		}
	}
	else
	{
		$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
	}	
}


?>
