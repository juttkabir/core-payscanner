<?php
session_start();
include ("../include/config.php");
$con=dbConnect();
//include ("security.php");


error_reporting(E_ALL);
ini_set("display_errors","on");




/**
 * convert xml string to php array - useful to get a serializable value
 *
 * @param string $xmlstr 
 * @return array
 * @author Adrien aka Gaarf
 */
function xmlstr_to_array($xmlstr) {
  $doc = new DOMDocument();
  $doc->loadXML($xmlstr);
  return domnode_to_array($doc->documentElement);
}
function domnode_to_array($node) {
  $output = array();
  switch ($node->nodeType) {
   case XML_CDATA_SECTION_NODE:
   case XML_TEXT_NODE:
    $output = trim($node->textContent);
   break;
   case XML_ELEMENT_NODE:
    for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) { 
     $child = $node->childNodes->item($i);
     $v = domnode_to_array($child);
     if(isset($child->tagName)) {
       $t = $child->tagName;
       if(!isset($output[$t])) {
        $output[$t] = array();
       }
       $output[$t][] = $v;
     }
     elseif($v) {
      $output = (string) $v;
     }
    }
    if(is_array($output)) {
     if($node->attributes->length) {
      $a = array();
      foreach($node->attributes as $attrName => $attrNode) {
       $a[$attrName] = (string) $attrNode->value;
      }
      $output['@attributes'] = $a;
     }
     foreach ($output as $t => $v) {
      if(is_array($v) && count($v)==1 && $t!='@attributes') {
       $output[$t] = $v[0];
      }
     }
    }
   break;
  }
  return $output;
}
//debug(date('s'),true);

/**
 * XML2Array: A class to convert XML to array in PHP
 * It returns the array which can be converted back to XML using the Array2XML script
 * It takes an XML string or a DOMDocument object as an input.
 *
 * See Array2XML: http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes
 *
 * Author : Lalit Patel
 * Website: http://www.lalit.org/lab/convert-xml-to-array-in-php-xml2array
 * License: Apache License 2.0
 *          http://www.apache.org/licenses/LICENSE-2.0
 * Version: 0.1 (07 Dec 2011)
 * Version: 0.2 (04 Mar 2012)
 *    Fixed typo 'DomDocument' to 'DOMDocument'
 *
 * Usage:
 *       $array = XML2Array::createArray($xml);
 */
class XML2Array {
 
    private static $xml = null;
 private static $encoding = 'UTF-8';
 
    /**
     * Initialize the root XML node [optional]
     * @param $version
     * @param $encoding
     * @param $format_output
     */
	 
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DOMDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
  self::$encoding = $encoding;
    }
 
    /**
     * Convert an XML to Array
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DOMDocument
     */
	 
    public static function &createArray($input_xml) {
        $xml = self::getXMLRoot();
  if(is_string($input_xml)) {
   $parsed = $xml->loadXML($input_xml);
   if(!$parsed) {
    throw new Exception('[XML2Array] Error parsing the XML string.');
   }
  } else {
   if(get_class($input_xml) != 'DOMDocument') {
    throw new Exception('[XML2Array] The input XML object should be of type: DOMDocument.');
   }
   $xml = self::$xml = $input_xml;
  }
  $array[$xml->documentElement->tagName] = self::convert($xml->documentElement);
        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $array;
    }
 
    /**
     * Convert an Array to XML
     * @param mixed $node - XML as a string or as an object of DOMDocument
     * @return mixed
     **/
	 
    private static function &convert($node) {
  $output = array();
 
  switch ($node->nodeType) {
   case XML_CDATA_SECTION_NODE:
    $output['@cdata'] = trim($node->textContent);
    break;
 
   case XML_TEXT_NODE:
    $output = trim($node->textContent);
    break;
 
   case XML_ELEMENT_NODE:
 
    // for each child node, call the covert function recursively
    for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
     $child = $node->childNodes->item($i);
     $v = self::convert($child);
     if(isset($child->tagName)) {
      $t = $child->tagName;
 
      // assume more nodes of same kind are coming
      if(!isset($output[$t])) {
       $output[$t] = array();
      }
      $output[$t][] = $v;
     } else {
      //check if it is not an empty text node
      if($v !== '') {
       $output = $v;
      }
     }
    }
 
    if(is_array($output)) {
     // if only one node of its kind, assign it directly instead if array($value);
     foreach ($output as $t => $v) {
      if(is_array($v) && count($v)==1) {
       $output[$t] = $v[0];
      }
     }
     if(empty($output)) {
      //for empty nodes
      $output = '';
     }
    }
 
    // loop through the attributes and collect them
    if($node->attributes->length) {
     $a = array();
     foreach($node->attributes as $attrName => $attrNode) {
      $a[$attrName] = (string) $attrNode->value;
     }
     // if its an leaf node, store the value in @value instead of directly storing it.
     if(!is_array($output)) {
      $output = array('@value' => $output);
     }
     $output['@attributes'] = $a;
    }
    break;
  }
  return $output;
    }
 
    /*
     * Get the root XML node, if there isn't one, create it.
     **/
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }
}

$str_xml = file_get_contents('http://premierfx.live.hbstech.co.uk/xe.php');
print_r($str_xml);
echo '<br><br><br><br><br>';
$str_xml1 = explode("<h1>Base Currency USD</h1>",$str_xml);
$str_xml2 = explode("<h1>Base Currency EUR</h1>",$str_xml1[1]);
$str_xml3 = explode("<h1>Base Currency GBP</h1>",$str_xml2[1]);


$str_xml_USD = $str_xml2[0];
$str_xml_EUR = $str_xml3[0];
$str_xml_GBP = $str_xml3[1];

$objCreateArr = new XML2Array();
//$arrStrUSD = explode("<xe-datafeed>",$str_xml_USD);
//$str_xml_USD = "<xe-datafeed>".$arrStrUSD[1];
//die($str_xml_USD);
function xml2array($string) {
   // $string = file_get_contents($file);
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($parser, $string, $vals, $index);
    xml_parser_free($parser);    
    $ary=array();
    $i=-1;
    foreach ($vals as $r){
        if($r['level'] == 1)continue;
        if($r['level'] == 2 && $r['type'] == "open"){
            ++$i;
            continue;
            }
        $ary[$i][$r['tag']] = @$r['value'];
    }
    return $ary;
}
$arrXmlResponseUSD  =  xml2array($str_xml_USD);
$arrXmlResponseEUR = xml2array($str_xml_EUR);
$arrXmlResponseGBP = xml2array($str_xml_GBP);

//$xml = new SimpleXMLElement($str_xml_USD);
//array_walk_recursive($test_array, array ($xml, 'addChild'));
//print $xml->asXML();
       //$arrXmlResponseUSD = $objCreateArr->createArray($str_xml_USD);
      // $arrXmlResponseEUR = $objCreateArr->createArray($str_xml_EUR);
       //$arrXmlResponseGBP = $objCreateArr->createArray($str_xml_GBP);
//print_r($arrXmlResponseEUR); echo phpinfo();exit();
$baseCurrencyUSD = $arrXmlResponseUSD[21]["hvalue"];

//$intCountUSD = count($arrXmlResponseUSD["xe-datafeed"]["currency"]);

//$arrQuotedCurrencyUSD = $arrXmlResponseUSD["xe-datafeed"]["currency"];


$baseCurrencyEUR = $arrXmlResponseEUR[21]["hvalue"];

//$intCountEUR = count($arrXmlResponseEUR["xe-datafeed"]["currency"]);

//$arrQuotedCurrencyEUR = $arrXmlResponseEUR["xe-datafeed"]["currency"];


$baseCurrencyGBP = $arrXmlResponseGBP[21]["hvalue"];

//$intCountGBP = count($arrXmlResponseGBP["xe-datafeed"]["currency"]);

//$arrQuotedCurrencyGBP = $arrXmlResponseGBP["xe-datafeed"]["currency"];


$date_time = date('Y-m-d  H:i:s');
$intCountUSD = 0;
print_r($arrXmlResponseUSD);
print_r(count($arrXmlResponseUSD));

print_r($arrXmlResponseGBP);
for($i=22;$i<count($arrXmlResponseUSD); $i++)
{
	$intCountUSD++;
	//$date_time = date('Y-m-d  H:i:s');
	$queryInsertFeedUSD = "insert into online_exchange_rates (baseCurrency,quotedCurrency,exchangeRate,inverseRate,createdDate) values ('".$baseCurrencyUSD."','".$arrXmlResponseUSD[$i]["csymbol"]."','".$arrXmlResponseUSD[$i]["crate"]."','".$arrXmlResponseUSD[$i]["cinverse"]."','".$date_time."')";
	
	print_r($queryInsertFeedUSD);
	$yes=insertInto($queryInsertFeedUSD);
	if($yes)
	{
	echo 'inserte4d';
	}else
	{
	
	echo 'Not ';}




}for($i=22;$i<count($arrXmlResponseEUR);$i++)
{
	//$date_time = date('Y-m-d H:i:s');
	$queryInsertFeedEUR = "insert into online_exchange_rates (baseCurrency,quotedCurrency,exchangeRate,inverseRate,createdDate) values ('".$baseCurrencyEUR."','".$arrXmlResponseEUR[$i]["csymbol"]."','".$arrXmlResponseEUR[$i]["crate"]."','".$arrXmlResponseEUR[$i]["cinverse"]."','".$date_time."')";
	insertInto($queryInsertFeedEUR);




}for($i=22;$i<count($arrXmlResponseGBP);$i++)
{
	//$date_time = date('Y-m-d H:i:s');
	$queryInsertFeedGBP = "insert into online_exchange_rates (baseCurrency,quotedCurrency,exchangeRate,inverseRate,createdDate) values ('".$baseCurrencyGBP."','".$arrXmlResponseGBP[$i]["csymbol"]."','".$arrXmlResponseGBP[$i]["crate"]."','".$arrXmlResponseGBP[$i]["cinverse"]."','".$date_time."')";
	insertInto($queryInsertFeedGBP);




}

$date = date('Y-m-d H:i:s', strtotime('-1 hour'));
$intMinOne = date('i');
//$intMinOne = 1;
if(count($arrXmlResponseGBP)>0 || count($arrXmlResponseEUR)>0 || count($arrXmlResponseUSD)>0)
{
	if($intMinOne == 01)
	{
		$strQueryBackup = "INSERT INTO `historical-Xe-rate` SELECT * from `online_exchange_rates` where online_exchange_rates.createdDate < '".$date."' ";
		//debug($strQueryBackup,true);
		insertInto($strQueryBackup);
		$strQueryDelete = "DELETE FROM `online_exchange_rates` where online_exchange_rates.createdDate < '".$date."' ";
		update($strQueryDelete);
	}
}
//echo phpinfo();


###############
$closeInfo=dbClose($con);


?>
