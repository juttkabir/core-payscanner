<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

//echo count($trnsid);
//echo $trnsid[0];
if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];
//echo count($trnsid);
//echo $trnsid[0];

if($_POST["btnAction"] != "")
{
	for ($i=0;$i<$_POST["totTrans"];$i++)
	{
		if(count($trnsid) >= 1)
		{
			if($_POST["btnAction"] != "")
			{
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $ReferenceNumber = $contentTrans["refNumber"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $transType  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];
				 $transDate = date("F j, Y", strtotime("$transDate"));

				$fromName = "$company Transfer";//SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				// getting BEneficiray agent Email
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

if($_POST["btnAction"] == "Authorize")
{

if($contentTrans["createdBy"] == 'CUSTOMER')
{
			$subject = "Status Updated";

			$custContents = selectFrom("select * from cm_customer where c_id= '". $custID ."'");
			$custEmail = $custContents["c_name"];
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["FirstName"];
			$custMiddleName = $custContents["MiddleName"];
			$custLastName = $custContents["LastName"];
			$custCountry = $custContents["c_country"];
			$custCity = $custContents["c_city"];
			$custZip = $custContents["c_zip"];
			$custLoginName = $custContents["username"];
			$custEmail = $custContents["c_email"];
			$custPhone = $custContents["c_phone"];


			$benContents = selectFrom("select * from cm_beneficiary where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction status is updated </p></td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$transType." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> $systemCode:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$transDate."  </td>
  </tr>
  <tr>
    <td><p>Authorised Date: $tran_date</p></td>
    <td><p>$manualCode: $tran_date</p></td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";

if(trim($transType) == 'Bank Transfer')
{
  		$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);

		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];

$message.="  <tr>
    <td><p><strong>Beneficiary Bank Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Bank Name:  ".$_SESSION['bankName']."  </td>
    <td> Acc Number:  ".$_SESSION['accNo']."  </td>
  </tr>
  <tr>
    <td> Branch Code:  ".$_SESSION['branchCode']."  </td>
    <td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
";
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["swiftCode"] = "";
		$_SESSION["accNo"] = "";
}
elseif(trim($transType) == "Pick up")
{

$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
$agentnamee = $cContents["cp_corresspondent_name"];
$contactperson = $cContents["cp_corresspondent_name"];
$company = $cContents["cp_branch_name"];
$address = $cContents["cp_branch_address"];
$country = $cContents["cp_country"];
$city = $cContents["cp_city"];
$phone = $cContents["cp_phone"];
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));


$message .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collestin Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];
$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];

if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}
if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}
$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> &nbsp; </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
else
{

			$subject = "Status Updated";

			$custContents = selectFrom("select * from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["firstName"];
			$custMiddleName = $custContents["middleName"];
			$custLastName = $custContents["lastName"];
			$custCountry = $custContents["Country"];
			$custCity = $custContents["City"];
			$custZip = $custContents["Zip"];
			$custEmail = $custContents["email"];
			$custPhone = $custContents["Phone"];


			$benContents = selectFrom("select * from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction status is updated </p></td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$transType." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> $systemCode:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$transDate."  </td>
  </tr>
  <tr>
    <td><p>Authorised Date: $tran_date</p></td>
    <td>$manualCode:   $ReferenceNumber </td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";

if(trim($transType) == 'Bank Transfer')
{

  		$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);

		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];

$message.="  <tr>
    <td><p><strong>Beneficiary Bank Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Bank Name:  ".$_SESSION['bankName']."  </td>
    <td> Acc Number:  ".$_SESSION['accNo']."  </td>
  </tr>
  <tr>
    <td> Branch Code:  ".$_SESSION['branchCode']."  </td>
    <td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
";
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["swiftCode"] = "";
		$_SESSION["accNo"] = "";
}
elseif(trim($transType) == "Pick up")
{

$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
$agentnamee = $cContents["cp_corresspondent_name"];
$contactperson = $cContents["cp_corresspondent_name"];
$company = $cContents["cp_branch_name"];
$address = $cContents["cp_branch_address"];
$country = $cContents["cp_country"];
$city = $cContents["cp_city"];
$phone = $cContents["cp_phone"];
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));

$message .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collestin Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];
$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];

if($amount > 500)
{
	$tempAmount = ($amount - 500);
	//$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount);
}
if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}
$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> &nbsp; </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
}
/**********************************************************/

				sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
			}

			if($_POST["btnAction"] == "Cancel")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Cancelled', cancelledBy = '$username', cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction cancelled successfully.");
			}

			if($_POST["btnAction"] == "Recall")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'recalled', recalledBy = '$username', recalledDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction recalled successfully.");
				//echo "HERE";
			}
			if($_POST["btnAction"] == "Verify")
			{
				//echo "update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username' where transID='". $trnsid[$i] ."'";
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username' where transID='". $trnsid[$i] ."'");
				insertError("Transaction verified successfully.");
				//echo "HERE";
			}

			if($_POST["btnAction"] == "Authorize")
			{
				// Update agents account limit;
				update("update " . TBL_ADMIN_USERS . " set limitUsed  = (limitUsed  + '".$contentTrans["totalAmount"]."') where userID = '". $contentTrans["custAgentID"] ."'");
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction Authorized successfully.");
				//echo "HERE";
			}

		}
	}
redirect("manage-transactions.php?msg=Y&action=" . $_GET["action"]);
}
$query = "select * from ". TBL_TRANSACTIONS." where 1 ";
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS." where 1 ";
$_SESSION["transType"] = $_POST["transType"];
$_SESSION["transStatus"] = $_POST["transStatus"];
if($_POST["Submit"] =="Search")
{
	if($_POST["transType"] != "")
	{
		$query .= " and transType='".$_POST["transType"]."'";
		$queryCnt .= " and transType='".$_POST["transType"]."'";
	}
	if($_POST["transStatus"] != "")
	{
		$query .= " and transStatus='".$_POST["transStatus"]."'";
		$queryCnt .= " and transStatus='".$_POST["transStatus"]."'";
	}
	if($_POST["transID"] != "")
	{
		$query .= " and (transID='".$_POST["transID"]."' OR refNumber = '".$_POST["transID"]."' OR  refNumberIM = '".$_POST["transID"]."') ";
		$queryCnt .= " and (transID='".$_POST["transID"]."' OR refNumber = '".$_POST["transID"]."' OR  refNumberIM = '".$_POST["transID"]."') ";
			}
}
if($_GET["action"] == "cancel")
{
	$query .= " and transStatus ='Pending' and transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour) ";
	$queryCnt .= " and transStatus ='Pending' and transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour) ";
}

if($_GET["action"] == "recall")
{
	$query .= " and (transStatus = 'Pending' OR transStatus = 'authorize' OR transStatus = 'Processing') ";
	$queryCnt .= " and (transStatus = 'Pending' OR transStatus = 'authorize' OR transStatus = 'Processing') ";
}

if($_GET["action"] == "verify")
{
	$query .= " and transStatus ='Pending' ";
	$queryCnt .= " and transStatus ='Pending' ";
}

if($_GET["action"] == "authorize")
{
	$query .= " and transStatus = 'Processing' ";
	$queryCnt .= " and transStatus = 'Processing' ";
}

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and custAgentID = '".$_SESSION["loggedUserData"]["userID"]."'";
		$queryCnt .= " and custAgentID = '".$_SESSION["loggedUserData"]["userID"]."'";
}
$query .= " order by transDate DESC";
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
$query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);
$allCount = countRecords($queryCnt );
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><strong><font color="#FFFFFF" size="2"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?> Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#6699CC">
	  <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
          <td nowrap bgcolor="#6699CC"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
        <td nowrap>
		  Ref No. OR Trans No.
		    <input name="transID" type="text" id="transID">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$_SESSION["transType"]; ?>");</script>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option>

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$_SESSION["transStatus"]; ?>");</script>
            <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#6699CC">
      <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
          <td height="25" nowrap bgcolor="#6699CC"><span class="tab-u"><strong>&nbsp;There are <? echo count($contentsTrans)?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center"><? if ($_GET["action"] != "") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
		      <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>

				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else
				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<?echo $i?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			   <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> <table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>



		</form>
      </table></td>
  </tr>

</table>
</body>
</html>