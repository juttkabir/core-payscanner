<?php 
	/**
	 * @package Payex
	 * @subpackage Sender Registration
	 * This page is used for registration Sender from the Premeir Exchange Website
	 * @author Mirza Arslan Baig
	 */
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Customer Registration</title>
<style>
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed,
	figure, figcaption, footer, header, hgroup,
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
	}
	/* HTML5 display-role reset for older browsers */
	article, aside, details, figcaption, figure,
	footer, header, hgroup, menu, nav, section {
	display: block;
	}
	body {
	line-height: 1;
	}
	ol, ul {
	list-style: none;
	}
	blockquote, q {
	quotes: none;
	}
	blockquote:before, blockquote:after,
	q:before, q:after {
	content: '';
	content: none;
	}
	table {
	border-collapse: collapse;
	border-spacing: 0;
	}
	*{
		color: #9D9D98;
		font-family: 'PT Sans',Tahoma,Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;
		font-weight: 400;
		letter-spacing: 0;
		line-height: 20px;
		outline: medium none;
		resize: none;
	}
	.wrapper{
		width:960px;
		margin:0 auto;
	}
	.head1{
		color: #E6A300;
		font-family: 'Cabin';
		font-size: 20px;
		font-weight: 400;
		line-height: 20px;
		margin: 15px 0;
		font-size:24px;
	}
	.form_container{
		border: 1px solid #CCCCCC;
		padding: 0;
		overflow:hidden;
	}
	.form_container input{
	 	background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		height: 28px;
		line-height: 28px;
		margin: 0;
		padding: 0 7px;
	}
	.form_container p{
		color: #E6A300;
		line-height: 9px;
		margin: 0;
		padding: 0 0 10px;
	}
	.form_container select{
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		height: 30px;
		line-height: 30px;
		margin: 0;
		padding: 4px;
	}
	label.error{
		color:#FF4B4B;
		font-size:11px;
		font-weight:bold;
		display:block;
		margin-top:5px;
		font-family:arial;
	}
	.row{
		padding:5px 0;
		overflow:hidden;
		margin:10px 0;
	}
	#file{
		border:1px solid #E5A000;
		background-color:#ECB83A;
		text-decoration:none;
		padding:3px 5px;
		color:#fff;
		font-family:'PT Sans',Tahoma,Verdana,Arial,Helvetica,sans-serif;
		font-size:14px;
	}
	.error{
		line-height:15px;
	}
</style>
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="javascript/jquery.validate.js" type="text/javascript"></script>
<script src="javascript/date.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="./javascript/jquery.maskedinput.min.js"></script>
<script language="javascript" src="./javascript/ajaxupload.js"></script>

<script>
var fileValidateFlag = 0;
$(
	function(){
		var button = $('#file');
		var testUpload = new AjaxUpload(button, {
			action: 'registration_form_conf.php', 
			name: 'passportDoc', // for single upload this should have 0 (zero appended)
			autoSubmit: false,
			allowedExtensions: ['.jpg' , '.png' , '.gif', 'jpeg'],        
			sizeLimit: 2048, // max size  
			onChange : function(file, ext){
				if((ext && /^(jpg|png|jpeg|gif)$/i.test(ext))){
					fileValidateFlag = 1;
					//button.text("Selected");
					button.css({"background-color" : "#CCCCCC", "border" : "1px solid #000000", "font-weight" : "bold"});
					button.html('Document Selected');
					alert("Document selected for upload!!");
				}
				else{
					alert('select a valid file!!');
					button.css({"background-color" : "#ECB83A", "border" : "1px solid #E5A000", "font-weight" : "normal"});
					button.html('Select Document');
				}
			},
			onSubmit : function(file, ext){
				
				// If you want to allow uploading only 1 file at time,
				// you can disable upload button
				this.disable();
				$('#wait').fadeIn("slow");
			},
			onComplete: function(file, response){
				// enable upload button
				this.enable();
				//this.disable();
				$('#emailValidator').html(' ');
				$('#wait').fadeOut("fast");
				alert(response);
			}
		});
		
		
		$(document).ready(		
			function(){
			
			
			// Date Picker
			Date.format = 'dd/mm/yyyy';
			/* $('#dob').datePicker({
				clickInput:true,
				createButton:false,
				endDate: (new Date()).asString()
			});
			$('#dob').dpSetStartDate(' '); */
			$('#passportExpiry').datePicker({clickInput:true,createButton:false});
			$('#passportIssue').datePicker({
				clickInput:true,
				createButton:false,
				endDate: (new Date()).asString()
			});
			$('#idExpiry').datePicker({clickInput:true, createButton:false});
			$('#passportIssue').dpSetStartDate(' ');
			
			
			// Masking
			$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');
			$('#line1').mask('**-***-*********-9-***************');
			$('#line2').mask('999999-9-*-999999-9-aaa-***********');
			
			
			// Form Validation 
			$("#senderRegistrationForm").validate({
				rules:{
				 	
					forename:{
						required:true
					},
					surname:{
						required:true
					},
					postcode:{
						required:true
					},
					passportNumber:{
						required: function(){
							if($('#line1').val() == '')
								return true;
							else
								return false;
						}
						//,minlength: 37
					},
					line1:{
						required: function(){
							if($('#passportNumber').val() == '')
								return true;
							else
								return false;
						}
					},
					line2:{
						required: function(){
							if($('#line1').val() == '')
								return false;
							else
								return true;
						}
					},
					line3:{
						required: function(){
							if($('#line2').val() == '' || $('#line1').val() == '')
								return false;
							else
								return true;
						}
					},
					idExpiry:{
						required: function(){
							if($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '')
								return true;
							else
								return false;
						}
					},
					passportExpiry:{
						required: function(){
							if($('#passportNumber').val() != '')
								return true;
							else
								return false;
						}
					}
				},
				messages:{
					passportNumber: "Please provide passport or national identity card!!",
					line1: "Please provide passport or national identity card!!",
					line2: "Please enter remaining part of national identity card number!!",
					line3: "Please enter remaining part of national identity card number!!",
					passportExpiry: "Enter the expiry date of passport."
				},
				submitHandler: function(){
					register();
					return false;
				}
			});
			
			// Validating Email availability checks
			$("#email").blur(
				function(){
					var email = $('#email').val();
					/* if(email == ''){
						$('#emailValidator').html('<em style="color:#FF4B4B;font-family:arail;font-size:11px">Please enter email address</em>');
						return false;
					} */
					if(email != ''){
						$.ajax({
							url: "registration_form_conf.php",
							data: {
								email: email,
								chkEmailID: '1'
							},
							type: "POST",
							cache: false,
							success: function(data){
								$('#emailValidator').html(data);
							}
						});
					}
				}
			);
			
			// Check if the passport availability checks
			
			$('#passportNumber').blur(
				function(){
					setTimeout(function(){
						passportAvailabilty();
					}, 100);
				}
			);
			
			function passportAvailabilty(){
				var passport = $('#passportNumber').val();
				if(passport != ''){
					$.ajax({
						url: "registration_form_conf.php",
						data: {
							passportNum: passport,
							chkPassport: '1'
						},
						type: "POST",
						cache: false,
						success: function(data){
							$('#passportAvailability').html(data);
						}
					});
				}
			}
			
			
			// ID card availability checks
			$('#line3').blur(
				function(){
					var idLine1 = $('#line1').val();
					var idLine2 = $('#line2').val();
					var idLine3 = $('#line3').val();
					if(idLine1 != '' && idLine2 != '' && idLine3 != ''){
						var idCard = idLine1+idLine2+idLine3;
					}
					else
						return false;
					$.ajax({
						url: "registration_form_conf.php",
						data: {
							idCardNumber: idCard,
							chkIDCard: '1'
						},
						type: "POST",
						cache: false,
						success: function(data){
							$('#NICAvailability').html(data);
						}
					});
				}
			);
			
			
			// Ajax Registration of Sender
			function register(){
				//Passport Issue and Expiry Date
				var passportIssue = new Date($('#passportIssue').val());
				var passportExpiry = new Date($('#idExpiry').val());
				if(passportIssue != ''){
					if(passportIssue >= passportExpiry){
						alert("Passport issue date must be smaller then expiry date.");
						return false;
					}
				}
				// Passport Document Validation
				/* if(fileValidateFlag == 0){
					alert('Select passport document for upload!!');
					return false;
				} */
				// After Email Validation register process must be stopped
				/*var emailValidatorMsg = $('#emailValidator').html();
				if(emailValidatorMsg == /already/){
					return false;
				}*/
				// If Idenetity Card Provided then Validation
				/*if($('#line1').val() != ''){
					if($('#line2').val() == ''){
						alert('Please provide the remaining part of ID card.');
						$('#line2').focus();
						return false;
					}
					else if($('#line3').val() == ''){
							alert('Please provide the remaining part of ID card.');
							$('#line3').focus();
							return false;
						}
				}
				else if($('#line2').val() != ''){
						if($('#line1').val() == ''){
							alert('Please provide the remaining part of ID card.');
							$('#line1').focus();
							return false;
						}
						else if($('#line3').val() == ''){
							alert('Please provide the remaining part of ID card.');
							$('#line3').focus();
							return false;
						}
					}
					else if($('#line3').val() != ''){
							if($('#line1').val() == ''){
								alert('Please provide the remaining part of ID card.');
								$('#line1').focus();
								return false;
							}
							else if($('#line2').val() == ''){
								alert('Please provide the remaining part of ID card.');
								$('#line2').focus();
								return false;
							}
						}*/
						
				// Registration Request
				var data = $('#senderRegistrationForm').serialize();
				//alert(data);
				data += "&register=Register";
				$('#wait').fadeIn("fast");
				$.ajax({
					url: "registration_form_conf.php",
					data: data,
					type: "POST",
					cache: false,
					success: function(msg){
						if(msg.search(/Sender is registered successfully/i) >= 0){
							if(fileValidateFlag != 0){
								fileValidateFlag = 0;
								testUpload.submit();
							}
							resetFormData();
						}
						$('#wait').fadeOut("fast");
						$('#msg').html(msg);
					}
				});
			}	
			function resetFormData(){
				$('#addressContainer').fadeOut('fast');
				document.forms[0].reset();
				$('#file').css({"background-color" : "#ECB83A", "border" : "1px solid #E5A000", "font-weight" : "normal"});
				$('#file').html('Select Document');
				/*setTimeout(function(){
					$('#msg').html(' ');
				}, 2000);*/
			}
			
			// Trigger the search address function
			$('#searchAddress').click(
				function(){
					searchAddress();
				}
			);
			
		}
		);
	}
);
	
	// Populate the Address in the fields
	function getAddressData(ele)
	{
		//window.location="http://arsbai09.dev.hbstech.co.uk/api/gbgroup/addresslookupCus.php?a="+ele.value;
		var value = ele.value;
		var arrAddress = value.split('/');
		var buildingNumber = $.trim(arrAddress[0]);
		var buildingName = $.trim(arrAddress[1]);
		var subBuilding = $.trim(arrAddress[2]);
		var street = $.trim(arrAddress[3]);
		var subStreet = $.trim(arrAddress[4]);
		var town = $.trim(arrAddress[5]);
		var postcode = $.trim(arrAddress[6]);
		var organization = $.trim(arrAddress[7]);
		var buildingNumberVal = '';
		var buildingNameVal = '';
		var streetValue = '';
		if(buildingNumber != '')
			buildingNumberVal += buildingNumber;
		if(buildingName != '')
			buildingNameVal += buildingName;
		if(subBuilding != '')
			buildingNameVal += ' '+subBuilding;
		
		if(street != '')
			streetValue += street;
		if(subStreet != '')
			streetValue += ' '+subStreet;
			
		$('#buildingNumber').val(buildingNumberVal);
		$('#buildingName').val(buildingNameVal);
		$('#street').val(streetValue);
		$('#town').val(town);
		$('#postcode').val(postcode);
	}
	
	// if Press Enter on any field in the address area trigger the search address function
	function enterToSearch(e){
		if(e.which){
			keyCode = e.which;
			if(keyCode == 13){
				e.preventDefault();
				searchAddress();
			}
		}
	}
	
	// Calls the API for suggessted address
	function searchAddress(){
		$('#residenceCountry').val('United Kingdom');
		$('#addressContainer').fadeOut('fast');
		postcode = $.trim($('#postcode').val());
		buildingNumber = $.trim($('#buildingNumber').val());
		street = $.trim($('#street').val());
		town = $.trim($('#town').val());
		if(postcode == ''){
			alert("Enter postcode to search address!!");
			$('#postcode').focus();
			return;
		}
		$('#wait').fadeIn("fast");
		$.ajax({
			//url: "http://premierexchange.test.hbstech.co.uk/api/gbgroup/addresslookupCus.php",
			url: "http://<?php echo $_SERVER['HTTP_HOST']; ?>/api/gbgroup/addresslookupCus.php",
			data: {
				postcode: postcode,
				buildingNumber: buildingNumber,
				street: street,
				town: town
			},
			type: "POST",
			cache: false,
			success: function(data){
				//alert(data.match(/option/i));
				$('#wait').fadeOut("slow");
				if(data.search(/option/i) >= 0){
					$('#addressContainer').fadeIn('fast');
					$('#suggesstions').html(data);
				}
				else{
					$('#addressContainer').fadeIn('fast');
					$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">No address found against your supplied post code!</i>');
				}
			}
		});
	}
	
	// Clear the address section
	function clearAddress(){
		$('#buildingNumber').val('');
		$('#buildingName').val('');
		$('#street').val('');
		$('#town').val('');
		$('#province').val('');
	}
	
	function passportMask(){
		passportCountry = $('#passportCountry').val();
		switch(passportCountry){
			case 'United Kingdom':
				$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<<<<<<<<<<<<<<-9-9');
				$('#passportNumber').removeAttr("disabled");
				break;
			case 'Portugal':
				$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-**********<<<<-9-9');
				$('#passportNumber').removeAttr("disabled");
				break;
			case 'USA':
				$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<<<<<<<<<<<<<<-9-9');
				$('#passportNumber').removeAttr("disabled");
				break;
			case 'Australia':
				$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<*********<<<<-9-9');
				$('#passportNumber').removeAttr("disabled");
				break;
			case 'Spain':
				$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-***********<<<-9-9');
				$('#passportNumber').removeAttr("disabled");
				break;
			default:
				alert('Select a country and enter passport number.');
				break;
		}
		
	}
	
</script>
</head>
<body>
	<div id="wait" style="position:absolute;height:390%;width:100%;background:#222;opacity:0.7;display:none;">
		<div style="margin: 0 auto;position: relative;top: 50%;width: 300px;font-size:48px;color:#fff;font-family:arial">
			LOADING...
		</div>
	</div>
	<div class="wrapper">
		<h1 class="head1">Create an Account</h1>
		<div class="form_container">
			<form name="senderRegistrationForm" id="senderRegistrationForm" action="" method="post" style="overflow:hiddenl;margin:5px 155px;">
				<div class="row">
					<div style="float:left;">
						<p>Title</p>
						<select name="title" style="height:30px; width:96px;">
							<option value="Mr.">Mr.</option>
							<option value="Mrs">Mrs</option>
							<option value="Miss.">Miss.</option>
							<option value="Dr.">Dr.</option>
							<option value="Prof.">Prof.</option>
							<option value="Eng.">Eng.</option>
							<option value="Herr">Herr</option>
							<option value="Frau">Frau</option>
							<option value="Ing.">Ing.</option>
							<option value="Mag.">Mag.</option>
							<option value="Sr.">Sr.</option>
							<option value="Sra.">Sra.</option>
							<option value="Srta.">Srta.</option>
						</select>
					</div>
					<div style="float:left;margin-left:20px;">
						<p>Forename <em>*</em></p>
						<input name="forename" type="text" style="width:178px;" id="forename" />
					</div>
					<div style="float:left;margin-left:20px;">
						<p>Surname <em>*</em></p>
						<input name="surname" type="text" style="width:294px;" id="surname" />
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>Email<em>*</em></p>
						<input style="width:295px;" name="email" type="text" id="email" class="required email"/>
						<div id="emailValidator" style="clear:both"></div>
						<input type="hidden" name="ipAddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>"/>
					</div>
					<div style="float:left;">
						<p>Date of Birth<em>*</em></p>
						<!--<input name="dob" id="dob" type="text" style="width:140px;margin-right:5px;" readonly="readonly" />-->
						<select name="dobDay" id="dobDay">
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
						</select>
						<select name="dobMonth" id="dobMonth">
							<option value="01">Jan</option>
							<option value="02">Feb</option>
							<option value="03">Mar</option>
							<option value="04">Apr</option>
							<option value="05">May</option>
							<option value="06">Jun</option>
							<option value="07">Jul</option>
							<option value="08">Aug</option>
							<option value="09">Sep</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
						</select>
						<select name="dobYear" id="dobYear">
							<option value="1929">1929</option>
							<option value="1930">1930</option>
							<option value="1931">1931</option>
							<option value="1932">1932</option>
							<option value="1933">1933</option>
							<option value="1934">1934</option>
							<option value="1935">1935</option>
							<option value="1936">1936</option>
							<option value="1937">1937</option>
							<option value="1938">1938</option>
							<option value="1939">1939</option>
							<option value="1940">1940</option>
							<option value="1941">1941</option>
							<option value="1942">1942</option>
							<option value="1943">1943</option>
							<option value="1944">1944</option>
							<option value="1945">1945</option>
							<option value="1946">1946</option>
							<option value="1947">1947</option>
							<option value="1948">1948</option>
							<option value="1949">1949</option>
							<option value="1950">1950</option>
							<option value="1951">1951</option>
							<option value="1952">1952</option>
							<option value="1953">1953</option>
							<option value="1954">1954</option>
							<option value="1955">1955</option>
							<option value="1956">1956</option>
							<option value="1957">1957</option>
							<option value="1958">1958</option>
							<option value="1959">1959</option>
							<option value="1960">1960</option>
							<option value="1961">1961</option>
							<option value="1962">1962</option>
							<option value="1963">1963</option>
							<option value="1964">1964</option>
							<option value="1965">1965</option>
							<option value="1966">1966</option>
							<option value="1967">1967</option>
							<option value="1968">1968</option>
							<option value="1969">1969</option>
							<option value="1970" selected="">1970</option>
							<option value="1971">1971</option>
							<option value="1972">1972</option>
							<option value="1973">1973</option>
							<option value="1974">1974</option>
							<option value="1975">1975</option>
							<option value="1976">1976</option>
							<option value="1977">1977</option>
							<option value="1978">1978</option>
							<option value="1979">1979</option>
							<option value="1980">1980</option>
							<option value="1981">1981</option>
							<option value="1982">1982</option>
							<option value="1983">1983</option>
							<option value="1984">1984</option>
							<option value="1985">1985</option>
							<option value="1986">1986</option>
							<option value="1987">1987</option>
							<option value="1988">1988</option>
							<option value="1989">1989</option>
							<option value="1990">1990</option>
							<option value="1991">1991</option>
							<option value="1992">1992</option>
							<option value="1993">1993</option>
							<option value="1994">1994</option>
							<option value="1995">1995</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
						</select>
					</div>
					<div style="float:right;margin-right:5px;">
						<p>Gender</p>
						<select name="gender" style="width:250">
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px">
						<p>Country of Birth</p>
						<select style="width:310px;" name="birthCountry" id="birthCountry">
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="Germany">Germany</option>
							<option value="USA">USA</option>
							<option value="Canada">Canada</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'Ivoire">Cote D'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
							<option value="Denmark">Denmark</option>
							<option value="Djiboutio">Djiboutio</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Escocia">Escocia</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadaloupe">Guadaloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kitts and Nevis">Kitts and Nevis</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marinique">Marinique</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia">Micronesia</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Monserrat">Monserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option><option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Island">Pitcairn Island</option>
							<option value="Poland">Poland</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa-American">Samoa-American</option>
							<option value="Samoa-Western">Samoa-Western</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakie">Slovakie</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkemenistan">Turkemenistan</option>
							<option value="Turkey">Turkey</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Araba Emirates">United Araba Emirates</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Virgin Islands">Virgin Islands</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Yugoslavia">Yugoslavia</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
				</div>
				<fieldset style="border:1px solid #ccc;padding:0 8px;">
					<legend style="color: #E6A300;">Address</legend>
					<div class="row">
						<div style="float:left;margin-right:30px">
							<p>Postcode / Zipcode<em>*</em></p>
							<input name="postcode" id="postcode"  type="text" style="width:150px;" onKeyPress='enterToSearch(event);' onFocus="clearAddress();"/>
						</div>
						<div style="float:left;margin-right:30px;">
							<p>Building No.</p>
							<input name="buildingNumber" id="buildingNumber" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left;margin-right:30px;">
							<p>Building Name</p>
							<input name="buildingName" id="buildingName" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
					</div>
					<div class="row">
						<div style="float:left;margin-right:30px">
							<p>Street</p>
							<input name="street" id="street" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left;margin-right:30px">
							<p>Town</p>
							<input name="town" id="town" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left">
							<p>Province</p>
							<input name="province" id="province" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
					</div>
					<div class="row">
						<div style="float:left">
							<p>Country of Residence</p>
							<select style="width:310px;" name="residenceCountry" id="residenceCountry">
								<option value="United Kingdom">United Kingdom</option>
								<option value="Portugal">Portugal</option>
								<option value="Germany">Germany</option>
								<option value="USA">USA</option>
								<option value="Canada">Canada</option>
								<option value="Afghanistan">Afghanistan</option>
								<option value="Albania">Albania</option>
								<option value="Algeria">Algeria</option>
								<option value="Andorra">Andorra</option>
								<option value="Angola">Angola</option>
								<option value="Anguilla">Anguilla</option>
								<option value="Antarctica">Antarctica</option>
								<option value="Antigua and Barbuda">Antigua and Barbuda</option>
								<option value="Argentina">Argentina</option>
								<option value="Armenia">Armenia</option>
								<option value="Aruba">Aruba</option>
								<option value="Australia">Australia</option>
								<option value="Austria">Austria</option>
								<option value="Azerbaijan">Azerbaijan</option>
								<option value="Bahamas">Bahamas</option>
								<option value="Bahrain">Bahrain</option>
								<option value="Bangladesh">Bangladesh</option>
								<option value="Barbados">Barbados</option>
								<option value="Belarus">Belarus</option>
								<option value="Belgium">Belgium</option>
								<option value="Belize">Belize</option>
								<option value="Benin">Benin</option>
								<option value="Bermuda">Bermuda</option>
								<option value="Bhutan">Bhutan</option>
								<option value="Bolivia">Bolivia</option>
								<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
								<option value="Botswana">Botswana</option>
								<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
								<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
								<option value="British Virgin Islands">British Virgin Islands</option>
								<option value="Brunei">Brunei</option>
								<option value="Bulgaria">Bulgaria</option>
								<option value="Burkina Faso">Burkina Faso</option>
								<option value="Burundi">Burundi</option>
								<option value="Cambodia">Cambodia</option>
								<option value="Cameroon">Cameroon</option>
								<option value="Canada">Canada</option>
								<option value="Cape Verde">Cape Verde</option>
								<option value="Cayman Islands">Cayman Islands</option>
								<option value="Central African Republic">Central African Republic</option>
								<option value="Chad">Chad</option>
								<option value="Chile">Chile</option>
								<option value="China">China</option>
								<option value="Christmas Island">Christmas Island</option>
								<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
								<option value="Colombia">Colombia</option>
								<option value="Comoros">Comoros</option>
								<option value="Congo">Congo</option>
								<option value="Cook Islands">Cook Islands</option>
								<option value="Costa Rica">Costa Rica</option>
								<option value="Cote D'Ivoire">Cote D'Ivoire</option>
								<option value="Croatia">Croatia</option>
								<option value="Cuba">Cuba</option>
								<option value="Cyprus">Cyprus</option>
								<option value="Czech Republic">Czech Republic</option>
								<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
								<option value="Denmark">Denmark</option>
								<option value="Djiboutio">Djiboutio</option>
								<option value="Dominica">Dominica</option>
								<option value="Dominican Republic">Dominican Republic</option>
								<option value="East Timor">East Timor</option>
								<option value="Ecuador">Ecuador</option>
								<option value="Egypt">Egypt</option>
								<option value="El Salvador">El Salvador</option>
								<option value="Equatorial Guinea">Equatorial Guinea</option>
								<option value="Eritrea">Eritrea</option>
								<option value="Escocia">Escocia</option>
								<option value="Estonia">Estonia</option>
								<option value="Ethiopia">Ethiopia</option>
								<option value="Falkland Islands">Falkland Islands</option>
								<option value="Faroe Islands">Faroe Islands</option>
								<option value="Fiji">Fiji</option>
								<option value="Finland">Finland</option>
								<option value="France">France</option>
								<option value="French Guiana">French Guiana</option>
								<option value="French Polynesia">French Polynesia</option>
								<option value="Gabon">Gabon</option>
								<option value="Gambia">Gambia</option>
								<option value="Georgia">Georgia</option>
								<option value="Germany">Germany</option>
								<option value="Ghana">Ghana</option>
								<option value="Gibraltar">Gibraltar</option>
								<option value="Greece">Greece</option>
								<option value="Greenland">Greenland</option>
								<option value="Grenada">Grenada</option>
								<option value="Guadaloupe">Guadaloupe</option>
								<option value="Guam">Guam</option>
								<option value="Guatemala">Guatemala</option>
								<option value="Guinea">Guinea</option>
								<option value="Guinea-Bissau">Guinea-Bissau</option>
								<option value="Guyana">Guyana</option>
								<option value="Haiti">Haiti</option>
								<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
								<option value="Honduras">Honduras</option>
								<option value="Hong Kong">Hong Kong</option>
								<option value="Hungary">Hungary</option>
								<option value="Iceland">Iceland</option>
								<option value="India">India</option>
								<option value="Indonesia">Indonesia</option>
								<option value="Iran">Iran</option>
								<option value="Iraq">Iraq</option>
								<option value="Ireland">Ireland</option>
								<option value="Israel">Israel</option>
								<option value="Italy">Italy</option>
								<option value="Jamaica">Jamaica</option>
								<option value="Japan">Japan</option>
								<option value="Jordan">Jordan</option>
								<option value="Kazakhstan">Kazakhstan</option>
								<option value="Kenya">Kenya</option>
								<option value="Kiribati">Kiribati</option>
								<option value="Kitts and Nevis">Kitts and Nevis</option>
								<option value="Kuwait">Kuwait</option>
								<option value="Kyrgyzstan">Kyrgyzstan</option>
								<option value="Laos">Laos</option>
								<option value="Latvia">Latvia</option>
								<option value="Lebanon">Lebanon</option>
								<option value="Lesotho">Lesotho</option>
								<option value="Liberia">Liberia</option>
								<option value="Libya">Libya</option>
								<option value="Liechtenstein">Liechtenstein</option>
								<option value="Lithuania">Lithuania</option>
								<option value="Luxembourg">Luxembourg</option>
								<option value="Macau">Macau</option>
								<option value="Macedonia">Macedonia</option>
								<option value="Madagascar">Madagascar</option>
								<option value="Malawi">Malawi</option>
								<option value="Malaysia">Malaysia</option>
								<option value="Maldives">Maldives</option>
								<option value="Mali">Mali</option>
								<option value="Malta">Malta</option>
								<option value="Marinique">Marinique</option>
								<option value="Marshall Islands">Marshall Islands</option>
								<option value="Mauritania">Mauritania</option>
								<option value="Mauritius">Mauritius</option>
								<option value="Mayotte">Mayotte</option>
								<option value="Mexico">Mexico</option>
								<option value="Micronesia">Micronesia</option>
								<option value="Moldova">Moldova</option>
								<option value="Monaco">Monaco</option>
								<option value="Mongolia">Mongolia</option>
								<option value="Monserrat">Monserrat</option>
								<option value="Morocco">Morocco</option>
								<option value="Mozambique">Mozambique</option>
								<option value="Myanmar">Myanmar</option>
								<option value="Namibia">Namibia</option>
								<option value="Nauru">Nauru</option>
								<option value="Nepal">Nepal</option>
								<option value="Netherlands">Netherlands</option>
								<option value="Netherlands Antilles">Netherlands Antilles</option>
								<option value="New Caledonia">New Caledonia</option>
								<option value="New Zealand">New Zealand</option>
								<option value="Nicaragua">Nicaragua</option>
								<option value="Niger">Niger</option>
								<option value="Nigeria">Nigeria</option>
								<option value="Niue">Niue</option>
								<option value="Norfolk Island">Norfolk Island</option>
								<option value="North Korea">North Korea</option>
								<option value="Northern Mariana Islands">Northern Mariana Islands</option>
								<option value="Norway">Norway</option><option value="Oman">Oman</option>
								<option value="Pakistan">Pakistan</option>
								<option value="Palau">Palau</option>
								<option value="Panama">Panama</option>
								<option value="Papua New Guinea">Papua New Guinea</option>
								<option value="Paraguay">Paraguay</option>
								<option value="Peru">Peru</option>
								<option value="Philippines">Philippines</option>
								<option value="Pitcairn Island">Pitcairn Island</option>
								<option value="Poland">Poland</option>
								<option value="Puerto Rico">Puerto Rico</option>
								<option value="Qatar">Qatar</option>
								<option value="Reunion">Reunion</option>
								<option value="Romania">Romania</option>
								<option value="Russia">Russia</option>
								<option value="Rwanda">Rwanda</option>
								<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
								<option value="Saint Helena">Saint Helena</option>
								<option value="Saint Lucia">Saint Lucia</option>
								<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
								<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
								<option value="Samoa-American">Samoa-American</option>
								<option value="Samoa-Western">Samoa-Western</option>
								<option value="San Marino">San Marino</option>
								<option value="Sao Tome and Principe">Sao Tome and Principe</option>
								<option value="Saudi Arabia">Saudi Arabia</option>
								<option value="Senegal">Senegal</option>
								<option value="Seychelles">Seychelles</option>
								<option value="Sierra Leone">Sierra Leone</option>
								<option value="Singapore">Singapore</option>
								<option value="Slovakie">Slovakie</option>
								<option value="Slovenia">Slovenia</option>
								<option value="Solomon Islands">Solomon Islands</option>
								<option value="Somalia">Somalia</option>
								<option value="South Africa">South Africa</option>
								<option value="South Korea">South Korea</option>
								<option value="Spain">Spain</option>
								<option value="Sri Lanka">Sri Lanka</option>
								<option value="Sudan">Sudan</option>
								<option value="Suriname">Suriname</option>
								<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
								<option value="Swaziland">Swaziland</option>
								<option value="Sweden">Sweden</option>
								<option value="Switzerland">Switzerland</option>
								<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
								<option value="Tajikistan">Tajikistan</option>
								<option value="Tanzania">Tanzania</option>
								<option value="Thailand">Thailand</option>
								<option value="Togo">Togo</option>
								<option value="Tokelau">Tokelau</option>
								<option value="Tonga">Tonga</option>
								<option value="Trinidad and Tobago">Trinidad and Tobago</option>
								<option value="Tunisia">Tunisia</option>
								<option value="Turkemenistan">Turkemenistan</option>
								<option value="Turkey">Turkey</option>
								<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
								<option value="Tuvalu">Tuvalu</option>
								<option value="Uganda">Uganda</option>
								<option value="Ukraine">Ukraine</option>
								<option value="United Araba Emirates">United Araba Emirates</option>
								<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
								<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
								<option value="Vatican City">Vatican City</option>
								<option value="Venezuela">Venezuela</option>
								<option value="Vietnam">Vietnam</option>
								<option value="Virgin Islands">Virgin Islands</option>
								<option value="Wallis and Futuna">Wallis and Futuna</option>
								<option value="Western Sahara">Western Sahara</option>
								<option value="Yemen">Yemen</option>
								<option value="Yugoslavia">Yugoslavia</option>
								<option value="Zambia">Zambia</option>
								<option value="Zimbabwe">Zimbabwe</option>
							</select>
						</div>
						<div style="margin:20px 0 20px 10px;display:inline-block;float:right">
							<input type="button" id="searchAddress" value="Search Address" style="background:#ECB83A;color:#fff;border:1px solid #E5A000;cursor:pointer" />
						</div>
					</div>
					<div style="padding:0 0 10px 0;display:none;float:left;" id="addressContainer">
						<p>Suggessted Address</p>
						<div id="suggesstions">
							
						</div>
					</div>
				</fieldset>				
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>Telephone - Landline<em>*</em></p>
						<input name="landline" type="text" style="width:294px;" id="landline" />
					</div>
					<div style="float:left;">
						<p>Telephone - Mobile</p>
						<input name="mobile" type="text" style="width:294px;" id="mobile"/>
					</div>
				</div>
				<div style="font-size:12px;margin:0 0 10px 0">
					<b style="color: rgb(255, 75, 75);">* Please provide either passport or national identity card number</b>
				</div>
				<fieldset style="border:1px solid #ccc;padding:0 8px;margin-bottom:15px;">
				<legend style="color:#E6A300">Passport</legend>
				<div class="row">
					<div style="float:left">
						<p>Issued by (country)<em>*</em></p>
						<select name="passportCountry" id="passportCountry" onChange="passportMask();" style="width:330px">
							<option value="" selected='selected'>-- Select Country of Issue --</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="USA">USA</option>
							<option value="Australia">Australia</option>
							<option value="Spain">Spain</option>
						</select><br />
						<span style="font-family: arial; margin: 5px 0px; color: rgb(255, 75, 75); clear: both; font-size: 9px;display:inline-block">
							Select country and then enter the passport number.
						</span>
					</div>
					<div style='float:right'>
						<img src="images/premier/uk_passport.jpg" alt=" " width="230px"/><br />
						<span style="font-family: arial; margin: 3px 0px; color: rgb(255, 75, 75); float: right; clear: both; font-size: 9px;">
							Please put in the passport number given in the bottom.
						</span>
					</div>
					<div style="float:left;margin-top:50px;">
						<p>Passport Number</p>
						<input name="passportNumber" type="text" size="62" id="passportNumber" style="padding:0 2px;text-transform:uppercase;font-size:11px;" disabled="true"/>
						<div id="passportAvailability">
							
						</div>
					</div>
				</div>
				<div class="row">
					<div style="margin:10px 0 0 0;width:375px;float:left">
						<div style="float:left;margin-right:20px;width:178px">
							<p>Passport Expiry Date<em>*</em></p>
							<input name="passportExpiry" id="passportExpiry" value="" readonly="readonly" style="margin-right:5px;width:140px"/>
						</div>
						<div>
							<p>Issue Date</p>
							<input name="passportIssue" id="passportIssue" type="text" readonly="readonly" style="margin-right:5px;width:150px"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>Passport Upload</p>
						<a href="javascript:void();" id="file">Select Document</a>
						<input type="file" name="passportDoc" id="passportDoc" style="visibility:hidden;width:25px;" />
						<div style="color: rgb(255, 75, 75);font-size:10px;font-family:arial;line-height:15px;text-align:justify">* Please upload a passport copy.If you cannot do this please e-mail or post a copy to us as soon as possible. Max file size 2 MB</div>
					</div>
				</div>
				</fieldset>
				<fieldset style="border:1px solid #ccc;padding:0 8px;">
				<legend style="color:#E6A300">National Identity Card</legend>
				<div class="row">
					<div style="float:left;margin-right:25px;">
						<p>Line1</p>
						<input name="line1" type="text" size="38" id="line1" style="text-transform:uppercase"/>
					</div>
					<div style="float:left">
						<p>Line2</p>
						<input name="line2" id="line2" size="40" type="text" style="text-transform:uppercase"/>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:25px;overflow:hidden;padding-top:2px;width:266px;">
						<p>Line3</p>
						<input name="line3" id="line3" type="text" size="30" maxlength="30" style="text-transform:uppercase;width:250px;"/>
						<div id="NICAvailability">
							
						</div>
					</div>
					<div style="float:left">
						<p>Expiry Date <em>*</em></p>
						<input name="idExpiry" id="idExpiry" type="text" readonly="readonly"/>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>Country of Nationality</p>
						<select name="nationalityCountry" id="nationalityCountry" style="width:270px">
							<option value="" selected='selected'>-- Select Originating Country --</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="Germany">Germany</option>
							<option value="USA">USA</option>
							<option value="Canada">Canada</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'Ivoire">Cote D'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
							<option value="Denmark">Denmark</option>
							<option value="Djiboutio">Djiboutio</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Escocia">Escocia</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadaloupe">Guadaloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kitts and Nevis">Kitts and Nevis</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marinique">Marinique</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia">Micronesia</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Monserrat">Monserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option><option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Island">Pitcairn Island</option>
							<option value="Poland">Poland</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa-American">Samoa-American</option>
							<option value="Samoa-Western">Samoa-Western</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakie">Slovakie</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkemenistan">Turkemenistan</option>
							<option value="Turkey">Turkey</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Araba Emirates">United Araba Emirates</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Virgin Islands">Virgin Islands</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Yugoslavia">Yugoslavia</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
					<div style="float:left">
						<p>Country of Issue</p>
						<select name="issueCountry" id="issueCountry">
							<option value="" selected='selected'>-- Select Country of Issue --</option>
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="Germany">Germany</option>
							<option value="USA">USA</option>
							<option value="Canada">Canada</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'Ivoire">Cote D'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
							<option value="Denmark">Denmark</option>
							<option value="Djiboutio">Djiboutio</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Escocia">Escocia</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadaloupe">Guadaloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kitts and Nevis">Kitts and Nevis</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marinique">Marinique</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia">Micronesia</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Monserrat">Monserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option><option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Island">Pitcairn Island</option>
							<option value="Poland">Poland</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa-American">Samoa-American</option>
							<option value="Samoa-Western">Samoa-Western</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakie">Slovakie</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkemenistan">Turkemenistan</option>
							<option value="Turkey">Turkey</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Araba Emirates">United Araba Emirates</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Virgin Islands">Virgin Islands</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Yugoslavia">Yugoslavia</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
				</div>
				</fieldset>
				<div class="row">
					<div style="float:left;margin-right:50px;margin-top:20px">
						<p>How did you hear about us?</p>
					</div>
					<div style="float:right">
						<textarea name="heardAboutUs" cols="50" rows="2"></textarea>
					</div>
				</div>
				<div style="margin-top:20px;overflow:hidden;padding:5px 0;text-align:center">
					<div>
						<input type="submit" name="register" value="Register" id="register" style="background:#ECB83A;border:1px solid #E5A000;color:#fff;font-size:14px;cursor:pointer;padding-bottom:5px"/>
					</div>
				</div>
				<div id="msg" style="text-align:center">
					
				</div>
			</form>
		</div>
	</div>
</body>
</html>
