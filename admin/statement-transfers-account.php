<?
session_start();
include ("../include/config.php");
include ("security.php");
include ("calculateBalance.php");

$agentType = getAgentType();

$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

//debug($_REQUEST);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

$intUserId = $_REQUEST["agentID"];
$strSubmit = $_REQUEST["Submit"];

if($intUserId != "")
	$_SESSION["agentID2"] = $intUserId;
else 
	$_SESSION["agentID2"] = "";

$today = date("Y-m-d");
$msg = "";


//debug($insertQuery);


if($strSubmit == "Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_REQUEST["fMonth"];
	$_SESSION["fDay"]=$_REQUEST["fDay"];
	$_SESSION["fYear"]=$_REQUEST["fYear"];
	
	$_SESSION["tMonth"]=$_REQUEST["tMonth"];
	$_SESSION["tDay"]=$_REQUEST["tDay"];
	$_SESSION["tYear"]=$_REQUEST["tYear"];
	
	$_SESSION["closingBalance"] = "";
	$_SESSION["openingBalance"] = "";
	$_SESSION["currDate"] = "";	
	
}


if ($offset == "")
	$offset = 0;
$limit=20;
if ($_REQUEST["newOffset"] != "") {
	$offset = $_REQUEST["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

$condition = false;
if($agentType == "admin") 
{
	$condition = true;
	$intUserId = $_REQUEST["agentID"];
}
else
	$intUserId = $_SESSION["loggedUserData"]["userID"];

?>
<html>
<head>
	<title>Statement of Account</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">


var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
.boundingTable {
	border: solid 2px #666666;
}

.inputBoxesNumerics {
	text-align: right;
	width: 70px;
	font-size: 11px;
}

.inputBoxesText {
	width: 255px;
	font-size: 11px;
}

.inputBoxesCustom {
	font-size: 11px;
}

.dropDownBoxes {
	font-size: 11px;
}

.reportToolbar {
	background-color: #EEEEEE;
}

.errorMessage {
	color: #FF0000;
	font-weight: bold;
	font-size: 12px;
}

.successMessage {
	color: #009900;
	font-weight: bold;
	font-size: 12px;
}

.fieldComments {
	color: #999999;
}

.tdTitle { 
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#0033CC;
	padding-left:4px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">Statement of Account</td>
  </tr>
	<tr>
      <td width="100%">
	<form action="" method="post" name="Search" id="Search">
  		<div align="center">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
					SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
				</script>
			 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
				</script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
               <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			   ?>
               </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
				</script>
                <br>
                &nbsp;&nbsp; 
                <? 
					if($condition) 
					{ 
						$agentQuery  = "
										select 
											userID,
											username, 
											agentCompany, 
											name, 
											agentContactPerson,
											adminType 
										from 
											".TBL_ADMIN_USERS." 
										where 
											    parentID > 0 
											AND adminType='Agent' 
											AND isCorrespondent = 'Y' 
											AND agentStatus='Active' 
										order by 
											adminType, 
											name	";
						//debug($agentQuery);
						$arrStaff = selectMultiRecords($agentQuery);
						//debug($arrStaff);
				?>
				<br />
				<select name="agentID" style="font-family:verdana; font-size: 11px">
					<option value="">- Select One -</option>
                	<?
						for($i=0; $i < count($arrStaff); $i++)
						{
							if($arrStaff[$i]["adminType"] != $arrStaff[$i-1]["adminType"])
								echo "<optgroup label='".$arrStaff[$i]["adminType"]."'>";
							
							if($intUserId == $arrStaff[$i]["userID"]) { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" selected="selected"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } else { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } 

							if($arrStaff[$i]["adminType"] != $arrStaff[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
                </select>
                 <? } ?>
				<br />				 
				<input type="hidden" id="latestCleared" name="latestCleared" value="" />
                <input type="submit" name="Submit" id="Submit" value="Search" /> 
              </div>
        
		<tr>
		<td>
		
      <?
	 if(isAnD($intUserId)){
	 	$ledgerTable = TBL_AnD_ACCOUNT;
	 }
	 else{
	 	$ledgerTable = TBL_AGENT_ACCOUNT;
	 }
	 if(!empty($strSubmit))
	 {
	 	
	 	$Balance="";
		if($strSubmit == "Search")
			$slctID = $intUserId;
	
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			

		if ($slctID !=""){
			 $settlementCurr  = selectFrom("select settlementCurrency from admin  where 1 and userID=".$slctID." ");	
       		 $settleCurr=$settlementCurr["settlementCurrency"];  
			
			$accountQuery = "Select * from ".$ledgerTable." where agentID = $slctID";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= " and status != 'Provisional' ";
			$accountQuery .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
				
			$accountQueryCnt = "Select count(*) from ".$ledgerTable." where agentID = $slctID";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			$accountQueryCnt .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
			
			$allCount = countRecords($accountQueryCnt);

			$accountQuery .= "order by aaID";
	  	$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);

			$accountQueryCurrency = "Select DISTINCT(currency) as totalCurrency from ".$ledgerTable." where agentID = $slctID";
			$accountQueryCurrency.= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";

			$contentsAccCurr = selectMultiRecords($accountQueryCurrency);
			$totalCurrRec    = count($contentsAccCurr);
			//debug($accountQuery);
		}else{
		
			$slctID = $changedBy;
			$accountQuery="Select * from ".$ledgerTable." where agentID = $changedBy";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= " and status != 'Provisional' ";
			$accountQuery .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
				
			$accountQueryCnt = "Select count(*) from ".$ledgerTable." where agentID = $changedBy";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			$accountQueryCnt .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= "order by aaID";
		    $accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);

			$accountQueryCurrency = "Select DISTINCT(currency) as totalCurrency from ".$ledgerTable." where agentID = $slctID";
			$accountQueryCurrency .= " and $queryDate";				
			$accountQueryCurrency .= " and status != 'Provisional' ";
			$accountQueryCurrency .= " and (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') ";
			$contentsAccCurr = selectMultiRecords($accountQueryCurrency);
			$totalCurrRec    = count($contentsAccCurr);
		}
		//debug($accountQuery);
		

		?>	
			
		  <table width="76%" border="1" cellpadding="0" bordercolor="#666666" align="center">
		  	<? if(!empty($msg)) 
			   { 
			   		$parts = explode("|", $msg);
			?>
					<tr>
						<td align="center" class="<?=$parts[1]?>">
							<?=$parts[0]?>
						</td>
					</tr>
			<? 
			  } 
			
			 $strExtraUrlParams = "&fDay=".$_REQUEST["fDay"]."&fMonth=".$_REQUEST["fMonth"]."&fYear=".$_REQUEST["fYear"].
			 					  "&tDay=".$_REQUEST["tDay"]."&tMonth=".$_REQUEST["tMonth"]."&tYear=".$_REQUEST["tYear"]."&agentID=".$intUserId."&Submit=Search";
			
			
			?>
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount?>
					<?php } ?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?=$PHP_SELF."?newOffset=0&total=first".$strExtraUrlParams?>"><font color="#005b90">First</font></a></td>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$prv&total=pre".$strExtraUrlParams?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$nxt&total=next".$strExtraUrlParams?>"><font color="#005b90">Next</font></a>&nbsp;</td>
				  <td width="50" align="right">&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
				$rowSpan="";
				if($totalCurrRec>1){
				$rowSpan="rowspan='2'";
				}
			?>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="100%" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF" valign="baseline">
					  <td align="left"  width="91" <?=$rowSpan?>><span class="style1">Voucher #</span></td>
					  <td align="left" width="101" <?=$rowSpan?>><span class="style1">Date</span></td>
					  <td align="center" width="254" <?=$rowSpan?>><span class="style1">Narration</span></td>
					  <td align="center"  width="105" <?=$rowSpan?>><span class="style1">Opening Balance</span></td>
					  <?
					  	$colSpan="";
					  	if($totalCurrRec>1){
							$colSpan = "colspan='".($totalCurrRec)."'";
						}
					  ?>
					  <td align="center" width="112" <?=$colSpan;?>><span class="style1">Debit</td>
					  <td align="center" width="120" <?=$colSpan?>>Credit</td>
		  			  <td align="center" width="103" <?=$rowSpan?>><span class="style1">Closing Balance</span></td>
					</tr>
					  <? 
					  	if($totalCurrRec>1){
							$printCurr = "<tr bgcolor='#FFFFFF'>";
							for($curr=0;$curr<$totalCurrRec;$curr++){
								$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
							}
							for($curr=0;$curr<$totalCurrRec;$curr++){
								$printCurr .= "<td align='center'><span class='style1'>".$contentsAccCurr[$curr]["totalCurrency"]."</span></td>";
							}
							$printCurr .= "</tr>";
							echo $printCurr;
					  	}
				
				$preDate = "";
				$openingBalanceSwap = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					$currDate = $contentsAcc[$i]["dated"];
					//$openingBalance = 0;
					$openingBalanceSwap = $closingBalance;
					if($i==0){
						// Total Transfer Withdraw Amount before first date.
						$summaryWithdrawSQL = "SELECT sum(amount) as openingAmountWith FROM " . $ledgerTable . " WHERE agentID = '$slctID' and dated <'".$currDate."' ";						$summaryWithdrawSQL .= " AND (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') AND type='WITHDRAW'";
						$summaryFirstWithdraw = selectFrom($summaryWithdrawSQL);
						// Total Transfer Deposit Amount before first date.
						$summaryDepositSQL = "SELECT sum(amount) as openingAmountDep FROM " . $ledgerTable . " WHERE agentID = '$slctID' and dated <'".$currDate."' ";
						$summaryDepositSQL .= " AND (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') AND type='DEPOSIT'";
						$summaryFirstDeposit = selectFrom($summaryDepositSQL);
						// Difference of Total Withdraw and Deposit is Opening for First date.
						$openingBalance =  $summaryFirstDeposit["openingAmountDep"]-$summaryFirstWithdraw["openingAmountWith"];
						$closingBalance = $openingBalance;
						$openingBalanceSwap = $openingBalance;
					}
					else{
						if($preDate==$currDate){
							$openingBalance =  $openingBalance;
						}
						else{
							$openingBalance =  $closingBalance;		
						}
						$openingBalanceSwap = $closingBalance;
					}
					$closingBalance =  $openingBalanceSwap;

					$currentAmount = $contentsAcc[$i]["amount"];
					$preDate = $currDate;
					$_SESSION["currDate"] = $currDate;
				?>
				<tr bgcolor="#FFFFFF">
					<td align="left" <?=$rowSpan;?>><strong><?=$contentsAcc[$i]["TransID"];?></strong></td>

					<td align="left" <?=$rowSpan;?>><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td align="left" <?=$rowSpan;?>><?=$contentsAcc[$i]["description"];?></td>
					      <td align="center" <?=$rowSpan;?>><? echo number_format($openingBalance,2,'.',','); ?></td> 
					  
					  <?
						if($contentsAcc[$i]["type"] == "WITHDRAW")
						{
							for($cu=0;$cu<$totalCurrRec;$cu++){
								if(strtoupper($contentsAccCurr[$cu]["totalCurrency"])==strtoupper($contentsAcc[$i]["currency"])){
									echo "<td align='center' ".$rowSpan.">".$currentAmount."  ".$contentsAccCurr[$cu]["totalCurrency"]."</td>";
								}
								else{
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
								}
							}
						}
						else{
							for($cu=0;$cu<$totalCurrRec;$cu++){
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
							}
						}
 
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						for($cu=0;$cu<$totalCurrRec;$cu++){
							if(strtoupper($contentsAccCurr[$cu]["totalCurrency"])==strtoupper($contentsAcc[$i]["currency"])){
								echo "<td align='center' ".$rowSpan.">".customNumberFormat($currentAmount)." ".$contentsAccCurr[$cu]["totalCurrency"]."</td>";
							}
							else{
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
							}
						}
					}
					else{
						for($cu=0;$cu<$totalCurrRec;$cu++){
								echo "<td align='center' ".$rowSpan.">&nbsp;</td>";
						}
					}
					?>

			<? 
				if($contentsAcc[$i]["type"] == "WITHDRAW")
				{
					$closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
				}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
				{
					$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
				}
				?>
				<td align="center" <?=$rowSpan;?>><? echo(number_format($closingBalance,2,'.',',')); ?></td>


		</tr>
			<? 
				 $totalColspan=7;
				if($totalCurrRec>1){
					$totalColspan +=$totalCurrRec;
			?>
			<tr bgcolor="#FFFFFF"></tr>
			<? }?>

		<? } ?>
		<tr bgcolor="#FFFFFF">
		   <td colspan='"<?=$totalColspan;?>"'>&nbsp;</td>
		</tr>
			</table>		
            <? } else { ?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
			<? } ?>

<?php /*?>	  		<? if($agentType == "admin") { ?>
			<tr bgcolor="#FFFFFF">
              <td colspan="15" align="left">
				  		<a href="#" onClick="$('#manual-row').show();" style="color:#0066FF">Manual Deposit/Withdraw into AnD account?</a>
					&nbsp;
			  </td>
            </tr>
			<? } ?><?php */?>
       </table>
	   
	<?	
		 } 
		 else 
		 {
			$_SESSION["grandTotal"]="";
			$_SESSION["CurrentTotal"]="";
	 
	
			$_SESSION["fMonth"]="";
			$_SESSION["fDay"]="";
			$_SESSION["fYear"]="";
			
			$_SESSION["tMonth"]="";
			$_SESSION["tDay"]="";
			$_SESSION["tYear"]="";
		}
	?>
		</td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
	<? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>