<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//include ("javaScript.php");
$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
?>
<html>
<head>
<title>Manage Alert</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<!--<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />-->
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.inlineedit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var extraParams;
	jQuery(document).ready(function(){
		var lastSel;
		jQuery("#getAlertList").jqGrid({
			url:'manageAlertControllerAction.php?getGrid=getAlertList',
			datatype: "json",
			height: 210,
			width: 700, 
			colNames:[
				'Sr.', 
				'ID Type', 
				'User Type',
				'Alert Type',
				'Days',
				'Status',
				'Message'
			],
			colModel:[
				{name:'sr',			index:'sr', 		width:50,	align:"center"},
				{name:'id_type_id',	index:'id_type_id', width:80,	align:"left",	editable:true, editrules: {required: true }, edittype:"select", editoptions:{value:"-1:All<? foreach($arrIDTypeData as $kay=>$val){echo ";".$val["id"].":".$val["title"];}?>"}},
				{name:'userType',	index:'userType',	width:80,	align:"left",	editable:true, editrules: {required: true }, edittype:"select", editoptions:{value:"All:All;Customer:Customer;Beneficiary:Beneficiary;Agent:Agent"}},
				{name:'alertType',	index:'alertType', 	width:80,	align:"left",	editable:true, editrules: {required: true }, edittype:"select", editoptions:{value:"Expired Ids:Expired Ids;Future Expiry:Future Expiry"}},
				{name:'day',		index:'day', 		width:50,	align:"right",	editable:true, editrules: {required: true, number:true, minValue:"1"}, edittype:"text", editoptions:{size:25}},
				{name:'status',		index:'status',		width:50, 	align:"left",	editable:true, editrules: {required: true }, edittype:"select", editoptions:{value:"Y:Enable;N:Disable"}},
				{name:'message',	index:'message', 	width:290, 	align:"left",	editable:true, editrules: {required: true }, edittype:"textarea"}
			],
			imgpath:gridimgpath, 
			rowNum: 10,
			rowList: [5,10,20,50,100,200],
			pager: jQuery('#pagernavT'),
			multiselect:true,
			sortname: "id_type_id",
			sortorder: "ASC",
			loadtext: "Loading, please wait...",
			editurl: 'manageAlertControllerAction.php',
			caption: "Manage Alert"
		}).navGrid('#pagernavT',
				{edit:true, add:false, del:true, search:false},
				{afterSubmit: editMessage, closeAfterEdit:true}, 	// edit options
      			{}, 	// add options
      			{} 		// del options
			);
	});
	
	
	// define handler function for 'afterSubmit' event.
	var editMessage = function(response, postdata){
		var json   = response.responseText;		// response text is returned from server.
		var result = JSON.parse(json);			// convert json object into javascript object.
		return [result.status,result.message,null]; 
	}
	
	
	
	function gridReload(grid)
	{
		var theUrl = "manageAlertControllerAction.php?";		
		var extraParam='';
		var id_type_id	= jQuery("#id_type_id").val();
		var alertType	= jQuery("#alertType").val();
		var userType 	= jQuery("#userType").val();
		var status 		= jQuery("#status").val();
		
		if(grid=='getAlertList')
		{
			extraParam = extraParam + "&id_type_id="+id_type_id+"&alertType="+alertType+"&userType="+userType+"&status="+status+"&getGrid="+grid+"&Submit=searchAlerts";
		}
		else
		{
			extraParam = extraParam + "&getGrid=getAlertList";
		}
		jQuery("#getAlertList").setGridParam({
			url: theUrl+extraParam,
			page:1
		}).trigger("reloadGrid");
	}
	
	function Export(action)
	{
		var id_type_id	= jQuery("#id_type_id").val();
		var alertType	= jQuery("#alertType").val();
		var userType 	= jQuery("#userType").val();
		var status 		= jQuery("#status").val();		
		window.open ("export_alert_controller.php?act="+action+"&id_type_id="+id_type_id+"&alertType="+alertType+"&userType="+userType+"&status="+status,"Export Alerts");
	}
	
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

#searchTable
{
	background-color:#ededed;
	border:1px solid #000000;
}
#searchTable tr td
{
	border-top:1px solid #FFFFFF;
}

-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
<div style=" margin:5px; padding-left:5px; height:30px; line-height:30px;background-color:#6699cc; color:#FFFFFF"><strong>Manage Alert<strong></div>
	<form name="frmSearch" id="frmSearch">
	<table width="100" border="0" align="center">
		<tr>
			<td>
				<table id="searchTable" align="center" border="0" cellpadding="5" bordercolor="#666666">
					<tr>
						<th align="left"colspan="4" bgcolor="#6699cc"><span class="tab-u">Search Alert</span></th>
					</tr>
					<tr>
						<td nowrap align="right">ID Type</td>
						<td><select id="id_type_id" name="id_type_id" style="font-family:verdana; font-size: 11px">
								<option value="">- Select -</option>
								<option value="-1">All</option>
								<?php
								if(CONFIG_DB_ID_TYPES == "1")
								{
									foreach($arrIDTypeData as $key=>$val)
									{
									?>
										<option value="<?=$val["id"];?>"><?=$val["title"];?></option>
									<?php
									}
								}
								else
								{?>				
									<option value="Passport">Passport</option>
									<option value="ID Card">ID Card</option>
									<option value="Driving License">Driving License</option>
									<option value="Other ID">Other ID</option>
									<option value="MSB">MSB Expiry</option>
								<?php
								}
								?>				
						  </select>
						</td>
						<td nowrap align="right">Alert Type</td>
						<td><select id="alertType" name="alertType" style="font-family:verdana; font-size: 11px">
								<option value="">- Select -</option>
								<option value="">- All -</option>
								<option value="Expired Ids">Expired Ids</option>				
								<option value="Future Expiry">Future Expiry</option>
							</select>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">User Type</td>
						<td><select id="userType" name="userType" style="font-family:verdana; font-size: 11px">
								<option value="">- Select -</option>
								<option value="All">All</option>
								<option value="Agent">Agent</option>
								<option value="Beneficiary">Beneficiary</option>				
								<option value="Customer">Customer</option>
							</select>
						</td>
						<td nowrap align="right">Status</td>
						<td><select id="status" name="status" style="WIDTH: 140px;HEIGHT: 18px; ">
								<option value="">- Select -</option>
								<option value="">- All -</option>
								<option value="Y">Enable</option>						
								<option value="N">Disable</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="4" nowrap align="center">
							<input type="button" name="Submit" value="Search" onClick="gridReload('getAlertList')">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table id="getAlertList" border="0" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<input type="button" name="Submit" value="Print All" onClick="Export('print')">
				<input type="button" name="Submit" value="Export All" onClick="Export('export')">
			</td>
		</tr>
	</table>
</form>
</body>
</html>