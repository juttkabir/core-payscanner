<?php
/**
 * @package : online registered users
 * @subpackage: issue password processing to senders
 * @author: Mirza Arslan Baig
 */
session_start();
include ("../include/config.php");
require_once("lib/phpmailer/class.phpmailer.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
//debug($_POST);
function generateRandomString($length = 6) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
if((isset($_REQUEST['customer']) && !empty($_REQUEST['customer'])) || (isset($_GET['customer']) && !empty($_GET['customer']))){
	if(isset($_REQUEST['customer']) && !empty($_REQUEST['customer']) && is_array($_REQUEST['customer'])){
		//echo json_encode($_REQUEST['customer']);
		$strCustomerIDs = implode(",", $_REQUEST['customer']);
		$strQueryCustomers = "SELECT Mobile, email, customerID, custCategory FROM ".TBL_CUSTOMER." WHERE customerID IN ($strCustomerIDs)";
		$arrCustomers = selectMultiRecords($strQueryCustomers);
		for($x = 0; $x < count($arrCustomers); $x++){
			$password = generateRandomString();
			if($arrCustomers[$x]['custCategory'] == 'W')
			{
				$strUpdatePassword = "UPDATE ".TBL_CUSTOMER." SET `password` = '$password',custCategory='B' WHERE customerID = ".$arrCustomers[$x]['customerID']."";
			}
			else
			{
				$strUpdatePassword = "UPDATE ".TBL_CUSTOMER." SET `password` = '$password' WHERE customerID = ".$arrCustomers[$x]['customerID']."";
			}
			update($strUpdatePassword);
			//debug($strUpdatePassword);
			$strQueryCustomer = "SELECT cust.Mobile AS mobile, accountName, cust.email AS email, cust.customerID AS customerID, cust.customerName AS customerName,cust.firstName AS firstName,cust.lastName AS lastName FROM ".TBL_CUSTOMER." AS cust WHERE customerID = '".$arrCustomers[$x]['customerID']."'";
			$arrCustomer = selectFrom($strQueryCustomer);

			$messageContent = selectFrom("select message from sms_templates where eventID = 5 and status = 'Enable'");
			$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='5' and email_templates.status = 'Enable' ");
			$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
			$message1 = $emailContent["body"];
			$varEmail = array("{customer}","{email}","{password}","{logo}","{accountName}");
			$varPassword = array("{password}");
			$adminFname= explode(" ",$arrCustomer["adminName"]);
			$countName=count($adminFname);
			if($countName>2){
				$fullNameAccM=$adminFname[0]." ".$adminFname[$countName-1];

			}
			else{
				$fullNameAccM=$adminFname[0]." ".$adminFname[1];

			}
			//$contentEmail = array($arrCustomer['firstName']." ".$arrCustomer['lastName'],$arrCustomer['email'],$password,$fullNameAccM,$logoCompany);
			$contentEmail = array($arrCustomer['firstName'],$arrCustomer['email'],$password,$logoCompany,$arrCustomer['accountName']);
			$contentPassword = array($password);
			$messageT = str_replace($varEmail,$contentEmail,$message1);
			$message = str_replace($varPassword, $contentPassword, $messageContent["message"]);

			//$subject = "Issue Password";
			$subject = $emailContent["subject"];
			$customerMailer = new PHPMailer();
			$strBody = $messageT;
			/*$strBody = "Dear ".$arrCustomer[$x]['customerName']."\r\n<br />";
            $strBody .= "Your Trading Account password has been set.\n<br />
            Your username is: ".$arrCustomer[$x]['email']."\r\n<br />
            Your new password is: [ ".$password." ]\r\n\n\r<br /><br />
            \n<br />".$arrCustomer[$x]["adminName"]."<br />\n
            Follow this link to login: http://clients.premfx.com/private_registration.php\n<br />
            <table>
                <tr>
                    <td align='left' width='30%'>
                    <img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width='20%'>&nbsp;</td>
                    <td align='left'>
                        <font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
                        55 Old Broad Street,London, EC2M 1RX.\n<br>
                        <font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
                        <font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
                        Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
                        <font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
                        <font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
                        <font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
                        C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
                        <font color='#F7A30B'>Tel:</font> +34 971 576 724
                         \n <br>
                         <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
                    </td>
                </tr>
            </table>
            ";*/
			//$fromName  = SYSTEM;
			$fromName  = $emailContent["fromName"];
			$fromEmail  = $emailContent["fromEmail"];

			$customerMailer->FromName =  $fromName;
			//$customerMailer->From =  $arrCustomer[$x]['adminEmail'];
			$customerMailer->From =  $fromEmail;
			if(CONFIG_SEND_EMAIL_ON_TEST == "1")
			{
				$customerMailer->AddAddress(CONFIG_TEST_EMAIL,'');
				$number = $arrCustomer['mobile'];
				if ($number != '' && strlen($number) == 13 && substr($number, 0, 1) == '+')
					sendSMS($message, $number);
			}
			else
			{
				$number = $arrCustomer['mobile'];
				if ($number != '' && strlen($number) == 13 && substr($number, 0, 1) == '+')
					sendSMS($message, $number);
				$customerMailer->AddAddress($arrCustomer['email'],'');
				if($emailContent['cc'])
					$customerMailer->AddAddress($emailContent['cc'],'');

				//Add BCC
				if($emailContent['bcc'])
					$customerMailer->AddBCC($emailContent['bcc'],'');

			}
			$customerMailer->Subject = $subject;
			$customerMailer->IsHTML(true);
			$customerMailer->Body = $strBody;

			if(CONFIG_EMAIL_STATUS == "1")
			{
				$custEmailFlag = $customerMailer->Send();
			}

		}
	}else{
		if (isset($_REQUEST['customer']) && !empty($_REQUEST['customer']))
			$intCustomerID = $_REQUEST['customer'];
		else
			$intCustomerID = $_GET['customer'];

		$password = generateRandomString();

		$strQueryCustomer = "SELECT cust.Mobile AS mobile, cust.email AS email,accountName,Address,Phone, cust.customerID AS customerID,cust.custCategory,cust.firstName AS firstName,cust.parentID  AS parentID ,cust.lastName AS lastName, cust.customerName AS customerName FROM ".TBL_CUSTOMER." AS cust WHERE customerID = $intCustomerID";
		//debug($strQueryCustomer);
		$arrCustomer = selectFrom($strQueryCustomer);

		if($arrCustomer['custCategory'] == 'W')
		{
			$strUpdatePassword = "UPDATE ".TBL_CUSTOMER." SET `password` = '$password',custCategory='B' WHERE customerID = '$intCustomerID'";
		}else{
			$strUpdatePassword = "UPDATE ".TBL_CUSTOMER." SET `password` = '$password' WHERE customerID = '$intCustomerID'";
		}
		update($strUpdatePassword);
		$messageContent = selectFrom("select message from sms_templates where eventID = 5 and status = 'Enable'");
		$emailContent = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='5' and email_templates.status = 'Enable'");// AND agentID='".$arrCustomer['userID']."'
		$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
		$message1 = $emailContent["body"];
//	$varEmail = array("{customer}","{email}","{password}","{admin}","{logo}","{accountName}","{address}","{phone}");
		$varEmail = array("{customer}","{password}","{email}","{address}","{phone}");
		$varPassword = array("{password}");
		$adminFname= explode(" ",$arrCustomer["adminName"]);
		$countName=count($adminFname);
		if($countName>2){
			$fullNameAccM=$adminFname[0]." ".$adminFname[$countName-1];

		}
		else{
			$fullNameAccM=$adminFname[0]." ".$adminFname[1];

		}

		//$contentEmail = array($arrCustomer['firstName']." ".$arrCustomer['lastName'],$arrCustomer['email'],$password,$fullNameAccM,$logoCompany);
//	$contentEmail = array($arrCustomer['firstName'],$arrCustomer['email'],$password,$fullNameAccM,$logoCompany,$arrCustomer['accountName'],$arrCustomer['Address'],$arrCustomer['Phone']);
		$contentEmail = array($arrCustomer['firstName'],$password,$arrCustomer['email'],$arrCustomer['Address'],$arrCustomer['Phone']);
		$contentPassword = array($password);
		$messageT = str_replace($varEmail,$contentEmail,$message1);
		$message = str_replace($varPassword, $contentPassword, $messageContent["message"]);
		//debug($arrCustomer);
		//$subject = "Password Issued";
		$subject = $emailContent["subject"];
		$customerMailer = new PHPMailer();
		$strBody = $messageT;
		/*$strBody = "Dear ".$arrCustomer['customerName']."\r\n<br /><br />\r\n";
        $strBody .= "You have issued the passowrd your login details are as under:<br /><br />\r\n\n
        Login Link : http://clients.premfx.com/private_registration.php\r\n<br />
        Username: ".$arrCustomer['email']."\r\n<br />
        Password: ".$password."\r\n\n\r<br /><br />
        \n<br />".$arrCustomer["adminName"]."<br />\n
        <table>
            <tr>
                <td align='left' width='30%'>
                <img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width='20%'>&nbsp;</td>
                <td align='left'>
                    <font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
                    55 Old Broad Street,London, EC2M 1RX.\n<br>
                    <font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
                    <font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
                    Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
                    <font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
                    <font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
                    <font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
                    C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
                    <font color='#F7A30B'>Tel:</font> +34 971 576 724
                     \n <br>
                     <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
                </td>
            </tr>
        </table>
        ";*/
		//debug($strBody);
		//$fromName  = SYSTEM;
		$fromName  = $emailContent["fromName"];
		$fromEmail  = $emailContent["fromEmail"];

		$customerMailer->FromName =  $fromName;
		//$customerMailer->FromName =  $fromName;
		//$customerMailer->From =  $arrCustomer['adminEmail'];
		$customerMailer->From =  $fromEmail;
		if(CONFIG_SEND_EMAIL_ON_TEST == "1")
		{
			$customerMailer->AddAddress(CONFIG_TEST_EMAIL,'');
			$number = $arrCustomer['mobile'];
			if ($number != '' && strlen($number) == 13 && substr($number, 0, 1) == '+')
				sendSMS($message, $number);
		}
		else
		{
			$number = $arrCustomer['mobile'];
			if ($number != '' && strlen($number) == 13 && substr($number, 0, 1) == '+')
				sendSMS($message, $number);
			$customerMailer->AddAddress($arrCustomer['email'],'');
			if($emailContent['cc'])
				$customerMailer->AddAddress($emailContent['cc'],'');

			//Add BCC
			if($emailContent['bcc'])
				$customerMailer->AddBCC($emailContent['bcc'],'');
		}
		$customerMailer->Subject = $subject;
		$customerMailer->IsHTML(true);
		$customerMailer->Body = $strBody;
		if(CONFIG_EMAIL_STATUS == "1")
		{
			$custEmailFlag = $customerMailer->Send();
		}
	}
	//header("LOCATION: issue_password_customer.php");
	echo "<p style='color:#1F8E23'>Password is issued to the customer</p>";
}
?>
<!--<meta http-equiv="refresh" content="2;url=http://--><?php //echo $_SERVER['HTTP_HOST']."/admin/issue_password_customer.php" ?><!--">-->