<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
define("PIC_SIZE","51200");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}
$_SESSION["IDAcountry"] = (is_array($_POST["IDAcountry"]) ? implode(",", $_POST["IDAcountry"]) :"");
$_SESSION["authorizedFor"] = (is_array($_POST["authorizedFor"]) ? implode(",", $_POST["authorizedFor"]) :"");



/*
if($_POST["supAgentID"] != '' )
	$supAgentID = $_SESSION["supAgentID"];
else*/
	$supAgentID = 0;

/*echo $supAgentID;
echo "<br>";

exit();*/
//echo  implode(",", $_POST["IDAcountry"]);
//echo implode(" ", $arr );
//exit;
if ($_POST["userID"] == ""){
	session_register("agentCompany");
	session_register("agentContactPerson");
	session_register("agentAddress");
	session_register("agentAddress2");
	session_register("postcode");
	session_register("agentCountry");
	session_register("agentCity");
	session_register("agentZip");
	session_register("agentPhone");
	session_register("agentFax");
	session_register("email");
	session_register("agentURL");
	session_register("agentMSBNumber");
	session_register("msbMonth");
	session_register("msbDay");
	session_register("msbYear");
	session_register("agentCompRegNumber");
	session_register("agentCompDirector");
	session_register("agentDirectorAdd");
	session_register("agentProofID");
	session_register("otherProofID");
	session_register("idMonth");
	session_register("idDay");
	session_register("idYear");
	session_register("agentDocumentProvided");
	session_register("agentBank");
	session_register("agentAccountName");
	session_register("agentAccounNumber");
	session_register("agentBranchCode");
	session_register("agentAccountType");
	session_register("agentCurrency");
	session_register("agentAccountLimit");
	session_register("agentCommission");
	session_register("agentStatus");
	$_SESSION["agentCompany"] = $_POST["agentCompany"];
	$_SESSION["agentContactPerson"] = $_POST["agentContactPerson"];
	$_SESSION["agentAddress"] = $_POST["agentAddress"];
	$_SESSION["agentAddress2"] = $_POST["agentAddress2"];
	$_SESSION["postcode"] = $_POST["postcode"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["agentZip"] = $_POST["postcode"];
	$_SESSION["agentPhone"] = $_POST["agentPhone"];
	$_SESSION["agentFax"] = $_POST["agentFax"];
	$_SESSION["email"] = $_POST["email"];
	$_SESSION["agentURL"] = $_POST["agentURL"];
	$_SESSION["agentMSBNumber"] = $_POST["agentMSBNumber"];
	$_SESSION["msbMonth"] = $_POST["msbMonth"];
	$_SESSION["msbDay"] = $_POST["msbDay"];
	$_SESSION["msbYear"] = $_POST["msbYear"];
	$_SESSION["agentCompRegNumber"] = $_POST["agentCompRegNumber"];
	$_SESSION["agentCompDirector"] = $_POST["agentCompDirector"];
	$_SESSION["agentDirectorAdd"] = $_POST["agentDirectorAdd"];
	$_SESSION["agentProofID"] = $_POST["agentProofID"];
	$_SESSION["otherProofID"] = $_POST["otherProofID"];
	$_SESSION["idMonth"] = $_POST["idMonth"];
	$_SESSION["idDay"] = $_POST["idDay"];
	$_SESSION["idYear"] = $_POST["idYear"];
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");
	$_SESSION["agentBank"] = $_POST["agentBank"];
	$_SESSION["agentAccountName"] = $_POST["agentAccountName"];
	$_SESSION["agentAccounNumber"] = $_POST["agentAccounNumber"];
	$_SESSION["agentBranchCode"] = $_POST["agentBranchCode"];
	$_SESSION["agentAccountType"] = $_POST["agentAccountType"];
	$_SESSION["agentCurrency"] = $_POST["agentCurrency"];
	$_SESSION["agentAccountLimit"] = $_POST["agentAccountLimit"];
	$_SESSION["agentCommission"] = $_POST["agentCommission"];
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
	$_SESSION["isCorrespondent"] = $_POST["correspondent"];
	//$_SESSION["IDAcountry"] = implode(",", $_POST["IDAcountry"]);
	$backURL = "add-admin-manager.php?msg=Y";
} else {
	$backURL = "update-admin-manager.php?userID=$_POST[userID]&msg=Y";
}
if (trim($_POST["agentCompany"]) == ""){
	insertError(AG1);
	redirect($backURL);
}
if (trim($_POST["agentContactPerson"]) == ""){
	insertError(AG2);
	redirect($backURL);
}
if (trim($_POST["agentAddress"]) == ""){
	insertError(AG3);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(AG4);
	redirect($backURL);
}
/*
if (trim($_POST["City"]) == ""){
	insertError(AG5);
	redirect($backURL);
}
if (trim($_POST["agentZip"]) == ""){
	insertError(AG6);
	redirect($backURL);
}*/
if (trim($_POST["agentPhone"]) == ""){
	insertError(AG7);
	redirect($backURL);
}/*
if(!email_check(trim($_POST["email"]))){
	insertError(AG8);
	redirect($backURL);
}*/
if (trim($_POST["agentCompDirector"]) == ""){
	insertError(AG9);
	redirect($backURL);
}
if (trim($_POST["agentBank"]) == ""){
	insertError(AG10);
	redirect($backURL);
}
if (trim($_POST["agentAccountName"]) == ""){
	insertError(AG11);
	redirect($backURL);
}
if (trim($_POST["agentAccounNumber"]) == ""){
	insertError(AG12);
	redirect($backURL);
}




if ($_POST["userID"] == ""){


	$password = createCode();
	/*if($supAgentID != "")
	{
		$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID = $supAgentID");
		$agentNumber = selectFrom("select MAX(subagentNum) from ".TBL_ADMIN_USERS." where parentID='".$_POST["supAagentID"]."'");
		$agentCode = $agentDetails["username"]."-".($agentNumber[0]+1);
		$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, email, created,
		 parentID, subagentNum, agentCompany, agentContactPerson, agentAddress, agentAddress2,  agentCity, agentZip, agentCountry, 
		 agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber, agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		  agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber, 
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, agentType, accessFromIP, logo, authorizedFor, postCode) VALUES 
		('$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."', 
		'".getCountryTime(CONFIG_COUNTRY_CODE)."', 0, '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."', 
		'".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		'".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["postcode"]."', '".$_POST["Country"]."', 
		'$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', '".checkValues($_POST["agentURL"])."',
		 '".checkValues($_POST["agentMSBNumber"])."', '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
		 '".checkValues($_POST["agentCompRegNumber"])."', '".checkValues($_POST["agentCompDirector"])."', 
		 '".checkValues($_POST["agentDirectorAdd"])."', '".checkValues($_POST["agentProofID"])."', 
		 '".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', '".$_SESSION["agentDocumentProvided"]."', 
		 '".checkValues($_POST["agentBank"])."', 
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		'".checkValues($_POST["agentAccountLimit"])."', '".checkValues($_POST["commPackage"])."', 
		'".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', 'N', 
		'".$_SESSION["IDAcountry"]."', 'Sub', '".checkValues($_POST["accessFromIP"])."', '$Pict', 
		'".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."')";
		
	}
	else
	{ */
		$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
		$strQuery = "select max(userID) as maxid from admin";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		$rstRow = mysql_fetch_array($nResult);
 		$supAgentID = ($rstRow['maxid']+1);
		$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, email, created, parentID, 
		agentNumber, agentCompany, agentContactPerson, agentAddress, agentAddress2, agentCity, agentZip, agentCountry, 
		agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber, agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		 agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, accessFromIP, authorizedFor, postCode,adminType) VALUES 
		('$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '0', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		  '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		  '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["agentZip"]."', '".$_POST["Country"]."', 
		  '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		  '".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."',
		   '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		   '".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', 
		   '".checkValues($_POST["agentProofID"])."', '".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', 
		   '".$_SESSION["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', 
		'".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		'".checkValues($_POST["agentAccountLimit"])."', '".checkValues($_POST["commPackage"])."', 
		'".checkValues($_POST["agentCommission"])."', 'Active', 'N', 
		'".$_SESSION["IDAcountry"]."', '".checkValues($_POST["accessFromIP"])."', '".checkValues($_SESSION["authorizedFor"])."','".$_POST["postcode"]."','Admin Manager')";

	//}

	insertInto($Querry_Sqls);
	
	////To record in History
	$insertedID = @mysql_insert_id();
	$descript = ""Admin Manager is added;
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);
	$_SESSION["supAgentID"] = '';
	// PICTURE UPLOAD CODE
	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict = strtolower($agentCode.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($agentCode . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='$agentCode'");
		}
	}

	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Admin Account creation at $company";
	$message = "Congratulations! ".$_POST["name"].",\n\n
		Your agent account has been created at $company agent module. Your login information is as follows:\n\n
		Login: ".$agentCode."\n\n
		Password: ".$password."\n\n
		Name: ".$_POST["agentContactPerson"]."\n\n
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a>\n\n

		$company Support";
	sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail);
	$_SESSION["agentCompany"] = "";
	$_SESSION["agentContactPerson"] = "";
	$_SESSION["agentAddress"] = "";
	$_SESSION["agentAddress2"] = "";
	$_SESSION["postcode"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["agentZip"] = "";
	$_SESSION["agentPhone"] = "";
	$_SESSION["agentFax"] = "";
	$_SESSION["email"] = "";
	$_SESSION["agentURL"] = "";
	$_SESSION["agentMSBNumber"] = "";
	$_SESSION["msbMonth"] = "";
	$_SESSION["msbDay"] = "";
	$_SESSION["msbYear"] = "";
	$_SESSION["agentCompRegNumber"] = "";
	$_SESSION["agentCompDirector"] = "";
	$_SESSION["agentDirectorAdd"] = "";
	$_SESSION["agentProofID"] = "";
	$_SESSION["otherProofID"] = "";
	$_SESSION["idMonth"] = "";
	$_SESSION["idDay"] = "";
	$_SESSION["idYear"] = "";
	$_SESSION["agentDocumentProvided"] = "";
	$_SESSION["agentBank"] = "";
	$_SESSION["agentAccountName"] = "";
	$_SESSION["agentAccounNumber"] = "";
	$_SESSION["agentBranchCode"] = "";
	$_SESSION["agentAccountType"] = "";
	$_SESSION["agentCurrency"] = "";
	$_SESSION["agentAccountLimit"] = "";
	$_SESSION["agentCommission"] = "";
	$_SESSION["agentStatus"] = "";
	if($_POST["supAagentID"] != "")
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Supper",'Sub', eregi_replace("Agent",'IDA', AG34)). " The login name for the agent is <b>$agentCode</b> and password is <b>$password</b>");		
		}
		else
		{
			insertError(eregi_replace("Supper",'Sub', AG34). " The login name for the Branch Manager is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}
	else
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Agent",'IDA', AG34). " The login name for the Branch Manager is <b>$agentCode</b> and password is <b>$password</b>");		
		}
		else
		{
			insertError(AG34 . " The login name for the Branch Manager is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}

		insertError( ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'IDA', AG34) : AG34). " The login name for the Branch Manager is <b>$agentCode</b> and password is <b>$password</b>");
} else {
	if (trim($_POST["oldCountry"]) != trim($_POST["Country"])){
		$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
		$agentAcountUpdate = "username='$agentCode', agentNumber='".($agentNumber[0]+1)."', ";
	}
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");

	 $Querry_Sqls = "update ".TBL_ADMIN_USERS." set $agentAcountUpdate 
	 name='".$_POST["agentContactPerson"]."', 
	 email='".checkValues($_POST["email"])."', 
	 agentCompany='".checkValues($_POST["agentCompany"])."', 
	 agentContactPerson='".checkValues($_POST["agentContactPerson"])."', 
	 agentAddress='".checkValues($_POST["agentAddress"])."', 
	 agentAddress2='".checkValues($_POST["agentAddress2"])."',	 
	 agentCity='".$_POST["City"]."', 
	 agentZip='".$_POST["agentZip"]."', 
	 agentCountry='".$_POST["Country"]."', 
	 agentCountryCode='$ccode', 
	 agentPhone='".checkValues($_POST["agentPhone"])."', 
	 agentFax='".checkValues($_POST["agentFax"])."', 
	 agentURL='".checkValues($_POST["agentURL"])."', 
	 agentMSBNumber='".checkValues($_POST["agentMSBNumber"])."', 
	 agentMCBExpiry='".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
	 agentCompRegNumber='".checkValues($_POST["agentCompRegNumber"])."', 
	 agentCompDirector='".checkValues($_POST["agentCompDirector"])."', 
	 agentDirectorAdd='".checkValues($_POST["agentDirectorAdd"])."', 
	 agentProofID='".checkValues($_POST["agentProofID"])."', 
	 agentIDExpiry='".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', 
	 agentDocumentProvided='".$_SESSION["agentDocumentProvided"]."', 
	 agentBank='".checkValues($_POST["agentBank"])."', 
	 agentAccountName='".checkValues($_POST["agentAccountName"])."', 
	 agentAccounNumber='".checkValues($_POST["agentAccounNumber"])."', 
	 agentBranchCode='".checkValues($_POST["agentBranchCode"])."', 
	 agentAccountType='".$_POST["agentAccountType"]."', 
	 agentCurrency='".$_POST["agentCurrency"]."', 
	 agentAccountLimit='".checkValues($_POST["agentAccountLimit"])."', 
	 commPackage = '".$_POST["commPackage"]."',
	 agentCommission='".checkValues($_POST["agentCommission"])."', 
	 agentStatus='".$_POST["agentStatus"]."', 
	 isCorrespondent = 'N',
	 IDAcountry = '".$_SESSION["IDAcountry"]."',
	 accessFromIP = '".checkValues($_POST["accessFromIP"])."',
	 authorizedFor = '".checkValues($_SESSION["authorizedFor"])."',
	 adminType  = 'Admin Manager',
	 postCode='".checkValues($_POST["postcode"])."'
	 where userID='".$_POST[userID]."'";

	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict=strtolower($username.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($_POST["usern"] . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='".$_POST["usern"]."'");
		}
	}

	update($Querry_Sqls);
	$descript ="Admin Manager is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);
	//echo("\n\n Query is updated");
	insertError(AG33);
}
$backURL .= "&success=Y";
redirect($backURL);
?>