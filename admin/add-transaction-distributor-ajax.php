<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	/*
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
		
	if ($_GET["newOffset"] != "")
	{
		$offset = $_GET["newOffset"];
	}
	*/
		
	//$nxt = $offset + $limit;
	//$prv = $offset - $limit;
	/**
	$sortBy = $_GET["sortBy"];
	
	if ($sortBy == "")
	{
		$sortBy = " transDate";
	}
	**/
	
	$transType   = "";
	$transStatus = "";
	$Submit      = "";
	$transID     = "";
	$by          = "";
		
	if(!empty($_REQUEST["Submit"]))
	{
		$Submit = $_REQUEST["Submit"];
	}
			
	if(!empty($_REQUEST["transID"]))
	{
		$transID = $_REQUEST["transID"];
	}
	
	if(!empty($_REQUEST["searchBy"]))
	{
		$by = $_REQUEST["searchBy"];
	}
	
	//$trnsid = $_REQUEST["trnsid"];
	$strForeignAmountCurrency = $_REQUEST["foreignAmountCurrency"];
	$strLocalAmountCurrency = $_REQUEST["localAmountCurrency"];
	
	//$ttrans = $_REQUEST["totTrans"];
	
	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")	
		$strBankTableSelect = ", bankDetails as d ";
	
	if(!empty($_REQUEST["btnAction"]))
	{
		/**
		 * updatiung the transactions with the distributor(s)
		 */
		//if(!empty($_REQUEST["distribut"]) && count($_REQUEST["transBenIds"]) > 0)
		if(!empty($_REQUEST["distribut"]) && !empty($_REQUEST["transIds"]))
		{
			/**
			 * Getting the parent Id of the agent to store in transactions table
			 */	
			$benParentIdRs = selectFrom("select parentID from ".TBL_ADMIN_USERS." where userID = ".$_REQUEST["distribut"]);
			$arrTransIds = explode(",", $_REQUEST["transIds"]);
			
			//foreach ($_REQUEST["transBenIds"] as $dk => $dv)
			for($x=0; $x<sizeof($arrTransIds); $x++)
			{
				/**
				 * Cheking which trasactions are assigned to distributors
				 * If they contain any distributor then update the transaction 
				 */
				if(!empty($arrTransIds[$x]))	
				{
					$updateTransSql = "Update ".TBL_TRANSACTIONS." set benAgentID=".$_REQUEST["distribut"].", benAgentParentID = ".$benParentIdRs["parentID"] ." where transID=".$arrTransIds[$x];

					update($updateTransSql);
					activities($_SESSION["loginHistoryID"],"UPDATION",$arrTransIds[$x],TBL_TRANSACTIONS,"Updating the distributor to ".$_REQUEST["distribut"]);	
				}
			}
		}
	}
	
	$query = "select * from ". TBL_TRANSACTIONS . " as t $strBankTableSelect where 1";
	$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t $strBankTableSelect where 1";
	
	if($Submit == "Search")
	{
		if(!empty($transID))
		{
			switch($by)
			{
				case 1:
					$query = "select * from ". TBL_TRANSACTIONS . " as t $strBankTableSelect where 1 ";
					$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t $strBankTableSelect where 1 ";
					
					$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
					$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
					break;

				case 2:
					$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c $strBankTableSelect where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
					$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c $strBankTableSelect where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
								
					$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
					$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
					break;

				case 3:
					$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b $strBankTableSelect where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
					$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b $strBankTableSelect where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
						
					$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
					$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
					break;
			}
		}
	}
	
	if(empty($_REQUEST["fYear"]) || empty($_REQUEST["fMonth"]) || empty($_REQUEST["fDay"]))
		$fromDate = date("Y-m-d");
	else
		$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
		
	if(empty($_REQUEST["tYear"]) || empty($_REQUEST["tMonth"]) || empty($_REQUEST["tDay"]))
		$toDate = date("Y-m-d");
	else
		$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	
	$filters = " and (t.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";
	
	/* Filter option for Total Amount */
	if(!empty($_REQUEST["totalAmount"]))
	{
		if($_REQUEST["totalAmountCondition"] == "gt")
			$strTotalAmountCond = ">";
		elseif($_REQUEST["totalAmountCondition"] == "lt")
			$strTotalAmountCond = "<";
		else
			$strTotalAmountCond = "=";
			
		//debug($_REQUEST["totalAmountCondition"] ." * ".$strTotalAmountCond);
			
		$filters .= " and t.totalAmount ".$strTotalAmountCond." '".trim($_REQUEST["totalAmount"])."' ";
	}
	
	/* Filter Option for Local Total Amount */
	if(!empty($_REQUEST["totalLocalAmount"]))
	{
		if($_REQUEST["localAmountCondition"] == "gt")
			$strLocalAmountCond = ">";
		elseif($_REQUEST["localAmountCondition"] == "lt")
			$strLocalAmountCond = "<";
		else
			$strLocalAmountCond = "=";

		
		$filters .= " and t.localAmount ".$strLocalAmountCond." '".trim($_REQUEST["totalLocalAmount"])."' ";
	}
	
	/* bank Name search filter */
	if(!empty($_REQUEST["bankName"]))
	{
		$filters .= " and d.bankName LIKE '".trim($_REQUEST["bankName"])."%' ";
	}
	
	/* Filter option for created by */
	if(!empty($_REQUEST["createdBy"]))
	{
		$filters .= " and t.addedBy = '".trim($_REQUEST["createdBy"])."' ";
	}
	
	/* Filter option for banking type i.e. normal or 24hr banking */
	if(!empty($_REQUEST["bankType"]))
	{
		$filters .= " and t.bankingType = '".trim($_REQUEST["bankType"])."' ";
	}
	
	/**
	 *  Changing the logic to assign distributor 
	 *  Now in this queue only transaction which are in authorize status will appear
	 *	$query .= " and (t.transStatus ='Pending' and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";
	 *	$queryCnt .= " and (t.transStatus ='Pending' and t.transType = 'Bank Transfer' and t.benAgentID = 0) ";
	 */
	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")	
	{ 
		$query .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.transType = 'Bank Transfer' and t.benAgentID = 0) and d.transID = t.transID";
		$queryCnt .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.transType = 'Bank Transfer' and t.benAgentID = 0) and d.transID = t.transID";
	}
	else
	{
		$query .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.benAgentID = 0) ";
		$queryCnt .= " and ((t.transStatus ='Authorize' or t.transStatus ='Amended') and t.benAgentID = 0) ";
	}

	/**
	 * Inserting the local and foreign currency 
	 */
	if(!empty($strLocalAmountCurrency) && !empty($strForeignAmountCurrency))
	{
		$query .= " and t.currencyFrom ='$strLocalAmountCurrency' and t.currencyTo = '$strForeignAmountCurrency' ";
		$queryCnt .= " and t.currencyFrom ='$strLocalAmountCurrency' and t.currencyTo = '$strForeignAmountCurrency' ";
	}
	//$queryCnt .= " group by t.transID ";
	
	/* Search Filters */
	$query .= $filters;
	$queryCnt .= $filters;
	$query .= " group by t.transID ";
	$queryCnt .= " group by t.transID ";
	//$allCount = countRecords($queryCnt);
	//$count = $allCount;
	//debug($query);
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	//debug($_REQUEST);
	//debug($query);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());

	//debug($queryCnt);

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($result))
	{
		if($row["createdBy"] == "CUSTOMER")
		{
			$customerContent = selectFrom("select FirstName as firstName, LastName as lastName from cm_customer where c_id ='".$row["customerID"]."'");  
			$beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$row["benID"]."'");
			$custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
			$createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
		} else {
			$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$row["customerID"]."'");
			$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$row["benID"]."'");
			$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$row["custAgentID"]."'");
			$createdBy = ucfirst($agentContent["name"]);
		}
		
		$response->rows[$i]['id'] = $row["transID"];
		
		if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")
		{
			$bankNameSql = "select bankName from bankDetails where transID = ".$row["transID"];
			$bankNameData = selectFrom($bankNameSql);
			
			$response->rows[$i]['cell'] = array(
										dateFormat($row["transDate"], "2"),
										$row["refNumberIM"],
										$row["refNumber"],
										$row["transStatus"],
										$row["totalAmount"]." ".$row["currencyFrom"],
										$row["localAmount"]." ".$row["currencyTo"],
										ucfirst($customerContent["firstName"])." ".ucfirst($customerContent["lastName"]),
										ucfirst($beneContent["firstName"])." ".ucfirst($beneContent["lastName"]),
										ucfirst($bankNameData["bankName"]),
										$createdBy
									);
		}
		else
		{
			$response->rows[$i]['cell'] = array(
												dateFormat($row["transDate"], "2"),
												$row["refNumberIM"],
												$row["refNumber"],
												$row["transStatus"],
												$row["totalAmount"]." ".$row["currencyFrom"],
												$row["localAmount"]." ".$row["currencyTo"],
												ucfirst($customerContent["firstName"])." ".ucfirst($customerContent["lastName"]),
												ucfirst($beneContent["firstName"])." ".ucfirst($beneContent["lastName"]),
												$createdBy
											);		
		}
		
		$i++;
	}
	
	echo $response->encode($response); 

?>