<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($_POST["feeID"] == "")
{
	$_SESSION["origCountry"] = $_POST["origCountry"];
	$_SESSION["destCountry"] = $_POST["destCountry"];	
	$_SESSION["amountRangeFrom"] = $_POST["amountRangeFrom"];
	$_SESSION["amountRangeTo"] = $_POST["amountRangeTo"];
	$_SESSION["Chrages"] = $_POST["Charges"];
	$_SESSION["chargeType"] = $_POST["chargeType"];
	$_SESSION["agentID"] = $_POST["agentID"];
	$_SESSION["chargeBasedOn"] = $_POST["chargeBasedOn"];
	$_SESSION["currencyDest"] = $_POST["currencyDest"];
	$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];

}
?>
<html>
<head>
	<title>Add Bank Charges</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	if(theForm.origCountry.options.selectedIndex == 0){
		alert("Please select originating Country.");
		theForm.origCountry.focus();
		return false;
	}

	if(theForm.destCountry.options.selectedIndex == 0){
		alert("Please select Destination Country.");
		theForm.destCountry.focus();
		return false;
	}

	if(theForm.amountRangeFrom.value == "" || IsAllSpaces(theForm.amountRangeFrom.value)){
    	alert("Please provide Lower range of money.");
        theForm.amountRangeFrom.focus();
        return false;
    }
	if(theForm.amountRangeTo.value == "" || IsAllSpaces(theForm.amountRangeTo.value)){
    	alert("Please provide Upper range of money.");
        theForm.amountRangeTo.focus();
        return false;
    }
    if(theForm.chargeType.options.selectedIndex == 0){
		alert("Please select Type for Charges.");
		theForm.chargeType.focus();
		return false;
	}
	if(theForm.Charges.value == "" || IsAllSpaces(theForm.Chrges.value)){
    	alert("Please provide Charges for spcified money ranges.");
        theForm.Charges.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
  function checkFocus(){
 	bankCharges.origCountry.focus();
} 
	// end of javascript -->
	</script>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Add Bank Charges</td>
  </tr>
  <form action="add-bank-charges-conf.php" method="post"onSubmit="return checkForm(this);" name="bankCharges">
  <tr>
      <td align="center"> 
        <table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="550" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add Bank Charges</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<font color="#FF0000">* Compulsory Fields</font>
			</td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Originating Country<font color="#ff0000">*</font></strong></font></td>
            <td width="210"><SELECT name="origCountry" id="origCountry" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Country-</OPTION>
				<?
					
                		$countryTypes = " and  countryType like '%origin%' ";
					
					if(CONFIG_COUNTRY_SERVICES_ENABLED){
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = fromCountryId  $countryTypes order by countryName");
							}
					else
						{
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
						}
				
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT>
              <script language="JavaScript">
		         			SelectOption(document.forms[0].origCountry, "<?=$_SESSION["origCountry"] ?>");
		          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($_SESSION["countryOrigin"] == $rstRow["country"] && $_SESSION["currencyOrigin"] == "")
									echo "<option selected value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.forms[0].currencyOrigin, "<?=$_SESSION["currencyOrigin"] ?>");
     	  </script>
        
        </tr>
        
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Destination Country<font color="#ff0000">*</font></strong></font></td>
          <td><SELECT name="destCountry" id="destCountry" style="font-family:verdana; font-size: 11px">
            <OPTION value="">- Select Country-</OPTION>
            <?
                        	
                		$countryTypes = " and  countryType like '%destination%' ";
	          
            if(CONFIG_COUNTRY_SERVICES_ENABLED){
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
							}
					else
						{
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
						}
					for ($i=0; $i < count($countires); $i++){
				?>
            <OPTION value="<?=$countires[$i]["countryName"]; ?>">
            <?=$countires[$i]["countryName"]; ?>
            </OPTION>
            <?
					}
				?>
          </SELECT> <script language="JavaScript">
		         			SelectOption(document.forms[0].destCountry, "<?=$_SESSION["destCountry"] ?>");
		          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Destination Currency<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<select name="currencyDest">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($selectedCurrency == $currency)
								{
									echo "<option value ='$currency' selected> ".$currency." --> ".$description."</option>";			
								}
								else
								{
								    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
								  }
							}
						  ?>				
				</select>
				 <script language="JavaScript">
     				SelectOption(document.forms[0].currencyDest, "<?=$_SESSION["currencyDest"]?>");
      	  </script>
        
        </tr>
        
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Charges For<font color="#ff0000">*</font></strong></font></td>
            <td width="210"><SELECT name="chargeBasedOn" id="chargeBasedOn" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='add-bank-charges.php'; document.forms[0].submit()">
                <OPTION value="Generic">Generic</OPTION>
                <OPTION value="Distributor">Distributor</OPTION>
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].chargeBasedOn, "<?=$_SESSION["chargeBasedOn"] ?>");
		       	  </script>
          </td>
      </tr>
     <? if($_SESSION["chargeBasedOn"]== "Distributor") {
     	$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'";
     	if (CONFIG_IDACOUNTRY_CHECK =='1'){
				$agentQuery .= " and IDAcountry like '%".$_SESSION["destCountry"]."%' ";
			}
				$agentQuery .= " order by agentCompany";				
				$agents = selectMultiRecords($agentQuery);			
			
     	 ?>
      <tr bgcolor="#ededed">
       		<td><font color="#005b90"><strong>Select Distributor</strong></font></td>
	       			<td>
	       				<select name="agentID" style="font-family:verdana; font-size: 11px">
								<option value="">- Select One -</option>
								<?
									 	for ($i=0; $i < count($agents); $i++){
									?>
								<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
								<?
										}
									?>
							  </select>
							  <script language="JavaScript">
		         			SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"] ?>");
		          	</script>
		          </td>
       			</tr>
      
      <? 
      } 
      ?>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="amountRangeFrom" value="<?=stripslashes($_SESSION["amountRangeFrom"]); ?>" size="10" maxlength="16"> To <input type="text" name="amountRangeTo" value="<?=stripslashes($_SESSION["amountRangeTo"]); ?>" size="10" maxlength="16"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong>Charges Type<font color="#ff0000">*</font></strong></font></td>
            <td width="210">
            	<SELECT name="chargeType" id="chargeType" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Charges Type -</OPTION>
								<option value="fixed">Fixed Charges</option>
								<option value="percent">Percentage of Amount</option>
							</SELECT>
              <script language="JavaScript">
		         			SelectOption(document.forms[0].chargeType, "<?=$_SESSION["chargeType"] ?>");
		       	  </script></td>
		    </tr>
        <tr bgcolor="#ededed">
            <td width="285"><font color="#005b90"><strong> Charges <font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="Charges" value="<?=stripslashes($_SESSION["Charges"]); ?>" size="15" maxlength="16"> <? echo ($_SESSION["feeType"] == 'percent' ? "<font color='#005b90'><strong>%</strong></font>" : "") ?></td>
        </tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>