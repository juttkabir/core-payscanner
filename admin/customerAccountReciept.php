<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//include("calculateBalance.php");
///////////////////////History is maintained via method named 'activities'

$custID = $_SESSION["custID"] = $_GET["custID"];
$insertedID = $_GET["insertedID"];

 $customerLedgerDataQuery = "select * from agents_customer_account where  caID=$insertedID";
$customerLedgerData = selectFrom($customerLedgerDataQuery);

 $customerDataQuery = "select firstName,lastName,middleName from customer where accountname = '$custID'";

$customerData = selectFrom($customerDataQuery);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<title>Reconcile Sender Balances</title>
<style type="text/css">

body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.companyName {
	font-weight: bold;
	font-size: 18px;
	letter-spacing: 5px;
	line-height: 10px;
}

.companyAddress {
	font-size: 10px;
}

.receiptTitle {
	font-weight: bold;
	font-size: 16px;
	height: 40px;
}

.receiptLayout {
	border: solid 1px #000000;
}

.fieldLines {
	border-bottom: solid 1px #000000;
	height: 30px;
	vertical-align: bottom;
}

.labels {
	vertical-align: bottom;
}
</style>
</head>

<body>


<table width="600" border="0" align="center" cellpadding="10" cellspacing="0" class="receiptLayout">
	<!-- Receipt Header Starts -->
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="25%"><img src="images/connectplus/company-logo.jpg" width="100" height="50" /></td>
				<td width="75%"><table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="right" class="companyName">GLOBAL EXCHANGE LTD. </td>
					</tr>
					<tr>
						<td align="right"><hr size="2" noshade="noshade" /></td>
					</tr>
					<tr>
						<td align="right" class="companyAddress">54 Ealing Road, Wembley HA0 4TQ<br />
							Ph: 0208 902 3366 &nbsp;Fax: 0208 902 9922<br />
							globalexchange@btclick.com</td>
					</tr>
				</table></td>
			</tr>
		</table></td>
	</tr>
	<!-- Receipt Header Ends -->
	<!-- Receipt Title Starts -->
	<tr>
		<td align="center" class="receiptTitle">Receipt Received In</td>
	</tr>
	<!-- Receipt Title Ends -->
	<!-- Receipt Body Starts -->
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="15%" align="center"><img src="images/globalexchange/customer-copy.GIF" width="27" height="150" /></td>
				<td width="85%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<!-- Receipt Details Start -->
					<tr>
						<td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="39%" class="labels">Transaction No. </td>
								<td width="61%" class="fieldLines"><? echo $customerLedgerData["refNo"]; ?></td>
							</tr>
							<tr>
								<td class="labels">Account No. </td>
								<td class="fieldLines"><? echo $_GET["custID"];?></td>
							</tr>
						</table></td>
						<td width="50%" align="right"><table width="75%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="15%" class="labels">Date</td>
								<td width="69%" class="fieldLines"><? echo $customerLedgerData["Date"]; ?></td>
							</tr>
							<tr>
								<td class="labels">Time</td>
								<td class="fieldLines"><? echo date("H:i:s"); ?></td>
							</tr>
						</table></td>
					</tr>
					<!-- Receipt Details End -->
					<tr>
						<td colspan="2">
							<br /></td>
					</tr>
					<!-- Amount Details Start -->
					<tr>
						<td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="108" class="labels">Received From </td>
								<td colspan="4" class="fieldLines"><? echo $customerData["firstName"]." ".$customerData["middleName"]." ".$customerData["lastName"];?></td>
							</tr>
							<tr>
								<td width="108" class="labels">The sum of Amount </td>
								<td width="94" class="fieldLines"><? echo $customerLedgerData["amount"];?></td>
								<td width="21" align="center" class="labels">in</td>
								<td width="82" class="fieldLines"><? echo $customerLedgerData["paymentType"]; ?></td>
								<td width="157" class="labels">in above mentioned Account </td>
							</tr>
							<tr>
								<td colspan="5" class="fieldLines"><? echo $customerLedgerData["note"]; ?></td>
							</tr>
						</table></td>
					</tr>
					<!-- Amount Details End -->
					<tr>
						<td colspan="2"></td>
					</tr>
					<!-- Signature Section Starts -->
					<tr>
						<td colspan="2"><table width="50%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="26%" class="labels">Signature</td>
								<td width="74%" class="fieldLines">&nbsp;</td>
							</tr>						
							
						</table></td>
					</tr>
					<!-- Signature Section Ends -->
					<tr>
						<td colspan="2">
							<br /></td>
					</tr>
				</table></td>
			</tr>
		</table></td>
	</tr>
	<!-- Receipt Body Ends -->
</table>
<br />
<table width="50%" border="0" cellspacing="0" cellpadding="3">
	<tr>
	<!--
		<tr> 
      
    <td height="19"><div class='noPrint'><a href="AgentsCustomerAccount.php?custID=<?=$_GET["custID"]?>&search=search" class="style2" ><font color="#000000">Go Back <? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?> Account</font></a></div></td>

	</tr>
	-->
</table>
<!-- <p>&nbsp;</p>  -->

<table width="597" height="437" border="0" align="center" cellpadding="10" cellspacing="0" class="receiptLayout">
	<!-- Receipt Header Starts -->
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="25%"><img src="images/connectplus/company-logo.jpg" width="100" height="50" /></td>
				<td width="75%"><table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="right" class="companyName">GLOBAL EXCHANGE LTD. </td>
					</tr>
					<tr>
						<td align="right"><hr size="2" noshade="noshade" /></td>
					</tr>
					<tr>
						<td align="right" class="companyAddress">54 Ealing Road, Wembley HA0 4TQ<br />
							Ph: 0208 902 3366 &nbsp;Fax: 0208 902 9922<br />
							globalexchange@btclick.com</td>
					</tr>
				</table></td>
			</tr>
		</table></td>
	</tr>
	<!-- Receipt Header Ends -->
	<!-- Receipt Title Starts -->
	<tr>
		<td align="center" class="receiptTitle">Receipt Received In</td>
	</tr>
	<!-- Receipt Title Ends -->
	<!-- Receipt Body Starts -->
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="15%" align="center"><img src="images/connectplus/office-copy.jpg" width="27" height="150" /></td>
				<td width="85%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<!-- Receipt Details Start -->
					<tr>
						<td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="39%" class="labels">Transaction No. </td>
								<td width="61%" class="fieldLines"><? echo $customerLedgerData["refNo"]; ?></td>
							</tr>
							<tr>
								<td class="labels">Account No. </td>
								<td class="fieldLines"><? echo $_GET["custID"];?></td>
							</tr>
						</table></td>
						<td width="50%" align="right"><table width="75%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="15%" class="labels">Date</td>
								<td width="69%" class="fieldLines"><? echo $customerLedgerData["Date"]; ?></td>
							</tr>
							<tr>
								<td class="labels">Time</td>
								<td class="fieldLines"><? echo date("H:i:s"); ?></td>
							</tr>
						</table></td>
					</tr>
					<!-- Receipt Details End -->
					<tr>
						<td colspan="2"><br />
					  </td>
					</tr>
					<!-- Amount Details Start -->
					<tr>
						<td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="108" class="labels">Received From </td>
								<td colspan="4" class="fieldLines"><? echo $customerData["firstName"]." ".$customerData["middleName"]." ".$customerData["lastName"];?></td>
							</tr>
							<tr>
								<td width="108" class="labels">The sum of Amount </td>
								<td width="94" class="fieldLines"><? echo $customerLedgerData["amount"];?></td>
								<td width="21" align="center" class="labels">in</td>
								<td width="82" class="fieldLines"><? echo $customerLedgerData["paymentType"]; ?></td>
								<td width="157" class="labels">in above mentioned Account </td>
							</tr>
							<tr>
								<td colspan="5" class="fieldLines"><? echo $customerLedgerData["note"]; ?></td>
							</tr>
						</table></td>
					</tr>
					<!-- Amount Details End -->
					<tr>
						<td colspan="2">
							<br /></td>
					</tr>
					<!-- Signature Section Starts -->
					<tr>
						<td colspan="2"><table width="50%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="26%" class="labels">Signature</td>
								<td width="74%" class="fieldLines">&nbsp;</td>
							</tr>
							
							
						</table></td>
					</tr>
					<!-- Signature Section Ends -->
					<tr>
						<td height="15" colspan="2">
					  <br /></td>
					</tr>
				</table></td>
			</tr>
		</table></td>
	</tr>
	<!-- Receipt Body Ends -->
</table>
<!-- <p>&nbsp;</p> -->
<table width="50%" border="0" cellspacing="0" cellpadding="3">
	<tr>      
    <td height="19"><div class='noPrint'><a href="AgentsCustomerAccount.php?custID=<?=$_GET["custID"]?>&search=search" class="style2" ><font color="#000000">Go Back <? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?> Account</font></a></div></td>
    <td><div class='noPrint'>
		<input type="button" name="Submit2" value="Print this Receipt" onClick="print()"></div></td>
	
  </tr>
	
</table>

</body>
</html>
