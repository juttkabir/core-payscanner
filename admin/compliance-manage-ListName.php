<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");
   	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
    $LastModified = date('Y-m-d h:i:s');
	$strMainCaption = "Manage Compliance List Name"; 

	$cmd = $_REQUEST["cmd"];
	
	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into complianceLists 
								(listName,Description,LastModified)
							 values
								('".addslashes($_REQUEST["txtlist"])."','".addslashes($_REQUEST["txtdes"])."',
								'".$LastModified."')";
			if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$listID = "";
		}
		
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			
			if(!empty($_REQUEST["listID"]))
			{
				$strUpdateSql = "update complianceLists 
								 set
									listName = '".addslashes($_REQUEST["txtlist"])."',
									Description = '".addslashes($_REQUEST["txtdes"])."',
									LastModified = '".$LastModified."'
								 where
								  listID = '".$_REQUEST["listID"]."'";
						
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$listID = "";
				$cmd = "";
						
			}	
		}
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["listID"]))
			{
				$strGetDataSql = "select listID,listName,Description,LastModified from complianceLists where listID='".$_REQUEST["listID"]."'";
				$arrQuestionData = selectFrom($strGetDataSql);
								
				$listID = $arrQuestionData["listID"];
				$txtlist = stripcslashes($arrQuestionData["listName"]);
				$txtdes = stripcslashes($arrQuestionData["Description"]);
				$LastModified = $arrQuestionData["LastModified"];
				$cmd = "UPDATE";
			}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["listID"]))
			{
				$strDelSql = "delete from complianceLists where listID='".$_REQUEST["listID"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
			}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}
	
  if(empty($cmd))
		$cmd = "ADD";
	
		
	/* Fetching the list of list name to display at the bottom */	
$arrAllQuestionData = selectMultiRecords("Select listID,listName,Description,LastModified from complianceLists order by listID");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Compliance List Name</title>
<script language="javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.cluetip.js"></script>
<script>
	$(document).ready(function() {
		$("#submitList").validate({
			rules: {
				txtlist: {
					required: true
				},
				txtdes: {
					required: true
				}
			},
			messages: {
				txtlist: {
					required: "<br />Please enter the list name."
				},
				txtdes: {
					required: "<br />Please enter the description."
				}
			}
		});
		
			
		
		$('img').cluetip({splitTitle:'|'});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
</style>
</head>
<body>
<form name="submitList" id="submitList" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>
			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!
				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination"><b>List Name:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
			<input type="text" name="txtlist" id="txtlist" size="40" value="<?=$txtlist?>" />
				
			</td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination">Description:&nbsp;<font color="red">*</font></td>
		<td align="left" width="60%"><input type="text" name="txtdes" id="txtdes" size="40" value="<?=$txtdes?>" />
		&nbsp;
				<img src="images/info.gif" border="0" title="Description | write here description." />
		</td>
		</tr>
			
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp; 
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="listID" value="<?=$listID?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($listID)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>
			</td>
		</tr>
	</table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;List Name&nbsp;
	</legend>
	<table width="95%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="20%">List Name</th>
			<th width="40%">Description</th>
			<th>Actions</th>
			<!--<th>&nbsp;</th>-->
		</tr>
		<?php
			foreach($arrAllQuestionData as $questionVal)
			{
			
							
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			
			<td><?=stripcslashes($questionVal["listName"])?></td>
			<td><?=stripcslashes($questionVal["Description"])?></td>
			<td><?=$questionVal["LastModified"]?></td>
			
		<!--<td>
				<a href="<?=$_SERVER['PHP_SELF']?>?listID=<?=$questionVal["listID"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit List Name| You can edit the list by clicking on this thumbnail." />
				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?listID=<?=$questionVal["listID"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove List|By clicking on this the list will be no longer available." />
				</a>
			</td>-->
		</tr>
				
		<?
			}
		?>
	</table>
</fieldset>

</body>
</html>