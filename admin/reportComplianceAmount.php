<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");
$agentType = getAgentType();

extract(getHttpVars());
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$reportName='reportComplianceAmount.php';
$reportLabel='Amount AML Report';
$reportFlag=true;
$executeQuery=false;
$filterValue=array();
$filterName=array();

$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

    $rfDay = $fDay;
	  $rfMonth =$fMonth;
	  $rfYear =$fYear;
	
	  $rtDay=$tDay;
	  $rtMonth =$tMonth;
	  $rtYear =	$tYear;

if($RID!=''){
	
	$reportFlag=false;
	$filterArray=array(); 
	 $value=array(); 
	 $finalArray=array();
		$compQuery = "select queryID,RID,filterName,filterValue from " . TBL_COMPLIANCE_QUERY_LIST ." where RID=".$RID."";
		
		$compRecord = selectMultiRecords($compQuery); 
		$compQueryMain = selectFrom("select note from " . TBL_COMPLIANCE_QUERY ." where RID=".$RID."");
				
		for($c=0; $c< count($compRecord) ; $c++){
		
		 $filterArray[$c]= $compRecord[$c]["filterName"];
		 $value[$c]= $compRecord[$c]["filterValue"];
		  $finalArray[$filterArray[$c]]=$value[$c];
		 
} 
	$Save=$finalArray["Save"];
	$Submit=$finalArray["Submit"];
	$amount=$finalArray["amount"];
	$amountCriteria=$finalArray["amountCriteria"];
	$fromDate=$finalArray["fromDate"];
	$toDate=$finalArray["toDate"];


}	  


if($Submit == "Search" || $Save == 'Save Report'){
 
 if($Submit!= ""){
	$filterValue[]=$Submit;
	$filterName[]='Submit';
}elseif($Save == 'Save Report'){
	$filterValue[]=$Save;
	$filterName[]='Save';
	}
	
 if ($_POST["note"] != "")
	{
		$note = $_POST["note"];
	}elseif ($_GET["note"] != "")
	{
		$note = $_GET["note"];	
	}
		 
	 if($fromDate!='' && $toDate!=''){
		$fD = explode("-",$fromDate);
		if (count($fD) == 3)
		{
				$rfDay = $fD[2];
				$rfMonth = $fD[1];
				$rfYear = $fD[0];
		}
				$tD = explode("-",$toDate);
		if (count($tD) == 3)
		{
				$rtDay = $tD[2];
				$rtMonth = $tD[1];
				$rtYear = $tD[0];
		}
	}	
		 $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
  	 $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;

	if ($agentType == "SUPA" || $agentType == "SUBA") {
		$query=selectMultiRecords("select customerID from customer where agentID = $userID");
		$customerIDs = "'-1'";
		foreach ($query as $row){
			$customerIDs .= ", '".$row["customerID"]."'";
		}
		$query="select transID,refNumberIM,refNumber,transDate,createdBy,benID,customerID,custAgentID,transStatus,transAmount from ".TBL_TRANSACTIONS." where 1 AND customerID IN ($customerIDs) ";

		$queryCnt="select count(transID) from ".TBL_TRANSACTIONS." where 1 AND customerID IN ($customerIDs)";
	} else {
		$query="select transID,refNumberIM,refNumber,transDate,createdBy,benID,customerID,custAgentID,transStatus,transAmount from ".TBL_TRANSACTIONS." where 1 ";

		$queryCnt="select count(transID) from ".TBL_TRANSACTIONS." where 1";
	}

if($amount!= ''){
	$query .=" and transAmount".$amountCriteria."".$amount." ";
	$queryCnt .=" and transAmount".$amountCriteria."".$amount." ";
	
	$filterValue[]=$amount;
	$filterName[]='amount';
	$filterValue[]=$amountCriteria;
	$filterName[]='amountCriteria';
}
if ($fromDate!= '' && $toDate!= ''){
	$query .=" and transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59'";
	$queryCnt .=" and transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59'";
		
		$filterValue[]=$fromDate;
	  $filterName[]='fromDate';
	  $filterValue[]=$toDate;
	  $filterName[]='toDate';
	}

 $allCount = countRecords($queryCnt);
$query .= " LIMIT $offset , $limit";
 $contentsTrans = selectMultiRecords($query);

if($Save == 'Save Report' && $reportFlag == true){
 	
  $Querry_Sqls="insert into ".TBL_COMPLIANCE_QUERY." (reportName,logedUser,logedUserID,queryDate,reportLabel,note) values ('".$reportName."','".$agentType."','".$loggedUserID."','".$date_time."','".$reportLabel."','".$note."')";
	insertInto($Querry_Sqls);
	$reportID = @mysql_insert_id();
	

	for($i=0;$i< count($filterName);$i++){
	$Querry_Sqls2="insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$filterName[$i]."','".$filterValue[$i]."')";
	insertInto($Querry_Sqls2); 
  }
	$msg="Report Has Been Saved";  
    
}


}
	
?>
<html>
<head>
	<title>Amount AML Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<!-- <script language="javascript" src="./javascript/compliance_ajax.js"></script> -->
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
    
 <script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {

		if(theForm.amount.value == "" || IsAllSpaces(theForm.amount.value)){
    	alert("Please Enter Amount.");
        theForm.amount.focus();
        return false;
    }
    	
    return true;
}

	function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
  	$(document).ready(function(){
	
		$("#exportAMLReport").change(function(){
			if($(this).val() != "")
			{
				var strUrl = "export_compliance_AML_report.php?exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()
							+"&amount="+$("#amount").val()+"&amountCriteria="+$("#amountCriteria").val()+"&Submit=Search";		
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
	
	$("#exportBtn").click(function(){
	  $("#exportAMLReport").show();
		});
	
	}); 
   -->
</script>   
</head>
<body>
	
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <form action="reportComplianceAmount.php" method="post" name="Search" onSubmit="return checkForm(this);" >
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Amount AML Report</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
   	  <td align="center">
          Amount <input type="text" name="amount" value=<?= $amount?> > 
          <select name="amountCriteria" id="amountCriteria">
            		<option value="=">Equal To</option>
            		<option value="<">Less Than</option>
            		<option value=">">Greater Than</option>
            		<option value="<=">Less Than or Equal To</option>
            		<option value=">=">Greater Than or Equal To</option>
            		<option value="!=">Not Equal To</option>
           </select>	 
           <script language="JavaScript">
		           	SelectOption(document.forms[0].amountCriteria, "<?=$amountCriteria ?>");
		     </script> 
                    
       </td>
      </tr>
		       <tr>
        <td align="center" nowrap>
		From
		<?
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					
           <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
					
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         		SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
				
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
      </tr>
     <tr> <td align="center">
	<!--	<input type="submit" name="Submit" value="Search" onClick="showReport(amountCriteria.value,amount.value,fMonth.value,fDay.value,fYear.value,tMonth.value,tDay.value,tYear.value)"> -->
		 <input type="submit" name="Submit" value="Search"> 
		</td>
    </tr>
	   
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666" id="txthint">
      <?
			if (count($contentsTrans) > 0){
			?>
			<tr>
	    	<td  bgcolor="#000000">
				
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">	
					<? if($msg!= '') { ?>
				    <tr>
					  	<td colspan="2" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></i></b><strong><? echo $msg; ?></strong></font></td>
				    </tr>
			   	<? } ?>	
	 					<tr>
							<td valign="center" align="right">Add Note/Comment</td>
							<td><textarea name="note" cols="40"><?=$compQueryMain["note"];?></textarea></td>
	        		
						</tr>  
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">  <input type="submit" name="Save" value="Save Report"> </td>
			</tr>

      <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
         
                <tr>
                  <td>
                    <?php if ($allCount > 0) {
                    
                    	?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ?>
                  </td>
                  
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>

    
    
         
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
	  <td width="85"><span class="style1"><strong><font color="#006699">Date</font></strong></span></td> 	
		<td><span class="style1"><strong><font color="#006699"><? echo 	$systemCode ?></font></strong></span></td>
    <td><span class="style1"><strong><font color="#006699"><? echo $manualCode ?></font></strong></span></td>      
    <td><span class="style1"><strong><font color="#006699">Status</font></strong></span></td>
    <td><span class="style1"><strong><font color="#006699">Trans Amount</font></strong></span></td>    
    <td><span class="style1"><strong><font color="#006699">Sender Name</font></strong></span></td>
    <td><span class="style1"><strong><font color="#006699">Beneficiary Name</font></strong></span></td>
    <td><span class="style1"><strong><font color="#006699">Created By</font></strong></span></td>
    <? if(CONFIG_AML_AMOUNT_REPORT_DOCUMENTS == "1") { ?>
    <td>&nbsp;</td>
	<? }?>
	
			 
			</tr>
		    <? 
		    
		    for($i=0;$i < count($contentsTrans);$i++)			{
		    
		  

					
			?>
				
				<tr bgcolor="#FFFFFF">
				
				<td> &nbsp;<a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong> 
            <font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2"); ?></font></strong></a>
            
          </td>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
          </td>
         <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumber"]; ?>
          </td>
        <td> &nbsp; 
            <?  echo $contentsTrans[$i]["transStatus"]; ?>
          </td>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["transAmount"]; ?>
          </td>
       <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");   
            }   
						if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
							$beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
							$otherClient->closeConnection();
							dbConnect();
						}else{
							$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
							$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
						}
				  	?>
				  	<td><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  	<td><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  	}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
            <? if(CONFIG_AML_AMOUNT_REPORT_DOCUMENTS == "1") { 
			?>
			  		<td bgcolor="#FFFFFF" align="center">
						<? if($contentsTrans[$i]["createdBy"]!="CUSTOMER"){?>
						<a class="style2" onClick="javascript:window.open('viewDocument.php?customerID=<?=$contentsTrans[$i]["customerID"]?>', 'ViewDocuments', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="#">
 	                        View Documents
 	                    </a>
						<? }else{?>
						&nbsp;
						<? }?>
					</td>
			  		<? } ?> 
       
     	
				
					</tr>
				<?
			}
			
			} // greater than zero
			
			?>
          </table></td>
        </tr>

     <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset + count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
          
        <tr>
					<td align="center" colspan="5">
					<input type="button" id="exportBtn" value="Export AML Report" />
					<input type="hidden" name ="from_date" id="from_date" value="<?=$fromDate?>" />
					<input type="hidden" name="to_date" id="to_date" value="<?=$toDate?>" />
					<input type="hidden" name="amount" id="amount" value="<?=$amount?>" />
					<input type="hidden" name="amountCriteria" id="amountCriteria" value="<?=$amountCriteria?>" />
					&nbsp;&nbsp;
					<select name="exportAMLReport" id="exportAMLReport" style="display:none">
					<option value="">Select Format</option>
					<option value="XLS">Excel</option>
					<option value="CSV">CSV</option>
					<option value="HTML">HTML</option>
				</select>
					</td>
				</tr>
          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		
      </table></td>
  </tr>
</form>
</table>
</body>
</html>