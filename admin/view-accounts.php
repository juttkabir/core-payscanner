<?
	session_start();

	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$currentdate = date('d-m-Y  h:i:s A');
	$loggedUserID = $_SESSION["loggedUserData"]["userID"];
	$sql_agent_based="";
if($agentType=='SUPA' ||  $agentType=='SUBA' )
{
$sql_agent_based=" AND userID='".$loggedUserID."'";
}


	//$currentdate = date('Y-m-d');
	$print = $_REQUEST["print"];
	$account = $_REQUEST["account"];
	$currency = $_REQUEST["currency"];
	//debug($_REQUEST);
	$strFilter = "";
	$strBalanceFilter = "";
	if(!empty($_REQUEST["date_from"]))
		$date_from	= $_REQUEST["date_from"];
	else
		$date_from = date('d/m/Y');

	if(!empty($_REQUEST["date_to"]))
	 	$date_to = $_REQUEST["date_to"];
	else
		$date_to = date('d/m/Y');

	if(!empty($date_from))
	{
		$date = explode("/",$date_from);
		$strFilter .= " AND created >= '".date("Y-m-d 00:00:00",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";

		$strDateFrom = " AND dated = '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}

	if(!empty($date_to))
	{
		$date = explode("/",$date_to);
		$strFilter .= " AND created <= '".date("Y-m-d 23:59:59",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
		$strDateTo = " AND dated = '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	if(!empty($account))
		 $strFilter .= "AND (accounNumber = '".$account."')";



   if(!empty($_REQUEST["Submit"]))
	{

		$strGetDataSql = "SELECT
							   id,
							   name,
							   accounNumber,
							   accounType,
							   balance,
							   currency
						  FROM
						  	 accounts
						  WHERE 1
						    ".$strFilter." ";
		//debug($strGetDataSql);
		$accountData = selectFrom($strGetDataSql);
		//$openingBalance = $accountData["balance"];
		$accounNumber = $accountData["accounNumber"];
		$id = $accountData["id"];
		$currency = $accountData["currency"];
		if(!empty($id))
		{
			$strAccount = "SELECT
								   currencyBuy,
								   currencySell,
								   amountBuy,
								   amountSell,
								   accountSell,
								   accountBuy,
								   exchangeRate,
								   description,
								   created
							  FROM
								 account_details
							  WHERE
								accountBuy = '".$accounNumber."' OR accountSell = '".$accounNumber."'";
					//debug($strAccount);
					$accountRs = selectMultiRecords($strAccount);

			$strCancelAccount = "SELECT
								   currencyBuy,
								   currencySell,
								   amountBuy,
								   amountSell,
								   accountSell,
								   accountBuy,
								   exchangeRate,
								   description,
								   note,
								   created
							  FROM
								 account_details
							  WHERE
								(accountBuy = '".$accounNumber."' OR accountSell = '".$accounNumber."') AND status = 'TC'";
					//debug($strCancelAccount);
					$accountCancellRs = selectMultiRecords($strCancelAccount);

	  $strOpeningBalance = selectFrom("SELECT
									id,
									opening_balance,
									currency
							  	FROM
									".TBL_ACCOUNT_SUMMARY."
								WHERE
									accountNumber = '".$account."'
									".$strDateFrom."
								");


	$openingBalance = $strOpeningBalance["opening_balance"];

	 $strClosingBalance = selectFrom("SELECT
									id,
									closing_balance,
									currency
							  	FROM
									".TBL_ACCOUNT_SUMMARY."
								WHERE
									accountNumber = '".$account."'
									".$strDateTo."
								");

	$closing_balance = $strClosingBalance["closing_balance"];
	  }
 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View Accounts</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
.ce{
	color:#0066FF;
	cursor:pointer;
}
</style>
<script src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  $("#date_from").datePicker({
		startDate: '<?=date("d/m/Y", strtotime("-2 years"))?>'
	});

	$("#date_to").datePicker({
		startDate: '<?=date("d/m/Y", strtotime("-2 years"))?>'
	});

});

function updateBalance(b)
{
	var banks = parseFloat(document.getElementById("banks").value);
	var closingBalance = parseFloat(document.getElementById("closingBalance").value);
	var final = 0;

	closingBalance = closingBalance + b;
	final = closingBalance - banks;

	document.getElementById("closingBalance").value = final;
	document.getElementById("balanceAfterBank").innerHTML = final;
	document.getElementById("closingFinal").innerHTML = final;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function manageDisplay(rec)
{
	if($("#right"+rec).html() == "[+]")
		$("#right"+rec).html("[-]");
	else
		$("#right"+rec).html("[+]");

	$(".sub"+rec).toggle();
}
<!--
function popup(url)
{
 var width  = 1000;
 var height = 900;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=yes';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}
// -->

</script>

</head>

<body>

<?
if($print != "Y")
{
?>
	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="4">Search</td>
		</tr>
		<tr>
			<td align="left"><b>From Date:</b> </td>

				<td> <input type="text" name="date_from" id="date_from" value="<?=$_REQUEST["date_from"]?>" /></td>
			<td align="left">&nbsp;<b>To Date:</b></td>

				<td><input type="text" name="date_to" id="date_to" value="<?=$_REQUEST["date_to"]?>" />	</td>
		</tr>
		<tr>
			<td align="right">Account </td>

			<td align="center">
				<div align="left">
				  <?
			   $sql = "SELECT accounNumber, userType,accountName FROM accounts WHERE status = 'AC'  $sql_agent_based  ORDER BY accounNumber";
			   $accountData = mysql_query($sql) or die('invalid query'.mysql_error());
			   ?>
				  <select name="account" id="account">
				    <option value="">--select account--</option>
			        <?

					while($row = mysql_fetch_array($accountData))
					{
					//debug ($row);
						$accounNumber = $row["accounNumber"];
						$accountName = $row["accountName"];
						echo "<option value='$accounNumber'>".$accounNumber." ". '['.$accountName.']'."</option>";
					}
		        ?>
			      </select>
				  <script language="JavaScript">
					SelectOption(document.forms[0].account, "<?=$_REQUEST["account"];?>");
			      </script>
				    </div>

			  </div></td>
			  <td>&nbsp;</td>
			   <td>&nbsp;</td>
		</tr>
		<!--<tr>
			<td align="right">Date of Expense</td>
			<td>
			<input type="text" name="currentDate" id="currentDate" size="10" readonly="" style="color:#339933; font-size:14px; font-weight:bold" />
			</td>
		</tr>-->
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="4"><input type="submit" name="Submit" value="Submit" /></td>

		</tr>
	</table>
	</form>
<?
}
else
{
?>
	<input type="hidden" name="userId" value="<?=$_SESSION["loggedUserData"]["userID"]?>" />
	<input type="hidden" name="Submit" value="Submit" />
<?
}
?>
<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<? /* if($print=="Y"){
						if(!empty($_SERVER['QUERY_STRING'])){
							$queryStringArr = str_replace("print=Y&","",$_SERVER['QUERY_STRING']);
						}

						$agentNameSql = "SELECT
										name,
										username
									FROM
										".TBL_ADMIN_USERS."
									WHERE
										userID = '".$_REQUEST["userId"]."'
									";
						$agentNameResult = selectFrom($agentNameSql);
						$agentName = $agentNameResult["name"];*/

					?>
				<!--<tr>
					<td align="left" class="reportSubHeader" >
						<span><a href='<?=$_SERVER['SCRIPT_NAME'] . "?" . $queryStringArr?>'>GO BACK</a></span>
						<span style="margin-left:150px;">Agent Name : &nbsp;&nbsp;<em><?=$agentName?></em></span>
					</td>
				</tr> -->
				<? //}?>
				<tr>
					<td align="center" class="reportHeader"><?=strtoupper(SYSTEM)?> (Accounts)<br />
					<!--<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />-->
					<span class="reportSubHeader"><?=date('d-m-Y  h:i:s A')?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="4%" class="columnTitle">Sr.#</td>
					<td width="26%" class="columnTitle">Particulars</td>
					<td width="30%" align="right" class="columnTitle">&nbsp;</td>
					<td width="12%" align="right" class="columnTitle">&nbsp;</td>
					<td width="7%" align="right" class="columnTitle">Dr.  </td>
					<td width="7%" align="right" class="columnTitle">Cr. </td>
					<td width="14%" align="right" class="columnTitle">Balance</td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
					<td>&nbsp;</td>
					<td class="heading">Opening Balance</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;<?=$openingBalance?></td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>

				<tr class="columnTitle">
					<td align="right" id="right1" class="ce" onclick="manageDisplay(1);">&nbsp;[-]</td>
					<td class="heading">Accounts</td>
					<td align="right"><span class="subheading"><?=$_REQUEST["account"]?></span></td>
					<td align="right">&nbsp;<?=$currency?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					$sumBuy  = 0;
					$sumSell = 0;
					$closingBalance = 0;
					for($i=0;$i < count($accountRs);$i++)
					{
						$strDebtColum = 'amountSell';
						$strCrdtColum = 'amountBuy';

						$fltDebt = '-';
						$fltCrdt = '-';

						if($_REQUEST['account'] == $accountRs[$i]["accountBuy"])
							$fltCrdt = $accountRs[$i]["amountBuy"];
						else
							$fltDebt = $accountRs[$i]["amountSell"];

				$sumBuy += $fltCrdt;
				$sumSell += $fltDebt;
				?>
				<tr class="sub1">
					<td>&nbsp;</td>
					<td class="subheading">&nbsp;<?=$accountRs[$i]["description"]?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;<?=$fltDebt?></td>
					<td align="right">&nbsp;<?=$fltCrdt?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>

				<? if(count($accountCancellRs) > 0) { ?>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>

				<tr class="columnTitle">
					<td align="right" id="right1" class="ce" onclick="manageDisplay(1);">&nbsp;[-]</td>
					<td class="heading">Cancell Transaction</td>
					<td align="right"><span class="subheading"><?=$_REQUEST["account"]?></span></td>
					<td align="right">&nbsp;<?=$currency?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					$fltCrdtCancell = 0;
					$fltDebtCancell = 0;
					for($i=0;$i < count($accountCancellRs);$i++)
					{
						$strDebtColum = 'amountSell';
						$strCrdtColum = 'amountBuy';

						$fltDebtCancell = '-';
						$fltCrdtCancell = '-';

						if($_REQUEST['account'] == $accountCancellRs[$i]["accountBuy"])
							$fltCrdtCancell = $accountCancellRs[$i]["amountBuy"];
						else
							$fltDebtCancell = $accountCancellRs[$i]["amountSell"];

				$sumBuy  -= $fltCrdtCancell;
				$sumSell -= $fltDebtCancell;
				?>
				<tr class="sub1">
					<td>&nbsp;</td>
					<td class="subheading">&nbsp;<?=$accountCancellRs[$i]["note"]?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;<?=$fltCrdtCancell?></td>
					<td align="right">&nbsp;<?=$fltDebtCancell?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
				 }
				}
				 ?>

				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
				<tr class="columnTitle">
				<?
					//$closingBalance = ($openingBalance + $sumBuy)-$sumSell;
				?>
					<td align="right">&nbsp;</td>
					<td class="heading">Closing Balance</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;<?=$closing_balance?></td>
				</tr>
				<tr>
					<td colspan="7">&nbsp;</td>
				</tr>
			</table>
			<br />
			<?
				if($print != "Y")
				{
			?>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="50%" class="reportToolbar"><input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: location.href='view-accounts.php?id=<?=$id?>&print=Y&userId=<?=$userId?>&Submit=print&account=<?=$_REQUEST["account"]?>';" /></td>
					      <td width="50%" align="right" class="reportToolbar">
						</tr>
					</table>
			<?
				}
			?>
			</form>
		</td>
	</tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
