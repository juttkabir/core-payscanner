<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");
$agentType = getAgentType();
$reportName='beneficiary-trans-summary.php';
$reportLabel='Beneficiary Transaction Summary';
extract(getHttpVars());
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$reportFlag=true;
$executeQuery=false;
$filterValue=array();
$filterName=array();

$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

    $rfDay = $fDay;
	  $rfMonth =$fMonth;
	  $rfYear =$fYear;
	
	  $rtDay=$tDay;
	  $rtMonth =$tMonth;
	  $rtYear =	$tYear;

if($RID!=''){
	
	$reportFlag=false;
	$filterArray=array(); 
	 $value=array(); 
	 $finalArray=array();
		$compQuery = "select queryID,RID,filterName,filterValue from " . TBL_COMPLIANCE_QUERY_LIST ." where RID=".$RID."";
		
		$compRecord = selectMultiRecords($compQuery); 
		$compQueryMain = selectFrom("select note from " . TBL_COMPLIANCE_QUERY ." where RID=".$RID."");
				
		for($c=0; $c< count($compRecord) ; $c++){
		
		 $filterArray[$c]= $compRecord[$c]["filterName"];
		 $value[$c]= $compRecord[$c]["filterValue"];
		  $finalArray[$filterArray[$c]]=$value[$c];
		 
} 
	$Save=$finalArray["Save"];
	 $Submit=$finalArray["Submit"];
	 $benName=$finalArray["benName"];
	 $phone=$finalArray["phone"];
	 $country=$finalArray["country"];
	 $idType=$finalArray["idType"];
	 $idNumber=$finalArray["idNumber"];
	 $bankDetail=$finalArray["bankDetail"];
	 $bankName=$finalArray["bankName"];
	 $fromDate=$finalArray["fromDate"];
	 $toDate=$finalArray["toDate"];


}	  

if($Submit!= "" || $Save == 'Save Report'){
	
	if($Submit!= ""){
	$filterValue[]=$Submit;
	$filterName[]='Submit';
}elseif($Save == 'Save Report'){
	$filterValue[]=$Save;
	$filterName[]='Save';
	}

if ($_POST["note"] != "")
{
	$note = $_POST["note"];
}elseif ($_GET["note"] != "")
{
	$note = $_GET["note"];	
}

if($fromDate!='' && $toDate!=''){
		$fD = explode("-",$fromDate);
		if (count($fD) == 3)
		{
				$rfDay = $fD[2];
				$rfMonth = $fD[1];
				$rfYear = $fD[0];
		}
				$tD = explode("-",$toDate);
		if (count($tD) == 3)
		{
				$rtDay = $tD[2];
				$rtMonth = $tD[1];
				$rtYear = $tD[0];
		}
	}	
		 $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
  	 $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;


$executeQuery=false;
if (($bankDetail!= '') && ($benName!= ''|| $country!= '' || $phone!= ''|| $idType != '' || $fromDate != '' || $toDate!= '' )){

$benIDQry = "SELECT b.benID FROM " . TBL_BENEFICIARY . " as b," . TBL_BANK_DETAILS . " as bd  WHERE 1 and b.benID=bd.benID";
$executeQuery=true;
}elseif(($bankDetail!= '') && ($benName == ''|| $country == '' || $phone == ''|| $idType == '' || $fromDate == '' || $toDate== '' )){
	
	$benIDQry = "SELECT bd.benID FROM " . TBL_BANK_DETAILS . " as bd WHERE 1";
	$executeQuery=true;
}elseif(($bankDetail == '') && ($benName!= ''|| $country!= '' || $phone!= ''|| $idType != '' || $fromDate != '' || $toDate!='' )){		
  
   $benIDQry = "SELECT b.benID FROM " . TBL_BENEFICIARY . " as b  WHERE 1 ";
   $executeQuery=true;	
}elseif(($fromDate != '' && $toDate!='') && ($bankDetail == '' && $benName == '' && $country == '' && $phone == '' && $idType == '' )){		
  
   $benIDQry = "SELECT b.benID FROM " . TBL_BENEFICIARY . " as b  WHERE 1 ";
   $executeQuery=true;	
}   	


if($benName!= '' ){
	
	// searchNameMultiTables function (Niaz)
        $fn="firstName";
			  $mn="middleName";
			  $ln="lastName"; 		
			  $alis="b";
			  $q=searchNameMultiTables($benName,$fn,$mn,$ln,$alis);
			 $benIDQry  .=$q;
			 
		$filterValue[]=$benName;
	  $filterName[]='benName';
	}

if($phone!= ''){
	
	$benIDQry  .= " and  b.Phone='".$phone."' ";
	$filterValue[]=$phone;
	$filterName[]='phone';
	}	
if ($country != '') {
	
	    $benIDQry  .= " and  b.Country='".$country."' ";
	    $filterValue[]=$country;
	    $filterName[]='country';
	}


if ($idType != "" && $idNumber != "") {
		
		 $benIDQry .= " AND b.IDType= '".$idType."' ";
		 $benIDQry .= " AND b.IDNumber= '".$idNumber."' ";
		 $filterValue[]=$idType;
	   $filterName[]='idType';
	   $filterValue[]=$idNumber;
	   $filterName[]='idNumber';
	}

if ($fromDate != "" && $toDate!= "") {
	$benIDQry .= " and (created >= '$fromDate 00:00:00' and created <= '$toDate 23:59:59')";
	 
	   $filterValue[]=$fromDate;
	   $filterName[]='fromDate';
	   $filterValue[]=$toDate;
	   $filterName[]='toDate';
	}

if ($bankDetail !='') {
	
	 if($bankDetail == "Bank Name"){
	 	 	
	 	 	$benIDQry .= " AND bd.bankName= '".$bankName."' ";
	 	
	 	 	
	}elseif($bankDetail == "IBAN"){
		
		$benIDQry .= " AND bd.IBAN= '".$bankName."' ";
	
		
	}elseif($bankDetail == "SWIFT"){
		
		$benIDQry .= " AND bd.swiftCode= '".$bankName."' ";
		
		
	}elseif($bankDetail == "Account Number"){
		
		$benIDQry .= " AND bd.accNo= '".$bankName."' ";
	
	}
	$filterValue[]=$bankDetail;
	$filterName[]='bankDetail';
	$filterValue[]=$bankName;
	$filterName[]='bankName';
		
	}		
	
	
	//echo $benIDQry;
if($executeQuery==true)	{
	$benIDRes = selectMultiRecords($benIDQry);

}
// beneficiary id	
//echo "--count--".count($benIDRes);
	for ($i = 0; $i < count($benIDRes); $i++) {
	 
		$benIDs .= "'" . $benIDRes[$i]['benID'] . "',";
	
	}
	$benIDs = substr($benIDs, 0, (strlen($benIDs) - 1));
	//echo"<br>--id--".$benIDs;
	
	
	
	if ($benIDs != "") {
		
		$query = "SELECT count(transID), sum(totalAmount), transDate, benID,custAgentID FROM ". TBL_TRANSACTIONS . "  WHERE custAgentID = $userID AND transStatus != 'Cancelled'  ";
	  $queryCnt = "SELECT count(distinct(benID)) FROM ". TBL_TRANSACTIONS . "  WHERE custAgentID = $userID AND transStatus != 'Cancelled' ";
	
		$query .= " AND benID IN (".$benIDs.") ";
		$queryCnt .= " AND benID IN (".$benIDs.") ";
	
    $query .= " Group By benID ORDER BY $sortBy DESC ";
	  // $queryCnt .= " Group By benID ORDER BY $sortBy DESC ";
    $query .= " LIMIT $offset , $limit";
    $queryCnt .= " LIMIT $offset , $limit";
    $allCount = countRecords($queryCnt);
	  $contentsTrans = selectMultiRecords($query);
 }
 
 if($Save == 'Save Report' && $reportFlag == true){
 	
  $Querry_Sqls="insert into ".TBL_COMPLIANCE_QUERY." (reportName,logedUser,logedUserID,queryDate,reportLabel,note) values ('".$reportName."','".$agentType."','".$loggedUserID."','".$date_time."','".$reportLabel."','".$note."')";
	insertInto($Querry_Sqls);
	$reportID = @mysql_insert_id();
	

	for($i=0;$i< count($filterName);$i++){
	$Querry_Sqls2="insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$filterName[$i]."','".$filterValue[$i]."')";
	insertInto($Querry_Sqls2); 
  }
		$msg="Report Has Been Saved";
    
}

}
	
?>
<html>
<head>
	<title>Beneficiary Transaction Summary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
   
 <script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {

		/*if(theForm.idNumber.value == "" || IsAllSpaces(theForm.idNumber.value)){
    	alert("Please provide <?=$idType ?> Number.");
        theForm.idNumber.focus();
        return false;
    }*/
    	if(theForm.bankName.value == "" || IsAllSpaces(theForm.bankName.value)){
    	alert("Please provide <?=$bankDetail ?>");
       theForm.bankName.focus();
        return false;
    }
    return true;
}

	function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
</script>  
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Beneficiary Transaction Summary</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="beneficiary-trans-summary.php" method="post" name="Search" onSubmit="return checkForm(this);" >
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap align="center"> 
         
       Name <input type="text" name="benName" id="benName" value="<? echo $benName ?>">
       Phone <input type="text" name="phone" id="phone" value="<? echo $phone ?>">
       Country <input type="text" name="country" id="country" value="<? echo $country ?>">
      </td></tr>
      <tr>
      	 <td nowrap align="center">
       ID Type<SELECT name="idType" id="idType" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='beneficiary-trans-summary.php'; document.forms[0].submit()">
                <OPTION value="">--Select ID Type---</OPTION>
                <OPTION value="Passport">Passport</OPTION>
                <OPTION value="ID Card">ID Card</OPTION>
                <OPTION value="Driving Licence">Driving Licence</OPTION>
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].idType, "<?=$idType ?>");
		       	  </script>
		       	 
         <? if($idType!= '') { ?>
			<?=$idType ?> Number<input type="text" name="idNumber" id="idNumber" value="<? echo $idNumber ?>">
			<? } ?>
			Bank Detail
			<SELECT name="bankDetail" id="bankDetail" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='beneficiary-trans-summary.php'; document.forms[0].submit()">
				<OPTION value="" >-- Select Bank ---</OPTION>
				<OPTION value="Bank Name" >Bank Name</OPTION>
				<OPTION value="IBAN" >IBAN</OPTION>
				<OPTION value="SWIFT" >SWIFT</OPTION>
				<OPTION value="Account Number" >Account Number</OPTION>
					
			</SELECT>
			 <script language="JavaScript">
		           	SelectOption(document.forms[0].bankDetail, "<?=$bankDetail ?>");
		     </script>
		    <? if($bankDetail!= '') { ?> 
        <?=$bankDetail ?><font color="#ff0000">*</font>
        <input type="text" name="bankName" id="bankName" value="<? echo $bankName ?>">
      </td>
      <? } ?>
	</tr>
		       <tr>
        <td align="center" nowrap>
		From
		<?
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					
           <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
					
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         		SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
				
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
      </tr>
     <tr> <td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
     
     <?
			if (count($contentsTrans) > 0){
		?>
			<tr>
	    	<td  bgcolor="#000000">
				
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">	
						
	 					<? 
	 					if($msg!= '') { ?>
							<tr>
								  <td colspan="2" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></b></i><strong><? echo $msg; ?></strong></font></td>
							</tr>
						<? 
						} 
						?>	
	 					<tr>
							<td valign="center" align="right">Add Note/Comment</td>
							<td><textarea name="note" cols="40"><?=$compQueryMain["note"];?></textarea></td>
	        		
						</tr>  
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
         <input type="submit" name="Save" value="Save Report">
               	
        </td>
	    </tr>
   
      <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ($allCount > 0) {
                   
                    	?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ?>
                  </td>
                                    
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$alloffset&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>

   
   
        
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Beneficiary Number</span></td>
			  <td width="100"><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Registration Date </span></td>
			  <td width="100"><span class="style1">Number of Transaction </span></td>
		    <td width="100"><span class="style1">Amount Received</span></td>
			
			</tr>
		    <?
		     $totalTrans=count($contentsTrans);
		    for($i=0;$i < count($contentsTrans);$i++)			{
		   
		     $custQuery = selectFrom("SELECT benID,firstName,lastName,created FROM " . TBL_BENEFICIARY . "  WHERE  benID='".$contentsTrans[$i]["benID"]."' ") ;
	   // echo "SELECT benID,firstName,lastName,created FROM " . TBL_BENEFICIARY . "  WHERE  benID='".$contentsTrans[$i]["benID"]."' ";
			
			?>
				
				<tr bgcolor="#FFFFFF">
					
				
				<td width="81" >
				  	<a href="#" onClick="javascript:window.open('beneficiary-trans-summary-detail.php?benID=<? echo $contentsTrans[$i]["benID"]; ?> &toDate=<? echo $toDate;?>&fromDate=<? echo $fromDate; ?>','viewTranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $custQuery["benID"];?></font></strong></a>
				</td>
				
			
					  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $custQuery["firstName"]." ".$custQuery["lastName"];?></font></td>				  	
				   <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $custQuery["created"]; ?></font></td>
				   <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["count(transID)"]; ?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo number_format($contentsTrans[$i]["sum(totalAmount)"],2);?></font></td>
				</tr>
				<?
			}
			
			} // greater than zero
			
			?>
          </table></td>
        </tr>

     <tr>
            <td  bgcolor="#000000">
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset + count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$alloffset&country=$country&idType=$idType&idNumber=$idNumber&bankDetail=$bankDetail&bankName=$bankName&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
         

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				}
			} 
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>