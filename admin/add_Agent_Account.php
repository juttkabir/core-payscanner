<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
include ("javaScript.php");
///////////////////////History is maintained via method named 'activities'

$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

if($_GET["agent_Account"]!="")
	$_SESSION["agentID2"] = $_GET["agent_Account"];
	
$_SESSION["settleCurr"]=$_POST["settleCurr"];
//echo $currency=$_POST["settleCurr"];
$condition = true;
//debug($_REQUEST);

function updateAmountCurrency($intInsertedID, $strCurrency)
{
	update("update agent_account set currency  = '".$strCurrency."' where aaID = '".$intInsertedID."'");
}


if($_REQUEST["settleCurr"]!="")
	$settleCurr = $_REQUEST["settleCurr"];
	
if ($_POST["Submit"] == "Save") {
	if (CONFIG_BACKDATING_PAYMENTS == "1") {
		if ($_POST["dDate"] != "") {
			$dDate = explode("/", $_POST["dDate"]);
			$today = $dDate[2] . "-" . $dDate[1] . "-" . $dDate[0];
			if ($today > date("Y-m-d")) {
				$condition = false;	
			}
		} else {
			$today = date("Y-m-d");
		}
	} else {
		$today = date("Y-m-d");
	}
	
	if(isset($_REQUEST["modeOfPayment"]))
	{
		$modeOfPayment = $_REQUEST["modeOfPayment"];
		$chequeNumber = $_REQUEST["chequeNumber"];
		$chequeStatus = "UNCLEARED";
		
		if($modeOfPayment == "CHEQUE" && empty($chequeNumber))
		{
			$errMessage = "<font color='#ff0000'><b>Please enter cheque number.</b></font>";
		} elseif($modeOfPayment == "CHEQUE" && !empty($chequeNumber)) {
			// Check for duplicate cheque number
			$sql = "SELECT
						chequeNumber
					FROM
						agent_account
					WHERE
						chequeNumber = '".$chequeNumber."'
					";
			$result = mysql_query($sql) or die(mysql_error());
			$numrows = mysql_num_rows($result);
			
			if($numrows > 0)
			{
				$errMessage = "<font color='#ff0000'><b>Duplicate cheque number, please enter a different (unique) cheque number.</b></font>";
			}
		} elseif ($modeOfPayment == "MIXED"){
			$chequeStatus = "UNCLEARED";
		} elseif ($modeOfPayment != "CHEQUE"){
			$chequeStatus = "";
		}
	} else {
		$modeOfPayment = "CASH";
		$chequeNumber = "";
		$chequeStatus = "";
	}
}


?>
<html>
<head>
<title>Add into Account</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function handleChequeNumField()
{
	var paymentMode = document.getElementById("modeOfPayment").value;
	var chequeNum = document.getElementById("chequeNumber");
	
	if(paymentMode == "CHEQUE")
	{
		chequeNum.disabled = false;
	} else {
		chequeNum.disabled = true;
	}
}
function checkForm(theForm){
<? if(defined("CURRENCY_AT_ACCOUNT_DEPOSIT") && CURRENCY_AT_ACCOUNT_DEPOSIT=="1"){ ?>	
	if(theForm.accountCurrency.value==""){
		alert("Please select the currency.");
		return false;
	}
	return true;
<? }?>
}
</script>
</head>

<body>
	<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">Agent Account Statement</td>
  </tr>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="add_Agent_Account.php" onSubmit="return checkForm(this);"
    <tr> 
      <td width="39%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Amount &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="money"></td>
    </tr>
     <tr> 
      <td><div align="right"><font color="#005b90">Note &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="note"></td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Type &nbsp;&nbsp;</font></div></td>
      <td><select name="type">
      			<option value="Deposit">Deposit</option>
      			<option value="Withdraw">Withdraw</option>
      		</select>
      	</td>
    </tr>
	<?
		if(CONFIG_ENABLE_PAYMENT_MODE_ON_AGENT_ACCOUNT == "1")
		{
	?>
			<tr> 
			  <td><div align="right"><font color="#005b90">Mode of Payment &nbsp;&nbsp;</font></div></td>
			  <td><select name="modeOfPayment" id="modeOfPayment" onChange="javascript: handleChequeNumField();">
						<option value="CASH" selected="selected">Cash</option>
						<option value="MIXED">Mixed Banking</option>
						<option value="CHEQUE">Cheque</option>
					</select>
				</td>
			</tr>
			
			<tr> 
			  <td><div align="right"><font color="#005b90">Cheque Number &nbsp;&nbsp;</font></div></td>
			  <td><input type="text" size="30" name="chequeNumber" id="chequeNumber" value="" disabled="disabled" />
			  		<?
						if(!empty($errMessage))
						{
							echo $errMessage;
						}
					?>
				</td>
			</tr>
	<?
		}
	?>
     <!-- Added By Niaz Ahmad #2810 Connect Plus at 18-01-08 -->
    <? 
	if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') 
	{ 
		$currencyFilter = "&settleCurr=".$_REQUEST["settleCurr"];
		$sCurrency  = selectFrom("select settlementCurrency from admin  where userID=".$_SESSION["agentID2"]." ");	
		$settlementCurr=$sCurrency["settlementCurrency"];
		if($settlementCurr!='')
		{	
   	?>
		<tr> 
			<td><div align="right"><font color="#005b90">Settlement Currency &nbsp;&nbsp;</font></div></td>
			<td><select name="settleCurr">
			<option value="<? echo $settlementCurr; ?>"><? echo $settlementCurr;?></option>
			</select>
			<input type="hidden" name="settleCurr" value="<?=$_REQUEST["settleCurr"]?>" />
			</td>
		</tr>
    <?
		} 
	}
    
	
if(defined("CURRENCY_AT_ACCOUNT_DEPOSIT") 
		&& CURRENCY_AT_ACCOUNT_DEPOSIT == "1" 
		&& (CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "0"
		    || !defined("CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT"))
		)
	{
		if($_REQUEST["settleCurr"]!="")
			$currencyFilter = "&currency=".$_REQUEST["settleCurr"];
		else
			$currencyFilter = "&currency=".$_REQUEST["accountCurrency"];
		$uniqueCurrencyListRA = array();
		if ( !empty( $_SESSION["agentID2"] ) )
		{
	
			$recievingAgentQuery = "select agentCountry from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$_SESSION["agentID2"]."'";
			$recievingAgentData = selectFrom( $recievingAgentQuery );
			
			//$currQuery = "SELECT distinct(currencyOrigin) FROM " . TBL_EXCHANGE_RATES . " where countryOrigin = '".$recievingAgentData["agentCountry"]."' ORDER BY currencyOrigin ";
			$currQuery = "SELECT distinct(currencyName) as currencyOrigin FROM currencies ORDER BY currencyName ";
			$currencyData = selectMultiRecords( $currQuery );
			
			foreach( $currencyData as $currency)
			{
				if( !empty($currency["currencyOrigin"]) )
					$uniqueCurrencyListRA[]["settlementCurrency"] = $currency["currencyOrigin"];
			}	
			$recievingAgentData = $uniqueCurrencyListRA;	
		}
		
	?>
		<tr> 
			<td align="right"><font color="#005b90">Account Currency &nbsp;&nbsp;</font></td>
			<td>
				<select name="accountCurrency" style="font-family:verdana; font-size: 11px" onChange="updateInterface();">
					<option value="">- Currency  -</option>
					<?
					foreach ($uniqueCurrencyListRA as $uniqueCurrency )
					{
						$uniqueCurrency = $uniqueCurrency["settlementCurrency"];
						$selected = "";
						if($_REQUEST["settleCurr"] == $uniqueCurrency)
							$selected = "selected='selected'";
					?>
						<option value="<?=trim($uniqueCurrency)?>" <?=$selected?>><?=trim($uniqueCurrency)?></option>
					<? } ?>
				</select>
			</td>
		</tr>
	<?
	}
	
	
	if (CONFIG_BACKDATING_PAYMENTS == "1") 
	{  
	?>
    <tr> 
      <td><div align="right"><font color="#005b90">Date &nbsp;&nbsp;</font></div></td>
      <td>
      	<input type="text" name="dDate" readonly>&nbsp;
      	<a href="javascript:show_calendar('form1.dDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
      </td>
    </tr>
    <?	}  ?>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Save"></td>
    </tr>
	 <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if(empty($errMessage))
{
	if($_POST["Submit"] == "Save")
	{
		if ($condition) {	
			if ($agentType == "COLLECTOR") {
				$status	= "Provisional";
			} else {
				$status	= "Verified";	
			}
			
			$currency=$_POST["settleCurr"];
			if ($_POST["type"] == "Deposit")
			{
				
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settlementCurr!='') 
				{
					$insertQuery="insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description, status, note,settleAmount,currency, modeOfPayment, chequeNumber, chequeStatus) values('".$_SESSION["agentID2"]."', '$today', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', '', 'Manually Deposited','$status','".$_POST["note"]."','".$settlementAmount."','".$currency."', '".$modeOfPayment."', '".$chequeNumber."', '".$chequeStatus."')";
				}else{
				
					$insertQuery="insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description, status, note,currency, modeOfPayment, chequeNumber, chequeStatus) values('".$_SESSION["agentID2"]."', '$today', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', '', 'Manually Deposited','$status','".$_POST["note"]."','".$currency."', '".$modeOfPayment."', '".$chequeNumber."', '".$chequeStatus."')";
				
				}
						
				$q= insertInto($insertQuery);
				$insertedID = @mysql_insert_id();
				//debug($insertedID);
				$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
				$agentBalance	= $contantagentID["balance"];
				$agentBalance += $_POST["money"];
				update("update " . TBL_ADMIN_USERS . " set balance  = $agentBalance where userID = '".$_SESSION["agentID2"]."'");
				
				$descript = "Amount ".$_POST["money"]." is added into Agents account";
				activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agent_account",$descript);
					
				///////Updating Agent Account Summary/////////
				agentSummaryAccount($_SESSION["agentID2"], "DEPOSIT", $_POST["money"]);		
					////////////////////
				
				/* Update the manula deposit or withdraw currency */
				if(isset($_REQUEST["accountCurrency"]))
					updateAmountCurrency($insertedID, $_REQUEST["accountCurrency"]);
			} 
			elseif ($_POST["type"] == "Withdraw") 
			{
				
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settlementCurr!='') 
				{
					$insertQuery="insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description, status, note,settleAmount,currency, modeOfPayment, chequeNumber, chequeStatus) values('".$_SESSION["agentID2"]."', '$today', 'WITHDRAW', '".$_POST["money"]."', '$changedBy', '', 'Manually Withdrawn','$status','".$_POST["note"]."','".$settlementAmount."','".$currency."', '".$modeOfPayment."', '".$chequeNumber."', '".$chequeStatus."')";
				}else{
					$insertQuery="insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description, status, note,currency, modeOfPayment, chequeNumber, chequeStatus) values('".$_SESSION["agentID2"]."', '$today', 'WITHDRAW', '".$_POST["money"]."', '$changedBy', '', 'Manually Withdrawn','$status','".$_POST["note"]."','".$currency."', '".$modeOfPayment."', '".$chequeNumber."', '".$chequeStatus."')";
				} 

				$q=mysql_query($insertQuery);
				$insertedID = @mysql_insert_id();
				
				$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
				$agentBalance	= $contantagentID["balance"];
				$agentBalance -= $_POST["money"];
				update("update " . TBL_ADMIN_USERS . " set balance  = $agentBalance where userID = '".$_SESSION["agentID2"]."'");
				
				$descript = "Amount ".$_POST["money"]." is withdrawn from Agents account";
				activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agent_account",$descript);
					
				///////Updating Agent Account Summary/////////
				
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settlementCurr!='') 
				{
					agentSummaryAccount($_SESSION["agentID2"], "WITHDRAW", $_POST["money"],$currency);		
				}else{
					agentSummaryAccount($_SESSION["agentID2"], "WITHDRAW", $_POST["money"]);
				}		
				////////////////////
				/* Update the manula deposit or withdraw currency */
				if(isset($_REQUEST["accountCurrency"]))
					updateAmountCurrency($insertedID, $_REQUEST["accountCurrency"]);
			}
			
	
		////////////////////Auto Authorization//////////
	if(CONFIG_AUTO_AHTHORIZE == "1")
	{
		$contantagentID = selectFrom("select * from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
		$agentID = $contantagentID["userID"];
		$agentBalance	= agentBalance($_SESSION["agentID2"]);//$contantagentID["balance"];
		$agentLimit	= $contantagentID["agentAccountLimit"];
		
		
		$amountInHand = $agentBalance;
		$contantTrans = selectMultiRecords("select * from ".TBL_TRANSACTIONS." where custAgentID = '".$_SESSION["agentID2"]."' and transStatus = 'Pending' order by transDate ");
		//echo("select * from ".TBL_TRANSACTIONS." where custAgentID = '".$_SESSION["agentID2"]."' and transStatus = 'Pending' order by transDate ");
		for($i = 0; $i < count($contantTrans); $i++)
		{
		//	echo("Amount in Hand $amountInHand    Transaction Amount ".$contantTrans[$i]["totalAmount"]);
		if(CONFIG_AGENT_LIMIT == "1")
		{
			$agentValidBalance = $amountInHand + $agentLimit;///Agent Limit is also used
		}else{
			$agentValidBalance = $amountInHand;///Agent Limit is not used
			}
		//echo("Valid Amount   $agentValidBalance    Transaction Amount ".$contantTrans[$i]["totalAmount"]);
			if($agentValidBalance >= $contantTrans[$i]["totalAmount"])
			{	//////////////////If Balance is enough
				
				$queryCust = "select payinBook  from ".TBL_CUSTOMER." where customerID ='" . $contantTrans[$i]["customerID"] . "'";
				$custContents = selectFrom($queryCust);
					
				if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
				{	
					
					$Status	= "Authorize";
					
					if(CONFIG_LEDGER_AT_CREATION != "1")
					{
						$insertQuery = "insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description, modeOfPayment, chequeNumber, chequeStatus) values('$agentID', '$today', 'WITHDRAW', '".$contantTrans[$i]["totalAmount"]."', '$changedBy', '". $contantTrans[$i]["transID"]."', 'Transaction Authorized', '".$modeOfPayment."', '".$chequeNumber."', '".$chequeStatus."')";
						$q=mysql_query($insertQuery);//////Insert into Agent Account
						 update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '$agentID'");
						 
						 ///////Updating Agent Account Summary/////////
							agentSummaryAccount($agentID, "WITHDRAW", $contantTrans[$i]["totalAmount"]);		
										////////////////////
	
					}
					$amountInHand = $amountInHand - $contantTrans[$i]["totalAmount"];			
					
					update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$contantTrans[$i]["transID"]."'");				
					//echo("Trans ID ---".$contantTrans[$i]["transID"]);
					
	
								/////////////////////Added to audit trial/////////
										$descript = "Transaction is Authorized";
										activities($_SESSION["loginHistoryID"],"UPDATION",$contantTrans[$i]["transID"],TBL_TRANSACTIONS,$descript);	
								//////////////////////////////////				 
					 
					$contentBenID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$contantTrans[$i]["benAgentID"]."'");
					$benBalance	= $contentBenID["balance"];
					$benBalance -= $contantTrans[$i]["transAmount"];
					update("update " . TBL_ADMIN_USERS . " set balance  = $benBalance where userID = '".$contantTrans[$i]["benAgentID"]."'");
	
					insertInto("insert into bank_account (bankID, dated, type, amount, currency, modified_by, TransID, description) values('".$contantTrans[$i]["benAgentID"]."', '$today', 'WITHDRAW', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contantTrans[$i]["localAmount"] : $contantTrans[$i]["transAmount"])."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contantTrans[$i]["currencyTo"] : $contantTrans[$i]["currencyFrom"])."', '$changedBy', '".$contantTrans[$i]["transID"]."', 'Transaction Authorized')");		
					
					 ///////Updating Agent Account Summary/////////
							agentSummaryAccount($contantTrans[$i]["benAgentID"], "WITHDRAW", $contantTrans[$i]["transAmount"]);		
										////////////////////
	
				}
			}
		}
	
		
		
	}
	
	////////////////////////
		
		
		
		
	if($q)
		{?>
	  <tr> 
		  <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>">You 
			have successfully <? echo ($_POST["type"] == "Deposit" ? "deposited" : "withdrawn"); ?> 
			<? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
			</font></div></td>
		  <td bgcolor="#CCCCCC" align="left">&nbsp;<!--font color="#990000"> &nbsp; Now Balance 
		  is <? $balanceCal=$amountInHand;//agentBalance($_SESSION["agentID2"]); 
		  echo number_format($balanceCal,2,'.',',');?></font--> </td>
	  </tr>
	  <? }
	} else {
		?>
	  <tr> 
		  <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo CAUTION_COLOR ?>">
			Enter Date should not be greater than today Date.
			</font></div></td>
		  <td bgcolor="#CCCCCC" align="left">&nbsp;</td>
	  </tr>
		<?
	}	
	} 
}
?>
	<tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr> 
      
    <td height="19">&nbsp;&nbsp;&nbsp;<a href="agent_Account.php?agentID=<?=$_SESSION["agentID2"]?><?=$currencyFilter;?>&search=search" class="style2" >
	<font color="#000000">Back to Agent Account Statement</font></a></td>
      
    <td>&nbsp;</td>
    </tr>
</table>
</table>
</body>
</html>
