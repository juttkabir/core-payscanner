<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

$totalAmount="";



////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

if($_POST["CalculateAmount"] != "" || $_POST["save"] != "")
{
	$originAmount = $_POST["originAmount"];
	$originCurrency = $_POST["currencyOrigin"];
	
	$intermediateRate = $_POST["intermediateRate"];
	$intermediateCurr = $_POST["intermediateCurrency"];

	$destinationRate = $_POST["destinationRate"];	
	$destinationCurrency = $_POST["destinationCurrency"];	
	
	$bankCharges2 = $_POST["bankCharges2"];
	//$bankChargCurr = $_POST["bankChargeCurrency"];
	
	$originCountry = $_POST["origCountry"];
	$destCountry =$_POST["destCountry"];
	
	
	$intermediateAmount = $originAmount * $intermediateRate;
	$intermediateAmountNet = $intermediateAmount - $bankCharges2;
	
	if($_POST["save"] != "")
	{
		//$send_rate_inter_to_dest = $destinationRate/$intermediateRate;
		
		$date_time = getCountryTime(CONFIG_COUNTRY_CODE);
		$insertionQuery = "insert into ".TBL_PROFIT_EARNING."(entry_time, origin_country, dest_country, origin_amount, origin_currency, intermediate_rate, intermediate_curr, send_rate_inter_to_dest, dest_currency, bank_charges) Values
		('".$date_time."','".$_POST["origCountry"]."','".$_POST["destCountry"]."','".$originAmount."','".$originCurrency."','".$intermediateRate."','".$intermediateCurr."','".$destinationRate."','".$destinationCurrency."','".$bankCharges2."')";	
		insertInto($insertionQuery);
		
		$profitID = @mysql_insert_id();
		
		insertError("Profit Report added successfully");
		$msg = True;
		////////For History
			$descript="Profit Earning ".$profitID." is added ";
			activities($_SESSION["loginHistoryID"],"INSERTION",$profitID,TBL_PROFIT_EARNING,$descript);
			/////////////////////////
			
	}
	
	
}

?>
<html>
<head>
	<title>Calculate Profit</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
<tr>
    <td class="topbar">Profit Earning
      Report</td>
  </tr>
 </table>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="profit_earning_Bayba.php" method="post" name="calculator">
  
  <tr>
  	<td>
  		<table width="100%" border="0" align="center">	
  			<tr bgcolor="#ededed">
             <td width="210">
             	Originating Country
             	<SELECT name="origCountry" id="origCountry" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Country-</OPTION>
				<?
					
                		$countryTypes = " and  countryType like '%origin%' ";
					
					if(CONFIG_COUNTRY_SERVICES_ENABLED){
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = fromCountryId  $countryTypes order by countryName");
							}
					else
						{
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
						}
				
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT>
              <script language="JavaScript">
		         SelectOption(document.calculator.origCountry, "<?=$_POST["origCountry"] ?>");
		      </script></td>
		      <td width="210">
             	Destination Country
             	<SELECT name="destCountry" id="destCountry" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Destination Country-</OPTION>
				<?
					
                		$countryTypes = " and  countryType like '%destination%' ";
					
					if(CONFIG_COUNTRY_SERVICES_ENABLED){
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
							}
					else
						{
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
						}
				
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT>
              <script language="JavaScript">
		         SelectOption(document.calculator.destCountry, "<?=$_POST["destCountry"] ?>");
		      </script></td>
        </tr>
  			
  				
  			<tr>
  				<td>
  					Originating Amount <input type="text" name="originAmount" value="<?=$originAmount?>">   					
  				</td>
  				<td>
  					In Currency 
  					<select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?				  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($originCurrency == $currency)
									echo "<option selected value ='$currency'> ".$currency." -->  ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." -->  ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     				SelectOption(document.calculator.currencyOrigin, "<?=$originCurrency ?>");
     	  		</script>
  				</td>
  			</tr>
  				<tr>
  				<td>
  					Intermediate Rate <input type="text" name="intermediateRate" value="<?=$intermediateRate?>">   					
  				</td>
  				<td>
  					In Currency 
  					<select name="intermediateCurrency">
				<option value="">-- Select Currency --</option>
						  <?				  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($intermediateCurr == $currency)
									echo "<option selected value ='$currency'> ".$currency." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     				SelectOption(document.calculator.intermediateCurrency, "<?=$intermediateCurr ?>");
     	  		</script>
  				</td>
  			</tr>
  			<tr>
  				<td>
  					Destination Rate <input type="text" name="destinationRate" value="<?=$intermediateCurr?>">   					
  				</td>
  				<td>
  					In Currency 
  					<select name="destinationCurrency">
				<option value="">-- Select Currency --</option>
						  <?				  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($destinationCurrency == "")
									echo "<option selected value ='$currency'> ".$currency." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     				SelectOption(document.calculator.destinationCurrency, "<?=$destinationCurrency ?>");
     	  		</script>
  				</td>
  			</tr>
  			
  			<tr>
  				<td>
  					Bank Charges <input type="text" name="bankCharges2" value="<?=$bankCharges2?>">   					
  				</td>
  				<td>
  					&nbsp;
  				</td>
  			</tr>
  			<tr>
  				<td>
  					<input type="submit" name="CalculateAmount" value="Calculate Amount">
  					<input type="submit" name="save" value="Save Calculation">
  				</td>
  			</tr>
		</table>	     
         <table>
         	<? if ($msg){ ?>
		  <tr bgcolor="EEEEEE"> 
            <td colspan="2"> 
              <table align="center">
              	<tr>
              		<td width="40" align="center"><font size="5" color="#990000"><b><i><? echo ($msg ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='#990000'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
              		</td>
              	</tr>
               </table>
             </td>
            </tr>
     		<? } ?>	
     		<tr bgcolor="#DFE6EA" align="center"> 
	            <td width="146"><font color="#005b90"><strong> From <?=$_POST["origCountry"]?></strong></font></td>
	            <td width="159"><font color="#005b90"><strong> To <?=$_POST["destCountry"]?></strong></font></td>	
        	</tr>
         	<tr bgcolor="#DFE6EA"> 
	            <td width="146"><font color="#005b90"><strong>Amount Sent <?=$originCurrency?></strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Conversion Rate <?=$intermediateCurr?></strong></font></td>	
	            <td width="146"><font color="#005b90"><strong>Total Amount <?=$intermediateCurr?></strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Bank Charges <?=$intermediateCurr?></strong></font></td>	
	            <td width="159"><font color="#005b90"><strong>Net Amount <?=$intermediateCurr?></strong></font></td>	            
         	</tr>
         	<tr bgcolor="#DFE6EA"> 
	            <td width="146"><? echo($originAmount);?></td>
	            <td width="159"><?=$intermediateRate?></td>	
	            <td width="146"><?=$intermediateAmount?></td>
	            <td width="159"><?=$bankCharges2?></td>	
	            <td width="159"><?=$intermediateAmountNet?></td>	            
         	</tr>
        </table>      
			
	 
	 </td>
	</tr>
	</form>
</table>
</body>
</html>