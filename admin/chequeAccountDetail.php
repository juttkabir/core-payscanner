<?php
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

	if(!empty($_REQUEST["userid"]))
	{
		$strUserIdSql = "AND paid_by like '".$_REQUEST["userid"]."%' ";
		$arrCreatorDetail = selectFrom("select username from admin where userid='".$_REQUEST["userid"]."'");
	}
	//else
	//	$userId = $_SESSION["loggedUserData"]["userID"];


	$date_from			= $_REQUEST["df"];
	$date_to		 	= $_REQUEST["dt"];

	$strDateFilter = "";
	
	if(!empty($date_from))
	{
		$date = explode("/",$date_from);
		$strDateFilter = " AND paid_on >= '".date("Y-m-d 00:00:00",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	
	if(!empty($date_to))
	{
		$date = explode("/",$date_to);
		$strDateFilter = " AND paid_on <= '".date("Y-m-d 23:59:59",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	
	if(empty($date_from) && empty($date_to))
	{
		$strDateFilter = " AND paid_on like '".date("Y-m-d")."%'";
	}

	$strSql = "select 
					cheque_ref,
					cheque_no, 
					cheque_amount, 
					paid_amount,
					cheque_currency,
					paid_on, 
					concat(c.firstName, ' ', c.middleName, ' ', c.lastName) as fullName
					
				from
					cheque_order,
					customer c
				where
					c.customerid = customer_id
					AND status = 'ID'
					".$strDateFilter." 
					$strUserIdSql
					";
	//debug($strSql);
	$result = SelectMultiRecords($strSql);

	activities($_SESSION["loginHistoryID"],"VIEW",0,"cheque_order","Cheque Account Ledger Details Viewed");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cheque Order Reports</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>

<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader">Minas Center (Cash Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y")?><br />
					<br />
					Detailed Report for <?=$arrCreatorDetail["username"]?>
					</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<a href="dailyCheque.php?userId=<?=$_REQUEST["userid"]?>&date_from=<?=$_REQUEST["df"]?>&date_to=<?=$_REQUEST["dt"]?>"><b>Go Back</b></a>
					</td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="5%" class="columnTitle">Sr.#</td>
					<td width="15%" class="columnTitle">Cheque Number</td>
					<td width="50%" class="columnTitle">Particulars</td>
					<td width="10%" align="right" class="columnTitle">Dr. </td>
					<td width="10%" align="right" class="columnTitle">Cr. </td>
					<td width="10%" align="right" class="columnTitle">Balance </td>
				</tr>
				<?
					//$i=1;
					$totalPaidAmount = 0;
					foreach($result as $k => $v)
					{
						$totalPaidAmount += $v["paid_amount"];
				?>
				<tr>
					<td><?=$k+1?></td>
					<td><?=$v["cheque_no"]?></td>
					<td><?=$v["fullName"]?> <b>paid on</b> <?=date("D, M d,Y",strtotime($v["paid_on"]))?></td>
					<td align="right"><?=$v["paid_amount"]." ".$v["cheque_currency"]?></td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					//$i++;
					}
				?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
