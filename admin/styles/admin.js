// Java script code for admin module
IE = (document.all) ? 1:0;
NS = (document.layers) ? 1:0;
newCustomerTable = '<table width="100%" cellpadding="2" cellspacing="1" border="0"><tr bgcolor="#ededed"><td width="17%" height="20" align="right"><font color="#005b90"><b>Title&nbsp;</b></font></td><td width="26%"> <select name="Title"><option value="Mr.">Mr.</option><option value="Miss.">Miss</option><option value="Dr.">Dr.</option><option value="Mrs.">Mrs.</option><option value="Other">Other</option></select><script language="JavaScript">SelectOption(document.forms[0].Title, "<?=$_SESSION["Title"]; ?>");</script></td><td width="20%" align="right"><font color="#005b90"><b>First Name*&nbsp;</b></font></td><td width="37%"><input type="text" name="firstName2" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td></tr><tr bgcolor="#ededed"> <td width="17%" height="20" align="right"><font color="#005b90"><b>Middle Name&nbsp;</b></font></td><td width="26%"><input type="text" name="middleName" value="<?=$_SESSION["middleName"]; ?>" maxlength="25"></td><td width="20%" align="right"><font color="#005b90"><b>Last Name*&nbsp;</b></font></td><td width="37%"><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td></tr><tr bgcolor="#ededed"> <td width="17%" height="20" align="right"><font color="#005b90"><b>Date of Birth&nbsp;</b></font></td><td width="26%"><SELECT name="dobMonth" size="1" style="font-family:verdana; font-size: 11px; width:50"><OPTION value="">Month</OPTION><OPTION value="01">Jan</OPTION><OPTION value="02">Feb</OPTION><OPTION value="03">Mar</OPTION><OPTION value="04">Apr</OPTION><OPTION value="05">May</OPTION><OPTION value="06">Jun</OPTION><OPTION value="07">Jul</OPTION><OPTION value="08">Aug</OPTION><OPTION value="09">Sep</OPTION><OPTION value="10">Oct</OPTION><OPTION value="11">Nov</OPTION><OPTION value="12">Dec</OPTION></SELECT> <script language="JavaScript">SelectOption(document.forms[0].dobMonth, "<? echo $dobMonth; ?>");</script> <select name="dobDay" size="1" style="font-family:verdana; font-size: 11px; width:45"><option value="">Day</option><? for ($Day=1;$Day<32;$Day++){ if ($Day<10) $Day="0".$Day; echo "<option value=\"$Day\">$Day</option>\n"; }?></select> <script language="JavaScript">SelectOption(document.forms[0].dobDay, "<?echo $dobDay;?>");</script> <select name="dobYear" size="1" style="font-family:verdana; font-size: 11px"><option value="" selected>Year</option><? $cYear=date("Y"); for ($Year=$cYear-80; $Year<($cYear-15);$Year++){ echo "<option value=\"$Year\">$Year</option>\n"; }?></select> <script language="JavaScript">SelectOption(document.forms[0].dobYear, "<?echo $dobYear?>");</script></td><td width="20%" align="right"><font color="#005b90"><b>Accepted Terms*&nbsp;</b></font></td><td width="37%"><input type="radio" name="acceptedTerms" value="Y"> Yes <input type="radio" name="acceptedTerms" value="N"> No</td></tr><tr bgcolor="#ededed"><td width="17%" height="20" align="right" valign="top"><font color="#005b90"><b>ID Type&nbsp;</b></font></td><td width="26%"><input type="radio" name="IDType" value="Passport" <? if ($_SESSION["IDType"] == "Passport" || $_SESSION["IDType"] == "") echo "checked"; ?>> Passport <br> <input type="radio" name="IDType" value="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>> ID Card<br> <input type="radio" name="IDType" value="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>> Driving Licence</td><td width="20%" align="right" valign="top"><font color="#005b90"><b>ID Numnber*&nbsp;</b></font></td><td width="37%" valign="top"><input type="text" name="IDNumnber " value="<?=$_SESSION["IDNumnber "]; ?>" maxlength="32"></td></tr><tr bgcolor="#ededed"><td width="17%" height="20" align="right"><font color="#005b90"><b>Issued By&nbsp;</b></font></td><td width="26%"><input type="text" name="issuedBy" value="<?=$_SESSION["issuedBy"]; ?>" maxlength="255"></td><td width="20%" align="right"><font color="#005b90"><b>ID Expiry*&nbsp;</b></font></td><td width="37%"><SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px; width:50"><OPTION value="">Month</OPTION><OPTION value="01">Jan</OPTION><OPTION value="02">Feb</OPTION><OPTION value="03">Mar</OPTION><OPTION value="04">Apr</OPTION><OPTION value="05">May</OPTION><OPTION value="06">Jun</OPTION><OPTION value="07">Jul</OPTION><OPTION value="08">Aug</OPTION><OPTION value="09">Sep</OPTION><OPTION value="10">Oct</OPTION><OPTION value="11">Nov</OPTION><OPTION value="12">Dec</OPTION></SELECT> <script language="JavaScript">SelectOption(document.forms[0].idMonth, "<? echo $idMonth; ?>");</script> <select name="idDay" size="1" style="font-family:verdana; font-size: 11px; width:45"><option value="">Day</option><? for ($Day=1;$Day<32;$Day++){ if ($Day<10) $Day="0".$Day; echo "<option value=\"$Day\">$Day</option>\n";}?></select> <script language="JavaScript">SelectOption(document.forms[0].idDay, "<?echo $idDay;?>");</script> <select name="idYear" size="1" style="font-family:verdana; font-size: 11px"><option value="" selected>Year</option><? $cYear=date("Y"); for ($Year=$cYear; $Year<($cYear-10);$Year++){ echo "<option value=\"$Year\">$Year</option>\n"; }?></select> <script language="JavaScript">SelectOption(document.forms[0].idYear, "<?echo $idYear?>");</script></td></tr><tr bgcolor="#ededed"> <td width="17%" height="20" align="right"><font color="#005b90"><b>Address Line 1*&nbsp;</b></font></td><td width="26%"><input type="text" name="Address" value="<?=$_SESSION["Address"]; ?>" maxlength="25"></td><td width="20%" align="right"><font color="#005b90"><b>Address Line 2&nbsp;</b></font></td><td width="37%"><input type="text" name="Address1" value="<?=$_SESSION["Address1"]; ?>" maxlength="25"></td></tr><tr bgcolor="#ededed"> <td width="17%" height="20" align="right"><font color="#005b90"><b>Country*&nbsp;</b></font></td><td width="26%"><select name="Country" style="font-family:verdana; font-size: 11px" onChange="ChangeDetail(document.forms[0], \'add-agent.php\');"><option value="">- Select Country-</option><option value="United States">United States</option><option value="Canada">Canada</option><? $countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country"); for ($i=0; $i < count($countires); $i++){ ?><option value="<?=$countires[$i]["country"]; ?>"> <?=$countires[$i]["country"]; ?></option><? }?></select> <script language="JavaScript">SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");</script></td><td width="20%" align="right"><font color="#005b90"><b>City*&nbsp;</b></font></td><td width="37%"><select name="City" style="font-family:verdana; font-size: 11px"><option value="">- Select City-</option></select></div><script language="JavaScript">SelectOption(document.forms[0].City, "<?=$_SESSION[City];?>");</script></td></tr><tr bgcolor="#ededed"><td width="17%" height="20" align="right"><font color="#005b90"><b>Phone*&nbsp;</b></font></td><td width="26%"><input type="text" name="Phone" value="<?=$_SESSION["Phone"]; ?>" maxlength="25"></td><td width="20%" align="right"><font color="#005b90"><b>Mobile&nbsp;</b></font></td><td width="37%"><input type="text" name="Mobile" value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td></tr><tr bgcolor="#ededed"> <td width="17%" height="20" align="right"><font color="#005b90"><b>Email&nbsp;</b></font></td><td width="26%"><input type="text" name="email" value="<?=$_SESSION["email"]; ?>" maxlength="25"></td><td width="20%" align="right"><font color="#005b90"><b>Documents Provided&nbsp;</b></font></td><td width="37%"><input type="text" name="documentProvided" value="<?=$_SESSION["documentProvided"]; ?>" maxlength="100"></td></tr></table>';
/*newBenificiaryTable = '<table width="100%" cellpadding="2" cellspacing="1" border="0">
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Title&nbsp;
                    </b></font></td>
                  <td width="26%"> <select name="benTitle">
                      <option value="Mr.">Mr.</option>
                      <option value="Miss.">Miss</option>
                      <option value="Dr.">Dr.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="Other">Other</option>
                    </select><script language="JavaScript">
         	SelectOption(document.forms[0].benTitle, "<?=$_SESSION["benTitle"]; ?>");
                                </script></td>
                  <td width="20%" align="right"><font color="#005b90"><b>First 
                    Name*&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="benfirstName" value="<?=$_SESSION["benfirstName"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Middle Name&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="benmiddleName" value="<?=$_SESSION["benmiddleName"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Last 
                    Name*&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="benlastName" value="<?=$_SESSION["benlastName"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right" valign="top"><font color="#005b90"><b>ID Type&nbsp;</b></font></td>
                  <td width="26%"><input type="radio" name="benIDType" value="Passport" <? if ($_SESSION["benIDType"] == "Passport" || $_SESSION["benIDType"] == "") echo "checked"; ?>> Passport <br><input type="radio" name="benIDType" value="ID Card" <? if ($_SESSION["benIDType"] == "ID Card") echo "checked"; ?>>
                    ID Card<br>
                    <input type="radio" name="benIDType" value="Driving License" <? if ($_SESSION["benIDType"] == "Driving License") echo "checked"; ?>>
                    Driving Licence</td>
                  <td width="20%" align="right" valign="top"><font color="#005b90"><b>ID Numnber*&nbsp;</b></font></td>
                  <td width="37%" valign="top"><input type="text" name="benIDNumnber " value="<?=$_SESSION["benIDNumnber "]; ?>" maxlength="32"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Address Line 1*&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="Address" value="<?=$_SESSION["Address"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Address Line 2&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="Address1" value="<?=$_SESSION["Address1"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Country*&nbsp;</b></font></td>
                  <td width="26%"><select name="Country" style="font-family:verdana; font-size: 11px" onChange="ChangeDetail(document.forms[0], \'add-agent.php\');">
                <option value="">- Select Country-</option>
                <option value="United States">United States</option>
                <option value="Canada">Canada</option>
                <?
					$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
                <option value="<?=$countires[$i]["country"]; ?>">
                <?=$countires[$i]["country"]; ?>
                </option>
                <?
					}
				?>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script></td>
                  <td width="20%" align="right"><font color="#005b90"><b>City*&nbsp;</b></font></td>
                  <td width="37%"><select name="City" style="font-family:verdana; font-size: 11px">
                <option value="">- Select City-</option> 
              </select></div>
              <script language="JavaScript">
         	SelectOption(document.forms[0].City, "<?=$_SESSION[City];?>");
                                </script></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Phone*&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="Phone" value="<?=$_SESSION["Phone"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Mobile&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="Mobile" value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Email&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="email" value="<?=$_SESSION["email"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>&nbsp;</b></font></td>
                  <td width="37%">&nbsp;</td>
                </tr>
              </table>';*/