<?
//session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
// include ("javaScript.php");
$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
	$offset = 0;
	
if($limit == 0)
	$limit=50;

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

$_SESSION["back"] = "";

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";
	
$moneyPaid = "";

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

			$_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			  = "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"] = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"]     = "";	
			$_SESSION["currencyCharge"] = "";
			$_SESSION["transDate"]="";


/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$agentID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}
		
if(CONFIG_VALUE_DATE_TRANSACTIONS == "1")
	$chequeAmountFieldWitTableAlias = "t.valueDate,";
$chequeAmountField = "valueDate,";
if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
{
	$chequeAmountFieldWitTableAlias .= "t.chequeAmount,";
	$chequeAmountField .= "chequeAmount,";
}	


if($_POST["btnAction"]=="Validate"){
	$ttrans = count($_POST["trnsid"]);
	for ($i=0;$i< $ttrans;$i++){
		if($_POST["trnsid"][$i] != ""){
			update("UPDATE ".TBL_TRANSACTIONS." SET validValue='Y' WHERE transID='".$_POST["trnsid"][$i]."'");
		}
	}
}
if($_POST["Submit"] == "Search" || $_GET["search"] == "Search")
{
	$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;

	$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
	$search = "Search";
	if($_POST["transID"] != "")
	{
		if($_POST["transID"] != "")
			{
				$id = $_POST["transID"];
			}
		if($_POST["searchBy"] != "")
			{
			$by = $_POST["searchBy"];
			}
	}
	
	if ($_POST["moneyPaid"] != "") {
		$moneyPaid = $_POST["moneyPaid"];
	} else if ($_GET["moneyPaid"] != "") {
		$moneyPaid = $_GET["moneyPaid"];
	}
	
	if($_REQUEST["transID"] != "")
	{
		$id = $_REQUEST["transID"];
	}
	if($_REQUEST["searchBy"] != "")
	{
		$by = $_REQUEST["searchBy"];
	}
	if($_REQUEST["nameType"] != "")
	{
		$nameType = $_REQUEST["nameType"];
	}
	if($_REQUEST["transID"] != "")
	{
		$transID = $_REQUEST["transID"];
	}
	if($_REQUEST["value_date"] != "")
	{
		$val_date = $_REQUEST["value_date"];
		$value_date_arr = explode("/",$val_date);
		$value_date = $value_date_arr[2]."-".$value_date_arr[1]."-".$value_date_arr[0];
	}
	if($id != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . "t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ".$extraCondition;
				$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ".$extraCondition;
				break;
			}
			case 1:
			{
				
				
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$transID);
										$nameClause = "(c.firstName LIKE '$name[0]' and lastName LIKE  '$name[1]') ";
									
								}else{
									
									$nameClause = " (c.".$nameType." LIKE '".$transID."%')";
									
									}
					
					
								 $query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 and ".$nameClause." ".$extraCondition;
								$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 and ".$nameClause." ".$extraCondition;
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
			  	
			  // searchNameMultiTables function end (Niaz)
			  
				//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				//$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				
			 $query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$q." ".$extraCondition;
				$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$q." ".$extraCondition;
			}
				
				
				break;
			}
			case 2:
			{
				
				
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$transID);
										$nameClause = "(b.firstName LIKE '$name[0]' and b.lastName LIKE  '$name[1]') ";
									
								}else{
									
									$nameClause = " (b.".$nameType." LIKE '".$transID."%')";
									
									}
					
					
								$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 and ".$nameClause." ".$extraCondition;
								$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 and ".$nameClause." ".$extraCondition;
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
			 
				
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 ".$q." ".$extraCondition;
				$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".$extraJoin." where 1 ".$q." ".$extraCondition;
				
				//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
				//$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
			}
				
				
				break;
			}
			case 3:
			{
				$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountFieldWitTableAlias . " t.benAgentID  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where 1 and u.name like '$id%' ".$extraCondition;
				$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where 1 and u.name like '$id%' ".$extraCondition;
				break;
			}

		}
		 
			 		
	}
	// Forcefully not displaying Transactions of Online Sender
	$query .= " and t.createdBy != 'CUSTOMER' ";
	$queryCnt .= " and t.createdBy != 'CUSTOMER' ";
	
	if($agentType == "Branch Manager"){
		$query .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID )";
		$queryCnt .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID) ";
	}
	
switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
}
if(defined("CONFIG_VERIFY_TRANSACTION_ENABLED") && CONFIG_VERIFY_TRANSACTION_ENABLED)
{
	$query .= " and (t.transStatus = 'Processing' OR t.transStatus ='Pending') ";
	$queryCnt .= " and (t.transStatus = 'Processing' OR t.transStatus ='Pending') ";
}else{
	$query .= " and (t.transStatus ='Pending')";	
	$queryCnt .= " and (t.transStatus ='Pending')";
}
	if ($moneyPaid != "") {
		
		$query .= " and (t.moneyPaid = '".$moneyPaid."')";	
		$queryCnt .= " and (t.moneyPaid = '".$moneyPaid."')";	
	}
if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){
	if(!empty($val_date)){
		$queryCnt .= " and valueDate LIKE '".$value_date."%' ";
		$query .= " and valueDate LIKE '".$value_date."%' ";
	}
	$queryCnt .= "  AND valueDate != '0000-00-00 00:00:00' AND validValue='N'";
	$query .= "  AND valueDate != '0000-00-00 00:00:00' AND validValue='N'";
}

$query .= "  order by t.transDate DESC";

$countRec = countRecords($queryCnt);
$totalContent = $countRec;
$query .= " LIMIT $offset , $limit";

$contentsTrans = selectMultiRecords($query);
if($_GET["allCount"] == ""){
	$allCount = $countRec;
}else{
	$allCount = $_GET["allCount"];
}

$rangeOffset = count($contentsTrans)+$offset;

//echo(" Agents are -->".count($totalContent)."--");
/* 
 $other = $offset + $limit;
 if($other > $countOnlineRec)
 {
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
  $query .= " LIMIT $offset2 , $limit2";
	$contentsTrans = selectMultiRecords($query);
 }*/
}else{

	$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
	$query = "select t.transID,t.custAgentID,t.refNumber,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.totalAmount, " . $chequeAmountField . " t.benAgentID from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
	
	
	if($agentType == "Branch Manager")
	{
    $query .= " and (custAgentParentID ='$agentID' OR benAgentParentID =  $agentID)";
    $queryCnt .= " and (custAgentParentID ='$agentID' OR benAgentParentID =  $agentID)";
		   
	}
	switch ($agentType)
	{
		case "SUPA":
		case "SUPAI":
		case "SUBA":
		case "SUBAI":
			$query .= " and (custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
			$queryCnt .= " and (custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		
	}
	$query .= " and createdBy != 'CUSTOMER' ";
	$queryCnt .= " and createdBy != 'CUSTOMER' ";
	
if(defined("CONFIG_VERIFY_TRANSACTION_ENABLED") && CONFIG_VERIFY_TRANSACTION_ENABLED)
{
	$query .= " and (transStatus = 'Processing' OR transStatus ='Pending') ";
	$queryCnt .= " and (transStatus = 'Processing' OR transStatus ='Pending') ";
}else{
	$query .= " and (transStatus ='Pending')";	
	$queryCnt .= " and (transStatus ='Pending')";
}
if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){
	$queryCnt .= "  AND valueDate != '0000-00-00 00:00:00' AND validValue='N'";
	$query .= "  AND valueDate != '0000-00-00 00:00:00' AND validValue='N'";
}	
	$allCount = countRecords($queryCnt);
	
	$query .= " order by transDate DESC";
  	$query .= " LIMIT $offset , $limit";

	$contentsTrans = selectMultiRecords($query);
	$rangeOffset = count($contentsTrans)+$offset;	
}
//debug($query);
?>
<html>
<head>
	<title>Update Value Date Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javaScript.js"></script>
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
// end of javascript -->
</script>
<script language="JavaScript">
$(document).ready(function(){
	$("#value_date").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-1 years"))?>'
	});
});

<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>


<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {
	color: #005B90;
	font-weight: bold;
}
.dp-choose-date{
	color:#000000;
}
-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong ><font color="#FFFFFF" size="2">Update Transactions</font></strong></td>
  </tr>
  <tr>
		<td>
			<table width="255" border="1" cellpadding="5" bordercolor="#666666" align="center">
        <form action="validate-value-transactions.php" method="post" name="Search">
          <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
          </tr>
          <tr>
            <td align="center" nowrap>
            	<input name="transID" type="text" id="transID" value="<?=$id?>" size="15" style="font-family:verdana; font-size: 11px; width:100"> 
              <select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
                  <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
                  <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
                  <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
						<?	if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
							{
							?>
							 	<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Name</option>
							<?
							}
						?>
              </select>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			<br>Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Mode -</option>
          <option value="By Cash" <? echo ($moneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
          <option value="By Cheque" <? echo ($moneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
          <option value="By Bank Transfer" <? echo ($moneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
		  </select>
		<?	}  ?>
		<?php if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){?>
			<br>Value Date 
	      <INPUT NAME="value_date" TYPE="text" ID="value_date" VALUE="<?=$_POST["value_date"]; ?>">
	  <?php }?>
			</td>
</tr>
		<? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
						</select>
   		
  	</td></tr>
  	<? } ?>
		<tr>
			<td align="center">
		
                <input type="submit" name="Submit" value="Search"></td>
          </tr>
        </form>
      </table>
		</td>
	</tr>
  
  
  <tr>
    <td align="center">

      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="validate-value-transactions.php?" method="post" name="trans">
			<tr>
    		<td>
    			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="250">
                    <?php if (count($contentsTrans) > 0) {?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($rangeOffset);?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=$search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&allCount=$allCount&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=$search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  				
                <?php } ?>
                <?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=$search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&allCount=$allCount&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&search=$search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&allCount=$allCount&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 					
                  <?php } 
                	}
                  ?>
                </tr>
              </table>
    		</td>
    	</tr>
	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo(count($contentsTrans))?> records to Update.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
			  <?php if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){?>
			  <td><span class="style1">Value Date</span></td>
			  <?php }?>
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?> </span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
				<?
					if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
					{
						?>
							<td><span class="style1">Cash Amount</span></td>
							<td><span class="style1">Cheque Amount</span></td>
						<?
					}
				?>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td width="100"><span class="style1">Sender Agent</span></td>
			  </tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				$tempTransId = $contentsTrans[$i]["transID"];
				?>
				<? 
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
										
										
									/*	if ($onlineCustomerDetails["status"] == "Disable"){
									
																$fontColor = "#CC000";
																$custStatus = "Disable";
									}elseif ($onlineCustomerDetails["status"] == "Enable" || $onlineCustomerDetails["status"]=""){
										
																$fontColor = "#006699";
																$custStatus = "Enable";
										
									}*/
									
								}else{
									$fontColor = "#006699";
									}
				
				?>
				<tr bgcolor="#FFFFFF">
				 <td width="75" bgcolor="#FFFFFF"><input type="checkbox" name="trnsid[]" id='<?=$tempTransId?>' value='<?=$tempTransId?>' ></td>
	 			  <?php if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){?>
					<td width="100" bgcolor="#FFFFFF"><? echo dateFormat($contentsTrans[$i]["valueDate"], "2")?></td>
				  <?php }?>
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"><strong><font color="<? echo $fontColor; ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				 	<?
						if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
						{
							$cashAmount = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["chequeAmount"];
							$chequeAmt = ($contentsTrans[$i]["chequeAmount"] > 0 ? $contentsTrans[$i]["chequeAmount"] : 0);
							?>
								<td><?=number_format($cashAmount, 4) .  " " . $contentsTrans[$i]["currencyFrom"]; ?></td>
								<td><?=number_format($chequeAmt, 4) .  " " . $contentsTrans[$i]["currencyFrom"];?></td>
							<?
						}
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]);?></td>
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
				 	 <? } ?>
				 	 
			    </tr>
				<?
			}
			?>
					<? for($i=0;$i < count($onlinecustomer);$i++)
					{
					$tempTransId = $onlinecustomer[$i]["transID"];
					?>
               <tr bgcolor="#FFFFFF">
				 <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?><input type=checkbox name="trnsid[]" id='<?=$tempTransId?>' value='<?=$tempTransId?>' ></td>
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["transAmount"])?></td>
				  
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
			    </tr>
          <?
					}
					?> 
			<tr bgcolor="#FFFFFF">
			<?php if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){?>
			  <td>&nbsp;</td>
			<?php }?>  
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<? echo ($i + $j); ?>'>
			<? 	if(count($contentsTrans) > 0){ ?>
			    <input name="btnAction" type="submit"  value="Validate" onClick="return confirm('Are you sure to validate?');"/>
			<?php }?>
				</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
			<?
			//} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
    </table></td>
  </tr>

</table>
</body>
</html>
