<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
		$currExchSQL = ("select ce.*,a.name,a.userID,c.firstname, c.middlename, c.lastname from curr_exchange_account ce LEFT JOIN customer c ON ce.customerID = c.customerID,admin a WHERE ce.createdBy=a.userID ");

		$currExchSQL.=" AND ce.transStatus !='Cancelled'";
		if($_REQUEST["Submit"] == "Search")
		{
			if(!empty($_REQUEST["from_date"]))
			{
				$date = explode("/",$_REQUEST["from_date"]);
				$currExchSQL .= " and created_at >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
			}
			
			if(!empty($_REQUEST["to_date"]))
			{
				$date = explode("/",$_REQUEST["to_date"]);
				$currExchSQL .= " and created_at <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
			}
		
			if(!empty($_REQUEST["clientName"])){
				$names = $_REQUEST["clientName"];
				$currExchSQL .= " and (firstName like '%".$names."%' || middleName like '%".$names."%' || lastName like '%".$names."%') ";
			}
			if(!empty($_REQUEST["createdBy"])){
				$currExchSQL .= " and name like '%".$_REQUEST["createdBy"]."%'";
			}
			if(!empty($_REQUEST["type_of_exchange"])){
				$currExchSQL .= " and buy_sell ='".$_REQUEST["type_of_exchange"]."'";
			}
			if(CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE=="1"){
				if(!empty($_REQUEST["currency"])){
					$currExchSQL .= " and buysellCurrency ='".$_REQUEST["currency"]."'";
				}
			}
		}

	$currExchData = selectMultiRecords($currExchSQL);

	$exportType = $_REQUEST["exportType"];

	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "XLS")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=buySelCommissionExport.".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=buySelCommissionExport.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Currency Exchange Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Rate ID".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Client Name".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Date In".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Type of Exchange".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Currency Buy/Sell".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Local Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Fee".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Local Total Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Created By".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	$localAmountTotal = 0;
	$totalAmountTotal = 0;
	$totalFee		  = 0;
	foreach($currExchData as $key => $val)
	{
		$exchangeType = $val["buy_sell"] =="B" ? "Buy" : "Sell";
		$opcurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$val["operationCurrency"]);
		$bscurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$val["buysellCurrency"]);
		$opcurrName   = $opcurrData['currencyName'];
		$opcountName  = $opcurrData['country'];
		$bscurrName   = $bscurrData['currencyName'];
		$bscountName  = $bscurrData['country'];
		$creator 	  = $val["createdBy"];

		$localAmountTotal +=$val['localAmount'];
		$totalAmountTotal +=$val["totalAmount"];
		$total_localAmountTotal +=$val["total_localAmount"];
		$totalFee		  +=$val["currency_fee"];

		if($exportType == "CSV")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$val["status"]."'>";
			
		$strFullHtml .= $strColumnStart.$val["id"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["firstname"]." &nbsp".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["created_at"].$strColumnEnd;

		$strFullHtml .= $strColumnStart.$exchangeType.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$bscurrName.$strColumnEnd;

		$strFullHtml .= $strColumnStart.$val['localAmount'].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["currency_fee"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["total_localAmount"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["totalAmount"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["name"].$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		
	}
	if(CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE=="1" && !empty($_REQUEST["currency"])){
		$strFullHtml .= $strRowStart;
		$strFullHtml .= $strColumnStart."Local Amount".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$localAmountTotal." GBP".$strColumnEnd;
		$strFullHtml .= $strColumnStart."Total Fee".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$totalFee." GBP".$strColumnEnd;
		$strFullHtml .= $strColumnStart."Total Local Amount".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$total_localAmountTotal." GBP".$strColumnEnd;
		$strFullHtml .= $strColumnStart."Total Amount".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$totalAmountTotal." ".$bscurrName.$strColumnEnd;
		for($s=0;$s<7;$s++)
				$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
	}	
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>