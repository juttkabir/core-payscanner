<?php
session_start();
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$currentDate = date('d/m/Y');

$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$amount2 = 0;

$condition = "";
$value = "";
$trnsid = "";
if($_GET["fileID"] != ""){
	
$fileID = $_GET["fileID"];

}elseif($_POST["fileID"] != ""){
	
$fileID = $_GET["fileID"];
	
}

if($_POST["trnsid"] != ""){
	$trnsid = $_POST["trnsid"];
}

for($i = 0; $i < count($trnsid); $i++){
	$trnsid[$i] = "'".$trnsid[$i]."'";
}
$trnsidList = implode(",",$trnsid);

if(CONFIG_EXPORT_TRANS_OLD == '1')
{
	if ($_GET['old'] == 'Y') {
		$condition = " and is_exported = 'Y' ";
		$value = "old";
	} else {
		$condition = " and is_exported = '' ";
		$value = "current";
	}
	
	update("update transactions set dispatchDate = '".date("Y-m-d")."' where transID IN(".$trnsidList.")");	
}

//$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");


if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	
	$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
		
	$condition .= " and (transStatus = 'Authorize' or transStatus = 'Amended')".$FileConditionQuery["conditions"]." ";
}else{
	
	 $condition .= " and benAgentID = '100264'";
	}

//$condition .= " and transStatus = 'Authorize'";

 $queryCnt = "select COUNT(*) from ". TBL_TRANSACTIONS." where transID IN(".$trnsidList.")$condition ";
 $countTrans = countRecords($queryCnt);

$disticntCurrency = selectMultiRecords ("select DISTINCT(currencyTo) from ".TBL_TRANSACTIONS." where 1 $condition");
$curr = explode(',',$disticntCurrency);


if ($countTrans <= 0) {
	insertError("Please select a transaction to export.");
	redirect("select_trans_to_export_opal.php?msg=Y&fileID=$fileID&old=".$_GET['old']."");
}

if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
} else {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and File_Name = 'export_trans_file.php'");
}

$lineBreak = $fileFormatQuery["lineBreak"];
$fieldSpacing = $fileFormatQuery["fieldSpacing"];
if ($fieldSpacing == 'tab') {
	$fieldSpace = "\t";	
} else if($fieldSpacing != '' && $fieldSpacing != 'space'){
	$fieldSpace = $fieldSpacing;
	}else {
	$fieldSpace = " ";
}
$fileFormat = $fileFormatQuery["Format"];
$fileID = $fileFormatQuery["id"];
if($fileFormatQuery["fileName"] != ''){
	$date_time = date("dmy.h:i");
	$fileNamed = $fileFormatQuery["fileName"].$date_time;
}else{
	$date_time = date('d-m-Y-h-i-s-A');
	$fileNamed = $username."_".$date_time;
}
	if ($fileFormat == "txt") {
		$appType = "txt";
	} else if ($fileFormat == "csv") {
		$appType = "csv";
	} else {
		$appType = "x-msexcel";	
	}
	
	header ("Content-type: application/$appType");
	if($FileConditionQuery["conditions"] == "and toCountry = 'Lithuania'"){
		
		$newFormatVal = "mokesis";
	
	}else{
		
		$newFormatVal = $fileFormat;
	}
	
	header ("Content-Disposition: attachment; filename=$fileNamed.$newFormatVal"); 
	header ("Content-Description: PHP/INTERBASE Generated Data");         

 $query = "select * from ". TBL_TRANSACTIONS." where transID IN(".$trnsidList.")$condition ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	$query .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";

$contentTrans = selectMultiRecords($query);

if ($fileFormat == "xls")
{

	
	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	
if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
	$data = "<table width='900' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	$data .="<tr>";
	
	
	
		$data .= "<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["heading"]."</font>\n";	
		
		
			if($labelQuery["address"] == 'Y'){
		
			$data .= "<font face='Verdana' color='#000000' size='2'><b>".COMPANY_ADDR."</font></td>\n";
		}
		
		
		
		$data .="</tr>";
		$data .="<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label1"]."</font></td>\n</tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label2"]." ".$currentDate." ".$labelQuery["label3"]."</font></td>\n</tr>
						<tr><td>&nbsp;</td></tr>
						</table>";
	
		
	}
	$data .= "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	if($fileFormatQuery["showLable"] == 'Y'){
		$data .="<tr>";
	}
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
	
		if($fileFormatQuery["showLable"] == 'Y'){
			$data .= "<td><font face='Verdana' color='#000000' size='2'><b>".$fieldQuery[$j]["Lable"]."</font></td>\n";	
		}
	
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
	
		/* this is if any prededined value is to be printed against a respective field*/
		
		if($fieldQuery[$j]["isFixed"]== 'Y' && $fieldQuery[$j]["appearOnce"] != 'Y'){
			$fixedValue[$j] = $fieldQuery[$j]["Value"];
		}
		if($fieldQuery[$j]["isFixed"]== 'N' && $fieldQuery[$j]["appearOnce"] != 'Y' && $fieldQuery[$j]["Value"]!= ""){
			$fixVal[$j] = $fieldQuery[$j]["Value"];
		}
			$dataTable[$j] = $fieldQuery[$j]["tableName"];
			
			$dataPattern[$j] = $fieldQuery[$j]["customPattern"];
			//$data .="</tr>";
			if($fieldQuery[$j]["appearOnce"] == 'Y'){
			$data .="<tr>";
			$data .= $fieldQuery[$j]["Value"];	
			$data .= "</tr>";
		}
	}
	if($fileFormatQuery["showLable"] == 'Y'){
		$data .="</tr>";
	}
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		
		
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y', dispatchDate = '".date("Y-m-d")."' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		//$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName,middleName,lastName,accountName from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		    if($beneContent["paymentDetails"] != ""){
					$paymentDetails[$i] = $beneContent["paymentDetails"].",";
				}else{
					$paymentDetails[$i] = "";
				}
				$arrayPmt[$i] = $paymentDetails;
	  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		//echo "select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'";
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select IBAN from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select IBAN from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						//$bankName = $bankDetails["bankName"];						
						//$account = $bankDetails["accNo"];			
						//$accountType = $bankDetails["accountType"];		
						//$brCode = $bankDetails["branchCode"];						
						//$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						//$Remarks = $bankDetails["Remarks"];
					}
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
/*	if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= " <tr>
		
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$refNumberIM</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$transDate</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Express</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$accountName</font></td>";
		
		$data .="
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$customerName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$collectionPointBen</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$foriegnAmount</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$currencyTo</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$IBAN</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$Remarks</font></td>
		</tr>";
		  	
	
	$data.="<tr>	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>";
			
			$data.="<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	</tr>";
}  else {	*/
		//$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		//$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		
	$data .= " <tr>";
	
		for($k = 0; $k < count($dataVariable); $k ++)
		{
			
			$value = "";
			$field = explode(',',$dataVariable[$k]); // this it to break a string on any index of the array[array containing database fields]
		
      if($fieldQuery[$k]["isFixed"]== 'Y' && $fieldQuery[$k]["appearOnce"] != 'Y')
      {
      	$value .= $fixedValue[$k]; 
      }else{
					

			/*
values for the fields are pulled from the respective tables in the database

*/
					if($dataTable[$k] == 'transactions')
						{
					
							for($ex = 0; $ex < count($field); $ex++){
								if($ex > 0)
								{
									$value .=" ";	
								}
								if($field[$ex] == "localAmount" && $contentTrans[$i]["toCountry"] == "Lithuania"){
									$value .=number_format($contentTrans[$i][$field[$ex]],2,'','');
								}else{
									$value .= $contentTrans[$i][$field[$ex]];
								}
								
								
								if($field[$ex] == "localAmount")
								{
									if($contentTrans[$i]["toCountry"] == "United Kingdom" &&  CONFIG_TOCOUNTRY_UK == '1'){
										$amount2 +=  $contentTrans[$i]["localAmount"];
									}else{
										$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
										$amount +=  $contentTrans[$i]["localAmount"];
										$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
									}
								}
																																				
						}
						
											
						}elseif($dataTable[$k] == 'beneficiary'){
								for($ex = 0; $ex < count($field); $ex++)
								{
									if($ex > 0)
									{
										$value .=" ";	
									}	
										$value .= $beneContent[$field[$ex]];
									}
										
							}elseif($dataTable[$k] == 'customer'){

								for($ex = 0; $ex < count($field); $ex++)
								{
									if($dataPattern[$k] == 'Y' )
									{	
										if($ex == 0)
											$value .= $arrayPmt[$i]." From ";
										if($ex == 2)
													$value .= ",";
									}
									if($ex > 0){
										$value .= " ";
									}
										$value .= $customerContent[$field[$ex]];					
								}
							}elseif($dataTable[$k] == 'bankDetails'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
										$value .= $bankDetails[$field[$ex]];					
							}
							}
							
							/*if($contentTrans[$i]["transType"]=="Pick up" && $dataTable[$k] == 'cm_collection_point'){
							
									for($ex = 0; $ex < count($field); $ex++)
									{

										if($ex != 0)
												$value .= "-";
										$value .= " ".$collectionPoint[$field[$ex]];					
									}
							}elseif($contentTrans[$i]["transType"]=="Bank Transfer" && $dataTable[$k] =='bankDetails'){
							
							for($ex = 0; $ex < count($field); $ex++)
										$value .= " ".$bankDetails[$field[$ex]];					
							}*/
							
						/*if($value == "")
						{
								$value = "None";
							}*/
					if($value == ""){
						$value .= $fixVal[$k];
					}
				}	
					if($dataVariable[$k] == 'transDate')
					{
						if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
							$value = dateFormat($value, 4);
						}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
							$value = dateFormat($value, 3);
						}else{
							$value = dateFormat($value, 1);
						}
					}
					
					if($dataVariable[$k] == 'dispatchDate')
					{
						if(!empty($value))
						{
							if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
							}
						}
						
					}

					
					if($fieldQuery[$k]["appearOnce"] != 'Y')
					{		
						$data .="<td><font color='#000000' size='1' face='Verdana'>$value</font></td>";
					}
			}
			$data .="</tr>";
		
	
	//}
	}
	if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME == $fileFormatQuery["FileLable"]){
		$comm = 0;		
		$distEnd = 0;	
		$comm2 = 0;
		$moneyOwed = 0;		
		$data .="<tr><td colspan='6'>&nbsp;</td></tr>";
					
		$z=0;
				foreach($currencyArray as $keys => $values){
					$comm = $values * 0.008;
					$distEnd = $values + 	$comm;	
					$comm2 = $amount2 * 0.005;		
					$moneyOwed = $amount2 + $comm2;
					
					
					if($z > 0){
				
						$data .="	<tr><td colspan='6'>&nbsp;</td></tr>";
						
						}	
						
						
	$data .="<td><font color='#000000' size='1' face='Verdana'>Total for ".$keys."</font></td>
					<td>&nbsp;</td>
					<td><font color='#000000' size='1' face='Verdana'>'".$values."'</font></td>
			<td><font color='#000000' size='1' face='Verdana'>'".$keys."'</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Incoming Transfer</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".$amount2."</font></td>";
			}else{
				$data .="<td>&nbsp;</td>
				<td>&nbsp;</td>";
			} 
			
		$data .="	
			</tr>
			<tr><td><font color='#000000' size='1' face='Verdana'>Calculation of commission for opal</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>". number_format($comm,2,'.','')."</font></td>
			<td><font color='#000000' size='1' face='Verdana'>".$keys."</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Opal Commission</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".number_format($comm2,2,'.','')."</font></td>";
			}else{
					$data .="<td>&nbsp;</td>
					<td>&nbsp;</td>";
			}
			
			
		$data .="	
			</tr>
			<tr><td><font color='#000000' size='1' face='Verdana'>Money to be given to the distribution end</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>".number_format($distEnd,2,'.','')."</font></td>
			<td><font color='#000000' size='1' face='Verdana'>".$keys."</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Money Owed</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".number_format($moneyOwed,2,'.','')."</font></td>";
			}else{
				$data .="<td>&nbsp;</td>
				<td>&nbsp;</td>";
			}
			
		$data .="	
			
			</tr>";
	
	if(CONFIG_TOCOUNTRY_UK == '1'){
		if($keys == 'USD'){
			$netDebt = $distEnd - $moneyOwed;
			$data .="<tr>
			<td><font color='#000000' size='1' face='Verdana'>Net Debt</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>".number_format($netDebt,2,'.','')."</font></td>
			<td>&nbsp;</td>
			</tr>";
		}
	}
			$z++;
		
			}
			
		}
	
$data.="</table>";
}
elseif ($fileFormat == "csv")
{

	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
		
		$data .= $labelQuery["heading"]."$fieldSpace";	
		$data .= "\r\n";
		$data .= COMPANY_ADDR;
		$data .= $labelQuery["label1"]."$fieldSpace";	
		$data .= "\r\n";
		$data .= $labelQuery["label2"]."$fieldSpace".$currentDate. $labelQuery["label3"];	
		$data .= "\r\n";
	
		}

	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
	if($fileFormatQuery["showLable"] == 'Y'){	
		$data .= $fieldQuery[$j]["Lable"]."$fieldSpace";	
	}
	
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y' && $fieldQuery[$j]["appearOnce"] != 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}
	if($fieldQuery[$j]["isFixed"]== 'N' && $fieldQuery[$j]["appearOnce"] != 'Y' && $fieldQuery[$j]["Value"]!= ""){
			$fixVal[$j] = $fieldQuery[$j]["Value"];
	}
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
	$dataPattern[$j] = $fieldQuery[$j]["customPattern"];	
	
	if($fieldQuery[$j]["appearOnce"] == 'Y'){
			$data .= $fieldQuery[$j]["Value"];	
			$data .= "\r\n";
		}
		
	}
	if($fileFormatQuery["showLable"] == 'Y'){
		$data .= "\r\n";
	}
//	$data .= "\r\n";
	
	
	
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y', dispatchDate = '".date("Y-m-d")."' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		//$transDate = dateFormat($date,"3");
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		    if($beneContent["paymentDetails"] != ""){
					$paymentDetails[$i] = $beneContent["paymentDetails"].",";
				}else{
					$paymentDetails[$i] = "";
				}
				$arrayPmt[$i] = $paymentDetails;
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select IBAN from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select IBAN from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
					}	
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
	/* if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= "$refNumberIM,,$transDate,Express";
		
	  	
		$data .= ",$accountName,$customerName,,,,,,,$collectionPointBen,$beneName,$foriegnAmount,$currencyTo,$IBAN,$Remarks\r\n";
	} else*/ { 	
		
			for($k = 0; $k < count($dataVariable); $k ++)
		{
			$value = "";
			$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
		
      if($fieldQuery[$k]["isFixed"]== 'Y' && $fieldQuery[$k]["appearOnce"] != 'Y')
      {
      	$value .= $fixedValue[$k]; 
      }else{
								
/*
values for the fields are pulled from the respective tables in the database

*/
			
					if($dataTable[$k] == 'transactions')
						{
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
									$value .= " ";
								}
								if($field[$ex] == "localAmount" && $contentTrans[$i]["toCountry"] == "Lithuania"){
									$value .=number_format($contentTrans[$i][$field[$ex]],2,'','');
								}else{
									$value .= $contentTrans[$i][$field[$ex]];
								}
								
								if($field[$ex] == "totalAmount")
								{
									$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
									$amount +=  $contentTrans[$i]["totalAmount"];
									$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
								}
							}	
						
						}elseif($dataTable[$k] == 'beneficiary'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
										$value .= $beneContent[$field[$ex]];
							}
						
						}elseif($dataTable[$k] == 'customer'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								
								if($dataPattern[$k] == 'Y' )
								{	
									if($ex == 0)
										$value .=  $arrayPmt[$i]." From ";
									if($ex == 2)
												$value .= ",";
								}
								if($ex > 0){
										$value .= " ";
									}
										$value .= $customerContent[$field[$ex]];					
							}
						}elseif($dataTable[$k] == 'bankDetails'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
										$value .= $bankDetails[$field[$ex]];					
							}
						}
						
					if($value == ""){
						$value .= $fixVal[$k];
					}
				}	
					if($dataVariable[$k] == 'transDate')
					{
						if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
							$value = dateFormat($value, 4);
						}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
							$value = dateFormat($value, 3);
						}else{
							$value = dateFormat($value, 1);
						}
					}
					
					if($dataVariable[$k] == 'dispatchDate')
					{
						if(!empty($value))
						{
							if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
							}
						}
						
					}
		
		/* values are now displayed here*/
		if($fieldQuery[$k]["appearOnce"] != 'Y')
		{	
			$data .= "$value,";
		}
			
	}
	
		
	}
	$data .= "\n\r";
	
	
		
	if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME != $fileFormatQuery["FileLable"]){
		
		$z=0;
		$data .= "\n\r";
				foreach($currencyArray as $keys => $values){
				
					$data .= "$values";
					
					}
		
		
		
		}	
	
	}
}
elseif ($fileFormat == "txt")
{
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		if($fileFormatQuery["showLable"] == 'Y'){
			$data .= $fieldQuery[$j]["Lable"]."$fieldSpace";	
			
		}
		
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
	
		/* this is if any prededined value is to be printed against a respective field*/
		
		if($fieldQuery[$j]["isFixed"]== 'Y' && $fieldQuery[$j]["appearOnce"] != 'Y'){
			$fixedValue[$j] = $fieldQuery[$j]["Value"];
		}
		if($fieldQuery[$j]["isFixed"]== 'N' && $fieldQuery[$j]["appearOnce"] != 'Y' && $fieldQuery[$j]["Value"]!= ""){
			$fixVal[$j] = $fieldQuery[$j]["Value"];
		}
			$dataTable[$j] = $fieldQuery[$j]["tableName"];
			
			$dataPattern[$j] = $fieldQuery[$j]["customPattern"];
			
		if($fieldQuery[$j]["appearOnce"] == 'Y'){
			$data .= $fieldQuery[$j]["Value"];	
			$data .= "\n";
		}
	}
	
	//	$data .= "$lineBreak\n\r";
		if($fileFormatQuery["showLable"] == 'Y'){
				$data .= "\n";
	}
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y', dispatchDate = '".date("Y-m-d")."' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		//$transDate = dateFormat($date,"3");
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		    if($beneContent["paymentDetails"] != ""){
					$paymentDetails = $beneContent["paymentDetails"].",";
				}else{
					$paymentDetails = "";
				}
				$arrayPmt[$i] = $paymentDetails;
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select IBAN from ".TBL_CM_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];	
						$accountType = $bankDetails["accountType"];					
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select IBAN from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];		
						$accountType = $bankDetails["accountType"];				
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
					}		
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
	/*if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= $refNumberIM ."$fieldSpace".$transDate."$fieldSpace"."Express";
		
	  	
		$data .= "$fieldSpace"."$accountName $fieldSpace"."$customerName $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"."$collectionPointBen $fieldSpace"."$beneName $fieldSpace"."$foriegnAmount $fieldSpace"."$currencyTo $fieldSpace"."$IBAN $fieldSpace"."$Remarks $lineBreak\r\n";
	} else {*/
		for($k = 0; $k < count($dataVariable); $k ++)
		{
			$value = "";
			$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
		
      if($fieldQuery[$k]["isFixed"]== 'Y' && $fieldQuery[$k]["appearOnce"] != 'Y')
      {
      	$value .= $fixedValue[$k]; 
      }else{
								
/*
values for the fields are pulled from the respective tables in the database

*/
			
					if($dataTable[$k] == 'transactions')
						{
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
									if($field[$ex] == "localAmount" && $contentTrans[$i]["toCountry"] == "Lithuania"){
										$value .=number_format($contentTrans[$i][$field[$ex]],2,'','');
									}else{
										$value .= $contentTrans[$i][$field[$ex]];
									}
							}
											
						}elseif($dataTable[$k] == 'beneficiary'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex == 0){
									$value .= "\"";
								}
								
								if($dataPattern[$k] == 'Y' )
								{	
									if($ex == 1){
										$value .=" ";	
									}
									if($ex > 1){
										$value .= "|";
									}
								}elseif($ex > 0)
								{
									$value .=" ";	
								}	
							
										$value .= $beneContent[$field[$ex]];
								if($ex == (count($field)-1)){
									$value .= "\"";
								}
							}			
							}elseif($dataTable[$k] == 'customer'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($dataPattern[$k] == 'Y' )
									{	
										if($ex == 0)
											$value .= $arrayPmt[$i]." From ";
										if($ex == 2)
													$value .= ",";
									}
								if($ex > 0){
										$value .= " ";
									}
							
										$value .= $customerContent[$field[$ex]];					
							}
						}elseif($dataTable[$k] == 'bankDetails'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex == 0){
									$value .= "\"";
								}
								if($ex > 0){
										$value .= " ";
									}
										$value .= $bankDetails[$field[$ex]];		
								if($ex == (count($field)-1)){
								$value .= "\"";
								}			
							}
						}
						
						if($value == ""){
							$value .= $fixVal[$k];
						}
				}	
				
				if($dataVariable[$k] == 'IBAN')
				{
					$value = str_replace("PL","",$value);
				}
				
				if($dataVariable[$k] == 'localAmount' && $fileFormatQuery["FileLable"] == "Transactions For Poland")
				{
						$value = number_format($value,2,'','');
				}
					
					if($dataVariable[$k] == 'transDate')
					{
						/**
						 * Commented code as describe in Ticket #3862  
						if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
							$value = dateFormat($value, 4);
						}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
							$value = dateFormat($value, 3);
						}else{
							$value = dateFormat($value, 1);
						}
						*/
						$value = "";
					}	
								
					if($dataVariable[$k] == 'dispatchDate')
					{
						if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
							$value = dateFormat($value, 4);
						}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
							$value = dateFormat($value, 3);
						}else{
							$value = dateFormat($value, 1);
						}
					}
					
		if($k != (count($dataVariable)-1)){			
			$value .= $fieldSpace;
		}
		
		/* values are now displayed here*/
	
	if($fieldQuery[$k]["appearOnce"] != 'Y')
	{	
		$data .= $value;
	}
		
		
	}
	//}
	
	$data .= "$lineBreak";
	$data .= "\r\n";
	}
}
echo $data;

?>