<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$mtlink = mysql_connect("staging.horivert.com",MT_USER, MT_PASSWORD);
	if(!mysql_select_db(MTDB, $mtlink))
		die("Could not connect to ".MTDB." database.");

	
	if($_REQUEST["vi"] == "dt" && !empty($_REQUEST["id"]))
	{
		$strSql = "select * from ".MTDB.".external_reponse_code where id = '".$_REQUEST["id"]."'";
		$arrData = selectFrom($strSql);
		
		$arrFiles = unserialize($arrData["files"]);
		
		//debug($arrData);
		$arrTransIds = explode(",",$arrData["trans_ids"]);
		?>
		
		<table align="center" width="60%" cellpadding="2" cellspacing="1">
			<tr>
				<td colspan="4" align="left" class="reportHeader">
					Detail Report of Communication
				</td>
			</tr>
			<tr>
				<td>Total Files Received:</td>
				<td><u><?=count($arrFiles)?></u></td>
				<td>Total Transaction Found:</td>
				<td><u><?=$arrData["total_transactions"]+$arrData["rejected"]?></u></td>
			</tr>
			<tr>
				<td>Parsed Transaction(s):</td>
				<td><u><?=$arrData["parsed"]?></u></td>
				<td>Rejected Transaction(s):</td>
				<td><u><?=$arrData["rejected"]?></u></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="left" class="reportHeader">
					<b>Received Files</b>
				</td>
				<td colspan="2" align="left" class="reportHeader">
					<b>Received Transaction's <i><?=MANUAL_CODE?></i></b>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="left">
					<? for($i = 0; $i< count($arrFiles); $i++) { ?>	
						<b><?=$i+1?></b>)&nbsp;&nbsp;<?=substr(strrchr($arrFiles[$i], "/"), 1)?><br />
					<? } ?>
				</td>
				<td colspan="2" align="left">
					<? foreach($arrTransIds as $tk => $tv) 
					   { 
					   		$tv = trim($tv);
					   		if(!empty($tv))
							{
					?>
								<b><?=$tk+1?></b>)&nbsp;&nbsp;<?=$tv?><br />
					<? 
							}
					   } 
					?>
				</td>
			</tr>

		</table>
		
		<?
		
		exit;
	}
	
	
	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	//$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
		
	if(!empty($_REQUEST["Submit"]))
	{
		$Submit = $_REQUEST["Submit"];
	}
			
	if(!empty($_REQUEST["transID"]))
	{
		$transID = $_REQUEST["transID"];
	}
	
		
	$query = "select * from ".MTDB.".external_reponse_code where client_id = '1'";
	$queryCnt = "select count(*) from ".MTDB.".external_reponse_code where client_id = '1'";
	
	if(empty($_REQUEST["fYear"]) || empty($_REQUEST["fMonth"]) || empty($_REQUEST["fDay"]))
		$fromDate = time() - (24 * 60 * 60);
	else
		$fromDate = mktime(0,0,0,$_REQUEST["fMonth"],$_REQUEST["fDay"],$_REQUEST["fYear"]);
		
	if(empty($_REQUEST["tYear"]) || empty($_REQUEST["tMonth"]) || empty($_REQUEST["tDay"]))
		$toDate = time();
	else
		$toDate = mktime(23,59,59,$_REQUEST["tMonth"],$_REQUEST["tDay"],$_REQUEST["tYear"]);
	
	$filters = " and (datetime BETWEEN '$fromDate' AND '$toDate')";
	
	/* Search Filters */
	$query .= $filters;
	$queryCnt .= $filters;
	//$allCount = countRecords($queryCnt);
	//$count = $allCount;
	//debug($query);
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	//debug($_REQUEST);
	//debug($query);
	
	if($count > 0)
		$total_pages = ceil($count / $limit);
	else
		$total_pages = 0;
	
	if($page > $total_pages)
		$page = $total_pages;
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	if($start < 0)
		$start = 0;
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());

	//debug($queryCnt);

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	$_arrInteractionSteps = array(
							"1" => "Getting Transactions",
							"5" => "Request for Transaction Cancellation"
						);	
	
	$intTotalNewTransaction = 0;
	
	while($row = mysql_fetch_array($result))
	{
		
		$response->rows[$i]['id'] = $row["id"];
		
		$response->rows[$i]['cell'] = array(
									date("d-m-Y H:i:s",$row["datetime"]),
									$_arrInteractionSteps[$row["interaction_step"]],
									count(unserialize($row["files"])),
									($row["total_transactions"]+$row["rejected"]),
									$row["parsed"],
									$row["rejected"]
								);
		
		if($row["interaction_step"] == "1")
			$intTotalNewTransaction += $row["parsed"];
		
		$i++;
	}
	
	$response->totalTransactions = $intTotalNewTransaction;
	
	echo $response->encode($response); 

?>