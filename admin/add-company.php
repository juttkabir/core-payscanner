<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	require_once("lib/phpmailer/class.phpmailer.php");
	$modifyby = $_SESSION["loggedUserData"]["userID"];
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$payinAmount = CONFIG_PAYIN_FEE;
	$company = COMPANY_NAME;
	$created_date = date("Y-m-d");
 require_once("lib/audit-functions.php");
	if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
	{	
		$ttTransFlag=true;
	}
	
	//debug($_REQUEST);
	/**
	 * If the record is for editing 
	 * For this check if the company_id field is empty or not, if not than request will be for editing the record
	 */
	$strMainCaption = "Add New Company Record"; 
	
	if(!empty($_REQUEST["customerID"]))
	{
			
			
		$strGetDataSql = "SELECT 
							  
							  agentID,
							  firstName,
							  Address,
							  
							  Address1,
							  Country,
							  City,
							  State,
							  email,
							  Zip,
							  Phone,
							  Mobile,
							  acceptedTerms,
							  senderRemarks,
							  customerNumber,
							  customerName,
							  password,
							  balance,
							  passport_number,
							  customerType,
							  company_msb_number,
							 
							  created ";
							  
		if(CONFIG_TRANS_TYPE_TT!="1")
			$strParentField=", parentID, source,  tradingAcccount, contactPerson";
			
		$strGetDataSql .=$strParentField;
			
		$strGetDataSql .= "	FROM 
								 ".TBL_CUSTOMER." 
							WHERE 
								customerID = '".$_REQUEST["customerID"]."'";
		
		$arrCompanyData = selectFrom($strGetDataSql);
		//debug($strGetDataSql);
		$companyName = $arrCompanyData["firstName"];
		$address = $arrCompanyData["Address"];
		$accManagerID = $arrCompanyData["parentID"];
		$agentID = $arrCompanyData["agentID"];
		$source = $arrCompanyData["source"];
		$address1 = $arrCompanyData["Address1"];
		$country = $arrCompanyData["Country"];
		$city = $arrCompanyData["City"];
		$state = $arrCompanyData["State"];
		$email = $arrCompanyData["email"];
		$zip = $arrCompanyData["Zip"];
		$phone = $arrCompanyData["Phone"];
		$passportValidate  = $arrCompanyData["passport_number"];
		$mobile = $arrCompanyData["Mobile"];
		$acceptedTerms = $arrCompanyData["acceptedTerms"];
		$senderRemarks = $arrCompanyData["senderRemarks"];
		$msbNumber = $arrCompanyData["company_msb_number"];
		$contactPerson = $arrCompanyData["contactPerson"];
		$tradingAccount = $arrCompanyData["tradingAcccount"];	
		$strMainCaption = "Modify Existing Company Record"; 
	}
	 
	

	/**
	 * Picking up the country and city data to display in the drop down list
	 */
	$arrCountries = selectMultiRecords("select countryname from countries order by countryname");
	if($_GET["customerID"] == "")
	{
		//if($_POST["passportValidate"] != "")
//		{
//			$_SESSION["passportValidate"] = $_POST["passportValidate"];	
//		}


		$_SESSION["tradingAcccount"] = "";
		if ($_POST["tradingAcccount"] != ""){
		$_SESSION["tradingAcccount"] = $_POST["tradingAcccount"];
		$_SESSION["email"] = "";
		if ($_POST["email"] != "")
		$_SESSION["email"] = $_POST["email"];
		}

	}
	else
	{
	
		$strQuery = "SELECT * FROM customer where customerID =".$_REQUEST["customerID"]."";
		$nResult = mysql_query($strQuery); 		
		$rstRow = mysql_fetch_array($nResult);		
		$_SESSION["tradingAcccount"] = $rstRow["tradingAcccount"];
		$_SESSION["email"] = $rstRow["email"];
		$agentID = $rstRow["agentID"];
		$passportValidate = $rstRow["passport_number"];
		
	}
$rsQuery = "SELECT id,accountName from accounts";
$arguments = array("flag"=>"tradingAccountsDropdown","selectHead"=>"tradingAcccount","query"=>$rsQuery,"selected"=>$_SESSION["tradingAcccount"]);
$tradingAccountsDropdown = gateway("CONFIG_TRADING_ACCOUNT_CUSTOMER",$arguments,CONFIG_TRADING_ACCOUNT_CUSTOMER);
$tradingAccountsL = $tradingAccountsDropdown["label"];
$tradingAccountsDDV = $tradingAccountsDropdown["dropValues"];
 //debug($_REQUEST);

?>
<script type="text/javascript">
function printFunction(){
	var printDiv = document.getElementById("printDiv");
	if(printDiv && printDiv!=null){
		//printDiv.style.display = 'none';
		print();
	}
}
// end of javascript -->
	
</script>	
<?php	
	//print_r($_REQUEST);
	$customerID = 0;
	if($_REQUEST["from"] == "popUp") {
		$returnPage = "add-transaction.php";
		}elseif($_REQUEST["from"] == "search-edit-cust"){
		$returnPage = "search-edit-cust.php";
		}
		elseif($_REQUEST["from"] == "alertPage"){
			$returnPage = "viewAlertControllerNew.php";
		}
	
		// @5278- Multiple ID Types

	// Fetch available ID Types
	$query = "SELECT * from id_types
				WHERE active='Y'
				AND show_on_sender_as_company='Y'";

	$IDTypes = selectMultiRecords($query);

	// If its an update sender request, find the sender's ID Type values.
	if (!empty( $_REQUEST["customerID"]))
	{
		// Now fetch values against available id types.
		$numberOfIDTypes = count( $IDTypes );
		for( $i = 0; $i < $numberOfIDTypes; $i++ )
		{
			$IDTypeValuesQ = "SELECT * from user_id_types
									WHERE
										id_type_id = '" . $IDTypes[$i]["id"] . "'
										AND	user_id = '" . $_REQUEST["customerID"] . "'
										AND user_type = 'C'";
			$IDTypeValues = selectFrom($IDTypeValuesQ);
			if (!empty( $IDTypeValues ))
				$IDTypes[$i]["values"] = $IDTypeValues;
		}
	}

	// End  Multiple ID Types
	//debug($_REQUEST);
	//debug($_REQUEST["saveInputs"]);
	if(!empty($_REQUEST["saveInputs"]))
	{
	//debug($_REQUEST);
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if(empty($_REQUEST["customerID"]))
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			 /**
			    To generate custoemr account
			 */
			$customerNumber = selectFrom ("
										  SELECT
										    	MAX(customerNumber) as customerNumber  
										  FROM
										        ".TBL_CUSTOMER." 
											");
			//debug($customerNumber);							
	        $customerNumber_value = $customerNumber[0]+1;
			$password = createCode();
									
			if($_REQUEST["payinBook"]=="" || CONFIG_PAYIN_CUSTOMER != '1')
				{
					$balance = 0;
				}
			else
				{
					$customerCode = $_REQUEST["payinBook"];
					$balance = - $payinAmount ;
				}
				
				echo '<input type="hidden" value="C-'.$customerNumber_value.'" id="comp_code" />';
	$arguments = array("flag"=>"addCompanyData","contactPerson" => $_POST["contactPerson"]);
	$insertUserArr = gateway("CONFIG_INSERT_COMP_CONTACT_PERSON",$arguments,CONFIG_INSERT_COMP_CONTACT_PERSON);
	$insertIntoKeys	= $insertUserArr["keys"];//",parentID";
	$insertIntoValues = $insertUserArr["values"];// ",'".$_POST["parentID"]."'";
	
	
		$arguments = array("flag"=>"addCustomerData","tradingAcccount" => $_POST["tradingAcccount"]);
	$insertUserArr = gateway("CONFIG_TRADING_ACCOUNT_CUSTOMER",$arguments,CONFIG_TRADING_ACCOUNT_CUSTOMER);
	$insertIntoKeys	.= $insertUserArr["keys"];//",parentID";
	$insertIntoValues .= $insertUserArr["values"];// ",'".$_POST["parentID"]."'";
		//debug($_REQUEST, true);
	
	$arguments = array("flag"=>"addCustomerData","parentID" => $_POST["customerParentID"]);
	 $insertUserArr = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
	$insertIntoKeys	.= $insertUserArr["keys"];//",parentID";
	$insertIntoValues .= $insertUserArr["values"];// ",'".$_POST["parentID"]."'";	
	
	$arguments = array("flag"=>"addCustomerData","source" => $_POST["source"]);
	//debug($arguments);
	$insertUserArr = gateway("CONFIG_SOURCE_FIELD_CUSTOMER",$arguments,CONFIG_SOURCE_FIELD_CUSTOMER);
	$insertIntoKeys	.= $insertUserArr["keys"];//",parentID";
	$insertIntoValues .= $insertUserArr["values"];// ",'".$_POST["parentID"]."'";

			$strInsertSql = "insert into ".TBL_CUSTOMER." 
											(
											  firstName,
											  Address,
											  Address1,
											  Country,
											  City,
											  agentID,
											  State,
											  email,
											  Zip,
											  Phone,
											  Mobile,
											  acceptedTerms,
											  senderRemarks,
											  customerNumber,
											  customerName,
											  password,
											  online_customer_services,
											  balance,
											  passport_number,
											  customerType,
											  company_msb_number,
											  created
											  ".$insertIntoKeys.",
												custCategory
											)
									 values
										   (
											 '".$_REQUEST["companyName"]."',
											 '".$_REQUEST["address"]."',
											 '".$_REQUEST["address1"]."',
											 '".$_REQUEST["country"]."',
											 '".$_REQUEST["city"]."',
											 '".$_REQUEST["agentID"]."',
											 '".$_REQUEST["state"]."',
								 			 '".$_REQUEST["email"]."',
											 '".$_REQUEST["zip"]."',

											 '".$_REQUEST["phone"]."',
											 '".$_REQUEST["mobile"]."',
								 			 '".$_REQUEST["acceptedTerms"]."',
											 '".$_REQUEST["senderRemarks"]."',
											 '".$customerNumber_value."',
											 '".$_REQUEST["companyName"]."',
											 '".$password."',
											 'TC',
											 '".$balance."',
											  '".$_POST["passportValidate"]."',
											 'company',
											  '".$_REQUEST["msbNumber"]."',
											 '".$created_date."'
											 ".$insertIntoValues.",
											 'W'
											)"; 
			//debug($strInsertSql,true);
			
			if(mysql_query($strInsertSql))
			{
				$customerID = mysql_insert_id();
				$bolDataSaved = true;
				/**** Start To Generate accountName Code *****/
			if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
				$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
		    else
				$strCustomerPrefix = "C-";
			$customerCnt = $customerID;
			if(defined("CONFIG_CUSTOM_SENDER_COUNTER_START") && CONFIG_CUSTOM_SENDER_COUNTER_START!="0" && is_numeric(CONFIG_CUSTOM_SENDER_COUNTER_START)){
				if($customerNumber_value < CONFIG_CUSTOM_SENDER_COUNTER_START){
					$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
					$custNumberUpdate = ",customerNumber = '".$customerCnt."' ";
				}
				else{
					$customerCnt = $customerNumber_value;
					$strCustCnt = "SELECT customerNumber FROM ".TBL_CUSTOMER." WHERE customerNumber = '".CONFIG_CUSTOM_SENDER_COUNTER_START."' ";
					$custCntDate = selectFrom($strCustCnt);
				}
				if($custCntDate["accountName"] == $strCustomerPrefix.$customerCnt)
					$customerCnt = CONFIG_CUSTOM_SENDER_COUNTER_START;
			}
			if($_POST["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != '1')
			{
				$customerCode = $strCustomerPrefix.$customerCnt;
				$numberQuery = " UPDATE 
									".TBL_CUSTOMER." 
								SET 
									accountName = '".$customerCode."'
								WHERE
								    customerID = '".$customerID."' ";
				//debug($numberQuery);						
		    	update($numberQuery);	
		       }
	
		 	if ($_POST["payinBook"] == "" && CONFIG_AUTO_PAYIN_NUMBER == '1') 
			{	
			   $customerCode = $strCustomerPrefix.$customerCnt;
			   $_REQUEST["payinBook"] = $customerCode;
				$payinQuery = "	UPDATE 
									".TBL_CUSTOMER." 
								SET 
									payinBook = '".$customerCode."' 
								WHERE 
									customerID = '".$customerID."' ";
		
		if(defined("CONFIG_USE_AGENT_AT_ADD_SENDER") && CONFIG_USE_AGENT_AT_ADD_SENDER == 1)
		{
			if(!empty($_REQUEST["ch_payinBook"]) && $_REQUEST["ch_payinBook"] == "Y")
			{
				update($payinQuery);
				//debug($payinQuery);		
			}
		}
		elseif(!defined("CONFIG_USE_AGENT_AT_ADD_SENDER") || CONFIG_USE_AGENT_AT_ADD_SENDER == 0)
		{
			update($payinQuery);
			//debug($payinQuery);	
		}
		
	}
		
	    $descript = "Customer data is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$customerID,TBL_CUSTOMER,$descript);
		
		$startMessage="Congratulations! You have been registered as a company of $company. <br>
		 Your registration information is as follows:";
			
		include("lib/html_to_doc.inc.php");
		$htmltodoc= new HTML_TO_DOC();
		include ("mailCustomerInfo.php");


$tmpfname = tempnam("/tmp", "WELCOME_".$_POST["firstName"]."_");
$fpOpen = fopen($tmpfname, 'w+');
if($fpOpen){
	//debug($fpOpen);
	fwrite($fpOpen, $data);
	fclose($fpOpen);
	//include("lib/html_to_doc.inc.php");
	//$htmltodoc= new HTML_TO_DOC();
	//$htmltodoc->createDoc($timeStamp.".html","test");
}
$To = $_POST["email"]; 
$fromName = SUPPORT_NAME;
$fromEmail = SUPPORT_EMAIL;

$subject = SYSTEM;//"Premier FX ";

$message="\n Dear ".$_POST["contactPerson"].",\n<br>";
//$message.="\n<br />Please find attached details of your trading account with Premier FX. To"."\n\r";
$passQuery=selectFrom(" select documentId from document_upload where userId='".$customerID."'");
$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
if($idTypeValuesData)
{
foreach( $idTypeValuesData as $idTypeValues )
		{
			
if($idTypeValues["id_number"]=="" && $idTypeValues["issued_by"]==""){
	//$message.=" a SIGNED copy of your passport and"."\n\r";
	$message .= "\n<br> Please find attached details of your trading account with $subject."."\n<br>" ;
$message.= "\n To complete your application we will require a scanned copy of your passport and a proof of address document. This is common practice for all financial institutions under money laundering regulations."."\n<br>";
$message.="\n<br> Please do not hesitate to contact me if you have any further questions.  I look forward to being of service to you in the near future."."\n<br>";
}
else {
$message.= "\n<br> Please find attached details of your trading account with $subject."."\n<br>" ;
$message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
}
}
}else {
$message.= "\n<br> Please find attached details of your trading account with $subject."."\n<br>" ;
$message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
}
$message.=" \n<br>Regards,"."\n<br />";

$whrClauseAdminTable = " and userID = '".$_REQUEST["agentID"]."' ";
if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
	$whrClauseAdminTable = " and userID = '".$_REQUEST["customerParentID"]."' ";
}
//$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom(" select name,email from admin where 1 $whrClauseAdminTable ");
$agentEmail = $agentUserName['email'];
$message .= "\n ".$agentUserName["name"]."\n";
//Premier Support\n
$message.="<table><tr><td align='left' width='30%'><img width='30%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg' /></td><td>&nbsp;</td></tr><tr><td width='20%'>&nbsp;</td><td align='left'><font size='2px'><font color='#F7A30B'>LONDON OFFICE</font>\n<br>11 th Floor City Tower 40 Basinghall St London EC2V 5DE \n<br><font color='#F7A30B'> PORTUGAL HEAD OFFICE</font> \n<br> Rua Sacadura Cabral Edificio Golfe 1A, 8135-144 Almancil \n<br> <font color='#F7A30B'>Tel:</font> UK +44 (0) 845 021 2370 | <font color='#F7A30B'>Int:</font> +351 289 358 511 \n<br> <font color='#F7A30B'>FAX:</font> +351 289 358 513 \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | http://www.premfx.com</font></td></tr></table>";

$strresume = $tmpfname;//"htmlEmaiWelcomeLetter.html";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

// Generate pdf document starts here
	if($_POST['sendServiceEmail']!="" && CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){
		include_once ('../pdfClass/class.ezpdf.php');
		$sfsData = attachPDFServiceEmail();
	}

	

// Prepare Email for Sender and Account Manger
$fromName  = SYSTEM;
$fromEmail = $fromEmail;
$welcomeLetter = "WelcomeLetter.html";
$stress_free_service_name = $sfsData['file_name'];
$stress_free_service_path = $sfsData['file_path'];
if(!empty($agentEmail)){
	$fromName  = $agentUserName["name"];
	$fromEmail = $agentEmail;	
}
$sendermailer = new PHPMailer();
$agentmailer = new PHPMailer();

$sendermailer->FromName = $agentmailer->FromName = $fromName;
$sendermailer->From = $agentmailer->From = $fromEmail; 
$sendermailer->AddAddress($To,'');
$agentmailer->Subject = $sendermailer->Subject = $subject;
$sendermailer->IsHTML(true);
$agentmailer->IsHTML(true);

// attachements
$sendermailer->AddAttachment($strresume, $welcomeLetter,"base64");
$agentmailer->AddAttachment($strresume, $welcomeLetter,"base64");

if(!empty($stress_free_service_name)){
	$sendermailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
	$agentmailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
}

// Body
$agentmailer->Body = $sendermailer->Body = $message;

	if(CONFIG_MAIL_GENERATOR == '1' && ($_POST["sendEmail"]=='Y' || $_POST["sendServiceEmail"]=='Y')){		
		$senderEmailFlag = $sendermailer->Send();
	
		if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
			if(!empty($_POST["customerParentID"])){
				$sqlAccMananger = "SELECT name,email FROM ".TBL_ADMIN_USERS." WHERE userID='".trim($_POST["customerParentID"])."' ";
				$accManangerData = selectFrom($sqlAccMananger);
				if(!empty($accManangerData['email'])){
					$agentmailer->AddAddress($accManangerData['email'],'');
					$agentEmailFlag = $agentmailer->Send();
				}
			}
		}		
	}	
		
	if($_POST["payinBook"] != "")
	{
		$strQuery = " insert into  
								agents_customer_account 
							 (
							    customerID,
								Date,
								tranRefNo,
								payment_mode,
								Type,
								amount,
								modifiedBy
							 ) 
					values
					         (
							   '".$customerID."',
							   '".$created_date."',
							   '',
							   'Customer Registered',
							   'WITHDRAW',
							   '".$payinAmount."',
							   '".$userID."'
							  )";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		//debug($strQuery);
	}
	$queryCustomerType=selectFrom(" select customerType from customer where customerID='".$customerID."'");
	$CustomerType=$queryCustomerType["customerType"];


		
		/*****End accountName Logic Code*/
	
	/************ Start Multiple ID Code *********/
	
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		
		if(isset($_REQUEST["arrMultipleIds"]))
			$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
		else	
			$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
	
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				// If id is empty, its an insert request.
				// Insert new reocrd in this case.

				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				$insertQuery = "insert into user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date 
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $customerID . "', 
											'C', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "', 
											'" . $expiryDate . "'
										)";
				insertInto( $insertQuery );
/*
 * @Ticket #4794
 */
				$lastIdInsertId = @mysql_insert_id();
				if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
					update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
				}
			}
		}
	}
	// End of code against #3299: MasterPayex - Multiple ID Types
		/********uplaod document code start******/
	$complete = false;	
	if($_REQUEST["uploadImage"] == "Y"){	
	
	  if($_REQUEST["DocID"] != "")
			$DocID = $_REQUEST["DocID"];

		for ($i=0; $i < count($_FILES); $i++)
		{ 
			if($_FILES["file".$i]['type'] != "")
			{
				if ( $_FILES["file".$i]['type'] == "image/gif"
					|| $_FILES["file".$i]['type'] == "image/jpg"
					|| $_FILES["file".$i]['type'] == "image/jpeg"
					|| $_FILES["file".$i]['type'] == "image/pjpeg"
					|| ( defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") && CONFIG_CUSTOM_UPLOAD_FILE_LIST == 1 && 
					$_FILES["file".$i]['type'] == "image/tiff" ) 
					// Added by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
					|| ( (!defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") || CONFIG_CUSTOM_UPLOAD_FILE_LIST == 0) && 
					$_FILES["file".$i]['type'] == "image/png" ) 
					// Condition modified by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
					|| ( (!defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") || CONFIG_CUSTOM_UPLOAD_FILE_LIST == 0) &&
					 $_FILES["file".$i]['type'] == "image/x-png" ) 
					  // Condition modified by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
					 )
				{
					
					$logo_name = $_FILES["file".$i]['tmp_name'];
					$newName = "../files/customer/";
					$newName =  $newName .  "00000" .$customerID."_".$DocID[$i]."_".basename($_FILES["file".$i]['name']) ; 
						if (is_uploaded_file($logo_name))
						{
							if(move_uploaded_file($_FILES["file".$i]['tmp_name'], $newName ))
							{
								$docQuery = "INSERT INTO 
													".TBL_DOCUMENT_UPLOAD." 
													(
														documentId,
														 userId,
														 path,
														 status
													) 
												 VALUES 
												 	(
													  '".$DocID[$i]."',
													  '".$customerID."',
													  '".$newName."', 
													  'Enable'
												    )";
								insertInto($docQuery);
							    $msg = "Logo uploaded successsfully.";
								$complete = true;
							 }
							 else
							 {
								$bolDataSaved = false;
		
							  }
						}
						else
						{
							$bolDataSaved = false;
						}
				}
				else
				{
					$bolDataSaved = false;
				}
			}
		}
		if($complete)
		   $bolDataSaved = true;		
	}
		
		/********uplaod document code end here*************/
		$bolDataSaved = true;
	//$transURL="add-transaction.php?customerID=".$customerID;
		if($ttTransFlag && $_REQUEST["callFrom"]=="TT" && $customerID)
		{
			
			//debug($customerID,true);
				echo '<script>
						window.opener.document.TT_form.cusdi.value ='.$customerID.';
						window.opener.LoadCustInfo();
				
					window.close();
					</script>';	
			
			exit;
			
			
		}
		else{
		insertError(CU10."The login name for the company is C-<b>$customerNumber_value</b>");
		//header( 'Location: add-transaction.php?customerID='.$customerID.'&pageID=popUp&msg=Y&success=Y') ;
		 $agentID=$_REQUEST["agentID"];
		echo '<script language="javascript"> 
		 		opener.document.location = "add-transaction.php?customerID='.$customerID.'&pageID=popUp&msg=Y&success=Y&agentID='.$agentID.'";
				window.close();
			</script>';
		}
	}	
		else
			$bolDataSaved = false;
	if($_REQUEST['uploadImage'] == "Y"){
     header( 'Location: uploadCustomerImage.php?customerID='.$customerID.'&pageID=popUp&callFrom='.$_REQUEST["callFrom"]);
	}
	
}
	else
	{
		/**
		 * Else the record is not new and required the updation
		 */	
		$arguments = array("flag"=>"updateCompanyData","contactPerson" => $_REQUEST["contactPerson"]);
	 	$updateCompArr = gateway("CONFIG_INSERT_COMP_CONTACT_PERSON",$arguments,CONFIG_INSERT_COMP_CONTACT_PERSON);
	 	$updatationKeyValues = $updateCompArr["keysValues"]; // ",parentID = '".$arguments["parentID"]."' ";			
		
		$arguments = array("flag"=>"updateCustomerData","parentID" => $_REQUEST["customerParentID"]);
	 	$updateCompUserArr = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
	 	$updatationKeyValues .= $updateCompUserArr["keysValues"];	
		
		$arguments = array("flag"=>"updateCustomerData","tradingAcccount" => $_POST["tradingAcccount"]);
		
	$updateCompUserArr = gateway("CONFIG_TRADING_ACCOUNT_CUSTOMER",$arguments,CONFIG_TRADING_ACCOUNT_CUSTOMER);
	$updatationKeyValues .= $updateCompUserArr["keysValues"]; // ",parentID = '".$arguments["parentID"]."' ";
	
		$arguments = array("flag"=>"updateCustomerData","source" => $_POST["source"]);
		//debug($arguments);
		$updateCompUserArr = gateway("CONFIG_SOURCE_FIELD_CUSTOMER",$arguments,CONFIG_SOURCE_FIELD_CUSTOMER);
		$updatationKeyValues .= $updateCompUserArr["keysValues"]; 
		
	$QuerOldData = "SELECT * FROM ".TBL_CUSTOMER." WHERE customerID ='".$_REQUEST["customerID"]."'";
	 $arrOldData = selectFrom($QuerOldData);
		
			$strUpdateSql = "update ".TBL_CUSTOMER." 
							 set
							 	firstName = '".$_REQUEST["companyName"]."',
								Address = '".$_REQUEST["address"]."',
								Address1 = '".$_REQUEST["address1"]."',
								agentID = '".$_REQUEST["agentID"]."',
								Country = '".$_REQUEST["country"]."',
								City = '".$_REQUEST["city"]."',
								State = '".$_REQUEST["state"]."',
								email = '".$_REQUEST["email"]."',
								Zip = '".$_REQUEST["zip"]."',
								Phone = '".$_REQUEST["phone"]."',
								Mobile = '".$_REQUEST["mobile"]."',
								acceptedTerms = '".$_REQUEST["acceptedTerms"]."',
								senderRemarks = '".$_REQUEST["senderRemarks"]."',
								customerName = '".$_REQUEST["companyName"]."',
								company_msb_number = '".$_REQUEST["msbNumber"]."',
								passport_number='".$_POST["passportValidate"]."',
								balance='".$balance."'
								".$updatationKeyValues."
							 where
							 	customerID = '".$_REQUEST["customerID"]."'
						"; 
			 
	if(update($strUpdateSql)){
	
	$customerID	= $_REQUEST["customerID"];
	
	$descript ="Customer data is updated";
	
	activities($_SESSION["loginHistoryID"],"UPDATION",$_REQUEST["customerID"],TBL_CUSTOMER,$descript);
 
	$id_activity_id = mysql_insert_id();
	
	logChangeSet($arrOldData,$modifyby,"customer","customerID",$_REQUEST["customerID"],$_SESSION["loginHistoryID"]);
	
	$modifyHistoryId = mysql_insert_id();
   
    $insertActivityId = "UPDATE ".TBL_AUDIT_MODIFY_HISTORY." SET id_activity_id = ".$id_activity_id." WHERE id = ".$modifyHistoryId."";

    update($insertActivityId);
	
	
	
	
	
	/************* Update Multiple ID Code ****/
		// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] )  || !empty($_REQUEST["arrMultipleIds"]))
	{
		if(isset($_REQUEST["arrMultipleIds"]))
			$idTypeValuesData = unserialize(stripslashes(stripslashes($_REQUEST["arrMultipleIds"])));
		else	
			$idTypeValuesData = $_REQUEST["IDTypesValuesData"];

		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				if ( !empty($idTypeValues["id"]) )
				{
					// If id is not empty, its an update request.
					// Update the existing record in this case.

					$updateQuery = "update user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
					update( $updateQuery );
/*
 * @Ticket #4794
 */
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$idTypeValues["id"]."'");
						
					}
				}
				else
				{
					// If id is empty, its an insert request.
					// Insert new reocrd in this case.

					

					$insertQuery = "insert into user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["customerID"] . "', 
												'C', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
					insertInto( $insertQuery );
/*
 * @Ticket #4794
*/
					$lastIdInsertId = @mysql_insert_id();
  
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='". mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
					}
				}
			}
		}
	}

	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] ); 

	/*********** End Update Multiple ID Code ***********/
	
	
		$complete = false;		
   		$bolDataSaved = true;
	}			
			else
				$bolDataSaved = false;
				
				/////
		
			include("lib/html_to_doc.inc.php");
		$htmltodoc= new HTML_TO_DOC();
		include ("mailCustomerInfo.php");

$tmpfname = tempnam("/tmp", "WELCOME_".$_POST["firstName"]."_");
$fpOpen = fopen($tmpfname, 'w+');
if($fpOpen){
	//debug($fpOpen);
	fwrite($fpOpen, $data);
	fclose($fpOpen);
	//include("lib/html_to_doc.inc.php");
	//$htmltodoc= new HTML_TO_DOC();
	//$htmltodoc->createDoc($timeStamp.".html","test");
}
// Generate pdf document starts here
	if($_POST['sendServiceEmail']!="" && CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){
		include_once ('../pdfClass/class.ezpdf.php');
		$sfsData = attachPDFServiceEmail();
	}
	
$To = $_POST["email"]; 
$fromName = SUPPORT_NAME;
$fromEmail = SUPPORT_EMAIL;

$subject = SYSTEM;//"Premier FX ";

$headers  = 'MIME-Version: 1.0' . "\r\n";
// To send HTML mail, the Content-type header must be set
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$num = md5( time() );


$message="\n Dear ".$_POST["contactPerson"].",\n<br>";

//$message.="\n<br />Please find attached details of your trading account with Premier FX. To"."\n\r";
$passQuery=selectFrom(" select documentId from document_upload where userId='".$customerID."'");
$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
if($idTypeValuesData){

foreach( $idTypeValuesData as $idTypeValues )
		{
			
if($idTypeValues["id_number"]=="" && $idTypeValues["issued_by"]==""){
	//$message.=" a SIGNED copy of your passport and"."\n\r";
	$message .= "\n<br> Please find attached details of your trading account with $subject.\n<br>" ;
$message.= "\n To complete your application we will require a scanned copy of your passport and a proof of address document. This is common practice for all financial institutions under money laundering regulations."."\n<br>";
$message.="\n<br> Please do not hesitate to contact me if you have any further questions.  I look forward to being of service to you in the near future."."\n<br>";
}
else {
$message.= "\n<br> Please find attached details of your trading account with $subject\n<br>" ;
$message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
	}
}
}else {
$message.= "\n<br> Please find attached details of your trading account with $subject\n<br>" ;
$message.="\n<br> Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
}
$message.=" \n<br>Regards,"."\n<br />";

$whrClauseAdminTable = " and userID = '".$_POST["agentID"]."' ";
if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
	$whrClauseAdminTable = " and userID = '".$_POST["customerParentID"]."' ";
}
//$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom(" select name,email from admin where 1 $whrClauseAdminTable ");
$agentEmail = $agentUserName['email'];
$message .= "\n ".$agentUserName["name"]."\n";
//Premier Support\n
$message.="<table><tr><td align='left' width='30%'><img width='30%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg' /></td><td>&nbsp;</td></tr><tr><td width='20%'>&nbsp;</td><td align='left'><font size='2px'><font color='#F7A30B'>LONDON OFFICE</font>\n<br>11 th Floor City Tower 40 Basinghall St London EC2V 5DE \n<br><font color='#F7A30B'> PORTUGAL HEAD OFFICE</font> \n<br> Rua Sacadura Cabral Edificio Golfe 1A, 8135-144 Almancil \n<br> <font color='#F7A30B'>Tel:</font> UK +44 (0) 845 021 2370 | <font color='#F7A30B'>Int:</font> +351 289 358 511 \n<br> <font color='#F7A30B'>FAX:</font> +351 289 358 513 \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | http://www.premfx.com</font></td></tr></table>";

$strresume = $tmpfname;//"htmlEmaiWelcomeLetter.html";

// Prepare Email for Sender and Account Manger
$welcomeLetter = "WelcomeLetter.html";
$stress_free_service_name = $sfsData['file_name'];
$stress_free_service_path = $sfsData['file_path'];
if(!empty($agentEmail)){
	$fromName  = $agentUserName["name"];
	$fromEmail = $agentEmail;	
}
$sendermailer = new PHPMailer();
$agentmailer = new PHPMailer();

$sendermailer->FromName = $agentmailer->FromName = $fromName;
$sendermailer->From = $agentmailer->From = $fromEmail; 
$sendermailer->AddAddress($To,'');
$agentmailer->Subject = $sendermailer->Subject = $subject;
$sendermailer->IsHTML(true);
$agentmailer->IsHTML(true);

// attachements
$sendermailer->AddAttachment($strresume, $welcomeLetter,"base64");
$agentmailer->AddAttachment($strresume, $welcomeLetter,"base64");

if(!empty($stress_free_service_name)){
	$sendermailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
	$agentmailer->AddAttachment($stress_free_service_path, $stress_free_service_name,"base64");
}

// Body
$agentmailer->Body = $sendermailer->Body = $message;

// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

	if(CONFIG_MAIL_GENERATOR == '1' && $_POST["sendEmail"]=='Y' ){	
	
		if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		
			if(!empty($_REQUEST["customerParentID"])){
				$sqlAccMananger = "SELECT name,email FROM ".TBL_ADMIN_USERS." WHERE userID='".trim($_REQUEST["customerParentID"])."' ";
				
				$accManangerData = selectFrom($sqlAccMananger);
				/*if(!empty($accManangerData['email']))
					mail($accManangerData['email'],$subject,$message,$headers);	*/
				if(!empty($accManangerData['email'])){
					$agentmailer->AddAddress($accManangerData['email'],'');
					$agentEmailFlag = $agentmailer->Send();
				}
			}
		}		
	}
			/////////////////////////////////////////////

		if($_POST["payinBook"] != "")
	{
		$strQuery = " insert into  
								agents_customer_account 
							 (
							    customerID,
								Date,
								tranRefNo,
								payment_mode,
								Type,
								amount,
								modifiedBy
							 ) 
					values
					         (
							   '".$customerID."',
							   '".$created_date."',
							   '',
							   'Customer Registered',
							   'WITHDRAW',
							   '".$payinAmount."',
							   '".$userID."'
							  )";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		//debug($strQuery);
	}
	$queryCustomerType=selectFrom(" select customerType from customer where customerID='".$customerID."'");
	$CustomerType=$queryCustomerType["customerType"];

	if(CONFIG_MAIL_GENERATOR == '1' && !empty($_REQUEST['sendEmail']))
	{
		//sendMail($To, $subject, $data, $fromName, $fromEmail);
		/*@mail($To,$subject,$message,$headers); */
		$senderEmailFlag = $sendermailer->Send();
	}		
				
				
				
				
				
				
				
				
				
				/////
				
	if($_REQUEST['uploadImage'] == "Y"){
     $upUrl = "uploadCustomerImage.php?mode=update&customerID=".$customerID."&callFrom=".$_REQUEST["callFrom"];
	     // header( 'Location: uploadCustomerImage.php?customerID='.$customerID.'&pageID=popUp');

	 redirect($upUrl);
	}
	elseif($ttTransFlag && $_REQUEST["callFrom"]=="TT" && $customerID)
	{
		
		//debug($customerID,true);
			echo '<script>
					window.opener.document.TT_form.cusdi.value ='.$customerID.';
					window.opener.LoadCustInfo();
			
				window.close();
				</script>';	
		
		exit;
		
		
	}
	else{
	insertError("Data Updated Successfully");
	 //$upUrl = "search-edit-cust.php?msg=Y&custName=".$_REQUEST["companyName"]."&success=Y";
	 if($returnPage == "add-transaction.php")
	 {
	 	echo '<script language="javascript"> 
		 		opener.document.location = "add-transaction.php?customerID='.$customerID.'&pageID=popUp&msg=Y&success=Y";
				window.close();
			 </script>';	
	 }elseif($returnPage == "viewAlertControllerNew.php"){
	 	$upUrl = "viewAlertControllerNew.php?&newOffset=".$_POST["newOffsetF"]."&userName=".$_POST["userNameF"]."&userType=".$_POST["userTypeF"]."&IDType=".$_POST["IDTypeF"]."&alertType=".$_POST["alertTypeF"]."&Submit=Search"; 
		redirect($upUrl);
	 }else{
		$upUrl = "search-edit-cust.php?msg=Y&success=Y";
	 	redirect($upUrl);
	 }
	
	}	
	
			
}
	if($bolDataSaved){
			//echo "*".$customerID."*";
			}else{
			echo 0;
		}
	//exit();	
 }
if(CONFIG_SHOW_DOCUMENTS_SENDER_FORM=="1")
{
	 if($_REQUEST["customerID"]!="")
	 {
			$documentQueryy = "select du.documentId,du.id,du.path,d.description from ".TBL_DOCUMENT_UPLOAD." as du,".TBL_DOCUMENT_NAMES." as d where du.documentId = d.id and du.userId ='".$_REQUEST["customerID"]."' "; 
			$documents = selectMultiRecords($documentQueryy);
			//debug($documentQueryy);
			 $doc_flag=1;
	 }
	 else
	 {
	 $doc_flag=0;
	 }
	 
}
	
	if($_REQUEST["category"] != "" && $_REQUEST["callFrom"] =="ajax")
	{ 
	
		$documentQuery = "select description,id from ".TBL_DOCUMENT_NAMES." where id IN (select DISTINCT(documentId) 
		from ".TBL_DOCUMENT_CATEGORY." where categoryId = '".$_REQUEST["category"]."') "; 
		$document = selectMultiRecords($documentQuery);
		$strData ="<table boder='1'><tr>";
		if(count($document) == '0')
		{ 
			$strData .=  "<td align='left' colspan='4' ><b>No Document(s) in this Category</b></td></tr>";
		 }
		 else
		 { 
	         $strData .= "<br /><td align='left' colspan='4'><b>Upload Document</b><br />";
			if (defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") && CONFIG_CUSTOM_UPLOAD_FILE_LIST == 1)
			{
				$strData .=	"<br />Gif, JPEG or Tiff files only<br /></td></tr>";	
			}
					for ($i=0; $i < count($document); $i++)
					{ 
					   	$strData .= "<tr><td align='left'>".$document[$i]["description"]."</td>";
						$strData .="<td><input onchange='selected_doc(this)' name='file".$i."' type='file' id='file".$i."'>";
						//$strData .="<input type='hidden' name='DocID[".$i."]' id='DocID' value='".$document[$i]['id']."'>";
						$strData .="</td></tr>";	
				   } 
					   $dCount = count($document);
					   $strData .="<input type='hidden' name='docCount' value='".$dCount."'>";
					   $strData .="</table>";
				 }

 	   echo $strData;
	   exit;	
 }	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Company Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
<script language="javascript" src="./javascript/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('documents_div', 'fade=1,speed=400,group=document_divs,hide=1')
animatedcollapse.ontoggle=function($, divobj, state){ //fires each time a DIV is expanded/contracted
	//$: Access to jQuery
	//divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
	//state: "block" or "none", depending on state
}

animatedcollapse.init()
</script>
<script language="javascript">
function checkForm(submitCompany) {


if(submitCompany.companyName.value=="")
{
	alert("Please Enter Company Name");
	submitCompany.companyName.focus();
	return false;	

}
 <? if (CONFIG_PASSPORT_VALIDATION_MANDATORY == "1"){ ?>
	if(theForm.passportValidate.value == "" || IsAllSpaces(theForm.passportValidate.value))
	{
		alert("Please provide the Passport Number.");
        theForm.passportValidate.focus();
        return false;
	}	
    
  <? } 
  if (CONFIG_TRANS_TYPE_TT != "1"){
  
  ?> 
//alert("Select Account Manager");
if(submitCompany.customerParentID.options.selectedIndex == 0)
{
	alert("Please select the Account Manager from the list.");
	submitCompany.customerParentID.focus();
	return false;	
}
	

<?php  }
if(defined("CONFIG_CONTACT_PERSON_MANDATORY") && CONFIG_CONTACT_PERSON_MANDATORY=="1") { ?>
if(submitCompany.contactPerson.value=="")
{
	alert("Please Enter Contact Person Name");
	submitCompany.contactPerson.focus();
	return false;	

}

<?php }
?>

if(submitCompany.country.options.selectedIndex == 0)
{
	alert("Please Select Country from the list.");
	submitCompany.country.focus();
	return false;	
}
if(submitCompany.city.value=="")
{
	alert("Please Enter City Name");
	submitCompany.city.focus();
	return false;	
}
if(submitCompany.ch_payinBook.checked!=1)
{
	alert("Please Check Customer Account");
	submitCompany.ch_payinBook.focus();
	return false;	

}



if($("#passportValidate").val() !='')
	{	
		var returnFlag = validatePasspotNumber();
		//console.log(returnFlag);
		//alert(returnFlag);
		if(returnFlag != 1)
		{
			theForm.passportValidate.focus();
			return false;
		}
		else
		{
			return true;
		}
		
		
	}	
//alert(if(submitCompany.ch_payinBook.value!="");	

//alert(chk);
//return false;

//if(chk=="-- Select Account Manager --")
//alert("Select Account Manager");
//return false; 
}
//if(submitCompany.customerParentID.options.selectedIndex == 0)
//	{
//			alert("Please select the Account Manager from the list.");
//			  theForm.customerParentID.focus();
//			 return false;	
//	}

	//$(document).ready(function() {
		//$("#submitCompany").validate({//
//			rules: {
//				companyName: "required",
//				country: "required",
//				city:  "required",
//				ch_payinBook: "required",
//			<? //if(defined("CONFIG_COMPULSORY_MOBILE") && CONFIG_COMPULSORY_MOBILE=="1") { ?>	
//				mobile: "required"
//			<?// } ?>					
//			},
//			messages: {
//				companyName: "<br />Please enter full name of company",
//				country: "<br />Please select the country from dropdown",
//				city: "<br />Please enter the city.",
//				ch_payinBook: "<br />Please check customer account number.",
//			<? //if(defined("CONFIG_COMPULSORY_MOBILE") && CONFIG_COMPULSORY_MOBILE=="1") { ?>
//				mobile: "<br />Please enter the mobile."
//			<? //} ?>	
//			},
//			submitHandler: function(form) {/*
//				
//				if (checkIdTypes($("#submitCompany")) == false )
//					return false;
//
//					$("#submitCompany").ajaxSubmit(function(data) { 
//						//alert(data);
//						document.getElementById('temp').innerHTML = data;
//						var code =document.getElementById('comp_code').value;
//						//return false;
//						var msg = "'"+$("#companyName").val()+"' saved successfully! and Login Name is "+code+" \n Do you want to add more companies information?";
//						if(data != 0){
//							var from = '<?//=$_REQUEST["from"]?>';
//							//alert(from);
//							var customerID = '<?//=$_REQUEST["customerID"]?>';
//							var agentID = '<?//=$_REQUEST["agentID"]?>';
//							var transID = '<?//=$_REQUEST["transID"]?>';
//							var returnPage = '<?//=$returnPage?>';
//							var	backURL = returnPage+"?customerID="+data+"&agentID="+agentID+"&transID="+transID;				
//							if(from == 'popUp' )
//							{ 
//								//alert(backURL);
//								//$("#submitCompany").clearForm();
//								clearFormCustomize();
//								opener.document.location = backURL;
//								window.close();
//							} else if(from == 'search-edit-cust'){
//								alert("customer inforamtion updated successfully");
//								//window.history.go(-1);
//							}
//					
//							else
//							{
//							clearFormCustomize();
//							chkDocumnet();
//							if(!confirm(msg)){
//								window.close();
//							}
//							/*else
//							{
//							//alert(data);
//							alert("The record could not be saved due to error, Please try again."); 
//							}*/
//						}
//					}else{
//						alert("The record could not be saved due to error, Please try again."); 
//					}	
//				});
//		   */}
//		}); 

			
		//$("#closeWindow").click(function(){
//			close();
//		});
	//});
	
	function chkDocumnet()
	{
		if($("#uploadImage").attr('checked'))
		{
		  $("#selectDoc").show();
		}
		else
		{
		 $("#selectDoc").hide();
		}
	}
	
	function selectCategory()
	{
		//alert($(this).val());
		if($("#category").val() != "")
			$("#setCategory").load("add-company.php?callFrom=ajax&category="+$("#category").val());
		else
			$("#setCategory").html("<td align='left' colspan='4' ><b>first select category from dropdown</b></td></tr>");

	}	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;

			}
		}
	}
	var cnt =0;
   function selected_doc(file)
   {
   	var newIn= document.createElement('input');
	newIn = file;
	newIn.name = "file"+cnt;
	alert(newIn.name+newIn.value+newIn.accept+newIn.type+newIn.form);
	file.parentNode.insertBefore( newIn, file );
	document.getElementById('doc_sel').innerHTML =document.getElementById('doc_sel').innerHTML+ newIn.value+newIn.accept+"<br />"+"<input type='file' name='nfile"+cnt+"' id='nfile"+cnt+"' style='display:none;' />";
	temp = document.getElementById('nfile'+cnt);
	alert(temp.name+temp.value+temp.accept+temp.type+temp.form);
	temp.value = newIn.value;
	temp.accept = newIn.accept;
	document.getElementById('DocID').value = cnt;
	cnt++;
   }
 // function will clear input elements on each form  
    function clearFormCustomize(){  
    	  // declare element type  
      var type = null;  
      // loop through forms on HTML page  
      for (var x=0; x<document.forms.length; x++){  
        // loop through each element on form  
        for (var y=0; y<document.forms[x].elements.length; y++){  
          // define element type  
         type = document.forms[x].elements[y].type  
         // alert before erasing form element  
         //alert('form='+x+' element='+y+' type='+type);  
         // switch on element type  
         switch(type){  
           case "text":  
           case "textarea":  
           case "password":  
           //case "hidden":  
             document.forms[x].elements[y].value = "";  
             break;  
           case "radio":  
           case "checkbox":  
             document.forms[x].elements[y].checked = "";  
             break;  
           case "select-one":  
             document.forms[x].elements[y].options[0].selected = true;  
             break;  
           case "select-multiple":  
             for (z=0; z<document.forms[x].elements[y].options.length; z++){  
               document.forms[x].elements[y].options[z].selected = false;  
             }  
           break; 
         }  
       }  
     }  
   } 
   
function validatePassportNumber()
			{
				var submitCompany = document.getElementById("submitCompany");
				if ( submitCompany.passportValidate != null && submitCompany.passportValidate.value != ""
					&& !( /^\d+$/.test(submitCompany.passportValidate.value) ) ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please only provide numerals as Passport Number");
					submitCompany.passportValidate.focus();
					return false;
				}
			
				
				return true;
			}

			function passportEventHandler( e )
			{
				keynum = -1;
				if(window.event) // IE
				{
					keynum = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					keynum = e.which;
				}

				if ( 
						keynum > -1
					&& 	keynum != 8 	// Backspace
					&& 	keynum != 13	// Enter
					&& 	keynum != 27	// Escape
					 )
				{
					validatePassportNumber();
				}
			}

function validatePasspotNumber(){
	var bolToReturn;

	//$("#passportData").load("passport_validation_uk.php",
	var dataToPass = {
		passportValidate: $("#passportValidate").val(),
		Submit: 'Validate', 
	
	};
	
	$.ajax({
		url: "passport_validation_uk.php",
		async: false,
		data: dataToPass,
		success: function(data){
			//console.log(data); 
			if(data == '1')
			{
				$("#passportValidStatus").html("<strong><font color='blue' size='2'>Passport Number is Valid</font></strong>");
				bolToReturn = data;
			}
			else 
			{
				$("#passportValidate").val('');
				$("#passportValidStatus").html("<strong><font color='red' size='2'>Passport Number was Invalid</font></strong>");
				bolToReturn = false;
			}
		}
	});

	return bolToReturn;
}

function Associated_Introducers(){
//alert("aa");
	var bolToReturn;

	//$("#passportData").load("passport_validation_uk.php",
	var dataToPass = {
		AccountMangr: $("#customerParentID").val(),
		agentID: '<?=$agentID?>',
		customerID: '<?=$_REQUEST["customerID"]?>',
		Submit: 'Validate', 
	
	};
	
	$.ajax({
		url: "AsocAgents.php",
		async: false,
		data: dataToPass,
		success: function(data){
		//console.log(data); 
		$("#agentID").html(data);
				
			
		}
	});

	// bolToReturn;
}
</script>
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.remarks
{
	font-size:12px;
	text-align:center;
	font-weight:bold;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
</style>
</head>
<body onLoad="Associated_Introducers();">  

<form name="submitCompany" id="submitCompany" action="" method="post" onSubmit="return checkForm(this);">
<input type='hidden' name='DocID[]' id='DocID' value='' />
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if(!empty($msg)) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:10px;">
					<img src="images/info.gif" />&nbsp;<?=$msg?>				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		<?
	if($agentType == "Admin" || $agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager" || strstr($userDetails["rights"], "Create Transaction"))
		{	
			$linkedAgent = array();
			//debug($linkedAgent);
			if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && ($agentType != 'admin' && $agentType != 'Branch Manager' /*&& $agentType != 'Admin Manager'*/))
			{
				 
				$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$parentID."'");
				//debug($adminLinkDetails);
				$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
				//$query .= " and username in '".$linkedAgent."'";
				//debug($linkedAgent);				
			}
		?>
		<?php
			$arguments = array("flag"=>"populateUsers","selectedID"=>$accManagerID,"userType"=>"populateAdminStaff","hideSelectMain"=>true);
			//debug($arguments);
			$populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);			
			if(!empty($populateUser)){
		?>
			<tr bgcolor="#ededed">
				<td width="100" class="tdDefination"><b>Select <?=__("Account Manager")?></b><font color="#ff0000">*</font>&nbsp;</td>
				<td width="250" colspan="3">
				
				<?php
				 
						?>
						<select name="customerParentID" id="customerParentID" style="font-family:verdana; font-size: 11px" onChange="Associated_Introducers();">
				<option value="">-- Select Account Manager --</option>
				 <?php echo $populateUser;
				 ?>
				  </select>
			  </td><?php //debug($populateUser);?>    
			</tr>
			<? } ?>
<?php			$arguments = array("flag"=>"sourceFieldDisplay","selectedSource"=>$source);
				//debug($arguments);
				$sourceField = gateway("CONFIG_SOURCE_FIELD_CUSTOMER",$arguments,CONFIG_SOURCE_FIELD_CUSTOMER);		
					if(count($sourceField)>0){
						$sourceFieldLabel = $sourceField["fieldLabel"];
						$sourceFieldStr = $sourceField["fieldStr"];
				}	
				//debug($linkedAgent);
			if(CONFIG_COMPANY_CUSTOMIZE!="1")
			{	
				?>
			
		<tr bgcolor="#ededed">
            <td width="100" class="tdDefination"><b>Select <?=__("Agent")?></b></td>
            <td width="250">
			
			
			
			<select name="agentID" id="agentID" style="font-family:verdana; font-size: 11px">
            	<? if(CONFIG_LINK_SENDING_AGENT_COUNTRY == "1" || CONFIG_USE_AGENT_AT_ADD_SENDER == 1){ ?>
            	<option value="">-Select One-</option>
            	<?php } 
				     
						
						             
					$queryCust = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
					
						$queryCust .=" order by agentCompany";					
					$agents = selectMultiRecords($queryCust);					
					for ($i=0; $i < count($agents); $i++){
						$flag = False;	
										
						if(in_array($agents[$i]["username"], $linkedAgent) && CONFIG_ADMIN_ASSOCIATE_AGENT == '1')		
						{							
							$flag = True;
							
						}
						//debug($agentType);
						if((CONFIG_ADMIN_ASSOCIATE_AGENT != '1') || (strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false)){							
							$flag = True;
							}

						if($flag)
						{						
							?>
			                <option value="<?=$agents[$i]["userID"]; ?>" <? echo ($agentID==$agents[$i]["userID"] ? "selected" : "")?>><?  echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
							
			                <?
			           }
					   
					}
				
					
				?>
              </select>
			  <!--<td width="20%" align="right"><font color="#005b90"><b><?=$sourceFieldLabel?>&nbsp;</font></b></td>
                  <td width="37%"><?=$sourceFieldStr?>&nbsp;</td> -->
              <script language="JavaScript">
         				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID"]; ?>");
              </script>
          </td>
              <td width="100" class="tdDefination"><?php /*if($_SESSION["customerID"]!="" && CONFIG_ADD_REF_NUMBER == "1"){
			  	if(CONFIG_UPDATE_CUSTOMER_NUMBER=="1"){
				//debug($_SESSION["accountName"]);
			  		$customerLabel = CU22." Number";
					$customerNumber = '<input type="text" id="accountName" name="accountName" value="'.$_SESSION["accountName"].'" style="border:1px solid;">';
				}
				else{
					$customerLabel ="Reference No.";
					$customerNumber = $_SESSION["accountName"];
				}
			  ?>
			  <font color="#005b90"><strong><?php echo $customerLabel;?></strong></font><?php }
			  else { ?><font color="#005b90"><b><?=$sourceFieldLabel?>&nbsp;</font></b><? } ?></td>
               <td><?php if($_SESSION["customerID"]!="" && CONFIG_ADD_REF_NUMBER == "1"){ 
				   		echo $customerNumber;
			   }
			   else
			   {
			   echo $sourceFieldStr; } */?>&nbsp;
			   <?=$sourceFieldLabel?>&nbsp; </td>
			   <td><?=$sourceFieldStr?>&nbsp;</td>
			 
        </tr>
		<?
		
			}
		} ?>
		<tr>
		<td> <div id="AsocAgnts"> </div></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Company Name:&nbsp;</b><font color="red">*</font></td>
		  <td align="left" colspan="3"><input type="text" name="companyName" id="companyName" size="75" maxlength="150" value="<?=$companyName?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Address Line 1:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="address" id="address" size="75" maxlength="150" value="<?=$address?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Address Line 2:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="address1" id="address1" size="75" maxlength="150" value="<?=$address1?>" /></td>
		</tr>
		
		<tr bgcolor="#ededed">
				
			<td width="25%" class="tdDefination"><b>Country:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<select name="country" id="country">
					<option value="">Select Country</option>
					<? 
						for($i = 0; $i < count($arrCountries); $i++) 
						{ 
							$strSelected = '';
							if($arrCountries[$i]["countryname"] == $country)
								$strSelected = ' selected="selected"';
					?>
							<option value="<?=$arrCountries[$i]["countryname"]?>" <?=$strSelected?>><?=$arrCountries[$i]["countryname"]?></option>
					<? } ?>
				</select>			</td>
			
			<td width="25%" class="tdDefination"><b>City:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="25%">
				<input type="text" name="city" id="city" value="<?=$city?>" />			</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination"><b>County:</b></td>
		  <td align="left"><input type="text" name="state" id="state" value="<?=$state?>" /></td>
		  <td class="tdDefination"><b>E-Mail:</b></td>
		  <td align="left"><input type="text" name="email" id="email" size="40" maxlength="150" value="<?=$email?>" /></td>
	  </tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Zip Code:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="zip" id="zip" size="20" maxlength="10" value="<?=$zip?>" /></td>
			<?php if(CONFIG_HIDE_MSB_CUST=="0"){ ?>
			<td class="tdDefination"><b>MSB Number &nbsp;</b></td>			
			<td align="left"><input type="text" name="msbNumber" id="msbNumber" value="<?=$msbNumber?>" /></td>
			<? } 
				else { ?>
				<td class="tdDefination">&nbsp;</td>			
			<td align="left">&nbsp;</td>				
			<? } ?>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Phone:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="phone" id="phone" size="40" maxlength="150" value="<?=$phone?>" /></td>
			<td width="25%" class="tdDefination"><b>Mobile:&nbsp;<font color="red"><? if(defined("CONFIG_COMPULSORY_MOBILE") && CONFIG_COMPULSORY_MOBILE=="1") { ?> * <? } ?></font></b></td>
			<td align="left" width="25%"><input type="text" name="mobile" id="mobile" size="40" maxlength="150" value="<?=$mobile?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Contact Person Name:&nbsp;</b>
			<?php 
			if(defined("CONFIG_CONTACT_PERSON_MANDATORY") && CONFIG_CONTACT_PERSON_MANDATORY=="1") { ?>
			<font color="red">*</font>
			<?php } ?>
			</td>
			<td align="left" width="25%"><input type="text" name="contactPerson" id="contactPerson" size="40" maxlength="150" value="<?=$contactPerson?>" /></td>
			<td width="25%" class="tdDefination">&nbsp;</td>
			<td align="left" width="25%">&nbsp;</td>
		</tr>
	<?php 
					  ////////passport validation  	
				
				if(CONFIG_PASSPORT_VALIDATION == "1"){ ?> 	
           	<tr  bgcolor="#ededed" id="senderRowID_7" style=" <?=$displayRow; ?>">
				<td width="20%" class="tdDefination"><b>
				<?=__("Passport Number"); ?>
				<? if(CONFIG_PASSPORT_VALIDATION_MANDATORY == "1") {?>
				<font color="#ff0000">*</font>
				<? } 
				
				
				$jsCode = "";
				
						$jsCode = " onkeyup='passportEventHandler(event);' ";
					?>
				</b></td>
            	  <td><input name="passportValidate" <?=$jsCode;?>type="text" value="<?=$passportValidate; ?>" size="20" maxlength="10" id="passportValidate" onBlur="validatePasspotNumber();">&nbsp;
				  <a href="#" onClick="javascript:window.open('passport-validation-readme.htm', 'Passport Validation', 'toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=300,width=350,top=200,left=170')"><img src="images/info.gif" id="passposrtImg" border="0"></a>
				
			 <!--img src="images/info.gif" id="passposrtImg" title="Passport Valiidation | <img src='images/passport_sample_uk.JPG'/> " />-->
	
			 </td>
			<td colspan="2"> <div id="passportValidStatus"></div></td>
			
				
				  <!--<td width="20%" align="right"><input type="button" value="Validate" name="Submit" id="Validate" >
				 
				  </td> -->
           <tr bgcolor="#ededed" style="display:none">
			<td colspan="4" id="passportData" >&nbsp;</td>
		</tr>
			<? } 
			
			////////////////
			
			
			?>
				
	
		
		<?
		 	/*************** @5278- Multiple ID Types Code Start From Here***********/ 
			 if(CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1)
			 {
				if ( !empty( $IDTypes ) )
				{
		?>
		<tr bgcolor="#ededed">
		  <td colspan="4" class="tdDefination">
		  <div id="temp" style="display:none;"></div>
		  	<div align="center">
		  		<table width="100%">
					<tr>
						 <?
							$tdNumber = 1;
							$numberOfIDTypes = count($IDTypes);
							$chkIDTypesJS = "";
							$serverDateArray["year"]  = date("Y");
							$serverDateArray["month"] = date("m");
							$serverDateArray["day"]   = date("d");
	
							for( $i = 0; $i < $numberOfIDTypes; $i++ )
							{
								$idNumber = "";
								$idTypeValuesSESS = "";
								$issuedBy = "";
	
								if( !empty($_SESSION["IDTypesValuesData"])
									&& !empty($_SESSION["IDTypesValuesData"][$IDTypes[$i]["id"]]))
								{
								   
									$idTypeValuesSESS = $_SESSION["IDTypesValuesData"];
									if(isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"]))
									{
										$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
										$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
									}
								}
								else if (!empty($IDTypes[$i]["values"]))
								{
									$idNumber = $IDTypes[$i]["values"]["id_number"];
									$issuedBy = $IDTypes[$i]["values"]["issued_by"];
								}

							?>
							<td style="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:1px solid black;" : "");?> ">
								<table border="1" cellpadding="2" cellspacing="0" align="center">
									  <tr>
										 <td colspan="2" style="border:2px solid black; font-weight:bold;">
											<?=$IDTypes[$i]["title"];?> <?=($IDTypes[$i]["mandatory"] == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?>
										  </td>
									   </tr>
									   <tr>
										 <td width="80">ID Number:</td>
										  <td>
											   <input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id]" 
											   value="<?=( !empty($IDTypes[$i]["values"]) ? $IDTypes[$i]["values"]["id"] : "" );?>" />
											   <input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_type_id]" 
											    value="<?=$IDTypes[$i]["id"];?>" />
												<input type="text" id="id_number_<?=$IDTypes[$i]["id"];?>" 
												 name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_number]" value="<?=$idNumber;?>" size="40" />
											</td>
										 </tr>
										 <tr>
											<td>Issued by:</td>
											 <td>
											  		<input type="text" id="id_issued_by_<?=$IDTypes[$i]["id"];?>" 
											  		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issued_by]" value="<?=$issuedBy;?>" size="40" />
											 </td>
										 </tr>
										 <tr>
										    <td>Issue Date:</td>
											<td>
												<?
													$startYear = 1985;
													$currentYear = $serverDateArray["year"];

													$issueDay = 0;
													$issueMonth = 0;
													$issueYear = 0;

													if ( !empty($idTypeValuesSESS)
														&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) 
														&& isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]))
													{
														$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
														$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
														$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;
													}
													else if ( !empty($IDTypes[$i]["values"]) )
													{
														$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
														$issueDay = $dateArray[2] - 1;
														$issueMonth = $dateArray[1] - 1;
														$issueYear = $dateArray[0] - $startYear;
													}
												?>
													<select id="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
													</select>
													
													<select id="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
													</select>
													
													<select id="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
															name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
													</select>

												<script type="text/javascript">
													   initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>',
													   'issue_date_year_<?=$IDTypes[$i]["id"];?>', '', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', 
														<?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
												</script>
												</td>
											  </tr>
											  <tr>
												<td>Expiry Date:</td>
												<td>
													<?
														$expiryStartYear = $currentYear;
														$expiryDay = $serverDateArray["day"] - 1; 
														// Didn't do "less 1" here because atleast one day further from current has to be selected.
														$expiryMonth = $serverDateArray["month"] - 1;
														$expiryYear = $serverDateArray["year"] - $expiryStartYear;
	
														if ( !empty($idTypeValuesSESS)
															 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) 
															 && isset($idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]))
														{
															$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
															$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
															$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;
														}
														else if ( !empty($IDTypes[$i]["values"]) )
														{
															$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );
	
															$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );
	
															$expiryDay = $dateArray[2] - 1;
															$expiryMonth = $dateArray[1] - 1;
															$expiryYear = $dateArray[0] - $expiryStartYear;
														}
													?>
														<select id="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
														</select>

														<select id="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
														</select>

														<select id="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
																name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
														</select>

													<script type="text/javascript">
														initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>',
														'expiry_date_year_<?=$IDTypes[$i]["id"];?>', '', '1:12', '<?=$expiryStartYear;?>:2035',
														 <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
													</script>
													</td>
												 </tr>
											</table>
										</td>
										   <?
											if ($tdNumber == 2 )
											{
												// Starting a new TR when two TDs are output.
												?>
										  		</tr><tr>
												<?
												$tdNumber = 1;
											}
											else
												$tdNumber++;

											$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
											id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

											if ( $IDTypes[$i]["mandatory"] == 'Y' )
											{
												$chkIDTypesJS .= "
															if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																id_number_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
															if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
															{
																alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																return false;
															}
														  ";
											}
											else
											{
												$chkIDTypesJS .= "
														if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
														{
															alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
															id_number_" . $IDTypes[$i]["id"] . ".focus();
															return false;
														}
														if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
														{
															alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
															id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
															return false;
														}
													";
													}

													$chkIDTypesJS .= "
															issueDate = new Date();
															todayDate = new Date( issueDate );
															expiryDate = new Date();
															todayDate.setFullYear(" . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " .
															 $serverDateArray["day"] . " );
															issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value,
														    document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, 
															document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
															expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, 
															document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, 
															document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );
														if ( issueDate >= todayDate )
														{
															alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
															document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
														if ( expiryDate < todayDate )
														{
															alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
															document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
															return false;
														}
													   ";
											}
										 ?>
										</tr>
									</table>
												<script type="text/javascript">
													function checkIdTypes(submitCompany )
													{
														<?
															if ( !empty($chkIDTypesJS) )
																echo $chkIDTypesJS;
														?>
														return true;
													}
												</script>
			</div>
		  </td>
	  </tr>
	      <?
				}
			} /*****End Code of - Multiple ID Types @5278 *****/
			
			if($ttTransFlag && $_REQUEST["callFrom"]=="TT")
				$colspan="colspan='3'";
	 ?>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Upload Document :&nbsp;&nbsp;</b></td>
			<td align="left" width="25%" <?php echo $colspan?>><input type="checkbox" name="uploadImage" id="uploadImage" value="Y" <?=($_REQUEST["uploadImage"]=="Y")?'checked="checked"':''?>/> 
		            <?php if(CONFIG_SHOW_DOCUMENTS_SENDER_FORM=="1"){?>
            <a href="#" rel="toggle[documents_div]" data-openimage="images/collapse.jpg" data-closedimage="images/expand.jpg"><img src="images/collapse.jpg" border="0" /></a><br/>
            <table id="documents_div" width="40%" border="1" cellspacing="1" cellpadding="2" align="center" style="display:none; margin-right:300px">

          <? 
		  
	
	if(count($documents)>0 &&  $doc_flag!=0){

	for ($i=0; $i < count($documents); $i++){ 
    
                  ?>
          <tr bgcolor="#ededed" style="font-size:9px">
            <td width="25%" height="10"><font color="#005b90"><b><? echo $documents[$i]["description"]; ?></b></font></td>
            <?php if($documents[$i]["path"] != ""){ ?>
            <td width="12%"><a href="<?php echo $documents[$i]["path"]; ?>" target="_blank" class="style2" >View</a></td>
            <td width="12%" ><a href="mailto: ?subject=my doc&body=see attachment&attachment=""C:\Documents and Settings\Administrator\My Documents\My Pictures\error.bmp" class="style2">mail to</a> </td>
            <td width="12%" ><a href="delete-document.php?docID=<?=$documents[$i]["id"]?>&custID=<?=$_GET["customerID"]?>&backUrl=<?php 
  echo base64_encode("http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);?>" class="style2" onClick="return confirm('Are you sure you want to delete !')">Delete</a> </td>
            <?php }elseif($documents[$i]["path"] == ""){ ?>
            <td width="12%" >Image not available</td>
            <?php } ?>
          </tr>
          <?php } 
               }else{ ?>
          <tr bgcolor="#ededed">
            <td width="12%" height="10" align="center"><font color="#005b90"><b>No documents Uploaded</b></font></td>
          </tr>
          <? } ?>
        </table>
        <?php }?>

         
	</td>
	</tr>
		
<!--		--><?php //if(!$ttTransFlag && $_REQUEST["callFrom"]!="TT") {?><!--	-->
<!--			<tr bgcolor="#ededed">-->
<!--			<td width="25%" class="tdDefination">	-->
<!--			<b>Customer Account:&nbsp;</b><font color="red">*</font></b></td>-->
<!--			<td align="left" width="25%">-->
<!--			<input type="checkbox" name="ch_payinBook" id="ch_payinBook" value="Y" checked="checked" />-->
<!--			<input type="textbox" name="payinBook" value="--><?//=$payinBook;?><!--" style='visibility:hidden'>			</td>-->
<!--			 </tr>-->
<!--	--><?php //} ?><!--		-->
<!--      -->
		
			<tr bgcolor="#ededed">
		  <td colspan="4" class="tdDefination">
		  	 <table id= "selectDoc" style="display:none">
			 <tr bgcolor="#ededed">
			    <td width="25%" class="tdDefination"><b>Select Document category:&nbsp;</b></td>
				<td width="25%" class="tdDefination">
				<select name="category" id="category" style="WIDTH: 140px;HEIGHT: 18px;" onChange="selectCategory();">
                  		<option value="">Select a Category</option>
                  		<?
						 $categoryQuery = "select Id,description from ".TBL_CATEGORY." "; 
                  	     $categoryData = selectMultiRecords($categoryQuery);
						 
						 for ($i=0; $i < count($categoryData); $i++){
						  ?>
                  		<option value='<? echo $categoryData[$i]["Id"] ?>'  <? echo ($category == $categoryData[$i]["Id"] ? "selected" : "")?>><? echo $categoryData[$i]["description"]?></option>
						<? } ?>
						</select>				</td>
				<td class="tdDefination"><b>&nbsp;</b></td>
			    <td align="left">&nbsp;</td>
				</tr>
				
			  <!--<tr id="setCategory" bgcolor="#ededed">-->
			  <tr bgcolor="#ededed">
			    <td colspan="4" id="setCategory" >&nbsp;				</td>
				<td id="doc_sel" >&nbsp;				</td>
			</tr>

			<!--</tr>-->
			 </table>		   </td>
	  </tr>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Accepted Terms :</b></td>
			<td align="left" width="25%"  <?php echo $colspan?>><input type="radio" name="acceptedTerms" value="Y" <? if($acceptedTerms=="" || $acceptedTerms="Y") echo "checked"; ?> />
Yes
  <input type="radio" name="acceptedTerms" value="N" <? if($acceptedTerms=="N") echo "checked"; ?> />
No</td> 
</tr>
<?php if(!$ttTransFlag && $_REQUEST["callFrom"]!="TT") {?>	
			
		
		
<!--		<tr>-->
<!--		  <td class="tdDefination"><b>--><?//=$tradingAccountsL?><!-- :</b></td>-->
<!--           <td width="37%">--><?//=$tradingAccountsDDV?><!--</td>-->
<!--		 </tr>-->
		 <tr bgcolor="#ededed">
		 	<td class="tdDefination" align="right" valign="top">
			 <b>Send Email: &nbsp;
				<?php if(CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){?><br/><br/>Guide to trading with <?php echo SYSTEM; }?></b></td>
			<td align="left"><input type="checkbox" name="sendEmail" <?php 
			if(empty($_REQUEST['customerID']))
			{ 
			echo "checked='checked'";
			} ?>
			 id="sendEmail"  value="Y" style="float:left;" />
			<?php if(CONFIG_STEPS_STRESS_FREE_SERVICE_EMAIL=='1'){?><br/><input type="checkbox" name="sendServiceEmail" id="sendServiceEmail" value="Y"   style="float:left;clear:both;"/>
			<?php }?>
			</td>
		 </tr>
		 
		<?php } ?>	
		<tr bgcolor="#ededed">
		  <td colspan="4" class="remarks">Remarks about <?=__("Sender");?><br />
            <textarea name="senderRemarks" id="senderRemarks" cols="75" rows="5"><?=$senderRemarks?></textarea></td>
	  </tr>	  
		<tr bgcolor="#ededed" id="printDiv">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="customerID" value="<? if(!empty($customerID)){echo $customerID;}else{ echo $_REQUEST["customerID"];}?>" />
				<input type="hidden" name="from" value="<?=$_REQUEST["from"]?>" />
				<input type="hidden" name="agentID1" value="<?=$_REQUEST["agentID"]?>" />
				<input type="hidden" name="transID" value="<?=$_REQUEST["transID"]?>" />
				<input type="hidden" name="parentID" value="<? echo $_REQUEST["customerParentID"]?>" />
				<input type="hidden" name="saveInputs" value="y" />
				
				<input type="hidden" name="userNameF" value="<?=$_GET["userName"]?>" />
				<input type="hidden" name="userTypeF" value="<?=$_GET["userType"]?>" />
				<input type="hidden" name="IDTypeF" value="<?=$_GET["IDType"]?>" />
				<input type="hidden" name="alertTypeF" value="<?=$_GET["alertType"]?>" />
				<input type="hidden" name="newOffsetF" value="<?=$_GET["newOffset"]?>" />
				&nbsp;&nbsp;
				<input type="button" id="closeWindow" value="Close Window" onClick="window.close()"/>
				<input id="printBtn" name="printBtn" type="button" value=" Print " onClick="printFunction();">
							</td>
		</tr>
	</table>
</form>
</body>
</html>