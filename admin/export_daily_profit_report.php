<?php

	session_start();
	include ("../include/config.php");
	include ("security.php");
	$rootPath = $_SERVER['DOCUMENT_ROOT']."/admin/lib/";
	include_once($rootPath."currency_exchange_functions.php");
	$systemCode = SYSTEM_CODE;
  $manualCode = MANUAL_CODE;
	//debug($_REQUEST,true);
	$roundAmountTo	= 2;
	$roundRateTo 	= 4;

	$exportType 		= $_POST['exportReport'];
	$colHeads 			= str_replace(' ','+',$_REQUEST["colHeads"]);
	$exportData 		= str_replace(' ','+',$_REQUEST["exportData"]);
	$exportTotalData 	= str_replace(' ','+',$_REQUEST["exportTotalData"]);
	
	
	if(empty($colHeads))
		die("Export header columns do not exist.");
	if(empty($exportData))
		die("Data cannot be exported.");
	if(empty($exportTotalData))
		die("Data cannot be exported.");

	$colHeadsV 				= unserialize(base64_decode($colHeads));
	$exportDataV			= unserialize(base64_decode($exportData));
	$exportTotalDataV = unserialize(base64_decode($exportTotalData));
	$multipleData 	= $exportDataV['daily_date'];
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";

	if($exportType == "XLS")
	{
	
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=daily_profit_report_".time().".".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strCenterColumnStart = "<td align='center'>";
		$strColumnStart = "<td align='right' style='mso-number-format:\"\@\"'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=daily_profit_report_".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart =	$strCenterColumnStart	= "\"";
		$strColumnEnd   = "\",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strCenterColumnStart = "<td align='center'>";
		$strColumnStart = "<td align='right'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Daily Profit Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
	
	// Heading column labels
	foreach($colHeadsV as $k=>$v)
		$strFullHtml .= $strCenterColumnStart.$strBoldStart.$v.$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;

	// Currencies data
	foreach($multipleData as $curr)
	{
		// currency name for each row
		$strFullHtml .= $strColumnStart.$curr.$strColumnEnd;
		// columns for specific data
		foreach($exportDataV[$curr] as $k=>$v){
			$data = $v['data'];
			if(!empty($v['dec']))
				$data = number_format($v['data'],$v['dec'],'.',',');
			$strFullHtml .= $strColumnStart.$data.$strColumnEnd;
		}
		$strFullHtml .= $strRowEnd;
	}
	$strFullHtml .= $strRowStart;

	// Grand Totla row
	foreach ($exportTotalDataV as $k=>$v){
		// columns for specific data
		$data = $v['data'];
			if(!empty($v['dec']))
				$data = number_format($v['data'],$v['dec'],'.',',');
		$strFullHtml .= $strColumnStart.$data.$strColumnEnd;
	}
	$strFullHtml .= $strRowEnd;	   	   
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>