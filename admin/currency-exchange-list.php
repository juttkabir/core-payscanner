<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}

$agentType = getAgentType();
// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = "id";

$colSpans = 12;


/**
 * Adding more filter to search the data
 * @Ticket# 3426
 *
 * Add the functionality that filter search can available to Super Agents also
 * @Ticket# 3425
 */
$filters = "";
if($_GET["Submit"]!="" || $_POST["Submit"]!="")
{
	$fromDate = $_GET["fYear"] . "-" . $_GET["fMonth"] . "-" . $_GET["fDay"]." 00:00:00";
	$toDate = $_GET["tYear"] . "-" . $_GET["tMonth"] . "-" . $_GET["tDay"]." 23:59:59";
	$fromStrDate = date("Y-m-d H:i:s",strtotime($fromDate));
	$toStrDate = date("Y-m-d H:i:s",strtotime($toDate));
	$filters .= " and (created_at >= '$fromStrDate' and created_at <= '$toStrDate')";

	if(!empty($_GET["createdBy"]))
	{
		$filters .= " and name = '".$_GET['createdBy']."'";
	}
	if(!empty($_GET["buysellCurrency"]))
	{
		$filters .= " and buysellCurrency = ".$_GET['buysellCurrency'];
	}
}
//debug($filters,true);
$SQL_Qry = "select ce.*, a.name from curr_exchange ce,admin a where a.userID=ce.createdBy  ";
$SQL_Qry .= $filters;
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by ce.$sortBy DESC ";
$rates = SelectMultiRecords($SQL_Qry);
$allCount = count($rates);
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
//debug($SQL_Qry);
$rates = SelectMultiRecords($SQL_Qry);

?>
<html>
<head>
<title>Categories List</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($rates); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete selected currency exchange(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No currency exchange(s) is selected for deletion.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				/*	indexes of objects. as there are two hidden fields 
					objects which are not to be checked. so starting index is
					2 and passed 0,1 for both hidden fields.
				*/
				j=2;
				for(i=0; i < <?=count($rates);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 3;
				for(i=0; i< <?=count($rates);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		}
		else{
			alert("Fields selection is not working.");
		}
	}
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar">Manage Currency Exchanges</td>
	</tr>
	<tr>
		<td><fieldset>
			<table border="0" align="center" cellpadding="5" cellspacing="1">
				<tr>
					<td align="center"><b>SEARCH FILTER</b> </td>
				</tr>
				<tr>
					<form name="search" method="get" action="currency-exchange-list.php">
						<td width="100%" colspan="4" bgcolor="#DFE6EA" align="center">
						<?
							$getBSCurrData1 = SelectMultiRecords("select cID,currencyName 
																	from currencies 
																	where cID IN 
																(select buysellCurrency 
																	from curr_exchange) 
																	group by currencyName");
						?>
							&nbsp;&nbsp; <b>Buy/Sell Currency</b>
							<select name="buysellCurrency" style="font-family: verdana; font-size: 11px;">
								<option value="">Select Currency</option>
								<?
									for ($i=0; $i < count($getBSCurrData1); $i++){
										$getBSCurrName1 = $getBSCurrData1[$i]['currencyName'];		
										$getBSCurrID1   = $getBSCurrData1[$i]["cID"];
											
								 ?>
								<OPTION value="<?=$getBSCurrID1; ?>">
								<?=$getBSCurrName1; ?>
								</OPTION>
								<?
																}
														 ?>
							</SELECT>
							<script language="JavaScript">
											     				SelectOption(document.search.buysellCurrency, "<?=$_GET["buysellCurrency"]?>");
										      	  </script>
							<br />
							<br />
							<b>From </b>
							<? 
															$month = date("m");
															$day = date("d");
															$year = date("Y");
															?>
							<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
								<?
														  	for ($Day=1;$Day<32;$Day++)
																{
																	if ($Day<10)
																	$Day="0".$Day;
																	echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
																}
														  ?>
							</select>
							<script language="JavaScript">
											         	SelectOption(document.search.fDay, "<?=(!empty($_GET["fDay"])?$_GET["fDay"]:"")?>");
											        </script>
							<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
								<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
								<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
								<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
								<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
								<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
								<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
								<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
								<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
								<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
								<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
								<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
								<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
							</SELECT>
							<script language="JavaScript">
											         	SelectOption(document.search.fMonth, "<?=(!empty($_GET["fMonth"])?$_GET["fMonth"]:"")?>");
											        </script>
							<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
								<?
															  	$cYear=date("Y");
															  	for ($Year=2004;$Year<=$cYear;$Year++)
																	{
																		echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
																	}
															  ?>
							</SELECT>
							<script language="JavaScript">
											         	SelectOption(document.search.fYear, "<?=(!empty($_GET["fYear"])?$_GET["fYear"]:"")?>");
											        </script>
							<b>To </b>
							<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
								<?
															  	for ($Day=1;$Day<32;$Day++)
																	{
																		if ($Day<10)
																		$Day="0".$Day;
																		echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
																	}
															  ?>
							</select>
							<script language="JavaScript">
											         	SelectOption(document.search.tDay, "<?=(!empty($_GET["tDay"])?$_GET["tDay"]:"")?>");
											        </script>
							<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
								<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
								<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
								<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
								<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
								<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
								<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
								<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
								<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
								<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
								<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
								<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
								<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
							</SELECT>
							<script language="JavaScript">
											         	SelectOption(document.search.tMonth, "<?=(!empty($_GET["tMonth"])?$_GET["tMonth"]:"")?>");
											        </script>
							<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
								<?
														  	$cYear=date("Y");
														  	for ($Year=2004;$Year<=$cYear;$Year++)
																{

																	echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
																}
														  ?>
							</SELECT>
							<script language="JavaScript">
											        SelectOption(document.search.tYear, "<?=(!empty($_GET["tYear"])?$_GET["tYear"]:"")?>");
											        </script>
							<br />
							<br />
							&nbsp;&nbsp; <b> Creator Name </b>
							<input type="text" id="createdBy" name="createdBy" style="font-family: verdana; font-size: 11px;">
							<br/>
							<br/>
							<input type="submit" name="Submit" value="Process">
						</td>
					</form>
				</tr>
			</table>
			</fieldset>
			<br>
		</td>
	</tr>
	<form action="delete-rates.php" method="post" onSubmit="return checkForm(this);">
		<input type="hidden" name="del" value="0">
		<input type="hidden" name="app" value="0">
		<tr>
			<td align="center"><table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
					<?
			if ($allCount > 0){
		?>
					<tr>
						<td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
								<tr>
									<td><?php if (count($rates) > 0) {;?>
										Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> of
										<?=$allCount; ?>
										<?php } ;?>
									</td>
									<?php if ($prv >= 0) { ?>
									<td width="50"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> </td>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a> </td>
									<?php } ?>
									<?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; </td>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; </td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<?php } ?>
								</tr>
							</table></td>
					</tr>
					<tr bgcolor="#DFE6EA">
						<td width="50"><a href="#" onClick="checkAllFn(document.forms[1], 1);"><font color="#005b90"><strong>Delete</strong></font></a></td>
						<td width="109"><font color="#005b90"><strong>Buying/Selling Currency</strong></font></a></td>
						<td width="125"><font color="#005b90"><b>Buying Rate</b></font></a></td>
						<td width="109"><font color="#005b90"><b>Selling Rate</b></font></a></td>
						<td width="109"><font color="#005b90"><strong>Created By</strong></font></a></td>
						<td width="109"><font color="#005b90"><strong>Creation Date/Time</strong></font></a></td>
					</tr>
					<?
			for ($i=0; $i < count($rates); $i++){
			$getBSCurrData = selectFrom("select currencyName from currencies where cID = ".$rates[$i]['buysellCurrency']);
			$getBSCurrName = $getBSCurrData['currencyName'];		
			$Curr_exch_id=selectFrom("select id from curr_exchange where buysellCurrency = '".$rates[$i]['buysellCurrency']."'");
		?>
					<tr valign="top" bgcolor="#eeeeee" title="Rate for : <? echo $getBSCurrName;?>">
						<td align="center" ><input type="checkbox" name="id[<?=$i;?>]" value="<?=$rates[$i]['id']?>"></td>
						<td><a href="#" onClick="javascript:window.open('add-currency-exchange.php?id=<? echo $rates[$i]["id"];?>')"><font color="#005b90"><b><?=$getBSCurrName?></b></font></a></td>
						<td><?=stripslashes($rates[$i]["buying_rate"]); ?></td>
						<td><?=stripslashes($rates[$i]["selling_rate"]); ?></td>
						<td><?=stripslashes($rates[$i]["name"]); ?>
						</td>
						<td><?=date("d/m/Y H:i",strtotime($rates[$i]["created_at"])); ?></td>
					</tr>
					<?
			}
		?>
					<tr>
						<td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
								<tr>
									<td><?php if (count($rates) > 0) {;?>
										Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> of
										<?=$allCount; ?>
										<?php } ;?>
									</td>
									<?php if ($prv >= 0) { ?>
									<td width="50"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> </td>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a> </td>
									<?php } ?>
									<?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; </td>
									<td width="50" align="right"><a href="<?php print $PHP_SELF . "".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; </td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<?php } ?>
								</tr>
							</table></td>
					</tr>
					<tr>
						<td colspan="<?=$colSpans?>" align="center">
						<input type="submit" value="Delete Rates">
						<input type="hidden" name="ceRates" value="ceRates">
						</td>
					</tr>
					<?
			} else {
		?>
					<tr>
						<td colspan="5" align="center"> No Exchnage Rates found in the database. </td>
					</tr>
					<?
			}
		?>
				</table></td>
		</tr>
	</form>
</table>
</body>
</html>
