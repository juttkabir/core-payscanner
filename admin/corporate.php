<?
if (isset($_GET['type'])) {
    $register_type = base64_decode(urldecode($_GET['type']));
} else {
    $register_type = "";
}
if (isset($_GET['cur_type'])) {
    $cust_type = $_GET['cur_type'];
} else {
    $cust_type = "USD";
}
?><!DOCTYPE html>
<?include $_SERVER['DOCUMENT_ROOT'] . "/include/config.php";?>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Corporate Client Login - Payscanner Online Currency Transfers - PAYscanner.com</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets_spi/css/styles00.min.css">
    <!-- Favicon and Apple Icons -->
    <link rel="icon" type="image/png" href="./assets_spi/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="./assets_spi/images/icons/faviconx57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./assets_spi/images/icons/faviconx72.png">
    <script src="./assets_spi/js/scriptjs.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
        .normal{
            font-weight: normal !important;
        }
    </style>
    <style type="text/css">
        .passport-filedroppable {
            background: #e6e6e6;
            color: #000;
            /* padding: 28px 0; */
            text-align: center;
            line-height: 88px;
            height: 88px;
            border: dashed 2px #343434;
            margin-bottom: 20px;
            border-radius: 4px;
        }

        .passport-filedroppable.dragover {
            background: #009688;
            color: #fff;
        }
        .file_preview{
            border: solid 5px #d3d3d3;
            padding: 10px;
            text-align: center;
        }
    </style>
    <style>
        .box
        {
            font-size: 1.25rem; /* 20 */
            background-color: #c8dadf;
            position: relative;
            padding: 100px 20px;
        }
        .box.has-advanced-upload
        {
            outline: 2px dashed #92b0b3;
            outline-offset: -10px;

            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear;
        }
        .box.is-dragover
        {
            outline-offset: -20px;
            outline-color: #c8dadf;
            background-color: #fff;
        }
        .box__dragndrop
        {
            display: inline-block;
        }
        .box.has-advanced-upload .box__dragndrop
        {
            display: inline;
        }
        .box.has-advanced-upload .box__icon
        {
            width: 100%;
            height: 80px;
            fill: #92b0b3;
            display: block;
            margin-bottom: 40px;
        }

        .box.is-uploading .box__input,
        .box.is-success .box__input,
        .box.is-error .box__input
        {
            visibility: visible;
        }

        .box__uploading,
        .box__success,
        .box__error
        {
            display: none;
        }
        .box.is-uploading .box__uploading,
        .box.is-success .box__success,
        .box.is-error .box__error
        {
            display: block;
            position: absolute;
            top: 50%;
            right: 0;
            left: 0;

            -webkit-transform: translateY( -50% );
            transform: translateY( -50% );
        }
        .box__uploading
        {
            font-style: italic;
        }
        .box__success
        {
            -webkit-animation: appear-from-inside .25s ease-in-out;
            animation: appear-from-inside .25s ease-in-out;
        }
        @-webkit-keyframes appear-from-inside
        {
            from	{ -webkit-transform: translateY( -50% ) scale( 0 ); }
            75%		{ -webkit-transform: translateY( -50% ) scale( 1.1 ); }
            to		{ -webkit-transform: translateY( -50% ) scale( 1 ); }
        }
        @keyframes appear-from-inside
        {
            from	{ transform: translateY( -50% ) scale( 0 ); }
            75%		{ transform: translateY( -50% ) scale( 1.1 ); }
            to		{ transform: translateY( -50% ) scale( 1 ); }
        }

        .box__restart
        {
            font-weight: 700;
        }
        .box__restart:focus,
        .box__restart:hover
        {
            color: #39bfd3;
        }
        .box__file{
            opacity: 0;
            z-index: 99;
            margin-top: -92px;
            width: 520px;
        }

        .no-js .box__button
        {
            display: block;
        }
        .box__button
        {
            font-weight: 700;
            color: #e5edf1;
            background-color: #39bfd3;
            display: none;
            padding: 8px 16px;
            margin: 40px auto 0;
        }
        .box__button:hover,
        .box__button:focus
        {
            background-color: #0f3c4b;
        }
    </style>
    <link rel="canonical" href="https://css-tricks.com/examples/DragAndDropFileUploading/">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,400" />
    <link rel="stylesheet" href="./assets_spi/css/bootstrap-datetimepicker.min.css">
    <!-- Google Analytics -->
    <?php include '_ga.inc.php';?>
    <script type="text/javascript" src="javascript/custom-corporate.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript">
        function validateMyForm()
        {
            var form = $('#senderRegistrationForm');
            var formdata = false;
            if (window.FormData){
                formdata = new FormData(form[0]);
            }

            var formAction = form.attr('action');
            $.ajax({
                url         : 'https://<?php echo $_SERVER['HTTP_HOST']; ?>/admin/corporate_conf.php',
                data        : formdata ? formdata : form.serialize(),
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                success     : function(data, textStatus, jqXHR){
                    // Callback code
                    alert("oyehoye");
                }
            });

            return false;
        }

        // $('#senderRegistrationForm').before('sumbit', function(){
        // var form = $('#senderRegistrationForm');
        // var formdata = false;
        // if (window.FormData){
        // formdata = new FormData(form[0]);
        // }

        // var formAction = form.attr('action');
        // $.ajax({
        // url         : '/corporate_conf.php',
        // data        : formdata ? formdata : form.serialize(),
        // cache       : false,
        // contentType : false,
        // processData : false,
        // type        : 'POST',
        // success     : function(data, textStatus, jqXHR){
        // // Callback code
        // alert("oyehoye");
        // }
        // });



        //});



        // function btnSubmitCorporate() {
        // console.log("before submit");
        // var data = $('#senderRegistrationForm').serialize();
        // $.ajax({
        // url: 'corporate_conf.php',
        // data: data,
        // type: 'POST',
        // success: function (msg) {
        // / alert(msg);
        // console.log("after submit");
        // //console.log(msg);
        // }
        // });
        // //
        // //
        // }
    </script>
</head>
<body>
<div></div>
<!-- End .boss-loader-overlay -->
<div id="login-section" class="fu llheight">
    <div class="vcen ter-container">
        <div class="vcent er">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="logo text-center mt30">
                            <a href="http://payscanner.com" title="PAYscanner">
                                <img src="images/spi_payscanner_com/logo_ps.png"
                                     alt="PAYscanner">
                            </a>
                        </h1>
                        <div class="form-wrapper">
                            <h2 class="h4 title-underblock custom mb30">Open an account: corporate client</h2>
                            <p>Please complete the form below and upload one form of identification to comply
                                with UK money laundering regulations, we require:</p>
                            <form name="senderRegistrationForm"  onsubmit="event.preventDefault(); validateMyForm();"  id="senderRegistrationForm"  method="post" enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo $register_type?>" id="register_type" name="register_type" />
                                <input type="hidden" name="ipAddress" value="95.210.228.178" />
                                <input type="hidden" name="initial_load" id="initial_load" class="form_fields" value="0.00" style="text-transform:uppercase" />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Company</small></span></h2>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="companyname" class="input-desc">Company Name</label>
                                            <input type="text" class="form-control" id="companyname" name="companyname"
                                                   placeholder="Your Company Name">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="postCodeSearch" class="input-desc">Post Code</label>
                                            <input type="text" class="form-control" id="postCodeSearch" name="postCodeSearch" placeholder="Company Post Code"  onKeyPress='enterToSearch(event);' onFocus="clearAddress();" >
                                        </div><!-- End .from-group -->


                                    </div><!-- End .col-sm-6 -->

                                    <div class="col-sm-3">
                                        <div class="form-group"> <label for="postCodeSearch" class="input-desc">&nbsp;</label>
                                            <input type="button" id="searchAddress" value="Search for address" class=" form-control btn btn-custom" />
                                        </div><!-- End .from-group -->


                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="addressline1" class="input-desc">Address line 1</label>
                                            <input type="text" class="form-control" id="addressline1" name="addressline1"
                                                   placeholder="First line Address">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="addressline2" class="input-desc">Address line 2</label>
                                            <input type="text" class="form-control" id="addressline2" name="addressline2"
                                                   placeholder="Second line Address">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="city" class="input-desc">City</label>
                                            <input type="text" class="form-control" id="city" name="city"
                                                   placeholder="City">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="country" class="input-desc">Country</label>
                                            <select name="residenceCountry" class="form-control select_field" id="residenceCountry">
                                                <?$custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
                                                $custCountries = array_filter($custCountries);
                                                for ($k = 0; $k < count($custCountries); $k++) {
                                                    $selected = "";
                                                    if (trim($custCountries[$k]) == "United Kingdom") {
                                                        $selected = "selected";}?>
                                                    <option value="<?echo trim($custCountries[$k]) ?>" <?echo $selected ?> >
                                                        <? echo trim($custCountries[$k]) ?>
                                                    </option>
                                                <?}?>
                                            </select>
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone" class="input-desc">Phone</label>
                                            <input type="text" class="form-control" id="phone" name="phone"
                                                   placeholder="Phone">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="registerationnumber" class="input-desc">Registration  Number</label>
                                            <input type="text" class="form-control" id="registerationnumber"
                                                   name="registerationnumber" placeholder="Registration Number">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Contact Person</small></span></h2>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="personname" class="input-desc">Contact Person Name</label>
                                            <input type="text" class="form-control" id="personname" name="personname"
                                                   placeholder="Your Name">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="contactpersondesignation" class="input-desc">Contact
                                                Person Designation</label>
                                            <input type="text" class="form-control" id="contactpersondesignation"
                                                   name="contactpersondesignation" placeholder="Your Name">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="contactpersonphone" class="input-desc">Contact Person
                                                Phone</label>
                                            <input type="text" class="form-control" id="contactpersonphone"
                                                   name="contactpersonphone" placeholder="Your Phone">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="contactpersonemail" class="input-desc">Contact Person
                                                Email</label>
                                            <input type="text" class="form-control" id="contactpersonemail"
                                                   name="contactpersonemail" placeholder="Your Email">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Beneficial Owner</small></span></h2>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="BeneficialOwner" class="input-desc">Name</label>
                                            <input type="text" class="form-control" id="BeneficialOwner" name="BeneficialOwner" placeholder="Name">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Director</small></span></h2>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <label for="directorname" class="input-desc">Name</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <label for="directorphone" class="input-desc">Phone</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <label for="directoremail" class="input-desc">Email</label>
                                    </div>
                                </div>

                                <div id="cp_row" class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group mb10">
                                            <input type="text" class="form-control directorname" id="directorname" name="directorname[0]" placeholder="Director Name 1" group="SD001">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group mb10">
                                            <input type="text" class="form-control directorphone" id="directorphone" name="directorphone[0]" placeholder="Phone" group="SD001">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group mb10">
                                            <input type="text" class="form-control directoremail" id="directoremail" name="directoremail[0]" placeholder="Email" group="SD001">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                        <div class="form-group mb10">


                                            <input type="button"onClick="copy_director();"  id="add-Director-row" value="+"  class="fa fa-plus btn btn-success"    aria-hidden="true"  />

                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">

                                        <!--------
                                                           <input type="button"onClick="remove_director();"  id="add-Director-row" value="-"  class="fa fa-plus btn btn-success"    aria-hidden="true"  />
                                               --->


                                    </div>
                                </div>
                                <div class="row row_Next_Directors">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="mt30 mb20 title-border custom title-bg-line text-left"><span><small>Passport Details</small></span></h2>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportCountry" class="input-desc">Country of issue</label>
                                            <select name="passportCountry" id="passportCountry" class="form-control select_field">
                                                <option value="" selected='selected'>-- Select Country of Issue --</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="USA">USA</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Canada">Canada</option>
                                            </select>

                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportexpirydate" class="input-desc">Expiry date</label>
                                            <div class="input-group date form-date" data-date=""
                                                 data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                                                 data-link-format="dd-mm-yyyy">
                                                <input class="form-control" id="passportexpirydate" name="passportexpirydate"
                                                       size="16" type="date">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div><!-- End .input-group -->
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportupload" class="input-desc">Passport Upload</label>




                                            <div class="passport-filedroppable">
                                                <div class="box__input">
                                                    <label for="file"><strong id="filename">Choose a file or drag it here.</strong></label>

                                                </div>
                                            </div>
                                            <div class="output"></div>




                                                    <span class="help-block mt10"><strong>Hint:</strong>
                                                        <small>Please
                                                            upload a scanned copy of your passport, the maximum file
                                                            size is 2Mb.<br>If you are having difficulty uploading the
                                                            file please post a copy or e-mail us at
                                                            hello@payscanner.com as soon as possible.
                                                        </small>
                                                    </span>
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="passportNumber" class="input-desc">Passport Number</label>
                                            <input type="text" class="form-control" id="passportNumber" name="passportNumber"
                                                   placeholder="Passport Number">
                                                    <span class="help-block mt10"><strong>Hint:</strong> <small>Please
                                                            enter your passport number as highlighted in the picture</small></span>
                                            <img src="./assets_spi/images/uk_passport.jpg"
                                                 alt="UK Specimin Passport" class="img-responsive mt15 mb15 pull-right">
                                        </div><!-- End .from-group -->
                                    </div><!-- End .col-sm-6 -->



                                </div><!-- End .row -->
                                <div class="mb20"></div><!-- space -->
                                <? /*<div class="form-group mt15-r">
                                            <div class="checkbox">
                                                <label class="custom-checkbox-wrapper">
                                                    <span class="custom-checkbox-container">
                                                        <input type="checkbox" name="policy" id="policy" value="true">
                                                        <span class="custom-checkbox-icon"></span>
                                                    </span>
                                                    <span>I read and agreed on the <a href="#">Terms and conditions</a>
                                                        of PAYscanner.</span>
                                                </label>
                                            </div><!-- End .checkbox -->
                                        </div><!-- End .form-group -->
                                        */ ?>
                                <div class="form-group mb5">
                                    <input type="submit" class="btn btn-custom"  value="Register Now">

                                </div><!-- End .from-group -->
                            </form>
                        </div><!-- End .form-wrapper -->
                        <div class="mb20"></div><!-- space -->
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="copyright text-center">
                                    Payscanner Online is a registered trademark of Payscanner Ltd.<br>
                                    Copyright (c) 2019 Payscanner Ltd. All rights reserved.
                                    <a href="">www.payscanner.com</a>
                                </p>
                            </div>
                        </div>
                        <div class="mb20"></div><!-- space -->
                    </div><!-- End .col-sm-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .vcenter -->
    </div><!-- End .vcenter-container -->
</div><!-- End .fullheight -->
<a href="#top" id="scroll-top" title="Back to Top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- END -->

<script src="./assets_spi/js/script00.min.js"></script>



<script type="text/javascript">
    (function(window) {
        function triggerCallback(e, callback) {
            if(!callback || typeof callback !== 'function') {
                return;
            }
            var files;
            if(e.dataTransfer) {
                files = e.dataTransfer.files;
            } else if(e.target) {
                files = e.target.files;
            }
            callback.call(null, files);
        }
        function makeDroppable(ele, callback) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('name', 'upload_passport');
            input.setAttribute('multiple', true);
            input.style.display = 'none';
            input.addEventListener('change', function(e) {
                triggerCallback(e, callback);
            });
            ele.appendChild(input);

            ele.addEventListener('dragover', function(e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.add('dragover');
            });

            ele.addEventListener('dragleave', function(e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.remove('dragover');
            });

            ele.addEventListener('drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
                ele.classList.remove('dragover');
                triggerCallback(e, callback);
            });

            ele.addEventListener('click', function() {
                input.value = null;
                input.click();
            });
        }
        window.makeDroppable = makeDroppable;
    })(this);
    (function(window) {
        makeDroppable(window.document.querySelector('.passport-filedroppable'), function(files) {
            console.log(files);
            var output = document.querySelector('.output');
            output.innerHTML = '';
            for(var i=0; i<files.length; i++) {
                if(files[i].type.indexOf('image/') === 0) {
                    output.innerHTML += '<img width="320" height="225" src="' + URL.createObjectURL(files[i]) + '" />';
                }
                output.innerHTML += '<p>'+files[i].name+'</p>';
            }
        });
    })(this);
</script>
<?php /*
        <script src="./assets_spi/js/script_register.js"></script>
        */?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
    // $(document).ready(function(){
    // $('input[type="file"]').change(function(e){
    // document.getElementById('filename').innerText = e.target.files[0].name;
    // });
    // });
</script>
<script>
    function copy_director()
    {
        var nbSD = ($("#senderRegistrationForm").find("[group^='SD001']").length / 3);
        var number=nbSD+1;
        var html_clone='   <div id="auto_copy" class="row auto_copy_class">  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"> <div class="form-group mb10"><input type="text" class="form-control directorname" id="directorname" name="directorname['+nbSD+']" placeholder="Director Name '+number+' " group="SD001"></div></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><div class="form-group mb10"><input type="text" class="form-control directorphone" id="directorphone" name="directorphone['+nbSD+']" placeholder="Phone" group="SD001"></div></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><div class="form-group mb10"><input type="text" class="form-control directoremail" id="directoremail" name="directoremail['+nbSD+']" placeholder="Email" group="SD001"></div></div>  <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"><div class="form-group mb10">'+''+' </div></div>   <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"><div class="form-group mb10">'+''+'</div></div></div> ';
        var ccn_div = $('.row_Next_Directors').last();
        console.log(ccn_div);
        // alert(ccn_div);
        //  e.preventDefault();

        if (nbSD < 10) {
            // $("#cp_row").clone().insertBefore(".row_Next_Directors");
            $(html_clone).insertBefore(".row_Next_Directors");
        }else{
            alert("Director more than 10 not allowed");
        }



    }

    // function remove_director()
    // {

    // /* remove-Director-row */

    // //  e.preventDefault();
    // $(this).closest('.auto_copy_class').fadeOut(300, function(){$(this).closest('.auto_copy_class').remove();});
    // return false;

    // }




    function enterToSearch(e) {

        if (e.which) {

            keyCode = e.which;

            if (keyCode == 13) {

                e.preventDefault();

                searchAddress();

            }

        }

    }

    function clearAddress() {

        $('#buildingNumber').val('');

        $('#buildingName').val('');

        $('#street').val('');

        $('#town').val('');

        $('#province').val('');

    }
    function searchAddress() {

        $('#residenceCountry').val('United Kingdom');

        $('#addressContainer').fadeOut('fast');

        postcode = $.trim($('#postCodeSearch').val());

        buildingNumber = $.trim($('#buildingNumber').val());

        street = $.trim($('#street').val());

        town = $.trim($('#town').val());

        if (postcode == '') {

            alert("Enter a postcode to search for your address");

            $('#postCodeSearch').focus();

            return;

        }
        $("#loadingMessage").text('Searching Address...');
        $('#wait').fadeIn("fast");

        $.ajax({
            url: "https://<?php echo $_SERVER['HTTP_HOST']; ?>/api/gbgroup/addresslookupCus.php",

            data: {

                postcode: postcode,

                buildingNumber: buildingNumber,

                street: street,

                town: town

            },

            type: "POST",

            cache: false,

            success: function(data) {

                //alert(data.match(/option/i));

                $('#wait').fadeOut("slow");

                if (data.search(/option/i) >= 0) {

                    $('#addressContainer').fadeIn('fast');

                    $('#suggesstions').html(data);

                } else {

                    $('#addressContainer').fadeIn('fast');

                    $('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');

                }

            }

        });

    }
    $('#searchAddress').click(



        function() {

            searchAddress();

        });
</script>
</body>
</html>