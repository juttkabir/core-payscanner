<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
extract(getHttpVars());


if($submit == "Search"){
// searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="cp";
			 $q=searchNameMultiTables(trim($userName),$fn,$mn,$ln,$alis);
			 
$exactMatchquery="select firstName,middleName,lastName,PersonID,listName,enteredBy,matchUserType,cp.userDataBaseID,cml.userDataBaseID from ".TBL_COMPLIANCE_PERSON." as cp,".TBL_COMPLIANCE_LIST." as cl,".TBL_COMPLIANCE_MATCH_LIST." as cml where  cml.compliancePersonID=cp.compliancePersonID and cml.listID=cl.listID and isFullMatch= 'Y' and isBlocked = 'Y' ".$q." ";
//echo "select firstName,middleName,lastName,PersonID,listName,enteredBy,matchUserType,userDataBaseID from ".TBL_COMPLIANCE_PERSON." as cp,".TBL_COMPLIANCE_LIST." as cl,".TBL_COMPLIANCE_MATCH_LIST." as cml where cml.compliancePersonID=cp.compliancePersonID and cml.listID=cl.listID and isFullMatch= 'Y' ".$q." ";
$exactMatch = selectMultiRecords($exactMatchquery);

$likelyMatchquery="select firstName,middleName,lastName,PersonID,listName,enteredBy,matchUserType,cp.userDataBaseID,cml.userDataBaseID from ".TBL_COMPLIANCE_PERSON." as cp,".TBL_COMPLIANCE_LIST." as cl,".TBL_COMPLIANCE_MATCH_LIST." as cml where cml.compliancePersonID=cp.compliancePersonID and cml.listID=cl.listID and isFullMatch= 'N' ".$q."";
//echo"<br>"."select firstName,middleName,lastName,PersonID,listName,enteredBy,matchUserType,userDataBaseID from ".TBL_COMPLIANCE_PERSON." as cp,".TBL_COMPLIANCE_LIST." as cl,".TBL_COMPLIANCE_MATCH_LIST." as cml where cml.compliancePersonID=cp.compliancePersonID and cml.listID=cl.listID and isFullMatch= 'N' ".$q."";
$likelyMatch = selectMultiRecords($likelyMatchquery);

}
?>

<html>
<head>
<title>Compliance List Report</title>	
</head>	
<body>
<table width="665" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">Compliance List Report.</font></b></td>
  </tr>
  
<tr>
 <td align = "center" >
 <table width="255" border="1" cellpadding="5" bordercolor="#666666">
        <form action="reportComplianceList.php" method="post" name="Search" >
       <tr>
       <td align = "center">
        Name <input type="text" name="userName" id="userName" value="<? echo $userName ?>">
      
        <input type="submit" name="submit" value="Search"></td>
      
     </tr>
	  </form>
    </table>

</td>
</tr>
  
  <tr>
  <td>
  <table><tr><td><span class="style1"><strong><font color="#006699">Exact Matches</font></strong></span></td></tr></table>	
  	</td>	
  </tr>   
   
    <tr> 
    
    <td align="center" bgcolor="#EFEFEF">
    <table width="665" border="0" bordercolor="#EFEFEF">
    <tr bgcolor="#DFE6EA"> 
    	
		<td><span class="style1"><strong><font color="#006699">Full Name</font></strong></span></td>
         
          <td><span class="style1"><strong><font color="#006699">ID</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">List</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">Addition Attempt</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">User</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">No of Transactions</font></strong></span></td>
         </tr>
        <?

 if(count($exactMatch) > 0)
 {       
 	
  
 	

	 for($i = 0; $i < count($exactMatch); $i++)
   {      
   	
		?>
        <tr bgcolor="#ffffff"> 
		<td> &nbsp; 
            <?  echo $exactMatch[$i]["firstName"]." ".$exactMatch[$i]["middleName"]." ".$exactMatch[$i]["lastName"]; ?>
            
          </td>
          <td> &nbsp; 
            <?  echo $exactMatch[$i]["PersonID"]; ?>
          </td>
         <td> &nbsp; 
            <?  echo $exactMatch[$i]["listName"]; ?>
          </td>
        <td> &nbsp; 
            <?  echo $exactMatch[$i]["enteredBy"]; ?>
          </td>
          <td> &nbsp; 
            <?  echo $exactMatch[$i]["matchUserType"]; ?>
          </td>
         <?
            if($exactMatch[$i]["matchUserType"]=='sender' && $exactMatch[$i]["userDataBaseID"]!= ''){
            	
            	$query = "SELECT count(transID),customerID FROM ". TBL_TRANSACTIONS . "  WHERE transStatus != 'Cancelled' and  customerID=".$exactMatch[$i]["userDataBaseID"]." ";
	            $query .= " Group By customerID  ";
	            $contentsTrans = selectMultiRecords($query); 
	                 	
            	
         ?>
        <td width="81" >
				  	<a href="#" onClick="javascript:window.open('reportComplianceListDetail.php?customerID=<? echo $contentsTrans[$i]["customerID"]; ?>','viewTranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo  $contentsTrans[$i]["count(transID)"]; ?></font></strong></a>
				</td>  
				<? }elseif($exactMatch[$i]["matchUserType"]=='beneficiary' && $exactMatch[$i]["userDataBaseID"]!= ''){
					    $query = "SELECT count(transID),benID FROM ". TBL_TRANSACTIONS . "  WHERE transStatus != 'Cancelled' and  benID=".$exactMatch[$i]["userDataBaseID"]." ";
	            $query .= " Group By benID  ";
	            $contentsTrans = selectMultiRecords($query); 
					
					?>
				 <td width="81" >
				  	<a href="#" onClick="javascript:window.open('reportComplianceListDetail.php?benID=<? echo $contentsTrans[$i]["benID"]; ?>','viewTranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo  $contentsTrans[$i]["count(transID)"]; ?></font></strong></a>
				</td>  
				<? }else{ ?>
				 <td width="81" >&nbsp;</td>
				 <? } ?>	
          
        </tr>
  
     
		
  <?
} 
}
 ?>
	
</table>
</td>
</tr>


  <tr>
  <td>
  <table><tr><td><span class="style1"><strong><font color="#006699">Likely Matches</font></strong></span></td></tr></table>	
  	</td>	
  </tr>   
   
    <tr> 
    
    <td align="center" bgcolor="#EFEFEF">
    <table width="665" border="0" bordercolor="#EFEFEF">
    <tr bgcolor="#DFE6EA"> 
    	
		<td><span class="style1"><strong><font color="#006699">Full Name</font></strong></span></td>
         
          <td><span class="style1"><strong><font color="#006699">ID</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">List</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">Addition Attempt</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">User</font></strong></span></td>
          <td><span class="style1"><strong><font color="#006699">No of Transactions</font></strong></span></td>
         </tr>
        <?

 if(count($likelyMatch) > 0)
 {       
 	
  
 	

	 for($i = 0; $i < count($likelyMatch); $i++)
   {      
   	
		?>
        <tr bgcolor="#ffffff"> 
		<td> &nbsp; 
             <?  echo $likelyMatch[$i]["firstName"]." ".$likelyMatch[$i]["middleName"]." ".$likelyMatch[$i]["lastName"]; ?>
            
          </td>
          <td> &nbsp; 
            <?  echo $likelyMatch[$i]["PersonID"]; ?>
          </td>
         <td> &nbsp; 
            <?  echo $likelyMatch[$i]["listName"]; ?>
          </td>
        <td> &nbsp; 
            <?  echo $likelyMatch[$i]["enteredBy"]; ?>
          </td>
          <td> &nbsp; 
            <?  echo $likelyMatch[$i]["matchUserType"]; ?>
          </td>
           <?
            if($likelyMatch[$i]["matchUserType"]=='sender' && $likelyMatch[$i]["userDataBaseID"]!= ''){
            	
            	$query = "SELECT count(transID),customerID FROM ". TBL_TRANSACTIONS . "  WHERE transStatus != 'Cancelled' and  customerID=".$likelyMatch[$i]["userDataBaseID"]." ";
	            $query .= " Group By customerID  ";
	            $contentsTrans = selectMultiRecords($query); 
	                 	
            	
         ?>
        <td width="81" >
				  	<a href="#" onClick="javascript:window.open('reportComplianceListDetail.php?customerID=<? echo $contentsTrans[$i]["customerID"]; ?>','viewTranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo  $contentsTrans[$i]["count(transID)"]; ?></font></strong></a>
				</td>  
				<? }elseif($likelyMatch[$i]["matchUserType"]=='beneficiary' && $likelyMatch[$i]["userDataBaseID"]!= ''){
					    $query = "SELECT count(transID),benID FROM ". TBL_TRANSACTIONS . "  WHERE transStatus != 'Cancelled' and  benID=".$likelyMatch[$i]["userDataBaseID"]." ";
	            $query .= " Group By benID  ";
	            $contentsTrans = selectMultiRecords($query); 
					
					?>
				 <td width="81" >
				  	<a href="#" onClick="javascript:window.open('reportComplianceListDetail.php?benID=<? echo $contentsTrans[$i]["benID"]; ?>','viewTranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo  $contentsTrans[$i]["count(transID)"]; ?></font></strong></a>
				</td>  
				<? }else{ ?>
				 <td width="81" >&nbsp;</td>
				 <? } ?>	
          
          
        </tr>
  
     
		
  <?
} 
}
 ?>
	
</table>
</td>
</tr>
   
</table>
</body>
</html>