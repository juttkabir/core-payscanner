<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

if(CONFIG_TRANS_ROUND_LEVEL > 0)
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}
	
$exRate24hr = 0.0;


if ($_GET["newOffset"]!="")
{
	$offset = $_GET["newOffset"];
}
if($_POST["erID"] == "")
	$SQL_Qry = "select * from ".TBL_EXCHANGE_RATES." where erID='".$_GET["erID"]."'";
else
	$SQL_Qry = "select * from ".TBL_EXCHANGE_RATES." where erID='".$_POST["erID"]."'";
$rates = selectFrom($SQL_Qry);
if($_POST["rateFor"] == "")
{
	$_SESSION["rateFor"] = $rates["rateFor"];
}else{
	$_SESSION["rateFor"] = $_POST["rateFor"];
	}//$_SESSION["currencyOrigin"]

	if($_POST["currencyOrigin"] == "")
	{
		$_SESSION["currencyOrigin"] = $rates["currencyOrigin"];
	}else{
		$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];
	}
	
	if($_POST["intermediateCurrency"] == "")
	{
		$_SESSION["intermediateCurrency"] = $rates["intermediateCurrency"];
	}else{
		$_SESSION["intermediateCurrency"] = $_POST["intermediateCurrency"];
	}
	
	
	if($_POST["currency"] == "")
	{
		$_SESSION["currency"] = $rates["currency"];
	}else{
		$_SESSION["currency"] = $_POST["currency"];
	}
	
	if($_POST["marginType"] != '')
	{
		$_SESSION["marginType"] = $_POST["marginType"];
	}
	
if($_POST["agentMarginType"] != '')
{
	$_SESSION["agentMarginType"] = $_POST["agentMarginType"];
}else{
	$_SESSION["agentMarginType"] = $rates["agentMarginType"];
	}

if($_POST["agentMarginValue"] != '')
{
	$_SESSION["agentMarginValue"] = $_POST["agentMarginValue"];
}else{
	$_SESSION["agentMarginValue"] = $rates["agentMarginValue"];
	}


if($_POST["intermediatePrimRate"] == "")
	{
		$_SESSION["intermediatePrimRate"] = $rates["intermediatePrimRate"];
	}else{
		$_SESSION["intermediatePrimRate"] = $_POST["intermediatePrimRate"];
	}	
	
	
	if($_POST["intermediateDestExchangeRate"] == "")
	{
		$_SESSION["intermediateSecondaryRate"] = $rates["intermediateSecondaryRate"];
	}else{
		$_SESSION["intermediateSecondaryRate"] = $_POST["intermediateDestExchangeRate"];
	}
	
	/**
	 * @Ticket# 3318
	 * Handling the session for the 24 Hr banking values
	 */
	if(CONFIG_24HR_BANKING == "1")
	{
		/**
		 * Retriving the margin value
		 */
		if($_POST["_24hrMarginValue"] == "")
			$_SESSION["_24hrMarginValue"] = $rates["_24hrMarginValue"];
		else
			$_SESSION["_24hrMarginValue"] = $_POST["_24hrMarginValue"];
			
		/**
		 * Retriving the margin type
		 */
		if($_POST["_24hrMarginType"] == "")
			$_SESSION["_24hrMarginType"] = $rates["_24hrMarginType"];
		else
			$_SESSION["_24hrMarginType"] = $_POST["_24hrMarginType"];
			
	}
	
		
//////////////////////////////////
if($_SESSION["marginType"] == 'fixed')
	{
		$marginValueNet = ($rates["primaryExchange"] - $rates["marginPercentage"]);
	}else{
		$marginValueNet = ($rates["primaryExchange"] - ($rates["primaryExchange"] * $rates["marginPercentage"]) / 100);
		}
		
		
	
		if(CONFIG_AGENT_RATE_MARGIN == '1' && $_SESSION["rateFor"] == 'agent')
		{
	
			$agentNet = $marginValueNet;
			
			
			
		
			if($_SESSION["agentMarginType"] == 'fixed')
			{
				$marginValueNet = ($marginValueNet - $_SESSION["agentMarginValue"]);
			}else{
				$marginValueNet = ($marginValueNet - ($marginValueNet * $_SESSION["agentMarginValue"]) / 100);
				}
				
		}
		if(CONFIG_24HR_BANKING == "1"){
			if($_SESSION["_24hrMarginType"] == 'lower'){
				$exRate24hr = $rates["primaryExchange"] - $rates["_24hrMarginValue"];
			}elseif($_SESSION["_24hrMarginType"] == 'upper'){
				$exRate24hr = $rates["primaryExchange"] + $rates["_24hrMarginValue"];
			}
			if($_SESSION["marginType"] == 'fixed')
			{
				$exRate24hr = ($exRate24hr - $rates["marginPercentage"]);
			}else{
				$exRate24hr = ($exRate24hr - ($exRate24hr * $rates["marginPercentage"]) / 100);
			}
		}
		
		if(!empty($_REQUEST["denominationBased"]))
			$_SESSION["denominationBased"] = $_REQUEST["denominationBased"];
		else
			$_SESSION["denominationBased"] = $rates["isDenomination"];
		
		if($_SESSION["denominationBased"] == "Y")
		{
			$arrDenominationBasedRate = selectMultiRecords("select * from denominationBasedRate where parentRateId = '".$rates["erID"]."' order by id");
			//debug($arrDenominationBasedRate);
			
			
			if(!empty($_REQUEST["amountFrom"]))
				$_SESSION["amountFrom"] = $_REQUEST["amountFrom"];
			else
				$_SESSION["amountFrom"] = $arrDenominationBasedRate[0]["amountFrom"];
			
			if(!empty($_REQUEST["amountUpto"]))
				$_SESSION["amountUpto"] = $_REQUEST["amountUpto"];
			else
				$_SESSION["amountUpto"] = $arrDenominationBasedRate[count($arrDenominationBasedRate)-1]["amountTo"];
			
			if(!empty($_REQUEST["denominationInterval"]))
				$_SESSION["denominationInterval"] = $_REQUEST["denominationInterval"];
			else
			{
				$intInterval = $arrDenominationBasedRate[0]["amountTo"]-$arrDenominationBasedRate[0]["amountFrom"] + 1;
				if($arrDenominationBasedRate[0]["amountTo"] == "0")
					$intInterval = $intInterval + 1;
				$_SESSION["denominationInterval"] = $intInterval;
			}

			for($xc=0; $xc<count($arrDenominationBasedRate); $xc++)
				$_SESSION["intervalRate".$xc] = $arrDenominationBasedRate[$xc]["exchangeRate"];
		}

?>
<html>
<head>
<title>Update Exchange Rate</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function checkForm(theForm) {
	
<? if(CONFIG_ADD_SUB_AGENT_RATE != "1"){ ?>
	if(theForm.rateFor.value == 'agent' && theForm.agentID.value == '' ){
		alert("Please select Agent.");
		theForm.agentID.focus();
		return false;
	}
<? } ?>
	
	if(document.getElementById("rateFor").value == "distributor" && theForm.agentID.options.selectedIndex == 0)
	{
		alert("Please select Distributor.");
		theForm.agentID.focus();
		return false;
	}else if(document.getElementById("rateFor").value == "transType" && theForm.agentID.options.selectedIndex == 0)
	{
		alert("Please select Transaction Type.");
		theForm.agentID.focus();
		return false;
	}else if(document.getElementById("rateFor").value == "MoneyPaid" && theForm.agentID.options.selectedIndex == 0)
	{
		alert("Please select Money Paid option from the list.");
		theForm.agentID.focus();
		return false;
	}
	
	if(theForm.primaryExchange.value == "" || IsAllSpaces(theForm.primaryExchange.value)){
    	alert("Please provide primary exchnage rate.");
        theForm.primaryExchange.focus();
        return false;
    }
  if(theForm.currency.options.selectedIndex == 0){
    	alert("Please provide Currency Name.");
        theForm.currency.focus();
        return false;
    }
	if(theForm.marginPercentage.value == "" || IsAllSpaces(theForm.marginPercentage.value)){
    	alert("Please provide margin percentage.");
        theForm.marginPercentage.focus();
        return false;
    }
	
	
	return true;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   



function calculateNet()
{
	if(document.updateRate.rateFor.value == 'intermediate')
	{ 
		calculateIntermediateRate();
	}
	var Margintype = '';
	<?
	if(CONFIG_EXCHNG_MARGIN_TYPE == '1')
	{ ?>	
		Margintype = document.updateRate.marginType.value;
<?	}?>
var MarginValue = 0;
	if(Margintype == 'fixed')
	{
		MarginValue = (document.updateRate.primaryExchange.value - document.updateRate.marginPercentage.value);
	}else{
		MarginValue = (document.updateRate.primaryExchange.value - (document.updateRate.primaryExchange.value * document.updateRate.marginPercentage.value) / 100);
		}
		
		
	<?	
		if(CONFIG_AGENT_RATE_MARGIN == '1' && $_SESSION["rateFor"] == 'agent')
		{
	?>
			document.updateRate.agentNet.value = MarginValue;
			
			agentMarginType = document.updateRate.agentMarginType.value;
			
		var MarginValue2 = 0;
			if(agentMarginType == 'fixed')
			{
				MarginValue2 = (MarginValue - document.updateRate.agentMarginValue.value);
			}else{
				MarginValue2 = (MarginValue - (MarginValue * document.updateRate.agentMarginValue.value) / 100);
				}
				
				return MarginValue2;
	<?
		}
	?>
		
 return	MarginValue;	
}

function calculate24hrExRate()
{
	var Margintype = '';
	<?
	if(CONFIG_EXCHNG_MARGIN_TYPE == '1')
	{ ?>	
		Margintype = document.updateRate.marginType.value;
	<?
	}
	?>
	var Margintype24hr = document.updateRate._24hrMarginType.value;
	var exRate = 0.0;
	var primaryEx = parseFloat(document.updateRate.primaryExchange.value);
	var margin24hr = parseFloat(document.updateRate._24hrMarginValue.value);
	
	
	if(Margintype24hr == 'lower'){
		exRate = primaryEx - margin24hr;
	}else if(Margintype24hr == 'upper'){
		exRate = primaryEx + margin24hr;
	}
	
	if(Margintype == 'fixed')
	{
		exRate = (exRate - document.updateRate.marginPercentage.value);
	}else{
		exRate = (exRate - (exRate * document.updateRate.marginPercentage.value) / 100);
		}
 return	exRate;	
}

function calculateIntermediateRate()
{
	var intermediateRate = '';

		document.updateRate.primaryExchange.value = (document.updateRate.intermediateExchangeRate.value * document.updateRate.intermediateDestExchangeRate.value);
		
	//document.addRate.primaryExchange.value = intermediateRate;
	
 //return	intermediateRate;	
}


function updateGUI()
{
	if($("#denominationBased").attr('checked'))
	{
		$("#intervalRow").show();
		$("#rangeRow").show();
		
		$("#detailRow").show();
		
		$("#normalRateRow1").hide();
		$("#normalRateRow2").hide();
		$("#normalRateRow3").hide();
		$("#normalRateRow4").hide();
		
	}
	else
	{
		$("#intervalRow").hide();
		$("#rangeRow").hide();
		
		$("#detailRow").hide();

		$("#normalRateRow1").show();
		$("#normalRateRow2").show();
		$("#normalRateRow3").show();
		$("#normalRateRow4").show();
		
		
	}
}

function checkDenomiationRates()
{
	var totalElements = Number(document.getElementById("totalInterval").value);
	for(var j=0; j<totalElements; j++)
	{
		if(document.getElementById("intervalRate"+j).value == "")
		{
			alert("Please provide denomination based exchange rate.");
			document.getElementById("intervalRate"+j).focus();
			return false;
		}
	}
	return true
}


// end of javascript -->
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body onLoad="updateGUI();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update Exchange 
      Rate</font></strong></td>
  </tr>
  <form action="add-rate-conf.php?newOffset=<?=$offset;?>" method="post" onSubmit="return checkForm(this);" name="updateRate">
  <input type="hidden" name="erID" value="<?=$rates["erID"]; ?>">
  <tr>
      <td align="center"> 
        <table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update 
                    Exchange Rate</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		<tr bgcolor="#ededed"><td colspan="2" bgcolor="#FFFFCC"><table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2"><a class="style2" href="rate-list.php?newOffset=<?=$offset;?>">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <!--tr bgcolor="#ededed">
            <td colspan="2"><font color="#005b90"><strong>All exchange rates are from GBP to other currencies </strong></font></td>
          </tr-->
        <tr bgcolor="#ededed">
            <td width="212"><font color="#005b90"><strong>Date</strong></font></td>
            <td width="183"><input type="hidden" name="dDate" value="<?=$rates["dated"] ?>"><?=dateFormat($rates["dated"],"2") ?></td>
        </tr>
<?
	if(CONFIG_AGENT_OWN_RATE == '1' && ($agentType == 'SUPA' || $agentType == 'SUPAI'))
	{
		
		
		if(CONFIG_ADD_SUB_AGENT_RATE == "1" && $agentType == 'SUPA'){
			
			$agentNameQuery = "select userID,name,username from admin where userID = '".$rates["rateValue"]."'";
			$agentName = selectFrom($agentNameQuery);
	?>
	<tr bgcolor="#ededed">
            <td width="212"><font color="#005b90"><strong>Agent</strong></font></td>
            <td width="183"><input type="hidden" name="agentID" value="<?=$rates["rateValue"]?>"><? echo $agentName["name"]."[".$agentName["username"]."]"?></td>
        </tr>
	<input type="hidden" name="rateFor" value="agent">	
	 
	
	
	<? }else{ ?>
	 <input type="hidden" name="rateFor" value="agent">	
	 <input type="hidden" name="agentID" value="<?=$parentID?>">	
	 
	 <?
	}
	}else{
	 ?>	       
                <tr bgcolor="#ededed"> 
	      	<td><font color="#005b90"><strong>Exchange Rate Based on</strong></font></td>
	         <td>
	         	<?
            if((CONFIG_EXCHNG_TRANS_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr") || CONFIG_NOT_EDIT_MARGIN == '1')
            {
            		echo "24 hr Banking";
            		echo '<input type="hidden" name="rateFor" value="bank24hr" />';
          	}
          	else
          	{
            ?>
						<select name="rateFor" id="rateFor" style="font-family:verdana; font-size: 11px" onChange="document.forms.updateRate.action='update-rate.php'; document.forms.updateRate.submit();">
		            <option value="">- Select One -</option>
		            <option value="generic">- Generic Rate -</option>
		            <option value="distributor">- Distributor -</option>
		             <?
		            if(CONFIG_EXCHNG_TRANS_TYPE == '1')
		            {
		            ?>
		            <option value="transType">- Transaction Type -</option>
		            <?
		        	  }elseif(CONFIG_EXCHANGE_MONEY_PAID == '1'){
		            ?>
		            <option value="MoneyPaid">- Money Paid -</option>
		            <?}?>
		            <?
		            if(CONFIG_AGENT_OWN_RATE == '1'){
		            ?>
		            	<option value="agent">- Agent -</option>
		            <?
		          	}
		           
		             if(CONFIG_SETTLEMENT_COMMISSION == '1')
		           {          
		           ?>
		           			<option value="intermediate">- Intermediate -</option>
		           
		           <? 
				   } 
				   	if(CONFIG_CUSTOMER_CATEGORY == '1'){
		           ?>
		         	<option value="customer">- Customer Category -</option>
				  <? } ?>
		            <!--option value="money">- Money Range -</option-->
		         </select>
		         <script language="JavaScript">SelectOption(document.forms.updateRate.rateFor,"<?=$_SESSION["rateFor"]; ?>");</script>
		         <?
		        	}
		         ?>
				  <? if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1") { ?>
				 &nbsp;&nbsp;&nbsp;&nbsp;
				 <input type="checkbox" name="denominationBased" id="denominationBased" value="Y" <?=$_SESSION["denominationBased"] == "Y"?'checked="checked"':''?> onChange="document.forms.updateRate.action='update-rate.php'; document.forms.updateRate.submit();updateGUI();" />
				 <b>Denomination Based Exchange Rate</b>
				 <? } ?>
	         </td>
	       </tr> 
	        <tr bgcolor="#ededed" id="rangeRow" style="display:none">
  				<td valign="top"><font color="#005b90"><strong>Amount Range<font color="#ff0000">*</font></strong></font></td>
       			<td>
					<b>From:</b>&nbsp;<input type="text" name="amountFrom" value="<?=$_SESSION["amountFrom"]?>" readonly="readonly" />
					<br />
					<b>Upto: </b>&nbsp;<input type="text" name="amountUpto" value="<?=$_SESSION["amountUpto"]?>" readonly="readonly" />
				</td>
			</tr>
			<tr bgcolor="#ededed" id="intervalRow" style="display:none">
  				<td><font color="#005b90"><strong>Denomination Interval<font color="#ff0000">*</font></strong></font></td>
       			<td>
					<input type="text" name="denominationInterval" value="<?=$_SESSION["denominationInterval"]?>" readonly="readonly" />
				</td>
			</tr>
	        
	       <? if($_SESSION["rateFor"] == "distributor" || $_SESSION["rateFor"] == "intermediate")
	       		{
	       ?>  
	       
	       			<tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Select Distributor<font color="#ff0000">*</font></strong></font></td>
		       			<td>
		       				<select name="agentID" style="font-family:verdana; font-size: 11px">
									<option value="">- Select One -</option>
									<?
											if(CONFIG_SUB_DISTRIBUTOR_AT_EXCHANGE_RATE == 1)
												echo "<optgroup label='Super Distributors'>";
											
											$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
											for ($i=0; $i < count($agents); $i++){
												if($rates["rateValue"] == $agents[$i]["userID"])
												{
										?>
												<option value="<?=$agents[$i]["userID"]; ?>" selected><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
										<?
												}else{
										?>
									<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
									<?
										}
											}
											
											if(CONFIG_SUB_DISTRIBUTOR_AT_EXCHANGE_RATE == 1)
											{
												
												echo "</optgroup><optgroup label='Sub Distributors'>";
												
												$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Sub' and isCorrespondent != 'N' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
												for ($i=0; $i < count($agents); $i++)
												{
													
													if($rates["rateValue"] == $agents[$i]["userID"])
													{
										?>
														<option value="<?=$agents[$i]["userID"]; ?>" selected><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
										<?
													}
													else
													{
										?>
														<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
										<?
													}
												
												}
												echo "</optgroup>";
											}
											
											
											
										?>
								  </select>
		       			</td>
	       			</tr>
	       
	       <?
	      		}elseif($_SESSION["rateFor"] == "money")
	      		{
	      			$rateValues = explode("," , $rates["rateValue"]);
	       ?>
	       <tr bgcolor="#ededed">
	       				<td width="185"><font color="#005b90"><strong>Money Range From </strong></font></td>
	       				<td><input type="text" name="from" value="<?=stripslashes($rateValues[0]); ?>" size="15" maxlength="32"></td>
	       	</tr>
	       	<tr bgcolor="#ededed">
	       				<td width="185"><font color="#005b90"><strong>Money Range To </strong></font></td>
								<td><input type="text" name="to" value="<?=stripslashes($rateValues[1]); ?>" size="15" maxlength="32"></td>	      			
					</tr>
	       <?	
	       }elseif($_SESSION["rateFor"] == "transType" && CONFIG_EXCHNG_TRANS_TYPE == '1')
	       		{
	       			////////////////  Here agentID is to represent Transaction Type or Distributor ID  //////////////////
	       ?>  
	       
	       			<tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Select Transaction Type<font color="#ff0000">*</font></strong></font></td>
		       			<td>
		       				<select name="agentID" style="font-family:verdana; font-size: 11px">
									<option value="">- Select One -</option>
									<option value="Pick up" <? if($rates["rateValue"] == "Pick up"){echo("selected");} ?>>- Pick up -</option>
									<option value="Bank Transfer" <? if($rates["rateValue"] == "Bank Transfer"){echo("selected");} ?>>- Bank Transfer -</option>
									<option value="Home Delivery" <? if($rates["rateValue"] == "Home Delivery"){echo("selected");} ?>>- Home Delivery -</option>
							</select>
		       			</td>
	       			</tr>
	       
	        <?
	      		}elseif($_SESSION["rateFor"] == "MoneyPaid" ){
	       ?>
				
				<tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Select Money Paid<font color="#ff0000">*</font></strong></font></td>
		       			<td>
		       				<select name="agentID" style="font-family:verdana; font-size: 11px">
									<option value="">- Select One -</option>
									<option value="By cash" <? if($rates["rateValue"] == "By cash"){echo("selected");} ?>>- By cash -</option>
									<option value="By Bank Transfer" <? if($rates["rateValue"] == "By Bank Transfer"){echo("selected");} ?>>- By Bank Transfer -</option>
									
							</select>
		       			</td>
	       			</tr>
	       		<? }elseif($_SESSION["rateFor"] == "agent")
	       		{
	       ?>  
	       
	       			<tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Select Agent<font color="#ff0000">*</font></strong></font></td>
		       			<td>
		       				<select name="agentID" style="font-family:verdana; font-size: 11px">
										<option value="">- Select One -</option>
										<?
										if(CONFIG_SUB_AGENT_AT_EXCHANGE_RATE == 1)
										{
											echo "<optgroup label='Super Agents'>";
										}
										$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'Only' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
										for ($i=0; $i < count($agents); $i++){
											if($rates["rateValue"] == $agents[$i]["userID"])
											{
										?>
												<option value="<?=$agents[$i]["userID"]; ?>" selected><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
										<?
												}else{
										?>
									<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
									<?
										}
											}
										if(CONFIG_SUB_AGENT_AT_EXCHANGE_RATE == 1)
										{
											echo "</optgroup><optgroup label='Sub Agents'>";
											
											$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Sub' and isCorrespondent != 'Only' and (agentStatus='Active' or agentStatus='New') order by agentCompany");
											for ($i=0; $i < count($agents); $i++){
												if($rates["rateValue"] == $agents[$i]["userID"])
												{
												?>
													<option value="<?=$agents[$i]["userID"]; ?>" selected><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
												<?
												}else{
												?>
													<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["name"]." [".$agents[$i]["username"]."]"); ?></option>
												<?
												}
											}
										}
										?>
										
										
								  </select>
		       			</td>
	       			</tr>
	       
	       <?
	      	}elseif($_SESSION["rateFor"] == "customer")
			 {
			?>
			<tr bgcolor="#ededed">
	       		<td><font color="#005b90"><strong>Select Customer Category<font color="#ff0000">*</font></strong></font></td>
		    	<td>
			 <select name="agentID" id="agentID" style="font-family:verdana; font-size: 11px">
		 	<option value="">-Select One-</option>
		  <?
			$custCatSql = "select id,name from ".TBL_CUSTOMER_CATEGORY." where enabled = 'Y'";
			$category = selectMultiRecords($custCatSql);
			for($k =0; $k <count($category); $k++){
		  ?>
		  <option value="<?=$category[$k]["id"]; ?>" <? echo ($rates["rateValue"]== $category[$k]["id"] ? "selected" : "")?>>
				   <?  echo($category[$k]["name"]); ?></option>	
		<? } ?>		   
		</select>
		
			</td>
	    </tr>		   	
		<?  
		 }
		 ?> 
			
        
<?
}
?>
        
        <tr bgcolor="#ededed">
            <td width="212"><font color="#005b90"><strong>Originating Country</strong></font></td>
            <td width="183"><input type="hidden" name="countryOrigin" value="<?=$rates["countryOrigin"] ?>"><?=$rates["countryOrigin"] ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Originating Currency<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
            		if(CONFIG_EXCHNG_MARGIN_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr")
            		{
            			$strQuery = "SELECT DISTINCT(currencyName), description, country  FROM  currencies where country = '".$rates["countryOrigin"]."'";
									$countryCaption = selectFrom($strQuery);
									echo $countryCaption["currencyName"]." --> ".$countryCaption["description"];
									/**
									 * Displays as hidden value
									 */		
									echo "<input type='hidden' name='currencyOrigin' value='".$countryCaption["currencyName"]."' />"; 
								}
								else
								{
            	?>
            	<select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?
						 	/* #5955-premier exchange by Niaz Ahmad*/
							if(CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES == '1'){
							$strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies
							WHERE currency_for = 'S' OR currency_for = 'B' ORDER BY currencyName";
							}else{									  					  
							$strQuery = "SELECT DISTINCT(currencyName), description, country  FROM  currencies ORDER BY currencyName ";
							}
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($rates["countryOrigin"] == $rstRow["country"] && $rates["currencyOrigin"] == "")
									echo "<option selected value ='$currency'> ".$currency." --> ".$description."</option>";			
								else
								  echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.forms.updateRate.currencyOrigin, "<?=$_SESSION["currencyOrigin"] ?>");
     	  </script>
     	  <?
     	  }
     	  ?>
     	  
            	<? if($_SESSION["rateFor"] == "intermediate" || $rates["rateFor"]== "intermediate"){ ?>
            	<tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Intermediate Currency<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<select name="intermediateCurrency" onChange="document.forms.updateRate.action='update-rate.php'; document.forms.updateRate.submit();">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];				
								
								if($rates["countryOrigin"] == $rstRow["country"] && $rates["intermediateCurrency"] == "")
									echo "<option selected value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.updateRate.intermediateCurrency, "<?=$_SESSION["intermediateCurrency"] ?>");
     	  </script>
            	
            	<tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Primary Intermediate Exchange Rate<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="intermediateExchangeRate" value="<?=stripslashes($_SESSION["intermediatePrimRate"]); ?>" size="15" maxlength="32" onBlur="javascript:document.updateRate.net.value=calculateNet();"></td>
        </tr>
        
            	
            	<? } ?>
     	  
            	<!--input type="text" name="currency" size="15" maxlength="255" --></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="212"><font color="#005b90"><strong>Destination Country</strong></font></td>
            <td width="183"><input type="hidden" name="country" value="<?=$rates["country"] ?>"><?=$rates["country"] ?></td>
        </tr>
        
      
        
        
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Currency<font color="#ff0000">*</font></strong></font></td>
          <td>
          	<?			
          		if(CONFIG_EXCHNG_MARGIN_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr")
          		{
          			
														  
					$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies where currencyName = '".$rates["currency"]."'";
					
          			$curencyCaption = selectFrom($strQuery);
          			echo $curencyCaption["currencyName"]." --> ".$curencyCaption["description"];
								/**
								 * Displays as hidden value
								 */		
								echo "<input type='hidden' name='currency' value='".$curencyCaption["currencyName"]."' />"; 
          		}
          		else
          		{
          	?>
          	<select name="currency">
				<option value="">-- Select Currency --</option>
						  <?
						  /* #5955-premier exchange by Niaz Ahmad*/
							if(CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES == '1'){
					$strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies
					WHERE currency_for = 'R' OR currency_for = 'B' ORDER BY currencyName";
					}else{
							$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName";
							}
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
							
							while($rstRow = mysql_fetch_array($nResult))
							{
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($rates["currency"] == $currency)
									echo "<option value ='$currency' selected > ".$currency." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<?
						}
				?>
				 <!--script language="JavaScript">
     				SelectOption(document.forms.updateRate.currency, "<?=$_SESSION["currency"] ?>");
      	 </script-->
          	<!--input type="text" name="currency" value="<?=stripslashes($rates["currency"]); ?>" size="15" maxlength="255"--></td>
     
        </tr>
        <tr bgcolor="#ededed" id="normalRateRow1">
            <td width="212"><font color="#005b90"><strong><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_PIMARY_RATE);}else{echo("Primary Exchange");} ?></strong></font></td>
            <td><input type="text" name="primaryExchange" value="<?=stripslashes($rates["primaryExchange"]); ?>" size="15" maxlength="32" onBlur="javascript:document.updateRate.net.value=calculateNet();" <? if((CONFIG_EXCHNG_TRANS_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr") || CONFIG_NOT_EDIT_MARGIN == '1') echo "readonly"; ?>></td>
        </tr>
        
         <?
        if(CONFIG_EXCHNG_MARGIN_TYPE == '1'){
        ?>
        <tr bgcolor="#ededed" id="normalRateRow2">
            <td width="185"><font color="#005b90"><strong>Margin Type<font color="#ff0000">*</font></strong></font></td>
            <td>
            <?
            	if((CONFIG_EXCHNG_TRANS_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr") || CONFIG_NOT_EDIT_MARGIN == '1')
            	{
            		if($rates["marginType"] == "percent")
            			echo "Percentage Margin";
            		elseif($rates["marginType"] == "fixed")
            			echo "Fixed Margin";
            		echo '<input type="hidden" name="marginType" value="'.$rates["marginType"].'" />';
            	}
            	else
            	{
            ?>
            	<select name="marginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.updateRate.net.value=calculateNet();" readonly="readonly">
		             <option value="percent">- Percentage Margin -</option>
		             <option value="fixed">- Fixed Margin -</option>
		         </select>
		         <script language="JavaScript">SelectOption(document.forms.updateRate.marginType,"<?=$rates["marginType"]; ?>");</script>
	        	<?
		        	}
	        	?>
            </td>
        </tr>
        <?
    	}
        ?>
        <tr bgcolor="#ededed" id="normalRateRow3">
            <td width="212"><font color="#005b90"><strong>Margin <? if(CONFIG_EXCHNG_MARGIN_TYPE != '1'){echo("Percentage");}?><font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="marginPercentage" value="<?=stripslashes($rates["marginPercentage"]); ?>" size="15" maxlength="32" onBlur="javascript:document.updateRate.net.value=calculateNet();" <? if((CONFIG_EXCHNG_TRANS_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr") || CONFIG_NOT_EDIT_MARGIN == '1') echo "readonly";  ?>></td>
        </tr>
        <?
        if(CONFIG_AGENT_RATE_MARGIN == '1' && $_SESSION["rateFor"] == "agent")
        {
        ?>
         <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Exchange Rate </strong></font>(Before Agent Margin)</td>
          <td><input name="agentNet" type="text" id="agentNet" value="<?=$agentNet?>" size="15" maxlength="32" readonly></td>
        </tr>
        	<tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Agent Margin Type<font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
            	if((CONFIG_EXCHNG_TRANS_TYPE == '1' && $_SESSION["rateFor"] == "bank24hr") || CONFIG_NOT_EDIT_MARGIN == '1')
            	{
            		if($rates["marginType"] == "percent")
            			echo "Percentage Margin";
            		elseif($rates["marginType"] == "fixed")
            			echo "Fixed Margin";
            		echo '<input type="hidden" name="marginType" value="'.$rates["marginType"].'" />';
            	}
            	else
            	{
            	?>
	            	<select name="agentMarginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.updateRate.net.value=calculateNet();" >
			             <option value="percent">- Percentage Margin -</option>
			             <option value="fixed">- Fixed Margin -</option>
			         </select>
			         <script language="JavaScript">SelectOption(document.forms.updateRate.agentMarginType,"<?=$_SESSION["agentMarginType"]; ?>");</script>
			        <?
			        }
			        ?>
            </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Agent Margin value</strong></font></td>
            <td><input type="text" name="agentMarginValue" value="<?=stripslashes($_SESSION["agentMarginValue"]); ?>" size="15" maxlength="32" <? if(CONFIG_NOT_EDIT_MARGIN == '1'){echo("readonly");}?> onBlur="javascript:document.updateRate.net.value=calculateNet();"></td>
        </tr>
        <?
      	}
        ?>
        <tr bgcolor="#ededed" id="normalRateRow4">
          <td><font color="#005b90"><strong><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_NET_RATE);}else{echo("Net Exchange");} ?></strong></font></td>
          <td><input name="net" type="text" id="net" value="<?=$marginValueNet?>" size="15" maxlength="32" readonly></td>
        </tr>
        <? 
       	 if(CONFIG_SHARE_OTHER_NETWORK == '1')
	       { 
	       	$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
	       	?>
	       	 	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Available to</strong></font>
					</td>
					<td> 
						<select name="remoteAvailable" id="remoteAvailable">
							<option value=""> Select One </option>
							<?
							for($cl = 0; $cl < count($jointClients); $cl++)
							{
							?>
							
								 <OPTION value="<?=$jointClients[$cl]["clientId"]; ?>" <? echo (strstr($_SESSION["remoteAvailable"],$jointClients[$cl]["clientId"]) ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
							<?
							}
							?>
	            			
					</td>
				</tr>
	       
	       	<?
	       }
	       ?>
        
       <input name="relatedRateId" type="hidden" id="relatedRateId" value="<?=$rates["relatedID"]?>">
       
       <? if($_SESSION["rateFor"] == "bank24hr"){ ?>
		    	<tr bgcolor="#ededed">
		        <td><font color="#005b90"><strong>24 hr Banking Margin<font color="#ff0000">*</font></strong></font></td>
		        <td>
		        	<input name="_24hrMarginValue" type="text" id="_24hrMarginValue" value="<?=$_SESSION["_24hrMarginValue"]?>" size="15" onBlur="javascript:document.updateRate._24hrExRate.value=calculate24hrExRate();">&nbsp;
		       	 	<select name="_24hrMarginType" style="font-family:verdana; font-size: 11px" onChange="javascript:document.updateRate._24hrExRate.value=calculate24hrExRate();">
		       	 		<option value="">-Select margin type-</option>
		       	 		<option value="lower">Lower</option>
		       	 		<option value="upper">Upper</option>
		       	 	</select><font color="#ff0000">*</font>
		       	 	<script language="JavaScript">
		       	 		SelectOption(document.forms.updateRate._24hrMarginType,"<?=$_SESSION["_24hrMarginType"]; ?>");
		       	 	</script>
		       </td>
		      </tr>
		      <tr bgcolor="#ededed">
		        <td><font color="#005b90"><strong>24 hr Banking Exchange Rate</strong></font></td>
		        <td>
        			<input name="_24hrExRate" type="text" id="_24hrExRate" value="<?=$exRate24hr?>" size="15" maxlength="32" readonly>
       			</td>
      		</tr>
		  <? } ?>
       
		<? 
		if($_SESSION["denominationBased"] == "Y" && $_SESSION["amountFrom"] != "" && !empty($_SESSION["amountUpto"]) && !empty($_SESSION["denominationInterval"])) 
		{ 
			$intTotalIntervals = ceil(($_SESSION["amountUpto"] - $_SESSION["amountFrom"])/$_SESSION["denominationInterval"]);
		?>
			<tr bgcolor="#ededed" id="detailRow">
				<td align="left" colspan="2">
					<input type="hidden" name="totalInterval" id="totalInterval" value="<?=$intTotalIntervals?>" />
					<fieldset>
						<legend>Denomination Based Exchange Rate</legend>
						<table align="center" width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<th width="30%">From</th>
								<th width="30%">To</th>
								<th width="40%">Exchange Rate</th>
							</tr>
							
							<? for($in = 0; $in < $intTotalIntervals; $in++) 
							   { 
							   		$upToAmountVal = ($_SESSION["denominationInterval"]*$in)+$_SESSION["denominationInterval"]+($_SESSION["amountFrom"]);
									if($upToAmountVal > $_SESSION["amountUpto"])
										$upToAmountVal = $_SESSION["amountUpto"];
							?>
								<tr bgcolor="#ededed">
									<td align="center"><?=$_SESSION["amountFrom"]+($_SESSION["denominationInterval"]*$in)?></td>

									<td align="center"><?=$upToAmountVal?></td>
									<td align="center">
										<input type="text" name="intervalRate<?=$in?>" id="intervalRate<?=$in?>" value="<?=$_SESSION["intervalRate".$in]?>" style="text-align:right" />
									</td>
								</tr>
							<? } ?>
						</table>
					</fieldset>		
	
				</td>
			</tr>
	
		<? } ?>

		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>