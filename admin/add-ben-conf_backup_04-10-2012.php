<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
$tran_date = date("Y-m-d") ;
$tran_dateTime = getCountryTime(CONFIG_COUNTRY_CODE);
$modifyby = $_SESSION["loggedUserData"]["userID"];


if($_GET["from"] != "")
{
	$from = $_GET["from"];
}
if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
{	
	$ttTransFlag=true;
}

switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break; 
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}
 
 /*
* Added by Niaz Ahmad
* @4997
* In case of companies it will show just one field instead of first/mid and last name
* Dated: 01-06-2009
*/  
 $beneficiaryFullnameFlag = false;
	if(defined("CONFIG_BENEFICIARY_FULLNAME_FOR_COMPANIES") && CONFIG_BENEFICIARY_FULLNAME_FOR_COMPANIES=="1"){
	$beneficiaryFullnameFlag = true;
}	

$_SESSION["customerID"] = $_POST["customerID"];

//if ($_POST["benID"] == ""){
	session_register("Title");
	session_register("other_title");
	session_register("firstName");
	session_register("S/O");
	session_register("sonOfType");
	session_register("lastName");
	session_register("middleName");
	session_register("IDType");
	session_register("IDNumber");
	session_register("Otherid_name");
	session_register("Otherid");
	session_register("Address");
	session_register("Address1");
	session_register("Country");
	session_register("City");
	session_register("Zip");
	session_register("State");
	session_register("Phone");
	session_register("Mobile");
	session_register("benEmail");
	session_register("userType");
	
	/**
   * If the beneficiary is with bank details
   * #Ticket# 3321
   */
	if(CONFIG_BEN_BANK_DETAILS == "1")
	{
		session_register("bankName1");
		session_register("account1");
		session_register("branchName1");
		session_register("branchAddress1");
		session_register("iban1");
		session_register("swiftCode1");
		session_register("accountType1");
	}
	/* End #3321 */
	
	$_SESSION["Title"] = $_POST["Title"];
	$_SESSION["other_title"] = $_POST["other_title"];
	$_SESSION["firstName"] = $_POST["firstName"];
	$_SESSION["lastName"] = $_POST["lastName"];
	$_SESSION["S/O"] = $_POST["S/O"];
	$_SESSION["sonOfType"] = $_POST["sonOfType"];
	$_SESSION["middleName"] = $_POST["middleName"];
	$_SESSION["secMiddleName"] = $_POST["secMiddleName"];
	$_SESSION["dobDay"] = $_POST["dobDay"];
	$_SESSION["dobMonth"] = $_POST["dobMonth"];
	$_SESSION["dobYear"] = $_POST["dobYear"];
	$_SESSION["IDType"] = $_POST["IDType"];
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
	$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
	$_SESSION["Otherid"] = $_POST["Otherid"];
	$_SESSION["Address"] = $_POST["Address"];
	$_SESSION["Address1"] = $_POST["Address1"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["Zip"] = $_POST["Zip"];
	$_SESSION["CPF"] = $_POST["CPF"];
	$_SESSION["State"] = $_POST["State"];
	$_SESSION["Phone"] = $_POST["Phone"];
	$_SESSION["Mobile"] = $_POST["Mobile"];
	$_SESSION["benEmail"] = $_POST["benEmail"];
	$_SESSION["customerID"] = $_POST["customerID"];
  $_SESSION["Citizenship"] = $_POST["Citizenship"];	
  $_SESSION["IDissuedate"] = $_POST["IDissuedate"];
  $_SESSION["issuedBy"] = $_POST["issuedBy"];
  $_SESSION["IDexpirydate"] = $_POST["IDexpirydate"];

	/**
   * If the beneficiary is with bank details
   * @Ticket# 3321
   */
  
  $_SESSION["userType"] = $_POST["userType"];
  
  if(CONFIG_BEN_BANK_DETAILS == "1")
  {
		$_SESSION["usedBankId"] = $_POST["usedBankId"];
		 $_SESSION["bankName1"] = $_POST["bankName"];
		$_SESSION["account1"] = $_POST["account"];
		$_SESSION["branchCode1"] = $_POST["branchName"];
		$_SESSION["branchAddress1"] = $_POST["branchAddress"];
		$_SESSION["iban1"] = $_POST["iban"];
		$_SESSION["swiftCode1"] = $_POST["swiftCode"];
		$_SESSION["accountType1"] = $_POST["accountType"];
		$_SESSION["country1"] = $_POST["country1"];
	}
	/* End #3321 */

	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	
	if ( is_array( $_REQUEST["IDTypesValuesData"] ) )
	{
		$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
	}
	
	// End of code against #3299: MasterPayex - Multiple ID Types

   $beneficiaryName = $_POST["firstName"] . " " . $_POST["middleName"] . " " . $_POST["lastName"];

if($beneficiaryFullnameFlag){
	 if(isset($_POST["compnayFullName"]))
		 $beneficiaryName = $_POST["compnayFullName"];
}

	/*$field = "benificiaryName,Mobile,IDNumber";
	$values = $beneficiaryName.",".$_POST["Mobile"].",".$_POST["IDNumber"];
	//$values = $customerName;
	$tble = "customer";
	$var = checkExistence($field,$values,$tble);*/
    
    
    
	
//}
if ($from==""){
	$backURL = "add-beneficiary.php?notEndSessions=Y&benID=".$_POST["benID"]."&msg=Y";
	}
	else
	{
	$backURL = "add-beneficiary.php?notEndSessions=Y&from=".$from."&benID=".$_POST["benID"]."&msg=Y";
	}

/*if($var != ""){  //checks whether the customer already exists or not
			insertError(BE14." ".$var." ".BE15);
			
			redirect($backURL);
		
		}*/
		
		
if (trim($_POST["agentID"]) == ""){
	$agentIDs=$_GET["agentId2"];
	/*insertError(CU1);
	redirect($backURL);*/
}elseif(trim($_POST["agentID"])!="")
{
	$agentIDs = $_POST["agentID"];
	}
	
if (trim($_POST["Country"]) == ""){
	insertError(BE5);
	redirect($backURL);
}
if (trim($_POST["firstName"]) == ""){
	insertError(BE1);
	redirect($backURL);
}
if (trim($_POST["lastName"]) == ""){
	insertError(BE2);
	redirect($backURL);
}


if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) {
	if (trim($_POST["Address"]) == ""){
		
		insertError("Please provide the customer's address line 1.");
		redirect($backURL);
	}
	if (trim($_POST["Zip"]) == "" && CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS != 1){
		
		insertError("Please provide the customer's Zip/Postal Code.");
		redirect($backURL);
	}
	
	
	
	
}
if (trim($_POST["IDType"]) == "" && CONFIG_ID_MANDATORY == '1') {		
		
		insertError(BE13);
		redirect($backURL);
	}

if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["IDexpirydate"] != "") {
			$dyear = substr($_POST["IDexpirydate"],6,4);
		//$dDate = explode("-", $_POST["IDexpirydate"]);
		
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["IDexpirydate"])) {
			if ($dyear <= $maxFutureYear) {
				$idExpiryDate = substr($_POST["IDexpirydate"],6,4) . "-" . substr($_POST["IDexpirydate"],3,2)	. "-" . substr($_POST["IDexpirydate"],0,2);
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";	
	}
} else {
	if ($_POST["IDexpirydate"] != "") {
	//	$dDate = explode("/", $_POST["IDexpirydate"]);
		$idExpiryDate = substr($_POST["IDexpirydate"],6,4) . "-" . substr($_POST["IDexpirydate"],3,2)	. "-" . substr($_POST["IDexpirydate"],0,2);
		
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(BE12);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";		
	}
}

if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxPastYear = date("Y") - CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["IDissuedate"] != "") {
			$dyear = substr($_POST["IDissuedate"],6,4);
		//$dDate = explode("-", $_POST["IDissuedate"]);
		
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["IDissuedate"])) {
			
				$idIssueDate = substr($_POST["IDissuedate"],6,4) . "-" . substr($_POST["IDissuedate"],3,2)	. "-" . substr($_POST["IDissuedate"],0,2);
				if ($idIssueDate > date("Y-m-d")) {
					insertError(AG55);
					redirect($backURL);
				
			} 
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$$idIssueDate = "";	
	}
	if ($_POST["IDexpirydate"] != "") {
			$dyear = substr($_POST["IDexpirydate"],6,4);
		//$dDate = explode("-", $_POST["IDexpirydate"]);
		
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($_POST["IDexpirydate"])) {
			if ($dyear >= $maxPastYear) {
				$idExpiryDate = substr($_POST["IDexpirydate"],6,4) . "-" . substr($_POST["IDexpirydate"],3,2)	. "-" . substr($_POST["IDexpirydate"],0,2);
				if ($idExpiryDate < date("Y-m-d")) {
					insertError(BE12);
					redirect($backURL);
				}
			} else {
				insertError("ID issue year should be greater than " . $maxPastYear . ". If not, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Invalid Date Format");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";	
	}
} else {
	if ($_POST["IDissuedate"] != "") {
	//	$dDate = explode("/", $_POST["IDexpirydate"]);
		$idIssueDate = substr($_POST["IDissuedate"],6,4) . "-" . substr($_POST["IDissuedate"],3,2)	. "-" . substr($_POST["IDissuedate"],0,2);
		if ($idIssueDate > date("Y-m-d")) {
			insertError(BE16);
			redirect($backURL);
		}
	} else {
		$idIssueDate = "";		
	}
}


if (CONFIG_CPF_ENABLED == "1" && $_POST["Country"] == CONFIG_CPF_COUNTRY)
{
	/**
	 * Updated for the CPF & CNPJ number
	 * @Ticket# 3315
	 */
	require_once ("lib/classValidateCpfCpnj.php");
	$validate = new VALIDATE; 
	
	if($_POST["cpfcpnj"] == "CPF")
	{
		if(!$validate->cpf($_POST["CPF"]) && $_POST["CPF"] != "00000000000")
		{
			insertError(BE11);	
			if ($_POST["benID"] == "")
			{
				redirect($backURL);
			}
			
			else
			
			{
				redirect("add-beneficiary.php?notEndSessions=Y&benID=$_POST[benID]&msg=Y&from=".$from."&transID=".$_GET["transID"]);	
			}
		}
	}
	elseif($_POST["cpfcpnj"] == "CPNJ")
	{
		if(!$validate->cnpj($_POST["CPF"]) && $_POST["CPF"] != "00000000000000")
		{
			insertError("This is not a valid CNPJ Number");	
			if ($_POST["benID"] == "")
			{
				redirect($backURL);
			}
			else
			{
				redirect("add-beneficiary.php?notEndSessions=Y&benID=$_POST[benID]&msg=Y&from=".$from."&transID=".$_GET["transID"]);	
			}
		}
	}
}

if(CONFIG_RADIOIDTYPE_IDNUMBER_CHECK == '1')
{
	if(trim($_POST["IDType"] != "") && trim($_POST["IDNumber"] == "")){
		insertError(BE3);
		redirect($backURL);
	}elseif(trim($_POST["IDType"] == "") && trim($_POST["IDNumber"] != "")){
		insertError(BE13);
		redirect($backURL);
	}

}

if (trim($_POST["City"]) == "" && CONFIG_CITY_NON_COMPULSORY != '1'){
	insertError(BE6);
	redirect($backURL);
}
if (trim($_POST["Phone"]) == "" && CONFIG_PHONE_NON_COMPULSORY != '1'){
	insertError(BE7);
	redirect($backURL);
}

if (trim($_POST["Mobile"]) == "" && CONFIG_MOBILE_MANDATORY == '1'){
	insertError(BE17);
	redirect($backURL);
}

/**
 * If bank details with beneficiries
 * @Ticket# 3589
 */
if(CONFIG_BEN_BANK_DETAILS == "1")
{
	/**
	 * If no bank selected from the bank search
	 * than check if the provided bank id already exists in bank table
	 */
	if(empty($_POST["usedBankId"]) && !empty($_POST["bankId"]))
	{
		$checkBankId = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$_POST["bankId"]."' ");
			
		if(!empty($checkBankId["bankId"]))
		{
			insertError(B1);
			redirect($backURL);
		}
	}
}
/* End #3589 */


if($_POST["submit"] == "Submit"){

$arrListId = explode(",",$_POST["matchListID"]);

if($_POST["chkBlock"]!= ''){
	   $chk= $_POST["chkBlock"];
	
	  if(count($arrListId) > 0){
		  for($k=0;$k<count($arrListId);$k++)
		  {
		 	if(!empty($arrListId[$k])){
			$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  isBlocked = '".$_POST["chkBlock"]."',
							  remarks = '".$_POST["remarks"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";
		
			update($Querry_Sqls);
		  }
		}
  }			   
    $compFlag =false; 
	 insertError($_POST["message"]);
		$backURL = "add-beneficiary.php?notEndSessions=Y&from=".$from."&benID=".$_POST["benID"]."&msg=Y";
		 redirect($backURL); 
	    
	}else{
		$chk= 'N';
		
	  if(count($arrListId) > 0){
		  for($k=0;$k<count($arrListId);$k++)
		  {
		 	if(!empty($arrListId[$k])){
			$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  isBlocked = '".$chk."',
							  remarks = '".$_POST["remarks"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";
		
			update($Querry_Sqls);
		  }
		}
  }			   
		 
	   $compFlag =true; 
       insertInto($Querry_Sqls);
   
		}
	}


   if($_GET["cmpCheck"] != 'Y')
	{
	 
	   $compFlag = true;
		// compliance logic (Niaz)
	   $queryExact="select userType,listID,isEnable,applyAt,matchCriteria,autoBlock,message from ".TBL_COMPLIANCE_LOGIC." where userType ='beneficiary' and isEnable ='Y' and applyAt='User Creation' and matchCriteria = 'Exact Match' ";
		 $compExactMatch=selectMultiRecords($queryExact);

		  $queryPartial="select userType,listID,isEnable,applyAt,matchCriteria,autoBlock,message from ".TBL_COMPLIANCE_LOGIC." where userType ='beneficiary' and isEnable ='Y' and applyAt='User Creation' and matchCriteria = 'Partial Match' ";
			$compPartialMatch=selectMultiRecords($queryPartial);
		    
			$firstName = trim($_POST["firstName"]);
			$middleName = trim($_POST["middleName"]);
			$lastName = trim($_POST["lastName"]);
		
		     $firstName = str_replace("'", "", $firstName);
			 $middleName = str_replace("'", "", $middleName);
			 $lastName = str_replace("'", "", $lastName);
			 
			 $firstName = stripslashes($firstName);
			 $middleName = stripslashes($middleName);
			 $lastName = stripslashes($lastName);
	
			if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
		  	    $full_name = $firstName.",".$middleName.",".$lastName;
		  	  }elseif($firstName !='' &&  $middleName!=''){
		  	  	$full_name = $firstName.",".$middleName;
		  	  }elseif($firstName !='' &&  $lastName!=''){
		  	  	$full_name = $firstName.",".$lastName;
		  	  }elseif($middleName!='' &&  $lastName){
		  	  	$full_name = $middleName.",".$lastName;
		  	  }elseif($firstName !=''){
		  	  	$full_name = $firstName;
		  	  }elseif($middleName!=''){
		  	  	$full_name = $middleName;
		  	  }elseif($lastName!=''){
		  	   $full_name = $lastName;
		  	 	}	
							
			/////////////exact match /////////////////////////////
			
			$k=0;
		
	 	while(($k < count($compExactMatch)) &&  $compFlag){

			
			 $queryListName = selectFrom("select listID,listName from ".TBL_COMPLIANCE_LIST." where listID= '".$compExactMatch[$k]["listID"]."' ");
			
			 if(trim($queryListName["listName"]) == 'OFAC')
			 {
		  	 	$queryExactData = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON." where listID= '".$compExactMatch[$k]["listID"]."' "; 
		  	  
		  	 	if($firstName!=''){
		      		$queryExactData .= " and (firstName ='".$firstName."')"; 
		      	}
		        if($middleName!=''){
		      		$queryExactData .= " and (middleName ='".$middleName."')"; 
		      	}
		        if($lastName!=''){
		      		$queryExactData .= " and (lastName ='".$lastName."')"; 
		      	}	
				
			
		      	      	   
		  	     $rs_exact = mysql_query($queryExactData);
				 $compExactSubRecord = mysql_num_rows($rs_exact);
				 
				   if($compExactSubRecord < 1 )
		           {
				     $queryExactData = "select alt_id,compliancePersonID,alt_name,listID from ".TBL_COMPLIANCE_OFAC_ALT." 
				                    where listID= '".$compExactMatch[$k]["listID"]."' "; 
				
		  	         if($firstName!='' || $middleName !='' || $lastName!=''){
		      	     $queryExactData .= " and  alt_name = '".$full_name."'"; 
		      	     }
					
				  }
				 
					  	   				
		  	  }
			   if($queryListName["listName"] == 'payex'){
			   
			       $queryExactData = "select compliancePersonID as id,listID  from ".TBL_COMPLIANCE_PERSON."
				     where listID= '".$compExactMatch[$k]["listID"]."'"; 
		  	  
		  	      if($firstName!=''){
		      	   $queryExactData .= " and (firstName ='".$firstName."')"; 
		      	   }
		          if($middleName!=''){
		      	    $queryExactData .= " and (middleName ='".$middleName."')"; 
		      	  }
		         if($lastName!=''){
		      	  $queryExactData .= " and (lastName ='".$lastName."')"; 
		      	 }	
		   
			   }
			   
			   if($queryListName["listName"] == 'PEP'){
			   
				 if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
					$full_name_pep = $firstName." ".$middleName." ".$lastName;
				  }elseif($firstName !='' &&  $middleName!=''){
					$full_name_pep = $firstName." ".$middleName;
				  }elseif($firstName !='' &&  $lastName!=''){
					$full_name_pep = $firstName." ".$lastName;
				  }elseif($middleName!='' &&  $lastName){
					$full_name_pep = $middleName." ".$lastName;
				  }elseif($firstName !=''){
					$full_name_pep = $firstName;
				  }elseif($middleName!=''){
					$full_name_pep = $middleName;
				  }elseif($lastName!=''){
				   $full_name_pep = $lastName;
					}	
			        $queryExactData = "select comp_PEPID as id,listID,fullName,position,portfolio from compliancePEP
				     where listID= '".$compExactMatch[$k]["listID"]."' "; 
				
		             if($firstName!='' || $middleName !='' || $lastName !=''){
		      	     $queryExactData .= " and  fullName =('".trim($full_name_pep)."')"; 
		      	    }
			  } 
			   if($queryListName["listName"] == 'HM Treasury'){
			  
			    $queryExactData = "select hm_id as id,list_id as listID,full_name from compliance_hm_treasury
				where list_id= '".$compExactMatch[$k]["listID"]."' ";
				
			/*if($firstName !='' &&  $middleName!='' &&  $lastName!=''){
		  	    $full_name = $lastName.",".$firstName.",".$middleName;
		  	  }elseif($firstName !='' &&  $lastName!=''){
		  	  	$full_name = $lastName.",".$firstName;
		  	  }elseif($middleName!='' &&  $lastName){
		  	  	$full_name = $lastName.",".$middleName;
		  	   }*/
				
		      if($firstName!='' || $middleName !='' || $lastName !=''){
		      	     $queryExactData .= " and  full_name =('".trim($full_name)."')"; 
					 $queryExactData .=" AND group_type = 'Individual' " ;
		      	   }		  
			 }
		  	    $result = mysql_query($queryExactData);
		       if($compPersonRecord = mysql_fetch_array($result))
		        {
		         		  // check auto block

						  if($compExactMatch[$k]["autoBlock"] == 'Y')
						  {
						    	//Entry in the Data Base[complianceMatchList] and exit?
							  	$Querry_Sqls="insert into ".TBL_COMPLIANCE_MATCH_LIST." (compliancePersonID,listID,isFullMatch,matchUserType,isBlocked,enteredBy,enterByID,dated) values ('".$compPersonRecord["id"]."','".$compPersonRecord["listID"]."','Y','beneficiary','Y','".$agentType."',".$modifyby.",'".$tran_dateTime."')";                 
						
								insertInto($Querry_Sqls);
								$compFlag = false;
								insertError($compExactMatch[$k]["message"]);
								$backURL = "add-beneficiary.php?notEndSessions=Y&from=".$from."&benID=".$_POST["benID"]."&msg=Y";
								redirect($backURL);
		           
						  }
						  else
						  {
						    	
						    	//entry in Data Base[complianceMatchList] table
						    	$Querry_Sqls="insert into ".TBL_COMPLIANCE_MATCH_LIST." (compliancePersonID,listID,isFullMatch,isBlocked,matchUserType,enteredBy,enterByID,dated) values ('".$compPersonRecord["id"]."','".$compPersonRecord["listID"]."','Y','beneficiary','N','".$agentType."',".$modifyby.",'".$tran_dateTime."')";
								insertInto($Querry_Sqls);
								insertError($compExactMatch[$k]["message"]);
				          //redirect($backURL); 
						     	
						  	}
						}
					$k++;
				
				 
			} // end while loop exact match
		   /////////////End Exact Match /////////////////////////////
			
			/////////////Start Partial Match  Code//////////////////////////
			
		//$compFlag = true;
		$p=0;
		$includeFlag=false;
		$matchListID = '';
		while(($p < count($compPartialMatch)) &&  $compFlag)
		{
			 $queryListName = selectFrom("select listID,listName from ".TBL_COMPLIANCE_LIST." 
			 where listID= '".$compPartialMatch[$p]["listID"]."' ");	
		
			 if($queryListName["listName"] == 'OFAC'){
		        $query = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON."
				 where listID= '".$compPartialMatch[$p]["listID"]."' "; 
		   
		     if($firstName!=''){
		      $query .= " and (firstName ='".$firstName."' or lastName ='".$firstName."'"; 
		      }	
		      if($middleName!=''){
		      	$query .= " or middleName ='".$middleName."'"; 
		      	}

		       if($lastName!=''){
		      	$query .= " or lastName ='".$lastName."' or firstName ='".$lastName."')"; 
		      	}	
								
				 $rs_partial = mysql_query($query);
				 $compPartialSubRecord = mysql_num_rows($rs_partial);
				  
				   
				   if($compPartialSubRecord < 1 )
		           {
				     $query = "select alt_id,compliancePersonID,alt_name,listID from ".TBL_COMPLIANCE_OFAC_ALT." 
				               where listID= '".$compPartialMatch[$p]["listID"]."' "; 
				
		  	     	      	    
					 /* if($firstName!=''){
		              $query .= "  and  alt_name LIKE('".$firstName."')"; 
		               }	
					  if($middleName!=''){
		              $query .= "  or  alt_name LIKE ('".$middleName."')"; 
		               }	
					 if($lastName!=''){
		              $query .= "  or alt_name LIKE ('".$lastName."')"; 
		               }*/
					   
					    if($firstName!=''){
		              $query .= "  and  (MATCH (alt_name) AGAINST ('".$firstName."')"; 
		               }	
					  if($middleName!=''){
		              $query .= "  OR  MATCH(alt_name) AGAINST ('".$middleName."')"; 
		               }	
					 if($lastName!=''){
		              $query .= "  OR MATCH(alt_name) AGAINST ('".$lastName."'))"; 
		               }		
					
				  }
		      }
			 if($queryListName["listName"] == 'payex')
			 {
			  
			    $query = "select compliancePersonID as id,listID from ".TBL_COMPLIANCE_PERSON." 
				where listID= '".$compPartialMatch[$p]["listID"]."' "; 
		     	if($firstName!=''){
		      		$query .= " AND (firstName ='".$firstName."' or lastName ='".$firstName."'"; 
		      	}	
		      	if($middleName!=''){
		      		$query .= " OR middleName ='".$middleName."'"; 
		      	}
		        if($lastName!=''){
					$query .= " OR lastName ='".$lastName."'  or firstName ='".$lastName."')"; 			   
			      	}

		         }
	
			 if($queryListName["listName"] == 'PEP')
			 {
			     $query = "select comp_PEPID as id,listID,fullName,position,portfolio from compliancePEP where listID= '".$compPartialMatch[$p]["listID"]."' "; 
				
		     	 if($firstName!='')
		              $query .= "  AND ( MATCH(fullName) AGAINST('".$firstName."')"; 			     
				 if($middleName!='')
		              $query .= "  OR  MATCH(fullName) AGAINST ('".$middleName."')"; 
				 if($lastName!='')
		              $query .= "  OR MATCH(fullName) AGAINST ('".$lastName."%'))"; 
		      	  
			  }	
			  
			  if($queryListName["listName"] == 'HM Treasury'){
			  
			    $query = "select hm_id as id,list_id as listID,full_name from compliance_hm_treasury
				where list_id= '".$compPartialMatch[$p]["listID"]."' "; 
				
		      /*if($firstName!=''){
		             $query .= "  and  full_name LIKE('%".$firstName."%')"; 
		               }	
					  if($middleName!=''){
		              $query .= "  or  full_name LIKE ('%".$middleName."%')"; 
		               }	
					 if($lastName!=''){
		              $query .= "  or full_name LIKE ('%".$lastName."%')"; 
		               }*/
					   if($firstName!=''){
					   $query .= "  and  (MATCH(full_name) AGAINST ('".$firstName."')"; 
		               }	
					  if($middleName!=''){
		              $query .= "  OR  MATCH(full_name) AGAINST ('".$middleName."')"; 
		               }	
					 if($lastName!=''){
		              $query .= "  OR MATCH(full_name) AGAINST ('".$lastName."'))"; 
		               } 
					   	
					  $query .=" AND group_type = 'Individual' " ;
		      	   }		  
			   	
		         $rs = mysql_query($query);
		     
		       if( $compPartailRecord = mysql_fetch_array($rs))
		        {
					   $includeFlag=true;
				       $compliancePersonID = $compPartailRecord["id"];
				       $listID = $compPartailRecord["listID"];
					   $agentType = $agentType;
					   $enterByID = $modifyby;
					   $sender = 'beneficiary';
					   $dated = $tran_dateTime;
					   
					   $message .= $compPartialMatch[$p]["message"]." <br>";
					   $comp_page = "add-ben-conf.php";
					   
					   $Querry_Sqls = "insert into ".TBL_COMPLIANCE_MATCH_LIST." 		
					   					(
										 compliancePersonID,
										 listID,
										 isFullMatch,
										 matchUserType,
										 enteredBy,
										 enterByID,
										 dated
										 )
										values 
										(
										'".$compliancePersonID."',
										'".$listID."',
										'N',
										'beneficiary',
										'".$agentType."',
										'".$enterByID."',
										'".$dated."'
										)";
    				   insertInto($Querry_Sqls);
							 
					   $matchListID .= @mysql_insert_id().",";
						}
						$p++;
					
				    }       //end while loop partial match 
						
			//////////////////////////////End Partial Match//////////////////////////////////////////////
	
			if($includeFlag)
			{
				$compFlag = false;
				if($ttTransFlag)
				{
					if($_REQUEST["callFrom"]=="TT")
					{
						
						$BenStatus ="Disabled";
						
						
					}
				}
				include ("compliance-partialMatch.php"); 
		    }
		}	
		
if($compFlag)
{	

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

if ($_POST["benID"] == ""){
		
 	/**
 	 * Mr Usman code commented, in effort to recover my last saved code that lost
 	 *
 	$Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." (Title, other_title, firstName, middleName, secMiddleName ,lastName, SOF, Address, Address1, Country, City, Zip, Citizenship, State, Phone, Mobile, email, IDissuedate, issuedBy,IDexpirydate,IDType, IDNumber, CPF, customerID, agentID, otherId, otherid_name, created,beneficiaryName) VALUES 
	('".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".$_POST["S/O"]."' , '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."','". $_POST["Citizenship"]."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".checkValues($_POST["benEmail"])."','".$idIssueDate."', '".$_POST["issuedBy"]."','".$idExpiryDate."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '" . $_POST["CPF"] . "', '" . $_SESSION["customerID"] . "', '$agentIDs',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','".$tran_date."','".$beneficiaryName."')";
	*/
	
	$Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." (Title, other_title, firstName, middleName, secMiddleName ,lastName, SOF, SOF_Type, Address, Address1, Country, City, Zip, Citizenship, State, Phone, Mobile, email,dob, IDissuedate, issuedBy,IDexpirydate,IDType, IDNumber, CPF, customerID, agentID, otherId, otherid_name, created,beneficiaryName, userType) VALUES 
	('".$_POST["Title"]."','".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["secMiddleName"])."' ,'".checkValues($_POST["lastName"])."', '".$_POST["S/O"]."' ,'".$_POST["sonOfType"]."' , '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."','". $_POST["Citizenship"]."', '".checkValues($_POST["State"])."', '".checkValues($_POST["Phone"])."', '".checkValues($_POST["Mobile"])."', '".checkValues($_POST["benEmail"])."','".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."','".$idIssueDate."', '".$_POST["issuedBy"]."','".$idExpiryDate."','".$_POST["IDType"]."', '".checkValues($_POST["IDNumber"])."', '" . $_POST["CPF"] . "', '" . $_SESSION["customerID"] . "', '$agentIDs',  '" . $_POST["Otherid"] . "','" . $_POST["Otherid_name"] . "','".$tran_date."','".$beneficiaryName."','".$_POST["userType"]."')";
	
	
	insertInto($Querry_Sqls);
	$benID = @mysql_insert_id();
	if($ttTransFlag)
	{
		$BenStatus=$_REQUEST["BenStatus"];
		
		
		if($_REQUEST["callFrom"]=="TT" && !empty($BenStatus))
		{
			 $updateQuery = " update ".TBL_BENEFICIARY." set status = '".$BenStatus."' where benID = '".$benID."'";
			//debug($updateQuery,true);
			  update($updateQuery);   
		}
	}
	
   if(count($arrListId) > 0){
		  for($k=0;$k<count($arrListId);$k++)
		  {
		 	if(!empty($arrListId[$k])){
			$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  userDataBaseID = '".$benID."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";
		
			update($Querry_Sqls);
		  }
		}
  }			   
	/**
	 * Storing the IBAN detail to beneficiary table
	 * @Ticket# 
	 */
	if(CONFIG_BEN_BANK_DETAILS == "1")
	{
		$updateIbanSql = "update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"]."' where benID=$benID";
		update($updateIbanSql);
	}

	/**
	 * Storing the Beneficiary bank details to table
	 * @Ticket# 3321
	 */
	if(CONFIG_BEN_BANK_DETAILS == "1")
	{
		if(!empty($_POST["usedBankId"]))
		{
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
										values ($benID, ".$_POST["usedBankId"].",'".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";
		}
		else
		{
			$dbBankId = $_POST["bankId"];
			
			if(empty($dbBankId))
			{
				/**
				 * Generate a new id, and keep generating new id untill no db entry matched
				 */
				do
				{
					$dbBankId = uniqid();
					$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
				}	
				while(!empty($dbBankIdData["bankId"]));
			}	
			
			$addNewBank = "insert into ".TBL_BANKS." ( bankId, name, country)
											values ( '".$dbBankId."','".$_POST["bankName"]."','".$_POST["country1"]."')";
 	
			insertInto($addNewBank);
			$lastBankId = @mysql_insert_id();
				
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
										values ($benID, '$lastBankId','".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";			
		}
		insertInto($benBankQry);
		$lastBenBankID =  @mysql_insert_id();
		if(CONFIG_IBAN_BEN_BANK_DETAILS == "1"){
			if($_POST["iban"]!="")
				update("UPDATE ".TBL_BEN_BANK_DETAILS. " SET IBAN = '".$_POST["iban"]."' WHERE id='".$lastBenBankID."' ");
		}
	}
	/* End #3321 */
	/* #4794 - added notes field by aslam*/
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				// If id is empty, its an insert request.
				// Insert new reocrd in this case.

				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				$insertQuery = "insert into user_id_types
										(
											id_type_id, 
											user_id, 
											user_type, 
											id_number, 
											issued_by, 
											issue_date, 
											expiry_date
										)
									values
										(
											'" . $idTypeValues["id_type_id"] . "', 
											'" . $benID . "', 
											'B', 
											'" . $idTypeValues["id_number"] . "', 
											'" . $idTypeValues["issued_by"] . "', 
											'" . $issueDate . "',
											'" . $expiryDate . "'
										)";
				insertInto( $insertQuery );
/*
 * @Ticket #4794
 */
				$lastIdInsertId = @mysql_insert_id();
				if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
					update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
				}
			}
		}
	}

	// End of code against #3299: MasterPayex - Multiple ID Types

	////To record in History
	$descript ="Beneficiary is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$benID,TBL_BENEFICIARY,$descript);
	
	$startMessage="Congratulations! You have been registered as a Beneficiary of Rich Almond. <br> Your registration information is as follows:";
	include ("mailBenInfo.php");
	$To = $_POST["benEmail"]; 
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Beneficiary Account creation ";	
	if(CONFIG_MAIL_GENERATOR == '1'){
		sendMail($To, $subject, $data, $fromName, $fromEmail);
	}
		
	$_SESSION["Title"] = "";
	$_SESSION["other_title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["secMiddleName"] = "";
	$_SESSION["dobDay"] = "";
	$_SESSION["dobMonth"] = "";
	$_SESSION["dobYear"] = "";
	$_SESSION["S/O"] = "";
	$_SESSION["sonOfType"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Otherid"] = "";
	$_SESSION["Otherid_name"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["CPF"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["IDexpirydate"] = "";
	$_SESSION["Citizenship"] = "";

	$_SESSION["bankName1"] = "";
	$_SESSION["name1"] = "";
	$_SESSION["account1"] = "";
	$_SESSION["branchName1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["iban1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["accountType1"] = "";
	$_SESSION["country1"] = "";
	$_SESSION["bankId1"] = "";

	$_SESSION["userType"] = "";

	unset($_SESSION["ben_bank_id"]);

	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	
	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] ); 
	
	// End of code against #3299: MasterPayex - Multiple ID Types

	//insertError(BE9);//"Beneficiary Added successfully");//
	$_SESSION["error"] = "Beneficiary Added successfully";

} else {

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

 	$Querry_Sqls = "update ".TBL_BENEFICIARY." set Title='".$_POST["Title"]."',
	other_title='".checkValues($_POST["other_title"])."',
	 firstName='".checkValues($_POST["firstName"])."', 
	 middleName='".checkValues($_POST["middleName"])."', 
	 secMiddleName = '".checkValues($_POST["secMiddleName"])."',
	 lastName='".checkValues($_POST["lastName"])."', 
	 SOF='".checkValues($_POST["S/O"])."',
	 SOF_Type='".checkValues($_POST["sonOfType"])."',
	 IDType='".$_POST["IDType"]."', 
	 IDNumber='".checkValues($_POST["IDNumber"])."',
	 otherId='".checkValues($_POST["Otherid"])."',
	 otherId_name='".checkValues($_POST["Otherid_name"])."',
	 Address='".checkValues($_POST["Address"])."', 
	 Address1='".checkValues($_POST["Address1"])."', 
	 dob =  '".$_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"]."',
	 Zip='".$_POST["Zip"]."', 
	 CPF='".$_POST["CPF"]."',
	 Country='".$_POST["Country"]."', 
	 City='".$_POST["City"]."', 
	 State='".$_POST["State"]."', 
	 Phone='".$phone."', 
	 Mobile='".$mobile."', 
	 email='".checkValues($_POST["benEmail"])."',
	 Citizenship = '". $_POST["Citizenship"]."',
	 IDissuedate = '". $idIssueDate."',
	 issuedBy =  '".$_POST["issuedBy"]."',
	 IDexpirydate = '".$idExpiryDate."',
	 userType =  '".$_POST["userType"]."',
	 customerID = '".$_SESSION["customerID"]."' where benID='".$_POST["benID"]."'";
   update($Querry_Sqls);
   
   $benID = $_POST["benID"];
	if($ttTransFlag)
	{
		$BenStatus=$_REQUEST["BenStatus"];
		
		
		if($_REQUEST["callFrom"]=="TT" && !empty($BenStatus))
		{
			 $updateQuery = " update ".TBL_BENEFICIARY." set status = '".$BenStatus."' where benID = '".$benID."'";
			//debug($updateQuery,true);
			   
		}
		else
		 $updateQuery = " update ".TBL_BENEFICIARY." set status = 'Enabled' where benID = '".$benID."'";
		 update($updateQuery); 
	}
	if(count($arrListId) > 0){
		  for($k=0;$k<count($arrListId);$k++)
		  {
		 	if(!empty($arrListId[$k])){
			$Querry_Sqls = "update ".TBL_COMPLIANCE_MATCH_LIST." 
						  set
							  userDataBaseID = '".$_POST["benID"]."'
						  where 
								matchListID = ".$arrListId[$k]."
						   ";
		
			update($Querry_Sqls);
		  }
		}
  }			   
	/**
	 * Updating the beneficiary bank details
	 * @Ticket# 3321
	 */
	if(CONFIG_BEN_BANK_DETAILS == "1")
	{
		
		/**
		 * Updating the IBAN number to the beneficiary table
		 * @Ticket# 3558
		 */
		
		update("update ".TBL_BENEFICIARY." set IBAN='".$_POST["iban"]."' where benID='".$_POST["benID"]."'");
		
		
		if(!empty($_POST["usedBankId"]))
		{
			$benBankUpdateQry = "update ".TBL_BEN_BANK_DETAILS. " set 
													bankId = ".$_POST["usedBankId"].",
													accountNo = '".$_POST["account"]."',
													accountType = '".$_POST["accountType"]."',
													branchCode = '".$_POST["branchName"]."',
													branchAddress = '".$_POST["branchAddress"]."',
													swiftCode = '".$_POST["swiftCode"]."'
													where benId = $benID ";
			/*
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType)
										values ($benID, ".$_POST["usedBankId"].",'".$_POST["account"]."','".$_POST["accountType"]."')";
			*/
		}
		else
		{
		
			$dbBankId = $_POST["bankId"];
			
			if(empty($dbBankId))
			{
				/**
				 * Generate a new id, and keep generating new id untill no db entry matched
				 */
				do
				{
					$dbBankId = uniqid();
					$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
				}	
				while(!empty($dbBankIdData["bankId"]));
			}
			
			$addNewBank = "insert into ".TBL_BANKS." ( bankId, name, country)
											values ( '".$dbBankId."','".$_POST["bankName"]."','".$_POST["country1"]."')";
 	
			insertInto($addNewBank);
			$lastBankId = @mysql_insert_id();
				
			$benBankUpdateQry = "update ".TBL_BEN_BANK_DETAILS. " set 
													bankId = ".$lastBankId.",
													accountNo = '".$_POST["account"]."',
													accountType = '".$_POST["accountType"]."',
													branchCode = '".$_POST["branchName"]."',
													branchAddress = '".$_POST["branchAddress"]."',
													swiftCode = '".$_POST["swiftCode"]."'
													where benId = $benID ";			
		}
		if(isExist("select id from " .TBL_BEN_BANK_DETAILS. " where benId = $benID ")){
			update($benBankUpdateQry);	
		}
		else{
			$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
										values ($benID, '$lastBankId','".$_POST["account"]."','".$_POST["accountType"]."','".$_POST["branchName"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."')";			
			insertInto($benBankQry);
		}
		
		if(CONFIG_IBAN_BEN_BANK_DETAILS == "1"){
			if($_POST["iban"]!="")
				update("UPDATE ".TBL_BEN_BANK_DETAILS. " SET IBAN = '".$_POST["iban"]."' WHERE benID='".$benID."' ");
		}
	 }	
    
   /* End #3321 */
  
 

	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	if ( !empty( $_REQUEST["IDTypesValuesData"] ) )
	{
		$idTypeValuesData = $_REQUEST["IDTypesValuesData"];
		foreach( $idTypeValuesData as $idTypeValues )
		{
			if ( !empty( $idTypeValues["id_type_id"] )
				 && !empty( $idTypeValues["id_number"] )
				 && !empty( $idTypeValues["issued_by"] )
				 && !empty( $idTypeValues["issue_date"] )
				 && !empty( $idTypeValues["expiry_date"] ) )
			{
				$issueDate = $idTypeValues["issue_date"]["year"] . "-" . $idTypeValues["issue_date"]["month"] . "-" . $idTypeValues["issue_date"]["day"];
				$expiryDate = $idTypeValues["expiry_date"]["year"] . "-" . $idTypeValues["expiry_date"]["month"] . "-" . $idTypeValues["expiry_date"]["day"];

				if ( !empty($idTypeValues["id"]) )
				{
					// If id is not empty, its an update request.
					// Update the existing record in this case.

					$updateQuery = "update user_id_types 
										set 
											id_number='" . $idTypeValues["id_number"] . "', 
											issued_by='" . $idTypeValues["issued_by"] . "', 
											issue_date='" . $issueDate . "', 
											expiry_date='" . $expiryDate . "' 
										where 
											id='" . $idTypeValues["id"] . "'";
					update( $updateQuery );
/*
 * @Ticket #4794
 */
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='".mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$idTypeValues["id"]."'");
						
					}
				}
				else
				{
					// If id is empty, its an insert request.
					// Insert new reocrd in this case.

					

					$insertQuery = "insert into user_id_types
											(
												id_type_id, 
												user_id, 
												user_type, 
												id_number, 
												issued_by, 
												issue_date, 
												expiry_date 
											)
										values
											(
												'" . $idTypeValues["id_type_id"] . "', 
												'" . $_REQUEST["benID"] . "', 
												'B', 
												'" . $idTypeValues["id_number"] . "', 
												'" . $idTypeValues["issued_by"] . "', 
												'" . $issueDate . "', 
												'" . $expiryDate . "'
											)";
					insertInto( $insertQuery );
/*
 * @Ticket #4794
*/
					$lastIdInsertId = @mysql_insert_id();
  
					if(isset($idTypeValues["notes"]) && !empty($idTypeValues["notes"])){
						update("update user_id_types set notes='". mysql_real_escape_string($idTypeValues["notes"])."' where id ='".$lastIdInsertId."'");
					}
				}
			}
		}
	}
	
	if ( !empty( $_SESSION["IDTypesValuesData"] ) )
		unset( $_SESSION["IDTypesValuesData"] ); 
	

	// End of code against #3299: MasterPayex - Multiple ID Types

	$benID = $_POST["benID"];
	$_SESSION["Title"] = "";
	$_SESSION["other_title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["secMiddleName"] = "";
	$_SESSION["S/O"] = "";
	$_SESSION["sonOfType"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Otherid"] = "";
	$_SESSION["Otherid_name"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["CPF"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["IDexpirydate"] = "";
	$_SESSION["Citizenship"] = "";
	
	$_SESSION["bankName1"] = "";
	$_SESSION["account1"] = "";
	$_SESSION["branchName1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["iban1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["accountType1"] = "";
	$_SESSION["userType"] = "";
	$_SESSION["bankId1"] = "";
	$_SESSION["country1"] = "";
	
	unset($_SESSION["ben_bank_id"]);
	
	if($_GET["from"]!=''){
	$from = $_GET["from"];
}elseif($_POST["from"]!=''){
	$from = $_POST["from"];
	}
	////To record in History
	$descript ="Beneficiary is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["benID"],TBL_BENEFICIARY,$descript);
	insertError(BE10);
	
	$startMessage="Congratulations! Your account information has been updated successfully.<br> Your new acoount information is as follows:";
	include ("mailBenInfo.php");
	$To = $_POST["benEmail"]; 
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Beneficiary Account Updation ";
	
	if(CONFIG_MAIL_GENERATOR == '1'){
		sendMail($To, $subject, $data, $fromName, $fromEmail);
  }
}

if($from == "popUp"  && $_GET["r"] != "searchBen")
{
	//$_SESSION["benID"] = $benID;
	
	?>
	<script language="javascript">
		opener.document.location = "<?=$transactionPage?>?msg=Y&success=Y&benID=<?=$benID?>&transID=<?=$_GET['transID']?>&transType=<? echo ($_REQUEST["transType"]!="" ? $_REQUEST["transType"]:$_SESSION["transType"])?>&docWaiver=<?=$_REQUEST["docWaiver"]?>";
		window.close();
	</script>
	<?
}else if ($from == "searchBen" || $_GET["r"] == "searchBen") {
	$_SESSION["benID"] = $benID;
	?>
	<script language="javascript">
		opener.document.location = "create-bene-trans.php?msg=Y&success=Y";
		window.close();
	</script>
	<? 
} 
else if ($from == "enableDisableBen") {
	
	?>
	<script language="javascript">
		opener.document.location = "enable_disable_beneficiary.php?msg=Y&success=Y";
		window.close();
	</script>
	<? 
} 
else {
		$backURL .= "&success=Y";
	if($ttTransFlag)
	{
		
		$CompBen="1";
		if($_REQUEST["from"]=="TTtransfer" || $_REQUEST["callFrom"]=="TT")
		{
			
		
			echo '<script>
					
					window.opener.SelectBen('.$benID.','.$CompBen.');
			
					window.close();
				</script>';	
		}
	}
		else
			redirect($backURL);}

}// end compliance check
//echo $_POST["benID"];
?>