<?php
/*if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
    session_cache_limiter("public");
}*/
session_start();
include ("../include/config.php");
include ("security.php");
$userID  = $_SESSION["loggedUserData"]["userID"];
if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "txt")	
{
/* Export version is txt */
	//echo '##1!te';
 
   $fileName = CONFIG_MT103_EXPORT_FILE_NAME;
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	header('Content-type: plain/text');
	header("Content-disposition: Attachment; filename=$fileName.txt");
	
	
	$strFieldsToFetchFromDb = "
					tr.refNumberIM,
tr.transID,					
					tr.transaction_notes as transaction_notes,
					tr.transDate as transDate,
					tr.transType as transType,
					tr.currencyTo as currencyTo,
					tr.currencyFrom as currencyFrom,
					tr.transAmount as transAmount,
					tr.localAmount as localAmount,
					tr.fundSources as fundSources,
					tr.transactionPurpose as transactionPurpose,
					tr.collectionPointID,
					tr.paymentValueDate,
					tr.trading_sell_account,
					tr.tip,
					concat(ben.firstName,' ',ben.middleName,' ',ben.lastName) as beneficiaryName,
					ben.IDType as beneficiaryIDType,
					ben.IDNumber as beneficiaryIDNumber,
					ben.Phone as beneficiaryPhone,
					ben.Address1 as beneficiaryAddress,
					concat(cust.firstName,' ',cust.middleName,' ',cust.lastName) as senderName,
					cust.country as senderAddress,
					cust.Phone as senderTelephone,
					tbd.accNo,
					tbd.IBAN,
					tbd.swiftCode,
					tbd.sortCode,
					tbd.routingNumber,
					tbd.bankName,
					tbd.branchAddress,
					tbd.branchCode
			";
			
	if(!empty($_REQUEST["recId"]))
	{
			$strSql = "
					select 
						$strFieldsToFetchFromDb					
					from 
						transactions as tr
						LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
						LEFT JOIN customer as cust ON tr.customerID = cust.customerID
						LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
					where 
						tr.dispatchNumber = '".$_REQUEST["recId"]."'
					order by 
						tr.transDate ";
	$accountNumber = $_REQUEST["accountNumber"];
	}
	
	if(strtolower($_REQUEST["searchTrans"]) == "old" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					$strFieldsToFetchFromDb
				from 
					transactions as tr
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where 
					tr.transID IN (".$arrAllTransactions.")
				order by 
					tr.transDate ";
		
		
	}
	if(strtolower($_REQUEST["searchTrans"]) == "new" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					$strFieldsToFetchFromDb
				from 
					transactions as tr
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where 
					tr.transID IN (".$arrAllTransactions.")
				order by 
					tr.transDate ";
		
		
	}
	 
	$fullRS = SelectMultiRecords($strSql);
	
	
	
	$strCsvOutput = "";

	
	//$cpfseperator = array("-","/",".");
	$transseperator = "-";
	$lineseperator = "\r\n";
	$sortCode = 'SC200000';
	$accountCode = 'BARCGB22';
	$address = CONFIG_MT103_ADDRESS;
	
	function just_clean($string)
	{
	// Replace other special chars
	$specialCharacters = array(
	'#' => '',
	'$' => '',
	'%' => '',
	'&' => '',
	'@' => '',
	'.' => '',
	'+' => '',
	'=' => '',
	'\\' => '',
	'/' => '',
	'-' => '',
	);
	
	while (list($character, $replacement) = each($specialCharacters)) {
	$string = str_replace($character, '' . $replacement . '', $string);
	}
	
	
	// Remove all remaining other unknown characters
	$string = preg_replace('/[^a-zA-Z0-9\-]/', ' ', $string);
	$string = preg_replace('/^[\-]+/', '', $string);
	$string = preg_replace('/[\-]+$/', '', $string);
	$string = preg_replace('/[\-]{2,}/', ' ', $string);
	
	return $string;
	}

	for($i=0; $i < count($fullRS); $i++)
	{
		
		
		$transPurpose=$fullRS[$i]['transactionPurpose'];
		$fundSource=$fullRS[$i]['fundSources'];
		if($fullRS[$i]["IBAN"] !="")
		{
			if($fullRS[$i]["IBAN"]!='No')
				$ibanoracc=$fullRS[$i]["IBAN"];
			elseif($fullRS[$i]["accNo"]!="")
				{
					$ibanoracc=$fullRS[$i]["accNo"];
				}
				
		}
		elseif($fullRS[$i]["accNo"]!="")
				{
					$ibanoracc=$fullRS[$i]["accNo"];
				}
		else
		{
		$ibanoracc="";
		} 
		if($_REQUEST["exportsepa"]!="sepa")
		{
			if(!empty($fullRS[$i]["tip"]))
				$tip=$fullRS[$i]["tip"];
			else
				$tip="Payment";
		}
		elseif($_REQUEST["exportsepa"]=="sepa")
		{
		 $tip="/REC/SCT"." ".$fullRS[$i]["tip"]; 
		}
	$_arrNote = explode("|",$fullRS[$i]["transaction_notes"]);
	//$transNote = str_replace("-","",$_arrNote[2]);
	$transNote = just_clean($_arrNote[2]);
	$transNote = wordwrap($transNote,35,$lineseperator);
	$refNumberIM = just_clean($fullRS[$i]["refNumberIM"]);
	if( defined("CONFIG_REF_NUMBER_FORMAT_MT103") && CONFIG_REF_NUMBER_FORMAT_MT103 != "0" ) 
		$refNumberIM = $fullRS[$i]["refNumberIM"];
	$localAmount = number_format($fullRS[$i]["localAmount"],2,",","");
	$beneficiaryName = just_clean($fullRS[$i]["beneficiaryName"]);
	if(strtolower($_REQUEST["searchTrans"]) == "old")
		$accountNumber = $fullRS[$i]["trading_sell_account"];
		$accNo = $fullRS[$i]['currencyTo'];
		//$accNo = $fullRS[$i]['1'];
	
		//$strAccNumber = SelectFrom("select accNum from dispatch_table_payment where transIds = '".$accNo."'");
		$strAccNumber = SelectFrom("select accounNumber from accounts where currency = '".$accNo."'");
		//$intAccountNumber = $strAccNumber['accNum'];
		$intAccountNumber = $strAccNumber['accounNumber'];
	//debug($intAccountNumber);
	//debug($fullRS[$i]["transDate"]."-->".$fullRS[$i]["currencyFrom"]."-->".$fullRS[$i]["transAmount"]."-->".$fullRS[$i]["transaction_notes"]);
	
		//$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"];
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"];
		if( defined("CONFIG_REF_NUMBER_FORMAT_MT103") && CONFIG_REF_NUMBER_FORMAT_MT103 != "0" )
			$strCsvOutput .=":20: ".substr($refNumberIM,0,16).$lineseperator;
		else 
			$strCsvOutput .=":20:TRANS REF ".substr($refNumberIM,0,16).$lineseperator;
			$strCsvOutput .=":23B: CRED".$lineseperator;
			$strCsvOutput .=":32A:".substr(date("ymd",strtotime($fullRS[$i]["paymentValueDate"])),0,8).substr($fullRS[$i]["currencyTo"],0,3).
			substr($localAmount,0,14).$lineseperator;
			$strCsvOutput .=":33B:".substr($fullRS[$i]["currencyTo"],0,3).substr($localAmount,0,14).$lineseperator;
			$strCsvOutput .=":50K:"."/".substr($intAccountNumber,0,34).$lineseperator;
			if(!empty($address))
		{
			$strCsvOutput .=$address.$lineseperator;
		}
			$strCsvOutput .=":53A:"."".substr($intAccountNumber,0,34).$lineseperator;
			/*$strCsvOutput .="XYZ PLC".$lineseperator;
			$strCsvOutput .="20 SMITH ST".$lineseperator;
			$strCsvOutput .="BOURNEMOUTH".$lineseperator; 
			$strCsvOutput .="GB".$lineseperator;*/
		if(!empty($fullRS[$i]["swiftCode"]))
		{
			$strCsvOutput .=":57A:"."".$fullRS[$i]["swiftCode"].$lineseperator;
		}
		else if(!empty($fullRS[$i]["sortCode"]) && $fullRS[$i]["sortCode"]!='No' ){
			$strCsvOutput .=":57D:"."//SC".$fullRS[$i]["sortCode"].$lineseperator;
			if(!empty($fullRS[$i]["branchAddress"]))
			{
				$strCsvOutput .=$fullRS[$i]["branchAddress"].$lineseperator;
			}
		
		}
		else if(!empty($fullRS[$i]["routingNumber"]) && $fullRS[$i]["routingNumber"]!='No' )
		{
			$strCsvOutput .=":57D:"."//FW".$fullRS[$i]["routingNumber"].$lineseperator;
			//$strCsvOutput .=$fullRS[$i]["bankName"].$lineseperator;
			if(!empty($fullRS[$i]["branchAddress"]))
			{			
				$strCsvOutput .=$fullRS[$i]["branchAddress"].$lineseperator;
			}
		}
			/*$strCsvOutput .=substr($accountCode,0,34).$lineseperator;*/
			//$strCsvOutput .=":57D:"."//".$fullRS[$i]["bankName"].$lineseperator;
			//$strCsvOutput .=$fullRS[$i]["branchAddress"].$lineseperator; 
			$strCsvOutput .=":59:"."/".substr($ibanoracc,0,34).$lineseperator;
		if(!empty($fullRS[$i]["bankName"]))
		{
			$strCsvOutput .=$fullRS[$i]["bankName"].$lineseperator;
		}	
		if(!empty($fullRS[$i]["branchAddress"]))
		{
			 $strCsvOutput .=$fullRS[$i]["branchAddress"].$lineseperator;
		}
		if(!empty($fullRS[$i]["beneficiaryAddress"]))
		{
			$strCsvOutput .=$fullRS[$i]["beneficiaryAddress"].$lineseperator;
		}
			//$strCsvOutput .=substr($beneficiaryName,0,40).$lineseperator;
			// $strCsvOutput .=":70:".$fullRS[$i]["tip"].$lineseperator;
			$strCsvOutput .=":70:".$tip.$lineseperator;
			$strCsvOutput .=":72:".$tip."-}".$lineseperator;
			$strCsvOutput .= "-".$lineseperator;
	}
 
	if(( $_REQUEST["searchTrans"] == "new"))
	{
	 $updateTransStatus=update("UPDATE transactions SET `transStatus` = 'Credited' WHERE dispatchNumber ='".$_REQUEST["recId"]."'");
	 
	 $querTransID1 = "SELECT transID FROM ".TBL_TRANSACTIONS." WHERE dispatchNumber ='".$_REQUEST["recId"]."' ";
	 $querTransID = mysql_query($querTransID1);
	 $i=0;
	 while($transID = mysql_fetch_array($querTransID)){
	 $transIDs .= $transID["transID"].",";
	 $i++;
	 }
	 
	 $transIDs  = rtrim($transIDs,",");
	 activities($_SESSION["loginHistoryID"],"UPDATION",$_REQUEST["recId"],"transactions","For transactions ".$transIDs." status is updated to Credited for ".CONFIG_MT103_EXPORT_FILE_NAME."");
	 
	 $queryDispatchBookId = selectFrom("SELECT recId FROM ".TBL_DISPATCH_BOOK." WHERE dispatchNumber = '".$_REQUEST["recId"]."'");
	
	  activities($_SESSION["loginHistoryID"],"EXPORT",$_REQUEST["recId"],"dispatch_book","".CONFIG_MT103_EXPORT_FILE_NAME ." file exported with ".$i." transactions");
	
	}
	  
	if( $_REQUEST["searchTrans"] == "old"){
	 
 	activities($_SESSION["loginHistoryID"],"Re-exported",$_REQUEST["recId"],"transactions","transactions with dispatchNumber ".$_REQUEST["recId"]." are re-exported");
	
	
	}
	echo $strCsvOutput;
	/* End of txt version */
}
?>
