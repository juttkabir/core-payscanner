<?
session_start();
include ("../include/config.php");
include ("security.php");

if ($_POST["Submit"] == "Send") {	// [by JAMSHED]
	
	if ($_POST["subject"] == "") {
		$msg2 = CAUTION_MARK . " Please provide subject of your feedback.";
	} else if ($_POST["feedback"] == "") {
		$msg2 = CAUTION_MARK . " Please provide feedback to send.";
	} else {
		/*The variable "FEEDBACK_EMAIL_ADDRESS" is in commonAdmin.php 
      and not in client config file since its not related to
      a client but is for payex core*/
		$emailTo = FEEDBACK_EMAIL_ADDRESS; 
		
		$subject = trim($_POST["subject"]);
		$message = trim($_POST["feedback"]);
		
		$fromName  = $_SESSION["loggedUserData"]["name"];
		$fromEmail = $_SESSION["loggedUserData"]["email"];
		
		$headers = "From: $fromName <$fromEmail>";
		
		sendMail($emailTo, $subject, $message, $headers, $fromName);
		$msg = "Your feedback has been sent";
		$action = "performed";
		
	}
}
?>
<html>
<head>
	<title>Send Feedback</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!--
function checkForm(theForm) {
	if (theForm.subject.value == "") {
		alert("Please provide subject of your feedback.");
		theForm.subject.focus();
		return false;	
	}
	if (theForm.feedback.value == "") {
		alert("Please provide feedback to send.");
		theForm.feedback.focus();
		return false;	
	}
	return true;
}

function redirect(str) {
	if (str == 'Close') {
		window.location = 'main.php';
	} else if (str == 'Go Back') {
		window.location = 'feedback.php';
	}	
}
// end of javascript -->
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<?	if ($msg2 != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td colspan="2" align="center" class="tab-r"><? echo $msg2 ?></td>
              </tr>
            </table></td>
          </tr>
	<?	} else if ($msg != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center">&nbsp;</td>
                <td width="635" align="center"><font size="5" color="<? echo  SUCCESS_COLOR; ?>"><b><i><? echo SUCCESS_MARK ;?></i></b></font>&nbsp;&nbsp;<? echo "<font color='" .  SUCCESS_COLOR  . "'><b>".$msg."</b></font>"; ?></td>
              </tr>
            </table></td>
          </tr>
	<?	}  ?>
  <form action="feedback.php" method="post" onSubmit="return checkForm(this);" name="frmFeedBack">
  <tr>
    <td align="center">
		<table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Client Feedback</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
<?	if ($action == "performed") {  ?>
		<tr bgcolor="#ededed">
			<td height="25" colspan="2" align="center">
				<input type="button" value="Go Back" onClick="redirect('Go Back');">&nbsp;&nbsp; <input type="button" value="Close" onClick="redirect('Close');">
			</td>
		</tr>
<?	} else {  ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="130" valign="top"><font color="#005b90"><strong>Subject*</strong></font></td>
            <td><input type="text" name="subject" size="40" value="<? echo $_POST["subject"] ?>"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td valign="top"><font color="#005b90"><strong>Feed Back*</strong></font></td>
            <td>
          <textarea name="feedback" cols="60" rows="10" style="font-family: verdana; font-size: 11px"><? echo $_POST["feedback"] ?></textarea>
          </td>
        </tr>
		<tr bgcolor="#ededed">
			<td height="25" colspan="2" align="center">
				<input type="submit" name="Submit" value="Send">&nbsp;&nbsp; <input type="reset" value="Clear">
			</td>
		</tr>
<?	}  ?>
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
