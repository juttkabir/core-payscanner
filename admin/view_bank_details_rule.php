<?php 
	session_start();
	//ini_set("display_errors","1");
	//error_reporting("1");
	// Including files
	include_once ("../include/config.php");
	include_once ("security.php");
	if(isset($_POST['searchRule'])){
		//$searchCountry = $_POST['country'];
		$searchCurrency = trim($_POST['currency'],' ');
		$searchStatus = $_POST['status'];
		$condition = "";
		/* if($searchCountry != 'All')
			$condition .= " and country.countryId = '$searchCountry' "; */
		/* if($searchCurrency != 'All')
			$condition .= " AND currency.currencyName = '$searchCurrency' "; */
			if($searchCurrency != 'All')
			$condition .= " AND currency = '$searchCurrency' ";
		if($searchStatus != 'All')
			$condition .= " AND benBankDetailsRule.status = '$searchStatus'";
		//$queryRules = "SELECT id, countryName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule INNER JOIN `countries` AS country ON benBankDetailsRule.benCountry  WHERE benBankDetailsRule.benCountry = country.countryId".$condition;
		// query for the country & currecny based search
	
		/* $queryRules = "SELECT DISTINCT(id), countryName, currencyName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status 
        FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule
INNER JOIN  `countries` AS country ON benBankDetailsRule.benCountry = country.countryId
INNER JOIN  `currencies` AS currency ON benBankDetailsRule.currency = currency.currencyName
WHERE 1 ".$condition; */
		// query for the currency based search 
		
		/* $queryRules = "SELECT id, currencyName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule ,currencies AS currency   WHERE benBankDetailsRule.currency = currency.currencyName".$condition; */
		$queryRules = "SELECT id,currency, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule  WHERE 1 ".$condition;		
		$bankDetailsRules = selectMultiRecords($queryRules);
		$records = count($bankDetailsRules);
	}
	
	if(isset($_GET['rid']) && isset($_GET['status'])){
		$statusFlag = $_GET['status'];
		$ruleID = $_GET['rid'];
		if($statusFlag == '0')
			$changeStatus = 'Disable';
		elseif($statusFlag == '1')
			$changeStatus = 'Enable';
		$queryChangeStatus = "UPDATE ".TBL_BEN_BANK_DETAILS_RULE." SET `status` = '$changeStatus' WHERE id = '$ruleID'";
		mysql_query($queryChangeStatus) or die(mysql_error());
		//$queryRules = "SELECT id, countryName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule INNER JOIN `countries` AS country ON benBankDetailsRule.benCountry  WHERE benBankDetailsRule.benCountry = country.countryId";
		
		/* $queryRules = "SELECT DISTINCT(id), countryName, currencyName, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status 
        FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule
		INNER JOIN  `countries` AS country ON benBankDetailsRule.benCountry = country.countryId
		INNER JOIN  `currencies` AS currency ON benBankDetailsRule.currency = currency.currencyName WHERE 1 $condition  and benBankDetailsRule.benCountry = country.countryId ";
        debug ($queryRules); */
		/* debug ($queryRules);
		$bankDetailsRules = selectMultiRecords($queryRules);
	
		$records = count($bankDetailsRules); */
	}
 
?>
<html>
	<head>
		<title>Bank Details Rule</title>
		<style>
			body{
				margin:0;
				font-family:arial;
				color:#555;
				font-size:12px;
			}
			h2{
				margin-bottom:0;
			}
			.wrapper{
				max-width:980px;
				margin:0 auto;
				overflow:hidden;
			}
			.headtxt{
				color:#fff;
				background:#6699CC;
				padding:2px;
				font-size:14px;
			}
			label,p,.link{
				font-size:12px;
			}
			p{
				font-weight:bold;
			}
			.container{
				border:1px solid #aaa;
				padding:10px 5px;
				overflow:hidden;
			}
			.link{
				float:right;
				display:inline-block;
				text-decoration:none;
			}
			label{
				display:inline-block;
				vertical-align:top;
				padding-top:2px;
			}
			input{
				margin:0;
			}
			.left-col{
				float:left;
				width:50%;
			}
			.right-col{
				float:right;
				width:50%;
			}
			.row{
				clear:both;
				margin:5px 0;
				padding:5px;
				overflow:hidden;
			}
			.msg{
				font-size:12px;
			}
			.title{
				background:#A2CAF2;
			}
			table{
				border:1px solid #000000;
			}
			table tr td, table, table tr,table tr th{
				font-size:11px;
			}
			table tr td, table tr th{
				text-align:center;
				padding:2px;
			}
			table tr th{
				color:#fff;
			}
			input, select{
				font-size:12px;
				margin-right:10px;
			}
		</style>
		<script>
			function SelectOption(OptionListName, ListVal)
			{
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="wrapper">
			<h2 class="headtxt">View Rule Against Currency</h2>
			<div class="container">
				<div class="row">
					<form action="" method="post">
						<!--<label>Country:</label>
						<?php 
							/*$queryCountries = "SELECT DISTINCT countryName, countryId FROM `countries` WHERE countryName != ''";
							$countries = selectMultiRecords($queryCountries);
						?>
						<select name="country">
							<option value="All">All</option>
							<?php 
								for($c = 0; $c < count($countries); $c++){
							?>
							<option value="<?php echo $countries[$c]['countryId']?>"><?php echo $countries[$c]['countryName']; ?></option>
							<?php 
								}*/
							?>
						</select>
						-->
						<label>Currency:</label>
						<?php 
							/* $queryCurrencies = "SELECT DISTINCT currencyName, cID FROM `currencies` WHERE currencyName != ''"; */
//							$queryCurrencies = "select distinct(currencyRec) from services where currencyRec != ''";
							$queryCurrencies ="select distinct(currencyName) from currencies order by currencyName ASC";

							$currencies = selectMultiRecords($queryCurrencies);
							
						?>
						<select name="currency">
							<option value="All">All</option>
							<?php 
	$arrLink=array();
	for($d= 0; $d < count($currencies); $d++){
		
		if(strpos($currencies[$d]['currencyRec'],',')){
		$arr = explode(',',$currencies[$d]['currencyRec']);
		//$arrLink[]=$arr;
		
		// array unique function return the new array accept 
		foreach($arr as $val){
	
		  $arrLink[]=trim($val);
					}
					}else{
$arrLink[]=trim($currencies[$d]['currencyName']);
				}
	
	}
	$strUnique=array_unique($arrLink);
	foreach($strUnique as $val){
		?>
		<option value="<?php echo  $val;?>"><?php echo  $val; ?></option>
		
		<?}
	
	
	/* print_r($arrLink);
	for($k=0; $k<count($arrLink);$k++)
	{
	$arrLink
	
	} */
							?>
						</select>
						<script language="JavaScript">
							SelectOption(document.forms[0].country, "<?echo $_POST['country']; ?>");
                        </script>
						<script language="JavaScript">
							SelectOption(document.forms[0].currency, "<?echo $_POST['currency']; ?>");
                        </script>
						<label>Status:</label>
						<select name="status">
							<option value="All">All</option>
							<option value="Enable" selected="selected">Enable</option>
							<option value="Disable">Disable</option>
						</select>
						<script language="JavaScript">
							SelectOption(document.forms[0].status, "<?echo $_POST['status']; ?>");
                        </script>
						<input name="searchRule" value="Search" type="submit"/>
					</form>
				</div>
				<?php 
					if(isset($_POST['searchRule']) || (isset($bankDetailsRules) && $records > 0)){
				?>
					<table border="1" bordercolor="#555555" cellpadding="0" cellspacing="0">
						<tr class="title">
							<!--<th width="10%">Country</th>-->
							<th width="8%">Currency</th>
							<th>Beneficiary Account Name</th>
							<th>Account Number</th>
							<th>Branch Name / Number</th>
							<th>Branch Address</th>
							<th>Swift Code</th>
							<th>Sort Code</th>
							<th>IBAN</th>
							<th>Routing Number</th>
							<th>Current Status</th>
							<th>Created Date</th>
							<th>Updated Date</th>
							<th>Enable / Disable</th>
							<th width="30px">Edit</th>
						</tr>
						<?php 
							
							for($x = 0; $x < count($bankDetailsRules); $x++){
						?>
						<tr>
							<!-- <td>
								 <?php// echo $bankDetailsRules[$x]['countryName']; ?>
							</td> -->
							<td>
								<?php echo $bankDetailsRules[$x]['currency']; ?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['accountName'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['accNo'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php
									if($bankDetailsRules[$x]['branchNameNumber'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['branchAddress'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['swiftCode'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php
									if($bankDetailsRules[$x]['sortCode'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['iban'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							<td>
								<?php 
									if($bankDetailsRules[$x]['routingNumber'] == 'Y')
										echo "<img src='images/premier/tick.png' title='Mandatory' height='20px' />";
									else
										echo "<img src='images/premier/cross.png' title='Non-Mandatory' height='18px' />";
								?>
							</td>
							
							<td>
								<?php echo "<b>".$bankDetailsRules[$x]['status']."</b>"; ?>
							</td>
							
							<td>
								<?php echo $bankDetailsRules[$x]['creationDate']; ?>
							</td>
							
							<td>
								<?php echo $bankDetailsRules[$x]['updateDate']; ?>
							</td>
							<td>
								<?php
									if($bankDetailsRules[$x]['status'] == 'Enable')
										echo "<a href='".$_SERVER['PHP_SELF']."?rid=".$bankDetailsRules[$x]['id']."&status=0'>Disable</a>";
									   
									else
										echo "<a href='".$_SERVER['PHP_SELF']."?rid=".$bankDetailsRules[$x]['id']."&status=1'>Enable</a>";
								?>
							</td>
							<td><a href="<?php 
								echo "add_bank_details_rule.php?rid=".$bankDetailsRules[$x]['id']."&act=e";
							?>">Edit</a></td>
						</tr>
						<?php 
							}
						?>
					</table>
				<?php 
					}
				?>
			</div>
		</div>
	</body>
</html>