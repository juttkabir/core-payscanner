<?php

session_start();
include ("../include/config.php");
include ("security.php");
//debug("****".$_GET); 
//debug($_POST);
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('Y-m-d');//date("d-m-Y");
$loggedUName = $_SESSION["loggedUserData"]["name"];
$returnPage = 'add-transaction.php';

 /*
 * 7897 - Premier FX
 * Rename 'Create Transaction' text in the system.
 * Enable  : string
 * Disable : "0"
 */
$strTransLabel = 'Transaction';
if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
	$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;

//debug($_SESSION);
$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
//$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["benAgentID"] 		= $_POST["distribut"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];
$_SESSION["customerID"]=$_POST['customerID'];

if($_POST["dDate"] != "")
	$_SESSION["transDate"] = $_POST["dDate"];

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];

$_SESSION["discount_request"] = $_POST["discount_request"];	
$_SESSION["bankingType"] = $_POST["bankingType"];
if($_SESSION["transType"]=="Bank Transfer")
	$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
else
	$_SESSION["bankCharges"]="";
	
$_SESSION["distribut"]     = $_POST["distribut"];

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=".$_POST["transID"]."&transType=".$_REQUEST["transType"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y&transType=".$_REQUEST["transType"];
}
if(trim($_POST["transType"]) == "Pick up")
{
	//debug($_POST["transType"]."-->".$_POST["collectionPointID"]."-->".$_POST["selectDist"]."-->".$_SESSION["collectionPointID"]);
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
 if(trim($_POST["selectDist"]) == "" && CONFIG_SELECT_DISTRIBUTOR_PICK_UP_TRANS == "1")
 {
 		//insertError("Please Select Distributor");	
		//redirect($backUrl);
 }
}
if(trim($_POST["benID"]) == "" && CONFIG_TRANS_WITHOUT_BENEFICIARY != '1')
{
	insertError(TE6);	
	redirect($backUrl);
}
if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
	
}else{

	if((isset($_POST["IMFee"]) && trim($_POST["IMFee"]) == "") || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}

if((isset($_POST["localAmount"]) && trim(($_POST["localAmount"]) == "") || $_POST["localAmount"] <= 0))
{
	insertError(TE9);	
	redirect($backUrl);
}
if(isset($_POST["value_date"]) && trim($_POST["value_date"]) == "" && CONFIG_VALUE_DATE_MANDATORY == "1")
{
	insertError(TE32);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == "" && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}


$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";
//debug($cumulativeRuleQuery);
$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);
//debug($currentRuleQuery);
$agentType = getAgentType();
//debug ($agentType);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Confirm <?=$strTransLabel;?></title>
<style type="text/css">
td.myFormat
{
font-size:10px;	
}
td.Arial
{
font-size:10px;
font-family:"Arial",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
/*background:#EEEEEE; */
}
.style2 {
	color: black;
	font-weight: bold;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
}
td.bottom
{
border-bottom: solid 1px #000000;
font-size:10px;
}
td.sign
{
font-size:10px;
}
</style>
<script language="javascript">
function checkForm(theForm,strval){
		//alert (theForm+"-->"+strval);
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = '<? echo $senderAccumulativeAmount ?>';
	var currentRuletransAmount = '<?=$_POST["transAmount"]?>';	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			//document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender;
				document.addTrans.submit();
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> 