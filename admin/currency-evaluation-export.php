<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
		$query = "select id,currency,amount,revaluation_rate FROM ".TBL_CURRENCY_STOCK_LIST." WHERE 1";
		if($_REQUEST["Submit"] == "Search")
		{
			if(!empty($_REQUEST["from_date"]))
			{
				$date = explode("/",$_REQUEST["from_date"]);
				$query .= " and created_on >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
			}
			
			if(!empty($_REQUEST["to_date"]))
			{
				$date = explode("/",$_REQUEST["to_date"]);
				$query .= " and created_on <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
			}
		}
	$query .= " ORDER BY id";
	$currExchData = selectMultiRecords($query);

	$exportType = $_REQUEST["exportType"];

	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "XLS")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=currRevaluationReport_".time().".".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnStartRight = "<td class='rightTD'>";
		$strColumnStartCenter = "<td class='centerTD'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=currRevaluationReport_".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnStartRight = "";
		$strColumnStartCenter = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnStartRight = "<td class='rightTD'>";
		$strColumnStartCenter = "<td class='centerTD'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Currency Revaluation Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							td.rightTD{
								text-align:right;
							}
							td.centerTD{
								text-align:center;
							}
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStartCenter.$strBoldStart."Cur cd".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Currency Description".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."F.C Balance Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."L.C Balance Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Revaluation Rate".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Revalued Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Profit/(Loss)".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Avg. Rate".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Avg. Rate Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	
	$LAmountTotal = 0;
	$FAmountTotal = 0;
	$RAmountTotal = 0;
	$avgRateAmTotal = 0;

	$srNo = 1;
	foreach($currExchData as $key => $val)
	{
		$LAmount = '0';
		$rate = '0';
		$RAmount = '0';
		$PL = '0';
		$currency = $val["currency"];
		$rate = number_format($val["revaluation_rate"],2,'.','');
		$FAmountN = number_format($val["amount"],2,'.','');
		if($rate>0)
			$RAmount = $FAmountN / $rate ;
		$FAmount = number_format($val["amount"],2,'.','');
		$RAmountN =  number_format($RAmount,2,'.','');
		$RAmount =  number_format($RAmount,2,'.','');
		if(!empty($currency))	
			$strCurrencyQ = selectFrom("SELECT cID,description from ".TBL_CURRENCY." WHERE currencyName='".$currency."'");
		//debug($strCurrencyQ);
		if(!empty($strCurrencyQ["cID"])){
		  $currency = $strCurrencyQ["description"]." [".$currency."]";
		  $amountsql = "SELECT SUM(localAmount) as localAmount, sum(rate) as totalRate, count(id) as totalCnt from curr_exchange_account WHERE buysellCurrency='".$strCurrencyQ["cID"]."' AND buy_sell = 'B'";
			if(!empty($_REQUEST["from_date"]))
			{
				$date = explode("/",$_REQUEST["from_date"]);
				$amountsql .= " and created_at >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
			}
			
			if(!empty($_REQUEST["to_date"]))
			{
				$date = explode("/",$_REQUEST["to_date"]);
				$amountsql .= " and created_at <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
			}
			//debug($amountsql);
			$amountData = selectFrom($amountsql);
			$LAmountN = number_format($amountData["localAmount"],2,'.','');
			$LAmount = number_format($amountData["localAmount"],2,'.',',');
			$avgRate = 0;
			$avgRateAm = 0;
			if(!empty($amountData["totalCnt"]))
				$avgRate = number_format(($amountData["totalRate"] / $amountData["totalCnt"]),2,'.','');
			if($avgRate>0){
				$avgRateAm = $FAmountN / $avgRate;
				$avgRate = number_format($avgRate,2,'.','');
				$avgRateAm = ($RAmountN-$avgRateAm);
				$avgRateAm = number_format($avgRateAm,2,'.','');
				$avgRateAmTotal += number_format($avgRateAm,2,'.','');
			}

		}
		$PL = ($LAmountN - $RAmountN);
		$PL =  number_format($PL,2,'.','');
		$LAmountTotal += $LAmountN;
		$FAmountTotal += $FAmountN;
		$RAmountTotal += $RAmountN;	
		if($exportType == "CSV")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr>";

		$strFullHtml .= $strColumnStartCenter.$srNo.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$currency." &nbsp".$strColumnEnd;
		$strFullHtml .= $strColumnStartRight.$FAmount.$strColumnEnd;

		$strFullHtml .= $strColumnStartRight.$LAmount.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$rate.$strColumnEnd;

		$strFullHtml .= $strColumnStartRight.$RAmount.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$PL.$strColumnEnd;
		
		$strFullHtml .= $strColumnStart.$avgRate.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$avgRateAm.$strColumnEnd;
				
		$strFullHtml .= $strRowEnd;
		$srNo++;
	}
	$strFullHtml .= $strRowStart;
	for($s=0;$s<9;$s++)
		$strFullHtml .= $strColumnStart."&nbsp;".$strColumnEnd;
	$strFullHtml .= $strRowEnd;
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strColumnStartRight.$strBoldStart."&nbsp;".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStartRight.$strBoldStart."Total Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStartRight.$strBoldStart.number_format($LAmountTotal,2,'.','').$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strColumnStartRight.$strBoldStart.number_format($RAmountTotal,2,'.','')." ".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStartRight."&nbsp;".$strColumnEnd;
	$strFullHtml .= $strColumnStartRight."&nbsp;".$strColumnEnd;
	$strFullHtml .= $strColumnStartRight.$strBoldStart.number_format($avgRateAmTotal,2,'.','').$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>