<?php
session_start();

include ("../include/config.php");
include ("security.php");
if (isset($_GET['id'])) {
//$userID = $_SESSION["loggedUserData"]["userID"];
//$customerID = $_GET["customerID"];
    $viewReport = selectFrom("SELECT id,name,reportingInstitution,created_on,suspectID,tableForSuspect,remarks,activityFrom,activityTo,amountInvolved,activityCategory,finServices,activityType FROM savedSARs WHERE id =" . $_GET['id']);
    $adminRecord = selectFrom("SELECT name , agentAddress , agentCity , agentCountry , postCode , agentAccounNumber  FROM admin WHERE userID =" . $viewReport["reportingInstitution"]);
    $custRecord = selectFrom("SELECT customerID , lastName , firstName , middleName , Address , City , State , Phone , dob FROM customer WHERE customerID =" . $viewReport["suspectID"]);
    $uidtypeRecord = selectFrom("SELECT id_number , issued_by FROM user_id_types WHERE user_id =" . $viewReport["suspectID"]);
    $sarRecord = selectMultiRecords("SELECT id, activityName FROM suspiciousActivityList WHERE status='Active'");
    $idTypeTitle=selectMultiRecords("SELECT title FROM id_types,user_id_types WHERE id_types.id=user_id_types.user_id AND user_id_types.user_id=" . $viewReport["suspectID"]);

}
else{
    redirect("saved-SAR-reports.php");
}
//$beneRecord = selectFrom("SELECT lastName , firstName , middleName ,  Address , City , State , Zip , Country , Phone , Mobile , dob FROM beneficiary WHERE benID = 7809");

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Suspicious Activity Report</title>
    <!--    <LINK href="images/interface_admin.css" type=text/css rel=stylesheet>-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?php /*<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> */?>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script language="javascript" src="./javascript/functions.js"></script>
    <!-- Include the above in your HEAD tag -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="content/stylesheets/application.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery.fancybox-1.2.6.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/new-agent.css" />
    <link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
    <style>.form-control{font-size: 13px;}</style>
</head>
<body <? if(CONFIG_SKIP_CONFIRM == '1'&& $buttonValue != ""){ ?> onLoad="print()"<? } ?> style="padding:15px;">
<div class="alert introducer">Suspicious Activity Report</div>
<form id="SARReport" action="suspicious-activity-report-conf.php?customerID=<?php echo$customerID;?>" method="post" style="background-color:#d4edda; border-color: #fff">
    <fieldset class="individual group">
        <div>
            <input type="hidden" id="name" name="name">
        </div>
        <div style="background-color: black; width: 50%;">
            <p style="color: white; padding-left: 130px">Always Complete Entire Report</p>
        </div>
        <br>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 1</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Reporting Financial Institution Information</b></div>
        </div>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["name"]?>" placeholder="Name of Financial Institution" readonly>
        </div>
        <br>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentAddress"]?>" placeholder="Address of Financial Institution" readonly>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCity"]?>" placeholder="City" readonly>
            </div>

            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCountry"]?>" placeholder="State" readonly>
            </div>

            <div class="col col-2">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["postCode"]?>" placeholder="Zip Code" readonly>
            </div>
        </div>
        <br>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentAddress"]?>" placeholder="Address of Branch Office where Activity Occured" readonly>
        </div>
        <input type="checkbox"   style="margin-left: 15px;margin-right: -70px; width: 20px;">
        <div><p></p>
            <p style="position: relative; top: -27px; margin-left: 35px; margin-bottom: -25px">Multiple Branches Include Information in Narrative</p>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCity"]?>" placeholder="City" readonly>
            </div>

            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCountry"]?>" placeholder="State" readonly>
            </div>

            <div class="col col-2">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["postCode"]?>" placeholder="Zip Code" readonly>
            </div>
        </div>
        <br>
        <div style="margin-left: 15px"><p>If Institution Closed, Date Closed.</p></div>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="date" readonly>
        </div>
        <br>
        <div style="margin-left: 15px"><p>Account Number(s), if any</p></div>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentAccounNumber"]?>" placeholder="Account 1" readonly>
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="text" value="" placeholder="Account 2" readonly>
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="text" value="" placeholder="Account 3" readonly>
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="text" value="" placeholder="Account 4" readonly>
        </div>
        <div style="position: relative; top: -220px; margin-left: 410px;margin-bottom: -170px">
            <p style="margin-left: 40px"> Closed?</p>
            <div>
                <input type="radio" name="Acc1" style="margin-left: 28px;margin-right: 5px; margin-top:10px; width: 20px">Yes
                <input type="radio" name="Acc1" style="margin-left: 7px;margin-right: 5px; width: 20px">No
            </div>
            <div>
                <input type="radio" name="Acc2" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc2" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
            <div>
                <input type="radio" name="Acc3" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc3" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
            <div>
                <input type="radio" name="Acc4" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc4" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
        </div>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 2</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Customer/Sender Information:</b></div>
        </div>
        <div style="margin-left: 0px" class="row">
            <div class="col col-5">Last Name or Name of Entry
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["lastName"]?>" placeholder="Last Name or Name of Entry" readonly>
            </div>
            <div class="col col-3">First Name
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["firstName"]?>" placeholder="First Name" readonly>
            </div>
            <div class="col col-3">Middle Name
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["middleName"]?>" placeholder="Middle Name" readonly>
            </div>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-8">Address
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["Address"]?>" placeholder="Address" readonly>
            </div>
            <div class="col col-3">City
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["City"]?>" placeholder="City" readonly>
            </div>
        </div>
        <br>
        <div>
            <p>&nbsp; &nbsp; <b>Govt issued identification:</b></p>
            <div style="margin-left: 0px" class="row">

                <?php
                foreach ($idTypeTitle AS $GovtIdentity){
                    echo'<input type="radio" name="GOVT.1" value="'.$GovtIdentity["title"].'" style="margin-left: 24px;margin-right: 9px; margin-top:3px; width: 20px" disabled >'.$GovtIdentity["title"];
                }
                ?>
<!--                <div class="col col-3" >-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 24px;margin-right: 9px; margin-top:3px; width: 20px">Driver's License/State ID-->
<!--                </div>-->
<!--                <div class="col col-2">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Passport-->
<!--                </div>-->
<!--                <div class="col col-2">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Alien Registration-->
<!--                </div>-->
<!--                <div class="col col-5">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Other-->
<!--                    <div class="col col-6" style="position: relative; top: -25px; margin-bottom: -10px;margin-left: 110px;">-->
<!--                        <input class="required form-control" id="source" name="source" type="text" value="" readonly>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="col col-3">Number
                    <input class="required form-control" id="source" name="source" type="text" value="<?=$uidtypeRecord["id_number"]?>" placeholder="Number" readonly>
                </div>
                <div class="col col-3">Issuing State/Country
                    <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["State"]?>" placeholder="Issuing State/Country" readonly>
                </div>
            </div>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div style="margin-top: 17px" class="col col-4">SSN,EIN or TIN
                <input class="required form-control" id="source" name="source" type="text" value="" placeholder="SSN,EIN or TIN" readonly>
            </div>
            <div style="margin-top: 17px" class="col col-3">Date of Birth
                <div>
                    <input class="required form-control" id="source" name="source" value="<?=$custRecord["dob"]?>" type="date" readonly>
                </div>
            </div>
            <div style="margin-top: 17px" class="col col-4">Telephone Number
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["Phone"]?>" placeholder="Telephone Number" readonly>
            </div>
        </div>
        <br>
        <br>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 3</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Suspicious activity Information:</b></div>
        </div>
        <div>
            <p> <b>&nbsp;&nbsp; Date or date range of suspicious activity:</b></p>
            <div class="row">
                <div style="margin-left: 30px; margin-top: 10px" class="col col-4">
                    From
                    <div style="position: relative; top: -30px; margin-left: 35px">
                        <input class="required form-control" id="source" name="activityFrom" type="date" value="<?=$viewReport["activityFrom"]?>"  readonly>
                    </div>
                </div>
                <div style="margin-left: 30px; margin-top: 10px" class="col col-4">
                    To
                    <div style="position: relative; top: -30px; margin-left: 20px">
                        <input class="required form-control" id="source" name="activityTo" type="date" value="<?=$viewReport["activityTo"]?>" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col col-4" style="margin-right: -95px">
                    <p><b>&nbsp;&nbsp; Total amount involved in suspicious activity</b></p>
                </div>
                <div class="col col-6">
                    <input type="checkbox" style="margin-right: 7px; margin-left: 100px; width: 20px">Amount Unknown
                </div>
            </div>
            <div style="margin-top: 17px" class="col col-4">
                <input class="required form-control" id="amountInvolved" name="amountInvolved" type="text" value="<?=$viewReport["amountInvolved"]?>" placeholder="Enter Amount" readonly>
            </div>
        </div>
        <br>
        <div>
            <p><b>&nbsp;&nbsp; Category of suspicious activity (Check all that apply)</b></p>
        </div>
        <div style="margin-left: 0px" class="row">
            <?php
            $arrActivityCategory = explode(",", trim($viewReport["activityCategory"]));
            foreach ($arrActivityCategory as $activityCategory) {
                echo '<input type="checkbox" name="activityCategory[]" value="'.$activityCategory.'" style="margin-left: 20px;margin-right: 5px; margin-top:3px; width: 20px" checked disabled>'.$activityCategory;
            }
            ?>
        </div>
        <br>
        <div>
            <p>
                <b>&nbsp;&nbsp; Financial services involved in the suspicious activity and character of the suspicious activity. <br>
                    &nbsp;&nbsp; Including unusual use (Check all that apply)</b>
            </p>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <?php
            $arrFinServices = explode(",", trim($viewReport["finServices"]));
            foreach ($arrFinServices as $finServices) {
                echo '<input type="checkbox" name="finServices[]" value="'.$finServices.'" style="margin-left: 20px;margin-right: 5px; margin-top:3px; width: 20px" checked disabled>'.$finServices;
            }
            ?>
            <!--            <div>-->
            <!--                <input type="checkbox" name="finServices[]" value="Money order" style="margin-left: 20px;margin-right: 5px; margin-top:-4px; width: 20px">Money order-->
            <!--            </div>-->
            <!--            <div>-->
            <!--                <input type="checkbox" name="finServices[]" value="Traveller's check" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Traveller's check-->
            <!--            </div>-->
            <!--            <div>-->
            <!--                <input type="checkbox" checked name="finServices[]" value="Money transfer" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Money transfer-->
            <!--            </div>-->
            <!--            <div>-->
            <!--                <input type="checkbox" name="finServices[]" value="Other(specify)" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Other(specify)-->
            <!---->
            <!--                <div class="" style="position: relative; top: -23px; margin-bottom: -10px ; margin-left: 180px">-->
            <!--                    <input class="required form-control" id="source" name="finServicesOther" type="text" value="" readonly>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--            <div>-->
            <!--                <input type="checkbox" name="finServices[]" value="Currency exchange" style="margin-left: 20px;margin-right: 5px; margin-top:-4px; width: 20px">Currency exchange-->
            <!--            </div>-->
        </div>
        <br>
        <div>
            <p>&nbsp; &nbsp; <b>Check all of the following that apply</b></p>
        </div>
        <div class="row">
            <?php
                $arrActivityType = explode(",", trim($viewReport["activityType"]));
                foreach ($arrActivityType as $activityType) {
                    if ($activityType != "") {
                        $activityType = selectFrom("select activityName from suspiciousActivityList where id = $activityType and status = 'Active'");
                        echo '<div class="col col-6"><input type="checkbox" name="activityType[]" value="'.$activityType["activityName"].'" style="margin-left: 20px;margin-right: 5px; margin-top:3px; width: 20px" checked disabled>'.$activityType["activityName"]."</div>";
                    }
                }
            ?>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-11"><p><b> REMARKS</b></p>
                <textarea name="remarks" id="remarks" style="width:100%" rows="7" readonly><?php echo $viewReport["remarks"]; ?></textarea>
            </div>
        </div>
        <br>
        <script type="text/javascript">
            function save() {
                if (document.getElementById("amountInvolved").value == "" || document.getElementById("amountInvolved").value == null) {
                    alert("Amount can not be empty");
                } else {
                    document.getElementById("name").value = prompt("Enter Name for Report: ");
                    document.getElementById("SARReport").submit();
                }
            }
            $("#print").click(function () {
                $("#print").css("display","none");
                print();
            });
        </script>
    </fieldset>
</form>
</body>
</html>