<?
	// Usman Ghani
	session_start();
	include ("../include/config.php");
	include ("security.php");
	include ("javaScript.php");
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	//$parentID = $_SESSION["loggedUserData"]["userID"];
	// Handle form submit request
	$isToSubmitForm = "1";
	if( !empty($_REQUEST["addFormSubmit"]))
	{
		if ( !empty($_REQUEST["title"]) )
		{
			// First check if an ID Type with the same title already exists.

			$previousRecord = selectFrom( "select * from id_types where title = '" . trim($_REQUEST["title"]) . "'" );
			if ( !empty( $previousRecord ) )
			{
				$formTitleError = "<span class='formError'>An ID Type with the same title already exists.</span>";
			}
			else
			{
				
				 if(defined("CONFIG_MULTI_ID_MANDATORY_SEPARATE") && CONFIG_MULTI_ID_MANDATORY_SEPARATE=="1"){
					$mandFieldTypes = ",mandatorySender,mandatoyBeneficiary";
					$custFieldValues = ",'".$_REQUEST["mandatorySender"]."'"; 
					$benFieldValues = ",'".$_REQUEST["mandatoyBeneficiary"]."'";
				 }
				
				if(CONFIG_SENDER_AS_COMPANY == "1"){
					$senderAsCompanyFieldTypes = ",show_on_sender_as_company";
					$senderAsCompanyFieldValues = ",'".$_REQUEST["showOnSenderAsCompany"]."'"; 
				}
				if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){
					$benAsCompanyFieldTypes = ",show_on_beneficiary_as_company";
					$benAsCompanyFieldValues = ",'".$_REQUEST["showOnBeneficiaryAsCompany"]."'"; 
				}
				$q = "insert into id_types
						(
							title, 
							mandatory, 
							show_on_sender_page, 
							show_on_ben_page
							".$mandFieldTypes ."
							".$senderAsCompanyFieldTypes ."
							".$benAsCompanyFieldTypes ."
													
						)
					  values
						(
							'" . $_REQUEST["title"] . "', 
							'" . $_REQUEST["mandatory"] . "', 
							'" . ( !empty($_REQUEST["showOnSenderPage"]) ? "Y" : "N" ) . "', 
							'" . ( !empty($_REQUEST["showOnBeneficiaryPage"]) ? "Y" : "N" ) . "'
							".$custFieldValues."
							".$benFieldValues."
							".$senderAsCompanyFieldValues."
							".$benAsCompanyFieldValues."
							
						)";

				insertInto( $q );
				//debug($q);
				$statusMessage = "New ID Type added successfully.";
				$isToSubmitForm = "";
			}
		}
		else
		{
			$formTitleError = "<span class='formError'>Title is required</span>";
		}
	}
	else if ( !empty( $_REQUEST["newActiveStatus"] ) )
	{
		$q = "update id_types "
				  . " set "
						. " active='" . $_REQUEST["newActiveStatus"] . "' "
				  . " where "
				  		. " id='" . $_REQUEST["id"] . "'";

		update( $q );
		$statusMessage = "Status changed successfully.";
	}

	// Fetch already availble id types
	$q = "select * from id_types";
	$idTypes = selectMultiRecords($q);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage ID Types</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="css/general.css" rel="stylesheet" type="text/css">

<? // include ("javaScript.php"); ?>

<script language="javascript">

	function checkForm(theForm)
	{
		if ( trim( theForm.title.value ) == "" )
		{
			alert("Please provide a title.");
			theForm.title.focus();
			return false;
		}
		if ( /^\d+$/.test( theForm.title.value ) )
		{
			alert("Please provide a valid title.");
			theForm.title.focus();
			return false;
		}
		return true;
	}

	function trim(stringToTrim)
	{
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}

	function submitForm( formId )
	{
		formElement = document.getElementById(formId);
		formElement.submit();
	}
</script>

</head>
<body>
<br />
<span class="pageHeading">
Manage ID Types
</span>
<br /><br />
<br />
<table width="687" border="1" align="center" cellpadding="5" cellspacing="1">
	<tr>
		<td colspan="8" class="tableHeading">
			ID Types
		</td>
	</tr>
	<tr>
		<td width="57" class="fieldLable">ID Title</td>
		<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE !="1"){  ?>
		<td width="79" class="fieldLable">Mandatory</td>
		<? }else{ ?>
		<td width="79" class="fieldLable">Mandatory Sender</td>
		<td width="79" class="fieldLable">Mandatory Beneficiary</td>
		<? } ?>
		<td width="42" align="center" valign="top" class="fieldLable">Status</td>
		<td width="98" class="fieldLable">On Sender Form</td>
		<td width="118" class="fieldLable">On Beneficiary Form</td>
		<? if(CONFIG_SENDER_AS_COMPANY == "1"){?>
		<td width="98" class="fieldLable">On Sender As Company</td>
		<? } if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){ ?>
		<td width="118" class="fieldLable">On Beneficiary As Company</td>
		<? } ?>
		<td width="120" align="center" valign="top" class="fieldLable">
			Operation</td>
	</tr>
	<?
		$altRow = 0;
		foreach( $idTypes as $idType )
		{
			$altRow = !$altRow;
			?>
			<tr valign="top" class="<?=($altRow ? "altRow2" : "altRow1");?>">
				<td>
					<strong><?=$idType["title"]; ?></strong></td>
				<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE !="1"){  ?>
				<td align="center">
					<?=($idType["mandatory"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?>
				</td>
				<? }else{ ?>
					<td align="center">
					<?=($idType["mandatorySender"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?>
				</td>
					<td align="center">
					<?=($idType["mandatoyBeneficiary"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?>
				</td>
				<? } ?>
				<td align="center"><?=($idType["active"] == "Y" ? "<span class='greenText'>Enabled</span>" : "<span class='redText'>Disabled</span>"); ?></td>
			  <td align="center" valign="top"><?=($idType["show_on_sender_page"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?></td>
				<td align="center" valign="top"><?=($idType["show_on_ben_page"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?></td>
				<? if(CONFIG_SENDER_AS_COMPANY == "1"){?>
				  <td align="center" valign="top">
				 	 <?=($idType["show_on_sender_as_company"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?>
				  </td>
				  <? } if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){ ?>
				
				<td align="center" valign="top">
					<?=($idType["show_on_beneficiary_as_company"] == "Y" ? "<span class='greenText'>Yes</span>" : "<span class='redText'>No</span>"); ?>
				</td>
				<? } ?>
				<td align="center" valign="top">
						<a href="javascript:;" class="link1" onClick="window.open('id-type-edit.php?id=<?=$idType["id"];?>', '', 'width=450,height=250');">Edit</a>
						|
						<?
							$newStatus = ($idType["active"] == "Y" ? "N" : "Y");
						?>
						<a href="id-types.php?newActiveStatus=<?=$newStatus;?>&id=<?=$idType["id"];?>" onClick="" class="link1"><?=($idType["active"] == "Y" ? "Disable" : "Enable"); ?></a>
			  </td>
			</tr>
			<?
		}
	?>
	<?
		if( !empty( $statusMessage ) )
		{
			?>
				<tr>
					<td colspan="8" class="successMessage">
						<?=$statusMessage; ?>
					</td>
				</tr>
			<?
		}
	?>
	<tr>
		<td colspan="8" class="fieldLable">Add Another ID Type</td>
	</tr>
	<tr>
		<td colspan="6">
			<form method="post" onsubmit="return checkForm(this);">
				<table border="1" cellspacing="1" cellpadding="5" align="center">
					<tr>
						<td class="fieldLable">
							ID Title
						</td>
						<td>
							<input type="text" size="40" id="title" name="title" value="<?=(!empty($_REQUEST["title"]) ? $_REQUEST["title"] : "");?>" /> <?=(!empty($formTitleError) ? "<br />" . $formTitleError : ""); ?>
						</td>
					</tr>
					<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE !="1"){  ?>
					<tr>
						<td class="fieldLable">
							Mandatory
						</td>
						<td>
							<input type="radio" name="mandatory" value="Y" <?=(!empty($_REQUEST["mandatory"]) && $_REQUEST["mandatory"] == "Y" ? "checked='checked'" : "");?> />&nbsp;Yes&nbsp;&nbsp;
							<input type="radio" name="mandatory" value="N" <?=(empty($_REQUEST["mandatory"]) || $_REQUEST["mandatory"] == "N" ? "checked='checked'" : "");?> />&nbsp;No
						</td>
					</tr>
					<? } ?>
					<tr>
						<td valign="top" class="fieldLable">Available on</td>
						<td>
							<input type="checkbox" value="Y" name="showOnSenderPage" <?=(!empty($_REQUEST["showOnSenderPage"]) && $_REQUEST["showOnSenderPage"] == "Y" ? "checked='checked'" : "");?> /> 
							Add Sender Page 
								<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE == "1"){  ?>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<font color="#005b90"><strong>Mandatory</strong></font> &nbsp; <input type="checkbox" value="Y" name="mandatorySender" <?=(!empty($_REQUEST["mandatorySender"]) && $_REQUEST["mandatorySender"] == "Y" ? "checked='checked'" : "");?> />
							<? } ?>
							<br />
							<input type="checkbox" value="Y" name="showOnBeneficiaryPage" <?=(!empty($_REQUEST["showOnBeneficiaryPage"]) && $_REQUEST["showOnBeneficiaryPage"] == "Y" ? "checked='checked'" : "");?> /> Add Beneficiary Page
							<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE == "1"){  ?>&nbsp; <font color="#005b90"><strong>Mandatory</strong></font> &nbsp; <input type="checkbox" value="Y" name="mandatoyBeneficiary" <?=(!empty($_REQUEST["mandatoyBeneficiary"]) && $_REQUEST["mandatoyBeneficiary"] == "Y" ? "checked='checked'" : "");?> />
							<? } ?>
							<? if(CONFIG_SENDER_AS_COMPANY == "1") { ?>
							<br />
							<input type="checkbox" value="Y" name="showOnSenderAsCompany" <?=(!empty($_REQUEST["showOnSenderAsCompany"]) && $_REQUEST["showOnSenderAsCompany"] == "Y" ? "checked='checked'" : "");?> /> 
								Add Sender As Company 
							<? } ?>
							  
							<? if(CONFIG_BENEFICIARY_AS_COMPANY == "1") { ?>
							<br />
							<input type="checkbox" value="Y" name="showOnBeneficiaryAsCompany" <?=(!empty($_REQUEST["showOnBeneficiaryAsCompany"]) && $_REQUEST["showOnBeneficiaryAsCompany"] == "Y" ? "checked='checked'" : "");?> /> Add Beneficiary As Compnay
							
						   <? } ?>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					    <td>
							<input type="hidden" name="addFormSubmit" id="addFormSubmit" value="<?=$isToSubmitForm?>"/>
							<input type="submit" value="Save" />&nbsp;</td>
					</tr>
				</table>
			</form>		</td>
	</tr>
</table>
</body>
</html>