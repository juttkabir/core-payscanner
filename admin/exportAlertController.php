<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	$systemCode = SYSTEM_CODE;
    $manualCode = MANUAL_CODE;
	
	if(!empty($_REQUEST["exportQuery"]))		
	$exportQueryMulti=base64_decode($_REQUEST["exportQuery"]);
	$arrIdExpiredData	= selectMultiRecords($exportQueryMulti);
	
	//debug($arrIdExpiredData);
	$exportType = $_REQUEST["exportType"];
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "XLS")
	{
	
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=Alert_Controller.".xls); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table border='1'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td align='left'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=Alert_Controller.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	elseif($exportType == "HTML")
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Alert Controller</title>
							<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
							<script language="javascript" type="text/javascript">
								jQuery(document).ready(function(){
									$("#printBtn").click(function(){
										$("#printBtn").hide();
										print();	
									});
								});
							</script>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
		$strHtmlButton = '<br /><div style="text-align:center;"><input type="button" id="printBtn" value="Print"></div>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."User Name".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Reference Number".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."User Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."ID Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Alert Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Expiry date".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Message".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	foreach($arrIdExpiredData as $key => $val)
	{	   
		$strIDtypeName = selectFrom("SELECT title FROM ".TBL_ID_TYPES." WHERE id = '".$val["id_type_id"]."'");
		
		$strFullHtml .= $strRowStart;
		$strFullHtml .= $strColumnStart.$val["firstName"]." ".$val["middleName"]." ".$val["lastName"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["accountName"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["userType"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strIDtypeName["title"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["alertType"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["expiry_date"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["message"]." ".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
	}
	   	   
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlButton;
	$strFullHtml .= $strHtmlClosing;
	echo $strFullHtml;
?>
