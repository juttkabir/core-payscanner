<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'
$isAgentAnD = 'N';
$isBankAnD = 'N';

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == ""){
	$offset = 0;
}
		
if($limit == 0){
	$limit=50;
}
	
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == ""){
	$sortBy = " transDate";
}

$trnsid = $_POST["trnsid"];
$ttrans = $_POST["totTrans"];
if($_POST["btnAction"] != "")
{
	for ($i=0;$i< $ttrans;$i++)
	{
		
		if(count($trnsid) >= 1)
		{
			if($trnsid[$i] != "")
			{
			
			if($_POST["btnAction"] != "")
			{
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				 if(CONFIG_EXCLUDE_COMMISSION == '1')
					{
						$agentLedgerAmount = $contentTrans["totalAmount"] - $contentTrans["AgentComm"];
					}else{
						$agentLedgerAmount = $contentTrans["totalAmount"];
					}
				 
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $transType  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];
				 $transDate = date("F j, Y", strtotime("$transDate"));
			
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				}
				// getting BEneficiray agent Email
				if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				}
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

if($_POST["btnAction"] == "Authorize")
{

if($contentTrans["createdBy"] == 'CUSTOMER')
{
			$subject = "Status Updated";

			$custContents = selectFrom("select * from cm_customer where c_id= '". $custID ."'");
			$custEmail = $custContents["c_name"];
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["FirstName"];
			$custMiddleName = $custContents["MiddleName"];
			$custLastName = $custContents["LastName"];
			$custCountry = $custContents["c_country"];
			$custCity = $custContents["c_city"];
			$custZip = $custContents["c_zip"];
			$custLoginName = $custContents["username"];
			$custEmail = $custContents["c_email"];
			$custPhone = $custContents["c_phone"];


			$benContents = selectFrom("select * from cm_beneficiary where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction is updated </p></td>
  </tr>
			
			
					  <tr>
						<td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
					  </tr>				
			
								  <tr>
						<td width='205'>&nbsp;</td>
						<td width='295'>&nbsp;</td>
					  </tr>
							  <tr>
						<td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
					  </tr>			
			
			
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Transaction Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
						
			
					  <tr>
						<td> Transaction Type:  ".$transType." </td>
						<td> Status: $Status </td>
					  </tr>
					  <tr>
						<td> Transaction No:   $imReferenceNumber </td>
						<td> Transaction Date:  ".$transDate."  </td>
					  </tr>
					  <tr>
						<td><p>Authorised Date: $transDate</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					
					
					  <tr>
						<td><p><strong>Beneficiary Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Address:  $benAddress </td>
						<td> Postal / Zip Code:  $benZip </td>
					  </tr>
					  <tr>
						<td> Country:   $benCountry   </td>
						<td> Phone:   $benPhone   </td>
					  </tr>
					  <tr>
						<td><p>Email:  $benEmail   </p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>";			
			if(trim($transType) == 'Bank Transfer')
			{
			$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
			
			$_SESSION["bankName"] = $rstRow["bankName"];
			$_SESSION["branchCode"] = $rstRow["branchCode"];
			$_SESSION["branchAddress"] = $rstRow["branchAddress"];
			$_SESSION["swiftCode"] = $rstRow["swiftCode"];
			$_SESSION["accNo"] = $rstRow["accNo"];
			
					$message.="  <tr>
						<td><p><strong>Beneficiary Bank Details </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Bank Name:  ".$_SESSION['bankName']."  </td>
						<td> Acc Number:  ".$_SESSION['accNo']."  </td>
					  </tr>
					  <tr>
						<td> Branch Code:  ".$_SESSION['branchCode']."  </td>
						<td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
					  </tr>
					  <tr>
						<td><p>Swift Code:  ".$_SESSION['swiftCode']."  </p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					";
			$_SESSION["bankName"] = "";
			$_SESSION["branchCode"] = "";
			$_SESSION["branchAddress"] = "";
			$_SESSION["swiftCode"] = "";
			$_SESSION["accNo"] = "";
			}
			elseif(trim($transType) == "Pick up")
			{
			
			$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
			$agentname = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			
					$message .= "
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Collection Point Details </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Agent Name : $agentname </td>
						<td> Contact Person:  $contactperson </td>
					  </tr>
					  <tr>
						<td> Company:  $company </td>
						<td> Address:  $address </td>
					  </tr>
					  <tr>
						<td>Country:   $country</td>
						<td>City:  $city</td>
					  </tr>
					  <tr>
						<td>Phone:  $phone</td>
						<td>&nbsp;</td>
					  </tr> ";
}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used
$exRate 		= $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}

/*if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}*/


if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}
	
/*if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}*/

$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
else
{

			$subject = "Status Updated";

			$custContents = selectFrom("select * from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["firstName"];
			$custMiddleName = $custContents["middleName"];
			$custLastName = $custContents["lastName"];
			$custCountry = $custContents["Country"];
			$custCity = $custContents["City"];
			$custZip = $custContents["Zip"];
			$custEmail = $custContents["email"];
			$custPhone = $custContents["Phone"];


			$benContents = selectFrom("select * from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction status is updated </p></td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thanks for choosing $company as your money transfer company. We will keep you informed through email regarding your transaction till the money reached to your beneficiary. Please see the attached your transaction detail and some important information. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$transType." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> Transaction No:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$transDate."  </td>
  </tr>
  <tr>
    <td><p>Authorised Date: $tran_date</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";

if(trim($transType) == 'Bank Transfer')
{

  		$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
		$rstRow = mysql_fetch_array($nResult);

		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];

$message.="  <tr>
    <td><p><strong>Beneficiary Bank Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Bank Name:  ".$_SESSION['bankName']."  </td>
    <td> Acc Number:  ".$_SESSION['accNo']."  </td>
  </tr>
  <tr>
    <td> Branch Code:  ".$_SESSION['branchCode']."  </td>
    <td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
  </tr>
  <tr>
    <td><p>Swift Code:  ".$_SESSION['swiftCode']."  </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
";
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["swiftCode"] = "";
		$_SESSION["accNo"] = "";
}
elseif(trim($transType) == "Pick up")
{

$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
$agentname = $cContents["cp_corresspondent_name"];
$contactperson = $cContents["cp_corresspondent_name"];
$company = $cContents["cp_branch_name"];
$address = $cContents["cp_branch_address"];
$country = $cContents["cp_country"];
$city = $cContents["cp_city"];
$phone = $cContents["cp_phone"];
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));

$message .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collection Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used 
$exRate = $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}


$benAgentID = $contentTrans["benAgentID"];

$packageQuery="select * from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
			$agentPackage = selectFrom($packageQuery);

			$package = $agentPackage["commPackage"];
			
			switch ($package)
				  {
				  	case "001": // Fixed amount per transaction
					{
						$agentCommi = $agentPackage["agentCommission"];
						$commType = "Fixed Amount";
						break;
					}
				  	case "002": // percentage of total transaction amount
					{
						$agentCommi = ( $amount* $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of total Amount";
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of Fee";
						break;
					}
					case "004":
					{
						$agentCommi = 0;
						$commType = "None";
						break;
					}
				}				  
				   
		 $agentCommi;



$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
}
/**********************************************************/
				
			}

sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

			if($_POST["btnAction"] == "Authorize")
			{
				// Update agents account limit;
				update("update " . TBL_ADMIN_USERS . " set limitUsed  = (limitUsed  + '$agentLedgerAmount') where userID = '". $contentTrans["custAgentID"] ."'");
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");				
				
				/*$today = date("Y-m-d");;  
				$insertQuery = "insert into bank_account values('','".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$username', '". $trnsid[$i] ."','Transaction Authorized')";
				$q=mysql_query($insertQuery);*/
				
				$today = date("Y-m-d");  
				if(CONFIG_LEDGER_AT_CREATION != "1")
				{
					if($contentTrans["createdBy"] != 'CUSTOMER')
					{
						if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
								$typeAgent = selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." where userID = '".$contentTrans["custAgentID"]."'");
							$type = $typeAgent["agentType"];
							if($type=="Sub")
							{
								$agent=$typeAgent["parentID"];
							}else{
								$agent=$contentTrans["custAgentID"];
							}
							if(CONFIG_AnD_ENABLE == '1' && $typeAgent["isCorrespondent"] == 'Y')
							{
								$isAgentAnD = 'Y';
							}

							////////////////Either to insert into Agent or A&D Account////////
							if($isAgentAnD == 'Y')
							{
								$insertQuery = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description) values('$agent', '$today', 'WITHDRAW', '$agentLedgerAmount', '$userID', '". $trnsid[$i]."', 'Transaction Authorized')";
							}else{
								$insertQuery = "insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description) values('$agent', '$today', 'WITHDRAW', '$agentLedgerAmount', '$userID', '". $trnsid[$i] ."', 'Transaction Authorized')";
							}
							////////////////////////////////////
							$q=mysql_query($insertQuery);
							
							$checkBalance = selectFrom("select balance from ".TBL_ADMIN_USERS." where userID = '".$agent."'");						
							$newBalance = $checkBalance["balance"] - $agentLedgerAmount; 
							$update_Balance = "update ".TBL_ADMIN_USERS." set balance = '".$newBalance."' where userID= '". $agent."'";
							update($update_Balance);
							
							agentSummaryAccount($agent, "WITHDRAW", $agentLedgerAmount); 
						}
					}
				}
				if(CONFIG_POST_PAID != '1')
				{
					$checkBalance2 = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$contentTrans["benAgentID"]."'");						
					
					if(CONFIG_AnD_ENABLE == '1' && $checkBalance2["isCorrespondent"] == 'Y')
					{
						$isBankAnD = 'Y';
					}
					
					$currentBalanceBank = $checkBalance2["balance"]	- $contentTrans["transAmount"];				
					update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$contentTrans["benAgentID"]."'");
					////////////////Either to insert into Bank or A&D Account////////
					if($isBankAnD == 'Y')
					{
						insertInto("insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description, actAs) values('".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".$contentTrans["transAmount"]."', '$userID', '". $trnsid[$i]."', 'Transaction Authorized','Distributor')");
					}else{
						insertInto("insert into bank_account values('', '".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["localAmount"] : $contentTrans["transAmount"])."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contentTrans["currencyTo"] : $contentTrans["currencyFrom"])."', '$userID', '". $trnsid[$i] ."', 'Transaction Authorized')");
					}
				}
				
				insertError("Transaction Authorized successfully.");

				$descript = "Transaction is Authorized";
				activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);		 

				agentSummaryAccount($contentTrans["benAgentID"], "WITHDRAW", $contentTrans["transAmount"]); 

			}
		}

		}
	}
redirect("requestforBD.php?msg=Y");
}

$query = "select * from ". TBL_TRANSACTIONS." where 1 and transStatus = 'RequestforBD'";

$query .= " order by transDate DESC";

$contentsTrans = selectMultiRecords($query);
$allCount = count($contentsTrans);

$query .= " LIMIT $offset , $limit";
 
$contentsTrans = selectMultiRecords($query);
		
?>
<html>
<head>
	<title>Request for Backdated Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Backdated Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="requestforBD.php" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center"><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
			  <td width="74"><span class="style1">Edit </span></td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><?  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>"; ?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER") {?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-customer-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>&back=requestforBD.php&focusID=transSend" class="style2">Edit</a></td>
					<? } else { ?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>&back=requestforBD.php&focusID=transSend" class="style2">Edit</a></td>
					<? } ?>
					  
				</tr>
				<?
			}
			?>
			
			<? for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["totalAmount"] .  " " . $onlinecustomer[$j]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>


				  <td align="center" bgcolor="#FFFFFF"><?  $tempTransId = $onlinecustomer[$j]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>"; ?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER") {?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-customer-transaction.php?transID=<? echo $onlinecustomer[$j]["transID"]?>&back=requestforBD.php&focusID=transSend" class="style2">Edit</a></td>
					<? } else { ?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-transaction.php?transID=<? echo $onlinecustomer[$j]["transID"]?>&back=requestforBD.php&focusID=transSend" class="style2">Edit</a></td>
					<? } ?>
					  
				</tr>
				<?
			}
			?>
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
			  <td colspan="10" align="center"><input type='hidden' name='totTrans' value='<? echo ($i + $j); ?>'>
			  <input name="btnAction" type="submit" value="Authorize"></td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td align="center"> No Transaction found in the database.
            </td>
          </tr>
          <tr>
            <td align="center"> <a href="main.php" class="style2">Main Menu</a>
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>