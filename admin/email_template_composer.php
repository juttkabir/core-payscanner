<?php
/**
 * @package : Online Module
 * @subpackage: Email Template Composer
 * @author: Mirza Arslan Baig
 */
session_start();
include "../include/config.php";
$date_time = date('d-m-Y  h:i:s A');
include "security.php";
//$arrUsers = explode(",", USERS_LIST_EMAIL_TEMPLATE);
$arrUsers = unserialize(USERS_LIST_EMAIL_TEMPLATE);


	$userID  = $_SESSION["loggedUserData"]["userID"];
//$agentType = getAgentType();
 $where_sql="";
if($_SESSION["loggedUserData"]['adminType']=='Agent')
{

 $where_sql="   AND agentID='".	$userID."' ";
}

// $_GET["action"];

function generateEmailTemplateRefNumber($event) {
	$strQueryEvents = "SELECT name FROM " . TBL_EVENTS . " WHERE id = '$event'";
	$arrEvent = selectFrom($strQueryEvents);
	$strQueryRefNumber = "SELECT refNumber FROM " . TBL_EMAIL_TEMPLATES . " WHERE eventID = '$event' ORDER BY createdDate DESC";
	$arrLargestRefNumber = selectFrom($strQueryRefNumber);
	if (empty($arrLargestRefNumber)) {
		$pre = substr($arrEvent['name'], 0, 3);
		$pre = ucfirst($pre);
		$refNumber = $pre . "-1";
		return $refNumber;
	} else {
		$arrRefNumber = explode("-", $arrLargestRefNumber['refNumber']);
		$strPre = $arrRefNumber[0];
		$counter = $arrRefNumber[count($arrRefNumber) - 1];
		$refNumber = $strPre . "-" . ($counter + 1);
		return $refNumber;
	}
}
if (isset($_POST['saveTemplate'])) {

	$strEvent = $_POST['event'];
	$agentName = $_POST['agentName'];
	$strFromEmail = trim($_POST['fromEmail']);
	$strFromName = trim($_POST['fromName']);
	$strTo = $_POST['toUsers'];

	for ($i = 0; $i < count($strTo); $i++) {
		$mailTo .= $strTo[$i] . "/";
	}

	$strCc = $_POST['cc'];
	$strBcc = $_POST['bcc'];
	$strSubject = trim($_POST['subject']);
	//$strSalutation = trim($_POST['salutation']);
	$strBody = $_POST['body'];
	$strCurrentDate = date("Y-m-d h:i:s");
	$strRefNumber = generateEmailTemplateRefNumber($strEvent);
	$strQueryTemplate = "INSERT INTO " . TBL_EMAIL_TEMPLATES . " (`refNumber`, `eventID`, `fromEmail`, `fromName`, `to`, `cc`, `bcc`, `subject`, `body`, `createdDate`, `updatedDate`, `agentID`) VALUES('$strRefNumber', '$strEvent', '$strFromEmail', '$strFromName', '$mailTo', '$strCc', '$strBcc', '$strSubject', '$strBody', '$strCurrentDate', '$strCurrentDate', '$agentName')";

	if (insertInto($strQueryTemplate)) {
		$strMsg = "Template saved successfully.";
	} else {
		$strError = "Template cannot saved!!";
	}

}

if (isset($_POST['updateTemplate'])) {
	$strEvent = $_POST['event'];
	$strFromEmail = trim($_POST['fromEmail']);
	$strFromName = trim($_POST['fromName']);
	$strTo = $_POST['toUsers'];
	for ($i = 0; $i < count($strTo); $i++) {
		$mailTo .= $strTo[$i] . "/";
	}

	$strCc = $_POST['cc'];
	$strBcc = $_POST['bcc'];
	$strSubject = trim($_POST['subject']);
	//$strSalutation = trim($_POST['salutation']);
	$strBody = $_POST['body'];
	$strCurrentDate = date("Y-m-d h:i:s");
	$intTemplateID = $_POST['templateID'];
	$strUpdateQuery = "UPDATE " . TBL_EMAIL_TEMPLATES . " SET `eventID` = '$strEvent', `fromEmail` = '$strFromEmail', `fromName` = '$strFromName', `to` = '$mailTo', `cc` = '$strCc', `bcc` = '$strBcc', `subject` = '$strSubject', `body` = '$strBody', `updatedDate` = '$strCurrentDate' WHERE id = '$intTemplateID'";
	if (update($strUpdateQuery)) {
		$strMsg = "Template is updated!!";
	}

}

if (isset($_GET['templateID']) && !empty($_GET['templateID']) && isset($_GET['action']) && $_GET['action'] == 'edit') {
	$intEditTemplateFlag = true;
	$strTemplateID = trim($_GET['templateID']);
	$strQueryEdit = "SELECT templates.id AS templateID, templates.fromEmail AS fromEmail, templates.fromName AS fromName, templates.to AS toEmail, templates.cc AS cc, templates.bcc AS bcc, templates.subject AS subject, templates.body AS body, ev.name AS eventName, ev.id AS evID FROM " . TBL_EMAIL_TEMPLATES . " AS templates INNER JOIN " . TBL_EVENTS . " AS ev ON ev.id = templates.eventID WHERE templates.id = '$strTemplateID'";
	//debug($strQueryEdit);
	$arrTemplate = selectFrom($strQueryEdit);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email Template Composer</title>
<script src="javascript/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="lib/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="javascript/jquery.validate.js"></script>
<link rel="stylesheet" href="lib/ckeditor/ckeditor.css" />
<link rel="stylesheet" href="css/email_template.css" />
<style>

::-webkit-input-placeholder { color:#CCC; }
::-moz-placeholder { color:#CCC; } /* firefox 19+ */
:-ms-input-placeholder { color:#CCC; } /* ie */
input:-moz-placeholder { color:#666; font-style:italic}


</style>


	}
<script>
$(document).ready(function(){

   var editCase = '<?php echo $_GET["action"]; ?>';

   if(editCase == 'edit'){

	  $("#toUsers").attr("disabled",true);

	return true;
	}else


	$("#toUsers").attr("disabled",false);

  });





</script>
<script>
$(document).ready(function(){
    $('#event').change(function() {
    var expireAlert = $("#event option:selected").text();
	if(expireAlert == 'Expired ID Alert' || expireAlert == 'Future ID Alert'){

		  $("#toUsers").attr("disabled",true);
		}else {

		 $("#toUsers").attr("disabled",false);

		}

  });
});
</script>
</head>

<body>
<div class = "main">
	<h3 class="well-small align_center well">Email Template Composer</h3>
	<form action="" method="post" id="emailTemplate">
		<table border = "0" width="100%">
		<?php
if (isset($strMsg)) {
	?>
		<tr class="success">
			<td align="center" colspan="2" class="align_center success"><?php echo $strMsg; ?></td>
		</tr>
		<?php
}
if (isset($strError)) {
	?>
		<tr class="error">
			<td colspan="2" class="align_center error"><?php echo $strError; ?></td>
		</tr>
		<?php
}
?>

	<tr>
						<td>	<label>Agent For:	</label></td>
				 
<td >
<?php $agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active'   $where_sql  ";


 
 
 ?>
 <?php  if($_SESSION["loggedUserData"]['adminType']!='Agent')
{
?>				<select  name="agentName" id="agentName" >
					<option value="">Select One</option>
 	          		<?php 
					
							$agentQuery .= "order by agentCompany";
								 $agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
 	    		        		?>
 	            				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            				<?
 	                			}
								?>
 	          			</select>
						
						<?php } else {?>
						<input  type="hidden" name="agentName" id="agentName" value="<?=$userID?>"><?=$_SESSION["loggedUserData"]['name']?>
						<?php }?>

</td>					
					</tr>
					
		<tr>
			<td>
				<label>Events <font color="#FF0000">*</font>:</label>
			</td>
			<td>
				<select name="event" id="event" class="dropdown">
					<option value = "" id="test"> -Select Event-</option>
					<?php
$strQueryEvents = "SELECT DISTINCT(name) AS name, id FROM " . TBL_EVENTS;
$arrEvents = selectMultiRecords($strQueryEvents);
for ($x = 0; $x < count($arrEvents); $x++) {
	echo "<option value='" . $arrEvents[$x]['id'] . "'>" . $arrEvents[$x]['name'] . "</option>";
}
?>
				</select>
			</td>
		</tr>

		<tr>
			<td>
				<label>From <font color="#FF0000">*</font>:</label>
			</td>
			<td>
				<input name="fromEmail" id="fromEmail" type="text" <?php if ($intEditTemplateFlag) {echo "value='" . $arrTemplate['fromEmail'] . "'";}?> />
				<label>Name: </label>
				<input name="fromName" id="fromName" type="text" <?php if ($intEditTemplateFlag) {echo "value='" . $arrTemplate['fromName'] . "'";}?> />
			</td>
		</tr>

		<tr>
			<td>
				<label>To <font color="#FF0000">*</font>:</label>
			</td>
			<td id="hide">

              <?
 	                $cashierType =  "select userID,name,username,adminType,agentType,isCorrespondent from ".TBL_ADMIN_USERS." where adminType IN ('Supper','call','admin','COLLECTOR','Branch Manager','Admin Manager','MLRO'".((CONFIG_FORUM_UPDATION_BETWEEN_AGENTS == "1" && $_SESSION["loggedUserData"]["adminType"]!="Agent")  ?",'Agent','SUPI Manager'":"").",'Support') ";
					//debug($cashierType);
					$x = 0;
 	                $cashierTypeQuery= selectMultiRecords($cashierType);
 	                //debug($cashierTypeQuery);
 	                for($k = 0; $k < count($cashierTypeQuery); $k++)
 	                {
 	                     $cashierAdminType[$k] = $cashierTypeQuery[$k]['adminType'];
 	                     $cashierUsername[$k]= $cashierTypeQuery[$k]['name'];
 	                     $cashierName[$k]= $cashierTypeQuery[$k]['username'];
 	                     $cashierID[$k]= $cashierTypeQuery[$k]['userID'];
 	                     $cashierAgentType[$k]= $cashierTypeQuery[$k]['agentType'];
 	                     $cashierIsCorrespondent[$k]= $cashierTypeQuery[$k]['isCorrespondent'];
 	                     $userTbl[$k]= 'admin';

 	                    $x++;

 	                }

                     ?>

                     <br>
                    <span class="style5">(Hold Ctrl key for multiple selection)<br>
                    <br>
            		 <SELECT name="toUsers[]" size="50"  multiple id="toUsers" style="font-family:verdana; font-size: 11px; width:350px; height:150px;" >
                      <option value="Sender">Sender</option>
                       <option value="Beneficiary">Beneficiary</option>
					  <?

	 	                     for($ab = 0; $ab < $x ; $ab++)
 	                    {
 	                    	$maskingVal = maskingOfAdminTypeValues($cashierAdminType[$ab],$cashierAgentType[$ab],$cashierIsCorrespondent[$ab]);
                      ?>
                      <option value="<?=$cashierID[$ab]?>" <? echo (!empty($_POST["toUsers"]) && in_array($cashierUsername[$ab]["name"],$_POST["toUsers"]) ? "selected"  : "")?>>
                      <?=$maskingVal . " [" . $cashierUsername[$ab] . "]"?>
                      </option>
                      <?

                      }

 	                ?>
                    </SELECT>
                    </span>

				<!--
                <input name="to" id="to" size="80px" type="text" readonly="readonly" <?php //if($intEditTemplateFlag){ echo "value='".$arrTemplate['toEmail']."'"; }?> class="input-xxlarge uneditable-input" placeholder="It will take variable value from sender email" />

             -->
			<!--<a href="javascript:void(0);" class="select_users" id="toSelectUsers">Select Users</a>
				<div class="users_cont" id="users">
					<div class="users">
						<div style="text-align:right;">
							<a href="javascript:void(0)" class="close" id="close">X</a>
						</div>
						<p>
							Select Users<br />
							Please Click for selection<br />
							Click on delete button to delete last inserted user
						</p>
						<select name="toUsers" multiple="multiple" id="toUsers" style="display:none" >
							<?php
foreach ($arrUsers as $i => $v) {
	echo "<option value='$v'>$i</option>";
}
?>
						</select>
						<div id="delToUsers" style="display:none;">
							<input name="delUsersTo" id="delUsersTo" class="btn right isolate input-small" type="button" value="Delete"/>
						</div>
						<select name="ccUsers" multiple="multiple" id="ccUsers" style="display:none" >
							<?php
foreach ($arrUsers as $i => $v) {
	echo "<option value='$v'>$i</option>";
}
?>
						</select>
						<div id="delCcUsers" style="display:none;">
							<input name="delUsersCc" id="delUsersCc" class="btn right isolate input-small" type="button" value="Delete"/>
						</div>
						<select name="bccUsers" multiple="multiple" id="bccUsers" style="display:none" >
							<?php
foreach ($arrUsers as $i => $v) {
	echo "<option value='$v'>$i</option>";
}
?>
						</select>
						<div id="delBccUsers" style="display:none;">
							<input name="delUsersBcc" id="delUsersBcc" class="btn right isolate input-small" type="button" value="Delete"/>
						</div>
					</div>
				</div>-->
			</td>
		</tr>
		<tr>
			<td>
				<label>CC:</label>
			</td>
			<td>
				<input name="cc" id="cc" size="80px" type="text" <?php if ($intEditTemplateFlag) {echo "value='" . $arrTemplate['cc'] . "'";}?> class="editable-input input-xxlarge"/>
				<!--<a href="javascript:void(0);" class="select_users" id="ccSelectUsers">Select Users</a>-->
			</td>
		</tr>

		<tr>
			<td>
				<label>BCC:</label>
			</td>
			<td>
				<input name="bcc" id="bcc" size="80px" type="text" <?php if ($intEditTemplateFlag) {echo "value='" . $arrTemplate['bcc'] . "'";}?> class="editable-input input-xxlarge"/>
				<!--<a href="javascript:void(0);" class="select_users" id="bccSelectUsers">Select Users</a>-->
			</td>
		</tr>

		<tr>
			<td>
				<label>Subject <font color="#FF0000">*</font>:</label>
			</td>
			<td>
				<input name="subject" id="subject" size="65px" type="text" <?php if ($intEditTemplateFlag) {echo "value='" . $arrTemplate['subject'] . "'";}?>/>
			</td>

		</tr>


		<tr>
			<td colspan="2">
				<label for="editor1" class="info">
					Please compose your message body in the below editor
				</label>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<textarea class="ckeditor" cols="80" id="body" name="body" rows="10"><?php if ($intEditTemplateFlag) {echo $arrTemplate['body'];}?></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<?php
if ($intEditTemplateFlag) {
	?>
				<input name="updateTemplate" id="updateTemplate" type="submit" class="btn btn-success input-small" value="Update"/>
				<input type="hidden" name="templateID" value="<?php echo $arrTemplate['templateID']; ?>"/>
				<?php
} else {
	?>
				<input name="saveTemplate" id="saveTemplate" type="submit" class="btn btn-success input-small" value="Save"/>
				<?php
}
?>
				<input type="reset" class="btn btn-primary input-small" value="Clear"/>
			</td>
		</tr>
		</table>
	</form>
</div>
 <script type="text/javascript">
	$(document).ready(
		function(){


			/*function displayUsers(){
				$('#users').fadeIn('slow');
			}

			function hideUsers(){
				$('#users').css('display','none');
			}

			function addUsers(id, value){
				var oldValue = $('#'+id).val();
				oldValue = $.trim(oldValue);
				if(oldValue == ''){
					$('#'+id).val(value);
				}else{
					if(oldValue.search(value) >= 0){
						return false;
					}else{
						var newValue = oldValue+","+value;
						$('#'+id).val(newValue);
					}
				}
			}

			/*function deleteUsers(id){
				var value = $('#'+id).val();
				if(value.search(',') >= 0){
					arrValue = value.split(',');
					length = arrValue.length;
					newValue = '';
					for(x = 0;x < length - 1; x++){
						if(x != 0)
							newValue += ','+arrValue[x];
						else
							newValue += arrValue[x];
					}
					$('#'+id).val(newValue);
				}else{
					$('#'+id).val('');
				}
			}*/


			function SelectOption(OptionListName, ListVal){
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}

			/*$(document).click(
				function(){
					hideUsers();
				}
			);

			$('#users').click(
				function(){
					return false;
				}
			);

			$('#close').click(
				function(){
					hideUsers();
				}
			);

			/*$('#toSelectUsers').click(
				function(){
					displayUsers();
					$('#ccUsers').hide('fast');
					$('#bccUsers').hide('fast');
					$('#toUsers').show('fast');

					$('#delCcUsers').hide('fast');
					$('#delBccUsers').hide('fast');
					$('#delToUsers').show('fast');

					return false;
				}
			);*/

			/*$('#toUsers').change(
				function(){
					addUsers('to' , $('#toUsers').val());
				}
			);


			$('#ccSelectUsers').click(
				function(){
					displayUsers();
					$('#toUsers').hide('fast');
					$('#bccUsers').hide('fast');
					$('#ccUsers').show('fast');

					$('#delToUsers').hide('fast');
					$('#delBccUsers').hide('fast');
					$('#delCcUsers').show('fast');
					return false;
				}
			);

			$('#ccUsers').change(
				function(){
					addUsers('cc' , $('#ccUsers').val());
				}
			);

			$('#delUsersTo').click(
				function(){
					deleteUsers('to');
					$("#toUsers option:selected").removeAttr("selected");
				}
			);

			$('#delUsersCc').click(
				function(){
					deleteUsers('cc');
					$("#ccUsers option:selected").removeAttr("selected");
				}
			);

			$('#delUsersBcc').click(
				function(){
					deleteUsers('bcc');
					$("#bccUsers option:selected").removeAttr("selected");
				}
			);

			/*$('#bccSelectUsers').click(
				function(){
					displayUsers();
					$('#toUsers').hide('fast');
					$('#ccUsers').hide('fast');
					$('#bccUsers').show('fast');

					$('#delToUsers').hide('fast');
					$('#delCcUsers').hide('fast');
					$('#delBccUsers').show('fast');
					return false;
				}
			);*/

			/*$('#bccUsers').change(
				function(){
					addUsers('bcc' , $('#bccUsers').val());
				}
			);*/

			$('#event').change(
				function(){
					if($('#event').val() == 1 || $('#event').val() == 5 || $('#event').val() == 6)
					{
						$('#to').val('Sender');
						$('#fromEmail').removeAttr('readonly','readonly');
						$('#fromEmail').val('');
					}
					else if($('#event').val() == 2 || $('#event').val() == 7 || $('#event').val() == 3 || $('#event').val() == 4 )
					{
						$('#to').val('Sender');
						$('#fromEmail').val('Account Manager');
						$('#fromEmail').attr('readonly','readonly');

					}

				}

			);
			<?php
if ($intEditTemplateFlag) {
	?>
				SelectOption(document.forms[0].event, <?php echo $arrTemplate['evID']; ?>);
			<?php
}
?>
			$('#emailTemplate').validate({
				rules: {
					event: {
						required: true
					},
					fromEmail: {
						//email: true,
						required: true
					},
					to: {
						required: true
					},
					subject: {
						required: true
					}
				},
				messages: {
					event: "Please select an event for the template."
				}
			});
		}
	);
</script>
</body>
</html>