<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$trnsid = $_POST["trnsid"];
//echo count($trnsid);
//echo $trnsid[0];

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
	/*if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($_POST["transSource"]!="")
		{
			$transSource = $_POST["transSource"];
		
		}
	}
	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($_GET["transSource"]!="")
		{
			$transSource = $_GET["transSource"];
		
		}
	}*/
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$_SESSION["loggedUserData"]["userID"]."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

if($_POST["btnAction"] != "")
{
	for ($i=0;$i<$_POST["totTrans"];$i++)
	{
		if(count($trnsid) >= 1)
		{
			if($_POST["btnAction"] != "")
			{
				$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				$custID 	= $contentTrans["customerID"];
				$benID 		= $contentTrans["benID"];
				$custAgentID = $contentTrans["custAgentID"];
				$benAgentID = $contentTrans["benAgentID"];
				$imReferenceNumber = $contentTrans["refNumberIM"];

				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				 // getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				// getting BEneficiray agent Email
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custAgentID'");
				$custEmail = $custContents["email"];

				$subject = "Status Updated";
							$message = "Congratulations!<br>
				The status of transaction has been changed to " . $_POST["btnAction"] . "<br>
				Transaction reference nunmber is: $imReferenceNumber <br><br>

				Thank you for using $company<br>
				$company Support<br>";
				//sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
				//sendMail($custAgentEmail, $subject, $message, $fromName, $fromEmail);
				//sendMail($benAgentEmail, $subject, $message, $fromName, $fromEmail);
				//sendMail($benEmail, $subject, $message, $fromName, $fromEmail);
				//sendMail($custEmail, $subject, $message, $fromName, $fromEmail);
			}
			if($_POST["btnAction"] == "Cancel")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Cancelled', cancelledBy = '$username', cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction cancelled successfully.");
			}

			if($_POST["btnAction"] == "Recall")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'recalled', recalledBy = '$username', recalledDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction recalled successfully.");
				//echo "HERE";
			}
			if($_POST["btnAction"] == "Verify")
			{
				//echo "update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username' where transID='". $trnsid[$i] ."'";
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username' where transID='". $trnsid[$i] ."'");
				insertError("Transaction verified successfully.");
				//echo "HERE";
			}

			if($_POST["btnAction"] == "Authorize")
			{
				// Update agents account limit;
				update("update " . TBL_ADMIN_USERS . " set limitUsed  = (limitUsed  + '".$contentTrans["totalAmount"]."') where userID = '". $contentTrans["custAgentID"] ."'");
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");
				insertError("Transaction Authorized successfully.");
				//echo "HERE";
			}

		}
	}
redirect("transactions-history.php?msg=Y&action=" . $_GET["action"]);
}
$query = "select * from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
$transType = "";
$transStatus = "";
$transID = "";

if($_POST["Submit"] =="Search")
{
	if($_POST["transType"] != "")
	{
		$transType = $_POST["transType"];
		$query .= " and (transType='".$_POST["transType"]."')";
	}
	if($_POST["transStatus"] != "")
	{
		$transStatus = $_POST["transStatus"];
		$query .= " and (transStatus='".$_POST["transStatus"]."')";
	}
	if($_POST["transID"] != "")
	{
		$transID = $_POST["transID"];
		$query .= " and (transID='".$_POST["transID"]."' OR refNumber = '".$_POST["transID"]."' OR  refNumberIM = '".$_POST["transID"]."') ";
	}
}

if($_GET["transType"] != "")
{
		$transType = $_GET["transType"];
		$query .= " and (transType='".$_GET["transType"]."')";
}
if($_GET["transStatus"] != "")
{
		$transStatus = $_GET["transStatus"];
		$query .= " and (transStatus='".$_GET["transStatus"]."')";
}
if($_GET["transID"] != "")
{
		$transID = $_GET["transID"];
		$query .= " and (transID='".$_GET["transID"]."' OR refNumber = '".$_GET["transID"]."' OR  refNumberIM = '".$_GET["transID"]."') ";
}

if($_GET["action"] == "cancel")
{
	$query .= " and transStatus ='Pending' and transDate > date_sub('".getCountryTime(CONFIG_COUNTRY_CODE)."', interval 1 hour) ";
}

if($_GET["action"] == "recall")
{
	$query .= " and (transStatus = 'Pending' OR transStatus = 'authorize' OR transStatus = 'Processing') ";
}

if($_GET["action"] == "verify")
{
	$query .= " and transStatus ='Pending' ";
}

if($_GET["action"] == "authorize")
{
	$query .= " and transStatus = 'Processing' ";
}
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where createdBy = 'CUSTOMER'".$extraCondition;

	
			$query.= " and trans_source = 'O' ";
			$queryCnt .= " and trans_source = 'O' ";
		



switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and custAgentID = '".$_SESSION["loggedUserData"]["userID"]."'";
}
$query .= " and (createdBy = 'CUSTOMER') order by transDate DESC";


if ($sortBy !="")

$query .= " LIMIT $offset , $limit";

$contentsTrans = selectMultiRecords($query);
$allCount = countRecords($queryCnt );
//
//debug($query);
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Transactions")?> 
      History</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
        <form action="transactions-history.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
        <td nowrap>
		  Ref No. OR Trans No.
		    <input name="transID" type="text" id="transID" value="<?=$transID;?>">
			
		  <!--<select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType; ?>");</script>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option>

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus; ?>");</script>-->
            <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="transactions-history.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
		  
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>




	    <tr>
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans)?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?

		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?> </span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Tot Amount </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <td width="146" align="center"><? if ($_GET["action"] != "") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
		      <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? //$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else
				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($createdBy)?></td>
				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>&transHist=Y&transID=<?=$transID;?>&transType=<?=$transType;?>&transStatus=<?=$transStatus;?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<?echo $i?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			   <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>

          <tr>
            <td  bgcolor="#000000"> <table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transSource=$transSource&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>


		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
