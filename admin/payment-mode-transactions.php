<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";

if($_POST["transType"]!="")
		$transType = $_POST["transType"];
	elseif($_GET["transType"]!="") 
		$transType=$_GET["transType"];
		
	if($_POST["transStatus"]!="")
		$transStatus = $_POST["transStatus"];
	elseif($_GET["transStatus"]!="")
		$transStatus = $_GET["transStatus"];

if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];
		
		
	if($_POST["payMode"]!="")
		$payMode = $_POST["payMode"];
	elseif($_GET["payMode"]!="")
		$payMode = $_GET["payMode"];
	

if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];
	
//$trnsid = $_POST["trnsid"];
//echo count($trnsid);
//echo $trnsid[0];
//$ttrans = $_POST["totTrans"];

$query = "select * from ". TBL_TRANSACTIONS . " as t where 1";
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1";

//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType");
if($Submit == "Search")
{

	
	if($transID != "")
	{
		
		switch($by)
		{
			case 0:
			{		
				$query = "select * from ". TBL_TRANSACTIONS . " as t where 1 ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1 ";
				
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				
				$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
								
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}
				
				$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				
				$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";		
				$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
				
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}
				
				$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				break;
			}
			case 3:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
		
				$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
								
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}
				
				$query .= "  and (c.accountName like '".$transID."%')";
				$queryCnt .= "  and (c.accountName like '".$transID."%')";
				break;
			}
			case 4:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
		
				$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
								
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}
				
				$query .= "  and (a.name like '".$transID."%' or a.username like '".$transID."%')";
				$queryCnt .= "  and (a.name like '".$transID."%' or a.username like '".$transID."%')";
				break;	
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
			}
}
	if($transType != "")
	{
		$query .= " and (t.transType='".$transType."')";
		$queryCnt .= " and (t.transType='".$transType."')";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType."')";
		  $queryonlineCnt .= " and (t.transType='".$transType."')";
			}
		
	}
	if($transStatus != "")
	{
		$query .= " and (t.transStatus='".$transStatus."')";
		$queryCnt .= " and (t.transStatus='".$transStatus."')";
		
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transStatus='".$transStatus."')";
		  $queryonlineCnt .= " and (t.transStatus='".$transStatus."')";
			}
		
	}
	
	
	if($payMode != "")
	{
		$query .= " and (t.moneyPaid='".$payMode."')";
		$queryCnt .= " and (t.moneyPaid='".$payMode."')";
		
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.moneyPaid='".$payMode."')";
		  $queryonlineCnt .= " and (t.moneyPaid='".$payMode."')";
			}
		
	}

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
	if($transID != "")
		{	
		$queryonline .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."')";
	}
}

if($agentType == "Branch Manager"){
	$query .= " and t.custAgentParentID ='$agentID' ";
	$queryCnt .= " and t.custAgentParentID ='$agentID' ";
	}
 $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);

 $allCount = countRecords($queryCnt);


		
		if($transID != "" && $by != 0)
		{	
			
				$onlinecustomerCount = countRecords($queryonlineCnt );
				
				$allCount = $allCount + $onlinecustomerCount;
			
			
			
					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
			 $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
		
?>
<html>
<head>
	<title>Transactions By Money Paid</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Transactions By Money Paid</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="payment-mode-transactions.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="transID" type="text" id="transID" value=<?=$transID?>>
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option> 

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Amended">Amended</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>		  
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select>
		<script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus?>");</script>
		   </td>
      </tr>
      <tr>
      	<td align="center">
      		<select name="payMode" style="font-family:verdana; font-size: 11px; width:100">
	          <option value=""> - Money Paid By - </option>
	          <option value="By Cash"> Cash</option>
	          <option value="By Cheque"> Cheque</option>
	          <option value="By Bank Transfer"> Bank Transfer</option>
	          <? 
            	/**
            	 * @Ticket# 3564
            	 */
            	if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1) { 
            ?>
            	<option value="By Card">By Card</option>
            <? } ?>
	        </select><script language="JavaScript">SelectOption(document.forms[0].payMode, "<?=$payMode?>");</script>
      		<select name="searchBy" >
						<option value=""> - Search By - </option>
						<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
						<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
						<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
						<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Payin Book Number</option>
						<option value="4" <? echo ($_POST["searchBy"] == 4 ? "selected" : "")?>>By Agent Name/Code</option>
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
					<input type="submit" name="Submit" value="Search">
      	</td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="payment-mode-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&payMode=$payMode&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&payMode=$payMode&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&payMode=$payMode&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&payMode=$payMode&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			   <td width="100"><span class="style1">Payment Mode </span></td>
		    <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
				   <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["moneyPaid"]?></td>


				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			
			<? for($i=0;$i < count($onlinecustomer);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["totalAmount"] .  " " . $onlinecustomer[$i]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
					  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["moneyPaid"]?></td>

				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$i]["transID"]?>" class="style2">Enquiry</a></td>
			  </tr>
				<?
			}
			
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>