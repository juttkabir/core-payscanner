<?
session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";
$moneyPaid   = "";
$fMonth      = "";
$fDay        = "";
$fYear       = "";
$tMonth			 = "";
$tDay				 = "";
$tYear   		 = "";
$fhr         = ""; 
$fmin        = "";
$fsec        = "";
$thr         = "";
$tmin        = "";
$tsec        = "";

if($_POST["transType"]!="")
	$transType = $_POST["transType"];
elseif($_GET["transType"]!="") 
	$transType=$_GET["transType"];
		
if($_POST["transStatus"]!="")
	$transStatus = $_POST["transStatus"];
elseif($_GET["transStatus"]!="")
	$transStatus = $_GET["transStatus"];

if($_POST["Submit"]!="")
	$Submit = $_POST["Submit"];
elseif($_GET["Submit"]!="") 
	$Submit=$_GET["Submit"];
	
if($_POST["transID"]!="")
	$transID = $_POST["transID"];
elseif($_GET["transID"]!="")
	$transID = $_GET["transID"];

if($_POST["agentID"]!="")
	$agentID = $_POST["agentID"];
elseif($_GET["agentID"]!="") 
	$agentID = $_GET["agentID"];
	

if($_POST["fMonth"] != ""){
	$fMonth=$_POST["fMonth"];
}elseif($_GET["fMonth"] != ""){
	$fMonth=$_GET["fMonth"];
}
if($_POST["fDay"] != ""){
	$fDay=$_POST["fDay"];
}elseif($_GET["fDay"] != ""){
	$fDay=$_GET["fDay"];
}
if($_POST["fYear"] != ""){
	$fYear=$_POST["fYear"];
}elseif($_GET["fYear"] != ""){
	$fYear=$_GET["fYear"];
}
		
if($_POST["tMonth"] != ""){
	$tMonth=$_POST["tMonth"];
}elseif($_GET["tMonth"] != ""){
	$tMonth=$_GET["tMonth"];
}
if($_POST["tDay"] != ""){
	$tDay=$_POST["tDay"];
}elseif($_GET["tDay"] != ""){
	$tDay=$_GET["tDay"];
}
if($_POST["tYear"] != ""){
	$tYear=$_POST["tYear"];
}elseif($_GET["tYear"] != ""){
	$tYear=$_GET["tYear"];
}



////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPI" && $agentType != "SUPAI" && $agentType != "SUBI" && $agentType != "SUBAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////


	$fromDate = $fYear . "-" . $fMonth . "-" . $fDay;
	$toDate = $tYear . "-" . $tMonth . "-" . $tDay;
	if($fromDate == "--")
		$fromDate = date('Y-m-d');
		
	if($toDate == "--")
		$toDate = date('Y-m-d');	
	
$query = "select t.benID, transID, transDate, totalAmount, custAgentID, transType, transAmount, localAmount, refNumber, refNumberIM, transStatus, t.customerID, collectionPointID, createdBy, AgentComm, IMFee, bankCharges, currencyFrom, currencyTo from ". TBL_TRANSACTIONS . " as t where 1";
$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t where 1";

//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType");

	
	if($transID != "")
	{
		
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
	}
	
	if($agentID != '')
	{
		$query .= " and (t.benAgentID = '".$agentID."' OR t.benAgentParentID ='".$agentID."')";
		$queryCnt .= " and (t.benAgentID = '".$agentID."' OR t.benAgentParentID ='".$agentID."')";
	}

	if($transType != "")
	{
		$query .= " and (t.transType='".$transType."')";
		$queryCnt .= " and (t.transType='".$transType."')";
		
	}
	if($transStatus != "")
	{
		$query .= " and (t.transStatus='".$transStatus."')";
		$queryCnt .= " and (t.transStatus='".$transStatus."')";
		
	}
	if ($moneyPaid != "")
	{
		
		$query .= " and (t.moneyPaid='".$moneyPaid."')";
		$queryCnt .= " and (t.moneyPaid='".$moneyPaid."')";
		
	}
	
	



switch ($agentType)
{
	case "SUPI":
	case "SUPAI":
	case "SUBI":
	case "SUBAI":
		$query .= " and (t.benAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.benAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	
}

if($agentType == "Branch Manager"){
	$query .= " and t.custAgentParentID ='$userID' ";
	$queryCnt .= " and t.custAgentParentID ='$userID' ";
	}
	

	$queryDate = "(t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";	
	$query .=  " and $queryDate";				
	$queryCnt .=  " and $queryDate";
		
	
  $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
  $query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);

 $allCount = countRecords($queryCnt);


		
		
		
?>
<html>
<head>
	<title>Payment To Distributors</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/datetimepicker.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Payment To Distributrs</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="distributor-payement.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0" colspan="2"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap colspan="2"> Search By Reference Code 
              <input name="transID" type="text" id="transID" value=<?=$transID?>>
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
		<select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
		  <option value=""> - Status - </option> 

		  <option value="Pending">Pending</option>
		  <option value="Processing">Verify</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		   <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select>
		<script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus?>"); 
		</script>
       
	</td></tr>
	 
	<tr>
	<td align="center" nowrap colspan="2">
		Date &nbsp;<b>From</b> 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$fDay; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$fMonth; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$fYear; ?>");
        </script>
        <b>To</b>	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$tDay; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$tMonth; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
      
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$tYear; ?>");
        </script>
      <br><br>
     
		
				  <?    
       if($condition)//101
			  {
			  ?>
			  Select Distributor 
			  <select name="agentID" style="font-family:verdana; font-size: 11px">
				<option value="">- Select Distributor -</option>
				<?
						$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
						}
					?>
			  </select>
			  <script language="JavaScript">
				SelectOption(document.Search.agentID, "<?=$agentID?>");
									</script>
		<? }?>
		   
      </tr>
	<tr><td align= "center" colspan="2">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="100%" border="1" cellpadding="0" bordercolor="#666666">
      <form action="distributor-payement.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000" >
			<table  width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans); ?> record(s).</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
			$pageTotalAmount = 0;
			$pageAgentComm = 0;
			$pageCompanyComm = 0; 
			$pageBankCharges = 0;
			$pageDistPayment = 0;

		?>
        <tr>
      <td nowrap bgcolor="#EFEFEF"><table width="100%" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Agent Commission </span></td>
			  <td><span class="style1"><? echo(SYSTEM_PRE); ?> Commission </span></td>
			  <td width="100"><span class="style1">Bank Charges</span></td>
			  <td><span class="style1">Amount to Distributor</span></td>
				</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
				{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong>
				  	<font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
          <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF">
				  	<? 
										$pageTotalAmount += $contentsTrans[$i]["totalAmount"];				  	
				  	echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]
				  	?>
				  </td>
				 	<td width="100" bgcolor="#FFFFFF">
				 		<? 
				 				$pageAgentComm += $contentsTrans[$i]["AgentComm"];
				 		echo ($contentsTrans[$i]["AgentComm"] .  " " . $contentsTrans[$i]["currencyFrom"]); 
				 		?>
				 	</td>
				 	<td width="100" bgcolor="#FFFFFF">
				 		<? 
				 				$companyComm = $contentsTrans[$i]["IMFee"] - $contentsTrans[$i]["AgentComm"];
				 				$pageCompanyComm += $companyComm;
				 		echo ($companyComm. " " . $contentsTrans[$i]["currencyFrom"]); 
				 		?>
				 	</td>
				  <td align="center" bgcolor="#FFFFFF">
				  	<? 
				  		$pageBankCharges += $contentsTrans[$i]["bankCharges"];
				  	echo($contentsTrans[$i]["bankCharges"]); 
				  	?>
				  </td>
				   <td align="center" bgcolor="#FFFFFF">
				   	<? 
				   		$distPayment = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["IMFee"];
				   		$pageDistPayment += $distPayment; 
				   	echo($distPayment); 
				   	?>
				   </td>
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong>Page Total</strong></td>
				  <td width="75" bgcolor="#FFFFFF"><strong>&nbsp;</strong></td>
          <td width="75" bgcolor="#FFFFFF"><strong>&nbsp;</strong></td>
				  <td width="75" bgcolor="#FFFFFF"><strong>&nbsp;</strong></td>
				  <td width="100" bgcolor="#FFFFFF"><strong><?=$pageTotalAmount?></strong></td>
				 	<td width="100" bgcolor="#FFFFFF"><strong><?=$pageAgentComm?></strong></td>
				 	<td width="100" bgcolor="#FFFFFF"><strong><?=$pageCompanyComm?></strong></td>
				  <td align="center" bgcolor="#FFFFFF"> <strong><?=$pageBankCharges?></strong></td>
				   <td align="center" bgcolor="#FFFFFF"><strong><?=$pageDistPayment?></strong></td>
				</tr>
			<?
			
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset + count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>