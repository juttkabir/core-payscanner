<?
			$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where clientName = '".$benAgentInfo["fromServer"]."' and isEnabled = 'Y'");
			for($cl = 0; $cl < count($jointClients); $cl++)
			{
				$otherClient = new connectOtherDataBase();
				$otherClient-> makeConnection($jointClients[$cl]["serverAddress"],$jointClients[$cl]["userName"],$jointClients[$cl]["password"],$jointClients[$cl]["dataBaseName"]);
				
				$copyTransaction = selectFrom("select * from ".TBL_TRANSACTIONS." where transID = '".$transID."'");
				
				//Collecting DIstributor/Agent Information from other system
					$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClients[$cl]["dataBaseName"].".admin as a, ".$jointClients[$cl]["dataBaseName"].".sharedUsers as s where a.userID = s.localServerId and s.remoteServerId = '".$copyTransaction["benAgentID"]."'");		
					$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClients[$cl]["dataBaseName"].".admin where userID = (select remoteServerId from ".TBL_SHAREDUSERS." where localServerId = '".$copyTransaction["custAgentID"]."')");		
				//////
				
				
				$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$copyTransaction["custAgentID"]."'");
				$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$copyTransaction["benAgentID"]."'");
				
				$sql = "INSERT INTO ".$jointClients[$cl]["dataBaseName"].".transactions (  customerID, benID, benAgentID, custAgentID,  exchangeID, refNumber, transAmount,
											 exchangeRate, 
											localAmount, IMFee, totalAmount, transactionPurpose, other_pur, fundSources, moneyPaid, 
											Declaration, addedBy, transDate, transType, toCountry, fromCountry, refNumberIM, 
											currencyFrom, 
											currencyTo, custAgentParentID, benAgentParentID,collectionPointID,bankCharges,AgentComm,CommType,admincharges, cashCharges,
											question, answer,tip, outCurrCharges, distributorComm, discountRequest, transStatus, creation_date_used, distCommPackage, internalRemarks,benIDPassword,
											agentExchangeRate
											)
								VALUES ( '".$copyTransaction["customerID"]."', '".$copyTransaction["benID"]."', '".$remoteBenAgent["userID"]."', 
								'".$remoteCustAgent["userID"]."', 
								'".$copyTransaction["exchangeID"]."', '".$copyTransaction["refNumber"]."', '".$copyTransaction["transAmount"]."', 
								'".$copyTransaction["exchangeRate"]."', 
											'".$copyTransaction["localAmount"]."', '".$copyTransaction["IMFee"]."', '".$copyTransaction["totalAmount"]."', 
											'".checkValues($copyTransaction["transactionPurpose"])."','".$copyTransaction["other_pur"]."', '".checkValues($copyTransaction["fundSources"])."', 
											'".$copyTransaction["moneyPaid"]."', 
											'Y', '".$copyTransaction["addedBy"]."', '".$copyTransaction["transDate"]."', '".$copyTransaction["transType"]."', '".$copyTransaction["toCountry"]."', 
											'".$copyTransaction["fromCountry"]."', '".$copyTransaction["refNumberIM"]."', '".$copyTransaction["currencyFrom"]."',
											 '".$copyTransaction["currencyTo"]."', 
											'".$remoteCustAgent["parentID"]."', '".$remoteBenAgent["parentID"]."',
											'".$copyTransaction["collectionPointID"]."','".$copyTransaction["bankCharges"]."','".$copyTransaction["agentComm"]."','".$copyTransaction["commType"]."','".$copyTransaction["admincharges"]."', '".$copyTransaction["cashCharges"]."',
											'".$copyTransaction["question"]."','".$copyTransaction["answer"]."','".$copyTransaction["tip"]."','".$copyTransaction["outCurrCharges"]."','".$copyTransaction["distributorComm"]."','".$copyTransaction["discountRequest"]."','".$copyTransaction["transStatus"]."','".$copyTransaction["creation_date_used"]."','".$copyTransaction["distCommPackage"]."', '".$copyTransaction["internalRemarks"]."', '".$copyTransaction["benIDPassword"]."',
											'".$copyTransaction["agentExchangeRate"]."')";

		
				$remoteTransID = $otherClient-> insertInto($sql); 	
				
				/////////Insert into shared Trans Tables///////////
				
				
				$localSharedTrans = "INSERT INTO ".TBL_SHARED_TRANSACTIONS." (localTrans, remoteTrans, generatedLocally, remoteServerId) VALUES
				('".$transID."', '".$remoteTransID."', 'Y', '".$jointClients[$cl]["clientId"]."')";
				insertInto($localSharedTrans);
				
				
				$remoteSharedTrans = "INSERT INTO ".$jointClients[$cl]["dataBaseName"].".sharedTransactions (localTrans, remoteTrans, generatedLocally, remoteServerId) VALUES
				('".$remoteTransID."', '".$transID."', 'N', '".$remoteBenAgent["availableAt"]."')";
				$otherClient-> insertInto($remoteSharedTrans);
				
				////////////////////Update Ledgers
				if($agentContents["agentType"] == 'Sub')
				{
					
						if($agentContents["isCorrespondent"] == 'N')
						{
						
							$copyAgentLedger = selectFrom("select * from ".TBL_SUB_AGENT_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_SUB_AGENT_ACCOUNT." where agentID = '".$copyTransaction["custAgentID"]."' and TransID = '".$transID."')");	
						}elseif($agentContents["isCorrespondent"] == 'Y')
						{
						
							$copyAgentLedger = selectFrom("select * from ".TBL_SUB_AnD_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_SUB_AnD_ACCOUNT." where agentID = '".$copyTransaction["custAgentID"]."' and actAs = 'Agent' and TransID = '".$transID."')");	
						}
				}else{
						
						if($agentContents["isCorrespondent"] == 'N')
						{
						
							$copyAgentLedger = selectFrom("select * from ".TBL_AGENT_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_AGENT_ACCOUNT." where agentID = '".$copyTransaction["custAgentID"]."' and TransID = '".$transID."')");						
						}elseif($agentContents["isCorrespondent"] == 'Y')
						{
						
							$copyAgentLedger = selectFrom("select * from ".TBL_AnD_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_AnD_ACCOUNT." where agentID = '".$copyTransaction["custAgentID"]."' and actAs = 'Agent' and TransID = '".$transID."')");
						}
				}
				if($copyAgentLedger["amount"] > 0)
				{
					if($remoteCustAgent["agentType"] == 'Sub')
					{
						$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $copyAgentLedger["amount"], $remoteTransID, $copyAgentLedger["type"], $transactionDescription, "Agent", $copyAgentLedger["dated"]);
						$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $copyAgentLedger["amount"], $remoteTransID, $copyAgentLedger["type"], $transactionDescription, "Agent", $copyAgentLedger["dated"]);
					}else{
						$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $copyAgentLedger["amount"], $remoteTransID, $copyAgentLedger["type"], $transactionDescription, "Agent", $copyAgentLedger["dated"]);
					}				
				}
				
				if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1' && $copyTransaction["transStatus"] == 'Authorize')
				{
					/////Distributor////
					if($benAgentContents["agentType"] == 'Sub')
				{
						
						if($benAgentContents["isCorrespondent"] != 'Y')
						{
						
							$copyBenAgentLedger = selectFrom("select * from ".TBL_SUB_DISTRIBUTOR_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_SUB_DISTRIBUTOR_ACCOUNT." where bankID = '".$copyTransaction["benAgentID"]."' and TransID = '".$transID."')");	
						}elseif($agentContents["isCorrespondent"] == 'Y')
						{
						
							$copyBenAgentLedger = selectFrom("select * from ".TBL_SUB_AnD_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_SUB_AnD_ACCOUNT." where agentID = '".$copyTransaction["benAgentID"]."' and actAs = 'Distributor' and TransID = '".$transID."')");	
						}
				}else{
						
						if($benAgentContents["isCorrespondent"] == 'N')
						{
							
							$copyBenAgentLedger = selectFrom("select * from ".TBL_DISTRIBUTOR_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_DISTRIBUTOR_ACCOUNT." where bankID = '".$copyTransaction["benAgentID"]."' and TransID = '".$transID."')");						
						}elseif($benAgentContents["isCorrespondent"] == 'Y')
						{
							
							$copyBenAgentLedger = selectFrom("select * from ".TBL_AnD_ACCOUNT." where aaID = (select Max(aaID) from ".TBL_AnD_ACCOUNT." where agentID = '".$copyTransaction["benAgentID"]."' and actAs = 'Distributor' and TransID = '".$transID."')");	
						}
				}
				if($copyAgentLedger["amount"] > 0)
				{
					if($remoteBenAgent["agentType"] == 'Sub')
					{
						$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $copyBenAgentLedger["amount"], $remoteTransID, $copyBenAgentLedger["type"], $transactionDescription, "Distributor", $copyAgentLedger["dated"]);
						$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $copyBenAgentLedger["amount"], $remoteTransID, $copyBenAgentLedger["type"], $transactionDescription, "Distributor", $copyAgentLedger["dated"]);
					}else{
						$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $copyBenAgentLedger["amount"], $remoteTransID, $copyBenAgentLedger["type"], $transactionDescription, "Distributor", $copyAgentLedger["dated"]);
					}				
				}
					
					///////Distributor////
					}
				///////////
				$otherClient->closeConnection();
			}
			
?>			