<?php
session_start();

include ("../include/config.php");
include ("security.php");
include ("calculateBalance.php");
include("double-entry-functions.php");
include("lib/compliance-function.php");
$date_time = date('d-m-Y  h:i:s A');
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

 
$strMainCaption = "TT TRANSFERS";
$rateDigitsRight = false;
$roundLevel = "4"; 
$roundRateTo = "8"; 
$flagUp='';
$transAction="Send";
if(defined("CONFIG_ROUND_LEVEL_RATE") && is_numeric(CONFIG_ROUND_LEVEL_RATE)){
	$rateDigitsRight = true;
	$roundRateTo = CONFIG_ROUND_LEVEL_RATE;
}
if(CONFIG_TRANS_ROUND_NUMBER == "1" && defined("CONFIG_TRANS_ROUND_LEVEL") && is_numeric(CONFIG_TRANS_ROUND_LEVEL)){
	$rateDigitsRight = true;
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}
	
$created 		= date('Y-m-d  H:i:s');
$createdBy 		= $_SESSION["loggedUserData"]["userID"];
$TTtransId		= $_REQUEST["TTtransId"];
if(!empty($_REQUEST["flagUp"]))
	$flagUp= $_REQUEST["flagUp"];
//////////

$cusdi=$_REQUEST["cusdi"];
$BenID=$_REQUEST["BenID"];
$bankid=$_REQUEST["bankid"];
$bankid2=$_REQUEST["bankid2"];
$sendingAc=$_REQUEST["sendingAc"];

$sendingCurrency=$_REQUEST["sendingCurrency"];


$amount=$_REQUEST["amount"];
	
$receivingAc=$_REQUEST["receivingAc"];

$receivingCurrency=$_REQUEST["receivingCurrency"];

$ramount=$_REQUEST["ramount"];
$rate=$_REQUEST["rate"];
$PrevStatus=$_REQUEST["PrevStatus"];

if(!empty($TTtransId) && $flagUp=="update")
{
	$transAction="Update";
	$qrTransdata="SELECT 
						t.customer_Id,
						t.ben_Id,
						t.sender_account_number,
						t.sending_currency,
						t.sending_amount,
						t.receiving_account_number,
						t.receiving_currency,
						t.receiving_amount,
						t.exchange_rate,
						t.status,
						t.internalDetails,
						t.prevAmountDetail,
						
						bb.bankId,
						bb.id as bankid2,
						bd.benID as bnkBenId,
						bd.bankName,
						bd.accNo,
						bd.branchCode,
						bd.branchAddress
					FROM
					    tt_transactions as t 
						JOIN TTbankDetails as bd ON t.id=bd.transID
						JOIN ben_banks as bb ON  bd.originalBankId=bb.bankId AND bd.accNo=bb.accountNo
					WHERE
						t.id='".$TTtransId."'";
//debug($qrTransdata,true);
	$arrTransData= selectFrom($qrTransdata);
	//debug($arrTransData);
	
	$cusdi=$arrTransData["customer_Id"];
	$BenID=$arrTransData["ben_Id"];
	$bankid=$arrTransData["bankId"];
	$sendingAc=$arrTransData["sender_account_number"];
	$sendingCurrency=$arrTransData["sending_currency"];
	$amount=$arrTransData["sending_amount"]; 
	$receivingAc=$arrTransData["receiving_account_number"];
	$receivingCurrency=$arrTransData["receiving_currency"];
	$ramount=$arrTransData["receiving_amount"];
	$rate=$arrTransData["exchange_rate"];
	$bankid2=$arrTransData["bankid2"];
	$PrevStatus=$arrTransData["status"];
	$arrComplianceDetails=$arrTransData["internalDetails"];
	$benAccount=$arrTransData["accNo"];
	$prevAmountDetail=$arrTransData["prevAmountDetail"];
	//debug($arrComplianceDetails);
	/**
	* Array of transaction amount/exchangerate/accounts before updation
	**/
	
	
	$arrBeforeUp	=	array(
							"sAcc"=>$sendingAc,
							"rAcc"=>$receivingAc,
							"sAmt"=>$amount,
							"rAmt"=>$ramount,
							"sCurr"=>$sendingCurrency,
							"rCurr"=>$receivingCurrency,
							"exRate"=>$rate
						);
	
	
}


//TT TRANSACTION STATUS ARRAY
$status_arr=array("1"=>"Pending","2"=>"Hold","3"=>"Cancelled","4"=>"Incomplete");
/**
 * Validating and Inserting the form entries into database
 */
 
if(!empty($cusdi) && !empty($BenID) && ($_REQUEST["storeData"]=="Update" || $_REQUEST["storeData"]=="Send" || !empty($_REQUEST["storeIncmpData"])))
{
	
	/**
	 * Making the exchange rate entry in note field
	 */ 
	$noteToInsert = "";
	//debug($sendingCurrency."~".$receivingCurrency);
	if(!empty($_REQUEST["sendingCurrency"]) || !empty($_REQUEST["receivingCurrency"]))
		$noteToInsert = "( Exchange Rate: 1 ".$_REQUEST["sendingCurrency"]." = ".$_REQUEST["rate"]." ".$_REQUEST["receivingCurrency"]." ) ";
		
	//$noteToInsert .= $_REQUEST["note"];
//debug($noteToInsert);
	if(!empty($rate))
		$recevingAmount = $rate * $amount;
	else
		$recevingAmount = $amount;
		
	$rateValue = $rate;
	if(empty($rateValue))
		$rateValue = number_format(($ramount / $amount),$roundRateTo,'.','');

	
	
	//Check if sender is disable due to being in compliance list
	$arrComplianceCust = selectFrom("SELECT 
											customerStatus
											 
										FROM customer 
										WHERE 
											customerID='".$_REQUEST["cusdi"]."'");
	//Check if beneficiary is disable due to being in compliance list
	$arrComplianceBen = selectFrom("SELECT 
											status 
									FROM beneficiary 
									WHERE 
										benID='".$_REQUEST["BenID"]."'");
	
	/*debug($arrComplianceCust);
	debug($arrComplianceBen);*/
	
	
	
	if($_REQUEST["storeData"]=="Update"  || $_REQUEST["storeData"]!="Update")
	{
		//Compliance Check
		$custComp=array(
							"customerID"=>$_REQUEST["cusdi"],
							"ruleFor"=>'customer'
						);
		$benfComp=array(
							"benID"=>$_REQUEST["BenID"],
							"ruleFor"=>'beneficiary'
						);
 		//debug($benfComp);
		$isComplianceCust=complainceVerify($custComp);
		$isComplianceBenf=complainceVerify($benfComp);
		
		$arrComplianceDetails=array(
									"sender" =>$isComplianceCust,
									"benef"  =>$isComplianceBenf
									);
			//debug($arrComplianceDetails,true);
		/*****Disable sender/beneficiary if any one of them found in compliance*****/
		$flagUpC='N';
		$flagUpB='N';
		if(!empty($arrComplianceDetails))
		{ 
			foreach($arrComplianceDetails as $key=>$val)
			{
				if($key=='sender' && !empty($val))
					$flagUpC='Y';
				
				if($key=='benef' && !empty($val))
					$flagUpB='Y';
					
			}
			//debug($flagUpC."=>".$flagUpB);
		    //debug($arrComplianceDetails,true);
			//Disable Sender
			$arrCustStatus = array(); 
			$arrCustStatus["customerStatus"]   	= 'Disable';
			//Disable Benficiary
			$arrBenefStatus = array();
			$arrBenefStatus["status"] 		 	= 'Disabled';
			
			if($flagUpC=='Y')
				$idCust=dataInsertionOperation($arrCustStatus,"customer", "","UPDATE", " customerID='".$_REQUEST["cusdi"]."'");
			if($flagUpB=='Y')
				$idBenef=dataInsertionOperation($arrBenefStatus,"beneficiary", "","UPDATE", " benID ='".$_REQUEST["BenID"]."'");
	
		}//debug($idCust."~~".$idBenef,true);
			$arrComplianceDetails=serialize($arrComplianceDetails);
	}
	
	
	if($arrComplianceCust["customerStatus"]=="Disable" || $arrComplianceBen["status"]=="Disabled" || !empty($isComplianceCust) || !empty($isComplianceBenf))
	{
		
		$status=2;  //2=Hold
	}
	elseif(!empty($_REQUEST["storeIncmpData"]))
	{
		$status=4;   //4=Incomplete
	}
	else
	{
		$status=1;   //1=Pending
	} 
		
	//insert/update data into TT TRANSACTION TABLE
	$arrTTtrans = array();
	$arrTTtrans["customer_Id"]        		  = $_REQUEST["cusdi"];
	$arrTTtrans["ben_Id"] 		    		  = $_REQUEST["BenID"];
	$arrTTtrans["sender_account_number"]      = $_REQUEST["sendingAc"];
	
	$arrTTtrans["sending_currency"]           = $_REQUEST["sendingCurrency"];
	
	$arrTTtrans["sending_amount"]             = $_REQUEST["amount"]; 
	$arrTTtrans["receiving_account_number"]   = $_REQUEST["receivingAc"];
	$arrTTtrans["receiving_currency"]         = $_REQUEST["receivingCurrency"];
	$arrTTtrans["receiving_amount"]           = $_REQUEST["ramount"];
	$arrTTtrans["exchange_rate"]              = $_REQUEST["rate"];
	$arrTTtrans["internalDetails"]			  = $arrComplianceDetails;
	$arrTTtrans["status"]                     = $status;
	
	//debug($arrTTtrans);
	if(!empty($TTtransId) && $flagUp=="update" && $_REQUEST["storeData"]=="Update")
	{
		$idUp=dataInsertionOperation($arrTTtrans, "tt_transactions", "","UPDATE", " id='$TTtransId'");
		if($idUp)
		{
			$arrAfterUp	=	array(
							"sAcc"=>$_REQUEST["sendingAc"],
							"rAcc"=>$_REQUEST["receivingAc"],
							"sAmt"=>$_REQUEST["amount"],
							"rAmt"=>$_REQUEST["ramount"],
							"sCurr"=>$_REQUEST["sendingCurrency"],
							"rCurr"=>$_REQUEST["receivingCurrency"],
							"exRate"=>$_REQUEST["rate"]
						);
		}
	
	}	
	else
	{	
		$arrTTtrans["created"]                    = $created;
		$arrTTtrans["created_by"]                 = $createdBy;
		$TTtransId = dataInsertionOperation($arrTTtrans, 'tt_transactions');
	}
	//activities($_SESSION["loginHistoryID"],"UPDATE",$customerID,"customer",$descript);
	
	

	//InsertData into bankdetail table of ttt transaction
	
		$arrBankData = array();
		$arrBankData["benID"] 		    		  	= $BenID;
		
		$arrBankData["bankName"] 		    		= $_REQUEST["BankNam"];
		$arrBankData["accNo"] 		    		  	= $_REQUEST["AccNum"];		
		$arrBankData["branchCode"]           		= $_REQUEST["BrCode"];
		$arrBankData["branchAddress"]             	= $_REQUEST["BrAdd"]; 
		$arrBankData["originalBankId"]             	= $_REQUEST["bankid"];
	
		if(!empty($TTtransId) && $flagUp=="update" && $_REQUEST["storeData"]=="Update")
			dataInsertionOperation($arrBankData, 'TTbankDetails',"","UPDATE", " transID='$TTtransId'");
		else
		{
			$arrBankData["transID"] = $TTtransId;
			$NewBankId = dataInsertionOperation($arrBankData, 'TTbankDetails');
		}
	
	
//	/////lEDGERS UPDATION


		// get total tt transfers count and make transID for tt transfer
		$type = 'TT';
		$args = array('flag'=>'getTransID','type'=>$type,'CancelTrans'=>$TTtransId);
		$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'TT-'.;
		if(!empty($returnData['transID'])){
			$transID = $returnData['transID'];
			$transType = $type;
		}
		/**
		* Comparing both transaction detail arrays at the time of updation. 
		* If both arrays are same i.e., trans details are not changed then ledgers will not be affected.
		*/
		if(!empty($TTtransId) && $flagUp=="update" && $_REQUEST["storeData"]=="Update")
			$arrDiff=array_diff($arrBeforeUp, $arrAfterUp);
			
		/** 
		* If trans flag is UPDATE...and trans status changes to hold..and amount details are changed..then save $arrBeforeUp to make 		* cancel entery of this amount while unholding trans
		**/
		if(!empty($arrDiff) && !empty($TTtransId) && $flagUp=="update" && $status==2 && empty($prevAmountDetail))
		{
			$prevAmountDetail=serialize($arrBeforeUp); 
			
		}
		else
			$prevAmountDetail='';
			
		$qrUpPrevAmountDetail="UPDATE tt_transactions SET prevAmountDetail='".$prevAmountDetail."' WHERE id='$TTtransId'";
		update($qrUpPrevAmountDetail);
		
		if(!empty($arrDiff) || $_REQUEST["storeData"]=="Send")
		{
			if(!empty($TTtransId) && $flagUp=="update" && $_REQUEST["storeData"]=="Update")
			{
				/** First send a reverse entry in ledger for specific transaction then update
				**  qrPrevEntry is to check whether any previous entry of this transaction is found in ledger or not 
				**  if previous entry is found only then reverse entry will be made.
				**/
				/*debug($arrBeforeUp);
				debug($arrAfterUp);*/
				
					$PrevEntry='Y';
					if($PrevStatus==2 || $PrevStatus==4) 
					{
						$qrPrevEntry=selectFrom("SELECT 
														id 
												  FROM
														account_ledgers
												  WHERE
														transID='".$transID."'
												");
						
						
						if(empty($qrPrevEntry))
							$PrevEntry="N";
						else
							$PrevEntry="Y";	
					}
					//debug($qrPrevEntry,true);							
					if($status==1 && $PrevEntry=='Y')
					{
						$arguments = array(
											'flagTradingLedger'=>true,
											'transID'=>$transID,
											'amount'=>$ramount, //ramount
											'ramount'=>$amount, //amount
										
											'sendingAc'=>$receivingAc, //receivingAc
											'receivingAc'=>$sendingAc, //sendingAc
											
											'sendingCurrency'=>$receivingCurrency, //receivingCurrency
											'receivingCurrency'=>$sendingCurrency, //sendingCurrency
											 
											'createdBy'=>$createdBy,
											'created'=>$created,
											'extraFields'=>array('transType'=>$transType),
											'status'=>'C');
								
								$acountLF = accountLedger($arguments);
								//debug($acountLF,true);
						
					}
				
				
				
			}
			if($status==1 && ($_REQUEST["storeData"]=="Update" || $_REQUEST["storeData"]=="Send"))
			{
				$arguments = array(
									'flagTradingLedger'=>true,
									'transID'=>$transID,
									'amount'=>$_REQUEST["amount"],
									'ramount'=>$_REQUEST["ramount"],
								
									'sendingAc'=>$_REQUEST["sendingAc"],
									'receivingAc'=>$_REQUEST["receivingAc"],
									
									'sendingCurrency'=>$_REQUEST["sendingCurrency"],
									'receivingCurrency'=>$_REQUEST["receivingCurrency"],
									
									'note'=>addslashes($noteToInsert),
									'createdBy'=>$createdBy,
									'created'=>$created,
									'extraFields'=>array('transType'=>$transType)
									);
									
				//debug($arguments,true);			
				$acountLF = accountLedger($arguments);
				
				if($acountLF ){
					$TranID="TT-".$TTtransId;
					
					$arrAccDetail = array();
					$arrAccDetail["currencySell"]	=$_REQUEST["sendingCurrency"];
					$arrAccDetail["currencyBuy"] 	= $_REQUEST["receivingCurrency"];
					$arrAccDetail["amountSell"] 	= $_REQUEST["amount"];		
					$arrAccDetail["amountBuy"]      = $_REQUEST["ramount"];
					$arrAccDetail["accountSell"]    = $_REQUEST["sendingAc"]; 
					$arrAccDetail["accountBuy"]     = $_REQUEST["receivingAc"];
					//$arrAccDetail["exchangeRate"]   = $rateValue;
					$arrAccDetail["exchangeRate"]   =  $_REQUEST["rate"];
					$arrAccDetail["description"]    = addslashes($noteToInsert); 
					//debug($arrAccDetail,true);		
					
					if(!empty($TTtransId) && $flagUp=="update" && $_REQUEST["storeData"]=="Update")
						dataInsertionOperation($arrAccDetail,'account_details',"","UPDATE", " transID='$TranID'");
					else
					{
						$arrAccDetail["created"]        = $created;
						$arrAccDetail["transID"] 		= $TranID;
						dataInsertionOperation($arrAccDetail,'account_details');
					}
					
				}
			}
		}
	
	//debug($TTtransId."-->0".$flagUp,true);
	if(!empty($TTtransId) && $flagUp!="update")
	{

		echo '<script>window.open ("/admin/TT_Receipt.php?TransId='.$TTtransId.'","TT Transfer Receipt","location=0,scrollbars=1,width=350,height=550");</script>';	
	}
	else
	{
		redirect($_REQUEST["src"]."?msg=$TTtransId");
		//exit;
		
		
	}

	
	
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $strMainCaption ?></title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<!--<script type="text/javascript" src="maxlength.js">

/***********************************************
* jQuery MaxLength for INPUT fields- by JavaScript Kit (www.javascriptkit.com)
* This notice must stay intact for usage
* Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code
***********************************************/

</script>-->
<script>
	$(document).ready(function(){
	
	/**
	* Amount digits lendth validating limit on sending funds
	**/
	$.validator.addMethod("limitedDigitsOnly", 
		function(value, element) {
			var objVal = value;
			var arrObjVal = objVal.split('.');
			if(arrObjVal[0].length > 9)
				return false;
			else
				return true;
		}, 
		"<br/>Maximum 9 digits only."
	);
	$("#sendingCurrency").val('');
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		   $("#storeIncmpData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		   $("#storeIncmpData").attr("disabled",false);
		});
		
		$("#estimated_date").datePicker();
		$("#issue_date").datePicker({
			startDate: '01/01/2000'
		});
		
		//$("#date_in").click(function(){
			$("#date_in").dpSetStartDate('<?=date("d/m/Y")?>');
		//});
		<?php if(!empty($TTtransId) && $flagUp=="update")
		{	//LOAD INFORMATION IN CASE OF EDIT
					//disable few sections on update
		?>
			$('#customer_input').attr('disabled', true);
			/*$(".disableMe a").click(function () {
			$(this).hide();
			return true;
			});*/
			LoadCustInfo('0',$("#cusdi").val());
			SelectBen($("#BenID").val());
			//$('a.disableMe').css('visibility','hidden');			
	<?php } ?>
		
		if($("#TTtransId").val())
		{
		 $("#storeIncmpData").hide();
		}
		$("#customer_input").autocomplete('searchCustomer-ajax.php', {
				mustMatch: true,
				maxItemsToShow : -1,
			}
		);
		$("#customer_input").result(function(event, data, formatted) {
			if (data)
			{
				//alert(data);
				$("#cusdi").val(data[1]); 
				LoadCustInfo('0',$("#cusdi").val());
				$("#BankLabel").hide(); 
				$("#BankInput").hide();
				$("#bankid").val('');
				if(!$("#TTtransId").val())
				{
					$("#TransDetails").hide();
					$("#sendingAc").val('');
					$("#sendingCurrency").val('');
					
					$("#closingBal").val('');
					$("#amount").val('');
					$("#receivingAc").val('');
					$("#receivingCurrency").val('');
					
					$("#closingBalRECV").val('');
					$("#ramount").val('');
					$("#exchangeRate").hide();
					
					$("#sendingAc").val('');
					//$("#receivingAc").val('');
					$("#rate").val('');
					
					$("#ta").val('');
					$("#ra").val('');
				}
			}
		});
		
		$("#showHideCustomerDetailBtn").click(function () {
		 
			if($("#showHideCustomerDetailBtn").html() == "[-]")
			{
				$("#customerDetails").hide();
				$("#showHideCustomerDetailBtn").html("[+]");
			}
			else
			{
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").html("[-]");
			}
		});
		///Bank infoi
		
		//if($("#BenID").val()!="")
		//{
			//?benID='+$("#BenID").val()+'&searchType='+$('input[name=searchType]:checked').val()
		$("#bank_input").autocomplete('searchBank-ajax.php',
			 {
				mustMatch: true,
				maxItemsToShow : -1,
				deferRequestBy: 200,
				cacheLength: 0,
				extraParams: {
					benID: function() { return $("#BenID").val() },
					searchType: function() { return $('input[name=searchType]:checked').val() }
				}
			}
		);
		$("#bank_input").result(function(event, data, formatted) {
				if (data)
				{
					//console.log(data);
					$("#bankid").val(data[1]);
					$("#bankid2").val(data[2]);
					//alert($('input[name=searchType]:checked').val());
					var searchTypeV = $('input[name=searchType]:checked').val();//$(".searchType:checked").val();
					 $("#BankLabel").show(); 
					 $("#BankInput").show();
					 
					searchBank($("#BenID").val(),searchTypeV);
	
				}
			});
		//}
	
		$("#receivingAc").change(function(){
			receivingAcChanged();
		});
		 
		<?php //if(empty($TTtransId) && $flagUp!="update")
		//{	
		?>
		$("#newCustomerBtn").click(function(){
			window.open ("/admin/add-customer.php?callFrom=TT","AddCustomer","location=1,scrollbars=1,width=900,height=450"); 
		});
		$("#newCompanyBtn").click(function(){
			window.open ("/admin/add-company.php?callFrom=TT","AddCompany","location=1,scrollbars=1,width=900,height=450"); 
		});
		
		$("#newBenBtn").click(function(){
			window.open ("/admin/add-beneficiary.php?callFrom=TT&custID="+$("#cusdi").val(),"AddBeneficiary","location=1,scrollbars=1,width=900,height=450"); 
		});
		
		
		$("#newBenCompanyBtn").click(function(){
			window.open ("/admin/add-ben-company.php?callFrom=TT&customerID="+$("#cusdi").val(),"AddBeneficiaryAsCompany","location=1,scrollbars=1,width=900,height=450"); 
		}); 
		<?php //} ?>
		$('img').cluetip({splitTitle:'|'});
		
		
		/* vaildations of the fields start */
		var doValidate;
		var doValidateBankAcc;
		/*$(".searchType").click(function () { 
			if($(".searchType").val()==1 && $("#AccNum").val()==null && $("#benAccount").val()==null)
				doValidateBankAcc = true;
			else
				 doValidateBankAcc = false;
				
		});*/
		
		$('#storeData').click(function () { doValidate = true; });
		$('#storeIncmpData').click(function () { 
													if($("#amount").val()==0)
														$("#amount").val(''); 
													if($("#ramount").val('')==0)
														$("#ramount").val('');
													doValidate = false;
												});
		/*if($("#TTtransId").val()!=null)
		{	
			if($("#AccNum").val()!=null && $(".searchType").val()==0){
				doValidateBankAcc = false;
			}
			else
			{
				doValidateBankAcc = true; 
			}
		}
		else if($("#AccNum").val()==null && ($("#benAccount").val()==0 || $(".searchType").val()==1))
		{	doValidateBankAcc = true;}
		else
		doValidateBankAcc = false;*/
		
		$("#TT_form").validate({
		
			//debug: true,
			rules: {
				
				cusdi: "required",
				
				
				BenID: {
					required: true
				},
				bankid:{
					required:true
				},
				AccNum:{
					required:true
				},
			
				benAccount:{
					required:function () { 
					//var parentId=$(".addUpdateBank").parent().attr("id").is(':visible');
						 $(".addUpdateBank:visible").trigger("click");
						 return false;
						
						//alert("s");
					}//$(".addUpdateBank:visible").trigger("click");required:true
				},
				
				sendingAc: {
							required: function () { return doValidate; }

				},
				amount: {
					required: function () { return doValidate; },

					number:function () { return doValidate; },
					min   :0.0000001,
					limitedDigitsOnly:true
					//,maxlength   :9
				},
				receivingAc: {
					required: function () { return doValidate; }

					
				},
				ramount: {
					required: function () { return doValidate; },
					number: function () { return doValidate; },
					min   :0.0000001
					//,maxlength   :9
				},
				
				
			},
			messages: {
				
				customer_input: "<br />Please select the customer",
				
				BenID: "<br />Please Select Beneficiary.",
				bankid: "<br /> Please Select Bank (Account)",
				AccNum:"<br /> Please Enter(Account No.)",
				benAccount: "<br /> Please Add Bank (Account)",
				sendingAc: "<br />Please Select Sending Account.",
				amount:{ 
						required:"<br />Please Select Sending Amount.",
						number:  "<br/>Enter valid amount",
						min:     "<br />Sending Amount can't be less than zero"
						},
				receivingAc: "<br />Please Select Sending Amount",
				ramount:{ 
							required:"<br />Please Enter valid Receiving Amount",
							number:  "<br />Valid amount required",
							min:     "<br />Receiving Amount can't be less than zero",
							limitedDigitsOnly:"<br/>Maximum 9 digits only."
						},  
					
				
				
			},
			/*submitHandler: function(form) {
				var a=confirm('Are you sure to create transaction');
				if(a)
				{
					document.getElementById("TT_form").submit();
				}
				
		   }*/
		});
		
		
		////////////
			var roundLevel = "<?=$roundLevel?>";
		$(".decimalsLimit").keyup(function(){
			if(roundLevel!=""){
				var decNumber = $(this).val();
				var decArray  = decNumber.split(".");
				var decValue  = decArray[1];
				if(decValue)
				{
					if(decValue.length>roundLevel)
						$(this).val(decNumber.substring(0,decNumber.length-1));
				}
			}
		});
		$(".decimalsLimit").blur(function(){
			if(roundLevel!=""){
				var fixedDec = parseFloat($(this).val());
				if(!isNaN(fixedDec))
					$(this).val(fixedDec.toFixed(roundLevel));
			}
		});	
		var roundRateTo = "<?=$roundRateTo?>";
		$(".decimalsLimitRate").keyup(function(){
			if(roundRateTo!=""){
				var decNumber = $(this).val();
				var decArray  = decNumber.split(".");
				var decValue  = decArray[1];
				if(decValue)
				{
					if(decValue.length>roundRateTo)
						$(this).val(decNumber.substring(0,decNumber.length-1));
				}
			}
		});
		$(".decimalsLimitRate").blur(function(){
			if(roundRateTo!=""){
				var fixedDec = parseFloat($(this).val());
				if(!isNaN(fixedDec))
					$(this).val(fixedDec.toFixed(roundRateTo));
			}
		});
		$("#revcal").click(function(){
			calculateAmount();
		});
		
	
		
	});
	
	
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}	

function SelectBen(intParam,ComplianceFlag)
{//
/*
newBenSelected is used while editing a transaction.it hold id of new selected ben from assoc ben list. 
*/
	var newBenSelected='';
	$("#BankLabel").hide(); 
	$("#BankInput").hide();
	$("#BankDetails").hide();
	$("#TransDetails").hide();
	
	$("#Selectedbeneficiary").load("searchBeneficiary-ajax.php?BenId="+intParam+"&ComplianceFlag="+ComplianceFlag+"&getBenDetails=getDetail");
	$("#Selectedbeneficiary").show();
	if($("#TTtransId").val())
	{
		if($("#BenID").val() && ($("#BenID").val()!=intParam))
		{
			var newBenSelected=1;
		}	
	}
	$("#BenID").val(intParam);
	//Bank details
	
	if(intParam!='')
	{
		
		
		var searchTypeV = $('input[name=searchType]:checked').val();//$(".searchType:checked").val();
		 $("#BankLabel").show(); 
		 $("#BankInput").show();
		 
		searchBank($("#BenID").val(),searchTypeV,newBenSelected);
	}
		
	
}

function searchBank(BenID,searchTypeV,newBenSelected)
{
	
					
			if($("#bankid").val() !='' && (newBenSelected!=1 && $("#TTtransId").val()!=null))
			{
					$.ajax({
							
							url: "searchBank-ajax.php?bankId="+$("#bankid").val()+"&BenAccNo="+$("#bankid2").val()+"&BenID="+BenID+"&searchTypeV="+searchTypeV+"&benAccount="+'<?=$benAccount?>'+"&getDetails=banks&RequestFrom=TT",
							async: true,
							success: function(funReturn){
								valToResponse = funReturn;
								
								$("#BankDetails").html(valToResponse);
								$("#BankDetails").show();
								
								/******************************/
								showTransDetail();
								/******************************/
								
								
							}
					});
					
				}
				else if(newBenSelected==1 && $("#TTtransId").val()!=null)
				{
					$.ajax({
							url: "searchBank-ajax.php?bankId="+$("#bankid").val()+"&BenAccNo="+$("#bankid2").val()+"&BenID="+BenID+"&searchTypeV="+searchTypeV+"&benAccount="+'<?=$benAccount?>'+"&getDetails=banks&RequestFrom=TT",
							async: true,
							success: function(funReturn){
								valToResponse = funReturn;
								
								$("#BankDetails").html(valToResponse);
								$("#BankDetails").show();
								showTransDetail();
								}
						});
				}
			//});

}
function showTransDetail()
{
	$("#TransDetails").show();
	if($("#TTtransId").val())
	{
		$selectedAcc='<?=$sendingAc?>';
	}
	else
	{
		$selectedAcc=$("#sendingAc").val();
	}
	var data = {
		CustID:$("#cusdi").val(),
		sendingAc:$selectedAcc
		
		};
	$.ajax({
			url: 'TTAccount-ajax.php',
			async: false,
			data: data,
			success: function(funReturn) {
			valToResponse = funReturn;
			$("#SenderAccountsDropDown").html(valToResponse);
			$("#SenderAccountsDropDown").show();
			if($("#TTtransId").val())
			{	
				/*	passing 1 to sendingAcChanged function 
					so that it dznt empty recvng accnt number (happens when sending acc is changed not when trans is loaded for first time for editing)
				*/
				sendingAcChanged(1);
				//INCase of Update :disable all clicks events of ebenficiary section
				$('a.disableMe').attr('onClick',' ');
				$('a.disableMe').unbind('click');
				$('a.disableMe').css('color','grey');	
			}
		}
	
	});
}
function sendingAcChanged(param)
{

	if(param !='1')
	{
		$("#receivingAc").val(''); 
	}
	$("#receivingAc option").show();
	var data = {
				sendingAcc:$("#sendingAc").val(),
				getCurr:'sendingAcCurr'
			};
			$.ajax({
				url: 'TransDetail-ajax.php',
				async: false,
				data: data,
				success: function(funReturn) {
					valToResponse = funReturn;
					var retVal=valToResponse.split("~");
					
					$("#sendingCurrency").val(retVal[0]);
					$("#closingBal").val(retVal[1]);
					$("#receivingAc option[value="+$('#sendingAc').val()+"]").hide();
					receivingAcChanged();
					
				}
						  
			});
}

function receivingAcChanged()
{
	var data = {
		receivingAcc:$("#receivingAc").val(),
		getCurr:'receivingAcCurr'
	};
	$.ajax({
			url: 'TransDetail-ajax.php',
			async: false,
			data: data,
			success: function(funReturn) {
				valToResponse = funReturn;
				var retVal=valToResponse.split("~");
				//alert(retVal);
			
				$("#receivingCurrency").val(retVal[0]);
				$("#closingBalRECV").val(retVal[1]);
				/**REMINDER  BY NIMRA : VERIFY THIS CODE IN CASE RECVNG ACNT IS CHANGED ON EDIT"**/
				if(!$("#TTtransId").val())
				{
					updateInterface();
				}
				else
				{
					if(document.getElementById("sendingCurrency").value != document.getElementById("receivingCurrency").value)
						$("#exchangeRate").show();
					
					else{
							$("#rate").val('1.00000000');
							$("#exchangeRate").hide();
						
						}
					document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
					document.getElementById("rc").innerHTML = document.getElementById("receivingCurrency").value;
				}
				//alert(retVal);
			}
					  
		});

}
function AddBank(BenID,BankID)
{
	var data = {
				BenID: BenID,
				bankid: BankID,
				AccNum: $("#AccNum").val(),
				BrCode: $("#BrCode").val(),
				BrAdd: 	$("#BrAdd").val(),
				addBank: 'Y'
			 };
	$.ajax({
			url: 'searchBank-ajax.php',
			data: data,
			async: false,
			success: function(funReturn){
				valToResponse = funReturn;
				if(valToResponse !='')
				{
					$("#addBank").hide();
					$("#addEvent").hide();
					$("#addBankMsg").show();
					$("#upBankMsg").show();
					$("#upBankEvent").show();
					$("#benAccount").val('1');
				}
			}
	});
	$("#bankid").val(BankID);
	return true;
}

function UpdateBank(BenBankID)
{
	if($("#AccNum").val() == "" || $("#BrCode").val() == "" || $("#BrAdd").val() == "")
	{
		alert("Required fields are empty.");
		return false;
	}
	var data = {
				BenBankID: BenBankID,
				
				AccNum: $("#AccNum").val(),
				BrCode: $("#BrCode").val(),
				BrAdd: 	$("#BrAdd").val(),
				updateBank: 'Y'
			 };
	$.ajax({
			url: 'searchBank-ajax.php',
			data: data,
			async: false,
			success: function(funReturn){
				valToResponse = funReturn;
				if(valToResponse !='')
				{
					//$("#updateBank").hide();
					$("#upBankMsg").show();
					$("#upBankEvent").show();
					$("#benAccount").val('1');
				}
			}
	});
	return true;
}

function LoadCustInfo(paramid,paramNewCust){
//alert(paramid+"=paramid");
//					alert(paramNewCust+"=paramNewCust");
			if($("#cusdi").val() !='')
			{
				
				$("#customerDetails").load("searchCustomer-ajax.php?cusdi="+$("#cusdi").val()+"&getCustomerDetails=get&RequestFrom=TT");
				$("#customerDetails").show();
				if(paramid==0 && paramNewCust!=null)
				{
					
					$("#beneficiaryDetails").load("searchBeneficiary-ajax.php?cusdi="+$("#cusdi").val()+"&getBenDetails=get");
					$("#beneficiaryDetails").show();
					
					$("#showHideCustomerDetailBtn").show();
					$("#newBenBtn").show();
					$("#newBenCompanyBtn").show();
					$("#Selectedbeneficiary").html('');
					
					$("#bank_input").val('');
					$("#BankInput").hide();
					$("#BankDetails").html('');
				}
				
				
		    }
}

function calculateAmount()
{

	if($('input[name=revcal]').is(':checked'))
		var totCnt = 1;
	var opr = '*';
	if(totCnt>0)
		opr = '/';


	var roundLevel = "<?=$roundLevel?>";
	var roundRateTo = "<?=$roundRateTo?>";
	
	var finalAmount;
	var amount = document.getElementById("amount").value;
	var rate = document.getElementById("rate").value;

	if(rate<=0){
		var rate = 1;
		rate = rate.toFixed(roundRateTo);
 	    document.getElementById("rate").value = rate;
	}
	if(amount != "" && rate != "")
	{
		finalAmount = amount * rate;
		
		if(opr == '/')
			finalAmount = amount / rate;
		else
			finalAmount = amount * rate;
		finalAmount = finalAmount.toFixed(roundLevel);
		document.getElementById("ramount").value = finalAmount;
	}
	
	if(document.getElementById("sendingCurrency").value == document.getElementById("receivingCurrency").value)
	{
		var amountValue = document.getElementById("amount").value;
		if(amountValue){
			amountValue = parseFloat(amountValue);
			document.getElementById("ramount").value = amountValue.toFixed(roundLevel);
		}
	}
	
}
function updateInterface()
{

	var roundLevel = "<?=$roundLevel?>";
	var roundRateTo = "<?=$roundRateTo?>";
	var defaultRate = 1;
	defaultRate = defaultRate.toFixed(roundRateTo);
	var amountObj = document.getElementById("amount");
	if(amountObj){
		var amountValue = "";
		if(amountObj.value!=""){
			amountValue = parseFloat(amountObj.value);
			if(amountValue)
				amountValue = amountValue.toFixed(roundLevel);
		}
	}

	if(document.getElementById("sendingCurrency").value != "" && document.getElementById("receivingCurrency").value != "")
	{
		
		if(document.getElementById("sendingCurrency").value == document.getElementById("receivingCurrency").value)
		{
			$("#exchangeRate").hide();
			document.getElementById("ramount").value = amountValue;
		}
		else
		{
		
		
			$("#exchangeRate").show();
			document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
			document.getElementById("rc").innerHTML = document.getElementById("receivingCurrency").value;
			
			document.getElementById("ramount").value = amountValue;
			document.getElementById("rate").value = defaultRate;
			//document.getElementById("rate").value = defaultRate.toFixed(roundRateTo);
		}
	}
	/*document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("receivingCurrency").value;*/

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("receivingCurrency").value;	 
}


</script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form name="TT_form" id="TT_form"  method="post" action="">
<?php if(!empty($_REQUEST["src"])){?>
 <a href="<?=$_REQUEST["src"]?>" style="font-size:12px">GO BACK</a>
 <?php } ?>
  <table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
    <tr>
      <td colspan="4" align="center" bgcolor="#DFE6EA"><strong>
        <?=$strMainCaption?>
        </strong> </td>
    </tr>
    <tr bgcolor="#ededed">
      <td colspan="4" align="center" class="tdDefination" style="text-align:center"> All fields mark with <font color="red">*</font> are compulsory Fields. </td>
    </tr>
    <tr bgcolor="#ededed">
      <td colspan="4" align="right">
	  <a href="javascript:void(0)" id="newCustomerBtn" class="links disableMe">[ Add New Customer ]</a>
	   <a href="javascript:void(0)" id="newCompanyBtn" class="links disableMe">[ Add New Customer as Company ]</a> </td>
    </tr>
    <tr bgcolor="#ededed">
      <? 
			$searchName ="";
			if($searchByFlag)
				$searchName = "by Names";
		?>
      <td width="34%" class="tdDefination"><b>Search Customer
        <?=$searchName?>
        :&nbsp;</b><font color="red">*</font></td>
      <td width="66%" colspan="3" align="left" ><input type="text" name="customer_input" id="customer_input" size="80" maxlength="150" />
        <input type="hidden" name="cusdi" id="cusdi" value="<?=$cusdi?>" />
        &nbsp; <a href="javascript:void(0)" id="showHideCustomerDetailBtn" style="display:none; text-decoration:none" title="Show Hide Customer Details">[-]</a> </td>
    </tr>
    <tr bgcolor="#ededed">
      <td colspan="4" id="customerDetails" style="display:none">&nbsp;</td>
    </tr>
    <tr bgcolor="#ededed">
      <td colspan="4" align="left"><fieldset>
        <legend>Select Beneficiary</legend>
        <table align="left" width="100%">
          <tr bgcolor="#ededed">
           
			 <td colspan="4" align="right">
			 <a href="javascript:void(0)" id="newBenBtn" class="links disableMe" style="display:none">[ Add New Beneficiary ]</a> 
			 <a href="javascript:void(0)" id="newBenCompanyBtn" class="links disableMe" style="display:none">[ Add New Beneficiary as Company ]</a> </td>
			
          </tr>
          <tr bgcolor="#ededed">
            <td colspan="4" id="beneficiaryDetails" style="display:none">&nbsp;</td>
          </tr>
        </table>
        </fieldset>
        <br />
        <fieldset>
        <legend>Beneficiary Detail</legend>
        <table align="left" width="100%">
          <tr bgcolor="#ededed">
            <td colspan="4" id="Selectedbeneficiary" style="display:none">&nbsp;</td>
          </tr>
        </table>
        </fieldset>
        <br />
        <fieldset>
        <legend>Bank Details</legend>
        <div style="" id="BankINFO">
          <table align="left" width="100%">
            <tr bgcolor="#ededed">
              <td width="25%" class="tdDefination"  id="BankLabel" style="display:none"><b>Search Bank
                :&nbsp;</b><font color="red">*</font></td>
              <td align="left" colspan="3" id="BankInput"  style="display:none"><input type="text" maxlength="150" size="80" id="bank_input" name="bank_input"/>
			 
                <input type="hidden" value="<?=$bankid?>" id="bankid" name="bankid">
				<input type="hidden" value="<?=$bankid2?>" id="bankid2" name="bankid2">
                &nbsp; <!--<a title="Show Hide Bank Details" style="text-decoration: none;" id="showHideBankDetailBtn" href="javascript:void(0)">[-]</a>--> 
				Existing Account <input type="radio" name="searchType" value="0" checked="checked" class="searchType"/> 
				&nbsp;New Account<input type="radio" name="searchType" value="1" class="searchType"/>
				<!--<input type="hidden" name="searchType" id="searchType" value="0">--></td>
			</tr>
			 <!--<tr bgcolor="#ededed">
				 <td align="right" colspan="3" id="BankRad">Banks<input type="radio" name="BankInfo" id="BankInfo" value="" /></td>
				 <td align="left" colspan="3" id="BenBankRad">Ben Banks<input type="radio" name="BenBank" id="BenBank" value="1"/></td>
            </tr>-->
			
			 <tr bgcolor="#ededed">
    		  	<td colspan="4" id="BankDetails" style="display:none">&nbsp;</td>
   			 </tr>
          </table>
        </div>
        </fieldset>
		  <br />
        <fieldset>
        <legend>Transaction Details</legend>
        <div id="TransDetails" name="TransDetails" style="display:none"> 
		<table align="left" width="100%">
            <tr bgcolor="#ededed">
              <td width="20%"><font color="#005b90"><strong>Sending Account <font color="#ff0000">*</font></strong></font></td>
              <td id="SenderAccountsDropDown" name="SenderAccountsDropDown" style="display:none">&nbsp;</td>
            </tr>
            <tr bgcolor="#ededed">
              <td>Closing Balance [Sending Acc]</td>
			  <td><input type="text" name="closingBal" id="closingBal" value="" readonly="readonly"></td>
            </tr>
            <tr bgcolor="#ededed">
              <td><font color="#005b90"><strong>Sending Funds<font color="#ff0000">*</font></strong></font></td>
			
              <td><input type="text" id="amount" name="amount" style="font-family:verdana; font-size: 11px" value="<?=$amount?>" class="decimalsLimit" onBlur="calculateAmount();" autocomplete="off" />
			<!--   data-maxsize="9"-->
                &nbsp;<b id="ta"></b> </td>
            </tr>
            <tr bgcolor="#ededed">
              <td><font color="#005b90"><strong>Receiving Account <font color="#ff0000">*</font></strong></font></td>
              <td><?php
					//$agents = array();
					
					$strSql = "SELECT 
									id,
									accounNumber,
									accounType,
									accountName,
									currency 
								FROM 
									accounts 
								WHERE 
									status = 'AC' 
									AND CustID = ''";
					$src = selectMultiRecords($strSql);
					//}
				?>
                <select name="receivingAc" id="receivingAc">
                  <option value="">- Select Account -</option>
                  <?php
							for ($i=0; $i < count($src); $i++)
							{
							
						?>
                  <option value="<?=$src[$i]["accounNumber"]; ?>"><? echo $src[$i]["accountName"] . " [" . $src[$i]["accounNumber"] . "]" ; ?></option>
                  <?php
							
							}
						?>
                </select>
                <script language="JavaScript">
			   			SelectOption(document.forms.TT_form.receivingAc,"<?=$receivingAc; ?>");
			   </script>
                &nbsp;
			<input type="text" name="receivingCurrency" id="receivingCurrency" style="font-family:verdana; font-size: 11px;border:hidden;font-weight:bold; background-color:#ededed;"   value="" readonly="readonly">              </td>
            </tr>
           
            <tr bgcolor="#ededed">
              <td >Closing Balance [Receiving Acc]</td>
           
              <td><input type="text" name="closingBalRECV" id="closingBalRECV" value="" readonly="readonly"></td>
            </tr>
            <tr bgcolor="#ededed" id="exchangeRate" style="display:none">
              <td><font color="#005b90"><strong>Exchange Rate<font color="#ff0000">*</font></strong></font></td>
              <td><b> 1 </b><span id="sc"></span> =
                <input type="text" size="15" id="rate" name="rate" style="font-family:verdana; font-size: 11px" value="<?=$rate?>" onBlur="calculateAmount();" class="decimalsLimitRate"/>
                <span id="rc"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#005b90"><strong>Reverse Calculate</strong></font>
                <input type="checkbox" id="revcal" name="revcal"/>              </td>
            </tr>
            <tr bgcolor="#ededed">
              <td><font color="#005b90"><strong>Receiving Funds</strong><font color="#ff0000">*</font></font></td>
              <td><input type="text" id="ramount" name="ramount" style="font-family:verdana; font-size: 11px" value="<?=$ramount?>" class="decimalsLimit" readonly="readonly"  autocomplete="off" />
                &nbsp;<b id="ra"></b> </td>
            </tr>
          </table>
		  </div>
        </fieldset></td>
    </tr>
    <tr bgcolor="#ededed">
      <td colspan="4" align="center">
	  	<input type="submit" id="storeData" name="storeData" value="<?=$transAction?>"/>
        &nbsp;&nbsp;
		<input type="submit" id="storeIncmpData" name="storeIncmpData" value="Save Incomplete"/>
		 &nbsp;&nbsp;
        <input type="reset" value="Clear Fields" />
        <input type="hidden" name="BenID"  id="BenID" value="<?=$BenID?>" />  
		<input type="hidden" name="TTtransId" id="TTtransId" value="<?=$_REQUEST["TTtransId"]?>" /> 
		<input type="hidden" name="PrevStatus" id="PrevStatus" value="<?=$PrevStatus?>"/> 
		   </td>
    </tr>
  </table>
</form>
</body>
</html>
