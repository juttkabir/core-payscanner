<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
/*	#4632
	Author : Aslam Shahid
	Description :	Right for create transaction. range of time given.
					Preference given to the Individually time assigned i.e Super Agent or Super Distributor then All.
					To only work for individually. Disable right for All on Valid System Time functionality.
*/
if(defined("CONFIG_ADD_USER_RIGHT") && CONFIG_ADD_USER_RIGHT!="0" && $_REQUEST["create"]!="" && $_REQUEST["transID"]=="" && $_REQUEST["benID"]=="")
{
$queryRights = "select * from setUserRights where userGroup = '".$agentType."' AND rightName  = 'Create Transaction' AND rightStatus='1'";
$rightsData = selectFrom($queryRights);

if($rightsData==""){
	$queryRights = "select * from setUserRights where userGroup = 'All' AND rightName  = 'Create Transaction' AND rightStatus='1'";
	$rightsData = selectFrom($queryRights);
}
$rightValues = explode("|",$rightsData["rightValue"]);
$rightValue1 =$rightValues[0] ; // lower time limit.
$rightValue2 =$rightValues[1] ; // upper time limit.
	if($rightsData !="" && (time() < strtotime($rightValue1) || time() > strtotime($rightValue2)) && $rightValue1!="" && $rightValue2!=""){
		insertError("You are only allowed to Create Transaction between ".date("g A",strtotime($rightValue1))."-".date("g A",strtotime($rightValue2))."");
		$msgTime = "Y";
	}
}
include ("javaScript.php");
 $parentID = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$flagBenCountry = 0;

$fromTotalConfig = CONFIG_CALCULATE_FROM_TOTAL;

if(CONFIG_CUSTOM_SENDER == '1')
{
	$customerPage = CONFIG_SENDER_PAGE;
}else{
	$customerPage = "add-customer.php";
	
	}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}



if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}
	
function currencyValue()
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].","))
			$compareCurrency = true;	
	}elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].","))
			$compareCurrency = true;	
	}else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}	

function RoundVal($val){
	
	if(CONFIG_TRANS_ROUND_LEVEL >= 0)
	{
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	}else{
		$roundLevel = 4;
		}
		
		
	$compareCurrency2 = currencyValue();
	
    if((strstr(CONFIG_ROUND_NUM_FOR, $_POST["calculateBy"].",")) && $compareCurrency2){
        if(CONFIG_TRANS_ROUND_NUMBER == 1){
      	$ArrVal = explode(".",$val);
       	$return=$ArrVal[0];
    }else{
    	
        $return=round($val,$roundLevel);
        }
    }else{
    	
      	$return=round($val,$roundLevel);        
    }
return $return;	
}



//$backUrl = "add-transaction_opaldesign.php;

if ($_GET["back"])
{
	$_SESSION["back"] = $_GET["back"];
}

if ($_GET["val"] != "")
	$_SESSION["amount_transactions"] = $_GET["val"];
if(CONFIG_SKIP_CONFIRM == '1')
{
	$confirmURL = "add-transaction-conf.php";
	}elseif(CONFIG_CUSTOM_RECIEPT == "1")
{
	$confirmURL = CONFIG_RECIEPT_NAME;
}else{
	$confirmURL = "confirm-transaction.php";
}

if($agentType == "TELLER")
{
	
	$userDetails = selectFrom("select * from " . TBL_COLLECTION . " as c, ". TBL_TELLER. " as t  where t.loginName='$username' and t.collection_point = c.cp_id");
	
	$_SESSION["senderAgentID"]  = $senderAgentID = $userDetails["cp_ida_id"];
	
}elseif($agentType == "PAYING BOOK CUSTOMER"){
	
	$userDetails = selectFrom("select * from " . TBL_CUSTOMER . "   where accountName='$username' ");
	
 $_SESSION["senderAgentID"] = $senderAgentID = $userDetails["agentID"];
$_SESSION["customerID"]= $senderID = $userDetails["customerID"];
	
	}
	
	else{
	$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");
	//echo "NO TELLER $agentType";
}

if($_GET["act"]!="") {
	$_SESSION["act"] = $_GET["act"];
}

if($_POST["calculateBy"] != ""){
	$_SESSION["calculateBy"] =  $_POST["calculateBy"];	
}


if($_POST["submitSearch"] == "Search")
{
	$_SESSION["searchTrans"] = $_POST["searchTrans"];
}
//echo($_SESSION["transType"]);

if($_GET['benID'] != ""){
		if($_SESSION["transType"] == "")
		{
			$_SESSION["transType"] = "Pick up";
		}
	}
	

//echo("at line 39 action is ".$_SESSION["act"]." transID  ".$_GET["transID"]."  and transType ".$_SESSION["transType"]);
if($_GET['benID'] != "")
{
		if($_SESSION["transType"] == "Bank Transfer")
		{
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";		
			$_SESSION["ibanRemarks"] 			= "";	
			$_SESSION["accountType"] = "";
		}
		$_SESSION["collectionPointID"] = '';
		$queryBen = "select Country  from ".TBL_BENEFICIARY." where benID ='$_GET[benID]'";
		//echo $queryBen;
		$benificiaryContent = selectFrom($queryBen);	
		$contentExchagneRate = selectFrom("select *  from exchangerate where country = '".$benificiaryContent["Country"]."'");								
		//$_SESSION["currencyTo"] = $contentExchagneRate["currency"];		
		if($_POST["currencyTo"] != "")
			$_SESSION["currencyTo"] = $_POST["currencyTo"];
		if($_POST["currencyFrom"] != "")
			$_SESSION["currencyFrom"] = $_POST["currencyFrom"];
			
}



if($_GET['benAgentID'] != '')
{
	$_SESSION["benAgentID"] = $_GET['benAgentID'];	
}	

if ($_GET['collectionPointID'] != '') {	
	$_SESSION["collectionPointID"] = $_GET['collectionPointID'];
} else if ($_POST['collPoints'] != '' && CONFIG_COLLPOINTS_DROPDOWN == '1') {	// [by JAMSHED]
	$_SESSION["collectionPointID"] = $_POST['collPoints'];
}

	
/*
foreach ($_SESSION as $k=>$v) 
{
	$str .= "$k : $v<br>";
}
echo $str;
*/
//echo "agent type: " . $username . $agentType;

if($_GET["search"] == "end")
{
	$_SESSION["searchTrans"] = "";	
}

if($_GET["create"] != "")
{
			$_SESSION["transType"]		= "";
			if($agentType != "PAYING BOOK CUSTOMER"){
		$_SESSION["customerID"] 	= "";}//customerid remains there if the user is paying book customer
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"]		 = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
			$_SESSION["currencyTo"]		= "";
			$_SESSION["currencyFrom"]		= "";
			$_SESSION["discount"]       = "";
			$_SESSION["chDiscount"] 	= "";
			$_SESSION["discount_request"] = "";
			
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			$_SESSION["ibanRemarks"] = "";
			$_SESSION["accountType"] = "";
			$_SESSION['RequestforBD'] = "";
			
			$_SESSION["senderAgentID"]  = "";
			$_SESSION["collectionPointID"] = "";
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"] = "";	
			
			$_SESSION["currencyCharge"] = "";
			
			//$_SESSION["admincharges"] = "";
			$_SESSION["bankCharges"] = "";
			$_SESSION["distribut"] ="";
			$_SESSION["transDate"] = "";
			$_SESSION["amount_transactions"] = "";
			$_SESSION["amount_left"] = "";
			$_SESSION["batch"] = "";
			$_SESSION["searchTrans"] = "";
			
			$_SESSION["benAgentParentID"] = "";
			$_SESSION["custAgentParentID"] ="";
			$_SESSION["custName"] = "";
			$_SESSION["customerSearch"] = "";
			$_SESSION["custCountry"] = "";
			$_SESSION["benCountry"] = "";
			$_SESSION["custAgentID"] = "";
			$_SESSION["notService"] = "";
			$_SESSION["dDate"] ="";
			$_SESSION["Declaration"] = "";
			$_SESSION["transID"] = "";
			$_SESSION["transSend"] = "";
			$_SESSION["act"] = "";
			$_SESSION["Submit"] = "";
			$_SESSION["mydDate"] = "";
			$_SESSION["imReferenceNumber"] = "";
			$_SESSION["document_remarks"] = "";
			$_SESSION["addressDescription"]="";
			$_SESSION["internalRemarks"] = "";
			$_SESSION["benIdPassword"] = "";
			

}


if (CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && strstr(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",")) {
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID = '".$parentID."'");
	
	$linkedAgent = explode(",", $adminLinkDetails["linked_Agent"]);
	$linkedAgentStr = implode("','", $linkedAgent);
	$userIDs = selectMultiRecords("select userID,username,name from ".TBL_ADMIN_USERS." where username in ('".$linkedAgentStr."')");
	if (count($userIDs) == 1) {
		for ($i = 0; $i < count($userIDs); $i++) {
			$_SESSION["senderAgentID"] = $userIDs[$i]["userID"];
		}	
	}
}

if($_POST["moneyPaid"] != ""){
$_SESSION["moneyPaid"] = $_POST["moneyPaid"];
}
if (CONFIG_DEFAULT_PAYMENT_MODE == '1' || $_SESSION["moneyPaid"]== "") {
if (CONFIG_DEFAULT_PAYMENT_MODE == '1' && $_SESSION["moneyPaid"]== "") {	

	$defMoneyPaid = selectFrom("SELECT defaultMoneyPaid FROM " . TBL_ADMIN_USERS . " WHERE `userID` = '" . $parentID . "'");
	
		$_SESSION["moneyPaid"] = $defMoneyPaid["defaultMoneyPaid"];
}

if($defMoneyPaid["defaultMoneyPaid"] == "By Bank Transfer")
{
	$fromTotalConfig = '1';
	$_SESSION["calculateBy"] = 'inclusive';
	$_SESSION["moneyPaid"] = "By Bank Transfer";
	
	
	}elseif($defMoneyPaid["defaultMoneyPaid"] == "By Cash")
{ 

	$fromTotalConfig = '0';
	$_SESSION["calculateBy"] = 'exclusive';
		$_SESSION["moneyPaid"] = "By Cash";
	
	
	}
	if($_SESSION["moneyPaid"] == "") {
	//	if(CONFIG_MONEY_PAID_NON_MANDATORY == "1"){
			
		//	$_SESSION["moneyPaid"] = '';	
			
		//	}else{
		$_SESSION["moneyPaid"] = 'By Cash';	
	//}
	}
}


if($_GET["transID"] != "" && trim($_POST["transType"])== "" && $_GET['collectionPointID'] == "" && $_GET['msg'] != 'Y')
{

//	exit();
	$contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
	$_SESSION["transType"] 			= $contentTrans["transType"];
	$_SESSION["benAgentID"] 		= $contentTrans["benAgentID"];
	$_SESSION["distribut"] = $_SESSION["benAgentID"];
$dist = $_SESSION["distribut"];
	
	$_SESSION["senderAgentID"] 		= $contentTrans["custAgentID"];
	$_SESSION["customerID"] 		= $contentTrans["customerID"];
	$_SESSION["benID"] 				= $contentTrans["benID"];
	$_SESSION["moneyPaid"] 			= $contentTrans["moneyPaid"];

	
	if(CONFIG_CALCULATE_BY == '1' && $_SESSION["moneyPaid"] != "By Cash")
{ 

 				
				$fromTotalConfig = '1';
				$_SESSION["calculateBy"] = "inclusive";
	
}
elseif(CONFIG_CALCULATE_BY == '1' && $_SESSION["moneyPaid"] == "By Cash")
{
				
				$fromTotalConfig = '0';
				$_SESSION["calculateBy"] = 'exclusive';
		
}
	
	
	
	
	$_SESSION["transactionPurpose"] = $contentTrans["transactionPurpose"];
	
	$_SESSION["other_pur"] 			= $contentTrans["other_pur"];
	$_SESSION["fundSources"] 		= $contentTrans["fundSources"];
	$_SESSION["refNumber"] 			= $contentTrans["refNumber"];
	
	$_SESSION["transAmount"]		= $contentTrans["transAmount"];
	$_SESSION["totalAmount"]		= $contentTrans["totalAmount"];
	$_SESSION["localAmount"]		= $contentTrans["localAmount"];
	$_SESSION["question"] 			= $contentTrans["question"];
 	$_SESSION["answer"]     = $contentTrans["answer"];	
 	$_SESSION["tip"]     = $contentTrans["tip"];
 	$_SESSION["transDate"] = $contentTrans["creation_date_used"];
	//$_SESSION["admincharges"] 		= $contentTrans["admincharges"];
	$_SESSION["bankCharges"] 		= $contentTrans["bankCharges"];	
	$_SESSION["currencyTo"] = $contentTrans["currencyTo"];	
	$_SESSION["currencyFrom"] = $contentTrans["currencyFrom"];	
	$_SESSION["internalRemarks"] = $contentTrans["internalRemarks"];	
	
	
	
	$totalAmount = $contentTrans["totalAmount"];
	$transAmount = $contentTrans["transAmount"];
	$localAmount = $contentTrans["localAmount"];

	
	if(CONFIG_BACK_DATED == '1' && $contentTrans["creation_date_used"]!="0000-00-00")
	{
	$dbDate = explode("-",$_SESSION["transDate"]);
					if(count($dbDate) == 3)
					{
						$dbDate = $dbDate[2]."/".$dbDate[1]."/".$dbDate[0];
						$_SESSION["transDate"] = $dbDate;
					}
	
}else{
	$_SESSION["transDate"] = "";
	}
	
	
	if(CONFIG_CASH_PAID_CHARGES_ENABLED)
	{
		if($_SESSION["moneyPaid"]=='By Cash')
		{
			$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
			}
	}

	//$_SESSION["act"] = $_GET["act"];	
	// Session vars for trans_type Bank Transfer
	$act = $_SESSION["act"];
	
	if($contentTrans["transType"] == "Bank Transfer")
	{
		$contentBank = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID='".$contentTrans["transID"]."'");
		$_SESSION["bankName"] 		= $contentBank["bankName"];
		$_SESSION["branchCode"] 	= $contentBank["branchCode"];
		$_SESSION["branchAddress"] 	= $contentBank["branchAddress"];
		$_SESSION["swiftCode"] 		= $contentBank["swiftCode"];
		$_SESSION["accNo"] 			= $contentBank["accNo"];
		$_SESSION["ABACPF"] 		= $contentBank["ABACPF"];
		$_SESSION["IBAN"] 			= $contentBank["IBAN"];
		$_SESSION["ibanRemarks"] 			= $contentBank["Remarks"];
		$_SESSION["accountType"] = $contentBank["accountType"];	
	}

	if($_GET["collectionPointID"] == ""){
		$_SESSION["collectionPointID"] = $contentTrans["collectionPointID"];
		//echo $contentTrans["transType"]. "select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["agentID"]."'";	
		$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["benAgentID"]."'");
		//if ($_POST["Country"] != "")
			$_SESSION["Country"] = $agentContent["agentCountry"];
		//if ($_POST["City"] != "")
			$_SESSION["City"] = $agentContent["agentCity"];
	}
	
}
else
{
//	$_SESSION["transType"] = (is_array($_POST) && $_GET["msg"] == "" ? $_POST["transType"] : $_SESSION["transType"]);

if($_POST["transType"] != "")
{
	if($_SESSION["transType"] !=  $_POST["transType"])
	{
		$_SESSION["transType"] = (is_array($_POST) && $_GET["msg"] == "" ? $_POST["transType"] : $_SESSION["transType"]);
	}
}


	$_SESSION["benAgentID"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["benAgentID"]);
	$_SESSION["distribut"] = $_SESSION["benAgentID"];
$dist = $_SESSION["distribut"];
	
	$_SESSION["customerID"] = ($_POST["customerID"] != "" || $_GET["customerID"] ? ($_GET["customerID"] != "" ? $_GET["customerID"] : $_POST["customerID"]) : $_SESSION["customerID"] );
	//$_SESSION["benID"] = ($_POST["benID"] != "" ? $_POST["benID"] : $_SESSION["benID"]);

	$_SESSION["benID"] = ($_POST["benID"] != "" || $_GET["benID"] ? ($_GET["benID"] != "" ? $_GET["benID"] : $_POST["benID"]) : $_SESSION["benID"] );

	//$_SESSION["admincharges"] = ($_POST["admincharges"] != "" ? $_POST["admincharges"] : $_SESSION["admincharges"]);
	// Session vars for trans_type Bank Transfer
	$_SESSION["bankName"] 		= ($_POST["bankName"] != "" ? $_POST["bankName"] : $_SESSION["bankName"]);
	$_SESSION["branchCode"] 	= ($_POST["branchCode"] != "" ? $_POST["branchCode"] : $_SESSION["branchCode"]);
	$_SESSION["branchAddress"] 	= ($_POST["branchAddress"] != "" ? $_POST["branchAddress"] : $_SESSION["branchAddress"]);
	$_SESSION["swiftCode"] 		= ($_POST["swiftCode"] != "" ? $_POST["swiftCode"] : $_SESSION["swiftCode"]);
	$_SESSION["accNo"] 			= ($_POST["accNo"] != "" ? $_POST["accNo"] : $_SESSION["accNo"]);
	$_SESSION["ABACPF"] 		= ($_POST["ABACPF"] != "" ? $_POST["ABACPF"] : $_SESSION["ABACPF"]);
	$_SESSION["IBAN"] 			= ($_POST["IBAN"] != "" ? $_POST["IBAN"] : $_SESSION["IBAN"]);
	$_SESSION["ibanRemarks"] 			= ($_POST["ibanRemarks"] != "" ? $_POST["ibanRemarks"] : $_SESSION["ibanRemarks"]);
	$_SESSION["bankCharges"] = ($_POST["bankCharges"] != "" ? $_POST["bankCharges"] : $_SESSION["bankCharges"]);
	$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);
	$_SESSION["currencyTo"] = ($_POST["currencyTo"] != "" ? $_POST["currencyTo"] : $_SESSION["currencyTo"]);
	$_SESSION["currencyFrom"] = ($_POST["currencyFrom"] != "" ? $_POST["currencyFrom"] : $_SESSION["currencyFrom"]);
	$_SESSION["accountType"] = ($_POST["accountType"] != "" ? $_POST["accountType"] : $_SESSION["accountType"]);
	//$_SESSION["document_remarks"] = ($_POST["document_remarks"] != "" ? $_POST["document_remarks"] : $_SESSION["document_remarks"]);
	$_SESSION["internalRemarks"] = ($_POST["internalRemarks"] != "" ? $_POST["internalRemarks"] : $_SESSION["internalRemarks"]);
	$_SESSION["benIdPassword"] = ($_POST["benIdPassword"] != "" ? $_POST["benIdPassword"] : $_SESSION["benIdPassword"]);
	//echo(" Got at line 305 ".$_SESSION["currencyTo"]);
	//echo("at line 305 transID  ".$_GET["transID"]."  and transType ".$_SESSION["transType"]);
	$_SESSION["transAmount"] = ($_POST["transAmount"] != "" ? $_POST["transAmount"] : $_SESSION["transAmount"]);
	$_SESSION["localAmount"] = ($_POST["localAmount"] != "" ? $_POST["localAmount"] : $_SESSION["localAmount"]);
	$_SESSION["totalAmount"] = ($_POST["totalAmount"] != "" ? $_POST["totalAmount"] : $_SESSION["totalAmount"]);
	
	if(CONFIG_BACK_DATED == '1')
	{
		if ($_POST["manageTransDate"] != "") {
			$_SESSION["transDate"] = $_POST["dDate"];
		} else {
	 		$_SESSION["transDate"] = ($_POST["dDate"] != "" ? $_POST["dDate"] : $_SESSION["transDate"]);
	 	}
	}else{
		$_SESSION["transDate"] = "";
		}
	if ($_POST["Country"] != "")
		$_SESSION["Country"] = $_POST["Country"];
	if ($_POST["City"] != "")
		$_SESSION["City"] = $_POST["City"];
	if($_POST["discount"] != "")
		$_SESSION["discount"] = $_POST["discount"];
	if($_POST["chDiscount"] != "")
		$_SESSION["chDiscount"] = $_POST["chDiscount"];
	if ($_POST["t_amount"] != "")
		$_SESSION["amount_transactions"] = $_POST["t_amount"];
	if ($_POST["l_amount"] != "")
		$_SESSION["amount_left"] = $_POST["l_amount"];
	if ($_GET["batch"] != "")
		$_SESSION["batch"] = $_GET["batch"];
	

	if(CONFIG_CASH_PAID_CHARGES_ENABLED)
	{
		if($_SESSION["moneyPaid"]=='By Cash')
		{
			$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
			}
	}
	
	
	//if($_POST["discount_request"] != "")
	if($_GET["from"] != 'conf')
	{
		 	$_SESSION["discount_request"] = $_POST["discount_request"];	
		
	
	
			$_SESSION["question"] = $_POST["question"];
			$_SESSION["answer"] = $_POST["answer"];
			$_SESSION["tip"] = $_POST["tip"];	
			if($_POST["moneyPaid"] != ""){		
				$_SESSION["moneyPaid"] = $_POST["moneyPaid"];
			}
			$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
			$_SESSION["other_pur"] = $_POST["other_pur"];
			$_SESSION["fundSources"] = $_POST["fundSources"];
			$_SESSION["refNumber"] = $_POST["refNumber"];
	}
	
}


if($_GET["from"] != 'conf'  && ($_GET["transID"] == "" || $_POST["Amount"] != '' || $_POST["localAmount"] != '' ||$_POST["fromTotal"] != ''))
{
	
		//$_SESSION["transAmount"] = $_POST["transAmount"];
		$transAmount = (float) $_SESSION["transAmount"];
	
		//$_SESSION["localAmount"] = $_POST["localAmount"];
		$localAmount = (float) $_SESSION["localAmount"];
		
		if(CONFIG_ROUND_NUMBER_ENABLED == '1')
		{
			$localAmount = round($localAmount);		
		}
		
	
	if($fromTotalConfig == '1')
	{	
		
			//$_SESSION["totalAmount"] = $_POST["totalAmount"];
			$totalAmount = (float) $_SESSION["totalAmount"];
		
	}
}



$_SESSION["distribut"] = ($_POST["distribut"] != "" ? $_POST["distribut"] : $_SESSION["distribut"]);

if($_POST["refNumber"] != "")
{	
	$refNumber = str_replace(" ","",$_POST["refNumber"]);
	$_SESSION["refNumber"] = $refNumber;
}
/*if ($_SESSION["transType"] != ""){
	$queryCust ="select customerID, Title, firstName, lastName, middleName from ".TBL_CUSTOMER . " order by firstName";
	$customers = selectMultiRecords($queryCust);
	//echo $queryCust . "<br>";
	if($_SESSION["customerID"] != "")
	{
		$queryBen  ="select * from ".TBL_BENEFICIARY." where customerID ='$_SESSION[customerID]' order by firstName";
		$beneficiaries = selectMultiRecords($queryBen);
		//echo $queryBen . "<br>";
	}
}*/
if($_POST["transID"] != "" && $_POST["transType"]=="Pick up" && $_POST["Country"] == "")
{
	$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentTrans["benAgentID"]."'");
	$_SESSION["Country"] = $agentContent["agentCountry"];
	$_SESSION["City"] = $agentContent["agentCity"];
}



if($_GET["aID"] != "")
{ 
	$_SESSION["senderAgentID"] = $_GET["aID"];
	
}elseif($_GET["aID"] == "")
	{ 
		if(CONFIG_ENABLE_DEFAULT_AGENT == "1")
		{
		if($_SESSION["senderAgentID"]=="")
				$_SESSION["senderAgentID"] ='3';
			
		}
		
			if($_POST["aID"] != ""){
				
						$_SESSION["senderAgentID"] = $_POST["aID"];
	}else{
		
		if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
		{
			$_SESSION["senderAgentID"] = $_SESSION["loggedUserData"]["userID"];
		}if($agentType == "TELLER")
		{
			$_SESSION["senderAgentID"] = $userDetails["cp_ida_id"];
	
		}
	}
	}


if($_GET["benAgentID"] != "")
{
	$_SESSION["benAgentID"] = $_GET["benAgentID"];
	$_SESSION["transType"] = "Pick up";
}

if($_GET["customerID"]!="")
	$_SESSION["customerID"] = $_GET["customerID"];


/******************
* This variable is used to show the customer and beneficiary 
* last transaction details (if occured) when beneficiary is selected
******************/
if (CONFIG_CUSTBEN_OLD_TRANS == '1' || CONFIG_INTERNAL_REMARKS == '1') {
	if($_GET["benID"] != "")
	{
		$oldContent = selectFrom("select benAgentID,transID, collectionPointID, transType, internalRemarks from " . TBL_TRANSACTIONS." where transID = (select  Max(transID) from " . TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and benID = '".$_GET["benID"]."' and  toCountry = '".$benificiaryContent["Country"]."')");
	}
	
}
/*if(CONFIG_INTERNAL_REMARKS == '1')
	{
		if($_SESSION["internalRemarks"] == "")
		{ 
			$_SESSION["internalRemarks"] = $oldContent["internalRemarks"];
		} echo "line 598".$_SESSION["internalRemarks"];
	}*/

	
	
	
if (CONFIG_CUSTBEN_OLD_TRANS == '1') {
	if ($_GET["benID"] != "") {
		
		if ($oldContent["transType"] != "" && $_POST["transType"]== "") {
			$_SESSION["transType"] = $oldContent["transType"];
	  	$_SESSION["distribut"] = $oldContent["benAgentID"];
	  	$dist = $_SESSION["distribut"];
	  	$_SESSION["benAgentID"] = $_SESSION["distribut"];
		} elseif ($_POST["transType"]!= "") {
				$_SESSION["transType"] = $_POST["transType"];
	  	
		}
	
		if ($_SESSION["transType"] == "Bank Transfer") {
			if ($oldContent["transType"] != "") {
				$contentBank5 = selectFrom("select * from " . TBL_BANK_DETAILS . " where transID = '".$oldContent["transID"]."'");
			} else {		 
				$contentBank5 = selectFrom("select * from " . TBL_BANK_DETAILS . " where benID='".$_GET["benID"]."'");
			}
			$_SESSION["bankName"] 		 = $contentBank5["bankName"];
			$_SESSION["branchCode"] 	 = $contentBank5["branchCode"];
			$_SESSION["branchAddress"] = $contentBank5["branchAddress"];
			$_SESSION["swiftCode"] 		 = $contentBank5["swiftCode"];
			$_SESSION["accNo"] 			   = $contentBank5["accNo"];
			$_SESSION["ABACPF"] 		   = $contentBank5["ABACPF"];
			$_SESSION["IBAN"] 			   = $contentBank5["IBAN"];
			$_SESSION["ibanRemarks"] 	 = $contentBank5["Remarks"];
			$_SESSION["accountType"]   = $contentBank5["accountType"];
		} elseif ($_SESSION["transType"] == "Pick up" && $oldContent["transType"] == $_SESSION["transType"]) {
			if ($oldContent["collectionPointID"] != "" && $_GET['collectionPointID'] == "") {
				$_SESSION["transType"] = $oldContent["transType"];
				$_SESSION["collectionPointID"] = $oldContent["collectionPointID"];
				$_SESSION["distribut"] = $oldContent["benAgentID"];
				$dist = $_SESSION["distribut"];
	 			$_SESSION["benAgentID"] = $_SESSION["distribut"];
				
			}
			
		}
	}
}





if($_SESSION["moneyPaid"] == 'By Cash')	
	$_SESSION["moneyPaidCharges"]=CONFIG_CASH_PAID_CHARGES;
	
if (CONFIG_DEFAULT_TRANSACTION_PURPOSE == "1") {
	// Do nothing
	if($_SESSION["transactionPurpose"] == "") {
		$_SESSION["transactionPurpose"] = 'Family Assistant';
	}
}
if(CONFIG_NONCOMPUL_FUNDSOURCES != '1' && $_SESSION["fundSources"]==""){
	$_SESSION["fundSources"]='Salary';
}

if(CONFIG_CALCULATE_BY == '1' && ($_POST["calculateBy"] == 'inclusive' || $_GET["calculateBy"] == 'inclusive' || ($_POST["calculateBy"]== '' && $_SESSION["calculateBy"]== '')))
{ 
				$fromTotalConfig = '1';
				$_SESSION["moneyPaid"] = "By Bank Transfer";
				$_SESSION["calculateBy"]== 'inclusive';
	
}
elseif(CONFIG_CALCULATE_BY == '1' && $_POST["calculateBy"]== 'exclusive' || $_GET["calculateBy"] == 'exclusive')
{
				$fromTotalConfig = '0';
				$_SESSION["moneyPaid"] = "By Cash";
				$_SESSION["calculateBy"]== 'exclusive';
		
}
// Added by Niaz Ahmad against #1727 at 08-01-2008

if(CONFIG_COMPLIANCE_PROMPT == "1"){
$currentRuleQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Create Transaction')";
$currentRule = selectFrom($currentRuleQuery);

$complianceQuery = " select ruleID,applyAt,matchCriteria,message,applyTrans,amount,cumulativeFromDate,cumulativeToDate from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Create Transaction')";
$cumulativeRule = selectFrom($complianceQuery);
}

if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");
         
            		  $to = getCountryTime(CONFIG_COUNTRY_CODE);
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
								  $noOfDays = CONFIG_NO_OF_DAYS;
								  $fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));
									$from = $fromDate." 00:00:00"; 
									
									// These dates set from configuration page
									if(CONFIG_COMPLIANCE_NUM_OF_DAYS == "1"){
									
										if($cumulativeRule["cumulativeFromDate"]!='' && $cumulativeRule["cumulativeToDate"]!=''){
  			        	    $from	= $cumulativeRule["cumulativeFromDate"]." 00:00:00"; 
  		                $to	= $cumulativeRule["cumulativeToDate"]." 23:59:59"; 
  	                } 
  	              }
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								  //echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_SESSION["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
                 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}

//echo "<br>--Cumulative Amount--".$senderAccumulativeAmount;



?>
<html>
<head>
<title>Create Transaction</title>
<script language="JavaScript" src="./javascript/GCappearance2.js"></script>
<script language="JavaScript" src="./javascript/GurtCalendar.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
	<script src="jquery.js" type="text/javascript"></script>
	<script type="text/javascript" src="jquery.form.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 
    var searchagent = { 
        target:        '#search_result_agents'
         };   
     $('#frmSearch').ajaxForm(searchagent); 
     
  });
	function showDetails(elementid,str) {
		if (str == 'show') {
			document.getElementById(elementid).style.display = 'table';
		} else {
			document.getElementById(elementid).style.display = 'none';
		}
	}

function ShowRows(elementid,str) {
	if (str == 'show') {
			document.getElementById(elementid).style.display = 'table-row';
		} else {
			document.getElementById(elementid).style.display = 'none';
		}
	}

	</script>


<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="./styles/calendar2.css">
<script language="javascript" src="./styles/admin.js"></script>

<script language="javascript">
	
	////////
/*	function getHTTPObject() {
var xmlhttp;

if(@_jscript_version >= 5)
try {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
alert("Your browser is IE-0"); // Triggers OK in IE 7 - kmk
} catch (e) {
try {
xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
alert("Your browser is IE-1");
} catch (E) {
xmlhttp = false;
}
}else
	xmlhttp = false;

if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
try {
xmlhttp = new XMLHttpRequest();
alert("Your browser is Firefox Based");
} catch (e) {
xmlhttp = false;
}
}
return xmlhttp;
}
	
	*/
	
	////////
	function getHTTPObject() { 
	var xmlhttp; 
	if (typeof XMLHttpRequest != 'undefined') { 
		try { 
			xmlhttp = new XMLHttpRequest(); 
			} catch (e) { 
				xmlhttp = false; 
				} 
		} 
		return xmlhttp; 
	}
 
var http = getHTTPObject(); // We create the HTTP Object 
var url1 = "getAmounts.php"; 

function handleHttpResponse() {

  if (http.readyState == 4) {



    results = http.responseText.split(",");

    

   document.getElementById('transAmountID').value = results[0];
    document.getElementById('localAmountID').value = results[1];
    document.getElementById('totalAmountID').value = results[2];
    document.getElementById('IMFee').value = results[3];
    document.getElementById('exchangeRate').value = results[4];
    document.getElementById('currencyChargeID').value = results[5];
    document.getElementById('exchangeID').value = results[6];
   

	}

}

function disableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=true;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=true;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=true;	
		<?
	}
		?>
}
function enableClicks()
{
	<?
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
	?>
		document.getElementById('transSend').disabled=false;
		<?
	}
	if(CONFIG_SEND_ONLY_BUTTON == '1')
	{
		?>
		document.getElementById('sendToConfirmID').disabled=false;
		<?
	}else{
		?>
		document.getElementById('sendID').disabled=false;	
		<?
	}
		?>
}
function updateAmounts(param) {

 var amountFlag = param;	
 var amount = document.getElementById("transAmountID").value;
 var amountLocal = document.getElementById("localAmountID").value;
 var benCountry =  document.getElementById("benCountryID").value;
 var custCountry =  document.getElementById("custCountryID").value;
 var dist =  document.getElementById("distribut").value;
 var exRate =  document.getElementById("exchangeRate").value;
 
 var fee =  document.getElementById("IMFee").value;
 var transType = document.getElementById("transType").value;
 var currencyFrom = document.getElementById("currencyFrom").value;
 var currencyTo = document.getElementById("currencyTo").value;
 var inclusive = 'No';
<?
if(CONFIG_CALCULATE_BY == '1')
{?>
	
	if(document.getElementById("calculateByID").value == 'inclusive')
	{
		var moneyPaid = "Bank Transfer";
		inclusive = 'Yes';
	}else{
		var moneyPaid = "By Cash";
		inclusive = 'No';
		}
<?
}else{
	?>
	 var moneyPaid = document.getElementById("moneyPaid").value;
	 inclusive = 'No';
	<?
	}
?> 

 var customerAgentID = document.getElementById("customerAgentID").value;
 var bankChargesID = document.getElementById("bankChargesID").value;
 

 
 var amountValue = '?amount='+amount+'&benCountry='+benCountry+'&custCountry='+custCountry+'&dist='+dist+'&exRate='+exRate+'&fee='+fee+'&transType='+transType+'&currencyFrom='+currencyFrom+'&currencyTo='+currencyTo+'&amountFlag='+amountFlag+'&amountLocal='+amountLocal+'&moneyPaid='+moneyPaid+'&customerAgentID='+customerAgentID+'&bankChargesID='+bankChargesID+'&inclusive='+inclusive;

  http.open("GET", url1 + amountValue, true);
  


 enableClicks();
  http.onreadystatechange = handleHttpResponse;
	
  http.send(null);
}

	



function SelectOption(OptionListName, ListVal)
{ 
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkSearch()
{
	if(document.frmSearchTrans.searchTrans.value == "")
	{
		alert("Please provide Reference No/<?=$manualCode?> in textbox to search transaction.");
		frmSearchTrans.searchTrans.focus();
		return false;
	}
	return true;	
}

function checkForm(theForm,strval){

	var transSender = '';

	if(strval == 'Yes')
	{
		transSender="transSend";
	}
	else
	{
		transSender="NoSend";
	}

	 var ruleConfirmFlag=true;
	 var condition;
	 var minAmount = 0;

	/// Compliance /////////////////////
<?	
if($currentRule["ruleID"]!='') {

	if($currentRule["matchCriteria"] == "BETWEEN"){

		  $betweenAmount= explode("-",$currentRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
		 
		 ?>
		condition=<?=$fromAmount?> <= parseInt(document.addTrans.transAmount.value) && parseInt(document.addTrans.transAmount.value) <= <?=$toAmount?>;
  <?
  }else{
 	?>
 		   	 minAmount=<?=$currentRule["amount"]?>;
 		   	 condition =parseInt(document.addTrans.transAmount.value)<?=$currentRule["matchCriteria"]?> minAmount;
 <?	}?>	

	
		if(condition)
	   {
      	if(confirm("<?=$currentRule["message"]?>"))
    	{
    		ruleConfirmFlag=true;
    	
                
      }else{
      			<? 
    	if(CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE == '1'){
    		  	?>
    
   					window.open("add-customer.php?transID=<? echo $_GET["transID"]?>&from=popUp&focusID=transSend&customerID=<? echo $_SESSION["customerID"]?>&agentID=<? echo $_SESSION["senderAgentID"]?>&transSend='+transSender","[_self]","scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740");
    <? } ?>
      	addTrans.localAmount.focus();
        
         ruleConfirmFlag=false;
      }

    }else{
    	
    	
    	 ruleConfirmFlag=true;
    	
    } 
   <?
    
      }
   ?> 
   
     

   // cummulative amount/////////

  <? if($cumulativeRule["ruleID"]!=''){ ?> 
	var conditionCumulative;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	<?
	if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		
		
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
	?>	
    conditionCumulative=<?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
 <?
  }else{
 	?>
 		    amountToCompare = <?=$cumulativeRule["amount"]?>;
 		    conditionCumulative=accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;	
 <?	}?>		  
     
		
		 if(conditionCumulative)
	   {

      	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    		ruleConfirmFlag=true;
    	
                
      }else{
      			<? 
    	if(CONFIG_COMPLIANCE_TARGET_CUSTOMER_PAGE == '1'){
    		  	?>
    
   					window.open("add-customer.php?transID=<? echo $_GET["transID"]?>&from=popUp&focusID=transSend&customerID=<? echo $_SESSION["customerID"]?>&agentID=<? echo $_SESSION["senderAgentID"]?>&transSend='+transSender","[_self]","scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740");
    <? } ?>
      	 addTrans.localAmount.focus();
      	
      	 ruleConfirmFlag=false;
      
      }

    }else{
    	 
    	 ruleConfirmFlag=true;
    	
    } 
   
   <?
       
    } 
   ?>

if(ruleConfirmFlag){
   		<? 
    		if(CONFIG_SKIP_CONFIRM == '1')
    		{ 
    		?>
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		<?
    		}else{
    		
    			if(CONFIG_CUSTOM_RECIEPT == 1)
    			{
    			?>
    				document.addTrans.action='<? echo CONFIG_RECIEPT_NAME ?>?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    			<?
    			}else{
    			?>	
    				document.addTrans.action='confirm-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    			<?
    			}
    		}
    		?>
	
	}

	
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
   
   function resetDate(){
        document.addTrans.dDate.value = '';
        document.addTrans.manageTransDate.value= 'not null';
        
   }
   
   function shorProvinces()
{
	if(document.addTrans.chDiscount.checked)
	{
	document.getElementById("Discount").style.display = '';
	document.getElementById("Fee").style.display = 'none';
	}
	else 
	{
	document.getElementById("Discount").style.display = 'none';
	document.getElementById("Fee").style.display = '';
	
	}
	

}

function batch_trans_msg()
{
	if (document.frmAmount.amount_transactions.value == "")
	{
		alert("Please Enter total amount for Transaction");
		addTrans.transAmount.value = "";
		frmAmount.amount_transactions.focus();
		return false;	
	}
	else
	{
			var a_left = document.frmAmount.amount_left.value;
			var a_trans = document.frmAmount.amount_transactions.value;
			document.addTrans.t_amount.value = a_trans;
			if(document.frmAmount.amount_left.value == "")
			{
				if (document.frmAmount.help2.value == "")
				{
					document.frmAmount.help2.value = document.frmAmount.amount_transactions.value;
					document.frmAmount.amount_left.value = (document.frmAmount.amount_transactions.value - document.addTrans.transAmount.value);
				}
				else
				{
					document.frmAmount.amount_left.value = (document.frmAmount.help2.value - document.addTrans.transAmount.value);
				}
			}
			else
			{
				if (document.frmAmount.help2.value == "")
				{
					document.frmAmount.help2.value = document.frmAmount.amount_left.value;
					document.frmAmount.amount_left.value = (document.frmAmount.amount_left.value - document.addTrans.transAmount.value);
				}
				else
				{
					document.frmAmount.amount_left.value = (document.frmAmount.help2.value - document.addTrans.transAmount.value);
				}
			}
			document.addTrans.l_amount.value = document.frmAmount.amount_left.value;
			if (document.frmAmount.amount_left.value < 0)
			{
				alert("The amount you entered is greater than amount left");
				document.addTrans.transAmount.value = "";	
				addTrans.transAmount.focus();
				document.frmAmount.amount_left.value = a_left;
				document.addTrans.l_amount.value = a_left;
				return false;
			}
			else
			{
				return true;	
			}
	}
}

function fill()
{
	document.addTrans.t_amount.value = document.frmAmount.amount_transactions.value;
	return true; 	
}

function cleanForm()
{
	document.addTrans.transAmount.value = '';	
	document.addTrans.localAmount.value = '';	
	document.addTrans.refNumber.value = '';	
	document.addTrans.IMFee.value = '';	
	document.addTrans.exchangeRate.value = '';	
	document.addTrans.answer.value = '';	
	document.addTrans.question.value = '';	
	document.addTrans.tip.value = '';	
	document.addTrans.fundSources.value = '';	
	document.addTrans.moneyPaid.value = '';	
	document.addTrans.transactionPurpose.value = '';	
	document.addTrans.totalAmount.value = '';	
	document.addTrans.discount.value = '';	
	document.addTrans.chDiscount.value = '';	
	
	
	}



	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #FFFFFF; font-weight: bold; }
.style4 {
	color: #660000;
	font-weight: bold;
}
.style5 {color: #663333}
.style6 {color: #005b90}
-->
    </style>
</head>
<body <? if ($_GET['focusID'] != '') { ?> onLoad="document.getElementById('<?=$_GET["focusID"]?>').focus(); <? if ($_SESSION["IBAN"] != "") { ?>document.getElementById('ibanDiv').innerHTML = 'You have entered ' + <? echo strlen($_SESSION[IBAN]) ?> + ' IBAN characters';<? } ?>" <? } ?>>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#c0c0c0"><b><font color="#FFFFFF" size="2"><? echo ($_GET["transID"] != "" ? "Update" : "Create");?> 
      <? echo ($_SESSION["batch"] != "" ? "Batch " : "");?>Transactions </font></b></td>
  </tr>
  <? if ($_GET["transID"]!=""){ ?>
 		<tr>
 		<td><a class="style2" href="<?=$_SESSION["back"];?>?search=Search">Go Back</a></td>
 		</tr>
 	<? } ?>
    <tr>
      <td align="center">
	<?
	  if($msgTime!="Y"){
	?>
	  <font color="#FF0000">* Compulsory Fields </font>
	<? }?>
        <br>
        <table width="700" border="0" cellpadding="0" cellspacing="0">
          <? if ($_GET["msg"] == "Y" || $_GET["msg1"] == "Y"  || $msgTime=="Y"){ ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
		  <? 
			  if($msgTime=="Y"){
			   exit;
			  }
		  } ?>
          <tr>
            <td width="700" valign="top"><table width="700" border="0" cellpadding="0" cellspacing="0" bordercolor="#FF0000">
              <?
		  if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager" || strstr($userDetails["rights"], "Create Transaction"))
		  {
		  ?>
		  
                <tr>
                	<td>&nbsp;</td>
                </tr>
                
                <? if ($_SESSION["batch"]!= "") { ?>
 		
				 		 			<tr>
				            <td colspan="4">
				            	<fieldset>
				            	<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
				            		<form name="frmAmount" method="post" >
				              <tr>
				                <td  align="center"><font  color="#990000">Total amount for transactions</font></td>
				                <td ><font color='#990000'><input type="text" name="amount_transactions" value="<?= $_SESSION["amount_transactions"];?>" onChange="return fill();"></font></td>
				              	<td  align="center"><font color="#990000">Amount left</font></td>
				                <td ><font color='#990000'><input type="text" name="amount_left" value="<?= $_SESSION["amount_left"];?>" ReadOnly></font></td>
				                <input type="hidden" name="help2" value="<?= $_SESSION["amount_left"];?>">
				              </tr>
				            </form>
				              </table>
				            </fieldset>
				            </td>
				          </tr>
      
		          <? } ?>
		       <?
		       IF(CONFIG_DISABLE_SEARCH != '1')
		       {
		       ?>   
		          <tr>
                <td valign="top"><fieldset>
                  <legend class="style2">Search Transaction</legend>
                  <table width="650" border="0" cellpadding="2" cellspacing="0">
                    <form name="frmSearchTrans" method="post" action="add-transaction_opaldesign.php" onSubmit="return checkSearch();">
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Search Transaction </font></td>
                        <td width="500" height="20"><input name="searchTrans" type="text" value="<?=$_SESSION["searchTrans"];?>" size="15">
                        	<input type="submit" name="submitSearch" value="Search">
                        	By Reference No/<?=$manualCode?>
                        </td>
                      </tr>
                    </form>
                  </table>
                  
                  <table width="650" border="0" cellpadding="2" cellspacing="0">
                    <? 
                    if ($_SESSION["searchTrans"] != "")
                    {
	   $querySearchTrans = "select * from ". TBL_TRANSACTIONS . " where (refNumber = '".$_SESSION["searchTrans"]."' OR refNumberIM = '".$_SESSION["searchTrans"]."')";
	   $searchTransResult = selectFrom($querySearchTrans);
			if($searchTransResult["transID"] != "")
			{
		?>
                    <tr>
                      <td width="150" align="right"><font color="#005b90"><? echo $systemCode; ?></font></td>
                      <td width="150" align="right"><? echo $searchTransResult["refNumberIM"];?> </td>
                      <td width="150" align="right"><font color="#005b90"><? echo $manualCode; ?> </font></td>
                      <td width="150"><? echo $searchTransResult["refNumber"]?></td>
                    </tr>
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Date</font></td>
                      <td width="150"><? echo dateFormat($searchTransResult["transDate"], "2")?></td>
                      <td width="150" align="right"><font color="#005b90">Status</font></td>
                      <td width="150"><? echo $searchTransResult["transStatus"]?></td>
                    </tr>
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Total Amount</font></td>
                      <td width="150"><? echo $searchTransResult["totalAmount"]?></td>
                      <td width="150" align="right"><font color="#005b90">Created By</font></td>
                      <?
				  if($searchTransResult["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$searchTransResult["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);
				  }
				  else

				  {
				  $agentContent2 = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$searchTransResult["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent2["name"]);
				  }
				  ?>
                      <td width="150"><? echo $createdBy?></td>
                    </tr>
            <? 
            if($searchTransResult["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent2 = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$searchTransResult["customerID"]."'");  
				   $beneContent2 = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$searchTransResult["benID"]."'");
				   ?>
				   <tr>
                      <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                      <td width="150"><? echo ucfirst($customerContent2["FirstName"]). " " . ucfirst($customerContent2["LastName"]).""?></td>
                      <td width="150" align="right"><font color="#005b90">Beneficiary Name</font></td>
                      <td width="150"><? echo ucfirst($beneContent2["firstName"]). " " . ucfirst($beneContent2["lastName"]).""?></td>
                    </tr>
            <?
				   }
				   else
				   {
				  	$customerContent2 = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$searchTransResult["customerID"]."'");
				  	$beneContent2 = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$searchTransResult["benID"]."'");
				  	?>
				  					<tr>
                      <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                      <td width="150"><? echo ucfirst($customerContent2["firstName"]). " " . ucfirst($customerContent2["lastName"]).""?></td>
                      <td width="150" align="right"><font color="#005b90">Beneficiary Name</font></td>
                      <td width="150"><? echo ucfirst($beneContent2["firstName"]). " " . ucfirst($beneContent2["lastName"]).""?></td>
                    </tr>
            <? } ?>
                    <tr>
                      <td width="150" align="right">&nbsp;</td>
                      <td width="150">&nbsp;</td>
                      <td width="150" align="right">&nbsp;</td>
                      <td width="150" align="right"><a href="add-transaction_opaldesign.php?search=end" class="style2">End Search</a></td>
                    </tr>

                    <?
			  }else{
			  ?>
			               <tr>
                      <td width="150" align="Center">&nbsp;</td>
                     </tr>
			               <tr>
                      <td width="150" align="Center">
                      	<i><font color="#FF0000">Sorry, No Transaction Found!</font></i>
                      </td>
                     </tr>
                     <tr>
                      <td width="150" align="right"><a href="add-transaction_opaldesign.php?search=end" class="style2">End Search</a></td>
                     </tr>
            <? }
          }
             ?>
                  </table>
                </fieldset></td>
              </tr>
		     <?
		    }
		     ?>
              <tr>
                <td valign="top"><fieldset>
                  <legend class="style2">Agent</legend>
<?  

if(strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false){
                   
												$flagAgent = False;
											}else{
												$flagAgent = True;
												}


?>
                  
                  
                  
                  <? if($_SESSION["senderAgentID"] == "")
									{
									?>
									<form id="frmSearch" method="get" action="search_agent_ajax.php" name="frmSearch">
                  <table width="650" border="0" cellpadding="2" cellspacing="0">
                    
                   <? 	
                   	
												
                   if(CONFIG_ADMIN_ASSOCIATE_AGENT == "1" && $flagAgent){
                  
                  
														 ?>
                    	   <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name or Number<font color="#ff0000">*</font> </font></td>
                        <td width="500" height="20">
                        	<input type="hidden" name="agentType" value="Supper">
                        	<select name="aID" id="aID" style="font-family:verdana; font-size: 11px" onChange="document.frmSearch.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&focusID=exchangeRate'; document.frmSearch.submit();">
                        		 <option value="">- Select Agent -</option>
                              <? for($j=0;$j < count($userIDs); $j++)
                              	{
                              		if($userIDs[$j]["userID"] != '')
                              			{
                              	?>
                              <option value="<?=$userIDs[$j]["userID"]?>"><? echo $userIDs[$j]["name"]."[".$userIDs[$j]["username"]."]" ?></option>
                              <? 	
                              		}
                               }
                               ?>
                             
                            </select>
                           
							
							
                    	<? }else{ ?>
                    	
                    	
                    	
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name or Number<font color="#ff0000">*</font> </font></td>
                        <td width="500" height="20"><input name="agentName" type="text" size="15">
                            <select name="agentType" id="agentType" style="font-family:verdana; font-size: 11px">
                              <option value="Supper">Super Agent</option>
                              <option value="Sub">Sub Agent</option>
                            </select>
                            
                            <? if ($_SESSION["batch"]!= "") { ?>
                            <input type="submit" name="Submit" value="Search" />
                          <? }else{ ?>
                            <input type="submit" name="Submit" value="Search" />
                          <? }
                          		}
                           ?>
                        </td>
                      </tr>
                    		<tr>
							<td colspan="2">
								<div id="search_result_agents"></div>
							</td>
							</tr>
                  </table>
                  </form>
                  <?
								}
								else
								{          
				        if($_GET["transID"] == "" || UPDATE_TRANSACTIONS_AGENT == '1')
				        {
				        	?>
				        	<form id="frmSearch" method="get" action="search_agent_ajax.php" name="frmSearch">
				        	<table width="650" border="0" cellpadding="2" cellspacing="0">
                    
				        	<?
				        	if(CONFIG_ADMIN_ASSOCIATE_AGENT == "1" && $flagAgent){ ?>
				        	
				        	<tr> 
	 	                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name or Number<font color="#ff0000">*</font> </font></td> 
	 	                        <td width="500" height="20"> 
	 	                                <select name="aID" id="aID" style="font-family:verdana; font-size: 11px"  onChange="document.frmSearch.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&focusID=exchangeRate'; document.frmSearch.submit();"> 
	 	                                         <option value="">- Select Agent -</option> 
	 	                              <? for($j=0;$j < count($userIDs); $j++) 
	 	                                {  
	 	                                        if($userIDs[$j]["userID"] != '') 
	 	                                        {        
	 	                                        ?> 
	 	                                        <option value="<?=$userIDs[$j]["userID"]?>" <? if($_SESSION["senderAgentID"] == $userIDs[$j]["userID"]){echo("selected");} ?>><? echo $userIDs[$j]["name"]."[".$userIDs[$j]["username"]."]" ?></option> 
	 	                                        <?  
	 	                                         } 
	 	                                        }  
	 	                               
	 	                              ?> 
	 	                              
	 	                            </select> 
									</tr>
				        	
				        	<? }else{
				        	
				        ?>
                  
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name or Number<font color="#ff0000">*</font> </font></td>
                        <td width="500" height="20"><input name="agentName" type="text" size="15">
                            <select name="agentType" id="agentType" style="font-family:verdana; font-size: 11px">
                              <option value="Supper">Super Agent</option>
                              <option value="Sub">Sub Agent</option>
                            </select>
                            <? if ($_SESSION["batch"]!= "") { ?>
                            <input type="submit" name="Submit" value="Search" />
                          <? }else{ ?>
                            <input type="submit" name="Submit" value="Search" />
                          <? } ?>
                        </td>
                      </tr>
                    <tr>
							<td colspan="2">
								<div id="search_result_agents"></div>
							</td>
							</tr>
                <?
              }
              ?>
            
                  </table>  </form>
              <?
              }
                ?>   
                  
                  <table width="650" border="0" cellpadding="2" cellspacing="0">
                    <? 
		
			$queryCust = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_SESSION["senderAgentID"] . "'";
			if($agentType == "Branch Manager"){
				$queryCust .= " and parentID = '$parentID'";						
			}
			$senderAgentContent = selectFrom($queryCust);
			if($senderAgentContent["userID"] != "")
			{
		?>
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                      <td width="200"><? echo $senderAgentContent["name"];?> </td>
                      <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                      <td width="200"><? echo $senderAgentContent["agentContactPerson"]?></td>
                    </tr>
                    <tr>
                    	<td colspan="4" align="right">
                    		<a href="#" onClick="showDetails('AgentInformation','show');" class="style2">Show Details</a>
                    		&nbsp;&nbsp;&nbsp;&nbsp;
												<a href="#" onClick="showDetails('AgentInformation','hide');" class="style2">Hide Details</a>
                    	</td>
                    </tr>	
										<tr>
											<td colspan="4">                  
                    	<table id="AgentInformation" style="display:none">
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Address</font></td>
                      <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
                    </tr>
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Company</font></td>
                      <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                      <td width="100" align="right"><font color="#005b90">Country</font></td>
                      <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
                    </tr>
                    <tr>
                      <td width="150" align="right"><font color="#005b90">Phone</font></td>
                      <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                      <td width="100" align="right"><font color="#005b90">Email</font></td>
                      <td width="200"><? echo $senderAgentContent["email"]?></td>
                    </tr>
			
                    <?
			  }
			  ?>
                  </table>
                </td>
                </tr>
                </table>
                
                  <?
			}
			?>
			
                </fieldset></td>
              </tr>
              <?
		  }
			
			if(($agentType != "admin" && $agentType != "Call" && $agentType != "Admin Manager") || $_SESSION["senderAgentID"] != "")
		  	{
		  	$contentsAgentCountry = selectFrom("select agentCountry from ".TBL_ADMIN_USERS." where username='$username'");
			$agentCountry = $contentsAgentCountry["agentCountry"];
		  	?>
              
              
              
<form action="<?=$confirmURL?>?transID=<?=$_GET["transID"]?>" method="post" name="addTrans">
              	<input type="hidden" name="customerAgent" id="customerAgentID" value="<? echo $_SESSION["senderAgentID"];?>">
                <input type="hidden" name="distribut" value="<? echo $_SESSION["benAgentID"];?>">
				<input name="collectionPointID" type="hidden" id="collectionPointID" value="<? echo $_SESSION["collectionPointID"]?>">
				<? 
				
		  }
	//if ($_SESSION["transType"] != "")
	//{ ?>
                <tr>
                  <td>
<fieldset>
          <legend class="style2">Sender Details </legend>
        
          <table width="699" border="0" cellpadding="2" cellspacing="0">
          	<?
          	$flag = 0;
          	if($agentType == "SUPA" ||  $agentType == "SUPAI")
          	{
          		$flag = 1;	
          	}else{
          			if($_SESSION["senderAgentID"]!="")
      				{
      					$flag = 1;	
      				}
          		}
          	if($_GET["transID"] == "" && $flag)
          	{
          		
          	if (CONFIG_PAYIN_RESTRICT == '1') 
          	{
				$arrAgentTypes = explode(",", CONFIG_PAYIN_RESTRICT_LIST);
				if (in_array($agentType, $arrAgentTypes)) 
				{
					$queryCust .= " AND payinBook = '' ";
					$queryCnt  .= " AND payinBook = '' ";
				}
			}	
          	?>
          	
            <tr>
       	<?	if (CONFIG_SENDER_SEARCH_FORMAT == '1') {  ?>
       				<td align="left" width="180"><font color="#005b90">Sender Number</font>
            <input name="custNumber" type="text" id="custNumber" size="10"></td>
             <td align="left" width="150"> <font color="#005b90">Sender Name</font>
              <input name="custName" type="text" id="custName" size="10"></td>
              <td align="left"><font color="#005b90">Contact Number</font>
              <input name="contactNumber" type="text" id="contactNumber" size="10">
									<? if ($_SESSION["batch"]!= "") { ?>
                  <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '') { alert ('Please provide sender name or number OR both.') } else { window.open('search-cust.php?custNumber='+ document.addTrans.custNumber.value +'&val=' + document.frmAmount.amount_transactions.value + '&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
                <? }else{ 
                				if (CONFIG_SENDER_SEARCH_FORMAT == '1') {	?>
                	<input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '' && document.addTrans.contactNumber.value == '') { alert ('Please provide sender name/number OR contact number OR all three to search.') } else { window.open('search-cust.php?custNumber='+ document.addTrans.custNumber.value +'&contactNumber='+ document.addTrans.contactNumber.value +'&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>'+ '&Submit=Search', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
                					<?}else{?>
                  <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.custName.value == '' && document.addTrans.custNumber.value == '') { alert ('Please provide sender name or number OR both.') } else { window.open('search-cust.php?custNumber='+ document.addTrans.custNumber.value +'&custName=' + document.addTrans.custName.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') }">
								<? } }?>
							</td>
      <?	} else {  ?>
              <td align="right" width="100"><font color="#005b90">Search Sender<font color="#ff0000">*</font> </font></td>
              <td colspan="3"><input name="custName" type="text" id="custName" size="13">
                  <select name="customerSearch" id="customerSearch">
									  <option value="1"> Sender Name</option>
									<?
									if(!in_array($agentType, $arrAgentTypes)) {
									?>
									  <option value="2"> <? if(CONFIG_PAYIN_CUSTOMER == '1'){echo("Payin Book Number");}else{echo("Sender Number");}?></option>
									<?
									}
									?>
									<?	if (CONFIG_PAYIN_CUSTOMER == '1') {  ?>
									 <option value="4" <? echo ($customerSearch == 4 ? "selected" : "") ?>> <? if(CONFIG_PAYIN_CUSTOMER == '1'){echo("Sender Number");}?></option>
									<?	}  ?>
									  <option value="3"> Contact Number</option>
									</select>
									<? if ($_SESSION["batch"]!= "") { ?>
                  <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.custName.value != '') { window.open('search-cust.php?customerSearch='+ document.addTrans.customerSearch.value +'&val=' + document.frmAmount.amount_transactions.value + '&custName=' + document.addTrans.custName.value + '&searchid=' + document.addTrans.customerSearch.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please provide sender name or number.')}">
                <? }else{ ?>
                  <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.custName.value != '') { window.open('search-cust.php?customerSearch='+ document.addTrans.customerSearch.value +'&custName=' + document.addTrans.custName.value + '&searchid=' + document.addTrans.customerSearch.value + '&transID=<? echo $_GET["transID"]?>' + '&agentID=<? echo $_SESSION["senderAgentID"]?>', 'searchCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740') } else { alert ('Please provide sender name or number.')}">
								<? } ?>
        </td>
        <div id="search_result_agents"></div>
      <?	}  if (CONFIG_SENDER_SEARCH_FORMAT == '1') {?>
              <!--td width="280" align="center" bgcolor="#c0c0c0"><a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">Add New Sender </a></td-->
            	<td width="90" align="center" bgcolor="#c0c0c0">
            		<? if ($_SESSION["batch"]!= "") { ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            		<? }else{ ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            	<? } ?>Add New Regular Sender</a>
                </td>
            </tr>
          
              <? }else{ ?>
          
          <td width="200" align="center" bgcolor="#c0c0c0">
            		<? if ($_SESSION["batch"]!= "") { ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            		<? }else{ ?>
            		<a href="javascript:;" class="style3" onClick=" window.open('<?=$customerPage?>?from=popUp&agnetID=<?=$_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
            	<? } ?>
            			Add 
                New Regular Sender</a>
                
                <? if ($_SESSION["batch"]!= "") { ?>
                <a href="javascript:;" class="style3" onClick=" window.open('add-customer-Quick.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agnetID=<? echo $_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
                <? }else{ ?>
                <a href="javascript:;" class="style3" onClick=" window.open('add-customer-Quick.php?from=popUp&agnetID=<? echo $_SESSION["senderAgentID"]; ?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
                <? } ?>
                	/Add 
                New Quick Sender</a></td>
            </tr>
       
       <? }}?>
               <input type="hidden" name="customerID" value="<? echo $_SESSION["customerID"]?>">
           
            </table>
            
            <table  width="699" border="0" cellpadding="2" cellspacing="0">   
            <? 
if($_GET["customerID"]!="")
$_SESSION["customerID"] = $_GET["customerID"];
$queryCust = "select * from ".TBL_CUSTOMER." where customerID ='".$_SESSION[customerID]."'";
$customerContent = selectFrom($queryCust);
if($customerContent["customerID"] != "")
{
		if($_POST["document_remarks"] != $_SESSION["document_remarks"] && $_GET["customerID"] == "" && $_GET["from"] != 'conf')
		{
		$updateSender = "update ".TBL_CUSTOMER." set remarks ='".$_POST["document_remarks"]."'
		where customerID = '".$_SESSION["customerID"]."'";	
		update($updateSender);
		$_SESSION["document_remarks"] = $_POST["document_remarks"];
		
		}else{	
				$_SESSION["document_remarks"] = $customerContent["remarks"];
			
		}
	
			if($_POST["addressDescription"] != $_SESSION["addressDescription"] && $_GET["customerID"] == "" && $_GET["from"] != 'conf')
			{
			$updateSender = "update ".TBL_CUSTOMER." set 
			addressDescription = '".$_SESSION["addressDescription"]."' 
			where customerID = '".$_SESSION["customerID"]."'";	
			update($updateSender);
			
			$_SESSION["addressDescription"] = $_POST["addressDescription"];
			}else{	
			
					$_SESSION["addressDescription"] = $customerContent["addressDescription"];
			}
	
	
?>
            <tr>
              <td width="150" align="left"><font color="#005b90">Sender Name</font></td>
              <td width="200" height="20"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
              <td width="150" height="20" align="right"><font color="#005b90"> Remarks</font> </td>
              <td width="200" height="20"><input type="text" name="document_remarks" value="<? echo $_SESSION["document_remarks"] ?>"></td>
            </tr>
            <tr>
            	<td align="right" colspan="6">
            		       <a href="#" onClick="showDetails('SenderInformation','show');" class="style2">Show Details</a>
                    		&nbsp;&nbsp;&nbsp;&nbsp;
											<a href="#" onClick="showDetails('SenderInformation','hide');" class="style2">Hide Details</a>
            	</td>
            	</tr>
            </table>
            <table id="SenderInformation" style="display:none;">
            <tr >
              <td width="150" align="right"><font color="#005b90">Address</font> </td>
              <td width="221"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
              <td width="15" align="right"><font color="#005b90">Phone</font></td>
              <td width="179"><? echo $customerContent["Phone"]?></td>
            </tr>
           
         		<?	if (CONFIG_PROVE_ADDRESS == "1") {  ?>
         		
            <tr >
              <td width="157" align="right"><font color="#005b90">Proved Address</font></td>
              <td width="200"><? echo ($customerContent["proveAddress"] == "Y" ? "Yes" : "No") ?></td>
               <td width="150" height="20" align="right"><font color="#005b90"> Description</font> </td>
              <td width="200" height="20"><input type="text" name="addressDescription" value="<? echo $_SESSION["addressDescription"] ?>"></td>
            </tr>
          	<?	}  ?>          
           
      </table>
      
       		
            
			<?
				if (CONFIG_COMPLIANCE_SENDER_TRANS == "1" || CONFIG_EDIT_SENDER == "1")
				{
			?><table   width="699" border="0" cellpadding="2" cellspacing="0">
            <tr>
            	
            <?
            if(CONFIG_COMPLIANCE_SENDER_TRANS == '1'){
            		 $to = getCountryTime(CONFIG_COUNTRY_CODE);
									
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
									if($month > 1)
									{
										$month --;
										if($month < 10)
											$month = "0".$month;
									}else{
										$month = 12;
										$year = $year --;
										}
									
									 $from = $year."-".$month."-".$day." 00:00:00";
            		$countTrans = selectFrom("select count(transID) as transCount, sum(totalAmount) as totalAmount from ".TBL_TRANSACTIONS." where customerID = '".$customerContent["customerID"]."' and transDate between '$from' and '$to'");
            ?>
            <td align="center" bgcolor='#c0c0c0' width="280"><font color="#005b90">
            	<a href="javascript:;" class="style3" onClick=" window.open('compliance_cust_trans.php?customer=<?=$customerContent["customerID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=780')">
            	Last 30 Day Transactions 
            	</a><?=$countTrans["transCount"]?>
            	</font></td>
            	
						<td align="center" bgcolor='#c0c0c0' width="280"><font color="#005b90"> Total Amount <? echo(round($countTrans["totalAmount"], $roundLevel));?></font></td>		
             		
             			 
            <?	} else {  ?>
            	<td colspan="2" align="center">&nbsp;	 </td>
            <?	}  ?>
             	
            	<td align="center">&nbsp;</td>
            	<td align="center" <? echo (CONFIG_EDIT_SENDER == '1' ? "bgcolor='#c0c0c0'" : "") ?>>
          	<?	if (CONFIG_EDIT_SENDER == "1") {  ?>
           		<? if ($_SESSION["batch"]!= "") { ?>
           		<a href="#" onClick="javascript:window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&customerID=<? echo $customerContent["customerID"] ?>&transID=<? echo $_GET["transID"] ?>&transType=<?=($_REQUEST["transType"]!=""?$_REQUEST["transType"]:$_SESSION["transType"])?>', 'editCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style3">
           		<? }else{ ?>
           		<a href="#" onClick="javascript:window.open('<?=$customerPage?>?from=popUp&customerID=<? echo $customerContent["customerID"] ?>&transID=<? echo $_GET["transID"] ?>&transType=<?=($_REQUEST["transType"]!=""?$_REQUEST["transType"]:$_SESSION["transType"])?>', 'editCust', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style3">
           		<? } ?>
           			Edit Sender</a>
          	<?	} else {  ?>
          		&nbsp;
          	<?	}  ?>
            	</td>
            </tr>
       		<?	}  ?>
<?
}
?>
          </table>
</fieldset>
                  </td>
                </tr>
				<tr>
					
                  <td> <fieldset><legend class="style2">Select Beneficiary </legend>
                    <? if($_SESSION["customerID"]!="")
						{
							$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, otherId_name  from ".TBL_BENEFICIARY." where customerID ='$_SESSION[customerID]'";			
							$benificiaryContent2 = selectMultiRecords($queryBen);
							if(count($benificiaryContent2) > 0)
							{?>
                    <table width="699" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <tr class="style2"> 
                        <td width="30%">Beneficairy Name</td>
                        <td width="13%">Country</td>
                        <td width="13%">Phone</td>
                        <?
                        if(CONFIG_SHOW_BEN_ID == '1')
                        {
                        ?>
                        	<td width="12%">ID Name</td>
                        	<td width="12%">Other ID Name</td>	
                        <?
                        }
                        ?>
                        <td width="12%">&nbsp;</td>
                        <td width="8%"></tr>
                      <? }
							for($i=0;$i < count($benificiaryContent2); $i++)
							{?>
                      <tr> 
                        <td><? echo $benificiaryContent2[$i]["firstName"]." ".$benificiaryContent2[$i]["lastName"];?></td>
                        <td><? echo $benificiaryContent2[$i]["Country"];?></td>
                        <td><? echo $benificiaryContent2[$i]["Phone"];?></td>
                         <?
                        if(CONFIG_SHOW_BEN_ID == '1')
                        {
                        ?>
                        	<td><? echo $benificiaryContent2[$i]["IDType"];?></td>
                        	<td><? echo $benificiaryContent2[$i]["otherId_name"];?></td>
                        <?
                        }
                        ?>
                        <td align="center">
                        	<? if ($_SESSION["batch"]!= "") { ?>
                        	<a href="#" onClick="document.addTrans.action='add-transaction_opaldesign.php?val=' + document.frmAmount.amount_transactions.value + '&msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=exchangeRate';document.addTrans.submit();" class="style2">
                        	<? }else{ ?>
                        	<a href="#" onClick="document.addTrans.action='add-transaction_opaldesign.php?msg=1&benID=<? echo $benificiaryContent2[$i]["benID"]?>&transID=<? echo $_GET["transID"]?>&focusID=exchangeRate';document.addTrans.submit();" class="style2">
                        	<? } ?>
                        		Select</a></td>

                     	<td align="center">
                	<?	if (CONFIG_DISABLE_EDIT_CUSTBEN == '1' && ($agentType == 'SUPAI' || $agentType == 'SUPA' || $agentType == 'SUBAI' || $agentType == 'SUBA')) {  ?>
                				&nbsp;
                	<?	} else {  ?>
                     		<? if ($_SESSION["batch"]!= "") { ?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		<? }else{
                     					if(CONFIG_REMOVE_QUICK_LINK != "1"){ ?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary.php?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		<? }else{?>
                     		<a href="#" onClick="javascript:window.open('add-beneficiary-quick.php?from=popUp&benID=<? echo $benificiaryContent2[$i]["benID"]?>&agnetID=<?=$_SESSION["senderAgentID"]; ?>&transID=<? echo $_GET["transID"]?>', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style2">
                     		
                     <?	} }?>
                     			Edit</a>
                 	<?	}  ?>
                     			</td>

					  </tr>
                    
                    <? }
						}
					?>
					</table>
                  </fieldset></td>
				</tr>
				
				<tr>
                  <td>
                  	<fieldset>
                      <legend class="style2">Beneficiary Details </legend>
                   
                  
					<table width="699" border="0" cellpadding="2" cellspacing="0">
                                          <tr>
					 <td colspan="4"><br></td>
					 </tr>
					 <? if($_SESSION["customerID"]!="")
						{
						?>
					  <tr>
                        <td width="146" align="right"><font color="#005b90">&nbsp; </font></td>
                        <td colspan="2">    <input name="benID" type="hidden" id="benID" value="<? echo $_SESSION["benID"]?>">
                        </td>
                        <td width="245" height="25" align="center" bgcolor="#c0c0c0" class="style1">
                        	 <? if(CONFIG_REMOVE_QUICK_LINK != 1){?>
                        	<? if ($_SESSION["batch"]!= "") { ?>
                        	<a href="javascript:;" class="style1" onClick=" window.open('add-beneficiary.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=380,width=740')">
                        	<? }else{ ?>
                        	<a href="javascript:;" class="style1" onClick=" window.open('add-beneficiary.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=380,width=740')">
												<? } ?>
                        		<b>Add 
                          New Beneficiary/</b></a><? }?>
                         
                          <? if ($_SESSION["batch"]!= "") { ?>
                          <a href="javascript:;" class="style3" onClick=" window.open('add-beneficiary-quick.php?val=' + document.frmAmount.amount_transactions.value + '&from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"> 
                          <? }else{ ?>
                          <a href="javascript:;" class="style3" onClick=" window.open('add-beneficiary-quick.php?from=popUp&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]; ?>&transID=<? echo $_GET["transID"]?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">
                          <? } ?>
                          Add
                          New Quick Beneficiary</a></td>
                      </tr>
					  <? } ?>
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,otherId,otherId_name  from ".TBL_BENEFICIARY." where benID ='$_SESSION[benID]' and customerID ='$_SESSION[customerID]'";
			//echo $queryBen;
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
				if (CONFIG_MANUAL_FEE_RATE == "1")
				{
 					$countryList = explode(" , ",CONFIG_BEN_COUNTRY);
 					for ($i = 0; $i < count($countryList); $i++)
					{
						if (strtoupper($benificiaryContent["Country"]) == $countryList[$i])
						{
							$flagBenCountry = 1;	
						}	
					}
				}
		?>
                      <tr>
                        <td width="146" align="right"><font color="#005b90"> Name</font></td>
                        <td><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="96" align="right"><font color="#005b90">Country</font></td>
                        <td width="245"><? echo $benificiaryContent["Country"]?>
                        <input type="hidden" name="benCountry" value="<? echo $benificiaryContent["Country"]?>">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                        
                      </tr>
                      <?
				}
			//}
  ?>
                    </table>
                                   
					<? if($benificiaryContent["benID"] != "")
					{
					$countryContent = selectFrom("select serviceAvailable, bankCharges, outCurrCharges  from " . TBL_SERVICE. ", ".TBL_COUNTRY." where 1 and (toCountryId = countryId and  countryName='".$benificiaryContent["Country"]."')");
					
					?>
					<table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td colspan="2"><span class="style6">Services available for <strong><? echo $benificiaryContent["Country"]?></strong>: <? echo ($countryContent["serviceAvailable"] != "" ? $countryContent["serviceAvailable"] : "None")?></span></td>
                      </tr>
                      <tr>
                        <td width="150" align="right" valign="top"><font color="#005b90">Transaction Type</font></td>
                        <td width="500">
												<select name="transType" id="transType" style="font-family:verdana; font-size: 11px" onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&benID=<?=$benificiaryContent["benID"];?>&focusID=exchangeRate'; document.addTrans.submit();">
                            <option value="">- Select Transaction Type -</option>
                            <?
							if (strstr($countryContent["serviceAvailable"], "Cash Collection"))
							{
								if ($_SESSION["transType"] == "Pick up")
									echo "<option selected value='Pick up'>Pick up</option>";
								else
									echo "<option value='Pick up'>Pick up</option>";
							}
							if (strstr($countryContent["serviceAvailable"], "Bank Deposit"))
							{
								
								if ($_SESSION["transType"] == "Bank Transfer")
								{
									$_SESSION["bankCharges"] = $countryContent["bankCharges"];
									echo "<option value='Bank Transfer' selected>Bank Transfer</option>";
									
								}
								else
									echo "<option value='Bank Transfer'>Bank Transfer</option>";
							}
							if (strstr($countryContent["serviceAvailable"], "Home Delivery"))
							{
								if ($_SESSION["transType"] == "Home Delivery")
									echo "<option value='Home Delivery' selected>Home Delivery</option>";
								else
									echo "<option value='Home Delivery'>Home Delivery</option>";
							}

							?><?  //echo $_SESSION["transType"] = "";?>
                        </select>
                        <? if($_POST['transType']== "Home Delivery"){
                                if($benificiaryContent["Address"]== "" && $benificiaryContent["Address1"] == ""){
                            ?>
                        <div id="msgdiv" style="visibility:visible; vertical-align:bottom;">
                            <font color="#3366FF"><br>Please Enter Address Detail by clicking Edit button on Select Beneficiary Section</font>
                        </div>
                        <?
                                }
                           }
                        ?>

                            <script language="JavaScript">SelectOption(document.addTrans.transType,"<?=$_SESSION["transType"]; ?>");</script>
<? if($agentType == 'SUPA' || $agentType == 'SUPAI' || $agentType == 'SUBA' || $agentType == 'SUBAI' ) 
{

 $senderAgentID  = $username;
}
/**
 * Introduce the 'Admin' userType in below clause to cater transaction made by 'admin staff' for agents 
 * Bug found in Ticket #3881
 *
 * In any case other than aove the uagent will be selected from the agent search 
 */
else/*if($agentType == 'admin' || $agentType == 'Admin' || $agentType == 'Admin Manager')*/
{
	 $senderAgentID  = $senderAgentContent['userID'];
}
?>
	
                        <input type="hidden" name="custAgentID" value="<? echo $senderAgentID ; ?>"></td>
                      </tr>
                    </table>
					 <? 
					 }
					if ($_SESSION["transType"] != "")
					{ ?>
                    <table width="650" border="0" cellpadding="2" cellspacing="0">
<?
if (strstr($countryContent["serviceAvailable"], "Cash Collection")){
if ($_SESSION["transType"] == "Pick up")
{
?>						
							<?	if (CONFIG_COLLPOINTS_DROPDOWN != '1') {  ?>
                      <tr>
                        <td colspan="3"><br>
                          Select correspondent - Please enter city <? if(CONFIG_CP_SEARCH_VIA_COUNTRY == '1'){?>/country<? }?> name to display 
                          <?=$company?> Transfer 's correspondents.<br>
						</td>
                      </tr>
				  		<?	} else {	echo "<br>";  }  ?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">
						<?
						if ($_SESSION["transType"] == "Pick up")
						{
							if (CONFIG_COLLPOINTS_DROPDOWN == '1') {
								echo "Select Collection Point";
							} else {
								echo "Search Collection Point";
							}
						}
						else
						{
							echo "Search correspondent";
						}
						?>
						<font color="#ff0000">*</font>
						 </font></td>
                        <td width="300">
                   		<?	if (CONFIG_COLLPOINTS_DROPDOWN == '1') {  ?>
                   				<select name="collPoints" id="collPoints" onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&benID=<?=$benificiaryContent["benID"];?>&focusID=exchangeRate'; document.addTrans.submit();">
                   					<option value="">- Select Collection Point -</option>
                   			<?
                   					$queryCollPoints = "SELECT `cp_id`, `cp_corresspondent_name` FROM " . TBL_COLLECTION . " WHERE `cp_country` = '".$benificiaryContent["Country"]."'";
                   					$collPoints = selectMultiRecords($queryCollPoints);
                   					for ($i = 0; $i < count($collPoints); $i++) {
                   			?>
                   					<option value="<? echo $collPoints[$i]["cp_id"] ?>" <? echo ($_SESSION["collectionPointID"] == $collPoints[$i]["cp_id"] ? "selected" : "") ?>><? echo $collPoints[$i]["cp_corresspondent_name"] ?></option>
                   			<?	}  ?>
                   				</select>
                   		<?	} else {  ?>
                        	<input name="agentCity" type="text" id="agentCity" size="15">
                          <input type="button" name="Submit" value="Search" onClick=" if(document.addTrans.agentCity.value != '') { window.open('search-agent-city.php?agentCity=' + document.addTrans.agentCity.value + '&transID=<? echo $_GET["transID"]?>' +  '&benCountry=<? echo $benificiaryContent["Country"]?>', 'agentCity', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=800') } else { alert ('Please write city name in the text box to search.')}">
                      <?	}  ?>
                          <input name="distribut" type="hidden" id="distribut" value="<? echo $_SESSION["benAgentID"]?>">
                      
					    </td>
                        <td width="200">&nbsp;</td>
                      </tr>

                    </table>
<table width="650" border="0" cellpadding="2" cellspacing="0"> 
                      <? 
		
			 $queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$senderAgentContent["cp_id"];
			$dist = $senderAgentContent["cp_ida_id"];
			$_SESSION["distribut"] = $dist;
			$_SESSION["benAgentID"] = $dist;
			
			$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                        <tr>
                        <td width="150" align="right"><font color="#005b90">Collection point name</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> </td>
                        <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200"><? echo $senderAgentContent["cp_contact_person_name"]?></td>
                      </tr>
                      <tr>
                      
                        <td width="150" align="right"><font class="style2">Distributor</font></td>
                        <td width="200" colspan="3"><? echo $queryExecute["name"]?></td>
                      </tr> 
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font></td>
                        <td colspan="3"><? echo $senderAgentContent["cp_branch_address"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">City</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_city"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_country"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Fax</font></td>
                        <td width="200"><? echo $senderAgentContent["cp_fax"]?></td>
                      </tr> 
                      

                      <?
			  }
			  ?>
<?
}}
?>			  
                    </table>                    <?
					  }
					  if($_SESSION["transType"] == "Home Delivery")
					  {
					  	if (CONFIG_CITY_SERVICE_ENABLED)
					  	{
					  		$cityCount = countRecords("select count(*) from ".TBL_SRV_NOT_AVAILABLE." where country = '".$benificiaryContent["Country"]."' and city = '".$benificiaryContent["City"]."' and service like '%Home Delivery%' and Active = 'Yes'");
					  		if ($cityCount > 0)
					  		{
					  	$queryBenAgentID = selectFrom("select dist_ID from ".TBL_SRV_NOT_AVAILABLE." where country = '".$benificiaryContent["Country"]."' and city = '".$benificiaryContent["City"]."' and Active = 'Yes'");
               
               	$_SESSION["distribut"] = $queryBenAgentID[0];
				$dist = $_SESSION["distribut"];
	 			$_SESSION["benAgentID"] = $_SESSION["distribut"];
	 		      	 ?>
                    <table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td width="650"><span class="style4">Note:</span><br>
                        <span class="style5">Please make sure that Beneficiary's Home Address is correct. <?=$company?> Transfer will not be responsible for faulty address provided by Sender or Agent.</span>
                        <input type="hidden" name="notService" value="">
                        </td>
                      </tr>
                    </table>
					  				
               <? } else
               {
               	 ?>
                    <table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td width="650"><span class="style4">Sorry:</span><br>
                        <span class="style5">Service for Home Delivery in city: <? echo "\"" . $benificiaryContent["City"] . "\"" ?> is not Available.</span>
                        <input type="hidden" name="notService" value="Home Delivery">
                        </td>
                      </tr>
                    </table>
                    <?
                  }
                }
                else
                {
                	?>
                	<table width="650" border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td width="650"><span class="style4">Note:</span><br>
                        <span class="style5">Please make sure that Beneficiary's Home Address is correct. <?=$company?> Transfer will not be responsible for faulty address provided by Sender or Agent.</span>
                        <input type="hidden" name="notService" value="">
                        </td>
                      </tr>
                    </table>
                    <?
                    
                    $distributors = selectFrom("select userID from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != '' and authorizedFor like '%Home Delivery%' and IDAcountry like '%".$benificiaryContent["Country"]."%'");					
				
					if($distributors["userID"] != "")
					{
						   $_SESSION["distribut"] = $distributors["userID"];
						   $dist = $_SESSION["distribut"];
						   $_SESSION["benAgentID"] = $_SESSION["distribut"];
					}else{
						   $_SESSION["distribut"] = "";
						   $dist = "";
						   $_SESSION["benAgentID"] = "";
						}	
                    
					}
				
				
				
				}
			  if($_SESSION["transType"] == "Bank Transfer")
			  {?>
                    
                    <table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <tr>
                        <td height="20" colspan="4"><span class="style2">Beneficiary Bank Details</span></td>
                      </tr>
				<?
					$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
					if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European") {
				?>
            <tr>
            <td width="135" height="20" align="right"><font color="#005b90">IBAN<font color="#ff0000">*</font> </font></td>
            <td width="209" height="20"><input type="text" name="IBAN" id="IBAN" value="<? echo $_SESSION["IBAN"]?>" maxlength="26" onBlur="if (document.getElementById('IBAN').value != ''){ document.getElementById('ibanDiv').innerHTML = 'You have entered ' + document.getElementById('IBAN').value.length + ' IBAN characters'; document.getElementById('trIBANChars').style.display = 'table-row'; } else { document.getElementById('trIBANChars').style.display = 'none'; }"></td>
        	 	<td width="102" height="20" align="right"><font color="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></font></td>
           	<td width="188" height="20">
           		<? echo ($benificiaryContent["Country"] == "GEORGIA" ? "<input type='text' name='ibanRemarks' value='".$_SESSION["ibanRemarks"]."'>" : "&nbsp;") ?>
           	</td>
          	</tr>
            <tr id="trIBANChars" <? if ($_SESSION["IBAN"] == '') { ?>style="display:none;"<? } ?>>
            <td width="135" height="20" align="center" colspan="3"><div id="ibanDiv"></div></td>
           	<td width="188" height="20"><input type="hidden" name="benCountryRegion" value="European">&nbsp;</td>
            </tr>
				<?	
					} else {
        ?>   
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name<font color="#ff0000">*</font> </font></td>
                        <td width="200" height="20">
						<? 
						$benCountryName = $benificiaryContent["Country"];
						$bankdataQ = "select * from imbanks where 1 and country = '".$benCountryName."'";
						$bank_data = selectMultiRecords($bankdataQ);
						if ($benCountryName == CONFIG_CPF_COUNTRY && count($bank_data) > 0)
						{
							?>
							<select name='bankName' style='font-family:verdana; font-size: 11px; width:150'  >
								<option value="">- select bank -</option>
							<?
							for($i=0;$i < count($bank_data); $i++)
							{
							  	echo "<option value='".$bank_data[$i]["bankName"]."'>".$bank_data[$i]["bankName"]."</option>\n";
							}
							?>
							</select>
							<script language="JavaScript">SelectOption(document.addTrans.bankName, "<?=$_SESSION["bankName"]; ?>");</script>
							<?
						}
						else
						{?>
							<input name="bankName" type="text" id="bankName" value="<?=$_SESSION["bankName"]; ?>" maxlength="25">
						<?
						}
						?>
						</td>
                        <td width="100" align="right"><font color="#005b90">Acc Number<font color="#ff0000">*</font> </font></td>
                        <td width="200"><input name="accNo" type="text" id="accNo" value="<?=$_SESSION["accNo"]; ?>" maxlength="50"></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_BRANCH_NAME_LABEL == '1'){ ?>Branch name<? }else {?>Branch 
                        Name/Number<? }?><font color="#ff0000">*</font> </font></td>
                        <td width="200" height="20"><input name="branchCode" type="text" id="branchCode" value="<? echo $_SESSION["branchCode"]; ?>" maxlength="25"></td>
                        
					<td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
        	<td width="200" height="20">&nbsp;</td>
		        </tr>
		        
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address<font color="#ff0000">*</font></font> </td>
                        <td width="200" height="20"><input name="branchAddress" type="text" id="branchAddress" value="<?=$_SESSION["branchAddress"]; ?>" size="30" maxlength="254"></td>
                       <td width="150" height="20" align="right"><font color="#005b90">Swift Code<font color="#ff0000">*</font></font></td>
                        <td width="200" height="20"><input name="swiftCode" type="text" id="swiftCode" value="<?=$_SESSION["swiftCode"]; ?>" maxlength="25"></td>
                      </tr>
                      <tr>
                      <?
                      if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
                      {
                      ?>	
                        <td width="150" height="20" align="right"><font color="#005b90">Account Type</font></td>
                        <td width="200" height="20"><br>
                        	<input name="accountType" type="radio" id="accountType" value="Regular" <? echo ($_SESSION["accountType"] != "Savings" ? "checked"  : "")?>> Regular<br>
                        	<input name="accountType" type="radio" id="accountType" value="Savings" <? echo ($_SESSION["accountType"] == "Savings" ? "checked"  : "")?>> Savings
                        	</td>
                     	<?
                    }else{
                    	?>
                    	 <td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                       <td width="200" height="20">&nbsp;</td>                  	
                    	<?
                    	}
                     	?>
                       <td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                       <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <!--tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Swift Code<font color="#ff0000">*</font></font></td>
                        <td width="200" height="20"><input name="swiftCode" type="text" id="swiftCode" value="<?=$_SESSION["swiftCode"]; ?>" maxlength="25"></td>
                        <td width="100" height="20" align="right" title="For european Countries only"><font color="#005b90">IBAN Number </font></td>
                        <td width="200" height="20" title="For european Countries only"><input name="IBAN" type="text" id="IBAN" value="<?=$_SESSION["IBAN"]; ?>" maxlength="50"></td>
                      </tr>
					  <tr>
					  <td colspan="2">Swift Code - Required to ensure accuracy of Bank Details</td>
					  <td colspan="2" align="right">IBAN - Required for European Bank Tansfers</td>
							</tr-->
							
						<?	}  ?>
					  <tr>
					  <td colspan="2" align="right"><font color="#005b90">Select Distributor<font color="#ff0000">*</font></font>
					  	<select name="distribut" id="distribut" style="font-family:verdana; font-size: 11px; width:226" onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=exchangeRate'; document.addTrans.submit();">
               <option value="">- Select One -</option>
               <?
	               /*$defaulted = selectFrom("select userID, username, name  from admin where userID = '100111'");				
	               
	               if($defaulted["userID"] == $_SESSION["distribut"])
	               {
	               	
	               	$dist =  $defaulted["userID"];
	               ?>

	               <option value="<? echo $defaulted["userID"] ?>" selected >
	               	<? echo $defaulted["username"]." [".$defaulted["name"]."]"; 
	               	?>
	                </option> 
					  	<?
					  		}
					  		else{?>
					  			<option value="<? echo $defaulted["userID"] ?>">
	               	<? echo $defaulted["username"]." [".$defaulted["name"]."]"; 
	               	?>
	                </option> 
					  			<? }*/
					  	$agenCountry = $benificiaryContent["Country"];
					  	/* Code modified by Khola @ ticket#3497
					  		Have changed the query in $distributors; added the condition userID !='".$_SESSION["senderAgentID"]."' 
					  		which is for, if AnD selected as agent cannot be seleced as distributor for the same transaction. 
					  		Applied restriction for core functionality */
					  	$distributors = "select * from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and userID !='".$_SESSION["senderAgentID"]."'  order by userID";				
					  	$ida = selectMultiRecords($distributors);
							for ($j=0; $j < count($ida); $j++)
							{
								$authority = $ida[$j]["authorizedFor"];
								$toCountry = $ida[$j]["IDAcountry"];
								$isDefault = $ida[$j]["defaultDistrib"];
								if(strstr($authority,"Bank Transfer") && strstr($toCountry,$agenCountry))
								{
									if($_SESSION["distribut"] == "" && $isDefault == 'Y')
									{
										 $_SESSION["distribut"] = $ida[$j]["userID"];
										}
									//
								if($ida[$j]["userID"] == $_SESSION["distribut"])
								{
									$dist =  $ida[$j]["userID"];
									?>
                <option value="<? echo $_SESSION["distribut"]; ?>" selected>
                <? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
                </option>
                <?
								}
								else
								{
								?>
                <option value="<? echo $ida[$j]["userID"]; ?>">
                <? echo $ida[$j]["username"]." [".$ida[$j]["name"]."]"; ?>
                </option>
                <?
									}
									
								}
							}
					  	?>
					  </select>
					  
					  	</td>
					  </tr>				
					  	  
                      <?
                      
  				}
  				//Just Commited as we have drop dwon to distributor be selected manually by user.
  						/*if($_SESSION["transType"] == "Bank Transfer" || $_SESSION["transType"] == "Home Delivery")
							{
									
									$agenCountry = $benificiaryContent["Country"];
									$query = "select * from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != ''";				
									
									$ida = selectMultiRecords($query);
									for ($j=0; $j < count($ida); $j++)
									{
										$authority = $ida[$j]["authorizedFor"];
										$toCountry = $ida[$j]["IDAcountry"];
										if(strstr($authority,"Bank Transfer") && strstr($toCountry,$agenCountry))
										{
											$dist =  $ida[$j]["userID"];
											
											}
									}
									// if find no IDA against this BenCountry then assign this transaction to AdminIDA 
									if($dist == '')
									{
										$dist = 100111;
										}//100098
										// $benAgentIDs = $adminID;
								//}
								
							}*/
  				
  				
  				?>
                    </table>
                  
                
                <?
		  if($benificiaryContent["benID"] != "")
		  {
		  ?>
		  
                <tr>
                  <td valign="top"><fieldset>
                  	
                    <legend class="style2">Transaction Details </legend>
                    <? if ($_SESSION["batch"]!= "") { ?>
                    <input type="hidden" name="t_amount" value="<?= $_SESSION["amount_transactions"];?>">
                    <input type="hidden" name="l_amount" value="<?= $_SESSION["amount_left"];?>">
                  <? }
				$contentCustomer = selectFrom("select *  from ".TBL_CUSTOMER . " where customerID = '".$_SESSION["customerID"]."'");								
				$customerContent["Country"] .  $benificiaryContent["Country"];

				if($_POST['currencyTo'] != "" )
				{
				
				 	 $currencyTo = $_POST['currencyTo'];	
					 $_SESSION["currencyTo"] = $_POST['currencyTo'];
					
				}
		
				$dDate = "";
				
				if($_SESSION['transDate'] != "" )
				{
				
					//$dDate = $_SESSION["transDate"];
					$dDate = explode("/",$_SESSION["transDate"]);
					if(count($dDate) == 3) {
						$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
					} else {
						$dDate = $_SESSION["transDate"];
					}
				
					$_SESSION["transDate"]=$dDate;
				
				}
				
				
				//echo("Distributor ".$dist." ");
			 	//$exchangeData = getExchangeRateTransaction( $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["currencyTo"]);
		/***********
		*
		*Here starts with calculation of Fee and Exchange rates
		*
		************/	 	
		//if(!($fromTotalConfig == '1' && $_POST["totalAmount"]!= ""))
		
		$benCountryName = $benificiaryContent["Country"];
		
		if($fromTotalConfig != '1')
		{
		
			 	if (CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
			 	{
			 			$exID = 0;
			 			if ($_SESSION["exchangeRate"] != "")
			 			{
			 				$exRate	= $_SESSION["exchangeRate"];
			 				$_SESSION["exchangeRate"] = "";
			 			}
			 			elseif ($_POST["exchangeRate"] != "")
			 			{
			 				$exRate	= $_POST["exchangeRate"];
			 			}
			 			else
			 			{
			 				$exRate = 0;
			 			}
			 	} else {
			 	
						$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
				   $exID 			  = $exchangeData[0];
	        $exRate 		  = $exchangeData[1];	
				$currencyFrom = $exchangeData[2];
				$currencyTo 	= $exchangeData[3];
				
				$_SESSION["currencyFrom"] = $currencyFrom;
			}
					//echo(" Got at line 1596 ".$_SESSION["currencyTo"]." and value i Variable ".$currencyTo);
			if($_POST["local"] == "Local Amount")
			{
				if($_SESSION["transAmount"] != "")
				{
				 $localAmount = $transAmount * $exRate;
				 
				 	if(CONFIG_ROUND_NUMBER_ENABLED == '1' && strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].","))
					{
						$localAmount = round($localAmount);		
					}
					
				 $_SESSION["localAmount"] = $localAmount;
				}
				if ($_POST["t_amount"] != "")
				{
					$_SESSION["amount_transactions"] = $_POST["t_amount"];
				}
				if ($_POST["l_amount"] != "")
				{
					$_SESSION["amount_left"] = $_POST["l_amount"];
				}
			}
			$compareCurrency2 = currencyValue();///it is used for #1772 calculations
			if($_POST["amount"] == "Amount" || (CONFIG_TRANS_ROUND_NUMBER == '1' && $_POST["moneyPaid"] == 'By Cash' && $compareCurrency2))
			{				
				
				if($_SESSION["localAmount"] != "")
				{
					//////This condition is based on opal requirement to calculate amount on rounded value based on cash payment
					if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM')
              		{
              			$localAmount= RoundVal($localAmount);
              		}
              		////////////////////#1772 Ticket
					$transAmount = $localAmount / $exRate;
					$_SESSION["transAmount"] = $transAmount;
				}
				if ($_POST["t_amount"] != "")
				{
					$_SESSION["amount_transactions"] = $_POST["t_amount"];
				}
				if ($_POST["l_amount"] != "")
				{
					$_SESSION["amount_left"] = $_POST["l_amount"];
				}
			}
				
{
	
			  
	       if (CONFIG_FEE_BASED_TRANSTYPE == "1" && $_SESSION["transType"] == 'Pick up') {
	       		
	       	 $amountType=$_SESSION["localAmount"];
	      	 $feetransType=$_SESSION["transType"];
	       		
	       
	       	}else{
	      	
	         $amountType=$_SESSION["transAmount"];
	         $feetransType=$_SESSION["transType"];
	       	}  
	     
			
			
				$imFee = 0;
				if ($_SESSION["chDiscount"]!=""){ 
					$imFee = $_SESSION["discount"];
				}elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 ){
					$imFee = 0 ;
						 //discount_request
					}elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
				{
					if ($_SESSION["IMFee"] != "")
			 		{
			 			$imFee	= $_SESSION["IMFee"];
			 			$_SESSION["IMFee"] = "";
			 		}
			 		elseif ($_POST["IMFee"] != "")
			 		{
			 			$imFee	= $_POST["IMFee"];
			 		}
				}
				else
				{
					if(CONFIG_PAYIN_CUSTOMER != '1')
					{
						if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
						{
							$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						}
						if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
						{
							$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	

						}
						if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
						{
						 $imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						
						}
						if($imFee <= 0)
						{
							$imFee = imFee($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"],$feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						
						}
							
					}else
					{	
						if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
						{
							if($customerContent["payinBook"] != "" )
								$imFee = imFeeAgentPayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
								$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["collectionPointID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						}
						if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
						{
							if($customerContent["payinBook"] != "" )
								$imFee = imFeeAgentPayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
						 		$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $dist, $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 		
						}
						if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
						{
							if($customerContent["payinBook"] != "" )
								$imFee = imFeeAgentPayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
						 		$imFee = imFeeAgent($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $_SESSION["senderAgentID"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
						 		
					 	}
						if($imFee == 0)
						{		
							if($customerContent["payinBook"] != "" )
								$imFee = imFeePayin($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);	
							else
						 		$imFee = imFee($_SESSION["transAmount"],  $customerContent["Country"], $benificiaryContent["Country"], $feetransType, $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);
					 	}
			  	}
				}			
			//echo("at line 2157 transID  ");
			//	echo("Distributor is $dist and Fee is $imFee");
				 $totalAmount =  $_SESSION["transAmount"] ;
				 $localAmount = $_SESSION["transAmount"] * $exRate;
					if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					{
						$localAmount = round($localAmount);		
					}
					
				
			 	$imFeetemp = ($imFee);
			
				
				if(CONFIG_CURRENCY_CHARGES == '1')
				{
					$local = "No";
					$_SESSION["currencyCharge"]="";
					$strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '".$_SESSION["currencyTo"]."'";
					$currencies = selectMultiRecords($strCurrency);
					for ($c=0; $c < count($currencies); $c++)
					{
						if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
						$local = "Yes";
					}
					
					if($local=="No")
					{
						///////Out Currency Charges are working on Per 100 Amount
						$tempCharge = $_SESSION["transAmount"]/100.01;
						$tempCharge2 = (int)$tempCharge;
						$tempCharge2 = $tempCharge2 + 1;
						$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
						$_SESSION["currencyCharge"] = $tempCharge2; 
						$totalAmount = $totalAmount + $_SESSION["currencyCharge"];
					}
					
				}
				//echo("at line 2665-".$_SESSION["moneyPaid"] );
				if($_SESSION["moneyPaid"] == 'By Cash')
				{
				
						$totalAmount = ( $totalAmount +  $imFee);
						$imFeetemp = ($imFee);		
						if(CONFIG_CASH_PAID_CHARGES_ENABLED)
						{
							$totalAmount = ( $totalAmount +  CONFIG_CASH_PAID_CHARGES);
							}			
										
				}
				elseif($_SESSION["moneyPaid"] == 'By Cheque')
				{
					$totalAmount = ($totalAmount +  $imFee);
					$imFeetemp = ($imFee);
							
				}
				else//if($_SESSION["moneyPaid"] == 'By Bank Transfer')
				{		 
			  
					$totalAmount = ($totalAmount + $imFee);
					$imFeetemp = ($imFee);
				}
				//echo("at line 2689-".$_SESSION["moneyPaid"] );
				if($_SESSION["transType"]=="Bank Transfer")
				{		 
					$totalAmount = ( $totalAmount + $_SESSION["bankCharges"]);
					
				}
				
			  if($_SESSION["transAmount"] != "" && $_SESSION["chDiscount"]=="")
			  {
			  	
			  	if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
				{
			  ?>
                    <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
                      <tr>
                        <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                        <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".TE2."</b</font>"; ?></td>
                      </tr>
                    </table>                    
                    <?
                }
			  }
}

}else{ 
	
	$msgWrongTotal = "";
	/////Condition OF Calculation from Total Starts Here///////////
	if($_POST["totalAmount"] != '')
	{
		
		$totalAmount = $_POST["totalAmount"];
	}
	$totalAmountTemp = $totalAmount;
	/////////////////Bank Charges///////////////
	if($totalAmount > 0)
	{
	
		if($_SESSION["transType"]=="Bank Transfer")
		{		 
			$totalAmountTemp = ( $totalAmountTemp - $_SESSION["bankCharges"]);
		}
			
		/////////////////////////////Cash Charges
		//echo("at line 2733-".$_SESSION["moneyPaid"] );
		if($_SESSION["moneyPaid"] == 'By Cash')
		{
	 		if(CONFIG_CASH_PAID_CHARGES_ENABLED)
			{
				$totalAmountTemp = ( $totalAmountTemp -  CONFIG_CASH_PAID_CHARGES);
			}			
								
		}
		//echo("at line 2742-".$_SESSION["moneyPaid"] );
		////////////////////////////Outer Currency Charges////////////
		
		if(CONFIG_CURRENCY_CHARGES == '1')
		{
			$local = "No";
			$_SESSION["currencyCharge"]="";
			$strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '".$_SESSION["currencyTo"]."'";
			$currencies = selectMultiRecords($strCurrency);
			for ($c=0; $c < count($currencies); $c++)
			{
				if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benificiaryContent["Country"]))
				$local = "Yes";
			}
			
			if($local=="No")
			{
				///////Out Currency Charges are working on Per 100 Amount
				$tempCharge = $totalAmountTemp/100.01;
				$tempCharge2 = (int)$tempCharge;
				$tempCharge2 = $tempCharge2 + 1;
				$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
				$_SESSION["currencyCharge"] = $tempCharge2; 
				$totalAmountTemp = $totalAmountTemp - $_SESSION["currencyCharge"];
			}
			
		}
		///////////////////Calculation of Fee//////////////////
			if ($chDiscount != ""){ 
					$imFee = $discount;
				}elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0){
					$imFee = 0 ;
						 //discount_request
					}elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
					{
						if ($_SESSION["IMFee"] != "")
				 		{
				 			$imFee	= $_SESSION["IMFee"];
				 			$_SESSION["IMFee"] = "";
				 		}
				 		elseif ($_POST["IMFee"] != "")
				 		{
				 			$imFee	= $_POST["IMFee"];
				 		}
					}
					
					if($customerContent["payinBook"] != "" )
					{
						$payinBook = True;
					}else{
						$payinBook = False;
						}

				$FeeData = CalculateFromTotal($imFee, $totalAmountTemp , $customerContent["Country"], $benificiaryContent["Country"], $dist, $_SESSION["senderAgentID"],$payinBook, $_SESSION["transType"], $_SESSION["currencyFrom"], $_SESSION["currencyTo"]);
					
				 $transAmount = $FeeData[0];
				 $imFee = $FeeData[1];
				/*
				if ($transAmount != "" && $imFee != "") {
				 	$validTranAmount = selectFrom("select amountRangeFrom from " . TBL_IMFEE . " where Fee = '".$imFee."' and destCountry='".$benificiaryContent["Country"]."' and origCountry='".$customerContent["Country"]."'");
				 	if ($transAmount < $validTranAmount["amountRangeFrom"]) {
				 		unset($transAmount);
				 		unset($imFee);
				 		$msgWrongTotal = "The Total Amount you entered is invalid. Please consult the fee table.";
				 	}
				}
				*/
				//echo("at line 2334  values are ".$transAmount."  and  ".$_SESSION["transAmount"]);
				//$_SESSION["transAmount"] = $transAmount;
				//echo("at line 2336  values are ".$transAmount."  and  ".$_SESSION["transAmount"]);
				///////////////////////Calculation of Exchange Rates//////////
				
				if (CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")
			 	{
			 			$exID = 0;
			 			if ($_SESSION["exchangeRate"] != "")
			 			{
			 				$exRate	= $_SESSION["exchangeRate"];
			 				$_SESSION["exchangeRate"] = "";
			 			}
			 			elseif ($_POST["exchangeRate"] != "")
			 			{
			 				$exRate	= $_POST["exchangeRate"];
			 			}
			 			else
			 			{
			 				$exRate = 0;
			 			}
			 				
				 	}else{
				 	
				 //		echo("at line 2833-".$_SESSION["moneyPaid"] );
				 	$exchangeData = getMultipleExchangeRates($customerContent["Country"], $benificiaryContent["Country"],$_SESSION["currencyTo"], $dist, 0 , $dDate, $_SESSION["currencyFrom"],$_SESSION["transType"],$_SESSION["moneyPaid"], $_SESSION["senderAgentID"]);
					//echo("at line 2835-".$_SESSION["moneyPaid"] );
					$exID 			= $exchangeData[0];
	$exRate 		= $exchangeData[1];
					$currencyFrom 	= $exchangeData[2];
					$currencyTo 	= $exchangeData[3];
					
					$_SESSION["currencyFrom"] = $currencyFrom;
				}
				///////////////////	Calculating Local Amount
				if($_SESSION["transAmount"] != "")
				{
				 $localAmount = $transAmount * $exRate;
				 
				 	if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					{
						$localAmount = round($localAmount);		
					}
					
				 $_SESSION["localAmount"] = $localAmount;
				}
				
				if ($msgWrongTotal != "") {
					$localAmount = "";
					$_SESSION["localAmount"] = "";
				}
					
					/////////////////////Error Message if Fee Doesn't Exist/////////
					if($_SESSION["transAmount"] != "")
					  {
						if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
						{
					  	?>
		                    <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
		                      <tr>
		                        <td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
		                        <td width="635"><? echo "<font color='#990000'><b>".TE2."</b</font>"; ?></td>
		                      </tr>
		                    </table>                    
		                <?
		                }
		            }
			}
			if ($msgWrongTotal != "") {
			?>
		                    <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
		                      <tr>
		                        <td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
		                        <td width="635"><? echo "<font color='#990000'><b>".$msgWrongTotal."</b</font>"; ?></td>
		                      </tr>
		                    </table>                    
			<?	
			}		
		}
		/***************
		*
		*Uptill here is the logic to calculate fee and exchange rate etc
		*
		****************/
		
		/***********
		*To Add the Limit
		*
		*if  CONFIG_LIMIT_TRANS_AMOUNT is 1
		************/	
		if(CONFIG_LIMIT_TRANS_AMOUNT == '1')
		{
			$limitObject = new TransLimit;	
			//echo("Values to be sent to check");
			$limitCheckData[] = $_SESSION["loggedUserData"]["userID"];
			$limitCheckData[] = $totalAmount;
			$limitCheckData[] = $_SESSION["currencyFrom"];
			$value = $limitObject-> calculateLimit($limitCheckData);
			if(!$value)
			{
			?>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
	          <tr>
                <td width="40" align="center"><font size="5" color="#990000"><b><i>!</i></b></font></td>
                <td width="635"><? echo "<font color='#990000'><b>You have Exceeded your Limit</b</font>"; ?></td>
              </tr>
            </table>   	
			<? }
	
			
		}

?>
		        		          	<table width="700" border="0" cellpadding="2" cellspacing="0">
<?		        		          		 if(CONFIG_BACK_DATED == '1')
		         {
		          if((strstr($userDetails["rights"], "Backdated")) || ($userDetails["parentID"]== "0" && $agentType == "admin" )) {?>

                      <tr>
                        <td width="128" height="20" align="right"><font color="#005b90">Back Date</font></td>
					              		                        
                        
                        <!--script language="JavaScript">
												
												var GC_SET_0 = {
													'appearance': GC_APPEARANCE2
												}
												new gCalendar(GC_SET_0);
                        </script-->
                        
                        <td height="20" width="195"><input name="dDate" type="text" id="dDate" value="<? echo $_SESSION["transDate"];?>" readonly>&nbsp;<a href="<? if (strstr($countryContent["serviceAvailable"], "Cash Collection") && $_SESSION["transType"] == "Pick up" && $senderAgentContent["cp_id"] == "") { ?>javascript:alert('Please select the collection point first.');<? } else { ?>javascript:show_calendar('addTrans.dDate');<? } ?>" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=34 height=20 border=0></a>&nbsp;
                        <input  type="button" name="Submit3" value=" Reset Date " onClick="resetDate();"><input type="hidden" name="manageTransDate" id="manageTransDate" value=""></td>
                        </td>
                        <td width="15" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                        <td width="281" height="20" class="style1">&nbsp;</td>
                      </tr>
                      <? }
                      
                    } ?>
                    
                                 
            
                    
               <?
               

              if(CONFIG_CALCULATE_BY == '1'){ ?>
  <tr>
    <td width="128" height="20" align="right"><font color="#005b90">Money Paid:</td>
    	<td width="195" height="20">
    		
											
    		
    		<select  name="calculateBy" id="calculateByID" onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();">
    		
                           
                            <option value="inclusive" <? if($_SESSION["moneyPaid"] == "By Bank Transfer"){echo ("selected");} ?>>By Bank Transfer</option>
                            <option value="exclusive" <? if($_SESSION["moneyPaid"] == "By Cash"){echo ("selected");} ?>>By Cash</option>
                            
                          </select>
					</td>
                                
    </tr>
		<input type="hidden" name="moneyPaid" value="<? echo $_SESSION["moneyPaid"];?>">
              	<? } ?>     
       
            	
     	<?
            	
            	
            		
            		/***********
            		*	This page will swap the amount and local amount up and down
            		* With above, currency from and currency to are also swapped
            		* For this purpose, there is a config variable CONFIG_SWAP_AMOUNT_LOCALAMOUNT
            		* [by Jamshed]
            		*************/
            		include "swap-amount-local.php";
            	?>
            	
     
            	
           
                        	
						<input type="hidden" name="benCountry" id="benCountryID" value="<? echo $benCountryName;?>">
						<input type="hidden" name="custCountry" id="custCountryID" value="<? echo $customerContent["Country"];?>">
						  
						 <? if ($_SESSION["chDiscount"]==""){ ?>
                          <tr id=Fee style="DISPLAY: ">
                            <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?><? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "<font color='#ff0000'>*</font>" : "");?></font></td>
                            <td width="200"><input type="text" name="IMFee" id="IMFee" value="<? 
                               if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                               	
                                    echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                    echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                                
                                 ?>" maxlength="25" <? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "" : "readonly");?>>
                              
                              </td>
							  <?
							  //$_SESSION["discount"]=$imFee;
							   ?>
                          </tr>
                          <tr id=Discount style="DISPLAY: none">
                            <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</font></td>
                            <td width="200" align="left"><span class="style1">
                              <input type="text" name="discount" maxlength="32" value="<?=$_SESSION["discount"]?>">
                            </span></td>
                          </tr>
      
						  <? }
						  
						   else 
						   {
						    ?>
							<tr id=Fee style="DISPLAY:">
                            <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?><? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "<font color='#ff0000'>*</font>" : "");?></font></td>
                            <td width="200" align="left"><input type="text" name="IMFee" id="IMFee" value="<? 
                                if(CONFIG_TRANS_ROUND_NUMBER == 1  && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                    echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                                 
                             ?>" maxlength="25" <? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "" : "readonly");?>>
                             
                              </td>
							 
                          </tr>
							 
                            <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</font></td>
                            <td width="200" align="left"><span class="style1">
                              <input type="text" name="discount" maxlength="32" value="<?=$_SESSION["discount"]?>" onBlur="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();">
                            </span></td>
                         
						  <? } ?>
                   </td>     
						<? if ($_SESSION["chDiscount"]==""){ ?>
                   <td>&nbsp;</td>
                    <td>&nbsp;</td>
            <? }
				if($fromTotalConfig != '1'){
               
      if (CONFIG_FRONT_BUTTONS != '1') {
      ?>
					  		<td align="right"><font color="#005b90">Calculate</font> </td>	
				<td> 	  		
					  		
              		<input type="submit" name="amount" value="Amount" onClick="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend';">
              		<input type="submit" name="local" value="Local Amount" onClick="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend';">
              	
              	</td>
       <?	} else { ?>
					  		<td align="right">&nbsp;</td>	
								<td>&nbsp;</td>
      <?	} ?>
              	<?
              	}
              	?>
              	<!--td> 
              		<input type="button" name="amount" value="Amount" onClick="window.location='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>';">
              		<input type="button" name="local" value="Local Amount" onClick="window.location='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>';">
              	</td-->
              </tr>	
                      	<td width="128" height="20" align="right">
                      		<font color="#005b90">Transaction Purpose<font color="#ff0000"><? if(CONFIG_NON_COMP_TRANS_PURPOSE != "1"){ ?>* <? } ?></font></font></td>
                        <td width="195" height="20"><select  name="transactionPurpose" onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();">
                            <option value="">-select one-</option>
                            <option value="Business">Business</option>
                            <option value="Family Assistant"> Family Assistance</option>
                            <option value="Help">Help</option>
														<option value="Other">Other</option>
                          </select>
					<script language="JavaScript">
         	SelectOption(document.addTrans.transactionPurpose, "<?=$_SESSION["transactionPurpose"]?>");
                                </script></td>
                        <td width="140" height="20" align="right"><font color="#005b90">Total Amount<? if($fromTotalConfig == '1'){echo("<font color='#ff0000'>*</font>");} ?></font></td>
                   
                        <td width="241" height="20"><input type="text" name="totalAmount" id="totalAmountID" value="<?
                                if(CONFIG_TRANS_ROUND_NUMBER == 1  && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    echo ($totalAmount > 0 ? RoundVal($totalAmount) : "");
                                }else{
                                    echo ($totalAmount > 0 ? round($totalAmount, $roundLevel) : "");
                                }
                             ?>" onChange="disableClicks();" maxlength="25" <? if($fromTotalConfig != '1'){echo("readonly");} ?>>
                             
                             <? 
                if($fromTotalConfig == '1')
                {
                ?>     
                 <input type="submit" name="fromTotal" value="Calculate" onClick="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend';">    
                 <?
                }
                ?>
                             </td>
                        
                             
                      </tr>
					   <? if($_SESSION["transType"]=="Bank Transfer" && CONFIG_REMOVE_ADMIN_CHARGES != "1")
			  {?>
					  <tr>
					  <td align="right"><font color="#005b90"> Admin Charges </font></td>
					  <td><input type="text" name="bankCharges" id="bankChargesID" value="<?=$_SESSION["bankCharges"]?>" maxlength="25" readonly></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  
					  </tr>
					  <? }else{ 
					  	$_SESSION["bankCharges"]= 0;
					  	?>
					  <input type="hidden" name="bankCharges" id="bankChargesID" value="<?=$_SESSION["bankCharges"]?>" maxlength="25">
					  <?
					}
					  ?>
						<? if (CONFIG_CASH_PAID_CHARGES_ENABLED && $_SESSION["moneyPaid"]=='By Cash')
			  {?>
					  <tr>
					  <td align="right"><font color="#005b90"> Cash Handling Charges </font></td>
					  <td><input type="text" name="cashHandling" value="<? echo CONFIG_CASH_PAID_CHARGES;?>" maxlength="25" readonly></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  
					  </tr>
					  <? } ?>
					  <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <tr>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td><input type="text" name="currencyCharge" id="currencyChargeID" value="<?=$_SESSION["currencyCharge"]?>" maxlength="25" readonly></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  
					  </tr>
					  <? } ?>
	                      <tr>
                        <td width="128" height="20" align="right">&nbsp;</td>
              <td width="195" height="20">&nbsp;
              	</td>
                   <? if(CONFIG_REMOVE_FUNDS_SOURCES != "1")
                   {?>             
                        <td width="96" align="right"><font color="#005b90">Funds Sources<? echo (CONFIG_NONCOMPUL_FUNDSOURCES != '1' ? "<font color='#ff0000'>*</font>" : "") ?></font></td>
						<td width="241" height="20"><select name="fundSources">
                            <option value="">-select one-</option>
                            <option value="Salary">Salary</option>
                            <option value="Savings">Savings</option>
                            <option value="Loan">Loan</option>
                          </select>
                            <script language="JavaScript">
         	SelectOption(document.addTrans.fundSources, "<?=$_SESSION["fundSources"]?>");
                                </script></td>
                        <!-- Changed by Kashi 11_Nov   td width="241"><input type="text" name="fundSources" value="<?=$_SESSION["fundSources"]; ?>" maxlength="100">
                        </td-->
                   <? }else{?>
                   <td>&nbsp;</td>
                  <? }?> 
                   
                       
                      </tr>
					  <? if($_SESSION["transactionPurpose"] == "Other")
			  {?>
					  <tr>
					    <td><div align="right"><font color="#005b90">Other Purpose&nbsp;</font></div></td>
					    <td><span class="style1">
					      <input type="text" name="other_pur" maxlength="32" value="<?=$_SESSION["other_pur"]?>">
					    </span></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					  <? }
					  
			if(IS_BRANCH_MANAGER){		  
			if($userDetails["isMain"] == "Y" || $agentType == "Admin Manager") {
			 ?>				  
            <tr>
            	<td width="150" height="20" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?>  </font> </td>
            	<td width="200" height="20"><input name="tip" type="text" id="tip" value="<?=$_SESSION["tip"]; ?>"></td>
            	<? if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <td width="140" height="20" align="right"><font color="#005b90">Fee Discount vvdd</font></td>
				<? if ($_SESSION["chDiscount"]!=""){$ch= "checked";}?>
				<td width="241" height="20" class="style1"><input type="checkbox" name="chDiscount" <? echo $ch; ?>   onClick="<? if($_SESSION["chDiscount"]!=""){$_SESSION["chDiscount"]="";}?>return shorProvinces()" onBlur="<? if($_SESSION["chDiscount"]==""){?>document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();<? }?>"  >
				<? } ?>
				</td>
           </tr>
                                
            <? }
            
          	else
          	{ ?>
          		<tr>
            	<td width="150" height="20" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?>  </font> </td>
            	<td width="200" height="20"><input name="tip" type="text" id="tip" value="<?=$_SESSION["tip"]; ?>"></td>
            	<? if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <td width="96" height="20" align="right"><font color="#005b90"><b>Fee Discount Request sdse</b></font></td>
				<td width="241" height="20" class="style1"><input type="text" name="discount_request" value="<? echo $_SESSION["discount_request"]; ?>"></td>
			<? } ?>
           </tr>
        <? }} else
        { if(CONFIG_DISCOUNT_REQUEST != '1')
        	{
        	?>
         	 <tr>
            	<td width="150" height="20" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?>  </font> </td>
            	<td width="200" height="20"><input name="tip" type="text" id="tip" value="<?=$_SESSION["tip"]; ?>"></td>
            	<? if (!(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1")){ ?>
                <td width="96" height="20" align="right"><font color="#005b90">Fee Discount</font></td>
				<? if ($_SESSION["chDiscount"]!=""){$ch= "checked";}?>
				<td width="241" height="20" class="style1"><input type="checkbox" name="chDiscount" <? echo $ch; ?>   onClick="<? if($_SESSION["chDiscount"]!=""){$_SESSION["chDiscount"]="";}?>return shorProvinces()" onBlur="<? if($_SESSION["chDiscount"]==""){?>document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();<? }?>"  >
				<? } ?>
				</td>
           </tr>
        <? }elseif(CONFIG_DISCOUNT_REQUEST == '1')
        {
        	?>
        	<tr>
            	<td width="150" height="20" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?>  </font> </td>
            	<td width="200" height="20"><input name="tip" type="text" id="tip" value="<?=$_SESSION["tip"]; ?>"></td>
            	<td width="96" height="20" align="right"><font color="#005b90"><b>Fee Discount Request ccca</b></font></td>
				<td width="241" height="20" class="style1"><input type="checkbox" name="discount_request" value="<? 
					if($_SESSION['discount_request'] > 0)
					{
						echo($_SESSION['discount_request']);
						}else{
                               if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                    echo ($imFeetemp > 0 ? RoundVal($imFeetemp) : RoundVal($imFee));
                                }else{
                                    echo ($imFeetemp > 0 ? round($imFeetemp, $roundLevel) : round($imFee, $roundLevel));
                                }
                            }
                                 ?>" <? echo ($_SESSION['discount_request'] > 0 ? "checked" : "") ?>
                                 <?
					if(CONFIG_TOTAL_FEE_DISCOUNT == '1')
					 {// && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType))discount_request
					 	?>
					 onChange="document.addTrans.action='add-transaction_opaldesign.php?transID=<? echo $_GET["transID"]?>&focusID=transSend'; document.addTrans.submit();"	
					 	
					<?
					}
					?>></td>
			</tr>
        	<?
        	}
         }
        
            
            if(CONFIG_SECRET_QUEST_ENABLED)
            {	?>
             <tr>
              <td width="150" height="20" align="right"><font color="#005b90"> Secret Question</font> </td>
              <td width="200" height="20"><input name="question" type="text" id="question" value="<?=$_SESSION["question"]; ?>" ></td>
             	<td width="150" height="20" align="right"><font color="#005b90">Answer</font></td>
              <td width="200" height="20"><input name="answer" type="text" id="answer" value="<?=$_SESSION["answer"]; ?>"></td>
            </tr>
            
            
            <?          
          	}?>
          	            
        	  
       		
       		   <?
					  if(CONFIG_INTERNAL_REMARKS == '1')
					  {
					  ?>
					  <tr>
					  	<td align="right"><font color="#005b90"> <? echo(CONFIG_LABEL_INTERNAL_REMARKS != '' ? CONFIG_LABEL_INTERNAL_REMARKS : "Internal Remarks"); ?> </font></td> 
				    	<td><input type="text" name="internalRemarks"  type="text" id="internalRemarks" value="<?=$_SESSION["internalRemarks"]; ?>"></td> 
				    	<td>&nbsp;</td>
					  	<td>&nbsp;</td>			  
					  </tr>
					  <?
						}
					  ?>		
					  
					   <?
					  if(CONFIG_IDTYPE_PASSWORD == '1' && $benificiaryContent["otherId"]!= "")
					  {
					  ?>
					  <tr>
					  	<td align="right"><font color="#005b90"> Password </font></td> 
				    	<td><input type="text" name="benIdPassword"  type="text" id="benIdPassword" value="<?=$_SESSION["benIdPassword"]; ?>"></td> 
				    	<td>&nbsp;</td>
					  	<td>&nbsp;</td>			  
					  </tr>
					  <?
						}
					  ?>		
                      <tr>
                      <?	if (CONFIG_TnC_CHECKBOX_OFF == "1") {  ?>
                      	<td width="128" height="20" align="right" valign="top"><input type="hidden" name="Declaration" value="Y"></td>
                      	<td height="20" colspan="3">&nbsp;</td>
                      <?	} else {  ?>
                        <td width="128" height="20" align="right" valign="top"><input type="checkbox" name="Declaration" value="Y"></td>
                        <td height="20" colspan="3">
                        	<? if(CONFIG_TRANS_CONDITION_ENABLED == '1')
                        	{
                        		echo(CONFIG_TRANS_COND);
                        		//echo(CONFIG_CONDITIONS.defined('CONFIG_CONDITIONS'));
                        		?>
                     	<?	if (defined('CONFIG_CONDITIONS')) {  ?>
                        		<a href="#" onClick="javascript:window.open('<?=CONFIG_CONDITIONS?>', 'Conditions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" class="style2">Full Terms and Conditions</a>
                     	<?	}  ?>
                        		<? }else{
                        	 ?>
                        	I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
                        	<? } ?>
                        	</td>
                      <?	}  ?>
                      </tr>
             
                     <tr>
                        <td width="128" height="20" align="right" valign="top">&nbsp;</td>
                        <td height="20" colspan="3" align="center"><input type="hidden" name="transID" value="<? echo $_GET["transID"]?>">
                            <? If (CONFIG_SEND_ONLY_BUTTON == 1){?>
                          	<input type="button" value="Send & Print" name="transSend" id="transSend" onClick="<? if(CONFIG_COMPLIANCE_PROMPT == '1'/* && $cumulativeRule["ruleID"]!=''*/){ ?>checkForm(addTrans,'Yes'); <? }else{?> document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=transSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                           
                           <? } ?>
                           
                           <? if(CONFIG_SEND_ONLY_BUTTON == 1){?>
                          	<input  type="button" name="save" value=" Send " id="sendToConfirmID" onClick="<? if(CONFIG_COMPLIANCE_PROMPT == '1'/* && $cumulativeRule["ruleID"]!=''*/){ ?>checkForm(addTrans,'No');<? }else{?> document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=noSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                           <? }elseif (CONFIG_CUSTOM_RECIEPT == '1'){ ?>
                          	<input  type="button" name="save" value="Send" id="sendID"  onclick="<? if(CONFIG_COMPLIANCE_PROMPT == '1' /*&& $cumulativeRule["ruleID"]!=''*/){ ?>checkForm(addTrans,'No');<? }else{?>document.addTrans.action='<? echo CONFIG_RECIEPT_NAME ?>?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=noSend'; document.addTrans.submit();<? } ?>">&nbsp;&nbsp;
                           <? }else{ ?>
                           	<input  type="button" name="save" value="Send" id="sendID"  onclick="<? if(CONFIG_COMPLIANCE_PROMPT == '1' /*&& $cumulativeRule["ruleID"]!=''*/){ ?>checkForm(addTrans,'No');<? }else{?> document.addTrans.action='confirm-transaction.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend=noSend'; document.addTrans.submit();<? }?>">&nbsp;&nbsp;
                           <? } ?>
              							<input  type="button" name="reset" value=" Clear " onClick="cleanForm();" ></td>
                      </tr>
                      <tr>
                        <td height="20" align="right" valign="top">&nbsp;</td>
                        <td height="20" colspan="3" align="center">&nbsp;</td>
                      </tr>
                      <tr>
                        <td height="20" align="right" valign="top">&nbsp;</td>
                        <td height="20" colspan="3" align="center"><input type="hidden" name="act" value="<?=$act?>">&nbsp;</td>
                      </tr>
                    </table>
                  </fieldset></td>
                </tr>
                <?
		  }
//}
?>
              </form>
            </table></td>
          </tr>
        </table></td>
    </tr>
</table>
</body>
</html>
