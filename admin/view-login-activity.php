<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$benAgentID =  $_GET["benAgentID"];
$dated =  $_GET["dated"];
$agentID =  $_GET["agentID"];
//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if($_GET["viewHistory"]!="")
{
	$_SESSION["viewHistory"] =  $_GET["viewHistory"];
}


$query2 = "select * from " . TBL_LOGIN_ACTIVITIES . " where login_history_id='".$_SESSION["viewHistory"]."'";	
//debug($query2);
	
	$contentsHistory = selectMultiRecords($query2);
	
?>
<html>
<head>
<title>View Login Activities</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript" src="jquery.js"></script>
<script language="javascript">
	$(document).ready(function(){
		<? for($count=0;$count<count($contentsHistory);$count++){?>
		$("#showHideActivitiesDetailBtn_<?=$count?>").click(function () {
		 
			if($("#showHideActivitiesDetailBtn_<?=$count?>").html() == "[-]")
			{
				$("#activitiesDetails_<?=$count?>").hide();
				$("#showHideActivitiesDetailBtn_<?=$count?>").html("[+]");
			}
			else
			{
				$("#activitiesDetails_<?=$count?>").show();
				$("#showHideActivitiesDetailBtn_<?=$count?>").html("[-]");
			}
		});
		<? }?>
	});
</script>
<style type="text/css">
<!--
.style1 {
	font-weight: bold;
	font:Verdana, Arial, Helvetica, sans-serif;
}

.style2 {
	color: #6699CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">Login Activities</font></b></td>
  </tr>
  <tr> 
    <td align="center"> 
    	<font size="10">
    		<table width="800" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
	        <tr class="style2"> 
						<td>Dated and Time</td>
	          <td>Action</td>
	          <td>Description</td>
	          <td align="center">Show/Hide<br/>Details</td>
	        </tr>
			<? for($k = 0;$k < count($contentsHistory); $k++){ ?>      
				<tr class="style1"> 
					<td><?=$contentsHistory[$k]["activity_time"]?></td>
					<td><?=$contentsHistory[$k]["activity"]?></td>
					<td><?=$contentsHistory[$k]["description"]?></td>
					<td align="center"><a href="javascript:void(0)" id="showHideActivitiesDetailBtn_<?=$k?>" style="font-size:12px;color:#000099; font-weight:bold" title="Show Hide Activities Details">[+]</a></td>
				</tr>
				<tr class="style1" id="activitiesDetails_<?=$k?>" style="display:none; background-color:#CCCCCC;"> 
					<td colspan="4">
							<table width="98%" cellpadding="2" cellspacing="0" align="center">
							<?php
								
									$strSearchSql = "
													select 
														t.refNumber,
														t.refNumberIM,
														h.change_log,
														h.userid,
														h.datetime
													from
														transactions as t,
														transaction_modify_history as h
													where
														t.transID = h.transid and 
														t.transID='".$contentsHistory[$k]["action_for"]."'
													";
													
									$arrSearchData = selectMultiRecords($strSearchSql);
									$arrDispalyLogData = array();
							$transDesc = strtoupper(trim($contentsHistory[$k]["description"]));
							if($transDesc=="TRANSACTION  IS UPDATED")
							{
								for($i=0 ; $i <count($arrSearchData); $i++)
								{
									if(!empty($arrSearchData[$i]["change_log"]))
									{
										
										$arrDispalyLogData[$arrSearchData[$i]["datetime"]]["userid"] = $arrSearchData[$i]["userid"];
										
										$arrChangeLog = explode("|",$arrSearchData[$i]["change_log"]);	
										//debug($arrChangeLog);
										
										foreach($arrChangeLog as $strLog)
										{
											if(!empty($strLog))
											{
												$arrDetailLog = explode(":", $strLog);
												
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["field"] = $arrDetailLog[0];
												
												$arrDetailLogValues = explode("!", $arrDetailLog[1]);
												
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["lastValue"] = $arrDetailLogValues[0];
												$arrDispalyLogData[$arrSearchData[$i]["datetime"]][$arrDetailLog[0]]["newValue"] = $arrDetailLogValues[1];
												
											}
										}
										
									}
								}
								
								//debug($arrDispalyLogData);
								
								/**
								 * Modify the array data, such that to store the IDs real name value
								 */
								 foreach($arrDispalyLogData as $dateTime => $arrDetailData)
								 {
									if(!empty($arrDetailData["userid"]))
									{
										$arrUserData = selectFrom("select userid, username, name, adminType from admin where userid='".$arrDetailData["userid"]."'");
										$arrDispalyLogData[$dateTime]["userid"] = $arrUserData;
									}
								 }
								 //debug($arrDispalyLogData);

								 if(count($arrSearchData) > 0)
								 {
									foreach($arrDispalyLogData as $dateTime => $arrDetailData)
									{
										$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($dateTime))." GMT";
										foreach($arrDetailData as $detailKey => $detailVal)
										{
											//$bgColor = '#cecece';
											if($bgColor == 'lightcyan')
												$bgColor = '#cecaff';
											else
												$bgColor = 'lightcyan';
											
											if($detailKey != "userid")
											{
								?>
										<tr>
											<td class="transactions" width="100%" bgcolor="<?=$bgColor?>">
												<?=$strFormatedDateTime?>&nbsp;
							
												<b><?=$arrDetailData["userid"]["name"]." / ".$arrDetailData["userid"]["username"]." / ".$arrDetailData["userid"]["adminType"]?></b>&nbsp;
												modified &nbsp;
							
												<b><?=$detailVal["field"]?></b> from
							
												<b><?=(empty($detailVal["lastValue"])?" No Value ":$detailVal["lastValue"])?></b> to
												<b><?=(empty($detailVal["newValue"])?" No Value ":$detailVal["newValue"])?></b>
											</td>
										</tr>
								<?
											}
										}
									}
								}
							}
							elseif($transDesc=="TRANSACTION  IS ADDED" || $transDesc=="TRANSACTION IS AUTHORIZED" || $transDesc=="TRANSACTION IS CANCELLED"  || $transDesc=="TRANSACTION IS AWAITING CANCELLATION")
							{
								if($transDesc=="TRANSACTION  IS ADDED"){
									$userType="addedBy";
								}
								elseif($transDesc=="TRANSACTION IS AUTHORIZED"){
									$userType="authorisedBy";								
								}
								elseif($transDesc=="TRANSACTION IS CANCELLED" || $transDesc=="TRANSACTION IS AWAITING CANCELLATION"){
									$userType="cancelledBy";								
								}		
								$strSearchSql = "
												select 
													t.refNumber,
													t.refNumberIM,
													t.addedBy,
													t.authorisedBy,
													t.transDate,
													t.authoriseDate,
													t.cancelDate,
													a.name,
													a.username,
													a.adminType
												from
													transactions as t,
													admin as a
												where
													t.".$userType." = a.name
												and 
													t.transID='".$contentsHistory[$k]["action_for"]."'
												";
												
								$arrSearchData = selectFrom($strSearchSql);
								$bgColor = '#cecaff';
								if($transDesc=="TRANSACTION  IS ADDED"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["transDate"]))." GMT";
									$stFormateAction = "has Created Transaction";
								}
								elseif($transDesc=="TRANSACTION IS AUTHORIZED"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["authoriseDate"]))." GMT";
									$stFormateAction = "has Authorized Transaction";
								}
								elseif($transDesc=="TRANSACTION IS CANCELLED" || $transDesc=="TRANSACTION IS AWAITING CANCELLATION"){
									$strFormatedDateTime = date("l jS M, Y H:i:s",strtotime($arrSearchData["cancelDate"]))." GMT";
									$stFormateAction = "has Cancelled Transaction";
								}
								if($arrSearchData["refNumberIM"]!="")
								{
							?>
							<tr>
								<td class="transactions" width="100%" bgcolor="<?=$bgColor?>">
									<?=$strFormatedDateTime?>&nbsp;
				
									<b><?=$arrSearchData["name"]." / ".$arrSearchData["username"]." / ".$arrSearchData["adminType"]?></b>&nbsp;
									 <?=$stFormateAction?> 
									<?=($arrSearchData["refNumberIM"]!=""?"<i>Transaction Code</i> -> <strong>".$arrSearchData["refNumberIM"]." </strong>":"")?>
									<?=($arrSearchData["refNumber"]!=""?"<i>Manual Code</i> -> <strong>".$arrSearchData["refNumberIM"]."</strong>":"")?>
								</td>
							</tr>
								<?
								}
					
							}
							else{
							?>
							<tr>
								<td class="transactions" width="100%" bgcolor="<?=$bgColor?>" colspan="4">No Details Specified in System</td>
							</tr>
						<?	}
						?>						
						</table>
					</td>
				</tr>
		  	<? }?>
      	</table>
      </font>
    </td>
  </tr>
  <tr>
    <td align="center">
	<div align="center">
	<form name="form1" method="post" action="">                  
		<input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
	</form>
	</div>
	</td>
  </tr>
</table>
</body>
</html>