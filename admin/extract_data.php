<?php
include("dbRender.php");

	header ("Content-type: application/x-msexcel"); 
	header ("Content-Disposition: attachment; filename=testfile.xls" ); 
	header ("Content-Description: PHP/INTERBASE Generated Data" );
	
$input_array = array(refNumber => 'Reference', 
refNumberIM => 'Bayba Code', 
transDate => 'Date Transaction Created',
Customer => 'Customer',
Beneficiary => 'Beneficiary',
transAmount => 'Amount Sent', 
IMFee => 'Transaction Fee',
totalAmount => 'Net Amount',
localAmount => 'Foreign Amount',
Pick_Up_Location => 'Pick Up Location', 
IMFee => 'Transaction Fee',
transStatus => 'Status',
deliveryDate => 'Pick Up Date',
);


$query = 'SELECT transactions.refNumber,
transactions.refNumberIM,
transactions.transDate,
CONCAT(customer.Title," ",customer.firstName," ", customer.middleName," ",customer.lastName) AS Customer,
CONCAT(beneficiary.Title," ",beneficiary.firstName," ",beneficiary.middleName," ",beneficiary.lastName) AS Beneficiary,
transactions.transAmount,
transactions.IMFee,
transactions.totalAmount,
transactions.localAmount,
cm_collection_point.cp_branch_address AS "Pick_Up_Location",
transactions.transStatus,
transactions.deliveryDate
 FROM 
transactions LEFT JOIN (customer, beneficiary, cm_collection_point) ON (transactions.customerID=customer.customerID AND transactions.benID=beneficiary.benID AND transactions.collectionPointID=cm_collection_point.cp_id) ORDER BY transactions.refNumberIM';
	
echo Grid($query,$input_array);
?>