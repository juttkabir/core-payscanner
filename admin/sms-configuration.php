<?php
    session_start();
    include "../include/config.php";
    include "security.php";
    $userID  = $_SESSION["loggedUserData"]["userID"];
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SMS Configuration</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script language="javascript" src="./javascript/functions.js"></script>
    <!-- Include the above in your HEAD tag -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="content/stylesheets/application.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery.fancybox-1.2.6.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/new-agent.css" />
    <link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
</head>
<body style="padding:15px;">

<div class="alert introducer">SMS Configuration</div>
<form action="sms-configuration-conf.php" method="post">
<div class="row">
    <div class="col col-2" style="margin-right: -100px;">
        <strong>Select Agent:&nbsp;&nbsp;&nbsp;</strong>
    </div>
    <div class="col col-10">
        <?php
            $where_sql="";
            if($_SESSION["loggedUserData"]['adminType']=='Agent') {
                $where_sql="   AND agentID='".	$userID."' ";
            }
            $agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' $where_sql";
            if($_SESSION["loggedUserData"]['adminType']!='Agent') {
                $agentQuery .= "order by agentCompany";
                $agents = selectMultiRecords($agentQuery);
                echo '<select name="agent">
                        <option value="0">- Select Agent -</option>';
                for ($i=0; $i < count($agents); $i++) {
                    echo '<option value="'.$agents[$i]["userID"].'">'.$agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]".'</option>';
                }
                echo '</select>';
            } else
                echo '<input type="hidden" name="agentName" id="agentName" value="' . $userID . '">';
        ?>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col col-2" style="margin-right: -100px;">
        <strong>To <font color="#FF0000">*</font>:&nbsp;&nbsp;&nbsp;</strong>
    </div>
    <div class="col col-10">
        <select name="beneficiary" id="beneficiary" onchange="disableAlert('beneficiaryAlert');">
            <option value="">- Select Sender/Beneficiary -</option>
            <?php
                $cashierType =  "select userID,name,username,adminType,agentType,isCorrespondent from ".TBL_ADMIN_USERS." where adminType IN ('Supper','call','admin','COLLECTOR','Branch Manager','Admin Manager','MLRO'".((CONFIG_FORUM_UPDATION_BETWEEN_AGENTS == "1" && $_SESSION["loggedUserData"]["adminType"]!="Agent")  ?",'Agent','SUPI Manager'":"").",'Support') ";
                //debug($cashierType);
                $x = 0;
                $cashierTypeQuery= selectMultiRecords($cashierType);
                //debug($cashierTypeQuery);
                for($k = 0; $k < count($cashierTypeQuery); $k++)
                {
                    $cashierAdminType[$k] = $cashierTypeQuery[$k]['adminType'];
                    $cashierUsername[$k]= $cashierTypeQuery[$k]['name'];
                    $cashierName[$k]= $cashierTypeQuery[$k]['username'];
                    $cashierID[$k]= $cashierTypeQuery[$k]['userID'];
                    $cashierAgentType[$k]= $cashierTypeQuery[$k]['agentType'];
                    $cashierIsCorrespondent[$k]= $cashierTypeQuery[$k]['isCorrespondent'];
                    $userTbl[$k]= 'admin';

                    $x++;

                }

                for($ab = 0; $ab < $x ; $ab++) {
                    $maskingVal = maskingOfAdminTypeValues($cashierAdminType[$ab], $cashierAgentType[$ab], $cashierIsCorrespondent[$ab]);
                    echo '<option value="'.$cashierID[$ab].'"'.(!empty($_POST["toUsers"]) && in_array($cashierUsername[$ab]["name"],$_POST["toUsers"]) ? "selected"  : "").'>'.$maskingVal . " [" . $cashierUsername[$ab] . "]".'</option>';
                }
            ?>
        </select>&nbsp;&nbsp;
        <font color="#FF0000" size="small" id="beneficiaryAlert" style="display: none;">This is a compulsory field</font>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col col-2" style="margin-right: -100px;">
        <strong>Select Event <font color="#FF0000">*</font>:&nbsp;&nbsp;&nbsp;</strong>
    </div>
    <div class="col col-10">
        <select name="event" id="event" onchange="disableAlert('eventAlert');">
            <option value="">- Select Event -</option>
            <?php
                $strQueryEvents = "SELECT DISTINCT(name) AS name, id FROM " . TBL_EVENTS;
                $arrEvents = selectMultiRecords($strQueryEvents);
                for ($x = 0; $x < count($arrEvents); $x++) {
                    echo "<option value='" . $arrEvents[$x]['id'] . "'>" . $arrEvents[$x]['name'] . "</option>";
                }
            ?>
        </select>
        &nbsp;&nbsp;
        <font color="#FF0000" size="small" id="eventAlert" style="display: none;">This is a compulsory field</font>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col col-2" style="margin-right: -100px;">
        <strong>Select API <font color="#FF0000">*</font>:&nbsp;&nbsp;&nbsp;</strong>
    </div>
    <div class="col col-10">
        <select name="API" id="API" onchange="disableAlert('APIAlert');">
            <option value="">- Select API -</option>
            <option>Twillio</option>
        </select>
        &nbsp;&nbsp;
        <font color="#FF0000" size="small" id="APIAlert" style="display: none;">This is a compulsory field</font>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col col-1">
        <strong>Message <font color="#FF0000">*</font>:&nbsp;&nbsp;&nbsp;</strong>
    </div>
    <div>
        <textarea name="message" id="message" cols="80" rows="10" style="border: 1px solid #E0E0E0; min-height: 200px" onkeypress="disableAlert('messageAlert')"></textarea>&nbsp;&nbsp;
        <font color="#FF0000" size="small" id="messageAlert" style="display: none;">This is a compulsory field</font>
    </div>
</div><br/>
    <div class="col col-3">
        <input class="btn btn-sm btn-primary btn-block" type="submit" value="SAVE" onclick="return checkCompulsoryFields();">
    </div>
</form>
<script type="text/javascript">
    function checkCompulsoryFields() {
        if (document.getElementById("beneficiary").value == "" || document.getElementById("event").value == "" || document.getElementById("API").value == "") {
            document.getElementById("beneficiaryAlert").style.display = "inline";
            document.getElementById("eventAlert").style.display = "inline";
            document.getElementById("APIAlert").style.display = "inline";
            document.getElementById("messageAlert").style.display = "inline";
            return false;
        }
        else
            return true;
    }
    function disableAlert(id) {
        document.getElementById(id).style.display = "none";
    }
</script>
</body>
</html>