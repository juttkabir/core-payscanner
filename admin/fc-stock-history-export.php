<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$userName  = $_SESSION["loggedUserData"]["name"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$date_time = date('d-m-Y  h:i:s A');
	
	/*** 
	* Fireign Currency Stock History Export
	* .xls format only
	* AMB: #9363 
	* Created by Kalim ul Haq
	* Jan 18, 2012
	***/
	
	// Get URL Variables
	$intForeignCurrID	=	$_REQUEST["currencyID"];
	$from_date			=	$_REQUEST["from_date"];
	$to_date			=	$_REQUEST["to_date"];
	
	/*** Fitch date from DB according Filters to populate Export *** Start ***/
	$stockHistoryQuery	= "	SELECT 
								id,
								currency,
								amount,
								used_amount,
								available_amount,
								buying_rate,
								purshased_from,
								status,
								created_date,
								closing_date
							FROM 
								".TBL_CURRENCY_STOCK_HISTORY."	
							WHERE
								currency	= '".$intForeignCurrID."'";
						
	if(!empty($from_date))
	{
		$date = explode("/",$from_date);
		$stockHistoryQuery .= " AND created_date >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	
	if(!empty($to_date))
	{
		$date = explode("/",$to_date);
		$stockHistoryQuery .= " AND created_date <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}

	$stockHistoryData = selectMultiRecords($stockHistoryQuery);
	/***	Data Fitching End	***/
	
	$exportType = $_REQUEST["exportType"];

	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	
	if($exportType == "XLS")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=FC_Stock_History_Export.xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
		$strMainStart   		= "<table border='1'>";
		$strMainEnd     		= "</table>";
		$strRowStart    		= "<tr>";
		$strRowEnd      		= "</tr>";
		$strColumnStart 		= "<td>";
		$strRightAlignColStart	= "<td align='right'>";
		$strColumnEnd   		= "</td>";
		$strBoldStart 			= "<b>";
		$strBoldClose 			= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=FC_Stock_History_Export.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   		= "";
		$strMainEnd     		= "";
		$strRowStart    		= "";
		$strRightAlignColStart	= "";
		$strRowEnd      		= "\r\n";
		$strColumnStart 		= "";
		$strColumnEnd   		= ",";

	}
	else
	{
		$strMainStart   		= "<table align='center' border='1' width='100%'>";
		$strMainEnd     		= "</table>";
		$strRowStart    		= "<tr>";
		$strRowEnd     			= "</tr>";
		$strColumnStart 		= "<td>";
		$strRightAlignColStart	= "<td align='right'>";
		$strColumnEnd   		= "</td>";
		$strBoldStart 			= "<b>";
		$strBoldClose 			= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Currency Exchange Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Batch ID".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Date IN".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Used Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Available Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Buying Rate".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Purchased From".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Stock Closing Date".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	foreach($stockHistoryData as $key => $val)
	{	
		// To not Show 0000-00-00 date if stock is not closed
		if($val["status"] != 'Closed')
			$strClosingDate	= "";
		else
			$strClosingDate	= $val["closing_date"];
		
		// Amounts
		$fltAmount			= $val["amount"];
		$fltUsedAmount		= $val["used_amount"];
		$fltAvailableAmount	= $val["available_amount"];
		$fltBuyingRate		= $val["buying_rate"];
		
		// Get Name of Customer from whome Stock Purchased
		$strPurshasedFrom	= "";
		$arrPurshasedFrom	= selectFrom("SELECT firstName, middleName, lastName FROM customer WHERE customerID='".$val['purshased_from']."'");
		$strPurshasedFrom	= $arrPurshasedFrom["firstName"]." ".$arrPurshasedFrom["middleName"]." ".$arrPurshasedFrom["lastName"];	
		
		$strFullHtml .= $strRowStart;	
		$strFullHtml .= $strColumnStart.$val["id"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["created_date"].$strColumnEnd;
		
		$strFullHtml .= $strRightAlignColStart.$fltAmount.$strColumnEnd;
		$strFullHtml .= $strRightAlignColStart.$fltUsedAmount.$strColumnEnd;
		$strFullHtml .= $strRightAlignColStart.$fltAvailableAmount.$strColumnEnd;
		$strFullHtml .= $strRightAlignColStart.$fltBuyingRate.$strColumnEnd;
		
		$strFullHtml .= $strColumnStart.$strPurshasedFrom.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["status"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strClosingDate.$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		
	}
	
	$strFullHtml .= $strMainEnd ;
	
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>