<?
session_start();
include ("../include/config.php");
include ("security.php");
$date_time = date('d-m-Y  h:i:s A');
$agentType2 = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

/*	#5441 - AMB Exchange
	Operator ID is added in AnD form.
	to be displayed in Western Union Output export file.
	by Aslam Shahid
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1"){
	$operatorFlag = true;
}
/*	#4966 
	Company Type dropdown added on agents form and Trading field also added.
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
/*	#4966  point 2
	Upload document functionality.
	by Aslam Shahid
*/
if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1"){
	if($_REQUEST["userID"]!="" && $_REQUEST["page"] == "uploadDocument"){
	$userQuery = selectFrom("select userID,username,isCorrespondent,adminType,agentType,agentType,password from admin
	where userID = '".$_REQUEST["userID"]."'");
	
	 $userName = $userQuery["username"];
	 $password = $userQuery["password"];
	 $isCorrespondent = $userQuery["isCorrespondent"];
	 $agentType = $userQuery["agentType"];
	 $adminType = $userQuery["agentType"];
	
		 if($isCorrespondent == "N" && $agentType == "Supper" && $_REQUEST["mode"] =="Add"){
			insertError(AG16." The login name is <b>$userName</b> and password is <b>$password</b>");
		 }elseif($isCorrespondent == "ONLY" && $agentType == "Supper" && $_REQUEST["mode"] =="Add"){
		 insertError("Super Distributor added successfully.The login name is <b>$userName</b> and password is <b>$password</b>");
		 }elseif($_REQUEST["mode"] =="update"){
			insertError("Record Updated Successfully");	
		 }
	}
}
session_register("agentCompany");
session_register("companyType");
session_register("agentContactPerson");
session_register("agentAddress");
session_register("agentAddress2");
session_register("agentCountry");
session_register("agentCity");
session_register("agentZip");
session_register("agentPhone");
session_register("agentFax");
session_register("email");
session_register("agentURL");
session_register("agentMSBNumber");
session_register("msbMonth");
session_register("msbDay");
session_register("msbYear");
session_register("agentCompRegNumber");
session_register("agentCompDirector");
session_register("agentDirectorAdd");
session_register("agentProofID");
session_register("otherProofID");
session_register("idMonth");
session_register("idDay");
session_register("idYear");
session_register("agentDocumentProvided");
session_register("agentBank");
session_register("agentAccountName");
session_register("agentAccounNumber");
session_register("agentBranchCode");
session_register("agentBranchName");  //Add by hasan for monex and created a filed in admin table as agentBranchName
session_register("agentAccountType");
session_register("agentCurrency");
session_register("agentAccountLimit");
session_register("agentCommission");
session_register("CommissionAnD");
session_register("commPackageAnD");
session_register("agentStatus");
session_register("tradingName");
session_register("uploadImage");
if($operatorFlag)
	session_register("OperatorID");

if($_GET["type"] == "sub" && $_POST["supAgentID"] != "" && $_GET["f"] == "Y")
{
	//$supAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_POST["supAgentID"]."'");
	//echo "select * from ".TBL_ADMIN_USERS." where userID='".$_POST["supAgentID"]."'";
	$_SESSION["supAgentID"] = $_POST["supAgentID"];
	$_SESSION["agentCompany"] = $supAgentDetails["agentCompany"];
		$_SESSION["agentContactPerson"] = $supAgentDetails["agentContactPerson"];
		$_SESSION["agentAddress"] = $supAgentDetails["agentAddress"];
		$_SESSION["agentAddress2"] = $supAgentDetails["agentAddress2"];
		$_SESSION["Country"] = $supAgentDetails["agentCountry"];
		$_SESSION["City"] = $supAgentDetails["agentCity"];
		$_SESSION["agentZip"] = $supAgentDetails["postcode"];
		$_SESSION["agentPhone"] = $supAgentDetails["agentPhone"];
		$_SESSION["agentFax"] = $supAgentDetails["agentFax"];
		$_SESSION["email"] = $supAgentDetails["email"];
		$_SESSION["agentURL"] = $supAgentDetails["agentURL"];
		$_SESSION["agentMSBNumber"] = $supAgentDetails["agentMSBNumber"];
		$_SESSION["msbMonth"] = $supAgentDetails["msbMonth"];
		$_SESSION["msbDay"] = $supAgentDetails["msbDay"];
		$_SESSION["msbYear"] = $supAgentDetails["msbYear"];
		$_SESSION["agentCompRegNumber"] = $supAgentDetails["agentCompRegNumber"];
		$_SESSION["agentCompDirector"] = $supAgentDetails["agentCompDirector"];
		$_SESSION["agentDirectorAdd"] = $supAgentDetails["agentDirectorAdd"];
		$_SESSION["agentProofID"] = $supAgentDetails["agentProofID"];
		$_SESSION["otherProofID"] = $supAgentDetails["otherProofID"];
		$_SESSION["idMonth"] = $supAgentDetails["idMonth"];
		$_SESSION["idDay"] = $supAgentDetails["idDay"];
		$_SESSION["idYear"] = $supAgentDetails["idYear"];
		$_SESSION["agentDocumentProvided"] = $supAgentDetails["agentDocumentProvided"];
		$_SESSION["agentBank"] = $supAgentDetails["agentBank"];
		$_SESSION["agentAccountName"] = $supAgentDetails["agentAccountName"];
		$_SESSION["agentAccounNumber"] = $supAgentDetails["agentAccounNumber"];
		$_SESSION["agentBranchCode"] = $supAgentDetails["agentBranchCode"];
		$_SESSION["agentBranchCode"] = $supAgentDetails["agentBranchName"];  // Add by hasan for monex
		$_SESSION["agentAccountType"] = $supAgentDetails["agentAccountType"];
		$_SESSION["agentCurrency"] = $supAgentDetails["agentCurrency"];
		$_SESSION["agentAccountLimit"] = $supAgentDetails["agentAccountLimit"];
		$_SESSION["agentCommission"] = $supAgentDetails["agentCommission"];
		$_SESSION["agentStatus"] = $supAgentDetails["agentStatus"];
		$_SESSION["commPackage"] = $supAgentDetails["commPackage"];
		$_SESSION["isCorrespondent"] = $supAgentDetails["isCorrespondent"];
		$_SESSION["IDAcountry"] = $supAgentDetails["IDAcountry"];  
		$_SESSION["CommissionAnD"] = $supAgentDetails["CommissionAnD"];
		$_SESSION["commPackageAnD"] = $supAgentDetails["commPackageAnD"];
		$_SESSION["defaultDistrib"] = $supAgentDetails["defaultDistrib"];
		$_SESSION["companyType"] = $supAgentDetails["companyType"];
		$_SESSION["tradingName"] = $supAgentDetails["tradingName"];
	if($operatorFlag)
		$_SESSION["OperatorID"] = $supAgentDetails["OperatorID"];
}
else
{
if ($_POST["agentCompany"] != "")
	$_SESSION["agentCompany"] = $_POST["agentCompany"];
if ($_POST["agentContactPerson"] != "")
	$_SESSION["agentContactPerson"] = $_POST["agentContactPerson"];
if ($_POST["agentAddress"] != "")
	$_SESSION["agentAddress"] = $_POST["agentAddress"];
if ($_POST["agentAddress2"] != "")
	$_SESSION["agentAddress2"] = $_POST["agentAddress2"];
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
if ($_POST["agentZip"] != "")
	$_SESSION["agentZip"] = $_POST["postcode"];
if ($_POST["agentPhone"] != "")
	$_SESSION["agentPhone"] = $_POST["agentPhone"];
if ($_POST["agentFax"] != "")
	$_SESSION["agentFax"] = $_POST["agentFax"];
if ($_POST["email"] != "")
	$_SESSION["email"] = $_POST["email"];
if ($_POST["agentURL"] != "")
	$_SESSION["agentURL"] = $_POST["agentURL"];
if ($_POST["agentMSBNumber"] != "")
	$_SESSION["agentMSBNumber"] = $_POST["agentMSBNumber"];
if ($_POST["msbMonth"] != "")
	$_SESSION["msbMonth"] = $_POST["msbMonth"];
if ($_POST["msbDay"] != "")
	$_SESSION["msbDay"] = $_POST["msbDay"];
if ($_POST["msbYear"] != "")
	$_SESSION["msbYear"] = $_POST["msbYear"];
if ($_POST["agentCompRegNumber"] != "")
	$_SESSION["agentCompRegNumber"] = $_POST["agentCompRegNumber"];
if ($_POST["agentCompDirector"] != "")
	$_SESSION["agentCompDirector"] = $_POST["agentCompDirector"];
if ($_POST["agentDirectorAdd"] != "")
	$_SESSION["agentDirectorAdd"] = $_POST["agentDirectorAdd"];
if ($_POST["agentProofID"] != "")
	$_SESSION["agentProofID"] = $_POST["agentProofID"];
if ($_POST["otherProofID"] != "")
	$_SESSION["otherProofID"] = $_POST["otherProofID"];
if ($_POST["idMonth"] != "")
	$_SESSION["idMonth"] = $_POST["idMonth"];
if ($_POST["idDay"] != "")
	$_SESSION["idDay"] = $_POST["idDay"];
if ($_POST["idYear"] != "")
	$_SESSION["idYear"] = $_POST["idYear"];
if ($_POST["agentDocumentProvided"] != "")
	$_SESSION["agentDocumentProvided"] = $_POST["agentDocumentProvided"];
if ($_POST["agentBank"] != "")
	$_SESSION["agentBank"] = $_POST["agentBank"];
if ($_POST["agentAccountName"] != "")
	$_SESSION["agentAccountName"] = $_POST["agentAccountName"];
if ($_POST["agentAccounNumber"] != "")
	$_SESSION["agentAccounNumber"] = $_POST["agentAccounNumber"];
if ($_POST["agentBranchCode"] != "")
	$_SESSION["agentBranchCode"] = $_POST["agentBranchCode"];
if ($_POST["agentAccountType"] != "")
	$_SESSION["agentAccountType"] = $_POST["agentAccountType"];
if ($_POST["agentCurrency"] != "")
	$_SESSION["agentCurrency"] = $_POST["agentCurrency"];
if ($_POST["agentAccountLimit"] != "")
	$_SESSION["agentAccountLimit"] = $_POST["agentAccountLimit"];
if ($_POST["agentCommission"] != "")
	$_SESSION["agentCommission"] = $_POST["agentCommission"];
if ($_POST["agentStatus"] != "")
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
if($operatorFlag){
	if ($_POST["OperatorID"] != "")
		$_SESSION["OperatorID"] = $_POST["OperatorID"];
}
if ($_POST["commPackageAnD"] != "")
	$_SESSION["commPackageAnD"] = $_POST["commPackageAnD"];
if ($_POST["CommissionAnD"] != "")
	$_SESSION["CommissionAnD"] = $_POST["CommissionAnD"];	
if($_POST["defaultDistrib"] != "")
	$_SESSION["defaultDistrib"] = $_POST["defaultDistrib"];	
if($_POST["agentPaymentMode"] != "")
	$_SESSION["agentPaymentMode"] = $_POST["agentPaymentMode"];	
} 
if ($_POST["companyType"] != "")
	$_SESSION["companyType"] = $_POST["companyType"];
if ($_POST["tradingName"] != "")
	$_SESSION["tradingName"] = $_POST["tradingName"];
if($_POST["uploadImage"]!= "") 
 	$_SESSION["uploadImage"] = $_POST["uploadImage"];
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	<?
	if($_GET["type"] == "sub" && $_GET["ida"] != "Y")
	{
	?>

	if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super agent against which you want to add sub agent.");
        theForm.supAgentID.focus();
        return false;
    }
	<?
	}
	else if($_GET["type"] == "sub" && $_GET["ida"] == "Y")
	{
	?>
			if(theForm.supAgentID.options.selectedIndex == 0){
    	alert("Please select super distributor against which you want to add sub distributor.");
        theForm.supAgentID.focus();
        return false;
    }
			if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the sub distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
    if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the sub distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the sub distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the sub distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the sub distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
    <?
  }
  else if($_GET["ida"] == "Y")
  {
  	?>
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the distributor name.");
        theForm.agentCompany.focus();
        return false;
    }
   if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the distributor's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the distributor's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the distributor's country from the list.");
        theForm.Country.focus();
        return false;
    }
   if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the distributor's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
   <?
  }
  ?>
<?	if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {  ?>
  if(theForm.manualAgentNum.value == "" || IsAllSpaces(theForm.manualAgentNum.value)){
    	alert("Please provide the AnD's login name.");
        theForm.manualAgentNum.focus();
        return false;
  }
<?	}  ?>
  if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	<?
		/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
		if ( defined("CONFIG_POST_CODE_MENDATORY")
			&& CONFIG_POST_CODE_MENDATORY == 1 )
		{
			?>
				if ( theForm.postcode.value == "" )
				{
					alert("Please provide post code.");
					theForm.postcode.focus();
					return false;
				}
			<?
		}
		/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
	?>
/*	
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
<? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?> 
	
	if(document.getElementById("settlementCurrencyAsAgent").value == "")
	{
    	alert("Please select Settlement Currency As Agent.");
        document.getElementById("settlementCurrencyAsAgent").focus();
        return false;
    }
	
	if(document.getElementById("settlementCurrencyAsDistributor").value == "")
	{
    	alert("Please select Settlement Currency As Distributor.");
        document.getElementById("settlementCurrencyAsDistributor").focus();
        return false;
    }
<? }?>	
	
	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value)){
    	alert("Please select atleast one country.");
        theForm.IDAcountry.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
function processCompanyType(){
	var compTypeID = document.getElementById("companyType");
	if(compTypeID!=null){
		if(compTypeID.value!="MSB"){
			var compVal = "3rd Party";
		}
		else{
			var compVal = "MSB";
		}
		var s = 1;
		while(document.getElementById("companyLabel_"+s)!=null){
			document.getElementById("companyLabel_"+s).innerHTML = compVal;
			s++;
		}
	}
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Add Super A&D
    </font></strong></td>
  </tr>
  <form action="add-agent_distributor-conf.php?type=<?=$_GET["type"]?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
	<input type="hidden" name="agent_type" value="<?=$_GET["type"]; ?>">
  <tr>
    <td align="center">
		<table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> 
              <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#EEEEEE"> <font color="#000066" size="2"><strong>Add 
                    <? //echo ($_GET["type"] == "sub" ? "Sub" : "Super");
                    ?>
                    Super A&D
                    </strong></font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="FFFFCCC"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["msg"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["msg"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><font color="#FF0000"> 
                    <? echo "<font color='" . ($_GET["msg"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
                    </font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>
          <tr bgcolor="#EEEEEE"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <?
        		
		if(IS_BRANCH_MANAGER){
		$nAgentType = $_GET["type"];
			if($agentType2 != 'Branch Manager'){
		?>
		
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>
             <? if(CONFIG_CUSTOMER_CATEGORY == '1'){ echo CONFIG_ADMIN_STAFF_LABEL; }else{?>
              Select Branch Manager
              <? } ?>
              </strong></font></td>
            <td>
              <select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226" >
                <option value="">- Select One -</option>
                <?
					
						
						$agents = selectMultiRecords("select userID,name,username, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Branch Manager'");
					
						
					for ($i=0; $i < count($agents); $i++){
				if($agents[$i]["userID"] == $_SESSION["supAgentID"])
				{
				?>
                <option value="<? echo $_SESSION["supAgentID"]; ?>"selected>
                <? echo $agents[$i]["username"]." [".$agents[$i]["name"]."]"; ?>
                </option>
                <?
				}
				else
				{
				?>
                <option value="<? echo $agents[$i]["userID"]; ?>">
                <? echo $agents[$i]["username"]." [".$agents[$i]["name"]."]"; ?>
                </option>
                <?
			}
					}
				?>
              </select>
            </td>
          </tr>
          <?
        }
	}
	
		?>		
		<?	if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {  
			  	$loginName = "AnD Login Name";
		?>
		      <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> <?=$loginName?><font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="text" name="manualAgentNum" value="<?=stripslashes($_SESSION["manualAgentNum"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
		<?	} ?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90">
            	<strong>
                 Name* 
              	</strong>
              </font>
             </td>
            <td width="361">
              <input type="text" name="agentCompany" value="<?=stripslashes($_SESSION["agentCompany"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
		  <? if($companyTypeFlag){?>
          <tr bgcolor="#ededed"> 
            <td width="156">
				<font color="#005b90"><strong>Company Type* </strong></font>
             </td>
            <td width="361">
              <SELECT name="companyType" id="companyType" style="font-family:verdana; font-size: 11px" onChange="processCompanyType();"> 
                <OPTION value="MSB">MSB</OPTION>
                <OPTION value="3rd party">3rd Party</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].companyType, "<?=$_SESSION["companyType"]; ?>");
                                </script>
            </td>
          </tr>
		  <? }?>
		  <? if($tradingNameFlag){?>
          <tr bgcolor="#ededed" id="tradingDIV"> 
            <td width="156"><font color="#005b90">
            	<strong>
                 Trading Name 
              	</strong>
              </font>
             </td>
            <td width="361">
              <input type="text" name="tradingName" value="<?=stripslashes($_SESSION["tradingName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
		  <? }?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> Contact Person*</strong></font></td>
            <td>
              <input type="text" name="agentContactPerson" value="<?=stripslashes($_SESSION["agentContactPerson"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Address Line 
              1*</b></font></td>
            <td>
              <input type="text" name="agentAddress" value="<?=stripslashes($_SESSION["agentAddress"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Address Line 
              2</b></font></td>
            <td>
              <input type="text" name="agentAddress2" value="<?=stripslashes($_SESSION["agentAddress2"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Country*</b></font></td>
            <td>
              <SELECT name="Country" style="font-family:verdana; font-size: 11px" > 
                <OPTION value="">- Select Country-</OPTION>
                
                <?
               
              		$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 order by countryName");
				
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>">
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>City</b></font></td>
            <td> 
              <input type="text" name="City" value="<?=$_SESSION[City];?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b> 
              <? if ($_SESSION["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code</b>
			  	<?
					/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
					if ( defined("CONFIG_POST_CODE_MENDATORY")
						&& CONFIG_POST_CODE_MENDATORY == 1 )
					{
					?>
						<strong>*</strong>
					<?
					}
					/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
				?></font>
			  </td>
            <td>
              <input type="text" name="postcode" value="<?=stripslashes($_SESSION["agentZip"]); ?>" size="35" maxlength="15">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td>
              <input type="text" name="agentPhone" value="<?=stripslashes($_SESSION["agentPhone"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Fax</strong></font></td>
            <td>
              <input type="text" name="agentFax" value="<?=stripslashes($_SESSION["agentFax"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Email</strong></font></td>
            <td>
              <input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td>
              <input type="text" name="agentURL" value="<?=stripslashes($_SESSION["agentURL"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong><label id="companyLabel_1">MSB</label> Number</strong></font>
			<? if(CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT != "1"){ ?>
			<br><font size="1">OR (State Distributor license or permission from 
              Ministry of Finance)</font>
			  <? } ?>
			  </td>
            <td valign="top">
              <input type="text" name="agentMSBNumber" value="<?=stripslashes($_SESSION["agentMSBNumber"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong><label id="companyLabel_2">MSB</label> Expiry</strong></font></td>
            <td> 
              <SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Day</OPTION>
                <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
              </select>
              <script language="JavaScript">
         			SelectOption(document.forms[0].msbDay, "<?echo $msbDay;?>");
          		</script>
              <SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Month</OPTION>
                <OPTION value="01">Jan</OPTION>
                <OPTION value="02">Feb</OPTION>
                <OPTION value="03">Mar</OPTION>
                <OPTION value="04">Apr</OPTION>
                <OPTION value="05">May</OPTION>
                <OPTION value="06">Jun</OPTION>
                <OPTION value="07">Jul</OPTION>
                <OPTION value="08">Aug</OPTION>
                <OPTION value="09">Sep</OPTION>
                <OPTION value="10">Oct</OPTION>
                <OPTION value="11">Nov</OPTION>
                <OPTION value="12">Dec</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].msbMonth, "<? echo $msbMonth; ?>");
          </script>
              <SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="" selected>Year</OPTION>
                <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].msbYear, "<?echo $msbYear?>");
          </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company Registration 
              Number</strong></font></td>
            <td>
              <input type="text" name="agentCompRegNumber" value="<?=stripslashes($_SESSION["agentCompRegNumber"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company Director</strong></font></td>
            <td>
              <input type="text" name="agentCompDirector" value="<?=stripslashes($_SESSION["agentCompDirector"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td>
              <input type="text" name="agentDirectorAdd" value="<?=stripslashes($_SESSION["agentDirectorAdd"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><strong>Director 
              Proof ID</strong></font></td>
            <td valign="top">
              <input type="radio" name="agentProofID" value="Passport" <? if ($_SESSION["agentProofID"] == "Passport" || $_SESSION["agentProofID"] == "") echo "checked"; ?>>
              Passport <br>
              <input type="radio" name="agentProofID" value="Driving License" <? if ($_SESSION["agentProofID"] == "Driving License") echo "checked"; ?>>
              Driving License <br>
              <input type="radio" name="agentProofID" value="Other" <? if ($_SESSION["agentProofID"] == "Other") echo "checked"; ?>>
              Other please specify 
              <input type="text" name="otherProofID" value="<?=$_SESSION[otherProofID]; ?>" size="15">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td> 
              <SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Day</OPTION>
                <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
              </select>
              <script language="JavaScript">
         			SelectOption(document.forms[0].idDay, "<?echo $idDay;?>");
          		</script>
              <SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="">Month</OPTION>
                <OPTION value="01">Jan</OPTION>
                <OPTION value="02">Feb</OPTION>
                <OPTION value="03">Mar</OPTION>
                <OPTION value="04">Apr</OPTION>
                <OPTION value="05">May</OPTION>
                <OPTION value="06">Jun</OPTION>
                <OPTION value="07">Jul</OPTION>
                <OPTION value="08">Aug</OPTION>
                <OPTION value="09">Sep</OPTION>
                <OPTION value="10">Oct</OPTION>
                <OPTION value="11">Nov</OPTION>
                <OPTION value="12">Dec</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].idMonth, "<? echo $idMonth; ?>");
          </script>
              <SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
                <OPTION value="" selected>Year</OPTION>
                <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].idYear, "<?echo $idYear?>");
          </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><strong> Document 
              Provided</strong></font></td>
            <td>
              <p> 
                <input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($_SESSION["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
                <label id="companyLabel_3">MSB</label> License <br>
                <input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($_SESSION["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
                Company Registration Certificate <br>
                <input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($_SESSION["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
                Utility Bill <br>
                <input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($_SESSION["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
                Director Proof of ID</p>
            </td>
          </tr>
	<? if($operatorFlag){?>
		  <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Operator ID</strong></font></td>
            <td>
              <input type="text" name="OperatorID" value="<?=stripslashes($_SESSION["OperatorID"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
	<? }?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Name</strong></font></td>
            <td>
              <input type="text" name="agentBank" value="<?=stripslashes($_SESSION["agentBank"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Name</strong></font></td>
            <td>
              <input type="text" name="agentAccountName" value="<?=stripslashes($_SESSION["agentAccountName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Number</strong></font></td>
            <td>
              <input type="text" name="agentAccounNumber" value="<?=stripslashes($_SESSION["agentAccounNumber"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Branch Code</strong></font></td>
            <td>
              <input type="text" name="agentBranchCode" value="<?=stripslashes($_SESSION["agentBranchCode"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Branch Name</strong></font></td>
            <td>
              <input type="text" name="agentBranchName" value="<?=stripslashes($_SESSION["agentBranchBranch"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Bank Account Type</strong></font></td>
            <td>
              <select name="agentAccountType">
                <option value="Current">Current</option>
                <option value="Saving">Saving</option>
                <option value="Other">Other</option>
              </select>
              <script language="JavaScript">
						         			SelectOption(document.forms[0].agentAccountType, "<? echo $_SESSION["agentAccountType"]; ?>");
						          	  </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" height="24"><font color="#005b90"><strong>Account 
              Currency</strong></font></td>
            <td height="24"> 
              <SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
              	 <?
						 						  
					$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
					$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
					while($rstRow = mysql_fetch_array($nResult))
					{
						$currency = $rstRow["currencyName"];	
						$description = $rstRow["description"];
						$erID  = $rstRow["cID"];							
						if($selectedCurrency == $currency)
						{
							echo "<option value ='$currency' selected> ".$currency." --> ".$description."</option>";			
						}
						else
						{
						    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
						  }
					}
				  ?>				
              </SELECT>
              <script language="JavaScript">
						         			SelectOption(document.forms[0].agentCurrency, "<? echo $_SESSION["agentCurrency"]; ?>");
						          	  </script>
            </td>
          </tr>
          
          <!--tr bgcolor="#ededed"> 
            <td width="156" justify="middle" height="20"><font color="#005b90"></font></td>
            <td height="20"> </td>
          </tr-->
		  
		  <? if (CONFIG_AGENT_LIMIT == 1){ ?>
		  <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Agent Account Limit</strong></font></td>
            <td>
              <input type="text" name="agentAccountLimit" value="<?=stripslashes($_SESSION["agentAccountLimit"]); ?>" size="35" maxlength="15">
            </td>
          </tr>
        <? } ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Commission package</strong>(As Agent)</font></td>
            <td>
              <select name="commPackage" id="commPackage">
                <option value="001">Fixed Amount</option>
                <option value="002">Percentage of Transaction Amount</option>
                <option value="003">Percentage of Fee</option>
                <option value="004">None</option>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].commPackage, "<?=$_SESSION["commPackage"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Commission Percentage/Fee</strong>(As Agent)</font></td>
            <td>
              <input type="text" name="agentCommission" value="<?=stripslashes($_SESSION["agentCommission"]); ?>" size="35" maxlength="6">
            </td>
          </tr>
          <!--------->
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Commission package</strong>(As Distributor)</font></td>
            <td>
              <select name="commPackageAnD" id="commPackageAnD">
                <option value="001">Fixed Amount</option>
                <option value="002">Percentage of Transaction Amount</option>
                <option value="003">Percentage of Fee</option>
                <option value="004">None</option>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].commPackageAnD, "<?=$_SESSION["commPackageAnD"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Commission Percentage/Fee</strong>(As Distributor)</font></td>
            <td>
              <input type="text" name="CommissionAnD" value="<?=stripslashes($_SESSION["CommissionAnD"]); ?>" size="35" maxlength="6">
            </td>
          </tr>
		  
          <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?> 
		  <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency</strong>(As Agent)</font><font color="red">*</font></td>
            <td>
              <select name="settlementCurrencyAsAgent" id="settlementCurrencyAsAgent">
			  <option value="">-- Select Currency --</option>
			  <?
				$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		
				while($rstRow = mysql_fetch_array($nResult))
				{
					$country = $rstRow["country"];
					$currency = $rstRow["currencyName"];	
					$description = $rstRow["description"];
					$erID  = $rstRow["cID"];							
					
				 echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
				}
			  ?>				
				</select>
              </select>
              <script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsAgent, "<?=$_SESSION["settlementCurrencyAsAgent"]; ?>");</script>
            </td>
          </tr>
		  
		  <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency</strong>(As Distributor)</font><font color="red">*</font></td>
            <td>
              <select name="settlementCurrencyAsDistributor" id="settlementCurrencyAsDistributor">
			  <option value="">-- Select Currency --</option>
			  <?
				$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		
				while($rstRow = mysql_fetch_array($nResult))
				{
					$country = $rstRow["country"];
					$currency = $rstRow["currencyName"];	
					$description = $rstRow["description"];
					$erID  = $rstRow["cID"];							
					
				 	echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
				}
			  ?>				
				</select>
              </select>
              <script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsDistributor, "<?=$_SESSION["settlementCurrencyAsDistributor"]; ?>");</script>
            </td>
          </tr>
		  <? }?>
		  
		  
		  
          <!---------->
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Status</strong></font></td>
            <td>
              <input type="radio" name="agentStatus" value="New" <? if ($_SESSION["agentStatus"] == "" || $_SESSION["agentStatus"] == "New") echo "checked"; ?>>
              New 
              <input type="radio" name="agentStatus" value="Active" <? if ($_SESSION["agentStatus"] == "Active") echo "checked"; ?>>
              Active 
              <input type="radio" name="agentStatus" value="Disabled" <? if ($_SESSION["agentStatus"] == "Disabled") echo "checked"; ?>>
              Disabled 
              <input type="radio" name="agentStatus" value="Suspended" <? if ($_SESSION["agentStatus"] == "Suspended") echo "checked"; ?>>
              Suspended</td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Correspondent</strong></font></td>
            <td>
              <select name="correspondent" id="correspondent">
               <? /* if($_GET["ida"] != "Y") {?> <option value="N">Agent</option><!--Can't distribute transactions--><? } ?>
                <? if($_GET["ida"] == "Y"){ ?><option value="ONLY">Distributor</option><!--Only distribute transactions--><? } */ ?>
                <option value="Y" selected>Both Agent and Distributor</option><!--Can distribute transactions-->
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].correspondent, "<?=$_SESSION["isCorrespondent"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Select country to Assign distribution/Send money*</strong></font></td>
            <td>Hold Ctrl key for multiple selection<br>
              <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
                <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
	                	
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
                //$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Company Logo </strong></font></td>
            <td>
              <input type="file" name="logo">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Authorised for the services</strong></font></td>
            <td> Hold Ctrl key for multiple selection<br>
              <SELECT name="authorizedFor[]" size="4" multiple style="font-family:verdana; font-size: 11px" >
                <?
			if(trim($_SESSION["authorizedFor"]) == "")
			{?>
                <option value="Pick up" selected>Pick up</option>
                <?
			}
			else
			{
			?>
                <option value="Pick up" <? echo (strstr($_SESSION["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick 
                up</option>
                <?
			}
			?>
                <option value="Bank Transfer" <? echo (strstr($_SESSION["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank 
                Transfer</option>
                <option value="Home Delivery" <? echo (strstr($_SESSION["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home 
                Delivery</option>
              </SELECT>
            </td>
          </tr>
					
					<!--
					Start of code @ticket #3497 point 4
					The at source and at end of month options already exists for agent added for AnD  
					-->	 					
 					<? 
 					if(CONFIG_AGENT_PAYMENT_MODE == "1"){ 
 					?>
						<tr bgcolor="#ededed"> 
            	<td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            	<td>
            		<select name="agentPaymentMode" id="agentPaymentMode">
              	<? 
              	if(CONFIG_PAYMENT_MODE_OPTION == "1"){ 
              	?>
              		<option value="exclude">At Source</option>
                <? 
                }else{ 
                ?>
	                <option value="include">At End Of Month</option>
	                <option value="exclude">At Source</option>
               	<? 
               	} 
               	?>
              	</select>
              	<script language="JavaScript">
         					SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
              	</script>
            	</td>
          	</tr>
          <? 
          }else{ 
          ?>
          	<tr bgcolor="#ededed"> 
	            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
	            <td>
	            	<select name="agentPaymentMode" id="agentPaymentMode">
		            	<option value="include">At End Of Month</option>  
    						</select>
              	<script language="JavaScript">
         					SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
                </script>
            	</td>
          	</tr>
          
          <? 
          } 
          ?>
          <!--
          End of code @ticket #3497 
          -->
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Access From IP</strong></font></td>
            <td>For multiple please separate by comma. 
              <input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255">
            </td>
          </tr>
		  <!--  #4996 point 2 AMB upload document functionality-->
		  <? if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1") { ?> 
		  <tr bgcolor="#ededed"> 
           <td width="37%"align="right"><font color="#005b90"><b>Upload Documents&nbsp;</b></font></td>
           	<td width="37%"><input type="checkbox" name="uploadImage" value="Y" <?=($_SESSION["uploadImage"]=="Y")?'checked="checked"':''?>>
           		<input type="hidden" name="mode" value="Add">
           		</td>
           				</tr>
         <? 
		    }
		 ?>
           <?
         if(CONFIG_DEFAULT_DISTRIBUTOR == '1')
         {
         	?>

         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Default Distributor</strong></font>
					</td>
					<td> 
						<select name="defaultDistrib" id="defaultDistrib">
              			<option value="N" <? echo($_SESSION["defaultDistrib"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($_SESSION["defaultDistrib"]=="Y" ? "selected"  : "")?>>Yes</option>	
						</select>
					</td>
				</tr>
        <?
         	}
         	?>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> 
              <input type="submit" value=" Save ">
              &nbsp;&nbsp; 
              <input type="reset" value=" Clear ">
            </td>
          </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>