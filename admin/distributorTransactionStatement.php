<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$systemCode = SYSTEM_CODE;
///////////////////////History is maintained via method named 'activities'


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
if(CONFIG_REPORT_ALL == '1')
{
	if($agentType != "SUPI" && $agentType != "SUPAI")
		$condition = True;	
}
else
{
	if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager" || $agentType == "SUPI")
		$condition = True;
}
	
		
//////////////////////////Making of Condition///////////////




session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");


if($_POST["Submit"]=="Search")
{
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";		
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
}

if ($_POST['destcurrency'] != "") {
	$destcurrency = $_POST['destcurrency'] ;	
} else if ($_GET['destcurrency'] != "") {
	$destcurrency = $_GET['destcurrency'] ;	
}

$_SESSION["agentID2"] = "";

if($_POST["agentID"]!="")
	$_SESSION["agentID2"] = $_POST["agentID"];
elseif($_GET["agentID"]!="")
	$_SESSION["agentID2"]=$_GET["agentID"];
elseif($agentType == 'SUPI')
	$_SESSION["agentID2"] = $changedBy;

if ($offset == "")
	$offset = 0;
$limit=20;

if ($_GET["newOffset"] != "") 
	$offset = $_GET["newOffset"];

$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Distributor Account Statement</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function validForm(theForm) {
	if (theForm.agentID.selectedIndex == 0) {
		alert("Please select Distributor")	;
		theForm.agentID.focus() ;
		return false ;
	}
	return true ;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">GE Distributor Transaction Statement</td>
  </tr>
  <tr>
  <td width="563">
			<form action="distributorTransactionStatement.php" method="post" name="Search" onSubmit="return validForm(this);">
        <div align="center">
		From 
		<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

		 <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script><br><br>
       <?    
       
       if($condition)
			  {
			  ?>
			  Distributor &nbsp;
			  <select name="agentID" style="font-family:verdana; font-size: 11px" <? if (DEST_CURR_IN_ACC_STMNTS == "1") { ?>onchange="document.Search.submit();"<? } ?>>
				<option value="">- Select Distributor -</option>
					<? if($agentType != "SUPI Manager" && DEST_CURR_IN_ACC_STMNTS != "1") {?>
				<?
			    }
						$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' order by agentCompany");
											
						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
						}
					?>
			  </select>
			  <script language="JavaScript">SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");</script>
		<? }?>
		<?	if (DEST_CURR_IN_ACC_STMNTS == "1" && $_SESSION["agentID2"] != "") {  ?>
					<br><br>
					Currency &nbsp;
					<? 
					if ($_SESSION["agentID2"] != "") {
										
						
						if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1"){
							 $settlementCurr   = selectFrom("select userID,settlementCurrency from ".TBL_ADMIN_USERS." where userID = '".$_SESSION["agentID2"]."' ");	
              
						}else{
							
							 //$query = "select distinct(currency) from exchangerate where rateFor = 'settlement' and rateValue = '".$_SESSION["agentID2"]."'";
						   	$queryCurrency1 = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																  WHERE c.countryId = s.toCountryId " ;
						   $queryCurrency = selectMultiRecords($queryCurrency1);
						}
						$distIDACountries = selectFrom("SELECT `IDAcountry` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_SESSION["agentID2"]."'") ;						
						$arrDistCountries = explode(",", $distIDACountries['IDAcountry']);
						$strCountries = implode ("','", $arrDistCountries);
					  $query = "select DISTINCT(s.currencyRec) from countries as c, services as s where c.countryId = s.toCountryId and c.countryName IN ('".$strCountries."') ";
					  $queryCurrency = selectMultiRecords($query);
					  
						$strCurrency = "";
						for($j = 0; $j < count($queryCurrency); $j++)
						{
							if($j > 0)
								
								$strCurrency .= ", ";
								$strCurrency .= trim($queryCurrency[$j]["currencyRec"]);
						}
														
					$arrCurrency = explode (", ",$strCurrency);
					$uniqueCurrencyList1 = array_unique($arrCurrency);	
					
				 foreach($uniqueCurrencyList1 as $value)
					{
					     $uniqueCurrencyList[]=$value;
				  }
				
				  
					} else {

						$queryCurrency = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																WHERE c.countryId = s.toCountryId " ;
					} ?>
					
					
					
					<select name="destcurrency" style="font-family:verdana; font-size: 11px; width:130">
			    
			     <?  if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1"){ ?>
			      
							 <option value="<?=trim($settlementCurr["settlementCurrency"])?>"><?=trim($settlementCurr["settlementCurrency"])?></option>	
							
			     	<?  
			      }else{
						
						for($k = 0; $k < count($uniqueCurrencyList); $k++)
						{?>
							 <option value="<?=trim($uniqueCurrencyList[$k])?>"><?=trim($uniqueCurrencyList[$k])?></option>	
						<? 
						}
					}	
						
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].destcurrency, "<?=$destcurrency?>");</script>
		&nbsp;&nbsp;
		<?	}  ?>
          <input type="submit" name="Submit" value="Search">
        </div>
      	</form> 
		</td>
		</tr>
		<tr>
		
    <td> 
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["agentID"]!="")
			$slctID=$_GET["agentID"];
		elseif($_GET["agentID"]=="") 
		{
			$slctID=$_POST["agentID"];
			$_SESSION["grandTotal"]="";
			$_SESSION["CurrentTotal"]="";
		}
					
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
									
		if($condition)
		{
			if($slctID!='')
			{
				$accountQuery = "Select * from bank_account where 1";
				$accountQueryCnt = "Select count(*) from bank_account where 1";
				
				
				if($slctID!='All')
				{
					$accountQuery .= " and bankID = $slctID";
					$accountQueryCnt .= " and bankID = $slctID";
				}
				if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") 
				{
					$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
					$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
				}
				$accountQuery .= " and $queryDate";	
				$accountQueryCnt .= " and $queryDate";
				
				$allCount = countRecords($accountQueryCnt);
				$strSqlForExport = $accountQuery;
				$accountQuery .= " LIMIT $offset , $limit";
				$contentsAcc = selectMultiRecords($accountQuery);
			}
		}
		else
		{
			$accountQuery="Select * from bank_account where 1";
			$accountQuery.= " and bankID = $changedBy";
			if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
					$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
			}
			$accountQuery .= " and $queryDate";	
			
			$accountQueryCnt = "Select count(*) from bank_account where 1";
			$accountQueryCnt .= " and bankID = $changedBy";
			if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
					$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
			}
			$accountQueryCnt .= " and $queryDate";

			$allCount = countRecords($accountQueryCnt);
			$strSqlForExport = $accountQuery;
			$accountQuery .= " LIMIT $offset , $limit";
			$contentsAcc = selectMultiRecords($accountQuery);
		}
		$strSqlForExport = addslashes($strSqlForExport);
	?>
      <div align="center">
        <table width="758" height="278" border="1" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC"> 
				<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
					<tr> 
					<td align="left">
					<?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> 
					of 
					<?=$allCount; ?>
					<?php } ;?>
					</td>
					<?php if ($prv >= 0) { ?>
					<td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
					<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
					<?php } ?>
					<?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
					$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
					<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
					<td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
					<?php } ?>
					</tr>
				</table>
			</td>
          </tr>
          <?
          		$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				
		if(count($contentsAcc) > 0)
		{
		?>
		
        <tr> 
            <td nowrap bgcolor="#EFEFEF"> 
			  <table width="809" height="181" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td align="center"><span class="style1">Date</span></td>
                  <td align="center"><span class="style1"><?=$systemCode?></span></td>
                  <td align="center"><span class="style1">Fax Number</span></td>
				  <td align="center"><span class="style1">Beneficiary Name</span></td>
				  <td align="center"><span class="style1">Amount Received</span></td>
				  <td align="center"><span class="style1">Payout Amount</span></td>
                  <td align="center"><span class="style1">Balance</span></td>
				  <td align="center"><span class="style1">Exchange Rate</span></td>
				  <td align="center"><span class="style1">Sending Amount</span></td>
				  <td align="center"><span class="style1">Description</span></td>
                </tr>
                <? 
				
				$intRunningBalance = distributorOpeningBalance($slctID,$_REQUEST["destcurrency"],$fromDate);
				
				for($i=0;$i<count($contentsAcc);$i++)
				{
					/**
					 * Fetching the associated transaction and beneficiary details
					 */
					if(!empty($contentsAcc[$i]["TransID"]))
					{
						$strTransactionSql = "
										select 
											t.refNumberIM,
											t.exchangeRate,
											t.localAmount,
											t.transAmount,
											t.currencyFrom,
											t.currencyTo,
											t.transDate,
											t.fromCountry,
											t.toCountry,
											t.faxNumber,
											b.firstName,
											b.lastName
										from
											beneficiary as b,
											transactions as t
										where
											    b.benID = t.benID
											and t.transID = ".$contentsAcc[$i]["TransID"];
						
						$arrTransactionData = selectFrom($strTransactionSql);
						
						$faxNumber = $arrTransactionData["faxNumber"];
						$refNumber = $arrTransactionData["refNumberIM"];
						$name = $arrTransactionData["firstName"]." ".$arrTransactionData["lastName"];
						$localAmount = $arrTransactionData["localAmount"];
						$exchangeRate = $arrTransactionData["exchangeRate"];
						$transAmount = $arrTransactionData["transAmount"];
					}
					else
					{
						$faxNumber = "&nbsp;";
						$refNumber = "&nbsp;";
						$name = "&nbsp;";
						$localAmount = "&nbsp;";
						$exchangeRate = "&nbsp;";
						$transAmount = "&nbsp;";
					}
				
					if(CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT == 1)
						$cumulativeCurr=$contentsAcc[$i]["currency"];
				
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="230" align="center">
				  	<strong><font color="#006699">
					<? 
						$authoriseDate = $contentsAcc[$i]["dated"];
						echo date("F j, Y", strtotime("$authoriseDate"));
					?>
		           </font></strong>
				  </td>
            <?  
             if(!empty($contentsAcc[$i]["TransID"])) 
			 {
			?>
         	<td align="center">
				<a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					<strong><font color="#006699"><?=$arrTransactionData["refNumberIM"]?></font></strong>
				</a>
			</td>      
			<? }else{?>
				<td>&nbsp;</td>
			<? }?>
			<td width="231" align="center">   
				<?=$faxNumber?>
			</td>
			<td width="231" align="center">   
				<?=$name?>
			</td>
			<td>
			<?  
				if($contentsAcc[$i]["type"] == "DEPOSIT")
				{
					if (DEST_CURR_IN_ACC_STMNTS == "1") 
						echo (CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT == 1?" ".$contentsAcc[$i]["currency"]." ":"") ;
			
					echo $contentsAcc[$i]["amount"];
					$Balance = $Balance+$contentsAcc[$i]["amount"];
				}
			?>
			</td>
			<td align="center">
				<?
					if($contentsAcc[$i]["type"] == "WITHDRAW")
						echo $contentsAcc[$i]["amount"].(CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT == 1?" ".$contentsAcc[$i]["currency"]:"");
				?>
			</td>
			<? 
				$currDate = $contentsAcc[$i]["dated"];
				
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
				}else{
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
				}
				$FirstDated = $datesFrom["datedFrom"];
				
				$openingBalance = 0;
				
				
				if($FirstDated != "")
				{
					$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
					$account1 .= " and dated = '$FirstDated'";				
					$account1 .= " and  user_id = '".$slctID."' ";	
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') 
					{
						$account1 .= " and  currency='".$settleCurr."' ";		
					}
					
					$contents1 = selectFrom($account1);
					
					//echo $currDate." == ".$FirstDated."<br>";
					if($currDate != $FirstDated){
						$openingBalance =  $contents1["opening_balance"];
					}else{
						$openingBalance =  $contents1["closing_balance"];
					}
					
					//$intRunningBalance = $openingBalance;

				}
	
				$closingBalance = $openingBalance;
				
				$preDate = $currDate;
				
				/* Running balance functionality*/
				if($contentsAcc[$i]["type"] == "DEPOSIT")
					$intRunningBalance = $intRunningBalance + $contentsAcc[$i]["amount"];
				elseif($contentsAcc[$i]["type"] == "WITHDRAW")
					$intRunningBalance = $intRunningBalance - $contentsAcc[$i]["amount"];
					
          		/* Closing balance funcionality */
				if($contentsAcc[$i]["type"] == "WITHDRAW")
					$closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
				
				elseif($contentsAcc[$i]["type"] == "DEPOSIT")
					$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
				
			  ?>
			  <td width="231" align="center"><?=$intRunningBalance.(CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT == 1?" ".$contentsAcc[$i]["currency"]:"")?></td>
			  <!--td width="231" align="center"><?=$closingBalance.(CONFIG_SHOW_CURRENCY_LABELS_AT_STATEMENT == 1?" ".$contentsAcc[$i]["currency"]:"")?></td-->
			  
			  <td align="center">
				<?=$exchangeRate?>
			  </td>	
			  <td align="center">
				<?=$transAmount?>
			  </td>	
			  <td align="center">
				<?=$contentsAcc[$i]["description"]?>
			  </td>	
            </tr>
			<? 
	       	  } 
			?>
			<tr bgcolor="#FFFFFF"> 
				<td colspan="10">&nbsp;</td>
			</tr>
			<? if(CONFIG_EXPORT_DISTRIBUTOR_STATEMENT_REPORT == 1) { ?>
				<tr bgcolor="#FFFFFF"> 
                  <td colspan="10" align="center">
				  	<form action="exportDistriburtorStatementReport.php" method="post" target="_blank">
				  		<input type="hidden" name="da" value="<?=$strSqlForExport?>" />
						<input type="hidden" name="destcurrency" value="<?=$_REQUEST["destcurrency"]?>" />
						<input type="hidden" name="slctID" value="<?=$slctID?>" />
						<input type="hidden" name="fromDate" value="<?=$fromDate?>" />
						<input type="submit" value="Export Transactions" />
					</form>
				  </td>
                </tr>
				<? } ?>
			<?
				if($_GET["total"]=="first")
				{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
				}
				elseif($_GET["total"]=="pre")
				{
					$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
					$_SESSION["CurrentTotal"]=$Balance;
				}
				elseif($_GET["total"]=="next")
				{
					$_SESSION["grandTotal"] += $Balance;
					$_SESSION["CurrentTotal"]= $Balance;
				}
				else
				{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
				}

			}
			else
			{
			?>
			  <tr> 
				<td nowrap bgcolor="#EFEFEF" colspan="10" align="center">
					Could not find data for selected filters.
				</td>
			  </tr>
          <?
			}
			if($slctID!='')
			{
				if($agentType == "admin" || $agentType == "Admin Manager")
				{
					?>
				  <tr> 
					<td bgcolor="#c0c0c0" colspan="10" align="left">
						<span class="child">
							<a HREF="add_Bank_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>&destcurrency=<?=$destcurrency?>&returnUrl=distributorTransactionStatement" target="mainFrame" class="tblItem style4">
								<font color="#990000">Deposit Money</font>
							</a>
						</span>
					</td>
				  </tr>
				  <? 
				}
			 }
		  ?>
        </table>
        <?	
	 	}
	 	?>
     				</td>
				</tr>
			</table>
		</div>
	</td>
</tr>
</table>
</body>
</html>