<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$filters = "";
$excludeDistFlag = false;
$transTypeFlag = true;
$pickupFlag = false;

if(CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION == "1")
{
		
	$excludeDistId = CONFIG_CUSTOMIZE_BANK_BASED_TRANSACTION_EXCLUDE_DISTRIBUTOR;
	$excludeDistFlag = true;
		if(trim($_REQUEST["transType"]) =="Pick up"){
			$transTypeFlag = false;
			$pickupFlag = true;
		}else{
			$transTypeFlag = true;	
			}
}	

if(!empty($_GET["RID"]))
{
	$openSaveReportSql = "select * from ".TBL_COMPLIANCE_QUERY_LIST." where RID=".$_GET["RID"];
	$openSaveReportSqlData = SelectMultiRecords($openSaveReportSql);
	//debug($openSaveReportSqlData);
	
	//foreach($openSaveReportSqlData as $ork => $orv)
	$fromDateSR 	= $openSaveReportSqlData[0]["filterValue"]." 00:00:00";
	$toDateSR 		= $openSaveReportSqlData[1]["filterValue"]." 23:59:59";
	$distributorSR 	= $openSaveReportSqlData[2]["filterValue"];
	
	if($excludeDistFlag){
	$transType 	= $openSaveReportSqlData[3]["filterValue"];
		if($transType == "Pick up"){
			$transTypeFlag = false;
				$pickupFlag = true;
			}else{
				$transTypeFlag = true;	
				}
	}
	
	$filters .= " and (tr.transDate BETWEEN '$fromDateSR' AND '$toDateSR') ";
    if(!empty($distributorSR))
        $filters .= " and tr.benAgentID = ".$distributorSR;
	
	if($transTypeFlag){
	$sql = "select 
					tbd.bankName, 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben 
				where
					tbd.transID = tr.transID and
					tr.benID = ben.benID and
					tr.dispatchNumber = '0' and
					tbd.bankName != '' and
					tr.benAgentID != 0
					". $filters ." 
				order by tbd.bankName	asc";
	}
	if($excludeDistFlag && $pickupFlag && !$transTypeFlag){
			
			$sql = "select 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					cp.cp_corresspondent_name,
					cp.cp_city
				from 
					transactions as tr
					LEFT JOIN cm_collection_point as cp ON tr.collectionPointID = cp.cp_id
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
				where
					 transType = '".trim($transType)."'
					". $filters ." 
				order by tr.transID	asc
					";
				//debug($sql);	
			}	
}


/* Dispatched transaction */
if(!empty($_REQUEST["dt"]))
{

	$openDispatchReportSql = "select dispatchNumber, reportQuery from dispatch_book where recId=".$_REQUEST["dt"];
	$openDispatchReportSqlData = SelectFrom($openDispatchReportSql);
	
	$atrTansSql = $openDispatchReportSqlData["reportQuery"];
	$atrTansSql = stripslashes($atrTansSql);
	$atrTansSql = str_replace("tr.dispatchNumber = '0'","tr.dispatchNumber = '".$openDispatchReportSqlData["dispatchNumber"]."'",$atrTansSql);
	
	$sql = $atrTansSql;
}





if($_POST["Submit"])
{
	/**
	 * Date filter
	 */
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	$filters .= " and (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";

	if(!empty($_REQUEST["distributors"]))
	{
		$filters .= " and tr.benAgentID = '".$_REQUEST["distributors"]."' ";
	}
	
	if(CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT == "1"){
	$filters .= " and (tr.transStatus = 'Authorize' OR tr.transStatus = 'Amended')";
	}
	if(!empty($_REQUEST["dispatchNumber"]))
	{
		$openDispatchReportSql = "select dispatchNumber, reportQuery from dispatch_book where recId=".$_REQUEST["dispatchNumber"];
		$openDispatchReportSqlData = SelectFrom($openDispatchReportSql);
		
		$filters .= " and tr.dispatchNumber = '".$openDispatchReportSqlData["dispatchNumber"]."' ";
		
	}
	else
		$dispatchFilter .= " and tr.dispatchNumber = '0' ";
	
	if($transTypeFlag){
	$sql = "select 
					tbd.bankName, 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben 
				where
					tbd.transID = tr.transID and
					tr.benID = ben.benID and
					tbd.bankName != '' 
					".$dispatchFilter." 
					and tr.benAgentID != 0
					". $filters ." 
				order by tbd.bankName	asc
					";
		}
			if($excludeDistFlag && $pickupFlag && !$transTypeFlag){
			
			$sql = "select 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					cp.cp_corresspondent_name,
					cp.cp_city
				from 
					transactions as tr
					LEFT JOIN cm_collection_point as cp ON tr.collectionPointID = cp.cp_id
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
				where
					transType = '".trim($_REQUEST["transType"])."' 
					".$dispatchFilter." 
					". $filters ." 
				order by tr.transID	asc
					";
				//debug($sql);	
			}	
}

		
		if($_GET["sr"] == "y")
		{
				$saveSql="insert into ".TBL_COMPLIANCE_QUERY." 
				(reportName,logedUser,logedUserID,queryDate,reportLabel,note) 
				values ('bank-trasactions-report.php','".$agentType."','".$_SESSION["loggedUserData"]["userID"]."','".date("Y-m-d H:i:s")."','Bank Based Transactions','')";
				insertInto($saveSql);
				$reportID = @mysql_insert_id();
				foreach($_GET as $gk => $gv)
				{
					$saveFiltersSql = "insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$gk."','".$gv."')";
					insertInto($saveFiltersSql); 
				}
				$msg="Report Has Been Saved";  
				
		}
		
		/* If to export transactions */
		if(!empty($_REQUEST["export"]) && $_REQUEST["export"] == "t" && !empty($_REQUEST["at"]))
		{
			$allTrans = $_REQUEST["at"];
			$arrIds = explode(",",$allTrans);
			if(count($arrIds) > 1)
			{
				$strDispatchNumSql = "select Max(dispatchNumber) as mdn from transactions";
				$arrDispatchNumData = selectFrom($strDispatchNumSql);
				$intDispatchNumber = $arrDispatchNumData["mdn"];
				$intDispatchNumber++;
								
				$strSqlSer = addslashes($sql);
				
				$strDispatchSql = "insert into dispatch_book 
										(dispatchNumber, created, reportQuery)
									values
										('".$intDispatchNumber."','".date("Y-m-d h:i:s")."','".$strSqlSer."')";

				
				//insertInto($strDispatchSql);
				$intDispatchId = mysql_insert_id();
				
				foreach($arrIds as $key => $val)
				{
					if(!empty($val))
					{
						$strDispatchNumUpdateSql = "update transactions 
													set 
														dispatchNumber='".$intDispatchNumber."',
														dispatchDate='".date("d/m/y")."' 
													where transID=".$val;
						//update($strDispatchNumUpdateSql);
					}
				}
				if(CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT == "1"){
					$format = '';
					if(defined("CONFIG_MT103_EXPORT_FORAMT"))
						$format = CONFIG_MT103_EXPORT_FORAMT;
					else
						$format = 'csv';
				echo "<script>window.open('BankTransactionExcel_AMB.php?recId=".$intDispatchId."&distributors=".$_REQUEST["distributors"]."&transTypeFlag=".$transTypeFlag."&pickupFlag=".$pickupFlag."&excludeDistFlag=".$excludeDistFlag."&format=".$format."&transType=".$_REQUEST["transType"]."','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');</script>";
				}else{
					echo "<script>window.open('BankTransactionExcel.php?recId=".$intDispatchId."','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');</script>";
				}
			}
		}
		
		
		
		
		/**
		 * @Ticket #3965
		 */
		if(!empty($_REQUEST["exportspb"]) && $_REQUEST["exportspb"] == "t" && !empty($_REQUEST["at"]))
		{
			$allTrans = $_REQUEST["at"];
			$arrIds = explode(",",$allTrans);
			if(count($arrIds) > 1)
			{
				$strDispatchNumSql = "select Max(dispatchNumber) as mdn from transactions";
				$arrDispatchNumData = selectFrom($strDispatchNumSql);
				$intDispatchNumber = $arrDispatchNumData["mdn"];
				$intDispatchNumber++;
				
				$strSqlSer = addslashes($sql);
				
				$strDispatchSql = "insert into dispatch_book 
										(dispatchNumber, created, reportQuery)
									values
										('".$intDispatchNumber."','".date("Y-m-d h:i:s")."','".$strSqlSer."')";

				
				//insertInto($strDispatchSql);
				$intDispatchId = mysql_insert_id();
				 
				foreach($arrIds as $key => $val)
				{
					if(!empty($val))
					{
						$strDispatchNumUpdateSql = "update transactions 
													set 
														dispatchNumber='".$intDispatchNumber."', 
														dispatchDate='".date("d/m/y")."' 
													where transID=".$val;
					//	update($strDispatchNumUpdateSql);
					}
				}
				if(CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT == "1"){
				$format = '';
					if(defined("CONFIG_MT103_EXPORT_FORAMT"))
						$format = CONFIG_MT103_EXPORT_FORAMT;
					else
						$format = 'csv';
						
					echo "<script>window.open('BankTransactionExcel_AMB.php?recId=".$intDispatchId."&format=<?=$format?>&distributors=".$_REQUEST["distributors"]."','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');</script>";
			}else{
				echo "<script>window.open('BankTransactionExcel.php?recId=".$intDispatchId."&format=csv','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740');</script>";
			}
			}
			
		}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bank Based Trasactions Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 24px;
	font-weight: bold;
	height: 50px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 16px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->
</style>
<script>
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
</head>
<body>
	<?
		/**
		 * If report is not opened from saved reports section 
		 * Than display the filters
		 */
		if(empty($_GET["RID"]))
		{
	?>
	<table border="0" align="center" cellpadding="5" cellspacing="1" >
      <tr>
      	<td align="center">
  			<b>SEARCH FILTER</b>    				
      	</td>
    	</tr>
      <tr>
			  <form name="search" method="post" action="bank-trasactions-report.php">
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
			<b>From </b>
			<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.fDay, "<?=(!empty($_POST["fDay"])?$_POST["fDay"]:"")?>");
			</script>
					<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.fMonth, "<?=(!empty($_POST["fMonth"])?$_POST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			  <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
			</SELECT> 
					<script language="JavaScript">
				SelectOption(document.search.fYear, "<?=(!empty($_POST["fYear"])?$_POST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			  <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
			</select>
					<script language="JavaScript">
				SelectOption(document.search.tDay, "<?=(!empty($_POST["tDay"])?$_POST["tDay"]:"")?>");
			</script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
	
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
					<script language="JavaScript">
				SelectOption(document.search.tMonth, "<?=(!empty($_POST["tMonth"])?$_POST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
					<script language="JavaScript">
			SelectOption(document.search.tYear, "<?=(!empty($_POST["tYear"])?$_POST["tYear"]:"")?>");
			</script>
			
			<? if($excludeDistFlag){ ?>
			<br /><br />
			&nbsp;&nbsp;
			 <b> Transaction Type </b>
			 <select name="transType" id="transType" style="font-family: verdana; font-size: 11px;">
			 <option value="">--Select Type--</option>
			 <option value="Bank Transfer" selected="selected">Bank Transfer</option>
			 <option value="Pick up">Pick up</option>
			 </select>
			 <script language="JavaScript">
				SelectOption(document.search.transType, "<?=(!empty($_POST["transType"])?$_POST["transType"]:"Bank Transfer")?>");
			</script>
			 <? } ?>
			 
			  <br /><br />
				&nbsp;&nbsp;
				 <b> Distributors </b>
				<select name="distributors" style="font-family: verdana; font-size: 11px;">
				   <? if(CONFIG_BANK_BASED_TRASACTIONS_CUSTOM_EXPORT_FORAMT != "1") {?>
				 		 <option value="">All</option>
				  <?
				   }
					$distributors = "select userID, username, name, agentType from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and PPType = '' and agentStatus = 'Active' ";				
					if($excludeDistFlag){
					$distributors .= "AND userID!=".$excludeDistId." ";
					}
					$distributors .="order by username";
					$ida = selectMultiRecords($distributors);
					
					echo "<optgroup label='Super Distributors'>";
					for ($j=0; $j < count($ida); $j++)
					{
						if($ida[$j]["agentType"] == "Supper")
						{
							if($ida[$j]["userID"] == $_REQUEST["distributors"])
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>" selected="selected">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
							else
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
						}
						elseif($ida[$j]["agentType"] == "Sub")
						{
						
						}
					}
					echo "</optgroup>";
					
					echo "<optgroup label='Sub Distributors'>";
					for ($j=0; $j < count($ida); $j++)
					{
						if($ida[$j]["agentType"] == "Sub")
						{
							if($ida[$j]["userID"] == $_REQUEST["distributors"])
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>" selected="selected">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
							else
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
						}
					}
					echo "</optgroup>";
				 ?>
				</select>      
				
				<? if(!empty($_REQUEST["dt"])) { ?>
					<input type="hidden" name="dispatchNumber" value="<?=$_REQUEST["dt"]?>" />
				<? }elseif(!empty($_REQUEST["dispatchNumber"])) { ?>
					<input type="hidden" name="dispatchNumber" value="<?=$_REQUEST["dispatchNumber"]?>" />
				<? } ?>
				
				<br /><br />
				  <input type="submit" name="Submit" value="Process">
				  <? if(!empty($msg)) { ?>
					<br /><br />
					<b><?=$msg?></b>
				  <? } ?>
						</td>
					</form>
				</tr>
			</table>
		<?
			}
		?>
	
	<table width="90%" cellpadding="2" cellspacing="0" align="center">
		<!-- Report Header Starts -->
		<tr>
		<?
				if($excludeDistFlag){
				$colSpan3 ="4";
			    }else{
			
				$colSpan3 ="6";
			   }
				?>
			<td colspan="<?=$colSpan3?>" class="reportHeader">
				Payment Transaction 
				<? 
					if(!empty($_REQUEST["dt"]) || !empty($_REQUEST["dispatchNumber"])) 
						echo "(Dispatch Number: ".$openDispatchReportSqlData["dispatchNumber"].")";
				?>
				<br /></td>
				
			<td colspan="3" align="right"><?=date("Y-m-d")?></td>
		</tr>
		<!-- Report Header Ends -->
		<!-- Column Header Starts -->
		<? 	if($transTypeFlag){?>
		<tr>
			<td class="columnHeader">DATE</td>
			<td class="columnHeader">ORDER</td>
			<td class="columnHeader">BENEFICIARY</td>
			<? if(!$excludeDistFlag){?>
			<td colspan="2" class="columnHeader">ID</td>
			<? } ?>
			<td class="columnHeader">SORT CODE</td>
			<td colspan="2" class="columnHeader">ACCOUNT</td>
			<td class="columnHeader">VALUE</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?
			$amountUnderOneBank = 0;
			$totalAmount = 0;
			$totalTrasactions = 0;
			$tids = "";
			$intTotalTransactionCount = 0;
			
			if(!empty($sql))
				$fullRS = SelectMultiRecords($sql);
			for($i=0; $i < count($fullRS); $i++)
			{
					$tids .= $fullRS[$i]["transID"].",";
					
					$lastBankName = $fullRS[$i-1]["bankName"];
					$nextBankName = $fullRS[$i+1]["bankName"];
					$totalAmount += $fullRS[$i]["localAmount"];
					$totalTrasactions += 0;
					if((!empty($lastBankName) && $lastBankName != $fullRS[$i]["bankName"]) || $i == 0)
					{
						$noOfOrders = 0;
						$amountUnderOneBank = 0;
			?>
						<tr>
							<td colspan="9" class="bankName"><?=$fullRS[$i]["bankName"]?></td>
						</tr>
			<?
					}
					$noOfOrders += 1;
					$amountUnderOneBank += $fullRS[$i]["localAmount"];
			?>
			<!-- Bank Name Ends -->
			<!-- Transactions List Starts -->
			<tr>
				<td class="transactions"><?=$fullRS[$i]["transDate"]?></td>
				<td class="transactions"><?=$fullRS[$i]["refNumberIM"]?></td>
				<td class="transactions"><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?></td>
				
				<?
				 	if(!$excludeDistFlag){
						if($fullRS[$i]["IDType"] != "CPF") {
				 ?>
				<td class="transactions"><?=($fullRS[$i]["userType"] == "Business User"?"CPNJ":"CPF")?></td>
				<td class="transactions"><?=$fullRS[$i]["CPF"]?></td>
				<? } else { ?>
				<td class="transactions"><?=(!empty($fullRS[$i]["IDType"]) ? $fullRS[$i]["IDType"] :"&nbsp;")?></td>
				<td class="transactions"><?=(!empty($fullRS[$i]["IDNumber"]) ? $fullRS[$i]["IDNumber"] :"&nbsp;")?></td>
				<? }} ?>
				
				<td class="transactions"><?=(!empty($fullRS[$i]["branchCode"])? $fullRS[$i]["branchCode"] : "&nbsp;" )?></td>
				<td class="transactions"><?=$fullRS[$i]["accNo"]?></td>
				<td class="transactions"><?=$fullRS[$i]["accountType"]?></td>
				<td class="transactions"><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></td>
			</tr>
			<!-- Transactions List Ends -->
			<!-- SubTotal Starts  -->
	<?
			if((!empty($nextBankName) && $nextBankName != $fullRS[$i]["bankName"]) || $i == count($fullRS)-1)
			{
			if($excludeDistFlag){
				$colSpan1 ="2";
				$colSpan2 ="1";
			}else{
			
				$colSpan1 ="3";
				$colSpan2 ="2";
			}
	?>
			<tr>
				<td class="subTotal">&nbsp;</td>
				<td class="subTotal">&nbsp;</td>
				<td colspan="<?=$colSpan1?>" class="subTotal">Number of Orders: <?=$noOfOrders?> </td>
				<td class="subTotal">&nbsp;</td>
				<td colspan="<?=$colSpan2?>" class="subTotal">Total Bank: </td>
				<td class="subTotal"><?=number_format($amountUnderOneBank,2,".",",")?></td>
			</tr>
	<?
			}
	?>
			<!-- SubTotal Ends -->
			<!-- Transactions Data Ends -->
	<?
				$intTotalTransactionCount++;

		}
	}	
?>

<!-- Pick up transaction type code Start -->
<? 	if($excludeDistFlag && $pickupFlag && !$transTypeFlag){ ?>
	
		<tr>
			<td class="columnHeader">DATE</td>
			<td class="columnHeader">ORDER</td>
			<td class="columnHeader">BENEFICIARY</td>
			<td class="columnHeader">Collection Point</td>
			<td class="columnHeader">VALUE</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?
			$totalAmount = 0;
			$totalTrasactions = 0;
			$tids = "";
			$intTotalTransactionCount = 0;
			
			if(!empty($sql))
				$fullRS = SelectMultiRecords($sql);
			for($i=0; $i < count($fullRS); $i++)
			{
					$tids .= $fullRS[$i]["transID"].",";
					
					$totalAmount += $fullRS[$i]["localAmount"];
					$totalTrasactions += 0;
					
		?>
			<tr>
				<td class="transactions"><?=$fullRS[$i]["transDate"]?></td>
				<td class="transactions"><?=$fullRS[$i]["refNumberIM"]?></td>
				<td class="transactions"><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?>
				</td>
				<td class="transactions"><?=$fullRS[$i]["cp_corresspondent_name"]."-".$fullRS[$i]["cp_city"]?></td>
				<td class="transactions"><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></td>
			</tr>
			<!-- Transactions List Ends -->

	<?
				
		}
    ?>


<!-- Pick up transaction type code End-->

<? } ?>

<?
/**
 * If no records to show
 */
 if(count($fullRS) < 1)
 {
?>
	<tr>
			<td class="transactions" align="center" colspan="9">No Records found!</td>
	</tr>
<?
	}
?>

</table>

<br />

	<!-- Report Summary Starts -->
	<table width="500" border="0" align="center" cellpadding="10" cellspacing="0" class="reportSummary">
		<tr>
			<td width="35%" align="right">No. Transactions </td>
			<td width="15%"><?=$i?></td>
			<td width="35%" align="right">Final Total </td>
			<td width="15%"><?=number_format($totalAmount,2,".",",")?></td>
		</tr>
		<tr>
			<td align="right">Exchange</td>
			<td><?=1?></td>
			<td align="right">Total Foreign </td>
			<td><?=number_format($totalAmount,2,".",",")?></td>
		</tr>
	</table>
	<!-- Report Summary Ends -->
	<table width="500" border="0" align="center" cellpadding="10" cellspacing="0" class="reportSummary">
		<tr>
			<td width="50%" align="center">
				<input type="button" value="Print Report" onclick="print();" />
				<?
					if($_POST["Submit"])
					{
				?>
				&nbsp;&nbsp;
				<input type="button" value="Save Report" onclick="document.location='bank-trasactions-report.php?from=<?=$fromDate?>&to=<?=$toDate?>&distributors=<?=$_POST["distributors"]?>&transType=<?=$_POST["transType"]?>&sr=y'" />
				<? } ?>
				
				<? 
					if(empty($_REQUEST["RID"]) && empty($_REQUEST["dt"]) && empty($_REQUEST["dispatchNumber"]))
					{
				?>
					&nbsp;&nbsp;<br /><br />
					<form action="bank-trasactions-report.php" method="post">
						<input type="hidden" name="at" value="<?=$tids?>" />
						<input type="hidden" name="export" value="t" />
						
						<input type="hidden" name="tDay" value="<?=$_REQUEST["tDay"]?>" />
						<input type="hidden" name="tMonth" value="<?=$_REQUEST["tMonth"]?>" />
						<input type="hidden" name="tYear" value="<?=$_REQUEST["tYear"]?>" />
						<input type="hidden" name="fDay" value="<?=$_REQUEST["fDay"]?>" />
						<input type="hidden" name="fMonth" value="<?=$_REQUEST["fMonth"]?>" />
						<input type="hidden" name="fYear" value="<?=$_REQUEST["fYear"]?>" />
						<input type="hidden" name="distributors" value="<?=$_REQUEST["distributors"]?>" />
						<input type="hidden" name="transType" value="<?=$_REQUEST["transType"]?>" />
						<input type="hidden" name="Submit" value="Process">
						
						
						<input type="submit" value="Export" />
					</form>
				<?
					}
				?>
				
				<?
					/**
					 * Export SPB format 
					 * @Ticket #3965
					 */ 
					if(empty($_REQUEST["RID"]) && empty($_REQUEST["dt"]))
					{ /*
				?>
					&nbsp;&nbsp;
					<form action="bank-trasactions-report.php" method="post">
						<input type="hidden" name="at" value="<?=$tids?>" />
						<input type="hidden" name="exportspb" value="t" />
						
						<input type="hidden" name="tDay" value="<?=$_REQUEST["tDay"]?>" />
						<input type="hidden" name="tMonth" value="<?=$_REQUEST["tMonth"]?>" />
						<input type="hidden" name="tYear" value="<?=$_REQUEST["tYear"]?>" />
						<input type="hidden" name="fDay" value="<?=$_REQUEST["fDay"]?>" />
						<input type="hidden" name="fMonth" value="<?=$_REQUEST["fMonth"]?>" />
						<input type="hidden" name="fYear" value="<?=$_REQUEST["fYear"]?>" />
						<input type="hidden" name="distributors" value="<?=$_REQUEST["distributors"]?>" />
						<input type="hidden" name="Submit" value="Process">
						
						
						<input type="submit" value="Export SPB format" />
					</form>
				<?
					*/
					}
				?>
			</td>
		</tr>
	</table>
	
</body>
</html>
