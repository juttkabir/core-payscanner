<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
$dt= date('Y-m-d H:i:s');

if($_GET["Submit"]!= "")
{
	if($_GET["origCountry"] != "")
		$origCountry = $_GET["origCountry"];
	if($_GET["destCountry"] != "")
		$destCountry = $_GET["destCountry"];
	if($_GET["customer"] != "")
		$customer = $_GET["customer"];	
	if($_GET["feeType"] != "")
		$feeType = $_GET["feeType"];	
	if($_GET["paymentMode"] != "")
		$paymentMode = $_GET["paymentMode"];
	
	$submit = $_GET["Submit"];
} 

	$_SESSION["origCountry"] = $_POST["origCountry"];
	$_SESSION["destCountry"] = $_POST["destCountry"];
	$_SESSION["amountRangeFrom"] = $_POST["amountRangeFrom"];
	$_SESSION["amountRangeTo"] = $_POST["amountRangeTo"];
	$_SESSION["Fee"] = $_POST["Fee"];
	$_SESSION["feeType"] = $_POST["feeType"];
	$_SESSION["payinFee"] = $_POST["payinFee"];
	$_SESSION["interval"] = $_POST["interval"];
	$_SESSION["agentID"] = $_POST["agentID"];
	$_SESSION["feeBasedOn"] = $_POST["feeBasedOn"];
	$_SESSION["currencyDest"] = $_POST["currencyDest"];
	$_SESSION["currencyOrigin"] = $_POST["currencyOrigin"];
	$_SESSION["distributor"] = $_POST["distributor"];
	$_SESSION["transactionType2"] = $_REQUEST["transactionType2"];
	
	$_SESSION["paymentMode"] = ( !empty($_POST["paymentMode"]) ? $_POST["paymentMode"] : "" );

if ($_POST["feeID"] == ""){
	
	$backURL = "add-fee.php?msg=Y";
} else {
	$backURL = "update-fee.php?feeID=$_POST[feeID]&msg=Y&origCountry=$origCountry&destCountry=$destCountry&customer=$customer&feeType=$feeType&Submit=$submit";
}
if (trim($_POST["origCountry"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF1);
	redirect($backURL);
}
if (trim($_POST["destCountry"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF8);
	redirect($backURL);
}

if (trim($_POST["amountRangeFrom"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF2);
	redirect($backURL);
}
if (trim($_POST["amountRangeTo"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF3);
	redirect($backURL);
}
if ((trim($_POST["amountRangeTo"]) <= trim($_POST["amountRangeFrom"])) && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF4);
	redirect($backURL);
}
if (trim($_POST["Fee"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF5);
	redirect($backURL);
}
if (trim($_POST["feeType"]) == "" && $_POST['FeeFor'] == "deal_contract"){
	insertError(IMF9);
	redirect($backURL);
}

if (trim($_POST["feeType"]) == "agentDenom" || trim($_POST["feeType"]) == "agentBased" || trim($_POST["feeType"]) == "distributorBased" ){
	if($_POST["agentID"] == "")
	{
			insertError(IMF10);
			redirect($backURL);
	}
}

if (trim($_POST["feeType"]) == "collectionPoint" && CONFIG_FEE_BASED_COLLECTION_POINT ==1){
	if (trim($_POST["distributor"]) == ""){
		insertError(IMF16);
		redirect($backURL);
	}
	if($_POST["agentID"] == "")
	{
			insertError(IMF15);
			redirect($backURL);
	}
}
// Added by Niaz Ahmad Against Ticket # 2538 at 24-10-2007
  
 if (CONFIG_FEE_BASED_DROPDOWN == "1") { 
  if($_POST["feeBasedOn"]== "Generic"){
  	$generic=$_POST["feeBasedOn"];
  	$transactionType="";
}elseif ($_POST["feeBasedOn"]== "transactionType"){
	   	$transactionType=$_POST["transactionType2"];
	    $generic=$_POST["feeBasedOn"];
}elseif($_POST["feeBasedOn"]== "customer" && CONFIG_CUSTOMER_CATEGORY == '1'){
	$generic=$_POST["feeBasedOn"];
  	$transactionType = $_POST["catID"];
}
}
else{
$generic="Generic";
$transactionType="";
}

if($_SESSION["feeType"] == "denominator" || $_SESSION["feeType"] == "agentDenom")
{
	if ($_SESSION["interval"] == '') {
		insertError(IMF12);
		redirect($backURL);
		exit;
	}
}
	
	/**
	 * Updating the query to feeBasedOn = $generic
	 * as this field set through config var
	 */ 
	 
	$strAdditionalCondition = (!empty($_REQUEST["transactionType2"])? " and transactionType = '".$_REQUEST["transactionType2"]."'": "");
	 
	if(isExist("select manageFeeID  from ".TBL_MANAGE_FEE." where feeType = '$_POST[feeType]' ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?" and paymentMode = '".$_POST["paymentMode"]."' ":"") ."  and origCountry = '$_POST[origCountry]' and destCountry = '$_POST[destCountry]' and '$_POST[currencyDest]' and '$_POST[currencyOrigin]' and agentNo = '$_POST[agentID]' and feeBasedOn = '$generic' and ((amountRangeFrom > '".checkValues($_POST["amountRangeFrom"])."' and amountRangeFrom < '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeFrom = '".checkValues($_POST["amountRangeTo"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom < '".checkValues($_POST["amountRangeFrom"])."' and amountRangeTo > '".checkValues($_POST["amountRangeFrom"])."'))". $strAdditionalCondition)) {

	//insertError(IMF11." * ".__LINE__);
	redirect($backURL);
	exit;
	}
if(CONFIG_FEE_CURRENCY == '1')
{
	
	if($_POST["currencyOrigin"] == "")
	{
		insertError(IMF13);
		redirect($backURL);
		exit;
	}
	if($_POST["currencyDest"] == "")
	{
		insertError(IMF14);
		redirect($backURL);
		exit;
	}
}
switch ($_POST["feeType"]) {
	case "percent":
		$percent = (checkValues($_POST["amountRangeTo"]) * checkValues($_POST["Fee"])) / 100;
		$rangeToWithFee = checkValues($_POST["amountRangeTo"]) + $percent;
		break;
	default:
		$rangeToWithFee = checkValues($_POST["amountRangeTo"]) + checkValues($_POST["Fee"]);
		break;
}
if($_SESSION["feeType"] == "denominator" || $_SESSION["feeType"] == "agentDenom")
{
	if ($_POST["feeID"] != ""){
		
		$deleteQuery = "delete from ".TBL_IMFEE." where  manageID = '".$_POST["feeID"]."'";
		mysql_query($deleteQuery);	
		
		$manageId = $_POST["feeID"];
	
	}
	// This formula change by Niaz Ahmad at 01-11-2007 against issue raise against ticket # 2538
	 $rangeDifference=$_SESSION["amountRangeTo"]- $_SESSION["amountRangeFrom"];
	 $loop = $rangeDifference/$_SESSION["interval"];
	 $loop=ceil($loop);
	 		
		/*% Implement during the testing phase checkout %*/
		
		 if(CONFIG_CLIENT_NAME == 'DemoFX') {
		 $manageQuery = "insert into ".TBL_MANAGE_FEE." 
		(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, regularFee, feeType, payinFee, agentNo, intervalUsed, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").",FeeFor,Charges,PaymentCurrency,PaymentType,PayPer,PaymentDate) 
		values('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$_POST["interval"]."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."' ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") ." , '".$_POST["FeeFor"]."', '".$_POST["Fee"]."', '".$_POST["PaymentCurrency"]."', '".$_POST["Payment_Type"]."', '".$_POST["PayPer"]."','".$dt."' )";
		
		 
		 
		 }else{
		$manageQuery = "insert into ".TBL_MANAGE_FEE." 
		(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, regularFee, feeType, payinFee, agentNo, intervalUsed, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").") 
		values('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$_POST["interval"]."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."' ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") ." )";
		}
		insertInto($manageQuery);
		$manageId = @mysql_insert_id();
		
		if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
			($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
		{
			$updateFeeSql = "update ".TBL_MANAGE_FEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where manageFeeID = $manageId";
			update($updateFeeSql);
		}	
		/*% Implementation Completed %*/
		
		for($i=0; $i < $loop ; $i++)
		{ 
		
			if($_SESSION["amountRangeFrom"] == 1)
			$_SESSION["amountRangeFrom"] = 0;
			
			$lower = $_SESSION["amountRangeFrom"] + ($_SESSION["interval"]*$i);
			$upper = $_SESSION["amountRangeFrom"] + ($_SESSION["interval"]*($i+1));
		 
		  if( $upper > $_SESSION["amountRangeTo"]){
		  	$upper=$_SESSION["amountRangeTo"];
		  	}
			if($i > 0)
			{
				$lower = $lower + 0.01;
			}else{
				if($lower == 0)
					$lower = 1;
				}
			$payinFees = (($i + 1) * $_SESSION["payinFee"]);
			
			$fees = (($i + 1) * $_SESSION["Fee"]);
			
			$rangeToWithFee = $upper + $fees;
			
			if(isExist("select feeID  from ".TBL_IMFEE." where 
			origCountry = '".$_POST["origCountry"]."' and destCountry = '".$_POST["destCountry"]."' 
			and agentNo = '".$_POST["agentID"]."' and feeBasedOn = '".$_POST["feeBasedOn"]."' and transactionType = '".$_POST["transactionType2"]."'
			".(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?" and paymentMode = '".$_POST["paymentMode"]."' ":"") ."
			and currencyOrigin = '".$_POST["currencyOrigin"]."' and FeeFor <> 'payments' and currencyDest = '".$_POST["currencyDest"]."'
			and ((amountRangeFrom > '".$lower."' and amountRangeFrom < '".$upper."') OR (amountRangeFrom = '".$lower."' OR amountRangeFrom = '".$upper."' OR amountRangeTo = '".$lower."' OR amountRangeTo = '".$upper."') OR (amountRangeFrom < '".$lower."' and amountRangeTo > '".$lower."'))"))
			{
				insertError(IMF11);
			
				redirect($backURL);
				exit;
			}
			 if(CONFIG_CLIENT_NAME == 'DemoFX') {
            	$Querry_Sqls = "insert into ".TBL_IMFEE." 
			(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, Fee, feeType, payinFee, agentNo, manageID, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").",FeeFor,Charges,PaymentCurrency,PaymentType,PayPer,PaymentDate)  
			values ('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".$lower."', '".$upper."', '".$rangeToWithFee."', '".$fees."', '".checkValues($_SESSION["feeType"])."', '".$payinFees."', '".$_POST["agentID"]."','".$manageId."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."'". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") .", '".$_POST["FeeFor"]."', '".$_POST["Fee"]."', '".$_POST["PaymentCurrency"]."', '".$_POST["Payment_Type"]."', '".$_POST["PayPer"]."' ,'".$dt."' )";
		 }else{
			$Querry_Sqls = "insert into ".TBL_IMFEE." 
			(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, Fee, feeType, payinFee, agentNo, manageID, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").")  
			values ('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".$lower."', '".$upper."', '".$rangeToWithFee."', '".$fees."', '".checkValues($_SESSION["feeType"])."', '".$payinFees."', '".$_POST["agentID"]."','".$manageId."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."'". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") ." )";
		}
			insertInto($Querry_Sqls);
			$feeid = @mysql_insert_id();
			if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
				($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
			{
				$updateFeeSql = "update ".TBL_IMFEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where feeID = $feeid";
				update($updateFeeSql);
			}
			
			$descript = "Transaction Fee is added";
			activities($_SESSION["loginHistoryID"],"INSERTION",$feeid,TBL_IMFEE,$descript);
		}
 
			$_SESSION["origCountry"] = "";
			$_SESSION["destCountry"] = "";
			$_SESSION["Country"] = "";
			$_SESSION["amountRangeFrom"] = "";
			$_SESSION["amountRangeTo"] = "";
			$_SESSION["Fee"] = "";
			$_SESSION["feeType"] = "";
			$_SESSION["payinFee"] = "";
			$_SESSION["interval"] = "";
			$_SESSION["agentID"]  = "";
			$_SESSION["feeBasedOn"]  = "";
			$_SESSION["paymentMode"] = "";
			$_SESSION["currencyDest"] = "";
			$_SESSION["currencyOrigin"] = "";
			$_SESSION["transactionType2"] = "";
			if($_POST['FeeFor']=='payments' && CONFIG_CLIENT_NAME == 'DemoFX')	{
			insertError(IMF6A); 
			}else{
			insertError(IMF6);
			}
	
	}else{
		
		if ($_POST["feeID"] == ""){
		
			$strAdditionalCondition = (!empty($_REQUEST["transactionType2"])? " and transactionType = '".$_REQUEST["transactionType2"]."'": "");
			if(CONFIG_CUSTOMER_CATEGORY == 1)
				$stCatID = (!empty($_REQUEST["catID"])? " and transactionType = '".$_REQUEST["catID"]."'": "");
			 if($_POST['FeeFor']=='deal_contract' && CONFIG_CLIENT_NAME == 'DemoFX')	{	
		if(isExist("select feeID  from ".TBL_IMFEE." where feeType = '$_POST[feeType]'  ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?"  and paymentMode =  '".$_POST["paymentMode"]."' ":"") ."  and origCountry = '$_POST[origCountry]' and destCountry = '$_POST[destCountry]' and currencyOrigin = '".$_POST["currencyOrigin"]."' and currencyDest = '".$_POST["currencyDest"]."' and agentNo = '$_POST[agentID]' and feeBasedOn = '$generic' and ((amountRangeFrom > '".checkValues($_POST["amountRangeFrom"])."' and amountRangeFrom < '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeFrom = '".checkValues($_POST["amountRangeTo"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom < '".checkValues($_POST["amountRangeFrom"])."' and amountRangeTo > '".checkValues($_POST["amountRangeFrom"])."'))". $strAdditionalCondition.$stCatID))
			{
				insertError(IMF11);
			
				redirect($backURL);
				exit;
			}
			
			
			
		}	elseif($_POST['FeeFor']=='payments' && CONFIG_CLIENT_NAME == 'DemoFX'){
		
		if(isExist("select feeID  from ".TBL_IMFEE." where Charges = '$_POST[feeType]'    and origCountry = '$_POST[origCountry]' and FeeFor = '$_POST[FeeFor]' and PaymentCurrency = '$_POST[destCountry]' and PaymentType = '$_POST[currencyOrigin]' and PayPer = '$_POST[currencyDest]'"))
			{
				insertError(IMF11);
			
				redirect($backURL);
				exit;
			}
		
		
		}else{
		
		if(isExist("select feeID  from ".TBL_IMFEE." where feeType = '$_POST[feeType]'  ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?"  and paymentMode =  '".$_POST["paymentMode"]."' ":"") ."  and origCountry = '$_POST[origCountry]' and destCountry = '$_POST[destCountry]' and currencyOrigin = '".$_POST["currencyOrigin"]."' and currencyDest = '".$_POST["currencyDest"]."' and agentNo = '$_POST[agentID]' and feeBasedOn = '$generic' and ((amountRangeFrom > '".checkValues($_POST["amountRangeFrom"])."' and amountRangeFrom < '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeFrom = '".checkValues($_POST["amountRangeTo"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeFrom"])."' OR amountRangeTo = '".checkValues($_POST["amountRangeTo"])."') OR (amountRangeFrom < '".checkValues($_POST["amountRangeFrom"])."' and amountRangeTo > '".checkValues($_POST["amountRangeFrom"])."'))". $strAdditionalCondition.$stCatID))
			{
				insertError(IMF11);
			
				redirect($backURL);
				exit;
			}
		
		
		}
			
    if(CONFIG_CLIENT_NAME == 'DemoFX') {
	$manageQuery = "insert into ".TBL_MANAGE_FEE." 
	(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, regularFee, feeType, payinFee, agentNo, intervalUsed, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").",FeeFor,Charges,PaymentCurrency,PaymentType,PayPer,PaymentDate) 
	values('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$_POST["interval"]."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."' ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") .", '".$_POST["FeeFor"]."', '".$_POST["Fee"]."', '".$_POST["PaymentCurrency"]."', '".$_POST["Payment_Type"]."', '".$_POST["PayPer"]."','".$dt."' )";
	
	
	}else{
	$manageQuery = "insert into ".TBL_MANAGE_FEE." 
	(origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, regularFee, feeType, payinFee, agentNo, intervalUsed, feeBasedOn, transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").") 
	values('".$_POST["origCountry"]."','".$_POST["destCountry"]."',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$_POST["interval"]."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."' ". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") ." )";
	
	}
	insertInto($manageQuery);
	//debug($manageQuery, true);
	$manageId = @mysql_insert_id();
	
	if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
		($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
	{
		$updateFeeSql = "update ".TBL_MANAGE_FEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where manageFeeID = $manageId";
		update($updateFeeSql);
		
		//debug($updateFeeSql, true);
	}	
                  if(CONFIG_CLIENT_NAME == 'DemoFX') {
				  
				   $Querry_Sqls = "insert into ".TBL_IMFEE."
			 (origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, Fee, feeType, payinFee, agentNo, manageID,feeBasedOn,transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").",FeeFor,Charges,PaymentCurrency,PaymentType,PayPer,PaymentDate)  
			 values ('$_POST[origCountry]','$_POST[destCountry]',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$manageId."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."'". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") .", '".$_POST["FeeFor"]."', '".$_POST["Fee"]."', '".$_POST["PaymentCurrency"]."', '".$_POST["Payment_Type"]."', '".$_POST["PayPer"]."','".$dt."'  )";
            
				  }else{
			
			 $Querry_Sqls = "insert into ".TBL_IMFEE."
			 (origCountry, destCountry, amountRangeFrom, amountRangeTo, rangeToWithFee, Fee, feeType, payinFee, agentNo, manageID,feeBasedOn,transactionType, currencyOrigin, currencyDest". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode":"").")  
			 values ('$_POST[origCountry]','$_POST[destCountry]',  '".checkValues($_POST["amountRangeFrom"])."', '".checkValues($_POST["amountRangeTo"])."', '".$rangeToWithFee."', '".checkValues($_POST["Fee"])."', '".checkValues($_POST["feeType"])."', '".$_SESSION["payinFee"]."', '".$_POST["agentID"]."', '".$manageId."','".$generic."','".$transactionType."', '".$_POST["currencyOrigin"]."', '".$_POST["currencyDest"]."'". (CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", '".$_POST["paymentMode"]."'":"") ." )";
            }
			insertInto($Querry_Sqls);
			
			$feeid = @mysql_insert_id();
			$descript = "Transaction Fee is added";
			activities($_SESSION["loginHistoryID"],"INSERTION",$feeid,TBL_IMFEE,$descript);
			
			
			if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
				($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
			{
				$updateFeeSql = "update ".TBL_IMFEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where feeID = $feeid";
				update($updateFeeSql);
				
				//debug($updateFeeSql, true);
			}
			
			
			$_SESSION["origCountry"] = "";
			$_SESSION["destCountry"] = "";
			$_SESSION["Country"] = "";
			$_SESSION["amountRangeFrom"] = "";
			$_SESSION["amountRangeTo"] = "";
			$_SESSION["Fee"] = "";
			$_SESSION["feeType"] = "";
			$_SESSION["payinFee"] = "";
			$_SESSION["interval"] = "";
			$_SESSION["agentID"]  = "";
			$_SESSION["feeBasedOn"]  = "";
			$_SESSION["paymentMode"] = "";
			$_SESSION["currencyDest"] = "";
			$_SESSION["currencyOrigin"] = "";
			$_SESSION["transactionType2"] = "";
			if($_POST['FeeFor']=='payments' && CONFIG_CLIENT_NAME == 'DemoFX')	{
			insertError(IMF6A); 
			}else{
			insertError(IMF6); 
			}
		} else {
		
			$manageQuery = "update ".TBL_MANAGE_FEE." set 
				origCountry = '".$_POST["origCountry"]."',
				destCountry = '".$_POST["destCountry"]."',
				amountRangeFrom = '".checkValues($_POST["amountRangeFrom"])."',
				amountRangeTo =  '".checkValues($_POST["amountRangeTo"])."',
				rangeToWithFee = '".$rangeToWithFee."',
				regularFee =  '".checkValues($_POST["Fee"])."',
				feeType =  '".checkValues($_POST["feeType"])."',
				payinFee =  '".$_POST["payinFee"]."',
				agentNo =  '".$_POST["agentID"]."',
				intervalUsed = '".$_POST["interval"]."',
				feeBasedOn = '".$generic."' ,
				transactionType = '".$transactionType."',
				intervalUsed = '".$_POST["interval"]."',
				currencyOrigin = '".$_POST["currencyOrigin"]."',
				currencyDest = '".$_POST["currencyDest"]."'".
				(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode = '".$_POST["paymentMode"]."'":"")
				." where manageFeeID = '".$_POST["feeID"]."'	";
				
				//echo $manageQuery;
				
				update($manageQuery);
				///die();
				$manageId = $_POST["feeID"];
				
				if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
					($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
				{
					$updateFeeSql = "update ".TBL_MANAGE_FEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where manageFeeID = $manageId";
					update($updateFeeSql);
					
					//debug($updateFeeSql, true);
				}
				
				
		
			$Querry_Sqls = "update ".TBL_IMFEE." 
			set origCountry='".$_POST["origCountry"]."', destCountry='".$_POST["destCountry"]."', 
			amountRangeFrom='".checkValues($_POST["amountRangeFrom"])."', amountRangeTo='".checkValues($_POST["amountRangeTo"])."', 
			rangeToWithFee = '".$rangeToWithFee."', Fee='".checkValues($_POST["Fee"])."', 
			feeType='".checkValues($_POST["feeType"])."', payinFee='".$_POST["payinFee"]."', 
			agentNo = '".$_POST["agentID"]."', feeBasedOn = '".$generic."', 
			transactionType = '".$transactionType."',
			currencyOrigin = '".$_POST["currencyOrigin"]."', currencyDest = '".$_POST["currencyDest"]."'". 
			(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == "1"?", paymentMode = '".$_POST["paymentMode"]."'":"")
			." where manageID = '".$_POST["feeID"]."'";
			update($Querry_Sqls);
		
			if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
				($_REQUEST["feeType"] == "denominator" || $_REQUEST["feeType"] == "agentDenom" || $_REQUEST["feeType"] == "agentBased" ))
			{
				$updateFeeSql = "update ".TBL_IMFEE." set feeSubType = '".$_REQUEST["feeSubType"]."' where manageID = ".$_POST["feeID"];
				update($updateFeeSql);
				
			}
			
			
			$descript ="Transaction fee is updated";
			activities($_SESSION["loginHistoryID"],"UPDATION",$_POST[feeID],TBL_IMFEE,$descript);
			insertError(IMF7);
		}
}

	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		include("remoteFee.php");
		
	}
$backURL .= "&success=Y";
redirect($backURL);
?>
