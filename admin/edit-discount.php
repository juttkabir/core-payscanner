<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$transID = (int) $_GET["transID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

	$flag = 0;
	if($_GET["flag"] == 1)
	{
		$msg="Discount can not be more than Fee";
 		$flag = 1;
	}
$_SESSION["discounted_amount"]="";

if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
	
	//$_SESSION["discountType"]		= $contentTrans["discountType"];
	$_SESSION["transAmount"]		= $contentTrans["transAmount"];
	$_SESSION["localAmount"]		= $contentTrans["localAmount"];
	$_SESSION["IMFee"]				= $contentTrans["IMFee"];
	$_SESSION["exchangeRate"]		= $contentTrans["exchangeRate"];
		
}
if($_POST["discounted_amount"] != ""){	
	
	if($_POST["discounted_amount"] > $contentTrans["IMFee"])
	{
		$_SESSION["discounted_amount"] = $_POST["discounted_amount"];
 		$msg="Discount can not be more than Fee";
 		$flag = 1;
			
	}else{	
	
			$_SESSION["discounted_amount"] = $_POST["discounted_amount"];
			$_SESSION["discountType"] 		= $_POST["discountType"];
			if($_POST["discountType"] == "amount"){
				
			$_SESSION["IMFee"] = $_SESSION["IMFee"] - $_POST["discounted_amount"];
			
			$_SESSION["transAmount"]= $contentTrans["transAmount"]+ $_POST["discounted_amount"];
			
			$_SESSION["localAmount"]=  ($contentTrans["transAmount"]+$_POST["discounted_amount"])*$_SESSION["exchangeRate"];	
		}
			else
			{
			$_SESSION["IMFee"] = $_SESSION["IMFee"] - $_POST["discounted_amount"];	
		}
	}
}elseif($_POST["discountType"] != "" && $_POST["discounted_amount"] == ""){
	$flag = 1;
	$msg = "Please Enter Discounted Amount First!";
	}
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {
	
	if(theForm.discounted_amount.value > <?=$_SESSION["IMFee"]?>)){
    	alert("Discount is more than Fee");
        theForm.discounted_amount.focus();
        return false;
    }
    return true;
}		
</script>	
<script language="javascript" src="./styles/admin.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}  
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
	<form name="form1" method="post" action="edit-discount-conf.php?transID=<?=$transID?>">
	<input type="hidden" name="transAmount" value="<?=$_SESSION["transAmount"]?>">
	<input type="hidden" name="localAmount" value="<?=$_SESSION["localAmount"]?>">
	<input type="hidden" name="IMFee" value="<?=$_SESSION["IMFee"]?>">
	<input type="hidden" name="discounted_amount" value="<?=$_SESSION["discounted_amount"]?>">
	<input type="hidden" name="discountType1" value="<?=$_SESSION["discountType"]?>">
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">View transaction for discount.</font></b></td>
  </tr>
  <? if($flag == 1){ 
  ?>
  <tr>
    <td align="center"><b><font color="RED" size="2"><? echo($msg);?></font></b></td>
  </tr>
  <? } ?>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details <?=$_POST["discounted_amount"]?></legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font class="style2">Transaction No </font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong>
                </td>
                <td width="100" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="150" height="20"><? 
				 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
				{
					if($authoriseDate != '0000-00-00 00:00:00')
						echo date("F j, Y", strtotime("$authoriseDate"));				
				}
				?></td>
              </tr>			
              <? if($contentTrans["transStatus"] == "Failed" || $contentTrans["transStatus"] == "Picked up" || $contentTrans["transStatus"] == "Rejected" || $contentTrans["transStatus"] == "Cancelled" || $contentTrans["transStatus"] == "Credited") 
              {
              ?>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90"><? echo $contentTrans["transStatus"]; ?> Date </font></td>
                <td width="196" height="20"><? if($contentTrans["transStatus"] == "Failed"){echo(date("F j, Y", strtotime($contentTrans["failedDate"])));}
                	elseif($contentTrans["transStatus"] == "Rejected"){echo(date("F j, Y", strtotime($contentTrans["rejectDate"])));}
                	elseif($contentTrans["transStatus"] == "Cancelled"){echo(date("F j, Y", strtotime($contentTrans["cancelDate"])));}
                	else echo(date("F j, Y", strtotime($contentTrans["deliveryDate"])));
                	?></td>
                <td width="148" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="142"> &nbsp;</td>
              </tr>
            <? } ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Customer Details </legend>
              <table width="650" border="0">
                <? 
		if($createdBy == 'CUSTOMER')
		{
			$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			$custId = $customerContent["c_id"];
			$custName = $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"];
			$custAddress = $customerContent["c_address"] . " " . $customerContent["c_address2"];
			$custZip = $customerContent["c_zip"];
			$custCountry = $customerContent["c_country"];
			$custPhone = $customerContent["c_phone"];
			$custEmail = $customerContent["c_email"];
			
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			
			$benId = $benificiaryContent["benID"];
			$benName = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
			$benAdd = $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"];
			$benZip = $benificiaryContent["Zip"];
			$benCountry = $benificiaryContent["Country"];
			$benPhone = $benificiaryContent["Phone"];
			$benEmail = $benificiaryContent["Email"];
			
			
			
		}elseif($createdBy != 'CUSTOMER')
		{
			$queryCust = "select * from ".TBL_CUSTOMER." where customerID ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			$custId = $customerContent["customerID"];
			$custName = $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"];
			$custAddress = $customerContent["Address"] . " " . $customerContent["Address1"];
			$custZip = $customerContent["Zip"];
			$custCountry = $customerContent["Country"];
			$custPhone = $customerContent["Phone"];
			$custEmail = $customerContent["email"];
			
			$queryBen = "select benID, Title, firstName, middleName, lastName, Address, Address1, City, State, Zip,Country, Phone, email  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			
			$benId = $benificiaryContent["benID"];
			$benName = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
			$benAdd = $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"];
			$benZip = $benificiaryContent["Zip"];
			$benCountry = $benificiaryContent["Country"];
			$benPhone = $benificiaryContent["Phone"];
			$benEmail = $benificiaryContent["Email"];
		}
			if($custId != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $custName; ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $custAddress; ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $custZip; ?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $custCountry; ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $custPhone; ?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $custEmail; ?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  			  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			if($benId != "")
			{
		?>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                        <td height="20" colspan="2"><strong><? echo $benName; ?></strong></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
                        <td height="20" colspan="2"><? echo $benAdd; ?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" height="20"><? echo $benZip; ?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benCountry; ?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" height="20"><? echo $benPhone; ?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benEmail; ?></td>
                      </tr>
                      <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
					?>
                      <tr>
                        <td colspan="4" class="style2">Beneficiary Bank Details </td>
                        
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				if($benificiaryContent["Country"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($benificiaryContent["Country"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                          </font></td>
                        <td width="200"><?
				if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
				{
				?>
                          <? echo $benBankDetails["ABACPF"]; ?>
                          <?
				}
				?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
<td width="100" height="20"  align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font>&nbsp;</td>
                        <td width="200" height="20">&nbsp;  </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Agent Name</font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong></td>
                        <td width="100" align="right"><font class="style2">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><strong><? echo $senderAgentContent["cp_branch_address"]; ?></strong>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?> </strong>                       </td>
						<td width="150" align="right"><font class="style2">Branch Code</font></td>
						<td width="200" colspan="3"><strong><? echo $senderAgentContent["cp_ria_branch_code"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Compnay </font> </td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?> </strong>                       </td>
						<td width="100" align="right"><font class="style2">Country</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>                       
                      </tr>
					  <tr> 
						<td width="150" align="right"><font class="style2">Phone</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
					    <td width="100" height="20" align="right"><font class="style2">City</font></td>
                        <td width="200" height="20"><strong><?  echo $senderAgentContent["cp_city"]; ?></strong></td>
					  </tr>
					  
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          
          
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo $_SESSION["transAmount"];?>                </td>
              </tr>
			  <tr>
			    <td height="20" align="right"><font color="#005b90"><? echo $systemPre;?> Fee</font></td>
			    <td height="20"><? echo $_SESSION["IMFee"];?> </td>
                <td width="100" height="20" align="right"><font class="style2">Local Amount </font></td>
                <td width="200" height="20"><strong><? echo $_SESSION["localAmount"];?>                   
				<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>
				             </strong> </td>
			  </tr>
			   <tr>
                <td align="right" ><font color="#005b90"> Discount Request </font></td>
                <td width="200" height="20" colspan="2"><? echo $contentTrans["discountRequest"];?></td>
                
         </tr>
			  <? if($contentTrans["transType"] == "Bank Transfer"){
            	?>
        	<tr> 
          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
        	
        	
        	<tr>
                
                <td width="150" height="20" align="right"><font color="#005b90">Discounted Amount</font></td>
          	<?
          			if (CONFIG_TOTAL_FEE_DISCOUNT == '1') {
          				$_SESSION["discounted_amount"] = $contentTrans["discountRequest"];
          				$readonly = "readonly";
          			} else {
          				$readonly = "";	
          			}
          	?>
                <td width="200" height="20"><input type="text" name="discounted_amount" value="<?=$_SESSION["discounted_amount"]?>" <? echo $readonly; ?>></td>
                <td width="100" align="right"><font color="#005b90">Credit to**</font></td>
              	<td width="200" height="20">
              		<select name="discountType" onChange="document.form1.action='edit-discount.php?transID=<? echo $_GET["transID"]?>'; document.form1.submit();">
                            <option value="">-select one-</option>
                            <option value="amount" <? if($_SESSION["discountType"] == 'amount'){echo("Selected");}?>>Amount Sent to Beneficiary</option>
                            <option value="agent" <? if($_SESSION["discountType"] != 'amount'){echo("Selected");}?>>Agent/Sender Account</option>                           
                    </select>
                            <script language="JavaScript">
         	SelectOption(document.form1.discountType, "<?=$_SESSION["discountType"]?>");
                                </script></td>
              </tr>
        	
        	
        	
        	
        	
              <tr>
                <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" height="20"><? echo $contentTrans["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
                <? if(CONFIG_CASH_PAID_CHARGES_ENABLED && $contentTrans["moneyPaid"]=='By Cash')
							  {
							  	?>
								 
								  <td align="right"><font color="#005b90"> Cash Handling Charges </font></td>
								  <td><? echo $contentTrans["cashCharges"];?></td>
								 
					  <? 
					  			}
					  		else{?>
					  			<td>&nbsp;</td>
					  			<td>&nbsp;</td>
					  			<? } ?>
	
              </tr>
              
              <? if ($contentTrans["remarks"] != ""){ ?>
              <tr>
                <td align="right" ><font color="#005b90"> Remarks </font></td>
                <td width="200" height="20" colspan="2"><? echo $contentTrans["remarks"];?></td>
                
              </tr>
              <? } ?>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?> </font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>	              
                <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>
					   </tr>
             
             <tr>
             	
                <td align="left" colspan="4"><font color="#000000">  **Credit to Amount Sent to Beneficiary adds Discounted Amount to the Amount being Sent to the Beneficiary. </font></td>  
              </tr> 
					  <tr>
                <td align="left" colspan="4"><font color="#000000">  **Credit to Agent/Sender Account adds Discounted Amount to the Sender's Account if it has sender Account and to Agent's Account otherwise. </font></td>  
              </tr> 
             
            </table>
            </fieldset></td>
          </tr>
		  <tr><td align="center" height="30">
                          <div align="center">
                            <input type="submit" name="Submit" value="Send"">
                          </div>
                        </form></td></tr>
		  
  <?
  if($contentTrans["moneyPaid"] == 'By Credit Card' || $contentTrans["moneyPaid"] == 'By Debit Card')
  {
  $benCreditCardDetail = selectFrom("select * from  cm_cust_credit_card where customerID  ='".$contentTrans["customerID"]."'");
  ?>
					  
					  <td colspan="4"><fieldset>
                    <legend class="style2">Credit/Debit Card Details </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
                        <td width="196" height="20" align="left">
						<? echo $benCreditCardDetail["cardName"];?>
						</td>
                        <td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
                        <td width="183" height="20" align="left">
						<? 
						$credCardNo = "************". substr($benCreditCardDetail["cardNo"],12,4);
						echo $credCardNo;?>
						</td>
                      </tr>
                      <tr>
                        <td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
                        <td width="196" height="20" align="left">
						<? 
						echo $ExpirtDagte =  $benCreditCardDetail["expiryDate"];
						//echo date("F j, Y", strtotime("$ExpirtDagte"));
						?>
						</td>
                        <td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
                        <td width="183" height="20" align="left" title="For european Countries only">
						<? echo $benCreditCardDetail["cardCVV"];?>
						</td>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					  <tr>
					  <td colspan="4">&nbsp;</td>
					  </tr>		
<?
}
?>					  			  
        </table></td>
    </tr>

</table>
</body>
</html>