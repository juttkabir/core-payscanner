<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//$agentType = getAgentType();
///////////////////////History is maintained via method named 'activities'
$setRight = $_POST["setRight"];
$country = $_POST["country"];
$rightValue = $_POST["rightValue"];
$rightName = $_POST["rightsOption"];
$queryRights = "select * from setRoutineRight where country = '".$country."' AND rightName  = '".$rightName."'";
$rightsData = selectFrom($queryRights);

if($_POST["Submit"] == "Submit")
{
	if($rightsData!="" ){
		$updateQuery = "Update setRoutineRight set rightValue = '".$rightValue."', rightName = '".$rightName."' where country = '".$country."'";
		update($updateQuery);
		$msg = "Routine for '".$country."' has been Updated.";
	}else{
		$insertQuery = "Insert into setRoutineRight(country, rightValue, rightName, updated) values('".$country."','".$rightValue."','".$rightName."','".date("Y-d-m")."')";
		insertInto($insertQuery);
		$msg = "Routine for '".$country."' has been Created.";
	}
/*	if($flagIbanBank = "Y"){
		if($rightValue = "normalRtn")
			$ibanOrBank = "iban";
		else
			$ibanOrBank = "bank";

			$updateBenQ   = "Update beneficiary set IbanOrBank = '".$ibanOrBank."', rightName = '".$rightName."' where country = '".$country."'";
	}*/
}
else{
	$rightValue	= $rightsData["rightValue"];
	$rightName	= $rightsData["rightName"];
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {
	if(theForm.country.value == ""){
    	alert("Please select a right.");
        theForm.country.focus();
        return false;
    }
	if(theForm.rightValue.value == "" || IsAllSpaces(theForm.rightValue.value) || theForm.rightValue.value >24 || theForm.rightValue.value < 0){
    	alert("Please provide Start Time (between 0-24).");
        theForm.rightValue.focus();
        return false;
    }
	if(theForm.rightValue1.value == "" || IsAllSpaces(theForm.rightValue1.value) || theForm.rightValue1.value >24 || theForm.rightValue1.value <0){
    	alert("Please provide End Time (between 0-24).");
        theForm.rightValue1.focus();
        return false; 
    }
	if(eval(theForm.rightValue1.value) < eval(theForm.rightValue.value)){
    	alert("Start time should be less than End time (in hrs).");
        theForm.rightValue.value="";
        theForm.rightValue1.value="";
        theForm.rightValue.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
 
    
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar"><b><strong><font class="topbar_tex">Add/Update Routine Rights</font></strong></b></td>
	</tr>
	<tr>
		<td><table width="70%"  border="0">
				<tr>
					<td><fieldset>
						<legend class="style2">Add/Update Routine Rights </legend>
						<br>
						<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
							<form name="addRoutineRight" id="addRoutineRight" method="post" onSubmit="return checkForm(this);">
								<? if($msg!= "")
			  {
			  ?>
								<tr align="center">
									<? if (CLIENT_NAME == "Spinzar") { ?>
									<td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
									<? } else { ?>
									<td colspan="2" class="tab-r"><? echo $msg?><br></td>
									<? } ?>
								</tr>
								<?
			  }
			  
			  ?>
								<tr>
									<td width="35%" align="right" bgcolor="#DFE6EA"> Right Name</td>
									<td width="65%" bgcolor="#DFE6EA"><SELECT name="rightsOption" id="rightsOption" style="font-family:verdana; font-size: 11px">
											<OPTION value="Create Transaction">Transaction Routine</OPTION>
										</SELECT>
										<script language="JavaScript">
         	SelectOption(document.forms[0].rightsOption, "<?=$rightsName?>");
                                </script></td>
								</tr>
								<tr>
									<td width="35%" align="right" bgcolor="#DFE6EA"><input type="hidden" name="setRight" value="Y">
										Select Country</td>
									<td width="65%" bgcolor="#DFE6EA">
										<SELECT id="country" name="country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].setRight.value = ''; document.forms[0].submit();">
																<option value="">- Select Country -</option>
																<?	
											if(CONFIG_ENABLE_ORIGIN == "1"){
												$countryTypes = " and countryType like '%destination%' ";
											}else{
												$countryTypes = " ";
											} 
											$benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName";
											$myCountries = selectMultiRecords($benBankCountrySql);
										
											for ($i = 0; $i < count($myCountries); $i++)
											{
												if ($myCountries[$i]["countryName"] == $country)
												{
										?>
																<option  value="<?=$country;?>" selected>
																<?=$myCountries[$i]["countryName"]?>
																</option>
																<?
												}
												else
												{
										?>
																<option value="<?=$myCountries[$i]["countryName"]; ?>">
																<?=$myCountries[$i]["countryName"]; ?>
																</option>
																<?
												}
											} 
										
										?>
															</select>
										<script language="JavaScript">
         	SelectOption(document.forms[0].country, "<?=$country?>");
                                </script>
									</td>
								</tr>
								<tr>
									<td align="right" valign="top" bgcolor="#DFE6EA" style="padding-top:15px;">Functionality</td>
									<? if($rightValue==""){?>
									<td bgcolor="#DFE6EA"><input name="rightValue" type="radio" value="normalRtn" checked>
									Normal Routine(IBAN/Bank)<br>
										<input name="rightValue" type="radio" value="countryRtn" >
									Country Based
									Routine</td>
									<? }else{ ?>
								  <td bgcolor="#DFE6EA"><input name="rightValue" type="radio" value="normalRtn" <? echo ($rightValue=="normalRtn" ? "checked"  : "")?>>
								    Normal Routine(IBAN/Bank)<br>
										<input name="rightValue" type="radio" value="countryRtn" <? echo ($rightValue=="countryRtn" ? "checked"  : "")?>>
										Country Based
									Routine</td>
									<? } ?>
								</tr>
								<tr>
									<td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
									<td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
								</tr>
							</form>
						</table>
						<br>
						</fieldset></td>
				</tr>
			</table></td>
	</tr>
</table>
</body>
</html>
