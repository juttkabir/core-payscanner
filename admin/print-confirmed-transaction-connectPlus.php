<?
session_start();
include ("../include/config.php");


$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];
$today = date("d-m-Y");
$agentType= getAgentType();
if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

$buttonValue = $_GET["transSend"];	


if($transID!='')
			{
				$queryTransaction = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction["custAgentID"]!='')
			{
				$querysenderAgent = "select name,username,agentCompany,logo  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction["custAgentID"] . "'";
				$custAgent=$queryTransaction["custAgentID"];
			
			


			$senderAgentContent = selectFrom($querysenderAgent);
			
			$custAgentParentID = $senderAgentContent["parentID"];
		}
		$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction["customerID"] . "'";
		$customerContent = selectFrom($queryCust);
		
		$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
		
		$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
		
		 $queryCp = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction["collectionPointID"] . "'";
			$collectionPoint = selectFrom($queryCp);	
			$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);


?>
<html>
<head>
<title>CONNECT PLUS BUSINESS LIMITED</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.labelFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
}
.dataFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	vertical-align: baseline;
}
.headingFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-transform: uppercase;
}

.tableBorder {
	border-right:1px solid #000000;
}
-->
</style>
</head>

<body <? if(CONFIG_SKIP_CONFIRM == '1'&& $buttonValue != ""){ ?> onLoad="print()"<? } ?>>
<font align="left" face="Arial, Helvetica, sans-serif" >CONNECT PLUS BUSINESS LIMITED</font>
<BR>
<table width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td> <? if($agentType != "SUPA" && $agentType != "SUBA"){
    	
    		
    	?>
    	<img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0>
    	
    	
    	<? }else{ ?>
    			&nbsp;
    	
    	<? } ?>
    	</td>
    	
    	
    <td class="dataFont" align="right">
    	
    	<? 
    	if($agentType == "SUPA" || $agentType == "SUBA"){
    	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
				{
						$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
								$width = ($arr[0] > 200 ? 200 : $arr[0]);
								$height = ($arr[1] > 100 ? 100 : $arr[1]);
    	
    }?>
    <img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo CONFIG_LOGO_WIDTH?>" height="<? echo CONFIG_LOGO_HEIGHT?>">
    
   <? }else{
    	
    	 ?>
    	
    	&nbsp;
    	<?} ?>
    	
    	</td>
  </tr>
  
  <tr>
  	<td>&nbsp;</td>
  	<td>&nbsp;</td>
  	
  	
  	</tr>
  <tr>
    <td <? if($agentType != "SUPA" && $agentType != "SUBA"){?>class="headingFont"<? }else{ ?>class="dataFont"<? } ?> align="center"><b><? echo COMPANY_NAME; ?></td>
    <td class="dataFont">&nbsp;</td>
  </tr>
  <tr>
    <td class="dataFont"><center><? echo COMPANY_ADDR; ?></td>
    <td <? if($agentType == "SUPA" || $agentType == "SUBA"){?>class="headingFont"<? }else{ ?>class="dataFont" <? } ?>align="right"><b><? echo $senderAgentContent["agentCompany"]; ?></b></td>
  </tr>
  <tr>
    <td class="dataFont"><center><? echo CONFIG_INVOICE_FOOTER; ?></td>
    <td class="dataFont" align="right"><? echo $senderAgentContent["agentCompany"]; ?></td>
  </tr>
  <tr>
    <td class="dataFont"><center>Company No.<? echo " ".CONFIG_COMPANY_NUMBER; ?>MLR.</td>
    <td class="dataFont" align="right"><? echo $senderAgentContent["username"]; ?></td>
  </tr>
</table>


<br>
<table width="75%" style="border:1px solid #000000;" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="labelFont tableBorder" colspan="2" >Remitente</td>
    <td colspan="3" class="dataFont"><? echo $senderAgentContent["username"]; ?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo  $customerContent["firstName"]." ".$customerContent["middleName"]." ".$customerContent["lastName"]?></td>
    <td colspan="3" class="dataFont">Concepto:  <? echo $queryTransaction['transactionPurpose']?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo $customerContent["Address"]; ?></td>
    <td colspan="3" class="dataFont">Feche/hora:<? $date=$queryTransaction['transDate'];
    	$transDate = substr($queryTransaction['transDate'],0,10); 
    		$transTime = substr($queryTransaction['transDate'],12,19);
    	
    	echo dateFormat($transDate)." ".$transTime; ?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $customerContent["Address1"]; ?>&nbsp;</td>
    <td colspan="3" class="dataFont">Referencia:<? echo $queryTransaction['refNumberIM']?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo $customerContent["Phone"]; ?></td>
    <td colspan="3" class="dataFont">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2" class="dataFont tableBorder ">&nbsp;</td>
    <td colspan="3" class="dataFont">Cambio:</td>
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">Beneficiaro</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["middleName"]." ".$benificiaryContent["lastName"]; ?></td>
    <td class="dataFont">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">&nbsp;</td>
    <td class="dataFont">&nbsp;</td>
    <td class="dataFont" align="center">GBP</td>
    <td class="dataFont" align="center">Dolares</td>
    
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">&nbsp;</td>
    <td class="dataFont">Enviado</td>
    <td class="dataFont" align="center">0,00</td>
    <td class="dataFont" align="center"><? echo $queryTransaction["transAmount"]; ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["Address"]?>&nbsp;</td>
    <td class="dataFont">Cargo</td>
    <td class="dataFont" align="center">0,00</td>
    <? $charges = $queryTransaction["bankCharges"] + $queryTransaction["outCurrCharge"] + $queryTransaction["cashCharges"]; ?>
    <td class="dataFont" align="center"><? echo $charges; ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["Phone"]?></td>
    <td class="dataFont" >Total</td>
    <td class="dataFont" align="center">0,00</td>
    <td  class="dataFont" align="center"><? echo $queryTransaction["totalAmount"] ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2">&nbsp;</td>
    <td colspan="3" align="center" class="dataFont">A pager en <? echo $queryTransaction["toCountry"] ?></td>
  </tr>
  
  <tr> 
  	 <?
  if($queryTransaction["transType"] == "Bank Transfer")
			  {
		  		 ?>
    <td  class="dataFont tableBorder" colspan="2"><? echo $benBankDetails["bankName"]; ?></td>
    <? }elseif($queryTransaction["transType"] == "Pick up"){ ?>
    
    <td class="dataFont" class="dataFont tableBorder" colspan="2"><? echo $queryExecute["name"]; ?></td>
    <? } ?>
    <td colspan="3" align="center" class="dataFont"><? echo $queryTransaction["localAmount"] ." ". $queryTransaction["toCountry"];?></td>
  </tr>
  
  
  <?
  if($queryTransaction["transType"] == "Bank Transfer")
			  {
		  		 ?>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benBankDetails["branchAddress"]; ?></td>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  <tr> 
    <td class="dataFont"><? echo $benBankDetails["branchCode"]; ?></td>
    <td class="dataFont tableBorder"><? echo $benBankDetails["bankName"]; ?></td>
    <td colspan="3" class="dataFont"><center>Vantanilla</td>
    
  </tr>
  <? }elseif($queryTransaction["transType"] == "Pick up"){?>
  
   <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $collectionPoint["cp_corresspondent_name"]; ?></td>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  <tr> 
    <td class="dataFont tableBorder"><? echo $collectionPoint["cp_branch_name"]; ?></td>
    <td class="dataFont"><? echo $queryExecute["name"]; ?></td>
    <td colspan="3"  align="center">Vantanilla</td>
    
  </tr>
  
  <? } ?>
  
  <tr> 
    <td colspan="2" class="dataFont tableBorder">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>

</table>
<p>&nbsp;</p>

<table width="75%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="dataFont" align="center">Firma dei cliente</td>
    <td class="dataFont" align="center">firma dei (RAFA)</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
</table>


<br>





<table width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td> <? if($agentType != "SUPA" && $agentType != "SUBA"){
    	
    		
    	?>
    	<img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0>
    	
    	
    	<? }else{ ?>
    			&nbsp;
    	
    	<? } ?>
    	</td>
    	
    	
    <td class="dataFont" align="right">
    	
    	<? 
    	if($agentType == "SUPA" || $agentType == "SUBA"){
    	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
				{
						$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
								$width = ($arr[0] > 200 ? 200 : $arr[0]);
								$height = ($arr[1] > 100 ? 100 : $arr[1]);
    	
    }?>
    <img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo CONFIG_LOGO_WIDTH?>" height="<? echo CONFIG_LOGO_HEIGHT?>">
    
   <? }else{
    	
    	 ?>
    	
    	&nbsp;
    	<?} ?>
    	
    	</td>
  </tr>
  
  <tr>
  	<td>&nbsp;</td>
  	<td>&nbsp;</td>
  	
  	
  	</tr>
  <tr>
    <td <? if($agentType != "SUPA" && $agentType != "SUBA"){?>class="headingFont"<? }else{ ?>class="dataFont"<? } ?> align="center"><b><? echo COMPANY_NAME; ?></td>
    <td class="dataFont">&nbsp;</td>
  </tr>
  <tr>
    <td class="dataFont"><center><? echo COMPANY_ADDR; ?></td>
    <td <? if($agentType == "SUPA" || $agentType == "SUBA"){?>class="headingFont"<? }else{ ?>class="dataFont" <? } ?>align="right"><b><? echo $senderAgentContent["agentCompany"]; ?></b></td>
  </tr>
  <tr>
    <td class="dataFont"><center><? echo CONFIG_INVOICE_FOOTER; ?></td>
    <td class="dataFont" align="right"><? echo $senderAgentContent["agentCompany"]; ?></td>
  </tr>
  <tr>
    <td class="dataFont"><center>Company No.<? echo " ".CONFIG_COMPANY_NUMBER; ?>MLR.</td>
    <td class="dataFont" align="right"><? echo $senderAgentContent["username"]; ?></td>
  </tr>
</table>


<br>
<table width="75%" style="border:1px solid #000000;" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="labelFont tableBorder" colspan="2" >Remitente</td>
    <td colspan="3" class="dataFont"><? echo $senderAgentContent["username"]; ?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo  $customerContent["firstName"]." ".$customerContent["middleName"]." ".$customerContent["lastName"]?></td>
    <td colspan="3" class="dataFont">Concepto:  <? echo $queryTransaction['transactionPurpose']?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo $customerContent["Address"]; ?></td>
    <td colspan="3" class="dataFont">Feche/hora:<? $date=$queryTransaction['transDate'];
    	$transDate = substr($queryTransaction['transDate'],0,10); 
    		$transTime = substr($queryTransaction['transDate'],12,19);
    	
    	echo dateFormat($transDate)." ".$transTime; ?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $customerContent["Address1"]; ?>&nbsp;</td>
    <td colspan="3" class="dataFont">Referencia:<? echo $queryTransaction['refNumberIM']?></td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder " colspan="2"><? echo $customerContent["Phone"]; ?></td>
    <td colspan="3" class="dataFont">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2" class="dataFont tableBorder ">&nbsp;</td>
    <td colspan="3" class="dataFont">Cambio:</td>
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">Beneficiaro</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["middleName"]." ".$benificiaryContent["lastName"]; ?></td>
    <td class="dataFont">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">&nbsp;</td>
    <td class="dataFont">&nbsp;</td>
    <td class="dataFont" align="center">GBP</td>
    <td class="dataFont" align="center">Dolares</td>
    
  </tr>
  <tr> 
    <td class="labelFont tableBorder" colspan="2">&nbsp;</td>
    <td class="dataFont">Enviado</td>
    <td class="dataFont" align="center">0,00</td>
    <td class="dataFont" align="center"><? echo $queryTransaction["transAmount"]; ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["Address"]?>&nbsp;</td>
    <td class="dataFont">Cargo</td>
    <td class="dataFont" align="center">0,00</td>
    <? $charges = $queryTransaction["bankCharges"] + $queryTransaction["outCurrCharge"] + $queryTransaction["cashCharges"]; ?>
    <td class="dataFont" align="center"><? echo $charges; ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benificiaryContent["Phone"]?></td>
    <td class="dataFont" >Total</td>
    <td class="dataFont" align="center">0,00</td>
    <td  class="dataFont" align="center"><? echo $queryTransaction["totalAmount"] ?></td>
    
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td class="dataFont tableBorder" colspan="2">&nbsp;</td>
    <td colspan="3" align="center" class="dataFont">A pager en <? echo $queryTransaction["toCountry"] ?></td>
  </tr>
  
  <tr> 
  	 <?
  if($queryTransaction["transType"] == "Bank Transfer")
			  {
		  		 ?>
    <td  class="dataFont tableBorder" colspan="2"><? echo $benBankDetails["bankName"]; ?></td>
    <? }elseif($queryTransaction["transType"] == "Pick up"){ ?>
    
    <td class="dataFont" class="dataFont tableBorder" colspan="2"><? echo $queryExecute["name"]; ?></td>
    <? } ?>
    <td colspan="3" align="center" class="dataFont"><? echo $queryTransaction["localAmount"] ." ". $queryTransaction["toCountry"];?></td>
  </tr>
  
  
  <?
  if($queryTransaction["transType"] == "Bank Transfer")
			  {
		  		 ?>
  <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $benBankDetails["branchAddress"]; ?></td>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  <tr> 
    <td class="dataFont"><? echo $benBankDetails["branchCode"]; ?></td>
    <td class="dataFont tableBorder"><? echo $benBankDetails["bankName"]; ?></td>
    <td colspan="3" class="dataFont"><center>Vantanilla</td>
    
  </tr>
  <? }elseif($queryTransaction["transType"] == "Pick up"){?>
  
   <tr> 
    <td class="dataFont tableBorder" colspan="2"><? echo $collectionPoint["cp_corresspondent_name"]; ?></td>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  <tr> 
    <td class="dataFont tableBorder"><? echo $collectionPoint["cp_branch_name"]; ?></td>
    <td class="dataFont"><? echo $queryExecute["name"]; ?></td>
    <td colspan="3"  align="center">Vantanilla</td>
    
  </tr>
  
  <? } ?>
  
  <tr> 
    <td colspan="2" class="dataFont tableBorder">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>

</table>
<p>&nbsp;</p>


<table width="75%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="dataFont" align="center">Firma dei cliente</td>
    <td class="dataFont" align="center">firma dei (RAFA)</td>
  </tr>
  
   <tr>
  	<td class="dataFont" align="center">
  	<div class='noPrint'>
			<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
		</div>
	</td>
</tr>
  
</table>


</body>
</html>
