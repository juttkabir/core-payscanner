<?
	session_start();
	$rootPath = $_SERVER['DOCUMENT_ROOT']."/admin/lib/";
	include ($_SERVER['DOCUMENT_ROOT']."/include/config.php");
	include ("security.php");
	include_once($rootPath."currency_exchange_functions.php");
	
//	flag for ticket#9363
		$stockHistoryFlag="";
	if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY==1)
		$stockHistoryFlag=true;
	else	
		$stockHistoryFlag=false;
//	end the flag of ticket #9363

	$agentType = getAgentType();
	// define page navigation parameters
	
	$limit=10000;
	
	if ($offset == "")
		$offset = 0;
		
	if($limit == 0)
		$limit=10;
	
	
	if ($_REQUEST['newOffset'] != "") {
		$offset = $_REQUEST['newOffset'];
	}
	//debug($limit."-->".$offset);
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	$sortBy = $_REQUEST['sortBy'];
	if ($sortBy == "")
		$sortBy = "id";
	
	//debug($nxt."-->".$prv );
	$colSpans 		= 6;
	$roundAmountTo	= 2;
	$roundRateTo 	= 4;
	$filters = "";
	
	if(!empty($_REQUEST['buysellCurrency']))
		$buysellCurrency = $_REQUEST['buysellCurrency'];
	
	if($_REQUEST['Submit'] != "")
		$Submit = $_REQUEST['Submit'];	
		
	
		//debug(date("d/m/Y",strtotime("-6 days")));
		$submitV = $_REQUEST['Submit'];
		
		$currencyV 	= $_REQUEST['buysellCurrency'];
		$fromDate	= $_REQUEST['from_date'];
		$toDate 	= $_REQUEST['to_date'];
		
		$currencyFilter = '';
		if(!empty($currencyV) && $currencyV !='all')
			$currencyFilter .= " AND buysellCurrency = '".$currencyV."'";
		
		// By default report view, show last weaks activity	
		if(empty($submitV)){
			$submitV	 = 'Y';
			$currencyV 	= 'all';
			$fromDate	= date("d/m/Y",strtotime("-6 days"));
			$toDate		= date("d/m/Y",time());
		}
		list($from_day,$from_month,$from_year) = explode('/',$fromDate);
		list($to_day,$to_month,$to_year) 		= explode('/',$toDate);
		
		$fromStrDate 	= $from_year.'-'.$from_month.'-'.$from_day.' 00:00:00';//date("Y-m-d H:i:s",strtotime($fromDate));
		$toStrDate 		= $to_year.'-'.$to_month.'-'.$to_day.' 23:59:59';//date("Y-m-d H:i:s",strtotime($toDate));
		$filtersDate 	= " and (created_at >= '$fromStrDate' and created_at <= '$toStrDate')";
		$filters 		.= " and (created_at >= '$fromStrDate' and created_at <= '$toStrDate')";
		
	//debug($submitV);	
	$groupBy = " GROUP BY daily_date ";	
	if(!empty($submitV))
	{
	
		$queryCurr = "SELECT 
							DATE(created_at) as daily_date
					  FROM 
						  curr_exchange_account as cea
					  WHERE 1
					  AND  transStatus != 'Cancelled'  ";							
		$whrClause = $currencyFilter.$filters.$groupBy;	
		$queryCurr .= $whrClause;
		
	
		//debug($queryCurr);
		$countRecords = SelectMultiRecords($queryCurr);
		$allCount = count($countRecords);
		
		//$queryCurrOut	= $queryCurr;
		//$queryCurr 		.= " LIMIT $offset , $limit";
		$currencyData 	= SelectMultiRecords($queryCurr);
		//debug($currencyData,true);
	}
	$strFieldsSel="";
	if($stockHistoryFlag)
		$strFieldsSel=", cs.actualAmount";

	$single_curr_flag = array();
	$currStockStr = "SELECT 
						crt.buysellCurrency,										
						curr.currencyName,
						crt.buying_rate,
						DATE(cs.created_on) as daily_date
						".$strFieldsSel."
					FROM 
						currencies as curr,curr_exchange as crt,currency_stock as cs
					WHERE	
						curr.cID = crt.buysellCurrency  
					AND	curr.currencyName = cs.currency    	
						".$currencyFilter."  
					ORDER BY 
						crt.id ASC";
	
	$currStockData = SelectMultiRecords($currStockStr);
	 //debug($currStockData,true);
	foreach($currStockData as $k=>$v){
		$single_curr_flag[$v['buysellCurrency']] = true;
		$foreingInitial = number_format(($v['actualAmount']/$v['buying_rate']),$roundAmountTo,'.','');
		$currencyDataInitial[$v['buysellCurrency']][$v['daily_date']][] = array(
											
													"created_at" => $v['created_on'],
													"buy_sell" => "B",
													"totalAmount" => $v['actualAmount'],
													"rate" => $v['buying_rate'],
													"localAmount" => $foreingInitial
											);
	}
	//debug($currencyDataInitial);
	
	$colHeads = array('Date','Rolling Profit','Profit per Day','Rolling Turn Over','Turnover Per Day','Transactions');
	
	/*** Currency Drop Down Query***/
	$sqlCurr = "SELECT 
					cID,
					currencyName
				FROM 
					currencies
				WHERE 
					cID IN
					(
					SELECT 
						buysellCurrency
					FROM 
						curr_exchange_account
					WHERE 1
					)";
												 
	//$sqlCurr .= $filters2;	
	$sqlCurr .= " GROUP BY currencyName";
	$getBSCurrData1 = SelectMultiRecords($sqlCurr);
	//debug($getBSCurrData1,true);
?>
<html>
<head>
<title>Currency Daily Profit Report</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">

	$(document).ready(function(){
			$("#exportReport").change(function(){
				if($(this).val() != "")
				{
					$("#exportForm").submit();
				}
			});
		
		$("#exportBtn").click(function(){
			$("#exportReport").show();
		});
		
		$("#printBtn").click(function(){
			$("#filterRow").hide();
			$("#printRow").hide();
			window.print();
		});
		
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-4 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-4 years"))?>'
		});
	});
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	 
</script>
 
<style type="text/css">
	.dp-choose-date{
		color: black;		
	}
	.error {
		color: red;
		font: 8pt verdana;
		font-weight:bold;
	}
</style>
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
	<tr bgcolor="#DFE6EA">
    	<td class="topbar" align="center">Currency Daily Profit Report</td>
  	</tr>
  	<form name="search" method="post" action="">
    <tr id="filterRow">
 	<td><fieldset>
    	<table width="50%" border="0" align="center" cellpadding="5" cellspacing="1">
        	<tr>
            	<td align="center"><b>SEARCH FILTER</b></td>
          	</tr>
          	<tr bgcolor="#DFE6EA">
		  		<td align="center">
					<b>From </b>
            		<input type="text" id="from_date" name="from_date" value="<?=$fromDate?>" readonly="readonly" />
					&nbsp;&nbsp;
					<b>To </b>
					<input type="text" id="to_date" name="to_date" value="<?=$toDate?>" readonly="readonly" />
				</td>
			</tr>
			<tr bgcolor="#DFE6EA"> 
				<td align="center"><b>Buy/Sell Currency</b>
              		<select name="buysellCurrency" style="font-family: verdana; font-size: 11px;">
						<option value="">Select Currency</option>
						<option value="all"> All </option>
						<?php
						for ($i=0; $i < count($getBSCurrData1); $i++)
						{
							$getBSCurrName1 = $getBSCurrData1[$i]['currencyName'];		
							$getBSCurrID1   = $getBSCurrData1[$i]['cID'];
						?>
							<OPTION value="<?=$getBSCurrID1; ?>"><?=$getBSCurrName1; ?></OPTION>
						<?php
						}
						?>
              		</SELECT>
					<script language="JavaScript">
						SelectOption(document.search.buysellCurrency, "<?=$currencyV?>");
					</script>
				</td>
			</tr>
			<tr  bgcolor="#DFE6EA">
				<td align="center"><input type="submit" name="Submit" value="Process"></td>
          	</tr>
        </table>
        </fieldset>
        <br>
      </td>
  </form>
  </tr>
  <tr>
    <td align="center"><table width="50%" border="0" cellspacing="1" cellpadding="1" align="center">
        <?
			if ($allCount > 0){
			$query_string = "&buysellCurrency=$buysellCurrency&Submit=$Submit&from_date=$fromDate&to_date=$toDate";
		?>
        <tr>
          <td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
              <tr>
                <td><?php if (count($currencyData) > 0) {;?>
                  Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
                  <?=$allCount; ?>
                  <?php } ;?>
                </td>
                <?php if ($prv >= 0) { ?>
                <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
                <?php } ?>
                <?php 
					  if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php } ?>
              </tr>
            </table></td>
        </tr>
        <tr bgcolor="#DFE6EA">
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[0]?>
            </strong></font></td>
          <td width="125"><font color="#005b90"><b>
            <?=$colHeads[1]?>
            </b></font></td>
          <td width="109"><font color="#005b90"><b>
            <?=$colHeads[2]?>
            </b></font></td>
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[3]?>
            </strong></font></td>
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[4]?>
            </strong></font></td>
           <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[5]?>
            </strong></font></td>
        </tr>
        <?php

			$rolling_turnover	= 0;
			$rolling_profit		= 0;			
			$total_trans		= 0;
			$exportData 		= array();
			$exportTotalData	= array();
			$lastBuyRateArr		= array();
			foreach($currencyData as $currData)
			{
				$currencyName 	= $currData['currencyName'];
				$daily_date		= $currData['daily_date'];
				$whrClause 		= "AND created_at LIKE '".$daily_date."%' AND transStatus != 'Cancelled' ".$filtersDate;
				
				$buyAllStr 	= "Select localAmount,rate,totalAmount, buy_sell,buysellCurrency FROM curr_exchange_account WHERE 1 ".$whrClause;
				$buyAllData	= selectMultiRecords($buyAllStr);
				
				foreach($buyAllData as $k=>$v){
					$cID = $v['buysellCurrency'];
					foreach($currencyDataInitial[$cID] as $cK=>$cV){
						if($single_curr_flag[$cID]){	
							$single_curr_flag[$cID] = false;
							$buyAllData = array_merge($cV,$buyAllData);
							//debug($buyAllData);
							$lastBuyRateArr[$cID] = $cV[0]['rate'];
						}
					}
				}
				$daily_profit	= 0;
				$daily_turnover	= 0;				
				$buyCostInGBP  	= 0;
				$sellCostInGBP 	= 0;
				$count			= 0;
				
				$exportData['daily_date'][]	= $daily_date;
				//debug($buyAllData);
				foreach($buyAllData as $k=>$v){
					//debug($k);
					$lastBuyRate = $lastBuyRateArr[$v['buysellCurrency']];
					//debug($lastBuyRate);
					$arrInput = array(
							"buy_sell"    		=> $v['buy_sell'],	
							"totalAmount" 		=> $v['totalAmount'],
							"rate"        		=> $v['rate'],	
							"lastBuyRate" 		=> $lastBuyRate,
							"localAmount" 		=> $v['localAmount'],
							"roundRateLevel"  	=> $roundRateTo,
							"roundAmountLevel"  => $roundAmountTo	
					);
					$arrCurrencyTotal = currency_totals($arrInput); 
					//debug($arrCurrencyTotal);
					$daily_profit 	+= $arrCurrencyTotal['profit_loss'];					
					$sellCostInGBP	= $arrCurrencyTotal['buyCostInGBP'];	// GBP amount
					$buyCostInGBP 	= $arrCurrencyTotal['sellCostInGBP'];	// GBP amount
					$daily_turnover	+= ($sellCostInGBP+$buyCostInGBP);
					
					$count++;
				}
				$rolling_profit		+= $daily_profit;
				$rolling_turnover	+= $daily_turnover;
				$total_trans		+= $count;

				// Assign Data in sequence to be displayed on report and export
				$exportData[$daily_date][]	=	array('data'=>$rolling_profit,'dec'=>$roundAmountTo);	
				$exportData[$daily_date][]	=	array('data'=>$daily_profit ,'dec'=>$roundAmountTo);
				$exportData[$daily_date][]	=	array('data'=>$rolling_turnover ,'dec'=>$roundAmountTo);
				$exportData[$daily_date][]	= array('data'=>$daily_turnover ,'dec'=>$roundAmountTo);
				$exportData[$daily_date][]	= array('data'=>$count);

		?>
			<tr valign="top" bgcolor="#eeeeee">
			  <td><font color="#005b90"><b>
				<?=$daily_date?>
				</b></font></a> </td>
			  <td align="right"><?=stripslashes(number_format($rolling_profit,$roundAmountTo,'.',',')); ?></td>
			  <td align="right"><?=stripslashes(number_format($daily_profit,$roundAmountTo,'.',',')); ?></td>
			  <td align="right"><?=stripslashes(number_format($rolling_turnover,$roundAmountTo,'.',',')); ?></td>
			  <td align="right"><?=stripslashes(number_format($daily_turnover,$roundAmountTo,'.',',')); ?></td>
			  <td align="center"><?=$count?></td>
			</tr>
		<?php
		} //end for loop
			
		// Assign Data in sequence to be displayed on report and export
		$exportTotalData[]	= array('data'=>'Grand Total');
		$exportTotalData[]	= array('data'=>$rolling_profit,'dec'=>$roundAmountTo);
		$exportTotalData[]	= array('data'=>$rolling_profit,'dec'=>$roundAmountTo);
		$exportTotalData[]	= array('data'=>$rolling_turnover,'dec'=>$roundAmountTo);
		$exportTotalData[]	= array('data'=>$rolling_turnover,'dec'=>$roundAmountTo);
		$exportTotalData[]	= array('data'=>$total_trans);
		?>
        <tr bgcolor="#DFE6EA">
          <td><font color="#005b90"><strong>Grand Total</strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($rolling_profit,$roundAmountTo,'.',',')); ?>
            </strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($rolling_profit,$roundAmountTo,'.',',')); ?>
            </strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($rolling_turnover,$roundAmountTo,'.',',')); ?>
            </strong></font> </td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($rolling_turnover,$roundAmountTo,'.',',')); ?>
            </strong></font> </td>
          <td align="center"><font color="#005b90"><strong>
            <?=$total_trans?>
            </strong></font> </td>
        </tr>
        <tr>
          <td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
              <tr>
                <td><?php if (count($currencyData) > 0) {;?>
                  Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
                  <?=$allCount; ?>
                  <?php } ;?>
                </td>
                <?php if ($prv >= 0) { ?>
                <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
                <?php } ?>
                <?php 
					  if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php } ?>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="<?=$colSpans?>" align="center">
          </td>
        </tr>
        <tr id="printRow">
          <form action="export_daily_profit_report.php" method="post" id="exportForm" name="exportForm" target="_blank">
          <td align="center" colspan="<?=$colSpans?>">
		  	<input type="button" name="Submit2" value="Print This Report" id="printBtn" />
            <input type="hidden" name="exportData" id="exportData" value="<?=base64_encode(serialize($exportData))?>" />
            <input type="hidden" name="exportTotalData" id="exportTotalData" value="<?=base64_encode(serialize($exportTotalData))?>" />
            <input type="hidden" name="colHeads" id="colHeads" value="<?=base64_encode(serialize($colHeads))?>" />
            <input type="button" id="exportBtn" id="exportBtn" value="Export All" />
            <select name="exportReport" id="exportReport" style="display:none" >
              <option value="">Select Format</option>
              <option value="XLS">Excel</option>
              <option value="CSV">CSV</option>
              <option value="HTML">HTML</option>
            </select>
          </td>
          </form>
        </tr>
        <?php
		} else {
		?>
			<tr>
			  <td colspan="5" align="center"> No Record found in the database. </td>
			</tr>
        <?php
		}
		?>
      </table>
	 </td>
  </tr>
</table>
</body>
</html>