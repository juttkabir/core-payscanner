<?

if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	}
	
function currencyValue()
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].","))
			$compareCurrency = true;	
	}elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].","))
			$compareCurrency = true;	
	}else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyTo"].",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $_POST["currencyFrom"].",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}	

function RoundVal($val){
	
	if(CONFIG_TRANS_ROUND_LEVEL != "")
	{
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	}else{
		$roundLevel = 4;
		}
		
		
	$compareCurrency2 = currencyValue();
	
    if((strstr(CONFIG_ROUND_NUM_FOR, $_POST["moneyPaid"].",")) && $compareCurrency2){
        if(CONFIG_TRANS_ROUND_NUMBER == 1){
      	$ArrVal = explode(".",$val);
       	$return=$ArrVal[0];
    }else{
    	
        $return=round($val,$roundLevel);
        }
    }else{
    	
      	$return=round($val,$roundLevel);        
    }
return $return;	
}

if(CONFIG_PAYIN_CUSTOMER != '1')
{
	if(CONFIG_FEE_DISTRIBUTOR == '1')
	{
		$imFeeOld = imFeeAgent($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"], $_SESSION["benAgentID"]);	
	}elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
	{
		$imFeeOld = imFeeAgent($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"], $_POST["custAgentID"]);	
	}
	if($imFeeOld <= '0')
	{
		$imFeeOld = imFee($_POST["transAmount"], $_POST["custCountry"], $_POST["benCountry"]);	
	}
}else
{	
	if(CONFIG_FEE_DISTRIBUTOR == '1')
	{
		if($custContents["payinBook"] != "" )
			$imFeeOld = imFeeAgentPayin($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"], $_SESSION["benAgentID"]);	
		else
			$imFeeOld = imFeeAgent($_POST["transAmount"], $_POST["custCountry"], $_POST["benCountry"], $_SESSION["benAgentID"]);	
	}elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
	{
		if($custContents["payinBook"] != "" )
			$imFeeOld = imFeeAgentPayin($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"], $_POST["custAgentID"]);	
		else
			$imFeeOld = imFeeAgent($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"], $_POST["custAgentID"]);	
	}
	if($imFeeOld == '0')
	{		
		if($custContents["payinBook"] != "" )
			$imFeeOld = imFeePayin($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"]);	
		else
			$imFeeOld = imFee($_POST["transAmount"],  $_POST["custCountry"], $_POST["benCountry"]);
	}
}
			  	
if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
	$imFeeOld = RoundVal($imFeeOld);
}else{
	$imFeeOld = round($imFeeOld, $roundLevel);
}
$discountValue = $imFeeOld - $_SESSION["discount"]; 


if(CONFIG_FEE_DISCOUNT_RESTRICTION == '1' && $discountValue < 0)
{
	insertError(TE25);	
	redirect($backUrl);
	}
			  	
?>