<?
session_start();
include ("../include/config.php");
include ("security.php");
$company = COMPANY_NAME;

$company = COMPANY_NAME;
$agentType = getAgentType();
if($agentType != "TELLER")
{
	$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");
}else{
	$userDetails = selectFrom("select * from ".TBL_TELLER." where loginName='$username'");
}

if(CONFIG_CUSTOM_SENDER == '1')
{
	$customerPage = CONFIG_SENDER_PAGE;
}else{
	$customerPage = "add-customer.php";
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
}

/*
 * 7897 - Premier FX
 * Rename 'Create Transaction' text in the system.
 * Enable  : string
 * Disable : "0"
 */
$strTransLabel = 'Transaction';
if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
	$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;

$userID = $_SESSION["loggedUserData"]["userID"];
$MSD_ID=explode(",",CONFIG_PAYING_BOOK_AGENTS);

if($agentType == 'Admin')
{
	$functionalityQuery = "Select * from ".TBL_FUNCTIONAL." where users like '%Basic User%'";
}else{
	$functionalityQuery = "Select * from ".TBL_FUNCTIONAL." where users like '%$agentType,%'";
}

$functionalities = selectMultiRecords($functionalityQuery);

for($counter = 0; $counter < count($functionalities); $counter++)
{
	$functionString .= $functionalities[$counter]["functionCode"].", ";
}

if ($_GET["for"] == "shortcut") {	// [by JAMSHED]

///For User Managment
	$viewReport = False;
	for($reports = 0; $reports <= 15; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}
	}

	for($reports = 101; $reports <= 103; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}
	}
	for($reports = 176; $reports <= 177; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}
	}


	if(strstr($functionString, "right179,"))
	{
		$viewReport= True;
	}


	for($reports = 107; $reports <= 109; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}
	}

	if(strstr($functionString, "right85,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right114,"))
	{
		$viewReport= True;
	}
	if(strstr($functionString, "right120,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right121,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right181,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right187,"))
	{
		$viewReport= True;
	}

	/*************Assigning links to shortcut menu***************/
	if($viewReport){
		$User_Management = array();
		if($userDetails["parentID"]== "0" && $agentType == "admin" ){
			$User_Management[] = "add_super_admin.php,Add Super Admin";
			$User_Management[] = "manage_super_admin.php,Manage Super Admin";
		}
		if(strstr($functionString, "right0,"))
		{////Add Super Agent
			$User_Management[] = "add-agent.php,Add Super Agent";
		}
		if(strstr($functionString, "right1,"))
		{///Manage Super Agent
			$User_Management[] = "agent-list.php,Manage Super Agents";
		}
		if(strstr($functionString, "right2,"))
		{///Add Sub Agent
			$User_Management[] = "add-sub-agent.php?type=sub,Add Sub Agent";
		}
		if(strstr($functionString, "right3,"))
		{///Manage Sub Agent
			$User_Management[] = "agent-list.php?type=sub,Manage Sub Agents";
		}
		if(CONFIG_AnD_ENABLE == '1')
		{

			if(strstr($functionString, "right106,"))
			{/// Add A&D to Be put in File
				$User_Management[] = "add-agent_Distributor.php,Add Super A&D";
			}
			if(strstr($functionString, "right107,"))
			{//// Manage A&D to Be put in File
				$User_Management[] = "agent_Distributor-list.php,Manage Super A&D";
			}

			if(strstr($functionString, "right108,"))
			{/// Add Sub A&D to Be put in File
				$User_Management[] = "add-sub-agent.php?type=sub&ida=ND,Add Sub A&D";
			}

			if(strstr($functionString, "right109,"))
			{//// Manage Sub A&D to Be put in File
				$User_Management[] = "agent-list.php?type=sub&ida=ND,Manage Sub A&D";
			}

		}
		if(strstr($functionString, "right4,"))
		{///Add Super Distributor
			$User_Management[] = "add-agent.php?ida=Y,Add Super Distributor";
		}
		if(strstr($functionString, "right5,"))
		{///Manage Super Distributor
			$User_Management[] = "agent-list.php?ida=Y,Manage Super Distributor";
		}
		if(strstr($functionString, "right6,"))
		{///Add Sub Distributor
			$User_Management[] = "add-sub-agent.php?type=sub&ida=Y,Add Sub Distributor";
		}
		if(strstr($functionString, "right7,"))
		{///Manage Sub Distributor
			$User_Management[] = "agent-list.php?type=sub&ida=Y,Manage Sub Distributor";
		}
		if(CONFIG_PP_TYPE_ON_TRANSACTION == '1'){
			if(strstr($functionString, "right175,"))
			{///Manage Sub Distributor
				$User_Management[] = "PPpoint-agent-list.php?type=sub&ida=Y,Manage Pay Points";
			}
		}
		if(strstr($functionString, "right117"))
		{///Manage Export Transactions
			$User_Management[] = "manage_export_config.php,Manage Export Pages";
		}
		if(strstr($functionString, "right8,"))
		{///Add Admin Staff
			if(CONFIG_ADMIN_STAFF_OPAL_PAGES == '1'){
				$addPage = "add-admin-opal.php";
			}else{
				$addPage = "add-admin.php";
			}
			$User_Management[] = $addPage."?act=addAdmin,Add Admin Staff";

		}
		if(strstr($functionString, "right9,"))
		{///Manage Admin Staff
			if(CONFIG_ADMIN_STAFF_OPAL_PAGES == '1'){
				$updatePage = "admins-list-opal.php";
			}else{
				$updatePage = "admins-list.php";
			}
			$User_Management[] = $updatePage.",Manage Admin Staff";
		}
		if(strstr($functionString, "right10,"))
		{///New Agents
			$User_Management[] = "new-agent-list.php,New Agents";
		}

		if(strstr($functionString, "right101,") && (CONFIG_AGENT_RECEIPT_RANGE == "1" || CONFIG_DIST_REF_NUMBER == "1"))
		{
			if(CONFIG_DIST_REF_NUMBER == '1'){
				$name = "Add ".CONFIG_DIST_REF_NAME." Range";
			}else{
				$name = "Add Agents Receipt Range";
			}
			$User_Management[] = "add-receipt-range.php,".$name;
		}
		if(strstr($functionString, "right102,") && (CONFIG_AGENT_RECEIPT_RANGE == "1" || CONFIG_DIST_REF_NUMBER == "1"))
		{
			if(CONFIG_DIST_REF_NUMBER == '1'){
				$viewName = "View ".CONFIG_DIST_REF_NAME." Ranges";
			}else{
				$viewName = "View Agents Receipt Ranges";
			}
			$User_Management[] = "view-receipt-ranges.php,".$viewName;
		}


		if(strstr($functionString, "right176,") && CONFIG_MANAGE_CLAVE == "1")
		{
			$User_Management[] = "add-clave-range.php,Add Distributor Clave Range";
		}
		if(strstr($functionString, "right177,") && CONFIG_MANAGE_CLAVE == "1")
		{

			$User_Management[] = "view-clave-ranges.php,View Distributor Clave Ranges";
		}

		if(strstr($functionString, "right193,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
		{///add sender's receipt number generator
			$User_Management[] = "add-sender-receipt-range.php,Add Sender Receipt Reference Number Generator";
		}

		if(strstr($functionString, "right194,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
		{///view sender's receipt number generator
			$User_Management[] = "view-sender-receipt-ranges.php,View Sender Receipt Reference Number Generator";
		}
		if (CONFIG_EXPORT_TABLE_MGT == "1")
		{
			if(strstr($functionString, "right85,"))
			{///Manage Export Pages Tobe Added in the DB
				$User_Management[] = "manage_export_config.php,Manage Export Templates";
			}
		}






		if(strstr($functionString, "right11,"))
		{///Assign Teller
			$User_Management[] = "assign_teller.php,Assign Teller";
		}
		if(strstr($functionString, "right12,"))
		{///Manage Teller
			$User_Management[] = "teller_list.php,Manage Teller";
		}
		if(strstr($functionString, "right13,"))
		{///Add Collection Point
			$User_Management[] = "add-collectionpoint.php,Add Collection Point";
		}
		if(strstr($functionString, "right14,"))
		{///Manage Collection Point
			$User_Management[] = "manage_collection_point.php,Manage Collection Point";
		}

		if(CONFIG_PP_TYPE_ON_TRANSACTION == "1"){
			if(strstr($functionString, "right169,"))
			{///Manage Collection Point
				$User_Management[] = "import-payPoints.php,Import Pay Point Names";
			}
		}

		if(CONFIG_BACK_DATED == '1')
		{

			if(strstr($functionString, "right15,"))
			{///Right for Backdated
				$User_Management[] = "backdated_trans_rights.php,Right for Backdated";

			}
		}

		if (CONFIG_CITY_SERVICE_ENABLED)
		{

			if(strstr($functionString, "right86,"))
			{///Home Delivery Services To be Added in File
				$User_Management[] = "city-services.php,Home Delivery Services";
			}
		}

		if (CONFIG_LIMIT_TRANS_AMOUNT == '1')
		{

			if(strstr($functionString, "right103,"))
			{///Home Delivery Services To be Added in File
				$User_Management[] = "limit_transactions.php,Admin Transaction Limit";
			}
		}

		if(CONFIG_EXCHNG_MARGIN_SHARE == '1')
		{
			if(strstr($functionString, "right114,"))
			{///Configure Margin Share To be Added in File
				$User_Management[] = "set-agent-share.php,Configure Margin Share";
			}
		}


		if(strstr($functionString, "right179,"))
		{///CPF Validation Link
			$User_Management[] = "CPF-Validator.php,Validate CPF Number";
		}


		if(CONFIG_PESHAWAR_REPORT == '1' || CONFIG_USER_COMMISSION_MANAGEMENT == '1')
		{
			if(strstr($functionString, "right120,"))
			{///Configure Margin Share To be Added in File,
				///for beacon crest agent commission or spinzar distributor commission
				if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
					$reportName = "Add ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission";
				}else{
					$reportName = "Configure Sub Distributor Commission";
				}
				$User_Management[] = "configure-distributor-commission.php,$reportName";
			}
		}

		if(CONFIG_USER_COMMISSION_MANAGEMENT == '1')
		{
			if(strstr($functionString, "right139,"))
			{///Configure Margin Share To be Added in File, for beacon crest agent commission

				$reportName = "Add ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission";

				$User_Management[] = "view-commission.php,$reportName";
			}
		}
		if(CONFIG_SHARE_OTHER_NETWORK == '1')
		{
			if(strstr($functionString, "right172,"))
			{///Manage API Commisssion
				$User_Management[] = "sharedUsersList.php,Manage Siblings";
			}
		}


	}

	if(CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1)
	{
		if(strstr($functionString, "right206,"))
		{// Multiple ID Types
			$User_Management[] = "id-types.php,Manage ID Types";
		}
	}

	if(strstr($functionString, "right240,"))
	{// Disabled & Suspended Users List
		$User_Management[] = "disabled-users-list.php,Disabled & Suspended Users";
	}
//End of User Managment

// start of online registered users

	if(strstr($functionString, "right288,"))
	{/// Senders Registered From Web Site
		$online_register_users[] = "senders_list_registered_from_website.php,New Registration";
	}
	if(strstr($functionString, "right310,"))
	{/// Senders Registered From Web Site
		$online_register_users[] = "prepaid-registration.php,Prepaid Registration";
	}
	if(strstr($functionString, "right290,"))
	{///View Saved Report
		$online_register_users[] = "view_gb_compliance_list.php,View GB Compliance List";
	}
	if(strstr($functionString, "right293,"))
	{
		$online_register_users[] = "issue_password_customer.php,Issue Password to Customers";
	}
	if(strstr($functionString, "right294,"))
	{
		$online_register_users[] = "customers_without_email.php,Customers List Without Email Addresses";
	}
	if(strstr($functionString, "right295,"))
	{
		$online_register_users[] = "email_template_composer.php,Email Template Composer";
	}
	if(strstr($functionString, "right296,"))
	{
		$online_register_users[] = "template_manager.php,Email Template Manager";
	}
	if(strstr($functionString, "right297,"))
	{
		$online_register_users[] = "document-upload-logs.php,Email Template Manager";
	}
	if(strstr($functionString, "right298,"))
	{
		$online_register_users[] = "send_online_link_customer.php,Send Online Link";
	}
// end of online registered users


///For Online Sender Admin
	$viewReport = False;
	for($reports = 16; $reports <= 21; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if($viewReport){
		$Sender_Admin = array();



		if(strstr($functionString, "right16,"))
		{///Quick Sender Search
			$Sender_Admin[] = "customer-list.php,Quick Sender Search";
		}
		if(strstr($functionString, "right17,"))
		{///Create Sender Transaction
			if(strstr($userDetails["rights"], "Create Transaction") || $agentType != 'Admin'){
				$Sender_Admin[] = "add-customer-transaction.php?create=Y,Create Sender Transaction";
			}
		}
		if(strstr($functionString, "right18,"))
		{///Transaction History
			$Sender_Admin[] = "transactions-history.php,Transaction History";
		}
		if(strstr($functionString, "right19,"))
		{///Today's Registration
			$Sender_Admin[] = "curr-customer-regis.php?msg=Y,Today's Registrations";
		}
		if(strstr($functionString, "right20,"))
		{///Today's Transactions
			$Sender_Admin[] = "curr-customer-transaction.php?msg=Y,Today's Transactions";
		}
		if(strstr($functionString, "right21,"))
		{///Today's Registration
			$Sender_Admin[] = "online-customer-account.php,Online Sender Account";
		}
		if(strstr($functionString, "right299,"))
		{///Today's Registration
			$Sender_Admin[] = "../online/admin/create_fx_rate.php,Create FX Rate";
		}
		if(strstr($functionString, "right300,"))
		{///Today's Registration
			$Sender_Admin[] = "../online/admin/manage_fx_rates.php,Manage FX Rate";
		}
		if(strstr($functionString, "right301,"))
		{///View XE Feed
			$Sender_Admin[] = "../online/admin/view_oanda_rates.php,View XE Feed";
		}
	}//End of Online Sender Admin


//For Services
	$viewReport = False;
	for($reports = 22; $reports <= 23; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}
	}
	if(strstr($functionString, "right228,") && CONFIG_ADD_PAYPOINT_TRANS == "1")
	{///Add/Vew Paypoint Services
		$viewReport= True;
	}
	if($viewReport){
		$Services_Management = array();
		if(strstr($functionString, "right22,"))
		{///Add/Update Services
			$Services_Management[] = "add-services.php,Add/Update Services";
		}
		if(strstr($functionString, "right228,") && CONFIG_ADD_PAYPOINT_TRANS == "1")
		{///Add/Vew Paypoint Services
			$Services_Management[] = "add-services-paypoint.php,Add/View Paypoint Service";
		}
		if(strstr($functionString, "right23,"))
		{///View Services
			$Services_Management[] = "amount-transfer-method.php,View Service";
		}
		if(strstr($functionString, "right245,"))
		{///View Services
			$Services_Management[] = "add-currency.php,Add/Update Currency";
		}
	}//End of Service


///For Fee

	$viewReport = False;
	for($reports = 24; $reports <= 26; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}

	if(CONFIG_CUSTOM_BANK_CHARGE == '1')
	{
		if(strstr($functionString, "right150,"))
		{
			$viewReport= True;
		}

		if(strstr($functionString, "right151,"))
		{
			$viewReport= True;
		}
	}

	if(strstr($functionString, "right199,"))
	{
		$viewReport= True;
	}


	if($viewReport)
	{
		if (CONFIG_FEE_DEFINED == '1') {
			$feeName = CONFIG_FEE_NAME;
		} else {
			$feeName = "Fee";
		}
		$Fee_Management = array();

		if(strstr($functionString, "right24,"))
		{///Add Fee
			$Fee_Management[] = "add-fee.php,Add $feeName";
		}
		if(strstr($functionString, "right25,"))
		{///Manage Commission
			$Fee_Management[] = "fee-list.php,Manage $feeName";
		}
		if(strstr($functionString, "right26,"))
		{///View Company Fee/Commission
			$Fee_Management[] = "fee-calander.php,View Company $feeName"."s";
		}

		if(strstr($functionString, "right219,"))
		{
			$Fee_Management[] = "distributorCommission.php,Distributor Commission";
		}

		if(CONFIG_CUSTOM_BANK_CHARGE == '1')
		{
			if(strstr($functionString, "right150,"))
			{
				$Fee_Management[] = "add-bank-charges.php,Set Bank Charges";
			}

			if(strstr($functionString, "right151,"))
			{
				$Fee_Management[] = "charges-list.php,View Bank Charges List";
			}

		}
	}


//For Exchange Rate
	$viewReport = False;
	for($reports = 27; $reports <= 30; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}



	if($viewReport){
		$Exchange_Rates = array();

		if(strstr($functionString, "right27,"))
		{///Add Rates
			$Exchange_Rates[] = "add-rate.php?message=exch,Add Rates";
		}
		if(strstr($functionString, "right28,"))
		{///Manage Exchange Rates
			$Exchange_Rates[] = "rate-list.php,Manage Exchange Rates";
		}


		if(strstr($functionString, "right174,"))
		{///Manage Old Exchange Rates
			$Exchange_Rates[] = "old-rate-list.php,Manage Old Exchange Rates";
		}

		if(strstr($functionString, "right29,"))
		{///Exchange Rates Table
			$Exchange_Rates[] = "exchange-rates-agent.php,Exchange Rates";
		}
		if(strstr($functionString, "right30,"))
		{///Import Exchange Rates
			$Exchange_Rates[] = "import-exchange-rates.php,Import Exchange Rates";
		}
	}
//End of Exchange Rate

///For Transactions
	$viewReport = False;
	for($reports = 31; $reports <= 54; $reports++)
	{

		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}

	for($reports = 93; $reports <= 94; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}


	if(strstr($functionString, "right91,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right118,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right122,") && CONFIG_MERCHANT_FILE_BEACON == '1')
	{
		$viewReport = true;
	}
	if(strstr($functionString, "right123,") && CONFIG_REFNUM_GENERATOR == '1')
	{
		$viewReport = true;
	}



	if(strstr($functionString, "right194,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
	{
		$viewReport = true;
	}

	if(strstr($functionString, "right171,"))
	{
		$viewReport = true;
	}
	if(strstr($functionString, "right182,") && CONFIG_MONEYPAID_ONCREDIT == '1')
	{
		$viewReport = true;
	}
	if(strstr($functionString, "right185,"))
	{
		$viewReport = true;
	}
	if(strstr($functionString, "right229,") && CONFIG_ADD_PAYPOINT_TRANS == '1')
	{
		$viewReport= True;
	}
	if($viewReport){
		$Transactions = array();

		if(strstr($functionString, "right31,"))
		{///Manage Detailed Transactions
			$Transactions[] = "payment-output-file.php?y=1,Detail Manage Transaction";
		}
		if(CONFIG_SUSPEND_TRANSACTION_ENABLED == '1')
		{

			if(strstr($functionString, "right32,"))
			{///Suspend Transactions
				$Transactions[] = "suspend-transaction.php,Suspend Transaction";
			}
		}
		if(strstr($functionString, "right135,"))
		{///Transactions By Money Paid
			$Transactions[] = "payment-mode-transactions.php,Transactions By Money Paid";
		}
		if(strstr($functionString, "right33,"))
		{///Add Sender
			$Transactions[] = "$customerPage,Add Sender";
		}
		/* Added by Niaz Ahmad at 23-06-2009 @4997: AMB */
		if(CONFIG_SENDER_AS_COMPANY == "1")
		{
			if(strstr($functionString, "right127,"))
			{///Add Company
				$Transactions[] = "add-company.php,Add Company";
			}
		}
		if(strstr($functionString, "right34,"))
		{///Update Sender
			$Transactions[] = "search-edit-cust.php,Update Sender";
		}

		if(CONFIG_IMPORT_TRANSACTION == '1' && strstr($functionString, "right105"))
		{
			$Transactions[] = "import-transactions.php,Import Transactions";
		}
		if(strstr($functionString, "right111,"))
		{///Update Sender
			if(CONFIG_IMPORT_PAGE_FOR_PC == "1"){
				$importPage = "import-customer-pc.php";
			}else{
				$importPage = "import-customer.php";
			}
			$Transactions[] = "$importPage,Import Senders";
		}

		if(strstr($functionString, "right136,"))
		{///Export Senders
			$Transactions[] = "export-customer.php,Export Senders";
		}

		if(strstr($functionString, "right112,"))
		{///Update Sender
			$Transactions[] = "import-beneficiary.php,Import Beneficiary";
		}
		if(strstr($functionString, "right178,"))
		{///Import Collection Point
			$Transactions[] = "import-collection-point-updated.php,Import Collection Point";
		}

		if(CONFIG_EXPORT_BENEFICIARY == "1"){
			if(strstr($functionString, "right144,"))
			{///export beneficiary
				$Transactions[] = "export-beneficiary.php,Export Beneficiary";
			}
		}

		if(CONFIG_PAYIN_CUSTOMER == "1")
		{

			if(strstr($functionString, "right35,") || $MSD_ID[0] == $userID)
			{///View Payin Book Sender
				$customerLabel = CONFIG_PAYIN_CUSTOMER_LABEL;
				$Transactions[] = "payin_Book_Customers.php,View". $customerLabel;

			}
		}
		if(strstr($functionString, "right36,"))
		{///Add Beneficiary
			if(CONFIG_REMOVE_QUICK_LINK != "1"){
				$Transactions[] = "add-beneficiary.php,Add Beneficiary";
			}else{
				$Transactions[] = "add-beneficiary-quick.php,Add Beneficiary";

			}
		}

		if(CONFIG_SEARCH_BENEFICIARY == '1')
		{
			if(strstr($functionString, "right100,"))
			{///Add Beneficiary
				$Transactions[] = "create-bene-trans.php,Search Beneficiary";
			}
		}
		if(strstr($functionString, "right239,"))
		{///Add Beneficiary As Company
			$Transactions[] = "add-ben-company.php,Add Beneficiary As Company";
		}

		if(strstr($functionString, "right37,"))
		{///Create Transactions
			if(strstr($userDetails["rights"], "Create Transaction")  || $agentType != 'Admin')
			{
				$Transactions[] = "$transactionPage?create=Y,Create Transaction";
			}
		}
		if(strstr($functionString, "right229,") && CONFIG_ADD_PAYPOINT_TRANS == '1')
		{// Create Paypoint Transaction
			$Transactions[] = "add-paypoint-transaction.php,Create Paypoint Transaction";
		}
		if(CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT == '1')
		{
			if(strstr($functionString, "right202,"))
			{///Bank Based Transaction
				$Transactions[] = "bank-trasactions-report.php,Bank Based Transactions";
			}
		}

		if(CONFIG_BATCH_TRANSACTION_ENABLED)
		{

			if(strstr($functionString, "right38,"))
			{///Create Batch Transaction
				$Transactions[] = "$transactionPage?create=Y&batch=Y,Create Batch Transaction";
			}
		}
		if(strstr($functionString, "right39,"))
		{///View Inputter Transaction
			$Transactions[] = "view_inputer_transactions.php,View inputter Transactions";
		}
		if(strstr($functionString, "right40,"))
		{///View Transactions
			$Transactions[] = "view-transactions.php,View Transactions";
		}
		if(strstr($functionString, "right43,"))
		{///Cancellation Request
			$Transactions[] = "request_cancellation.php?action=cancel,Cancellation Request";
		}

		if(DISPLAY_EDIT_TRANSACTIONS == '1')
		{
			if(strstr($functionString, "right41,")|| $MSD_ID[0] == $userID)
			{///Edit Transactions

				if(DISPLAY_TRANSACTIONS_LABEL == '1'){
					$Transactions[] = "edit-transactions.php,Edit Pending Transaction";
				}else{
					$Transactions[] = "edit-transactions.php,Edit Transaction";
				}
			}
		}
		if(strstr($functionString, "right42,")|| $MSD_ID[0] == $userID)
		{///To Ammend Transactions
			if(DISPLAY_TRANSACTIONS_LABEL == '1'){
				$Transactions[] = "amendment-transactions.php,Edit Authorized Transaction";
			}else{
				$Transactions[] = "amendment-transactions.php,To Amend Transaction";

			}
		}
		if(strstr($functionString, "right54,"))
		{///Cancellation Transactions
			$Transactions[] = "cancel_Transactions.php?act=cancel,Confirm Cancellation";
		}

		if(CONFIG_RETURN_CANCEL == "1"){
			if(strstr($functionString, "right148,"))
			{///Cancellation Transactions
				$Transactions[] = "cancelledTransactionsToReturn.php,Cancelled Transactions To Return";
			} }

		if(CONFIG_VERIFY_TRANSACTION_ENABLED)
		{
			if(strstr($functionString, "right89,"))
			{///Verify Transactions To be added in File for DB
				$Transactions[] = "verify-transactions.php?action=verify,New Transactions";
				//$Transactions[] = "verify-transactions.php?action=verify,Verify Transaction";

			}
		}
		if(strstr($functionString, "right44,"))
		{///Verify Agents Payments
			$Transactions[] = "verify_agent_account.php,Verify Agent Payments";
		}

		if(strstr($functionString, "right90,"))
		{///Verify A&D Payments To Be added in the File for DB
			$Transactions[] = "verify_AnD_account.php,Verify A&D Payments";
		}

		if(strstr($functionString, "right45,"))
		{///Online Bank Statement
			$Transactions[] = "import-barclays-fx.php,Reconcile Online Bank Statement";
		}
		if(strstr($functionString, "right46,"))
		{///Unresolved Bank Payments
			$Transactions[] = "unresolved-payments-fx.php,Unresolved Payments";
		}
		if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
		{

			if(strstr($functionString, "right47,") || $MSD_ID[0] == $userID)
			{///Payin Book Sender Account
				if (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1") {
					$payin_label = CONFIG_PAYIN_CUSTOMER_LABEL . " Account";
				} else {
					$payin_label = "Payin Book Sender Account";
				}
				$Transactions[] = "AgentsCustomerAccount.php,$payin_label";

			}
		}


		if(strstr($functionString, "right306,"))
		{///Unresolved Bank Payments
			$Transactions[] = "AgentCompanyAccount.php,Company Account";
		}
		if(strstr($functionString, "right48,") || $MSD_ID[0] == $userID)
		{///Authorize Transactions
			$Transactions[] = "authorize-transactions.php?action=authorize,Authorize Transactions";
		}

		if( defined("CONFIG_IMPORT_MT103_TRANSACTIONS") && CONFIG_IMPORT_MT103_TRANSACTIONS != "0" )
		{
			if(strstr($functionString, "right286,") || $MSD_ID[0] == $userID)
			{///Import MT103 Transactions
				$Transactions[] = "import-transactions-mt103.php,Import MT103 Transactions";
			}
		}

		if(strstr($functionString, "right190,") )
		{///Authorize cheque payment Transactions
			$Transactions[] = "authorizeCheque-transactions.php?action=authorize,Authorize Cheque Transactions";
		}

		if(strstr($functionString, "right182,") && CONFIG_MONEYPAID_ONCREDIT == '1')
		{///Money Paid On Credit Transactions
			$Transactions[] = "moneypaid_oncredit_trans.php,Money Paid On Credit Transactions";
		}

		if(IS_BRANCH_MANAGER == '1' || CONFIG_TOTAL_FEE_DISCOUNT == '1')
		{

			if(strstr($functionString, "right49,"))
			{///Fee Discount Request
				if(CONFIG_TOTAL_FEE_DISCOUNT == '1'){$fileName = CONFIG_DISCOUNT_LINK_LABLES;}else{ $fileName = "Fee Discount Request";}
				$Transactions[] = "discount-transactions.php,$fileName";

			}
		}

		if(CONFIG_HOLD_TRANSACTION_ENABLED)
		{

			if(strstr($functionString, "right52,") || $MSD_ID[0] == $userID)
			{///Hold Transaction
				$Transactions[] = "hold-transactions.php?action=hold,Hold Transaction";

			}

			if(strstr($functionString, "right53,") || $MSD_ID[0] == $userID)
			{//UnHold
				$Transactions[] = "unHold-transactions.php?action=unhold,Unhold Transaction";
			}
		}

		if(strstr($functionString, "right50,"))
		{///Transactions Status
			$Transactions[] = "trans-status.php,Transaction Status";
		}

		if(strstr($functionString, "right137,"))
		{///Export Transactions
			$Transactions[] = "export-trans.php,Export Transactions";
		}
		if(strstr($functionString, "right51,"))
		{///Distributor Output File
			$file = "Distributor_Output_File.php";
			if(defined("CONFIG_CUSTOMIZED_OUTPUT_FILE"))
				$file = CONFIG_CUSTOMIZED_OUTPUT_FILE;
			$Transactions[] = "$file,Distributor Output File";
		}
		if(strstr($functionString, "right123,") && CONFIG_REFNUM_GENERATOR == '1')
		{///Distributor Output File
			$Transactions[] = "ref-generator.php,Reference Number Generator";
		}



		if(strstr($functionString, "right142,"))
		{///Client configurations
			$Transactions[] = "add-confVar.php,Add Config Variable";
		}

		if(strstr($functionString, "right143,"))
		{///Client configurations
			$Transactions[] = "config.php,Manage Configurations";
		}

		if(strstr($functionString, "right118,")&& CONFIG_AVERT_CAMPAIGN == '1')
		{///Advertisement footer
			$Transactions[] = "advert_campaign.php,Advertisement Footer";
		}

		if(strstr($functionString, "right119,")&& CONFIG_AVERT_CAMPAIGN == '1')
		{///Manage Advertisement footer
			$Transactions[] = "advert_campaign_list.php,Manage Advertisement Footer";
		}

		if(strstr($functionString, "right91,"))
		{///Release Transactions
			$Transactions[] = "release-trans.php,Release Transaction";
		}


		if(strstr($functionString, "right122") && CONFIG_MERCHANT_FILE_BEACON == '1')
		{///Distributor Output File for Merchant Bank
			$Transactions[] = "Distributor_Output_File_Beacon.php,Distributor Output File for Merchant Bank";
		}

		if(strstr($functionString, "right93,"))
		{///Old Transaction Search
			$Transactions[] = "old-trans-search.php,Old Transaction Search";
		}
		if(strstr($functionString, "right94,"))
		{///Export New Transactions
			$Transactions[] = "ida-toml-excel-sheet.php,Export New Transactions";
		}
		if(strstr($functionString, "right171,"))
		{///Export New Transactions
			$Transactions[] = "import-connectplus.php,Import Transaction Status";
		}
		if(strstr($functionString, "right185,"))
		{///Inter-Distributor Funds transfer
			$Transactions[] = "interDistMoneyTransfer.php,Inter-Distributor Funds transfer";
		}
		if(strstr($functionString, "right185,"))
		{///Inter-Distributor Funds transfer
			$Transactions[] = "interDistMoneyTransfer.php,Inter-Distributor Funds transfer";
		}
		if(strstr($functionString, "right189,"))
		{///Inter-Distributor Funds transfer
			$Transactions[] = "inter-agent-money-transfer.php,Inter-Agent Funds transfer";
		}
		if(strstr($functionString, "right197,"))
		{ //Add Banks
			$Transactions[] = "add-bank.php?moneyPaid=By Bank Transfer&opener=left,Add Banks";
		}
		if(strstr($functionString, "right198,"))
		{ //Manage Banks
			$Transactions[] = "search-sender-bank.php?manage=y,Manage Banks";
		}

		/**
		 * Report that contant the history about the modification in transaction
		 * @Ticket #3337
		 */
		if(strstr($functionString, "right209,"))
			$Transactions[] = "transactionModifyHistory.php,Transaction Modify History";

		if(strstr($functionString, "right238,"))
			$Transactions[] = "manage_terms_conditions.php.php,Manage Terms & Conditions";

		if(strstr($functionString, "right244,"))
			$Transactions[] = "manage_customer_category.php,Manage Customer Category";

		if(strstr($functionString, "right291,"))
			$Transactions[] = "add_bank_details_rule.php,Create Country Base Rule for Beneficiary Bank Details";

		if(strstr($functionString, "right292,"))
			$Transactions[] = "view_bank_details_rule.php,View Bank Details Rules";
		/**
		 * #10340: Amb Exchange:Money Transfer output file
		 * Short Description
		 * Links are added for the work done
		 */
		if(strstr($functionString, "right248,"))
			$Transactions[] = "view-transactions-account-credit.php,Account Credit Output File";

		if(strstr($functionString, "right249,"))
			$Transactions[] = "view-transactions-cash.php,View Transaction Cash";
	}

///End of Transactions


	/**
	 * Cheque Cashing
	 * Making shorcut links to show at header
	 */
	if(CONFIG_ENABLE_CHEQUE_CASHING_MODULE == "1")
	{
		if(strstr($functionString, "right210,"))
			$arrChequeCashing[] = "chequeOrder.php,Create Cheque Order";

		if(strstr($functionString, "right211,"))
			$arrChequeCashing[] = "manageChequeFee.php,Manage Cheque Order Fee";

		if(strstr($functionString, "right212,"))
			$arrChequeCashing[] = "chequeReports.php,Cheque Report";

		if(strstr($functionString, "right213,"))
			$arrChequeCashing[] = "dailyCheque.php,Cheque Account";

		if(strstr($functionString, "right226,"))
			$arrChequeCashing[] = "chequeCommissionReport.php,Cheque Commission Report";
	}

//For Currency Exchange
	if(CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE == "1")
	{
		if(strstr($functionString, "right214,"))
			$arrCurrencyExchange[] = "add-currency-exchange.php,Add Currency Exchange Rate";

		if(strstr($functionString, "right215"))
			$arrCurrencyExchange[] = "currency-exchange-list.php,Manage Currency Exchange Rate";

		if(strstr($functionString, "right216"))
			$arrCurrencyExchange[] = "buySellCurr.php,Buy/Sell Currency Exchange";

		if(strstr($functionString, "right217"))
			$arrCurrencyExchange[] = "buySellCurrReports.php,View Buy/Sell Currency Reports";

		if(strstr($functionString, "right227"))
			$arrCurrencyExchange[] = "buySellCurrReports-cancelled.php,View Cancelled Currency Reports";

		if(strstr($functionString, "right241") && CONFIG_CURRENCY_STOCK=="1")
			$arrCurrencyExchange[] = "currencyExchangeStock.php,Currency Stock list";

		if(strstr($functionString, "right243"))
			$arrCurrencyExchange[] = "currency_revaluation_report.php,Currency Revaluation Report";
	}

///For FAQs
	$viewReport = False;
	for($reports = 55; $reports <= 56; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if($viewReport){
		$FAQs = array();

		if(strstr($functionString, "right55,"))
		{///Add FAQ
			$FAQs[] = "add-faq.php,Add FAQ";
		}
		if(strstr($functionString, "right56,"))
		{/// FAQ Listing
			$FAQs[] = "faqs-list.php,FAQs Listing";
		}
	}//End of FAQs


///For Enquiries
	$viewReport = False;
	for($reports = 57; $reports <= 58; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if($viewReport){
		$Enquiries = array();

		if(strstr($functionString, "right57,"))
		{///Add FAQ
			$Enquiries[] = "add-complaint.php,Make Enquiry";
		}
		if(strstr($functionString, "right58,"))
		{///View All Complaints
			$Enquiries[] = "view-complaint.php?sess=Not,View All";
		}
	}////End of Enquiries


///For Compliance
	$viewReport = False;
	for($reports = 152; $reports <= 168; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if($viewReport){
		$Compliance= array();

		if(strstr($functionString, "right152,"))
		{///View Compliance List
			$Compliance[] = "reportComplianceList.php,View Compliance List";
		}
		if(strstr($functionString, "right220,"))
		{///Manage Compliance List
			$Compliance[] = "compliance-manage-ListName.php,Manage Compliance List";
		}
		if(strstr($functionString, "right221,"))
		{///View PEPs List
			$Compliance[] = "view-pep-list.php,View PEPs List";
		}
		if(strstr($functionString, "right237,"))
		{///View  HM Treasury List
			$Compliance[] = "view-hm_treasury-list.php,View HM Treasury List";
		}

		if(strstr($functionString, "right179,"))
		{///View OFAC List
			$Compliance[] = "view-ofac-list.php,View OFAC List";
		}
		if(strstr($functionString, "right153,"))
		{///Import Compliance File
			$Compliance[] = "import-compliance.php,Import Compliance File";
		}
		if(strstr($functionString, "right154,"))
		{///Compliance Configuration at User Creation
			$Compliance[] = "compliance-configuration.php,Compliance Configuration at User Creation";
		}
		if(strstr($functionString, "right155,"))
		{///Compliance Configuration at Create Transaction
			$Compliance[] = "compliance-amount-configuration.php,Compliance Configuration at Create Transaction";
		}
		if(strstr($functionString, "right156,"))
		{///Amount AML Report
			$Compliance[] = "reportComplianceAmount.php,Amount AML Report";
		}
		if(strstr($functionString, "right157,"))
		{///View Popup List
			$Compliance[] = "compliance-view-popup.php,View Rule List";
		}
		if(strstr($functionString, "right180,"))
		{///View Users Creation Configuration
			$Compliance[] = "compliance-view-users-configuration.php,View Users Creation Configuration";
		}
		if(strstr($functionString, "right158,"))
		{///Sender Transaction Summary
			$Compliance[] = "sender-trans-summary.php,Sender Transaction Summary";
		}
		if(strstr($functionString, "right159,"))
		{///Beneficiary Transaction Summary
			$Compliance[] = "beneficiary-trans-summary.php,Beneficiary Transaction Summary";
		}
		if(strstr($functionString, "right160,"))
		{///Compliance Users List
			$Compliance[] = "compliance-users-list.php,Compliance Users List";
		}
		if(strstr($functionString, "right161,"))
		{///Enable Disable Customer List
			$Compliance[] = "enable-disable-cust.php,Enable Disable Customer List";
		}
		if(strstr($functionString, "right277,"))
		{///Enable Disable Customer List
			$Compliance[] = "enable_disable_beneficiary.php,Enable Disable Beneficiary List";
		}
		if(strstr($functionString, "right162,"))
		{///Transaction AML Report
			$Compliance[] = "trans-AML-report.php,Transaction AML Report";
		}

		if (CONFIG_PAYMENT_MODE_REPORT_ENABLE == "1")
		{

			if(strstr($functionString, "right163,"))
			{///Payment Mode Report
				$Compliance[] = "payment-mode-report.php,Payment Mode Report";
			}
		}

		if (CONFIG_DOCUMENT_VERIFICATION == "1")
		{
			if(strstr($functionString, "right164,"))
			{///Add document to upload
				$Compliance[] = "selectDocument.php,Add Document";
			}
		}
		if (CONFIG_DOCUMENT_VERIFICATION == "1")
		{
			if(strstr($functionString, "right165,"))
			{///Add category to upload
				$Compliance[] = "selectCategory.php,Select Document";
			}
		}

		if (CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES == "1")
		{
			if(strstr($functionString, "right191,"))
			{///Add category to upload
				$Compliance[] = "documentCategories.php,Add / Edit Document Categories";
			}
		}

		if(CONFIG_ALERT == "1"){

			if(strstr($functionString, "right166,"))
			{///Add Alert
				$Compliance[] = "CONFIG_ADD_ALERT_PAGE,Add Alert";
			}
			if(strstr($functionString, "right167,"))
			{///Manage Alert
				$Compliance[] = "CONFIG_MANAGE_ALERT_PAGE,Manage Alert";
			}
			if(strstr($functionString, "right168,"))
			{///View Saved Report
				$Compliance[] = "compliance-save-query,View Saved Report";
			}
		}

		if(CONFIG_MANAGE_USER_CONFIGURATION == '1')
		{
			if(strstr($functionString, "right236,"))
			{///Configure Margin Share To be Added in File
				$Compliance[] = "manage-users-configuration.php,User Configuration";
			}
		}
		if(strstr($functionString, "right208,"))
		{ /* Old Transaction Right For User */
			$Compliance[] = "oldTransactionGroupRights.php,Old Transaction Right For User";
		}

	}////End of Compliance



//For Reports
	$viewReport = False;
	for($reports = 59; $reports <= 82; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}

	if(strstr($functionString, "right110,"))
	{
		$viewReport= True;
	}

	for($reports = 95; $reports <= 99; $reports++)
	{
		if(strstr($functionString, "right$reports"))
		{
			$viewReport= True;
		}

	}

	if(strstr($functionString, "right115,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right116,"))//customer mobile data output report
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right129,"))
	{
		$viewReport= True;
	}

	if(strstr($functionString, "right146,") && CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		$viewReport= True;
	}
	if(strstr($functionString, "right149,") && CONFIG_CUSTOM_BANK_CHARGE == '1')
	{
		$viewReport= True;
	}
	if(strstr($functionString, "right186,") && CONFIG_DAILY_CASH_REPORT == '1')
	{
		$viewReport= True;
	}
	if(strstr($functionString, "right230,") && CONFIG_ADD_PAYPOINT_TRANS == '1')
	{
		$viewReport= True;
	}
	if($viewReport){
		$Reports = array();
		if(strstr($functionString, "right110,") && CONFIG_PESHAWAR_REPORT == '1')
		{///Report Suspecious Transactions
			$Reports[] = "daily-sub-distributor-report.php,Sub Distributor Commission Report";
		}

		if(strstr($functionString, "right59,"))
		{///Report Suspecious Transactions
			$Reports[] = "commission_summary_report.php,Agent Commission Report";
		}

		if(strstr($functionString, "right181,"))
		{///Report Suspecious Transactions
			$Reports[] = "commission_summary_report_new.php,Agents Commission Report";
		}

		if(strstr($functionString, "right95,"))
		{///A&D Account Statment
			$Reports[] = "agent_Distributor_Account.php,A&D Account Statement";
		}
		if(strstr($functionString, "right170,"))
		{///Sub Agent Account Statment
			$Reports[] = "sub_agent_Account.php,Sub Agent Account Statement";
		}
		if(!strstr(CONFIG_DISABLE_REPORT, "DIST_COMM"))
		{

			if(strstr($functionString, "right60,"))
			{///Distributor Commission
				$Reports[] = "distributor-comm-summary.php,Distributor Commission Report";

			}
		}
		if(strstr($functionString, "right61,"))
		{///Report Daily Distributor
			$Reports[] = "daily-bank-transfer-report.php,Daily Distributor Report";
		}

		if(strstr($functionString, "right173,"))
		{///Report Distributor Bank Tranfer Payments
			$Reports[] = "view-detailed-bank-transaction-worldlink.php,Distributor Bank Transfer Payments Report";
		}

		if(strstr($functionString, "right62,"))
		{///Report Suspecious Transactions
			$Reports[] = "bank_Account_List.php,Distributor Statement";
		}
		if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
		{

			if(strstr($functionString, "right80,"))
			{///Daily Cashier Report
				$Reports[] = "teller_daily_report.php,Daily Cashier Report";

			}
		}
		if(strstr($functionString, "right73,"))
		{///Payout Center Report
			$Reports[] = "payout_center_report.php,Payout Center Report";
		}

		if(strstr($functionString, "right133,"))
		{///Fax Output Report
			$Reports[] = "fax_output_report.php,Fax Output Report";
		}

		if(!strstr(CONFIG_DISABLE_REPORT, "TELLER_ACCOUNT"))
		{

			if(strstr($functionString, "right81,"))
			{///Teller Account
				$Reports[] = "teller_Account.php,Teller Account";
			}
		}
		if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_AGENT"))
		{

			if(strstr($functionString, "right63,"))
			{///Daily Agent Report
				$Reports[] = "daily-agent-transfer-report.php,Daily Agent Report";
			}
		}



		if(!strstr(CONFIG_DISABLE_REPORT, "OUSTSTANDING_AGENT"))
		{

			if(strstr($functionString, "right64,"))
			{///Out standing Agent Statment
				$Reports[] = "outStandingPay.php,Outstanding Agent Payments";
			}
		}
		if(strstr($functionString, "right65,"))
		{///Daily Transaction Summary
			$Reports[] = "daily-trans.php,Daily Transaction Summary";
		}
		if(strstr($functionString, "right66,"))
		{///Transaction Transfer Report
			$Reports[] = "transferReport.php,Transaction Transfer Report";
		}

		if(strstr($functionString, "right121,"))
		{///Report Transaction Summary
			$Reports[] = "repTransSummary.php,Report Transaction Summary";
		}

		if(strstr($functionString, "right67,"))
		{///Sender BeneFiciary Report
			$Reports[] = "beneficiaryReport.php,Sender/Beneficiary Report";
		}
		if(strstr($functionString, "right68,"))
		{///Report Sender Registration Report
			$Reports[] = "current-regis.php,Sender Registration Report";
		}
		if(strstr($functionString, "right138,"))
		{///Beneficiary Registration Report
			$Reports[] = "current-ben-regis.php,Beneficiary Registration Report";
		}
		if(!strstr(CONFIG_DISABLE_REPORT, "RATE_EARNING"))
		{

			if(strstr($functionString, "right69,"))
			{///Exchange Rate Earning
				$Reports[] = "company_profit.php,Exchange Rate Earning Report";

			}
		}


		if(strstr($functionString, "right70,"))
		{///Agent Account Balance

			if(CONFIG_SALES_AS_BALANCE == '1')
			{
				$Reports[] = "sales_Report.php,Agent Account Balance";
			}else{
				$Reports[] = "Agent_account_statement.php,Agent Account Balance";
			}
		}
		if(strstr($functionString, "right71,"))
		{///Agent Country Based Transactions
			if(CONFIG_AGENT_COUNTRY_BASED == '1')
			{
				$reportName = CONFIG_AGENT_COUNTRY_BASED_LABEL;
			}else{
				$reportName = "Agent Country Base Transactions";
			}
			$Reports[] = "agent_country_base_transactions.php,$reportName";
		}
		if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
		{
			if(strstr($functionString, "right72,"))
			{///Sender Account Balance
				$Reports[] = "customer_account_statement.php,Sender Account Balance";
			}
		}
		if(strstr($functionString, "right74,"))
		{///Outstanding Payout Centre Report
			$Reports[] = "outstanding_payout_center_report.php,Outstanding Payout Center Report";
		}

		if (CONFIG_AGENT_STATMENT_GLINK == '1')
		{

			if(strstr($functionString, "right132,"))
			{///Agent Statment
				$Reports[] = "agent_Account_List.php,Agent Statement";
			}
		}
		if(CONFIG_SALES_REPORT == '1')
		{

			if(strstr($functionString, "right79,"))
			{///Sender BeneFiciary Report
				$Reports[] = "sales_Report.php,Daily Sales Report";
			}
		}
		if(strstr($functionString, "right75,"))
		{///Agent Account Statment
			$Reports[] = "agent_Account.php,Agent Account Statement";
		}
		if(strstr($functionString, "right113,"))
		{///Sub A&D Account Statement
			$Reports[] = "sub_Account.php,Sub A&D Account Statement";
		}
		if(strstr($functionString, "right76,"))
		{///Distributor Account Statement
			$Reports[] = "bank_Account.php,Distributor Account Statement";
		}
		if(strstr($functionString, "right124,") && CONFIG_ACCOUNT_SUMMARY == '1'){
			$Reports[] = "agent_Account_Summary.php,Account Summary";
		}
		if(strstr($functionString, "right230,") && CONFIG_ADD_PAYPOINT_TRANS == '1')
		{
			$Reports[] = "account-paypoint-trans.php,Account Summary Paypoint";
		}

		if(CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT == '1'){

			if(strstr($functionString, "right125,"))
			{///consolidated sender registration report
				$Reports[] = "consolidated-sender-regis-report.php,Consolidated Sender Registration Report";
			}
		}

		if(strstr($functionString, "right126,") && CONFIG_CUST_AML_REPORT == '1'){
			$Reports[] = "cust-aml-report.php,Sender AML Report";
		}

		if(strstr($functionString, "right147,") && CONFIG_CONNECT_PLUS_DIST_COM_REPORT == '1'){
			$Reports[] = "connectPlusDistCommReport.php,Account Statement Report";
		}

		if(strstr($functionString, "right134,") && $agentType == 'MLRO'){
			$Reports[] = "cust-accumulate-aml-report.php,Sender Accumulative Amount AML Report";
		}

		if(strstr($functionString, "right129,") && CONFIG_EXPORT_ALL_TRANS == '1'){
			$Reports[] = "extract_data.php,Export All Transactions";
		}
		if(strstr($functionString, "right77,"))
		{///Payex Audit Logs
			$Reports[] = "user_audit_log_screen.php,Payex Audit Logs";
		}
		if(strstr($functionString, "right78,"))
		{///Sender BeneFiciary Report
			$Reports[] = "Transaction_Audit.php,Transactions Audit Window";
		}

		if(CONFIG_INTERMEDIATE_EXRATE_BREAK_UP == "1"){
			if(strstr($functionString, "right187,"))
			{///intermediate exchange rate break up report
				$reportsLabel = CONFIG_INTERMEDIATE_EXRATE_BREAK_UP_LABEL;
				$Reports[] = "transactionCurrencyBreakup.php,$reportsLabel";
			}
		}
		if(strstr($functionString, "right176,"))
		{///Daily Report for Spinzar Added by Niaz Ahmad #2829 at 20-03-2008
			$Reports[] = "daily_report.php,Daily Report";
		}

		if(strstr($functionString, "right196,"))
		{///Daily cash book
			$Reports[] = "cashBook.php,Daily Cash Book";
		}


		if(strstr($functionString, "right204,"))
		{///Daily cash book for Global Exchange
			$Reports[] = "cashBookGlobalExchange.php,Daily Cash Book";
		}

		if(strstr($functionString, "right201,"))
		{///Daily cash book history list
			$Reports[] = "cashBookSummary.php,Cash Book History";
		}

		if(strstr($functionString, "right205,"))
		{///Daily cash book history for Global Exchange
			$Reports[] = "cashBookSummaryGlobalExchange.php,Cash Book History";
		}

		if(strstr($functionString, "right195,"))
		{///Daily Expenses
			$Reports[] = "expenses.php,Daily Expenses";
		}


		if(strstr($functionString, "right192,"))
		{
			$Reports[] = "outstandingBalanceReport.php,Agent Outstanding Balance Report";
		}

		if(strstr($functionString, "right200,"))
		{  /* Dual Ledger Report For A&D */
			$Transactions[] = "dualLedgerReport.php?,Dual Ledger Report For A&D";
		}

		if(strstr($functionString, "right202,"))
		{  /* GE Distributor Transaction Statement */
			$Transactions[] = "distributorTransactionStatement.php?,GE Distributor Transaction Statement";
		}

		if(strstr($functionString, "right203,"))
		{  /* Distributor Email for Export & Email Transaction Summary */
			$Transactions[] = "distributorEmail.php?,Distributor Email";
		}


		if(strstr($functionString, "right82,"))
		{///Report Suspecious Transactions
			if(CONFIG_REPORT_SUSPICIOUS == '1')
			{
				$Reports[] = "release-trans.php,Report Suspicious Transaction";
			}
		}
		if(strstr($functionString, "right96,"))
		{///Export New Transactions
			if (CONFIG_CURR_DENOMINATION == "1") {
				$currDenominationLabel = CONFIG_CURR_CAPTION;
				$Reports[] = "DCurrency-List.php,$currDenominationLabel";

			}
		}
		if(strstr($functionString, "right97,") && CONFIG_DIALY_COMMISSION == '1')
		{///Export New Transactions
			$Reports[] = "daily-commission-summary.php,Daily Commission Report";
		}
		if(strstr($functionString, "right98,") &&  CONFIG_PROFIT_EARNING == '1')
		{
			$Reports[] = "profit_earning_Bayba.php,Profit Earning Report";
		}
		if(strstr($functionString, "right99,") &&  CONFIG_PROFIT_EARNING == '1')
		{
			$Reports[] = "profit_earning_list.php,Profit Earning Report List";
		}

		if(strstr($functionString, "right115,") &&  CONFIG_EXCHNG_MARGIN_SHARE == '1')
		{
			$Reports[] = "profit-sharing-report.php,Margin Share Report";
		}


		//customer mobile output data
		if(strstr($functionString, "right116,") &&  CONFIG_CUSTOMER_MOBILE_NUMBER == '1')
		{
			$Reports[] = "customer_data_report.php,Export Mobile Customer Data";
		}
//customer ID output data
		if(strstr($functionString, "right145"))
		{
			$Reports[] = "export_customer_ID.php,Export ID Customer";
		}

		if(strstr($functionString, "right146,") && CONFIG_SHARE_OTHER_NETWORK == '1')
		{
			$Reports[] = "APITransactionReportLevel1.php,Shared Transactions Report";
		}

		if(strstr($functionString, "right149,") && CONFIG_CUSTOM_BANK_CHARGE == '1')
		{
			$Reports[] = "distributor-payement.php,Payment To Distributor";
		}
		if(strstr($functionString, "right186,") && CONFIG_DAILY_CASH_REPORT == '1')
		{//daily cash report
			$cashReportLabel = CONFIG_REPORT_CAPTION;
			$Reports[] = "cashier_distributor_transSum.php,$cashReportLabel";
		}

	}//View Report Condition End

	$deal_sheet = array();

	if(strstr($functionString, "right242,") && CONFIG_DISTRIBUTION_DEAL_SHEET == '1')
	{///Add Deal Sheet
		$deal_user = defined("CONFIG_DEAL_SHEET_USER_TYPE")?CONFIG_DEAL_SHEET_USER_TYPE:"";
		$deal_sheet[] = "add_deal_sheet.php,$deal_user Deal Sheet";
	}
	/* Add trading module premier exchange*/
	if(CONFIG_ADD_TRADING_MODULE == '1')
	{
		$trading = array();
		if(strstr($functionString, "right254,"))
		{///Add Account
			$trading[] = "add-account.php,Add Account";
		}
		if(strstr($functionString, "right255,"))
		{///Update Account
			$trading[] = "update-account.php,Update Account";
		}
		if(strstr($functionString, "right256,"))
		{///Add Trade
			$trading[] = "add-trade.php,Add Trade";
		}
		if(strstr($functionString, "right257,"))
		{///Edit / Cancell Trade
			$trading[] = "update-trade.php,Edit / Cancell Trade";
		}
		if(strstr($functionString, "right258,"))
		{///View Accounts
			$trading[] = "view-accounts.php,View Accounts";
		}
	}
	/* Add double entry module AMB*/
	if(CONFIG_DOUBLE_ENTRY == '1')
	{
		$doubleEntry = array();
		if(strstr($functionString, "right259,"))
		{///Add Account
			$doubleEntry[] = "add-account.php,Add Account";
		}
		if(strstr($functionString, "right260,"))
		{///Update Account
			$doubleEntry[] = "update-account.php,Update Account";
		}
		if(strstr($functionString, "right261,"))
		{///View Accounts
			$doubleEntry[] = "view-ledgers.php,View Accounts";
		}
		if(strstr($functionString, "right262,"))
		{///Manage Chart of Accounts
			$doubleEntry[] = "manage-chart-of-account.php,Manage Chart of Accounts";
		}
		if(strstr($functionString, "right263,"))
		{///Inter Account Transfer
			$doubleEntry[] = "inter-account-transfer.php,Inter Account Transfer";
		}
		if(strstr($functionString, "right264,"))
		{///View Chart of Account
			$doubleEntry[] = "chart-of-accounts.php,View Chart of Account";
		}

		if(CONFIG_TRANS_TYPE_TT == '1'){
			if(strstr($functionString, "right281,"))
			{///create tt transactions
				$doubleEntry[] = "TTtransfer.php,TT Transfers";
			}
			if(strstr($functionString, "right282,"))
			{///View sender accounts
				$doubleEntry[] = "view-sender-accounts.php,View Sender Account";
			}


			if(strstr($functionString, "right283,"))
			{///View TT transactions
				$doubleEntry[] = "view_TT_Trans.php,View TT Transactions";
			}
		}

		if(strstr($functionString, "right284,"))
		{///view Inter Fund Transfer Transactions
			$doubleEntry[] = "view_inter_fund_transfer_trans.php,View Inter Fund Transfer Transactions";
		}

		if(strstr($functionString, "right285,"))
		{///Trial Balance
			$doubleEntry[] = "trial-balance-new.php,Trial Balance";
		}
	}

///For Forum
	$viewReport = False;
	for($reports = 140; $reports <= 141; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if($viewReport){
		$Forum = array();

		if(strstr($functionString, "right140,"))
		{///Add Forum
			$Forum[] = "add_enquiry.php,Add FORUM";
		}
		if(strstr($functionString, "right141,"))
		{/// FORUM VIEW
			$Forum[] = "view_enquiry.php,FORUM VIEW";
		}
	}//End of Forum




///For Export module
	$viewReport = False;
	for($reports = 183; $reports <= 200; $reports++)
	{
		if(strstr($functionString, "right$reports,"))
		{
			$viewReport= True;
		}

	}
	if(CONFIG_EXPORT_MODULE == "1"){
		if($viewReport){
			$ExportData = array();

			if(strstr($functionString, "right183,"))
			{///distributor's old transactions
				$ExportData[] = "Distributor_Output_File_with_count.php?isExported=Y,Export Old Transactions";
			}

			if(strstr($functionString, "right184,"))
			{///distributor's old transactions
				$ExportData[] = "Distributor_Output_File_with_count.php?isExported=N,Export New Transactions";
			}

		}
	}//End export data


	if($viewReport){
		$Forum = array();

		if(strstr($functionString, "right140,"))
		{///Add Forum
			$Forum[] = "add_enquiry.php,Add FORUM";
		}
		if(strstr($functionString, "right141,"))
		{/// FORUM VIEW
			$Forum[] = "view_enquiry.php,FORUM VIEW";
		}
	}//End of Forum


	if($agentType == "Support" && $company !="MIL Money Exchange Ltd")
	{
		$Payex_Configuration = array();

		$Payex_Configuration[] = "add-country.php,Multicountry Payex";

	}

} else {
?>
<HTML>
<HEAD>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
	<!--
<link rel="stylesheet" href="images/interface.css" type="text/css">
-->
	<SCRIPT LANGUAGE="javascript" SRC="./javascript/functions.js"></SCRIPT>
	<!-- Collapsible tables list scripts -->
	<SCRIPT TYPE="text/javascript" LANGUAGE="javascript">
		<!--
		var isDOM      = (typeof(document.getElementsByTagName) != 'undefined' && typeof(document.createElement) != 'undefined') ? 1 : 0;
		var isIE4      = (typeof(document.all) != 'undefined' && parseInt(navigator.appVersion) >= 4) ? 1 : 0;
		var isNS4      = (typeof(document.layers) != 'undefined') ? 1 : 0;
		var capable    = (isDOM || isIE4 || isNS4)  ? 1 : 0;
		// Uggly fix for Opera and Konqueror 2.2 that are half DOM compliant
		if (capable) {
			if (typeof(window.opera) != 'undefined') {
				capable = 0;
			}
			else if (typeof(navigator.userAgent) != 'undefined') {
				var browserName = ' ' + navigator.userAgent.toLowerCase();
				if (browserName.indexOf('konqueror') > 0) {
					capable = 0;
				}
			} // end if... else if...
		} // end if
		var fontFamily = 'verdana, helvetica, arial, geneva, sans-serif';
		var fontSize   = 'small';
		var fontBig    = 'large';
		var fontSmall  = '14px';
		var isServer   = true;
		//-->
	</SCRIPT>
	<SCRIPT SRC="./javascript/menu.js" TYPE="text/javascript" LANGUAGE="javascript1.2"></SCRIPT>
	<NOSCRIPT>
		<STYLE TYPE="text/css">
			<!--
			div {color: #000000}
			.heada {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: small; color: #000000}
			.headaCnt {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: x-small; color: #000000}
			.parent {font-family: verdana, helvetica, arial, geneva, sans-serif; color: #FF0000; text-decoration: none; font-weight:bold}
			.child {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: x-small; color: #333399; text-decoration: none}
			.item, .item:active, .item:hover, .tblItem, .tblItem:active {color: #666666; text-decoration: none}/*333399---666666*/
			.tblItem:hover {color: #FF0000; text-decoration: underline; font-size:12px}
			//-->
		</STYLE>
	</NOSCRIPT>
	<STYLE TYPE="text/css">
		<!--
		body {font-family: verdana, helvetica, arial, geneva, sans-serif; font-size: small}
		a { color: #FF0000; text-decoration: none; font-size:12px;
		}

		//-->
	</STYLE>

</HEAD><BODY BGCOLOR="#C0C0C0">
<TABLE WIDTH="95%" BORDER="1" ALIGN="center" CELLPADDING="0" CELLSPACING="0" BORDERCOLOR="#006699">
	<TR>
		<TD HEIGHT="20" ALIGN="center" BORDERCOLOR="#CCCCCC" BGCOLOR="#666666"><FONT COLOR="#ffffff"><STRONG>Main
					Administration</STRONG></FONT></TD>
	</TR>
	<TR>
		<TD VALIGN="top" BORDERCOLOR="#999999" BGCOLOR="#CCCCCC"><TABLE WIDTH="100%" BORDER="0" CELLPADDING="0" CELLSPACING="2">
				<TR>
					<TD BORDERCOLOR="#FFFFFF"><STRONG><A HREF="main.php" class='tblItem' TARGET="mainFrame">Main Menu</A></span></STRONG></TD>
				</TR>
				<TR>
					<TD BORDERCOLOR="#FFFFFF">
						<?

						///For User Managment
						$viewReport = False;
						for($reports = 0; $reports <= 15; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								//echo("right$reports,");
								$viewReport= True;
							}

						}

						for($reports = 101; $reports <= 103; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}
						}
						for($reports = 176; $reports <= 177; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}
						}

						if(strstr($functionString, "right179,"))
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right86,")&& CONFIG_CITY_SERVICE_ENABLED == '1')
						{
							$viewReport= True;
						}

						for($reports = 107; $reports <= 109; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}
						}
						if(strstr($functionString, "right114,"))
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right120,") && (CONFIG_PESHAWAR_REPORT == '1' || CONFIG_USER_COMMISSION_MANAGEMENT))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right139,") && CONFIG_USER_COMMISSION_MANAGEMENT)
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right172,") && CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right223,") && CONFIG_ADD_USER_RIGHT == '1')
						{
							$viewReport= True;
						}
						if($viewReport){
							?>
							<DIV ID="el0Parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el0', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el0', '#D0DCE0')}">
								<A HREF="#" onClick="if (capable) {expandBase('el0', true); return false;}">
									<IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" ALT="+" WIDTH="9" HEIGHT="9" ID="el0Img"></A>
								<A HREF="#" class='tblItem' onClick="if (capable) {expandBase('el0', true); return false;}" ><STRONG>User Management</STRONG></A> </DIV>

							<DIV ID="el0Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el0', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el0', '#D0DCE0')}">

								<? if($userDetails["parentID"]== "0" && $agentType == "admin" ){ ?>
									&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add_super_admin.php">Add Super Admin</A><BR>
									&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_super_admin.php">Manage Super Admin</A><BR>
								<? }?>
								<?
								if(strstr($functionString, "right0,"))
								{////Add Super Agent
								$label_agent = "Add Super ";
								$new_url_ag = "add-agent.php";
								if(CONFIG_MVC_LAYOUT=="1"){
									$label_agent = "Add ";
									$new_url_ag = "new_agent.php";
								}
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$new_url_ag?>"><?=$label_agent?><?=__("Agent")?></A><BR>
									<?
									}
									?>

									<?
									if(strstr($functionString, "right1,"))
									{///Manage Super Agent
										$label_m_agent = "Manage Super ";
										if(CONFIG_MVC_LAYOUT=="1"){
											$label_m_agent = "Manage ";
										}
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent-list.php"><?=$label_m_agent?><?=__("Agent")?>s</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right2,"))
									{///Add Sub Agent
										//debug($functionString);
										$label_s_agent = "Add Sub ";
										if(CONFIG_MVC_LAYOUT=="1"){
											$label_s_agent = "Add Sub ";
										}
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-sub-agent.php?type=sub"><?=$label_s_agent?><?=__("Agent")?></A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right3,"))
									{///Manage Sub Agent
										$label_ms_agent = "Manage Sub ";
										if(CONFIG_MVC_LAYOUT=="1"){
											$label_ms_agent = "Manage Sub ";
										}
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent-list.php?type=sub"><?=$label_ms_agent?><?=__("Agent")?>s</A></nobr><BR>
										<?
									}
									?>

									<?
									if (CONFIG_AnD_ENABLE == '1') {
										if(strstr($functionString, "right106,"))
										{///Add A&D
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-agent_Distributor.php">Add Super A&D</A></nobr><BR>
											<?
										}
										?>

										<?
										if(strstr($functionString, "right107,"))
										{///Manage A&D
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent_Distributor-list.php">Manage Super A&D</A></nobr><BR>
											<?
										}

										if(strstr($functionString, "right108,"))
										{///Add A&D
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-sub-agent.php?type=sub&ida=ND">Add Sub A&D</A></nobr><BR>
											<?
										}
										?>

										<?
										if(strstr($functionString, "right109,"))
										{///Manage A&D
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent-list.php?type=sub&ida=ND">Manage Sub A&D</A></nobr><BR>
											<?
										}


									}
									?>

									<?
									if(strstr($functionString, "right4,"))
									{///Add Super Distributor
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-agent.php?ida=Y"> Add Super Distributor </A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right5,"))
									{///Manage Super Distributor
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent-list.php?ida=Y"> Manage Super Distributor</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right6,"))
									{///Add Sub Distributor
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-sub-agent.php?type=sub&ida=Y">Add Sub Distributor</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right7,"))
									{///Manage Sub Distributor
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent-list.php?type=sub&ida=Y">Manage Sub Distributor</A></nobr><BR>
										<?
									}
									?>


									<?
									if(strstr($functionString, "right175,"))
									{///Manage pay points
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="PPpoint-agent-list.php?type=sub&ida=Y">Manage Pay Points</A></nobr><BR>
										<?
									}
									?>

									<?

									if(strstr($functionString, "right117")  && CONFIG_EXPORT_TABLE_MGT == '1')
									{///Manage Export Transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_export_config.php">Manage Export Pages</A></nobr><BR>
										<?
									}

									$canAddStaff = true;
									if(defined("CONFIG_SUPER_AGENTS_THAT_CAN_ADD_ADMIN_STAFF") && $agentType=="SUPA")
									{
										if(strpos(CONFIG_SUPER_AGENTS_THAT_CAN_ADD_ADMIN_STAFF, $_SESSION["loggedUserData"]["userID"]) !== false)
											$canAddStaff = true;
										else
											$canAddStaff = false;
									}

									if(strstr($functionString, "right8,")  && $canAddStaff)
									{///Add Admin Staff
										if(CONFIG_ADMIN_STAFF_OPAL_PAGES == '1'){
											$addPage = "add-admin-opal.php";
										}else{
											$addPage = "add-admin.php";
										}
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$addPage?>?act=addAdmin">Add Admin Staff</A></nobr><BR>
										<?
									}



									if(strstr($functionString, "right9,") && $canAddStaff)
									{///Manage Admin Staff
										if(CONFIG_ADMIN_STAFF_OPAL_PAGES == '1'){
											$updatePage = "admins-list-opal.php";
										}else{
											$updatePage = "admins-list.php";
										}
										?>

										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$updatePage?>">Manage Admin Staff</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right10,"))
									{///New Agents
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="new-agent-list.php">New <?=__("Agent"); ?>s</A></nobr><nobr></nobr><BR>

										<?
									}
									?>

									<?
									if(strstr($functionString, "right101,") && (CONFIG_AGENT_RECEIPT_RANGE == "1" || CONFIG_DIST_REF_NUMBER == "1"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-receipt-range.php">Add <? if(CONFIG_DIST_REF_NUMBER == "1"){ echo CONFIG_DIST_REF_NAME; }else{ ?><?=__("Agent")?>s Receipt<? }?> Range</A></nobr><nobr></nobr><BR>

										<?
									}
									?>

									<?
									if(strstr($functionString, "right102,") && (CONFIG_AGENT_RECEIPT_RANGE == "1" || CONFIG_DIST_REF_NUMBER == "1"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-receipt-ranges.php">View <? if(CONFIG_DIST_REF_NUMBER == "1"){ echo CONFIG_DIST_REF_NAME; }else{ ?><?=__("Agent")?>s Receipt<? }?> Ranges</A></nobr><nobr></nobr><BR>

										<?
									}
									?>

									<?
									if(strstr($functionString, "right193,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
									{///add sender's receipt number generator
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-sender-receipt-range.php">Add <?=__("Sender");?> Receipt Reference Number Generator</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right194,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
									{///view sender's receipt number generator
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-sender-receipt-ranges.php">View <?=__("Sender");?> Receipt Reference Number Generator</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right176,") && CONFIG_MANAGE_CLAVE == '1')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-clave-range.php">Add Distributor Clave Range</A></nobr><nobr></nobr><BR>

										<?
									}
									?>




									<?
									if(strstr($functionString, "right177,")  && CONFIG_MANAGE_CLAVE == '1')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-clave-ranges.php">View Distributor Clave Ranges</A></nobr><nobr></nobr><BR>

										<?
									}
									?>


									<? if (CONFIG_EXPORT_TABLE_MGT == "1")
									{


										if(strstr($functionString, "right85,"))
										{///Manage Export Pages Tobe Added in the DB
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_export_config.php">Manage Export Templates</A></nobr><BR>
											<?
										}

									}
									?>

									<?
									if(strstr($functionString, "right11,"))
									{///Assign Teller
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="assign_teller.php">Assign Teller</A></nobr><nobr></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right12,"))
									{///Manage Teller
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="teller_list.php">Manage Teller</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right13,"))
									{///Add Collection Point
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-collectionpoint.php">Add Collection Point</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right14,"))
									{///Manage Collection Point
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_collection_point.php">Manage Collection Point</A></nobr><BR>
										<?
									}
									?>


									<?
									if(CONFIG_PP_TYPE_ON_TRANSACTION == "1"){
										if(strstr($functionString, "right169,"))
										{///Import pay points
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-payPoints.php">Import Pay Points</A></nobr><BR>
											<?
										}}
									?>







									<? if(CONFIG_BACK_DATED == '1')
									{

										if(strstr($functionString, "right15,"))
										{///Right for Backdated

											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="backdated_trans_rights.php">Right for Backdated</A></nobr><BR>
											<?
										}
									}

									if (CONFIG_CITY_SERVICE_ENABLED)
									{
										if(strstr($functionString, "right86,"))
										{///Home Delivery Services To be Added in File
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="city-services.php">Home Delivery Services</A></nobr><BR>
											<?
										}
									}
									?>
									<?
									if (CONFIG_LIMIT_TRANS_AMOUNT == '1')
									{

										if(strstr($functionString, "right103,"))
										{///Home Delivery Services To be Added in File
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="limit_transactions.php">Admin Transaction Limit</A></nobr><BR>
											<?
										}
									}

									?>

									<?
									if(strstr($functionString, "right114,") && CONFIG_EXCHNG_MARGIN_SHARE == "1")
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="set-agent-share.php">Configure Margin Share</A></nobr><nobr></nobr><BR>

										<?
									}
									?>

									<?

									if(CONFIG_DISTRIBUTOR_COMMISSION_REPORT_HIDE_COMMISSION_COLUMN != 1)
									{

										if(strstr($functionString, "right120,") && (CONFIG_PESHAWAR_REPORT == '1' || CONFIG_USER_COMMISSION_MANAGEMENT == '1'))
										{
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="configure-distributor-commission.php"><? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){echo("Add ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");}else{ echo ("Configure Sub Distributor Commission");}?></A></nobr><nobr></nobr><BR>
											<?
										}
										?>
										<?
										if(strstr($functionString, "right139,") && CONFIG_USER_COMMISSION_MANAGEMENT == '1')
										{
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-commission.php"><? echo("View ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");?></A></nobr><nobr></nobr><BR>

											<?
										}

									}
									?>
									<?
									if(strstr($functionString, "right172,") && CONFIG_SHARE_OTHER_NETWORK == '1')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sharedUsersList.php">Manage Siblings </A></nobr><nobr></nobr><BR>

										<?
									}
									?>
									<?
									// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types


									if (strstr($functionString, "right206,") && CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1 )
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="id-types.php">Manage ID Types</A></nobr><nobr></nobr><BR>
										<?
										if(strstr($functionString, "right207,"))
										{
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="CPF-Validator.php">Validate CPF Number</A></nobr><nobr></nobr><BR>

											<?
										}
										?>

										<?
									}
									// End of code against #3299: MasterPayex - Multiple ID Types

									?>
									<?
									if(strstr($functionString, "right240,"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="disabled-users-list.php">Disabled & Suspended Users </A></nobr><nobr></nobr><BR>

										<?
									}
									?>

									<?
									// Added by Aslam Shahid against #4657: MilExchange
									if (CONFIG_ADD_USER_RIGHT=="1" && strstr($functionString, "right223,"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="setUserRights.php">Validate System Time</a></nobr><nobr></nobr><br>
										<?
									}
									// Ends of code against #4657: MilExchange

									?>
							</DIV>
							<?
						}//End of User Managment
						?>

						<!-- start of online register user -->
						<?
						if($viewReport){
							?>
							<DIV ID="e40Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e40', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e40', '#D0DCE0')}">
								<A HREF="#" onClick="if (capable) {expandBase('e40', true); return false;}">
									<IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" ALT="+" WIDTH="9" HEIGHT="9" ID="e40Img"></A>
								<A HREF="#" class='tblItem' onClick="if (capable) {expandBase('e40', true); return false;}" ><STRONG>Online Register Users</STRONG></A> </DIV>

							<DIV ID="e40Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e40', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e40', '#D0DCE0')}">
								<?php
								if(strstr($functionString, "right288,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="senders_list_registered_from_website.php">New Registration</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right310,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="prepaid-registration.php">Prepaid Registration</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right290,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_gb_compliance_list.php">View GB Compliance List</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right293,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="issue_password_customer.php">Issue Password to customer</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right294,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="customers_without_email.php">Customers List Without Email Addresses</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right295,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="email_template_composer.php">Email Template Composer</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right296,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="template_manager.php">Email Template Manager</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right297,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="document-upload-logs.php">Registration Logs</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right298,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="send_online_link_customer.php">Send Online Link</A></nobr><BR>
									<?
								}
								?>

							</Div>
							<?
						}?>
						<!-- end of online register user-->

						<!-- start of Customer Admin-->
						<?
						///For Online Sender Admin
						$viewReport = False;
						for($reports = 16; $reports <= 21; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if(strstr($functionString, "right41,")|| $MSD_ID[0] == $userID){
							$viewReport= True;
						}
						if(strstr($functionString, "right265,") && CONFIG_USE_TRANSACTION_DETAILS=="1"){
							$viewReport= True;
						}
						if(strstr($functionString, "right266,") && CONFIG_VALUE_DATE_TRANSACTIONS=="1"){
							$viewReport= True;
						}
						if($viewReport){
							?>

							<DIV ID="el5Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}">
								<A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el5', true); return false;}">
									<IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el5Img"></A>
								<A HREF="#" onClick="if (capable) {expandBase('el5', true); return false;}" class='tblItem'>Online <?=__("Sender");?> Admin</A> </DIV>
							<DIV ID="el5Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}">


								<? if(defined("CONFIG_HIDE_ONLINE_LINKS_FROM_LEFTEXPRESS") && CONFIG_HIDE_ONLINE_LINKS_FROM_LEFTEXPRESS=="0"){?>

									<?
									if(strstr($functionString, "right16"))
									{///Quick Sender Search
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="customer-list.php">Quick <?=__("Sender");?> Search</A></nobr><BR>
										<?
									}
									?>





									<?php ?>
									<?
									if(strstr($functionString, "right17"))
									{///Create Sender Transaction
										if(strstr($userDetails["rights"], "Create Transaction") || $agentType != 'Admin'){
											/* add-customer-transaction.php?create=Y */
											?>

											<? if(defined("CONFIG_SHOW_CREATE_SENDER_TRANSACTION") && CONFIG_SHOW_CREATE_SENDER_TRANSACTION=="1"){?>
												<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-customer-transaction.php?create=Y">Create <?=__("Sender");?> Transaction</A></nobr><BR>
											<? }?>
											<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="customer-list.php?ct=h">Create <?=__("Sender")?> Transaction</a></nobr><br>
											<?
										}
									}
									?>


									<?
									if(strstr($functionString, "right18"))
									{///Transaction History
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="transactions-history.php">Transaction History</A></nobr><BR>
										<?
									}
									?>


									<?
									if(strstr($functionString, "right19"))
									{///Today's Registration
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="curr-customer-regis.php?msg=Y">Today's Registrations</A></nobr><BR>
										<?
									}
									?>
								<?php } /**End of Config of  CONFIG_HIDE_ONLINE_LINKS_FROM_LEFTEXPRESS*/?>

								<?
								if(strstr($functionString, "right20"))
								{///Today's Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="curr-customer-transaction.php?msg=Y">Today's Transactions</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right21"))
								{///Today's Registration
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="online-customer-account.php">Online <?=__("Sender")?> Account</A></nobr><BR>
								<? } ?>
								<?

								if(DISPLAY_EDIT_TRANSACTIONS == '1')
								{
									if(strstr($functionString, "right41,")|| $MSD_ID[0] == $userID)
									{///Edit Online Transactions
										if(DISPLAY_TRANSACTIONS_LABEL == '1'){
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="edit-online-transactions.php">Edit Online Pending Transaction</a></nobr><br>
										<? }else{ ?>
											<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="edit-online-transactions.php">Edit Online Transaction</a></nobr><br>

										<? }
									}
								}
								?>

								<?
								if(strstr($functionString, "right42,")|| $MSD_ID[0] == $userID)
								{///To Ammend Transactions
									if(DISPLAY_TRANSACTIONS_LABEL == '1'){
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="amendment-online-transactions.php"> Edit Online Authorized Transaction</a></nobr><br>
									<? }else{ ?>
										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="amendment-online-transactions.php"> To Amend Online Transaction</a></nobr><br>

									<? }}
								if(strstr($functionString, "right299"))
								{///Create FX Rate
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="../online/admin/create_fx_rate.php">Add Exchange Rate Margin</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right300"))
								{///Manage FX Rate
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="../online/admin/manage_fx_rates.php">Manage Exchange Rate Margin</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right301"))
								{///View XE Feed
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="../online/admin/view_oanda_rates.php">View XE Feed</A></nobr><BR>
									<?
								}
								?>





							</DIV>
							<?
						}//End of Online Sender Admin
						?>
						<!-- end of Customer Admin  Starts Service Management -->
						<?

						//For Services
						$viewReport = False;
						for($reports = 22; $reports <= 23; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if($viewReport){
							?>
							<DIV ID="el1Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el1', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el1', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el1Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el1', true); return false;}" class='tblItem'>Services Management</A> </DIV>

							<DIV ID="el1Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el1', '#D0DCE0')}">
								<?
								if(strstr($functionString, "right22,"))
								{///Add/Update Services
									?>
									&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-services.php">Add/Update Services</A><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right23,"))
								{///View Services
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="amount-transfer-method.php">View Service</A><BR>
									<?
									}
									if(strstr($functionString, "right245,"))
									{///Add Currency
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-currency.php">Add/Update Currency</A><BR>
										<? } ?>
							</DIV>
							<?
						}//End of Service
						?>


						<!-- End of Services Managment -->
						<!-- Start of Commission/Fee -->
						<?
						///For Fee
						$viewReport = False;
						for($reports = 24; $reports <= 26; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						if(CONFIG_CUSTOM_BANK_CHARGE == '1')
						{
							if(strstr($functionString, "right150,"))
							{
								$viewReport= True;
							}

							if(strstr($functionString, "right151,"))
							{
								$viewReport= True;
							}
						}

						if(strstr($functionString, "right219,"))
						{
							$viewReport= True;
						}


						if($viewReport){
							?>
							<DIV ID="el2Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el2', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el2Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el2', true); return false;}" class='tblItem'><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Management</A> </DIV>
							<DIV ID="el2Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right24,"))
								{///Add Fee
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-fee.php">Add <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></A></nobr><BR>
									<?
								}
								?>

								<?

								if(strstr($functionString, "right25,"))
								{///Manage Commission
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="fee-list.php">Manage <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?></A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right26,"))
								{///View Company Fees/Commission
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="fee-calander.php">View Company <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fees");}?></A></nobr><BR>
									<?
								}
								?>


								<?
								if(strstr($functionString, "right150,") && CONFIG_CUSTOM_BANK_CHARGE == '1')
								{///Add Bank Charges
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-bank-charges.php"> Set Bank Charges </A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right151,") && CONFIG_CUSTOM_BANK_CHARGE == '1')
								{///View Bank Charges List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="charges-list.php"> View Bank Charges List </A></nobr><BR>
									<?
								}
								?>
								<?
								/**
								 * Distributor Commission Manager for WIHC
								 */
								if(strstr($functionString, "right219,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="distributorCommission.php">Distributor Commission</A></nobr><BR>
									<?
								}

								if(CONFIG_DISTRIBUTOR_COMMISSION_REPORT_HIDE_COMMISSION_COLUMN == 1)
								{
									if(strstr($functionString, "right120,") && (CONFIG_PESHAWAR_REPORT == '1' || CONFIG_USER_COMMISSION_MANAGEMENT == '1'))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="configure-distributor-commission.php"><? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){echo("Add ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");}else{ echo ("Configure Sub Distributor Commission");}?></A></nobr><nobr></nobr><BR>
										<?
									}
									if(strstr($functionString, "right139,") && CONFIG_USER_COMMISSION_MANAGEMENT == '1')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-commission.php"><? echo("View ".CONFIG_COMMISSION_MANAGEMENT_NAME." Commission");?></A></nobr><nobr></nobr><BR>

										<?
									}
								}
								?>


							</DIV>
							<?
						}
						?>

						<!-- End of Commission/Fee -->
						<!-- Start of Exchange Rate -->
						<?
						//For Exchange Rate
						$viewReport = False;
						for($reports = 27; $reports <= 30; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						if($agentType=="SUPA")
						{
							$viewReport=false;
						}
						if($viewReport){
							?>


							<DIV ID="el3Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el3', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el3Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Exchange Rates</A> </DIV>
							<DIV ID="el3Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right27"))
								{///Add Rates
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-rate.php?message='exch'">Add Rates</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right28"))
								{///Manage Exchange Rates
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="rate-list.php">Manage Exchange Rates</A></nobr><BR>
									<?
								}
								?>

								<? if(strstr($functionString, "right174"))
								{///Manage Old Exchange Rates
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="old-rate-list.php">Manage Old Exchange Rates</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right29"))
								{///Exchange Rates Table
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="exchange-rates-agent.php">Exchange Rates</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right30"))
								{///Import Exchange Rates
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-exchange-rates.php">Import Exchange Rates</A></nobr><BR>
									<?
								}
								?>
							</DIV>

							<?
						}//End of Exchange Rate
						?>






						<!-- Start of Exchange Exchange -->
						<?
						//For Currency Exchange
						if(CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE == "1")
						{
							$viewReport = False;
							for($reports = 214; $reports <= 217; $reports++)
							{
								if(strstr($functionString, "right$reports,"))
								{
									$viewReport= True;
								}
							}
							if(strstr($functionString, "right227,") && CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
							{
								$viewReport= True;
							}
							if(strstr($functionString, "right241,") && CONFIG_CURRENCY_STOCK=="1")
							{
								$viewReport= True;
							}
							if(strstr($functionString, "right243,"))
							{
								$viewReport= True;
							}
							if(strstr($functionString, "right246,") && defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && CONFIG_CURRENCY_EXCHANGE_FEE_USERS!="0")
							{
								$viewReport= true;
							}
							if(strstr($functionString, "right247,") && defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && CONFIG_CURRENCY_EXCHANGE_FEE_USERS!="0")
							{
								$viewReport= true;
							}
							if(strstr($functionString, "right269,") && CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS == '1')
							{
								$viewReport= true;
							}
							if($viewReport){
								?>


								<DIV ID="ece1Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('ece1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ece1', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('ece1', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="ece1Img"></A> <A HREF="#" onClick="if (capable) {expandBase('ece1', true); return false;}" class='tblItem'>Currency Exchange</A> </DIV>
								<DIV ID="ece1Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('ece1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ece1', '#D0DCE0')}">
									<?

									if(strstr($functionString, "right214"))
									{//Add Currency Exchange Rate
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-currency-exchange.php?message='exch'">Add Currency Exchange Rate</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right215"))
									{///Manag Currency Exchange Rate
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currency-exchange-list.php">Manage Currency Exchange Rate</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right216,"))
									{//Buy/Sell Currency Exchange
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="buySellCurr.php">Buy/Sell Currency Exchange</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right217,"))
									{///Manage Currency Exchange
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="buySellCurrReports.php">View Buy/Sell Currency Reports</A></nobr><BR>
										<?

									}
									if(strstr($functionString, "right227,") && CONFIG_CANCEL_TRANS_CURR_EXCH=="1")
									{///Manage Currency Exchange
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="buySellCurrReports-cancelled.php">View Cancelled Currency Reports</A></nobr><BR>
										<?

									}
									?>
									<?

									if(strstr($functionString, "right241,") && CONFIG_CURRENCY_STOCK=="1")
									{///Manage Currency Exchange
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currencyExchangeStock.php">Currency Stock list</A></nobr><BR>
										<?

									}
									if(strstr($functionString, "right243,"))
									{///Currency Revaluation Report
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currency_revaluation_report.php">Currency Revaluation Report</A></nobr><BR>
									<? } ?>
									<?
									if(strstr($functionString, "right246,") && defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && CONFIG_CURRENCY_EXCHANGE_FEE_USERS!="0")
									{///Currency Exchange Fee
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame"
																											HREF="manageCurrencyFee.php">Manage Currency Exchange Fee</A></nobr><BR>
									<? } ?>
									<?
									if(strstr($functionString, "right247,") && defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && CONFIG_CURRENCY_EXCHANGE_FEE_USERS!="0")
									{///Currency Exchange Fee Reports
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame"
																											HREF="buySellCommReports.php">Currency Exchange Fee Reports</A></nobr><BR>
									<? } ?>
									<?
									if(strstr($functionString, "right269,") && CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS == '1')
									{///Saved Currency Exchange Reports
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame"
																											HREF="buySellCurrReports-saved.php">Saved Currency Exchange Reports</A></nobr><BR>
									<? } ?>

									<?php
									if(defined("CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY") && CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1")
									{
										if(strstr($functionString, "right273,"))
										{ //FC (Foreign Currency) Stock History
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="foreign-currency-stock-history.php">FC Stock History</A></nobr><BR>
											<?php
										}
										if(strstr($functionString, "right274,"))
										{//Daily Deal Sheet
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currency_daily_deal_sheet_report_AMB.php">Daily Deal Sheet</A></nobr><BR>
											<?php
										}
										if(strstr($functionString, "right275,"))
										{//Stock Turn Over Report
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currency_stock_turnover_report.php">Stock Turn Over Report</A></nobr><BR>
											<?php
										}
										if(strstr($functionString, "right276,"))
										{//Daily Profit Report
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="currency_daily_profit_report.php">Daily Profit Report</A></nobr><BR>
											<?php
										}
									}
									?>

								</DIV>

								<?
							}
						}

						/*
            ?>
            <!-- End of Currency Exchange -->

			<!-- start of Customer Admin-->

            <!--div id="el5Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}">
              <a class="item" HREF="#" onclick="if (capable) {expandBase('el5', true); return false;}">
              <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el5Img"></a>
              <a href="#" onclick="if (capable) {expandBase('el5', true); return false;}" class='tblItem'><?=__("Sender")?> Admin</a> </div>
            <div ID="el5Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el5', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el5', '#D0DCE0')}">
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0">
              <a class="tblItem" target="mainFrame" HREF="customer-list.php">Quick
              <?=__("Sender")?> Search</a></nobr><br>
              <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transactions-history.php">Transaction History</a></nobr><br>
			  <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="curr-customer-regis.php?msg=Y">Today Registrations</a></nobr><br>
            </div-->

			 <!--end of Customer Admin-->
			<?
			*/
						?>


						<!-- Start of Paypoint Transactions -->
						<?
						//For Paypoint Transactions
						if(CONFIG_ADD_PAYPOINT_TRANS == "1")
						{
							$viewReport = False;
							for($reports = 228; $reports <= 230; $reports++)
							{
								if(strstr($functionString, "right$reports,"))
								{
									$viewReport= True;
								}
							}
							if($viewReport){
								?>


								<DIV ID="eptParent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('ept', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ept', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('ept', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="eptImg"></A> <A HREF="#" onClick="if (capable) {expandBase('ept', true); return false;}" class='tblItem'>Paypoint Transactions</A> </DIV>
								<DIV ID="eptChild" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('ept', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ept', '#D0DCE0')}">
									<?
									if(strstr($functionString, "right229,"))
									{///Create Paypoint Transaction
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-paypoint-transaction.php">Create Paypoint Transaction</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right230,") && CONFIG_ADD_PAYPOINT_TRANS == "1")
									{///Account Summary Paypoint
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="account-paypoint-trans.php">Account Summary Paypoint</A></nobr><BR>
										<?
									}
									?>
									<?
									if(strstr($functionString, "right228"))
									{///Add/View Paypoint Services
										?>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-services-paypoint.php">Add/View Paypoint Service</A><BR>
										<?
									}
									?>
								</DIV>

								<?
							}
						}
						?>
						<!-- End of Paypoint Transactions -->

						<?
						///For Transactions
						$viewReport = False;
						for($reports = 31; $reports <= 54; $reports++)
						{

							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						for($reports = 93; $reports <= 94; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						if(strstr($functionString, "right91,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right105,"))
						{
							$viewReport= True;
						}

						for($reports = 111; $reports <= 112; $reports++)
						{

							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						if(strstr($functionString, "right122,") && CONFIG_MERCHANT_FILE_BEACON == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right123,") && CONFIG_REFNUM_GENERATOR == '1')
						{
							$viewReport= True;
						}




						if(strstr($functionString, "right194,") && CONFIG_SENDER_NUMBER_GENERATOR == '1')
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right135,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right136,"))
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right137,"))
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right171,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right182,") && CONFIG_MONEYPAID_ONCREDIT == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right229,") && CONFIG_ADD_PAYPOINT_TRANS == "1")
						{
							$viewReport= True;
						}
						if($viewReport){
							?>
							<DIV ID="el4Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el4', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el4', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el4', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el4Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el4', true); return false;}" class='tblItem'>Transactions</A> </DIV>
							<DIV ID="el4Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el4', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el4', '#D0DCE0')}">

								<?
								if(strstr($functionString, "right31,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Manage Detailed Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payment-output-file.php?y=1">Detail Manage Transaction</A></nobr><BR>
									<?
								}
								?>

								<?
								if(CONFIG_SUSPEND_TRANSACTION_ENABLED == '1')
								{

									if(strstr($functionString, "right32,"))
									{///Suspend Transactions

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="suspend-transaction.php">Suspend Transaction</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right135,"))
								{///Transactions By Money Paid
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payment-mode-transactions.php">Transactions By Money Paid</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right33,"))
								{///Add Sender
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$customerPage?>">Add <?=__("Sender")?></A></nobr><BR>
									<?
								}
								////////////#7415 premier
								if(strstr($functionString, "right34,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Update Sender
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="search-edit-cust.php">Update <?=__("Sender")?></A></nobr><BR>
									<?
								}
								//////////////
								?>
								<?
								if(CONFIG_SENDER_AS_COMPANY == "1"){
									if(strstr($functionString, "right127,"))
									{///Add Company
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-company.php">Add Company</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right34,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Update Sender
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="search-edit-cust.php">Update <?=__("Sender")?></A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right116,") && CONFIG_CUSTOMER_MOBILE_NUMBER == "1")
								{///Export user mobile data
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="customer_data_report.php">Export Mobile Customer Data</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right145,") )
								{///Export customer ID Data
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="export_customer_ID.php">Export ID Customer</A></nobr><BR>
									<?
								}
								?>
								<?
								if(CONFIG_IMPORT_TRANSACTION == '1' && strstr($functionString, "right105"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-transactions.php">Import Transactions</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right111,"))
								{///Import Sender
									if(CONFIG_IMPORT_PAGE_FOR_PC == "1"){
										$importPage = "import-customer-pc.php";
									}else{
										$importPage = "import-customer.php";
									}
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$importPage?>">Import <?=__("Sender")?>s</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right136,"))
								{///Export Senders
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="export-customer.php">Export <?=__("Sender")?>s</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right112,"))
								{///Import Beneficiary
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-beneficiary.php">Import Beneficiary</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right178,"))
								{///Import Collection Points
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-collection-point-updated.php">Import Collection Point</A></nobr><BR>
									<?
								}
								?>
								<?
								if(CONFIG_EXPORT_BENEFICIARY == "1"){
									if(strstr($functionString, "right144,"))
									{///export Beneficiary
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="export-beneficiary.php">Export Beneficiary</A></nobr><BR>
										<?
									}
								}
								?>
								<?
								if(CONFIG_PAYIN_CUSTOMER == "1")
								{

									if(strstr($functionString, "right35,") || $MSD_ID[0] == $userID)
									{///View Payin Book Sender
										$customerLabel = CONFIG_PAYIN_CUSTOMER_LABEL;
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payin_Book_Customers.php">View <?=$customerLabel ?></A></nobr><BR>
										<?
									}
								}
								?>
								<?
								if(strstr($functionString, "right36,"))
								{///Add Beneficiary

									if(CONFIG_REMOVE_QUICK_LINK != "1"){

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-beneficiary.php">Add Beneficiary</A></nobr><BR>
										<?
									}else{?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-beneficiary-quick.php">Add Beneficiary</A></nobr><BR>

									<?	}

								}

								?>

								<?
								if(strstr($functionString, "right100,") && CONFIG_SEARCH_BENEFICIARY == '1')
								{///Add Beneficiary
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="create-bene-trans.php">Search Beneficiary</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right239,"))
								{///Add Beneficiary As Company
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-ben-company.php">Add Beneficiary As Company</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right37,"))
								{///Create Transactions
									if(strstr($userDetails["rights"], "Create Transaction")  || $agentType != 'Admin')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$transactionPage?>?create=Y">Create <?=$strTransLabel?></A></nobr><BR>
										<?
									}
								}
								if(CONFIG_SHOW_BANK_BASED_TRASACTIONS_REPORT)
								{

									if(strstr($functionString, "right218,"))
									{///Bank Based Transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="bank-trasactions-report.php">Bank Based Transactions</A></nobr><BR>
										<?
									}
								}

								if(CONFIG_BATCH_TRANSACTION_ENABLED)
								{

									if(strstr($functionString, "right38,"))
									{///Create Batch Transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$transactionPage?>?create=Y&batch=Y">Create Batch Transactions</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right39,"))
								{///View Inputter Transaction
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_inputer_transactions.php">View Inputter Transactions</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right40,"))
								{///View Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-transactions.php">View Transactions</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right31,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Manage Detailed Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payment-output-file.php?y=1">Detail Manage Transaction</A></nobr><BR>
									<?
								}

								if(DISPLAY_EDIT_TRANSACTIONS == '1' && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{
									if(strstr($functionString, "right41,")|| $MSD_ID[0] == $userID)
									{///Edit Transactions
										if(DISPLAY_TRANSACTIONS_LABEL == '1'){
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="edit-transactions.php">Edit Pending Transaction</A></nobr><BR>
										<?}else{ ?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="edit-transactions.php">Edit Transaction</A></nobr><BR>

										<? }
									}
								}
								?>
								<?
								if((strstr($functionString, "right42,")|| $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///To Ammend Transactions
									if(DISPLAY_TRANSACTIONS_LABEL == '1'){
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="amendment-transactions.php"> Edit Authorized Transaction</A></nobr><BR>
									<? }else { ?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="amendment-transactions.php"> To Amend Transaction</A></nobr><BR>

									<? }
								}

								if(defined("CONFIG_ASSIGN_MANUAL_CODE") && CONFIG_ASSIGN_MANUAL_CODE=="1"){
									if(strstr($functionString, "right218,"))
									{///Assign Manual Code
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="assign-manual-code.php">Assign Manual Code</A></nobr><BR>
										<?
									}
								}
								?>
								<?
								if(strstr($functionString, "right188,") && CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")
								{//Transaction without distributor
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-transaction-distributor.php">Assign Transactions to Distributor</A></nobr><BR>
									<?
								}
								?>



								<?
								if(strstr($functionString, "right43,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Cancellation Request
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="request_cancellation.php?action=cancel">Cancellation Request</A></nobr><BR>
									<?
								}
								if((strstr($functionString, "right48,") || $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Authorize Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="authorize-transactions.php?action=authorize">Authorize Transactions</A></nobr><BR>
									<?
								}

								if( defined("CONFIG_IMPORT_MT103_TRANSACTIONS") && CONFIG_IMPORT_MT103_TRANSACTIONS != "0" )
								{
									if((strstr($functionString, "right286,") || $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
									{///Import MT103 Transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-transactions-mt103.php">Import MT103 Transactions</A></nobr><BR>
										<?
									}
								}
								if(strstr($functionString, "right50,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Transactions Status
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trans-status.php">Transaction Status</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right91,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Release Transactions
									/* @Ticket #3973 */
									if(defined("CONFIG_RELEASE_BULK_TRANSACTIONS_FILE"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=CONFIG_RELEASE_BULK_TRANSACTIONS_FILE?>">Release Transaction</a></nobr><br>
										<?
									}
									else
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="release-trans.php">Release Transaction</A></nobr><BR>
										<?
									}
								}
								if(strstr($functionString, "right43,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Cancellation Request
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="request_cancellation.php?action=cancel">Cancellation Request</A></nobr><BR>
									<?
								}
								?>

								<?

								if(DISPLAY_EDIT_TRANSACTIONS == '1' && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{
									if(strstr($functionString, "right41,")|| $MSD_ID[0] == $userID)
									{///Edit Transactions
										if(DISPLAY_TRANSACTIONS_LABEL == '1'){
											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="edit-transactions.php">Edit Pending Transaction</A></nobr><BR>
										<? } else{ ?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="edit-transactions.php">Edit Transaction</A></nobr><BR>

										<? }
									}
								}

								if((strstr($functionString, "right42,")|| $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///To Ammend Transactions
									if(DISPLAY_TRANSACTIONS_LABEL == '1'){
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="amendment-transactions.php"> Edit Authorized Transaction</A></nobr><BR>
									<? }else { ?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="amendment-transactions.php"> To Amend Transaction</A></nobr><BR>

									<? }

								}

								if(strstr($functionString, "right54,"))
								{///Cancellation Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cancel_Transactions.php?act=cancel">Confirm Cancellation</A></nobr><BR>
									<?
								}

								if(CONFIG_RETURN_CANCEL == "1"){
									if(strstr($functionString, "right148,"))
									{///Cancelled Transactions To Return
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cancelledTransactionsToReturn.php">Cancelled Transactions To Return</A></nobr><BR>
										<?
									}
								}

								if(CONFIG_VERIFY_TRANSACTION_ENABLED)
								{
									if(strstr($functionString, "right89,"))
									{///Verify Transactions To be added in File for DB

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="verify-transactions.php?action=verify">New Transactions</A></nobr><BR>
										<?
									}
								}

								if(strstr($functionString, "right44,"))
								{///Verify Agents Payments
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="verify_agent_account.php">Verify <?=__("Agent")?> Payments</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right90,"))
								{///Verify A&D Payments To Be added in the File for DB
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="verify_AnD_account.php">Verify A&D Payments</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right45,"))
								{///Online Bank Statement
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-barclays-fx.php">Reconcile Online Bank Statement</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right303,"))
								{///Make Payment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="mt103-transaction.php">Make Payment</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right46,"))
								{///Unresolved Bank Payments

									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="unresolved-payments-fx.php">Unresolved Payments</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right307,"))
								{///Unresolved Bank Payments

									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="BarclaysPaymentQueue.php">Barclays Payments Queue</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right308,"))
								{///Unresolved Bank Payments

									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="verify-edited-payments.php">Verify Edited Payments</A></nobr><BR>
									<?
								}

								if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
								{

									if(strstr($functionString, "right47,") || $MSD_ID[0] == $userID)
									{///Payin Book Sender Account

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="AgentsCustomerAccount.php"><? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?> Account</A></nobr><BR>
										<?
									}
								}

								if(strstr($functionString, "right306,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="AgentCompanyAccount.php">Company Account</A></nobr><BR>	<?
								}

								?>
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-barclays-customer-module.php">Import Customer Barclays file</a></nobr><br>-->
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="import-collection-points.php">Import Collection Points</a></nobr><br-->
								<?

								if((strstr($functionString, "right48,") || $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Authorize Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="authorize-transactions.php?action=authorize">Authorize Transactions</A></nobr><BR>
									<?
								}

								if( defined("CONFIG_IMPORT_MT103_TRANSACTIONS") && CONFIG_IMPORT_MT103_TRANSACTIONS != "0" )
								{
									if((strstr($functionString, "right286,") || $MSD_ID[0] == $userID) && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
									{///Import MT103 Transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-transactions-mt103.php">Import MT103 Transactions</A></nobr><BR>
										<?
									}
								}
								if(strstr($functionString, "right190,") )
								{///Authorize cheque payment Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="authorizeCheque-transactions.php?action=authorize">Authorize Cheque Transactions</a></nobr><br>
									<?
								}
								?>

								<? if(CONFIG_MONEYPAID_ONCREDIT == '1')
								{

									if(strstr($functionString, "right182,"))
									{///Money Paid On Credit Transactions

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="moneypaid_oncredit_trans.php">Money Paid On Credit Transactions</A></nobr><BR>

										<?
									}
								}
								?>
								<? if(IS_BRANCH_MANAGER == '1' || CONFIG_BACKDATING_PAYMENTS == '1' || CONFIG_TOTAL_FEE_DISCOUNT == '1'|| CONFIG_FEE_DISCOUNT_REPORT == '1')
								{

									if(strstr($functionString, "right49,"))
									{///Fee Discount Request

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="discount-transactions.php"><? if(CONFIG_TOTAL_FEE_DISCOUNT == '1'){echo(CONFIG_DISCOUNT_LINK_LABLES);}elseif(CONFIG_FEE_DISCOUNT_REPORT == '1'){?>Fee Discount Report<?}else{ ?>Fee Discount Request<? } ?></A></nobr><BR>

										<?
									}
								}

								if(CONFIG_HOLD_TRANSACTION_ENABLED)
								{

									if(strstr($functionString, "right52,") || $MSD_ID[0] == $userID)
									{///Hold Transaction

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="hold-transactions.php?action=hold">Hold Transaction</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right53,") || $MSD_ID[0] == $userID)
									{//UnHold
										?>

										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="unHold-transactions.php?action=unhold">Unhold Transaction</a></nobr><br />
										<?
									}
								}


								if(strstr($functionString, "right50,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Transactions Status
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trans-status.php">Transaction Status</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right137,"))
								{///Export Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="export-trans.php">Export Transactions</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right51,"))
								{///Distributor Output File
									$file = "Distributor_Output_File.php";
									if(defined("CONFIG_CUSTOMIZED_OUTPUT_FILE"))
										$file = CONFIG_CUSTOMIZED_OUTPUT_FILE;
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=$file?>">Distributor Output File</a></nobr><br>
									<?
								}

								if(strstr($functionString, "right118,")&& CONFIG_AVERT_CAMPAIGN == '1')
								{///Advertisment footer
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="advert_campaign.php">Advertisement Footer</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right118,")&& CONFIG_AVERT_CAMPAIGN == '1')
								{///Manage Advertisment footer
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="advert_campaign_list.php">Manage Advertisement Footer</A></nobr><BR>
									<?
								}
								?>
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="trans-cust.php">Agent Transactions</a></nobr><br-->

								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="pending-amounts.php">Agent Pending Amounts</a></nobr><br-->
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent-comm.php">Agent Commission</a></nobr><br-->
								<?
								if(strstr($functionString, "right122,") && CONFIG_MERCHANT_FILE_BEACON == "1")
								{///Distributor Output File

									if($agentType == "admin" || $agentType == "Admin" || $agentType == "Branch Manager" || $agentType == "Admin Manager" || $agentType == "Call")
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Distributor_Output_File_Beacon.php">Distributor Output File for Merchant Bank</A></nobr><BR>
										<?
									}
									elseif($agentType == "SUPI")
									{
										$strPosition = strpos(DISTRIBUTOR_OUTPUT_FILE_FOR_SPECIFIC_USERS, $_SESSION["loggedUserData"]["userID"] );
										if($strPosition !== false)
										{

											?>
											<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Distributor_Output_File_Beacon.php">Distributor Output File for Merchant Bank</A></nobr><BR>
											<?
										}
									}


								}
								?>

								<?
								if(strstr($functionString, "right123,") && CONFIG_REFNUM_GENERATOR == '1')
								{///Distributor Output File
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="ref-generator.php">Reference Number Generator</A></nobr><BR>
									<?
								}
								?>




								<?
								if(strstr($functionString, "right142,") )
								{///Add Client Configuration
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-confVar.php">Add Config Variable</A></nobr><BR>
									<?
								}
								?>


								<?
								if(strstr($functionString, "right143,") )
								{///Manage Client Configuration
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="config.php">Manage Configurations </A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right91,") && CONFIG_CHANGE_ORDER_OF_LEFTMENU!="1")
								{///Release Transactions
									/* @Ticket #3973 */
									if(defined("CONFIG_RELEASE_BULK_TRANSACTIONS_FILE"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="<?=CONFIG_RELEASE_BULK_TRANSACTIONS_FILE?>">Release Transaction</a></nobr><br>
										<?
									}
									else
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="release-trans.php">Release Transaction</A></nobr><BR>
										<?
									}
								}

								if(strstr($functionString, "right203,"))
								{
									/* Distributor Email for Export & Email Transaction Summary */
									?>
									<nobr>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0">
										<A CLASS="tblItem" TARGET="mainFrame" HREF="distributorEmail.php">
											Distributor Email
										</A>
									</nobr>
									<BR>
									<?
								}

								if(strstr($functionString, "right207,"))
								{
									/* Attach Payout Receipt to Transaction */
									?>
									<nobr>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0">
										<A CLASS="tblItem" TARGET="mainFrame" HREF="view-transactions-attach-receipt.php">
											Attach Payout Receipt
										</A>
									</nobr>
									<BR>
									<?
								}

								?>




								<?
								if(strstr($functionString, "right93,"))
								{///Old Transaction Search
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="old-trans-search.php">Old Transaction Search</A></nobr><BR>
									<?
								}
								?>

								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="recalled-trans.php">Recalled Transactions</a></nobr><br-->
								<?
								if(strstr($functionString, "right94,"))
								{///Export New Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="ida-toml-excel-sheet.php">Export New Transactions</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right171,"))
								{///Export New Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-connectplus.php">Import Transaction Status</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right185,"))
								{///Export New Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="interDistMoneyTransfer.php">Inter-Distributor Funds transfer</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right189,"))
								{
									///Inter-Agent Funds transfer
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="inter-agent-money-transfer.php">Inter-<?=__("Agent")?> Funds transfer</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right224,"))
								{
									///Inter admin user Funds transfer
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="inter-admin-staff-transfer.php">Inter Admin Staff Funds transfer</A></nobr><BR>
									<?
								}



								if(strstr($functionString, "right209,"))
								{
									/**
									 * Report that contant the history about the modification in transaction
									 * @Ticket #3337
									 */
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="transactionModifyHistory.php">Transaction Modify History</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right238,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_terms_conditions.php">Manage Terms & Conditions </A></nobr><BR>

									<?
								}
								if(strstr($functionString, "right244,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage_customer_category.php">Manage Customer Category </A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right265,") && CONFIG_USE_TRANSACTION_DETAILS=="1" && CONFIG_CHANGE_ORDER_OF_LEFTMENU=="1")
								{///Incomplete Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="incomplete-transactions.php">Incomplete Transactions</A></nobr><BR>
								<? }
								if(strstr($functionString, "right266,") && CONFIG_VALUE_DATE_TRANSACTIONS=="1")
								{///Validate Value Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="validate-value-transactions.php">Validate Value Dated Transactions</A></nobr><BR>
								<? }
								if(strstr($functionString, "right291,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add_bank_details_rule.php">Create Country Base Rule for Beneficiary Bank Details</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right292,"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_bank_details_rule.php">View Bank Details Rules</A></nobr><BR>
									<?
								}

								/**
								 * Contain the bank related modules
								 * - Add bank
								 * - Manage Banks
								 */
								//				if(CONFIG_BEN_BANK_DETAILS == "1")
								//				{
								if(strstr($functionString, "right197,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-bank.php?moneyPaid=By Bank Transfer&opener=left">Add Banks for <? echo __("Sender")?>s</A></nobr><BR>

									<?
								}

								if(strstr($functionString, "right198,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="search-sender-bank.php?manage=y">Manage <?=__("Sender")?> Banks</A></nobr><BR>

									<?
								}

								if(strstr($functionString, "right271,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-bank.php?opener=left">Add Banks for <? echo __("Beneficiary")?></A></nobr><BR>

									<?
								}

								if(strstr($functionString, "right272,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="search-sender-bank.php?manage=y">Manage <?=__("Beneficiary")?> Banks</A></nobr><BR>

									<?
								}
								//				}
								?>

								<?
								/**
								 * #10340: Amb Exchange:Money Transfer output file
								 * Short Description
								 * Links are added for the work done
								 */
								if(strstr($functionString, "right248,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-transactions-account-credit.php">Account Credit Output File</A></nobr><BR>

									<?
								}

								?>

								<?
								/**
								 * #10340: Amb Exchange:Money Transfer output file
								 * Short Description
								 * Links are added for the work done
								 */
								if(strstr($functionString, "right249,"))
								{
									?>

									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-transactions-cash.php">View Transaction Cash</A></nobr><BR>

									<?
								}

								?>
							</DIV>

							<?
						}///End of Transactions
						// Trading Moule Starts
						$viewReport = FALSE;
						//debug($viewReport);
						if(CONFIG_ADD_TRADING_MODULE == '1'){
							for($reports = 254; $reports <= 258; $reports++)
							{
								if(strstr($functionString, "right$reports,"))
								{
									$viewReport= True;
								}
							}
							if(strstr($functionString, "right267,")){
								$viewReport= True;
							}
							if(strstr($functionString, "right268,")){
								$viewReport= True;
							}
						}
						//debug($viewReport);
						if(CONFIG_ADD_TRADING_MODULE == '1' && $viewReport){
							?>
							<DIV ID="el15Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el15', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el15', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el15', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el15Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el15', true); return false;}" class='tblItem'>Trading</A> </DIV>
							<DIV ID="el15Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el15', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el15', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right254"))
								{///Add Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-account.php">Add Account</A></nobr><BR>


									<?
								}
								if(strstr($functionString, "right255"))
								{///Update Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="update-account.php">Update Account</A></nobr><BR>

									<?
								}
								if(strstr($functionString, "right256"))
								{///Add Trade
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-trade.php">Add Trade</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right257"))
								{///Edit / Cancell Trade
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="update-trade.php">Edit / Cancel Trade</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right258"))
								{///View Accounts
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-accounts.php">View Accounts</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right267"))
								{///Deal Trades to Non-Trades
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trade-trans-deal.php">Deal Traded to Not-traded</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right268"))
								{///Trade Profit/Loss Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trade-commission-report.php">Trade Profit/Loss Report</A></nobr><BR>
									<?
								}
								?>
							</DIV>

							<?
						}
						// Trading Moule Ends
						/* Cheque Cashing Module */
						if(CONFIG_ENABLE_CHEQUE_CASHING_MODULE == "1")
						{

							$bolViewChequeCashingFileList = false;
							for($reports = 210; $reports <= 213; $reports++)
								if(strstr($functionString, "right$reports,"))
									$bolViewChequeCashingFileList = true;
							if(strstr($functionString, "right226,")){
								$bolViewChequeCashingFileList = true;
							}
							if($bolViewChequeCashingFileList)
							{
								?>

								<DIV ID="e22Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e22', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e22', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e22', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e22Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e22', true); return false;}" class='tblItem'>Cheque Cashing</A> </DIV>
								<DIV ID="e22Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e22', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e22', '#D0DCE0')}">
									<? if(strstr($functionString, "right210,")) { ?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="chequeOrder.php">Create Cheque Order</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right211,"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manageChequeFee.php">Manage Cheque Order Fee</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right212,"))
									{
										?>

										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="chequeReports.php">Cheque Report</A></nobr><BR>
										<?
									}

									if(strstr($functionString, "right213,"))
									{
										?>

										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="dailyCheque.php">Cheque Account</A></nobr><BR>
									<? } ?>
									<?
									if(strstr($functionString, "right226,"))
									{
										?>

										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="chequeCommissionReport.php">Cheque Commission Report</A></nobr><BR>
									<? } ?>
								</div>

								<?
							}

						}
						/* Cheque Cashing Module Ends */


						///For FAQs
						$viewReport = False;
						for($reports = 55; $reports <= 56; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if($viewReport){
							?>

							<DIV ID="el11Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el11', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el11', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el11', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el11Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el11', true); return false;}" class='tblItem'>FAQs</A> </DIV>
							<DIV ID="el11Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el11', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el11', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right55"))
								{///Add FAQ
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-faq.php">Add FAQ</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right56"))
								{/// FAQ Listing
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="faqs-list.php">FAQs Listing</A></nobr><BR>
									<?
								}
								?>
							</DIV>
							<?
						}//End of FAQs
						?>

						<?
						///For Enquiries
						$viewReport = False;
						for($reports = 57; $reports <= 58; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if($viewReport){
							?>

							<DIV ID="el6Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el6', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el6', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el6', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el6Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el6', true); return false;}" class='tblItem'>Enquiries</A> </DIV>
							<DIV ID="el6Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el6', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el6', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right57"))
								{///Add FAQ
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-complaint.php">Make Enquiry</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right58"))
								{///View All Complaints
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-complaint.php?sess='Not'">View All</A></nobr><BR>
									<?
								}
								?>
							</DIV>

							<?
						}////End of Enquiries
						?>

						<?
						///For Compliance
						$viewReport = False;
						for($reports = 152; $reports <= 168; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if(strstr($functionString, "right180,"))
						{
							$viewReport= True;
						}

						if($viewReport){
							?>

							<DIV ID="e39Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e39', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e39', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e39', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e39Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e39', true); return false;}" class='tblItem'>Compliance</A> </DIV>
							<DIV ID="e39Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e39', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e39', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right329"))
								{/// Add Compliance User Profile
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-compliance-user-profiling.php">Add Compliance User Profile</A></nobr><BR>
									<?
								}
								?>
								<?

								if(strstr($functionString, "right330"))
								{/// Manage Compliance User Profile
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-compliance-user-profiling.php">Manage Compliance User Profile</A></nobr><BR>
									<?
								}
								?>
								<?

								if(strstr($functionString, "right152"))
								{/// View Compliance List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="reportComplianceList.php">View Compliance List</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right220"))
								{/// View Compliance List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="compliance-manage-ListName.php">Manage Compliance List</a></nobr><br>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right221"))
								{/// View PEPs List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-pep-list.php">View PEPs List</a></nobr><br>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right237"))
								{/// View HM treasury List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="view-hm_treasury-list.php">View HM Treasury List</a></nobr><br>
									<?
								}
								?>
								<?

								if(strstr($functionString, "right179"))
								{/// View OFAC List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-ofac-list.php">View OFAC List</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right153"))
								{///Import Compliance File
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="import-compliance.php">Import Compliance File</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right154"))
								{///Compliance Configuration at User Creation
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-configuration.php">Compliance Configuration at User Creation</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right155"))
								{///Compliance Configuration at Create Transaction
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-amount-configuration.php">Compliance Configuration at Create Transaction</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right156"))
								{///Amount AML Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="reportComplianceAmount.php">Amount AML Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right157"))
								{///View Popup List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-view-popup.php">View Rule List</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right180"))
								{///View Users Creation Configuration
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-view-users-configuration.php">View Users Creation Configuration</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right158"))
								{///Sender Transaction Summary
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sender-trans-summary.php"><?=__("Sender")?> Transaction Summary</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right159"))
								{///Beneficiary Transaction Summary
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="beneficiary-trans-summary.php">Beneficiary Transaction Summary</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right160"))
								{///Compliance Users List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-users-list.php">Compliance Users List</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right161"))
								{///Enable Disable Customer List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="enable-disable-cust.php">Enable Disable Customer List</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right277"))
								{///Enable Disable Customer List
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="enable_disable_beneficiary.php">Enable Disable Beneficiary List</A></nobr><BR>
									<?
								}


								?>
								<?
								if(strstr($functionString, "right162"))
								{///Transaction AML Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trans-AML-report.php">Transaction AML Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if (CONFIG_PAYMENT_MODE_REPORT_ENABLE == "1")
								{

									if(strstr($functionString, "right163,"))
									{///Payment Mode Report

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payment-mode-report.php">Payment Mode Report</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(CONFIG_DOCUMENT_VERIFICATION == "1"){
									if(strstr($functionString, "right164,"))
									{///Add Document
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="selectDocument.php">Add Document</A></nobr><BR>
										<?
									}}
								?>
								<?
								if(CONFIG_ADD_EDIT_DOCUMENT_CATEGORIES == "1")
								{
									if(strstr($functionString, "right191,"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="documentCategories.php">Add Categories</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(CONFIG_DOCUMENT_VERIFICATION == "1")
								{
									if(strstr($functionString, "right165,"))
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="selectCategory.php">Document Category</A></nobr><BR>
										<?
									}
								}
								?>


								<?
								if(CONFIG_ALERT == "1"){

									if(strstr($functionString, "right166,"))
									{///Add Alert
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<? echo CONFIG_ADD_ALERT_PAGE ;?>">Add an Alert</A></nobr><BR>
										<?
									}
									?>

									<?
									if(strstr($functionString, "right167,"))
									{///Manage Alert
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<? echo CONFIG_MANAGE_ALERT_PAGE; ?>">Manage Alert</A></nobr><BR>
										<?
									}}
								?>

								<?
								if(strstr($functionString, "right168,"))
								{///View Saved Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="compliance-save-query.php">View Saved Report</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right289,"))
								{///Manage User Configuration
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="">User Configuration</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right208,"))
								{ /* Old Transaction Right For User */
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="oldTransactionGroupRights.php">Old Transaction Right For User</A></nobr><BR>
									<?
								}
								?>

							</DIV>

							<?
						}////End of Compliance
						?>



						<!--div id="el2Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el2', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el2Img"></a> <a href="#" onclick="if (capable) {expandBase('el2', true); return false;}" class='tblItem'><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Management</a> </div>
            <div ID="el2Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el2', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el2', '#D0DCE0')}">
			<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="fee-calander.php">Company <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("Fee");}?> Table</a></nobr><br>
            </div-->


						<?
						//For Reports
						$viewReport = False;
						for($reports = 59; $reports <= 82; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}

						if(strstr($functionString, "right110,"))
						{
							$viewReport= True;
						}

						for($reports = 95; $reports <= 99; $reports++)
						{
							if(strstr($functionString, "right$reports"))
							{
								$viewReport= True;
							}

						}

						if(strstr($functionString, "right115,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right116,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right124,") && CONFIG_ACCOUNT_SUMMARY == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right125,") && CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT == '1')
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right126,") && CONFIG_CUST_AML_REPORT == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right129,") && CONFIG_EXPORT_ALL_TRANS == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right138,"))
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right146,") && CONFIG_SHARE_OTHER_NETWORK == '1')
						{
							$viewReport= True;
						}

						if(strstr($functionString, "right170,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right186,") && CONFIG_DAILY_CASH_REPORT == '1')
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right222,") && CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT == '1')
						{
							$viewReport= True;
						}
						// For daily expense report.
						if(strstr($functionString, "right195,"))
						{
							$viewReport= True;
						}

						///Agent Commission Report [For Client]
						if(strstr($functionString, "right270,") && CONFIG_AGENT_COMMISSION_REPORT_FOR_CLIENT=='1')
						{
							$viewReport= True;
						}
						// For Cash Book ,Company Cash Book,User Denomination Report
						for($reports = 231; $reports <= 233; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						// For .
						if(strstr($functionString, "right278,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right279,"))
						{
							$viewReport= True;
						}
						if(strstr($functionString, "right280,"))
						{
							$viewReport= True;
						}

						if($viewReport){
							?>

							<DIV ID="e29Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e29', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e29', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e29', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e29Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e29', true); return false;}" class='tblItem'>Reports</A> </DIV>

							<DIV ID="e29Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e29', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e29', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right278,"))
								{///Sub Distributor Commission Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-import-payment-activity.php">Import Payment Audit</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right279,"))
								{///Sub Distributor Commission Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="unresolved-deleted-payments-fx.php">Unresolved Deleted Payments</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right280,"))
								{///Sub Distributor Commission Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="resolved-payments-ajax-fx.php?type=1">Resolved Payments(New)</A></nobr><BR>
									<?
								}
								?>
								<?

								if(strstr($functionString, "right110,") && CONFIG_PESHAWAR_REPORT == '1')
								{///Sub Distributor Commission Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-sub-distributor-report.php">Sub Distributor Commission Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right59,"))
								{///Report Suspecious Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="commission_summary_report.php"><?=__("Agent")?> Commission Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right270,") && CONFIG_AGENT_COMMISSION_REPORT_FOR_CLIENT=='1')
								{///Agent Commission Report [For Client]
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="commission_report_ajax.php"><?=__("Agent")?> Commission Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right181,"))
								{///new agents commission report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="commission_summary_report_new.php"><?=__("Agent")?>s Commission Report</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right95,"))
								{///A&D Account Statment
									if(CONFIG_DOUBLE_ENTRY == '1')
										$callPage = "agent_Distributor_Account_new.php";
									else
										$callPage = "agent_Distributor_Account.php";
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="<?=$callPage?>">A&D Account Statement</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right170,"))
								{///Sub Agent Account Statment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sub_agent_Account.php">Sub <?=__("Agent"); ?> Account Statement</A></nobr><BR>
									<?
								}
								?>

								<?

								if(!strstr(CONFIG_DISABLE_REPORT, "DIST_COMM"))
								{

									if(strstr($functionString, "right60,"))
									{///Distributor Commission

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="distributor-comm-summary.php">Distributor Commission Report</A></nobr><BR>

										<?
									}
								}
								?>
								<?
								if(strstr($functionString, "right61,"))
								{///Report Daily Distributor
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-bank-transfer-report.php">Daily Distributor Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right173,"))
								{///Report Distributor Bank Transfer Payments Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-detailed-bank-transaction-worldlink.php">Distributor Bank Transfer Payments</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right62,"))
								{///Report Suspecious Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="bank_Account_List.php">Distributor Statement</A></nobr><BR>
									<?
								}
								?>
								<?
								if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
								{

									if(strstr($functionString, "right80,"))
									{///Daily Cashier Report

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="teller_daily_report.php">Daily Cashier Report</A></nobr><BR>
										<?
									}
								}
								?>
								<?

								if(strstr($functionString, "right73,"))
								{///Payout Center Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payout_center_report.php">Payout Center Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right133,"))
								{///Fax Output Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="fax_output_report.php">Fax Output Report</A></nobr><BR>
									<?
								}
								?>

								<?
								if(!strstr(CONFIG_DISABLE_REPORT, "TELLER_ACCOUNT"))
								{

									if(strstr($functionString, "right81,"))
									{///Teller Account

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="teller_Account.php">Teller Account</A></nobr><BR>
										<?
									}
								}
								?>
								<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="outstanding_payout_center_report.php">Outstanding Payout Center Report</a></nobr><br-->



								<?

								if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_AGENT"))
								{

									if(strstr($functionString, "right63,"))
									{///Daily Agent Report

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-agent-transfer-report.php">Daily <?=__("Agent")?> Report</A></nobr><BR>
										<?
									}
								}



								if(!strstr(CONFIG_DISABLE_REPORT, "OUSTSTANDING_AGENT"))
								{

									if(strstr($functionString, "right64,"))
									{///Out standing Agent Statment

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="outStandingPay.php">Outstanding <?=__("Agent")?> Payments</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right65,"))
								{///Daily Transaction Summary
									?>
									<?php if(SYSTEM =='Premier FX'){?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-trans-summary.php">Daily Transaction Summary</A></nobr><BR>
								<?php } else {?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-trans.php">Daily Transaction Summary</A></nobr><BR>
								<?php } ?>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right302"))
								{///Guzatted Holidays
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="gazetted-holidays.php">Gazetted Holidays</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right66"))
								{///Transaction Transfer Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="transferReport.php">Transaction Transfer Report</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right121"))
								{///Report Transaction Summary
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="repTransSummary.php">Report Transaction Summary</A></nobr><BR>

									<?
								}
								if(strstr($functionString, "right67"))
								{///Sender BeneFiciary Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="beneficiaryReport.php"><?=__("Sender")?>/Beneficiary Report</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right68"))
								{///Report Sender Registration Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="current-regis.php"><?=__("Sender")?> Registration Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right138"))
								{///Beneficiary Registration Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="current-ben-regis.php">Beneficiary Registration Report</A></nobr><BR>
									<?
								}
								?>
								<?
								if(!strstr(CONFIG_DISABLE_REPORT, "RATE_EARNING"))
								{


									if(strstr($functionString, "right69"))
									{///Exchange Rate Earning

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="company_profit.php">Exchange Rate Earning Report</A></nobr><BR>
										<?
									}
								}


								if(strstr($functionString, "right70"))
								{///Agent Account Balance

									if(CONFIG_SALES_AS_BALANCE == '1')
									{

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sales_Report.php"><?=__("Agent")?> Account Balance</A></nobr><BR>
										<?
									}else{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Agent_account_statement.php"><?=__("Agent")?> Account Balance</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right71"))
								{///Agent Country Based Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent_country_base_transactions.php">
											<?
											if(CONFIG_AGENT_COUNTRY_BASED == '1')
											{
												echo(CONFIG_AGENT_COUNTRY_BASED_LABEL);
											}else{
												echo("Agent Country Base Transactions");
											}
											?>
										</A></nobr><BR>
									<?
								}
								?>

								<? if(CONFIG_PAYIN_CUSTOMER_ACCOUNT == '1')
								{


									if(strstr($functionString, "right72"))
									{///Sender Account Balance

										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="customer_account_statement.php"><?=__("Sender")?> Account Balance</A></nobr><BR>
										<?
									}
								}
								if(strstr($functionString, "right74"))
								{///Outstanding Payout Centre Report
									?>
									<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="payout_center_report.php">Payout Center Report</a></nobr><br-->
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="outstanding_payout_center_report.php">Outstanding Payout Center Report</A></nobr><BR>
									<?
								}

								?>
								<? if (CONFIG_AGENT_STATMENT_GLINK == '1')
								{

									if(strstr($functionString, "right132"))
									{///Agent Statment


										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent_Account_List.php"><?=__("Agent")?> Statement</A></nobr><BR>
										<?
									}
								}

								?>

								<?
								if(CONFIG_SALES_REPORT == '1')
								{

									if(strstr($functionString, "right79"))
									{///Sender BeneFiciary Report


										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sales_Report.php">Daily Sales Report</A></nobr><BR>
										<?
									}
								}
								?>

								<?
								if(strstr($functionString, "right75"))
								{///Agent Account Statment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent_Account.php"><?=__("Agent")?> Account Statement</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right199"))
								{///Agent Account Statment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="agent_account_statement_prime_currency.php"><?=__("Agent")?> A/c Balance Statement <strong>New</strong></a></nobr><br>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right113"))
								{///Sub A&D Account Statment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="sub_Account.php">Sub A&D Account Statement</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right76,"))
								{///Bank Account Statment
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="bank_Account.php">Distributor Account Statement</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right124,") && CONFIG_ACCOUNT_SUMMARY == '1'){
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="agent_Account_Summary.php">Account Summary</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right125,") && CONFIG_CONSOLIDATED_SENDER_REGIS_REPORT == '1')
								{//consolidated sender registration report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="consolidated-sender-regis-report.php">Consolidated <?=__("Sender")?> Registration Report</A></nobr><BR>
									<?
								}?>


								<?

								if(strstr($functionString, "right126,") && CONFIG_CUST_AML_REPORT == '1')
								{//consolidated sender registration report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cust-aml-report.php"><?=__("Sender")?> AML Report</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right147,") && CONFIG_CONNECT_PLUS_DIST_COM_REPORT == '1')
								{//Account Statement report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="connectPlusDistCommReport.php">Account Statement Report</A></nobr><BR>
									<?
								}

								if(strstr($functionString, "right134,") && $agentType == 'MLRO'){
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cust-accumulate-aml-report.php"><?=__("Sender")?> Accumulative Amount AML Report</A></nobr><BR>
									<?

								}

								if(strstr($functionString, "right129,") && CONFIG_EXPORT_ALL_TRANS == '1')
								{//Export All transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="extract_data.php">Export All Transactions</A></nobr><BR>
									<?
								}?>

								<? 	if(strstr($functionString, "right77"))
								{///Audit Trial Window
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="user_audit_log_screen.php">Payex Audit Logs</A></nobr><BR>
									<?
								}
								?>
								<? if(strstr($functionString, "right287"))
								{///View Reports Logs
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="reportViewHistory.php">View Reports Logs</A></nobr><BR>
									<?
								}
								?>



								<?
								if(strstr($functionString, "right78"))
								{///Sender BeneFiciary Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Transaction_Audit.php">Transactions Audit Window</A></nobr><BR>
									<?
								}
								?>



								<?
								if(CONFIG_INTERMEDIATE_EXRATE_BREAK_UP == "1"){
									if(strstr($functionString, "right187"))
									{///intermediate exchange rate break up report
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="transactionCurrencyBreakup.php"><?=CONFIG_INTERMEDIATE_EXRATE_BREAK_UP_LABEL ?></A></nobr><BR>
										<?
									}
								}
								?>


								<?
								if(strstr($functionString, "right176"))
								{///Daily Report for Spinzar Added by Niaz Ahmad #2829 at 20-03-2008
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily_report.php">Daily Report</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right196"))
								{///Daily cash book
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cashBook.php">Daily Cash Book</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right231"))
								{///Cash Book System
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="CashBookNew.php">Cash Book System</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right232"))
								{///Company Cash Book
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="CompanyCashBook.php">Company Cash Book</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right233"))
								{///User Denomination Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="UserDenominationReport.php">User Denomination Report</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right204"))
								{///Daily cash book for Global Exchange
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cashBookGlobalExchange.php">Daily Cash Book</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right201"))
								{///Daily cash book history list
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="cashBookSummary.php">Cash Book History List</a></nobr><br>
									<?
								}

								if(strstr($functionString, "right205"))
								{///Daily cash book history for Global Exchange
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cashBookSummaryGlobalExchange.php">Cash Book History</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right195,"))
								{///Daily expenses
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="expenses.php">Daily Expenses</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right304,"))
								{///Daily expenses
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Audit-Reports.php">Audit Logs Reports</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right305,"))
								{///Daily expenses
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="payment_report.php">Payment Report</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right309,"))
								{///Payment Mode Report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="recurring_Cron_Report.php">Recurring Payments Report</A></nobr><BR>

									<?
								}




								if(strstr($functionString, "right192"))
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="outstandingBalanceReport.php"><?=__("Agent")?> Outstanding Balance Report</A></nobr><BR>
									<?
								}


								if(strstr($functionString, "right200,"))
								{
									/*Dual Ledger Report for A&D*/
									?>
									<nobr>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0">
										<A CLASS="tblItem" TARGET="mainFrame" HREF="dualLedgerReport.php">
											Dual Ledger Report for A&D
										</A>
									</nobr>
									<BR>
									<?
								}

								if(strstr($functionString, "right202,"))
								{
									/* GE Distributor Transaction Statement */
									?>
									<nobr>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0">
										<A CLASS="tblItem" TARGET="mainFrame" HREF="distributorTransactionStatement.php">
											GE Distributor Transaction Statement
										</A>
									</nobr>
									<BR>
									<?
								}

								if(strstr($functionString, "right225,"))
								{
									/* Admin Staff Account Statement */
									?>
									<nobr>
										&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0">
										<A CLASS="tblItem" TARGET="mainFrame" HREF="admin-user-account.php">
											Admin Users Account Statement
										</A>
									</nobr>
									<BR>
									<?
								}

								if(strstr($functionString, "right82"))
								{///Report Suspecious Transactions
									?>
									<?
									if(CONFIG_REPORT_SUSPICIOUS == '1')
									{
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="release-trans.php">Report Suspicious Transaction</A></nobr><BR>
										<?
									}
									?>
									<?
								}
								?> <?
								if(strstr($functionString, "right96"))
								{///Export New Transactions
									?>
									<? if (CONFIG_CURR_DENOMINATION == "1") { ?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="DCurrency-List.php"><? echo(CONFIG_CURR_CAPTION); ?></A></nobr><BR>
								<? } ?>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right97") && CONFIG_DIALY_COMMISSION == '1')
								{///Export New Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="daily-commission-summary.php">Daily Commission Report</A></nobr><BR>
									<?
								}
								?>

								<?
								if(strstr($functionString, "right98") &&  CONFIG_PROFIT_EARNING == '1')
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="profit_earning_Bayba.php">Profit Earning Report</A></nobr><BR>

									<?
								}
								?>
								<?
								if(strstr($functionString, "right99") &&  CONFIG_PROFIT_EARNING == '1')
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="profit_earning_list.php">Profit Earning Report List</A></nobr><BR>

									<?
								}
								?>
								<?
								if(strstr($functionString, "right115,") && CONFIG_EXCHNG_MARGIN_SHARE == "1")
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="profit-sharing-report.php">Margin Share Report</A></nobr><nobr></nobr><BR>

									<?
								}
								?>
								<?
								if(strstr($functionString, "right146,") && CONFIG_SHARE_OTHER_NETWORK == '1')
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="APITransactionReportLevel1.php">Shared Transactions Report</A></nobr><nobr></nobr><BR>

									<?
								}
								?>

								<?
								if(strstr($functionString, "right149,") && CONFIG_CUSTOM_BANK_CHARGE == '1')
								{
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="distributor-payement.php">Payment To Distributor</A></nobr><nobr></nobr><BR>

									<?
								}
								?>
								<?
								if(strstr($functionString, "right186,") && CONFIG_DAILY_CASH_REPORT == '1')
								{//daily cash report
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="cashier_distributor_transSum.php"><?=CONFIG_REPORT_CAPTION;?></A></nobr><nobr></nobr><BR>

									<?
								}
								?>
								<?
								if(strstr($functionString, "right222,") && CONFIG_SHOW_BANK_BASED_TRASACTIONS_EXPORT == '1')
								{//Export Bank Based Transactions
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="exportBankTransactionsMT.php">Export Bank Based Transactions</A></nobr><nobr></nobr><BR>

									<?
								}
								?>

							</DIV>
							<?
						}//View Report Condition End
						?>




						<?
						$viewReport= False;
						for($reports = 234; $reports <= 235; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if($viewReport){
							?>

							<DIV ID="ec1Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('ec1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ec1', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('ec1', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="ec1Img"></A> <A HREF="#" onClick="if (capable) {expandBase('ec1', true); return false;}" class='tblItem'>Custom Reports</A> </DIV>

							<DIV ID="ec1Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('ec1', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('ec1', '#D0DCE0')}">

								<?
								if(strstr($functionString, "right234,"))
								{/// Statement of Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="statement-transfers-account.php">Statement of Account</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right235,"))
								{/// Statement of Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trial-balance.php">Trial Balance</A></nobr><BR>
									<?
								}
								?>
							</DIV>
							<?
						}//View Custom Report Condition End
						?>






						<?
						///For Forum
						$viewReport = False;
						for($reports = 140; $reports <= 141; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if($viewReport){
							?>

							<DIV ID="el12Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el12', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el12', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el12', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el12Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el12', true); return false;}" class='tblItem'>Forum</A> </DIV>
							<DIV ID="el12Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el12', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el12', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right140"))
								{///Add FORUM
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add_enquiry.php">Add Forum</A></nobr><BR>
									<?
								}
								?>
								<?
								if(strstr($functionString, "right141"))
								{/// VIEW FORUM
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_enquiry.php">View Forum</A></nobr><BR>
									<?
								}
								?>
							</DIV>
							<?
						}//End of Forum
						?>





						<?
						///For Export
						$viewReport = False;
						for($reports = 183; $reports <= 200; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if(CONFIG_EXPORT_MODULE == "1"){
							if($viewReport){
								?>

								<DIV ID="el13Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el13', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el13', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el13', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el13Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el13', true); return false;}" class='tblItem'>Export Data</A> </DIV>
								<DIV ID="el13Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el13', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el13', '#D0DCE0')}">
									<?

									if(strstr($functionString, "right183"))
									{///export transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Distributor_Output_File_with_count.php?isExported=Y">Export Old Transactions</A></nobr><BR>
										<?
									}
									?>
									<?
									if(strstr($functionString, "right184"))
									{///export transactions
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="Distributor_Output_File_with_count.php?isExported=N">Export New Transactions</A></nobr><BR>
										<?
									}
									?>

								</DIV>
								<?
							}
						}//End of Export
						if(CONFIG_DISTRIBUTION_DEAL_SHEET == '1'){
							?>
							<DIV ID="el14Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el14', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el14', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el14', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el14Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el14', true); return false;}" class='tblItem'>Deal Sheet</A> </DIV>
							<DIV ID="el14Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el14', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el14', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right242"))
								{///Add Deal Sheet
									$deal_user = defined("CONFIG_DEAL_SHEET_USER_TYPE")?CONFIG_DEAL_SHEET_USER_TYPE:"";
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add_deal_sheet.php"><?=$deal_user?> Deal Sheet</A></nobr><BR>
									<?
								}
								?>
							</DIV>
							<?
						}
						if(CONFIG_DOUBLE_ENTRY == '1')
						{
							?>
							<DIV ID="el16Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('el16', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el16', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('el16', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="el16Img"></A> <A HREF="#" onClick="if (capable) {expandBase('el16', true); return false;}" class='tblItem'>Double Entry</A> </DIV>
							<DIV ID="el16Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('el16', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('el16', '#D0DCE0')}">
								<?

								if(strstr($functionString, "right259"))
								{///Add Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-account.php">Add Account</A></nobr><BR>


									<?
								}
								if(strstr($functionString, "right260"))
								{///Update Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="update-account.php">Update Account</A></nobr><BR>

									<?
								}
								if(strstr($functionString, "right261"))
								{///View Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-ledgers.php">View Accounts</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right262"))
								{///Manage Chart of Account
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-chart-of-account.php">Manage Chart of Account</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right263"))
								{///Inter Account Transfer
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="inter-account-transfer.php">Inter Account Transfer</A></nobr><BR>
									<?
								}
								if(strstr($functionString, "right264"))
								{///View Chart of Accounts
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="chart-of-accounts.php">View Chart of Accounts</A></nobr><BR>
								<? } ?>



								<?php if(CONFIG_TRANS_TYPE_TT == '1'){
									if(strstr($functionString, "right281"))
									{///View Chart of Accounts
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="TTtransfer.php"> Create TT Transaction</A></nobr><BR>
									<? }

									if(strstr($functionString, "right282"))
									{///View sender Accounts
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view-sender-accounts.php">View Sender Account</A></nobr><BR>
									<? }
									if(strstr($functionString, "right283"))
									{///View stransactions tt
										?>
										<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_TT_Trans.php">View TT Transactions</A></nobr><BR>
									<? }

								}?>
								<? if(strstr($functionString, "right283"))
								{///View stransactions tt
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="view_inter_fund_transfer_trans.php">View Inter Fund TransferTransactions</A></nobr><BR>
								<? }
								if(strstr($functionString, "right285"))
								{///Trial Balance
									?>
									<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="trial-balance-new.php">Trial Balance</A></nobr><BR>
								<? } ?>

							</DIV>
							<?

						}
						?>
						<? /* ?>
	<!--Added By Kashif-->

				<!--nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="transferBalance.php">Transfer Balance</a></nobr><br-->

				<div id="el8Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el8', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el8Img"></a> <a href="#" onclick="if (capable) {expandBase('el8', true); return false;}" class='tblItem'>Transactions</a> </div>
            <div ID="el8Child" class="child" onmouseover="if (isDOM || isIE4) {hilightBase('el8', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el8', '#D0DCE0')}"> <nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="old-trans-search.php">Old Transaction Search</a></nobr><br>
            	<?
            	 	if(!strstr(CONFIG_DISABLE_REPORT, "DAILY_CASHIER"))
            	 	{
            	 	?>
            		<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="teller_daily_report.php">Daily Cashier Report</a></nobr><br>
            		<?
            		}
            		?>
            </div>
				<!--div id="el3Parent" class="parent" onmouseover="if (isDOM || isIE4) {hilightBase('el3', '#99CCCC')}" onmouseout="if (isDOM || isIE4) {hilightBase('el3', '#D0DCE0')}"> <a class="item" HREF="#" onclick="if (capable) {expandBase('el3', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="el3Img"></a> <a href="#" onclick="if (capable) {expandBase('el3', true); return false;}" class='tblItem'>Transactions</a> </div-->
        		<?
        		*/
						?>


						<?
						if($agentType == "Support" && $company !="MIL Money Exchange Ltd" ){

							?>
							<div id="e21Parent" class="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e21', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e21', '#D0DCE0')}"> <a class="item" HREF="#" onClick="if (capable) {expandBase('e2l', true); return false;}"> <img NAME="imEx" SRC="images/plus.gif" BORDER="0" width="9" height="9" ID="e21Img"></a> <a href="#" onClick="if (capable) {expandBase('e21', true); return false;}" class='tblItem'>Payex Configuration</a> </div>
							<div ID="e21Child" class="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e21', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e21', '#D0DCE0')}">
								<nobr>&nbsp;&nbsp;&nbsp;<img src="images/browse.gif" border="0"> <a class="tblItem" target="mainFrame" HREF="add-country.php">Multicountry Payex</a></nobr><br>
							</DIV>
							<?
						}
						?>
						<!--///////////////////////////////////////////////////////////////////-->
						<!-- Asad -->
						<?
						$viewReport = False;
						for($reports = 311; $reports <= 315; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if ($viewReport) {
						?>
						<DIV ID="e311Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e311', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e311', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e311', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e311Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e311', true); return false;}" class='tblItem'>Card Issuing Club</A> </DIV>
						<DIV ID="e311Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e311', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e311', '#D0DCE0')}">
							<?
							if(strstr($functionString, "right311"))
							{///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-card-issuer.php">Add Card Issuer</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right312"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-card-issuer.php">Manage Card Issuer</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right313"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="card-bassed-customer.php">Card Bassed Customer</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right314"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="top-up-card-for-customer.php">Top Up Card For Customer</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right315"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="balance-inquiry.php">Balance Inquiry</A></nobr><BR>
								<?
							}
							?>
						</DIV>
						<?php } ?>
						<?
							$viewReport = False;
							for($reports = 316; $reports <= 325; $reports++)
							{
								if(strstr($functionString, "right$reports,"))
								{
									$viewReport= True;
								}
							}
							if(strstr($functionString, "right328,"))
							{
								$viewReport= True;
							}
							if ($viewReport) {
						?>
						<DIV ID="e312Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e312', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e312', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e312', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e312Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e312', true); return false;}" class='tblItem'>Global Payments</A> </DIV>
						<DIV ID="e312Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e312', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e312', '#D0DCE0')}">
							<?
							if(strstr($functionString, "right328"))
							{///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-sub-distributor.php">Add Sub Distributor</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right316"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-sub-distributor.php">Manage Sub Distributor</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right317"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="assign-teller.php">Assign Teller</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right318"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-teller.php">Manage Teller</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right319"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-collection-point.php">Add Collection Point</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right320"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-collection-point.php">Manage Collection Point</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right321"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="distributor-output-file.php">Distributor Output File</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right322"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="release-transactions.php">Release Transactions</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right323"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="card-processing.php">Card Processing</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right324"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-card-processing-institute.php">Add Card Processing Institute</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right325"))
							{/// VIEW FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-CPIs.php">Manage CPIs</A></nobr><BR>
								<?
							}

							?>
						</DIV>
						<?php } ?>

						<?
							$viewReport = False;
							for($reports = 326; $reports <= 327; $reports++)
							{
								if(strstr($functionString, "right$reports,"))
								{
									$viewReport= True;
								}

							}
							if ($viewReport) {
						?>
						<DIV ID="e313Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e313', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e313', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e313', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e313Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e313', true); return false;}" class='tblItem'>Banking Partners</A> </DIV>
						<DIV ID="e313Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e313', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e313', '#D0DCE0')}">
							<?
							if(strstr($functionString, "right326"))
							{///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-banking-partner.php">Add Banking Partner</A></nobr><BR>
								<?
							}
							if(strstr($functionString, "right327"))
							{///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="manage-banking-partner.php">Manage Banking Partner</A></nobr><BR>
								<?
							}
							?>
						</DIV>
						<?php } ?>

						<?
						$viewReport = False;
						for($reports = 331; $reports <= 334; $reports++)
						{
							if(strstr($functionString, "right$reports,"))
							{
								$viewReport= True;
							}

						}
						if ($viewReport) {
						?>
						<DIV ID="e328Parent" CLASS="parent"
							 onMouseOver="if (isDOM || isIE4) {hilightBase('e328', '#99CCCC')}"
							 onMouseOut="if (isDOM || isIE4) {hilightBase('e328', '#D0DCE0')}"><A CLASS="item" HREF="#"
																								  onClick="if (capable) {expandBase('e328', true); return false;}">
								<IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e328Img"></A>
							<A HREF="#" onClick="if (capable) {expandBase('e328', true); return false;}"
							   class='tblItem'>Regulatory reports</A></DIV>
						<DIV ID="e328Child" CLASS="child"
							 onMouseOver="if (isDOM || isIE4) {hilightBase('e328', '#99CCCC')}"
							 onMouseOut="if (isDOM || isIE4) {hilightBase('e328', '#D0DCE0')}">
							<?
							if (strstr($functionString, "right331")) {///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem"
																									TARGET="mainFrame"
																									HREF="suspicious-activity-report.php">SAR</A>
								</nobr><BR>
								<?
							}

							if (strstr($functionString, "right332")) {///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem"
																									TARGET="mainFrame"
																									HREF="HMRc.php">HMRc</A>
								</nobr><BR>
								<?
							}

							if (strstr($functionString, "right333")) {///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem"
																									TARGET="mainFrame"
																									HREF="MOJ.php">MOJ</A>
								</nobr><BR>
								<?
							}

							if (strstr($functionString, "right334")) {///Add FORUM
								?>
								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem"
																									TARGET="mainFrame"
																									HREF="saved-SAR-reports.php">Saved
										SAR</A></nobr><BR>
								<?
							}
							?>
						</DIV>
						<?php } ?>

						<?php
							if (false) {
						?>
						<DIV ID="e314Parent" CLASS="parent" onMouseOver="if (isDOM || isIE4) {hilightBase('e314', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e314', '#D0DCE0')}"> <A CLASS="item" HREF="#" onClick="if (capable) {expandBase('e314', true); return false;}"> <IMG NAME="imEx" SRC="images/plus.gif" BORDER="0" WIDTH="9" HEIGHT="9" ID="e314Img"></A> <A HREF="#" onClick="if (capable) {expandBase('e314', true); return false;}" class='tblItem'>Emoney</A> </DIV>
						<DIV ID="e314Child" CLASS="child" onMouseOver="if (isDOM || isIE4) {hilightBase('e314', '#99CCCC')}" onMouseOut="if (isDOM || isIE4) {hilightBase('e314', '#D0DCE0')}">
							<?
							//							if(strstr($functionString, "right328"))
							//							{///Add FORUM
							//								?>
							<!--								<nobr>&nbsp;&nbsp;&nbsp;<IMG SRC="images/browse.gif" BORDER="0"> <A CLASS="tblItem" TARGET="mainFrame" HREF="add-super-agent.php">Add Super Agent</A></nobr><BR>-->
							<!--								--><?//
							//							}

							?>
						</DIV>
							<?php } ?>
						<!--////////////////////////////////////////////////////-->
				</TR>
			</TABLE></TD>
	</TR>
</TABLE>
<SCRIPT TYPE="text/javascript" LANGUAGE="javascript1.2">
	<!--
	if (isNS4) {
		firstEl  = 'el0Parent';
		firstInd = nsGetIndex(firstEl);
		nsShowAll();
		nsArrangeList();
	}
	expandedDb = '';
	//-->
</SCRIPT>
</BODY>
<?	}  ?>
