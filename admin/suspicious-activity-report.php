<?php
session_start();

include ("../include/config.php");
include ("security.php");

$userID = $_SESSION["loggedUserData"]["userID"];
if (isset($_POST["customerID"]) || isset($_GET["customerID"])) {
    if (isset($_POST["customerID"]))
        $customerID = $_POST["customerID"];
    else
        $customerID = $_GET["customerID"];

    $adminRecord = selectFrom("SELECT name , agentAddress , agentCity , agentCountry , postCode , agentAccounNumber  FROM admin WHERE userID = $userID");
    $custRecord = selectFrom("SELECT customerID , lastName , firstName , middleName , Address , City , State , Phone , dob FROM customer WHERE customerID = $customerID");
    $uidtypeRecord = selectFrom("SELECT id_number , issued_by FROM user_id_types WHERE user_id = $customerID");
    $sarRecord = selectMultiRecords("SELECT id, activityName FROM suspiciousActivityList WHERE status='Active'");
    $idTypeTitle=selectMultiRecords("SELECT title FROM id_types,user_id_types WHERE id_types.id=user_id_types.user_id AND user_id_types.user_id=$customerID");
}

//$beneRecord = selectFrom("SELECT lastName , firstName , middleName ,  Address , City , State , Zip , Country , Phone , Mobile , dob FROM beneficiary WHERE benID = 7809");

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Suspicious Activity Report</title>
    <!--    <LINK href="images/interface_admin.css" type=text/css rel=stylesheet>-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?php /*<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> */?>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script language="javascript" src="./javascript/functions.js"></script>
    <!-- Include the above in your HEAD tag -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="content/stylesheets/application.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery.fancybox-1.2.6.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/new-agent.css" />
    <link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
    <style>.form-control{font-size: 13px;}</style>
</head>
<body <? if(CONFIG_SKIP_CONFIRM == '1'&& $buttonValue != ""){ ?> onLoad="print()"<? } ?> style="padding:15px;">
<?php
if ($_GET['msg']=='success')
    {
     ?>
        <div class="alert alert-success">
            your report hs been successfully created.To view visit Click <a href="saved-SAR-reports.php" > View Saved SAR</a>
        </div>
<?php
    }
?>
<div class="alert introducer">Suspicious Activity Report</div>
<br>
<form id="formCustomerDropDown" method="post" action="suspicious-activity-report.php">
    <strong>Select Customer:&nbsp;&nbsp;&nbsp;</strong>
    <select name="customerID" onchange="refreshForm();">
        <option>- Select Customer -</option>
        <?php
            $queryCustomer = selectMultiRecords("select customerID, firstName, lastName from customer where agentID = $userID");
            foreach ($queryCustomer as $row) {
                if ($customerID == $row["customerID"])
                    echo "<option value='".$row["customerID"]."' selected>".$row["firstName"]." ".$row["lastName"]."</option>";
                else
                    echo "<option value='".$row["customerID"]."'>".$row["firstName"]." ".$row["lastName"]."</option>";
            }
        ?>
    </select>
    <script type="text/javascript">
        function refreshForm() {
            document.getElementById("formCustomerDropDown").submit();
        }
    </script>
</form>
<br><br>
<form id="SARReport" action="suspicious-activity-report-conf.php?customerID=<?php echo $customerID;?>" method="post" style="background-color:#d4edda; border-color: #fff">
    <fieldset class="individual group">
        <div>
            <input type="hidden" id="name" name="name">
        </div>
        <div style="background-color: black; width: 50%;">
            <p style="color: white; padding-left: 130px">Always Complete Entire Report</p>
        </div>
        <br>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 1</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Reporting Financial Institution Information</b></div>
        </div>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["name"]?>" placeholder="Name of Financial Institution" readonly>
        </div>
        <br>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentAddress"]?>" placeholder="Address of Financial Institution" readonly>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCity"]?>" placeholder="City" readonly>
            </div>

            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCountry"]?>" placeholder="State" readonly>
            </div>

            <div class="col col-2">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["postCode"]?>" placeholder="Zip Code" readonly>
            </div>
        </div>
        <br>
        <div class="col col-8">
            <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentAddress"]?>" placeholder="Address of Branch Office where Activity Occured" readonly>
        </div>
        <input type="checkbox"   style="margin-left: 15px;margin-right: -70px; width: 20px;">
        <div><p></p>
            <p style="position: relative; top: -27px; margin-left: 35px; margin-bottom: -25px">Multiple Branches Include Information in Narrative</p>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCity"]?>" placeholder="City" readonly>
            </div>

            <div class="col col-3">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["agentCountry"]?>" placeholder="State" readonly>
            </div>

            <div class="col col-2">
                <input class="required form-control" id="source" name="source" type="text" value="<?=$adminRecord["postCode"]?>" placeholder="Zip Code" readonly>
            </div>
        </div>
        <br>
        <div style="margin-left: 15px"><p>If Institution Closed, Date Closed.</p></div>
        <div class="col col-4">
            <input class="required form-control" id="source" name="source" type="date">
        </div>
        <br>
        <div style="margin-left: 15px"><p>Account Number(s), if any</p></div>
        <div class="col col-4">
            <input class="required form-control" id="source" name="arrAccount[]" type="text" value="" placeholder="Account 1">
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="arrAccount[]" type="text" value="" placeholder="Account 2">
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="arrAccount[]" type="text" value="" placeholder="Account 3">
        </div>
        <br>
        <div class="col col-4">
            <input class="required form-control" id="source" name="arrAccount[]" type="text" value="" placeholder="Account 4">
        </div>
        <div style="position: relative; top: -220px; margin-left: 410px;margin-bottom: -170px">
            <p style="margin-left: 40px"> Closed?</p>
            <div>
                <input type="radio" name="Acc1" style="margin-left: 28px;margin-right: 5px; margin-top:10px; width: 20px">Yes
                <input type="radio" name="Acc1" style="margin-left: 7px;margin-right: 5px; width: 20px">No
            </div>
            <div>
                <input type="radio" name="Acc2" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc2" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
            <div>
                <input type="radio" name="Acc3" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc3" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
            <div>
                <input type="radio" name="Acc4" style="margin-left: 28px;margin-right: 5px; margin-top: 35px; width: 20px;">Yes
                <input type="radio" name="Acc4" style="margin-left: 7px;margin-right: 5px; width: 20px;">No
            </div>
        </div>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 2</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Customer/Sender Information:</b></div>
        </div>
        <div style="margin-left: 0px" class="row">
            <div class="col col-5">Last Name or Name of Entry
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["lastName"]?>" placeholder="Last Name or Name of Entry" readonly>
            </div>
            <div class="col col-3">First Name
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["firstName"]?>" placeholder="First Name" readonly>
            </div>
            <div class="col col-3">Middle Name
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["middleName"]?>" placeholder="Middle Name" readonly>
            </div>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-8">Address
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["Address"]?>" placeholder="Address" readonly>
            </div>
            <div class="col col-3">City
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["City"]?>" placeholder="City" readonly>
            </div>
        </div>
        <br>
        <div>
            <p>&nbsp; &nbsp; <b>Govt issued identification:</b></p>
            <div style="margin-left: 0px" class="row">
                <?php
                foreach ($idTypeTitle AS $GovtIdentity){
                    echo'<input type="radio" name="GOVT.1" value="'.$GovtIdentity["title"].'" style="margin-left: 24px;margin-right: 9px; margin-top:3px; width: 20px" disabled >'.$GovtIdentity["title"];
                }
                ?>
<!--                <div class="col col-3" >-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 24px;margin-right: 9px; margin-top:3px; width: 20px">Driver's License/State ID-->
<!--                </div>-->
<!--                <div class="col col-2">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Passport-->
<!--                </div>-->
<!--                <div class="col col-2">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Alien Registration-->
<!--                </div>-->
<!--                <div class="col col-5">-->
<!--                    <input type="radio" name="GOVT.1" style="margin-left: 51px;margin-right: 9px; margin-top:3px; width: 20px">Other-->
<!--                    <div class="col col-6" style="position: relative; top: -25px; margin-bottom: -10px;margin-left: 110px;">-->
<!--                        <input class="required form-control" id="source" name="govtIssueOther" type="text" value="">-->
<!--                    </div>-->
<!--                </div>-->
                <div class="col col-3">Number
                    <input class="required form-control" id="source" name="source" type="text" value="<?=$uidtypeRecord["id_number"]?>" placeholder="Number" readonly>
                </div>
                <div class="col col-3">Issuing State/Country
                    <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["State"]?>" placeholder="Issuing State/Country" readonly>
                </div>
            </div>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div style="margin-top: 17px" class="col col-4">SSN,EIN or TIN
                <input class="required form-control" id="source" name="source" type="text" value="" placeholder="SSN,EIN or TIN" readonly>
            </div>
            <div style="margin-top: 17px" class="col col-3">Date of Birth
                <div>
                    <input class="required form-control" id="source" name="source" value="<?=$custRecord["dob"]?>" type="date" readonly>
                </div>
            </div>
            <div style="margin-top: 17px" class="col col-4">Telephone Number
                <input class="required form-control" id="source" name="source" type="text" value="<?=$custRecord["Phone"]?>" placeholder="Telephone Number" readonly>
            </div>
        </div>
        <br>
        <br>
        <div>
            <div style="background-color: black;width: 10%">
                <p style="color: white; padding-left: 30px">Part 3</p>
            </div>
            <div style="position: relative; top: -20px;margin-left: 140px"> <b>Suspicious activity Information:</b></div>
        </div>
        <div>
            <p> <b>&nbsp;&nbsp; Date or date range of suspicious activity:</b></p>
            <div class="row">
                <div style="margin-left: 30px; margin-top: 10px" class="col col-4">
                    From
                    <div style="position: relative; top: -30px; margin-left: 35px">
                        <input class="required form-control" id="source" name="activityFrom" type="date">
                    </div>
                </div>
                <div style="margin-left: 30px; margin-top: 10px" class="col col-4">
                    To
                    <div style="position: relative; top: -30px; margin-left: 20px">
                        <input class="required form-control" id="source" name="activityTo" type="date">
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col col-4" style="margin-right: -95px">
                    <p><b>&nbsp;&nbsp; Total amount involved in suspicious activity</b></p>
                </div>
                <div class="col col-6">
                    <input type="checkbox" style="margin-right: 7px; margin-left: 100px; width: 20px">Amount Unknown
                </div>
            </div>
            <div style="margin-top: 17px" class="col col-4">
                <input class="required form-control" id="amountInvolved" name="amountInvolved" type="text" placeholder="Enter Amount">
            </div>
        </div>
        <br>
        <div>
            <p><b>&nbsp;&nbsp; Category of suspicious activity (Check all that apply)</b></p>
        </div>
        <div style="margin-left: 0px" class="row">
            <input type="checkbox" name="activityCategory[]" value="Money Laundering" style="margin-left: 20px;margin-right: 5px; margin-top:3px; width: 20px" checked>Money Laundering
            <input type="checkbox" name="activityCategory[]" value="Structuring" style="margin-left: 50px;margin-right: 5px; margin-top:3px; width: 20px">Structuring
            <input type="checkbox" name="activityCategory[]" value="Terrorist Financing" style="margin-left: 50px;margin-right: 5px; margin-top:3px; width: 20px">Terrorist Financing
            <input type="checkbox" id="categoryOther" name="activityCategory[]" value="" style="margin-left: 50px;margin-right: 5px; margin-top:3px; width: 20px">Other(specify)
            <div class="col col-3" style="position: relative; top: -15px; margin-bottom: -10px">
                <input class="required form-control" id="activityCategoryOther" name="activityCategoryOther" type="text">
            </div>
        </div>
        <script>
            function categorySuspicious() {
                if (document.getElementById('categoryOther').checked==true && document.getElementById('activityCategoryOther').value=="")
                    alert("Must specify category of suspicious activity");
                else if (document.getElementById('checkOtherFin').checked==true && document.getElementById('OtherFinServices').value=="")
                    alert("Must specify financial services involved");
                else {
                    document.getElementById('categoryOther').value = document.getElementById('activityCategoryOther').value;
                    document.getElementById('checkOtherFin').value = document.getElementById('OtherFinServices').value;
                    save();
                }
            }
        </script>
        <br>
        <div>
            <p>
                <b>&nbsp;&nbsp; Financial services involved in the suspicious activity and character of the suspicious activity. <br>
                    &nbsp;&nbsp; Including unusual use (Check all that apply)</b>
            </p>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div>
                <input type="checkbox" name="finServices[]" value="Money order" style="margin-left: 20px;margin-right: 5px; margin-top:-4px; width: 20px">Money order
            </div>
            <div>
                <input type="checkbox" name="finServices[]" value="Traveller's check" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Traveller's check
            </div>
            <div>
                <input type="checkbox" checked name="finServices[]" value="Money transfer" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Money transfer
            </div>
            <div>
                <input type="checkbox" id="checkOtherFin" name="finServices[]" value="Other(specify)" style="margin-left: 50px;margin-right: 5px; margin-top:-4px; width: 20px">Other(specify)

                <div class="" style="position: relative; top: -23px; margin-bottom: -10px ; margin-left: 180px">
                    <input class="required form-control" id="OtherFinServices" name="finServicesOther" type="text" >
                </div>
            </div>
            <div>
                <input type="checkbox" name="finServices[]" value="Currency exchange" style="margin-left: 20px;margin-right: 5px; margin-top:-4px; width: 20px">Currency exchange
            </div>
        </div>
        
        <br>
        <div>
            <p>&nbsp; &nbsp; <b>Check all of the following that apply</b></p>
        </div>
        <div class="row">
            <?php
            $flag = true;
            foreach ($sarRecord as $value) {
                ?>
                <?php
                if ($flag) {
                    ?>
                    <div class="col col-6">
                        <input type="checkbox" name="activityType[]" value="<?=$value["id"]?>" style="margin-left: 27px;margin-right: -75px; margin-top:10px; width: 15px"><p style="position: relative; top: -23px; margin-left: 45px ; margin-bottom: -20px"><?=$value["activityName"]?></p>
                    </div>
                    <?php
                    $flag = false;
                } else {
                    ?>
                    <div class="col col-6">
                        <input type="checkbox" name="activityType[]" value="<?=$value["id"]?>" style="margin-left: 0px;margin-right: 0px"><p style="position: relative; top: -23px; margin-left: 18px ; margin-bottom: -20px"><?=$value["activityName"]?></p>
                    </div>
                    <?php
                    $flag = true;
                }
                ?>
                <?php
            }
            ?>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-11"><p><b> REMARKS</b></p>
                <textarea name="remarks" id="remarks" style="width:100%" rows="7"></textarea>
            </div>
        </div>
        <br>
        <div style="margin-left: 0px" class="row">
            <div class="col col-3">
                <input class="btn btn-sm btn-primary btn-block" type="button" value="SAVE" onclick="categorySuspicious();">
            </div>

            <div class="col col-3 noPrint">
                <input class="btn btn-sm btn-primary btn-block btn-right" id="print" name="Submit2" type="button" value="PRINT">
            </div>
        </div>
        <br>
        <script type="text/javascript">
            function save() {
                if (document.getElementById("amountInvolved").value == "" || document.getElementById("amountInvolved").value == null) {
                    alert("Amount can not be empty");
                } else {
                    document.getElementById("name").value = prompt("Enter Name for Report: ");
                    document.getElementById("SARReport").submit();
                }
            }
            $("#print").click(function () {
                $("#print").css("display","none");
                print();
            });
        </script>
    </fieldset>
</form>
</body>
</html>