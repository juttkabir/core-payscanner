<?php
	session_start();
	$rootPath = $_SERVER['DOCUMENT_ROOT']."/admin/lib/";
	include ($_SERVER['DOCUMENT_ROOT']."/include/config.php");
	include ("security.php");
	include_once($rootPath."currency_exchange_functions.php");
	
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	
	// define page navigation parameters
	if ($offset == "")
		$offset = 0;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$limit=50;
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	if($offset>0){
		$limit++;
		if($offset>=$limit)
			$offset++;
	}
		
	$sortBy = $_GET["sortBy"];
	if ($sortBy == "")
		$sortBy = "id";
	
	$colSpans 		= 11;
	$roundAmountTo 	= 2;
	$roundRateTo 	= 4;

	$currStr = "select curr.currencyName,crt.buying_rate from currencies as curr,curr_exchange as crt where curr.cID = crt.buysellCurrency AND curr.cID ='".$_REQUEST["buysellCurrency"]."' ORDER BY crt.id ASC";
	$currSql = selectFrom($currStr);

	
	$currencyData		= array();
	$filters 			= "";
	$Submit				= $_REQUEST["Submit"];
	$strBuySellCurrency	= $_REQUEST["buysellCurrency"];
	$strDisplyFromDate	= $_REQUEST["from_date"];
	$strDisplyToDate	= $_REQUEST["to_date"];
	
	if(empty($Submit)){
		$strDisplyFromDate	= date("d/m/Y",time());
		$strDisplyToDate	= date("d/m/Y",time());
	}
	 
	if($Submit == "Process")
	{
		$filters .= " AND currency = '".$strBuySellCurrency."'";
					
		if($strDisplyFromDate !='' && $strDisplyToDate !='')
		{
			$fromDateArr	= explode("/",$strDisplyFromDate);
			$fromDate 		= $fromDateArr[2] . "-" . $fromDateArr[1] . "-" . $fromDateArr[0]." 00:00:00";
			
			$toDateArr		= explode("/",$strDisplyToDate);
			$toDate 		= $toDateArr[2] . "-" . $toDateArr[1] . "-" . $toDateArr[0]." 23:59:59";
			
			$fromStrDate 	= date("Y-m-d H:i:s",strtotime($fromDate));
			$toStrDate 		= date("Y-m-d H:i:s",strtotime($toDate));
			
			$filters 	   .= " AND (created_date >= '".$fromStrDate."' AND created_date <= '".$toStrDate."')";
		}
		
		$queryCurr = "	SELECT 
							*
			   			FROM 
							currency_stock_history 
						WHERE 1  
					";
										 
		$queryCurr 		.= $filters;	
		$queryCurr 		.= " ORDER BY  id ASC";
		$countRecords 	= SelectMultiRecords($queryCurr);
		$allCount 		= count($countRecords);
		$queryCurrOut	= $queryCurr;
		$queryCurr		.= " LIMIT $offset , $limit";
		
		//debug($queryCurr);
		$currencyData 	= SelectMultiRecords($queryCurr);
			
	}	
	
	$allCountV = $allCount;
	
	// Column names
	$colHeads = array('Batch ID','Value Date','We Buy Amount','Buy Rate','Cost In GBP','Value Sold','Sold Rate','Cost In GBP','GBP Profit/Loss','Margin','On Hand Figure');
	
	/*** Currency Dropdown Menu Query***/
	$sqlCurr = "SELECT 
					cID,
					currencyName
				FROM 
					currencies
				WHERE 
					cID IN
					(
					SELECT 
						buysellCurrency
					FROM 
						curr_exchange_account
					WHERE 1
					)";
												 	
	$sqlCurr .= " GROUP BY currencyName";
	$getBSCurrData1 = SelectMultiRecords($sqlCurr);

?>

<html>
<head>
<title>Daily Deal Sheet Report</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>

<script language="javascript">		

$(document).ready(function(){

	/* vaildations of the fields start */

	/*$("#searchFrom").validate({
		rules: {
			buysellCurrency:"required"
		},
		messages: {
			buysellCurrency: "<br />Please Select Buy/Sell Currency"
		},
		submitHandler: function(form) {
			//Control Form Submition logic
		}
	});*/
	
	/* Validation ends */
	
	$("#exportReport").change(function(){
		if($(this).val() != "")
		{
			$("#exportForm").submit();
		}
	});
	
	$("#exportBtn").click(function(){
		$("#exportReport").show();
	});
	
	$("#printBtn").click(function(){
		$("#filterRow").hide();
		$("#printRow").hide();
		window.print();
	});
	
	$("#from_date").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});
	$("#to_date").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});

});

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
} 
</script>
<link rel="stylesheet" type="text/css" href="images/interface.css" >
<link rel="stylesheet" type="text/css" href="images/interface.css" />
<link rel="stylesheet" type="text/css" href="css/inputScreens.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style type="text/css">
	.dp-choose-date{
		color: black;		
	}
	.error {
		color: red;
		font: 8pt verdana;
		font-weight:bold;
	}
</style>
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
	<tr bgcolor="#DFE6EA">
		<td class="topbar" align="center">Daily Deal Sheet Report</td>
	</tr>
	<tr id="filterRow">
		<td><fieldset>
			<table width="50%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="search" id="searchFrom" method="post" action="">
				<tr>
					<td align="center"><b>SEARCH FILTER</b></td>
				</tr>
				<tr bgcolor="#DFE6EA">
					<td align="center">
						<b>From </b>
						<input type="text" id="from_date" name="from_date" value="<?=$strDisplyFromDate?>" readonly="readonly" />
						&nbsp;&nbsp;
						<b>To </b>
						<input type="text" id="to_date" name="to_date" value="<?=$strDisplyToDate?>" readonly="readonly" />
						<br />
					</td>
				</tr>
				<tr bgcolor="#DFE6EA">
					<td align="center"><b>Buy/Sell Currency <font color="red">*</font> </b>
              			<select id="buysellCurrencyID" name="buysellCurrency" style="font-family: verdana; font-size: 11px;">
                			<option value="">Select Currency</option>
                			<?php
							for ($i=0; $i < count($getBSCurrData1); $i++)
							{
								$getBSCurrName1 = $getBSCurrData1[$i]['currencyName'];		
								$getBSCurrID1   = $getBSCurrData1[$i]['cID'];				
				 			?>
                				<option value="<?=$getBSCurrID1; ?>"><?=$getBSCurrName1; ?></option>
                			<?php
							}
							?>
              			</select>
						<script language="JavaScript">
							SelectOption(document.search.buysellCurrency, "<?=$strBuySellCurrency?>");
						</script>
					</td>
				</tr>
				<tr bgcolor="#DFE6EA">
					<td align="center"><input type="submit" name="Submit" value="Process"></td>
				</tr>
			</form>
			</table>
			</fieldset>
			<br>
		</td>
	</tr>
	<tr>
		<td align="center">
		<table width="90%" border="0" cellspacing="1" cellpadding="1" align="center">
		<?php
		if ($allCount > 0)
		{
			$query_string = "&buysellCurrency=$strBuySellCurrency&Submit=Process&from_date=$strDisplyFromDate&to_date=$strDisplyToDate";
		?>
			 <tr>
                <td colspan="<?=$colSpans?>" bgcolor="#000000">
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
						<tr>
						  <td><?php if (count($currencyData) > 0) {;?>
							Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
							<?=$allCount; ?>
							<?php } ;?>                      </td>
						  <?php if ($prv >= 0) { ?>
						  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
						  <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
						  <?php } ?>
						  <?php 
						  if ( ($nxt > 0) && ($nxt < $allCountV) ) {
							$alloffset = (ceil($allCountV / $limit) - 1) * $limit;
							//debug(ceil($allCountV / $limit));
							//debug('(ceil('.$allCountV .'/'. $limit.') - 1) * '.$limit);
							?>
						 <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
						  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <?php } ?>
						</tr>
					</table>
				</td>
			</tr>
			<!--<tr>
				<td colspan="<?=$colSpans?>" align="center" bgcolor="#CCCCCC">
					<font color="#005b90" size="+1"><strong><?=$currSql["currencyName"]?> Current Stock
						&nbsp;&nbsp;<?=stripslashes(number_format($fltCurrentStock,2,'.',','));?>
					</strong></font>
				</td>
			</tr>-->
			<tr bgcolor="#DFE6EA" style="color:#005B90;">
				<th><?=$colHeads[0]?></th>
				<th><?=$colHeads[1]?></th>
				<th><?=$colHeads[2]." ".$currSql["currencyName"]?></th>
				<th><?=$colHeads[3]?></th>
				<th><?=$colHeads[4]?></th>
				<th><?=$currSql["currencyName"]." ".$colHeads[5]?></th>
				<th><?=$colHeads[6]?></th>
				<th><?=$colHeads[7]?></th>
				<th><?=$colHeads[8]?></th>
				<th><?=$colHeads[9]?></th>
				<th><?=$colHeads[10]?></th>
			</tr>
			<?php
			$grandBuy		= 0;
			$grandBuyGBP	= 0;
			$grandSell		= 0;
			$grandSellGBP	= 0;
			$grandBuyRate	= 0;
			$grandSellRate	= 0;
			$buyRateCounter = 0;
			$sellRateCounter= 0;
			$grandProfitLoss= 0;
			$grandMargin 	= 0;
			$cashInHand 	= 0;
			//debug($currencyData);
			foreach($currencyData as $keyBuy=>$valBuy)
			{
				$fltBuyRate		= $valBuy["buying_rate"];
				$localAmount	= ($valBuy["amount"] / $valBuy["buying_rate"]); // Calculate Local Amount
				$localAmount	= number_format($localAmount,$roundAmountTo,'.',',');
				$arrInput = array(
								"buy_sell"    		=> "B",
								"totalAmount" 		=> $valBuy["amount"],
								"rate"        		=> $fltBuyRate,	
								"lastBuyRate" 		=> $fltBuyRate,
								"localAmount" 		=> $localAmount,
								"roundRateLevel"  	=> $roundRateTo,
								"roundAmountLevel"	=> $roundAmountTo
				);

				$arrCurrencyTotal = currency_totals($arrInput);
				
				// Buy Stock
				$buyAmount 		= $arrCurrencyTotal["buyAmount"];		// foreign currency amount
				$buyRate 		= $arrCurrencyTotal["buyRate"];
				$buyCostInGBP 	= $arrCurrencyTotal["buyCostInGBP"];	// GBP amount
				
				// Total Buy
				$grandBuy 			+= $buyAmount;
				//$averageBuyRate 	= $currInitialBuyingRate;
				$grandBuyGBP 		+= $buyCostInGBP;
				?>
				<tr valign="top" bgcolor="#DDDDDD" title="Rate for : <? echo $getBSCurrName;?>">
					<td align="center"><?=$valBuy["batch_id"]?></td>
					<td align="left"><?=date("d/m/Y",strtotime($valBuy["created_date"]));?></td>
					<td align="right"><?=number_format($buyAmount,$roundAmountTo,'.',',')?>			</td>
					<td align="right"><?=number_format($buyRate,$roundRateTo,'.',','); ?>			</td>
					<td align="right"><?=number_format($buyCostInGBP,$roundAmountTo,'.',','); ?>	</td>
					
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>				
					<td>&nbsp;</td>
				</tr>
					<?php 
					$sellCurrancyData = SelectMultiRecords("SELECT * FROM currency_stock_history_used WHERE from_batch_id='".$valBuy['batch_id']."'");
					//debug($sellCurrancyData);
					foreach($sellCurrancyData as $keySell=>$valSell)
					{
						$localAmountSell 	= ($valSell["amount"] / $valSell["sell_rate"]);
						$localAmountSell	= number_format($localAmountSell,$roundAmountTo,'.',',');
						$arrInputSell	= 	array(
												"buy_sell"			=> "S",
												"totalAmount"		=> $valSell["amount"], 
												"rate"				=> $valSell["sell_rate"],
												"lastBuyRate" 		=> $fltBuyRate,
												"localAmount" 		=> $localAmountSell,
												"roundRateLevel"  	=> $roundRateTo,
												"roundAmountLevel"	=> $roundAmountTo
											);
											
						$arrCurrencyTotalSell = currency_totals($arrInputSell);
						// Sell Stock
						$sellAmount 	= $arrCurrencyTotalSell["sellAmount"];		// foreign currency amount
						$sellRate 		= $arrCurrencyTotalSell["sellRate"];
						$sellCostInGBP	= $arrCurrencyTotalSell["sellCostInGBP"];	// GBP amount
						
						// Profit/Loss & Margin 
						$pl		= $arrCurrencyTotalSell["profit_loss"];
						$margin = $arrCurrencyTotalSell["margin"];
						
						// Total Sell
						$grandSell 			+= $sellAmount;
						$grandSellGBP		+= $sellCostInGBP;
						$grandProfitLoss	+= $pl;
						?>
						<tr valign="top" bgcolor="#EEEEEE">
							<td>&nbsp;</td>
							<td align="left"><?=date("d/m/Y",strtotime($valSell["date_out"]));?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
							<td align="right"><?=number_format($sellAmount,$roundAmountTo,'.',','); ?>		</td>
							<td align="right"><?=number_format($sellRate,$roundRateTo,'.',','); ?>			</td>
							<td align="right"><?=number_format($sellCostInGBP,$roundAmountTo,'.',','); ?>	</td>
							<td align="right"><?=number_format($pl,$roundAmountTo,'.',',');?>				</td>
							<td align="right"><?=number_format($margin,$roundAmountTo,'.',',');?>			</td>				
							<td>&nbsp;</td>
						</tr>
					<?php		
					} //End Iner foreach loop  (End Sell Stock foreach)
			} //End Outer foreach loop	(End Buy Stock foreach)
							
			/* get grand totals*/
			//$getGrandTotals = currency_grand_totals($currencyData);
			
			//$grandBuy = $getGrandTotals["grandBuy"];
			//$averageBuyRate = $getGrandTotals["averageBuyRate"];
			//$grandBuyGBP = $getGrandTotals["grandBuyGBP"];
			//$grandSell = $getGrandTotals["grandSell"];
			//$averageSellRate = $getGrandTotals["averageSellRate"];
			//$grandSellGBP = $getGrandTotals["grandSellGBP"];
			//$grandProfitLoss = $getGrandTotals["grandProfitLoss"];
			//$grandMargin = $getGrandTotals["grandMargin"];
			//$cashInHand = $getGrandTotals["cashInHand"];
			$cashInHand	= $grandBuy - $grandSell;
			$cashInHand	= number_format($cashInHand,$roundAmountTo,'.',','); 
				
			?>
			<tr bgcolor="#DFE6EA">
				<td>&nbsp;</td>
				<td align="left"><font color="#005b90"><strong>Grand Total</strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=number_format($grandBuy,$roundAmountTo,'.',','); ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><? /*echo number_format($averageBuyRate,$roundRateTo,'.',',');*/ ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=number_format($grandBuyGBP,$roundAmountTo,'.',','); ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=number_format($grandSell,$roundAmountTo,'.',','); ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><? /*echo $averageSellRate;*/ ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=number_format($grandSellGBP,$roundAmountTo,'.',','); ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=number_format($grandProfitLoss,$roundAmountTo,'.',','); ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><? /* echo number_format($grandMargin,$roundAmountTo,'.',',');*/ ?></strong></font></td>
				<td align="right"><font color="#005b90"><strong><?=$cashInHand; ?></strong></font></td>
			</tr>
			<tr>
				<td colspan="<?=$colSpans?>" bgcolor="#000000">
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
						<tr>
							  <td><?php if (count($currencyData) > 0) {;?>
								Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
								<?=$allCount; ?>
								<?php } ;?>                      </td>
							  <?php if ($prv >= 0) { ?>
							  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
							  <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
							  <?php } ?>
							  <?php 
							  if ( ($nxt > 0) && ($nxt < $allCountV) ) {
								$alloffset = (ceil($allCountV / $limit) - 1) * $limit;
								?>
							 <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
							  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
							  <td>&nbsp;</td>
							  <td>&nbsp;</td>
							  <?php } ?>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="<?=$colSpans?>" align="center">
				</td>
			</tr>
			<tr id="printRow">
			<form action="export_daily_deal_sheet_report_AMB.php" method="post" id="exportForm" name="exportForm" target="_blank">
				<td align="center" colspan="10">
				<input type="button" name="Submit2" value="Print This Report" id="printBtn" >
				<input type="button" id="exportBtn" value="Export All" />
				<input type="hidden" name="colHeads" id="colHeads" value="<?=base64_encode(serialize($colHeads))?>" />
				<input type="hidden" name ="filters" id="filters" value="<?=$filters?>" />
				<input type="hidden" name="current_stock" id="current_stock" value="<?=$current_stock?>" />
				<input type="hidden" name ="round_amount_to" id="round_amount_to" value="<?=$roundAmountTo?>" />
				<input type="hidden" name="round_rate_to" id="round_rate_to" value="<?=$roundRateTo?>" />
				&nbsp;&nbsp;
				<select name="exportReport" id="exportReport" style="display:none">
				<option value="">Select Format</option>
				<option value="XLS">Excel</option>
				<option value="CSV">CSV</option>
				<option value="HTML">HTML</option>
			   </select>
				</td>
			  </form>
			</tr>
			<?php
			} else {
			?>
					<tr>
						<td colspan="5" align="center"> No Record found in the database. </td>
					</tr>
			<?php
			}
			?>
		</table>
		</td>
	</tr>
</table>
</body>
</html>
