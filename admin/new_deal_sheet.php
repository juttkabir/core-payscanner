<?
	session_start();
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	$dealFor = strtoupper(CONFIG_DEAL_SHEET_USER_TYPE);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");



	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$loggedUserID  = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
		$offset = 0;
	
			
	if($limit == 0)
		$limit=50;
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";

if($dealFor == 'DISTRIBUTOR'){
	if($agentType == 'admin'){	
		$query = "select * from deal_sheet where (status != 'IA' AND status != 'DL')";
	}else{
		$query = "select * from deal_sheet where (status != 'IA' AND status != 'DL') AND userId ='".$loggedUserID."' ";
	}
}elseif($dealFor == 'CUSTOMER'){
	$query = "select * from deal_sheet where (status != 'IA' AND status != 'DL')";
}


if($_REQUEST["getGrid"] == "payList"){
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	//debug($_REQUEST);
	//debug($query);
	if($count > 0)
		$total_pages = ceil($count / $limit);
	else
		$total_pages = 0;
	
	if($page > $total_pages)
		$page = $total_pages;
	
	$start = $limit * $page - $limit;
	
	if($start < 0)
		$start = 0;
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($result))
	{
		$strFormatStart = "";
		$strFormatEnd = "";
		$customer_id = "";
		$transTotal = "&nbsp;";
		$transSum = "&nbsp;";
		$response->rows[$i]['id'] = $row["id"];
		$userType = $_REQUEST["userType"];
		$name = "";
		//debug($row["userId"]."-->".$row["userId"]."-->".$userType);
	if($dealFor == 'DISTRIBUTOR'){	
		if(!empty($row["userId"]) && $row["userId"] !='generic' && $userType =='DISTRIBUTOR'){
		$strDist ="SELECT username from admin where userID='".$row["userId"]."'";
		//debug($strDist);
		$distRS = mysql_query($strDist) or die(__LINE__.": ".mysql_error());
		$distRow= mysql_fetch_row($distRS);
		$name = $distRow[0];
		}else{
		$name = $row["userId"];
		}
	}elseif($dealFor == 'CUSTOMER'){
		if(!empty($row["userId"]) && $row["userId"] !='generic' && $userType =='CUSTOMER'){
		$query ="SELECT accountName from customer where customerID='".$row["userId"]."'";
		//debug($strDist);
		$custRS = mysql_query($query) or die(__LINE__.": ".mysql_error());
		$custRow= mysql_fetch_row($custRS);
		$name = $custRow[0];
		}
	}	
		$queryTrans = "select count(dtd_id) as totalTrans,transID,sum(transAmount) as transAmount from deal_trans_details where id='".$row["id"]."' GROUP BY id";
		//debug($queryTrans);
		$transResult = mysql_query($queryTrans) or die(__LINE__.": ".mysql_error());
		$transData= mysql_fetch_array($transResult);
		$transCnt = $transData["totalTrans"]; 
		$transSum = number_format($transData["transAmount"],2,'.',',');
		$id = $row["id"];
		//debug($transTotal."-->".$transSum);
		if(!empty($transCnt))
			$transTotal = "<a href='deal_sheet_trans_details.php?id=$id' target='_blank'>$transCnt</a>";
			
		//$transTotal = "<a class='rmtSt' href='deal_sheet_trans_details.php?id=$id'>$transCnt<&frasl;a>";	
		$response->rows[$i]['cell'] = array(
									dateFormat($row["deal_date"], "2"),
									$row["purchase_from"],
									$row["currency_origin"],
									$row["purchase_amount"],
									$row["currency_destination"],
									$row["local_amount"],
									$row["balance"],
									$row["rate"],
									$row["deal_ref"],
									$name,
									$transTotal,
									$transSum,
									$row["profit_loss"]
								);
		$i++;
	}
	
	echo $response->encode($response); 

}	
?>