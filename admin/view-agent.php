<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
dbConnect();
$username=loggedUser();
$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
//debug($agentDetails);
if ($_GET["caller"] != ""){
	$supperAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$agentDetails["parentID"]."'");
}

/*	#4966 
	Company Type from agents(A&D) form and Trading field also .
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
?>
<html>
<head>
	<title>View <?=__("Agent")?>/Distributor Details</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">
		<table width="100%"><tr><td><strong><font color="#000000" size="2">View <? if ($_GET["caller"] == "") echo "Super"; else echo "Sub";?> <?=__("Agent")?>/Correspondent Details</font></strong></td><td><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td></tr></table></td>
  </tr>
  <tr>
    <td align="center">
		<table width="426" border="0" cellspacing="1" cellpadding="2" align="center">
		<?
			if($_GET["caller"] != ""){
		?>
		<tr>
		<td colspan="2" bgcolor="#006699" height="20"><font color="#FFFFFF"><b>Super <?=__("Agent")?></b></font></td>
		</tr>
       <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Branch Manager</strong></font></td>
            <?
            $branchMngr = selectFrom("select username from ".TBL_ADMIN_USERS." where userID='".$supperAgentDetails["parentID"]."'");
            ?>
            <td width="256"><?=stripslashes($branchMngr["username"]); ?></td>
        </tr>
	  <? 
	  $companyTypeV = "MSB";
	  if($companyTypeFlag){
	  $companyTypeV = $supperAgentDetails["companyType"];
	  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Type*</strong></font></td>
            <td width="256"><?=$companyTypeV; ?></td>
        </tr>
		<? }?> 
		<?
	  if($tradingNameFlag){
	  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Trading Name</strong></font></td>
            <td width="256"><?=$supperAgentDetails["tradingName"]; ?></td>
        </tr>
		<? }?>  
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><?=__("Agent");?> Company Name*</strong></font></td>
            <td width="256"><?=stripslashes($supperAgentDetails["agentCompany"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><?=__("Agent");?> Contact Person*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentContactPerson"]); ?></td>
        </tr>
		<tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Source</strong></font></td>
            <td><?=stripslashes($agentDetails["source"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>User Created Date</strong></font></td>
            <td><?=stripslashes($agentDetails["created"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Director*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentCompDirector"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Currency*</strong></font></td>
            <td><?=$agentDetails["supperAgentCurrency"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Limit*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentAccountLimit"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            
          <td width="159"><font color="#005b90"><strong>Commission Percentage/Fee*</strong></font></td>
            <td><?=stripslashes($supperAgentDetails["agentCommission"]); ?></td>
        </tr>
		<tr>
		<td colspan="2" bgcolor="#006699" height="20"><font color="#FFFFFF"><b>Sub <?=__("Agent")?></b></font></td>
		</tr>
		<?
			} 
		if ($_GET["caller"] == ""){
		?>
	  <? 
	  $companyTypeV = "MSB";
	  if($companyTypeFlag){
	  $companyTypeV = $agentDetails["companyType"];
	  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Type*</strong></font></td>
            <td width="256"><?=$companyTypeV; ?></td>
        </tr>
		<? }?> 
		<?
	  if($tradingNameFlag){
	  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Trading Name</strong></font></td>
            <td width="256"><?=$agentDetails["tradingName"]; ?></td>
        </tr>
		<? }?> 
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><?=__("Agent")?> Company Name*</strong></font></td>
            <td width="256"><?=stripslashes($agentDetails["agentCompany"]); ?></td>
        </tr><? } ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><? if ($_GET["caller"] == "") echo __("Agent")." Contact Person"; else echo "Sub". __("Agent") ." Name"; ?></strong></font></td>
            <td><?=stripslashes($agentDetails["agentContactPerson"]); ?></td>
        </tr>
		<?		if(defined("CONFIG_SHOW_INTRODUCER_FIELD") && (strstr(CONFIG_SHOW_INTRODUCER_FIELD,$agentType.",") || CONFIG_SHOW_INTRODUCER_FIELD=="1")){ ?>
		<tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Source</strong></font></td>
            <td><?=stripslashes($agentDetails["source"]); ?></td>
        </tr>
		<? } ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 1*</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress"]); ?></td>
        </tr><? if (trim($agentDetails["agentAddress2"]) != ""){ ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><?=stripslashes($agentDetails["agentAddress2"]); ?></td>
        </tr><? } ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Country*</b></font></td>
            <td><?=$agentDetails["agentCountry"]; ?></td>
        </tr>
		<? if ($agentDetails["Country"] != "United States" && $agentDetails["Country"] != "Canada"){ ?>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>City*</b></font></td>
            <td><?=$agentDetails["agentCity"];?></td>
        </tr><? } else {
		?><tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>
              <? if ($agentDetails["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code*</b></font></td>
            <td><?=stripslashes($agentDetails["agentZip"]); ?></td>
        </tr><?
		} ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentPhone"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><?=stripslashes($agentDetails["agentFax"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Email*</strong></font></td>
            <td><?=stripslashes($agentDetails["email"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>User Created Date</strong></font></td>
            <td><?=stripslashes($agentDetails["created"]); ?></td>
        </tr>
        <? if ($_GET["caller"] == ""){ ?>
	<?php if(CONFIG_HIDE_MSB_AGENT!="1"){?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><?=$companyTypeV?> Number</strong></font></td>
            <td><?=stripslashes($agentDetails["agentMSBNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><?=$companyTypeV?> Expiry</strong></font></td>
            <td><? echo $agentDetails["agentMCBExpiry"]; ?></td>
        </tr>
	<?php }?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Registration Number</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCompRegNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Company Director*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCompDirector"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td><?=stripslashes($agentDetails["agentDirectorAdd"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><strong>Director Proof ID</strong></font></td>
            <td valign="top"><?=$agentDetails["agentProofID"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td><?=$agentDetails["agentIDExpiry"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong> Document Provided</strong></font></td>
            <td><?=$agentDetails["agentDocumentProvided"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Bank*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentBank"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Name*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccountName"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Number*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccounNumber"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Branch Code</strong></font></td>
            <td><?=stripslashes($agentDetails["agentBranchCode"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Type</strong></font></td>
            <td><?=$agentDetails["agentAccountType"];?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Currency*</strong></font></td>
            <td><?=$agentDetails["agentCurrency"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Account Limit*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentAccountLimit"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            
          <td width="159"><font color="#005b90"><strong>Commission Percentage/Fee*</strong></font></td>
            <td><?=stripslashes($agentDetails["agentCommission"]); ?></td>
        </tr>

       
		<? } ?>
		<? if(CONFIG_AGENT_PAYMENT_MODE == "1"){ ?>
		 <tr bgcolor="#ededed">
            
          <td width="159"><font color="#005b90"><strong>Payment Mode</strong></font></td>
            <td><? echo ($agentDetails["paymentMode"] == "include" ? "At End Of Month"  : "At Source")?></td>
        </tr>
		
		<? } ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Status</strong></font></td>
            <td><?=$agentDetails["agentStatus"];?></td>
        </tr>
	<?	if ($agentDetails["agentStatus"] == 'Disabled') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Disable Reason</strong></font></td>
            <td><?=$agentDetails["disableReason"];?></td>
        </tr>
	<?	} else if ($agentDetails["agentStatus"] == 'Suspended') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Suspension Reason</strong></font></td>
            <td><?=$agentDetails["suspensionReason"];?></td>
        </tr>
	<?	}  ?>
	<? if(CONFIG_NOTES_AGENT_DIST == "1"){ ?>
	 <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Notes</strong></font></td>
            <td><?=$agentDetails["notes"];?></td>
        </tr>
        <? } ?>
      </table>
	</td>
  </tr>
</table>
</body>
</html>
