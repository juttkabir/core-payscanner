<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



if($_GET["Submit"]!="Search")
{
	session_register("fMonth");
	session_register("fDay");
	session_register("fYear");
	session_register("tMonth");
	session_register("tDay");
	session_register("tYear");
	
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
}

$query = "select *  from ". TBL_TRANSACTIONS . " as t where (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery')";
$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t where (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery')";

if($_POST["Submit"] == "Search" || $_GET["Submit"]== "Search")
{
		$Submit = "Search";
	
		if($_GET["Submit"]!="Search")
		{	
			if($_POST["fMonth"]!="")
				$_SESSION["fMonth"]=$_POST["fMonth"];
			if($_POST["fDay"]!="")
				$_SESSION["fDay"]=$_POST["fDay"];
			if($_POST["fYear"]!="")
				$_SESSION["fYear"]=$_POST["fYear"];
			if($_POST["tMonth"]!="")
				$_SESSION["tMonth"]=$_POST["tMonth"];
			if($_POST["tDay"]!="")
				$_SESSION["tDay"]=$_POST["tDay"];
			if($_POST["tYear"]!="")
				$_SESSION["tYear"]=$_POST["tYear"];
		}

	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	if($_GET["fromDate"]!="")
	{
		$fromDate = $_GET["fromDate"];
	}
	if($_GET["toDate"]!="")
	{
		$toDate = $_GET["toDate"];
	}

	if($fromDate <= $toDate)
	{
		$dated = " and (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
		$msgDate = "";
	}
	else
	{
		$dated = " and (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
		$msgDate = "Please select the correct date filter.";	
	}
	
	if($_POST["transType"] != "")
	{
		$type = $_POST["transType"];
		$query .= " and t.transType='$type'";
		$queryCnt .= " and t.transType='$type'";
	}
	elseif($_GET["transType"] != "")
	{	
		$type = $_GET["transType"];
		$query .= " and t.transType='$type'";
		$queryCnt .= " and t.transType='$type'";
	}
	
	if($_POST["transID"] != "" || $_GET["transID"] != "")
	{
		if($_POST["transID"] != "")
		{
			$id = $_POST["transID"];
		}
		else
		{
			$id = $_GET["transID"];
		}
		if($_POST["searchBy"] != "")
		{
			$by = $_POST["searchBy"];
		}
		else
		{
			$by = $_GET["searchBy"];
		}
	}
	
	if($id != "")
	{
	
		switch($by)
		{
			case 0:
			{
				$query .= " and (t.transID='$id' || t.refNumber = '$id' || t.refNumberIM= '$id')";
				$queryCnt .= " and (t.transID='$id' || t.refNumber = '$id' || t.refNumberIM= '$id')";
				
				break;
			}
			case 1:
			{	
				$query = " select *  from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where 1 and t.customerID =  c.customerID  ";
				$query .= " and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
			
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_CUSTOMER." as cm where 1 and t.customerID =  cm.c_id  and t.createdBy = 'CUSTOMER'  ";
				$queryonline .= " and (cm.FirstName like '$id%' OR cm.LastName like '$id%')";

				$queryCnt = " select count(*)  from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where 1 and t.customerID =  c.customerID  ";
				$queryCnt .= " and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
			
				$queryonlineCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_CUSTOMER." as cm where 1 and t.customerID =  cm.c_id  and t.createdBy = 'CUSTOMER'  ";
				$queryonlineCnt .= " and (cm.FirstName like '$id%' OR cm.LastName like '$id%')";
			
				$query .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
				$queryonline .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
						
				$queryCnt .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
				$queryonlineCnt .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
			
				$queryonline .= $dated;
				if ($type !="")
				{
					$queryonline .= " and t.transType='".$type."'";
					$queryonlineCnt .= " and t.transType='".$type."'";
				}
				$queryonline .= "  order by t.transDate DESC";
			
				$countOnlineRec = countRecords($queryonlineCnt);
				if($offset < $countOnlineRec)
				{
					$queryonline .= " LIMIT $offset , $limit";
				
				 	$onlinecustomer = selectMultiRecords($queryonline);
				}
			  
				break;
			}
			case 2:
			{
				$query = "select *  from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where 1 and t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER'";
				$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where 1 and t.benID =  b.benID and (b.firstName like '$id%' OR b.lastName like '$id%') and t.createdBy != 'CUSTOMER'";				
				
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where 1 and t.benID =  cb.benID and (cb.firstName like '$id%' OR cb.lastName like '$id%') and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where 1 and t.benID =  cb.benID and (cb.firstName like '$id%' OR cb.lastName like '$id%') and t.createdBy = 'CUSTOMER' ";
				
				$query .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
				$queryonline .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
						
				$queryCnt .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
				$queryonlineCnt .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
					
				$queryonline .= $dated;
				if ($type !="")
				{
					$queryonline .= " and t.transType='".$type."'";
				}
				$queryonline .= "  order by t.transDate DESC";
				
				$countOnlineRec = countRecords($queryonlineCnt);
			  if($offset < $countOnlineRec)
				{
					$queryonline .= " LIMIT $offset , $limit";
				
				 	$onlinecustomer = selectMultiRecords($queryonline);
				}
				
				break;
			}
			
			case 3:
			{
				$agent = "select userID  from ". TBL_ADMIN_USERS . " where username like '$id' ";
				$agentname = selectFrom($agent);
				$agentid = $agentname["userID"];
				
				$query = "select *  from ". TBL_TRANSACTIONS . " as t where 1 and t.custAgentID  =  '$agentid' ";
				$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t where 1 and t.custAgentID  =  '$agentid' ";				 
							
				$query .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";
				$queryCnt .= " and (t.transStatus = 'Authorize' OR t.transStatus = 'Amended' OR t.transStatus = 'Pending' OR t.transStatus = 'AwaitingCancellation' OR t.transStatus = 'Out for Delivery') ";	
				
				break;
			}
					
		}
		
	}
	
}	

$query .= $dated;
$queryCnt .= $dated;
if ($type !="")
{
	$query .= " and t.transType='".$type."'";
	$queryCnt .= " and t.transType='".$type."'";
}
$query .= "  order by t.transDate DESC ";
$totalContent = countRecords($queryCnt);
$allCount = $totalContent + $countOnlineRec;
$other = $offset + $limit;
if($other > $countOnlineRec)
{
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
	$query .= " LIMIT $offset2 , $limit2";
	$contentsTrans = selectMultiRecords($query);
}
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Following transactions 
      are available to change their status</font></strong></td>
  </tr>
  <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
  <tr>
    <td align="center"><br>
      <br>
      <table width="671" border="1" cellpadding="5" bordercolor="#666666">
        <form action="suspend-transaction.php" method="post" name="Search">
          <tr>
            <td width="387" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
			
          </tr>
          <tr>
            <td nowrap> Search Transaction
                <input name="transID" type="text" id="transID" value="<?=$id?>">
                <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
                  <option value=""> - Type - </option>
                  <option value="Pick up">Pick up</option>
                  <option value="Bank Transfer">Bank Transfer</option>
                  <option value="Home Delivery">Home Delivery</option>
                </select>
                <script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$type?>");</script>
			             
				<select name="searchBy" >
				<option value=""> - Search By - </option>
                <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No</option>
                <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
                <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
                <option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Code</option>
				  </select>
                <script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
				</td>
			</tr>	
			
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" name="Submit" value="Search">
                <br>       
        

        </td>
      </tr>
        </form>
      </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="suspend-transaction.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
        	<?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



        	
          <?  if($_GET["message"]!=""){?>
          <tr> 
            <td height="25" nowrap  bgcolor="#FFFFCC" align="center"><font color="#FF0000"><strong>&nbsp;<? echo $_GET["message"];?></strong></font></td>
          </tr>
          <? }?>
          <tr> 
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer));?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</strong></span></td>
          </tr>
          <?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td><span class="style1">Date</span></td>
                  <td><span class="style1"><? echo $systemCode; ?> </span></td>
                  <td><span class="style1"><? echo $manualCode; ?></span></td>
                  <td><span class="style1">Status</span></td>
                  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount</span></td>
                  <td width="100"><span class="style1">Sender Name</span></td>
                  <td width="100"><span class="style1">Beneficiary Name</span></td>
                  <td align="center">&nbsp; </td>
                </tr>
                
                
                
                  <? for($i=0;$i<count($onlinecustomer);$i++)
			{
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumber"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transAmount"]?></td>
                  
                  
                  	<? 
                    $transBene = selectFrom("select * from cm_beneficiary  where benID='".$onlinecustomer[$i]["benID"]."'");
				 						$benefName = $transBene["firstName"];
				 					  $benefName .= " ";
									  $benefName .= $transBene["lastName"];
									  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
                   $customerContent = selectFrom("select firstName, lastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="ch-transStatus.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&id=<?=$id;?>&type=<?=$type;?>&by=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>"><strong><font color="#006699">Change Status</font></strong></a> </td>
                </tr>
                <?
			}
      for($i=0;$i<count($contentsTrans);$i++)
			{
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transAmount"]?></td>
                  
                  
                  	<? if($contentsTrans[$i]["createdBy"] == "CUSTOMER") {
                    $transBene = selectFrom("select * from cm_beneficiary  where benID='".$contentsTrans[$i]["benID"]."'");
				 						$benefName = $transBene["firstName"];
				 					  $benefName .= " ";
									  $benefName .= $transBene["lastName"];
									  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
                   $customerContent = selectFrom("select firstName, lastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="ch-transStatus.php?transID=<? echo $contentsTrans[$i]["transID"]?>&id=<?=$id;?>&type=<?=$type;?>&by=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>"><strong><font color="#006699">Change Status</font></strong></a> </td>
                <? }else {               	
				  $transBene = selectFrom("select * from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  $benefName = $transBene["firstName"];
				  $benefName .= " ";
				  $benefName .= $transBene["lastName"];
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
                  <? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="ch-transStatus.php?transID=<? echo $contentsTrans[$i]["transID"]?>&id=<?=$id;?>&type=<?=$type;?>&by=<?=$by;?>&fromDate=<?=$fromDate;?>&toDate=<?=$toDate;?>&Submit=<?=$Submit;?>"><strong><font color="#006699">Change Status</font></strong></a> </td> 
                  	<? } ?>
                </tr>
                <?
							}
						?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="100" align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                </tr>
                <?
			} // greater than zero
			?>
              
          <!--tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr-->

          <?
			} else {
		?>
          <tr>
            <td  align="center"> 
            	<? if($msgDate!="")
            	{
            		echo "<font color=#FF0000>" . $msgDate . "</font>";
            	}
            	else
            	{
            		?>
            	No Transaction found in the database.
            <? } ?>
            </td>
          </tr>
          <?
			}
		?>
		</table></td>
          </tr>
        </form>
      </table></td>
	</tr>
</table>
</body>
</html>