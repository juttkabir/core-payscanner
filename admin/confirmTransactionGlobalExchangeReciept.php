<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('Y-m-d');//date("d-m-Y");
//$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentType=getAgentType();
$loggedUName = $_SESSION["loggedUserData"]["username"];
$returnPage = 'add-transaction.php';
//debug($_POST);
//$_SESSION);
unset($_SESSION["transPostData"]);
$_SESSION["transPostData"]=$_POST;

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";

$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);


/***************************************************
*    Code from express-confirm-transaction.php     *
***************************************************/

$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["other_pur"] 			= $_POST["other_pur"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["agentID"];
$_SESSION["discount"]       = $_POST["discount"];
$_SESSION["chDiscount"]     = $_POST["chDiscount"];
$_SESSION["exchangeRate"]     = $_POST["exchangeRate"];
$_SESSION["IMFee"]     = $_POST["IMFee"];

$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];
$_SESSION["ibanRemarks"] 			= $_POST["ibanRemarks"];
$_SESSION["currencyTo"]     = $_POST['currencyTo'];

if($_POST["dDate"] != "")
	$_SESSION["transDate"] = $_POST["dDate"];

$_SESSION["question"] 			= $_POST["question"];
$_SESSION["answer"]     = $_POST["answer"];	
$_SESSION["tip"]     = $_POST["tip"];

$_SESSION["discount_request"] = $_POST["discount_request"];	
$_SESSION["bankingType"] = $_POST["bankingType"];
if($_SESSION["transType"]=="Bank Transfer")
	$_SESSION["bankCharges"] 		= $_POST["bankCharges"];
else
	$_SESSION["bankCharges"]="";
	

if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS =="1")
{
	if(!empty($_REQUEST["docCategory"]))
	{
		$arrCategory = $_REQUEST["docCategory"];
		$catName = "";
		$catID = "";
		foreach($arrCategory as $key => $val){
			$categoryQuery = selectFrom("select id,description from ".TBL_CATEGORY." where id = '".$key."'"); 
			$catName .= $categoryQuery["description"].",";
			//$catID .= $categoryQuery["id"].",";
		}
		
	}
	  
}	

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=".$_POST["transID"]."&transType=".$_REQUEST["transType"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y&transType=".$_REQUEST["transType"];
}
/*
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}
*/
if(trim($_POST["transType"]) == "Pick up")
{
	if(trim($_POST["collectionPointID"]) == "")
	{
		insertError("Please Select Collection Point");	
		redirect($backUrl);
	}
}


if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}
/*if(trim($_POST["agentID"]) == "" && trim($_POST["transType"]) == "Pick up")
{
	insertError(TE5);	
	redirect($backUrl);
}*/

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}
if(isset($_POST["benIdTypeFlag"]) && trim($_POST["benIdTypeFlag"]) == "" && strstr(CONFIG_BEN_IDTYPE_COMP_TRANSACTION_TYPES,trim($_POST["transType"])))
{
	insertError("Selected Beneficiary should have ID details for Transactin Type ".trim($_POST["transType"]).". Please Edit the Beneficiary.");	
	redirect($backUrl);
}

// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if (CONFIG_EURO_TRANS_IBAN == "1" && $_POST["benCountryRegion"] == "European") {
		
		if (trim($_POST["IBAN"]) == "") {
			insertError(TE21);
			redirect($backUrl);	
		}
		
	}	else {
		
		if(trim($_POST["bankName"]) == "") {
			insertError(TE14);	
			redirect($backUrl);
		}
		if(trim($_POST["accNo"]) == "") {
			insertError(TE18);	
			redirect($backUrl);
		}
		if(trim($_POST["branchAddress"]) == "") {
			insertError(TE16);	
			redirect($backUrl);
		}
		if(CONFIG_CPF_ENABLED == '1') {
			if(trim($_POST["ABACPF"]) == "" && strstr(CONFIG_CPF_COUNTRY , '$_POST["benCountry"]')) {
				insertError(TE20);	
				redirect($backUrl);
			}
		}
		
	}
	
	if(trim($_POST["bankingType"]) == "" && CONFIG_24HR_BANKING == "1") {
		insertError("Please select Banking Type.");	
		redirect($backUrl);
	}
	
	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS != "1")
	{
		if(trim($_POST["distribut"]) == "") {
			insertError("Please select a Distributor from the list");	
			redirect($backUrl);
		}
	}
	
}
/*if(trim($_POST["refNumber"]) != "")
{
	
	if (isExist("select refNumber from ".TBL_TRANSACTIONS." where refNumber = '".$_POST["refNumber"]."'")){
		insertError(TE7);		
		redirect($backUrl);
	
}	
}*/

if($_POST["notService"] == "Home Delivery")
{
	insertError(TE22);	
	redirect($backUrl);
}

if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}	
	
}else{
	if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
	{
		insertError(TE2);	
		redirect($backUrl);
	}
}
if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}
/**
 * Commented through ticket #4044 
if($_POST["benCountry"] == 'Pakistan' || $_POST["benCountry"] == 'India' || $_POST["benCountry"] == 'Poland' || $_POST["benCountry"] == 'Brazil' || $_POST["benCountry"] == 'Philippines')
{

}
else
{
	if($_SESSION["chDiscount"]=="" && (trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0))
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
*/

/** Added code to replace above defined logic **/
if(CONFIG_ZERO_FEE == '1')
{
	if( $_POST["IMFee"] < 0)
	{
		insertError(TE9);	
		redirect($backUrl);
	}

}
else
{
	if($_SESSION["chDiscount"]=="" && (trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0))
	{
		insertError(TE9);	
		redirect($backUrl);
	}
}
/* Replaced code ended */

if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["fundSources"]) == "" && CONFIG_NONCOMPUL_FUNDSOURCES != '1')
{
	insertError(TE11);	
	redirect($backUrl);
}

if(trim($_POST["moneyPaid"]) == "")
{
	insertError(TE12);	
	redirect($backUrl);
}
/* This check added by Niaz Ahmad @4630 at 07-03-2009*/
if($_POST["transType"] != "Bank Transfer" && CONFIG_TIP_ENABLED  == "1"){ 
   if(trim($_POST["tip"]) == "")
  {
	insertError(TE31);	
	redirect($backUrl);
   }
}

$_SESSION['RequestforBD'] = "";
$currDay = date("d");
$backDays = $_SESSION["loggedUserData"]["backDays"];
if ($agentType != 'admin' && $backDays != '0') {
	if ($_POST['dDate'] != "") {
		$dDateArr = explode("-", $_POST['dDate']);
		$dDay = $dDateArr[2];
		$diff = $currDay - $dDay;
		if ($diff > $backDays) {
			$_SESSION['RequestforBD'] = "Y";
		}
	}
}

$refNumber = str_replace(" ","",$_POST["refNumber"]);
$_SESSION["refNumber"] = $refNumber;
$_POST["refNumber"] = $refNumber;

if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") {
	if ($_POST["refNumber"] == "") {
		insertError("Please provide " . $manualCode);
		redirect($backUrl);
	}
	$qDate = "SELECT MIN(created) AS created FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$_POST["custAgentID"]."' AND used != 'Used'";
	$qDRes = mysql_query($qDate) or die("Query Error: " . $qDate . "<br>" . mysql_error());
	$qDRow = mysql_fetch_array($qDRes);
	if ($qDRow["created"] == "") {
		insertError("Please first add agent's receipt book range");
		redirect($backUrl);
	} else {
		$qRanVal = "SELECT * FROM ".TBL_RECEIPT_RANGE." WHERE `agentID` = '".$_POST["custAgentID"]."' AND used != 'Used' AND `created` = '".$qDRow["created"]."'";
		$rValRes = mysql_query($qRanVal) or die("Query Error: " . $qRanVal . "<br>" . mysql_error());
		$rValRow = mysql_fetch_array($rValRes);
		$usedVal = ($rValRow["used"] == "0" ? $rValRow["rangeFrom"] : $rValRow["used"]);
		
		if ($rValRow["used"] != "0") {
			$usedVal++;
		}	
		$nextRangeVal = $usedVal;
		$_SESSION["refNumber"] = $_SESSION["refNumber"] . "-" . $nextRangeVal;
	}
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
/****    CODE FROM express-confirm-transaction.php    ********/






if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

if($_GET["transID"] != ""){
		
		$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$_GET["transID"]."'");
		$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$_GET["transID"]."'");
	}
	
if($_POST["customerID"]!= ""){
	$customerID = $_POST["customerID"];
	}else{
	$customerID = $transactionQuery["customerID"];
}

if($_POST["benID"]!= ""){
	$beneficiaryID = $_POST["benID"];
	}else{
	$beneficiaryID = $transactionQuery["benID"];
}


if($_POST["customerAgent"]!= ""){
	$custAgentID = $_POST["customerAgent"];
	}else{
	$custAgentID = $transactionQuery["custAgentID"];
}

		$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$custAgentID."'");

if($_POST["totalAmount"]!= ""){
$totalAmount= $_POST["totalAmount"];
}else{
$totalAmount=$transactionQuery["totalAmount"];	
	}
	
	
if($_POST["transAmount"]!= ""){
$transAmount= $_POST["transAmount"];
}else{
$transAmount=$transactionQuery["transAmount"];	
	}	
	
	if($_POST["localAmount"]!= ""){
$localAmount= $_POST["localAmount"];
}else{
$localAmount=$transactionQuery["localAmount"];	
	}
	



if($_POST["moneyPaid"]!= ""){
$moneyPaid= $_POST["moneyPaid"];
}else{
$moneyPaid=$transactionQuery["moneyPaid"];	
	}
	
	
if($_POST["transactionPurpose"]!= ""){
$transactionPurpose= $_POST["transactionPurpose"];
}else{
$transactionPurpose=$transactionQuery["transactionPurpose"];	
	}



$discount=$transactionQuery["discounted_amount"];	



if($_POST["exchangeRate"]!= ""){
$exchangeRate= $_POST["exchangeRate"];
}else{
$exchangeRate=$transactionQuery["exchangeRate"];	
	}


if($_POST["IMFee"]!= ""){
$IMFee= $_POST["IMFee"];
}else{
$IMFee=$transactionQuery["IMFee"];	
	}



if($_POST["bankName"]!= ""){
$bankName= $_POST["bankName"];
}else{
$bankName=$bankQuery["bankName"];	
	}

if($_POST["branchCode"]!= ""){
$branchCode= $_POST["branchCode"];
}else{
$branchCode=$bankQuery["branchCode"];	
	}

if($_POST["branchAddress"]!= ""){
$branchAddress= $_POST["branchAddress"];
}else{
$branchAddress=$bankQuery["branchAddress"];	
	}


if($_POST["swiftCode"]!= ""){
$swiftCode= $_POST["swiftCode"];
}else{
$swiftCode=$bankQuery["swiftCode"];	
	}



if($_POST["accNo"]!= ""){
$accNo= $_POST["accNo"];
}else{
$accNo=$bankQuery["accNo"];	
	}




if($_POST["currencyTo"]!= ""){
$currencyTo= $_POST["currencyTo"];
}else{
$currencyTo=$transactionQuery["currencyTo"];	
	}
	
	
	
if($_POST["currencyFrom"]!= ""){
$currencyFrom= $_POST["currencyFrom"];
}else{
$currencyFrom=$transactionQuery["currencyFrom"];	
	}
	
	
	if($_POST["benCountry"]!= ""){
$ToCountry= $_POST["benCountry"];
}else{
$ToCountry=$transactionQuery["toCountry"];	
	}
	
	
	if($_POST["transType"] != ""){
		
		$transactionType = $_POST["transType"];
		} else{
			$transactionType = $transactionQuery["transType"];	
			}
			

if($_POST["bankCharges"] != ""){
		
		$bankCharges = $_POST["bankCharges"];
		} else{
			$bankCharges = $transactionQuery["bankCharges"];	
		}
		
		if($_POST["outCurrCharges"] != ""){
		
		$outCurrCharges = $_POST["outCurrCharges"];
		} else{
			$outCurrCharges = $transactionQuery["outCurrCharges"];	
		}
		
		if($_POST["adminCharges"] != ""){
		
		$adminCharges = $_POST["adminCharges"];
		} else{
			$adminCharges = $transactionQuery["adminCharges"];	
		}
		
		
		if($_POST["cashCharges"] != ""){
				
		$cashCharges = $_POST["cashCharges"];
		} else{
			$cashCharges = $transactionQuery["cashCharges"];	
		}
		
$serviceCharges = $bankCharges + 	$outCurrCharges + $adminCharges;
			
if($_POST["refNumber"] != ""){			
	
			$refNumber=$_POST["refNumber"];
	
}else{
			$refNumber=$transactionQuery["refNumber"];
		}
	
	$discount=$transactionQuery["discounted_amount"];	
	
$refNumberIM=$transactionQuery["refNumberIM"];

/**
 * data retrieved to display on top of 
 * reciept from agent of Admin Staff
 * who has made a transaction.
 * @Ticket #4453
 */

$printCompany = $strAddressQuery["agentCompany"];
$printAddress = $strAddressQuery["agentAddress"];
$printAddress2 = $strAddressQuery["agentAddress2"];
$printCountry = $strAddressQuery["agentCountry"];
$printCity = $strAddressQuery["agentCity"];
$printFax = $strAddressQuery["agentFax"];
$printPhone = $strAddressQuery["agentPhone"];

$printSenderNo = $CustomerData["accountName"];
$agentUserName = $strAddressQuery["username"];
$agentName = $strAddressQuery["name"];
$custAgentParentID = $strAddressQuery["parentID"];

$distributorId = $transactionQuery["benAgentID"];

$distributorNameQuery = selectFrom("select name from ". TBL_ADMIN_USERS." where userID ='".$distributorId."'");
$distributorName = $distributorNameQuery["name"];

$currencyNameQuery = selectFrom("select description from ". TBL_CURRENCY." where currencyName ='".$currencyTo."'");
$currencyName = $currencyNameQuery["description"];

//debug($currencyName);

$fontColor = "#800000";
			$queryCust = "select *  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
			$customerContent = selectFrom($queryCust);
			//debug($customerContent);
			
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
			$benificiaryContent = selectFrom($queryBen);
//			echo "---------------------<br/>";
//			debug($benificiaryContent);


$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='" . $_SESSION["collectionPointID"] . "'and a.agentStatus = 'Active'";
$senderAgentContent = selectFrom($queryCust);
//debug($senderAgentContent);


// This code is added by Niaz Ahmad against #1727 at 08-01-2008
if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");


           
 $to = getCountryTime(CONFIG_COUNTRY_CODE);
	
	$month = substr($to,5,2);
	$year = substr($to,0,4);
	$day = substr($to,8,2);
	$noOfDays = CONFIG_NO_OF_DAYS;
	
	$fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));

	
	$from = $fromDate." 00:00:00"; 
	$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								// echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}


if(!empty($_POST["transID"]))	
{
	$amountRecievedRs = selectFrom("select recievedAmount from ".TBL_TRANSACTIONS." where transID=".$_POST["transID"]);
	$_POST["amountRecieved"] = 	$amountRecievedRs["recievedAmount"];
}
//debug($agentType,true);
if($agentType == "SUPA" || $agentType == "SUBA")
	{
		if($agentType == "SUBA")
		{
			$arrAgentDataSql = selectFrom("select * from admin where userID = '".$_SESSION["loggedUserData"]["parentID"]."' ");	
			$agentCompany = $arrAgentDataSql["agentCompany"];
			$agentAddress = $arrAgentDataSql["agentAddress"]; 
			$agentAddress2 = $arrAgentDataSql["agentAddress2"];

			$agentCity = $arrAgentDataSql["agentCity"];
			$agentCountry = $arrAgentDataSql["agentCountry"];
			
			$agentPhone = $arrAgentDataSql["agentPhone"];
			$agentFax = $arrAgentDataSql["agentFax"];	
		}
		else
		{
			$agentCompany = $_SESSION["loggedUserData"]["agentCompany"];
			$agentAddress = $_SESSION["loggedUserData"]["agentAddress"]; 
			$agentAddress2 = $_SESSION["loggedUserData"]["agentAddress2"];
			$agentCity = $_SESSION["loggedUserData"]["agentCity"];
			$agentCountry = $_SESSION["loggedUserData"]["agentCountry"];
			$agentPhone = $_SESSION["loggedUserData"]["agentPhone"];
			$agentFax = $_SESSION["loggedUserData"]["agentFax"];	
		}
		
	}
/**
 * On reciept of transaction. Address of
 * Agent is displayed now for Admin Staff.
 * @Ticket #4453
 */
	elseif($agentType == "Admin" && $strAddressQuery!=""){

$staffAgentDataSqlInner  = "(select linked_Agent from admin where userID='".$_SESSION["loggedUserData"]["userID"]."' )";
//debug($staffAgentDataSqlInner,true);
$staffAgentDataSqlInnerData  =selectFrom($staffAgentDataSqlInner);
$staffAgentName          = split(",",$staffAgentDataSqlInnerData['linked_Agent']);
$staffAgentDataSql = selectFrom("select * from admin where username = '".$staffAgentName[0]."'");

		$agentCompany = $printCompany;
		$agentAddress = $printAddress;
		$agentAddress2 = $printAddress2;

		$agentCity = $printCity;
		$agentCountry = $printCountry;
		
		$agentPhone = $printPhone;
		$agentFax = $printFax;
	}
	else
	{
		$agentCompany = "GLOBAL EXCHANGE LTD";
		$agentAddress = "54 Ealing Road"; 
		$agentCity = "Wembley HA0 4TQ";
		$agentPhone = "0208 902 3366";
		$agentFax = "0208 902 9922";
	}
/**
 * On the confirm and the print receipt client want to show 
 * the Passport ID number of the beneficiary
 * @Ticket #4425
 */
if(defined("CONFIG_USE_ID_TYPE_OF_BENEFICIARY"))
{
	
	$strPassportIdSql = "select id_number from user_id_types where user_id = '".$_REQUEST["benID"]."' and user_type='B' and id_type_id=(select id from id_types where title='".CONFIG_USE_ID_TYPE_OF_BENEFICIARY."')";
	$arrBeneficiaryId = selectFrom($strPassportIdSql);
	$strBeneficiaryId = $arrBeneficiaryId["id_number"];
}


if(!empty($_REQUEST["distribut"]))
{
	$arrDistributorData = selectFrom("select name from admin where userid='".$_REQUEST["distribut"]."'");
}

?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<link href="styles/displaying.css" rel="stylesheet" type="text/css" media="print">

 <script language="javascript">
<!--
function checkForm(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;

		var condition;
  
 
		var conditionCumulative;
		var conditionCurrent;
  	var ruleConfirmFlag=true;
  	var ruleFlag=false;
  	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	
	var accumulativeAmount = 0;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
	var currentRuletransAmount = <?=$_POST["transAmount"]?>;	
	var currentRuleAmountToCompare = 0;
	var amountToCompareTrans; 
	
// cummulative Amount Rule /////////
<?	
if($cumulativeRule["ruleID"]!='') {
    
     if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		  $fromAmount=$betweenAmount[0];   
		  $toAmount=$betweenAmount[1]; 
?>
		conditionCumulative = <?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>;
		
		<?
    }else{
?>
    amountToCompare = <?=$cumulativeRule["amount"]?>;
    conditionCumulative =	accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare;
 <?	}?>			
		
	
		 if(conditionCumulative)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    ruleConfirmFlag=true;
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
                
      }else{ 
    
    			/*	document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();*/
       
        ruleConfirmFlag=false;
       // ruleFlag=true;
      }

    }else{ 

    	   ruleConfirmFlag=true;
    } 
 
	
<? } ?>

////////////current transaction amount///////


<? if($currentRule["ruleID"]!=''){
	   
		 
	   if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		?>
		conditionCurrent = <?=$fromAmountCurrent?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmountCurrent?>;
		<?
 }else{
 	?>      
 	      
 		    currentRuleAmountToCompare = <?=$currentRule["amount"]?>;
 		    conditionCurrent =	currentRuletransAmount  <?=$currentRule["matchCriteria"]?> currentRuleAmountToCompare;
 <?	}?>
	
 
 if(conditionCurrent)
	   {

				if(confirm("<?=$currentRule["message"]?>"))
				{
					
    	     ruleConfirmFlag=true;
    			
    			/*document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();*/
    		
              
     }else{
    
    				/*document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				   	addTrans.localAmount.focus();*/
         ruleConfirmFlag=false;
         //ruleFlag=true;
      }

    }else{ 

    	     ruleConfirmFlag=true; 
    } 
  
<? } ?>  

	if(document.getElementById("amountRecieved").value=="")
	{
		alert("Please enter Amount Recieved");
		document.getElementById("amountRecieved").focus();
		return false;
	}
	else
	{
		javascript:document.getElementById('confTrans').disabled=true; 
		if(ruleConfirmFlag)
	   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
 	
		if(ruleConfirmFlag == false)
		{
			  document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
		 			addTrans.localAmount.focus();
		}		
	}
}	

function calculateOutStanding()
{
	var total = <?=$_POST["totalAmount"]?>;
	var recieved = document.getElementById("amountRecieved").value;
	document.getElementById("outstanding").value = total - recieved;
}
-->	
</script>    

<style type="text/css">
<!--
.boldFont {
	font-weight: bold;
}

.headingFont {
	font-weight: bold;
	font-size: 16px;
}
td{
	font-size:12px;
}
input{
	font-size:12px;
}
--> 
</style>
</head>

<body align="center" onLoad="<? if(!empty($_POST["transID"])) echo "calculateOutStanding();"; ?>">
<div align="center">
<form name="addTrans" method="post" action="">
  <table width="70%" border="1" cellspacing="0" cellpadding="1">
    <tr bgcolor="#502727"> 
         <td  colspan="4" height="30"> 
        <div align="right">
         <span class="headingFont">
         	<font color="#FFFFFF"><?=$agentCompany?></font>
						</span>.
					</div>
				</td>
			</tr>
			<tr> 
				<td colspan="4" align="right">
					<?php 
						echo $agentAddress.(!empty($agentAddress2)?", ".$agentAddress2:"");
						echo $agentCity.(!empty($agentCountry)?", ".$agentCountry:"");
						echo "&nbsp;&nbsp;Phone no: ".$agentPhone." Fax: ".$agentFax;
					?>
				</td>
			</tr>
    <tr> 
	<tr>
      <td>Global Exchange Fund Code </td>
      <td><input type="text" name="textRefNumber" value="<?=$_POST["refNumber"]?>" readonly></td>
      <td>&nbsp;</td>
      <td class="boldFont">Beneficiary Detail</td>
    </tr>
    <tr> 
      <td>Tran no</td>
      <td>&nbsp;</td>
      <td>Full Name</td>
      <td> 
          <b><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["lastName"]; ?></b>
      </td>
    </tr>
    <tr> 
      <td>Date</td>
      <td><input type="text" name="textfield2" value=<? echo  $today ;?> readonly></td>
       <td valign="top">Address</td>
      <td rowspan="2" valign="top">
      		<? echo $benificiaryContent["Address"].", ".$benificiaryContent["Address1"];?>
      		<br>
      		<? echo $benificiaryContent["City"].", ".$benificiaryContent["State"].", <br />".$benificiaryContent["Zip"].", ".$benificiaryContent["Country"];?>	
      </td>
    </tr>
    <tr> 
      <td>Time</td>
      <td><input type="text" name="textfield3" value="<?=date("H:i:s")?>" readonly></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="25" class="boldFont">Customer no</td>
      <td><input type="text" name="textfield4" value="<? echo $customerContent["accountName"];?>" readonly></td>
      <td>Tel No</td>
      <td><input type="text" name="textfield622" value="<? echo $benificiaryContent["Phone"]?>" readonly></td>
    </tr>
    <tr> 
      <td height="24">First names</td>
      <td><input type="text" name="textfield5" value="<? echo $customerContent["firstName"] ." ".$customerContent["lastName"];?>" readonly></td>
      <td valign="top"><?=CONFIG_USE_ID_TYPE_OF_BENEFICIARY?> ID No</td>
      <td valign="top"><input type="text" name="textfield622" value="<?=$strBeneficiaryId?>" readonly></td>
    </tr>
    
    <tr> 
      <td valign="top">Phone</td>
      <td><input type="text" name="textfield6" value="<? echo $customerContent["Phone"]?>" readonly></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td valign="top" rowspan="2">Address</td>
      <td rowspan="2"><textarea name="textarea" readonly><? echo $customerContent["Address"].", ".$customerContent["Address1"];?></textarea></td>
	  <? 
	if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS =="1"){?>
      <td valign="top">Document Provided</td>
      <td valign="top"><?=$catName?></td>
    </tr>
	<? } ?>
    <tr> 
      <td class="boldFont" colspan="4">&nbsp;</td>
    </tr>
    <tr> 
      <td class="boldFont" valign="top">Remittance Detail</td>
      <td>&nbsp;</td>
      <td class="boldFont" valign="top">Collection point</td>
      <td>
      	<?
      		if($_POST["transType"] == "Bank Transfer")
      			echo $_POST["bankName"];
      		else
      	 		echo $senderAgentContent["cp_corresspondent_name"];
      	?>
      </td>
    </tr>
    <tr> 
      <td>FC Amount</td>
      <td><input type="text" name="textfield62" value="<? echo $localAmount." ".$currencyTo; ?>" readonly></td>
      <td>&nbsp;</td>
      <td align="left" valign="top" rowspan="2">
      		<?php 
      		if($_POST["transType"] == "Bank Transfer")
      		{
      				echo $_POST["branchCode"].", <br />";
      				echo $_POST["branchAddress"].".<br />";
      				echo "SWIFT CODE: ".$_POST["swiftCode"]." Account No.: ".$_POST["accNo"];
      		}
      		else
      		{
	      		echo $senderAgentContent["cp_branch_address"];
	      		echo ",<br />".$senderAgentContent["cp_city"].", <br />".$senderAgentContent["cp_state"];
	      		echo ", ".$senderAgentContent["cp_country"].".";
      		}
      	?>	
      </td>
    </tr>
    <tr> 
      <td height="25">Exchange Rate</td>
      <td><input type="text" name="textfield63" value="<? echo $exchangeRate ?>" readonly></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>Amount in Pounds</td>
      <td><input type="text" name="aip" value="<?=$transAmount?>" readonly></td>
      <td>Phone:</td>
      <td><b><?=$senderAgentContent["cp_phone"]?></b></td>
    </tr>
    <tr> 
      <td>Service Charges</td>
      <td><input type="text" name="textfield65" value="<? echo $_POST["IMFee"]; ?>" readonly ></td>
       <td>Code</td>
      <td><input type="text" name="authCode"></td>
    </tr>
    <tr> 
      <td>Total amount</td>
      <td><input type="text" name="textfield66" value="<?=$_POST["totalAmount"]?>" readonly></td>
      <td>Distributor Name:</td>
      <td><?=$arrDistributorData["name"]?>&nbsp;</td>
    </tr>
	
	<?
		if(empty($customerContent["payinBook"]))
		{
			$_POST["amountRecieved"] = $_POST["totalAmount"];
			$txtAmountRec = "readonly";
			$hideTxtOutstanding = true;
		} else {
			$hideTxtOutstanding = false;
		}
	?>
	
    <tr> 
      <td>Recieved <font color="red">*</font></td>
      <td>
      		<input type="text" id="amountRecieved" name="amountRecieved" value="<?=$_POST["amountRecieved"]?>" onBlur="calculateOutStanding();" size="8" <?=$txtAmountRec?>>
      		&nbsp;in&nbsp;
      		<input type="text" name="recievedType" value="<?=$_SESSION["moneyPaid"]?>" size="15" readonly >
			</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<?
		if($hideTxtOutstanding == false)
		{
	?>
			<tr> 
			  <td>Outstanding</td>
			  <td><input type="text" name="outstanding" id="outstanding" readonly></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
	<?
		}
	?>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Authorized signature</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      
			<input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
			<input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
			<input type="hidden" name="imReferenceNumber" value="<?=$imReferenceNumber;?>">
			<input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
			<input type="hidden" name="mydDate" value="<?=$nowTime;?>">
			<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">				  				  
			<input name="senderAccumulativeAmount" type="hidden" value="<?=$senderAccumulativeAmount;?>">	
			<input type="hidden" name="modifyReason" value="<?=$_REQUEST["modifyReason"]?>" />  
	  <? 
			
			foreach ($_POST as $k=>$v) 
			{
				if($k != "amountRecieved")
					$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				//$str .= "$k\t\t:$v<br>";
			}
			
			echo $hiddenFields;
			
		?> 
      
      
    </tr>
  </table>
</form>
<table align="center">
	<tr>
		<td align="center">
			<input type="button" value="EDIT TRANSACTION" onClick="document.location='add-transaction.php?transID=<? echo $_POST["transID"]?>'" />
			&nbsp;&nbsp;&nbsp;
			<input type="button" id="confTrans" value="CONFIRM TRANSACTION" onClick="checkForm(addTrans,'yes')" />
		</td>
	</tr>
</table>
</div>
</body>
</html>
