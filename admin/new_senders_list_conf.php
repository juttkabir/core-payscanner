<?php 
	/**
	 * @Package Transaction
	 * @subpackage New Registered Senders List Processing
	 * This page Process the Filter from the New Senders List and deliver data for Display
	 */
	
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$user=$agentType;

	
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response	= new Services_JSON();
	$page		= $_REQUEST['page']; // get the requested page 
	$limit		= $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx		= $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord		= $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}				
	if($limit == 0)
	{
		$limit=50;
	}
	
	$_SESSION['status'] = $_REQUEST['status'];
	$_SESSION['customerNumber'] = $_REQUEST['custNum'];
	$_SESSION['fdate'] = $_REQUEST['fdate'];
	$_SESSION['tdate'] = $_REQUEST['tdate'];
	$condition = " ";
	$action = $_REQUEST['getGrid'];
	$bolExportFlag = false;
	if($_REQUEST['Submit'] == 'Search'){
		/**
		 * This Piece of Code is for Exporting and Printing Records
		 */
		if($action == 'export' || $action == 'print'){
			$bolExportFlag = true;
			if($action == 'export'){
				header("Content-type: application/msexcel");
				header("Content-Disposition: attachment; fileaname= New_Senders_List.php");
				header("Content-Description: PHP/MYSQL Generated Data");
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			}
			$strMainStart = '<table border="1" align="center" cellspacing="0" width="900">';
			$strMainEnd = '</table>';
			$strRowStart = '<tr>';
			$strRowEnd = '</tr>';
			$strColStart = '<td>&nbsp;';
			$strColStartSr = '<td align="left">';
			$strColEndSr = '</td>';
			$strColEnd = '</td>';
			
			$strHead = "<th colspan='6' style='font-size:20pt'>GB Compliance List</th>";
			$strHeadCols = "<th>Serial No.</th>";
			$strHeadCols .= "<th>Customer Name</th>";
			$strHeadCols .= "<th>Reference Number</th>";
			$strHeadCols .= "<th>Creation Date</th>";
			$strHeadCols .= "<th>Country</th>";
			$strHeadCols .= "<th>Status</th>";
		}
		if(!empty($_SESSION['status']) && $_SESSION['status'] != 'All')
			$condition = " AND customerStatus = '{$_SESSION['status']}'";
		if(!empty($_SESSION['customerNumber']))
			$condition .= " AND accountName = '{$_SESSION['customerNumber']}'";
		if(empty($_SESSION['tdate']))
			$_SESSION['tdate'] = date("Y-m-d");
			
		$condition .= " AND (created >= '{$_SESSION['fdate']} 00:00:00' AND created <= '{$_SESSION['tdate']} 23:59:59') ";
		
		$query = "SELECT date_format(created, '%Y-%m-%d') as creationDate, customerID, customerName, customerStatus, Country, accountName,customerType FROM ".TBL_CUSTOMER." WHERE 1";
		
		if(!empty($query) && !empty($condition))
			$query .= $condition;
			if(!empty($query)){
				$allCustomersData = mysql_query($query) or die(__LINE__ .":". mysql_error());
				$allCustomersCount = mysql_num_rows($allCustomersData);
			if($allCustomersCount > 0){
				$totalPages = ceil($allCustomersCount / $limit);
			}else{
				$totalPages = 0;
			}
			if($page > $totalPages)
				$page = $totalPages;
			$start = $limit * $page - $limit;
			
			if($start < 0)
				$start = 0;
			if($bolExportFlag)
				$query .= " ORDER BY customerName";
			else
				$query .= " ORDER BY $sidx $sord LIMIT $start, $limit";
			$customersData = mysql_query($query) or die(__LINE__.":".mysql_error());
			if($action == 'export' || $action == 'print'){
				$strExportData = $strMainStart.$strRowStart.$strHead.$strRowEnd;
				$strExportData .= $strRowStart.$strHeadCols;
			}else{
				$response->page = $page;
				$response->total = $totalPages;
				$response->records = $allCustomersCount;
			}
			$i = 0;
			while($customer = mysql_fetch_array($customersData)){
				$serialNo = $i +1;
				
				if($action == 'export' || $action == 'print'){
					$strExportData .= $strRowEnd.$strRowStart.$strColStartSr.$serialNo.$strColEndSr;
					$strExportData .= $strColStart.$customer['customerName'].$strColEnd;
					$strExportData .= $strColStart.$customer['accountName'].$strColEnd;
					$strExportData .= $strColStart.$customer['creationDate'].$strColEnd;
					$strExportData .= $strColStart.$customer['Country'].$strColEnd;
					$strExportData .= $strColStart.$customer['customerStatus'].$strColEnd;
				}
				
				$response->rows[$i]['id'] = 'AA-'.$customer['customerID'];
				$response->rows[$i]['cell'] = array(
					$serialNo,
					$customer['customerName'],
					$customer['accountName'],
					$customer['customerStatus'],
					$customer['creationDate'],
					$customer['Country'],
					'<a href="javascript:" onClick="customerEdit(\''.$customer['customerID'].'\',\''.$customer['customerType'].'\');">Edit</a>',
					'<a href="javascript:" onClick="customerAction('.$customer['customerID'].');">Enable/Disable</a>'
				);
				$i++;
			}
			if($action == 'export')
				echo $strExportData;
			elseif($action == 'print'){
				echo $strExportData;
				echo '<div align="center" style="clear:both"><input type="button" value="Print" onClick="print();"/></div>';
			}else
				echo $response->encode($response);
		}
	}

?>