<?php
session_start();
/**
* @package Payex
* @subpackage Reports
* Short Description
* This page shows the number of deleted unresolved payments.
*/
include ("../include/config.php");
$date_time = date('Y-m-d');
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));
include ("security.php");
$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($_POST["tlaCode"] != "")
{
$arraytlaCode = explode(",",$_POST["tlaCode"]);
$tlaCodeString = implode("','",$arraytlaCode);
}

if($_POST["Submit"] != "")
{
	
	if($_POST["bankName"] == "natwest")
	{
		
			$queryNAT = "select count(id) as cnt from natwestpayments  where isResolved='N' and isDeleted='Y'";
	
	//////  This area is being commited as we are going to merge all the entries of a bank regardless of their TLA Code		
			
	}
	elseif($_POST["bankName"] == "barclay")
	{
		
			$qur = "select count(id) as cnt from barclayspayments where isResolved='N' and isDeleted='Y'";
			
	}
	else
	{
		///tlaCode = 'REM'
				//$count3 = countRecords("select count(id) as cnt from barclayspayments where tlaCode in ('BBP','BGC','TFT') and isResolved='N' and isDeleted='N'");
				$count1 = countRecords("select count(id) as cnt from barclayspayments where 1 and isResolved='N' and isDeleted='Y'");
				$countNat2 = countRecords("select count(id) as cnt from natwestpayments  where 1 and isResolved='N' and isDeleted='Y'");
				$countLin3 = countRecords("select count(id) as cnt from banklinepayments  where 1 and isResolved='N' and isDeleted='Y'");
				//$countNat4 = countRecords("select count(id) as cnt from natwestpayments where tlaCode in ('BBP','BGC','TFT') and isResolved='N' and isDeleted='N'");
			
	}
}
else
{
	//$count3 = countRecords("select count(id) as cnt from barclayspayments where tlaCode in ('BBP','BGC','TFT') and isResolved='N' and isDeleted='N'");
	$count1 = countRecords("select count(id) as cnt from barclayspayments where 1 and isResolved='N' and isDeleted='Y'");
	$countNat2 = countRecords("select count(id) as cnt from natwestpayments  where 1 and isResolved='N' and isDeleted='Y'");
	$countLin3 = countRecords("select count(id) as cnt from banklinepayments  where 1 and isResolved='N' and isDeleted='Y'");
	//$countNat4 = countRecords("select count(id) as cnt from natwestpayments where tlaCode in ('BBP','BGC','TFT') and isResolved='N' and isDeleted='N'");
}
	$unresPayBarclay = "unres-payments-deleted-ajax.php";

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Unresolved Deleted Payments </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0">
  <tr>
    <td colspan="2" valign="top">
	</td>
    </tr>
     
  <tr>
  	<td width="431" valign="top" colspan="2">
	<fieldset>
		<? if($_POST["Submit"] != "") { ?>
    <legend class="style2">Search Result for Unresolved Deleted Payments</legend>
   <? }else{ ?>
   <legend class="style2">Unresolved Deleted Payments List</legend>
  <? } ?>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
      <form name="addComplaint" action="" method="post" enctype="multipart/form-data">
        <tr>
          <td valign="top" bgcolor="#DFE6EA">
          	<?
	/*if($count3 > 0)
	{
	?>
	<a href="unres-payments.php?type=3" class="style3">There are <strong><?=$count3?></strong> unresolved payments in the category of BBP, BGC and TFT Barclay</a><br><br> 
	<?
	}*/
	if($count1 > 0)
	{
	?>
	<a href="<?=$unresPayBarclay?>?type=1" class="style3">There are <strong><?=$count1?></strong> unresolved deleted payments in the Barclay</a> <br><br>
	<?
	}
	/*if($countNat4 > 0)
	{
	?>
	<a href="unres-payments-natwest.php?type=4" class="style3">There are <strong><?=$countNat4?></strong> unresolved payments in the category of BBP, BGC and TFT for Natwest</a><br><br>
	<?
	}*/
	if(CONFIG_NATWEST_FILE == "1"){
	if($countNat2 > 0)
	{
	?>
	<a href="<?=$unresPayBarclay?>?type=2" class="style3">There are <strong><?=$countNat2?></strong> unresolved payments in the Natwest</a> <br><br>
	<?
	}
	if($countLin3 > 0)
	{
	?>
	<a href="<?=$unresPayBarclay?>?type=3" class="style3">There are <strong><?=$countLin3?></strong> unresolved payments in the BankLine</a> <br><br>
	<?
	}
}
	if($count3 == 0 && $count1 == 0 && $countNat4 == 0 && $countNat2 == 0 && $countLin3 == 0)
	{
		echo "There are no Unresolved Payments in Database.";	
	}
	?>
          	</td>
          </tr>
      </form>
    </table>
    <br>
    </fieldset>
	</td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>
