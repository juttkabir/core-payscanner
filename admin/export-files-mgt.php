<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//$agentType = getAgentType();
///////////////////////History is maintained via method named 'activities'

$msg = '';

if($_POST["sel"] == "FileName" || $_POST["Submit"] == "Update" || $_POST["Submit"] == "Restore Default")
{
	$FileName = $_POST["FileName"];
	$payexField = $_POST["payexField"];
	$changeFieldName = $_POST["changeFieldName"];
	if ($_POST["Submit"] == "Update")
	{
		$updatetblFile	= update("update ".TBL_EXPORT_FILE." set isEnable = '".$_POST["isEnable"]."' , Format = '".$_POST["Format"]."', fieldSpacing = '".$_POST["fieldSpacing"]."', lineBreak = '".$_POST["lineBreak"]."' where id = '".$FileName."'");
		if ($changeFieldName != "")
		{
			$updatetblFields = update("update ".TBL_EXPORT_FIELDS." set Lable = '".$changeFieldName."' , isActive = 'Y' where id = '".$payexField."'");
		}
		$msg = SUCCESS_MARK . " Record updated successfully";
	}
	elseif ($_POST["Submit"] == "Restore Default")
	{
		$delQuery = deleteFrom("delete from ".TBL_EXPORT_FIELDS." where fileID = '".$FileName."'");
		if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$RestoreQuery = insertInto("INSERT INTO `tbl_export_fields` (`id`, `fileID`, `payex_field`, `tableName`, `isActive`, `Lable`, `tempID`, `exportDefaultID`, `field_title`) 
			VALUES (31, 2, 'refNumberIM', 'transactions', 'Y', 'Numeris', 0, 0, ''),
			(32, 2, 'Date', 'transactions', 'Y', 'Date', 0, 0, ''),
			(33, 2, 'transDate', 'transactions', 'Y', 'Bank Operation Date', 0, 0, ''),
			(34, 2, 'PaymentNumber', 'transactions', 'Y', 'Payment Number', 0, 0, ''),
			(35, 2, 'accountName', 'customer', 'Y', 'Sender Number', 0, 0, ''),
			(36, 2, 'firstName,lastName', 'customer', 'Y', 'Sender Name', 0, 0, ''),
			(37, 2, 'registrationNumber', 'customer', 'Y', 'Sender Code Registartion number', 0, 0, ''),
			(38, 2, 'BICcode', 'customer', 'Y', 'BIC code', 0, 0, ''),
			(39, 2, 'clientCode', 'customer', 'Y', 'Client code in system', 0, 0, ''),
			(40, 2, 'initPaymentNum', 'customer', 'Y', 'Initial payment account number', 0, 0, ''),
			(41, 2, 'initSendNameSurname', 'customer', 'Y', 'Initial sender name and surname', 0, 0, ''),
			(42, 2, 'initSenderPerCode', 'customer', 'Y', 'Initial sender personal code', 0, 0, ''),
			(43, 2, 'cp_corresspondent_name', 'cm_collection_point', 'Y', 'Beneficiary collection point branch name', 0, 0, ''),
			(44, 2, 'firstName,lastName', 'beneficiary', 'Y', 'Beneficiary name and surname', 0, 0, ''),
			(45, 2, 'localAmount', 'transactions', 'Y', 'Amount', 0, 0, ''),
			(46, 2, 'currencyTo', 'transactions', 'Y', 'currency code ISO', 0, 0, ''),
			(47, 2, 'IBAN', 'bankDetails', 'Y', 'Account Number', 0, 0, ''),
			(48, 2, 'Remarks', 'bankDetails', 'Y', 'Comment box to have maximum 1257', 0, 0, '');
			");
		} else {
		$RestoreQuery = insertInto("INSERT INTO ".TBL_EXPORT_FIELDS." (`id`, `fileID`, `payex_field`, `tableName`, `isActive`, `Lable`) 
				VALUES (1, 1, 'refNumberIM', 'transactions', 'Y', 'Order Number'),
				(2, 1, 'transDate', 'transactions', 'Y', 'Remittance Date'),
				(4, 1, 'transAmount', 'transactions', 'Y', 'Dollar Amount'),
				(5, 1, 'exchangeRate', 'transactions', 'Y', 'Rate'),
        (6, 1, 'localAmount', 'transactions', 'Y', 'Real Amount'),
        (7, 1, 'firstName', 'customer', 'Y', 'Sender Name'),
        (9, 1, 'firstName', 'beneficiary', 'Y', 'Beneficiary Name'),
        (11, 1, 'Address', 'beneficiary', 'Y', 'Beneficiary Address'),
        (13, 1, 'City', 'beneficiary', 'Y', 'Beneficiary City'),
        (15, 1, 'State', 'beneficiary', 'Y', 'Beneficiary State'),
        (17, 1, 'Phone', 'beneficiary', 'Y', 'Beneficiary Telephone'),
        (19, 1, 'CPF', 'beneficiary', 'Y', 'CPF'),
        (21, 1, 'bankNumber', 'bank_data', 'Y', 'Bank Number'),
        (23, 1, 'bankName', 'bankDetails', 'Y', 'Bank Name'),
        (25, 1, 'branchCode', 'bankDetails', 'Y', 'Branch'),
        (27, 1, 'accNo', 'bankDetails', 'Y', 'Account Number'),
        (29, 1, 'accountType', 'bankDetails', 'Y', 'Account Type');
        ");
     }
     $changeFieldName = "";
     $msg = SUCCESS_MARK . " Restored Default Fields Successfully";
	}
	$query = "select * from ".TBL_EXPORT_FILE." where id = '".$FileName."'";
	$tbl_exp_file = selectFrom($query);
	$FieldsContent = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where fileID = '".$FileName."' order by `id`");
	if (!$tbl_exp_file)
	{
		$otherFields = 0;
	}
	else
	{
		$otherFields = 1;
	}
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function valid()
{
	if(document.frmManage.changeFieldName.value != "" && document.frmManage.payexField.value == "")
	{
		alert("Please first select the Field Name from above dropdown.");	
		frmManage.payexField.focus();
		return false;
	}	
}
function makeVisible(act) {
	if (act == 'visible') {
		document.getElementById('lineBreak').style.display = 'table-row';	
		document.getElementById('fieldSpacing').style.display = 'table-row';
	} else {
	document.getElementById('lineBreak').style.display = 'none';	
	document.getElementById('fieldSpacing').style.display = 'none';
	}
}
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex">Export Pages Management</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Export Pages Management </legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="frmManage" action="export-files-mgt.php" method="post">
			  <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			    <td colspan="2"><font color="<? echo SUCCESS_COLOR ?>"><b><? echo $msg?></b></font><br></td>
			    </tr>
			  <?
			  }
			  
			  ?>
			   <? if ($otherFields != 1){ ?>
			  <tr align="center">
			    <td colspan="2"><font color="#990000"><b><i>! Please first select the Page Link</i></b></font></td>
			    </tr>
			  <? } ?>
		    <tr>
			 	<td width="35%" align="right" bgcolor="#DFE6EA">
				  Page Link</td>
			    <td width="65%" bgcolor="#DFE6EA"><input type="hidden" name="sel" value="">
			    	<SELECT name="FileName" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'FileName'; document.forms[0].submit();">
                  <OPTION value="">- Select Link -</OPTION>
                  <?	if (CONFIG_EXPORT_TRANS_OPAL != '1') {  ?>
                  <OPTION value="1" <? echo ($FileName == '1' ? "selected" : "")?>>Export Transactions</OPTION>
			<?	} else {  ?>
								<OPTION value="2" <? echo ($FileName == '2' ? "selected" : "")?>>Export Transactions</OPTION>
				<?	}  ?>
                </SELECT>
			      </td>
			  </tr>
			    <? if ($otherFields == 1){ ?>
			  <tr>
				<td width="35%" align="right" bgcolor="#DFE6EA">
				  Enable</td>
			    <td width="65%" bgcolor="#DFE6EA"><SELECT name="isEnable" style="font-family:verdana; font-size: 11px">
                  <OPTION value="Yes">Yes</OPTION>
                  <OPTION value="No" <? echo ($tbl_exp_file["isEnable"]=="No" ? "selected" : "");?>>No</OPTION>
                </SELECT>
			      </td>
			  </tr>
			  <tr>
				<td align="right" valign="top" bgcolor="#DFE6EA">Format</td>
			    <td bgcolor="#DFE6EA"><input name="Format" type="radio" id="Format" value="xls" <? echo ($tbl_exp_file["Format"]=="xls" ? "checked" : "");?> onClick="makeVisible('hide');">
xls
<br>
<input name="Format" type="radio" id="Format" value="csv" <? echo ($tbl_exp_file["Format"]=="csv" ? "checked" : "");?> onClick="makeVisible('hide');">
csv
<br>
<input name="Format" type="radio" id="Format" value="txt" <? echo ($tbl_exp_file["Format"]=="txt" ? "checked" : "");?> onClick="makeVisible('visible');">
txt
</td>
			  </tr>
			  <tr id="lineBreak" <? if ($tbl_exp_file["Format"] != 'txt') { ?>style="display:none;"<? } ?>>
				<td align="right" valign="top" bgcolor="#DFE6EA">Line End With</td>
			    <td bgcolor="#DFE6EA">
			    	<select name="lineBreak">
			    		<option value="">-select-</option>
			    		<option value="CRLF" <? echo ($tbl_exp_file["lineBreak"] == 'CRLF' ? "selected" : "") ?>>CRLF</option>
			    	</select>
			    </td>
			  </tr>
			  <tr id="fieldSpacing" <? if ($tbl_exp_file["Format"] != 'txt') { ?>style="display:none;"<? } ?>>
				<td align="right" valign="top" bgcolor="#DFE6EA">Choose spacing b/w Fields</td>
			    <td bgcolor="#DFE6EA">
			    	<select name="fieldSpacing">
			    		<option value="space">SPACE</option>
			    		<option value="tab" <? echo ($tbl_exp_file["fieldSpacing"] == "tab" ? "selected" : "") ?>>TAB</option>
			    	</select>
			    </td>
			  </tr>
			
			  <tr>
				  <td align="right" valign="top" bgcolor="#DFE6EA">Field Name</td>
			    <td bgcolor="#DFE6EA">
				<select name="payexField" style="font-family:verdana; font-size: 11px">
				<option value="">-- Select Field --</option>
						  <?
						  	for ($k = 0; $k < count($FieldsContent); $k++)
						  	{
						  	?>
						  		<option value="<? echo $FieldsContent[$k]["id"];?>" <? echo ($FieldsContent[$k]["payex_field"]==$payexField ? "selected" : "");?>><? echo $FieldsContent[$k]["Lable"];?></option>
						  	<?	
						  	}
						  ?>				
				</select>
				</td>
			  </tr>
			  			  
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Change Field Name </td>
			    <td bgcolor="#DFE6EA"><input name="changeFieldName" type="text" id="changeFieldName" value="<?=$changeFieldName;?>">&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" class="flat" value="Restore Default"></td>
			    </tr>
			  <tr>
			    <td align="center" bgcolor="#DFE6EA" colspan="2"><b>Note: </b>If you want to change the Field Name, then you must select the appropriate Field Name from Above dropdown.</td>
			    </tr>
			  <tr>
			    <td align="center" bgcolor="#DFE6EA" colspan="2"><input name="Submit" type="submit" class="flat" value="Update" onClick="return valid();"></td>
			    </tr>
			    	<? } ?>
			    	</form>
			</table>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>


<!--
<?
session_start();
include ("../include/config.php");
include ("security.php");

if ($_POST["Submit"] == "Delete") {
	if (is_array($_POST["tempID"])) {
		foreach ($_POST["tempID"] AS $key => $value) {
			$delQuery  = "DELETE FROM " . TBL_EXPORT_TEMPLATE . " WHERE `id` = '".$value."'";
			$delQuery2 = "DELETE FROM " . TBL_EXPORT_FIELDS . " WHERE `tempID` = '".$value."'";
			deleteFrom($delQuery);
			deleteFrom($delQuery2);
		}
		insertError("Selected templates are deleted successfully.");
		redirect("export-files-mgt.php?msg=Y&success=Y");
	} else {
		$msg2 = CAUTION_MARK . " Please check atleast one checkbox to delete template.";	
	}
}

$SQL_Query = "SELECT * FROM " . TBL_EXPORT_TEMPLATE . " WHERE 1";
$templates = selectMultiRecords($SQL_Query);
$allCount  = count($templates);

?>
<html>
<head>
	<title>Templates</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	
	var total = <? echo $allCount ?>;
	
	function checkAll() {
		
		if (document.getElementById("All").checked == true) {
			for (var i = 0; i < total; i++) {
				document.getElementById('tempID[' + i + ']').checked = true;
			}
		} else {
			for (var i = 0; i < total; i++) {
				document.getElementById('tempID[' + i + ']').checked = false;
			}
		}
		
	}
	
	function checkForm() {
		
		var flag  = false;
		
		for (var i = 0; i < total; i++) {
			if (document.getElementById('tempID[' + i + ']').checked == true) {
				flag = true;	
			}
		}
		
		if (flag) {
			if (confirm("Are you sure you want to delete the checked template(s).")) {
				return true;
			} else {
				return false;	
			}
		} else {
			alert("No template is checked for deletion.");
			return false;
		}
		
	}
		
	</script>
	
  <style type="text/css">
.style2 {color: #6699CC;
	font-weight: bold;
  </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<?	if ($msg2 != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td colspan="2" align="center" class="tab-r"><? echo $msg2 ?></td>
              </tr>
            </table></td>
          </tr>
	<?	} else if ($_GET["msg"] != "") {  ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center">&nbsp;</td>
                <td width="635" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"] != "" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>&nbsp;&nbsp;<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
	<?	}  ?>
  <form action="export-files-mgt.php" method="post" onSubmit="return checkForm();">
  <tr>
    <td>
		<table width="450" border="0" cellspacing="1" cellpadding="1">
          <tr bgcolor="#DFE6EA"> 
          <td colspan="5" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Export Templates</strong></font></td>
			</tr>
		  </table>
		</td>
          </tr>
          <tr bgcolor="#eeeeee"> 
            <td colspan="5"><a href="add-export-template.php" class="style2">Add New Template</a></td>
          </tr>
<?	if ($allCount > 0) {  ?>
          <tr bgcolor="#DFE6EA"> 
            <td align="center"><input type="checkbox" name="All" id="All" onClick="checkAll();"></td>
            <td><font color="#005b90"><strong>Template Title</strong></font></td>
            <td><font color="#005b90"><strong>Format</strong></font></td>
            <td><font color="#005b90"><strong>Export From</strong></font></td>
            <td>&nbsp;</td>
          </tr>
<?
			for ($i = 0 ; $i < count($templates) ; $i++) {
?>
          <tr bgcolor="#eeeeee"> 
            <td align="center"><input type="checkbox" name="tempID[<?=$i;?>]" id="tempID[<?=$i;?>]" value="<? echo $templates[$i]["id"] ?>"></td>
            <td><font color="#005b90"><? echo $templates[$i]["title"] ?></font></td>
            <td><font color="#005b90"><? echo $templates[$i]["format"] ?></font></td>
            <td><font color="#005b90"><? echo $templates[$i]["exportName"] ?></font></td>
            <td><font color="#005b90"><a href="add-export-template.php?tempID=<?=$templates[$i]["id"]?>" class="style2">Edit</a></font></td>
          </tr>
	<?
			}
	?>
          <tr bgcolor="#eeeeee"> 
            <td colspan="5" align="center"> <input type="submit" name="Submit" value="Delete"> 
            </td>
          </tr>
<?	} else {  ?>
          <tr bgcolor="#eeeeee"> 
            <td colspan="5" align="center"> No templates are found. </td>
          </tr>
<?	}  ?>
     </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
-->