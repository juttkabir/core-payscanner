<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];
define("CONFIG_USE_TRANSACTION_DETAILS", "1");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'
$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;

if($limit == 0)
	$limit=50;

	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}

	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	//

$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";



if($_POST["transType"]!="")
		$transType = $_POST["transType"];
	elseif($_GET["transType"]!="")
		$transType=$_GET["transType"];

if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
{
	if($_POST["transSource"]!="")
	{
		$transSource = $_POST["transSource"];

	}
	else if($_GET["transSource"]!="")
	{
		$transSource = $_GET["transSource"];

	}
}
if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="")
		$Submit=$_GET["Submit"];

	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];

if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="")
		$by = $_GET["searchBy"];

$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];
//echo count($trnsid);
//echo $trnsid[0];
$ttrans = $_POST["totTrans"];
if($_POST["btnAction"] != "")
{
	for ($i=0;$i< $ttrans;$i++)
	{

		if(count($trnsid) >= 1)
		{

			if($_POST["btnAction"] != "")
			{
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $transType  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];

				 if(!empty($transDate))
				 {
				 	$transDate = date("F j, Y", strtotime("$transDate"));
				}

				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				}
				// getting BEneficiray agent Email
				if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				}
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

/**********************************************************/

			}

//sendMail($custEmail, $subject, $message, $fromName, $fromEmail);

			if($_POST["btnAction"] == "Verify")
			{
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Processing', verifiedBy='$username', verificationDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");

				$descript = "Transaction is Verified";
				activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);

				insertError("Transaction Verified successfully.");
				//echo "HERE";
			}

		}
	}
redirect("verify-transactions.php?msg=Y&action=". $_GET["action"]);
}


$query = "select * from ". TBL_TRANSACTIONS . " as t where 1";
$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1";

if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t where 1 ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1 ";

				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";

				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

				$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}

				$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				$query = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";

				$queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";

				$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
				$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";

				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}

				$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
			}
}
	if($transType != "")
	{
		$query .= " and (t.transType='".$transType."')";
		$queryCnt .= " and (t.transType='".$transType."')";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType."')";
		  $queryonlineCnt .= " and (t.transType='".$transType."')";
			}

	}


if($_GET["action"] == "Verify" || $_GET["action"] == "verify")
{
	if(CONFIG_VERIFY_TRANSACTION_ENABLED){
		$query .= " and (t.transStatus ='Pending') ";
		$queryCnt .= " and (t.transStatus ='Pending') ";
	}
	else{
		$query .= " and (t.transStatus ='Processing') ";
		$queryCnt .= " and (t.transStatus ='Processing') ";
	}
	/**
	 * Excluding the transaction having no distributor assigned
	 * @Ticket# 3321 / 3320

	if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1")
	{
		$query .= " and (t.benAgentID != 0) ";
		$queryCnt .= " and (t.benAgentID != 0) ";
	}
	*/

	if($transID != "" && $by != 0)
	{
		if(CONFIG_VERIFY_TRANSACTION_ENABLED){
			$queryonline .= " and (t.transStatus='Pending')";
		  $queryonlineCnt .= " and (t.transStatus='Pending')";
		}else{
			$queryonline .= " and (t.transStatus='Processing')";
		  $queryonlineCnt .= " and (t.transStatus='Processing')";
		}
	}

	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($transSource!= "")
		{
			if($transSource == "O")
			{
				$query.= " and trans_source = 'O' ";
				$queryCnt .= " and trans_source = 'O' ";
			}
			else if ($transSource == "P")
			{
				$query.= " and trans_source != 'O' ";
				$queryCnt .= " and trans_source != 'O' ";
			}


		}
	}

}

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	if($transID != "")
		{
		$queryonline .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	}
}
	if($agentType == "Branch Manager")
	{
		$query .= " and t.custAgentParentID ='$agentID' ";
		$queryCnt .= " and t.custAgentParentID ='$agentID' ";
	}

	if(CONFIG_USE_TRANSACTION_DETAILS == "1"){
		// $query .= " and transDetails !='No'";
		// $queryCnt .= " and transDetails !='No'";
	}
	if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){
		// $queryCnt .= "  AND (valueDate != '0000-00-00 00:00:00' AND valueDate != '') AND validValue!='N'";
		// $query .= "  AND (valueDate != '0000-00-00 00:00:00' AND valueDate != '') AND validValue!='N'";
	}

 $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
  $query .= " LIMIT $offset , $limit";
//print_r(CONFIG_USE_TRANSACTION_DETAILS );
//   echo '<pre>' . $query . '</pre>';

  $contentsTrans = selectMultiRecords($query);

  $allCount = countRecords($queryCnt);



		if($transID != "" && $by != 0)
		{


			$onlinecustomerCount = countRecords($queryonlineCnt);

			$allCount = $allCount + $onlinecustomerCount;



					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}

				$queryonline .= " order by t.transDate DESC";
			  $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }

	  }

?>
<html>
<head>
	<title>Verify Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>
      Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="verify-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions
              <input name="transID" type="text" id="transID" value="<?=$transID?>">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
		<select name="searchBy" >
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=__("Sender");?> Name</option>
			<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
        <? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{

		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>


            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>


        </td>
        </tr>
        <tr align="center">
            <td>
                <input type="submit" name="Submit" value="Search">
            </td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="verify-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print($offset + 1) . ' - ' . ($offset + (count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount;?>
                    <?php }
;?>
                  </td>
                  <?php if ($prv >= 0) {?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&action=" . $_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&action=" . $_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php }?>
                  <?php
if (($nxt > 0) && ($nxt < $allCount)) {
	$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&action=" . $_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&action=" . $_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php }?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
			?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1"><?=__("Sender");?> Name </span></td>
			  <?
			  	if(CONFIG_SENDER_BANK_DETAILS == "1")
				{
			?>
					<td align="center" class="style1"><?=__("Sender");?> Bank</td>
				  	<td align="center" class="style1">Branch Address</td>
			    	<td align="center" class="style1">Branch Code</td>
			<?
				}
			  ?>

			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <?
					if(CONFIG_REMARKS_ENABLED == "1")
					{
			?>
						<td width="74" align="center" class="style1"><?=CONFIG_REMARKS?></td>
			<?
					}
				?>
			  <td width="146" align="center"><? if ($_GET["action"] != "") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
			  <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>


				  <?
$contentsTrans[$i]["totalAmount"] = number_format($contentsTrans[$i]["totalAmount"], 2, '.', '');
				  ?>

				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{

				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
					<?
						if(CONFIG_SENDER_BANK_DETAILS == "1")
						{
							if(!empty($contentsTrans[$i]["senderBank"]))
							{
								$senderBank = selectFrom("select name, country from ".TBL_BANKS." where bankId='".$contentsTrans[$i]["senderBank"]."'");
					?>
								<td bgcolor="#FFFFFF"><?=$senderBank["name"]?>
								<!--edit code for adding new field Branch Address,Branch Code-->
								<td bgcolor="#FFFFFF"><?=$senderBank["branchAddress"]?></td>
								<td bgcolor="#FFFFFF"><?=$senderBank["branchCode"]?></td>
								<!--closing of above edited code-->

								</td>
					<?
							} else {
					?>
								<td bgcolor="#FFFFFF" colspan="3">&nbsp;</td>
					<?
							}
						}
					?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
					<?
						if(CONFIG_SENDER_BANK_DETAILS == "1")
						{
							if(!empty($contentsTrans[$i]["senderBank"]))
							{
								$senderBank = selectFrom("select name,branchCode,branchAddress,country from ".TBL_BANKS." where bankId='".$contentsTrans[$i]["senderBank"]."'");
					?>
								<td bgcolor="#FFFFFF"><?=$senderBank["name"]?></td>
						<!--edit code for adding new field Branch Address,Branch Code-->
								<td bgcolor="#FFFFFF"><?=$senderBank["branchAddress"]?></td>
								<td bgcolor="#FFFFFF"><?=$senderBank["branchCode"]?></td>
						<!--closing of above edited code-->
					<?
							} else {
					?>
								<td bgcolor="#FFFFFF" colspan="3">&nbsp;</td>
					<?
							}
						}
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
						}

				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>				  </td>
					<?
						if(CONFIG_REMARKS_ENABLED == "1")
						{
					?>
							<td align="center" bgcolor="#FFFFFF"><?=$contentsTrans[$i]["clientRef"]?></td>
					<?
						}
					?>

				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>


				</tr>
				<?
			}
			?>

			<? for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["totalAmount"] .  " " . $onlinecustomer[$j]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{

				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>

				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>				  </td>


				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "") {  $tempTransId = $onlinecustomer[$j]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				   <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$j]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>

				<?
			}
			?>

			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<?echo $i?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
          <tr>
            <td  bgcolor="#000000">
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print($offset + 1) . ' - ' . ($offset + (count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount;?>
                    <?php }
;?>
                  </td>
                  <?php if ($prv >= 0) {?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=" . $_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=" . $_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php }?>
                  <?php
if (($nxt > 0) && ($nxt < $allCount)) {
	$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=" . $_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=" . $_GET["sortBy"] . "&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&action=" . $_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php }?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>