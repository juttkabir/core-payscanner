<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Add Distributor to Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	
	jQuery(document).ready(function(){
		jQuery("#transList").jqGrid({
			url:'add-transaction-distributor-ajax.php?q=2&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Date', 
				'<?=$systemCode?>', 
				'<?=$manualCode?>', 
				'Status', 
				'Total Amount',
				'Total Local Amount',
				'Sender Name',
				'Beneficiary Name',
				<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
				'Bank Name',
				<? } ?>
				'Created By'
			],
			colModel:[
				{name:'transDate',index:'transDate', width:55, sorttype:'date', datefmt:'d-m-Y'},
				{name:'systemCode',index:'refNumberIM', width:90},
				{name:'manualCode',index:'refNumber', width:100},
				{name:'status',index:'transStatus', width:80, align:"right"},
				{name:'totalAmount',index:'totalAmount', width:80, align:"right", align:'right'},		
				{name:'totalLAmount',index:'localAmount', width:80,align:"right"},
				{name:'sender',index:'firstNameS', width:100, sortable:false},
				{name:'beneficiary',index:'firstNameB', width:100, sortable:false},
				<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
					{name:'bank',index:'bankName', width:100},
				<? } ?>
				{name:'createdBy',index:'createdBy', width:100, sortable:false}
			],
			rowNum: <?=CONFIG_MAX_TRANSACTIONS?>,
			rowList: [10,20,50,100,150,200,250,300],
			imgpath: gridimgpath,
			pager: jQuery('#pager'),
			sortname: 'transDate',
			viewrecords: true,
			sortorder: "desc",
			multiselect: true,
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions without Distributors"<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>,
			onSelectRow:function(id)
						{
							if(id)
							{
								updateTotals(id);
							}
						},
			onSelectAll:function(ids)
						{
							var idsArray = new Array();
							var s_ids = new String();
							
							if(ids != "")
							{
								resetTotals();
								
								s_ids = ids.toString();
								idsArray = s_ids.split(",");
								
								for(var i=0; i<idsArray.length; i++)
								{
									updateTotals(idsArray[i]);
								}
							} else {
								resetTotals();
							}
						},
			loadComplete:function(){
				resetTotals();
			}
			<? } ?>
		});
		
		jQuery("#btnAction").click(function(){
			var s = jQuery("#transList").getGridParam('selarrrow');
			var but = jQuery("#distribut").val();
			
			if(but == "")
			{
				alert("Please select a distributor.");
			} else {
				if(s == "")
				{
					alert("Please select one or more transactions.");
				} else {
					extraParams = "&btnAction=Y&distribut="+ but +"&transIds=" + s;
					gridReload();
				}
			}
		});
		
		jQuery('a').cluetip({splitTitle: '|'});
	});
	
	var extraParams = "";
	
	function gridReload()
	{

		var l_fDay = jQuery("#fDay").val();
		var l_fMonth = jQuery("#fMonth").val();
		var l_fYear = jQuery("#fYear").val();
		var l_tDay = jQuery("#tDay").val();
		var l_tMonth = jQuery("#tMonth").val();
		var l_tYear = jQuery("#tYear").val();
		var l_transID = jQuery("#transID").val();
		var l_searchBy = jQuery("#searchBy").val();
		<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
		var l_localAmountCurrency = jQuery("#localAmountCurrency").val();
		var l_foreignAmountCurrency = jQuery("#foreignAmountCurrency").val();
		var l_totalAmount = jQuery("#totalAmount").val();
		var l_totalLocalAmount = jQuery("#totalLocalAmount").val();
		var l_bankName = jQuery("#bankName").val();
		var l_createdBy = jQuery("#createdBy").val();
		var l_bankType = jQuery("#bankType").val();
		var l_localAmountCondition = jQuery("#localAmountCondition").val();
		var l_totalAmountCondition = jQuery("#totalAmountCondition").val();
		
		var theUrl = "add-transaction-distributor-ajax.php?fDay="+l_fDay+"&fMonth="+l_fMonth+"&fYear="+l_fYear+"&tDay="+l_tDay+"&tMonth="+l_tMonth+"&tYear="+l_tYear+""
					+"&transID="+l_transID+"&searchBy="+l_searchBy+"&localAmountCurrency="+l_localAmountCurrency+"&foreignAmountCurrency="+l_foreignAmountCurrency+""
					+"&totalAmount="+l_totalAmount+"&totalLocalAmount="+l_totalLocalAmount+"&bankName="+l_bankName+"&createdBy="
					+l_createdBy+"&bankType="+l_bankType+"&localAmountCondition="+l_localAmountCondition+"&totalAmountCondition="+l_totalAmountCondition+"&Submit=Search";
		
		<? } else { ?>
		
		var theUrl = "add-transaction-distributor-ajax.php?fDay="+l_fDay+"&fMonth="+l_fMonth+"&fYear="+l_fYear+"&tDay="+l_tDay+"&tMonth="+l_tMonth+"&tYear="+l_tYear+""
					+"&transID="+l_transID+"&searchBy="+l_searchBy+"&Submit=Search";
		
		<? } ?>
		
		if(extraParams != "")
		{
			theUrl = theUrl + extraParams;
			extraParams = "";
		}
		
		jQuery("#transList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
		
		<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
		resetTotals();
		<? } ?>
	}
	
	function resetTotals()
	{
		var fCurrency = jQuery("#localAmountCurrency").val();
		var lCurrency = jQuery("#foreignAmountCurrency").val();
		
		tAmt = 0;
		lAmt = 0;
		
		jQuery("#tfa").text(tAmt.toFixed(4) + " " + fCurrency);
		jQuery("#tla").text(lAmt.toFixed(4) + " " + lCurrency);
	}
	
	function updateTotals(rowId)
	{
		var ret = jQuery("#transList").getRowData(rowId);
		var fCurrency = jQuery("#localAmountCurrency").val();
		var lCurrency = jQuery("#foreignAmountCurrency").val();
		
		if(jQuery("#jqg_"+rowId).attr("checked"))
		{
			tAmt += parseInt(ret.totalAmount);
			lAmt += parseInt(ret.totalLAmount);
		} else {
			tAmt = tAmt - parseInt(ret.totalAmount);
			lAmt = lAmt - parseInt(ret.totalLAmount);
		}
		
		jQuery("#tfa").text(tAmt.toFixed(4) + " " + fCurrency);
		jQuery("#tla").text(lAmt.toFixed(4) + " " + lCurrency);
	}
</script>
</head>
<body>
<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" id="boundingBox">
    <tr>
        <td colspan="2" class="pageTitle">Assign Distributor to Transactions </td>
    </tr>
    <tr>
        <td colspan="2"><table width="100%" border="0" cellpadding="5" bordercolor="#666666">

                <tr>
                    <td colspan="3" nowrap="nowrap" class="reportHeader">Search Transactions </td>
                </tr>
                <tr>
                    <td width="33%" valign="top" nowrap="nowrap">From 
                        <? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
                        <br />
                        <select name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                            <?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
                        </select>
                        <select name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                            <option value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</option>
                            <option value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</option>
                            <option value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</option>
                            <option value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</option>
                            <option value="05" <? echo ($month =="05" ? "selected" : "")?>>May</option>
                            <option value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</option>
                            <option value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</option>
                            <option value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</option>
                            <option value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</option>
                            <option value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</option>
                            <option value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</option>
                            <option value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</option>
                        </select>
                        <select name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                            <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
                        </select>
                        <a href="#" title="From Date|Select a date to define the begining of date range.">&nbsp;<img src="images/info.gif" width="12" height="12" border="0" /></a><br />
                        <br />
						To
						<br />
                        <select name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                            <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
                        </select>
                        <select name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                            <option value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</option>
                            <option value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</option>
                            <option value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</option>
                            <option value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</option>
                            <option value="05" <? echo ($month =="05" ? "selected" : "")?>>May</option>
                            <option value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</option>
                            <option value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</option>
                            <option value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</option>
                            <option value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</option>
                            <option value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</option>
                            <option value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</option>
                            <option value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</option>
                        </select>
                        <select name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                            <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
                        </select>
&nbsp;                        <a href="#" title="To Date|Select a date to define the end of date range."><img src="images/info.gif" width="12" height="12" border="0" /></a><br />
			<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
                        <br />
                        Local Amount Currency &nbsp;&nbsp;
                        <?
				$strLacSql = "select distinct(currencyFrom) from ".TBL_TRANSACTIONS;
				$arrLacData = selectMultiRecords($strLacSql);
			?>
                        <br />
                        <select name="localAmountCurrency" id="localAmountCurrency">
                            <? 
				foreach($arrLacData as $strKey => $strVal) 
			   	{
					if($strVal["currencyFrom"] == $_REQUEST["localAmountCurrency"])
					{
			?>
                            <option value="<?=$strVal["currencyFrom"]?>" selected="selected">
                            <?=$strVal["currencyFrom"]?>
                            </option>
                            <?
					}
					else
					{
			?>
                            <option value="<?=$strVal["currencyFrom"]?>">
                            <?=$strVal["currencyFrom"]?>
                            </option>
                            <? 
					}			
			   } 
			?>
                        </select>
                        &nbsp;&nbsp;<a href="#" title="Local Amount Currency|Select the currency for local amount."><img src="images/info.gif" width="12" height="12" border="0" /></a><br />
                        <br />                        Foreign Amount Currency &nbsp;&nbsp;
                        <?
				$strFacSql = "select distinct(currencyTo) from ".TBL_TRANSACTIONS;
				$arrFacData = selectMultiRecords($strFacSql);
			?>
                        <br />
                        <select name="foreignAmountCurrency" id="foreignAmountCurrency">
                            <? 
				foreach($arrFacData as $strKey => $strVal) 
				{
					if($strVal["currencyTo"] == $_REQUEST["foreignAmountCurrency"])
					{
			?>
                            <option value="<?=$strVal["currencyTo"]?>" selected="selected">
                            <?=$strVal["currencyTo"]?>
                            </option>
                            <?
					}
					else
					{
			?>
                            <option value="<?=$strVal["currencyTo"]?>">
                            <?=$strVal["currencyTo"]?>
                            </option>
                            <? 
					}
				} 
			?>
				</select>
   &nbsp;                     <a href="#" title="Foreign Amount Currency|Select the currency for foreign amount."><img src="images/info.gif" width="12" height="12" border="0" /></a><br />
                        <br />
                        <br />
					<? } ?>
					</td>
                    <td width="34%" valign="top" nowrap="nowrap">Search Word<br />
                        <input name="transID" type="text" id="transID" value="<?=$transID?>" />
                        &nbsp;                        <a href="#" title="Search Word|Enter a keyword to search on the selection made from Searcy By selection box (below). System will look into matching records beginning with your supplied keyword."><img src="images/info.gif" width="12" height="12" border="0" /></a><br />
<br />
Search By <br />
<select name="searchBy" id="searchBy">
    <option value=""> - Select - </option>
    <option value="1">By Reference No/<?=$manualCode?></option>
    <option value="2">By Sender Name</option>
    <option value="3">By Beneficiary Name</option>
</select>
&nbsp;<a href="#" title="Search By|Select a field to search the (above) supplied Search Word in. System will look into selected field for records beginning with supplied search keyword."><img src="images/info.gif" width="12" height="12" border="0" /></a><br />
<br />
<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
Created By&nbsp;<br />
<input type="text" name="createdBy" value="<?=$_REQUEST["createdBy"]?>" id="createdBy" />
&nbsp;<a href="#" title="Created By|Name of the user to search for certain transactions, were created by."><img src="images/info.gif" width="12" height="12" border="0" /></a>
<br />
<br />
Transaction Banking Type&nbsp;<br />
<select name="bankType" id="bankType">
	<option value="">ALL</option>
	<option value="normal">Normal</option>
	<option value="bank24hr">24 Hr Banking</option>
</select>
&nbsp;<a href="#" title="Transaction Banking Type| Banking type of the transaction."><img src="images/info.gif" width="12" height="12" border="0" /></a>
<? } ?>
</td>
                    <td width="33%" valign="top" nowrap="nowrap">
						<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
						Total Amount<br />
                        <input type="text" name="totalAmount" id="totalAmount" value="<?=$_REQUEST["totalAmount"]?>" />
						&nbsp;
						<select name="totalAmountCondition" id="totalAmountCondition">
							<option value="eq">Equal (=)</option>
							<option value="gt">Greater Than (&gt;)</option>
							<option value="lt">Less Than (&lt;)</option>
						</select>
						&nbsp;
						<a href="#" title="Total Amount|Enter the total (foreign) amount to search for matching records. The next field is to select the total amount criteria. <br />E.g. if you want to search all the transaction which have total amount grater then 15 then enter 15 in total amount field and select '&gt;' from drop down againt total amount field.">
						<img src="images/info.gif" width="12" height="12" border="0" /></a>
						&nbsp;&nbsp;<br />
						<br />
						Total Local Amount&nbsp;<br />
						<input type="text" name="totalLocalAmount" id="totalLocalAmount" value="<?=$_REQUEST["totalLocalAmount"]?>" />
						&nbsp;
						<select name="localAmountCondition" id="localAmountCondition">
							<option value="eq">Equal (=)</option>
							<option value="gt">Greater Than (&gt;)</option>
							<option value="lt">Less Than (&lt;)</option>
						</select>
						&nbsp;
						<a href="#" title="Total Local Amount|Enter the total local amount to search for matching records. The next field is to select the total local amount criteria. <br />E.g. if you want to search all the transaction which have total local amount less then 100 then enter 100 in total local amount field and select '&lt;' from drop down againt total local amount field.">
						&nbsp;<img src="images/info.gif" width="12" height="12" border="0" /></a><br />
						<br />
						Bank Name&nbsp;<br />
						<input type="text" name="bankName" id="bankName" value="<?=$_REQUEST["bankName"]?>" />
						&nbsp;<a href="#" title="Bank Name|Enter the bank name to search for. System will look for matching records beginning with the supplied bank name."><img src="images/info.gif" width="12" height="12" border="0" /></a>
						<? } ?>
					</td>
                </tr>
                <tr>
                    <td colspan="3" nowrap="nowrap" class="buttonsToolbar"><input type="button" name="Submit" id="submitButton" value="Search" onclick="gridReload()" />
                    &nbsp;<a href="#" title="Search Tip|System searches the records based on your specified criteria. However, if your search returns a wide range of records (i.e. more than you expected) then try narrow down the search by supplying data in more (available) fields. The same rule works to widen the search - in reverse - when you do not find the desired results; i.e. defining lesser criteria."><img src="images/info.gif" width="12" height="12" border="0" /></a></td>
                </tr>
                    </table></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0"></table>
			<div id="pager" class="scroll" style="text-align:center;"></div>		</td>
    </tr>
	<tr>
		<td>
			<? if(CONFIG_EXCLUDE_DISTRIBUTOR_AT_CREATE_TRANS == "1"){ ?>
			Total Amount = <strong><span id="tfa">0.0000</span></strong> | 
			Total Local Amount = <strong><span id="tla">0.0000</span></strong>
		&nbsp;&nbsp;<a href="#" title="System|Displaying total of Foreign and Local amounts for selected transactions."><img src="images/info.gif" width="12" height="12" border="0" /></a>
			<? } ?>
			&nbsp;
		</td>
		<td align="right">
			<select name="distribut" id="distribut">
				<option value=""> - Select One - </option>
				<?
					$transType = "Bank Transfer";
					$distributors = "select * from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and PPType = '' and agentStatus = 'Active' order by userID";
					$ida = selectMultiRecords($distributors);
					
					for ($j=0; $j < count($ida); $j++)
					{
						$authority = $ida[$j]["authorizedFor"];
						$toCountry = $ida[$j]["IDAcountry"];
						$isDefault = $ida[$j]["defaultDistrib"];
				?>
						<option value="<?=$ida[$j]["userID"]?>"><?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?></option>
				<?	
					}
				?>
			</select>
		
			<input type="hidden" name="foreignAmountCurrency" value="<?=$_REQUEST["foreignAmountCurrency"]?>" />
			<input type="hidden" name="localAmountCurrency" value="<?=$_REQUEST["localAmountCurrency"]?>" />
			<input type='hidden' name='totTrans' value='<?echo $i?>'>
			<input name="btnAction" id="btnAction" type="button" value="Assign Distributors">
&nbsp;<a href="#" title="Assign Distributor|Select a distributor from available (drop down) list, and click the button to assign selected transactions."><img src="images/info.gif" width="12" height="12" border="0" /></a>&nbsp; </td>
	</tr>
</table>
</body>
</html>