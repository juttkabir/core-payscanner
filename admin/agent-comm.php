<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

$_SESSION["fMonth"]="";
$_SESSION["fDay"]="";
$_SESSION["fYear"]="";

$_SESSION["tMonth"]="";
$_SESSION["tDay"]="";
$_SESSION["tYear"]="";

//$agentType = getAgentType();
if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
{
	$query = "select transStatus, transDate, transID, totalAmount, name, IMFee, refNumber, refNumberIM, commPackage, agentCommission, AgentComm, CommType, customerID, benID  from ". TBL_TRANSACTIONS . " as t,
 	" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
 	
	if($agentType == "Branch Manager")
	{
			$query .= " and a.parentID = '$parentID'";						
	} 	
}
else
{
	$query = "select transStatus, transDate, transID, totalAmount, name, IMFee, refNumberIM, refNumber, commPackage, agentCommission, AgentComm, CommType, customerID, benID  from ". TBL_TRANSACTIONS . " as t,
 	" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;

}
//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search")
{
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
		
	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	if($fromDate<$toDate)
	{
		$queryDate = " and (t.transDate >= '$fromDate' and t.transDate <= '$toDate')";
	}
	$query .=  " and (a.name like '" . $_POST["txtSearch"] . "%' OR a.username like '" . $_POST["txtSearch"] . "%' OR  t.transID='". $_POST["txtSearch"]."') $queryDate";				
	
	if($_POST["transStatus"] != "")
	{
		$query .= " and t.transStatus='".$_POST["transStatus"]."' ";
	}
	else
	{
		//$query .= " and t.transStatus='Confirmed' ";	
	}
	
}
else
{
	$todate = date("Y-m-d");
	//$query .= " and t.transStatus='Confirmed'";
}

$query .= "  order by transDate DESC";

$contentsTrans = selectMultiRecords($query);
//echo $query .  count($contentsTrans);
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Agent Commission 
      Report </font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
        <form action="agent-comm.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
        <br>	
        Agent Name / Transaction No.
        <input name="txtSearch" type="text" id="txtSearch">
        <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Status - </option>
          <option value="Processing">Processing</option>
          <option value="Pending">Pending</option>
          <option value="Recalled">Recalled</option>
          <option value="Confirmed">Confirmed</option>
          <option value="Rejected">Rejected</option>
          <option value="Cancelled">Cancelled</option>
          <option value="Cancelled">Waiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Delivered">Delivered</option>
        </select>            
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">

	    <tr>
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans)?> transactions. </span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Date</span></td>
			  <td width="75"><span class="style1"><? echo $systemCode;?></span></td>
			  <td width="75"><span class="style1"><? echo $manualCode;?></span></td>			  
			  <td width="100"><span class="style1">Status</span></td>
			  <td width="200"><span class="style1">Agent Name </span></td>
			  <td width="75"><span class="style1">Amount</span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="50" bgcolor="#FFFFFF"><span class="style1"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></span></td>
			  <td width="75"><span class="style1">Commission</span></td>
			  <td width="75"><span class="style1">Commission Type</span></td>
			  </tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>
				
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				   <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="200" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["name"]?></td>
				  <td width="75" bgcolor="#FFFFFF">
				  <? 
				  echo $contentsTrans[$i]["totalAmount"];
				  $totalAmount += $contentsTrans[$i]["totalAmount"];
				  ?></td>
				  
				  <? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <? $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  
				  
				  <td width="50" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["IMFee"];
				  $totalFee += $contentsTrans[$i]["IMFee"];
				  ?></td>
				  <? //$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="75" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <?
				  /*switch ($contentsTrans[$i]["commPackage"])
				  {
				  	case "001": // Fixed amount per transaction
					{
						$agentCommission = $contentsTrans[$i]["agentCommission"];
						break;
					}
				  	case "002": // percentage of total transaction amount
					{
						$agentCommission = ($contentsTrans[$i]["totalAmount"] * $contentsTrans[$i]["agentCommission"]) / 100;
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						$agentCommission = ($contentsTrans[$i]["IMFee"] * $contentsTrans[$i]["agentCommission"]) / 100;
						break;
					}
					case "004":
					{
						$agentCommission = 0;
						break;
					}
				}*/
				
				if($contentsTrans[$i]["transStatus"]!="Recall" && $contentsTrans[$i]["transStatus"]!="Cancel")
				{
				   $agentCommission = $contentsTrans[$i]["AgentComm"];//
				   				  
				   echo $agentCommission; //$contentsTrans[$i]["commPackage"] . " " . $contentsTrans[$i]["agentCommission"]
				   $totalComm += $agentCommission;
				  }
				   ?></td>
				   <td width="75" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <?
				 
				$commissionType = $contentsTrans[$i]["CommType"];			  
				   echo $commissionType; //$contentsTrans[$i]["commPackage"] . " " . $contentsTrans[$i]["agentCommission"]
				   
				   ?></td>
			    </tr>
				<?
			}
			?>
			<!--tr bgcolor="#CCCCCC">
			  <td width="100">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="100">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td width="200" align="right"><span class="style3">Total:</span></td>
			  <td width="75"><strong>< echo $totalAmount></strong></td>
			  <td width="50"><strong>< echo $totalFee></strong></td>
			  <td width="75"><strong>< echo $totalComm></strong></td>
			  </tr-->
			<?
			} // greater than zero
			?>
          </table>
		  <table border="2" bordercolor="#666666" bgcolor="#CCCCCC">
		  	<tr>
			      <td width="75" height="22">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="100" align="right"><span class="style3">Total:</span></td>
			  <td width="75"><strong><? echo $totalAmount?></strong></td>
			  <td width="75">&nbsp;</td>
			  <td width="75">&nbsp;</td>
			  <td width="50"><strong><? echo $totalFee?></strong></td>
			  <td width="75"><strong><? echo $totalComm?></strong></td>
			  <td width="75">&nbsp;</td>
			  
			 
			</tr>
		  </table></td>
        </tr>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
