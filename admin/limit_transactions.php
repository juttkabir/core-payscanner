<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'
$date_time = getCountryTime(CONFIG_COUNTRY_CODE);
$countOnlineRec = 0;
$limit = 50;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];
if($_POST["userName"]!="")
		$userName = $_POST["userName"];

if($_POST["Submit"] != "")
{
	$limitObject = new TransLimit;
	
	
	if($_POST["Submit"] == "UpdateLimit")
	{
		echo $limitData[] = $_POST["limitUserID"];
		echo $limitData[] = $_POST["limitType"];
		echo $limitData[] = $_POST["limitValue"];
		echo $limitData[] = $_POST["limitDuration"];
		echo $limitData[] = $date_time;
		echo $limitData[] = $_POST["applyDate"];
		 $limitData[] = $_POST["currency"];
		
		$limitObject-> insertLimit($limitData);
	}
	if($_POST["limitUserName"] != "")
	{
		$limitUserID = $_POST["limitUserName"];
		}
	
	$limitUser = selectFrom("select userID, username from ".TBL_ADMIN_USERS." where username = '".$limitUserID."' ");
		echo $limitUserID = $limitUser["userID"];
		$limitData = $limitObject-> selectLimit($limitUserID);                  
		
		$limitUserID = $limitData[1];
		$limitType = $limitData[2];
		$limitValue = $limitData[3];
		$limitDuration = $limitData[4];
		$limitUpdated  = $limitData[5];
		$limitApplyDate  = $limitData[6];
		$limitCurrency = $limitData[7];
          
}
	
?>
<html>
<head>
	<title>Limit Transactions</title>
<script language="JavaScript" src="./javascript/GCappearance2.js"></script>
<script language="JavaScript" src="./javascript/GurtCalendar.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="./styles/calendar2.css">
<script language="javascript" src="./styles/admin.js"></script>
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Admin Transaction Limit</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="limit_transactions.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0" colspan="2"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        
        <tr>
			<td>
				<input name="limitUserName" type="text" id="limitUserName" value=<?=$limitUserName?>>
				
			</td>
			<td>
				<input type="submit" name="Submit" value="Search">
			</td>
        </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
		<form action="limit_transactions.php" method="post" name="updationFrom">	          
			<tr>
				<td align="right">
					User Name 
				</td>
				<td>
					<?=$limitUser["username"]?>
					<input name="limitUserID" type="hidden" id="limitUserID" value=<?=$limitUser["userID"]?>>
				</td>
			</tr>
			<tr>
				<td align="right">
					User Limit
				</td>
				<td>
					<input name="limitValue" type="text" id="limitValue" value=<?=$limitValue?>>
				</td>
			</tr>
			<tr>
				<td align="right">
					Limit Type
				</td>
				<td>
					<select name="limitType">
						<option value="Transaction" <? if($limitType == "Transaction"){echo("selected");}?>>-- A Single Transaction --</option>
						<option value="Days" <? if($limitType == "Days"){echo("selected");}?>>-- Number of Days --</option>
					</select>
					
				</td>
			</tr>
			<tr>
				<td align="right">
					Limit Duration
				</td>
				<td>
					<input name="limitDuration" type="text" id="limitDuration" value=<?=$limitDuration?>>
					It is compulsory if limitType is  Number of Days.
				</td>
			</tr>
			<tr>
				<td align="right">
					Originating Currency
				</td>
				<td>
					<select name="currency">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								if($limitCurrency == "")
									echo "<option selected value ='$currency'> ".$currency." --> ".$description."</option>";			
								else
								    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.updationFrom.currency, "<?=$limitCurrency ?>");
     	  </script>
				</td>
			</tr>
			<tr>
				<td align="right">
					Date to Apply Limit
				</td>
				<td>
					<input name="applyDate" type="text" id="applyDate"  value="<? echo $limitApplyDate;?>">&nbsp;<!--a href="javascript:show_calendar('updationFrom.applyDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=34 height=20 border=0></a-->
				</td>
			</tr>
			
			
			<tr>
				<td align="right">
					Last Updated
				</td>
				<td>
					<?=$limitUpdated?>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="Submit" value="UpdateLimit">
				</td>
			</tr>
        </form>
      </table></td>
  </tr>
</table>
</body>
</html>