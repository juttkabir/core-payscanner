<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$paypointAgentID = $_POST["custAgentID"];
	$amount = $_POST["transAmount"];
	$currencyT = $_POST["currency"];
	$serviceIDV	= $_POST["serviceID"];
	$categoryIDV =	$_POST["categoryID"];
	$countryV =	$_POST["country"];
	
	/*
		#5544
		Dropdown for agents is displayed showing all Agents.
		Also ledger will be affected accordingly.
		by A.Shahid
	*/
	$showAgents = false;
	if(CONFIG_USE_PAYPOINT_AGENT_DROPDOWN=="1"){
		$showAgents = true;
	}
	if($_POST["action"] == "addBuySell" && $_POST["storeBuySellForm"]!="")
	{
		$serviceQ = selectFrom("SELECT serviceName FROM servicesPaypoint WHERE serviceID ='".$serviceIDV."'");
		$transType = $serviceQ["serviceName"];
		$refNumberRS = selectFrom("SELECT MAX(transID) as transIDVal FROM paypointTransactions");
		$refNumber	= $refNumberRS["transIDVal"]+1;
		$refNumber	= "PAY".date("Ymd")."-".$refNumber;
		$creationDate = date("Y-m-d H:i:s");
		 $sql = "INSERT INTO paypointTransactions (
									customerID, custAgentID,serviceID,categoryID, refNumber,transAmount,
									IMFee, totalAmount, addedBy, transDate,transType,agentComm,remarks,transStatus)
						VALUES   (  '".$_POST["customerID"]."','".$_POST["custAgentID"]."', 
						'".$_POST["serviceID"]."', '".$_POST["categoryID"]."', '$refNumber',
						'".$_POST["transAmount"]."','".$_POST["IMFee"]."', '".$_POST["totalAmount"]."','$username', '$creationDate',
						'".$transType."',
						'$agentCommi','".addslashes($_POST["remarks"])."','Pending')";
		if(insertInto($sql))
		{
			$transID = @mysql_insert_id();
			$lastTransIDq = selectFrom("SELECT LAST_INSERT_ID() as li");
			$transID = $lastTransIDq["li"];
			paypointSummaryAccount($agentID,$amount,$serviceIDV,$categoryIDV,"WITHDRAW");

			if($paypointAgentID!="" && $showAgents){
				$descript = "Paypoint Transaction Created";
				$tran_date = date("Y-m-d");
				$agentContents = selectFrom("SELECT adminType,isCorrespondent FROM ".TBL_ADMIN_USERS." WHERE userID='".$paypointAgentID."'");

				if($agentContents["adminType"] == 'Agent' && (($agentContents["isCorrespondent"] == 'Y' && CONFIG_AnD_ENABLE=="1") ||  $agentContents["isCorrespondent"] == 'N')){
					$ledgerCurrency = "GBP";
					if($currencyT!="")
						$ledgerCurrency = $currencyT;
					if($agentContents["isCorrespondent"] == 'Y')
						$ledgerTable = TBL_AnD_ACCOUNT;
					else
						$ledgerTable = TBL_DISTRIBUTOR_ACCOUNT;
					$insertQuery = "insert into ".$ledgerTable." (agentID, dated, type, amount, modified_by, TransID, description,currency,note) values('".$paypointAgentID."', '".$tran_date."', 'WITHDRAW', '".$amount."', '".$userID."', '".$refNumber."', '".$descript."','".$ledgerCurrency."','".addslashes($_POST["remarks"])."')";
					insertInto($insertQuery);//////Insert into Agent Account
					$lastLedgerIDq = selectFrom("SELECT LAST_INSERT_ID() as li");
					$ledgerID = $lastLedgerIDq["li"];
					if($agentContents["isCorrespondent"] == 'Y')
						update("UPDATE ".$ledgerTable." SET actAs='Agent' WHERE aaID='".$ledgerID."'");
						
					agentSummaryAccount($paypointAgentID, 'WITHDRAW', $amount,$ledgerCurrency);
					activities($_SESSION["loginHistoryID"],"INSERTION",$refNumber,$ledgerTable,$descript);
				}
				else{
					update("DELETE FROM paypointTransactions WHERE payID='".$transID."'");
					echo "E";
					exit;
				}
			}
			if($currencyT!=""){
				update("UPDATE paypointTransactions SET currency='".$currencyT."' WHERE payID='".$transID."'");
			}
			if($countryV!=""){
				update("UPDATE paypointTransactions SET country='".$countryV."' WHERE payID='".$transID."'");
			}
		}
		if(empty($transID) || $transID==""){
			echo "E";
			exit;
		}
		echo $transID;
		exit;
	}
	
?>