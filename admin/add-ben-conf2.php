<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
$userId = $_SESSION["customerID"];

switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}
//if ($_POST["benID"] == "")
	{
	session_register("Title");
	session_register("firstName");
	session_register("lastName");
	session_register("middleName");
	session_register("IDType");
	session_register("IDNumber");
	session_register("Address");
	session_register("Address1");
	session_register("Country");
	session_register("City");
	session_register("Zip");
	session_register("State");
	session_register("Phone");
	session_register("Mobile");
	session_register("benEmail");
	session_register("transType");
	
	session_register("bankName");
	session_register("branchCode");
	session_register("branchAddress");
	session_register("swiftCode");
	session_register("accNo");
	session_register("ABACPF");
	session_register("IBAN");
	

	$_SESSION["bankName"] = $_POST["bankName"];
	$_SESSION["branchCode"] = $_POST["branchCode"];
	$_SESSION["branchAddress"] = $_POST["branchAddress"];
	$_SESSION["swiftCode"] = $_POST["swiftCode"];
	$_SESSION["accNo"] = $_POST["accNo"];
	$_SESSION["ABACPF"] = $_POST["ABACPF"];
	$_SESSION["IBAN"] = $_POST["IBAN"];

	$_SESSION["Title"] = $_POST["Title"];
	$_SESSION["firstName"] = $_POST["firstName"];
	$_SESSION["lastName"] = $_POST["lastName"];
	$_SESSION["middleName"] = $_POST["middleName"];
	$_SESSION["IDType"] = $_POST["IDType"];
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
	$_SESSION["Address"] = $_POST["Address"];
	$_SESSION["Address1"] = $_POST["Address1"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["Zip"] = $_POST["Zip"];
	$_SESSION["State"] = $_POST["State"];
	$_SESSION["Phone"] = $_POST["Phone"];
	$_SESSION["Mobile"] = $_POST["Mobile"];
	$_SESSION["benEmail"] = $_POST["benEmail"];
	$_SESSION["customerID"] = $_POST["customerID"];
	$backURL = "add-benificiary2.php?msg=Y&from=" . $_GET["from"];
} 
/*else {
	$backURL = "update-beneficiary.php?benID=$_POST[benID]&msg=Y";
}*/

if (trim($_POST["firstName"]) == ""){
	insertError(BE1);
	redirect($backURL);
}
if (trim($_POST["lastName"]) == ""){
	insertError(BE2);
	redirect($backURL);
}
if ($_POST["IDNumber"] == ""){
	insertError(BE3);
	redirect($backURL);
}
if (trim($_POST["Address"]) == ""){
	insertError(BE4);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(BE5);
	redirect($backURL);
}
if (trim($_POST["City"]) == ""){
	insertError(BE6);
	redirect($backURL);
}
if (trim($_POST["Phone"]) == ""){
	insertError(BE7);
	redirect($backURL);
}
/*
if(!email_check(trim($_POST["benEmail"]))){
	insertError(BE8);
	redirect($backURL);
}
*/
if ($_POST["benID"] == ""){

$dDate = date("y-m-d");
if(trim($_POST["transType"]) != "Bank Transfer")
	$_POST["transType"] = "";
	$Querry_Sqls = "INSERT INTO ".TBL_CM_BENEFICIARY." (
													 Title,
													 firstName, 
													 middleName, 
													 lastName, 
													 Address, 
													 Address1, 
													 Country, 
													 City, 
													 Zip, 
													 State, 
													 Phone,
													 Mobile, 
													 email, 
													 IDType, 
													 NICNumber, 
													 customerID,
													 created,transType  													 
													 ) 
											   VALUES 
													(
													'".$_POST["Title"]."', 
													'".checkValues($_POST["firstName"])."', 
													'".checkValues($_POST["middleName"])."', 
													'".checkValues($_POST["lastName"])."', 
													'".checkValues($_POST["Address"])."', 
													'".checkValues($_POST["Address1"])."', 
													'".$_POST["Country"]."', 
													'".checkValues($_POST["City"])."', 
													'".checkValues($_POST["Zip"])."', 
													'".checkValues($_POST["State"])."', 
													'".checkValues($_POST["Phone"])."', 
													'".checkValues($_POST["Mobile"])."', 
													'".checkValues($_POST["benEmail"])."',
													'".$_POST["IDType"]."', 
													'".checkValues($_POST["IDNumber"])."', 
													$userId ,
													'$dDate','".checkValues($_POST["transType"])."'													
													)";
	insertInto($Querry_Sqls);
	$benID = @mysql_insert_id();
	
	////To record in History
	$descript ="Online Beneficiary is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$benID,TBL_CM_BENEFICIARY,$descript);
	insertError(BE10);

		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , benID   , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', $benID, $entryDate,'INSERTION',$userId)");		
		}	
		
			if(trim($_POST["transType"]) == "Bank Transfer")
			{
			
				$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			

				$sqlBank = "insert into cm_bankdetails
							( 
							benID, bankName, 
							accNo, branchCode, branchAddress,
							ABACPF , IBAN,swiftCode 
							)
							Values
							(
							'".$benID."',  '".$_POST["bankName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."'
							)
							";
				insertInto($sqlBank);
			}


	$_SESSION["Title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";

	insertError(BE9);
} else {

$entryDate = date("Y-m-d");
if(trim($_POST["transType"]) != "Bank Transfer")
	$_POST["transType"] = "";

	$Querry_Sqls = "update ".TBL_CM_BENEFICIARY." set Title='".$_POST["Title"]."',
	 firstName='".checkValues($_POST["firstName"])."', 
	 middleName='".checkValues($_POST["middleName"])."', 
	 lastName='".checkValues($_POST["lastName"])."', 
	 IDType='".$_POST["IDType"]."', 
	 NICNumber='".checkValues($_POST["IDNumber"])."',
	 Address='".checkValues($_POST["Address"])."', 
	 Address1='".checkValues($_POST["Address1"])."', 
	 Zip='".$_POST["Zip"]."', 
	 Country='".$_POST["Country"]."', 
	 City='".$_POST["City"]."', 
	 State='".$_POST["State"]."', 
	 Phone='".checkValues($_POST["Phone"])."', 
	 Mobile='".checkValues($_POST["Mobile"])."', 
	 email='".checkValues($_POST["benEmail"])."',
	 transType = '".checkValues($_POST["transType"])."'	,
	 editDate 	 = '".$entryDate."'	,
	 loginID  	 = '".$userId."'	
	 where benID='".$_POST["benID"]."'";
	update($Querry_Sqls);
////To record in History
$descript ="online beneficiary is updated";
		activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["benID"],TBL_CM_BENEFICIARY,$descript);
		if($_SESSION["agentID_id"] != "")
		{		
			$entryDate = date("Y-m-d");
			insertInto("insert into agent_logfile (agentID , benID   , entryDate , remarks,customerID ) 
			VALUES ('". $_SESSION["agentID_id"]."', '".$_POST["benID"]."', $entryDate,'UPDATION',$userId)");		
		}	
			if(trim($_POST["transType"]) == "Bank Transfer")
			{
			
				$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 			

				$sqlBank = "insert into cm_bankdetails
							( 
							benID, bankName, 
							accNo, branchCode, branchAddress,
							ABACPF , IBAN,swiftCode 
							)
							Values
							(
							'".$_POST["benID"]."',  '".$_POST["bankName"]."',
							'".$_POST["accNo"]."', '".$_POST["branchCode"]."', '".$_POST["branchAddress"]."',
							'".$_POST["ABACPF"]."',	'".$_POST["IBAN"]."',	'".$_POST["swiftCode"]."'
							)
							";
				insertInto($sqlBank);
			}
			else
			{
				$strQuery = "delete FROM cm_bankdetails where benID = '".$_POST["benID"]."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 	

				$_SESSION["bankName"] 		= "";
				$_SESSION["branchCode"] 	= "";
				$_SESSION["branchAddress"] 	= "";
				$_SESSION["swiftCode"] 		= "";
				$_SESSION["accNo"] 			= "";
				$_SESSION["ABACPF"] 		= "";
				$_SESSION["IBAN"] 			= "";
				$_SESSION["transType"] = "";
			}

	insertError(BE10);



	$_SESSION["Title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";
}
if($_GET["from"] == "popUp")
{
	$_SESSION["benID"] = $benID;
	?>
	<script language="javascript">
		opener.document.location = "add-customer-transaction.php?msg=Y";
		window.close();
	</script>
	<?
}
else
{
	redirect($backURL);
}
?>