<?php
// Session Handling
session_start();
include ("../include/config.php");
include ("security.php");

$IDType   	= $_POST["IDType"];
$userType 	= $_POST["userType"];
$alertType 	= $_POST["alertType"];
$day 		= $_POST["day"];
$emailText 	= $_POST["emailText"];
$isEnable 	= $_POST["isEnable"];
$alertID 	= $_GET["alertID"];

if(!empty($alertID))
{
	$action = "update";
}
else
{	$action = "add";
	
}
 
if ($action == "add")
{
	$backURL = "AlertController.php?msg1=Y";
	$existingReportQuery = " SELECT 
								* 
							FROM 
								".TBL_EMAIL_ALERT." 
							WHERE 
								id_type_id 	= '".$IDType."' 	AND 
								userType 	= '".$userType."' 	AND 
								alertType 	= '".$alertType."'
							";
	$existingReport	= selectFrom($existingReportQuery);
	$existingRId	= $existingReport["id"];		
	if(!empty($existingRId))
	{
		$backURL .= "&dup=Y&IDType=".$IDType."&userType=".$userType."&alertType=".$alertType."&day=".$day."&isEnable=".$isEnable."&emailText=".$emailText;
		redirect($backURL);	
	}
	
	$reportArr = selectFrom("SELECT id, title FROM  id_types WHERE id=".$IDType);
	$report	= $reportArr["title"];
	if($IDType == -1)
		$report = "All";
	
	$Querry_Sqls = "INSERT INTO 
						".TBL_EMAIL_ALERT." 
						(	
							id_type_id,
							report, 
							day, 
							message, 
							isEnable, 
							client, 
							userType,
							alertType
						) 
						VALUES  
						(	
							'".$IDType."',
							'".$report."', 
							'".$day."',
							'".$emailText."', 
							'".$isEnable."', 
							'".CLIENT_NAME."', 
							'".$userType."',
							'".$alertType."'
						)"; 
	insertInto($Querry_Sqls);
	$alertID = mysql_insert_id();		
	////To record in History
	$descript = "Alert added";
	$backURL .= "&success=Y&act=$action";
	activities($_SESSION["loginHistoryID"],"INSERTION",$alertID,TBL_EMAIL_ALERT,$descript);	
}
redirect($backURL); 
?>