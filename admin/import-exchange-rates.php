<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$flag="false";

/**
 * The new logic was implemented to cater the import of intermediate exchange rates
 * with the stand-alone rates. The stand-alone rates logic was working originally,
 * and for intermediate rates, we have added a new field to excel file (Group ID)
 * to identify the intermediate rates group (A.K.A. the path).
 *
 * For this new logic we added another (middle) page to show the data from excel file
 * before import, so that a user can select what to really import. Then the data is posted
 * back to this page for import in the database.
 */
if($_REQUEST["command"] == "IMPORT")
{
	/**
	 * Import stand alone rates if found.
	 */
	//debug($_SESSION["standAlone"]);
	if(isset($_SESSION["standAlone"]))
	{
		$standAlone = $_SESSION["standAlone"];
		
		
		if(CONFIG_24HR_BANKING == "1")
		{

			for($i=0; $i<sizeof($_SESSION["standAlone"]["orgCurrency"]); $i++)
			{
				
				$sql = "INSERT INTO " . TBL_EXCHANGE_RATES . " 
									(dated, countryOrigin, currencyOrigin, country, currency, rateFor, 
									rateValue, primaryExchange, marginPercentage, agentMarginType, agentMarginValue, updationDate, 
									_24hrMarginType, _24hrMarginValue)
								VALUES 
									('".$standAlone["date"][$i]."', '".$standAlone["orgCountry"][$i]."', 
									 '".$standAlone["orgCurrency"][$i]."', '".$standAlone["destCountry"][$i]."', 
									 '".$standAlone["destCurrency"][$i]."', '".$standAlone["exrateType"][$i]."', 
									 '".$standAlone["userID"][$i]."', '".$standAlone["exrate"][$i]."', 
									 '".$standAlone["margin"][$i]."', '".$standAlone["agentMarginType"][$i]."', 
									 '".$standAlone["agentMargin"][$i]."', '".$standAlone["date"][$i]." ".date("H:i:s")."',
									 '".$standAlone["24hrMarginType"][$i]."','".$standAlone["24hrMarginValue"][$i]."')";
				//debug($sql);
				if (insertInto($sql))
				{
					$flag="true";
					$msg = SUCCESS_MARK . " Data is imported successfully";
				} else {
					$msg = CAUTION_MARK . " Data is not imported";	
				}
			}
		}
		else
		{
			for($i=0; $i<sizeof($_SESSION["standAlone"]["orgCurrency"]); $i++)
			{
				$sql = "INSERT INTO " . TBL_EXCHANGE_RATES . " 
									(dated, countryOrigin, currencyOrigin, country, currency, rateFor, 
									rateValue, primaryExchange, marginPercentage, agentMarginType, agentMarginValue, updationDate)
								VALUES 
									('".$standAlone["date"][$i]."', '".$standAlone["orgCountry"][$i]."', 
									 '".$standAlone["orgCurrency"][$i]."', '".$standAlone["destCountry"][$i]."', 
									 '".$standAlone["destCurrency"][$i]."', '".$standAlone["exrateType"][$i]."', 
									 '".$standAlone["userID"][$i]."', '".$standAlone["exrate"][$i]."', 
									 '".$standAlone["margin"][$i]."', '".$standAlone["agentMarginType"][$i]."', 
									 '".$standAlone["agentMargin"][$i]."', '".$standAlone["date"][$i]." ".date("H:i:s")."')";
				//debug($sql);
				if (insertInto($sql))
				{
					$flag="true";
					$msg = SUCCESS_MARK . " Data is imported successfully";
				} else {
					$msg = CAUTION_MARK . " Data is not imported";	
				}
			}
		}
		
		unset($_SESSION["standAlone"]);
	}
	
	/**
	 * Import intermediate rates if found.
	 *
	 * NOTES (Logical flow of importing intermediate exchange rates.):
	 *	1. insert into related exchange rates table
	 *	2. insert into exchange rates table
	 *	   2.1 link with exchange rates table
	 *	   2.2 calculate exchange rate for master record which is linked with related exchange rates
	 *	       table. 
	 *	   2.3 update related exchange rates table also with id of master record (erid)
	 *	3. insert into historical exchange rates table
	 **/
	
	if(isset($_SESSION["groupedRates"]))
	{
	 	foreach($_SESSION["groupedRates"]  as $key => $group)
	 	{
	 		$isMasterRecord = false;
			$agentMarginCalcType = "";
	 		$calculatedAmount = 1;
			$calculteNetExchange = 1;
			$AgentCalculteNetExchange = 1;
			$AgentCalculatedMarginPercentage = 0;
	 		
 			for($i=0; $i<sizeof($group["orgCurrency"]); $i++)
 			{
 				if($i == 0)
 				{
	 				// inserting master record into related exchange rates table
			 		$sql = "insert into ".TBL_RELATED_EXCHANGE_RATES." 
										(DefaultSendingCurrency,defaultRecievingCurrency,defaultSendingCountry,defaultRecievingCountry) 
									values 
										('".$group["orgCurrency"][$i]."', '".$group["destCurrency"][$i]."', 
										 '".$group["orgCountry"][$i]."', '".$group["destCountry"][$i]."')";
					insertInto($sql);
					//debug($sql);
					/**
					 * CAUTION: If your AUTO_INCREMENT column has a column type of BIGINT, 
					 * the value returned by mysql_insert_id() will be incorrect. Instead, use the internal MySQL 
					 * SQL function LAST_INSERT_ID() in an SQL query. [Reference PHP Manual]
					 *
					 * So as a matter of fact, the table is using bigint type for auto-increment field so the PHP's
					 * function @mysql_insert_id() won't work, that's why firing a query to fetch last insert id from
					 * mysql's LAST_INSERT_ID() function.
					 */
					//$relatedRateId = @mysql_insert_id();
					$result = selectFrom("select LAST_INSERT_ID() as lastId");
					$relatedRateId = $result["lastId"];
					unset($result);
					
					$isMasterRecord = true;
				}
				
				if($i > 0)
				{
					/**
					 * Calculations - calculating for records other than master one.
					 *
					 * $tmpAgentMarginValue = calculateRate[$j]["agentMarginValue"]; 
					 * Below line of code should be equal to this (as per existing logic) but as per logic in add-rate-conf.php, 
					 * its provided by user input via the form. So here from import file it doesn't exist, so is set to zero.
					 */
					$tmpAgentMarginValue = $group["agentMargin"][$i];
					$calculteNetExchange2 = 0;
					$calculatedAmount = $calculatedAmount * $group["exrate"][$i];
					$calculatedAmount2 = $calculatedAmount;
					$calculteNetExchange2 = ($group["exrate"][$i]-($group["exrate"][$i] * $group["margin"][$i])/100);
					$calculteNetExchange = $calculteNetExchange * $calculteNetExchange2;
					
					// Maintaining the state for agentMarginType to later update in master record.
					if(empty($agentMarginCalcType))
					{
						$agentMarginCalcType = $group["agentMarginType"][$i];
					}
					
					//switch(strtolower($group["agentMarginType"][$i]))
					switch(strtolower($agentMarginCalcType))
					{
						case "percent":
							$AgentCalculteNetExchange = $AgentCalculteNetExchange *(($calculteNetExchange2 -($calculteNetExchange2 * $tmpAgentMarginValue)/100));
							break;
							
						case "fixed":
							//$AgentCalculteNetExchange = $AgentCalculteNetExchange *(($calculteNetExchange2 -($calculteNetExchange2 * $tmpAgentMarginValue)));
							$AgentCalculteNetExchange = $AgentCalculteNetExchange * ($calculteNetExchange2 - $tmpAgentMarginValue);
							break;
					}
				}
				
				// inserting into exchange rates table
				$sql = "insert into ".TBL_EXCHANGE_RATES." 
													(countryOrigin, country, currency, primaryExchange, marginPercentage, 
													 dated, rateFor, rateValue, updationDate, currencyOrigin, marginType,
													 agentMarginType, agentMarginValue,relatedID) 
												values 
													('".$group["orgCountry"][$i]."', '".$group["destCountry"][$i]."', 
													 '".$group["destCurrency"][$i]."', '".$group["exrate"][$i]."', 
													 '".$group["margin"][$i]."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 
													 '".$group["exrateType"][$i]."', '".$group["userID"][$i]."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 
													 '".$group["orgCurrency"][$i]."', '', 
													 '".$group["agentMarginType"][$i]."', '".$group["agentMargin"][$i]."',
													 '".$relatedRateId."')";
			
				insertInto($sql);
				//debug($sql);
				if($i == 0)
				{
					// linking exchange rate master record to related exchange rate record
					//$ratId = @mysql_insert_id();
					$result = selectFrom("select LAST_INSERT_ID() as lastId");
					$ratId = $result["lastId"];
					unset($result);
					$sql = "update ".TBL_RELATED_EXCHANGE_RATES." set erID='".$ratId."' where id='".$relatedRateId."'";
					update($sql);
				}
				
				// inserting historical data
				$sql = "insert into  historicalExchangeRate 
									(country, primaryExchange, dated, currency, marginPercentage, rateFor, 
									 rateValue, updationDate, countryOrigin, currencyOrigin, marginType,
									 agentMarginType, agentMarginValue, relatedID, erID) 
								values 
									('".$group["destCountry"][$i]."', '".$group["exrate"][$i]."', 
									 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$group["destCurrency"][$i]."',
									 '".$group["margin"][$i]."', '".$group["exrateType"][$i]."', '".$group["userID"][$i]."', 
									 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$group["orgCountry"][$i]."', 
									 '".$group["orgCurrency"][$i]."', '".$group["agentMarginType"][$i]."', '".$group["agentMargin"][$i]."', 
									 '','".$relatedRateId."','".$ratId."')";
				insertInto($sql);
				//debug($sql);
			}
			
			if($isMasterRecord)
			{
				//update master record for the primary exchange rate.
				$calculatedMargin = $calculatedAmount - $calculteNetExchange;
				$AgentCalculatedMargin =  $calculteNetExchange - $AgentCalculteNetExchange;
				$calculatedMarginPercentage = ($calculatedMargin * 100)/$calculatedAmount;
				
				//switch(strtolower($group["agentMarginType"][0]))
				switch(strtolower($agentMarginCalcType))
				{
					case "percent":
						$AgentCalculatedMarginPercentage = ($AgentCalculatedMargin * 100)/$AgentCalculteNetExchange;
						break;
						
					case "fixed":
						//$AgentCalculatedMarginPercentage = $AgentCalculatedMargin / $AgentCalculteNetExchange;
						$AgentCalculatedMarginPercentage = $AgentCalculatedMargin;
						break;
				}
				
			  	$sql = "update 
			  					".TBL_EXCHANGE_RATES." 
								set 
									primaryExchange = '".$calculatedAmount."', 
									marginPercentage = '".$calculatedMarginPercentage."', 
									updationDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', 
									agentMarginType = '".$agentMarginCalcType."', 
									agentMarginValue = '".$AgentCalculatedMarginPercentage."'
								where
									relatedID = '".$relatedRateId."' and erID='".$ratId."'";	//$group["agentMarginType"][0]
				update($sql);
				//debug($sql);
			}
		}
		
		unset($_SESSION["groupedRates"]);
	}
}
?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF" size="2">Import Exchange Rates </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="700"  border="0">
  <tr>
    <td width="259" valign="top"><fieldset>
    <legend class="style2">Import Exchange Rates </legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
				<!-- import-exchange-rates.php -->
			<form name="addRate" action="import-exchange-rates-middle-page.php" method="post" enctype="multipart/form-data">
				<?	if($msg!= "") {  ?>
			  <tr align="center">
			  	<?   if ($flag=="true") { ?>
			  	<td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg ?></font></b><br></td>
			  	<? } else { ?>
			    <td colspan="2"><b><font color= <? echo CAUTION_COLOR ?> ><? echo $msg ?></font></b><br></td>
			    <? } ?>
			    </tr>
			  <?	}  ?>
			  <tr align="center" class="tab-r">
			    <td colspan="2">Please follow the instructions on the Readme.txt file.</td>
			    </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="#" onClick="javascript:window.open('import-readme.php?page=exchrate', 'ReadMe', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=250,width=650,top=200,left=170')" class="style2">
				Readme.txt
			  </a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="<?=(CONFIG_24HR_BANKING == "1"?"xls/sampleTemplate-exchange-rate-MinasCenter.xls":"xls/sampleTemplate-exchange-rate-1.xls")?>" class="style2">Sample Template</a>	
				</td>
			  </tr>
			   <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Import </td>
			    <td width="75%" bgcolor="#DFE6EA">
			    	<input name="importFile" type="file" id="importFile" size="15">
			    </td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input type="submit" name="Submit" class="flat" value="Import"></td>
		      </tr></form>
			</table>
		    <br>
        </fieldset></td>
    <td width="431" valign="top">
	<?	if ($str != "")	{  ?>
	<fieldset>
    <legend class="style2">Import Exchange Rates Results </legend>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
        <tr>
          <td valign="top" bgcolor="#DFE6EA"><? echo $str ?></td>
        </tr>
    </table>
    <br>
    </fieldset>
	<?	}  ?>
	</td>
  </tr>
</table>
	</td>
  </tr>
</table>
</body>
</html>