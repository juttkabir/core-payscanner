<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
$transType = "Bank Transfer";
$exportType = $_REQUEST["exportType"];
if($exportType!=""){
	$filters .= " (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";
	$filters .= " AND transType='$transType'";
	if($agentName!="" && $agentName!="all")	
		$filters .= " AND custAgentID='$agentName' ";
	$sql = "select 
					tbd.bankName, 
					tbd.branchAddress,
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.custAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben,
					admin ad
				where
					tbd.transID = tr.transID and
					tr.benID = ben.benID and
					tr.custAgentID = ad.userID and
					". $filters ." 
				order by 
					tbd.bankName asc
					";
}
$amountUnderOneBank = 0;
$totalAmount = 0;
$totalTrasactions = 0;

if(!empty($sql))
	$fullRS = SelectMultiRecords($sql);

	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=bankTransactionsMT-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{

		header("Content-Disposition: attachment; filename=bankTransactionsMT-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Bank Transactions</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."DATE ".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."TRANS REF #".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."AGENT CODE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BENEFICIARY".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BANK NAME".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."BANK ADDRESS".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."SORT CODE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."ACCOUNT #".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."VALUE".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	for($s=0;$s<count($fullRS);$s++)
	{

		$agentCodeQuery = selectFrom("Select username from admin where userID = '".$fullRS[$s]["custAgentID"]."'");
		$agentCode		= strtoupper($agentCodeQuery["username"]);


		if($exportType == "csv")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$fullRS[$s]["status"]."'>";

		$tids .= $fullRS[$s]["transID"].",";
		
		$lastBankName = $fullRS[$s-1]["bankName"];
		$nextBankName = $fullRS[$s+1]["bankName"];
		$totalAmount += $fullRS[$s]["localAmount"];
		$totalTrasactions += 0;
		if((!empty($lastBankName) && $lastBankName != $fullRS[$s]["bankName"]) || $s == 0)
		{
			$noOfOrders = 0;
			$amountUnderOneBank = 0;
			
			$strFullHtml .= $strColumnStart.$fullRS[$s]["bankName"].$strColumnEnd;
			$strFullHtml .= $strRowEnd;
		}
		$noOfOrders += 1;
		$amountUnderOneBank += $fullRS[$s]["localAmount"];
			
		$strFullHtml .= $strColumnStart.$fullRS[$s]["transDate"].($exportType=="csv"?"         ":"").$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["refNumberIM"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$agentCode.$strColumnEnd;

		$strFullHtml .= $strColumnStart.$fullRS[$s]["firstName"]." ".$fullRS[$s]["middleName"]." ".$fullRS[$s]["lastName"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["bankName"])? $fullRS[$s]["bankName"] : "" ).$strColumnEnd;

		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["branchAddress"])? $fullRS[$s]["branchAddress"] : "" ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.(!empty($fullRS[$s]["branchCode"])? $fullRS[$s]["branchCode"] : "" ).$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["accNo"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$fullRS[$s]["localAmount"].$strColumnEnd;
		$strFullHtml .= $strRowEnd;

		if((!empty($nextBankName) && $nextBankName != $fullRS[$s]["bankName"]) || $s == count($fullRS)-1)
		{
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Number of Transactions:".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$noOfOrders.$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."Total Bank".$strColumnEnd;
			$strFullHtml .= $strColumnStart.($exportType!="csv"? number_format($amountUnderOneBank,2,".",","):number_format($amountUnderOneBank,2,".","")).$strColumnEnd;
			$strFullHtml .= $strRowEnd;
		}
		
	}
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strRowEnd;


			$strFullHtml .= $strColumnStart."Number of Transactions:".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$s.$strColumnEnd;
			$strFullHtml .= $strColumnStart."Foreign Total".$strColumnEnd;
			$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalAmount,2,".",",") : number_format($totalAmount,2,".","")).$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strColumnStart."".$strColumnEnd;
			$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>