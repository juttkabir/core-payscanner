<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$currContents= selectFrom(" select * from curr_exchange where id = '".$_REQUEST['id']."'");		
		$buysellCurrency=$currContents['buysellCurrency'];
		$buying_rate=$currContents['buying_rate'];
		$selling_rate=$currContents['selling_rate'];
	//debug($currContents, true);
	$buttontext="Update";
	$currHeading = "Update Money Exchange Rate";
	}
	else{	 
	 $buttontext="Add";
	 $currHeading = "Create Money Exchange Rate";
	 }	
	//debug($buysellCurrency); 
	//$currHeading = "Create Money Exchange Rate";
	$currDefault = "GBP";
	$currExchFormURL = "add-currency-exchange-conf.php";
	$strQuery = "select cID,currencyName,country,count(currencyName) as inter_curr  from currencies group by currencyName";
	$currDataArr = selectMultiRecords($strQuery);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Currency Excahge Rate</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script>
	$(document).ready(function(){

		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		});
		$('img').cluetip({splitTitle:'|'});
		
		
		/* vaildations of the fields start */

		$("#currExchForm").validate({
			rules: {
				operationCurrency: {
					required: true
				},
				buysellCurrency: {
					required: true
				},
				buying_rate: {
					required: true,
					number: true,
					min   :0.0000001
				},
				selling_rate: {
					required: true,
					number: true,
					min   :0.0000001
				},
			},
			messages: {
				operationCurrency: "Select the operational currency&nbsp;",
				buysellCurrency: "Select the operational currency&nbsp;",
				buying_rate: {
					required: "Please enter value of buying rate",
					number: "Please provide the valid amount.",
					min: "Enter amount sould be greater than 0."
				},
				selling_rate: {
					required: "Please enter value of selling rate",
					number: "Please provide the valid amount.",
					min: "Enter amount sould be greater than 0."
				},
			},
			submitHandler: function(form) {
				
				$("#storeData").attr("disabled",true);
				$("#currExchForm").ajaxSubmit(function(data) { 
					if(data != "E")
					{
						alert(data);
						//$("#currExchForm").clearForm();
						$("#buying_rate").val("");
						$("#selling_rate").val("");
						//window.open ("/admin/chequeReceipt.php?oi="+data,"CustomerReceipt","location=0,scrollbars=1,width=300,height=500"); 
					}
					else
						alert("The record could not be saved due to error, Please try again."); 
				});
				
				$('#storeData').removeAttr("disabled");
		   }
		});
		
		/* Validation ends */
	});

</script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form name="currExchForm" id="currExchForm" action="<?=$currExchFormURL?>" method="post">
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<h2><?=$currHeading?></h2>
				</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields marked with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		
		<tr bgcolor="#ededed">
			<td width="100%" colspan="4" align="left">
					<table align="left" width="100%" cellspacing="5">
						<tr bgcolor="#ededed">
							<td width="37%" class="tdDefination"><b>Operational Currency:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="63%">
								<select name="operationCurrency" id="operationCurrency" style="width:150px" disabled="disabled">
									<option value="">Select Currency</option>
									<?
										foreach($currDataArr as $curKey => $curVal)
										{
											$currSelectedOption = '';
											$showBold = "style='font-weight:bold;'";
											if($currDefault == strtoupper($curVal["currencyName"])){
												$currSelectedOption = 'selected="selected"';
												$selectedCID = $curVal["cID"];
											}
											if($curVal["inter_curr"]>1)
												$countryName = "(INTERNATIONAL)";
											else
												$countryName = "(".strtoupper($curVal["country"]).")";
									?>
										<option value="<?=$curVal["cID"]?>" <?=$currSelectedOption?> <?=showBold?> >
											<?=strtoupper($curVal["currencyName"])//." for ".$countryName?>
										</option>
									<? } ?>
								</select>
								<input type="hidden" name="operationCurrency" id="operationCurrency" value="<?=$selectedCID?>">
								&nbsp;
							<img src="images/info.gif" title="Note about Currency Selection | This currency is used for Operation (to buy and sell other currencies)." />
							</td>
						</tr>
						<tr bgcolor="#ededed">
							<td class="tdDefination"><b>Buying/Selling Currency:&nbsp;</b><font color="red">*</font></td>
							<td align="left"><select name="buysellCurrency" id="buysellCurrency" style="width:150px">
									<option value="">Select Currency</option>
									<?
										foreach($currDataArr as $curKey => $curVal)
										{
											$currSelectedOption = 'selected="selected"';
											$showBold 			= '';
/*											if($currDefault == strtoupper($curVal["currencyName"]))
												$currSelectedOption = 'selected="selected"';*/
											if($curVal["inter_curr"]>1){
												$countryName = "(INTERNATIONAL)";
												$showBold = "style='font-weight:bold;'";
											}
											else{
												if($curVal["currencyName"] == "PKR" || $curVal["currencyName"] == "PLN")
													$showBold = "style='font-weight:bold;'";
												$countryName = "(".strtoupper($curVal["country"]).")";
											}
											if($curVal["currencyName"] != "GBP"){
								if(isset($buysellCurrency) && !empty($buysellCurrency) && $buysellCurrency==$curVal['cID'])
								{
								?>
								<option value="<?=$curVal["cID"]?>" <?=$currSelectedOption?> <?=$showBold?> >
								<?=strtoupper($curVal["currencyName"])//." for ".$countryName?>
								</option>
								<? }
								else {?>
									<option value="<?=$curVal["cID"]?>"><?=strtoupper($curVal['currencyName'])?></option>
							<?		}
								
								}
									} ?>
								</select>
								&nbsp; <img src="images/info.gif" alt="s" title="Note about Currency Selection | This currency is bought or sold while Company will make or recieve payments in GBP. Currencies for multiple countries are also displayed Bold." /> </td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="37%" class="tdDefination"><b>Buying Rate:&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="63%"><input type="text" name="buying_rate" id="buying_rate" size="20" maxlength="150" value="<?php echo $buying_rate; ?>"  title="Buying Rate | This is latest Buying rate of selected currency." /></td>
						</tr>
						<tr bgcolor="#ededed">
							<td class="tdDefination"><b>Selling Rate :&nbsp;</b><font color="red">*</font></td>
							<td align="left"><input type="text" name="selling_rate" id="selling_rate" size="20" maxlength="150" value="<?php echo $selling_rate; ?>"  title="Selling Rate | This is latest Selling rate of selected currency." /></td>
						</tr>

					</table>
			</td>
		</tr>		

		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<?php $id=$_GET['id'];
				if(!empty($id)){
				
				 ?>				
				<input type="hidden" name="id" value="<?=$id;?>" />
				<?php }?>
				<input type="submit" id="storeData" name="storeData" value="<?php echo $buttontext; ?> Currency Rate" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="action" value="<?php echo $buttontext; ?>" />
			</td>
		</tr>
	</table>
</form>
</body>
</html>