<?
	session_start();
	
	
	include ("../include/config.php");
	include ("security.php");
	if ($_REQUEST["transNumber"]!="")
	{	
		$transNumber = htmlspecialchars($_REQUEST["transNumber"],ENT_QUOTES);	
		
	}
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$created = date('Y-m-d  H:i:s');
	$createdBy = $_SESSION["loggedUserData"]["userID"];

	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	//TT Transaction status array
	//$status_arr=array("1"=>"Pending","2"=>"Hold","3"=>"Cancelled");
	$status_arr=array("TN"=>"Pending","TC"=>"Cancelled", "TD"=>"Deleted");
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";


	if(!empty($_REQUEST["btnCancel"]))
	{
		$transIDArray		=	$_REQUEST["transIDs"];
		$status_new         =	'TC';		
		$type = 'FT';
		for ($d=0;$d < count($transIDArray);$d++)
		{
			if(isset($transIDArray[$d]))
			{
				
				$TransFTid	=	$type."-".$transIDArray[$d];
				//debug($TransFTid,true);
					
					$TransData = selectFrom("select * from ".TBL_ACCOUNT_DETAILS." where transID = '".$TransFTid."'");
					//$TransData = selectFrom("select * from account_ledgers where transID  = '".$TransFTid."'");
					if($TransData["status"]!="TC" )//&& $TransData["status"]!="2")
					{
						//debug ($TransData["status"]);
						update("update ".TBL_ACCOUNT_DETAILS." set status = '".$status_new."' where transID = '".$TransFTid."'");
						$err = "Transfer Cancelled successfully.";
						
						
						//Updating ledger
											
						
						$args = array('flag'=>'getTransID','type'=>$type,'CancelTrans'=>$transIDArray[$d]);
					
						$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);
										
						if(!empty($returnData['transID'])){
							$transID = $returnData['transID'];
							$transType = $type;
						}
						$arguments = array(
											'flagTradingLedger'=>true,
											'transID'=>$transID,
											'amount'=>$TransData["amountBuy"], //ramount
											'ramount'=>$TransData["amountSell"], //amount
										
											'sendingAc'=>$TransData["accountBuy"], //receivingAc
											'receivingAc'=>$TransData["accountSell"], //sendingAc
											
											'sendingCurrency'=>$TransData["currencyBuy"], //receivingCurrency
											'receivingCurrency'=>$TransData["currencySell"], //sendingCurrency
											
											'createdBy'=>$createdBy,
											'created'=>$created,
											'extraFields'=>array('transType'=>$transType),
											'status'=>'C');
					//	debug($arguments,true);
						//exit;
						$acountLF = accountLedger($arguments);
						/*echo '<script>
							
									alert("Transaction Successfully Cancelled");
								</script>';	*/
					}
					else
					{
							/*echo '<script>
							
									alert("Transaction Can not be cancelled");
								</script>';	*/
					}
				}
			
		}
	
		if(trim($err) == "")
			$err = "Error occured while performing this action";
		// Number of iteration should be equal to the number of  transactions
			echo "<input type='hidden' name='payListCancel' id='payListCancel' value='".$err."'>";
			exit;
	}
	
/*	if(!empty($_REQUEST["btnUnHold"])){
		$transIDArray		= $_REQUEST["transIDs"];
		$status_new         =1;
		for ($d=0;$d < count($transIDArray);$d++)
		{
			if(isset($transIDArray[$d]))
			{
				
				$TransData = selectFrom("select * from ".TBL_TT_TRANSACTIONS." where id = '".$transIDArray[$d]."'");
				if($TransData["status"]=="2")
				{
					update("update ".TBL_TT_TRANSACTIONS." set status = '".$status_new."' where id = '".$transIDArray[$d]."'");
					//$err = "Commission Cancel successfully.";
					
					//Updating Ledger
					
					$type = 'TT';
					$args = array('flag'=>'getTransID','type'=>$type,'CancelTrans'=>$transIDArray[$d]); //sending CancelTrans parameter so that ledger of this specific transaction is updated
					$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
					if(!empty($returnData['transID'])){
						$transID = $returnData['transID'];
						$transType = $type;
					}
					
					if($TransData["sending_currency"] != $TransData["receiving_currency"])
						$noteToInsert = "( Exchange Rate: 1 ".$TransData["sending_currency"]." = ".$TransData["exchange_rate"]." ".$TransData["receiving_currency"]." ) ";
					$arguments = array('flagTradingLedger'=>true,'transID'=>$transID,'amount'=>$TransData["sending_amount"],'ramount'=>$TransData["receiving_amount"],'sendingAc'=>$TransData["sender_account_number"],'receivingAc'=>$TransData["receiving_account_number"],'sendingCurrency'=>$TransData["sending_currency"],'receivingCurrency'=>$TransData["receiving_currency"],'note'=>addslashes($noteToInsert),'createdBy'=>$createdBy,'created'=>$created,'extraFields'=>array('transType'=>$transType));
					$acountLF = accountLedger($arguments);
					
					//echo '<script>
							
					//		alert("Transaction Unhold Successfully");
					//	</script>';	
				
				}
				else
				{
					//echo '<script>
							
						//			alert("Transaction can not be unHold !!! Only Hold Transaction can be Unhold");
						//		</script>';	
				}
				
			}
		}
	
		if(trim($err) == "")
			$err = "Error occured while performing this action";
		  // Number of iteration should be equal to the number of  transactions 
			echo "<input type='hidden' name='payListHoldData' id='payListHoldData' value='".$err."'>";
			exit;
	} 
	*/
	


if($_REQUEST["Submit"] == "SearchTrans")
{

	if ($_REQUEST["from"]!="")
		$from = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$to = $_REQUEST["to"];
	/*if ($_REQUEST["transNumber"]!="")
	{	//$transNumber = htmlspecialchars($_REQUEST["transNumber"],ENT_QUOTES);
		//$transNumber=str_replace("#","", $transNumber);	
		//$transNumber=preg_replace("/[^A-Za-z0-9]/","",$transNumber);
		$transNumber=strrpos("#", "b");
		debug($transNumber,true);
	}*/
	if ($_REQUEST["SenderID"]!="")
		$SenderID = $_REQUEST["SenderID"];
	if(!empty($_REQUEST["RecvAcc"]))
		$RecvAcc  = $_REQUEST["RecvAcc"];
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];
	if ($_REQUEST["RecvcurrencyTrans"]!="")
		$RecvcurrencyTrans = $_REQUEST["RecvcurrencyTrans"];
	
	if ($from != "")
	{
		$dDateF = explode("/",$from);
		if(count($dDateF) == 3)
			$from = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($to != "")
	{
		$dDate = explode("/",$to);
		if(count($dDate) == 3)
			$to = $dDate[2]."-".$dDate[1]."-".$dDate[0];
	}
	if ($from != "" || $to != "")
	{
		if ($from != "" && $to != "")
		{
			$queryDated = " and (accDet.created >= '$from 00:00:00' and accDet.created <= '$to 23:59:59') ";	
		}
		elseif ($from != "")
		{
			$queryDated = " and (accDet.created >= '$from 00:00:00') ";	
		}
		elseif ($to != "")
		{
			$queryDated = " and (accDet.created <= '$to 23:59:59') ";	
		}	
	}
	if ($queryDated != "")
	{
		$queryClause .= $queryDated;
	}
	
	if (!empty($transNumber))
	{
		$queryClause .= " AND accDet.transID = '$transNumber' ";
		
	}
	
	if (!empty($SenderID))
	{
		$queryClause .= " AND accDet.accountSell = '$SenderID' ";
	}
	if(!empty($RecvAcc))
	
		$queryClause .= " AND accDet.accountBuy = '$RecvAcc' ";
		
	if (!empty($currencyT))
	{
	
		$queryClause .= " AND accDet.currencySell = '$currencyT' ";
	}
	
	if(!empty($RecvcurrencyTrans))
	{
		$queryClause .= " AND accDet.currencyBuy = '$RecvcurrencyTrans' ";

	}
}

$extraQuery = "";




if($_REQUEST["getGrid"] == "intComReport"){

		$queryCom = "SELECT 					
						accLed.crAccount AS sendingAccount,
						SUM(accLed.crAmount) AS totTransAm,
						count(accLed.id) AS cntTrans,
						accDet.currencySell AS currency
					FROM 
						".TBL_ACCOUNT_LEDGERS." AS accLed,".TBL_ACCOUNT_DETAILS." AS accDet
					WHERE
						accLed.transType='FT' AND accLed.transID= accDet.transID AND accLed.status != 'C'";							
					
					
	if(!empty($queryClause))
		$queryCom .= $queryClause;

	if(!empty($extraQuery))
		$queryCom .= $extraQuery;
	//$queryCom .= " GROUP BY cust.customerID, tr.sending_currency ";
	$queryCom .= " GROUP BY crAccount,currency";
	//echo $queryCom;
	//exit;
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($resultCom);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryCom .= " order by $sidx $sord LIMIT $start , $limit";
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_error());

	//echo $queryCom;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultCom))
	{	
		$transAm = number_format($row["totTransAm"],2,'.',',');
				
		$response->rows[$i]['id'] = $row["sendingAccount"].'_'.$row["currency"];

		$response->rows[$i]['cell'] = array( 
										$row["sendingAccount"] , 
										$transAm, 
										$row["currency"],
										$row["cntTrans"]
									);
		$i++;
	}
	
	echo $response->encode($response); 
}	

if($_REQUEST["getGrid"] == "intComReportSubGrid"){
	$ridArr = explode('_',$_REQUEST["rid"]);
	$sendingAccount = $ridArr[0];
	$currFrom = $ridArr[1];
	//echo $queryComSub;
					
		$queryComSub = "SELECT 
							accDet.transID AS transID,
							accDet.acid as id,
							accDet.accountBuy AS receiving_account,
							accDet.created AS transDateT,
							accDet.amountSell AS totTransAmT,
							accDet.currencySell,
							accDet.exchangeRate,
							accDet.amountBuy AS totRecvAm,
							accDet.currencyBuy,
							accDet.status AS status
					
						FROM 
							".TBL_ACCOUNT_LEDGERS." AS accLed, ".TBL_ACCOUNT_DETAILS." AS accDet 
						WHERE
							accLed.transType='FT' AND accLed.transID= accDet.transID AND accLed.status != 'C'";
					
					
					
	//$queryComSub .= " AND cust.customerID = '$custID' AND tr.sending_currency = '$currFrom'";
	$queryComSub .= " AND accDet.accountSell = '$sendingAccount' AND accDet.currencySell = '".$currFrom."'";
	
	//if(!empty($currFrom))
		//$queryComSub .= " AND tr.sending_currency = '$currFrom' ";
	
	//echo $queryComSub;
	//exit;
	if(!empty($queryClause))
		$queryComSub .= $queryClause;

	if(!empty($extraQuery))
		$queryComSub .= $extraQuery;

	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_query());
	//print($queryComSub);
	$count = mysql_num_rows($resultComSub);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryComSub .= " order by $sidx $sord LIMIT $start , $limit";
	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_error());
	
	
	
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultComSub))
	{
		$transAm = number_format($row["totTransAmT"],2,'.',',');
		$totRecvAm = number_format($row["totRecvAm"],2,'.',',');
		
		$rowIdArr = explode('-',$row["transID"]);
		$response->rows[$i]['id'] = $rowIdArr[1];

		$response->rows[$i]['cell'] = array(		
									$row["transID"],
									$row["receiving_account"],
									$row["transDateT"],
									$transAm,
									$row["currencySell"],
									$row["exchangeRate"],
									$totRecvAm,
									$row["currencyBuy"],
									$status_arr[$row["status"]]									
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

?>