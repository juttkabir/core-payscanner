<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];

	$strMainCaption = "Manage Users Configuration Records"; 

	$cmd = $_REQUEST["cmd"];
	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into users_configuration
			                    (
									trans_hours_limit,
									customer_account_days_limit,
									created_by,
									created_on,
								    updated_on
								 )
							 values
								(
									'".$_REQUEST["trans_limit_hours"]."',
									'".$_REQUEST["customer_account_limit_days"]."',
									'".$strCreatedBy."',
									'".time()."',
									'".time()."'
								)";
			//debug($strInsertSql);
			if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$id = "";
						
			activities($_SESSION["loginHistoryID"],"INSERT",0,"users_configuration","New Limit Added");
		}
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["id"]))
			{
				$strUpdateSql = "update users_configuration 
								 set
									trans_hours_limit = '".$_REQUEST["trans_limit_hours"]."',
									customer_account_days_limit = '".$_REQUEST["customer_account_limit_days"]."',
									updated_on = '".time()."'
								 where
									id = '".$_REQUEST["id"]."'
							"; 
					//	debug($strUpdateSql);	
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$id = "";
				$cmd = "";
				
			activities($_SESSION["loginHistoryID"],"UPDATE",$_REQUEST["id"],"users_configuration","user configuration updated");
			}	
		}
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["id"]))
			{
				$strGetDataSql = "select
										 id,
										 trans_hours_limit,
										 customer_account_days_limit
								  from 
								  		users_configuration 
								  where 
								  		id='".$_REQUEST["id"]."'";
				$arrConfData = selectFrom($strGetDataSql);
				
				$id = $arrConfData["id"];
				$trans_limit_hours = $arrConfData["trans_hours_limit"];
				$customer_account_limit_days = $arrConfData["customer_account_days_limit"];
				$cmd = "UPDATE";
				activities($_SESSION["loginHistoryID"],"VIEW",$_REQUEST["id"],"users_configuration",
				"Cheque user configuration Fee Viewed");
			}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["id"]))
			{
				$strDelSql = "delete from users_configuration where id='".$_REQUEST["id"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
			activities($_SESSION["loginHistoryID"],"DELETE",$_REQUEST["id"],"users_configuration","User Configuration Deleted");
			}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}
	if(empty($cmd))
		$cmd = "ADD";
	/* Fetching the list of user configuration limit to display at the bottom */	
	$arrAllConfData = selectMultiRecords("Select 
												id, 
												trans_hours_limit,
										 		customer_account_days_limit,
												created_by,
												created_on,
												updated_on
										   from 
										   		users_configuration 
											order by updated_on desc
										");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Users Configuration Record</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script  language="javascript" >
	$(document).ready(function() {
		$("#submitUserConf").validate({
			rules: {
				   trans_limit_hours: {
					number: true,
					min: 1
				},
				customer_account_limit_days: {
					number: true,
					min: 1
				}
			
			},
			messages: {
				  trans_limit_hours: { 
					number: "<br />The hours entered is invalid.",
					min: "<br />Value should be greater than or equal to 1."
				},
				customer_account_limit_days: { 
					number: "<br />This is not valid days value must be numeric.",
					min: "<br />Value should be greater than or equal to 1"
				}
			}
		});
	
		$('imgDel').cluetip({splitTitle:'|'});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
</style>
</head>
<body>
<form name="submitUserConf" id="submitUserConf" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
				   
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Pending / Authorize transaction Limit </td>
		  <td align="left"><input type="text" name="trans_limit_hours" id="trans_limit_hours" size="10" value="<?=$trans_limit_hours?>" /> 
		    Hours <img src="images/info.gif" border="0" title="Hours Limit | Set Limit in hours e.g 24 or 48 if transaction over this limit then it will go in new queue and show alert ." /></td>
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Money Received Limit </td>
		  <td align="left"><input type="text" name="customer_account_limit_days" id="customer_account_limit_days" size="10" value="<?=$customer_account_limit_days?>" /> 
		    Days <img src="images/info.gif" border="0" title="Days Limit | Set Limit in days e.g 5 or 7 if  customer account retain amount  over this limit then it will go in new queue and show alert ." /></td>
	 </tr>
	
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="id" value="<?=$id?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($id)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>
			</td>
		</tr>
	
</table>

<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;AVAILABLE Users Configuration&nbsp;
	</legend>
	<table width="95%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="20%">Trans Status Limit</th>
			<th width="10%">Customer A/c Days Limit</th>
			<th width="20%">Created By</th>
			<th width="10%">Created On</th>
			<th width="15%">Updated On</th>
			<th width="15%">Actions</th>
		</tr>
		<?php
			foreach($arrAllConfData as $confVal)
			{
				$userData = explode("|",$confVal["created_by"]);
				$arrUserResultSet = array();
				if(!empty($userData[0]))
					$arrUserResultSet = selectFrom("select name, username from admin where userid='".$userData[0]."'");
			
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=$confVal["trans_hours_limit"]?></td>
			<td><?=$confVal["customer_account_days_limit"]?></td>
			<td><?=$arrUserResultSet["name"]." [".$arrUserResultSet["username"]."] / ".$userData[1]?></td>
			<td><?=date("d/m/Y",$confVal["created_on"])?></td>
			<td><?=date("d/m/Y H:i:s",$confVal["updated_on"])?> GMT</td>
			<td>
				<a href="<?=$_SERVER['PHP_SELF']?>?id=<?=$confVal["id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" id="imgTip" border="0" title="Edit configuration| You can edit the record by clicking on this thumbnail." />
				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?id=<?=$confVal["id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" id="imgTip" border="0" title="Remove Configuration|By clicking on this the configuration will be no longer available." />
				</a>
			</td>
		</tr>
				
		<?
			}
		?>
	</table>
</fieldset>
</form>
</body>
</html>