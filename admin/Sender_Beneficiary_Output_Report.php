<? 
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$fileNamed = "Sender_Beneficiary_Report";
$fileFormat = "xls";

$username  = $_SESSION["loggedUserData"]["username"];
$fromMonth=$_POST["fMonth"];
		$fromDay=$_POST["fDay"];
		$fromYear=$_POST["fYear"];
		
		$toMonth=$_POST["tMonth"];
		$toDay=$_POST["tDay"];
		$toYear=$_POST["tYear"];
		
		$beneficiary=$_POST["txtBeneficiar"];
		$sender=$_POST["txtSender"];
		$customerNumber = $_POST["customerNumber"];
		$contactNumber = $_POST["contactNumber"];
		
		$_SESSION["fMonth"]=$_POST["fMonth"];
		$_SESSION["fDay"]=$_POST["fDay"];
		$_SESSION["fYear"]=$_POST["fYear"];
		
		$_SESSION["tMonth"]=$_POST["tMonth"];
		$_SESSION["tDay"]=$_POST["tDay"];
		$_SESSION["tYear"]=$_POST["tYear"];
		
		$_SESSION["beneficiary"]=$_POST["txtBeneficiar"];
		$_SESSION["sender"]=$_POST["txtSender"];
		$_SESSION["customerNumber"] = $_POST["customerNumber"];
		$_SESSION["contactNumber"] = $_POST["contactNumber"];





	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=$fileNamed.$fileFormat"); 
	header ("Content-Description: PHP/INTERBASE Generated Data");


$query = "select c.customerID,b.benID,t.benAgentID,t.transAmount,t.transType,t.exchangeRate,t.localAmount,t.transAmount,t.totalAmount,t.IMFee,t.refNumberIM,t.refNumber,t.transDate from ". TBL_TRANSACTIONS . " as t,".TBL_BENEFICIARY." as b,
		" . TBL_CUSTOMER . " as c where t.benID = b.benID and t.customerID = c.customerID" ;
		$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t,".TBL_BENEFICIARY." as b,
		" . TBL_CUSTOMER . " as c where t.benID = b.benID and t.customerID = c.customerID" ;
		

$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	
		$queryDate = "(t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	
	
	
	$query .=  " and $queryDate";				
	$queryCnt .=  " and $queryDate";
	
	
	if($_SESSION["beneficiary"]!="")
	{
		
		$query .= " and (b.firstName like '".$_SESSION["beneficiary"]."%' OR b.lastName like '".$_SESSION["beneficiary"]."%')";
		$queryCnt .=" and (b.firstName like '".$_SESSION["beneficiary"]."%' OR b.lastName like '".$_SESSION["beneficiary"]."%')";
		
	}
		

	
	if($_SESSION["sender"]!="")
	{
		
			if(CONFIG_SENDER_BENIFICIARY_REPORT == '1'){
				
				
				$sName = explode(" ", $_SESSION["sender"]);
				if($sName[0] != ""){
					$fName = $sName[0];
					
					}
					if($sName[1] != ""){
					$mName = $sName[1];
				
					}
					if($sName[2] != ""){
					$lName = $sName[2];
					
					}				
					
					
					if(count($sName)== '1'){
				
							$query .= "  and (c.firstName like '".$_SESSION["sender"]."%' OR c.lastName like '".$_SESSION["sender"]."%')";
							$queryCnt .= "  and (c.firstName like '".$_SESSION["sender"]."%' OR c.lastName like '".$_SESSION["sender"]."%')";
							
				}
					
				elseif(count($sName) == '2'){
							$query .= "  and (c.firstName like '".$fName."' AND c.lastName like '".$mName."')";
							$queryCnt .= "  and (c.firstName like '".$fName."' AND c.lastName like '".$mName."')";
		    }
		
				elseif(count($sName) == '3'){
							$query .= "  and (c.firstName like '".$fName."' AND c.middleName like '".$mName."' AND c.lastName like '".$lName."')";
							$queryCnt .= "  and (c.firstName like '".$fName."' AND c.middleName like '".$mName."' AND c.lastName like '".$lName."')";
		    }
				
				
				}
			else{
				
				$query .= "  and (c.firstName like '".$_SESSION["sender"]."%' OR c.lastName like '".$_SESSION["sender"]."%')";
				$queryCnt .= "  and (c.firstName like '".$_SESSION["sender"]."%' OR c.lastName like '".$_SESSION["sender"]."%')";
				
			
		}
		
	}
	
	if($_SESSION["customerNumber"]!="")
	{
			$query .= "  and (c.accountName like '".$_SESSION["customerNumber"]."')";
			$queryCnt .= "  and (c.accountName like '".$_SESSION["customerNumber"]."')";
		
	}
	
	if($_SESSION["contactNumber"]!="")
	{
			$query .= "  and (c.Mobile like '".$_SESSION["contactNumber"]."%')";
			$queryCnt .= "  and (c.Mobile like '".$_SESSION["contactNumber"]."%')";
		
	}
	
	if($_SESSION["phoneNumber"]!="")
	{
			$query .= "  and (c.Phone like '".$_SESSION["contactNumber"]."%')";
			$queryCnt .= "  and (c.Phone like '".$_SESSION["contactNumber"]."%')";
		
	}

$query .= "  order by transDate DESC";
//$query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);
$allCount = countRecords($queryCnt);	





	$data = "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	
	$data .= " <tr>";
		
		 
		
$data .= "
				

					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Reference Code</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Family Express Code</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Date</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Sender Name</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Beneficiary Name</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Remitted Amount</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Exchange Rate</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Foriegn Amount</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Bank Name</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;FX Fee</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Total Amount</font></td>
					<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Transaction Type</font></td>";
					
		
		$data .= "</tr>";
		
		
	
		for($i=0;$i < count($contentsTrans);$i++)
	{
		$customerContent = selectFrom("select firstName, lastName,accountName,Mobile,Phone from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
      $beneContent = selectFrom("select firstName, lastName,Phone from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
      $bankContent = selectFrom("select username from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["benAgentID"]."'");
    
		$accountName = $customerContent["accountName"];
		$Sendername = ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]);  
      
		$Mobile = $customerContent["Mobile"];
		$Phone = $customerContent["Phone"]; 
		$benName = ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]); 
		$bankName = $bankContent["username"];       
		$IMFee = $contentsTrans[$i]["IMFee"];
    $transAmount = $contentsTrans[$i]["transAmount"];
    $transType = $contentsTrans[$i]["transType"];
    $exchangeRate = number_format($contentsTrans[$i]["exchangeRate"],2,'.',',');
    $localAmount = number_format($contentsTrans[$i]["localAmount"],2,'.',',');
    $remittedAmount = $contentsTrans[$i]["transAmount"];
		$refIM = $contentsTrans[$i]['refNumberIM'];
			$refNo = $contentsTrans[$i]['refNumber'];			
			$date = $contentsTrans[$i]['transDate'];			
			
	// number_format($contentsTrans[$i]["totalAmount"],2,'.',',');
				  
	$totalAmount += $contentsTrans[$i]["totalAmount"];
				 
		
		
		$data .= " <tr>
		

		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;  $refIM</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $refNo </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $date </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $Sendername </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $benName </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $remittedAmount</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $exchangeRate </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $localAmount </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; 	$bankName </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; 	$IMFee </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; 	$transAmount </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; 	$transType </font></td>	";
		$data .= "</tr>";
		
			
}		
		
					
		$data .= " <tr>		
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		 ";
		
		$data .= "</tr>";
		
			
		$data .= " <tr>
		

		
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; 	total Amount </font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
			<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>

		<td><font color='#000000'  size='1' face='Verdana'>&nbsp; $totalAmount </font></td>
		<td><font color='#000000'  size='1' face='Verdana'>There are&nbsp;$allCount Records.</font></td> ";
		
		$data .= "</tr>";
		
		
		

	$data.="</table>";

	$len = strlen($data);

	echo $data;


?>