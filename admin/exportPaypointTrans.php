<?php

session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
/**
 * Date filter
 */
$agentName = $_REQUEST["agentID"];
$fromDate = $_REQUEST["fromDate"];
$toDate = $_REQUEST["toDate"];
if($fromDate!="" && $toDate!="")
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
$exportType = $_REQUEST["exportType"];
$queryFlag = $_REQUEST["queryFlag"];
$queryService = $_REQUEST["serviceID2"];
$queryCategory = $_REQUEST["categoryID2"];
if($exportType!=""){
	 		 	$Balance="";
	 	
	 	if($agentName == "allSUPA")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Supper' ";
			//$accountQuery .= " and $queryDateSUP";
	 		
	 	}elseif($agentName == "allSUPI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Supper' ";
			//$accountQuery .= " and $queryDateSUP";	
	 		
	 	}elseif($agentName == "allSUBA")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Sub' ";
			//$accountQuery .= " and $queryDateSUP";	
	 		
	 	}elseif($agentName == "allSUBI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Sub' ";
			//$accountQuery .= " and $queryDateSUP";	
	 	}elseif($agentName == "allSUPAI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent != 'ONLY' and agentType = 'Supper' ";
			//$accountQuery .= " and $queryDateSUP";	
	 	}elseif($agentName == "allSUBAI")
	 	{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent != 'ONLY' and agentType = 'Sub' ";
			//$accountQuery .= " and $queryDateSUP";	
	 	}else{
	 		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0";
		////	$accountQuery .= " and $queryDate";				
			
			if($agentName != "" && $agentName != "all")
			{
				$accountQuery .= " and  userID = '$agentName' ";	
			}
		}
	 		$contentsAcc = selectMultiRecords($accountQuery);
	 		$allCount = count($contentsAcc);
			
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
	$subWhereClause = "";
	$subWhereClause .= " and serviceID = '$queryService'";
	$subWhereClause .= " and categoryID = '$queryCategory'";		
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=PaypointTrans-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=PaypointTrans-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export Paypoint Transaction</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	$strFullHtml .= $strRowStart;
// Column Heading starts
	$strFullHtml .= $strColumnStart.$strBoldStart."Agent".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Opening Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Running Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Closing Balance".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Column Heading ends
	$strFullHtml .= $strRowStart;

	 	 if($_REQUEST["queryFlag"]!="true") { 	
	 		$totalOpeningBalance = 0;
			$totalClosingBalance = 0;
			$totalDailyBalance = 0;
	 			 
				for($i=0;$i < count($contentsAcc);$i++)
				{
					 $openingBalance = 0;
					 $closingBalance = 0;
					 $dailyBalance = 0;

					$summaryTable = "account_summary_paypoint";	

					$subDatedFrom = "SELECT Max(dated) as datedFrom from " . $summaryTable . " WHERE dated <= '$fromDate' ".$subWhereClause;
					$subDatesFromRS = selectFrom($subDatedFrom);
					$FirstDated = $subDatesFromRS["datedFrom"];
					$subDatedTo = "SELECT Max(dated) as datedTo  from " . $summaryTable . " WHERE dated <= '$toDate' ".$subWhereClause;
					$subDatedToRS = selectFrom($subDatedTo);
				    $LastDated = $subDatedToRS["datedTo"];
					
					$dateFromQuery = "select opening_balance,currency from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated = '$FirstDated' ".$subWhereClause." ORDER BY id DESC";
					$dateFromRS = selectFrom($dateFromQuery);
					$openingBalance = $dateFromRS["opening_balance"];
					$dateToQuery = "select closing_balance,currency from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated = '$LastDated' ".$subWhereClause." ORDER BY id DESC";
					$dateToRS = selectFrom($dateToQuery);
					$closingBalance = $dateToRS["closing_balance"];

					$dailyBalance = $closingBalance - $openingBalance;
					$totalOpeningBalance += $openingBalance;
					$totalClosingBalance += $closingBalance;
					$totalDailyBalance   += $dailyBalance;	
					$currSending = ($dateToRS["currency"]!="" ? $dateToRS["currency"] :"");
			
			
		if($exportType == "csv")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$contentsAcc[$s]["status"]."'>";

// Data Rows Starts
		$strFullHtml .= $strColumnStart.$contentsAcc[$i]["name"]." [".$contentsAcc[$i]["username"]."]".$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($openingBalance,2,'.',','):number_format($openingBalance,2,'.','')." ".$currSending).$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($dailyBalance,2,'.',','):number_format($dailyBalance,2,'.','')." ".$currSending).$strColumnEnd;
		$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($closingBalance,2,'.',','):number_format($closingBalance,2,'.','')." ".$currSending).$strColumnEnd;
		$strFullHtml .= $strRowEnd;
// Data Row Ends
	}
}
// Breakline Starts
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strColumnStart."".$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Breakline Ends

// Total Runnig Row starts
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart."Total : ".$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalOpeningBalance,2,'.',','):number_format($totalOpeningBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalDailyBalance,2,'.',','):number_format($totalDailyBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strColumnStart.($exportType!="csv" ? number_format($totalClosingBalance,2,'.',','):number_format($totalClosingBalance,2,'.','')).$strColumnEnd;
	$strFullHtml .= $strRowEnd;
// Total Runnig Row starts
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>