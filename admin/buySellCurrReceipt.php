<?php

	session_start();
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$titleReciept = "Currency Exchange Reciept";
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	
	/* #5781 - AMB Exchange
	 * Currency Exchange Fee module added
	 * Also works for manual fee.
	 * by Aslam Shahid.
	*/
	$currencyFeeFlag = false;
	if(defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && 
		(strstr(CONFIG_CURRENCY_EXCHANGE_FEE_USERS,$agentType) || CONFIG_CURRENCY_EXCHANGE_FEE_USERS=="1")){
		$currencyFeeFlag = true;
	}
	
	$currExchData = selectFrom("select ce.*,a.name,a.userID,c.firstname, c.middlename, c.lastname, c.accountName from curr_exchange_account ce LEFT JOIN customer c ON  ce.customerID=c.customerID,admin a where ce.id = '".$_REQUEST["rid"]."' and ce.createdBy=a.userID");
/*	if($userID != $currExchData["userID"]){
		echo $userID ." - ". $currExchData['name']."<br/>";
		echo "<h2>You are not Authorized for this Operation!</h2>";
		exit;
	}*/
	$opcurrData   = selectFrom("select currencyName,country from currencies where cID = '".$currExchData["operationCurrency"]."'");
	$bscurrData   = selectFrom("select currencyName,country from currencies where cID = '".$currExchData["buysellCurrency"]."'");
	$opcurrName   = $opcurrData['currencyName'];
	$opcountName  = $opcurrData['country'];
	$bscurrName   = $bscurrData['currencyName'];
	$bscountName  = $bscurrData['country'];
	
	$exchangeType = $currExchData["buy_sell"] =="B" ? "Buy" : "Sell";
	$typeText     = $currExchData["buy_sell"] =="B" ? "Bought" : "Sold";
	$rateTypeText = $currExchData["buy_sell"] =="B" ? "Buying" : "Selling";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$titleReciept?></title>
<script language="javascript" src="jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#printbtn").click(function(){
			$(this).hide();
			print();
		});
	});
</script>
<style>
td{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
}
.amounts_right{
	width: 70px; 
	text-align: right; 
	float:left;
} 
</style>
</head>
<body>
<table border="1" bordercolor="#4C006F" cellspacing="0" cellpadding="0" width="320">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="3" id="table">
				<tr>
				   <td colspan="4" align="center" bgcolor="#FFFFFF">
						<img height='50' alt='' src='<?=CONFIG_LOGO?>' width='100' /><br />
					<span style="font-size:11px;">
					87,Edgware Road, London, W2 2HX <br />
					Tel: 0207 723 3622, Fax: 0207 723 2984
					</span>
						</td>
				</tr>
				<tr>
				  <td colspan="4" align="center" bgcolor="#FFFFFF">
					  <h2><?=$titleReciept?></h2>
						<br />					</td>
				</tr>
				<tr>
					<td width="37%" scope="col"><b>Date:</b></td>
					<td width="63%" scope="col"><?=date("d/m/Y H:i:s A",strtotime($currExchData["created_at"]))?></td>
				</tr>
				<tr>
					<td scope="col"><b>Teller:</b></td>
					<td scope="col"><?=$currExchData["name"]?></td>
				</tr>
				<tr>
					<td colspan="2"><hr width="100%" style="border-style:dotted" /></td>
				</tr>
				
				<tr>
					<td scope="col" colspan="2">
						<i><b>Customer</b></i><br />
                        <?php if($currExchData["firstname"]!=""){?>
						&nbsp;&nbsp;<?=$currExchData["firstname"]." ".$currExchData["middlename"]." "
										.$currExchData["lastname"]." (".$currExchData["accountName"].")"?>
                        <?php }?>
                        </td>
				</tr>
				<tr><td><br /></td></tr>
				<tr>
					<td align="left"><b>Type of Exchange :</b></td>
					<td align="left"><?=$exchangeType?></td>
				</tr>
				<tr>
					<td align="left"><b>Amount <?=$typeText?>:</b></td>
					<td align="left"><?=$currExchData["totalAmount"]. " ". $bscurrName?></td>
				</tr>
				<tr>
				  <td align="left"><b><?=$rateTypeText?> Rate
				    :</b></td>
				  <td align="left"><?=$currExchData["rate"]?></td>
			  </tr>
				<tr>
					<td align="right" valign="top"><b>Charged :</b></td>
					<td align="left" style="padding-left:60px;">
					<div class="amounts_right">
					<?=$currExchData["localAmount"]. " ".$opcurrName?></div></td>
				</tr>
             <?php 
			 	$total_local = $currExchData["localAmount"];
			 if($currencyFeeFlag){ 
			 	$total_local = $currExchData["total_localAmount"];
			 ?>
				<tr>
					<td align="right" valign="top"><b>Fee :</b></td>
					<td align="left" style="padding-left:60px;">
                        <div class="amounts_right">
                        <?=$total_local- $currExchData["localAmount"]. " ".$opcurrName?></div></td>
				</tr>
             <?php }?>
				<tr>
					<td colspan="2" align="right" valign="bottom"><hr width="80%" /></td>
				</tr>
				<tr>
					<td align="right" valign="top"><b>Total :</b></td>
					<td align="left" style="padding-left:60px;">
					<div  class="amounts_right">
					<?=$total_local. " ".$opcurrName?></div></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Customer Signature</b>
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Authorized</b>					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="button" id="printbtn" value="Print Order Receipt" />

</body>
</html>