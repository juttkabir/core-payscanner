<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$agentType = getAgentType();
//$agentType == "SUPI" in case of Distributor

unset($mycnt);

if ($offset == "")
{
	$offset = 0;
}
$limit = CONFIG_MAX_TRANSACTIONS_FOR_PRINT;
if($limit == 0)
	$limit = 20;

if ($_GET["newOffset"] != "")
{
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

if($_POST["Submit"] =="Search" || $_GET["Submit"] != "")
{	
	if($_POST["Submit"] =="Search")
	{
		$rfMonth = $_POST["fMonth"];
		$rfDay = $_POST["fDay"];
		$rfYear = $_POST["fYear"];
			
		$rtMonth = $_POST["tMonth"];
		$rtDay = $_POST["tDay"];
		$rtYear = $_POST["tYear"];
		$transStatus = $_POST["transStatus"];
		$currency = $_POST["currency"];
	
	$fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
	$toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	}elseif ($_GET["Submit"] != "")
	{
		$fromDate = $_GET["fromDate"];
		$fD = explode("-",$_GET["fromDate"]);
		if (count($fD) == 3)
		{
				$rfDay = $fD[2];
				$rfMonth = $fD[1];
				$rfYear = $fD[0];
		}
		$toDate = $_GET["toDate"];
		$tD = explode("-",$_GET["toDate"]);
		if (count($tD) == 3)
		{
				$rtDay = $tD[2];
				$rtMonth = $tD[1];
				$rtYear = $tD[0];
		}
		$transStatus = $_GET["transStatus"];
		$currency = $_GET["currency"];
	}
	
	$queryDate = " (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
	
	
		$myquery = "select distinct collectionPointID from transactions where transType like 'Pick up' ";
		$myquery2 = "select count(distinct collectionPointID) from transactions where transType like 'Pick up'";
		if($transStatus != '')
		{
			$myquery .=  " and (transStatus = '$transStatus') "; 
			$myquery2 .= " and (transStatus = '$transStatus')";
		}else{
						$myquery .= " and (transStatus = 'Authorize' OR transStatus = 'Amended')"; 
					 	$myquery2 .=" and (transStatus = 'Authorize' OR transStatus = 'Amended')";
			}
			
			$myquery .= " and (currencyTo = '".$currency."')"; 
			$myquery2 .=" and (currencyTo = '".$currency."')";
			
   $myquery .= " and $queryDate LIMIT $offset, $limit";
		$myquery2 .= " and $queryDate";                     
		
		$ress = mysql_query($myquery);
		$mycnt = 0;
		
		$mycnt = mysql_num_rows($ress);
		
		$allCount = countRecords($myquery2);
		
}


?>
<html>
<head>
	<title>Payout Center Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>

    <style type="text/css">
<!--
.style2 {color: #005b90; font-weight: bold; }
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
   <td><font class="topbar">Payout Center Report</font><font size="1">(By default Authorized and Amended Transactions are shown)</font></td>
  </tr>
  <tr>
    <td align="center"><br>
      <table width="255" border="1" cellpadding="5" bordercolor="#666666">
        <form action="payout_center_report.php" method="post" name="Search">
        <tr>
        	<td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
        <SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
        <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
         <br>
		Transaction Status <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">

		  <option value=""> - Status - </option>

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		    <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Hold">Holded</option>
		  <option value="Failed">Failed</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <!-- <option value="Credited">Credited</option> -->
        </select><script language="JavaScript">SelectOption(document.Search.transStatus, "<?=$transStatus?>");</script>
        &nbsp;Destination Currency
         <select name="currency" style="font-family:verdana; font-size: 11px; width:100">
			  <option value=""> - Select Currency - </option>
        <?
        $currencyQuery = "select distinct currencyTo from transactions where transType like 'Pick up' and currencyTo!= '' ";
        $currencies = selectMultiRecords($currencyQuery);
        for($i = 0 ; $i < count($currencies); $i++)
        {?>
        		<option value="<?=$currencies[$i]["currencyTo"]?>"><?=$currencies[$i]["currencyTo"]?></option>
        <?	
        }
        ?>
   	 
        </select><script language="JavaScript">SelectOption(document.Search.currency, "<?=$currency?>");</script>
      
        <input type="submit" name="Submit" value="Search"></td>
      </td>
     </tr>
	  </form>
    </table>
      <br>
	  <form action="payout_center_report.php" name="payout_report" method="post">
      <table width="500" border="1" cellpadding="0" bordercolor="#666666">
      <?
				if ($allCount > 0){
			?>
      	<tr>
          <td  bgcolor="#000000" border="1">
						<table width="700" cellpadding="2" cellspacing="0" border="1" bgcolor="#FFFFFF">
      					<tr>
                  <td>
                    <?php if ($mycnt > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+$mycnt);?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&Submit=$Submit&fromDate=$fromDate&toDate=$toDate&transStatus=$transStatus&currency=$currency";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&Submit=$Submit&fromDate=$fromDate&toDate=$toDate&transStatus=$transStatus&currency=$currency";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&Submit=$Submit&fromDate=$fromDate&toDate=$toDate&transStatus=$transStatus&currency=$currency";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&Submit=$Submit&fromDate=$fromDate&toDate=$toDate&transStatus=$transStatus&currency=$currency";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
             </table>
          </td>
        </tr>   
        
        		<tr>
            
          <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;
          	<?
          	if ($mycnt == 1)
          	{
          		echo "There is 1 record. ";
          	}
          	else
          	{ 
          		echo "There are " . $mycnt . " records. "; 
          	}
          	?>
          	</strong></span></td>
          </tr>

          <tr>
            <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF">
                  <td><span class="style1">Payout Center - City</span></td>
				  				<td><span class="style1">Amount Sent</span></td>
                  <td><span class="style1">Foreign Amount</span></td>
				   				<td><span class="style1">Transactions Count</span></td>
               </tr>   
        
                <? 

		$cnt = 0;
		while ($row2 = mysql_fetch_row($ress))
		{
			$cnt++;
			
					$query3 = "select sum(transAmount), max(currencyFrom), sum(localAmount),max(currencyTo) , count(transID) from transactions where collectionPointID = '".$row2[0]."' and transType like 'Pick up' ";
					
					if($transStatus != '')
					{
						$query3 .=  " and (transStatus = '$transStatus') "; 
					}else{
						$query3 .= " and (transStatus = 'Authorize' OR transStatus = 'Amended')"; 
					}
					
				$query3 .= " and (currencyTo = '".$currency."')";	
				$query3 .= " and $queryDate order by transDate";
					
				$res3 = mysql_query($query3);
				$row3 = mysql_fetch_row($res3);
					
					$query4 = "select cp_corresspondent_name, cp_city from cm_collection_point where cp_id = '".$row2[0]."'";
					$res4 = mysql_query($query4);
					$row4 = mysql_fetch_row($res4);
						?>
						<tr bgcolor="#FFFFFF">
              	
	            <td bgcolor="#FFFFFF">
	            	<a href="#" onClick="javascript:window.open('view-payout-center-transaction.php?collection=<?=$row2[0]?>&toDate=<?=$toDate?>&fromDate=<?=$fromDate?>&transStatus=<?=$transStatus?>&currencyTo=<?=$currency?>','viewPayoutCenterTransactions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
	            		<strong>
	            			<font color="#006699">
	            				<? echo $row4[0] . " - " . $row4[1]; ?>
	            			</font>
	            		</strong>
	            	</a>
	            </td>
		  				<td bgcolor="#FFFFFF" ><? echo (number_format($row3[0],2,'.',','). " " .getCurrencySign($row3[1])); ?></td>
	            <td bgcolor="#FFFFFF"><?  echo(number_format($row3[2],2,'.',','). " " . getCurrencySign($row3[3])); ?></td>
	            <td bgcolor="#FFFFFF"><? echo $row3[4]?></td>
            </tr>
		                  
		          <?
				
	}
					?>
						 <tr bgcolor="#FFFFFF">
              <td>&nbsp;</td>
		  				<td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
          	 </tr>  
										
				<?
	
		
		$query6 = "select sum(transAmount), sum(localAmount), count(transID) from transactions where transType like 'Pick up' ";
				
			if($transStatus != '')
			{
				$query6 .=  " and (transStatus = '$transStatus') "; 
			}else{
				$query6 .= " and (transStatus = 'Authorize' OR transStatus = 'Amended')"; 
			}
			$query6 .= " and (currencyTo = '".$currency."')";

			$query6 .= " and $queryDate order by transDate";
		
		$res6 = mysql_query($query6);
		$row6 = mysql_fetch_row($res6);
				
			?>
                <tr bgcolor="#C0C0C0">
                  <td align="right"><span class="style2"><font size="2">Grand Total:-</font></span></td>
				  				<td><strong><? echo(number_format($row6[0],2,'.',','))?></strong></td>
                  <td><strong><? echo(number_format($row6[1],2,'.',','))?></strong></td>
                  <td><strong><? echo $row6[2]?></strong></td>
               </tr>
             
				
			       </table>
			       
			       </td>
          </tr>
      </table>
	    <p>
		<table align="center"> <tr><td>
          <input type="button" name="print_report" value="Print the Report" onClick="print()">
		  </td></tr></table>
        </p>
        
        
         <?
       
			} else if (!isset($mycnt)){
	
			} else {
		?>
					<tr>
            <td  align="center"> No transaction found in the database.
            </td>
          </tr>
    <?
			} 
		?>
      </form>
      </td>
  </tr>

</table>
</body>
</html>
