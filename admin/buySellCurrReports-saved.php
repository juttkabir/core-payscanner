<?
session_start();
include ("../include/config.php");
include ("security.php");
$strLabel = 'Saved Currency Exchange Reports';
$strSql = "select 
			cpq.RID as RID,
			cpq.reportName as reportName,
			cpq.reportLabel as reportLabel,
			cpq.queryDate as queryDate,
			cpq.note as note,
			cpq.logedUser as logedUser, 
			ad.name as name
		from ".TBL_COMPLIANCE_QUERY." as cpq 
			LEFT JOIN ".TBL_ADMIN_USERS." as ad 
			ON cpq.logedUserID = ad.userID 
		where cpq.reportName = 'buySellCurrReports.php' ";
if(!empty($_REQUEST["Submit"])){
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	$strSql .= " and cpq.queryDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59'";
	if(!empty($_REQUEST["dispatcheNumber"]))
		$strSql .= " and cpq.note = '".$_REQUEST["dispatcheNumber"]."'";
	if(!empty($_REQUEST["dispatcheNumber"]))
		$strSql .= " and cpq.note = '".$_REQUEST["dispatcheNumber"]."'";
}
//debug($strSql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$strLabel;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 24px;
	font-weight: bold;
	height: 50px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 16px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->
</style>
<script>
	<!--
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	-->
</script>
</head>
<body>

	<table border="0" align="center" cellpadding="5" cellspacing="1" >
      <tr>
      	<td align="center">
  			<b>SEARCH FILTER</b>    				
      	</td>
    	</tr>
      <tr>
		  <form name="search" method="post" action="buySellCurrReports-saved.php">
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
			<b>From </b>
			<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.fDay, "<?=(!empty($_REQUEST["fDay"])?$_REQUEST["fDay"]:"")?>");
			</script>
					<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.fMonth, "<?=(!empty($_REQUEST["fMonth"])?$_REQUEST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			  <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
			</SELECT> 
					<script language="JavaScript">
				SelectOption(document.search.fYear, "<?=(!empty($_REQUEST["fYear"])?$_REQUEST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			  <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
			</select>
					<script language="JavaScript">
				SelectOption(document.search.tDay, "<?=(!empty($_REQUEST["tDay"])?$_REQUEST["tDay"]:"")?>");
			</script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
	
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
					<script language="JavaScript">
				SelectOption(document.search.tMonth, "<?=(!empty($_REQUEST["tMonth"])?$_REQUEST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
			<script language="JavaScript">
			SelectOption(document.search.tYear, "<?=(!empty($_REQUEST["tYear"])?$_REQUEST["tYear"]:"")?>");
			</script>
			<br /><br />
			&nbsp;&nbsp;
			<b> Dispatch number </b>
			&nbsp;<input type="text" name="dispatcheNumber" value="<?=$_REQUEST["dispatcheNumber"]?>" />
			<br /><br />
			  <input type="submit" name="Submit" value="Process">
			  <? if(!empty($msg)) { ?>
				<br /><br />
				<b><?=$msg?></b>
			  <? } ?>
					</td>
			</form>
			</tr>
		</table>

	<table width="90%" cellpadding="2" cellspacing="0" align="center">
		<tr>
			<td colspan="3" class="reportHeader"><?=$strLabel;?><br /></td>
			<td colspan="2" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td class="columnHeader" align="center">DATE</td>
			<td class="columnHeader" align="center">Dispatch Number</td>
			<td class="columnHeader" align="center">Creator</td>
			<td class="columnHeader" align="center">User Type</td>
			<td class="columnHeader" align="center">&nbsp;</td>
		</tr>
		<?
		if(!empty($strSql))
			$arrDispatchNumbers = SelectMultiRecords($strSql);

		for($i=0; $i < count($arrDispatchNumbers); $i++)
		{
		?>
		<tr>
			<td class="transactions" align="center"><?=$arrDispatchNumbers[$i]["queryDate"]?></td>
			<td class="transactions" align="center"><?=$arrDispatchNumbers[$i]["note"]?></td>
            <td class="transactions" align="center"><?=$arrDispatchNumbers[$i]["name"]?></td>
            <td class="transactions" align="center"><?=$arrDispatchNumbers[$i]["logedUser"]?></td>
			<td class="transactions" align="center">
				<a href="buySellCurrReports.php?RID=<?=$arrDispatchNumbers[$i]["note"]?>" target="_blank"><?=$arrDispatchNumbers[$i]["reportLabel"]?></a>
			</td>
		</tr>
		<?php	
		}	
		/**
		 * If no records to show
		 */
		if(count($arrDispatchNumbers) < 1)
		{
		?>
			<tr>
				<td class="transactions" align="center" colspan="5">No Record found!</td>
			</tr>
		<?
		}
		?>

</table>
</body>
</html>