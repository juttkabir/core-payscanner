<?php
	/** Maintains Report Viewed History (Report Logs)
	 *	To maintain report viewed history include this file on report and call maintainReportLogs function
	 *	or send an ajax call to this page.
	 *  @pakage Report
	 *  @subpaakge Report View History
	 *  @author Kalim ul Haq
	 *  copyrigth(c) HBS Technology (Pvt.) Ltd.
	 */
	 session_start();
	// Minatan logs by ajax call
	if($_REQUEST["maintainLogsAjax"] == "Yes")
	{
		include("../include/config.php");
		include("security.php");
		$systemCode	= SYSTEM_CODE;
		$company	= COMPANY_NAME;
		$systemPre	= SYSTEM_PRE; 
		$manualCode = MANUAL_CODE;
		$agentType	= getAgentType();
		$date_time	= date('d-m-Y  h:i:s A');
		$currDate	= date('Y-m-d');
		$parentID	= $_SESSION["loggedUserData"]["userID"];

		$strPageUrl	= $_REQUEST["pageUrl"];
		$strAction	= $_REQUEST["action"];
		maintainReportLogs($strPageUrl, $strAction);
		exit;
	}
	
	
	function maintainReportLogs($strUrl, $action="")
	{
		$arrSavePageAccess = array(
							"/admin/commission_summary_report.php" 		=> "Agent Commission Report",
							"/admin/admin-user-account.php"				=> "Admin User Account Statement",
							"/admin/agent_Account.php"					=> "Agent Account Statement",
							"/admin/sub_agent_Account.php"				=> "Sub Agent Account Statement",
							"/admin/agent_Account_Summary.php"			=> "Account Summary Report",
							"/admin/bank_Account.php"					=> "Distributor Account Statement",
							"/admin/agent_Distributor_Account_new.php"	=> "A&D Account Statement",
							"/admin/sub_Account.php"					=> "Sub A&D Account Statement",
							"/admin/sub_bank_Account.php"				=> "Sub Distributor Account Statement",
							"/admin/distributor-comm-summary.php"		=> "Distributor Commission Report",
							"/admin/daily-bank-transfer-report.php"		=> "Daily Distributor Report",
							"/admin/bank_Account_List.php"				=> "Distributor Statement",
							"/admin/outstanding_payout_center_report.php"=> "Outstanding Payout Center Report",
							"/admin/daily-trans.php"					=> "Daily Transaction Summary",
							"/admin/transferReport.php"					=> "Transaction Transfer Report",
							"/admin/reportTransactionSummary.php"		=> "Report Transaction Summary",
							"/admin/beneficiaryReport.php"				=> "Sender/Beneficiary Report",
							"/admin/current-regis.php"					=> "Sender Registration Report",
							"/admin/Agent_account_statement.php"		=> "Agent Account Balance",
							"/admin/customer_account_statement.php"		=> "Sender Account Balance",
							"/admin/sales_Report.php"					=> "Daily Sales Report",
							"/admin/user_audit_log_screen.php"			=> "Payex Audit Logs",
							"/admin/Transaction_Audit.php"				=> "Transactions Audit Window",
							"/admin/CashBookNew.php"					=> "Cash Book System",
							"/admin/CompanyCashBook.php"				=> "Company Cash Book",
							"/admin/savedCompanyCashBookReports.php"	=> "Saved Company Cash Book Reports",
							"/admin/expenses.php"						=> "Daily Expenses",
							"/admin/daily-commission-summary.php"		=> "Daily Commission Report",
							"/admin/reportViewHistory.php"				=> "View Reports Logs",
							"/admin/commission_report_ajax.php"			=> "Introducer Commission Report",
							"/admin/daily-bank-transfer-report.php"		=> "Daily Distributor Report",
							"/admin/teller_daily_report.php"			=> "Daily Cashier Report",
							"/admin/teller_Account.php"					=> "Teller/Cashier Account",
							"/admin/daily-agent-transfer-report.php"	=> "Daily Agent Report",
							"/admin/outStandingPay.php"					=> "Outstanding Agent Payments",
							"/admin/cust-aml-report.php"				=> "Customer AML Report",
							"/admin/daily_report.php"					=> "Daily Report",
							"/admin/company_profit.php"					=> "Exchange Rate Earning Report"
						);
	
		saveReportViewedHistory($strUrl, $arrSavePageAccess[$strUrl], $action);	
	}
	
	function saveReportViewedHistory($url, $title, $action)
	{
		
		/**
		 * 	This function will be called to update the VIEW COUNTER of specific report i.e., 
		 *	how many times and by whom the report is viewed. It is also called for maintainin 
		 *	other logs like how many times one report is PRINTED, EXPORTED orr SAVED.
		 *
		 * 	@param string 		$url 			Report name
		 * 	@param string		$title			Report Title
		 * 	@param char			$action			'E' for export, 'S' for save , 'P' for print
		 */
	
		$arrAction=array("E"=>0,"P"=>0,"S"=>0);
		
		if(!empty($action)){
			$extraField=" ,actionCount";
			$extraFieldVal=',1';		
		}
		
		//check if specific logged in user has already viewed specific report
		$strGetSql = " SELECT 
							count 
							$extraField
					   FROM 
							viewReportHistory 
					   WHERE 
							login_history_id = '".$_SESSION["loginHistoryID"]."'
						AND
							report_url='".$url."'";
		$arrGetData = selectFrom($strGetSql);
	
		if(empty($arrGetData))
		{
						$actionArr=serialize($arrAction);
						$strInsertSql	= "	INSERT INTO 
												viewReportHistory 
												(	login_history_id,
													report_url,
													report_title,
													count,
													actionCount
												)
												VALUES
												(	'". $_SESSION["loginHistoryID"]."', 
													'".$url."', 
													'".$title."', 
													1,
													'".$actionArr."'
												)";
						insertInto($strInsertSql);
	
		}
		else
		{
			//maintaining action log
			if(!empty($action)){
				$actionArr = unserialize($arrGetData["actionCount"]);
				foreach($actionArr as $key=>$val)
				{
					if($action==$key)
						$actionArr[$key]=$val+1;
				}
				$actionArr=serialize($actionArr);
				$extraFieldUpdate ="actionCount='$actionArr'";
			}else{
				$count=$arrGetData["count"]+1;
				$extraFieldUpdate ="count='$count'";
			}
			
			$strInsertSql	= "	UPDATE 
									viewReportHistory 
								SET
									".$extraFieldUpdate."
								WHERE 1  
									AND login_history_id = '".$_SESSION["loginHistoryID"]."'
									AND	report_url='".$url."'";
				update($strInsertSql);
		}
	}
?>