<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	$systemCode = SYSTEM_CODE;
    $manualCode = MANUAL_CODE;
		if($_REQUEST["Submit"] == "Search")
		{
		  
			$query = "SELECT
			 			 	transID,refNumberIM,refNumber,transDate,createdBy,benID,customerID,custAgentID,transStatus,transAmount
					  FROM 
					        ". TBL_TRANSACTIONS . "
					  WHERE 1 ";
		
			if(!empty($_REQUEST["from_date"]) && !empty($_REQUEST["to_date"]))
			{
			   $from_date = $_REQUEST["from_date"];
			   $to_date = $_REQUEST["to_date"];
			 $query .= " and (transDate >= '$from_date 00:00:00' and transDate <= '$to_date 23:59:59')";
			}
			
			if(!empty($_REQUEST["amount"])){
			$amount = $_REQUEST["amount"];
			$amountCriteria = $_REQUEST["amountCriteria"];
			$query .=" and transAmount".$amountCriteria."".$amount." ";
			}
			//$query .= " ORDER BY transDate DESC ";
			$amlReportData = selectMultiRecords($query);
		}
	$exportType = $_REQUEST["exportType"];
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "XLS")
	{
	
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=AML_Report.".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=AML_Report.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>AML Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Date".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$systemCode.$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$manualCode.$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Trans Amount".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Sender Name".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Beneficiary Name".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Created By".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;

	$strFullHtml .= $strRowStart;
	$totalTrans =0;
	$totalLocal =0;
	foreach($amlReportData as $key => $val)
	{
	 	$transAmount = number_format($val["transAmount"],2,'.',',');
		$transAmount = str_replace(",","",$transAmount);
        $transDate = dateFormat($val["transDate"], "2");
		$transDate = str_replace(",","",$transDate);
	    if($val["createdBy"]!='CUSTOMER'){
			$CollCustomer = selectFrom("select firstName, lastName from customer where customerID=".$val["customerID"]); 
			$collben = selectFrom("select firstName,middleName,lastName, Phone from beneficiary where benID=".$val["benID"]);
			$sender_name = $CollCustomer["firstName"]." ".$CollCustomer["lastName"];
			$ben_name = $collben["firstName"]." ".$collben["lastName"];
         }elseif($val["createdBy"] == 'CUSTOMER')
			{
			   $CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id=".$val["customerID"]); 
			   $collben = selectFrom("select firstName,middleName,lastName, Phone from cm_beneficiary where benID=".$val["benID"]);
				$sender_name = $CollCustomer["FirstName"]." ".$CollCustomer["LastName"];
				$ben_name =$collben["firstName"]." ".$collben["lastName"];
			}
		 if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
			  {
			  $custContent = selectFrom("select c_id,username from cm_customer where c_id='".$val["customerID"]."'");
			  $createdBy = ucfirst($custContent["username"]);
			  }
			  else
			  {
			  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$val["custAgentID"]."'");
			  $createdBy = ucfirst($agentContent["name"]);
			  }
	   
		$strFullHtml .= $strColumnStart.$transDate." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["refNumberIM"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["refNumber"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["transStatus"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val['transAmount']." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$sender_name." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$ben_name." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$createdBy." ".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
	}
	   	   
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>