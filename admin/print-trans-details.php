<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$transID = (int) $_GET["transID"];

//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

global $isCorrespondent;
global $adminType;
$isCorrespondent  = $_SESSION["loggedUserData"]["isCorrespondent"];
$adminType  = $_SESSION["loggedUserData"]["adminType"];
$userID  = $_SESSION["loggedUserData"]["userID"];
//session_register["trans"];
if ($_SESSION["trans"]=='')
{
	$_SESSION["trans"]=$_POST['checkbox'];
}

?>
<html>
					<head>
					<title>Print Transaction Details</title>
					<script language="javascript" src="./javascript/functions.js"></script>
					<link href="images/interface.css" rel="stylesheet" type="text/css">
					<script language="javascript" src="./styles/admin.js"></script>
					<style type="text/css">
					<!--
					.style2 {
						color: #6699CC;
						font-weight: bold;
					}
					-->
						</style>
					<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
					<body>
					 <form name="Print" method="post" action="print-trans-details.php">
					<table width="103%" border="0" cellspacing="1" cellpadding="5">
					  <!-- tr>
						
    <td class="topbar"><b><font color="#FFFFFF" size="2">Print transaction details.</font></b></td>
					  </tr -->
					  
						<tr>
						  <td align="center">
						 
				

<?



if($_POST["Submit"] != "")
{
//echo $_POST["Submit"];
	if($_POST["Submit"] == "Print")
	{
		
		//echo $trans = is_array($_POST["checkbox"]) ? implode(",",$_POST["checkbox"]) : "";
	   if(count($_POST['checkbox']))
	   {
	   		$_SESSION["trans"]=$_POST['checkbox'];
		}
		if(count($_SESSION["trans"]))
		{
			foreach($_SESSION["trans"] as $value)
			{
				//echo $value	;		
				$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$value."'");
				
				$createdBy = $contentTrans["createdBy"];
					if($createdBy != 'CUSTOMER')
					{ ?>
				
				
								<table width="700" border="0" bordercolor="#FF0000">
								  <tr>
									<td align="center"><fieldset>
									<legend class="style2">Sender Company</legend>
									<br>
									<table border="0" align="center">
									  <tr>
										
					  <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
									  </tr>
									</table>
									<br>
									</fieldset></td>
								  </tr>		  
								  <tr>
									<td><fieldset>
									<legend class="style2">Sender Agent Details </legend>
									<table width="650" border="0">
									  <? 
									$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["custAgentID"] . "'";
									$senderAgentContent = selectFrom($querysenderAgent);
									if($senderAgentContent["userID"] != "")
									{
								?>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Agent Name</font></td>
										<td width="200"><? echo $senderAgentContent["name"]?> </td>
										<td width="100" align="right"><font color="#005b90">Contact Person </font></td>
										<td width="200"><? echo $senderAgentContent["agentContactPerson"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Address</font></td>
										<td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Company</font></td>
										<td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
										<td width="100" align="right"><font color="#005b90">Country</font></td>
										<td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Phone</font></td>
										<td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
										<td width="100" align="right"><font color="#005b90">Email</font></td>
										<td width="200"><? echo $senderAgentContent["email"]?></td>
									  </tr>
									  <?
									  }
									  ?>
									</table>
									</fieldset></td>
								  </tr>
								  
								  <tr>
									<td><fieldset>
									  <legend class="style2">Sender Details </legend>
									  <table width="650" border="0">
										<? 
								
									$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
									$customerContent = selectFrom($queryCust);
									if($customerContent["customerID"] != "")
									{
								?>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
										  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Address</font></td>
										  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
										  <td width="200"><? echo $customerContent["Zip"]?></td>
										  <td width="100" align="right"><font color="#005b90">Country</font></td>
										  <td width="200"><? echo $customerContent["Country"]?></td>
										</tr>
										<tr>
											<td width="100" align="right"><font color="#005b90">Country</font></td>
										  <td width="200"><? echo $customerContent["Country"]?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Phone</font></td>
										  <td width="200"><? echo $customerContent["Phone"]?></td>
										  <td width="100" align="right"><font color="#005b90">Email</font></td>
										  <td width="200"><? echo $customerContent["Email"]?></td>
										</tr>
										<?
									  }
									  ?>
									</table>
									  </fieldset></td>
								  </tr>
								 <!-- <tr>
									<td><fieldset>
									<legend class="style2">Beneficiary Agent Details</legend>
									<table width="650" border="0">
									  <? 
									/*$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["benAgentID"] . "'";
									$senderAgentContent = selectFrom($querysenderAgent);
									if($senderAgentContent["userID"] != "")
									{*/
								?>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Agent Name</font></td>
										<td><? //echo $senderAgentContent["name"]?> </td>
										<td align="right"><font color="#005b90">Contact Person </font></td>
										<td><? //echo $senderAgentContent["agentContactPerson"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Address</font></td>
										<td colspan="3"><? //echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Company</font></td>
										<td width="200"><? //echo $senderAgentContent["agentCompany"]?></td>
										<td width="100" align="right"><font color="#005b90">Country</font></td>
										<td width="200"><? //echo $senderAgentContent["agentCountry"]?></td>
									  </tr>
									  <tr>
										<td width="150" align="right"><font color="#005b90">Phone</font></td>
										<td width="200"><? //echo $senderAgentContent["agentPhone"]?></td>
										<td width="100" align="right"><font color="#005b90">Email</font></td>
										<td width="200"><? //echo $senderAgentContent["email"]?></td>
									  </tr>
									  <?
									 // }
									  ?>
									</table>
									</fieldset></td>
								  </tr>-->
								  <tr>
									<td><fieldset>
									  <legend class="style2">Beneficiary Details </legend>
											
									<table width="650" border="0" bordercolor="#006600">
									  <? 
								
									$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
									$benificiaryContent = selectFrom($queryBen);
									if($benificiaryContent["benID"] != "")
									{
								?>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Beneficiary 
										  Name</font></td>
										<td height="20" colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
										<td width="200" height="20">&nbsp;</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
										<td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
										<td width="200" height="20">&nbsp;</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Postal 
										  / Zip Code</font></td>
										<td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
										<td width="100" align="right"><font color="#005b90">Country</font></td>
										<td width="200"><? echo $benificiaryContent["Country"]?> </td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
										<td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
										<td width="100" align="right"><font color="#005b90">Email</font></td>
										<td width="200"><? echo $benificiaryContent["Email"]?></td>
									  </tr>
									  <?
										}
									  if($contentTrans["transType"] == "Bank Transfer")
									  {
											$benBankDetails = selectFrom("select * from bankDetails where transID='".$transID."'");
											?>
									  <tr> 
										<td height="20" colspan="4" class="style2">Beneficiary Bank Details 
										</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Bank 
										  Name</font></td>
										<td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
										<td width="100" align="right"><font color="#005b90">Acc Number</font></td>
										<td width="200"><? echo $benBankDetails["accNo"]; ?> </td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Branch 
										  Code</font></td>
										<td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?> 
										</td>
										<td width="100" align="right"><font color="#005b90"> 
										  <?
										if($benificiaryContent["Country"] == "United States")
										{
											echo "ABA Number*";
										}
										elseif($benificiaryContent["Country"] == "Brazil")
										{
											echo  "CPF Number*";
										}
										?>
										  </font></td>
										<td width="200"> 
										  <?
										if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
										{
										?>
										  <? echo $benBankDetails["ABACPF"]; ?> 
										  <?
										}
										?>
										</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Branch 
										  Address</font> </td>
										<td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?> 
										</td>
										<td width="100" height="20"  align="right">&nbsp;</td>
										<td width="200" height="20">&nbsp;</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right">&nbsp;</td>
										<td width="200" height="20">&nbsp;</td>
										<td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
										<td width="200" height="20" title="For european Countries only">&nbsp;</td>
									  </tr>
									  <?
									 }
									  if($contentTrans["transType"] == "Pick up")
									  {
									 $_SESSION["collectionPoint"] = $contentTrans["collectionPointID"];
									$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPoint"] . "'";
									$senderAgentContent = selectFrom($queryCust);
									$_SESSION["collectionPoint"] = "";
									if($senderAgentContent["cp_id"] != "")
									{
								?>
									  <tr> 
										<td height="20" colspan="4" class="style2">Collection Point Details 
										</td>
									  </tr>
									  <tr> 
										<td width="150" align="right"><font color="#005b90">Agent Name</font></td>
										<td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?> 
										</td>
										<td width="100" align="right"><font color="#005b90">Contact Person 
										  </font></td>
										<td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
									  </tr>
									  <tr> 
										<td width="150" align="right"><font color="#005b90">Address</font></td>
										<td colspan="3" ><? echo $senderAgentContent["cp_branch_address"]?></td>
									  </tr>
									  <tr> 
										<td width="150" align="right"><font color="#005b90">Company</font></td>
										<td width="200"><? echo $senderAgentContent["cp_corresspondent_name"]?></td>
										<td width="100" align="right"><font color="#005b90">Country</font></td>
										<td width="200"><? echo $senderAgentContent["cp_country"]?></td>
									  </tr>
									  <tr> 
										<td width="150" align="right"><font color="#005b90">Phone</font></td>
										<td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
										<td width="100" align="right"><font color="#005b90">City</font></td>
										<td width="200"><? echo $senderAgentContent["cp_city"]?></td>
									  </tr>
									  <tr> 
										<td width="150" align="right"><font color="#005b90">Branch Code</font></td>
										<td width="200" colspan="3"><? echo $senderAgentContent["cp_ria_branch_code"]?></td>
						
									  </tr>
						
									  <?
									  }
									  
									  
									  }			 
						 ?>
									</table>
									  </fieldset></td>
								  </tr>
								  <tr>
									<td><fieldset>
									<legend class="style2">Amount Details </legend>
									
									  
									<table width="650" border="0">
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90"><font color="#005b90"><? echo $manualCode;?></font></font></td>
										<td height="20" width="200"><? echo $contentTrans["refNumber"]?></td>
										<td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
										<td width="200" height="20" ><? echo $contentTrans["transAmount"]?> 
										</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Exchange 
										  Rate</font></td>
										<td width="200" height="20"><? echo $contentTrans["exchangeRate"]?> 
										</td>
										<td width="100" height="20" align="right"><font color="#005b90">Local 
										  Amount</font></td>
										<td width="200" height="20"><? echo $contentTrans["localAmount"]?> 
										  <? 
											
										  if(is_numeric($contentTrans["currencyTo"]))
											  echo "";
										  else
											  echo "in ".$contentTrans["currencyTo"]; 
										  ?> 
										  <? 
										  /*$currencyTo = $contentTrans["currencyTo"];
										  if(is_numeric($currencyTo))
											{
												 $contentExchagneRate = selectFrom("select *  from exchangerate where erID  = '".$currencyTo."'");								
												echo $_SESSION["currencyTo"] = $contentExchagneRate["currency"];
											}*/
										  ?>
										  </td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
										<td width="200" height="20"><? echo $contentTrans["IMFee"]?> </td>
										<td width="100" height="20" align="right"><font color="#005b90">Total 
										  Amount</font></td>
										<td width="200" height="20"><? echo $contentTrans["totalAmount"]?> 
										</td>
									  </tr>
									  <tr> 
										<td width="150" height="20" align="right"><font color="#005b90">Transaction 
										  Purpose</font></td>
										<td width="200" height="20"><? echo $contentTrans["transactionPurpose"]?> 
										</td>
										<td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
										<td width="200"><? echo $contentTrans["fundSources"]?> </td>
									  </tr>
									  <tr> 
										
					<td width="150" height="39" align="right"><font color="#005b90">Money 
					  Paid</font></td>
										
					<td width="200" height="39"><? echo $contentTrans["moneyPaid"]?> 
					</td>
										<td align="right">&nbsp;</td>
										<td>&nbsp;</td>
									  </tr>
									</table>
									
									</fieldset></td>
								  </tr>
								  <tr>
									<td><fieldset>
									<legend class="style2">Transaction Details </legend>
									<table width="650" border="0">
									  <tr>
										<td width="146" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
										<td width="196" height="20"><? echo $contentTrans["transType"]?></td>
										<td width="148" align="right"><font color="#005b90">Status</font></td>
										<td width="142"><? echo $contentTrans["transStatus"]?></td>
									  </tr>
									  <tr>
										<td width="146" height="20" align="right"><font color="#005b90"><? echo $systemCode;?></font></td>
										<td width="196" height="20"><? echo $contentTrans["refNumberIM"]?>
										</td>
										<td width="148" height="20" align="right"><font color="#005b90"><? echo $manualCode;?> </font> </td>
										<td width="142" height="20"><? echo $contentTrans["refNumber"]; ?></td>
									  </tr>
									  <tr>
										<td width="146" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
										<td width="196" height="20"><? 
										$TansDate = $contentTrans['transDate'];
										if($TansDate != '0000-00-00 00:00:00')
											echo date("F j, Y", strtotime("$TansDate"));
										
										?>
										</td>
										<td width="148" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
										<td width="142" height="20"><? 
										$authoriseDate = $contentTrans['authoriseDate'];
										if($authoriseDate != '0000-00-00 00:00:00')
											echo date("F j, Y", strtotime("$authoriseDate"));				
										?></td>
									  </tr>				  
									</table>
									</fieldset></td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
								  </tr>
								</table>
						<?
						}
						else
						{
						?>
						
								<table width="700" border="0" bordercolor="#FF0000">
								  <tr>
									<td><fieldset>
									<legend class="style2">Transaction Details </legend>
									<table width="650" border="0">
									  <tr>
										<td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
										<td width="200" height="20"><? echo $contentTrans["transType"]?></td>
										<td width="100" align="right"><font color="#005b90">Status</font></td>
										<td width="200"><? echo $contentTrans["transStatus"]?></td>
									  </tr>
									  <tr>
										<td width="150" height="20" align="right"><font color="#005b90">Transaction No </font></td>
										<td width="200" height="20"><? echo $contentTrans["refNumberIM"]?>
										</td>
										<td width="100" height="20" align="right">&nbsp;</td>
										<td width="200" height="20">&nbsp;</td>
									  </tr>
									  <tr>
										<td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
										<td width="200" height="20"><? 
										$TansDate = $contentTrans['transDate'];
										if($TansDate != '0000-00-00 00:00:00')
											echo date("F j, Y", strtotime("$TansDate"));
										
										?>
										</td>
										<td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
										<td width="150" height="20"><? 
										 
										$authoriseDate = $contentTrans['authoriseDate'];
										if($authoriseDate != '0000-00-00 00:00:00')
										{
											if($authoriseDate != '0000-00-00 00:00:00')
												echo date("F j, Y", strtotime("$authoriseDate"));				
										}
										?></td>
									  </tr>				  
									</table>
									</fieldset></td>
								  </tr>
								  <tr>
									<td align="center"><fieldset>
									<legend class="style2">Sender Company</legend>
									<br>
									<table border="0" align="center">
									  <tr>
										
					  <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
									  </tr>
									</table>
									<br>
									</fieldset></td>
								  </tr>
								  <tr>
									<td><fieldset>
									  <legend class="style2">Customer Details </legend>
									  <table width="650" border="0">
										<? 
								
									$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
									$customerContent = selectFrom($queryCust);
									if($customerContent["c_id"] != "")
									{
								?>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
										  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Address</font></td>
										  <td colspan="3"><? echo $customerContent["c_address"] . " " . $customerContent["c_address2"]?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
										  <td width="200"><? echo $customerContent["c_zip"]?></td>
										  <td width="100" align="right"><font color="#005b90">Country</font></td>
										  <td width="200"><? echo $customerContent["c_country"]?></td>
										</tr>
										<tr>
										  <td width="150" align="right"><font color="#005b90">Phone</font></td>
										  <td width="200"><? echo $customerContent["c_phone"]?></td>
										  <td width="100" align="right"><font color="#005b90">Email</font></td>
										  <td width="200"><? echo $customerContent["c_email"]?></td>
										</tr>
										<?
									  }
									  ?>
									</table>
									  </fieldset></td>
								  </tr>
											  
								  <tr>
									<td><fieldset>
									  <legend class="style2">Beneficiary Details </legend>
											<table width="650" border="0" bordercolor="#006600">
											  <? 
								
									$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
									$benificiaryContent = selectFrom($queryBen);
									if($benificiaryContent["benID"] != "")
									{
								?>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Beneficiary Name</font></td>
												<td height="20" colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
												<td width="200" height="20">&nbsp;</td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
												<td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
												<td width="200" height="20">&nbsp;</td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Postal / Zip Code</font></td>
												<td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
												<td width="100" align="right"><font color="#005b90">Country</font></td>
												<td width="200"><? echo $benificiaryContent["Country"]?>      </td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
												<td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
												<td width="100" align="right"><font color="#005b90">Email</font></td>
												<td width="200"><? echo $benificiaryContent["Email"]?></td>
											  </tr>
											  <?
										}
									  if($contentTrans["transType"] == "Bank Transfer")
									  {
											$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
											?>
											  <tr>
												<td colspan="4" class="style2">Beneficiary Bank Details </td>
												
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
												<td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
												<td width="100" align="right"><font color="#005b90">Acc Number</font></td>
												<td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
												<td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
												<td width="100" align="right"><font color="#005b90">
												  <?
										if($benificiaryContent["Country"] == "United States")
										{
											echo "ABA Number*";
										}
										elseif($benificiaryContent["Country"] == "Brazil")
										{
											echo  "CPF Number*";
										}
										?>
												  </font></td>
												<td width="200"><?
										if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
										{
										?>
												  <? echo $benBankDetails["ABACPF"]; ?>
												  <?
										}
										?></td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
												<td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
						<td width="100" height="20"  align="right"><font color="#005b90">&nbsp;</font></td>
										<td width="200" height="20">&nbsp;</td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font>&nbsp;</td>
												<td width="200" height="20">&nbsp;  </td>
												<td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
												<td width="200" height="20" title="For european Countries only">&nbsp;</td>
											  </tr>
						 <?
						  }
						
									  if($contentTrans["transType"] == "Pick up")
									  {
											$benAgentID = $contentTrans["benAgentID"];
											$collectionPointID = $contentTrans["collectionPointID"];
											$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
											$senderAgentContent = selectFrom($queryCust);			
						
											?>
											  <tr>
												<td colspan="4" class="style2">Collection Point Details </td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Agent Name</font></td>
												<td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
												<td width="100" align="right"><font color="#005b90">Address</font></td>
												<td width="200"  valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?>                        </td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Contact Person </font></td>
												<td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
												<td width="150" align="right"><font color="#005b90">Branch Code</font></td>
												<td width="200" colspan="3"><? echo $senderAgentContent["cp_ria_branch_code"]?></td>
											  </tr>
											  <tr>
												<td width="150" height="20" align="right"><font color="#005b90">Compnay </font> </td>
												<td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
												<td width="100" align="right"><font color="#005b90">Country</font></td>
												<td width="200"><? echo $senderAgentContent["cp_country"]?></td>                       
											  </tr>
											  <tr> 
												<td width="150" align="right"><font color="#005b90">Phone</font></td>
												<td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
												<td width="100" height="20" align="right"><font color="#005b90">City</font></td>
												<td width="200" height="20"><?  echo $senderAgentContent["cp_city"]; ?></td>
											  </tr>
											  
						 <?
						  }
						  ?>
									</table>
									  </fieldset></td>
								  </tr>
								  <tr>
									<td><fieldset>
									<legend class="style2">Amount Details </legend>
									
									  <table width="650" border="0">
									  <tr>
										<td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
										<td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
										<td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
										<td width="200" height="20" ><? echo $contentTrans["transAmount"]?>                </td>
									  </tr>
									  <tr>
										<td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
										<td height="20"><? echo $contentTrans["IMFee"]?> </td>
										<td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
										<td width="200" height="20"><? echo $contentTrans["localAmount"]?>                   
										<? 
										  if(is_numeric($contentTrans["currencyTo"]))
											  echo "";
										  else
											  echo "in ".$contentTrans["currencyTo"]; 
										  ?>
													  </td>
									  </tr>
									  <tr>
										<td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
										<td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
										<td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
										<td width="200" height="20"><? echo $contentTrans["totalAmount"]?>                </td>
									  </tr>
									  <tr>
										<td height="20" align="right"><font color="#005b90">Money Paid</font></td>
										<td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
										<td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
														<td width="200">&nbsp;
										</td>
									  </tr>
									  <tr>
										<td width="150" height="20" align="right">&nbsp;</td>
										<td width="200" height="20">&nbsp;</td>
										<td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
										<td width="200">&nbsp;</td>
									  </tr>
									</table>
									</fieldset></td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
								  </tr>
								  
						  <?
						  if($contentTrans["moneyPaid"] == 'By Credit Card' || $contentTrans["moneyPaid"] == 'By Debit Card')
						  {
						  $benCreditCardDetail = selectFrom("select * from  cm_cust_credit_card where customerID  ='".$contentTrans["customerID"]."'");
						  ?>
											  
											  <td colspan="4"><fieldset>
											<legend class="style2">Credit/Debit Card Details </legend>
											  
											<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
											  
											  <tr>
												<td width="143" height="20" align="right"><font color="#005b90">Card Type*</font> </td>
												<td width="196" height="20" align="left">
												<? echo $benCreditCardDetail["cardName"];?>
												</td>
												<td width="112" height="20" align="right"><font color="#005b90">Card No*</font></td>
												<td width="183" height="20" align="left">
												<? 
												$credCardNo = "************". substr($benCreditCardDetail["cardNo"],12,4);
												echo $credCardNo;?>
												</td>
											  </tr>
											  <tr>
												<td width="143" height="20" align="right"><font color="#005b90">Expiry Date*</font></td>
												<td width="196" height="20" align="left">
												<? 
												echo $ExpirtDagte =  $benCreditCardDetail["expiryDate"];
												//echo date("F j, Y", strtotime("$ExpirtDagte"));
												?>
												</td>
												<td width="112" height="20" align="right" title="For european Countries only"><font color="#005b90">Card CVV or CV2*  </font></td>
												<td width="183" height="20" align="left" title="For european Countries only">
												<? echo $benCreditCardDetail["cardCVV"];?>
												</td>
											  </tr>
											</table>					  
											  </fieldset>
											  </td>					  
											  </tr>
											  
						<?
						}
						?>					  			  
								  
								</table></td>
							
				<? }
			}
		} 
		else {
			echo("No Transaction Found");
		}
	}
}

?>
</td>
						</tr>
					
					</table>
					
									
        
  <div align="center">
    <input type="submit" name="Submit" value="Print" onClick="print()">
  </div>
</form>
					</body>
					</html>