<?php
	session_start();
/**
*
* @package Payex
* @subpackage Trading 
*
* Short Description
* This page is performing the concile, transactions and trades listing,trade-commission-report.php listing operation called.
* 
*/	
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	include ("double-entry-functions.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	
		$sql_agent_QuertT='';
		$sql_agent_QuertP='';
		if($agentType=='SUPA' || $agentType=='SUBA'  )
		{
		$sql_agent_QuertT="  AND  custAgentId ='".$userID."'";
		$sql_agent_QuertP="  AND  created_by ='".$userID."'";
		
		
		}
		
		
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";

	$err = "";
// This control comes from trade-trans-deal.php on Making a Deal between Traded and Not-Traded
if($_REQUEST["btnAction"] == "makeDeal")
{

//\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\
	$transIDArray	= $_REQUEST["transIDs"];
	$payIDArray		= $_REQUEST["payIDs"];
	if(count($transIDArray)>0 && is_array($transIDArray)){
		$amount = 0;
		$amountTrade = 0;
		$tradeRate = 0;
		$transRate = 0;
		$transRateArr = array();
		$transAmountArr = array();
		$transRefNumberArr = array();
		$batchID = time();
		for($s=0;$s<count($transIDArray);$s++){
			$transID = $transIDArray[$s];			
			$err = "Operation performed successfully.";
		
			$sqlT = "SELECT refNumberIM,localAmount,transAmount,currencyFrom,currencyTo,exchangeRate from ".TBL_TRANSACTIONS." WHERE transID = '" .$transID."'";
			$transRS = selectFrom($sqlT);
			$transAmount += $transRS["transAmount"];
			$amount += $transRS["localAmount"];
			$transR += $transRS["exchangeRate"];
			$transRateArr[] = $transRS["exchangeRate"];
			//redirect("unres-payments.php?type=" . $_GET["type"] . "&msg=" . urlencode($err));	
			$currencyFrom = $transRS["currencyFrom"];
			$currencyTo = $transRS["currencyTo"];
			$transRate += $transRS["exchangeRate"];
			$transAmountArr[] = $transRS["transAmount"];
			$transRefNumberArr[] = $transRS["refNumberIM"];
		}
	}
	// Number of iteration should be equal to the number of  transactions
			
	if($_REQUEST["userTypes"]!="" && count($payIDArray)>0){
		$counter = 0;
		for($b=0;$b<count($payIDArray);$b++){
			$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
//debug($queryMatch);
update($queryMatch);
			$counter = $counter+1;
			$sqlTrade 	= "SELECT exchangeRate,amountSell from account_details WHERE acid='".$payIDArray[$b]."'";
			$tradeRS	= selectFrom($sqlTrade);
			$tradeRate	+= $tradeRS["exchangeRate"];
			$amountTrade+= $tradeRS["amountSell"];
		}
	}
	else{
		$err = "Operation was not performed successfully. Re-Login please.";
	}
	
	if($amount > $amountTrade){
	/*
	* if amount of transactions is greater than the amount of trades
	*/
		for($s=0;$s<count($transIDArray);$s++){
			$transID = $transIDArray[$s];
			$transRefNumber = $transRefNumberArr[$s];
	update("update transactions set isTraded  = 'Y' where transID = '".$transID."'");
			$sqlT = "SELECT localAmount,transAmount,currencyFrom,currencyTo,exchangeRate from ".TBL_TRANSACTIONS." WHERE transID = '" .$transID."'";
//debug($sqlT);
			$transRS = selectFrom($sqlT);
			$transAmount = $transRS["transAmount"];
			$localAmount = $transRS["localAmount"];
			$exchangeRate = $transRS["exchangeRate"];
			$currencyFrom = $transRS["currencyFrom"];
			$currencyTo = $transRS["currencyTo"];
			/**
			*
			* SQL query to get the total resolved amount of transaction from tradebatchhistory table
			* to resolve the remaining amount if there is any
			*/
			$sqlTResolved = "SELECT sum(transResolveAmount) as totalResolved  from tradeBatchHistory WHERE transIDs = '" .$transID."'";
			$transTRSD = selectFrom($sqlTResolved);
			$remainingResolved = $localAmount - $transTRSD["totalResolved"];
			
			if($remainingResolved > 0){			
				if(count($payIDArray)>0){
					$counter = 0;
					for($b=0;$b<count($payIDArray);$b++){
						if($remainingResolved < 1){
							continue;
						}
						$counter = $counter+1;
						$sqlTrade 	= "SELECT exchangeRate,amountSell from account_details WHERE acid='".$payIDArray[$b]."'";
						$tradeRS	= selectFrom($sqlTrade);
						$tradeRate	= $tradeRS["exchangeRate"];
						/**
						*
						* SQL query to get the total resolved amount of trade from tradebatchhistory table
						* to resolve the remaining amount if there is any
						*/
						$sqlTradeResolved = "SELECT sum(tradeResolveAmount) as totalResolved  from tradeBatchHistory WHERE tradeIDs = '" .$payIDArray[$b]."'";
						$transTradeRSD = selectFrom($sqlTradeResolved);
						if($tradeRS["amountSell"] > $transTradeRSD["totalResolved"]){
							$remainingTradeResolved = $tradeRS["amountSell"] - $transTradeRSD["totalResolved"];
							if($remainingResolved ==  $remainingTradeResolved){
								$transAmount = $remainingResolved / $exchangeRate;
	
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
	
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);
								$remainingResolved = 0;
							}else if($remainingResolved >  $remainingTradeResolved){
								$remainingResolved = $remainingResolved - $remainingTradeResolved;
								$transAmount = $remainingTradeResolved / $exchangeRate;
	
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm." WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
	
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingTradeResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);
	
							}else if($remainingResolved < $remainingTradeResolved){
								$remainingTradeResolved = $remainingTradeResolved - $remainingResolved;
								$transAmount = $remainingResolved / $exchangeRate;
	
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
	
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$remainingResolved = 0;
							}
						}
					}
				}
			}

		}	
		
	}else if($amount < $amountTrade){
	
//$err=($amount."-- < --".$amountTrade);
	/*
	* if amount of transactions is less than the amount of trades
	*/
		if(count($payIDArray)>0){
			$counter = 0;
			for($b=0;$b<count($payIDArray);$b++){
				$counter = $counter+1;
				$sqlTrade 	= "SELECT exchangeRate,amountSell from account_details WHERE acid='".$payIDArray[$b]."'";
				$tradeRS	= selectFrom($sqlTrade);
				$tradeRate	= $tradeRS["exchangeRate"];
				/**
				*
				* SQL query to get the total resolved amount of trade from tradebatchhistory table
				* to resolve the remaining amount if there is any
				*/
				$sqlTradeResolved = "SELECT sum(tradeResolveAmount) as totalResolved  from tradeBatchHistory WHERE tradeIDs = '" .$payIDArray[$b]."'";
				$transTradeRSD = selectFrom($sqlTradeResolved);
				if($tradeRS["amountSell"] > $transTradeRSD["totalResolved"]){
					$remainingTradeResolved = $tradeRS["amountSell"] - $transTradeRSD["totalResolved"];
					for($s=0;$s<count($transIDArray);$s++){
						if($remainingTradeResolved < 1){
							continue;
						}
						$transID = $transIDArray[$s];
						$transRefNumber = $transRefNumberArr[$s];
update("update transactions set isTraded  = 'Y' where transID = '".$transID."'");
						$sqlT = "SELECT localAmount,transAmount,currencyFrom,currencyTo,exchangeRate from ".TBL_TRANSACTIONS." WHERE transID = '" .$transID."'";
						$transRS = selectFrom($sqlT);
						$transAmount = $transRS["transAmount"];
						$localAmount = $transRS["localAmount"];
						$exchangeRate = $transRS["exchangeRate"];
						$currencyFrom = $transRS["currencyFrom"];
						$currencyTo = $transRS["currencyTo"];
						/**
						*
						* SQL query to get the total resolved amount of transaction from tradebatchhistory table
						* to resolve the remaining amount if there is any
						*/
						$sqlTResolved = "SELECT sum(transResolveAmount) as totalResolved  from tradeBatchHistory WHERE transIDs = '" .$transID."'";
						$transTRSD = selectFrom($sqlTResolved);
						$remainingResolved = $localAmount - $transTRSD["totalResolved"];
						
						if($remainingResolved > 0){
							if($remainingResolved ==  $remainingTradeResolved){
								$transAmount = $remainingResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);	
								$remainingTradeResolved = 0;
							}else if($remainingResolved >  $remainingTradeResolved){
								$remainingResolved = $remainingResolved - $remainingTradeResolved;
								$transAmount = $remainingTradeResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm." WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingTradeResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);
								$remainingTradeResolved = 0;
							}else if($remainingResolved < $remainingTradeResolved){
								$remainingTradeResolved = $remainingTradeResolved - $remainingResolved;
								$transAmount = $remainingResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
				
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
							}
			
						
						}
						/**
						* End if $remainingResolved > 0
						*/
					}
							
				}
				/**
				* End if $tradeRS["amountSell"] > $transTradeRSD["totalResolved"]
				*/
			}
		}
	
	}else if($amount == $amountTrade){
/* $err=($amount." == ".$amountTrade);
exit; */
	/*
	* if amount of transactions is equal to the amount of trades
	*/
		if(count($payIDArray)>0){
			$counter = 0;
			for($b=0;$b<count($payIDArray);$b++){
				$counter = $counter+1;
				$sqlTrade 	= "SELECT exchangeRate,amountSell from account_details WHERE acid='".$payIDArray[$b]."'";
				$tradeRS	= selectFrom($sqlTrade);
				$tradeRate	= $tradeRS["exchangeRate"];
				/**
				*
				* SQL query to get the total resolved amount of trade from tradebatchhistory table
				* to resolve the remaining amount if there is any
				*/
				$sqlTradeResolved = "SELECT sum(tradeResolveAmount) as totalResolved  from tradeBatchHistory WHERE tradeIDs = '" .$payIDArray[$b]."'";
				$transTradeRSD = selectFrom($sqlTradeResolved);
				if($tradeRS["amountSell"] > $transTradeRSD["totalResolved"]){
					$remainingTradeResolved = $tradeRS["amountSell"] - $transTradeRSD["totalResolved"];
					for($s=0;$s<count($transIDArray);$s++){
						if($remainingTradeResolved < 1){
							continue;
						}
						$transID = $transIDArray[$s];
						$transRefNumber = $transRefNumberArr[$s];
update("update transactions set isTraded  = 'Y' where transID = '".$transID."'");
						$sqlT = "SELECT localAmount,transAmount,currencyFrom,currencyTo,exchangeRate from ".TBL_TRANSACTIONS." WHERE transID = '" .$transID."'";
						$transRS = selectFrom($sqlT);
						$transAmount = $transRS["transAmount"];
						$localAmount = $transRS["localAmount"];
						$exchangeRate = $transRS["exchangeRate"];
						$currencyFrom = $transRS["currencyFrom"];
						$currencyTo = $transRS["currencyTo"];
						/**
						*
						* SQL query to get the total resolved amount of transaction from tradebatchhistory table
						* to resolve the remaining amount if there is any
						*/
						$sqlTResolved = "SELECT sum(transResolveAmount) as totalResolved  from tradeBatchHistory WHERE transIDs = '" .$transID."'";
						$transTRSD = selectFrom($sqlTResolved);
						$remainingResolved = $localAmount - $transTRSD["totalResolved"];
						
						if($remainingResolved > 0){
							if($remainingResolved ==  $remainingTradeResolved){
								$transAmount = $remainingResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);	
								$remainingTradeResolved = 0;
							}else if($remainingResolved >  $remainingTradeResolved){
								$remainingResolved = $remainingResolved - $remainingTradeResolved;
								$transAmount = $remainingTradeResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm." WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingTradeResolved."',
																				'".$remainingTradeResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
								$queryMatch = "update account_details set isTraded = 'Y',tradeBatchID='".$batchID."' where acid = '".$payIDArray[$b]."' ";
								update($queryMatch);
								$remainingTradeResolved = 0;
							}else if($remainingResolved < $remainingTradeResolved){
								$remainingTradeResolved = $remainingTradeResolved - $remainingResolved;
								$transAmount = $remainingResolved / $exchangeRate;
				
								$returnComm = ($tradeRate - $exchangeRate) * $transAmount;
								update("UPDATE ".TBL_TRANSACTIONS." SET tradeProfitLoss = tradeProfitLoss + ".$returnComm.",isTraded = 'Y' WHERE transID='".$transID."' ");
								$profitLoss = number_format($returnComm,"4",".","");
				
								$tradeInsert = "INSERT INTO tradeBatchHistory(
																				tradeIDs,
																				transIDs ,
																				amount ,
																				currencyBuy,
																				currencySell,
				
																				transRate,
																				tradeRate,
																				batchID,
																				profitLoss,
																				dated,
																				transResolveAmount,
																				tradeResolveAmount,
																				tranRefNumberIM 
																			)
																		VALUES(
																				'".$payIDArray[$b]."',
																				'".$transID."',
																				'".$transAmount."',
																				'".$currencyFrom."',
																				'".$currencyTo."',
																				'".$exchangeRate."',
																				'".$tradeRate."',
																				'".$batchID."',
																				'".$profitLoss."',
																				'".date("Y-m-d")."',
																				'".$remainingResolved."',
																				'".$remainingResolved."',
																				'".$transRefNumber."'
																		)
												";
							
								insertInto($tradeInsert);
							}
			
						
						}
						/**
						* End if $remainingResolved > 0
						*/
					}
							
				}
				/**
				* End if $tradeRS["amountSell"] > $transTradeRSD["totalResolved"]
				*/
			}
		}
	}
	/*
	* End if amount of transactions is equal to the amount of trades
	*/

	/*$transIDs = implode(",",$transIDArray);
	$tradeIDs = implode(",",$payIDArray);
	$arguments = array("tradeRates"=>$tradeRate,
						"transRates"=>$transRateArr,
						"transAmounts"=>$transAmountArr,
						"transIDs"=>$transIDArray
					);
	$profitLoss = calculateTradeProfitLoss($arguments);
	$tradeInsert = "INSERT INTO tradeBatchHistory(
													tradeIDs,
													transIDs ,
													amount ,
													currencyBuy,
													currencySell,
													transRate,
													tradeRate,
													batchID,
													profitLoss,
													dated 
												)
											VALUES(
													'".$tradeIDs."',
													'".$transIDs."',
													'".$transAmount."',
													'".$currencyFrom."',
													'".$currencyTo."',
													'".$transRate."',
													'".$tradeRate."',
													'".$batchID."',
													'".$profitLoss."',
													'".date("Y-m-d")."'
											)
					";

	insertInto($tradeInsert);*/
	echo "<input type='hidden' name='payListMatch' id='payListMatch' value='".$err."'>";
	exit;
}


if($_REQUEST["btnAction"] == "resolve"){
	$transIDArray	= $_REQUEST["transIDs"];
for($q=0;$q<count($transIDArray);$q++){
			$transID = $transIDArray[$q];
	update("update transactions set isTraded  = 'Y' where transID = '".$transID."'");
	
}
echo "<input type='hidden' name='payListMatch' id='payListMatch' value='Transactions has been successfully Resovled.'>";
	exit;
}
if($_REQUEST["btnAction"] == "loadDeals"){
	echo "E";
	exit;
}
if($_REQUEST["getGrid"] == "tradeCommList"){
	if($_REQUEST["getGrid"] == "tradeCommList" ){
		$searchName	= $_REQUEST["searchName"];
		$amount		= $_REQUEST["amount"];
		$currency	= $_REQUEST["currencyBuy"];
		$from	  	= $_REQUEST["from"];
		$to		  	= $_REQUEST["to"];
		$tradenumber1 = $_REQUEST["tradenumber1"];
		$referencenumber1 = $_REQUEST["referencenumber1"];
		$agentUsername = $_REQUEST["agentUsername"];
		$customerName = $_REQUEST["customerName"];
		
		$whrClause	= "";
		$fromWhr	= "";
		$toWhr		= "";
		
		if(!empty($tradenumber1)){
			$tradeNumberArr = explode('-',$tradenumber1);
			if(count($tradeNumberArr) > 1){
				$whrClause .= " AND tradeIDs='".$tradeNumberArr[1]."'";
			}else{
				$whrClause .= " AND tradeIDs='".$tradeNumberArr[0]."'";
			}
		}
		if(!empty($referencenumber1)){
			$whrClause .= " AND tranRefNumberIM='".$referencenumber1."'";
		}
		if(!empty($searchName))
			$searchName = " AND '".$searchName."'";
		if(!empty($amount))
			$whrClause .= " AND amount = '".$amount."'";
		if(!empty($currency))
			$whrClause .= " AND currencyBuy = '".$currency."'";
		if (!empty($from))
		{
			$dDateF = explode("/",$from);
			if(count($dDateF) == 3)
				$fromWhr = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
		}
		if (!empty($to))
		{
			$dDateT = explode("/",$to);
			if(count($dDateT) == 3)
				$toWhr = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
		}	
	
		if ($fromWhr != "" || $toWhr != "")
		{
			if ($fromWhr != "" && $toWhr != "")
			{
				$whrClause .= " and (dated >= '$fromWhr' and dated <= '$toWhr') ";
			}
			elseif ($fromWhr != "")
			{
				$whrClause .= " and (dated >= '$fromWhr') ";	
			}
			elseif ($toWhr != "")
			{
				$whrClause .= " and (dated <= '$toWhr') ";	
			}	
		}
	}

	$sql = "SELECT * from tradeBatchHistory WHERE 1 ";
	if(!empty($whrClause) && !empty($sql))
		$sql	= $sql." ".$whrClause;
	if(!empty($sql)){
		$tradeCommissionRS = mysql_query($sql) or die(__LINE__.": ".mysql_error());
		$countTrade = mysql_num_rows($tradeCommissionRS);
		
		if($countTrade > 0)
		{
			$total_pages = ceil($countTrade / $limit);
		} else {
			$total_pages = 0;
		}
		
		if($page > $total_pages)
		{
			$page = $total_pages;
		}
		
		$start = $limit * $page - $limit; // do not put $limit*($page - 1)
		
		/**
		 * A patch to handle -ve value in case the $count=0, 
		 * because in this case the $start is set to a -ve value 
		 * which causes query to break.
		 *
		 * @author: Aslam Shahid
		 */
		if($start < 0)
		{
			$start = 0;
		}
		
		$sql .= " order by $sidx $sord LIMIT $start , $limit";

		$tradeCommissionRS = mysql_query($sql) or die(__LINE__.": ".mysql_error());
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $countTrade;
	
		$i=0;
		$sum=0;
		$profitlosstotal=0;
		while($rowT = mysql_fetch_array($tradeCommissionRS))
		{
			$amountValue    = number_format($rowT["amount"],2,".","");
			$transRateValue = number_format($rowT["transRate"],2,".","");
			$tradeRateValue = number_format($rowT["tradeRate"],2,".","");
			/**
			 *	Condition to set the format of amount to 2 and 
			 *	exchange rate to 4 if the config below is enabled
			 */
			if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
			{
				$amountValue    = number_format($rowT["amount"],2,".","");
				$transRateValue = number_format($rowT["transRate"],4,".","");
				$tradeRateValue = number_format($rowT["tradeRate"],4,".","");
			}
			$response->rows[$i]['id']  = $rowT["id"];
			$response->rows[$i]['cell'] = array(
										dateFormat($rowT["dated"], "2"),
										//$rowT["batchID"],
										$amountValue,
										$rowT["currencyBuy"],
										$transRateValue,
										$tradeRateValue,
										$rowT["currencySell"],
										number_format($rowT["profitLoss"],2,".",""),
										'Trade-'.$rowT["tradeIDs"],
										$rowT["tranRefNumberIM"]
									);
			$i++;
			$sum+=$amountValue;
			$sumTransRate+=$transRateValue;
			$sumTradeRate+=$tradeRateValue;
			$sum = number_format($sum, 2, '.', '');
			$sumDealRate = number_format($sumTransRate, 2, '.', '');
			$sumTradeRate = number_format($sumTradeRate, 2, '.', '');
			$avgDealRate = $sumDealRate / $i; 
			$avgTradeRate = $sumTradeRate / $i;
			$profitlosstotal+=number_format($rowT["profitLoss"],4,".","");
			//$profitInTrade =  $sumT+=(($tradeRateValue-$transRateValue)*$amountValue);
			//$profitInTrade = number_format($profitInTrade, 4, '.', '');
			//$profitInDeal  = $profitInTrade*$avgTransRate;
			
			$profitInDeal  = $profitlosstotal*$avgDealRate;
			$profitInDeal = number_format($profitInDeal, 4, '.', '');
			
			$profit_Loss = $profitInDeal - $profitlosstotal;
/* 
// formulas:
Profit in trade selling currency =  Sum[(trade rate-exchange) * Buying Amount of Deal contract]

Profit in deal selling currency =  Profit in trade selling currency* Avg rate of deal contracts */

		}
		$response->rows[$i]['cell'] = array(
										"Total",
										$sum,
										"",
										"Avg="." ".$avgDealRate,
										"Avg="." ".$avgTradeRate,
										"",
										$profit_Loss,
										"Profit-Trade="." ".$profitlosstotal,
										"Profit-Deal="." ".$profitInDeal
									);
		
		echo $response->encode($response); 
	}
	exit;
}


//	acid,transNumber,refNumberIM,description,accountSell,accountBuy,created
$query = "select * from account_details where isTraded = 'N' and Status NOT IN ('TC','TD') ";	

if($_REQUEST["Submit"] == "SearchPay")
{
	if ($_REQUEST["from"]!="")
		$fromJS = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$toJS = $_REQUEST["to"];
	if ($_REQUEST["amount1"]!="")
		$amount1JS = $_REQUEST["amount1"];
	if ($_REQUEST["tradenumber1"]!="")
		$arrVal = explode('-',$_REQUEST["tradenumber1"]);
		if(count($arrVal)>1){
			$tradenumber1JS = intval($arrVal[1]);
		}else{
			$tradenumber1JS = intval($_REQUEST["tradenumber1"]);
		}
	if ($_REQUEST["desc"]!="")
		$descJS = $_REQUEST["desc"];
	if ($_REQUEST["currencyTrade"]!="")
		$currencyP = $_REQUEST["currencyTrade"];
	if ($_REQUEST["originCurrencyTrade"]!="")
		$originCurrencyTrade = $_REQUEST["originCurrencyTrade"];
	if ($fromJS != "")
	{
		$dDateF = explode("/",$fromJS);
		if(count($dDateF) == 3)
			$fromJS = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($toJS != "")
	{
		$dDateT = explode("/",$toJS);
		if(count($dDateT) == 3)
			$toJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	if ($fromJS != "" || $toJS != "")
	{
		if ($fromJS != "" && $toJS != "")
		{
			$queryDated = " and (created >= '$fromJS 00:00:00' and created <= '$toJS 23:59:59') ";	
		}
		elseif ($fromJS != "")
		{
			$queryDated = " and (created >= '$fromJS 00:00:00') ";	
		}
		elseif ($toJS != "")
		{
			$queryDated = " and (created <= '$toJS 23:59:59') ";	
		}	
	}
	if ($amount1JS != "")
	{
		$queryAmount = " and amountBuy = '$amount1JS' ";	
	}
	if($tradenumber1JS != "")
	{
		$queryTrade = " and acid = '$tradenumber1JS' ";	
	}
	if ($descJS != "")
	{
		$queryDesc = " and (description like '%$descJS%'  OR transNumber like '%$descJS%')";	
	}	
	if ($queryDated != "")
	{
		$query .= $queryDated;
	}
	if ($queryrefNumber != "")
	{
		$query .= $queryrefNumber;
	}
	if ($queryAmount != "")
	{
		$query .= $queryAmount;
	}
	if($queryTrade != "")
	{
		$query .= $queryTrade;
	}
	if ($queryDesc != "")
	{
		$query .= $queryDesc;	
	}
	if ($currencyP != "")
	{
		$query .= " and currencyBuy = '".$currencyP."'";
	}
	
	if ($originCurrencyTrade != "")
	{
		$query .= " and currencySell = '".$originCurrencyTrade."'";
	}
}

//$queryT = "select transID,totalAmount,localAmount,refNumberIM,transDate,customerID,createdBy,senderBank,currencyFrom,currencyTo,exchangeRate,custAgentID from ". TBL_TRANSACTIONS." where isTraded = 'N' and transStatus <> 'Cancelled' and transStatus <> 'Pending' ";

$queryT = "select transID,totalAmount,localAmount,transType,refNumberIM,transDate,customerID,createdBy,senderBank,currencyFrom,currencyTo,exchangeRate,custAgentID,transStatus from ". TBL_TRANSACTIONS." where isTraded = 'N' and (transStatus = 'Processing'  or (transStatus = 'Credited' and transType='Topup') )  ";
//$queryT = "select transID,totalAmount,localAmount,refNumberIM,transDate,customerID,createdBy,senderBank,currencyFrom,currencyTo,exchangeRate,custAgentID from ". TBL_TRANSACTIONS." where isTraded = 'N' and transStatus NOT IN ('Pending','Cancelled')";
//exit($queryT);	
if($_REQUEST["Submit"] == "SearchTrans")
{
	if ($_REQUEST["fromT"]!="")
		$fromTJS = $_REQUEST["fromT"];
	if ($_REQUEST["toT"]!="")
		$toTJS = $_REQUEST["toT"];
	if ($_REQUEST["amountT"]!="")
		$amountTJS = $_REQUEST["amountT"];
	if ($_REQUEST["searchBy"]!="")
		$searchByTJS = $_REQUEST["searchBy"];
	if ($_REQUEST["searchName"]!="")
		$searchNameTJS = $_REQUEST["searchName"];
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];
	if ($_REQUEST["originCurrencyTrans"]!="")
		$originCurrencyTrans = $_REQUEST["originCurrencyTrans"];
		if ($_REQUEST["customerName"]!="")
		$customerName = $_REQUEST["customerName"];
	if ($_REQUEST["agentUsername"]!="")
		$agentUsername = $_REQUEST["agentUsername"];
		
		


	if ($fromTJS != "")
	{
		$dDateFT = explode("/",$fromTJS);
		if(count($dDateFT) == 3)
			$fromTJS = $dDateFT[2]."-".$dDateFT[1]."-".$dDateFT[0];
	}
	//debug($toTJS);
	if ($toTJS != "")
	{
		$dDateTT = explode("/",$toTJS);
		if(count($dDateTT) == 3){
			$toTJS = $dDateTT[2]."-".$dDateTT[1]."-".$dDateTT[0];
		}
	}
	if ($fromTJS != "" || $toTJS != "")
	{
		if ($fromTJS != "" && $toTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00' and transDate <= '$toTJS 23:59:59') ";	
		}
		elseif ($fromTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00') ";	
		}
		elseif ($toTJS != "")
		{
			$queryDatedT = " and (transDate <= '$toTJS 23:59:59') ";	
		}	
	}
	if ($searchNameTJS != "")
	{
		
			$searchCustQ = selectFrom("select transID from ".TBL_TRANSACTIONS." where refNumberIM='".$searchNameTJS."'");
			$searchID = $searchCustQ["transID"];
			$querysearchNameT = " and transID = '".$searchID."' "; 
		}
	
	if ($agentUsername != "")
	{
		
			
			$searchAgentQ = selectFrom("select userID from ".TBL_ADMIN_USERS." where (username LIKE '%".$agentUsername."%' OR name LIKE '%".$agentUsername."%') ");
		
			$searchAgentID = $searchAgentQ["userID"];
			$queryAgentNameId = " and custAgentID = '".$searchAgentID."' ";
			
		}
		if ($customerName != ""){
			$customerName=explode(' ',$customerName);
			
			if(count($customerName)>1){
				
			$searchCustID = selectMultiRecords("select customerID from customer where ( 	firstName LIKE '%".$customerName[0]."%' and lastName LIKE '%".$customerName[1]."%' OR accountName LIKE '%".$customerName."%') ");
			}
			else{
					
				$searchCustID = selectMultiRecords("select customerID from customer where ( 	firstName LIKE '%".$customerName[0]."%' or lastName LIKE '%".$customerName[0]."%' OR accountName LIKE '%".$customerName."%') ");
			}
			$queryCustNameT .= " and (";
			for($index=0;$index<count($searchCustID); $index++){
				$queryCustNameT .= " customerID = '".$searchCustID[$index]['customerID']."' ";
				if($index+1!=count($searchCustID)){
					$queryCustNameT.="or ";
				}
				else{
					$queryCustNameT.=")";
					
				}
			}
	
		} 
		
		
		
	
	if ($amountTJS != "")
	{
		$queryAmountT = " and totalAmount = '$amountTJS' ";	
	}
	if ($queryDatedT != "")
	{
		$queryT .= $queryDatedT;
	}
	if ($querysearchNameT != "")
	{
		$queryT .= $querysearchNameT;
	}
	if ($queryAgentNameId != "")
	{
		$queryT .= $queryAgentNameId;
	}
	if ($queryCustNameT != "")
	{
		$queryT .= $queryCustNameT;
	}
	
	
	if ($queryAmountT != "")
	{
		$queryT .= $queryAmountT;
	}
	if ($currencyT != "")
	{
		$queryT .= " and currencyFrom = '".$currencyT."'";
	}
	if ($originCurrencyTrans != "")
	{
		$queryT .= " and currencyTo = '".$originCurrencyTrans."'";
	}
	
	
}

if($_REQUEST["getGrid"] == "payList"){

	
		$query.=$sql_agent_QuertP;
	
	$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$query .= " order by $sidx $sord LIMIT $start , $limit";
	$result = mysql_query($query) or die(__LINE__.": ".mysql_error());


	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;
	$i=0;
	
	while($row = mysql_fetch_array($result))
	{
		$response->rows[$i]['id'] = $row["acid"];

		/**
		*
		* SQL query to get the total resolved amount of trade from tradebatchhistory table
		* to resolve the remaining amount if there is any
		*/

		$totalResolvedBoughtAmount = 0;
		$remainingBoughtResolveAmount = 0;
		$sqlTradeResolved = "SELECT sum(tradeResolveAmount) as totalResolved  from tradeBatchHistory WHERE tradeIDs = '" .$row["acid"]."'";
		$transTradeRSD = selectFrom($sqlTradeResolved);
		$totalResolvedBoughtAmount = $transTradeRSD["totalResolved"];
		$remainingBoughtResolveAmount = $row["amountSell"] - $totalResolvedBoughtAmount; 
		
		$amountBuyValue = $row["amountBuy"];
		$exchangeRateTrade = $row["exchangeRate"];
		$amountSellValue = $row["amountSell"];
		$totalBoughtAmountResolved = $totalResolvedBoughtAmount/$row["exchangeRate"];
		$remainingBoughtAmountResolved  = $remainingBoughtResolveAmount/$row["exchangeRate"];
		
		/**
		 *	Condition to set the format of amount to 2 and 
		 *	exchange rate to 4 if the config below is enabled
		 */
		if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
		{
			$amountBuyValue = $row["amountBuy"];
			$exchangeRateTrade = $row["exchangeRate"];
			$amountSellValue = $row["amountSell"];
			$totalResolvedBoughtAmount = $totalResolvedBoughtAmount;
			$remainingBoughtResolveAmount = $remainingBoughtResolveAmount;
			$totalBoughtAmountResolved = $totalBoughtAmountResolved;
			$remainingBoughtAmountResolved = $remainingBoughtAmountResolved;
			
			/* 
			$amountBuyValue = number_format($row["amountBuy"],2);
			$exchangeRateTrade = number_format($row["exchangeRate"],4);
			$amountSellValue = number_format($row["amountSell"],2);
			$totalResolvedBoughtAmount = number_format($totalResolvedBoughtAmount,2);
			$remainingBoughtResolveAmount = number_format($remainingBoughtResolveAmount,2);
			$totalBoughtAmountResolved = number_format( $totalBoughtAmountResolved ,2 );
			$remainingBoughtAmountResolved = number_format($remainingBoughtAmountResolved,2); */
		}
		/**#12865: Premier Fx :
		* Trading- Mapping to be Correct on Trade ID ( Internal )
		** Work Done By Mudassar Rauf
		* old value('Trade-'.$row["acid"],)
		**/
		
		$response->rows[$i]['cell'] = array(
									$row["sys_Trade_No"],
									dateFormat($row["created"], "2"),
									$amountBuyValue,
									$row["currencyBuy"],
									$remainingBoughtAmountResolved,
									$totalBoughtAmountResolved,
									$exchangeRateTrade,
									$amountSellValue,
									$row["currencySell"],$remainingBoughtResolveAmount,
									$totalResolvedBoughtAmount
									
								);
		$i++;
		
	}
	
	echo $response->encode($response); 

}	

if($_REQUEST["getGrid"] == "transList"){
	$queryT.=$sql_agent_QuertT;
	
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_query());
	$countT = mysql_num_rows($resultT);
//exit($countT);	
	
	if($countT > 0)
	{
		$total_pages = ceil($countT / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryT .= " order by $sidx $sord LIMIT $start , $limit";
	//exit($queryT);
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_error());

	/* debug($queryT); */

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $countT;
//exit($response);
	$i=0;
	
	while($rowT = mysql_fetch_array($resultT))
	{
		$response->rows[$i]['id'] = $rowT["transID"];
		
			
			$custQ = "select firstName,lastName from ".TBL_CUSTOMER." where customerID = '".$rowT["customerID"]."'";
			$custRS = selectFrom($custQ);
			$custName = $custRS["firstName"].' '.$custRS["lastName"];
			
/*
			$agentQ = "select name from ".TBL_ADMIN_USERS." where userID = '".$rowT["custAgentID"]."'";
			$agentRS = selectFrom($agentQ);
			$agentName = $agentRS["name"];
		}*/
		/**
		*
		* SQL query to get the total resolved amount of trade from tradebatchhistory table
		* to resolve the remaining amount if there is any
		*/
		$totalResolvedSoldAmount = 0;
		$remainingSoldResolveAmount = 0;
		$sqlTradeResolved = "SELECT sum(transResolveAmount) as totalResolved  from tradeBatchHistory WHERE transIDs = '" .$rowT["transID"]."'";
		$transTradeRSD = selectFrom($sqlTradeResolved);
		$totalResolvedSoldAmount = $transTradeRSD["totalResolved"];
		$remainingSoldResolveAmount = $rowT["localAmount"] - $totalResolvedSoldAmount; 	

		$totalAmountValue  = $rowT["totalAmount"];
		$exchangeRateValue = $rowT["exchangeRate"];
		$localAmountValue  = $rowT["localAmount"];
		$resolvedAmount    = $totalResolvedSoldAmount/$rowT["exchangeRate"];
		$remainingAmount   = $remainingSoldResolveAmount/$rowT["exchangeRate"];
		if($rowT["transType"]=='Topup')
		{
			$Transtype="Prepaid";
		}
		else
		{
			$Transtype=$rowT["transType"];
		}
		
		/**
		 *	Condition to set the format of amount to 2 and 
		 *	exchange rate to 4 if the config below is enabled
		 */
		if( defined("CONFIG_ROUND_NUMBER_TRADING") && CONFIG_ROUND_NUMBER_TRADING != "0" )
		{
			$totalAmountValue		 	= $rowT["totalAmount"];
			$exchangeRateValue 		 	= $rowT["exchangeRate"];
			$localAmountValue 	 	 	= $rowT["localAmount"];
            $newRate                    = $localAmountValue/$totalAmountValue;
			
			$totalResolvedSoldAmount    = $transTradeRSD["totalResolved"];
			$remainingSoldResolveAmount = $remainingSoldResolveAmount; 
			$resolvedAmount    			= $resolvedAmount;
			$remainingAmount 			= $remainingAmount;
			
			/* $totalAmountValue		 	= number_format($rowT["totalAmount"],2);
			$exchangeRateValue 		 	= number_format($rowT["exchangeRate"],4);
			$localAmountValue 	 	 	= number_format($rowT["localAmount"],2);	
			$totalResolvedSoldAmount    = number_format($transTradeRSD["totalResolved"],2);
			$remainingSoldResolveAmount = number_format($remainingSoldResolveAmount,2); 
			$resolvedAmount    			= number_format($resolvedAmount ,2);
			$remainingAmount 			= number_format($remainingAmount,2); */
		}
		$response->rows[$i]['cell'] = array(
									$rowT["refNumberIM"],
									dateFormat($rowT["transDate"], "2"),
									$Transtype,
									$rowT["transStatus"],
									$totalAmountValue,
									$rowT["currencyFrom"],
									$remainingAmount,
									$resolvedAmount,
									$exchangeRateValue,
									$localAmountValue,
									$rowT["currencyTo"],
									$remainingSoldResolveAmount,
									$totalResolvedSoldAmount,
									$newRate
									
									
									
									/*,
									$agentName,*/
									
								);
		$i++;
	}

	echo $response->encode($response); 
}

	//$custName,
?>
