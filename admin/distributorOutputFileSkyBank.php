<?
session_start();
include ("../include/config.php");
include ("security.php");

$filters = "";

/* If to export the old transaction */
if($_REQUEST["export"] == "old")
	$filters .= " and tr.dispatchDate != '' ";
else /* else it will search for new transactions */
	$filters .= " and tr.dispatchDate IS NULL ";


if($_REQUEST["Submit"]) //!empty($_REQUEST["Submit"]) && $_REQUEST["Submit"] == "search"
{
	/**
	 * Date filter
	 */
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	$filters .= " and (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";

	if(!empty($_REQUEST["distributors"]))
	{
		$filters .= " and tr.benAgentID = '".$_REQUEST["distributors"]."' ";
	}
	
	$sql = "select 
					tr.transID,
					tr.refNumberIM, 
					tr.transDate,
					tr.localAmount,
					ben.firstName as bf,
					ben.middleName as bm, 
					ben.lastName as bl,
					ben.IDType,
					ben.IDNumber,
					ben.phone as bp,
					ben.address as ba,
					ben.city as bc,
					cust.firstName,
					cust.middleName, 
					cust.lastName,
					cust.phone,
					cust.address,
					cust.city 
				from 
					transactions as tr, 
					beneficiary as ben, 
					customer as cust
				where
					tr.benID = ben.benID and
					tr.customerID = cust.customerID and
					(tr.transStatus = 'Authorize' or tr.transStatus = 'Amended') 
					". $filters ." 
					and tr.refNumberIM LIKE 'LEMOD%'
				order by tr.transDate 
					";
	if(!empty($sql))
		$fullRS = SelectMultiRecords($sql);
	//debug($sql);
	//debug($fullRS);
}


if($_REQUEST["Submit"] == "Export")
{
	/* Export version is csv */
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
	header('Content-type: application/octet-stream');

	if($_REQUEST["export"] == "old")
		header("Content-disposition: filename=DistributorOutputSkyBank_OldTransactions.csv");
	else
		header("Content-disposition: filename=DistributorOutputSkyBank.csv");
	
	$strCsvOutput = "";
	$strCsvSeparator = ",";
	
	/*
	$strCsvOutput .= "Transaction Id".$strCsvSeparator.
					 "Pass Code".$strCsvSeparator.
					 "Trans Date".$strCsvSeparator.
					 "Amount".$strCsvSeparator.
					 "Sender Name".$strCsvSeparator.
					 "Sender Address".$strCsvSeparator.
					 "Sender Phone".$strCsvSeparator.
					 "Receiver Name".$strCsvSeparator.
					 "Receiver Address".$strCsvSeparator.
					 "Receiver Phone".$strCsvSeparator.
					 "Document Id".$strCsvSeparator.
					 "Other Info".$strCsvSeparator."\n";
	*/
	
	for($i=0; $i < count($fullRS); $i++)
	{
		
		/* Marking the transactions as exported, by assigning the dispatchDate column of transaction table to current data time entry*/
		if($_REQUEST["export"] != "old") /* if export transactions are new */
		{
			$strUpdateTransactionSql = "update transactions set dispatchDate = '".date("Y-m-d h:i:s")."' where transID= '".$fullRS[$i]["transID"]."'";
			update($strUpdateTransactionSql);
			//debug($strUpdateTransactionSql);
		}
		
		$strIdType = $fullRS[$i]["IDType"];
	
		$strIdNumber = "1";
		if(strtoupper($strIdType) == strtoupper("International Passport"))
			$strIdNumber = "1";
		elseif(strtoupper($strIdType) == strtoupper("Drivers License"))
			$strIdNumber = "2";
		elseif(strtoupper($strIdType) == strtoupper("national ID"))
			$strIdNumber = "3";
		elseif(strtoupper($strIdType) == strtoupper("NYSC ID"))
			$strIdNumber = "6";
		$strDate = strtotime($fullRS[$i]["transDate"]);
		$strDate = date("m/d/Y",$strDate);
			
		$strCsvOutput .= $fullRS[$i]["refNumberIM"].$strCsvSeparator;
		$strCsvOutput .= "12".$strCsvSeparator;
		$strCsvOutput .= $strDate.$strCsvSeparator;
		$strCsvOutput .= number_format($fullRS[$i]["localAmount"],2,".","").$strCsvSeparator;
		
		$strCsvOutput .= $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"].$strCsvSeparator;
		$strCsvOutput .= $fullRS[$i]["address"]." ".$fullRS[$i]["city"].$strCsvSeparator;
		$strCsvOutput .= $fullRS[$i]["phone"].$strCsvSeparator;
		
		$strCsvOutput .= $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"].$strCsvSeparator;
		$strCsvOutput .= $fullRS[$i]["ba"]." ".$fullRS[$i]["bc"].$strCsvSeparator;
		$strCsvOutput .= $fullRS[$i]["bp"].$strCsvSeparator;
		
		$strCsvOutput .= $strIdNumber.$strCsvSeparator;
		$strCsvOutput .= "Others"."\n";
	}	
	echo $strCsvOutput;
	die();
}
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Distributor Output File (SKY BANK <? if($_REQUEST["export"] == "old") echo " - Old Transactions";?>)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 24px;
	font-weight: bold;
	height: 50px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 16px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->
</style>
<script>
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
</head>
<body>
  <form name="search" id="searchFrm" method="post" action="distributorOutputFileSkyBank.php">
	<table border="0" align="center" cellpadding="5" cellspacing="1" >
      <tr>
      	<td align="center">
  			<b>SEARCH FILTER</b>    				
      	</td>
    	</tr>
      <tr>
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
			<b>From </b>
			<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.fDay, "<?=(!empty($_POST["fDay"])?$_POST["fDay"]:"")?>");
			</script>
					<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.fMonth, "<?=(!empty($_POST["fMonth"])?$_POST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			  <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
			</SELECT> 
					<script language="JavaScript">
				SelectOption(document.search.fYear, "<?=(!empty($_POST["fYear"])?$_POST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			  <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
			</select>
					<script language="JavaScript">
				SelectOption(document.search.tDay, "<?=(!empty($_POST["tDay"])?$_POST["tDay"]:"")?>");
			</script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
	
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
					<script language="JavaScript">
				SelectOption(document.search.tMonth, "<?=(!empty($_POST["tMonth"])?$_POST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
					<script language="JavaScript">
			SelectOption(document.search.tYear, "<?=(!empty($_POST["tYear"])?$_POST["tYear"]:"")?>");
			</script>
			<input type="hidden" name="export" value="<?=$_REQUEST["export"]?>" />
			<input type="hidden" name="distributors" value="1002640297"> <!-- Sky Bank user Id 1002640297 -->
			<!-- Testing userid GL9 / 10026402443 -->
			<br /><br />
 		    <input type="submit" name="Submit" value="search" onclick="document.getElementById('searchFrm').target='_self'" />
			</td>
		</tr>
	</table>
	
	<table width="90%" cellpadding="2" cellspacing="0" align="center">
		<!-- Report Header Starts -->
		<tr>
			<td colspan="12" class="reportHeader">
				Distributor Output File SKY BANK 
				<? if($_REQUEST["export"] == "old") echo " (Old Transactions)";?>
			</td>
		</tr>
		<!-- Report Header Ends -->
		<!-- Column Header Starts -->
		<tr>
			<td class="columnHeader">Transaction ID</td>
			<td class="columnHeader">Pass Code</td>
			<td class="columnHeader">Date</td>
			<td class="columnHeader">Amount</td>
			<td class="columnHeader">Sender Name</td>
			<td class="columnHeader">Sender Address</td>
			<td class="columnHeader">Sender Phone</td>
			<td class="columnHeader">Receiver Name</td>
			<td class="columnHeader">Receiver Address</td>
			<td class="columnHeader">Receiver Phone Number</td>
			<td class="columnHeader">Document Id</td>
			<td class="columnHeader">Other Info</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?
			$tids = "";
			
			for($i=0; $i < count($fullRS); $i++)
			{
					$tids .= $fullRS[$i]["transID"].",";
					
					//$totalAmount += $fullRS[$i]["localAmount"];
					//$totalTrasactions += 0;
					
					$strIdType = $fullRS[$i]["IDType"];
	
					$strIdNumber = "1";
					if(strtoupper($strIdType) == strtoupper("International Passport"))
						$strIdNumber = "1";
					elseif(strtoupper($strIdType) == strtoupper("Drivers License"))
						$strIdNumber = "2";
					elseif(strtoupper($strIdType) == strtoupper("national ID"))
						$strIdNumber = "3";
					elseif(strtoupper($strIdType) == strtoupper("NYSC ID"))
						$strIdNumber = "6";
					
					$strDateTime = strtotime($fullRS[$i]["transDate"]);
					$strDateTime = date("m/d/Y",$strDateTime);
			?>
			<tr>
				<td class="transactions" align="center"><?=$fullRS[$i]["refNumberIM"]?></td>
				<td class="transactions" align="center">12</td>
				<td class="transactions" align="center"><?=$strDateTime?></td>
				<td class="transactions" align="center"><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></td>
				
				<td class="transactions" align="center"><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["address"]." ".$fullRS[$i]["city"]?></td> <!-- Sender Address -->
				<td class="transactions" align="center"><?=$fullRS[$i]["phone"]?></td> <!-- Sender Phone -->
				
				<td class="transactions" align="center"><?=$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["ba"]." ".$fullRS[$i]["bc"]?></td> <!-- beneficiary Address -->
				<td class="transactions" align="center"><?=$fullRS[$i]["bp"]?></td> <!-- beneficiary Phone -->
				
				<td class="transactions" align="center"><?=$strIdNumber?></td>
				<td class="transactions" align="center">Others</td>
				
			</tr>
			<!-- SubTotal Ends -->
			<!-- Transactions Data Ends -->
			
	<?
		}
/**
 * If no records to show
 */
 if(count($fullRS) < 1)
 {
?>
	<tr>
		<td class="transactions" align="center" colspan="12">No Records found!</td>
	</tr>
<?
	}
?>
</table>
	<table width="500" border="0" align="center" cellpadding="10" cellspacing="0">
		<tr>
			<td width="50%" align="center">
				<? if(count($fullRS) > 0) {  ?>
					<input type="submit" name="Submit" value="Export" onclick="document.getElementById('searchFrm').target='_blank'" /> 
				<? } ?>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
