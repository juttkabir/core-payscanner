<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("ledgersUpdation.php");
$agentType = getAgentType();

$userID  = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = 0;//CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=200;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
if($_GET["fileID"] != ""){
$fileID = $_GET["fileID"];
}

$condition = "";
$value = "";

if($_POST["Submit"]!=""){
	$Submit = $_POST["Submit"];
}elseif($_GET["Submit"]!="") {
	$Submit=$_GET["Submit"];
}

if($_GET["old"]!=""){
	$old = $_GET["old"];
}else{
	$old = "";
}

if(CONFIG_EXPORT_TRANS_OLD == '1')
{
	if ($old == 'Y') {
		$condition = " and is_exported = 'Y' ";
		$val = "old";
	} else {
		$condition = " and is_exported = '' ";
		$val = "current";
	}
}	
		
$trnsid = $_POST["trnsid"];

//$ttrans = $_POST["totTrans"];
/*if($_POST["btnAction"] != "")
{
	if(count($trnsid) >= 1)
	{
			for ($i=0;$i< count($trnsid);$i++)
			{
				if($_POST["btnAction"] == "Reverse To Pending")
				{
					transStatusChange($trnsid[$i], 'Pending', $username, $remarks, $refundFee='Yes');	
				}
			}
	}
}*/
if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	
	$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
		
	$condition .= " and (transStatus = 'Authorize' or transStatus = 'Amended') ".$FileConditionQuery["conditions"]." ";
}else{
	
	 $condition .= " and benAgentID = '100264'";
}
 
if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
} else {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and File_Name = 'export_trans_file.php'");
}

$query = "select * from ". TBL_TRANSACTIONS." where 1 $condition ";
$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS." where 1 $condition ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	
	$query .= " and benAgentID = '".$userID."' ";
	$queryCnt .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";
$query .= " LIMIT $offset , $limit";
$allCount = countRecords($queryCnt);
$contentTrans = selectMultiRecords($query);
//debug($query);
//debug($contentTrans);
?>
<html>
<head>
	<title>Select Transactions To Export</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
/*function exportAndReload(){
	window.open('','export','');
	document.trans.action='export_trans_file_opal.php?fileID=<?=$fileID?>&old=<?=$old?>';
	
	document.location='?fileID=<?=$fileID?>&old=<?=$old?>';
}*/
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90;}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="800" border="0" cellspacing="0" cellpadding="3">
	
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Select <? echo ($old == 'Y' ? 'Old' : 'New');?> Transactions To Export</font></strong></td>
  </tr>
  <tr>
  	<td><a class="style2" href="main.php">Go Back</a></td>
  </tr>
	<tr>
		<td>
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          <tr>
            <td width="150">
              <?php if (count($contentTrans) > 0) {;?>
              Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentTrans));?></b>
              of
              <?=$allCount; ?>
             
            </td>
          <?php if ($prv >= 0) { ?>
            <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&fileID=$fileID&old=$old&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
            </td>
            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&fileID=$fileID&old=$old&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
            </td>
	  			<?php } ?>
          <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
            <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&fileID=$fileID&old=$old&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
            </td>
            <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&fileID=$fileID&old=$old&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
            </td>
            <?php } 
          	}
            ?>
          </tr>
        </table>
		</td>
	</tr>
  	<?
  	if($_GET["msg"] != ""){
  	?>
    	<tr>
    		<td>
    			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			    	<tr>
				      <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;<font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>
				     &nbsp;<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td>
			      </tr>
			    </table>
	    	</td>
	    </tr>
    <?
    }
    ?>
    	<tr>
    		<td align="center">
    			<strong><font color="#005b90" size="2"><?=$fileFormatQuery["FileLable"]?></font></strong>
    		</td>
    	</tr>
  <?
	if($allCount > 0) {
	?>
  <tr>
    <td align="center"><br>
     <table width="100%" border="1" cellpadding="0" bordercolor="#666666">
   		<form action="export_trans_file_opal.php?fileID=<?=$fileID?>&old=<?=$old?>" method="post" name="trans">
   			<tr>  
   				<td nowrap bgcolor="#EFEFEF">
   					<table width="100%" border="0" bordercolor="#EFEFEF">  
   						<tr bgcolor="#FFFFFF"> 
								<td width="6%" align="center"><input name="All" type="checkbox" id="All" onClick="CheckAll();"></td>
								<?
								$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");		
								for ($j = 0; $j < count($fieldQuery); $j++)
								{
									if($fieldQuery[$j]["appearOnce"] != 'Y')
									{
								?>
		      					<td><span class="style1"><b><? echo $fieldQuery[$j]["Lable"];?></b></span></td>
					 	 		<?
					  			}
						  		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
									/* this is if any predefined value is to be printed against a respective field*/
			
									if($fieldQuery[$j]["isFixed"]== 'Y' && $fieldQuery[$j]["appearOnce"] != 'Y'){
										$fixedValue[$j] = $fieldQuery[$j]["Value"];
									}
									if($fieldQuery[$j]["isFixed"]== 'N' && $fieldQuery[$j]["appearOnce"] != 'Y' && $fieldQuery[$j]["Value"]!= ""){
										$fixVal[$j] = $fieldQuery[$j]["Value"];
									}
									$dataTable[$j] = $fieldQuery[$j]["tableName"];
									
									$dataPattern[$j] = $fieldQuery[$j]["customPattern"];
								}
					  		?>
				 			</tr>
				 			<?
				 			for($i=0;$i < count($contentTrans);$i++)
							{
								if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
								{		  
								   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
								   $beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
								}else
							  {
							  	$customerContent = selectFrom("select firstName,middleName,lastName,accountName from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
							  	$beneContent = selectFrom("select firstName,middleName,lastName,City,Address,Address1,Zip from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
							  	if($beneContent["paymentDetails"] != ""){
										$paymentDetails = $beneContent["paymentDetails"].",";
									}else{
										$paymentDetails = "";
									}
									$arrayPmt[$i] = $paymentDetails;
							  }
							  	
								if($contentTrans[$i]["transType"]=='Pick up')
								{
									$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
								}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
								{
									if($contentTrans[$i]["createdBy"] == "CUSTOMER")
									{
										$bankDetails = selectFrom("select IBAN from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
									}else{
										$bankDetails = selectFrom("select IBAN from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
									}
									$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
									$bankNumber	= $bankNumQuery["bankCode"];				
								}
								
							?>
								<tr bgcolor="#FFFFFF">
									<td align="center" bgcolor="#FFFFFF">
										<? 
											$tempTransId = $contentTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";
										?>
										</td>
								<?
								
								for($k = 0; $k < count($dataVariable); $k ++)
								{
									$value = "";
									$field = explode(',',$dataVariable[$k]); // this it to break a string on any index of the array[array containing database fields]
									if($fieldQuery[$k]["isFixed"]== 'Y' && $fieldQuery[$k]["appearOnce"] != 'Y')
						      {
						      	$value .= $fixedValue[$k]; 
						      }else{
						     
										/*
										values for the fields are pulled from the respective tables in the database
										*/
										if($dataTable[$k] == 'transactions')
										{
											for($ex = 0; $ex < count($field); $ex++){
												if($ex > 0)
												{
													$value .=" ";	
												}
												
												if($field[$ex] == "localAmount" && $contentTrans[$i]["toCountry"] == "Poland"){
													$value .=number_format($contentTrans[$i][$field[$ex]],2,'','');
												}else{
													$value .= $contentTrans[$i][$field[$ex]];
												}
													
										 }
											
																
										}elseif($dataTable[$k] == 'beneficiary'){
											
											for($ex = 0; $ex < count($field); $ex++)
											{
												if($ex == 0){
													$value .= "\"";
												}
												if($dataPattern[$k] == 'Y' )
												{	
													if($ex == 1){
														$value .=" ";
													}
													if($ex > 1){
														$value .= "|";
													}
												}elseif($ex > 0)
												{
													$value .=" ";	
												}	
												$value .= $beneContent[$field[$ex]];
												if($ex == (count($field)-1)){
													$value .= "\"";
												}
											}
																						
										}elseif($dataTable[$k] == 'customer'){
											
											for($ex = 0; $ex < count($field); $ex++)
											{
												if($dataPattern[$k] == 'Y' )
												{	
													if($ex == 0)
													$value .=$arrayPmt[$i]." From ";
													if($ex == 2)
																$value .= ",";
												}
												if($ex > 0){
													$value .= " ";
												}
												$value .= $customerContent[$field[$ex]];					
											}
										}elseif($dataTable[$k] == 'bankDetails'){
											for($ex = 0; $ex < count($field); $ex++)
											{
												if($ex == 0){
													$value .= "\"";
												}
												if($ex > 0){
													$value .= " ";
												}
												$value .= $bankDetails[$field[$ex]];				
												if($ex == (count($field)-1)){
													$value .= "\"";
												}	
											}
										}
										/*if($value == ''){
											$value .= $fixVal[$k];
										}*/
									}	
									if($dataVariable[$k] == 'transDate')
									{
										if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
											$value = dateFormat($value, 4);
										}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
											$value = dateFormat($value, 3);
										}else{
											$value = dateFormat($value, 1);
										}
									}	

									if($dataVariable[$k] == 'dispatchDate')
									{
										if(!empty($value))
										{
											if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
												$value = dateFormat($value, 4);
											}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania" || $fileFormatQuery["FileLable"] == "Transactions For Poland"){
												$value = dateFormat($value, 3);
											}else{
												$value = dateFormat($value, 1);
											}
										}
									
									}		
									if($fieldQuery[$k]["appearOnce"] != 'Y')
									{
									?>		
										<td><? echo $value; ?></td>
									<?
									}
								}
								?>
								</tr> 
							<?
							} 	
							?>
				 			
				   	<table>
					</td>
				</tr>
		 </table>
   </td>
 </tr>
	<tr bgcolor="#FFFFFF">
 		<td align="center">
 			<!--<input type='hidden' name='totTrans' value='<?echo $i?>'-->
    	<input name="btnExport" type="submit"  value="Export">
    	<!--input name="btnAction" type="submit"  value="Reverse To Pending" onClick="document.trans.action='select_trans_to_export.php?fileID=<?=$fileID?>&old=<?=$old?>';document.trans.submit();"-->
    	
    	</td>
 	</tr>
	
	<?
	}else{
	
	if ($val != "") {
		$error = "There is no ".$val." transaction to export.";
	} 
	?>
	<tr>
		<td align="center"><br><br><font color="#FF0000" size="2"><? echo $error;?></font>
		</td>
	</tr>
	<?
	}
	?>				  	
					 
   	</form>
</table>
</body>
</html>
