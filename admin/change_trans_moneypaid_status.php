<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("connectOtherDataBase.php");
$transID = (int) $_GET["transID"];


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

if($_POST["btnStatus"] != ""){
	transMoneypaidStatusChange($_GET["transID"],$_POST["changeStatusTo"]);
	redirect("moneypaid_oncredit_trans.php?".$qryString);
}

if($transID > 0)
{
	
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
	
}
$qryString = "transType=".$_GET["transType"]."&Submit=".$_GET["Submit"]."&searchVal=".$_GET["searchVal"]."&searchBy=".$_GET["searchBy"]."&nameType=".$_GET["nameType"];
if($createdBy != 'CUSTOMER')
{
?>
<html>
<head>
<title><? echo $_GET["action"]; ?> Transaction Details</title>

<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
	
<!-- MeadCo ScriptX -->
<!--object id="factory" viewastext style="display:none"
classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
codebase="ScriptX.cab#Version=6,2,433,14">
</object-->	
<form name="changeStatus" method="post" action="change_trans_moneypaid_status.php?<?=$qryString?>&transID=<? echo $_GET["transID"]?>">	
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td>
            	<a class="style2" href="moneypaid_oncredit_trans.php?<?=$qryString?>">Go Back</a>
            </td></tr>
            <tr>
            	<td>
            	<fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="196" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="148" align="right"><font color="#005b90">Status</font></td>
                <td width="142"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
              <!-- Added by Niaz Ahmad againist ticket # 2533 at 09-10-2007 -->
                <? if(CONFIG_SHOW_REFERENCE_CODE_BOX == "1"){ ?>
                <td width="146" height="20" align="right"><font class="style2"><? echo $systemCode;?></font></td>
                <td width="196" height="20" border="1"><table border="1"><tr><td width="100" height="20" align="center"><strong><? echo $contentTrans["refNumberIM"]?></strong></td></tr></table></td>	
                 <? } else {?>
                 <td width="146" height="20" align="right"><font class="style2"><? echo $systemCode;?></font></td>
                 <td width="196" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong></td>
                <? } ?>
                <td width="148" height="20" align="right"><strong><font color="#005b90"><? echo $manualCode;?></font></strong></td>
                <td width="142" height="20"><strong><? echo $contentTrans["refNumber"]; ?></strong></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="196" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <? if ($contentTrans["transStatus"]!="Pending"){ ?>
                <td width="148" height="20" align="right"><font color="#005b90"><? echo ($contentTrans["transStatus"]=="Amended" ? "Amended" : "Authorization");?> Date </font></td>
                <td width="142" height="20"><? 
           if ($contentTrans["transStatus"]=="Amended")
           {
           	$amendedDate = selectFrom("select * from ".TBL_AMENDED_TRANSACTIONS." where transID='".$transID."'");
           	$aaDate = $amendedDate['modificationDate'];
          }
          else
          {
				$aaDate = $contentTrans['authoriseDate'];
			}
				if($aaDate != '0000-00-00 00:00:00' && $aaDate!='')
					echo date("F j, Y", strtotime("$aaDate"));
				else
					echo "No Date";				
				?></td><? } ?>
              </tr>
              <? if ($contentTrans["transStatus"]!="Authorize" && $contentTrans["transStatus"]!="Amended" && $contentTrans["transStatus"]!="Pending"){ ?>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90"><?=$contentTrans["transStatus"];?> Date </font></td>
                <td width="196" height="20"><? 
         switch($contentTrans["transStatus"])
         {
         		case "Suspended":
         			$dynamicDate = $contentTrans['suspendDate'];
         			break;
         		case "Out for Delivery":
         			$dynamicDate = $contentTrans['deliveryOutDate'];
         			break;
         		case "Delivered":
         		case "Sent By Courier":
         		case "Credited":
         		case "Picked up":
         			$dynamicDate = $contentTrans['deliveryDate'];
         			break;
         		case "Cancelled":
         			$dynamicDate = $contentTrans['cancelDate'];
         			break;
         			case "Cancelled - Returned":
         			$dynamicDate = $contentTrans['returnDate'];
         			break;
         		case "Rejected":
         			$dynamicDate = $contentTrans['rejectDate'];
         			break;
         		case "Failed":
         			$dynamicDate = $contentTrans['failedDate'];
         			break;
         		case "Recalled":
         			$dynamicDate = $contentTrans['recalledDate'];
         			break;
         		case "Suspicious":
         			$dynamicDate = $contentTrans['suspeciousDate'];
         			break;
         		case "Processing":
         			$dynamicDate = $contentTrans['verificationDate'];
         			break;
         		case "Hold":
         			$dynamicDate = $contentTrans['holdDate'];
         			break;
         		case "Unhold":
         			$dynamicDate = $contentTrans['unholdDate'];
         			break;
         }       	
				if($dynamicDate != '0000-00-00 00:00:00' && $dynamicDate != '')
					echo date("F j, Y", strtotime("$dynamicDate"));
				else
					echo "No Date";
				
				?>
                </td>
              </tr>
            <? } ?>				  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>		  
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Agent Details </legend>
            <table width="650" border="0">
              <? 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["custAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			if($senderAgentContent["userID"] != "")
			{
		?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                <td width="200"><? echo $senderAgentContent["name"]?> </td>
                <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                <td width="200"><? echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Company</font></td>
                <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
              <?
			  }
			  ?>
            </table>
            </fieldset></td>
          </tr>
		  
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table width="650" border="0">
                <? 
            if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
            	
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$transID."' and generatedLocally = 'N'");   
            }   
			if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
			{
				$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
				$otherClient = new connectOtherDataBase();
				$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
				
				$customerContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentTrans["customerID"]."'");		
				$benificiaryContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentTrans["benID"]."'");		
				if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from ".$jointClient["dataBaseName"].".bankDetails where transID='".$sharedTrans["remoteTrans"]."'");
				}
				
					$otherClient->closeConnection();
											dbConnect();
										
			}else{
				
				$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, proveAddress, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
				$customerContent = selectFrom($queryCust);
				
				$queryBen = "select *  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
				$benificiaryContent = selectFrom($queryBen);
				
				if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from bankDetails where transID='".$transID."'");
				}
			}
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
           		<?	if (CONFIG_PROVE_ADDRESS == "1") {  ?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Proved Address</font></td>
                  <td colspan="3"><? echo ($customerContent["proveAddress"] == "Y" ? "Yes" : "No") ?></td>
                </tr>
           		<?	}  ?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">City</font></td>
                  <td width="200"><? echo $customerContent["City"]?></td>
                </tr>
                <tr>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["Email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
         <!-- <tr>
            <td><fieldset>
            <legend class="style2">Beneficiary Agent Details</legend>
            <table width="650" border="0">
              <? 
			/*$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["benAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			if($senderAgentContent["userID"] != "")
			{*/
		?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                <td><? //echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><? //echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? //echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Company</font></td>
                <td width="200"><? //echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? //echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? //echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? //echo $senderAgentContent["email"]?></td>
              </tr>
              <?
			 // }
			  ?>
            </table>
            </fieldset></td>
          </tr>-->
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    
            <table width="650" border="0" bordercolor="#006600">
              <? 
		
			
			if($benificiaryContent["benID"] != "")
			{
		?>
              <tr> 
                <td width="150" height="20" align="right"><font class="style2">Beneficiary 
                  Name</font></td>
                <td height="20" colspan="2"><strong><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></strong></td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
                <td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			
					?>
              <tr> 
                <td height="20" colspan="4" class="style2">Beneficiary Bank Details 
                </td>
              </tr>
        	<?
							$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
        			if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European") {
        	?>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">IBAN</font></td>
                <td width="200" height="20"><? echo $benBankDetails["IBAN"]; ?></td>
                <td width="100" align="right"><font color="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></font></td>
                <td width="200"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? $benBankDetails["Remarks"] : "&nbsp;") ?></td>
              </tr>
        	<?
        			} else {
        	?>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Bank 
                  Name</font></td>
                <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                <td width="200"><? echo $benBankDetails["accNo"]; ?> </td>
              </tr>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Branch 
                  Code</font></td>
                <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?> 
                </td>
                <td width="100" align="right"><font color="#005b90"> 
                  <?
												if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												 	echo $benBankDetails["ABACPF"];
								                     
												}
												?>
                </td>
              </tr>
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Branch 
                  Address</font> </td>
                <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?> 
                </td>
                <?
                if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
                {?>
                	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
                  <td width="200"><?=$benBankDetails["accountType"]?> </td>
                <? 
                }else{
                ?>	
                <td width="138" height="20"  align="right"><font color="#005b90">&nbsp; 
                  </font></td>
                <td width="196" height="20">&nbsp; 
                  <!--echo $benBankDetails["IBAN"];-->
                </td>
                <?
              }
                ?>
              </tr>
            <?	}  ?>
              <tr> 
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                <td width="200" height="20" title="For european Countries only">&nbsp;</td>
              </tr>
              <?
			 }
			  if($contentTrans["transType"] == "Pick up")
			  {
			 $_SESSION["collectionPoint"] = $contentTrans["collectionPointID"];
			$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPoint"] . "'";
			$senderAgentContent = selectFrom($queryCust);
			$queryDistributor = "select name,agentCompany,distAlias  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
			$_SESSION["collectionPoint"] = "";
			if($senderAgentContent["cp_id"] != "")
			{
		?>
              <tr> 
                <td height="20" colspan="4" class="style2">Collection Point Details 
                </td>
              </tr>
              <tr> 
                <td width="150" align="right"><font class="style2">Collection Point Name</font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_corresspondent_name"]?></strong> 
                </td>
                <td width="100" align="right"><font class="style2">Contact Person 
                  </font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_contact_person_name"]?></strong></td>
              </tr>
             
              <? if(CONFIG_DISTRIBUTOR_ALIAS != '1') { ?>
              <tr>    <!-- Add by Niaz Ahmad against ticket # 2558 at 12-10-2007 (only for Beaconcrest)-->
                      <? if(CONFIG_CHANGE_LABEL_RECEIPT == "1"){ ?>
                        <td width="150" align="right"><font class="style2">Paying Agent</font></td>
                      <? } else { ?>
                       <td width="150" align="right"><font class="style2">Distributor</font></td>
                       <? } ?>
                        <td width="200" colspan="3"><? echo $queryExecute["name"]?></td>
                      </tr> 
               <? } ?>     
               
               <? if(CONFIG_DISTRIBUTOR_ALIAS == '1') {
                          
                          if($queryExecute["distAlias"] !='' && $agentType != "SUPA"){
                          	$alias = $queryExecute["distAlias"];
                          }else{
                          	$alias = $queryExecute["agentCompany"];
                          	}
                          if($agentType == "SUPA"){
                          	$alias = $queryExecute["distAlias"];
                          	}	
                          		
                        ?>  
                  <tr>    <!-- Add by Niaz Ahmad against ticket # 2558 at 12-10-2007 (only for Beaconcrest)-->
                      <? if(CONFIG_CHANGE_LABEL_RECEIPT == "1"){ ?>
                        <td width="150" align="right"><font class="style2">Paying Agent</font></td>
                      <? } else { ?>
                       <td width="150" align="right"><font class="style2">Distributor</font></td>
                       <? } ?>
                        <td width="200" colspan="3"><? echo $alias ?></td>
                      </tr> 
               <? } ?>   
                      
              <tr> 
                <td width="150" align="right"><font class="style2">Address</font></td>
                <td colspan="3" ><strong><? echo $senderAgentContent["cp_branch_address"]?></strong></td>
              </tr>
              <tr> 
                <td width="100" align="right"><font class="style2">City</font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_city"]?></strong></td>
                <td width="150" align="right"><font class="style2">Country</font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>
                
              </tr>
              <tr> 
                <td width="150" align="right"><font class="style2">Phone</font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
                <td width="100" align="right"><font class="style2">Fax</font></td>
                <td width="200"><strong><? echo $senderAgentContent["cp_fax"]?></strong></td>
              </tr>
              

              <?
			  }
			  
			  
			  }			 
 ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              
            <table width="650" border="0">
              <tr> 
                <td width="150" height="20" align="right"><strong><font color="#005b90"><? echo $manualCode;?></font></strong></td>
                <td height="20" width="200"><strong><? echo $contentTrans["refNumber"]?></strong></td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo number_format($contentTrans["transAmount"],$roundLevel,'.',',');
                	if(CONFIG_ENABLE_DOMESTIC != '1')
       						{
                		 echo " in ".$contentTrans["currencyFrom"]; 
                	}
                	?>
                </td>
              </tr>
         <?
       if(CONFIG_ENABLE_DOMESTIC != '1')
       {
       ?>       
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Exchange 
                  Rate</font></td>
                <td width="200" height="20"><? echo $contentTrans["exchangeRate"]?> 
                </td>
                <td width="100" height="20" align="right"><font class="style2">Local 
                  Amount</font></td>
                <td width="200" height="20"><strong><? echo number_format($contentTrans["localAmount"],$roundLevel,'.',',');?> 
                  <? 
				  	
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?> 
				  <? 
				  /*$currencyTo = $contentTrans["currencyTo"];
				  if(is_numeric($currencyTo))
					{
						 $contentExchagneRate = selectFrom("select *  from exchangerate where erID  = '".$currencyTo."'");								
						echo $_SESSION["currencyTo"] = $contentExchagneRate["currency"];
					}*/
				?></strong>
				  </td>
              </tr>
              
            <?
          }
            ?>  
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200" height="20"><? echo number_format($contentTrans["IMFee"],$roundLevel,'.',',');?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total 
                  Amount </font></td>
                <td width="200" height="20"><? echo number_format($contentTrans["totalAmount"],$roundLevel,'.',','); ?> 
                </td>
              </tr>
              <? if($contentTrans["transType"] == "Bank Transfer"  && CONFIG_REMOVE_ADMIN_CHARGES != "1"){
              	?>
              	<tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
                <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              	<?
              }
              	?>
              <? if ($contentTrans["transStatus"] == "Cancelled")
              { ?>
               <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Refund <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200" height="20"><? echo $contentTrans["refundFee"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200" height="20">&nbsp;
                </td>
              </tr>
              <? } ?>
              
              
              
              <tr> 
                <td width="150" height="20" align="right"><font color="#005b90">Transaction 
                  Purpose</font></td>
                <td width="200" height="20"><? echo $contentTrans["transactionPurpose"]?> 
                </td>
                 <? if(CONFIG_REMOVE_FUNDS_SOURCES != "1")
               {?> 
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                <td width="200"><? echo $contentTrans["fundSources"]?> </td>
              <? }?>
              </tr>
              
              <? if ($contentTrans["remarks"] != ""){ ?>
              <tr>
                <td align="right"><font color="#005b90"> <? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?>   </font></td>
                <td width="200" height="20"  colspan="2"><? echo $contentTrans["remarks"];?></td>
                
              </tr>
              <? } ?>
               <? if(CONFIG_IDTYPE_PASSWORD == '1'&& $benificiaryContent["otherId"]!= "")
					  {
					  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Password</font></td>
                <td width="200" height="20"><? echo $contentTrans["benIDPassword"];?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <? } ?>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>
                     <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td width="100" align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td width="200"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>	              
              </tr>
              <?
         if(CONFIG_INTERNAL_REMARKS == '1')
         {
         	 if($agentType != "SUPAI" && $agentType != "SUPI" && $agentType != "SUBI" && $agentType != "TELLER")
           {
         ?>
         		<tr> 
	            <td width="150" height="20" align="right"><font color="#005b90"><? echo(CONFIG_LABEL_INTERNAL_REMARKS != '' ? CONFIG_LABEL_INTERNAL_REMARKS : "Internal Remarks"); ?></font></td>
	            <td width="200" height="20"><? echo $contentTrans["internalRemarks"]?> </td>
	            <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
	            <td width="200">&nbsp; </td>
	          </tr>
         
         <? 
           }
         }
         ?>  
			    </table>
			
            </fieldset></td>
          </tr>
          
          <tr>
					  <td colspan="4"><fieldset>
            	<legend class="style3">Change Transaction's Money Paid Status </legend>
					  		<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600" align="center">
                	<tr>
                  	<td width="170" height="20" align="right"><font color="#005b90">Change Money Paid Status</font><font color="#ff0000">*</font> </td>
                    <td height="20" align="left">
                    	<select name="changeStatusTo" class="flat" id="changeStatusTo">
                    		<option value="">-Select Status-</option>
                          <option value="By Cash">By Cash</option>
                          <option value="By Cheque">By Cheque</option>
                          <option value="By Bank Transfer">By Bank Transfer</option>
											</select>
										</td>
										<td align="left"><input name="btnStatus" type="submit" class="flat" value="Submit">
                  </td>
                  </tr>
                </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
          
        </table></td>
    </tr>

</table>
</form>
</body>
</html>
<?
}
else
{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<form name="changeStatus" method="post" action="change_trans_moneypaid_status_conf.php?<?=$qryString?>&transID=<? echo $_GET["transID"]?>">	
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font class="style2">Transaction No </font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong>
                </td>
                <td width="100" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <? if ($contentTrans["transStatus"]!="Pending"){ ?>
                <td width="150" height="20" align="right"><font color="#005b90"><? echo ($contentTrans["transStatus"]=="Amended" ? "Amended" : "Authorization");?> Date </font></td>
                <td width="150" height="20"><? 
				if ($contentTrans["transStatus"]=="Amended")
           {
           	$amendedDate = selectFrom("select * from ".TBL_AMENDED_TRANSACTIONS." where transID='".$transID."'");
           	$aaDate = $amendedDate['modificationDate'];
          }
          else
          {
				$aaDate = $contentTrans['authoriseDate'];
			}
				if($aaDate != '0000-00-00 00:00:00' && $aaDate!='')
					echo date("F j, Y", strtotime("$aaDate"));
				else
					echo "No Date";				
				?></td><? } ?>
              </tr>			
              <? if ($contentTrans["transStatus"]!="Authorize" && $contentTrans["transStatus"]!="Amended" && $contentTrans["transStatus"]!="Pending"){ ?>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90"><?=$contentTrans["transStatus"];?> Date </font></td>
                <td width="196" height="20"><? 
         switch($contentTrans["transStatus"])
         {
         		case "Suspended":
         			$dynamicDate = $contentTrans['suspendDate'];
         			break;
         		case "Out for Delivery":
         			$dynamicDate = $contentTrans['deliveryOutDate'];
         			break;
         		case "Delivered":
         		case "Sent By Courier":
         		case "Credited":
         		case "Picked up":
         			$dynamicDate = $contentTrans['deliveryDate'];
         			break;
         		case "Cancelled":
         			$dynamicDate = $contentTrans['cancelDate'];
         			break;
         		case "Rejected":
         			$dynamicDate = $contentTrans['rejectDate'];
         			break;
         		case "Failed":
         			$dynamicDate = $contentTrans['failedDate'];
         			break;
         		case "Recalled":
         			$dynamicDate = $contentTrans['recalledDate'];
         			break;
         		case "Suspicious":
         			$dynamicDate = $contentTrans['suspeciousDate'];
         			break;
         		case "Processing":
         			$dynamicDate = $contentTrans['verificationDate'];
         			break;
         		case "Hold":
         			$dynamicDate = $contentTrans['holdDate'];
         			break;
         		case "Unhold":
         			$dynamicDate = $contentTrans['unholdDate'];
         			break;
         }       	
				if($dynamicDate != '0000-00-00 00:00:00' && $dynamicDate != '')
					echo date("F j, Y", strtotime("$dynamicDate"));
				else
					echo "No Date";
				
				?>
                </td>
              </tr>
            <? } ?>				  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Customer Details </legend>
              <table width="650" border="0">
                <? 
		
			$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["c_address"] . " " . $customerContent["c_address2"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["c_zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["c_country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["c_phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["c_email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  			  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                        <td height="20" colspan="2"><strong><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></strong></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
                        <td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
					?>
                      <tr>
                        <td colspan="4" class="style2">Beneficiary Bank Details </td>
                        
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
												if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												 	echo $benBankDetails["ABACPF"];
								                     
												}
												?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
											<?
			                if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
			                {?>
			                	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
			                  <td width="200"><?=$benBankDetails["accountType"]?> </td>
			                <? 
			                }else{
			                ?>	
			                <td width="138" height="20"  align="right"><font color="#005b90">&nbsp; 
			                  </font></td>
			                <td width="196" height="20">&nbsp; 
			                  <!--echo $benBankDetails["IBAN"];-->
			                </td>
			                <?
			              }
			                ?>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">&nbsp;</font>&nbsp;</td>
                        <td width="200" height="20">&nbsp;  </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Agent Name</font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong></td>
                        <td width="100" align="right"><font class="style2">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><strong><? echo $senderAgentContent["cp_branch_address"]; ?></strong>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?> </strong>                       </td>
												<td width="150" align="right"><font class="style2">Branch Code</font></td>
												<td width="200" colspan="3"><strong><? echo $senderAgentContent["cp_ria_branch_code"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Compnay </font> </td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?> </strong>                       </td>
												<td width="100" align="right"><font class="style2">Country</font></td>
												<td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>                       
                    </tr>
									  <tr> 
											<td width="150" align="right"><font class="style2">Phone</font></td>
											<td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
					    				<td width="100" height="20" align="right"><font class="style2">City</font></td>
                      <td width="200" height="20"><strong><?  echo $senderAgentContent["cp_city"]; ?></strong></td>
					  				</tr>
					  
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo number_format($contentTrans["transAmount"],$roundLevel,'.',',');?>                </td>
              </tr>
			  <tr>
			    <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
			    <td height="20"><? echo number_format($contentTrans["IMFee"],$roundLevel,'.',',');?> </td>
                <td width="100" height="20" align="right"><font class="style2">Local Amount </font></td>
                <td width="200" height="20"><strong><? echo number_format($contentTrans["localAmount"],$roundLevel,'.',',');?>                   
				<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>
				             </strong> </td>
			  </tr>
			  <? if($contentTrans["transType"] == "Bank Transfer" && CONFIG_REMOVE_ADMIN_CHARGES != "1"){
            	?>
        	<tr> 
          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
              <tr>
                <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" height="20"><? echo number_format($contentTrans["totalAmount"],$roundLevel,'.',',');?>                </td>
              </tr>
                            
              <? if ($contentTrans["remarks"] != ""){ ?>
              <tr>
                <td align="right" ><font color="#005b90"> Remarks </font></td>
                <td width="200" height="20" colspan="2"><? echo $contentTrans["remarks"];?></td>
                
              </tr>
              <? } ?>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?> </font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>	              
                <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td width="100" align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td width="200"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>
					  
              </tr>
            </table>
            </fieldset></td>
          </tr>
		  		  
  <tr>
					  <td colspan="4"><fieldset>
            	<legend class="style3">Change Transaction's Money Paid Status </legend>
					  		<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600" align="center">
                	<tr>
                  	<td width="170" height="20" align="right"><font color="#005b90">Change Money Paid Status</font><font color="#ff0000">*</font> </td>
                    <td height="20" align="left">
                    	<select name="changeStatusTo" class="flat" id="changeStatusTo">
                    		<option value="">-Select Status-</option>
                          <option value="By Cash">By Cash</option>
                          <option value="By Cheque">By Cheque</option>
                          <option value="By Bank Transfer">By Bank Transfer</option>
											</select>
										</td>
										<td align="left"><input name="btnStatus" type="submit" class="flat" value="Submit">
                  </td>
                  </tr>
                </table>					  
					  </fieldset>
					  </td>					  
					  </tr>  			  
		  
        </table></td>
    </tr>

</table>
</form>
</body>
</html>
<?
}
?>