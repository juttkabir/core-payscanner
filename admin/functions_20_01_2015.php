<?
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

//
// Get the system release number
//
// This has to extract the release from the string as follows
//
// 'file:///var/svn/payex/payex-php/branches/release-1.16'     'release-1.16'
// 'file:///var/svn/payex/payex-php/tags/release-1.16'         'release-1.16'
// 'file:///var/svn/payex/payex-php/trunk'                     'UNRELEASED'
//
function releaseNumber() {
    # The next line is automatically updated when you check out from subversion so don't change it
    $release = '$HeadURL: file:///var/svn/payex/payex-php/branches/release-1.74/admin/functions.php $';
    $pattern = '/(branches|tags)\/(.*)\/admin\/functions.php\s\$/';
    if (preg_match($pattern, $release, $matches)) {
        return $matches[2];
    }
    return 'UNRELEASED';
}

function waitCancellation($transId,$username)
{
	update("update ". TBL_TRANSACTIONS." set transStatus = 'AwaitingCancellation', cancelledBy = '$username', cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='$transId'");
	
	$descript = "Transaction is Awaiting Cancellation";
	activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		$otherNetwork = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$transId."' ");	
		$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$otherNetwork["remoteServerId"]."' and isEnabled = 'Y'");
		if($jointClient["clientId"] != '')
		{
			$otherClient = new connectOtherDataBase();
			$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
			$otherClient-> update("update ".$jointClients[$cl]["dataBaseName"].".transactions
			 set transStatus = 'AwaitingCancellation', cancelledBy = '$username', 
			 cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$otherNetwork["remoteTrans"]."'"
			 );
		$otherClient->closeConnection();
		dbConnect();
		}
	}
	
	insertError("Transaction is Awaiting Cancellation.");	
	
// Mail is sent on Status update of Transaction. b Aslam Shahid	
	if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1" || CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
		$newStatus = "AwaitingCancellation";
		$transQuery = "SELECT	customerID,benID,custAgentID,benAgentID,refNumberIM,refNumber,collectionPointID,
								transType,transDate,currencyFrom,currencyTo,exchangeRate,transAmount,IMFee,
								transactionPurpose,moneyPaid,cashCharges,authoriseDate,createdBy
						FROM ".TBL_TRANSACTIONS." 
						WHERE transID ='".$transId."'";
		$transRS = selectFrom($transQuery);
		$clientCompany = COMPANY_NAME;
		$systemPre = SYSTEM_PRE;
	
		$custID 	= $transRS["customerID"];
		$benID 		= $transRS["benID"];
		$custAgentID = $transRS["custAgentID"];
		$benAgentID = $transRS["benAgentID"];
		$imReferenceNumber = $transRS["refNumberIM"];
		$manualRefrenceNumber = $transRS["refNumber"];
		$collectionPointID = $transRS["collectionPointID"];
		$type  = $transRS["transType"];
		$transDate   = $transRS["transDate"];
		if($transDate!=""){
			$transDate = date("F j, Y", strtotime($transDate));
		}
		$currencyFrom 	= $transRS["currencyFrom"];
		$currencyTo 	= $transRS["currencyTo"];
		
		///Exchange rate used 
		$exRate = $transRS["exchangeRate"];
	
		$transAmount = $transRS["transAmount"];
		$fee = $transRS["IMFee"];
		$localamount =($transAmount * $exRate);
		$purpose = $transRS["transactionPurpose"];
		$totalamount =  $transAmount + $fee;
		$moneypaid = $transRS["moneyPaid"];
		if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
		{
			$cashCharges = $transRS["cashCharges"];
			$totalamount =  $totalamount +$cashCharges;
		}
		if($transRS["authoriseDate"]!=""){
			$strAuthoriseDate = date("F j, Y", strtotime($transRS["authoriseDate"]));
		}
		 
		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		// getting Admin Email address
		$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
		$adminEmail = $adminContents["email"];
		// Getting customer's Agent Email
		if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
		{
		$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
		$custAgentEmail	= $agentContents["email"];
		}
		// getting BEneficiray agent Email
		if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
		{
		$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
		$benAgentEmail	= $agentContents["email"];
		}
		// getting beneficiary email
		$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
		$benEmail = $benContents["email"];
		// Getting Customer Email
		$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
		$custEmail = $custContents["email"];
		
		if($transRS["createdBy"] == 'CUSTOMER')
		{
		
					$subject = "Status Updated";
		
					$custContents = selectFrom("select c_name,Title,FirstName,MiddleName,LastName,c_country,c_city,c_zip,username,c_email,c_phone from cm_customer where c_id= '". $custID ."'");
					$custEmail = $custContents["c_name"];
					$custTitle = $custContents["Title"];
					$custFirstName = $custContents["FirstName"];
					$custMiddleName = $custContents["MiddleName"];
					$custLastName = $custContents["LastName"];
					$custCountry = $custContents["c_country"];
					$custCity = $custContents["c_city"];
					$custZip = $custContents["c_zip"];
					$custLoginName = $custContents["username"];
					$custEmail = $custContents["c_email"];
					$custPhone = $custContents["c_phone"];
		
		
					$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from cm_beneficiary where benID= '". $benID ."'");
					$benTitle = $benContents["Title"];
					$benFirstName = $benContents["firstName"];
					$benMiddleName = $benContents["middleName"];
					$benLastName = $benContents["lastName"];
					$benAddress  = $benContents["Address"];
					$benCountry = $benContents["Country"];
					$benCity = $benContents["City"];
					$benZip = $benContents["Zip"];
					$benEmail = $benContents["email"];
					$benPhone = $benContents["Phone"];
		
		
		/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
					$AdmindLoginName = $AdmindContents["username"];
					$AdmindName = $AdmindContents["name"];
					$AdmindEmail = $AdmindContents["email"];*/
		
		/***********************************************************/
		$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
			<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
		  </tr>";
					
		$messageCust =     "				
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  ".$custTitle." ".$custFirstName." ".$custLastName."  </p></td>
							  </tr>				
					
										  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
									  <tr>
								<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
							  </tr>			
					
					
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
								
					
							  <tr>
								<td> Transaction Type:  ".$type." </td>
								<td> Status: ".$newStatus." </td>
							  </tr>
							  <tr>
								<td> Transaction No:   ".$imReferenceNumber." </td>
								<td> Transaction Date:  ".$transDate."  </td>
							  </tr>
							  <tr>
								<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
								<td> &nbsp;</td>
							  </tr>
							  <tr>
								<td><p>Dated: ".date("F j, Y")."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							
							
							  <tr>
								<td><p><strong>Beneficiary Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Address:  ".$benAddress." </td>
								<td> Postal / Zip Code:  ".$benZip." </td>
							  </tr>
							  <tr>
								<td> Country:   ".$benCountry."   </td>
								<td> Phone:   ".$benPhone."   </td>
							  </tr>
							  <tr>
								<td><p>Email:  ".$benEmail."   </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";
		$messageBen =     "				
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
							  </tr>				
					
										  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
									  <tr>
								<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
							  </tr>			
					
					
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
								
					
							  <tr>
								<td> Transaction Type:  ".$type." </td>
								<td> Status: ".$newStatus." </td>
							  </tr>
							  <tr>
								<td> Transaction No:   ".$imReferenceNumber." </td>
								<td> Transaction Date:  ".$transDate."  </td>
							  </tr>
							  <tr>
								<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
								<td> &nbsp;</td>
							  </tr>
							  <tr>
								<td><p>Dated: ".date("F j, Y")."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							
							
							  <tr>
								<td><p><strong>Sender Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Country:   ".$custCountry."   </td>
								<td> Phone:   ".$custPhone."   </td>
							  </tr>
							  <tr>
								<td><p>Email:  ".$custEmail."   </p></td>
								<td> Postal / Zip Code:  ".$custZip." </td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";			
					if(trim($type) == 'Bank Transfer')
					{
						$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benContents["Country"]."'");
		
						$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);
		
		
						if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
						{
							$message .= "	<tr>
												<td>IBAN</td>
												<td>" . $rstRow["IBAN"] . "</td>
											</tr>";
						}
						else
						{
							$bankNameV = $rstRow["bankName"];
							$branchCodeV = $rstRow["branchCode"];
							$branchAddressV = $rstRow["branchAddress"];
							$swiftCodeV = $rstRow["swiftCode"];
							$accNoV = $rstRow["accNo"];
							$branchCityV = $rstRow["BranchCity"];
							
							$messageRemain="	<tr>
											<td><p><strong>Beneficiary Bank Details </strong></p></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td> Bank Name:  ".$bankNameV."  </td>
											<td> Acc Number:  ".$accNoV."  </td>
										</tr>
										<tr>
											<td> Branch Code:  ".$branchCodeV."  </td>
											<td> Branch Address:  ".$branchAddressV.", ".$branchCityV."  </td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>";
						}
					}
					elseif(trim($type) == "Pick up")
					{
					
					$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone, from cm_collection_point where cp_id  = '". $collectionPointID ."'");
					$agentname = $cContents["cp_corresspondent_name"];
					$contactperson = $cContents["cp_corresspondent_name"];
					$company = $cContents["cp_branch_name"];
					$address = $cContents["cp_branch_address"];
					$country = $cContents["cp_country"];
					$city = $cContents["cp_city"];
					$phone = $cContents["cp_phone"];
					$tran_date = date("Y-m-d");
					$tran_date = date("F j, Y", strtotime("$tran_date"));
					
							$messageRemain = "
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Collection Point Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Agent Name : ".$agentname." </td>
								<td> Contact Person:  ".$contactperson." </td>
							  </tr>
							  <tr>
								<td> Company:  ".$company." </td>
								<td> Address:  ".$address." </td>
							  </tr>
							  <tr>
								<td>Country:   ".$country."</td>
								<td>City:  ".$city."</td>
							  </tr>
							  <tr>
								<td>Phone:  ".$phone."</td>
								<td>&nbsp;</td>
							  </tr> ";
		}
		
		/*if($amount > 500)
		{
			$tempAmount = ($amount - 500);
			$nExtraCharges =  (($tempAmount * 0.50)/100);
			$charges = ($fee + $amount + $nExtraCharges);
		}*/
			
		/*if($trnsid[$i] != "")
		{
		 update("update transactions set IMFee = '$fee',
														 totalAmount  = '$totalamount',
														 localAmount = '$localamount',
														 exchangeRate  = '$exRate'
															 where
															 transID  = '". $trnsid[$i] ."'");
		}*/
		
		$messageEnd ="
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Amount Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Exchange Rate:  ".$exRate."</td>
			<td> Amount:  ".$transAmount." </td>
		  </tr>
		  <tr>
			<td> ".$systemPre." Fee:  ".$fee." </td>
			<td> Local Amount:  ".$localamount." </td>
		  </tr>  <tr>
			<td> Transaction Purpose:  ".$purpose." </td>
			<td> Total Amount:  ".$totalamount." </td>
		  </tr>  <tr>
			<td> Money Paid:  ".$moneypaid." </td>
			<td> Bank Charges:  ".$nExtraCharges." </td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		</table>
		";
		
		}else{
		
		
					$subject = "Status Updated";
		
					$custContents = selectFrom("select Title,firstName,middleName,lastName,Country,City,Zip,email,Zip,Phone,customerStatus from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
					$custTitle = $custContents["Title"];
					$custFirstName = $custContents["firstName"];
					$custMiddleName = $custContents["middleName"];
					$custLastName = $custContents["lastName"];
					$custCountry = $custContents["Country"];
					$custCity = $custContents["City"];
					$custZip = $custContents["Zip"];
					$custEmail = $custContents["email"];
					$custPhone = $custContents["Phone"];
					$custStatus = $custContents["customerStatus"];
		
		
					$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
					$benTitle = $benContents["Title"];
					$benFirstName = $benContents["firstName"];
					$benMiddleName = $benContents["middleName"];
					$benLastName = $benContents["lastName"];
					$benAddress  = $benContents["Address"];
					$benCountry = $benContents["Country"];
					$benCity = $benContents["City"];
					$benZip = $benContents["Zip"];
					$benEmail = $benContents["email"];
					$benPhone = $benContents["Phone"];
		
		
		/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
					$AdmindLoginName = $AdmindContents["username"];
					$AdmindName = $AdmindContents["name"];
					$AdmindEmail = $AdmindContents["email"];*/
		
		/***********************************************************/
		$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
			<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
		  </tr>";
		 $messageCust =" 
		  <tr>
			<td colspan='2'><p><strong>Dear</strong>  ".$custTitle."&nbsp;".$custFirstName."&nbsp;".$custLastName ." </p></td>
		  </tr>
		  <tr>
			<td width='205'>&nbsp;</td>
			<td width='295'>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Transaction Detail: </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		
		  <tr>
			<td> Transaction Type:  ".$type." </td>
			<td> Status: ".$newStatus." </td>
		  </tr>
		  <tr>
			<td> Transaction No:   .".$imReferenceNumber." </td>
			<td> Transaction Date:  ".$transDate."  </td>
		  </tr>
			<tr>
			<td> Manual Reference No:   ".$strManualReferenceNumber ."</td> 
			<td> &nbsp; </td>
		  </tr>
		  <tr>
			<td><p>Dated: ".date("F j, Y")."</p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		
		
		  <tr>
			<td><p><strong>Beneficiary Detail: </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Address:  ".$benAddress." </td>
			<td> Postal / Zip Code:  ".$benZip." </td>
		  </tr>
		  <tr>
			<td> Country:   ".$benCountry."   </td>
			<td> Phone:   ".$benPhone."   </td>
		  </tr>
		  <tr>
			<td><p>Email:  ".$benEmail."   </p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>";

	$messageBen ="				
			  <tr>
				<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
			  </tr>				
	
						  <tr>
				<td width='205'>&nbsp;</td>
				<td width='295'>&nbsp;</td>
			  </tr>
					  <tr>
				<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
			  </tr>			
	
	
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Transaction Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
				
	
			  <tr>
				<td> Transaction Type:  ".$type." </td>
				<td> Status: ".$newStatus." </td>
			  </tr>
			  <tr>
				<td> Transaction No:   ".$imReferenceNumber." </td>
				<td> Transaction Date:  ".$transDate."  </td>
			  </tr>
			  <tr>
				<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
				<td> &nbsp;</td>
			  </tr>
			  <tr>
				<td><p>Dated: ".date("F j, Y")."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			
			
			  <tr>
				<td><p><strong>Sender Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Country:   ".$custCountry."   </td>
				<td> Phone:   ".$custPhone."   </td>
			  </tr>
			  <tr>
				<td><p>Email:  ".$custEmail."   </p></td>
				<td> Postal / Zip Code:  ".$custZip." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";	
		if(trim($type) == 'Bank Transfer')
		{
			$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benCountry."'");
		
			$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
		
			if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
			{
				$messageRemain .= "	<tr>
									<td>IBAN</td>
									<td>" . $rstRow["IBAN"] . "</td>
								</tr>";
			}
			else
			{
				$bankNameV = $rstRow["bankName"];
				$branchCodeV = $rstRow["branchCode"];
				$branchAddressV = $rstRow["branchAddress"];
				$swiftCodeV = $rstRow["swiftCode"];
				$accNoV = $rstRow["accNo"];
		
				$messageRemain="  <tr>
							<td><p><strong>Beneficiary Bank Details </strong></p></td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td> Bank Name:  ".$bankNameV."  </td>
							<td> Acc Number:  ".$branchCodeV."  </td>
						  </tr>
						  <tr>
							<td> Branch Code:  ".$branchAddressV."  </td>
							<td> Branch Address:  ".$swiftCodeV."  </td>
						  </tr>
						  <tr>
							<td><p>Swift Code:  ".$accNoV."  </p></td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						";
			}
		}
		elseif(trim($type) == "Pick up")
		{
		
		$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone from cm_collection_point where cp_id  = '". $collectionPointID ."'");
		$agentname = $cContents["cp_corresspondent_name"];
		$contactperson = $cContents["cp_corresspondent_name"];
		$company = $cContents["cp_branch_name"];
		$address = $cContents["cp_branch_address"];
		$country = $cContents["cp_country"];
		$city = $cContents["cp_city"];
		$phone = $cContents["cp_phone"];
		$tran_date = date("Y-m-d");
		$tran_date = date("F j, Y", strtotime("$tran_date"));
		
		$messageRemain = "
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Collection Point Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Agent Name : ".$agentname." </td>
			<td> Contact Person:  ".$contactperson." </td>
		  </tr>
		  <tr>
			<td> Company:  ".$company." </td>
			<td> Address:  ".$address." </td>
		  </tr>
		  <tr>
			<td>Country:   ".$country."</td>
			<td>City:  ".$city."</td>
		  </tr>
		  <tr>
			<td>Phone:  ".$phone."</td>
			<td>&nbsp;</td>
		  </tr> ";
		
		}
		/*if($amount > 500)
		{
			$tempAmount = ($amount - 500);
			$nExtraCharges =  (($tempAmount * 0.50)/100);
			$charges = ($fee + $amount + $nExtraCharges);
		}*/
		
		
		//Distributor Commition logic
		
		$benAgentID = $transRS["benAgentID"];
		
		$packageQuery="select agentCommission,commPackage from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
					$agentPackage = selectFrom($packageQuery);
		
					$package = $agentPackage["commPackage"];
					
					//
					switch ($package)
						  {
							case "001": // Fixed amount per transaction
							{
								$agentCommi = $agentPackage["agentCommission"];
								$commType = "Fixed Amount";
								break;
							}
							case "002": // percentage of total transaction amount
							{
								$agentCommi = ( $transAmount* $agentPackage["agentCommission"]) / 100;
								$commType = "Percentage of total Amount";
								break;
							}
							case "003": // percentage of IMFEE
							{
								$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
								$commType = "Percentage of Fee";
								break;
							}
							case "004":
							{
								$agentCommi = 0;
								$commType = "None";
								break;
							}
						}				  
						   
					//
				 $agentCommi;
		
		
		// end
		
		
		
		
		/*if($trnsid[$i] != "")
		{
		 update("update transactions set IMFee = '$fee',
														 totalAmount 	 = '$totalamount',
														 localAmount 	 = '$localamount',
														 exchangeRate 	 = '$exRate',
														 distributorComm = '$agentCommi'
															 where
															 transID  = '". $trnsid[$i] ."'");
		}*/
  $messageEnd="
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Amount Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Exchange Rate:  ".$exRate."</td>
			<td> Amount:  ".$transAmount." </td>
		  </tr>
		  <tr>
			<td> ".$systemPre." Fee:  ".$fee." </td>
			<td> Local Amount:  ".$localamount." </td>
		  </tr>  <tr>
			<td> Transaction Purpose:  ".$purpose." </td>
			<td> Total Amount:  ".$totalamount." </td>
		  </tr>  <tr>
			<td> Money Paid:  ".$moneypaid." </td>
			<td> Bank Charges:  ".$nExtraCharges." </td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		<tr>
			<td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
			
		  </tr>
		</table>
		";
		}
		if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			sendMail($custEmail, $subject, $message.$messageCust.$messageRemain.$messageEnd, $fromName, $fromEmail);
		}
		if(CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			sendMail($benEmail, $subject, $message.$messageBen.$messageRemain.$messageEnd, $fromName, $fromEmail);
		}
	}	
}
function confirmCancelled($transId,$username)
{
	
	$isAgentAnD = 'N';
	$isBankAnD = 'N';

	
	update("update " . TBL_TRANSACTIONS . " set transStatus= 'Cancelled', cancelledBy = '$username', refundFee = 'Yes', cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='$transId'");
	
	$descript = "Transaction is Cancelled";
	activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
	
	
	if(CONFIG_LEDGER_AT_CREATION == "1")
	{
					
			$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID, AgentComm, createdBy,commissionMode,currencyFrom,currencyTo,localAmount,IMFee,transDate from " . TBL_TRANSACTIONS . " where transID = '$transId'");
			
				$commission = $amount["AgentComm"];
				$balance = $amount["totalAmount"];
				$agent = $amount["custAgentID"];
				$bank = $amount["benAgentID"];
				$transBalance = $amount["transAmount"];
				$commissionMode = $amount["commissionMode"];
				$currencyFrom = $amount["currencyFrom"];
				$currencyTo = $amount["currencyTo"];
				$localAmount = $amount["localAmount"];
				$IMFee = $amount["IMFee"];
				$transDate = $amount["transDate"];
				
				$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$agent'");
				
				
				$currentBalance = $agentContents["balance"];
				
				if($amount["createdBy"] != 'CUSTOMER')
				{ 
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
						
						if(CONFIG_DOUBLE_ENTRY == '1')
							$doubleEntryAgentComm = '';
							
												$agentIDQuery = selectFrom("select custAgentID,commissionMode  from ". TBL_TRANSACTIONS." where transID = '".$transId."' ");
												$agentID = $agentIDQuery["custAgentID"];
												if(CONFIG_COMMISSION_STATUS_ON_CANCELATION == '1'){ // added this condition against the work done for express i.e if this config is on then paymnet mode is checked from transaction table otherwise from admin table payment mode checked
													$agentPaymentMode =  $agentIDQuery["commissionMode"];
												}else{
													$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$agentID."' ");
													$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
												}
												if($agentPaymentMode == 'exclude')
												{
														$balance = $amount["totalAmount"] - $commission;
														$doubleEntryAgentComm = $IMFee -  $commission;
												
												}elseif($agentPaymentMode == 'include')
												{
														$balance = $amount["totalAmount"];
														$doubleEntryAgentComm = $IMFee;
													
												}
												
								if(CONFIG_DOUBLE_ENTRY == '1')	
									$balance = 	$amount["transAmount"];		
	
						}else{
							$balance = $amount["totalAmount"];
						}
			
						////////////////Either to insert into Agent or A&D Account////////
						if($agentContents["agentType"]=="Sub")
						{
							$agent=$agentContents["parentID"];
							$q = updateAgentAccount($agent, $balance, $transId, "DEPOSIT", "Transaction Cancelled", "Agent",$note,$currencyFrom);
								 updateSubAgentAccount($amount["custAgentID"], $balance, $transId, "DEPOSIT", "Transaction Cancelled", "Agent",$currencyFrom);
						}else{
							$agent=$amount["custAgentID"];
							$q = updateAgentAccount($agent, $balance, $transId, "DEPOSIT", "Transaction Cancelled", "Agent",$note,$currencyFrom);
						}
						//////
						
						updateAdminStaffLedger($transId, "DEPOSIT", "Transaction Cancelled", $note, $currencyFrom);
												
						$currentBalance = $balance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
						
						

					}
					elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
						{
							$strAmountSql = "select amount from agents_customer_account where tranRefNo = ".$transId;
							$arrAmountData = selectFrom($strAmountSql);
							$balance = $arrAmountData["amount"];
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
					 		values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
						else
						{
							//debug("Customer Ledger 2 ...");
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						  values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
			
						$newBalance = $payin["balance"] + $balance ; 
						$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
						update($update_Balance);
						
						if(CONFIG_PAYIN_CUST_AGENT == '1')
						{
							$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
							if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agent!=""){
								$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent."'";
							}
							$agentPayinContents = selectFrom($agentPayinQuery);
							if($agentPayinContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$currencyFrom,getCountryTime(CONFIG_COUNTRY_CODE));
								$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$note,$currencyFrom, getCountryTime(CONFIG_COUNTRY_CODE));
							}else{
								$q = updateAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$note,$currencyFrom, getCountryTime(CONFIG_COUNTRY_CODE));
							}	
						}
					
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					 if(CONFIG_ONLINE_AGENT == '1')
					{	
						
						$descript = "Transaction is Cancelled";
						updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent',$note,$currencyFrom);	
					}	
				}
	
		/*
		 This config added by Niaz Ahmad @4691 (Muthoot) at 17-03-2009. when transaction cancell then
		 reverse entry of profit or loss made by agent in case of manual exchange rate will be done.
		*/
		if(CONFIG_USE_EXTENDED_TRANSACTION == "1")
		{
	   $contentExtendedTrans = selectFrom("select id,transID,margin,agentID from ".TBL_TRANSACTION_EXTENDED."  where transID = '".      										$transId."'");
	   $q2 = "select id,transID,margin,agentID from ".TBL_TRANSACTION_EXTENDED."  where transID = '". $transId."'";
//	   debug($q2);

	   	if($contentExtendedTrans["id"] != "")
				{	
				  if($contentExtendedTrans["margin"] < 0)
				  {
				    $note = "Agent Adjustment Lost ";
					$balance = -($contentExtendedTrans["margin"]);
				$q = updateAgentAccount($agent, $balance, $transId, "DEPOSIT", "Transaction Cancelled", "Agent",$note,$currencyFrom);
				  }elseif($contentExtendedTrans["margin"] > 0){
				    $note = "Agent Adjustment Gain";
					$balance = $contentExtendedTrans["margin"];
				$q = updateAgentAccount($agent, $balance, $transId, "WITHDRAW", "Transaction Cancelled", "Agent",$note,$currencyFrom);
				//debug($q,true);
				  }
				  
				}
		
		}
		
		if(CONFIG_DOUBLE_ENTRY == '1'){
		$param = array(
			 "transID"=>$transId,
			 "commission"=> $doubleEntryAgentComm,
			 "description"=>"Transaction Cancelled",
			 "updated"=>getCountryTime(CONFIG_COUNTRY_CODE),
			 "created"=> $transDate,
			 "action"=>'CANCELLED_TRANSACTION'
		 );
		  updateLedger($param);
		  
		  	$q = updateAgentAccount($agent, $doubleEntryAgentComm, $transId, "DEPOSIT", "Transaction Cancelled", "Agent",$note,$currencyFrom);
		  }
	}
	 //debug($param,true);
	insertError("Transaction is Cancelled .");
		
	// Mail is sent on Status update of Transaction. b Aslam Shahid	
		if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1" || CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			$newStatus = "Cancelled";
			$transQuery = "SELECT	customerID,benID,custAgentID,benAgentID,refNumberIM,refNumber,collectionPointID,
									transType,transDate,currencyFrom,currencyTo,exchangeRate,transAmount,IMFee,
									transactionPurpose,moneyPaid,cashCharges,authoriseDate,createdBy
							FROM ".TBL_TRANSACTIONS." 
							WHERE transID ='".$transId."'";
			$transRS = selectFrom($transQuery);
			$clientCompany = COMPANY_NAME;
			$systemPre = SYSTEM_PRE;
		
			$custID 	= $transRS["customerID"];
			$benID 		= $transRS["benID"];
			$custAgentID = $transRS["custAgentID"];
			$benAgentID = $transRS["benAgentID"];
			$imReferenceNumber = $transRS["refNumberIM"];
			$manualRefrenceNumber = $transRS["refNumber"];
			$collectionPointID = $transRS["collectionPointID"];
			$type  = $transRS["transType"];
			$transDate   = $transRS["transDate"];
			if($transDate!=""){
				$transDate = date("F j, Y", strtotime($transDate));
			}
			$currencyFrom 	= $transRS["currencyFrom"];
			$currencyTo 	= $transRS["currencyTo"];
			
			///Exchange rate used 
			$exRate = $transRS["exchangeRate"];
		
			$transAmount = $transRS["transAmount"];
			$fee = $transRS["IMFee"];
			$localamount =($transAmount * $exRate);
			$purpose = $transRS["transactionPurpose"];
			$totalamount =  $transAmount + $fee;
			$moneypaid = $transRS["moneyPaid"];
			if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
			{
				$cashCharges = $transRS["cashCharges"];
				$totalamount =  $totalamount +$cashCharges;
			}
			if($transRS["authoriseDate"]!=""){
				$strAuthoriseDate = date("F j, Y", strtotime($transRS["authoriseDate"]));
			}
			 
			$fromName = SUPPORT_NAME;
			$fromEmail = SUPPORT_EMAIL;
			// getting Admin Email address
			$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
			$adminEmail = $adminContents["email"];
			// Getting customer's Agent Email
			if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
			{
			$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
			$custAgentEmail	= $agentContents["email"];
			}
			// getting BEneficiray agent Email
			if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
			{
			$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
			$benAgentEmail	= $agentContents["email"];
			}
			// getting beneficiary email
			$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
			$benEmail = $benContents["email"];
			// Getting Customer Email
			$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
			$custEmail = $custContents["email"];
			
			if($transRS["createdBy"] == 'CUSTOMER')
			{
			
						$subject = "Status Updated";
			
						$custContents = selectFrom("select c_name,Title,FirstName,MiddleName,LastName,c_country,c_city,c_zip,username,c_email,c_phone from cm_customer where c_id= '". $custID ."'");
						$custEmail = $custContents["c_name"];
						$custTitle = $custContents["Title"];
						$custFirstName = $custContents["FirstName"];
						$custMiddleName = $custContents["MiddleName"];
						$custLastName = $custContents["LastName"];
						$custCountry = $custContents["c_country"];
						$custCity = $custContents["c_city"];
						$custZip = $custContents["c_zip"];
						$custLoginName = $custContents["username"];
						$custEmail = $custContents["c_email"];
						$custPhone = $custContents["c_phone"];
			
			
						$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from cm_beneficiary where benID= '". $benID ."'");
						$benTitle = $benContents["Title"];
						$benFirstName = $benContents["firstName"];
						$benMiddleName = $benContents["middleName"];
						$benLastName = $benContents["lastName"];
						$benAddress  = $benContents["Address"];
						$benCountry = $benContents["Country"];
						$benCity = $benContents["City"];
						$benZip = $benContents["Zip"];
						$benEmail = $benContents["email"];
						$benPhone = $benContents["Phone"];
			
			
			/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
						$AdmindLoginName = $AdmindContents["username"];
						$AdmindName = $AdmindContents["name"];
						$AdmindEmail = $AdmindContents["email"];*/
			
			/***********************************************************/
			$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
			  <tr>
				<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
			  </tr>";
						
			$messageCust =     "				
								  <tr>
									<td colspan='2'><p><strong>Dear</strong>  ".$custTitle." ".$custFirstName." ".$custLastName."  </p></td>
								  </tr>				
						
											  <tr>
									<td width='205'>&nbsp;</td>
									<td width='295'>&nbsp;</td>
								  </tr>
										  <tr>
									<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
								  </tr>			
						
						
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Transaction Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
									
						
								  <tr>
									<td> Transaction Type:  ".$type." </td>
									<td> Status: ".$newStatus." </td>
								  </tr>
								  <tr>
									<td> Transaction No:   ".$imReferenceNumber." </td>
									<td> Transaction Date:  ".$transDate."  </td>
								  </tr>
								  <tr>
									<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
									<td> &nbsp;</td>
								  </tr>
								  <tr>
									<td><p>Dated: ".date("F j, Y")."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								
								
								  <tr>
									<td><p><strong>Beneficiary Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Address:  ".$benAddress." </td>
									<td> Postal / Zip Code:  ".$benZip." </td>
								  </tr>
								  <tr>
									<td> Country:   ".$benCountry."   </td>
									<td> Phone:   ".$benPhone."   </td>
								  </tr>
								  <tr>
									<td><p>Email:  ".$benEmail."   </p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>";
			$messageBen =     "				
								  <tr>
									<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
								  </tr>				
						
											  <tr>
									<td width='205'>&nbsp;</td>
									<td width='295'>&nbsp;</td>
								  </tr>
										  <tr>
									<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
								  </tr>			
						
						
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Transaction Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
									
						
								  <tr>
									<td> Transaction Type:  ".$type." </td>
									<td> Status: ".$newStatus." </td>
								  </tr>
								  <tr>
									<td> Transaction No:   ".$imReferenceNumber." </td>
									<td> Transaction Date:  ".$transDate."  </td>
								  </tr>
								  <tr>
									<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
									<td> &nbsp;</td>
								  </tr>
								  <tr>
									<td><p>Dated: ".date("F j, Y")."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								
								
								  <tr>
									<td><p><strong>Sender Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Country:   ".$custCountry."   </td>
									<td> Phone:   ".$custPhone."   </td>
								  </tr>
								  <tr>
									<td><p>Email:  ".$custEmail."   </p></td>
									<td> Postal / Zip Code:  ".$custZip." </td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>";			
						if(trim($type) == 'Bank Transfer')
						{
							$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benContents["Country"]."'");
			
							$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							$rstRow = mysql_fetch_array($nResult);
			
			
							if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
							{
								$message .= "	<tr>
													<td>IBAN</td>
													<td>" . $rstRow["IBAN"] . "</td>
												</tr>";
							}
							else
							{
								$bankNameV = $rstRow["bankName"];
								$branchCodeV = $rstRow["branchCode"];
								$branchAddressV = $rstRow["branchAddress"];
								$swiftCodeV = $rstRow["swiftCode"];
								$accNoV = $rstRow["accNo"];
								$branchCityV = $rstRow["BranchCity"];
								
								$messageRemain="	<tr>
												<td><p><strong>Beneficiary Bank Details </strong></p></td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td> Bank Name:  ".$bankNameV."  </td>
												<td> Acc Number:  ".$accNoV."  </td>
											</tr>
											<tr>
												<td> Branch Code:  ".$branchCodeV."  </td>
												<td> Branch Address:  ".$branchAddressV.", ".$branchCityV."  </td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>";
							}
						}
						elseif(trim($type) == "Pick up")
						{
						
						$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone, from cm_collection_point where cp_id  = '". $collectionPointID ."'");
						$agentname = $cContents["cp_corresspondent_name"];
						$contactperson = $cContents["cp_corresspondent_name"];
						$company = $cContents["cp_branch_name"];
						$address = $cContents["cp_branch_address"];
						$country = $cContents["cp_country"];
						$city = $cContents["cp_city"];
						$phone = $cContents["cp_phone"];
						$tran_date = date("Y-m-d");
						$tran_date = date("F j, Y", strtotime("$tran_date"));
						
								$messageRemain = "
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Collection Point Details </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Agent Name : ".$agentname." </td>
									<td> Contact Person:  ".$contactperson." </td>
								  </tr>
								  <tr>
									<td> Company:  ".$company." </td>
									<td> Address:  ".$address." </td>
								  </tr>
								  <tr>
									<td>Country:   ".$country."</td>
									<td>City:  ".$city."</td>
								  </tr>
								  <tr>
									<td>Phone:  ".$phone."</td>
									<td>&nbsp;</td>
								  </tr> ";
			}
			
			/*if($amount > 500)
			{
				$tempAmount = ($amount - 500);
				$nExtraCharges =  (($tempAmount * 0.50)/100);
				$charges = ($fee + $amount + $nExtraCharges);
			}*/
				
			/*if($trnsid[$i] != "")
			{
			 update("update transactions set IMFee = '$fee',
															 totalAmount  = '$totalamount',
															 localAmount = '$localamount',
															 exchangeRate  = '$exRate'
																 where
																 transID  = '". $trnsid[$i] ."'");
			}*/
			
			$messageEnd ="
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Amount Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Exchange Rate:  ".$exRate."</td>
				<td> Amount:  ".$transAmount." </td>
			  </tr>
			  <tr>
				<td> ".$systemPre." Fee:  ".$fee." </td>
				<td> Local Amount:  ".$localamount." </td>
			  </tr>  <tr>
				<td> Transaction Purpose:  ".$purpose." </td>
				<td> Total Amount:  ".$totalamount." </td>
			  </tr>  <tr>
				<td> Money Paid:  ".$moneypaid." </td>
				<td> Bank Charges:  ".$nExtraCharges." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
			";
			
			}else{
			
			
						$subject = "Status Updated";
			
						$custContents = selectFrom("select Title,firstName,middleName,lastName,Country,City,Zip,email,Zip,Phone,customerStatus from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
						$custTitle = $custContents["Title"];
						$custFirstName = $custContents["firstName"];
						$custMiddleName = $custContents["middleName"];
						$custLastName = $custContents["lastName"];
						$custCountry = $custContents["Country"];
						$custCity = $custContents["City"];
						$custZip = $custContents["Zip"];
						$custEmail = $custContents["email"];
						$custPhone = $custContents["Phone"];
						$custStatus = $custContents["customerStatus"];
			
			
						$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
						$benTitle = $benContents["Title"];
						$benFirstName = $benContents["firstName"];
						$benMiddleName = $benContents["middleName"];
						$benLastName = $benContents["lastName"];
						$benAddress  = $benContents["Address"];
						$benCountry = $benContents["Country"];
						$benCity = $benContents["City"];
						$benZip = $benContents["Zip"];
						$benEmail = $benContents["email"];
						$benPhone = $benContents["Phone"];
			
			
			/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
						$AdmindLoginName = $AdmindContents["username"];
						$AdmindName = $AdmindContents["name"];
						$AdmindEmail = $AdmindContents["email"];*/
			
			/***********************************************************/
			$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
			  <tr>
				<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
			  </tr>";
			 $messageCust =" 
			  <tr>
				<td colspan='2'><p><strong>Dear</strong>  ".$custTitle."&nbsp;".$custFirstName."&nbsp;".$custLastName ." </p></td>
			  </tr>
			  <tr>
				<td width='205'>&nbsp;</td>
				<td width='295'>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Transaction Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			
			  <tr>
				<td> Transaction Type:  ".$type." </td>
				<td> Status: ".$newStatus." </td>
			  </tr>
			  <tr>
				<td> Transaction No:   .".$imReferenceNumber." </td>
				<td> Transaction Date:  ".$transDate."  </td>
			  </tr>
				<tr>
				<td> Manual Reference No:   ".$strManualReferenceNumber ."</td> 
				<td> &nbsp; </td>
			  </tr>
			  <tr>
				<td><p>Dated: ".date("F j, Y")."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			
			
			  <tr>
				<td><p><strong>Beneficiary Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Address:  ".$benAddress." </td>
				<td> Postal / Zip Code:  ".$benZip." </td>
			  </tr>
			  <tr>
				<td> Country:   ".$benCountry."   </td>
				<td> Phone:   ".$benPhone."   </td>
			  </tr>
			  <tr>
				<td><p>Email:  ".$benEmail."   </p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";
	
		$messageBen ="				
				  <tr>
					<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
				  </tr>				
		
							  <tr>
					<td width='205'>&nbsp;</td>
					<td width='295'>&nbsp;</td>
				  </tr>
						  <tr>
					<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
				  </tr>			
		
		
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td><p><strong>Transaction Detail: </strong></p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
					
		
				  <tr>
					<td> Transaction Type:  ".$type." </td>
					<td> Status: ".$newStatus." </td>
				  </tr>
				  <tr>
					<td> Transaction No:   ".$imReferenceNumber." </td>
					<td> Transaction Date:  ".$transDate."  </td>
				  </tr>
				  <tr>
					<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
					<td> &nbsp;</td>
				  </tr>
				  <tr>
					<td><p>Dated: ".date("F j, Y")."</p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				
				
				  <tr>
					<td><p><strong>Sender Detail: </strong></p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
				  <tr>
					<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td> Country:   ".$custCountry."   </td>
					<td> Phone:   ".$custPhone."   </td>
				  </tr>
				  <tr>
					<td><p>Email:  ".$custEmail."   </p></td>
					<td> Postal / Zip Code:  ".$custZip." </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>";	
			if(trim($type) == 'Bank Transfer')
			{
				$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benCountry."'");
			
				$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
				$rstRow = mysql_fetch_array($nResult);
			
				if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
				{
					$messageRemain .= "	<tr>
										<td>IBAN</td>
										<td>" . $rstRow["IBAN"] . "</td>
									</tr>";
				}
				else
				{
					$bankNameV = $rstRow["bankName"];
					$branchCodeV = $rstRow["branchCode"];
					$branchAddressV = $rstRow["branchAddress"];
					$swiftCodeV = $rstRow["swiftCode"];
					$accNoV = $rstRow["accNo"];
			
					$messageRemain="  <tr>
								<td><p><strong>Beneficiary Bank Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Bank Name:  ".$bankNameV."  </td>
								<td> Acc Number:  ".$branchCodeV."  </td>
							  </tr>
							  <tr>
								<td> Branch Code:  ".$branchAddressV."  </td>
								<td> Branch Address:  ".$swiftCodeV."  </td>
							  </tr>
							  <tr>
								<td><p>Swift Code:  ".$accNoV."  </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							";
				}
			}
			elseif(trim($type) == "Pick up")
			{
			
			$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone from cm_collection_point where cp_id  = '". $collectionPointID ."'");
			$agentname = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			
			$messageRemain = "
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Collection Point Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Agent Name : ".$agentname." </td>
				<td> Contact Person:  ".$contactperson." </td>
			  </tr>
			  <tr>
				<td> Company:  ".$company." </td>
				<td> Address:  ".$address." </td>
			  </tr>
			  <tr>
				<td>Country:   ".$country."</td>
				<td>City:  ".$city."</td>
			  </tr>
			  <tr>
				<td>Phone:  ".$phone."</td>
				<td>&nbsp;</td>
			  </tr> ";
			
			}
			/*if($amount > 500)
			{
				$tempAmount = ($amount - 500);
				$nExtraCharges =  (($tempAmount * 0.50)/100);
				$charges = ($fee + $amount + $nExtraCharges);
			}*/
			
			
			//Distributor Commition logic
			
			$benAgentID = $transRS["benAgentID"];
			
			$packageQuery="select agentCommission,commPackage from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
						$agentPackage = selectFrom($packageQuery);
			
						$package = $agentPackage["commPackage"];
						
						//
						switch ($package)
							  {
								case "001": // Fixed amount per transaction
								{
									$agentCommi = $agentPackage["agentCommission"];
									$commType = "Fixed Amount";
									break;
								}
								case "002": // percentage of total transaction amount
								{
									$agentCommi = ( $transAmount* $agentPackage["agentCommission"]) / 100;
									$commType = "Percentage of total Amount";
									break;
								}
								case "003": // percentage of IMFEE
								{
									$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
									$commType = "Percentage of Fee";
									break;
								}
								case "004":
								{
									$agentCommi = 0;
									$commType = "None";
									break;
								}
							}				  
							   
						//
					 $agentCommi;
			
			
			// end
			
			
			
			
			/*if($trnsid[$i] != "")
			{
			 update("update transactions set IMFee = '$fee',
															 totalAmount 	 = '$totalamount',
															 localAmount 	 = '$localamount',
															 exchangeRate 	 = '$exRate',
															 distributorComm = '$agentCommi'
																 where
																 transID  = '". $trnsid[$i] ."'");
			}*/
	  $messageEnd="
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Amount Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Exchange Rate:  ".$exRate."</td>
				<td> Amount:  ".$transAmount." </td>
			  </tr>
			  <tr>
				<td> ".$systemPre." Fee:  ".$fee." </td>
				<td> Local Amount:  ".$localamount." </td>
			  </tr>  <tr>
				<td> Transaction Purpose:  ".$purpose." </td>
				<td> Total Amount:  ".$totalamount." </td>
			  </tr>  <tr>
				<td> Money Paid:  ".$moneypaid." </td>
				<td> Bank Charges:  ".$nExtraCharges." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			<tr>
				<td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
				
			  </tr>
			</table>
			";
			}
			if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
				sendMail($custEmail, $subject, $message.$messageCust.$messageRemain.$messageEnd, $fromName, $fromEmail);
			}
			if(CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
				sendMail($benEmail, $subject, $message.$messageBen.$messageRemain.$messageEnd, $fromName, $fromEmail);
			}
		}	
	}
	
	

function UploadFile($Bext,$Uext)
{

	if ($BankcsvFile=='none' && $UsercsvFile=='none')
	{
		$msg = "Please select CSV files.";
		header("Location: import-excel-files.php?msg=$msg");
	}
	$uploadDir = '../uploads/';
	$uploadFile = $uploadDir . $_FILES['BankcsvFile']['name'];
	$uploadFile2 = $uploadDir . $_FILES['UsercsvFile']['name'];

	if($uploadFile == "./uploads/")
	{
	 return $BankcsvFile;
	}
	if($uploadFile == "./uploads/")
	{
	 return $UsercsvFile;
	}
		if (($Bext == ".csv"  ) && ( $Uext == ".csv"  ))
		{		
				$arrName=explode(".",$uploadFile);
				$arrName2=explode(".",$uploadFile2);			
				$uploadFile=$uploadDir."Bank_Report".".".$arrName[sizeof($arrName)-1];
				$uploadFile2=$uploadDir."Daily_Activity_report".".".$arrName2[sizeof($arrName2)-1];
				//$reUploadFile = $nTime.".".$arrName[sizeof($arrName)-1];			
						
				if (move_uploaded_file($_FILES['BankcsvFile']['tmp_name'], $uploadFile) && move_uploaded_file($_FILES['UsercsvFile']['tmp_name'], $uploadFile2))
				{
					//print "File is valid, and was successfully uploaded. ";//print "Here's some more debugging info:\n";//print_r($_FILES);
				}
				else
				{
					print "Possible file upload attack!  Here's some debugging info:\n";
					print_r($_FILES);
				}	
				
				//return $reUploadFile;
		}
		else
		{
					$msg = "File format must be  .csv";
					header("Location: import-excel-files.php?msg=$msg");		
		}
}


function ibanCountries($country)
{
	$listCountry[0] = "Austria";
	$listCountry[1] = "Belgium";
	$listCountry[2] = "Denmark";
	$listCountry[3] = "Finland";
	$listCountry[4] = "France";
	$listCountry[5] = "Germany";
	$listCountry[6] = "Greece";
	$listCountry[7] = "Iceland";
	$listCountry[8] = "Ireland";
	$listCountry[9] = "Italy";
	$listCountry[10] = "Luxembourg";
	$listCountry[11] = "Netherlands";
	$listCountry[12] = "Norway";
	$listCountry[13] = "Poland";
	$listCountry[14] = "Portugal";
	$listCountry[15] = "Spain";
	$listCountry[16] = "Sweden";
	$listCountry[17] = "Switzerland";
	$listCountry[18] = "United Kingdom";
	
	if(in_array($country, $listCountry))
		return true;
	else
		return false;
}

function getAgentType()
{
	
	global $username;
	
	if($username != "")
	{
			$users = selectFrom("select parentID, agentType, adminType, isCorrespondent, isMain from " . TBL_ADMIN_USERS . " where username='$username'");
		if(isExist("select parentID, agentType, adminType, isCorrespondent, isMain from " . TBL_ADMIN_USERS . " where username='$username'"))	
		{
	
			if(trim($users["adminType"])=="Supper" && trim($users["isMain"]) == "Y")
			{
				return "admin";
			}
			elseif(trim($users["adminType"])!="Agent" && trim($users["isMain"]) == "N")
			{
				return trim($users["adminType"]);
			}
			elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Supper")
			{
				switch ($users["isCorrespondent"])
				{
					case "Y":
					{
						return "SUPAI"; // super-agent-IDA
						break;
					}
					case "N":
					{
						return "SUPA"; // super-agent
						break;
					}
					case "ONLY":
					{
						return "SUPI"; // super--IDA
						break;
					}
				}
			}
			elseif(trim($users["adminType"])=="Agent" && trim($users["agentType"])=="Sub")
			{
				switch ($users["isCorrespondent"])
				{
					case "Y":
					{
						return "SUBAI"; // super-agent-IDA
						break;
					}
					case "N":
					{
						return "SUBA"; // super-agent
						break;
					}
					case "ONLY":
					{
						return "SUBI"; // super--IDA
						break;
					}
				}
			}elseif(trim($users["adminType"])== "Branch Manager")
			{
				return "Branch Manager";
				break;
				}elseif(trim($users["adminType"])== "Support")
				{
				return "Support";//payex supprot person
				break;
				}elseif(trim($users["adminType"])== "MLRO")
				{
				return "MLRO";//payex supprot person
				break;
				}
			}elseif(isExist("select tellerID, loginName, name from " . TBL_TELLER . " where loginName='".$username."'"))
			{
	
				return "TELLER";
				//return false;
			}
			elseif(isExist("select userID, userName, name from " . TBL_USER . " where userName='".$username."'"))
			{
	
				return "PAYING BOOK CUSTOMER";
				//return false;
			}
	}
	else
	{
		echo("User Name is empty");
		return false;
	}
}

function getExchangeRate($custCountry, $benCountry)
{
	//debug_print_backtrace();
	if($custCountry != "" && $benCountry != "")
	{
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'");
		$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country = '$benCountry'");

		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			$rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				if ($exchangeRateFrom["primaryExchange"] != 0)
				{
					$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
				}	
			}
			else
			{
				$rate =round(1 - (1*$exchangeRateTo["marginPercentage"]) /  100 , 4);			
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];
		
		return $data;
	}
}
///
function getMultipleExchangeRates($custCountry, $benCountry, $currencyTo, $dist, $amount, $dDate, $currencyFrom,$transType,$paymentMode, $agent="")
{
	//debug_print_backtrace();
	//echo $custCountry." ".$benCountry." ".$currencyTo." ".$dist." ".$amount." ".$dDate." ".$currencyFrom." ".$transType." ".$paymentMode." ".$agent;
	//$fromCountry = selectFrom("select * from " . TBL_COUNTRY . " where countryName = '$custCountry' ");
	
	/**
	 * Modifiy the exchange rate calculation functionality, to fix a know bug, that
	 * if you enter two exchange rate with same country and currencies, than it will pick the old one of that day
	 * instead of the latest one on a given day
	 *
	 * For this in where clause of query the "dated" field is replaced by the "updationDate" field, as this field 
	 * also has time stamp with date
	 * @Ticket #4368
	 */
	
	if($custCountry != "" && $benCountry != "" && $dDate != "")
	{
		
			
		////Money Paid By
		 $exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
		 //echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		
		// customer type
		if($exchangeRate["primaryExchange"] == "" && CONFIG_CUSTOMER_CATEGORY == '1')
		{
		 $custData = selectFrom("select cust_category_id from customer where customerID = '".$agent."'");
		 $catId = $custData["cust_category_id"];
		$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'customer'  and rateValue = '$catId') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' order by updationDate desc");
		//debug(__LINE__);
		}
		
		////Trans Type
		if($exchangeRate["primaryExchange"] == "")
		{
		$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'transType'  and rateValue = '$transType') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
		//echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'transType'  and rateValue = '$transType') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		}
		
		////Agent
		if($exchangeRate["primaryExchange"] == "" || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0))
		{
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'agent'  and rateValue = '$agent') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
			
			//echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'agent'  and rateValue = '$agent') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		}
		
		///Distributor
		if($exchangeRate["primaryExchange"] == "")
		{
			
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'distributor'  and rateValue = '$dist') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
			//echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = 'distributor'  and rateValue = '$dist') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		}
	/////Generic
		if($exchangeRate["primaryExchange"] == "")
		{
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
			//echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (updationDate >='$dDate 00:00:00' and updationDate <= '$dDate 23:59:59') and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		}
		
		if($exchangeRate["marginType"] == 'fixed')
		{
			$rate = $exchangeRate["primaryExchange"] - $exchangeRate["marginPercentage"];
			}else{
			$rate = round($exchangeRate["primaryExchange"] - ($exchangeRate["primaryExchange"] * $exchangeRate["marginPercentage"]) /  100 , 4);
		}
		
		
		$data[] = $exchangeRate["erID"];
		$data[] = $rate;
		$data[] = $currencyFrom;
		$data[] = $exchangeRate["currency"];
		
		return $data;
	}
	elseif($custCountry != "" && $benCountry != "")
	{
		if($dist == "bank24hr")
		{
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'bank24hr' and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  rateFor = 'bank24hr' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' )");
			$x = __LINE__."select * from " . TBL_EXCHANGE_RATES . " where  country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'bank24hr' and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  rateFor = 'bank24hr' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' )";
			
			//debug($x);
			
			if($exchangeRate["primaryExchange"] != "")
			{
				if($exchangeRate["_24hrMarginType"] == "lower")
				{
					$exchangeRate["primaryExchange"] = $exchangeRate["primaryExchange"] - $exchangeRate["_24hrMarginValue"];
				}
				else
				{
					$exchangeRate["primaryExchange"] = $exchangeRate["primaryExchange"] + $exchangeRate["_24hrMarginValue"];
				}
				//debug($exchangeRate["primaryExchange"]);
			}
			if($exchangeRate["primaryExchange"] == "")
			{
				$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
		 		$x =  "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
			}
		}
		else
		{
		//////Payment Mode
		 $exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' ");
		 $x =  "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		}
			// customer type
		if($exchangeRate["primaryExchange"] == "" && CONFIG_CUSTOMER_CATEGORY == '1')
		{
		 $custData = selectFrom("select cust_category_id from customer where customerID = '".$agent."'");
		 $catId = $custData["cust_category_id"];
		$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'customer'  and rateValue = '$catId') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' order by updationDate desc");
		//debug(__LINE__);
		}
		////TransType
		if($exchangeRate["primaryExchange"] == "")
		{
		$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'transType'  and rateValue = '$transType') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' order by updationDate desc");
		$x = "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and currency = '$currencyTo' and (rateFor = 'transType'  and rateValue = '$transType') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom'";
		//debug(__LINE__);
		}
		if(CONFIG_DIST_RATE_PRIORITY == "1")
		{ 
        	$agentData = selectFrom("select unlimitedRate from admin where userID ='".$agent."' "); 
         
            ///For Distributor 
            if($exchangeRate["primaryExchange"] == "") 
            { 
                        //debug(__LINE__); 
                        $exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$dist' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and rateValue = '$dist' and currencyOrigin = '$currencyFrom' and isActive !='N' )"); 
                         
                        $x =  "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$dist' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and rateValue = '$dist' and currencyOrigin = '$currencyFrom')"; 
            } 
                 
                 
                ///Agent 
				/**
				 * Change the condition from 
				 * $exchangeRate["primaryExchange"] == "" || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0 && $agentData["unlimitedRate"]== 'Y')
				 * to 
				 * $exchangeRate["primaryExchange"] == "" && (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0 && $agentData["unlimitedRate"]== 'Y')
				 * to fix the bug found in the banking rate calculation logic
				 * The banking rate logic here goes changed as "OR" condition involves
				 * @Ticket #3337
				 */
                if($exchangeRate["primaryExchange"] == "" && (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0 && $agentData["unlimitedRate"]== 'Y')) 
                { 
                       $exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N' )"); 
                        $x =  __LINE__."select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom')"; //debug(__LINE__);
                } 
                         
                         
			}
			else
			{ 
				///Agent
		/**
		 * //if($exchangeRate["primaryExchange"] == "" || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0)) changed the || to && in 
		 * this condition since in case of transaction type exrate added it still was entring this condition by khola against 3712
		 * Change the condition from 
		 * $exchangeRate["primaryExchange"] == "" || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0)
		 * to 
		 * $exchangeRate["primaryExchange"] == "" && (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0)
		 * to fix the bug found in the banking rate calculation logic
		 * The banking rate logic here goes changed as "OR" condition involves
		 * @Ticket #3337
		 */
		if($exchangeRate["primaryExchange"] == "" && (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0))
		{
			//echo "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom')";
			
			/* Check if the denomination based exchange rates exists in system, if the related config is ON */
			if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
			{
				$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N'  and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount')");

				$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and isDenomination='Y' and parentRateId = erID and amountFrom <= $amount and amountTo >= $amount and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . ", denominationBasedRate where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N'  and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount')";
				//debug($x);
			//debug(__LINE__);	
			}
			/* If rate does not found or config is off thean agent based normal exchange rate will be looked */
			if($exchangeRate["primaryExchange"] == "")
			{
				$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and isDenomination='N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='N')");
				$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$agent' and isDenomination='N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$agent' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='N')";
				//debug(__LINE__);
			}
			
			/** 
			 * Check exchange rate for super Agent 
			 * If no rate found for it self
			 * @Ticket# 3509
			 */
			if($exchangeRate["primaryExchange"] == "" && CONFIG_SUB_AGENT_AT_EXCHANGE_RATE == 1)
			{
				/* Get the suer agent*/
				$strSuperAgentSql = "select parentID from admin where userID=".$agent;
				$strSuperAgentData = selectFrom($strSuperAgentSql);
				$strSuperAgent = $strSuperAgentData["parentID"];
				
				
				$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$strSuperAgent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$strSuperAgent' and currencyOrigin = '$currencyFrom' and isActive !='N' )");
				$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$strSuperAgent' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'agent' and currency = '$currencyTo' and rateValue = '$strSuperAgent' and currencyOrigin = '$currencyFrom' and isActive !='N' )";
				//debug(__LINE__);	
			}
			
			/* End #3509 */
			
		}
		
		///For Distributor
		if($exchangeRate["primaryExchange"] == "")
		{
			
			
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$dist' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and rateValue = '$dist' and currencyOrigin = '$currencyFrom' and isActive !='N' )");
			
			$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and rateValue = '$dist' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and rateFor = 'distributor' and currency = '$currencyTo' and rateValue = '$dist' and currencyOrigin = '$currencyFrom')";
		//debug(__LINE__);
		}
		
}
		
		///Generic
		
		/* Check if the denomination based exchange rates exists for the generic exchange rate , if the related config is ON */
		if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $exchangeRate["primaryExchange"] == "")
		{
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . ",denominationBasedRate where  country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' )");
			
			
			
			$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . ",denominationBasedRate where  country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='Y' and parentRateId = erID and amountFrom <= '$amount' and amountTo >= '$amount' )";
			//debug(__LINE__);		
		}
		
		if($exchangeRate["primaryExchange"] == "")
		{

			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and isDenomination='N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='N'  )");
			///Generic One
		
			$x = __LINE__. "select * from " . TBL_EXCHANGE_RATES . " where  country='$benCountry' and countryOrigin='$custCountry' and (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo'  and currencyOrigin = '$currencyFrom' and isDenomination='N' and updationDate =(select  Max(updationDate) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin='$custCountry' and  (rateFor = '' OR rateFor = 'generic') and currency = '$currencyTo' and currencyOrigin = '$currencyFrom' and isActive !='N' and isDenomination='N'  )";
			//debug(__LINE__);
		}
		
				
		if($exchangeRate["marginType"] == 'fixed')
		{
			$rate = $exchangeRate["primaryExchange"] - $exchangeRate["marginPercentage"];
			}else{	
			$rate =round($exchangeRate["primaryExchange"] - ($exchangeRate["primaryExchange"] * $exchangeRate["marginPercentage"]) /  100 , 4);
		}
		
		
		if(CONFIG_AGENT_RATE_MARGIN == "1" && $agent > 0){
			
			
		
		if($exchangeRate["agentMarginType"] == 'fixed')
		{
			$rate = $rate - $exchangeRate["agentMarginValue"];
			}else{	
			$rate =round($rate - ($rate * $exchangeRate["agentMarginValue"]) /  100 , 4);
		}
		
	}
		
		$data[] = $exchangeRate["erID"];
		$data[] = $rate;
		$data[] = $currencyFrom;
		$data[] = $exchangeRate["currency"];

		//debug( $x );
		//debug($data);
		return $data;	
	
	}
	
}
function getCalculatorExchangeRates($custCountry, $benCountry,$dist,$amount)
{
	//debug_print_backtrace();
	if($custCountry != "" && $benCountry != "")
	{	
		$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin = '$custCountry' and rateFor = 'distributor' and rateValue = '$dist' and dated =(select  Max(dated) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin = '$custCountry') and (rateFor = 'distributor' and rateValue = '$dist')");
		if($exchangeRate["primaryExchange"] == "")
		{
			$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin = '$custCountry' and dated =(select  Max(dated) from " . TBL_EXCHANGE_RATES . " where country='$benCountry' and countryOrigin = '$custCountry') and (rateFor = '' OR rateFor = 'generic')");		
		}
		
		if($exchangeRate["marginType"] == 'fixed')
		{
			$rate = $exchangeRate["primaryExchange"] - $exchangeRate["marginPercentage"];
		}else{				
			$rate =round($exchangeRate["primaryExchange"] - ($exchangeRate["primaryExchange"] * $exchangeRate["marginPercentage"]) /  100 , 4);
		}
		
		$fromCountry = selectFrom("select * from " . TBL_COUNTRY . " where countryName = '$custCountry' ");
		
		$data[] = $exchangeRate["erID"];
		$data[] = $rate;
		$data[] = $fromCountry["currency"];
		$data[] = $exchangeRate["currency"];
		
		return $data;	
	}
}
///
function getExchangeRateTransaction($custCountry, $benCountry, $currencyTo)
{
	//debug_print_backtrace();
	if($custCountry != "" && $benCountry != "")
	{
		$exchangeRateFrom = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where country='$custCountry'");
		$exchangeRateTo = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where  currency = '$currencyTo'");

		if(strtolower($benCountry) ==  strtolower("United Kingdom"))
		{
			$rate =  round($exchangeRateTo["primaryExchange"] - (($exchangeRateTo["primaryExchange"]) * $exchangeRateTo["marginPercentage"] / 100 ) , 4);
		}
		else
		{
			if(strtolower($benCountry) != strtolower($custCountry))
			{
				if ($exchangeRateFrom["primaryExchange"] != 0)
				{
					$rate = round ( ($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) - (($exchangeRateTo["primaryExchange"] / $exchangeRateFrom["primaryExchange"]) * $exchangeRateTo["marginPercentage"]) / 100   , 4);			
				}
			}
			else
			{
				$rate =round(1 - (1*$exchangeRateTo["marginPercentage"]) /  100 , 4);			
			}
		}
		$data[] = $exchangeRateTo["erID"];
		$data[] = $rate;
		$data[] = $exchangeRateFrom["currency"];
		$data[] = $exchangeRateTo["currency"];
		
		return $data;
	}
}

function imFee($amount, $origCountry, $destCountry,$feetransType, $currencyFrom, $currencyTo,$collectionPointID,$distID,$customerID = '')
{
	//debug_print_backtrace();
	if($amount > 0 && $origCountry != "" && $destCountry != "")
	{
		$fee = 0;
		
		
		/** 
		 * #5957:Premierexchange
		 * Added new customer category
		 * by Niaz Ahmad
 		*/ 
		if(CONFIG_CUSTOMER_CATEGORY == '1')
		{
			if(!empty($customerID))
			{
				$custCatId = selectFrom("select cust_category_id from ".TBL_CUSTOMER." where customerID = '".$customerID."'");
				//debug("select cust_category_id from ".TBL_CUSTOMER." where customerID = '".$customerID."'");
				$catId = $custCatId["cust_category_id"];
					  // To get denominator Fee Value
						$imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$catId."' and feeBasedOn = 'customer'";
						//debug($imFeeQuery);
						 $imFee = selectFrom($imFeeQuery);
			}
			
		}
		 // Added By Niaz Ahmad #2538
	  	if(CONFIG_FEE_BASED_TRANSTYPE == "1")
	  	{
	  		/*
			 This code added by Niaz  Ahmad at 280-04-2008 @4546 Faith Exchange Commission Model
			*/
	  		if($feetransType == "Pick up")
			{
			    
					// To get Collection Point Fee Value
				if(CONFIG_FEE_BASED_COLLECTION_POINT == "1"){	
					 if($collectionPointID !=''){
					  $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType = 'collectionPoint' and agentNo = '$collectionPointID'";
					   $imFeeCP = selectFrom($imFeeQuery);
					   //debug($imFeeQuery);
					   }
					} 
					
				if(CONFIG_FEE_DISTRIBUTOR == "1"){	   
					   // To get distributor base Fee value
					 if($imFeeCP["feeID"] == '' && $distID !=''){
					   $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType = 'distributorBased' and agentNo = '$distID'";
					   $imFeeDist = selectFrom($imFeeQuery);
						//debug($imFeeQuery);
					 }
				  }	 
					  // To get denominator Fee Value
					   if($imFeeDist["feeID"] == '' && $imFeeCP["feeID"] == ''){
						$imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType ='denominator'";
						$imFeeDenominator = selectFrom($imFeeQuery);
						//debug($imFeeQuery);
					 }
					   // To get percentage Fee Value
					 if($imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
						$imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType = 'percent'";
						$imFeePercent = selectFrom($imFeeQuery);
						//debug($imFeeQuery);
					 }	
						// To get Fixed Fee Value	
					  if($imFeePercent["feeID"] == '' && $imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
						$imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and  feeType = 'fixed'";
						$imFeeFixed = selectFrom($imFeeQuery);
						//debug($imFeeQuery);
					 }										 
			}elseif($feetransType == "Bank Transfer")
			{
	 			
				if(CONFIG_FEE_DISTRIBUTOR == "1"){	 
				 // To get distributor base Fee value
				 if($distID !=''){
				   $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType = 'distributorBased' and agentNo = '$distID'";
	 		       $imFeeDist = selectFrom($imFeeQuery);
			        //debug($imFeeQuery);
				  }
				} 
				  // To get denominator Fee Value
				   if($imFeeDist["feeID"] == '' && $imFeeCP["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType ='denominator'";
	 		        $imFeeDenominator = selectFrom($imFeeQuery);
				    //debug($imFeeQuery);
				 }
				   // To get percentage Fee Value
				 if($imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and feeType = 'percent'";
	 		        $imFeePercent = selectFrom($imFeeQuery);
					//debug($imFeeQuery);
				 }	
				 	// To get Fixed Fee Value	
				  if($imFeePercent["feeID"] == '' && $imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' and  feeType = 'fixed'";
	 		        $imFeeFixed = selectFrom($imFeeQuery);
					//debug($imFeeQuery);
				 }
			}
			  $imFee = selectFrom($imFeeQuery);
		}// end config if
	    //else
		     //////****************      Generic Fee   ************//////////
			
		  	if($imFee["feeID"] == '')
		    { 
						       
			     // To get Generic Fee. First periority is Collection Point
			    				
			  if(CONFIG_FEE_BASED_COLLECTION_POINT == "1"){	
				// To get Collection Point Fee Value
				 if($collectionPointID !=''){
				  $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL ) and feeType = 'collectionPoint' and agentNo = '$collectionPointID'";
	 		       $imFeeCP = selectFrom($imFeeQuery);
				   //debug($imFeeQuery);
				   }
				  }
				  
			   if(CONFIG_FEE_DISTRIBUTOR == "1"){	 
				   // To get distributor base Fee value
				 if($imFeeCP["feeID"] == '' && $distID !=''){
				   $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL ) and feeType = 'distributorBased' and agentNo = '$distID'";
	 		       $imFeeDist = selectFrom($imFeeQuery);
			       // debug($imFeeQuery);
				 }
				} 
				  // To get denominator Fee Value
				  if($imFeeDist["feeID"] == '' && $imFeeCP["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL ) and feeType ='denominator'";
	 		        $imFeeDenominator = selectFrom($imFeeQuery);
				    //debug($imFeeQuery);
				 }
				   // To get percentage Fee Value
				  if($imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL ) and feeType = 'percent'";
	 		        $imFeePercent = selectFrom($imFeeQuery);
					//debug($imFeeQuery);
				 }	
				 	// To get Fixed Fee Value	
				  if($imFeePercent["feeID"] == '' && $imFeeDenominator["feeID"] == '' && $imFeeCP["feeID"] == '' && $imFeeDist["feeID"] == ''){
		  		    $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")."  from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL ) and  feeType = 'fixed'";
	 		        $imFeeFixed = selectFrom($imFeeQuery);
					//debug($imFeeQuery);
				 }
	  	}
	  	   
	  	   if(CONFIG_FEE_CURRENCY == '1')
	  	   {
  	   		$imFeeQuery .= " and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."'";
	  	   }
	  	   $imFeeQuery .= " and isActive != 'N' ";
	 
	 			/**
	 			 * Introducing the commission based on the payment mode
	 			 * @TicketNo 3319
	 			 */
	 			
	 			if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == 1)
	 			{      
						$paymentMode = "";
	 					if(!empty($_SESSION["moneyPaid"]))
	 					{
	 						if($_SESSION["moneyPaid"] == "By Cash")
	 							$paymentMode = "cash";
	 						elseif($_SESSION["moneyPaid"] == "By Bank Transfer")
	 							$paymentMode = "bank";
	 						if($_SESSION["moneyPaid"] == "By Cheque")
	 							$paymentMode = "cheque";
	 						
	 						
	 						if(CONFIG_MONEY_PAID_OPTION_BY_CARD == 1 && $_SESSION["moneyPaid"] == "By Card")
	 							$paymentMode = "card";
	 							
	 					}
	 					$imFeeQuery .= " and paymentMode = '$paymentMode' ";
	 			}
	 				 
	 			
	 $imFee = selectFrom($imFeeQuery);
	 //debug($imFeeQuery);	
	 //debug($imFee);
		
		if($imFee["Fee"] != "" && $imFee["Fee"] != 0)
		{
			if($imFee["feeType"]=="percent")
            {
            	$fee = ($amount * $imFee["Fee"])/100;
            }
            else
			{
				/* @Ticket #3820 */
				if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
					( $imFee["feeType"]=="denominator" || $imFee["feeType"]=="agentDenom" || $imFee["feeType"]=="agentBased" ))
				{
					if($imFee["feeSubType"] == "percent")
					{
						$fee = ($amount * $imFee["Fee"])/100;
						//debug($fee." = ".$amount." * ".$imFee["Fee"]);
					}
					else
						$fee = $imFee["Fee"];
				}	
				else
	            	$fee = $imFee["Fee"];
            }
    // echo "Fee=".$fee;       	
	  
		 return $fee;
		}
		else
		{
			return false;
		}
	}
}


function imFeePayin($amount, $origCountry, $destCountry,$feetransType, $currencyFrom, $currencyTo,$customerID = '')
{
	//debug_print_backtrace();
	if($amount > 0 && $origCountry != "" && $destCountry != "")
	{
		$fee = 0;
		 
		 /** 
		 * #5957:Premierexchange
		 * Added new customer category
		 * by Niaz Ahmad
 		*/ 
		if(CONFIG_CUSTOMER_CATEGORY == '1')
		{
			if(!empty($customerID))
			{
				$custCatId = selectFrom("select cust_category_id from ".TBL_CUSTOMER." where customerID = '".$customerID."'");
				//debug("select cust_category_id from ".TBL_CUSTOMER." where customerID = '".$customerID."'");
				$catId = $custCatId["cust_category_id"];
				 $imFeeQuery = "select feeID,payinFee, feeType,feeBasedOn,transactionType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType = '".$catId."' AND feeBasedOn = 'customer'";	 
			//debug($imFeeQuery);
			}
			 $imFee = selectFrom($imFeeQuery);
		}
		
		
		 // Added By Niaz Ahmad #2538
			    if(CONFIG_FEE_BASED_TRANSTYPE == "1" && $imFee["feeID"] == '') {
			    	
			    	if( $feetransType == "Pick up")
					{
			    			  $imFeeQuery = "select Fee, feeType,feeBasedOn,transactionType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and feeBasedOn = 'transactionType' ";		
			    	}
					else
					{
			    			   $imFeeQuery = "select payinFee, feeType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType != 'Pick up' ";
			    	}
					  $imFee = selectFrom($imFeeQuery);
				}
					
				if($imFee["feeID"] == '')
				{
				   $imFeeQuery = "select payinFee, feeType from " . TBL_IMFEE . " where amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL) ";
		      //debug($imFeeQuery);
			   }	    
	     
	   if(CONFIG_FEE_CURRENCY == '1')
	   {
   		$imFeeQuery .= " and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."'";
	   }
	   
	   $imFeeQuery .= " and isActive != 'N' ";
	   
	    /**
			 * Introducing the commission based on the payment mode
			 * @TicketNo 3319
			 */
			
			if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == 1)
			{
					$paymentMode = "";
					if(!empty($_SESSION["moneyPaid"]))
					{
						if($_SESSION["moneyPaid"] == "By Cash")
							$paymentMode = "cash";
						elseif($_SESSION["moneyPaid"] == "By Bank Transfer")
							$paymentMode = "bank";
						if($_SESSION["moneyPaid"] == "By Cheque")
							$paymentMode = "cheque";
					}
					$imFeeQuery .= " and paymentMode = '$paymentMode' ";
			}
	   
	   $imFee = selectFrom($imFeeQuery);
	   
		if($imFee["payinFee"] != "" && $imFee["payinFee"] != 0)
		{
			if($imFee["feeType"]=="percent")
            	{
            		$fee = ($amount * $imFee["payinFee"])/100;
            		}
            	else{
            		$fee = $imFee["payinFee"];
            		}
			return $fee;
		}
		else
		{
			return false;
		}
	}
}


function imFeeAgent($amount, $origCountry, $destCountry, $agentID,$feetransType, $currencyFrom, $currencyTo)
{
	//debug_print_backtrace();
	if($amount > 0 && $origCountry != "" && $destCountry != "")
	{
		$fee = 0;
		       
		        // Added By Niaz Ahmad #2538  
		    if(CONFIG_FEE_BASED_TRANSTYPE == "1" ){
		    	 	if($feetransType == "Pick up"){
		    		 	 $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")." from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and  feeBasedOn ='transactionType' ";
						// debug($imFeeQuery);
		    }else{
		      		 $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")." from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType != 'Pick up'";			    	 	
		    			 //debug($imFeeQuery);
						} 
		    	 } 
		    	 else{
		    	 	   $imFeeQuery = "select feeID,Fee, feeType ".(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1?", feeSubType":"")." from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL) ";			    	 	
		    	 	 //debug($imFeeQuery);
					}   
			 
		  if(CONFIG_FEE_CURRENCY == '1')
	   {
   			$imFeeQuery .= " and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."'";
	   }
	  
	  	$imFeeQuery .= " and isActive != 'N' ";
	   
	    /**
 			 * Introducing the commission based on the payment mode
 			 * @TicketNo 3319
 			 */
 			
 			if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == 1)
 			{
 					$paymentMode = "";
 					if(!empty($_SESSION["moneyPaid"]))
 					{
 						if($_SESSION["moneyPaid"] == "By Cash")
 							$paymentMode = "cash";
 						elseif($_SESSION["moneyPaid"] == "By Bank Transfer")
 							$paymentMode = "bank";
 						if($_SESSION["moneyPaid"] == "By Cheque")
 							$paymentMode = "cheque";
 					}
 					$imFeeQuery .= " and paymentMode = '$paymentMode' ";
 			}
	    $imFee = selectFrom($imFeeQuery); 	    

		if($imFee["Fee"] != "" && $imFee["Fee"] != 0)
		{
			if($imFee["feeType"]=="percent")
	     	{
	  			$fee = ($amount * $imFee["Fee"])/100;
	  		}
	  		else
			{
				/* @Ticket #3820 */
				if(CONFIG_OTHER_FEE_IN_PERCENT_OR_FIXED_TYPE == 1 && 
					( $imFee["feeType"]=="denominator" || $imFee["feeType"]=="agentDenom" || $imFee["feeType"]=="agentBased" ))
				{
					if($imFee["feeSubType"] == "percent")
					{
						$fee = ($amount * $imFee["Fee"])/100;
					}
					else
						$fee = $imFee["Fee"];
				}	
				else
		   	 		$fee = $imFee["Fee"];
	  		}
			 return $fee;
		}
		else
		{
			return false;
		}
	}
}


function imFeeAgentPayin($amount, $origCountry, $destCountry, $agentID,$feetransType, $currencyFrom, $currencyTo)
{
	//debug_print_backtrace();
	if($amount > 0 && $origCountry != "" && $destCountry != "")
	{
		 $fee = 0;
	    
	     // Added By Niaz Ahmad #2538
	    if(CONFIG_FEE_BASED_TRANSTYPE == "1" ){ 
	    	
	    	if($feetransType == "Pick up"){
	    	  
	    	    $imFeeQuery = "select payinFee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType='".$feetransType."' and  feeBasedOn = 'transactionType'";	
	    	}else{
            $imFeeQuery = "select payinFee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and transactionType != 'Pick up' ";	    		
	    		}
	   	} 
	    	else
	    	{
		 				$imFeeQuery = "select payinFee, feeType from " . TBL_IMFEE . " where agentNo = '$agentID' and amountRangeFrom <= '$amount' and amountRangeTo >= '$amount' and destCountry='$destCountry' and origCountry='$origCountry' and (feeBasedOn = 'Generic' or feeBasedOn = '' or feeBasedOn IS NULL) ";
	      }
		    
		
		
		  if(CONFIG_FEE_CURRENCY == '1')
	   {
   		$imFeeQuery .= " and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."'";
	   }
	   
	   $imFeeQuery .= " and isActive != 'N' ";
	   
	    /**
 			 * Introducing the commission based on the payment mode
 			 * @TicketNo 3319
 			 */
 			
 			if(CONFIG_ADD_PAYMENT_MODE_BASED_COMM == 1)
 			{
 					$paymentMode = "";
 					if(!empty($_SESSION["moneyPaid"]))
 					{
 						if($_SESSION["moneyPaid"] == "By Cash")
 							$paymentMode = "cash";
 						elseif($_SESSION["moneyPaid"] == "By Bank Transfer")
 							$paymentMode = "bank";
 						if($_SESSION["moneyPaid"] == "By Cheque")
 							$paymentMode = "cheque";
 					}
 					$imFeeQuery .= " and paymentMode = '$paymentMode' ";	
 			}
	   
	   
	   $imFee = selectFrom($imFeeQuery); 	    
		 
		
		if($imFee["payinFee"] != "" && $imFee["payinFee"] != 0)
		{
			if($imFee["feeType"]=="percent")
            	{
            		$fee = ($amount * $imFee["payinFee"])/100;
            		}
            	else{
            		$fee = $imFee["payinFee"];
            		}
			return $fee;
		}
		else
		{
			return false;
		}
	}
}


function sendMail($To,$Subject,$Message,$Name, $From,$status='')
{
	//if(validEmail($To))
	//{
	/*<input type="hidden" >*/
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $Name <$From>\r\n";
		
		if(CONFIG_CC_EMAIL == '1')
		{
			$headers .= "Bcc: ".CC_EMAIL."\r\n";
		}
		if(CONFIG_STOP_WHOLE_EMAIL_SYSTEM!="1" || $status=='Enabled')
			@mail($To,$Subject,$Message,$headers);
	//}
}
// create promotional code of any length
function createCode($length=8){
	$pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$promoCode = "";
	for ($i=0; $i < $length; $i++){
		$c = substr($pool, (rand()%(strlen($pool))), 1);
		$promoCode .= $c;
	}
	return $promoCode;
}
function checkValues($variable){
//	$variable = strip_tags($variable);
	$variable = addslashes($variable);
	$variable = trim($variable);
	return $variable;
}
// Email address validity
function validEmail($email) {
  $valid = 0;
  if(ereg("([[:alnum:]\.\-]+)(\@[[:alnum:]\.\-]+\.+)", $email)) {
    $valid = 1;
  }
  return ($valid);
}
// Random number generation
function MyRandom($min,$max) {
  srand(time());
  $random = (rand()%$max) + $min;
  return ($random);
}

// Generates 14 digit random Id based on time, IP address and pid
function genRandomId()
{
  srand((double)microtime()*1000000);
  $abcdef = rand(10000,32000);
  $ip = getenv ("REMOTE_ADDR");
  $ip = substr($ip, 0, 8);
  $ip = preg_replace("/\./","",$ip);
  srand($ip);
  $ghij = rand(1000,9999);
  $pid = getmypid();
  srand($pid);
  $kl = rand(10,99);
  $number = $abcdef.$ghij.$kl;
  return $number;
}

// return image co-ordinates after resizing to the wanted 
function Resize($x,$y,$targetx,$targety) {
  $ratio = $x/$y;
  if ($x > $y) {
    $newx = $targetx;
    $newy = $newx / $ratio;
    if ($newy > $targety) {
      $newy = $targety;
      $newx = $newy * $ratio;
    }
  }
  else {
    $newy = $targety;
    $newx = $newy * $ratio;
    if ($newx > $targetx) {
      $newx = $targetx;
      $newy = $newx / $ratio;
    }
  }
  return array($newx,$newy);
}

// Returnbs name of the month
function Num2Month($month) {
if ($month=="1")  { return ("Jan"); }
if ($month=="2")  { return ("Feb"); }
if ($month=="3")  { return ("Mar"); }
if ($month=="4")  { return ("Apr"); }
if ($month=="5")  { return ("May"); }
if ($month=="6")  { return ("Jun"); }
if ($month=="7")  { return ("Jul"); }
if ($month=="8")  { return ("Aug"); }
if ($month=="9")  { return ("Sep"); }
if ($month=="10") { return ("Oct"); }
if ($month=="11") { return ("Nov"); }
if ($month=="12") { return ("Dec"); }

}
// return false on failure and array of records on success
function SelectMultiRecords($Querry_Sql)
{   
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
	
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
	    $count = 0;
		$data = array();
		while ( $row = mysql_fetch_array($result)) 
		{
			$data[$count] = $row;
			$count++;
		}
			return $data;
	}
}

function selectFrom($Querry_Sql)
{ //echo DEBUG . $Querry_Sql;
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}



function checkExistence($fieldName,$fieldvalue,$table,$returnValue,$arrMultiID='')
{
	//debug_print_backtrace();
	$name = explode(",", $fieldName);
	$value = explode (",", $fieldvalue);
		
	$j = count($name);
	$query = "";
	
	for($i=0; $i< $j; $i++){
		
		if($i > 0)
		{
			$query .= " AND ";
			}
		$query .= $name[$i] ." = '".  $value[$i]."'";
		
		
		}
	$var = selectFrom("select $returnValue, $fieldName from $table where $query");
 // Added by Niaz Ahmad for check multiple ID  @4961
 	if(CONFIG_SEARCH_EXISTING_DATA_CUSTOMIZE == "1"){
		if(!empty($var["$returnValue"])){
		 $custID = $var["$returnValue"];
		  $var["$returnValue"] = "";
		 	if(!empty($arrMultiID)){
						foreach($arrMultiID as $idTypeValues )
						{
						  $strCustDuplicateIDNumberSql = selectFrom("
								   SELECT 
								   		user_id 
								    FROM 
										user_id_types 
									WHERE  
										id_number = '".$idTypeValues["id_number"]."' AND  
										user_id   = '".$custID."'
										");
			
			     if(!empty($strCustDuplicateIDNumberSql["user_id"]))
			       {    
						$customerID = $strCustDuplicateIDNumberSql["user_id"];
						$strCustSql = selectFrom("select accountName from $table where customerID = '".$customerID."'");
						$var["$returnValue"] = $strCustSql["accountName"];
						
						
			     }
			 }  
		  }
		}
	}
	
	return $var["$returnValue"];
	
	}

function countRecords($Query)
{

      // monitor_sql($Query);
      
   	if((@$result = mysql_query ($Query))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Query);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
		
      		return  $check[0];
   	
		}
			return false;	
	}
/*
	$query="SELECT count(id) as cnt FROM " . TBL_MESSAGES . " where dest='$username'";
	$contents=selectFrom($query);*/
}

function isExist($Querry_Sql)
{//	echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		if ($check=mysql_fetch_array($result))
   		{
      		return $check;
   		}
			return false;	
	}
}

function insertInto($Querry_Sql)
{	//echo $Querry_Sql;
   

   if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}
		//******** Function modification by Usman Ghani *********/
		// Add the code to return false in case of query error.
		return false; 
		// End of modification code.
	}   
	else
 	{	
		return true;	
	}
}
function update($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
		echo mysql_message($Querry_Sql);		
		}
		//******** Function modification by Usman Ghani *********/
		// Add the code to return false in case of query error.
		return false; 
		// End of modification code.
	}   
	else
 	{
		return true;
   	}
}
function deleteFrom($Querry_Sql)
{
        
	if((@$result = mysql_query ($Querry_Sql))==FALSE)
   	{
		if (DEBUG=="True")
		{
			echo mysql_message($Querry_Sql);		
		}	
	}   
	else
 	{	
		return true;	
	}
}

function mysql_message($Querry_Sql)
{
	$msg =  "<div align='left'><strong><font style='background-color: #FF0000' color='white'>$company MySql Debugger:</font></strong><br>";
	$msg .= "Error in your Query: $Querry_Sql<BR>";
	$msg .= "<strong><font color='red'>m y s q l &nbsp;g e n e r a t e d &nbsp;e r r o r:</font></strong><BR>";
	$msg .= mysql_errno() . " " . mysql_error() . "</div><HR>";
	echo $msg;
}

function fetchData($query,$selected = '')
{
	if((@$result = mysql_query ($query))==FALSE)
   	{
		return false;
	}   
	else
	{
		while ($row = mysql_fetch_array($result))
		{
			$returnOpt .= "<option value=$row[0] ".($selected==$row[0]?'selected':'').">$row[1]</option>\n\t";
		}
	}
	return $returnOpt;
}

function FillDateCombo($start, $end, $flag=0)
{
	if ($flag == 0){
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>$i</option>\n\t";
	} else {
		for ($i = $start; $i <= $end; $i++)
			echo "<option value=$i>". Num2Month($i)."</option>\n\t";
	}
}

function insertError($error) //insert error into session
{
	$backtrace = debug_backtrace();
	$backtrace_first = $backtrace[0];
	//echo $backtrace[0]["file"];
	session_register("error");
	$_SESSION['error']  = $error;
	$controlFilesFlag = false; 
	if(strstr($backtrace_first["file"],"admin/add-agent-conf.php") && $_POST["userID"]=="")
		$controlFilesFlag = true;
	if(CONFIG_MVC_LAYOUT=="1" && $controlFilesFlag){
		$_SESSION['error'] = $error;
		echo strip_tags($error); 
		//exit;
	}
}

function getIP() 
{
	if ($_SERVER) 
	{
		if ( $_SERVER["HTTP_X_FORWARDED_FOR"] ) 
		{
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} 
		elseif ( $_SERVER["HTTP_CLIENT_IP"] ) 
		{
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} 
		else 
		{
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} 
	else 
	{
 	if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) 
	{
		$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
	}
	elseif ( getenv( 'HTTP_CLIENT_IP' ) ) 
	{
		$realip = getenv( 'HTTP_CLIENT_IP' );
	} 
	else 
	{
		$realip = getenv( 'REMOTE_ADDR' );
	}
}
return $realip; 
}

function activities($login,$activity,$actionFor,$tableName,$descript)
{
		$activityQuery = "insert into ".TBL_LOGIN_ACTIVITIES." (login_history_id, activity, activity_time, action_for, table_name,description) values('$login','$activity','".getCountryTime(CONFIG_COUNTRY_CODE)."','$actionFor','$tableName','$descript')";
		insertInto($activityQuery);
		
}

function createSession($username, $is_admin='N')
{
    $ip=getIP();
    $agentType = getAgentType();
		$mySession="";
		$mySession = md5(uniqid(rand()) . genRandomId());  // To Creat a unique SessionID
		

        $now=date('Y-m-d H:i:s');        // Format of Session Start Date
		//echo "select username from ". TBL_SESSIONS . " where username='$username'"
		if (isExist("select username from ". TBL_SESSIONS . " where username='$username'"))
		{
			deleteFrom("delete from " . TBL_SESSIONS . " where username='$username'");
		}
		insertInto("INSERT INTO " . TBL_SESSIONS . "(session_id, username, session_time, ip) VALUES('$mySession','$username','$now', '$ip')");	

		//if ($is_admin == 'Y')
			$sessionTime = time();
			$_SESSION['sessionTime'] = $sessionTime;
			$_SESSION['is_admin'] = $is_admin;
		//}
		//if ($_SESSION["is_admin"] == "Y"){
			$loginContents = selectFrom("select * from " . TBL_ADMIN_USERS . " where username='$username'");
		
			$historyQuery ="insert into ".TBL_LOGIN_HISTORY." (login_time, login_name, access_ip, login_type, user_id) values
		( '".getCountryTime(CONFIG_COUNTRY_CODE)."', '$username', '$ip', '$agentType', '".$loginContents["userID"]."')"; 	
		insertInto($historyQuery);
		
		$historyID = @mysql_insert_id();	
		
			session_register("loginHistoryID");
			$_SESSION["loginHistoryID"] = $historyID;
			session_register("loggedUserData");
			$_SESSION["loggedUserData"] = $loginContents;
			session_register("parentID");
			$_SESSION["parentID"] = $loginContents["userID"];
			session_register("agent_country_name");
			$_SESSION["agent_country_name"] = $loginContents["agentCountry"];
			session_register("changedPwd");
			$_SESSION["changedPwd"] = $loginContents["changedPwd"];
			session_register("isMain");
			$_SESSION["isMain"] = $loginContents["isMain"];
			session_register("rights");
			$_SESSION["rights"] = $loginContents["rights"];
			session_register("adminType");
			$_SESSION["adminType"] = $loginContents["adminType"];
			session_register("agentType");
			$_SESSION["agentType"] = $loginContents["agentType"];
		//}
		$_SESSION["sid"]= $mySession;
		
		
		
		
		//echo $_SESSION["sid"];
		//exit;
}
/////
function createTellerSession($username, $is_admin='N')
{
    $ip=getIP();
    $agentType = getAgentType();
		$mySession="";
		$mySession = md5(uniqid(rand()) . genRandomId());  // To Creat a unique SessionID
		

        $now=date('Y-m-d H:i:s');        // Format of Session Start Date
		//echo "select username from ". TBL_SESSIONS . " where username='$username'"
		if (isExist("select username from ". TBL_SESSIONS . " where username='$username'"))
		{
			deleteFrom("delete from " . TBL_SESSIONS . " where username='$username'");
		}
		insertInto("INSERT INTO " . TBL_SESSIONS . "(session_id, username, session_time, ip) VALUES('$mySession','$username','$now', '$ip')");	

		//if ($is_admin == 'Y')
			$sessionTime = time();
			$_SESSION['sessionTime'] = $sessionTime;
			$_SESSION['is_admin'] = $is_admin;
		//}
		//if ($_SESSION["is_admin"] == "Y"){
			$loginContents = selectFrom("select * from " . TBL_COLLECTION . " as c, ". TBL_TELLER. " as t  where t.loginName='$username' and t.collection_point = c.cp_id");
			
			$historyQuery ="insert into ".TBL_LOGIN_HISTORY." (login_time, login_name, access_ip, login_type, user_id) values
		( '".getCountryTime(CONFIG_COUNTRY_CODE)."', '$username', '$ip', '$agentType', '".$loginContents["tellerID"]."')"; 	
		insertInto($historyQuery);
			
			
			$historyID = @mysql_insert_id();	
		
			session_register("loginHistoryID");
			$_SESSION["loginHistoryID"] = $historyID;
			
			session_register("loggedUserData");
			$_SESSION["loggedUserData"] = $loginContents;
			session_register("parentID");
			$_SESSION["parentID"] = $loginContents["cp_ida_id"];
			session_register("agent_country_name");
			$_SESSION["agent_country_name"] = $loginContents["cp_country"];
			session_register("isMain");
			$_SESSION["isMain"] = $loginContents["isMain"];
			session_register("agentType");
			$_SESSION["agentType"] = "TELLER";
			session_register("rights");
			$_SESSION["rights"] = $loginContents["rights"];
		//}
		$_SESSION["sid"]= $mySession;
		
		
		//echo $_SESSION["sid"];
		//exit;
}
/////



function createUserSession($username, $is_admin='N')
{
    $ip=getIP();
    $agentType = getAgentType();
		$mySession="";
		$mySession = md5(uniqid(rand()) . genRandomId());  // To Creat a unique SessionID
		

        $now=date('Y-m-d H:i:s');        // Format of Session Start Date
		//echo "select username from ". TBL_SESSIONS . " where username='$username'"
		if (isExist("select username from ". TBL_SESSIONS . " where username='$username'"))
		{
			deleteFrom("delete from " . TBL_SESSIONS . " where username='$username'");
		}
		insertInto("INSERT INTO " . TBL_SESSIONS . "(session_id, username, session_time, ip) VALUES('$mySession','$username','$now', '$ip')");	

		//if ($is_admin == 'Y')
			$sessionTime = time();
			$_SESSION['sessionTime'] = $sessionTime;
			$_SESSION['is_admin'] = $is_admin;
		//}
		//if ($_SESSION["is_admin"] == "Y"){
			$loginContents = selectFrom("select * from " . TBL_USER . "   where userName='$username' ");
			
			$historyQuery ="insert into ".TBL_LOGIN_HISTORY." (login_time, login_name, access_ip, login_type, user_id) values
		( '".getCountryTime(CONFIG_COUNTRY_CODE)."', '$username', '$ip', '$agentType', '".$loginContents["UserID"]."')"; 	
		insertInto($historyQuery);
			
			
			$historyID = @mysql_insert_id();	
		
			session_register("loginHistoryID");
			$_SESSION["loginHistoryID"] = $historyID;
			
			session_register("loggedUserData");
			$_SESSION["loggedUserData"] = $loginContents;
			session_register("parentID");
			$_SESSION["parentID"] = $loginContents["userID"];
			session_register("agent_country_name");
			//$_SESSION["agent_country_name"] = $loginContents["cp_country"];
			session_register("isMain");
			$_SESSION["isMain"] = $loginContents["isMain"];
			session_register("agentType");
			$_SESSION["agentType"] = "PAYING BOOK CUSTOMER";
			session_register("rights");
			//$_SESSION["rights"] = $loginContents["rights"];
		//}
		$_SESSION["sid"]= $mySession;
		
		
		//echo $_SESSION["sid"];
		//exit;
}

















function loggedUser() // returns the valid logged username
{
	global $chPassDate;
	$sessionID=$_SESSION["sid"];
	$now=date('Y-m-d H:i:s');
	//echo "SID: " . $sessionID;
	//echo "SELECT username from ". TBL_SESSIONS ." where session_id='$sid'";
	$sql4session="SELECT username from ". TBL_SESSIONS ." where session_id='$sessionID'";
    if (!isExist($sql4session) || !isset($sessionID)){
		return false;
	}
	update("update " . TBL_SESSIONS . " set session_time='$now' where session_id='$sessionID'");	
    $content=selectFrom($sql4session);
	if ($_SESSION["is_admin"] == "Y"){
		$loginContents = selectFrom("select userID, changedPwd from ".TBL_ADMIN_USERS." where username='".$content["username"]."'");
		$chPassDate = date_diff($loginContents["changedPwd"], $now);
		session_register("parentID");
		$_SESSION["parentID"] = $loginContents["userID"];
	}else{
		$loginContents = selectFrom("select cp_ida_id from ".TBL_COLLECTION." where cp_id = '".$content["cp_id"]."'");
		//$chPassDate = date_diff($loginContents["changedPwd"], $now);
		//session_register("parentID");
		$_SESSION["parentID"] = $loginContents["cp_ida_id"];
		}
	// check that a valid session exists, and has not timed out
	//echo "sad: " . $_SESSION["sessionTime"];
	//exit;
	if (($_SESSION["sessionTime"] == '') or ((time()-$_SESSION["sessionTime"]) > 18000)) {
		//session_unset();
		//header("Location: index.php?PHPSESSID=$PHPSESSID");
		//exit;
	} else {
		// on success, update the session time
		$_SESSION["sessionTime"] = time();
		
	}
    return $content["username"];
}

function dateFormat($date="", $pgFlag=1)
{
	if(!empty($date))
	{
		if($pgFlag == 3){
			return date("Ymd", strtotime($date));
		}elseif($pgFlag == 4){
			return date("d-M", strtotime($date));	
		}elseif ($pgFlag != 1){
			return date("j M, Y", strtotime($date));
		}else{
			return date("d/m/Y", strtotime($date));
		}
	}
}

function extension($PicFile)
{
        $Ext = strtolower(strrchr($PicFile,"."));
		//return $ext;
        if ($Ext ==".gif" || $Ext ==".jpg" || $Ext ==".jpeg" || $Ext ==".png")
        {
            return 1;
        }
        else
        {
			return 0;
        }
}

function adminPicture($pic_name, $size="s")
{
	$image="../thumbs/bucket" . substr($pic_name, 12,2) . "/s_" . $pic_name;									
	return "<img src='$image' border=0>";
}

function display_error_message($message="")
{
	//global $_GET["error"];
	if (isset($_GET['error']) && $_GET['error'] == 'Y' && $message=="")
	{
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br><?    } 
	else
	{ if ($message !=""){
?><br>
<table width="100%" height="50" border="1" bordercolor="#990000" cellpadding="10" cellspacing="0">
<tr>
	<td><font color="#FF0000"><b><? echo $_SESSION['error']; ?></b></font></td>
</tr>
</table><br>
<?
	}}
}

function email_check($email)
{
	if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		return false;
	}
	else 
	{
		return true;
	}
}

function redirect($url)
{
	
	global $PHPSESSID;
	if(strstr($url, '?'))
	{
		
		header("location: $url&PHPSESSID=$PHPSESSID");
			
	}
	else
	{
		header("location: $url?PHPSESSID=$PHPSESSID");
	}
	exit();
}

function cuttext($tring, $cuton) 
{
	$space=" ";
	if (!strstr($tring,$space)) 
	{
		/* No space is found in the whole string: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	}
	
	if (substr($tring,$cuton,1)==$space) 
	{
		/* 21st character is a space: cut at 20th character */
		$tring=substr($tring,0,$cuton);
	} 
	else 
	{
		/* 21st Character is NOT a space: return to last space and cut there */
		while ($teller <= $cuton) 
		{
			if (substr($tring,$cuton-$teller,1)==$space) 
			{
				$tring=substr($tring,0,$cuton-$teller);
				break;
			}
		$teller++;
		}
	}
	return $tring;
}
/*
function date_diff($str_start, $str_end) 
{ 
	if($str_start!="")
		$str_start = strtotime($str_start); // The start date becomes a timestamp 
	if($str_end!="")
		$str_end = strtotime($str_end); // The end date becomes a timestamp 
	
	$nseconds = $str_end - $str_start; // Number of seconds between the two dates 
	$ndays = round($nseconds / 86400); // One day has 86400 seconds 
	return $ndays;
} 
*/
function upload_picture($picture, $width, $height, $targetDir="../uploads/"){
//	global $picture;
	list($usec, $sec) = explode(" ",microtime());
	$createdID = eregi_replace(".","_", $sec + $usec);
	echo $picture_name;
	if (extension($picture_name)==1){
		$ext = strrchr($picture_name,".");
		if (strtolower($ext) == ".gif"){
			$ext = ".png"; 
		}
		if (@is_uploaded_file($picture)){ 
			// to further hide image name we add random number
			$picID = $createdID;
			$catPic = $targetDir. "c_".$picID . $ext;
			$catImage = "c_".$picID . $ext;

			$original = $targetDir. "o_c_" . $picID . $ext;
			@move_uploaded_file($picture, $original);
			@chmod ($original, 0755);   
			$size = getimagesize($original);
			if ($size[2]==1) { // if GIF, convert to png and then load 
	        	exec ("/usr/bin/gif2png $original");
       			$pngoriginal = $targetDir. "o_". $picID. ".png";
       			$im_in = imagecreatefrompng($pngoriginal); 
			}
			if ($size[2]==2) // if JPEG 
				$im_in = @imagecreatefromjpeg($original);
			if ($size[2]==3) // if PNG 
				$im_in = @imagecreatefrompng($original); 

			@imagegammacorrect($im_in, 1.0, 1.4);
			// Category Pictue
			list ($catx,$caty) = Resize($size[0],$size[1],$width,$height);
			
			$catImage = imagecreatetruecolor($catx,$caty);
			@imagecopyresized($catImage,$im_in,0,0,0,0,$catx,$caty,$size[0],$size[1]);
			if ($size[2]==1) { // if GIF it is not png
				@imagepng($catImage,$catPic);
			}
			if ($size[2]==2) { // if JPEG 
				@imagejpeg($catImage,$catPic);
			}
			if ($size[2]==3) { // if PNG 
				@imagepng($catImage,$catPic);
			}
		} else return ERR9;
	} else return ERR4;
	return $catImage;
}

function displayFlag($country, $flgPath = "")
{
	global $flags;
	if($country != "" && $flags[strtoupper($country)]!= "")
	{
		return  "<img src='" . $flgPath . "images/flags/". $flags[strtoupper($country)].".gif' alt='$country' border=0 align='absmiddle'>";
	}
}

// If you pass month and year to this function then it returns total days of that month.
function getTotalDays($month,$yr)
{
	switch($month)
	{
		case 01:
		case 1:
			return 31;
			break;
		case 02:
		case 2:
			if(($yr%4) == 0)
			{
				return 29;	
			}
			else
			{
				return 28;
			}
			break;
		case 03:
		case 3:
			return 31;
			break;
		case 04:
		case 4:
			return 30;
			break;
		case 05:
		case 5:
			return 31;
			break;
		case 06:
		case 6:
			return 30;
			break;
		case 07:
		case 7:
			return 31;
			break;
		case 08:
		case 8:
			return 31;
			break;
		case 09:
		case 9:
			return 30;
			break;
		case 10:
			return 31;
			break;
		case 11:
			return 30;
			break;
		case 12:
			return 31;
			break;
		default:
			return 31;
			break;	
	}	
}
	
//This function is used when the country is other than uk.	
function getCountryTime($countryCode)
{
	
	$row = selectFrom("select timeDiff from countryTime where countryCode = '$countryCode'");
	
	$timeDiff = $row[0];
	$temp = explode(":",$timeDiff);
	$year = date("Y");
	$mon = date("m");
	$day = date("d");
	$hour = date("H");
	$min = date("i");
	$sec = date("s");

		
	if ($temp[0].$temp[1] < 0)
	{
		$min = ($min - $temp[1]);
		if ($min < 0)
		{
			$min = 	(60 + ($min));
			$hour = ($hour - 1);
			if($hour < 0)
			{
				$hour = (24 + ($hour));
				$day = ($day - 1);
				if ($day == 0)
				{
					$mon = ($mon - 1);
					$day = (getTotalDays($mon,$year) + $day);
					if ($mon == 0)
					{
						$mon = (12 + $mon);	
						$year = ($year - 1);
					}
				}
			}
		}
		
		$hour = ($hour + ($temp[0]));
		if ($hour < 0)
		{
			$hour = (24 + ($hour));
			$day = ($day - 1);
			if ($day == 0)
			{
				$mon = ($mon - 1);
				$day = (getTotalDays($mon,$year) + $day);
				if ($mon == 0)
				{
					$mon = (12 + $mon);	
					$year = ($year - 1);
				}
			}
		}
	}
	
	else
	{
		$min = ($min + $temp[1]);
		if ($min >= 60)
		{
			$min = $min - 60;
			$hour = ($hour + 1);	
			if ($hour >= 24)
			{
				$hour = $hour - 24;
				$day = ($day + 1);
				if ($day > getTotalDays($mon,$year))
				{
					$day = "01";
					$mon = ($mon + 1);
					if ($mon > 12)
					{
						$mon = $mon - 12;
						$year = $year + 1;	
					}
				}
			}
		}
		
		$hour = ($hour + $temp[0]);
		if ($hour >= 24)
		{
			$hour = $hour - 24;
			$day = ($day + 1);
			if ($day > getTotalDays($mon,$year))
			{
				$day = "01";
				$mon = ($mon + 1);
				if ($mon > 12)
				{
					$mon = $mon - 12;
					$year = $year + 1;	
				}
			}
		}
	}

	if (strlen($mon) == 1)
	{
		$mon = "0" . $mon;	
	}
	if (strlen($day) == 1)
	{
		$day = "0" . $day;	
	}
	if (strlen($hour) == 1)
	{
		$hour = "0" . $hour;	
	}
	if (strlen($min) == 1)
	{
		$min = "0" . $min;	
	}
	
	$dateTime = $year . "-" . $mon . "-" . $day . " " . $hour . ":" . $min . ":" . $sec;
	
	return $dateTime;

}

//////By Jamshed
//// This function will let return either cpf(of user input) is valid or not...

function validCPF($var)
{
	$sum = 0;
	for ($i = 0; $i < 9; $i++)
	{
		$myarr[$i] = $var{$i} * ($i + 1);
		$sum = $sum + $myarr[$i];
	}
	$div = $sum / 11;
	$arr = explode(".",$div);
	if ($arr[1] != "")
	{
		$first_cdigit = substr($arr[1],0,1);
	}
	else
	{
		$first_cdigit = 0;
	}
	if ($first_cdigit != 9 && $arr[1] != "")
	{
		$first_cdigit++;
	}
	$next = substr($var,0,9) . $first_cdigit;
	$sum2 = 0;
	for ($j = 0; $j < 10; $j++)
	{
		$myarr2[$j] = $next{$j} * $j;
		$sum2 = $sum2 + $myarr2[$j];
	}
	$div2 = $sum2 / 11;
	$arr2 = explode(".",$div2);
	if ($arr2[1] != "")
	{
		$second_cdigit = substr($arr2[1],0,1);
	}
	else
	{
		$second_cdigit = 0;
	}
	if ($second_cdigit != 9 && $arr2[1] != "")
	{
		$second_cdigit++;
	}
	$orgcpf = substr($next,0,10) . $second_cdigit;
	if ($orgcpf == $var)
	{
		return("valid");
	}
	

return("not valid");
}

function CalculateFromTotal($imFee1, $totalAmount1, $customerCountry, $benificiaryCountry, $dist, $senderAgentID, $payinBook, $feeTransType, $currencyFrom, $currencyTo)
{
	
	//debug_print_backtrace();
	
	///Calculating Fee/////////
	$agentType = getAgentType();
		$imFee = 0;
		$totalAmount = $totalAmount1;
	
		if($imFee1 > 0 || (CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0))
		{
			$imFee = $imFee1;
			$amount = $totalAmount1 - $imFee;
			
			}else{
				
				$amount2= 0;///New calculated TransAmount
				$amount = $totalAmount;///TransAmount initially assumed as total Amount when Fee is Zero
				$fee2 = 0;////Fee is set to be Zero initially
				$count = 0;///To Avoid inifinit loop in situation where wrong amount entry and passing all conditions
					//while($amount != $amount2 && $count < 6)
					{////While Loop is commented to see the Total amount Based Commission
						//// Max. 6 Iterations 
						/// uptill last iteration amount is not equal 
						/// to current iteration amount
					
						
						$flag = false;///To check if Fee is not calculated in a particular iteration
						if(CONFIG_PAYIN_CUSTOMER != '1')
						{
							
							if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
							{
								$flag = true;
								
								$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
							
							}
							if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
							{
								$flag = true;
						
								$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
								
							}
							if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
							{
								$flag = true;
						
							 $imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);	
							 
							}
							if(!$flag || $imFee <= 0)
							{
								$imFee = imFee($amount,  $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);	
							
							}
								
						}else
						{	
							if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
							{
								$flag = true;
								if($payinBook)
									$imFee = imFeeAgentPayin($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
								else
									$imFee = imFeeAgent($amount, $customerCountry, $benificiaryCountry, $_SESSION["collectionPointID"], $feeTransType, $currencyFrom, $currencyTo);	
							
							}
							if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
							{ 
								$flag = true;
								if($payinBook)
									$imFee = imFeeAgentPayin($amount,  $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
								else
							 		$imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $dist, $feeTransType, $currencyFrom, $currencyTo);	
							
							}
							if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
							{
								$flag = true;
							
								if ($payinBook) {
									$imFee = imFeeAgentPayin($amount, $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);
									
								} else {
							 		$imFee = imFeeAgent($amount,  $customerCountry, $benificiaryCountry, $senderAgentID, $feeTransType, $currencyFrom, $currencyTo);
							 		
							 	}
							 		
						 	}
							if(!$flag || $imFee <= 0)
							{
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeePayin($amount, $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);	
										
									}else
									{
								 		$imFee = imFee($amount, $customerCountry, $benificiaryCountry, $feeTransType, $currencyFrom, $currencyTo);
								 		
								 	}
						 	}
				  	}
				  	
				  	////If Both Fees calculated in consecutive iterations are not same
				  	if($imFee != $fee2)
				  	{
				  		$fee2 = $imFee;
				  	}
				  	$amount2 = $amount;
				  	$amount = $totalAmount - $imFee;
				  	$count++;
				  	
				}
				
			  }
		
		
		$feeData[] = $amount;
		$feeData[] = $imFee;
		return $feeData;
			  
			  	///////End Fee
	
	}
	
	class TransLimit
	{
		var $limitID;
		var $limitUser;
		var $limitType;
		var $limitValue;
		var $limitDuration;
		var $limitUpdated;
		var $apply_date;
		var $currency;
		 
		
		function insertLimit($insertData)
		{
			$this->limitUser     = $insertData[0];
			$this->limitType     = $insertData[1];
			$this->limitValue    = $insertData[2];
			$this->limitDuration = $insertData[3];  
			$this->limitUpdated  = $insertData[4];
			$this->apply_date = $insertData[5];  
			$this->currency  = $insertData[6];
			if($insertData[6] == "")
			{
				$this->apply_date = getCountryTime(CONFIG_COUNTRY_CODE);
			}

			
			 $insertQuery = "insert into ".TBL_TRANSACTION_LIMIT." (user_id, limit_type, duration, limitValue, updation_date, apply_date, currency) 
			values('".$insertData[0]."','".$insertData[1]."','".$insertData[3]."','".$insertData[2]."','".$insertData[4]."','".$insertData[5]."','".$insertData[6]."')";
			$inserted = insertInto($insertQuery);		
			return $inserted;
		}
			
		function calculateLimit($limitCheckData)
		{
			$acceeded = true;
			
			$this->limitUser = $limitCheckData[0];
			$limitTransAmount = $limitCheckData[1];
			$currencyFrom = $limitCheckData[2];
			
			$selectLimitData = selectFrom("select limitID, user_id, limit_type, duration, limitValue, updation_date, apply_date from ".TBL_TRANSACTION_LIMIT." where user_id = '".$this->limitUser."' and currency = '".$currencyFrom."' and updation_date =(select  Max(updation_date) from ".TBL_TRANSACTION_LIMIT." where user_id = '".$this->limitUser."' and currency = '".$currencyFrom."')");	
			
			
			 $selectLimitData["user_id"];
			 $selectLimitData["limit_type"];
			 $selectLimitData["limitValue"];
			 $selectLimitData["duration"]; 
			 $selectLimitData["updation_date"];
			
			if($selectLimitData["limit_type"] == "Transaction" && $limitTransAmount > $selectLimitData["limitValue"])
			{
				//echo("First");
				$acceeded = false;
				return $acceeded;
				
			}elseif($selectLimitData["limit_type"] == "Days"){
				//echo("Second");
				$syear = substr($selectLimitData["apply_date"],0,4);
				$smonth = substr($selectLimitData["apply_date"],5,2);
				$sday = substr($selectLimitData["apply_date"],8,2);
				
				$end = getCountryTime(CONFIG_COUNTRY_CODE);
				$eyear = substr($end,0,4);
				$emonth = substr($end,5,2);
				$eday = substr($end,8,2);
				
				$diff = $eday - $sday;
				$diff += ($emonth - $smonth)*30;
				//echo("Period");
				$period = ($diff % $selectLimitData["duration"]);
				
				$compYear = 0;
				$compMont = 0;
				$compDay = 0;
				
				while($period > 365)
				{
					$period -= 365;
					$compYear++;
				}
				while($period > 30)
				{
					$period -= 30;
					$compMont++;
				}
				$compDay = $period;
				
				$compYear = $eyear - $compYear;
				$compMont = $emonth - $compMont;
				$compDay = $eday - $compDay;
				if($compDay < 10)
					$compDay = "0".$compDay;
				
				if($compMont < 10)
					$compMont = "0".$compMont;
				
				if($compYear < 10)
					$compYear = "0".$compYear;
				
				
				//echo("Start Date");
				 $startDate = ($compYear)."-".($compMont)."-".($compDay)." 00:00:00";
				//echo("End Date");
				 $endDate =  $eyear."-".$emonth."-".$eday." 23:59:59";
												
				//echo $start = $this-> $limitUpdated;
				//echo $difference = floor($difference / (24 * 60 * 60)) + 1;
				
				$copmareUser = selectFrom("select username from ".TBL_ADMIN_USERS." where userID = '".$selectLimitData["user_id"]."'");
				
				$compareTransQuery = "select totalAmount from ".TBL_TRANSACTIONS." where addedBy = '".$copmareUser["username"]."' and currencyFrom = '".$currencyFrom."' and transDate between '".$startDate."' and '".$endDate."'";
				
				$selectQueries = SelectMultiRecords($compareTransQuery);
				$amountTransactions = 0;
				for($i=0; $i < count($selectQueries); $i++)
				{
					$amountTransactions += $selectQueries[$i]["totalAmount"]; 	
				}			
				//echo("Total Amount");
					$amountTransactions += $limitTransAmount;	
				
				if($amountTransactions > $selectLimitData["limitValue"])
				{
					$acceeded = false;
					return $acceeded;
				}
				
								
			}
			
				
				
			return $acceeded;	
		}
		
		function selectLimit($limitUserID)
		{
			
			$selectLimitData = selectFrom("select limitID, user_id, limit_type, duration, limitValue, updation_date, apply_date, currency from ".TBL_TRANSACTION_LIMIT." where user_id = '".$limitUserID."' and updation_date =(select  Max(updation_date) from ".TBL_TRANSACTION_LIMIT." where user_id = '".$limitUserID."')");	
			
			 $limitData[] = $selectLimitData["limitID"];
			 $limitData[] = $selectLimitData["user_id"];
			 $limitData[] = $selectLimitData["limit_type"];
			 $limitData[] = $selectLimitData["limitValue"];
			 $limitData[] = $selectLimitData["duration"];
			 $limitData[] = $selectLimitData["updation_date"];
			 $limitData[] = $selectLimitData["apply_date"];
			 $limitData[] = $selectLimitData["currency"];
				
			return $limitData;
		}
		
			
	}

	
	
function authorizeTrans($transId, $username)
{

	$isAgentAnD = 'N';
	$isBankAnD = 'N';

	
	update("update " . TBL_TRANSACTIONS . " set transStatus= 'Authorize', authorisedBy = '$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='$transId'");
	
	$descript = "Transaction Authorized successfully.";
	activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
	
	$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID, AgentComm, createdBy from " . TBL_TRANSACTIONS . " where transID = '$transId'");
	$transBalance = $amount["transAmount"];		
	
	$commission = $amount["AgentComm"];
	$balance = $amount["totalAmount"];
	$agent = $amount["custAgentID"];
	$bank = $amount["benAgentID"];
	
	if(CONFIG_LEDGER_AT_CREATION != "1")
	{
			$amount = selectFrom("select totalAmount, custAgentID, benAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID, AgentComm, createdBy from " . TBL_TRANSACTIONS . " where transID = '$transId'");
			
				
			$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$agent'");
				
			
				$currentBalance = $agentContents["balance"];
				
				if($amount["createdBy"] != 'CUSTOMER')
				{
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
											$agentIDQuery = selectFrom("select custAgentID  from ". TBL_TRANSACTIONS." where transID = '".$transId."' ");
												$agentID = $agentIDQuery["custAgentID"];
												
											$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$agentID."' ");
											$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
											
												if($agentPaymentMode == 'exclude')
												{
														$balance = $amount["totalAmount"] - $commission;
												
												}elseif($agentPaymentMode == 'include')
												{
														$balance = $amount["totalAmount"];
													
												}
	
						}else{
							$balance = $amount["totalAmount"];
						}
					
						////////////////Either to insert into Agent or A&D Account////////
							if($agentContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentContents["userID"], $balance, $transId, "WITHDRAW", "Transaction Authorized", "Agent",$currencyFrom);
								updateAgentAccount($agentContents["parentID"], $balance, $transId, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currencyFrom);
							}else{
								updateAgentAccount($agentContents["userID"], $balance, $transId, "WITHDRAW", "Transaction Authorized", "Agent",$note,$currencyFrom);
							}
							
							
						
						$currentBalance = $balance - $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
						
						

					}
					elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
					 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
					 
			
					$newBalance = $payin["balance"] - $balance ; 
					$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
					update($update_Balance);
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$amount["totalAmount"]."','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					if(CONFIG_ONLINE_AGENT == '1')
					{	
						$descript = "Transaction Authorized";
						updateAgentAccount($agentContents["userID"], $amount["totalAmount"], $imReferenceNumber, "WITHDRAW", $descript, 'Agent',$note,$currencyFrom);	
					}	
				}
	
		
	}
	
		if(CONFIG_POST_PAID != '1')
		{
			$dist = $amount["benAgentID"];
			$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
			$currentBalance = $benAgentContents["balance"];
						
			
			$currentBalance =  $currentBalance - $transBalance;
			update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
				
			if($benAgentContents["agentType"] == 'Sub')
			{
				updateSubAgentAccount($benAgentContents["userID"], $transBalance, $transId, "WITHDRAW", "Transaction Authorized", "Distributor",$currencyFrom);
				updateAgentAccount($benAgentContents["parentID"], $transBalance, $transId, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currencyFrom);
			}else{
				updateAgentAccount($benAgentContents["userID"], $transBalance, $transId, "WITHDRAW", "Transaction Authorized", "Distributor",$note,$currencyFrom);
			}
			
							

		}
	
	insertError("Transaction Authorized successfully.");

	
}	
	
	
/*
$limitObject = new TransLimit;
*/
	
/*****************
*	Function:   number_to_word [by JAMSHED]
* Arguments:  int
* Returns:    string
* Description:
*     Converts a given integer (in range [0..1T-1], inclusive) into
*     alphabetical format ("one", "two", etc.).
******************/
function number_to_word($number) {
	
    if (($number < 0) || ($number > 999999999)) {
    	return "$number";
    }


    $Gn = floor($number / 1000000);  /* Millions (giga) */
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     /* Thousands (kilo) */
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      /* Hundreds (hecto) */
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       /* Tens (deca) */
    $n = $number % 10;               /* Ones */

    $res = "";

    if ($Gn) {
    	$res .= number_to_word($Gn) . " Million";
    }

    if ($kn) {
    	$res .= (empty($res) ? "" : " ") .
            number_to_word($kn) . " Thousand";
    }

    if ($Hn) {
        $res .= (empty($res) ? "" : " ") .
            number_to_word($Hn) . " Hundred";
    }

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
        "Nineteen");
        
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
        "Seventy", "Eighty", "Ninety");

    if ($Dn || $n) {
    	
    	if (!empty($res)) {
      	$res .= " and ";
      }

      if ($Dn < 2) {
      	$res .= $ones[$Dn * 10 + $n];
      } else {
	      $res .= $tens[$Dn];

        if ($n) {
        	$res .= " " . $ones[$n];
        }
      }
    }

    if (empty($res)) {
    	$res = "Zero";
    }

    return $res;

}

function generateRefNumber($intDistributorId = '', $intCollectionPointId = '') {
	//debug($intDistributorId.'='.$intCollectionPointId,true);
	//debug_print_backtrace();
	$bolCollectionPointNameMatch = false;
	if(CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1)
	{
		if(!empty($intDistributorId))
		{
			$Qry_ref = "SELECT * FROM `ref_generator` WHERE `prefixValue` = $intDistributorId order by dated desc";
			$refGen = selectFrom($Qry_ref);	
			if(!empty($intCollectionPointId))
			{
				$strCollectionPointNameSql = "select cp_corresspondent_name from cm_collection_point where cp_id = ".$intCollectionPointId;
				$arrCollectionPointName = selectFrom($strCollectionPointNameSql);
				$strCollectionPointName = $arrCollectionPointName["cp_corresspondent_name"];
				$bolCollectionPointNameMatch = strpos(strtoupper($strCollectionPointName), "SKY");
				//debug($strCollectionPointName);
			}
		}
		
		if(empty($refGen["id"]) || $refGen["id"] == "")
		{
			$Qry_ref = "SELECT * FROM `ref_generator` WHERE 1 AND prefix != 'byDistributor' AND`dated` = (SELECT MAX(dated) FROM `ref_generator` where prefix != 'byDistributor' )";
			$refGen = selectFrom($Qry_ref);	
		}
	}
	else
	{
		$Qry_ref = "SELECT * FROM `ref_generator` WHERE 1 AND`dated` = (SELECT MAX(dated) FROM `ref_generator`)";
		$refGen = selectFrom($Qry_ref);	
	
		//debug($refGen);	
	}
	
	//debug($refGen,true);
	//debug("", true);
	
	if ($refGen["id"] == "") {
		return false;	
	}
	$queryCustCntry = "select Country from ".TBL_CUSTOMER." where customerID ='" . $_POST['customerID'] . "'";
	$CustCntry = selectFrom($queryCustCntry);
	if ($refGen["origCountries"] != 'All' && $refGen["origCountries"] != '') {
		if (!strstr($refGen["origCountries"], $CustCntry["Country"])) {
			return false;
		}
	}
	if ($refGen["transType"] != "All") {
		if ($refGen["transType"] != $_POST['transType']) {
			return false;	
		}	
	}
	if ($refGen["moneyPaid"] != "All") {
		if ($refGen["moneyPaid"] != $_POST['moneyPaid']) {
			return false;	
		}	
	}
	
		
	//byAgentNumber
	if ($refGen["prefix"] == "byCompany") 
	{
		if(CONFIG_DONT_USE_HYPHEN_IN_TRANSACTION_REFERENCE_NUMBER == "1")
			$prefix = SYSTEM_PRE;
		else
			$prefix = SYSTEM_PRE."-";
	}
	else if ($refGen["prefix"] == "byAgentName") 
	{
		$Agent_name = selectFrom("select name from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'");
		
		/**
		 * If client want to have full name or number of agent at the prefix
		 * @Ticket #3833
		 */
		if(CONFIG_USE_FULL_NAME_AT_REFRENCE_GENERATOR_PREFIX == 1)
			$prefix = strtoupper($Agent_name["name"]);
		/* else it will only retain first 4 characters at prefix */
		else
			$prefix = strtoupper(substr($Agent_name["name"], 0, 4));
		
		if(CONFIG_DONT_USE_HYPHEN_IN_TRANSACTION_REFERENCE_NUMBER == "1")
			$prefix .= "";
		else
			$prefix .= "-";
		//debug(" agent post fix by name>> ".$prefix);	
	} 
	else if ($refGen["prefix"] == "byAgentNumber") 
	{
		$Agent_name = selectFrom("select username from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'");

		/**
		 * If client want to have full name or number of agent at the prefix
		 * @Ticket #3833
		 */
		if(CONFIG_USE_FULL_NAME_AT_REFRENCE_GENERATOR_PREFIX == "1")
			$prefix = strtoupper($Agent_name["username"]);
		/* else it will only retain first 4 characters at prefix */
		else
			$prefix = strtoupper(substr($Agent_name["username"], 0, 4));

		if(CONFIG_DONT_USE_HYPHEN_IN_TRANSACTION_REFERENCE_NUMBER == "1")
			$prefix .= "";
		else
			$prefix .= "-";
		//debug(" agent post fix by name>> ".$prefix);	
	} 
	/**
	 * If prefix is found on basis of distributor
	 * and for distributor config is ON
	 * and benAgentId and PrefixValue (i.e. agent value) found to be same 
	 * than distributor based refrence number will be generated
	 * @Ticket #3956
	 */
	else if ($refGen["prefix"] == "byDistributor" && CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1 && $refGen["prefixValue"] == $intDistributorId) 
	{
		//debug($refGen["prefixValue"] ."==". $intDistributorId);
		if($refGen["userPrefix"] == "byDistributorNumber")
			$strDesiredUserField = "username";
		else
			$strDesiredUserField = "name";
			
		$Agent_name = selectFrom("select $strDesiredUserField from ".TBL_ADMIN_USERS." where userID ='" . $intDistributorId . "'");
		
		
		if(!empty($strCollectionPointName))
		{
			if($bolCollectionPointNameMatch === false)
				$prefix = strtoupper(substr($Agent_name[$strDesiredUserField], 0, 2));
			else
				$prefix = "LEMOD";
			//$prefix = strtoupper(substr(str_replace(" ","",$Agent_name[$strDesiredUserField]), 0, 5));	
		}
		else
			$prefix = strtoupper(substr($Agent_name[$strDesiredUserField], 0, 2));
		
		
		if(CONFIG_DONT_USE_HYPHEN_IN_TRANSACTION_REFERENCE_NUMBER == "1")
			$prefix .= "";
		else
			$prefix .= "-";
	}
	else
	{
		$prefix = $refGen["customPrefix"];
	}
	
	if ($refGen["postFix"] == "byCompany") 
	{
		if(CONFIG_DONT_USE_HYPHEN_IN_TRANSACTION_REFERENCE_NUMBER == "1")
			$postfix = SYSTEM_PRE;
		else
			$postfix = SYSTEM_PRE."-";
	}
	else if ($refGen["postFix"] == "byAgentName") 
	{
		$Agent_name = selectFrom("select name from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'");
		$postfix = strtoupper(substr($Agent_name["name"], 0, 4));
	}
	else if ($refGen["postFix"] == "byAgentNumber") 
	{
		$Agent_name = selectFrom("select username from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'");
		/**
		 * If to show full agent number in the postfix of the Transaction refrence number
		 * @Ticket #4089
		 */
		if(CONFIG_SHOW_FULL_AGENT_NUMBER_IN_POSTFIX_REFNO == "1")
			$postfix = strtoupper($Agent_name["username"]);
		else
			$postfix = strtoupper(substr($Agent_name["username"], 0, 4));
	}
	else 
	{
		$postfix = $refGen["customPostFix"];
	}

	
	$transCountForToday = countRecords("SELECT COUNT(*) FROM ".TBL_TRANSACTIONS." WHERE transDate LIKE '".date("Y-m-d")."%'");
	$incrementalNum = ($transCountForToday + 1);
	if (strlen($incrementalNum) == 1) {
		$incrementalNum = "0" . $incrementalNum;	
	}
	
	if ($refGen["formula"] == "DDMM") {
		$formula = date("dm") . $incrementalNum;
		$imReferenceNumber = $prefix . "" . $formula ."". $postfix;
		while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) {
			$incrementalNum++;
			if (strlen($incrementalNum) == 1) {
				$incrementalNum = "0" . $incrementalNum;	
			}
			$formula = date("dm") . $incrementalNum;
			$imReferenceNumber = $prefix . "" . $formula ."". $postfix;
		}
	} else if ($refGen["formula"] == "custInitials") {
    $Query = "SELECT firstName, lastName FROM customer WHERE customerID = ".$_POST["customerID"];
    $Qresult = selectFrom($Query);
    $firstName = $Qresult["firstName"];
    $lastName = $Qresult["lastName"];
    $formula = strtoupper(substr($firstName, 0, 1)) . strtoupper(substr($lastName, 0, 1)) . $incrementalNum;
    $imReferenceNumber = $prefix . "" . $formula ."". $postfix;
		while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) {
			$incrementalNum++;
			if (strlen($incrementalNum) == 1) {
				$incrementalNum = "0" . $incrementalNum;	
			}
			$formula = strtoupper(substr($firstName, 0, 1)) . strtoupper(substr($lastName, 0, 1)) . $incrementalNum;
			$imReferenceNumber = $prefix . "" . $formula ."".$postfix;
		}
	} else if ($refGen["formula"] == "autoNum") {
		$transCount = countRecords("SELECT COUNT(refNumberIM) FROM ".TBL_TRANSACTIONS." WHERE 1");
		$formula = $transCount + 1;
		$imReferenceNumber = $prefix . "" . $formula ."".$postfix;
	} else if ( $refGen["formula"] == "agentLetterWithAutoNum" ) {
		$agentNameQuery = selectFrom("select name from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'"); 
		$firstLetter = "(".strtoupper(substr($agentNameQuery["name"], 0, 1)).")";
		$transCount = countRecords("SELECT COUNT(refNumberIM) FROM ".TBL_TRANSACTIONS." WHERE 1");
		$formula = $transCount + 1;
		$prefix = str_replace("-", $firstLetter, $prefix);
		$imReferenceNumber = $prefix . $formula . $postfix;
	} 
	else if($refGen["formula"] == "agentCityWithAutoNum")
	{
		$agentCityQuery = selectFrom("select agentCity  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'"); 		$fourLetter = strtoupper(substr($agentCityQuery["agentCity"], 0, 4));
		 $prefix = str_replace("-", $fourLetter, $prefix);
		//$transCount = countRecords("SELECT COUNT(refNumberIM) FROM ".TBL_TRANSACTIONS." WHERE 1");
		$transRef = selectFrom("select refNumberIM from ".TBL_TRANSACTIONS." where 	transDate =(select Max(transDate) from ".TBL_TRANSACTIONS.")");
		
		//$refNo = substr($transRef["refNumberIM"], 7, 13);
		$arrReference = preg_split("/[A-Z]+/", strtoupper($transRef["refNumberIM"]));
		//debug($arrReference, true);
		
		$refNo = $arrReference[1];
		
		$formula = $refNo + 1;
		$formula = str_pad($formula,6,"0",STR_PAD_LEFT);
		$imReferenceNumber = $prefix . $formula . $postfix;

		while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) 
		{
			$arrReference = preg_split("/[A-Z]+/", strtoupper($transRef["refNumberIM"]));
			$refNo = $arrReference[1];
			
			$formula = $refNo + 1;
			$formula = str_pad($formula,6,"0",STR_PAD_LEFT);
			$imReferenceNumber = $prefix . $formula . $postfix;
		}
		
	}else {
		
		$formula = $refGen["customFormula"];
		if($refGen["incementBased"] == "all")
		{
			$transCount = countRecords("SELECT COUNT(refNumberIM) FROM ".TBL_TRANSACTIONS." WHERE 1");
			$formula = $transCount + 1;
		  
		}elseif($refGen["incementBased"] == "refNumberIM"){
		// make sure refNumberIM should be numeric
				$refCount = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE 1 order by transID Desc Limit 1");
				$formula = $refCount["refNumberIM"] + 1;
				//debug($formula);
		}elseif($refGen["customFormula"] == "agent"){
			
			///Do nothing for now
		}
		$imReferenceNumber = $prefix . "" . $formula ."".$postfix;	
		$totalLength = strlen($imReferenceNumber);
		
		if($refGen["customLength"] >  0)
		{
			$strNum = "";
			for($i = 1; $i < $refGen["customLength"]; $i++)
			{	
						
						$strNum .= "0";
						//$imReferenceNumber = "0".$imReferenceNumber;
			}
			$imReferenceNumber = $prefix . "".$strNum."" . $formula."".$postfix;
		//	debug($imReferenceNumber);
		}
		
		
		while(isExist("SELECT `transID` FROM ".TBL_TRANSACTIONS." WHERE refNumberIM = '".$imReferenceNumber."'")) {
			$formula++;
			if (strlen($formula) == 1) {
				$formula = "0" . $formula;	
			}
			$imReferenceNumber = $prefix . "" . $formula ."".$postfix;
			//debug($imReferenceNumber);
			
			
			$totalLength = strlen($formula);
			//debug($refGen["customLength"] .">".  $totalLength. " || ".$formula);
			if($refGen["customLength"] >  $totalLength)
			{
				$strNum = "";
				for($i = $totalLength; $i < $refGen["customLength"]; $i++)
				{	
						$strNum .= "0";
							//$imReferenceNumber = "0".$imReferenceNumber;
				}
				$imReferenceNumber = $prefix . "".$strNum."" . $formula."".$postfix;
			//	debug($imReferenceNumber);
			}
			
			
			
		}
	}
	
	
	//debug($imReferenceNumber,true);
	return $imReferenceNumber;
}


/*****************
* This function is used to tell whether the date enter is a/c to DD-MM-YYYY
* and also other checks like:
* e.g.   30-02-2009 is invalid because FEBRUARY has 28 days
*        29-02-2008 is valid because FEBRUARY has 29 days in a leap year
*        31-06-2010 is invalid because JUNE has 30 days
*        and so on...
* [by JAMSHED]
*****************/
function isValidDate($dDate) {
	
	if (strlen($dDate) != 10) {
		return false;
	}
	
	$day  = substr($dDate,0,2);
	$mon  = substr($dDate,3,2);
	$year = substr($dDate,6,4);
	
	if (strlen($day) != 2 || strlen($mon) != 2 || strlen($year) != 4) {
		return false;
	}
	
	
		if (!is_numeric($day)) {
			return false;
			break;	
		}
		
		if (!is_numeric($mon)) {
			return false;
			break;	
		}
		
		if (!is_numeric($year)) {
			return false;
			break;	
		}
	
	
	if ($day < 1 || $day > 31 || $mon < 1 || $mon > 12) {
		return false;	
	}
	
	switch ($mon) {
		case '02':
			if (($year % 4) == 0 && (($year % 400) == 0 || ($year % 100) != 0)) {
				if ($day > 29) {
					return false;	
				}
			} else {
				if ($day > 28) {
					return false;	
				}
			}
			break;
			
		case '04':
		case '06':
		case '09':
		case '11':
			if ($day > 30) {
				return false;	
			}
			break;
	}
	
	// At last, if all above conditions are not true, then...
	return true;
}
	function updateAgentAccount($agentID, $amount, $transID, $type, $description, $actAs,$note,$currencyFrom,$today='0000-00-00')
	{
		//debug_print_backtrace(); 
		//echo "{VALUES}".$agentID."  ".$amount."  ".$transID."   ".$type."  ".$description."   ".$actAs."  ".$note."  ".$currencyFrom."  ".$today;

		$reverse = false;
		$agentQuery = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
		$isCorresspondent = $agentQuery["isCorrespondent"];
		
		if($isCorresspondent != 'N')
		{
			$agentType = "Distributor";
			
			if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
			{
				$agentType = "Both";///AnD
			}	
		}
		elseif($isCorresspondent == 'N')
		{
			$agentType = "Agent";
		}
		
	
		if($today == '0000-00-00' || $today=='')
			$today = date("Y-m-d"); 
			
		$loginID  = $_SESSION["loggedUserData"]["userID"];
		$currencyLedger=$currencyFrom;
	  
	 
	  
		if(CONFIG_TRANS_ROUND_LEVEL != "")
		{
			$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
		}
		else
		{
			$roundLevel = 4;
		}
	 
		///// Added by Niaz Ahmad #2810 connect plus at 17-01-08 Settlement Amount///////////
		
		/**************
			#5186 - AMB Exchange (CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD)
			This is first time introdueced in AMB that
			AnD not have Settlement currencies dropdowns ans logic as well.
			So we only swith OFF CONFIG_SETTLEMENT_CURRENCY_DROPDOWN
			and keep CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT and CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT ON 
			to use other logics of insertion for transaction status etc.
			by Aslam Shahid.
		**************/
		if((CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $actAs == "Agent") || (CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1' && $actAs == "Distributor") && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
		{
			if($actAs == "Agent")
			{
				$rateFor = "agent";	
			}
			else
			{
				$rateFor = "distributor";	
			}
  			
			
			/**
			 * If agent is A&D then select the settlementCurrencies field as this contains the both curreincies
			 * as agent and as distributor
			 */
			if($agentType == "Both")
			{
				$settlementCurr  = selectFrom("select settlementCurrencies from ".TBL_ADMIN_USERS." where userID ='".$agentID."' ");	
				$settlementCurrency = explode("|",$settlementCurr["settlementCurrencies"]);
				
				/* As agent */
				$strSettlemtnAsAgent = $settlementCurrency[0];
				/* As distributor */
				$strSettlemtnAsDistributor = $settlementCurrency[1];
				
				/**
				 * Using the same variable to hold the settlement currency 
				 * If A&D act as Agnet then Agent settlement curriecy will be used
				 * else as distributor currency will be used
				 */
				if($actAs == "Agent")
					$settlementCurr["settlementCurrency"] = $strSettlemtnAsAgent;
				else
					$settlementCurr["settlementCurrency"] = $strSettlemtnAsDistributor;
					
				$settlementCurrency = $settlementCurr["settlementCurrency"];
			}
			else
			{
				/* AgentType is not A&D */
				$settlementCurr  = selectFrom("select userID,settlementCurrency from ".TBL_ADMIN_USERS." where userID ='".$agentID."' ");	
				$settlementCurrency=$settlementCurr["settlementCurrency"];
			}
  			
			if($settlementCurr["settlementCurrency"]!='')
			{
				$exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='".$rateFor."' and rateValue=".$agentID."  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='agent' and rateValue=".$agentID."  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."') "); 
			
			
				if($exchangeRateAgent["erID"] == '')
				{
					$exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='generic'   and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='generic'  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."') "); 
			
				}
			
				if($exchangeRateAgent["erID"] == '')
				{
					$exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='".$rateFor."' and rateValue=".$agentID."  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='agent' and rateValue=".$agentID."  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."') "); 
				$reverse = true;
				}

				if($exchangeRateAgent["erID"] == '')
				{
					$exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='generic'   and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='generic'  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."') "); 
					$reverse = true;
				}	
			
				if ($exchangeRateAgent["erID"]!='')
				{
					// To get net exchange rate
					if($exchangeRateAgent["marginType"] == 'fixed')
					{
						$rateAgent = $exchangeRateAgent["primaryExchange"] - $exchangeRateAgent["marginPercentage"];
					}
					else
					{
						$rateAgent = $exchangeRateAgent["primaryExchange"] - ($exchangeRateAgent["primaryExchange"] * $exchangeRateAgent["marginPercentage"]) /  100;
					}
			
					if($reverse)
					{
						$rateAgent = 1/$rateAgent;
					}
					$rateAgent = round($rateAgent,4);
			
					
					if($rateFor == "agent")
					{
						if($description == "Transaction Created" || $description == "Transaction Amended")
						{
							$selectAgentData = selectfrom("select paymentMode from admin where userID = '".$agentID."'");
							$selectTransData = selectfrom("select AgentComm,IMFee,currencyFrom,fromCountry,toCountry,transDate,transType,moneyPaid from transactions where transID = '".$transID."'");
				
							if($settlementCurr["settlementCurrency"] != $selectTransData["currencyFrom"])
							{
								$exchangeRateCurrencyFrom = getMultipleExchangeRates($selectTransData["fromCountry"], $selectTransData["toCountry"], $settlementCurr["settlementCurrency"], $dist, 0, 0, $selectTransData["currencyFrom"],$selectTransData["transType"],$selectTransData["moneyPaid"], $agentID);
							
								$exErID = $exchangeRateCurrencyFrom[0];
								$exRateValue = $exchangeRateCurrencyFrom[1];
								$AgentCurrencyFrom = $exchangeRateCurrencyFrom[2];
								$AgentCurrencyTo 	= $exchangeRateCurrencyFrom[3];
							}
							else
							{
								$exErID = 0;
								$exRateValue = 1;
								$AgentCurrencyFrom = $selectTransData["currencyFrom"];
								$AgentCurrencyTo 	= $settlementCurr["settlementCurrency"];
							}
						}
					}		

					if($settlementCurr["settlementCurrency"] == $currencyFrom )
					{
						$settlementAmount = $amount;
						if($rateFor == "agent")
						{
							if($description == "Transaction Created" || $description == "Transaction Amended")
							{
								if($selectAgentData["paymentMode"] == "exclude")
								{
									$settlementAmount = $settlementAmount + ($selectTransData["IMFee"] - $selectTransData["AgentComm"]);
								}
								else
								{
									$settlementAmount += $selectTransData["IMFee"];
								}
							}
						}
						$currencyLedger = $currencyFrom;
					}
					else
					{
						$settlementAmount=$amount/$rateAgent;
			
						if($rateFor == "agent")
						{
							if($description == "Transaction Created" || $description == "Transaction Amended")
							{
								if($selectAgentData["paymentMode"] == "exclude")
								{
									$calcomm = $selectTransData["IMFee"] - $selectTransData["AgentComm"];
									$AgentCommissionSettlement = $calcomm * $exRateValue;        
								}
								else
								{
									$AgentCommissionSettlement =$selectTransData["IMFee"]* $exRateValue;        
								}
								$settlementAmount += $AgentCommissionSettlement;
							}
						}
						$amount=$settlementAmount;
						$currencyLedger = $settlementCurrency;
					}
				}
			
				$date_time = date('Y-m-d h:i:s'); 
				$settleQuery = "INSERT INTO ". TBL_SETTLEMENT_AMOUNT." (settlementCurrency,currencyFrom,settleAmount,agentID,transID,dated) VALUES ('".$settlementCurr["settlementCurrency"]."','".$currencyFrom."','".$settlementAmount."','".$agentID."','".$transID."','".$date_time."' )";
				
				insertInto($settleQuery);
				$settleID = @mysql_insert_id();
			}
		}
	
		$transData = selectFrom("select settlementCurrency,settlementValue from transactions where transID = '".$transID."'");
		
		if($agentType == 'Agent')
		{
			$accountQuery = "insert into ".TBL_AGENT_ACCOUNT." 
			(agentID, dated, type, amount, modified_by, TransID, description,note,settleAmount,SID,currency) values
			('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description','$note','".$settlementAmount."','".$settleID."','".$currencyLedger."')";	
		}
		elseif($agentType == 'Distributor')
		{
			$accountQuery = "insert into ".TBL_DISTRIBUTOR_ACCOUNT."
			(bankID, dated, type, amount, modified_by, TransID, description,settlementCurrency,settlementValue,currency) values
			('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description','".$transData['settlementCurrency']."','".$transData['settlementValue']."','".$currencyLedger."')";
		}
		elseif($agentType == "Both")
		{
			if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD == "1")
			{
				$currencyLegerQ  = selectFrom("select currencyFrom,currencyTo,localAmount from ".TBL_TRANSACTIONS." where transID ='".$transID."' ");	
				
				/* As agent */
				if($actAs == "Agent")
				{
					$currencyLedger = $currencyLegerQ["currencyFrom"];
				}
				/* As distributor */
				else
				{
					$currencyLedger = $currencyLegerQ["currencyTo"];
					$amount = $currencyLegerQ["localAmount"];
				}
			}
			$accountQuery = "insert into ".TBL_AnD_ACCOUNT." 
			(agentID, dated, type, amount, modified_by, TransID, description, actAs,note,currency) values
			('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs','$note','".$currencyLedger."')";
		}
		$q = insertInto($accountQuery);
										
		agentSummaryAccount($agentID, $type, $amount,$currencyLedger,$settlementAmount); 
		
		$currentBalance = $agentQuery["balance"];
		if($type == 'DEPOSIT')
		{
			$currentBalance += $amount;
		}
		else
		{
			$currentBalance -= $amount;
		}
		update("update " .TBL_ADMIN_USERS. " set balance = $currentBalance where userID = '".$agentID."'");											
		//debug($q,true);
		return $q;		
	}
	
	
	function updateSubAgentAccount($agentID, $amount, $transID, $type, $description, $actAs,$currencyFrom,$note='',$today='0000-00-00')
	{
		//debug_print_backtrace();
		$reverse = false;
		$agentQuery = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
		$isCorresspondent = $agentQuery["isCorrespondent"];
		if($isCorresspondent != 'N')
		{
			$agentType = "Distributor";
			
			if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
			{
				$agentType = "Both";///AnD
			}	
		}elseif($isCorresspondent == 'N')
		{
			$agentType = "Agent";
		}
		
		if($today == '0000-00-00')
			$today = date("Y-m-d");  
		
		$loginID  = $_SESSION["loggedUserData"]["userID"];
		$currencyLedger=$currencyFrom;
		
		if(CONFIG_TRANS_ROUND_LEVEL != "")
           {
	            $roundLevel = CONFIG_TRANS_ROUND_LEVEL;
           }else{
           	  $roundLevel = 4;
	         }
	 
			///// Added by Niaz Ahmad #2810 connect plus at 17-01-08 Settlement Amount///////////
if((CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $actAs == "Agent") || (CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1' && $actAs == "Distributor") && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1"){
	if($actAs == "Agent")
	{
		$rateFor = "agent";	
	}else{
		$rateFor = "distributor";	
		}
  
  	if($agentType == "Both")
	{
		/* Settlement currency For A&D*/
		$settlementCurr  = selectFrom("select settlementCurrencies from ".TBL_ADMIN_USERS." where userID ='".$agentID."' ");	
		$arrSettlementCurrency = explode("|",$settlementCurr["settlementCurrencies"]);
		if($rateFor == "agent")
			$settlementCurrency = $arrSettlementCurrency[0]; /* For Agent currency*/
		elseif($rateFor == "distributor")
			$settlementCurrency = $arrSettlementCurrency[1]; /* For distributor currency */
			
		/* Setting value to array so that not to change below variables*/
		$settlementCurr["settlementCurrency"] = $settlementCurrency;
	}
	else
	{
		$settlementCurr  = selectFrom("select userID,settlementCurrency from ".TBL_ADMIN_USERS." where userID ='".$agentID."' ");	
		$settlementCurrency=$settlementCurr["settlementCurrency"];
	}
  
  if($settlementCurr["settlementCurrency"]!=''){
     
     $exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='".$rateFor."' and rateValue=".$agentID."  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='agent' and rateValue=".$agentID."  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."') "); 
     
 
   if($exchangeRateAgent["erID"] == ''){
		 	
		 	  
		 	 $exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='generic'   and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='generic'  and currencyOrigin='".$settlementCurr["settlementCurrency"]."' and currency='".$currencyFrom."') "); 
		 	
		 	}
			
		if($exchangeRateAgent["erID"] == ''){
       		$exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='".$rateFor."' and rateValue=".$agentID."  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='agent' and rateValue=".$agentID."  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."') "); 
       		$reverse = true;
       }
    if($exchangeRateAgent["erID"] == ''){
		 	  
		 	 $exchangeRateAgent=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='generic'   and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='generic'  and currencyOrigin='".$currencyFrom."' and currency='".$settlementCurr["settlementCurrency"]."') "); 
		 	 $reverse = true;
		 	 
		 	}	
		
		
		 if ($exchangeRateAgent["erID"]!=''){
		 	
		 	 // To get net exchange rate
		 	if($exchangeRateAgent["marginType"] == 'fixed')
		   {
			$rateAgent = $exchangeRateAgent["primaryExchange"] - $exchangeRateAgent["marginPercentage"];
			}else{
				$rateAgent = $exchangeRateAgent["primaryExchange"] - ($exchangeRateAgent["primaryExchange"] * $exchangeRateAgent["marginPercentage"]) /  100;
			
		  }
		 
		 	 if($reverse)
		 	 {
		 	 		$rateAgent = 1/$rateAgent;
		 	 }
		 	 $rateAgent = round($rateAgent,4);
		 	 
		 	 if($rateFor == "agent"){
		 	 	
		 	 	if($description == "Transaction Created" || $description == "Transaction Amended"){
		 	 	$selectAgentData = selectfrom("select paymentMode from admin where userID = '".$agentID."'");
		 	 	$selectTransData = selectfrom("select AgentComm,IMFee,currencyFrom,fromCountry,toCountry,transDate,transType,moneyPaid from transactions where transID = '".$transID."'");
				if($settlementCurr["settlementCurrency"] != $selectTransData["currencyFrom"])
		 	 	{
	
					$exchangeRateCurrencyFrom = getMultipleExchangeRates($selectTransData["fromCountry"], $selectTransData["toCountry"], $settlementCurr["settlementCurrency"], $dist, 0, 0, $selectTransData["currencyFrom"],$selectTransData["transType"],$selectTransData["moneyPaid"], $agentID);
			 	 												
					$exErID = $exchangeRateCurrencyFrom[0];
					$exRateValue = $exchangeRateCurrencyFrom[1];
					$AgentCurrencyFrom = $exchangeRateCurrencyFrom[2];
					$AgentCurrencyTo 	= $exchangeRateCurrencyFrom[3];
				}else{
					$exErID = 0;
					$exRateValue = 1;
					$AgentCurrencyFrom = $selectTransData["currencyFrom"];
					$AgentCurrencyTo 	= $settlementCurr["settlementCurrency"];
					}
		 	 //$exchangeRateCurrencyFrom=selectFrom("select erID,primaryExchange,marginType,marginPercentage from " . TBL_EXCHANGE_RATES . " where rateFor='".$rateFor."' and rateValue=".$agentID."  and currencyOrigin='".$selectTransData["currencyFrom"]."' and currency='".$settlementCurr["settlementCurrency"]."'  and dated= (select Max(dated) from " . TBL_EXCHANGE_RATES . " where rateFor='agent' and rateValue=".$agentID."  and currencyOrigin='".$selectTransData["currencyFrom"]."' and currency='".$settlementCurr["settlementCurrency"]."') "); 
		 	 	
		 	}
		 	}
		 
		 if($settlementCurr["settlementCurrency"] == $currencyFrom ){
		 		 $settlementAmount = $amount;
		 		 
		 		 if($rateFor == "agent"){
		 		 		if($description == "Transaction Created" || $description == "Transaction Amended"){
		 		 	
		 				if($selectAgentData["paymentMode"] == "exclude"){
		 			
		 			
		 		 	
		 				$settlementAmount = $settlementAmount + ($selectTransData["IMFee"] - $selectTransData["AgentComm"]);
				 	}else{
		 		
		 				$settlementAmount += $selectTransData["IMFee"];
		 		
		 		}
		 	}
		 	}
		 		  $currencyLedger = $currencyFrom;
		 		
		 	}else{
              
     			$settlementAmount=$amount/$rateAgent;
 				
       

       if($rateFor == "agent"){
       	 	
       		if($description == "Transaction Created" || $description == "Transaction Amended"){
     if($selectAgentData["paymentMode"] == "exclude"){
     											
     									 $calcomm = $selectTransData["IMFee"] - $selectTransData["AgentComm"];
										
											 $AgentCommissionSettlement = $calcomm * $exRateValue;        
									
									
    							}else{
    								
    								$AgentCommissionSettlement =$selectTransData["IMFee"]* $exRateValue;        
    								   								
    								}
    										$settlementAmount += $AgentCommissionSettlement;
    					
      }
      }
      $amount=$settlementAmount;
      $currencyLedger = $settlementCurrency;
      }
    }
    
    $date_time = date('Y-m-d h:i:s'); 
   $settleQuery = "INSERT INTO ". TBL_SETTLEMENT_AMOUNT." (settlementCurrency,currencyFrom,settleAmount,agentID,transID,dated) VALUES ('".$settlementCurr["settlementCurrency"]."','".$currencyFrom."','".$settlementAmount."','".$agentID."','".$transID."','".$date_time."' )";
   insertInto($settleQuery);
   $settleID = @mysql_insert_id();
  }
}

//////// Added by Niaz at 09-02-2008 Disrtibutor Exchange Rate COP-USD or (USD-COP, 1/USD-COP Rate=COP-USD)//////

		$transData = selectFrom("select settlementCurrency,settlementValue from transactions where transID = '".$transID."'");
		if($agentType == 'Agent')
		{
			$accountQuery = "insert into sub_agent_account 
			(agentID, dated, type, amount, modified_by, TransID, description,currency,settleAmount,SID) values
			('$agentID', '$today', '$type', '$amount', '$loginID', '". $transID ."', '$description','".$currencyLedger."','".$settlementAmount."','".$settleID."')";	
		}elseif($agentType == 'Distributor')
		{
			$accountQuery = "insert into sub_bank_account
			(bankID, dated, type, amount, modified_by, TransID, description,settlementCurrency,settlementValue,currency) values
			('$agentID', '".$today."', '$type', '$amount', '$loginID', '". $transID."', '$description','".$transData['settlementCurrency']."','".$transData['settlementValue']."','".$currencyLedger."')";
		}elseif($agentType == "Both")
		{
			if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD == "1")
			{
				$currencyLegerQ  = selectFrom("select currencyFrom,currencyTo from ".TBL_TRANSACTIONS." where transID ='".$transID."' ");	
				
				/* As agent */
				if($actAs == "Agent")
				{
					$currencyLedger = $currencyLegerQ["currencyFrom"];
				}
				/* As distributor */
				else
				{
					$currencyLedger = $currencyLegerQ["currencyTo"];
					$amount = $currencyLegerQ["localAmount"];
				}
			}
			$accountQuery = "insert into sub_agent_Dist_account 
			(agentID, dated, type, amount, modified_by, TransID, description, actAs, note,currency) values
			('".$agentID."', '$today', '$type', '$amount', '$loginID', '". $transID."', '$description','$actAs', '".$note."','".$currencyLedger."')";
		}
		$q = insertInto($accountQuery);

		/* subAgentSummaryAccount($agentID, $type, $amount,$currency,$settlementAmount); */
		subAgentSummaryAccount($agentID, $type, $amount,$currencyFrom,$settlementAmount); 
		
		$currentBalance = $agentQuery["balance"];
		if($type == 'DEPOSIT')
		{
			$currentBalance += $amount;
		}else{
			$currentBalance -= $amount;
			}
		update("update " .TBL_ADMIN_USERS. " set balance = $currentBalance where userID = '".$agentID."'");											
		
		
	return $q;	
			
	}
	
	
function agentSummaryAccount($agentID, $type, $amount,$currency="",$settlementAmount="")
{

/* #4634 added to get Agent or Distributor from AnD.
   added by ashahid
*/
	if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING=="1" && $currency!=""){
		$getAnDActAsRecord = getAnDActAsLabel($agentID,$currency);
	}
	 $today = date("Y-m-d"); 
	 	///// Added by Niaz Ahmad #2810 connect plus at 21-01-08 Settlement Amount///////////
	 	
          if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
  	     $agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '$today' and currency = '".$currency."' ");
  	    }else if($currency!="" && $getAnDActAsRecord!=""){
  	    	
  	    	$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and currency = '".$currency."' order by id desc");
  	    	
  	    	}  
		else{
  	    	
  	    	$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '$today' ");
  	    	
  	    	}  
		if($agentContents["user_id"] != "")
		{
			$balance = $agentContents["closing_balance"];
						if($type == 'DEPOSIT')
			{
				$balance = $balance + $amount;	
				
			}else{
				$balance = $balance - $amount;	
				
			}
/* 	#4634 
	getAnDActAsRecord added in condition to use funcionality for AnD.
*/
			if($currency!="" && $getAnDActAsRecord!="" ){
		  	    insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance,currency) values('$today', '$agentID', '".$agentContents["closing_balance"]."', '".$balance."','".$currency."')");
			}
			else{
			      update("update ".TBL_ACCOUNT_SUMMARY." set closing_balance = '$balance',currency = '$currency' where id = '".$agentContents["id"]."'");			
			}
		 
	}else{
		       if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
		         $datesLast = selectFrom("select  Max(dated) as dated from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and currency = '".$currency."' ");	
			       $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance,currency  from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '".$datesLast["dated"]."' and currency = '".$currency."'");
				   
		    }
/* 	#4634 
	getAnDActAsRecord added in condition to use funcionality for AnD.
*/
			else if($currency!="" && $getAnDActAsRecord!="" ){
		          $summaryLast = selectFrom("select  Max(id) as lastID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID'  and currency = '".$currency."' order by id desc");	
			        $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance,currency  from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and id = '".$summaryLast["lastID"]."'");
		    }else{
		          $datesLast = selectFrom("select  Max(dated) as dated from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' ");	
			        $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance,currency  from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$agentID' and dated = '".$datesLast["dated"]."'");
		    	
		    	} 
			$balance = 0;
				
			$balance += $agentContents["closing_balance"];
		 			
			$openingBalance = $balance;
		  		
			if($type == 'DEPOSIT')
			{
				$closingBalance = $balance + $amount;	
				
			}else{
				$closingBalance = $balance - $amount;	
				
			}
			   if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
		         insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance,currency ) values('$today', '$agentID', '$openingBalance', '$closingBalance','$currency')");
			}
/* 	#4634 
	getAnDActAsRecord added in condition to use funcionality for AnD.
*/
			else if($currency!="" && $getAnDActAsRecord!="" ){	 
		  	    insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance,currency) values('$today', '$agentID', '$openingBalance', '$closingBalance','".$currency."')");
		   }else{
		  	    insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')");
		  	}
		
		}	
		
			
}

function subAgentSummaryAccount($agentID, $type, $amount,$currency,$settlementAmount)
{
	$today = date("Y-m-d"); 
	 
	if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
  	$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from sub_account_summary where user_id = '$agentID' and dated = '$today' and currency = '".$currency."' ");
  }else{
		$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from sub_account_summary where user_id = '$agentID' and dated = '$today'");
	}

		if($agentContents["user_id"] != "")
		{
			
			$balance = $agentContents["closing_balance"];
			
			if($type == 'DEPOSIT')
			{
				$balance = $balance + $amount;	
			}else{
				$balance = $balance - $amount;	
			}
			
		 update("update sub_account_summary set closing_balance = '$balance',currency = '$currency' where id = '".$agentContents["id"]."'");			
	}else{
		if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
			$datesLast = selectFrom("select  Max(dated) as dated from sub_account_summary where user_id = '$agentID' and currency = '".$currency."' ");	
			$agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance,currency  from sub_account_summary where user_id = '$agentID' and dated = '".$datesLast["dated"]."' and currency = '".$currency."'");
		}else{
			$datesLast = selectFrom("select  Max(dated) as dated from sub_account_summary where user_id = '$agentID'");
		 	$agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance from sub_account_summary where user_id = '$agentID' and dated = '".$datesLast["dated"]."'");
		}
			
			$balance = 0;
			
			$balance += $agentContents["closing_balance"];
				
			
			
			$openingBalance = $balance;
			
			if($type == 'DEPOSIT')
			{
				$closingBalance = $balance + $amount;	
			}else{
				$closingBalance = $balance - $amount;	
			}
				
	 	 	if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1") {
	    	insertInto("insert into sub_account_summary (dated, user_id, opening_balance, closing_balance,currency ) values('$today', '$agentID', '$openingBalance', '$closingBalance','$currency')");
	   	}else{
		 	insertInto("insert into sub_account_summary (dated, user_id, opening_balance, closing_balance) values('$today', '$agentID', '$openingBalance', '$closingBalance')");
		 	}
		}	
		
			
}
	
function recordSubAgentCommission($agentID, $transID, $description)
{
	
	$today = date("Y-m-d");  
	$loginID  = $_SESSION["loggedUserData"]["userID"];
	
	$agentQuery = selectFrom("select isCorrespondent, commPackage, agentCommission, commPackageAnDDist, commAnDDist, parentID from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
	$isCorresspondent = $agentQuery["isCorrespondent"];
	
	if($isCorresspondent != 'N')
	{
		$agentType = "Distributor";
		
		if(CONFIG_AnD_ENABLE == '1' && $isCorresspondent == 'Y')
		{
			$agentType = "Both";///AnD
		}	
	}elseif($isCorresspondent == 'N')
	{
		$agentType = "Agent";
	}
	
	if($agentType == 'Agent')
	{
		$accountQuery = "update sub_agent_account set
		commission = '',
		commPackage = ''
		where TransID = '$transID' and description = '$description'  	 
		";
	}elseif($agentType == 'Distributor')
	{
		$accountQuery = "insert into sub_bank_account
		commission = '',
		commPackage = ''
		where TransID = '$transID' and description = '$description'  	 
		";
	}elseif($agentType == "Both")
	{
		$accountQuery = "insert into sub_agent_Dist_account 
		commission = '',
		commPackage = ''
		where TransID = '$transID' and description = '$description'  	 
		";
	}
	$q = insertInto($accountQuery);
	
return $q;	
		
}	

function recordAgentCommission($agentID, $amount, $fee, $userType)
{
	$packageQuery="select userID, username, parentID, agentType, isCorrespondent, commPackage, agentCommission, commPackageAnDDist, commAnDDist from ". TBL_ADMIN_USERS." where userID = '".$agentID."'";
	$agentPackage = selectFrom($packageQuery);
	
	$agentParentID = $agentPackage["parentID"];
	
	if($userType == "Agent")
	{
		
		
		if(CONFIG_AnD_ENABLE == '1' && $agentPackage["isCorrespondent"] == 'Y')
		{
			$isAgentAnD = 'Y';
		}
	
		$package = $agentPackage["commPackage"];
		
		
		switch ($package)
			  {
			  	case "001": // Fixed amount per transaction
				{
					$agentCommi = $agentPackage["agentCommission"];
					$commType = "Fixed Amount";
					break;
				}
			  	case "002": // percentage of transaction amount
				{
					$agentCommi = ($amount * $agentPackage["agentCommission"]) / 100;
					$commType = "Percentage of transaction Amount";
					break;
				}
			  	case "003": // percentage of IMFEE
				{
					$agentCommi = ($fee * $agentPackage["agentCommission"]) / 100;
					$commType = "Percentage of Fee";
					break;
				}
				case "004":
				{
					$agentCommi = 0;
					$commType = "None";
					break;
				}
			}
			$agentCommission[0] = $agentCommi;				  
			$agentCommission[1] = $commType;
		}else{
				
				if(CONFIG_AnD_ENABLE == '1' && $agentPackage["isCorrespondent"] == 'Y')
				{
					$isBankAnD = 'Y';
					
					$packagedistr = $agentPackage["commPackageAnDDist"];
				
				//
				switch ($packagedistr)
					  {
					  	case "001": // Fixed amount per transaction
						{
							$distrCommi = $agentPackage["commAnDDist"];
							$commTypeDist = "Fixed Amount";
							break;
						}
					  	case "002": // percentage of total transaction amount
						{
							$distrCommi = ($amount * $agentPackage["commAnDDist"]) / 100;
							$commTypeDist = "Percentage of total Amount";
							break;
						}
					  	case "003": // percentage of IMFEE
						{
							$distrCommi = ($fee * $agentPackage["commAnDDist"]) / 100;
							$commTypeDist = "Percentage of Fee";
							break;
						}
						case "004":
						{
							$distrCommi = 0;
							$commTypeDist = "None";
							break;
						}
					}
					$agentCommission[0] = $distrCommi;				  
					$agentCommission[1] = $commTypeDist;
					
				}else{
						$packagedistr = $agentPackage["commPackage"];
				
				//
				switch ($packagedistr)
					  {
					  	case "001": // Fixed amount per transaction
						{
							$distrCommi = $agentPackage["agentCommission"];
							$commTypeDist = "Fixed Amount";
							break;
						}
					  	case "002": // percentage of total transaction amount
						{
							$distrCommi = ($amount * $agentPackage["agentCommission"]) / 100;
							$commTypeDist = "Percentage of total Amount";
							break;
						}
					  	case "003": // percentage of IMFEE
						{
							$distrCommi = ($fee * $agentPackage["agentCommission"]) / 100;
							$commTypeDist = "Percentage of Fee";
							break;
						}
						case "004":
						{
							$distrCommi = 0;
							$commTypeDist = "None";
							break;
						}
					}
					
				$agentCommission[0] = $distrCommi;				  
				$agentCommission[1] = $commTypeDist;	
				}				  
			}		   
	
	
return $agentCommission;	
}

function shareProfit($userID, $transID, $userType)
{
	$modifiedDate = getCountryTime(CONFIG_COUNTRY_CODE);
	$configContents = selectFrom("select shareConfigID, shareType, shareValue, shareFrom from ".TBL_SHARE_CONFIG." where userid = '".$userID."' and status = 'Active' and userType = '".$userType."'");
	if($configContents["shareConfigID"] > 0)
	{
		$transContent = selectFrom("select transAmount, exchangeID, exchangeRate from ".TBL_TRANSACTIONS." where transID = '".$transID."'");	
		$exchangeContents = selectFrom("select primaryExchange, marginPercentage, currency, marginType from ".TBL_EXCHANGE_RATES." where erID = '".$transContent["exchangeID"]."'");
		
		if($exchangeContents["marginType"] == 'fixed')
    	{
    		 $calculatedMargin = $exchangeContents["marginPercentage"];
    	}else{
    		
    		 $calculatedMargin = round(($exchangeContents["primaryExchange"] * $exchangeContents["marginPercentage"]/100), 4);
    	}
    //	echo("ConfigMargin is ".$calculatedMargin);
    	
    	if($configContents["shareFrom"] != 'company')
    	{
    		$agentconfigContents = selectFrom("select shareConfigID, shareType, shareValue, shareFrom from ".TBL_SHARE_CONFIG." where userid = '".$configContents["shareFrom"]."' and status = 'Active' and userType = '".$userType."'");
    		if($agentconfigContents["shareType"] == 'fixed')
    		{
    			$parentShare = $agentconfigContents["shareValue"];
    		}else{
    			$difference = ($exchangeContents["primaryExchange"] * $transContent["transAmount"]) - (($exchangeContents["primaryExchange"] - $calculatedMargin)* $transContent["transAmount"]);
    			$parentShare = round(($difference * $agentconfigContents["shareValue"])/100, 4);
    		}
    		
    		
	    	if($configContents["shareType"] == 'fixed')
	    	{
	    		$agentShare = $configContents["shareValue"];
	    	}else{
	    		
	    		$agentShare = round(($parentShare * $configContents["shareValue"])/100, 4);
	    		}
	    		
	    	$parentShare -= $agentShare;
	    	
	    		$checkParentExist = selectFrom("select profitShareID from ".TBL_SHARE_PROFIT." where transID = '".$transID."' and agentID = '".$configContents["shareFrom"]."'");
	    		if($checkParentExist["profitShareID"] != "")
	    		{
	    			$updateParentShare = "update ".TBL_SHARE_PROFIT." set 
	    			agentMargin = '".$parentShare."' where 
	    			profitShareID = '".$checkParentExist["profitShareID"]."'";	
	    			update($updateParentShare);
	    		}
    	}else{    	
    	
	    	if($configContents["shareType"] == 'fixed')
	    	{
	    		$agentShare = $configContents["shareValue"];
	    	}else{
	    		$difference = ($exchangeContents["primaryExchange"] * $transContent["transAmount"]) - (($exchangeContents["primaryExchange"] - $calculatedMargin)* $transContent["transAmount"]);
	    		$agentShare = round(($difference * $configContents["shareValue"])/100, 4);
	    		}
    	}
////	echo("Actual Amount".($exchangeContents["primaryExchange"] * $transContent["transAmount"]));
////	echo("Calculated Amount".(($exchangeContents["primaryExchange"] - $calculatedMargin)* $transContent["transAmount"]));
////	echo("CalculatedMargin is ".$difference);
////	echo("Share in config is ".$configContents["shareValue"]);
////	echo(" Calculated share is ".$agentShare);
////	echo("Calculation is ".($difference * $configContents["shareValue"])/100);
    	insertInto("insert into ".TBL_SHARE_PROFIT." (transID, exchRateUsed, exchRateActual, currencyTo, agentMargin, agentID, calculationTime, actAs) values
    	('".$transID."', '".$transContent["exchangeRate"]."', '".$exchangeContents["primaryExchange"]."', '".$exchangeContents["currency"]."', '".$agentShare."', '".$userID."', '".$modifiedDate."', '".$userType."' )");
		
	}	
}

function inputDates($frmDate, $tDate)
{
	 if($frmDate != '' && $tDate != '')
			{
				
				if(isValidDate($frmDate) && isValidDate($tDate)){
					$fromDate = $frmDate;
					$toDate = $tDate;
					$flag = "0";
					$data[] = $flag;
					$data[] = $fromDate ;
		      $data[] = $toDate;
				}else{
					$message = "Please Input Valid Date Format";
					$flag = "1";
					$data[] = $flag;
					$data[] = $message;
					}
			}else{
				$message = "Please input both from and to date values";
				$flag = "1";
				
				$data[] = $flag;
				$data[] = $message;
				
				}
				
			  
		return $data;	
			
}

function recordAgentCommissionCountrySpecific($agentID, $amount, $fee, $userType,$benCountry)
{
	//debug_print_backtrace();
	
	$packageQuery="select  parentID, isCorrespondent, commPackage, commPackageAnDDist, commAnDDist from ". TBL_ADMIN_USERS." where userID = '".$agentID."'";
  $agentPackage = selectFrom($packageQuery);
	
	 $configureCommQry = "select * from ". TBL_DISTRIBUTOR_COMM." where user_id = '".$agentID."' and country = '".$benCountry."'";
	//debug($configureCommQry);
	if(CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')
	{
		$configureCommQry .= " and actAs = '".$userType."'";
	}
	$configureComm = selectFrom($configureCommQry);	
	//debug($configureComm);
	$agentParentID = $agentPackage["parentID"];
	
		
		
		if(CONFIG_AnD_ENABLE == '1' && $agentPackage["isCorrespondent"] == 'Y')
		{
			$isAgentAnD = 'Y';
		}
	
		$type = $configureComm["commType"];
		
		//
		switch ($type)
			  {
			  	case "fixed": // Fixed amount per transaction
				{
					$agentCommi = $configureComm["commRate"];
					$commType = "Fixed Amount";
					break;
				}
			  	case "percentTrans": // percentage of transaction amount
				{
					$agentCommi = ($amount * $configureComm["commRate"]) / 100;
					$commType = "Percentage of transaction Amount";
					break;
				}
			  	case "percentFee": // percentage of IMFEE
				{
					$agentCommi = ($fee * $configureComm["commRate"]) / 100;
					$commType = "Percentage of Fee";
					break;
				}
				case "interval": // denomination / interval based fee
				{
					$intCommissionFee = 0;
					
					$intFeeCounter = 0;
					
					$intFeeCounter = ceil($amount / $configureComm["interval"]);
					/*
					while($intFeeCounter <= $amount)
					{
						$intCommissionFee += $configureComm["commRate"];
						$intFeeCounter += $configureComm["interval"];
					}
					*/
					$intCommissionFee = $intFeeCounter * $configureComm["commRate"];
					$agentCommi = $intCommissionFee;
					$commType = "Denominaion Based Fee";
					//debug($amount." ^ ".$configureComm["interval"]." : ".$intFeeCounter." & ".$configureComm["commRate"]." # ".$amount, true);
					break;
				}
				
			}
			$agentCommission[0] = $agentCommi;				  
			$agentCommission[1] = $commType;
		
	//debug($agentCommission);
	
return $agentCommission;	
}

function searchNameMultiTables($searchString,$fn,$mn,$ln,$alis){
	
	   
	     $sName = explode(" ",$searchString );
				if($sName[0] != ""){
					$fName = $sName[0];
					
					}
					if($sName[1] != ""){
					$mName = $sName[1];
				
					}
					if($sName[2] != ""){
					$lName = $sName[2];
					
					}				
					
						
					
					if(count($sName)== '1'){
				
						  $query = " and ( ".$alis.".".$fn." like '".$searchString."%' OR ".$alis.".".$ln." like '".$searchString."%' OR ".$alis.".".$mn." like '".$searchString."%') ";
							$queryCnt= " and ( ".$alis.".".$fn." like '".$searchString."%' OR ".$alis.".".$ln." like '".$searchString."%' OR ".$alis.".".$mn." like '".$searchString."%') ";
							
				}
					
			elseif(count($sName) == '2'){
				   	$query = " and(".$alis.".".$fn." like '".$fName."' AND ".$alis.".".$ln." like '".$mName."') ";
						$queryCnt= " and(".$alis.".".$fn." like '".$fName."' AND ".$alis.".".$ln." like '".$mName."') ";
		    }
		
				elseif(count($sName) == '3'){
							$query = " and (".$alis.".".$fn." like '".$fName."' AND ".$alis.".".$mn." like '".$mName."' AND ".$alis.".".$ln." like '".$lName."') ";
							$queryCnt = " and (".$alis.".".$fn." like '".$fName."' AND ".$alis.".".$mn." like '".$mName."' AND ".$alis.".".$ln." like '".$lName."') ";
		    }
								
			else{
				
				$query = " and (".$alis.".".$fn." like '".$fName."' OR ".$alis.".".$md." like '".$mName."' OR ".$alis.".".$ln." like '".$lName."') ";
				$queryCnt = " and (".$alis.".".$fn." like '".$fName."' OR ".$alis.".".$md." like '".$mName."' OR ".$alis.".".$ln." like '".$lName."') ";
		}
	     	
      	$combineString=$query.",".$queryCnt;
   
			
	//return  $combineString;
	return  $query;
	}
	function getHttpVars() {
		// test by Niaz
			
		$superglobs = array(
			'_POST',
			'_REQUEST',
			'_GET',
			'_FILES',
			'HTTP_POST_VARS',
			'HTTP_GET_VARS');
    
   	$httpvars = array();
     
		// extract the right array
		foreach ($superglobs as $glob) {
		  		// echo "<br>";
		  		// print_r( $glob);
			 global $$glob;
				if (isset($$glob) && is_array($$glob)) {
				
				$httpvars = $$glob;
				
			 }
			if (count($httpvars) > 0)
				break;
		}
		//print_r($httpvars);
		return $httpvars;

	}
	
	
	function getBankCharges($custCountry, $benCountry, $currencyFrom, $currencyTo, $amount, $dist)
	{
		$chargesGot = 0;
		
		if($dist > 0)
		{
			$chargesContent = selectFrom("select chargeID, Charges, chargeType from ".TBL_BANK_CHARGES." where chargesStatus != 'Disable' and
				origCountry = '".$custCountry."' and destCountry = '".$benCountry."' and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."' and chargeBasedOn = 'Distributor' and agentNo = '".$dist."'
				and (amountRangeFrom <= '".$amount."' and amountRangeTo >= '".$amount."')");
			
		}
		
		if($chargesContent["chargeID"] == ''){
			$chargesContent = selectFrom("select chargeID, Charges, chargeType from ".TBL_BANK_CHARGES." where chargesStatus != 'Disable' and
				origCountry = '".$custCountry."' and destCountry = '".$benCountry."' and currencyOrigin = '".$currencyFrom."' and currencyDest = '".$currencyTo."' 
				and (amountRangeFrom <= '".$amount."' and amountRangeTo >= '".$amount."')");
				
			}
			
			if($chargesContent["chargeType"] == 'fixed')
			{
				$chargesGot = $chargesContent["Charges"];
				}elseif($chargesContent["chargeType"] == 'percent')
				{
						$chargesGot = ($amount * $chargesContent["Charges"])/100;
				}
			
			
			return $chargesGot;
	}
	
function saveUsedID($exRateID)
{
	$selectExch = "select * from exchangerate  where erID = '".$exRateID."'";
	$usedExchQuery = "insert into  exRateUsed ()values()";
}	
	
function getMultiPatternRate($custCountry, $benCountry, $currencyTo, $dist, $amount, $dDate, $currencyFrom,$transType,$paymentMode, $agent, $withMargin)
{
	$actualRate = 0;
	//$currencyFrom = $_GET["curr1"];
	//$currencyTo = $_GET["curr2"];
	/*$custCountry = 'United Kingdom';
	$benCountry  = 'Pakistan';
	$dist	=	'100051';
	$amount	= 100;
	$dDate	= '';
	$transType	= 'Pick up';
	$paymentMode = 'By Cash';
	$agent	= '100071';*/
	$data = array();	
	$coreCurr = "";
	$interCurr = "";
	$actualRate = 0;
	/////////////////////////////////////////////////Currency Types Got///////////////////
	$intermediates = SelectMultiRecords("select distinct(currencyName), currencyType from ".TBL_CURRENCY." where currencyType like 'Intermediate'");
	$cores = SelectMultiRecords("select distinct(currencyName), currencyType from ".TBL_CURRENCY." where currencyType like 'Core'");
	/////////////////////////////////////////////////////////////////////////////////////
	
	for($i = 0; $i < count($cores); $i++)
	{
		if($i > 0)
		{
			$coreCurr .= ","; 	
		}
		$coreCurr .= "'".$cores[$i]["currencyName"]."'";
				
	}
	for($i = 0; $i < count($intermediates); $i++)
	{
		if($i > 0)
		{
			$interCurr .= ","; 	
		}
		$interCurr .= "'".$intermediates[$i]["currencyName"]."'";
		
	}
	/*$basicRate = SelectMultiRecords("select * from ".TBL_EXCHANGE_RATES." where 
		currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.")");
	echo("First Query --> select * from ".TBL_EXCHANGE_RATES." where 
		currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.")");
		*/
		///////////////////////////////////Fetching Basic Exchange Rate for Core Currency//////
				
		////Money Paid By
		 $basicRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
		 country='$benCountry' and countryOrigin='$custCountry' 
		 and currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.") 
		 and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') ";
			if($dDate != "")
			{
				$basicRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
			}
			
			$basicRate = SelectMultiRecords($basicRateQuery);
		////Trans Type
		if(count($basicRate) == 0)
		{
		$basicRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
		country='$benCountry' and countryOrigin='$custCountry' 
		and currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.")  
		and (rateFor = 'transType'  and rateValue = '$transType') ";
		
			if($dDate != "")
			{
				$basicRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
			}
			
			$basicRate = SelectMultiRecords($basicRateQuery);
		}
		////Agent
		if(count($basicRate) == 0 || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0))
		{
			$basicRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
			country='$benCountry' and countryOrigin='$custCountry' 
			and currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.")  
			and (rateFor = 'agent' and rateValue = '$agent') ";
			
			if($dDate != "")
			{
				$basicRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
			}
			
			$basicRate = SelectMultiRecords($basicRateQuery);
		}
		
		///Distributor
		if(count($basicRate) == 0)
		{
			$basicRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
			country='$benCountry' and countryOrigin='$custCountry' 
			and currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.") 
			and (rateFor = 'distributor'  and rateValue = '$dist')";
			
			if($dDate != "")
			{
				$basicRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
			}
			
			$basicRate = SelectMultiRecords($basicRateQuery);
		}
	/////Generic
		if(count($basicRate) == 0)
		{
			$basicRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
			country='$benCountry' and countryOrigin='$custCountry' 
			and currencyOrigin = '".$currencyFrom."' and currency in (".$coreCurr.",".$interCurr.") 
			and (rateFor = '' OR rateFor = 'generic') ";
			
			if($dDate != "")
			{
				$basicRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
			}
			
			$basicRate = SelectMultiRecords($basicRateQuery);
		}
		
		///////////////////////////////////////////////////////////////////////////////////////
		
		if(count($basicRate) > 0)
		{
			for($j = 0; $j < count($basicRate); $j++)
			{
				$data = array();
				
				if($basicRate[$j]["marginType"] == 'fixed')
				{
					$rate = $basicRate[$j]["primaryExchange"] - $basicRate[$j]["marginPercentage"];
				}else{
					$rate = round($basicRate[$j]["primaryExchange"] - ($basicRate[$j]["primaryExchange"] * $basicRate[$j]["marginPercentage"]) /  100 , 4);
				}
				
				if($withMargin == 'Yes')
				{
					if($basicRate[$j]["agentMarginType"] == 'fixed')
					{
						$rate = $rate - $basicRate[$j]["agentMarginValue"];
					}else{
						$rate = round($rate - ($rate * $basicRate[$j]["agentMarginValue"]) /  100 , 4);
					}
				}
				
				$data[] = $rate;
				/*
				Add Primary rate into DB
				*/
				
				$basicCurr2 = $basicRate[$j]["currency"];
				
				if(strstr($coreCurr, $basicCurr2) != '' || $actualRate == 0)
				{
			
					while(strstr($interCurr, $basicCurr2) == '')////A check is needed to avoid infinite loop
					{
							/*$interRate = SelectFrom("select * from ".TBL_EXCHANGE_RATES." where 
								currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.")");
							echo("Inter Currency --> select * from ".TBL_EXCHANGE_RATES." where 
								currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.")");*/
										///////////////////////////////////Fetching Basic Exchange Rate for Core Currency//////
				
									////Money Paid By
									 $interRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
									 country='$benCountry' and countryOrigin='$custCountry' 
									 and currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.") 
									 and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') ";
										if($dDate != "")
										{
											$interRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$interRate = SelectFrom($interRateQuery);
									////Trans Type
									if($interRate["erID"] <= 0)
									{
									$interRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
									country='$benCountry' and countryOrigin='$custCountry' 
									and currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.")   
									and (rateFor = 'transType'  and rateValue = '$transType') ";
									
										if($dDate != "")
										{
											$interRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$interRate = SelectFrom($interRateQuery);
									}
									////Agent
									if($interRate["erID"] <= 0 || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0))
									{
										$interRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.") 
										and (rateFor = 'agent' and rateValue = '$agent') ";
										
										if($dDate != "")
										{
											$interRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$interRate = SelectFrom($interRateQuery);
									}
									
									///Distributor
									if($interRate["erID"] <= 0)
									{
										$interRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.") 
										and (rateFor = 'distributor'  and rateValue = '$dist') ";
										
										if($dDate != "")
										{
											$interRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$interRate = SelectFrom($interRateQuery);
									}
								/////Generic
									if($interRate ["erID"] <= 0)
									{
										$interRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$coreCurr.",".$interCurr.") 
										and (rateFor = '' OR rateFor = 'generic') ";
										
										if($dDate != "")
										{
											$interRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$interRate = SelectFrom($interRateQuery);
									}
									
									///////////////////////////////////////////////////////////////////////////////////////
									
								
							if($interRate["marginType"] == 'fixed')
							{
								$rate = $interRate["primaryExchange"] - $interRate["marginPercentage"];
							}else{
								$rate = round($interRate["primaryExchange"] - ($interRate["primaryExchange"] * $interRate["marginPercentage"]) /  100 , 4);
							}
							
							if($withMargin == 'Yes')
							{
								if($interRate["agentMarginType"] == 'fixed')
								{
									$rate = $rate - $interRate["agentMarginValue"];
								}else{
									$rate = round($rate - ($rate * $interRate["agentMarginValue"]) /  100 , 4);
								}
							}			
							$data[] = $rate;
							$basicCurr2 = $interRate["currency"];
							/*
							add rate in DB from here
							*/
								
					}
					
					while(strstr($interCurr, $basicCurr2) != '' || $basicCurr2 != $currencyTo)
					{
						
							/*$lastRate = SelectFrom("select * from ".TBL_EXCHANGE_RATES." where 
								currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."')");	
							echo("Last Rate --> select * from ".TBL_EXCHANGE_RATES." where 
								currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."')");*/	
										///////////////////////////////////Fetching Basic Exchange Rate for Core Currency//////
				
									////Money Paid By
									 $lastRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
									 country='$benCountry' and countryOrigin='$custCountry' 
									 and currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."') 
									 and (rateFor = 'MoneyPaid'  and rateValue = '$paymentMode') ";
										if($dDate != "")
										{
											$lastRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$lastRate = SelectFrom($lastRateQuery);
									////Trans Type
									if($lastRate["erID"] <= 0)
									{
									$lastRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
									country='$benCountry' and countryOrigin='$custCountry' 
									and currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."')   
									and (rateFor = 'transType'  and rateValue = '$transType') ";
									
										if($dDate != "")
										{
											$lastRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$lastRate = SelectFrom($lastRateQuery);
									}
									////Agent
									if($lastRate["erID"] <= 0 || (CONFIG_AGENT_OWN_RATE == '1' && $agent > 0))
									{
										$lastRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."') 
										and (rateFor = 'agent' and rateValue = '$agent') ";
										
										if($dDate != "")
										{
											$lastRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$lastRate = SelectFrom($lastRateQuery);
									}
									
									///Distributor
									if($lastRate["erID"] <= 0)
									{
										$lastRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."') 
										and (rateFor = 'distributor'  and rateValue = '$dist') ";
										
										if($dDate != "")
										{
											$lastRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$lastRate = SelectFrom($lastRateQuery);
									}
								/////Generic
									if($lastRate["erID"] <= 0)
									{
										$lastRateQuery = "select * from " . TBL_EXCHANGE_RATES . " where 
										country='$benCountry' and countryOrigin='$custCountry' 
										and currencyOrigin = '".$basicCurr2."' and currency in (".$interCurr.",'".$currencyTo."') 
										and (rateFor = '' OR rateFor = 'generic') ";
										
										if($dDate != "")
										{
											$lastRateQuery .= " and (dated >='$dDate 00:00:00' and dated <= '$dDate 23:59:59') "; 	
										}
			
										$lastRate = SelectFrom($lastRateQuery);
									}
									
									///////////////////////////////////////////////////////////////////////////////////////
								
							if($lastRate["marginType"] == 'fixed')
							{
								$rate = $lastRate["primaryExchange"] - $lastRate["marginPercentage"];
							}else{
								$rate = round($lastRate["primaryExchange"] - ($lastRate["primaryExchange"] * $lastRate["marginPercentage"]) /  100 , 4);
							}
							
							if($withMargin == 'Yes')
							{
								if($lastRate["agentMarginType"] == 'fixed')
								{
									$rate = $rate - $lastRate["agentMarginValue"];
								}else{
									$rate = round($rate - ($rate * $lastRate["agentMarginValue"]) /  100 , 4);
								}
							}	
							$data[] = $rate;
							$basicCurr2 = $lastRate["currency"];
							/*
							Add Other Rate into Data Base
							*/
								
					}

					for($k = 0; $k < count($data); $k++)
					{	
							if($k == 0)
							{
								$actualRate = $data[$k];
							}else{
								$actualRate = $actualRate * $data[$k];
							}
					}
					//echo("Actual Rate Found is".$actualRate."--");
				}
					
			}	
		}
		if(CONFIG_TRANS_ROUND_LEVEL > 0)
		{
		 $roundLevel = CONFIG_TRANS_ROUND_LEVEL;
		}else{
		$roundLevel = 4;
		}
		$actualRate = round($actualRate,$roundLevel);
		return $actualRate;
}



function generateDistRef($benTransID){
					
					
					$contentBenTrans = selectFrom("select benAgentID,transID, distRefNumber from ".TBL_TRANSACTIONS."  where transID = '".$benTransID."'");
					
					
					if($contentBenTrans["distRefNumber"] == "")
					{
						$selectExistingQuery = selectFrom("select refNumber, id, transFrom from ".TBL_REUSED_DIST_REF." where distributor = '".$contentBenTrans["benAgentID"]."' and transTo = '' and datedGot = (select MIN(datedGot) from ".TBL_REUSED_DIST_REF." where distributor = '".$contentBenTrans["benAgentID"]."' and transTo = '')");
						if($selectExistingQuery["id"] != ""){
							$datedAssign = getCountryTime(CONFIG_COUNTRY_CODE);
							$refNum = $selectExistingQuery["refNumber"];
							$flag = true;
							update("update ".TBL_REUSED_DIST_REF." set transTo = '".$benTransID."', datedAssign = '".$datedAssign."' where id = '".$selectExistingQuery["id"]."'");
							update("update ".TBL_TRANSACTIONS." set distRefNumber = '".$refNum."' where transID ='".$contentBenTrans["transID"]."'");
										
							}else{
						
									$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$contentBenTrans["benAgentID"]."'  and status='Active'  and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentBenTrans["benAgentID"]."' and status='Active' and rangeTo >= (rangeFrom + used))");
									$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
									if($refNumRange["id"] != ''){
										$value = $refNumRange["rangeFrom"] + $refNumRange["used"];
							
										$refNum = $refNumRange["prefix"].$value;
										$used = $refNumRange["used"];
										$used = ++$used;
										
										update("update ".TBL_RECEIPT_RANGE." set used = '".$used."' where agentID ='".$contentBenTrans["benAgentID"]."' and id ='".$refNumRange["id"]."' ");
										IF(CONFIG_DIST_MANUAL_REF_NUMBER != "1"){
										update("update ".TBL_TRANSACTIONS." set distRefNumber = '".$refNum."' where transID ='".$contentBenTrans["transID"]."'");
									}
										
										if($limitVal <= 10){
											$cond = selectFrom(" SELECT id from ".TBL_RECEIPT_RANGE." where agentID ='".$contentBenTrans["benAgentID"]."'  and status='Active'  and created > (SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentBenTrans["benAgentID"]."'  and status='Active'  and rangeTo >= (rangeFrom + used) ) ");
											if($cond["id"] == ''){
												
												include ("distEmail.php");
											}
										}
										$flag = true;
									}else{
										
										$flag = false;
										$refNum = "Transaction cannot be authorized. Add a new distributor reference number range. ";
										
									}
								}
						}else{
							$flag = true;  
							$refNum = $contentBenTrans["distRefNumber"];

			}
			
	 $dataReturn[] = $flag;
	 $dataReturn[] = $refNum;
	 return $dataReturn;
	}
	
function removeDistRef($refTransID, $reason)
{
	$savingDate = getCountryTime(CONFIG_COUNTRY_CODE);
	$transInfo = selectFrom("select transID, benAgentID, distRefNumber from ".TBL_TRANSACTIONS." where transID = '".$refTransID."'");
	
	 $saveRefQuery = "insert into ".TBL_REUSED_DIST_REF." (refNumber, distributor, transFrom, datedGot, reason) values
	('".$transInfo["distRefNumber"]."', '".$transInfo["benAgentID"]."', '".$transInfo["transID"]."', '".$savingDate."','".$reason."') ";
		insertInto($saveRefQuery);
	
	update("update ".TBL_TRANSACTIONS." set distRefNumber = '' where transID = '".$refTransID."'");
}

function addComplaints($cmpTrans, $cmpSubject, $cmpMessage)
{
	$cmpTransContent = selectFrom("select refNumberIM, refNumber, custAgentID, benAgentID from  ". TBL_TRANSACTIONS."  where transID='".$cmpTrans."'");
	$agentType = getAgentType();
	
	if($agentType == "TELLER")
	{
		$loginUserName = $_SESSION["loggedUserData"]["loginName"];
		$userDetails = selectFrom("select * from ".TBL_TELLER . " where loginName='".$loginUserName."'");
		$LogedUserID =  $userDetails['tellerID'];
		$LogedUserName =  $userDetails['name'];
		
		
	}
	else
	{
		$loginUserName = $_SESSION["loggedUserData"]["username"];
		$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='".$loginUserName."'");
		
		$LogedUserID =  $userDetails['userID'];
		$LogedUserName =  $userDetails['name'];
		
	}
		insertInto("insert into ".TBL_COMPLAINTS." (dated, transID, agent, subject, details,logedUserID,logedUserName)
		 VALUES ('".getCountryTime(CONFIG_COUNTRY_CODE)."', '". $cmpTrans."','".$loginUserName."','". $cmpSubject."','". $cmpMessage."','".$LogedUserID."','".$LogedUserName."')");
		$msg = "Enquiry added successfully";
		
		$cmpID = @mysql_insert_id();
		
		/////For History
		$descript = "complaint is added";
		activities($_SESSION["loginHistoryID"],"INSERTION",$cmpID,TBL_COMPLAINTS,$descript);
		
		/////
		$fromName = "$company Money Support";//SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		$subject = "New Enquiry Added";
		$message = "New Enquiry has been added in <b>".COMPANY_NAME."</b><br><br>
			<b>$systemCode</b>: ".$cmpTransContent["refNumberIM"]." <br>
			
		  <b>$manualCode</b>: ".$cmpTransContent["refNumber"]." <br>
		  
		  <b>Enquiry Subject</b>: ".$cmpSubject." <br>
		  
		  <b>Enquiry Detail</b>: ".$cmpMessage." <br><br>
		   
			Thank you for using <b>$company</b>";

		$toMail  = $_SESSION["loggedUserData"]["email"];
		$qEmail  = selectMultiRecords("select email from " . TBL_ADMIN_USERS . " where userID IN ('".$cmpTransContent["custAgentID"]."', '".$cmpTransContent["benAgentID"]."')");
		$toMail1 = $qEmail[0]['email'];
		$toMail2 = $qEmail[1]['email'];
		
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "To: <$toMail1>, <$toMail2>" . "\r\n";
		$headers .= "From: $fromName <$fromEmail>" . "\r\n";
		$headers .= "Cc: <".INQUIRY_EMAIL.">" . "\r\n";
		if(CONFIG_STOP_WHOLE_EMAIL_SYSTEM!="1")
			@mail($toMail, $subject, $message, $headers);	
		
		return $msg;
}

function showColor($transStatus){
	
	if($transStatus == 'Pending'){
		    $fontColor='#FF9933'; // Dark Orange Color
		}elseif($transStatus == 'Authorize'){
			  $fontColor='#006600'; // Dark Green Color
		}elseif($transStatus == 'Cancelled'){
				$fontColor='#E00D1C';  // Dark Red Color
	  }else{
	  	  $fontColor='#000000';  // By Default Color is Black
	  	}
	return $fontColor;
	}
	
	
	function updateExtraInfo($transIDExtraInfo)
	{
		$extraTransData = selectFrom("select benAgentID from ".TBL_TRANSACTIONS." where transID = '".$transIDExtraInfo."'");
		$distInfo = selectFrom("select username, userID, parentID, agentType from ".TBL_ADMIN_USERS." where userID = '".$extraTransData["benAgentID"]."'");////selecting Distributor
		
		$isSuperDist = true;
		if($distInfo["agentType"] == "Sub")
		{
			$isSuperDist = false;
			$distParentInfo = selectFrom("select username, userID, parentID, agentType from ".TBL_ADMIN_USERS." where userID = '".$distInfo["parentID"]."'");////selecting Distributor
		}
		
		if($isSuperDist)
		{
				$distSubRef = '0';
				$distRef = $distInfo["username"];
		}else{
			$distSubRef = $distInfo["username"];
			$distRef = $distParentInfo["username"];
			}
		
		/*
		If Distributor is SUPER Distributor, its userName will be distRef elsewe will fetch its parent and username of parent will be distRef
		*/
		
		/*
		if Distributor is Sub Distributor, its username will be in distSubRef other wise its 0
		*/
		$currentTransmissionRef = 1;
		$transMissionRefInfo = selectFrom("select MAX(transmissionRefCount) as lastTransmissionRef from ".TBL_TRANSACTION_EXTENDED." where benAgentID = '".$extraTransData["benAgentID"]."' and isExported = 'N'");
		if($transMissionRefInfo["lastTransmissionRef"] == '')
		{
			$transMissionRefInfo = selectFrom("select MAX(transmissionRefCount) as lastTransmissionRef from ".TBL_TRANSACTION_EXTENDED." where benAgentID = '".$extraTransData["benAgentID"]."' and isExported = 'Y'");
			if($currentTransmissionRef <= $transMissionRefInfo["lastTransmissionRef"])
			{
				$currentTransmissionRef = $transMissionRefInfo["lastTransmissionRef"];
			}
			$currentTransmissionRef++;
		}else{
				
				if($currentTransmissionRef <= $transMissionRefInfo["lastTransmissionRef"])
			{
			$currentTransmissionRef = $transMissionRefInfo["lastTransmissionRef"];
			}	
		}
		
		
		
		
		$transmissionefValue = $distInfo["username"]."-".$currentTransmissionRef;
		$refData = generateDistRef($transIDExtraInfo);
		$flagExport = $refData[0];
		$payeeRefValue = $refData[1];
		
		if(!$flagExport)
		{
				$agentRefData = selectFrom("select MAX(payeeRef) as payeeRef from ".TBL_TRANSACTION_EXTENDED." where 1");
				$payeeRefValue = $agentRefData["payeeRef"];
				if($payeeRefValue == '')
				{
					$payeeRefValue  = 0;	
				}
				$payeeRefValue ++;
			
		}
		
			
		
		/*
		For transmissionRef, select last entries in the table for this distributor and increment it and adjust with userName of Dist to be usinque, to be more procise in it, we will have a column in table with isExported Field, and that field should be Y for selection. query will be like, select (MAX) transmission ref from table where distributor is = trans[BenAgentID] and is exported in transactionExtended is Y,
		then increment in it and concatinate username with it, bothe with username and without username will be there.
		*/
		/*
			define("CONFIG_DIST_REF_NUMBER", "0");should be 1
			define("CONFIG_DIST_REF_NAME", "Distributor's Reference Number");
			define("CONFIG_GENERATE_DIST_REF_ON_EXPORT", "1");
			define("CONFIG_DIST_REF_ON_EXPORT","1");should be 0
			define("CONFIG_REUSE_DIST_REF_NUMBER", "1");
		if agent ref is given, get that one or generate from system, 
		*/
		/*
		if agentRef is provided, same will be clave other wise clave is 0
		*/
		$checkExisting = selectFrom("select id from ".TBL_TRANSACTION_EXTENDED." where transID = '".$transIDExtraInfo."'");
		if($checkExisting["id"] != "")
		{
			$updateQuery = "update ".TBL_TRANSACTION_EXTENDED." set 
			payeeRef = '".$payeeRefValue."',
			transmissionRef = '".$transmissionefValue."',
			distSubRef = '".$distSubRef."',
			benAgentID = '".$extraTransData["benAgentID"]."',
			transmissionRefCount = '".$currentTransmissionRef."',
			isExported = 'N',
			distRef = '".$distRef."'
			where id = '".$checkExisting["id"]."'
			";
			update($updateQuery);
		}else{
			$insertQuery = "insert into ".TBL_TRANSACTION_EXTENDED." (transID, payeeRef, transmissionRef, distSubRef, benAgentID, transmissionRefCount, distRef, isExported) values
				('".$transIDExtraInfo."', '".$payeeRefValue."', '".$transmissionefValue."', '".$distSubRef."','".$extraTransData["benAgentID"]."','".$currentTransmissionRef."', '".$distRef."', 'N')";
			insertInto($insertQuery);		
		}
		//TBL_TRANSACTION_EXTENDED
	}
	
	
	
	
function generateDistClave($benAgentID){
					
					$checkClaveUsage = selectFrom("select hasClave from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");
					if($checkClaveUsage["hasClave"] == "Y")
					{		
							
														
											$refNumRange = selectFrom("SELECT id, rangeFrom, rangeTo, prefix, used from ".TBL_CLAVE_RANGE." where agentID ='".$benAgentID."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_CLAVE_RANGE." where agentID ='".$benAgentID."' and rangeTo >= (rangeFrom + used) and status = 'Active') and status = 'Active' ");
											
										
											$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
											if($refNumRange["id"] != '' ){
												$value = $refNumRange["rangeFrom"] + $refNumRange["used"];
												$refNum = $refNumRange["prefix"].$value;
												$used = $refNumRange["used"];
												$used++ ;
												
												update("update ".TBL_CLAVE_RANGE." set used = '".$used."' where agentID ='".$benAgentID."' and id ='".$refNumRange["id"]."' ");
											
												if($limitVal <= 10){
													$cond = selectFrom(" SELECT id from ".TBL_CLAVE_RANGE." where agentID ='".$benAgentID."'  and status = 'Active' and created > (SELECT min(created) from ".TBL_CLAVE_RANGE." where agentID ='".$benAgentID."'  and status = 'Active'  and rangeTo >= (rangeFrom + used)) ");
													if($cond["id"] == ''){
														$rangeName = "Clave";
														$userID = $_SESSION["loggedUserData"]["userID"];
														include ("distEmail.php");
													}
												}
												$flag = true;
											}else{
												
												$flag = false;
												$refNum = "Transaction cannot be processed. Please add a new clave range. ";
												
											}
										
								
					}else{
								$flag = true;  
								$refNum = "";
						}	
	 $dataReturn[] = $flag;
	 $dataReturn[] = $refNum;
	 return $dataReturn;
	}
	
	function maskingOfAdminTypeValues($adminType,$agentType,$isCorrespondent)
	{
		if($adminType == 'Supper'){
			$value = "Super Admin";
		}elseif($adminType =='Call'){
			$value = "Call Center Staff";
		}elseif($adminType == 'Agent' && $agentType == 'Supper' && $isCorrespondent == 'N'){
			$value = "Super Agent";
		}elseif($adminType == 'Agent' && $agentType == 'Sub' && $isCorrespondent == 'N'){
			$value = "Sub Agent";
		}elseif($adminType == 'Agent' && $agentType == 'Supper' && $isCorrespondent == 'ONLY'){
			$value = "Super Distributor";
		}elseif($adminType == 'Agent' && $agentType == 'Sub'&& $isCorrespondent == 'ONLY'){
			$value = "Sub Distributor";
		}elseif($adminType == 'Agent' && $agentType == 'Supper' && $isCorrespondent == 'Y'){
			$value = "Super A&D";
		}elseif($adminType == 'Agent' && $agentType == 'Sub'&& $isCorrespondent == 'Y'){
			$value = "Sub A&D";
		}elseif($adminType =='Admin'){
			$value = "Basic User";
		}elseif($adminType =='Support'){
			$value = "Support Staff";
		}else{
			$value = $adminType;
		}
		return $value;
	}
	
function transMoneypaidStatusChange($transId, $newStatus)
{
	update("update " . TBL_TRANSACTIONS . " set moneyPaid= '".$newStatus."' where transID='".$transId."'");
	$descript = "Payment done by customer";
	activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
  $amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount,localAmount,benAgentID, AgentComm, IMFee, createdBy,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '".$transId."'");
	if(DEST_CURR_IN_ACC_STMNTS == "1"){
		$currencyFrom = $amount["currencyTo"];
  }else{		
  	$currencyFrom = $amount["currencyFrom"];
  }
  
	$commission = $amount["AgentComm"];
	$balance = $amount["totalAmount"];
	$agent = $amount["custAgentID"];	
	
	$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent."'");	
			
			if($amount["createdBy"] != 'CUSTOMER')
				{
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
							$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$amount["custAgentID"]."' ");
							$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
							
								if($agentPaymentMode == 'exclude')
								{
										$balance = $amount["totalAmount"] - $commission;
								
								}elseif($agentPaymentMode == 'include')
								{
									$balance = $amount["totalAmount"];
								}
	
							}else{
								$balance = $amount["totalAmount"];
							}
						
						
						if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1')
						{
							$amountToLedger = $amount["localAmount"];	
							$currecyToLedger = $amount["currencyTo"];
						}else{
							$amountToLedger = $balance;	
							$currecyToLedger = $amount["currencyFrom"];
						}	
					
						if($agentContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentContents["userID"], $balance, $transId, "DEPOSIT", $descript, "Agent",$currecyToLedger);
							updateAgentAccount($agentContents["parentID"], $amountToLedger, $transId, "DEPOSIT", $descript, "Agent",$note,$currecyToLedger);
							
						}else{
							updateAgentAccount($agentContents["userID"], $amountToLedger, $transId, "DEPOSIT", $descript, "Agent",$note,$currecyToLedger);
						}
							
						
						//$currentBalance = $balance + $currentBalance;
						//update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
						
						

					}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','".$descript."','DEPOSIT','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						 
				
						$newBalance = $payin["balance"] + $balance ; 
						$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
						update($update_Balance);
						
						if(CONFIG_PAYIN_CUST_AGENT == '1')
						{
							$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
							if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agent!=""){
								$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent."'";
							}
							$agentPayinContents = selectFrom($agentPayinQuery);
							if($agentPayinContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', $descript, "Agent",$currencyFrom,getCountryTime(CONFIG_COUNTRY_CODE));
								$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $transId, 'DEPOSIT', $descript, "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
							}else{
								$q = updateAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', $descript, "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
							}	
						}
					
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Payment Done','DEPOSIT','$balance','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					 if(CONFIG_ONLINE_AGENT == '1')
					{	
						
						$descript = "Payment done by customer";
						updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent',$note,$currencyFromD);	
					}	
				}
				
		 
	
	insertError("Payment done by customer");
	
}

/**
 * printLogoOnReciept function prints the logo on the confrim trasaction and on print transaction pages
 * If Agent has its own logo then its logo will be used otherwise the copany logo will be used to print
 * 
 * @param $customerAgentId Int		Id of the logge in user
 * @param $agentName 			 String The username of the agent
 * 
 * @return The full tage of the image including the path, height and width information
 *
 * @Ticket# 3370
 * @AddedBy Jahangir <jahangir.alam@horivert.co.uk>
 */
function printLogoOnReciept($customerAgentId, $agentName)
{
		
		if($customerAgentId !='')
		{
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where username ='" . $agentName . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
		} 
												
		if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
		{
				$arr = getimagesize( "../logos/" .$senderAgentContent["logo"]);
				
				$width = ($arr[0] > 200 ? 200 : $arr[0]);
				$height = ($arr[1] > 100 ? 100 : $arr[1]);
	
			return "<img src='../logos/".$senderAgentContent["logo"]."' width='$width' height='$height'>";
		}
		else
		{
			return "<img height='".CONFIG_LOGO_HEIGHT."' alt='' src='".CONFIG_LOGO."' width='".CONFIG_LOGO_WIDTH."'>";
		}
	
}

/**
 * printRecieptHeader function prints the Company Name and related contact info on print transaction pages
 * If the 'adminType' is Agent then its company information will be printed, otherwise the company contact info used to print
 * 
 * @param $adminType 			 String 	The type of the current user
 * 
 * @return Print the full header info in proper format
 *
 * @Ticket# 3371
 */
function printRecieptHeader($adminType)
{
  	//$arrAgent
	
	if($adminType == "Agent")
  	{
		echo "<strong><font size='3'>". strtoupper($_SESSION["loggedUserData"]["agentCompany"]) ."</font></strong>";
		echo "<br><font size='2'>Address:";
		if(CONFIG_POS_ADDRESS == '1')
		{ 
				echo $_SESSION["loggedUserData"]["agentAddress"]; 
				if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
					echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
				echo '<br />';
					echo $_SESSION["loggedUserData"]["agentCity"].", ".$_SESSION["loggedUserData"]["agentCountry"].'<br />';
				echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
				if(!empty($_SESSION["loggedUserData"]["agentFax"]))
					echo ' Fax: '.$_SESSION["loggedUserData"]["agentFax"];	
		}
		else
		{
				echo $_SESSION["loggedUserData"]["agentAddress"]; 
				if(!empty($_SESSION["loggedUserData"]["agentAddress2"]))
					echo ', '.$_SESSION["loggedUserData"]["agentAddress2"];
				echo '<br />';
				echo $_SESSION["loggedUserData"]["agentCity"].", ".$_SESSION["loggedUserData"]["agentCountry"].'<br />';
				echo 'Phone: '.$_SESSION["loggedUserData"]["agentPhone"];
		} 
		echo "</font>";
	}
	else
	{
		echo "<strong><font size='3'>". strtoupper($company." ".COMPANY_NAME_EXTENSION) ."</font></strong>";
		echo "<br><font size='2'>Address:";
		
		if(CONFIG_POS_ADDRESS == '1')
			echo COMPANY_ADDRESS."<br>". COMPANY_PHONE;
		else
			echo $printAddress." <br> Phone:".$printPhone;
	
		echo "</font>";
	}
}

function debug( $variableToDebug = "", $shouldExitAfterDebug = false )
{
	if ( defined("DEBUG_ON")
		 && DEBUG_ON == true )
	{
		$arrayFileDetails = debug_backtrace();
		echo "<pre style='background-color:orange; border:2px solid black; padding:5px; color:BLACK; font-size:13px;'>Debugging at line number <strong>" . $arrayFileDetails[0]["line"] . "</strong> in <strong>" . $arrayFileDetails[0]["file"] . "</strong>";
	
		if ( !empty($variableToDebug)  )
		{
			echo "<br /><font style='color:blue; font-weight:bold'><em>Value of the variable is:</em></font><br /><pre style='color:BLACK; font-size:13px;'>" . print_r( $variableToDebug, true ) . "</pre>";
		}
		else
		{
			echo "<br /><font style='color:RED; font-weight:bold;'><em>Variable is empty.</em></font>";
		}
		echo "</pre>";
		if ($shouldExitAfterDebug)
			exit();
	}
}

/**
 * Calculates which-ever-is-higher commission.
 */
function calculateWIHC($amount=0, $userId="", $collectionPointId="")
{
	$newCommission = array();
	$newCommission["TYPE"] = "";
	$newCommission["VALUE"] = 0;
	
	// Find Distributor's Commission in commission table.
	if(!empty($collectionPointId))
	{
		$cpidWhere = "collectionPointId = ".$collectionPointId;
	}
	
	if(!empty($userId))
	{
		$didWhere = "userId = ".$userId;
	}
	
	if(!empty($cpidWhere) && !empty($didWhere))
	{
		$sqlWhere = $didWhere." and ".$cpidWhere;
	} elseif(!empty($cpidWhere) && empty($didWhere)) {
		$sqlWhere = $cpidWhere;
	} else {
		$sqlWhere = $didWhere . " and collectionPointId = ''";
	}
	
	if(!empty($sqlWhere))
	{
		$sql = "select fixedValue, percentValue from commission where ".$sqlWhere;
		$result = mysql_query($sql);
		$numrows = mysql_num_rows($result);
		
		if($numrows > 0)
		{
			while($rs = mysql_fetch_array($result))
			{
				$fixedValue = $rs["fixedValue"];
				$percentValue = $rs["percentValue"];
			}
			
			$newCommission["VALUE"] = $fixedValue;
			$newCommission["TYPE"] = "Fixed";
			$tmpCommission = ($amount * $percentValue) / 100;
			
			if($tmpCommission > $fixedValue)
			{
				$newCommission["VALUE"] = $tmpCommission;
				$newCommission["TYPE"] = "Percent";
			}
		}
	}	
	
	return $newCommission;
}

/**
 * Finds settlement exchange rate based on settlement currency of supplied user.
 * If not found then finds Generic rate for supplied settlement and destination currency.
 */
function getSettlementExchangeRate($userId="", $destinationCurrency="")
{
	$settlementExchangeRate = 0;
	
	if(!empty($userId) && !empty($destinationCurrency))
	{
		// Get settlementCurrency for supplied user
		$sql = "SELECT 
					settlementCurrency 
				FROM 
					admin 
				WHERE 
					userID=".$userId
				;
		$result = mysql_query($sql);
		$numrows = mysql_num_rows($result);
		
		if($numrows > 0)
		{
			while($rs = mysql_fetch_array($result))
			{
				$settlementCurrency = $rs["settlementCurrency"];
			}
			
			if(!empty($settlementCurrency))
			{
				// Find distributor and Settlement Currency -> Destination Currency based rate.
				$sql = "SELECT 
							(primaryExchange - (primaryExchange * marginPercentage / 100)) AS netExchange 
						FROM 
							exchangerate 
						WHERE 
							LOWER(rateFor) = 'distributor' 
							AND rateValue=".$userId." 
							AND currencyOrigin = '".$settlementCurrency."'
							AND currency = '".$destinationCurrency."'
						ORDER BY 
							erID DESC 
						LIMIT 
							0,1
						";
				$result = mysql_query($sql);
				$numrows = mysql_num_rows($result);
				
				if($numrows > 0)
				{
					while($rs = mysql_fetch_array($result))
					{
						$settlementExchangeRate = $rs["netExchange"];
					}
				} else {
					// If distributor based rate not found then,
					// Find generic and Settlement Currency -> Destination Currency based rate.
					$sql = "SELECT 
								(primaryExchange - (primaryExchange * marginPercentage / 100)) AS netExchange 
							FROM 
								exchangerate 
							WHERE 
								LOWER(rateFor) = 'generic' 
								AND currencyOrigin = '".$settlementCurrency."'
								AND currency = '".$destinationCurrency."'
							ORDER BY 
								erID DESC 
							LIMIT 
								0,1
							";
					$result = mysql_query($sql);
					$numrows = mysql_num_rows($result);
					
					if($numrows > 0)
					{
						while($rs = mysql_fetch_array($result))
						{
							$settlementExchangeRate = $rs["netExchange"];
						}
					}
				}
			}
		}
	}
	
	return $settlementExchangeRate;
}

/**
 * To display the currency signs
 * @Ticket #4178
 *
 * @param string $strCurrencyCode 	currency to which currency sign is required
 * @return string currency 			the currency sigh
 */
function getCurrencySign($strCurrencyCode)
{
	if(CONFIG_CURRENCY_SIGN_ENABLE == "1")
	{
		$arrCurrencySign = array(
                	                        "USD" => "$",
                                                "GBP" => "&pound;",
                                                "NGN" => "&#8358;",
                                                "CAD" => "Ca $"
                                                );

		if(isset($arrCurrencySign[$strCurrencyCode]))
			return $arrCurrencySign[$strCurrencyCode];
		else
			return $strCurrencyCode;
	}
	else
		return $strCurrencyCode;
}

function customNumberFormat($floatNumber,$intDecimalPnt=2, $bolIsWithoutRound=false)
{
	// this function is used to format and round on basis of provided paramters.
	$chrSeparator = ".";
	if($bolIsWithoutRound){
		$arrNumberFlt = explode($chrSeparator,$floatNumber);
		$arrNumberFlt[1]=substr($arrNumberFlt[1], 0,$intDecimalPnt);
		$fltReturnNumber = number_format($arrNumberFlt[0]).$chrSeparator.$arrNumberFlt[1];
	}
	else{
		$fltReturnNumber = number_format($floatNumber,$intDecimalPnt,$chrSeparator,',');
	}
	return $fltReturnNumber;
}
/* #4634 added to get Agent or Distributor from AnD.
   added by ashahid
*/
function getAnDActAsLabel($agentID="",$currency=""){
		$agentID2actAs = selectFrom("select settlementCurrencies from ".TBL_ADMIN_USERS." where userID='".$agentID."'");
		if($agentID2actAs["settlementCurrencies"]!=""){
			$actAsArr = explode("|",$agentID2actAs["settlementCurrencies"]);
			$actAsArrAgent 		 = $actAsArr[0];
			$actAsArrDistributor = $actAsArr[1];
			if($currency==$actAsArrAgent)
				$actAsString = "Agent";
			else if($currency==$actAsArrDistributor)					
				$actAsString = "Distributor";
			
		}
		else {
				$actAsString = "";
		}
		return $actAsString;
}

function maintainAdminStaffAccount($intUserId, $fltAmount, $intTransId, $strType, $strDescription, $strNote, $strCurrency, $strDate)
{
	//debug_print_backtrace();

	$arrAdminStaffData = selectFrom("select balance,adminType from ".TBL_ADMIN_USERS." where userID = '".$intUserId."'");
	$adminStaffs = array('Admin','Call','COLLECTOR','Branch Manager','Admin Manager','Support','SUPI Manager','MLRO');
	if(in_array($arrAdminStaffData["adminType"],$adminStaffs)){
		$loginID  = $_SESSION["loggedUserData"]["userID"];
	  
		if(CONFIG_TRANS_ROUND_LEVEL != "")
			$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
		else
			$roundLevel = 4;
	 
		$fltAmount = round($fltAmount, $roundLevel);
	 
		$strAccountSql = "insert into ".TBL_AGENT_ACCOUNT." 
						(agentID, 	  dated,    type,     amount,    modified_by,   TransID,       description,    note,      currency) 
				values  ('$intUserId', '$strDate', '$strType', '$fltAmount', '$loginID', '$intTransId', '$strDescription', '$strNote', '$strCurrency')";	
		//debug($strAccountSql);
		$bolNewAccountEntry = insertInto($strAccountSql);
		
		if($bolNewAccountEntry)
		{
			$currentBalance = $arrAdminStaffData["balance"];
			if($type == 'DEPOSIT')
				$currentBalance += $fltAmount;
			else
				$currentBalance -= $fltAmount;
		
			update("update " .TBL_ADMIN_USERS. " set balance = $currentBalance where userID = '".$intUserId."'");											
			/*****
				#5258 - Minas Center
						Admin Account Summary is maintained
						by Aslam Shahid
			*****/
			adminAccountSummary($loginID,$strType, $fltAmount,$strCurrency);	
			
		}
	}
	return $bolNewAccountEntry;		
}

function updateAdminStaffLedger($intTransaction, $strType, $strDescription, $strNote, $strCurrency)
{
	//debug_print_backtrace();
	
	$arrTransactionData = selectFrom("select addedBy, transAmount from transactions where transID = '$intTransaction'");
	
	$arrUserData = selectFrom("select userID, adminType from admin where username = '".$arrTransactionData["addedBy"]."'");
	$userType = $arrUserData["adminType"];
	
	
	if($userType == 'Admin' || 
	   $userType == 'Call' || 
	   $userType == 'COLLECTOR' || 
	   $userType == 'Branch Manager' || 
	   $userType == 'Admin Manager' || 
	   $userType == 'Support' || 
	   $userType == 'SUPI Manager' || 
	   $userType == 'MLRO'
	  ){
			
			$arrAccountData = selectFrom("select count(aaID) as c from agent_account where agentID = '".$arrUserData["userID"]."' and TransID = '".$intTransaction."'");
			
			if($arrAccountData["c"] > 0)
			{
				$arrAmountToCancel = selectFrom("select amount from agent_account where agentID = '".$arrUserData["userID"]."' and TransID = '".$intTransaction."'");
				
				$strDate = date("Y-m-d");
				maintainAdminStaffAccount($arrUserData["userID"], $arrAmountToCancel["amount"], $intTransaction, $strType, $strDescription, $strNote, $strCurrency, $strDate);
			}
			  	
	  }
	
}

function paypointSummaryAccount($userID,$amount,$serviceID,$categoryID,$type){
	$accountSumTable = "account_summary_paypoint";
	$today = date("Y-m-d"); 
	$agentQuery = "select id, user_id, dated, opening_balance, closing_balance from " . $accountSumTable . " where user_id = '$userID' and dated = '$today' ";
	if($serviceID!="")
		$agentQuery .= " AND serviceID ='".$serviceID."'";
	if($categoryID!="")
		$agentQuery .= " AND categoryID ='".$categoryID."'";
	$agentContents = selectFrom($agentQuery."order by id desc");
	if($agentContents["user_id"] != "")
	{
		$balance = $agentContents["closing_balance"];
/*		if($type == 'DEPOSIT') // Dr
		{
			$balance += $amount;
		}
		else // WITHDRAW(Cr)
		{
			$balance -= $amount;
		}*/
		// This is CREDIT(WITHDRAW) Amount as DEBIT(DEPOSIT) is cash in Hand for Company(Agents).
		$balance = $balance + $amount;	
		update("update ".$accountSumTable." set closing_balance = '$balance' where id = '".$agentContents["id"]."'");			
	}else{
		$dateLastQ = "select  Max(dated) as dated,id from " . $accountSumTable . " where user_id = '$userID' ";
		if($serviceID!="")
			$dateLastQ .= " AND serviceID ='".$serviceID."'";
		if($categoryID!="")
			$dateLastQ .= " AND categoryID ='".$categoryID."'";
		$datesLast = selectFrom($dateLastQ." GROUP BY id ORDER BY id desc");
		$agentQuery = "select user_id, dated, opening_balance, closing_balance  from " . $accountSumTable . " where user_id = '$userID' and id = '".$datesLast["id"]."' ";
		$agentContents = selectFrom($agentQuery);
		
		$balance = 0;
		$balance += $agentContents["closing_balance"];
		$openingBalance = $balance;
		
		// This is CREDIT(WITHDRAW) Amount as DEBIT(DEPOSIT) is cash in Hand for Company(Agents).
		$closingBalance = $balance + $amount;	
		insertInto("insert into ".$accountSumTable." (dated,user_id,serviceID,categoryID,opening_balance,closing_balance) values('$today','$userID','$serviceID','$categoryID','$openingBalance','$closingBalance')");
	}	
}

function adminAccountSummary($adminID, $type, $amount,$currency="")
{
	/***
		#5258 - Minas Center
				Admin Staff account summary is affected like for INTER-ADMIN-FUNDS-TRANSFER etc.
				by Aslam Shahid
	***/
	 $today = date("Y-m-d"); 
	    	
  	    	$agentContents = selectFrom("select id, user_id, dated, opening_balance, closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$adminID' and dated = '$today' ");
  	    	
		if($agentContents["user_id"] != "")
		{
			$balance = $agentContents["closing_balance"];
						if($type == 'DEPOSIT')
			{
				$balance = $balance + $amount;	
				
			}else{
				$balance = $balance - $amount;	
				
			}
	      update("update ".TBL_ACCOUNT_SUMMARY." set closing_balance = '$balance',currency = '$currency' where id = '".$agentContents["id"]."'");			
		 
	}else{
          $datesLast = selectFrom("select  Max(dated) as dated from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$adminID' ");	
        $agentContents = selectFrom("select user_id, dated, opening_balance, closing_balance,currency  from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$adminID' and dated = '".$datesLast["dated"]."'");

			$balance = 0;
				
			$balance += $agentContents["closing_balance"];
		 			
			$openingBalance = $balance;
		  		
			if($type == 'DEPOSIT')
			{
				$closingBalance = $balance + $amount;	
				
			}else{
				$closingBalance = $balance - $amount;	
				
			}
	  	    insertInto("insert into ".TBL_ACCOUNT_SUMMARY." (dated, user_id, opening_balance, closing_balance) values('$today', '$adminID', '$openingBalance', '$closingBalance')");
		}
		
}
function isAnD($aId)
{
	$query = "select userID from ".TBL_ADMIN_USERS." where userID = '" . $aId . "' and adminType = 'Agent' and
isCorrespondent = 'Y'";

	$queryReturn = selectFrom( $query );
	if ( empty( $queryReturn ) )
		return false;

	return true;
}
function sendCustomMail($transId){

	// Mail is sent on Status update of Transaction. b Aslam Shahid	
		if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1" || CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			$transQuery = "SELECT	customerID,benID,custAgentID,benAgentID,refNumberIM,refNumber,collectionPointID,
									transType,transDate,currencyFrom,currencyTo,exchangeRate,transAmount,IMFee,
									transactionPurpose,moneyPaid,cashCharges,authoriseDate,createdBy,transStatus
							FROM ".TBL_TRANSACTIONS." 
							WHERE transID ='".$transId."'";
			$transRS = selectFrom($transQuery);
			$newStatus = $transRS["transStatus"];
			$clientCompany = COMPANY_NAME;
			$systemPre = SYSTEM_PRE;
		
			$custID 	= $transRS["customerID"];
			$benID 		= $transRS["benID"];
			$custAgentID = $transRS["custAgentID"];
			$benAgentID = $transRS["benAgentID"];
			$imReferenceNumber = $transRS["refNumberIM"];
			$manualRefrenceNumber = $transRS["refNumber"];
			$collectionPointID = $transRS["collectionPointID"];
			$type  = $transRS["transType"];
			$transDate   = $transRS["transDate"];
			if($transDate!=""){
				$transDate = date("F j, Y", strtotime($transDate));
			}
			$currencyFrom 	= $transRS["currencyFrom"];
			$currencyTo 	= $transRS["currencyTo"];
			
			///Exchange rate used 
			$exRate = $transRS["exchangeRate"];
		
			$transAmount = $transRS["transAmount"];
			$fee = $transRS["IMFee"];
			$localamount =($transAmount * $exRate);
			$purpose = $transRS["transactionPurpose"];
			$totalamount =  $transAmount + $fee;
			$moneypaid = $transRS["moneyPaid"];
			if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
			{
				$cashCharges = $transRS["cashCharges"];
				$totalamount =  $totalamount +$cashCharges;
			}
			if($transRS["authoriseDate"]!=""){
				$strAuthoriseDate = date("F j, Y", strtotime($transRS["authoriseDate"]));
			}
			 
			$fromName = SUPPORT_NAME;
			$fromEmail = SUPPORT_EMAIL;
			// getting Admin Email address
			$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
			$adminEmail = $adminContents["email"];
			// Getting customer's Agent Email
			if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
			{
			$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
			$custAgentEmail	= $agentContents["email"];
			}
			// getting BEneficiray agent Email
			if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
			{
			$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
			$benAgentEmail	= $agentContents["email"];
			}
			// getting beneficiary email
			$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
			$benEmail = $benContents["email"];
			// Getting Customer Email
			$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
			$custEmail = $custContents["email"];
			
			if($transRS["createdBy"] == 'CUSTOMER')
			{
			
						$subject = "Status Updated";
			
						$custContents = selectFrom("select c_name,Title,FirstName,MiddleName,LastName,c_country,c_city,c_zip,username,c_email,c_phone from cm_customer where c_id= '". $custID ."'");
						$custEmail = $custContents["c_name"];
						$custTitle = $custContents["Title"];
						$custFirstName = $custContents["FirstName"];
						$custMiddleName = $custContents["MiddleName"];
						$custLastName = $custContents["LastName"];
						$custCountry = $custContents["c_country"];
						$custCity = $custContents["c_city"];
						$custZip = $custContents["c_zip"];
						$custLoginName = $custContents["username"];
						$custEmail = $custContents["c_email"];
						$custPhone = $custContents["c_phone"];
			
			
						$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from cm_beneficiary where benID= '". $benID ."'");
						$benTitle = $benContents["Title"];
						$benFirstName = $benContents["firstName"];
						$benMiddleName = $benContents["middleName"];
						$benLastName = $benContents["lastName"];
						$benAddress  = $benContents["Address"];
						$benCountry = $benContents["Country"];
						$benCity = $benContents["City"];
						$benZip = $benContents["Zip"];
						$benEmail = $benContents["email"];
						$benPhone = $benContents["Phone"];
			
			
			/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
						$AdmindLoginName = $AdmindContents["username"];
						$AdmindName = $AdmindContents["name"];
						$AdmindEmail = $AdmindContents["email"];*/
			
			/***********************************************************/
			$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
			  <tr>
				<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
			  </tr>";
						
			$messageCust =     "				
								  <tr>
									<td colspan='2'><p><strong>Dear</strong>  ".$custTitle." ".$custFirstName." ".$custLastName."  </p></td>
								  </tr>				
						
											  <tr>
									<td width='205'>&nbsp;</td>
									<td width='295'>&nbsp;</td>
								  </tr>
										  <tr>
									<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
								  </tr>			
						
						
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Transaction Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
									
						
								  <tr>
									<td> Transaction Type:  ".$type." </td>
									<td> Status: ".$newStatus." </td>
								  </tr>
								  <tr>
									<td> Transaction No:   ".$imReferenceNumber." </td>
									<td> Transaction Date:  ".$transDate."  </td>
								  </tr>
								  <tr>
									<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
									<td>&nbsp; </td>
								  </tr>
								  <tr>
									<td><p>Dated: ".date("F j, Y")."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								
								
								  <tr>
									<td><p><strong>Beneficiary Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Address:  ".$benAddress." </td>
									<td> Postal / Zip Code:  ".$benZip." </td>
								  </tr>
								  <tr>
									<td> Country:   ".$benCountry."   </td>
									<td> Phone:   ".$benPhone."   </td>
								  </tr>
								  <tr>
									<td><p>Email:  ".$benEmail."   </p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>";
			$messageBen =     "				
								  <tr>
									<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
								  </tr>				
						
											  <tr>
									<td width='205'>&nbsp;</td>
									<td width='295'>&nbsp;</td>
								  </tr>
										  <tr>
									<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
								  </tr>			
						
						
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Transaction Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
									
						
								  <tr>
									<td> Transaction Type:  ".$type." </td>
									<td> Status: ".$newStatus." </td>
								  </tr>
								  <tr>
									<td> Transaction No:   ".$imReferenceNumber." </td>
									<td> Transaction Date:  ".$transDate."  </td>
								  </tr>
								  <tr>
									<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
									<td>&nbsp; </td>
								  </tr>
								  <tr>
									<td><p>Dated: ".date("F j, Y")."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								
								
								  <tr>
									<td><p><strong>Sender Detail: </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td colspan='2'>-----------------------------------------------------------------------------------</td>
								  </tr>
								  <tr>
									<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Country:   ".$custCountry."   </td>
									<td> Phone:   ".$custPhone."   </td>
								  </tr>
								  <tr>
									<td><p>Email:  ".$custEmail."   </p></td>
									<td> Postal / Zip Code:  ".$custZip." </td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>";			
						if(trim($type) == 'Bank Transfer')
						{
							$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benContents["Country"]."'");
			
							$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
							$rstRow = mysql_fetch_array($nResult);
			
			
							if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
							{
								$message .= "	<tr>
													<td>IBAN</td>
													<td>" . $rstRow["IBAN"] . "</td>
												</tr>";
							}
							else
							{
								$bankNameV = $rstRow["bankName"];
								$branchCodeV = $rstRow["branchCode"];
								$branchAddressV = $rstRow["branchAddress"];
								$swiftCodeV = $rstRow["swiftCode"];
								$accNoV = $rstRow["accNo"];
								$branchCityV = $rstRow["BranchCity"];
								
								$messageRemain="	<tr>
												<td><p><strong>Beneficiary Bank Details </strong></p></td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td> Bank Name:  ".$bankNameV."  </td>
												<td> Acc Number:  ".$accNoV."  </td>
											</tr>
											<tr>
												<td> Branch Code:  ".$branchCodeV."  </td>
												<td> Branch Address:  ".$branchAddressV.", ".$branchCityV."  </td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>";
							}
						}
						elseif(trim($type) == "Pick up")
						{
						
						$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone, from cm_collection_point where cp_id  = '". $collectionPointID ."'");
						$agentname = $cContents["cp_corresspondent_name"];
						$contactperson = $cContents["cp_corresspondent_name"];
						$company = $cContents["cp_branch_name"];
						$address = $cContents["cp_branch_address"];
						$country = $cContents["cp_country"];
						$city = $cContents["cp_city"];
						$phone = $cContents["cp_phone"];
						$tran_date = date("Y-m-d");
						$tran_date = date("F j, Y", strtotime("$tran_date"));
						
								$messageRemain = "
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td><p><strong>Collection Point Details </strong></p></td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td> Agent Name : ".$agentname." </td>
									<td> Contact Person:  ".$contactperson." </td>
								  </tr>
								  <tr>
									<td> Company:  ".$company." </td>
									<td> Address:  ".$address." </td>
								  </tr>
								  <tr>
									<td>Country:   ".$country."</td>
									<td>City:  ".$city."</td>
								  </tr>
								  <tr>
									<td>Phone:  ".$phone."</td>
									<td>&nbsp;</td>
								  </tr> ";
			}
			
			/*if($amount > 500)
			{
				$tempAmount = ($amount - 500);
				$nExtraCharges =  (($tempAmount * 0.50)/100);
				$charges = ($fee + $amount + $nExtraCharges);
			}*/
				
			/*if($trnsid[$i] != "")
			{
			 update("update transactions set IMFee = '$fee',
															 totalAmount  = '$totalamount',
															 localAmount = '$localamount',
															 exchangeRate  = '$exRate'
																 where
																 transID  = '". $trnsid[$i] ."'");
			}*/
			
			$messageEnd ="
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Amount Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Exchange Rate:  ".$exRate."</td>
				<td> Amount:  ".$transAmount." </td>
			  </tr>
			  <tr>
				<td> ".$systemPre." Fee:  ".$fee." </td>
				<td> Local Amount:  ".$localamount." </td>
			  </tr>  <tr>
				<td> Transaction Purpose:  ".$purpose." </td>
				<td> Total Amount:  ".$totalamount." </td>
			  </tr>  <tr>
				<td> Money Paid:  ".$moneypaid." </td>
				<td> Bank Charges:  ".$nExtraCharges." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
			";
			
			}else{
			
			
						$subject = "Status Updated";
			
						$custContents = selectFrom("select Title,firstName,middleName,lastName,Country,City,Zip,email,Zip,Phone,customerStatus from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
						$custTitle = $custContents["Title"];
						$custFirstName = $custContents["firstName"];
						$custMiddleName = $custContents["middleName"];
						$custLastName = $custContents["lastName"];
						$custCountry = $custContents["Country"];
						$custCity = $custContents["City"];
						$custZip = $custContents["Zip"];
						$custEmail = $custContents["email"];
						$custPhone = $custContents["Phone"];
						$custStatus = $custContents["customerStatus"];
			
			
						$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
						$benTitle = $benContents["Title"];
						$benFirstName = $benContents["firstName"];
						$benMiddleName = $benContents["middleName"];
						$benLastName = $benContents["lastName"];
						$benAddress  = $benContents["Address"];
						$benCountry = $benContents["Country"];
						$benCity = $benContents["City"];
						$benZip = $benContents["Zip"];
						$benEmail = $benContents["email"];
						$benPhone = $benContents["Phone"];
			
			
			/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
						$AdmindLoginName = $AdmindContents["username"];
						$AdmindName = $AdmindContents["name"];
						$AdmindEmail = $AdmindContents["email"];*/
			
			/***********************************************************/
			$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
			  <tr>
				<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
			  </tr>";
			 $messageCust =" 
			  <tr>
				<td colspan='2'><p><strong>Dear</strong>  ".$custTitle."&nbsp;".$custFirstName."&nbsp;".$custLastName ." </p></td>
			  </tr>
			  <tr>
				<td width='205'>&nbsp;</td>
				<td width='295'>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Transaction Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			
			  <tr>
				<td> Transaction Type:  ".$type." </td>
				<td> Status: ".$newStatus." </td>
			  </tr>
			  <tr>
				<td> Transaction No:   .".$imReferenceNumber." </td>
				<td> Transaction Date:  ".$transDate."  </td>
			  </tr>
				<tr>
				<td> Manual Reference No:   ".$strManualReferenceNumber ."</td> 
				<td>&nbsp;  </td>
			  </tr>
			  <tr>
				<td><p>Dated: ".date("F j, Y")."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			
			
			  <tr>
				<td><p><strong>Beneficiary Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Address:  ".$benAddress." </td>
				<td> Postal / Zip Code:  ".$benZip." </td>
			  </tr>
			  <tr>
				<td> Country:   ".$benCountry."   </td>
				<td> Phone:   ".$benPhone."   </td>
			  </tr>
			  <tr>
				<td><p>Email:  ".$benEmail."   </p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";
	
		$messageBen ="				
				  <tr>
					<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
				  </tr>				
		
							  <tr>
					<td width='205'>&nbsp;</td>
					<td width='295'>&nbsp;</td>
				  </tr>
						  <tr>
					<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
				  </tr>			
		
		
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td><p><strong>Transaction Detail: </strong></p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
					
		
				  <tr>
					<td> Transaction Type:  ".$type." </td>
					<td> Status: ".$newStatus." </td>
				  </tr>
				  <tr>
					<td> Transaction No:   ".$imReferenceNumber." </td>
					<td> Transaction Date:  ".$transDate."  </td>
				  </tr>
				  <tr>
					<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
					<td>&nbsp; </td>
				  </tr>
				  <tr>
					<td><p>Dated: ".date("F j, Y")."</p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				
				
				  <tr>
					<td><p><strong>Sender Detail: </strong></p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan='2'>-----------------------------------------------------------------------------------</td>
				  </tr>
				  <tr>
					<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td> Country:   ".$custCountry."   </td>
					<td> Phone:   ".$custPhone."   </td>
				  </tr>
				  <tr>
					<td><p>Email:  ".$custEmail."   </p></td>
					<td> Postal / Zip Code:  ".$custZip." </td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>";	
			if(trim($type) == 'Bank Transfer')
			{
				$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benCountry."'");
			
				$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
				$rstRow = mysql_fetch_array($nResult);
			
				if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
				{
					$messageRemain .= "	<tr>
										<td>IBAN</td>
										<td>" . $rstRow["IBAN"] . "</td>
									</tr>";
				}
				else
				{
					$bankNameV = $rstRow["bankName"];
					$branchCodeV = $rstRow["branchCode"];
					$branchAddressV = $rstRow["branchAddress"];
					$swiftCodeV = $rstRow["swiftCode"];
					$accNoV = $rstRow["accNo"];
			
					$messageRemain="  <tr>
								<td><p><strong>Beneficiary Bank Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Bank Name:  ".$bankNameV."  </td>
								<td> Acc Number:  ".$branchCodeV."  </td>
							  </tr>
							  <tr>
								<td> Branch Code:  ".$branchAddressV."  </td>
								<td> Branch Address:  ".$swiftCodeV."  </td>
							  </tr>
							  <tr>
								<td><p>Swift Code:  ".$accNoV."  </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							";
				}
			}
			elseif(trim($type) == "Pick up")
			{
			
			$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone from cm_collection_point where cp_id  = '". $collectionPointID ."'");
			$agentname = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			
			$messageRemain = "
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Collection Point Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Agent Name : ".$agentname." </td>
				<td> Contact Person:  ".$contactperson." </td>
			  </tr>
			  <tr>
				<td> Company:  ".$company." </td>
				<td> Address:  ".$address." </td>
			  </tr>
			  <tr>
				<td>Country:   ".$country."</td>
				<td>City:  ".$city."</td>
			  </tr>
			  <tr>
				<td>Phone:  ".$phone."</td>
				<td>&nbsp;</td>
			  </tr> ";
			
			}
			/*if($amount > 500)
			{
				$tempAmount = ($amount - 500);
				$nExtraCharges =  (($tempAmount * 0.50)/100);
				$charges = ($fee + $amount + $nExtraCharges);
			}*/
			
			
			//Distributor Commition logic
			
			$benAgentID = $transRS["benAgentID"];
			
			$packageQuery="select agentCommission,commPackage from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
						$agentPackage = selectFrom($packageQuery);
			
						$package = $agentPackage["commPackage"];
						
						//
						switch ($package)
							  {
								case "001": // Fixed amount per transaction
								{
									$agentCommi = $agentPackage["agentCommission"];
									$commType = "Fixed Amount";
									break;
								}
								case "002": // percentage of total transaction amount
								{
									$agentCommi = ( $transAmount* $agentPackage["agentCommission"]) / 100;
									$commType = "Percentage of total Amount";
									break;
								}
								case "003": // percentage of IMFEE
								{
									$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
									$commType = "Percentage of Fee";
									break;
								}
								case "004":
								{
									$agentCommi = 0;
									$commType = "None";
									break;
								}
							}				  
							   
						//
					 $agentCommi;
			
			
			// end
			
			
			
			
			/*if($trnsid[$i] != "")
			{
			 update("update transactions set IMFee = '$fee',
															 totalAmount 	 = '$totalamount',
															 localAmount 	 = '$localamount',
															 exchangeRate 	 = '$exRate',
															 distributorComm = '$agentCommi'
																 where
																 transID  = '". $trnsid[$i] ."'");
			}*/
	  $messageEnd="
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Amount Details </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Exchange Rate:  ".$exRate."</td>
				<td> Amount:  ".$transAmount." </td>
			  </tr>
			  <tr>
				<td> ".$systemPre." Fee:  ".$fee." </td>
				<td> Local Amount:  ".$localamount." </td>
			  </tr>  <tr>
				<td> Transaction Purpose:  ".$purpose." </td>
				<td> Total Amount:  ".$totalamount." </td>
			  </tr>  <tr>
				<td> Money Paid:  ".$moneypaid." </td>
				<td> Bank Charges:  ".$nExtraCharges." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			<tr>
				<td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
				
			  </tr>
			</table>
			";
			}
			if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
				sendMail($custEmail, $subject, $message.$messageCust.$messageRemain.$messageEnd, $fromName, $fromEmail);
			}
			if(CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
				sendMail($benEmail, $subject, $message.$messageBen.$messageRemain.$messageEnd, $fromName, $fromEmail);
			}
		}	
}
	function calculateCurrencyExchangeFee($type, $currencyF='', $currencyT='')
	{
		// query to look for company based fee having the amount range also
		//debug_print_backtrace();
		$currencyF_Q = "select currencyName from ".TBL_CURRENCY." WHERE cID='".$currencyF."'";
		$currencyT_Q = "select currencyName from ".TBL_CURRENCY." WHERE cID='".$currencyT."'";
		$strFeeSql[] = "select 
							fee_id 
					  from 
					  		currency_exchange_fee
					  where
					  		type = 'F' and
							currencyF IN(".$currencyF_Q.") and 
							currencyT IN(".$currencyT_Q.") 
					 order by
					 		updated_on desc limit 1
						";
						
		/* query to look for company based fee without the specific range */
		$strFeeSql[] = "select 
							fee_id 
				  	from 
					  		currency_exchange_fee 
					  where
					  		type = 'P' and
							currencyF IN(".$currencyF_Q.") and 
							currencyT IN(".$currencyT_Q.") 
					 order by 
					 		updated_on desc limit 1
						";
		
		//debug($strFeeSql);
		for($i=0; $i<count($strFeeSql);$i++)
		{
			$intReturnFeeId = checkCurrencyFeeRecord($strFeeSql[$i]);
			if($intReturnFeeId)
				return $intReturnFeeId;
		}		
		return false;
	}

	function checkCurrencyFeeRecord($strSql)
	{
		//debug($strSql);
		$arrCheckResultSet = selectFrom($strSql);
		//debug($arrCheckResultSet);
		if(!empty($arrCheckResultSet["fee_id"]))
			return $arrCheckResultSet["fee_id"];
		else
			return false;
	}
	
	function __($string, $jsSafe = false)
	{
       	global $_arrLabels;
		if (isset($_arrLabels[$string])){
		return $_arrLabels[$string];
		}
		
		else
		{
			return $string;
		}
		//global $lang;
        //return $lang->_($string, $jsSafe);
}
function gateway($name, $arguments=false, $value=false)
{
	//debug_print_backtrace();
	//while($record = mysql_fetch_assoc($result))
	$returnValue = '';
	
	if (is_callable($name)) {
	    
	    $ret = call_user_func($name, $arguments, $value);
	   
	   // $ret = call_user_func($cb3, $arg1, $arg2);
	  return $ret;  
	}else{
			echo("Call to undefined function as ".$record[$i]["name"]);
		}
		
	return $returnValue;	
}

function CONFIG_POPULATE_USERS_DROPDOWN($arguments,$values="0"){
	$selectOption = "";
	$customInClause = "";
	
	if(!empty($arguments["isCorrespondent"])){ // "Admin ,SUPA,"
	 	$isCorrArr = explode(",",$arguments["isCorrespondent"]); // array('Admin ','SUPA');
		array_walk($isCorrArr, 'trim_value'); // array('Admin','SUPA');
		$isCorrStr = implode("','",$isCorrArr); // 'Admin','SUPA';
		$customCorrespondenceClause = $isCorrStr;
	}
	if(!empty($arguments["userTypes"])){ // "Admin ,SUPA,"
	 	$userTypesArr = explode(",",$arguments["userTypes"]); // array('Admin ','SUPA');
		array_walk($userTypesArr, 'trim_value'); // array('Admin','SUPA');
		$userTypes = "'".implode("','",$userTypesArr)."'"; // 'Admin','SUPA';
		$customInClause = $userTypes;
	}
	elseif(defined("CONFIG_POPULATE_ADMINSTAFF_DROPDOWN") && CONFIG_POPULATE_ADMINSTAFF_DROPDOWN!=""){
		$customInClause = CONFIG_POPULATE_ADMINSTAFF_DROPDOWN;
	}
	else{
		 $customInClause = "'Supper', 'Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM'";
	}
	if(!empty($customCorrespondenceClause)){
		$customCorrespondenceClause = " AND upper(isCorrespondent) IN('".strtoupper($customCorrespondenceClause)."') ";
	}
	if($values == "1"){
		if($arguments["flag"]=="populateUsers"){
			$selectedID = $arguments["selectedID"];			
		
			$qry = "select 
							userID,
							username, 
							name, 
							agentCountry, 
							adminType,
							isMain 
					from 
						".TBL_ADMIN_USERS." 
					where 
						adminType IN (".$customInClause.")
						AND agentStatus='Active' ".
						$customCorrespondenceClause." 
					order by adminType, name	
						";
						//debug($qry);
			$src = selectMultiRecords($qry);
			$selectOption = "";
			if(!$arguments["hideSelectMain"]){
				$selectOption = '<select class="required" id="supAgentID" name="supAgentID">';
				$selectOption .= '<option value="">- Select User -</option>';
			}
			//debug(count($src));
			for ($i=0; $i < count($src); $i++)
			{
				$selected = "";
				if($selectedID == $src[$i]["userID"])
					$selected = "selected";
				$adminTypeOpt = $src[$i]["adminType"]; // Supper
				$adminTypeOptPrev = $src[$i-1]["adminType"];
				$adminTypeOptNext = $src[$i+1]["adminType"];

				if($adminTypeOpt == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOpt = "Super";
				if($adminTypeOptPrev == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOptPrev = "Super";
				if($adminTypeOptNext == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOptNext = "Super";
				if($adminTypeOpt != $adminTypeOptPrev){
					$adminTypeOptLabel = __($adminTypeOpt);
					$adminTypeOptOrigin = "";
					if(!empty($adminTypeOptLabel)){
						$adminTypeOptOrigin = $adminTypeOpt;
						$adminTypeOpt = $adminTypeOptLabel;
					}
					$selectOption .= "<optgroup label='".$adminTypeOpt."'>";
				}
				$selectOption .='<option value="'.$src[$i]["userID"].'" '.$selected.'>'.$src[$i]["name"] . " [" . $src[$i]["username"] . "]".'</option>';
				$adminTypeOpt = $adminTypeOptOrigin;
				if($adminTypeOpt != $adminTypeOptNext)
					$selectOption .='</optgroup>';
			
			}
			if(!$arguments["hideSelectMain"])
				$selectOption .='</select>';
		}
	}
	return $selectOption;
}

function CONFIG_POPULATE_ADMINSTAFF_DROPDOWN($arguments,$values="0"){
	$selectOption = "";
	$adminStaffList = "";
	
	if(!empty($arguments["adminStaffList"]))
		$adminStaffList	=	$arguments["adminStaffList"];
	else
		$adminStaffList	=	" 'Admin' , 'Branch Manager' ";
		
	if($values == "1"){
		if($arguments["userType"]=="populateAdminStaff"){		
			$selectedID = $arguments["selectedID"];			
		
			$qry = "select 
							userID,
							username, 
							name, 
							agentCountry, 
							adminType,
							isMain 
					from 
						".TBL_ADMIN_USERS." 
					where 
						 adminType IN (".$adminStaffList. ") 
						AND agentStatus='Active' 
					order by adminType, name	
						";
					//debug($qry);
			$src = selectMultiRecords($qry);
			$selectOption = "";
			if(!$arguments["hideSelectMain"]){
				$selectOption = '<select class="required" id="supAgentID" name="supAgentID">';
				$selectOption .= '<option value="">- Select User -</option>';
				
				if(!empty($arguments["allOption"]))
					/**
					* #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
					* Short Description
					* This Condition is Applied For Checking the All Option
					*/
					if($selectedID == 'all')
						$selectOption .= '<option value="all" selected="selected">All</option>';
					else
						$selectOption .= '<option value="all">All</option>';
			}
			//debug(count($src));
			for ($i=0; $i < count($src); $i++)
			{
				$selected = "";
				if($selectedID == $src[$i]["userID"])
					$selected = "selected";
				$adminTypeOpt = $src[$i]["adminType"]; // Supper
				$adminTypeOptPrev = $src[$i-1]["adminType"];
				$adminTypeOptNext = $src[$i+1]["adminType"];

				if($adminTypeOpt == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOpt = "Super";
				if($adminTypeOptPrev == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOptPrev = "Super";
				if($adminTypeOptNext == "Supper" && trim($src[$i]["isMain"]) == "Y")
					$adminTypeOptNext = "Super";
				if($adminTypeOpt != $adminTypeOptPrev)
					$selectOption .= "<optgroup label='".$adminTypeOpt."'>";
				$selectOption .='<option value="'.$src[$i]["userID"].'" '.$selected.'>'.$src[$i]["name"] . " [" . $src[$i]["username"] . "]".'</option>';
				if($adminTypeOpt != $adminTypeOptNext)
					$selectOption .='</optgroup>';
			
			}
			if(!$arguments["hideSelectMain"])
				$selectOption .='</select>';
		}
	}
	return $selectOption;
}


function CONFIG_TANSACTION_PURPOSES($arguments,$values="0"){
	$returnArr = array();
	if(defined("CONFIG_TANSACTION_PURPOSES") && CONFIG_TANSACTION_PURPOSES!="0"){
		if($arguments["flag"] == "transPurposeDrodown"){
			$transPurposes = explode(",",CONFIG_TANSACTION_PURPOSES);
			$transPurposes = array_filter($transPurposes);
			$options = "";
			foreach($transPurposes as $k=>$v)
				$options .= "<option value='".$v."'>".$v."</option>";
			$returnArr["dropdownOptions"] = $options;
		}
	}
	return $returnArr;
}
function CONFIG_POPULATE_USERS_DROPDOWN_SENDER($arguments,$values="0"){
	$returnData = "";
	if($values == "1"){
		if($arguments["flag"]=="populateUsers"){
			//$arguments = array("flag"=>"populateUsers","selectedID"=>$_REQUEST["supAgentID"],"hideSelectMain"=>true);
			$returnData = gateway("CONFIG_POPULATE_USERS_DROPDOWN",$arguments,CONFIG_POPULATE_USERS_DROPDOWN);
		}
		elseif($arguments["userType"]=="populateAdminStaff"){
		$returnData = gateway("CONFIG_POPULATE_USERS_DROPDOWN",$arguments,CONFIG_POPULATE_ADMINSTAFF_DROPDOWN);
		}
		elseif($arguments["flag"] == "addCustomerData"){
			$returnData["keys"] = ",parentID";
			$returnData["values"] = ",'".$arguments["parentID"]."' ";
		}
		elseif($arguments["flag"] == "updateCustomerData"){
			$returnData["keysValues"] = ",parentID = '".$arguments["parentID"]."' ";
		}
		elseif($arguments["flag"] == "mandatoryFields"){
			$returnData["mandatoryFields"] = "true";
		}
		
	}
	return $returnData;
}
function CONFIG_INSERT_DATE_IN_COMMENTS($arguments,$values="0"){
	if($values=="1"){
		if($arguments["flag"] == "getDatedForComments"){
			$returnData["values"] = "\n[".date('j F Y')."]\n";
		}
	}
	return $returnData;
} 
function CONFIG_INSERT_COMP_CONTACT_PERSON($arguments,$values="0"){
	if($values=="1"){		
		if($arguments["flag"] == "addCompanyData"){
			$returnData["keys"] = ",contactPerson";
			$returnData["values"] = ",'".$arguments["contactPerson"]."' ";
		}
		elseif($arguments["flag"] == "updateCompanyData"){
			$returnData["keysValues"] = ",contactPerson = '".$arguments["contactPerson"]."' ";
			
		}
	}
	return $returnData;
} 
function CONFIG_SOURCE_FIELD_CUSTOMER($arguments,$values="0"){ 	 	 
	if($values=="1"){ 	 	 
			if($arguments["flag"] == "sourceFieldDisplay"){ 	 	 
					$returnData["fieldLabel"] = "Source"; 	 	 
					$returnData["fieldStr"] = "<input type='text' id='source' name='source' value='".$arguments["selectedSource"]."' />"; 	 	 
			} 	 	 
			elseif($arguments["flag"] == "addCustomerData"){ 	 	 
				$returnData["keys"] = ",source"; 	 	 
					$returnData["values"] = ",'".addslashes($arguments["source"])."' "; 	 	 
			} 	 	 
			elseif($arguments["flag"] == "updateCustomerData"){ 	 	 
					$returnData["keysValues"] = ",source = '".addslashes($arguments["source"])."' "; 	 	 
			} 	 	 
	} 	 	 
	return $returnData; 	 	 
} 
function CONFIG_IBAN_TRADING_ACCOUNT($arguments,$values="0"){ 	 	 
	//debug_print_backtrace();
	if($values=="1"){ 	 	 
		if($arguments["flag"] == "ibanAccountField"){ 	 	 
				$fieldLabel = $arguments["fieldLabel"];
				if(!empty($fieldLabel))
					$fieldLabel = "IBAN";
				$returnData["fieldLabel"] = $fieldLabel;
				$returnData["fieldStr"] = "<input type='text' id='IBAN' name='IBAN' value='".$arguments["IBAN"]."' />";
		}
		elseif($arguments["flag"] == "ibanAccountAddUpdate"){ 	 	 
				if(count($arguments["fieldsChecked"])>0)
					$fieldsChecked = serialize($arguments["fieldsChecked"]);
				$returnData["keys"] = ",IBAN, fieldsChecked ";
				$returnData["values"] = ",'".addslashes($arguments["IBAN"])."', '".$fieldsChecked."' ";
				$returnData["keysValues"] = ",IBAN = '".addslashes($arguments["IBAN"])."',fieldsChecked = '".$fieldsChecked."'  ";
		}
	}
	return $returnData; 	 	 
}
function CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT($arguments,$values="0"){ 	 	 
	//debug_print_backtrace();
	if($values=="1"){ 	 	 
		if($arguments["flag"] == "dealFieldsAccountField"){ 	 	 
			$fieldDealLabel = $arguments["fieldDealLabel"];
			if(empty($fieldDealLabel))
				$fieldDealLabel = "Show on Receipt(s)";
			$returnData["fieldDealLabel"] = $fieldDealLabel;
			$returnData["fieldDealStr"] = "<input type='checkbox' id='showOnDeal' name='showOnDeal' value='Y' ".($arguments["showOnDeal"]=='Y'?'checked':'')." />";
		}
		elseif($arguments["flag"] == "dealFieldsAddUpdate"){ 	 	 
			$returnData["keys"] = ", showOnDeal";
			$returnData["values"] = ", '".$arguments["showOnDeal"]."' ";
			$dealStr = "SELECT count(id) as cnt from accounts WHERE currency = '".$arguments["currency"]."' AND showOnDeal = 'Y' ";
			$dealRS  = selectFrom($dealStr);
			$returnData["keysValues"] = ",showOnDeal = '".$arguments["showOnDeal"]."' ";

			if($arguments["showOnDeal"] == 'N' && $dealRS["cnt"]<=1){
				$returnData["msg"] = "There should be at least one account for Receipt(s) details.";
				$returnData["keysValues"] = "";
			}
		}
		elseif($arguments["flag"] == "dealFieldsUpdateRest"){ 	 	
			if(!empty($arguments["id"]) && !empty($arguments["currency"])){
				update("UPDATE accounts SET showOnDeal = 'N' WHERE id NOT IN('".$arguments["id"]."') AND currency IN ('".$arguments["currency"]."')");
			}
		}
	}
	return $returnData; 	 	 
}
function getCalculatedAmounts($valA,$operator,$valB){
	$returnVal = 0;
	if($operator == "*")
		$returnVal = $valA * $valB;
	elseif($operator == "/")
		$returnVal = $valA / $valB;
	elseif($operator == "+")
		$returnVal = $valA + $valB;
	elseif($operator == "-")
		$returnVal = $valA * $valB;
	return $returnVal;
}
function trim_value(&$value) 
{ 
    $value = trim($value); 
}
function removeFromEnd(&$string) {
	//debug_print_backtrace();
	//debug($string);
//	$string = 'picture.jpg.jpg';
//	$string = removeFromEnd($string, '.jpg');
    $pos = strpos($string, '|', 0);
	//debug($pos);
    $string = substr($string, 0, $pos);
    //debug($out);
	//return $out;
}
function CONFIG_TRADING_ACCOUNT_CUSTOMER($arguments,$values="0"){
	//debug_print_backtrace();
	$returnData = array();
	if($values=="1"){
		if($arguments["flag"] == "tradingAccountsDropdown"){
			$returnData["label"] = "Client Account Details";
			$returnData["dropValues"] = fetchData($arguments["query"],$arguments["selected"]);
			if(!empty($arguments["selectHead"]))
				$returnData["dropValues"] = '<select name="'.$arguments["selectHead"].'" id="'.$arguments["selectHead"].'">'.$returnData["dropValues"].'</select>';
		}
		elseif($arguments["flag"] == "addCustomerData"){
			$returnData["keys"] = ",tradingAcccount";
			$returnData["values"] = ",'".$arguments["tradingAcccount"]."' ";
		}
		elseif($arguments["flag"] == "updateCustomerData"){
			$returnData["keysValues"] = ",tradingAcccount = '".$arguments["tradingAcccount"]."' ";
		}
	}
	return $returnData;
}
function accountLedger($arguments = array()){
	require_once('double-entry-functions.php');
	//debug_print_backtrace();
	$flagReturn = false;
	$flagTradingLedger = $arguments['flagTradingLedger'];
	$strTransID = $arguments['transID'];
	$strAmount = $arguments['amount'];
	$strRAmount= $arguments["ramount"];
	$strSendingAc = $sendAcc = $arguments['sendingAc'];
	$strReceivingAc = $receiveAcc = $arguments['receivingAc'];
	$strSendingCurrency = $arguments['sendingCurrency'];
	$strReceivingCurrency = $arguments['receivingCurrency'];
	$strNote = $arguments['note'];
	$strCreatedBy = $arguments['createdBy'];
	$strCreated = $arguments['created'];
	$buySell = $arguments['buySell'];
	$status =  $arguments['status'];
	
	$extraFields = "";
	$extraValues = "";
	$extraUpdates = "";
		
	$extraFieldsData = $arguments['extraFields'];
	if(is_array($extraFieldsData)){
		foreach($extraFieldsData as $k=>$v){
			$extraFields .= ", $k ";
			$extraValues .= ", '$v' ";
			$extraUpdates .= ", $k = '$v' ";
		}
	}	
	if($status == 'C' && $buySell == "S"){
		$strRAmount= $arguments["amount"];
		$strReceivingCurrency = $arguments['sendingCurrency'];
	}
	$strCr = 'Cr';
	$strDr = 'Dr';
	//debug($arguments);
	/*
		For currency exchange it affects only receiving currency account [GBP]
	*/
	if($buySell == 'B'){
		$strCr = 'Cr';
		$strDr = '';
		$strReceivingAc = '';
		$strSendingAc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
	}
	else if($buySell == 'S'){
		$strCr = '';
		$strDr = 'Dr';
		$strReceivingAc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
		$strSendingAc = '';
	}
	
	//debug("in cr ==>".$strCr);
	//debug("in deb ==>".$strDr);

	if(!empty($arguments['status'])){
		$status = $arguments['status'];
		$extraFields .= ", status ";
		$extraValues .= ", '$status' ";
		$extraUpdates .= ", '$status' ";
	}
	if($flagTradingLedger){
		$strInsert = "INSERT INTO ".TBL_ACCOUNT_LEDGERS." 
					(
					transID,
					crAmount,
					drAmount,
					crAccount,
					drAccount,
					currency,
					description,
					createdBy,
					created
					$extraFields
					)
			  VALUES(
					'".$strTransID."',
					'".$strAmount."',
					'".$strRAmount."',
					'".$strSendingAc."',
					'".$strReceivingAc."',
					'".$strSendingCurrency."',
					'".$strNote."',
					'".$strCreatedBy."',
					'".$strCreated."'
					$extraValues
					)";
		 //debug($strInsert);	 receivingAmountCB	sendingAmountCB
		 //debug($strInsert,true); 
		  if(InsertInto($strInsert)){
			if(!empty($strCr)){
				  /*
					insert value in account summary for sending account
				  */
				  $_arrInput = array(
					  "accountNumber" =>$sendAcc,
					  "amount" =>$strAmount,
					  "currency" => $strSendingCurrency,
					  "type" => $strCr
				);
				ledgerSummary($_arrInput);
			}		
			if(!empty($strDr)){		
				 /*
					insert value in account summary for receiving account
				  */
				  $_arrInput = array(
					  "accountNumber" =>$receiveAcc,
					  "amount" =>$strRAmount,
					  "currency" => $strReceivingCurrency,
					  "type" => $strDr
				);
				//debug($_arrInput);
				ledgerSummary($_arrInput);
			}
			$flagReturn = true;
		}
	}	
	return $flagReturn;
}

function sendMailReceipt($arguments = array()){
//debug($arguments);
	$returnArr = array();
	$receiptFlag = $arguments['receiptFlag'];
	$loggedUserDataName = $arguments['loggedUserDataName'];
	$loggedUserDataID = $arguments['loggedUserDataID'];
	$transID = $arguments['transID'];
	$benID = $arguments['benID'];
	$today = $arguments['today'];
$receiptContents = '';
$receiptContents = '<table border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA" width="80%">
<tr>';
//debug($receiptContents);
$custLogo = selectFrom("select logo  from ".TBL_ADMIN_USERS." where username ='".$loggedUserDataName."'");

if($custLogo["logo"] != "" && file_exists("../logos/" . $custLogo["logo"]))
{
	$arr = getimagesize("../logos/" . $custLogo["logo"]);		
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);	

	$receiptContents .= '<td align="center">'.printLogoOnReciept($loggedUserDataID, $loggedUserDataName).'</td>';
	
}else{
		$receiptContents .= '<td align="center" ><img id=Picture2 height="'.CONFIG_LOGO_HEIGHT.'" alt="" src="'.CONFIG_LOGO.'" width="'.CONFIG_LOGO_WIDTH.'" border="0"></td>';
	
} 
	
	$queryTrans = "select * from transactions where transID= '".$transID."'";
	$transContent = selectFrom($queryTrans);	//debug ($transContent);		
	$agentsql="SELECT * FROM admin WHERE userID  = '".$transContent['custAgentID']."'";
	$custAgentContents=selectFrom($agentsql);		

	
	$receiptContents .= '<td class="sign">';
	
	if ($agentType=='SUPA' && $custLogo["logo"] != "")
	{
		$receiptContents .= $custAgentContents['agentCompany']."<br>".$custAgentContents['agentAddress'].",".$custAgentContents['agentAddress2'].",<br>".$custAgentContents['agentCity'].",".$custAgentContents['agentZip'].",<br>".$custAgentContents['agentCountry']."<br>";
		$receiptContents .= "Tel : ".$custAgentContents['agentPhone']."Fax : ".$custAgentContents['agentFax']."<br>Email : ".$custAgentContents['email']."<br>Web : ".$custAgentContents['agentURL'];
//	echo "<br>company";
	} 
	
	else {
	$adminsql= "SELECT * FROM admin WHERE username = '".admin."'";
	$adminContents=selectFrom($adminsql);
	$receiptContents .= $adminContents['agentCompany']."<br>".$adminContents['agentAddress'].", ".$adminContents['agentAddress2'].",<br>".$adminContents['agentCity'].", ".$adminContents['agentZip'].", ".$adminContents['agentCountry']."<br>";
	$receiptContents .= "Tel : ".$adminContents['agentPhone']."<br>Fax : ".$adminContents['agentFax']."<br>Email : ".$adminContents['email']."<br>Web : ".$adminContents['agentURL'];
//debug ($adminContents);
	}
	//debug($receiptContents1);
	$receiptContents .= '</td>
	  </tr>
	</table>
	<form name="addTrans" method="post" action="">'; 
 
				//debug ($transContent);
				$customerID=$transContent['customerID'];
				$queryCust = "select customerID, Title, accountName, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, email,dob,IDType,IDNumber, remarks  from customer where customerID ='" . $customerID . "'";
				$customerContent = selectFrom($queryCust); 
				$custEmail = $customerContent["email"];
				if(!empty($mailto))
					$custEmail = $mailto;
				//debug ($customerContent);
				$benID=$transContent['benID'];
				/****************/
				if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
					$accountMgr = selectFrom("select parentID from customer where customerID='".$customerID."'");				
					$custAccMgrCont = selectFrom("SELECT name,email FROM admin WHERE userID  = '".$accountMgr['parentID']."'");				
					$accMgrEmail = $custAccMgrCont["email"];	
					$emailFlag = true;
				}			
				/***********************/
				
	$customerID=$transContent['customerID'];
	$queryCust = "select customerID, Title, accountName, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from customer where customerID ='" . $customerID . "'";
	$customerContent = selectFrom($queryCust); 
	//			debug ($customerContent);
	
	$queryusername= " select name from admin where username = '".$transContent['addedBy']."'";
	$usernameCont= selectFrom($queryusername);
	
	//debug ($refContent);

	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$transContent['currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
		
	$trAccountrTrading = '';
	for($i=0;$i<count($fieldsCheckedArr);$i++){
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr)){
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		$trAccountrTrading .= '	 <tr>
								<td class="Arial" width="20%">'.strtoupper($fieldsLabel).':</td>
								<td width="80%" class="bottom">'.$accountRS[$fieldsValue].'&nbsp;</td>
							  </tr>';	
	}

  $bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$transID."'");	
  
  $ibanOrAcc = '';
  $sortCodeV = '';
  $tipV = '';
  if($bankdetailsQuery["IBAN"]!='' && $bankdetailsQuery["accNo"]!=''){			
		$ibanOrAcc = "IBAN: ".$bankdetailsQuery["IBAN"]." / ". "Account No. ".$bankdetailsQuery["accNo"];
	}
	elseif($bankdetailsQuery["IBAN"]!=''){
		$ibanOrAcc = "IBAN: ".$bankdetailsQuery["IBAN"];
	}
	elseif($bankdetailsQuery["accNo"]!=''){
		$ibanOrAcc = "Account No. ".$bankdetailsQuery["accNo"];
	}	
	if($bankdetailsQuery["sortCode"]!="" &&  $bankdetailsQuery["routingNumber"]!="") {
		$sortCodeV = $bankdetailsQuery["sortCode"]." / ".$bankdetailsQuery["routingNumber"];			
	}
	elseif($bankdetailsQuery["sortCode"]=""){
		$sortCodeV = $bankdetailsQuery["routingNumber"];			
	}
	elseif($bankdetailsQuery["routingNumber"]=""){
		$sortCodeV = $bankdetailsQuery["sortCode"];
	}
	if(CONFIG_HIDE_TIP_FIELD != "1"){
		$tipV = $transContent["tip"];
	}	
	else{
		$tipV = "BARC GB 22";
	}
	
	$termsConditionStr = '';
	if(CONFIG_TRANS_CONDITION_ENABLED == '1')
	{
		if(defined("CONFIG_TRANS_COND"))
		{
			$termsConditionStr =  '<td class = "myFormat" align="center">'. (CONFIG_TRANS_COND).'</td>';						
			//debug($termsConditionStr);
		}
		else
		{
			$termsConditionSql = selectFrom("SELECT company_terms_conditions FROM company_detail
													WHERE company_terms_conditions!='' 
													AND dated = (SELECT MAX(dated) FROM company_detail WHERE company_terms_conditions!='')");
			if(!empty($termsConditionSql["company_terms_conditions"]))
				$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
			//eval("$tremsConditions");
			eval("\$tremsConditions = \"$tremsConditions\";");
			$termsConditionStr =  stripslashes($tremsConditions);
			//debug($termsConditionStr);
			//echo $tremsConditions;
		}
		if (defined('CONFIG_CONDITIONS')) { 
			$termsConditionStr = '';
		}  
	}else{
		$termsConditionStr = "I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.";
	//debug($termsConditionStr);
	} 
$receiptContents ='
<tr>
	<td>
	
	<table border="1" width="80%" align="center" class="double" bordercolor="#000000">
	   
         <tr border="1">
            <td style="padding-left:30px;" width="40%" class="Arial" bgcolor="#C0C0C0">CLIENT NAME:</td>
            <td width="80%" align="center" class="bottom" bgcolor="#C0C0C0">'. $customerContent["firstName"] . ' ' . $customerContent["middleName"] . ' ' . $customerContent["lastName"] .'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial"  style="padding-left:30px;" width="40%" bgcolor="#C0C0C0">CLIENT NO.</td>
            <td width="80%" align="center" class="bottom" bgcolor="#C0C0C0">'. $customerContent["accountName"].'&nbsp;</td>
          </tr>
          <tr>
            <td style="padding-left:30px;" width="40%" bgcolor="#C0C0C0">PFX DEALER</td>
            <td width="80%" align="center" class="bottom" bgcolor="#C0C0C0">'.$usernameCont['name'].'&nbsp;</td>
          </tr>
        </table>
        <table border="0" width="80%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">This Trade has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.
			</td>
			</tr>
			<tr>
			<td>
              <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">TRADE DATE:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'.dateFormat($transContent['transDate'],2) .'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">MATURITY DATE:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'.dateFormat($transContent['valueDate'],2).'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">YOU BUY:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'.$transContent["localAmount"]. " ". $transContent["currencyTo"].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">AT A RATE OF:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'. $transContent['exchangeRate'].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">YOU SELL:</td>

                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'. number_format($transContent["transAmount"],2).' '.$transContent["currencyFrom"].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">TT Fee:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">ZERO</td>
                </tr>
                <tr>
                  <td class="Arial" width="40%" bgcolor="#C0C0C0">REFERENCE:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'. $transContent['refNumber'].'
                    &nbsp;</td>
                </tr>
              </table>
			 </td>
          </tr>
        </table>
        <table border="0" width="80%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">Where to instruct your Bank to send payment to us:</td>
          </tr>
        </table>
		<table border="1" width="60%" align="center" class="double" bordercolor="#000000">
';
if(!empty($transContent['currencyFrom'])){
	$receiptContents .='
		<tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">ACCOUNT NAME:</td>
            <td width="80%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["accountName"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">ACCOUNT NUMBER:</td>
            <td width="35%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["accounNumber"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">BANK:</td>
            <td width="25%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["bankName"].'&nbsp;</td>
          </tr>
		  <tr>
          <td class="Arial" width="40%" bgcolor="#C0C0C0">IBAN:</td>
            <td width="35%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["IBAN"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">SORT CODE:</td>
            <td width="25%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["sortCode"].'&nbsp;</td>
          <tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">Swift Code:</td>
            <td width="35%" class="bottom" bgcolor="#C0C0C0">'.$accountRS["swiftCode"].'&nbsp;</td>
          </tr></table>
	';
	$receiptContents .= $trAccountrTrading;
}
	$receiptContents .='
	<table widh="80%" border="0" bordercolor="#FF0000" align="center">
		<tr>
		<td>
			<table border="0" width="64%" align="center" bordercolor="#000000">
			  <tr>
				<td colspan="2">I confirm that the details set out above are correct and that I will transmit the amount due to the account specified.  I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a &pound;25.00 per day late payment fee.</td>
			  </tr>
			  </table>
			  <table border="1" width="64%" class="single" align="center">
			  <tr>
				<td>
				  <table border="0" width="100%" class="single" align="center">
				  <tr>
					<td width="50%" bgcolor="#C0C0C0"><br/>SIGNED</td>
					<td width="50%" class="" bgcolor="#C0C0C0">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="50%" bgcolor="#C0C0C0">NAME (Print):</td>
					<td width="50" class="" bgcolor="#C0C0C0">&nbsp;</td>
				  </tr>
				  <tr>
					<td width="50%" bgcolor="#C0C0C0">DATED:</td>
					<td width="50%" class="" bgcolor="#C0C0C0"><br/>'.date("d/m/y",strtotime($transContent["transDate"])).'&nbsp;</td>
				  </tr>
				 
			</table>
				</td>
			  </tr>
			</table>
			 <table border="1" width="64%" class="single" align="center">
				  <tr>
					<td width="50%" style="font-family:Gill Sans MT;" bgcolor="#C0C0C0"><br/>PREMIER FX SIGN OFF:</td>
					<td width="50%" bgcolor="#C0C0C0"><td>
			</tr>
			</table>
		</td>
				</tr>
		</table>';
		
	$receiptContents .='
<table border="0" width="80%" align="center" class="">
           <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="center" style="font-weight:bold;font-size:14px;font-family:Gill Sans MT; text-decoration:underline;">REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</td>
          </tr>
          <tr>
            <td colspan="2" align="center">Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</td>
          </tr>
          </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
		  <tr>
            <td width="70%" bgcolor="#C0C0C0">CLIENT NUMBER:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'. $customerContent["accountName"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$transContent["localAmount"].' '.$transContent["currencyTo"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">BENEFICIARY&rsquo;S ACCOUNT NAME:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$bankdetailsQuery["bankName"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>		  
          <td width="70%" style="font-style:Gill Sans MT" bgcolor="#C0C0C0">BENEFICIARY&rsquo;S IBAN / ACCOUNT NO:
IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE
		</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$ibanOrAcc.'&nbsp;</td>
		</tr>
		</table>
		<table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$bankdetailsQuery["branchAddress"].'&nbsp;</td>
		</tr>
		</table>
		<table border="1" width="80%" align="center" class="double" bordercolor="#000000">		
          <tr>
            <td width="70%" bgcolor="#C0C0C0">SWIFT/BIC CODE OF THE YOUR BANK:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">
			'.$bankdetailsQuery["swiftCode"].'&nbsp;
			</td>
          </tr>
		  </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$sortCodeV.'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="80%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
            <td width="54%" class="leftborder" bgcolor="#C0C0C0">'.$tipV.'&nbsp;</td>
          </tr>
        </table>';
		//debug($termsConditionStr);
		//$message.=$termsConditionStr;
		//$message.=$termsConditionStr;
		$TRANSACTION_DATE = dateFormat($today,1);
		$receiptContents .='<table border="0" width="80%" align="center">
<tr>
<td align="center" style="font-weight:bold;font-size:14px;font-family:Gill Sans MT; text-decoration:underline;">CUSTOMER DECLARATION AND DETAILS</td>
</tr>
</table>
<table border="0" width="80%" align="center">
<tr>
<td width="100%">I confirm that I will make arrangements to transfer the requisite amount of funds to pay for the currency ordered as per my verbal instruction given in relation to this trade number and that cleared funds will be credited to Premier FX�s Client Account one (1) working day before the maturity date of this trade. Should I envisage any problem in keeping to this agreement, I will contact my personal dealer immediately. In the event that he or she is unavailable, I will speak to another available dealer of Premier FX to ensure that the Company is made aware of the situation.  I also confirm that I am aware that any changes to this trade may incur additional charges.</td>
</tr>		  
</table>	
<br>
<table border="1" width="80%" align="center" class="double">
<tr>
<td width="46%">Your Name:</td>
<td width="54%" class="leftborder">&nbsp; </td>
</tr>
</table>

<table border="1" width="80%" align="center" class="double">
<tr>
<td width="46%">Your Contact Number(s): </td>
<td width="54%" class="leftborder">&nbsp; </td>
</tr>
</table>

<table border="1" width="80%" align="center" class="double">
<tr>
<td width="46%">Your Signature: </td>
<td width="54%" class="leftborder">&nbsp; </td>
</tr>
</table>

<table border="1" width="80%" align="center" class="double">
<tr>
<td width="46%">DATED: DD/MM/YY</td>
<td width="54%" class="leftborder">'.$TRANSACTION_DATE.'&nbsp;</td>
</tr>
<tr>	
</table>
<table border="0" width="60%" align="right">
<tr>
<td width="50%">&nbsp;</td>
		<td align="left" width="50%" class="terms"><b><a href="mailto:info@premfx.com">Premier FX</a></b></tr>
<tr>		
<td width="50%">&nbsp;</td>
<td align="left" style="font-size:12px;" width="50%">
			<br/>Rua Sacadura Cabral, Ed Golfe, 1°A,<br> Almancil 8135-144, Portugal<br>Tel UK: + 44 (0) 845 021 2370<br>Tel Int : +351 289 358 511<br>Fax : +351 289 358 513<br>Email : info@premfx.com<br>Web : www.premfx.com</td>           
</tr>
</table>
';
 //echo $receiptContents;
 
$returnArr['receiptContents'] = base64_encode($receiptContents);
$returnArr['transID'] = $transID;
$returnArr['customerEmail'] = $customerContent['Email'];
$returnArr['agentEmail'] = $agentEmail;
$returnArr['accMgrEmail'] = $accMgrEmail;
//debug($returnArr);
return $returnArr;
}

function attachPDFServiceEmail($args = array()){
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,700,200);
	
	$pdf->ezSetMargins(0,0,100,100);
	$pdf->ezText('',72);
	$pdf->ezText('',48);
	$bullet=array();
	$bullet['bullet']=chr(149);

	$pdf->ezText('<u>A guide to trading with Premier FX.</u>',12,array("justification" => 'left'));	
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	$pdf->ezText('Sending money abroad with Premier FX is simple. There are two ways to trade with us: Contact us direct via phone or e mail, or use our on line trading system.',12,array("justification" => 'left'));
	$pdf->ezText('',12);
	$pdf->ezText('<b>1. Contact us direct</b>',12,array("justification" => 'left'));	
	$pdf->ezText("      ".$bullet["bullet"]." Telephone or e-mail us for an exchange rate. If you agree the rate",12,array("justification" => 'justify'));
	$pdf->ezText("        we will send you a deal confirmation with all the trade details.",12,array("justification" => 'justify'));
	$pdf->ezText("      ".$bullet["bullet"]." Send your money to our client account to pay for your currency",12,array("justification" => 'justify'));	
	$pdf->ezText("      ".$bullet["bullet"]." Once we have received your funds, your currency will be sent to your",12,array("justification" => 'left'));
	$pdf->ezText("        designated bank account free of charge.",12,array("justification" => 'left'));
	$pdf->ezText('',12);
	$pdf->ezText('<b>2. PFX Online </b>',12,array("justification" => 'justify'));	
	$pdf->ezText('You can use our on line trading system PFX Online up to STG 30,000 ',12,array("justification" => 'justify'));	
	$pdf->ezText("      ".$bullet["bullet"]."	Log on to your account with your secure password ",12,array("justification" => 'justify'));	
	$pdf->ezText("      ".$bullet["bullet"]." On the 'send money' page put in the currency you want to sell and",12,array("justification" => 'justify'));	
	$pdf->ezText("        the amount, or the currency you wish to buy and get the exchange",12,array("justification" => 'justify'));
	$pdf->ezText("        rate",12,array("justification" => 'justify'));
	$pdf->ezText("      ".$bullet["bullet"]." If your happy accept the rate and move on to adding the beneficiary",12,array("justification" => 'justify'));
	$pdf->ezText("        details",12,array("justification" => 'justify'));	
	
	$pdf->ezText("      ".$bullet["bullet"]." Then confirm your deal. A deal confirmation will be sent to you to",12,array("justification" => 'justify'));	
	$pdf->ezText("        confirm the trade.",12,array("justification" => 'justify'));
	
	$pdf->ezText("      ".$bullet["bullet"]." Send your money to our client account to pay for the trade.",12,array("justification</li>" => 'justify'));	
	
	$pdf->ezText("      ".$bullet["bullet"]." Once we have received your funds, your currency will be sent to your",12,array("justification" => 'justify'));	
	$pdf->ezText("         designated bank account free of charge.",12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	$pdf->ezText('Please do not hesitate to contact us if you have any questions. We are here to make your currency exchange stress free and simple.',12,array("justification" => 'justify'));	
	$pdf->ezText('',72);
	$pdf->ezText('',24);
	$image2 = imagecreatefromjpeg("images/premier/mail_signature.jpg");
	$pdf->addImage($image2,100,170,420);
	$pdf->ezText('<b>UK:</b> 55 Old Broad Street, London, EC2M 1RX  t +44 (0)845 021 2370',12,array("justification" => 'justify'));
	$pdf->ezText('<b>Portugal:</b> Rua Sacadura Cabral, Edifício Golfe lA, 8135-144 Almancil, Algarve  t +351 289 358 511',12,array("justification" => 'justify'));
	
	$pdf->ezText('<b>Spain:</b> La Rambla 13 - 07003, Palma de Mallorca, Mallorca.   t +34 971 576 724',12,array("justification" => 'justify'));
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	//$pdf->setColor(192, 192, 192,0);
	$pdf->ezText('<i>Premier FX is fully registered with both HM Customs & Excise and the FCA (no. 530712) as a payment services institution</i>',8,array("justification" => 'justify'));
	//$pdf->ezText('<b>www.premfx.com.</b>',12,array("justification" => 'left'));
	$pdfcode = $pdf->output();

	//$pdf->ezStream();

	//$dir =  $_SERVER['DOCUMENT_ROOT'].'/uploads/pdf_files';
	//save the file
	//if (!file_exists($dir)){
	//	mkdir ($dir,0777);	
	//}
	$fPDFname = tempnam('/tmp','PDF_SERVICE_').'.pdf';//$dir.'/Stress_Free_Service_4_Steps_'.time().'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	// MAIL HEADERS with attachment
	//echo $message;
	$filePDF = chunk_split(base64_encode($filePDF));
	
	$returnData['stream'] = $filePDF;
	$returnData['file_name'] = 'A guide to trading with Premier FX.pdf';
	$returnData['file_path'] = $fPDFname;
	return $returnData;
}

function attachPDFWelcomeLetterEmail($args = array()){
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	$tradingFields = "";
	if(CONFIG_TRADING_ACCOUNT_CUSTOMER == "1")
		$tradingFields .= ",tradingAcccount ";
	
	$whrClauseAdminTable = " and userID = '".$args["agentID"]."' ";
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
		$tradingFields .= ",parentID ";
		$whrClauseAdminTable = " and userID = '".$args["customerParentID"]."' ";
	}
	
	$queryCust = selectFrom("select firstName, lastName,password, middleName,accountName".$tradingFields." from ".TBL_CUSTOMER." where customerID = '".$args["customerID"]."'");
$accManagerQuery = selectFrom("select name,email  from ".TBL_ADMIN_USERS." where 1 $whrClauseAdminTable ");


	$accountSQl = "SELECT * FROM accounts WHERE id = '".$queryCust['tradingAcccount']."' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
		
	$accountData = "";
	$trAccountrTrading = "";
	for($i=0;$i<count($fieldsCheckedArr);$i++){
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr)){
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		$trAccountrTrading[] = array(array('name'=>strtoupper($fieldsLabel),'type'=>$accountRS[$fieldsValue]));
	}
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,700,200);
	
	$pdf->ezSetMargins(0,0,100,100);
	for($i=0;$i<=10;$i++)
		$pdf->ezText('',12);
	
	$pdf->ezText('<b>Dear '.$queryCust["firstName"].'</b>',12,array("justification" => 'left'));	
	$pdf->setStrokeColor(242,186,121);
	$pdf->ezText('',12);
	$pdf->ezText('Thank you for choosing Premier FX to transact your currency exchange.',12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('I have opened you a no obligation trading account number <b>'.$queryCust["accountName"].'</b>. Please use this account number in any communication you may have with Premier FX. I will also act as your personal dealer and will be on hand at anytime to answer any questions you may have.',12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('Please also find your password for our on-line trading system, PFX Online. We advise you to change this password once you have logged on to the system via our website. PFX Online enables you to send your international payments 24 hours a day, 7 days a week.',12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('Your username is '.$queryCust["accountName"].'',12,array("justification" => 'justify'));
	$pdf->ezText('Your password is '.$queryCust["password"].'',12,array("justification" => 'justify'));
	$pdf->ezText('Login URL is '.PAYEX_URL.'/premier_fx_online_module/',12,array("justification" => 'justify'));
	$pdf->ezText('',12);
	$pdf->ezText('We have also provided the bank account details for our '.$accountRS["currency"].' client account. These details are required when you need to send a payment to us once you have agreed a deal.',12,array("justification" => 'justify'));	

	$pdf->ezText('',12);
	$accountData = array(
						 array('name'=>'ACCOUNT NAME:','data'=>$accountRS["accountName"])
						,array('name'=>'ACCOUNT NUMBER:','data'=>$accountRS["accounNumber"])
						,array('name'=>'BANK:','data'=>$accountRS["bankName"])
						,array('name'=>'IBAN:','data'=>$accountRS["IBAN"])
						,array('name'=>'SORT CODE:','data'=>$accountRS["sortCode"])
						,array('name'=>'Swift Code:','data'=>$accountRS["swiftCode"])
					);
	foreach($trAccountrTrading as $k=>$v)
		$accountData[] = $v;
	$pdf->ezTable($accountData,array('name'=>'value','data'=>'column'),'<b>PREMIER FX '.$accountRS["currency"].' CLIENT ACCOUNT DETAILS:</b>'
						,array('showHeadings'=>0,'shaded'=>1,'showLines'=>1)
				);
	$pdf->ezText('',12);
	$pdf->ezText('We pride ourselves on providing a professional, courteous service and to be available to answer any questions you may have. Please feel free to call or email us.',12,array("justification" => 'left'));	
	$pdf->ezText('',12);
	$pdf->ezText('I look forward to hearing from you soon.',12,array("justification" => 'left'));	
	$pdf->ezText('',12);
	$pdf->ezText('Kind Regards,',12,array("justification" => 'left'));	
	$pdf->ezText('<b>'.$accManagerQuery["name"].'</b>',12,array("justification" => 'left'));	
	$pdf->ezText('',12);

	$pdf->ezText('Premier FX',12,array("justification" => 'right'));
	$pdf->ezText('Rua Sacadura Cabral, Ed Golfe, 1A,',12,array("justification" => 'right'));
	$pdf->ezText('Almancil 8135-144, Portugal',12,array("justification" => 'right'));
	$pdf->ezText('Tel UK  : +44 (0) 845 021 2370',12,array("justification" => 'right'));
	$pdf->ezText('Fax     : +351 289 358 513',12,array("justification" => 'right'));
	$pdf->ezText('Email   : info@premfx.com',12,array("justification" => 'right'));
	$pdf->ezText('Web     : www.premfx.com',12,array("justification" => 'right'));

	$pdfcode = $pdf->output();

	//$pdf->ezStream();

	//$dir =  $_SERVER['DOCUMENT_ROOT'].'/uploads/pdf_files';
	//save the file
	//if (!file_exists($dir)){
	//	mkdir ($dir,0777);	
	//}
	$fPDFname = tempnam('/tmp','PDF_WC_').'.pdf';//$dir.'/Stress_Free_Service_4_Steps_'.time().'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	// MAIL HEADERS with attachment
	//echo $message;
	$filePDF = chunk_split(base64_encode($filePDF));
	
	$returnData['stream'] = $filePDF;
	$returnData['file_name'] = 'WelcomeLetter.pdf';
	$returnData['file_path'] = $fPDFname;
	
	return $returnData;
}

function attachPDFWelcomeLetterEmailOnReg($args = array()){
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	$tradingFields = "";
	if(CONFIG_TRADING_ACCOUNT_CUSTOMER == "1")
		$tradingFields .= ",tradingAcccount ";
	
	
	
	if($args['accountName']!=''){
		$queryCust = selectFrom("select firstName, lastName, middleName,password,accountName".$tradingFields." from ".TBL_CUSTOMER." where accountName = '".$args["accountName"]."'");
	}
	else{
		$whrClauseAdminTable = " and userID = '".$args["agentID"]."' ";
		if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR == "1"){
			$tradingFields .= ",parentID ";
			$whrClauseAdminTable = " and userID = '".$args["customerParentID"]."' ";
		}
	
		$queryCust = selectFrom("select firstName, lastName, middleName,password,accountName".$tradingFields." from ".TBL_CUSTOMER." where customerID = '".$args["customerID"]."'");
	}
	$accManagerQuery = selectFrom("select name,email  from ".TBL_ADMIN_USERS." where 1 $whrClauseAdminTable ");
	$accManagerName=explode(" ",$accManagerQuery['name']);
	$countName=count($accManagerName);
		if($countName>2){
		$fullNameAccM=$accManagerName[0]." ".$accManagerName[$countName-1];
	
		}
		else{
			$fullNameAccM=$accManagerName[0]." ".$accManagerName[1];
	
		}

	$accountSQl = "SELECT * FROM accounts WHERE id = '".$queryCust['tradingAcccount']."' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
		
	$accountData = "";
	$trAccountrTrading = "";
	for($i=0;$i<count($fieldsCheckedArr);$i++){
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr)){
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		$trAccountrTrading[] = array(array('name'=>strtoupper($fieldsLabel),'type'=>$accountRS[$fieldsValue]));
	}
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,90,700,200);
	
	$pdf->ezSetMargins(0,0,100,100);
	for($i=0;$i<=10;$i++)
		$pdf->ezText('',12);
	
	//$pdf->ezText('<b>Dear '. $queryCust["firstName"].' '. $queryCust["lastName"].' </b>',12,array("justification" => 'left'));	
	$pdf->ezText('Dear '. $queryCust["firstName"],12,array("justification" => 'left'));	
	
	$pdf->setStrokeColor(242,186,121);
	$pdf->ezText('',12);
	$pdf->ezText('Thank you for choosing Premier FX to transact your currency exchange.',12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('We have now completed your registration.We have allocated you a client reference number, '. $queryCust["accountName"].'. Please quote this number in any communication you have with us, and also please quote this number when making a payment to us.',12,array("justification" => 'justify'));	
	$pdf->ezText('',12);
	$pdf->ezText('We will also, by separate e mail, send you a password and details for our on-line trading system, PFX Online. We advise you to change this password once you have logged on to the system via our website. PFX Online enables you to send your international payments 24 hours a day, 7 days a week. ',12,array("justification" => 'justify'));
	$pdf->ezText('',12);
	
	$pdf->ezText('Please do not hesitate to contact us if you have any further questions.',12,array("justification" => 'left'));	
	$pdf->ezText('',12);
	$pdf->ezText('I look forward to hearing from you in the near future.',12,array("justification" => 'left'));	
	$pdf->ezText('',12);
	$pdf->ezText('Kind Regards,',12,array("justification" => 'left'));	
	$image2 = imagecreatefromjpeg("images/premier/mail_signature.jpg");
	$pdf->addImage($image2,100,300,420);
	$pdf->ezText($fullNameAccM,12,array("justification" => 'left'));
	$pdf->ezText('Account Manager',12,array("justification" => 'left'));		
	$pdf->ezText('',48);
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	$pdf->ezText('<b>UK:</b> 55 Old Broad Street, London, EC2M 1RX  t +44 (0)845 021 2370',12,array("justification" => 'justify'));
	$pdf->ezText('<b>Portugal:</b> Rua Sacadura Cabral, Edifício Golfe lA, 8135-144 Almancil, Algarve  t +351 289 358 511',12,array("justification" => 'justify'));
	
	$pdf->ezText('<b>Spain:</b> La Rambla 13 - 07003, Palma de Mallorca, Mallorca.   t +34 971 576 724',12,array("justification" => 'justify'));
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	//$pdf->setColor(192, 192, 192,0);
	$pdf->ezText('<i>Premier FX is fully registered with both HM Customs & Excise and the FCA (no. 530712) as a payment services institution</i>',8,array("justification" => 'justify'));
	//$pdf->ezText('Email   : info@premfx.com',12,array("justification" => 'right'));
	//$pdf->ezText('Web     : www.premfx.com',12,array("justification" => 'right'));

	$pdfcode = $pdf->output();

	//$pdf->ezStream();

	//$dir =  $_SERVER['DOCUMENT_ROOT'].'/uploads/pdf_files';
	//save the file
	//if (!file_exists($dir)){
	//	mkdir ($dir,0777);	
	//}
	$fPDFname = tempnam('/tmp','PDF_WC_').'.pdf';//$dir.'/Stress_Free_Service_4_Steps_'.time().'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	// MAIL HEADERS with attachment
	//echo $message;
	$filePDF = chunk_split(base64_encode($filePDF));
	
	$returnData['stream'] = $filePDF;
	$returnData['file_name'] = 'WelcomeLetter.pdf';
	$returnData['file_path'] = $fPDFname;
	
	return $returnData;
}

function CONFIG_ACCOUNT_LEDGER_TRANS_TYPE($arguments,$values="0"){
	$returnData = array();
	$type = $arguments['type'];
	$flag = $arguments['flag'];
	$select = $arguments['select'];	
	$CancelTrans = $arguments['CancelTrans'];
	
	$transID = '';
	$options = '';
	if($values==1){
		if($flag == 'getTransID'){
			if(!empty($type)){
				$totalCntData = selectFrom("SELECT count(id) as cnt,transType FROM ".TBL_ACCOUNT_LEDGERS." WHERE transType LIKE '$type' and status LIKE 'NEW' GROUP BY transType ");
				
				if(!empty($totalCntData))
				{
					$totalCntFT 	= ($totalCntData['cnt']+1);
					$totalCntPrefix	= $totalCntData['transType'];
				}
				else
				{
					$totalCntFT 	= 1;
					$totalCntPrefix	= $type;
				}
				if(empty($CancelTrans))
					$transID = $totalCntPrefix.'-'.$totalCntFT;
				else
					$transID = $totalCntPrefix.'-'.$CancelTrans;
			}
		}
		elseif($flag == 'getTransTypeDropdown'){
			$options .= '<option value="">--Select Type--</option>\n\r';
			$options .= '<option value="CE">Currency Exchange</option>\n\r';
			$options .= '<option value="FT">Fund Transfers</option>\n\r';
			if(defined("CONFIG_ACCOUNT_LEDGER_TRANS_TYPE_TT") && CONFIG_ACCOUNT_LEDGER_TRANS_TYPE_TT=='1')
				$options .= '<option value="TT">TT Transfers</option>\n\r';
			$options .= '<option value="MN">Manual Adjustment Entries</option>\n\r';
		}
		elseif($flag == 'getTransIDField'){
			$strHTML .= 'Trans ID &nbsp;&nbsp;: <input type="text" name="transID" id="transID" value="'.$select.'" size="11" />';
		}
	}
	$returnData['transID'] = $transID;
	$returnData['transTypeDropdown'] = $options;
	$returnData['transIDHTML'] = $strHTML;
	return $returnData;
}
/**
	 * To save into database
	 * @param 
	 */
	function dataInsertionOperation($arrData, $strTableToInsert, $refData, $sqlType = "INSERT", $strWhereUpdateCondition = "")
	{
		$strSql = "";
		
		if(empty($strTableToInsert))
			return false;
	
		$strTableToInsert = DATABASE.".".$strTableToInsert;
		
		foreach($arrData as $strDbField => $strDbFieldVal)
		{
			$strSql .= "$strDbField = '".addslashes($strDbFieldVal)."', "; 
		}
		
		$strSql = substr($strSql, 0, -2);
		
		if($sqlType == "INSERT")
		{
			$strSql = "INSERT INTO ".$strTableToInsert." SET ".$strSql;
			
			//debug($strSql);
	
			if(insertInto($strSql, $refData))
			{
				//get last inserted id
				$arrLastId = selectFrom("SELECT LAST_INSERT_ID() as lid", $refData);
				if(!empty($arrLastId['lid']))
					return $arrLastId['lid'];
			}
			
			return false;
			
		}elseif($sqlType == "UPDATE"){
		
			//debug($strSql);
		
			if(empty($strWhereUpdateCondition))
				return false;
			
			$strSql = "UPDATE ".$strTableToInsert." SET ".$strSql." WHERE ".$strWhereUpdateCondition;
			
			//debug($strSql,true);
			return update($strSql);
			
		}
		//debug($strSql);
		return false;
	}
	function getTotalStockAmount(){

		// get total GBP of stock amount		
		$totalGBP = 0;

		$arrCurrenciesRows = SelectMultiRecords("select currencyName 

												from ".TBL_CURRENCY." 

												where cID IN 

											(select buysellCurrency 

												from curr_exchange order by id DESC)

												 OR 

												cID IN (select operationCurrency 

												from curr_exchange order by id DESC)

												group by currencyName");

	

		/* Fetching the list of fees to display at the bottom */	

		$arrAllCurrenyStockData = selectMultiRecords("Select * from ".TBL_CURRENCY_STOCK_LIST." order by currency asc");

		foreach($arrAllCurrenyStockData as $currVal)

		{

			$strCurrency = $currVal["currency"];



			$strAmount = number_format($currVal["amount"],2,".","");



			$strUpdatedOn = date("d/m/Y H:i:s",strtotime($currVal["updated_on"]));

			$cur_sql="select cID from currencies where currencyName ='$strCurrency'";

			$buying_rate_arr_id = selectFrom($cur_sql); 

			$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";

			//debug($cur_sql);

			$buying_rate_arr = selectFrom($cur_sql); 

			//debug($buying_rate_arr);

			$buying_rate=number_format($buying_rate_arr[0],4,'.','');

			$GBP_value =number_format($strAmount/$buying_rate,4,'.',NULL);

			$totalGBP +=$strAmount/$buying_rate;

		}

		return $totalGBP;

	}
	function checkDocumentExpiry($arr)
{
	//debug($arr);
	$currentDate = date('Y-m-d');
	//$docExpiryFlag = false;
	$idExpiryFlag = false;
	$docProvided = $arr["docProvided"];
	$customerID = $arr["customerID"];
	
				if(is_array($docProvided))
				{
					foreach($docProvided as $doc)
					{
						//debug($doc."-->".$currentDate);
						$getDate = explode("-",$doc);
						$y = $getDate[0];
						$m = $getDate[1];
						$d = $getDate[2];
						if($m <10)
								$m = '0'.$m;
						$doc2 =$y."-".$m."-".$d; 		 
						
						if($doc2 < $currentDate)
								$idExpiryFlag = true;
						
						//debug($idExpiryFlag);	
					}
				}
				$strSql = "select i.id_number,expiry_date from ".TBL_CUSTOMER." as c,  user_id_types as i where customerID='".$customerID."' 
								and user_id=customerID AND user_type= 'C' ";
				$multiId = selectMultiRecords($strSql);
				for($k=0;$k<count($multiId);$k++){
						//debug($multiId[$k]["expiry_date"]."-->".$currentDate);
						if($multiId[$k]["expiry_date"] < $currentDate)
							$idExpiryFlag = true;
				//debug($idExpiryFlag);		
				}
//debug($idExpiryFlag);
return $idExpiryFlag;
}

function futuredate($date, $days) { 
//CODE FROM WWW.GREGGDEV.COM 
/* A MySQL type datetime is inputted (YYYY-MM-DD), a date is calculated that is $days days in the future and converted back to YYYYMMDD format and returned*/ 
    $date = str_replace("-", "", $date); 
    $date = strtotime(substr($date,0,10)); 
    //seconds per day times number of days 
    $date = $date + (86400*$days); 
    $date = date("Y-m-d", $date); 
    return $date; 
} 

function linkAgentList($arguments){
/* # 7888 : premeir exchange - return userID of associated agents */
	$userID = $arguments["userID"];
	$agentData = selectFrom("SELECT userID,linked_Agent FROM ".TBL_ADMIN_USERS." WHERE userID='".$userID."'");
	$linkedStr = $agentData["linked_Agent"];
	$sendStr ='';
	if(!empty($linkedStr)){
		$arrLinkAgents = explode(",",$linkedStr);
		//debug($arrLinkAgents);
		/**
		 *  @var $arrOfLinkAgents type array
		 *  This array stores the exploded linked agents
		 */
		$arrOfLinkAgents = array();
		$arrLinkAgents = array_filter($arrLinkAgents);//array_merge(array(),$arrLinkAgents);//array_values($arrLinkAgents);//array_filter($arrLinkAgents);
		$arrLinkAgents = array_merge(array(),$arrLinkAgents);
		for($k=0;$k<count($arrLinkAgents);$k++)
		{
		  $strAgentSql = selectFrom("SELECT userID FROM ".TBL_ADMIN_USERS." WHERE username='".$arrLinkAgents[$k]."'");
		  if($strAgentSql["userID"]!='')
		  {
		  	$getUserId = $strAgentSql["userID"];
				$sendStr .="'$getUserId'".",";
		  }
		 
		}
		$sendStr = substr_replace($sendStr,"",-1);
	}
	
	 return $sendStr;
}

/**
 * Function Name    deComposeFullNameIntoParts
 * @param $strFullName type string
 * Parameter Description
 * The parameter $strFullName contains the full name which is later decomposed  
 * Method Description
 * To save into database, need to decompse full name into name parts, 
 * like firstname, middle name and last name
 * @return array containing decomposed name or false
 */
function deComposeFullNameIntoParts($strFullName)
{
	/**
	 * @var $arrNameParts type array 
	 * This array stores the decomposed parts of the name
	 */
	$arrNameParts = array();
	/**
	 *  Initializing the array $arrNameParts  
	 */
	$arrNameParts["firstName"]  = "";
	$arrNameParts["middleName"] = "";
	$arrNameParts["lastName"]   = "";
	
	if(empty($strFullName))
		return $arrNameParts;
	/**
	 * @var $arrParts type array
	 * Array storing the result of the name exploded 
	 */
	$arrParts = explode(" ", trim($strFullName));
	
	/**
	 * @var $intTotalCount type int 
	 * Variable containing the count of the elements of the
	 * array of name parts
	 */
	$intTotalCount = count($arrParts);
	
	if($intTotalCount > 2)
	{
		/** 
		 * get all the middle name into middleName
		 * put first index and last index into 
		 */
		 
		for($i=1; $i < ($intTotalCount-1); $i++)
			$arrNameParts["middleName"] .= $arrParts[$i]." ";
		
		$arrNameParts["middleName"] = trim($arrNameParts["middleName"]);
		$arrNameParts["firstName"]  = $arrParts[0];
		$arrNameParts["lastName"]   = $arrParts[$intTotalCount-1];
  }
	else
	{
		$arrNameParts["firstName"]  = $arrParts[0];
		$arrNameParts["middleName"] = "";
		$arrNameParts["lastName"]   = $arrParts[1];
	}
	return $arrNameParts;
}

function getUserDropdownList($arrInput, $value=NULL){
/*	Params:
* $arrInput: arguments array specify which type of dropdown list require.
* $value: this parameter is optioanl it can be use to set flag 1 or 0
* e.g. supper agent , sub agent , destination currency, payment mode etc.
* on the base of dropdown type function will return the fetch records rsult set
* By Niaz Ahmad 28-07-2010
*/
//debug_print_backtrace();
$userFlag = false;
$conditions = '';
$agentType ='';
$orderBy ='';

/* type of user e.g SUPA,SUBA*/
$userType = $arrInput["userType"];	
/*
SUPA = Super Agent
SUBA = Sub Agent
SUPI = Super Distributor
SUBI = Sub Distributor
*/

$format = $arrInput["format"];
if(!empty($format))
	{
		$formatArr = explode(",",$format);
                
		//if(count($formatArr) > 2)
		//{		 
		 if(!empty($formatArr[2]))
		 {
		   $optionVal = $formatArr[2];	
		 }
		 else
		 { 
		  $optionVal = "userID";
		 }	
		//}	
		if(!empty($formatArr[0]) && !empty($formatArr[1])){
			$outerElement = $formatArr[0];
			$innerElement = $formatArr[1];
		} else if(empty($formatArr[0]) && empty($formatArr[1])){
			$outerElement = "agentCompany";
			$innerElement = "username";
 		}else if(empty($formatArr[0]) && !empty($formatArr[1])){
			$outerElement = "agentCompany";
			$innerElement = $formatArr[1];
		}else if(!empty($formatArr[0]) && empty($formatArr[1])){
			$outerElement = $formatArr[0];
			$innerElement = "username";
		}
	
} else {
	//default case
	$outerElement = "agentCompany";
	$innerElement = "username";
        $optionVal = "userID";

}
//debug( $outerElement."-->".$innerElement."-->".$optionVal);
if($userType == 'SUPA' || $userType == 'SUPI'){
  $agentType = 'Supper';
 }elseif($userType == 'SUBA' || $userType == 'SUBI'){
 	$agentType = 'Sub';
 }
 
 if($userType == 'SUPA' || $userType == 'SUBA' || $userType =='SUPA_SUBA'){
    $conditions .= "AND isCorrespondent = 'N'";
 }elseif($userType == 'SUPI' || $userType == 'SUBI' || $userType =='SUPI_SUBI'){
 	$conditions .= "AND isCorrespondent = 'ONLY'";
 }
if($userType == 'SUPA' || $userType == 'SUBA' || $userType == 'SUPI' || $userType == 'SUBI'){
	$userFlag = true;
	$conditions .="AND agentType = '$agentType'";
 }

/* get the Id of logged user */
$loggedUser = $arrInput["loggedUser"];

if(!empty($loggedUser))
{
   $selectUser = selectFrom("SELECT adminType FROM  ".TBL_ADMIN_USERS." WHERE userID = '".$loggedUser."'");
   if($selectUser["adminType"] != 'Supper')
		$conditions .= "AND (userID = '$loggedUser' OR parentID = '$loggedUser')";
}
/* adminType will be comma separed list of user e.g Agent,Branch Manager,Admin Manager etc */
$adminType = $arrInput["adminType"];
if(!empty($adminType))
{
  $arrAdminType = explode(",",$adminType);
  $arrAdminType = array_filter($arrAdminType);
  //debug($arrAdminType);
  //debug(count($arrAdminType));
  if(sizeof($arrAdminType) > 1){
  	$conditions .= "AND adminType IN($adminType)";
	 $orderBy = "ORDER BY adminType,name";
  }else{	
    $conditions .= "AND adminType = $arrAdminType[0]";
	  $orderBy = "ORDER BY agentType,name";
  }
}	

/* list of custome fields e.g ,agentCompany,agentAddress */
$customFields = $arrInput["customFields"];	

/*
  Any extra condition like Brancher Manager can view his associated list only
  format: " AND parentID = '$parentID'" 
*/
$extraCondition = $arrInput["extraCondition"];	

/*
 listType specify return type of function if listType is dd (dropdown) then
 funtion will return dropdown list if listType is arr (array) then system return result set etc
*/
$returnType = $arrInput["returnType"];	
//debug($extraCondition,true );
/*
   pass id in selectedUser param to select the user in option list
*/
$selectedUser = $arrInput["selectedUser"];	
	
/* get supper agent  list */

$all = $arrInput["all"];	
    $strAgent ='';
	$agentSql = "SELECT 
							userID,
							username, 
							adminType,
							agentType,
							agentContactPerson, 
							agentCompany,
							isCorrespondent,
							name
							".$customFields."
						 FROM 
							".TBL_ADMIN_USERS."
						 WHERE 
							agentStatus='Active'
							".$conditions."
							".$extraCondition."
						";
			//		debug($agentSql,true);
				$agentSql .="$orderBy";	
				$agentResult = SelectMultiRecords($agentSql);
				
				if($all == 'all')
				$strAgent  .= '<option value="all" > All </option>';
				
				for ($a=0; $a < count($agentResult); $a++)
						{
						if($agentResult[$a]["adminType"] == 'Agent'){
							  if($agentResult[$a]["agentType"] != $agentResult[$a-1]["agentType"])
							  		$strAgent .= "<optgroup label='".$agentResult[$a]["agentType"]."' '".$agentResult[$a]["adminType"]."'>";
							 }else{ 
									if($agentResult[$a]["adminType"] != $agentResult[$a-1]["adminType"])
										$strAgent .= "<optgroup label='".$agentResult[$a]["adminType"]."'>";
							}	
								$agentName = $agentResult[$a]["name"]; 
								$agentUsername  = $agentResult[$a]["username"]; 
								$agentContactPerson = $agentResult[$a]["agentContactPerson"]; 
								//if($agentResult[$a]["userID"] == $selectedUser)
								$selected ='';
								if(strstr($selectedUser,$agentResult[$a][$optionVal]))
								   $selected = 'selected="selected"';
								
					$strAgent .='<option value="'.$agentResult[$a][$optionVal].'" '.$selected.'>'.$agentResult[$a][$outerElement].
						   " [".$agentResult[$a][$innerElement]."]".'</option>';
						 
						if($agentResult[$a]["adminType"] == 'Agent'){
							if($agentResult[$a]["agentType"] != $agentResult[$a+1]["agentType"])
								$strAgent .= "</optgroup>";  
						}else{
						 if($agentResult[$a]["adminType"] != $agentResult[$a+1]["adminType"])
								$strAgent .= "</optgroup>";  
						}		
					  }
  	if($returnType == 'dd'){				   
		return $strAgent;
	}elseif($returnType == 'arr'){
		return $agentResult;
    }else{
		return $strAgent; // by default funtion will return dropdown
	}	
}



function getUserDropdown($arrArgoument=array())
{
	/**	To built Dropdrown menu off all user
	 * 	This function make a dropdown menu of users from admin table by default at will get all users from admin table
	 * 	@pram
	 *		$arrArgoument["extraCondition"]	=> to filter the user instaned to get all user from admin table
	 *		$arrArgoument["orderBy"]		=> ORDER BY CLAUSE to order the result
	 *		$arrArgoument["format"]			=> Foramt to dispaly velues in dropdown menu. foramt value will be in the form of i.e. name,username which will make a foramt like name [username]
 	 *		$arrArgoument["all"]			=> To show all option in dropdown it value will be true or false
	 *	@return $strFullHTML containing droupdown mwnu
	 *	@author	Kalim
	 */
	
	$arrUser		= array();
	$arrUserData	= array();
	
	$strUserQuery = "	SELECT  
							userID,  
							name,
							username,
							adminType, 
							agentType,
							parentID,
							isCorrespondent,
							agentStatus,
							agentCompany,
							agentContactPerson
						FROM  
							admin 
						WHERE 1 
					";
	if(!empty($arrArgoument["extraCondition"]))	
		$strUserQuery .= " ".$arrArgoument["extraCondition"]." ";
	if(!empty($arrArgoument["orderBy"]))
		$strUserQuery .= " ".$arrArgoument["orderBy"]." ";
	else
		$strUserQuery .= " ORDER BY CONCAT(name) ";
		
	if(!empty($arrArgoument["format"])){
		$arrFormat = explode(",",$arrArgoument["format"]);
	}
	
	$arrUserData = selectMultiRecords($strUserQuery);

	foreach($arrUserData as $key=>$val)
	{
		$strAdminType		= $val["adminType"];
		$strAgentType		= $val["agentType"];
		$strIsCorrespondent	= $val["isCorrespondent"];
		$strAgentStatus		= $val["agentStatus"];
		
		if($strAdminType == "Agent")
		{
			if($strIsCorrespondent != "ONLY")
			{
				if($strAgentType == "Supper"){
					$arrUser["Supper Agents - ".$strAgentStatus][]	=	array(	"userID"			=>	$val["userID"],
																				"name"				=>	$val["name"],
																				"parentID"			=>	$val["parentID"],
																				"username"			=>	$val["username"],
																				"agentCompany"		=>	$val["agentCompany"],
																				"agentContactPerson"=>	$val["agentContactPerson"],
																				"adminType"			=>	$val["adminType"]
																			);
				}else{
					$arrUser["Sub Agents - ".$strAgentStatus][]	=	array(	"userID"			=>	$val["userID"],
																			"name"				=>	$val["name"],
																			"parentID"			=>	$val["parentID"],
																			"username"			=>	$val["username"],
																			"agentCompany"		=>	$val["agentCompany"],
																			"agentContactPerson"=>	$val["agentContactPerson"],
																			"adminType"			=>	$val["adminType"]
																		);
				}
			}
			else
			{
				if($strAgentType == "Supper"){
					$arrUser["Supper Distributors - ".$strAgentStatus][]	=	array(	"userID"			=>	$val["userID"],
																						"name"				=>	$val["name"],
																						"parentID"			=>	$val["parentID"],
																						"username"			=>	$val["username"],
																						"agentCompany"		=>	$val["agentCompany"],
																						"agentContactPerson"=>	$val["agentContactPerson"],
																						"adminType"			=>	$val["adminType"]
																					);
				}else{
					$arrUser["Sub Distributors - ".$strAgentStatus][]	=	array(	"userID"			=>	$val["userID"],
																					"name"				=>	$val["name"],
																					"parentID"			=>	$val["parentID"],
																					"username"			=>	$val["username"],
																					"agentCompany"		=>	$val["agentCompany"],
																					"agentContactPerson"=>	$val["agentContactPerson"],
																					"adminType"			=>	$val["adminType"]
																				);
				}		
			}
		}
		else
		{
			$arrUser[$strAdminType." - ".$strAgentStatus][]	=	array(	"userID"			=>	$val["userID"],
																		"name"				=>	$val["name"],
																		"parentID"			=>	$val["parentID"],
																		"username"			=>	$val["username"],
																		"agentCompany"		=>	$val["agentCompany"],
																		"agentContactPerson"=>	$val["agentContactPerson"],
																		"adminType"			=>	$val["adminType"]
																	);
		}
	}
	
	$strFullHTML = "<select class='required' id='userID' name='userID' style='width:200'>";
		$strFullHTML .= "<option value=''>- Select One -</option>";
	if($arrArgoument["all"])
		$strFullHTML .= "<option value='all'>All</option>";
		
	foreach($arrUser as $keyV=>$valV)
	{
		$strFullHTML .= "<optgroup label='".$keyV."'>";
		foreach($valV as $key=> $val)
		{
			if(!empty($val[$arrFormat[0]])){
				$srtDisplay	=	$val[$arrFormat[0]];				
				if(!empty($arrDisplay[1]))
					$srtDisplay	.=	" [".$val[$arrFormat[1]]."]";
			}else{
				if(strstr($keyV,"Supper Agent") || strstr($keyV,"Sub Agent"))
					$srtDisplay = $val["agentContactPerson"]." [".$val["username"]."]";
				elseif(strstr($keyV,"Supper Distributors") || strstr($keyV,"Sub Distributors"))
					$srtDisplay = $val["agentCompany"]." [".$val["username"]."]";
				else
					$srtDisplay = $val["name"]." [".$val["username"]."]";
			}
			
			$strFullHTML .= "<option value='".$val["userID"]."'>".$srtDisplay."</option>";	
		}
		$strFullHTML .= "</optgroup>";
	}
	return $strFullHTML;
}
?>
