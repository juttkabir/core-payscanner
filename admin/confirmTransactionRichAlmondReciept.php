<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentType=getAgentType();
$loggedUName = $_SESSION["loggedUserData"]["username"];
$returnPage = 'add-transaction.php';

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";

$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);


if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

if($_GET["transID"] != ""){
		
		$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$_GET["transID"]."'");
		$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$_GET["transID"]."'");
	}
	
if($_POST["customerID"]!= ""){
	$customerID = $_POST["customerID"];
	}else{
	$customerID = $transactionQuery["customerID"];
}

if($_POST["benID"]!= ""){
	$beneficiaryID = $_POST["benID"];
	}else{
	$beneficiaryID = $transactionQuery["benID"];
}





if($_POST["customerAgent"]!= ""){
	$custAgentID = $_POST["customerAgent"];
	}else{
	$custAgentID = $transactionQuery["custAgentID"];
}

		$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$custAgentID."'");


if($_POST["totalAmount"]!= ""){
$totalAmount= $_POST["totalAmount"];
}else{
$totalAmount=$transactionQuery["totalAmount"];	
	}
	
	
if($_POST["transAmount"]!= ""){
$transAmount= $_POST["transAmount"];
}else{
$transAmount=$transactionQuery["transAmount"];	
	}	
	
	if($_POST["localAmount"]!= ""){
$localAmount= $_POST["localAmount"];
}else{
$localAmount=$transactionQuery["localAmount"];	
	}
	



if($_POST["moneyPaid"]!= ""){
$moneyPaid= $_POST["moneyPaid"];
}else{
$moneyPaid=$transactionQuery["moneyPaid"];	
	}
	
	
if($_POST["transactionPurpose"]!= ""){
$transactionPurpose= $_POST["transactionPurpose"];
}else{
$transactionPurpose=$transactionQuery["transactionPurpose"];	
	}



$discount=$transactionQuery["discounted_amount"];	



if($_POST["exchangeRate"]!= ""){
$exchangeRate= $_POST["exchangeRate"];
}else{
$exchangeRate=$transactionQuery["exchangeRate"];	
	}


if($_POST["IMFee"]!= ""){
$IMFee= $_POST["IMFee"];
}else{
$IMFee=$transactionQuery["IMFee"];	
	}



if($_POST["bankName"]!= ""){
$bankName= $_POST["bankName"];
}else{
$bankName=$bankQuery["bankName"];	
	}

if($_POST["branchCode"]!= ""){
$branchCode= $_POST["branchCode"];
}else{
$branchCode=$bankQuery["branchCode"];	
	}

if($_POST["branchAddress"]!= ""){
$branchAddress= $_POST["branchAddress"];
}else{
$branchAddress=$bankQuery["branchAddress"];	
	}


if($_POST["swiftCode"]!= ""){
$swiftCode= $_POST["swiftCode"];
}else{
$swiftCode=$bankQuery["swiftCode"];	
	}



if($_POST["accNo"]!= ""){
$accNo= $_POST["accNo"];
}else{
$accNo=$bankQuery["accNo"];	
	}




if($_POST["currencyTo"]!= ""){
$currencyTo= $_POST["currencyTo"];
}else{
$currencyTo=$transactionQuery["currencyTo"];	
	}
	
	
	
if($_POST["currencyFrom"]!= ""){
$currencyFrom= $_POST["currencyFrom"];
}else{
$currencyFrom=$transactionQuery["currencyFrom"];	
	}
	
	
	if($_POST["benCountry"]!= ""){
$ToCountry= $_POST["benCountry"];
}else{
$ToCountry=$transactionQuery["toCountry"];	
	}
	
	
	if($_POST["transType"] != ""){
		
		$transactionType = $_POST["transType"];
		} else{
			$transactionType = $transactionQuery["transType"];	
			}
			

if($_POST["bankCharges"] != ""){
		
		$bankCharges = $_POST["bankCharges"];
		} else{
			$bankCharges = $transactionQuery["bankCharges"];	
			}
	
	$discount=$transactionQuery["discounted_amount"];	
	
$refNumberIM=$transactionQuery["refNumberIM"];

$printAddress = $strAddressQuery["agentAddress"];
$printPhone = $strAddressQuery["agentPhone"];
$printSenderNo = $CustomerData["accountName"];
$agentUserName = $strAddressQuery["username"];
$agentName = $strAddressQuery["name"];
$custAgentParentID = $strAddressQuery["parentID"];


if($_POST["benAgentID"] != ""){
		
			$distributorId = $_POST["benAgentID"];
} else{
			$distributorId = $transactionQuery["benAgentID"];
}

$distributorNameQuery = selectFrom("select name from ". TBL_ADMIN_USERS." where userID ='".$distributorId."'");
$distributorName = $distributorNameQuery["name"];

$currencyNameQuery = selectFrom("select description from ". TBL_CURRENCY." where currencyName ='".$currencyTo."'");
$currencyName = $currencyNameQuery["description"];


$fontColor = "#800000";

if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");


           
            		 $to = getCountryTime(CONFIG_COUNTRY_CODE);
									
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
									$noOfDays = CONFIG_NO_OF_DAYS;
									
									$fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));

									
									$from = $fromDate." 00:00:00"; 
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								// echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}
	

?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">

<script language="javascript">

	function showDetails() {
		 
			alert("disable");
			document.addTrans.Submit.Disabled= "disabled";
	}
	

function checkForm(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;
	var accumulativeAmount = 0;
	var ruleFlag=false;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
			
		
	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	var amountToCompareTrans; 
	var transAmount=<?=$_POST["transAmount"]?>;		

// cummulative amount/////////
	<?
	if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		
		
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		    $fromAmount=$betweenAmount[0];   
		    $toAmount=$betweenAmount[1]; 
		
 }else{
 	?>
 		    amountToCompare = <?=$cumulativeRule["amount"]?>;	
 <?	}?>
  	
  	////////////current transaction amount///////
  	
  		<?
	if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		
 }else{
 	?>
 		    amountToCompareTrans = <?=$currentRule["amount"]?>;	
 <?	}?>

		
		<?
	 if($cumulativeRule["matchCriteria"] != "BETWEEN"){	
	 	?>
		 if(accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();
        ruleFlag=false;
      }

    }else{ 

    	   ruleFlag=true;
    } 
 <? } ?>
 ///////BETWEEN Accumulative Amount/////////////////
 <?
 if($cumulativeRule["matchCriteria"] == "BETWEEN"){
 	?>
 if(<?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		 
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();
         ruleFlag=false;
      }

    }else{ 

    	    ruleFlag=true;
    } 
<? }  ?>

 ///Second Rule Compare transAmount with compliance rule amount  /////////////////////////////////
 <?
 if($currentRule["matchCriteria"] != "BETWEEN"){
 ?>
 if(transAmount  <?=$currentRule["matchCriteria"]?> amountToCompareTrans)
	   {

				
	   	if(confirm("<?=$currentRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				
      	addTrans.localAmount.focus();
        ruleFlag=false;
      }

    }else{ 

    	     ruleFlag=true; 
    } 
   <? } ?> 
 <?
  ///////BETWEEN Current Transaction/////////////////
 if($currentRule["matchCriteria"] == "BETWEEN"){
 	?>
 if(<?=$fromAmountCurrent?> <= transAmount && transAmount <= <?=$toAmountCurrent?>)
	   {

				
	   	if(confirm("<?=$currentRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
             
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	   addTrans.localAmount.focus();
           ruleFlag=false;
      }

    }else{ 

    	  	   ruleFlag=true;

    } 
<? }  ?>
   
   if(ruleFlag){
   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   	}
	
	
}		
	
	</script>





<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 
  <? if ($_GET["msg"] == "Y"){ ?>
  
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
              	
                <td width="40" align="center"><div class='noPrint'><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></div></td>
                <td width="635"><div class='noPrint'><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></div></td>
                 
              </tr>
            </table></td>
          </tr>
       
		  <?
		  }
		  ?>

   <tr>
      <td align="center"><br>
        <table width="100%" border="0" bordercolor="#FF0000">
         
          
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" >
            	
            	<? 	if($custAgentID!='')
												{
													$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where username ='" . $loggedUName . "'";
														$custAgent=$custAgentID;
														$senderAgentContent = selectFrom($querysenderAgent);
													} 
													
							if($senderAgentContent["logo"] != "" )
																{
																	
																							$arr = getimagesize( $senderAgentContent["logo"]);
																							
																							$width = ($arr[0] > 200 ? 200 : $arr[0]);
																							$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            
              <tr>
                <td align="center" bgcolor="#ffffff" align="center" colspan="4"><img src="<? echo $senderAgentContent["logo"]?>" width="<? echo CONFIG_LOGO_WIDTH?>" height="<? echo CONFIG_LOGO_HEIGHT?>"></td>
              </tr>
           
            <?
	}
	else
	{
	?>
            	
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#ffffff" align="center" colspan="4"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO ?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
         <? } ?>     
              <tr  bgcolor="#ffffff">
              	 <td width="100%" align="center" colspan="4">
            	Address:
            	<? echo(COMPANY_ADDR); ?><br>
            
            </td>
          <tr><td width="100%" align="center" colspan="4" bgcolor="#ffffff"><font size="3" face="Arial"><strong>Customer Transaction Receipt</strong></font>
          	<br><br>
          	</td></tr>
             
              <tr width="100%">
              	<td width="100%" colspan="4" bgcolor="#ffffff">
              		
              	<table width="100%" border="0" cellspacing="1">
  <tr>
    <th colspan="2" align="left" width="35%"><font size="2" face="Arial" color="#000000">REMITTANCE DETAILS</font></th>
    <th colspan="2"  align="left" width="35%"><font size="2" face="Arial" color="#000000">AGENT'S DETAILS</font></th>
    <th colspan="2"  align="left" width="30%"><font size="2" face="Arial" color="#000000">SATELLITE'S DETAILS</font></th>
  </tr>
  <tr>
    <th colspan="2">&nbsp;</th>
    <th colspan="2">&nbsp;</th>
    <th colspan="2">&nbsp;</th>
  </tr>
  <tr>
    <td width="20%"><font color="<? echo $fontColor ?>" >REMITTANCE NO:</td>
    <td align="left"><font color="#000000" ><? echo $refNumberIM; ?></td>
    <td width="12%"><font color="<? echo $fontColor ?>" >AGENCY CODE:</td>
    <td align="left"><font color="#000000" >[<? echo $agentUserName;  ?>]</td>
    <td width="12%"><font color="<? echo $fontColor ?>" >SATELLITE:</td>
    <td align="left"><font color="#000000" ><? echo $distributorName; ?></td>
  </tr>
   <tr>
    <td width="12%"><font color="<? echo $fontColor ?>" >REMITTANCE DATE:</td>
    <td align="left"><font color="#000000" ><? echo  $today ;?></td>
    <td width="12%"><font color="<? echo $fontColor ?>" >&nbsp;</td>
    <td><font color="<? echo $fontColor ?>" >&nbsp;</td>
    	<? if($transactionType== 'Pick up'){
	              			$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$_SESSION["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);?>   	
    <td width="20%"><font color="<? echo $fontColor ?>" >Collection point: </td>
  <td><font color="#000000" ><? echo $destinCollection["cp_corresspondent_name"];?><br> <? echo $destinCollection["cp_branch_address"];?></td> 
  	<? }elseif($transactionType== 'Bank Transfer'){?>
  	<td width="20%"><font color="<? echo $fontColor ?>" >BANK TRANSFER: </td>
  <td><font color="#000000" ><? echo $bankName;?><br> <? echo $branchAddress;?></td> 
  	<? }elseif($transactionType== 'Home Delivery'){ ?>
  	
  	<td width="20%"><font color="<? echo $fontColor ?>" >HOME DELIEVERY: </td>
  <td><font color="#000000" >&nbsp; </td> <? } ?> 	
  	
  	
  	
  </tr>
   <tr>
    <td width="12%"><font color="<? echo $fontColor ?>" >DESTINATION: </td>
    <td><font color="#000000" ><? echo  $ToCountry; ?></td>
    <td width="12%"><font color="<? echo $fontColor ?>" >AUTHORISATION:</td>
    <td><font color="#000000" ><? echo $loggedUName; ?></td>
    <td width="12%"><font color="#000000" >&nbsp;</td>
    <td><font color="#000000" >&nbsp;</td>
  </tr>
</table>
              		
              		</td>
              	</tr>
              	
              	
              	
            
             <tr bgcolor="#ffffff">
              	 <td width="100%" colspan="4">
              	 	<table width="100%" border="0" cellspacing="1">
              	 		<tr colspan="8"><td>&nbsp;</td></tr>
              	 		<tr colspan="8"><td>&nbsp;</td></tr>
              	 		<tr colspan="8"><td>&nbsp;</td></tr>
  <tr>
    <th colspan="4" scope="col" align="left" width="50%"><font color="#000000" size="2" face="Arial"><b>SENDER'S DETAILS</th>
    <th colspan="4" scope="col" align="left" width="50%"><font color="#000000" size="2" face="Arial"><b>RECIEPENT'S DETAILS</th>
  </tr>
  <tr colspan="8"><td>&nbsp;</td></tr>
  <tr>
  	 <? 
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, email  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
			$customerContent = selectFrom($queryCust);
			
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, email,Mobile from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($customerContent["customerID"] != "" || $benificiaryContent["benID"] != "")
			{
		?>
    <td width="12%"><font color="<? echo $fontColor ?>" >SURNAME</td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
    <td width="20%"><font color="<? echo $fontColor ?>" >FIRST NAME,INITIALS: </td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
    <td width="12%"><font color="<? echo $fontColor ?>" >SURNAME</td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
    <td width="20%"><font color="<? echo $fontColor ?>" >FIRST NAME,INITIALS: </td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
  </tr>
  
  <tr>
  	<td width="9%"><font color="#000000" ><? echo $customerContent["lastName"] ?></td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
    <td width="20%"><font color="#000000" ><? echo $customerContent["firstName"] .",".  substr($customerContent["middleName"],0,1);?> </td>
    <td width="9%"><font color="#000000" >&nbsp;</td>   
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["lastName"]?></td>
    	<td width="9%"><font color="#000000" >&nbsp;</td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["firstName"].",".  substr($benificiaryContent["middleName"],0,1);?></td>
    <td width="9%"><font color="#000000" >&nbsp;</td>
  </tr>
  	 
   <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >HOUSE NO: </td>
    <td width="12%"><font color="#000000" ><? echo $customerContent["Address"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >HOUSE NO: </td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["Address"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >STREET NAME: </td>
    <td width="9%"><font color="#000000" ><? echo $customerContent["Address1"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >STREET NAME: </td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["Address1"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
     <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >TOWN/CITY:  </td>
    <td width="9%"><font color="#000000" ><? echo $customerContent["City"] ?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >TOWN/CITY:  </td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["City"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
     <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >POSTCODE:  </td>
    <td width="9%"><font color="#000000" ><? echo $customerContent["Zip"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >POSTCODE:  </td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["Zip"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
     <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >TELEPHONE: </td>
    <td width="9%"><font color="#000000" ><? echo $customerContent["Phone"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >MOBILE: </td>
    <td width="9%"><font color="#000000" ><? echo $benificiaryContent["Mobile"]?></td>
    	<td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="9%"><font color="<? echo $fontColor ?>" >EMAIL: </td>
    <td width="9%" colspan="2"><font color="#000000" ><? echo $customerContent["email"]?></td>
    	
    <td>&nbsp;</td>
    <td width="9%"><font color="<? echo $fontColor ?>" >EMAIL: </td>
    <td width="9%" colspan="2"><font color="#000000" ><? echo $benificiaryContent["email"]?></td>
    	
    <td>&nbsp;</td>
  </tr>
  <? } ?>
</table>

</td>
  </tr>
  <tr bgcolor="#ffffff" width="100%">
  	<td width="100%" colspan="4">
  		
  		
  		<table width="100%" border="0" cellspacing="1">
 <tr colspan="8"><td>&nbsp;</td></tr>
  <tr>
    <th colspan="6" scope="col" align="left"><font size="2" face="Arial" color="#000000">TRANSACTION DETAILS</font></th>
  </tr>
  <tr colspan="8"><td>&nbsp;</td></tr>
  <tr>
    <td width="15%"><font color="#800000" >AMOUNT: </td>
    <td ><? echo number_format($transAmount,$roundLevel,'.',','); ?></td>
    <td width="15%"><font color="<? echo $fontColor ?>" >EXCHANGE RATE: </td>
    <td><? echo $exchangeRate; ?> </td>
	
    <td width="15%"><font color="<? echo $fontColor ?>" >CURRENCY NAME: </td>
    <td><font color="#000000"><b><? echo $currencyName ;?></font></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td width="15%"><font color="<? echo $fontColor ?>" >COMMISSION </td>
    <td ><? echo $IMFee ;?></td>
    <td width="15%"><font color="#000000" >&nbsp;</td>
    <td>&nbsp;</td>
	
    <td width="20%"><font color="#000000"><b>AMOUNT RECIEVEABLE: </td>
    <td><? echo number_format($localAmount,$roundLevel,'.',','); ?></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td width="15%"><font color="<? echo $fontColor ?>" >DELIVERY CHARGES: </td>
    <td ><? echo $bankCharges; ?></td>
    <td width="15%"><font color="<? echo $fontColor ?>" >DISCOUNT:</td>
    <td><? echo $discount; ?></td>
	
    <td width="15%"><font color="#000000" ><b>TOTAL AMOUNT DUE</td>
    <td><? echo number_format($totalAmount,$roundLevel,'.',','); ?> </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
   <tr>
    <td width="15%"><font color="<? echo $fontColor ?>" >OTHER CHARGES:</td>
    <td >&nbsp;</td>
    <td width="15%"><font color="<? echo $fontColor ?>" >&nbsp;</td>
    <td>&nbsp;</td>
	
    <td width="15%"><font color="<? echo $fontColor ?>" >BANK REFERENCE: </td>
    <td><? echo $refNumberIM; ?></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
   
</table>
  	</td>
  </tr>
  
           
            
          
            </table>
            
          
            <br>
            </fieldset></td>
           
          </tr>         
        
           <tr>
           	<td>
		           		<table width="100%" border="0" cellspacing="1">
		           				<tr>
		           			<td>&nbsp;</td>
		           			<td>&nbsp;</td>
		          			</tr>
		          			<tr>
		           			<td>&nbsp;</td>
		           			<td>&nbsp;</td>
		          			</tr>
		           			<tr>
		           			<td><font color="#000000" >______________________</td>
		           			<td align="right"><font color="#000000" >______________________</font></td>
		          			</tr>
		          		
		           			<tr>
		           			<td><font color="#000000" >Agent/Official Signature</font></td>
		           			<td align="right"><font color="#000000" >Customer Signature</td>
		          			</tr>
		            	</table>
            </td>
            </tr>
            		<tr>
            <td align="center">
         <form name="addTrans" action="add-transaction-conf.php?r=<?=$_GET["r"]?>" method="post">
        <table width="100%" border="0" cellspacing="1">
        	<tr><td align="center"> 	
		<div class='noPrint'>
			
           <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$refNumberIM;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">	
					<input name="transID" type="hidden" value="<? echo $_POST["transID"];?>">			
					<input name="custAgentID" type="hidden" value="<? echo $_POST["custAgentID"];?>">				            
           <input name="benAgentID" type="hidden" value="<? echo $_POST["benAgentID"];?>">	
          	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;//window.open('printing_reciept.php?transID=< echo $imReferenceNumber; >', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')

	  ?>
	  		
			<? if($success=='Y'){
				
				include "mailRecieptRichAlmond.php";
				$To = $customerContent["Email"];
				
			 $Subject = "New Transaction created";
			 $Name = $customerContent["firstName"];
		 $From = SUPPORT_EMAIL;
				sendMail($To,$Subject,$data,$From, $From);
				?>
	
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
			
		</div>
	</td>
		<? }else{ ?>
	<input type="Submit" name="confirmOrder" value="Confirm Order" id="once" onclick="javascript:document.getElementById('once').disabled=true;document.addTrans.submit();<? if(CONFIG_COMPLIANCE_PROMPT_CONFIRM== "1"){ ?> checkForm(addTrans,'yes')<? } ?>" >

<a href="<?=$returnPage?>?transID=<? echo $_POST["transID"]?>&from=conf" class="style2">Change Information</a>
</td>
		<? } ?>
		</tr>
			</table>
         </form>
			</td>
          </tr>
            </table>

           
            
            </td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>