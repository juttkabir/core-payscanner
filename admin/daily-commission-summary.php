<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
if(CONFIG_REPORTS_SURCHARG == '1')
{
	$extra = ", sum(bankCharges) as extra1, sum(cashCharges) as extra2, sum(admincharges) as extra3, sum(outCurrCharges) as extra4";	
}else{
	$extra = "";
	}
$query = "select transId,date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate, date_format(transDate, '%D %b %Y') as tDate, count(transID) as cnt, sum(IMFee) as totFee, sum(transAmount) as totAmount, sum(totalAmount) as grossAmount, sum(AgentComm) as agentComm $extra from ". TBL_TRANSACTIONS . " as t";

if(CONFIG_CANCEL_DAILY_COMM)
{
	$cancelDexcription = " and (ac.description like '%Transaction Cancelled%' OR ac.description like '%Transaction Failed%' OR ac.description like '%Transaction Rejected%')";
	$queryCancel = "select date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate, dated as accountDate, sum(amount) as cancelAmount, count(t.transID) as cnt, sum(totalAmount) as grossAmount, sum(AgentComm) as agentComm  from ". TBL_TRANSACTIONS . " as t, agent_account as ac";
}

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


if($_POST["Submit"] =="Search")
{


	$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
	$toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
	if(CONFIG_CANCEL_DAILY_COMM)
	{
		$cancelDate = " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
	}
	$queryDate = " and (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	if($_POST["fieldName"] != "")
	{
	//	echo("Value in the field is ".$_POST["fieldName"]);
		switch ($_POST["fieldName"])
		{         
			case "superAgent":
			{
				$query .= ", " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Supper' and ( a.name like '%" . $_POST["txtSearch"] . "%' OR a.username like '%" . $_POST["txtSearch"] . "%') ";				
				if(CONFIG_CANCEL_DAILY_COMM)
				{
					$queryCancel .= ", " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Supper' and ( a.name like '%" . $_POST["txtSearch"] . "%' OR a.username like '%" . $_POST["txtSearch"] . "%') ";				
				}
				break;
			}
			case "customer":
			{
				$query .= ", " . TBL_CUSTOMER . " as c where t.customerID = c.customerID and (c.firstName like '%" . $_POST["txtSearch"] . "%' OR c.lastName like '%" . $_POST["txtSearch"] . "%') ";								
				if(CONFIG_CANCEL_DAILY_COMM)
				{
					$queryCancel .= ", " . TBL_CUSTOMER . " as c where t.customerID = c.customerID and (c.firstName like '%" . $_POST["txtSearch"] . "%' OR c.lastName like '%" . $_POST["txtSearch"] . "%') ";
				}
				break;
			}
			case "Beneficiary":
			{
				$query .= ", " . TBL_BENEFICIARY . " as b where t.benID = b.benID and (b.firstName like '%" . $_POST["txtSearch"] . "%' OR b.lastName like '%" . $_POST["txtSearch"] . "%') ";								
				if(CONFIG_CANCEL_DAILY_COMM)
				{
					$queryCancel .= ", " . TBL_BENEFICIARY . " as b where t.benID = b.benID and (b.firstName like '%" . $_POST["txtSearch"] . "%' OR b.lastName like '%" . $_POST["txtSearch"] . "%') ";
				}
				break;
			}
		
			
		
		}
		
		if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
		{
			 $queryReverse = $query;
			//echo("<br>");
			$queryReverse .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
		}	
		if(CONFIG_CANCEL_DAILY_COMM)
		{
			$queryCancel .= " and ac.agentID = t.custAgentID and ac.type = 'DEPOSIT' $cancelDexcription  and t.transID = ac.TransID";
			$queryCancel .= " $cancelDate";
			$query .= " $queryDate";
		}else{
			$query .= " $queryDate";
			}
		
	}
	else
	{
		if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
		{
			 $queryReverse = $query;
			//echo("<br>");
			$queryReverse .= " where (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
		}	
		
		
		if(CONFIG_CANCEL_DAILY_COMM)
		{
			
			$queryCancel .= " where ac.agentID = t.custAgentID and ac.type = 'DEPOSIT' $cancelDexcription and t.transID = ac.TransID";
			$queryCancel .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
			
		}
		
		
		$query .= " where (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	}
	
	if($_POST["transStatus"] != "")
	{
		if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
		{
			$queryReverse .= " and t.transStatus='".$_POST["transStatus"]."' ";
		}
		$query .= " and t.transStatus='".$_POST["transStatus"]."' ";
	}
	else
	{
		//$query .= " and t.transStatus='Confirmed' ";	
	}
	
}
else
{
	
	$todate = date("Y-m-d");
	if(CONFIG_CANCEL_DAILY_COMM)
		{
			$queryCancel = $query;
			$queryCancel .= " where t.cancelDate >= '$todate'";
		}
	if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
		{
			$queryReverse = $query;
			$queryReverse .= " where t.cancelDate >= '$todate' ";
		}
	
	$query .= " where t.transDate >= '$todate'";
	
//	$query .= " where t.transDate >= '$todate' and t.transStatus='Confirmed'";
}

if($agentType == "Branch Manager")
{
		if(CONFIG_CANCEL_DAILY_COMM)
		{
			 $queryCancel .= " and t.custAgentParentID ='$parentID' ";
		}
	$query .= " and t.custAgentParentID ='$parentID' ";				
} 	


if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
{
	$queryCancel2 = $queryReverse; 
	 $queryCancel2 .= " and t.transStatus ='Cancelled' and t.refundFee ='No'";
	 $queryCancel2 .= " group by date_format(t.cancelDate, '%D %b %Y') order by t.cancelDate DESC";
	 $cancelData2 = selectMultiRecords($queryCancel2);
	
	
	$queryCancel3 = $queryReverse; 
	 $queryCancel3 .= " and (t.transStatus ='Cancelled' and (t.refundFee ='Yes' or t.refundFee = ''))";
	 $queryCancel3 .= " group by date_format(t.cancelDate, '%D %b %Y') order by t.cancelDate DESC";
	 $cancelData3 = selectMultiRecords($queryCancel3);
}



 if(CONFIG_CANCEL_DAILY_COMM)
 {
	$queryCancel .= " group by date_format(t.cancelDate, '%D %b %Y') order by t.cancelDate DESC";
	
	$contentsCancel = selectMultiRecords($queryCancel);
 }
 
 if(CONFIG_EXCLUDE_CANCEL_DAILY_COMM == '1')
 {
 	$excludeCancelled = $query;
 	$excludeCancelled .= " and t.transStatus = 'Cancelled' and t.refundFee = 'No'";
	$excludeCancelled .= " group by date_format(transDate, '%D %b %Y') order by transDate DESC";
	$contentsExcludeCancel = selectMultiRecords($excludeCancelled);
	 
 	$query .= " and t.transStatus != 'Cancelled'";
 	
 }
 	
 $query .= " group by date_format(transDate, '%D %b %Y') order by transDate DESC";



$contentsTrans = selectMultiRecords($query);
//echo("query is". $query);
//echo("\n\r <br> Reverse Query1 is". $queryCancel2);
//echo("\n\r <br> Reverse Query2 is". $queryCancel3);
//echo("\n\r <br> Query Cancel is". $queryCancel);

//echo $query .  count($contentsTrans);
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2">Daily Commission Report </font></strong><font color="#000000" size="2"></font> </td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="666666">
        <form action="daily-commission-summary.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                <br>
              <br>	
        <input name="txtSearch" type="text" id="txtSearch">
        <select name="fieldName" id="fieldName">
          <option value="">- Search By </option>
          <option value="superAgent"> Agent</option>
          <option value="customer">Sender</option>
          <option value="Beneficiary">Beneficiary</option>
          
        </select>
		<script language="JavaScript">SelectOption(document.Search.txtSearch, "<?=$_POST["txtSearch"]; ?>");</script>
        
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="666666">
        <form action="daily-commission-summary.php?action=<? echo $_GET["action"]?>" method="post" name="trans">

	    <tr>
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($contentsCancel) > 0 || count($contentsExcludeCancel) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1">Transaction Amount(Net) </span></td>
			  <td width="100"><span class="style1"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></span></td>
			  <td><span class="style1">Agent Commission </span></td>
			  <td><span class="style1"><?=$company?> Commission</span></td>
			  <td width="50"><span class="style1"><? if(CONFIG_REPORTS_SURCHARG == '1'){echo"Surcharges";} ?>&nbsp;</span></td>
			  <td><span class="style1">Transaction Amount(Gross) </span></td>
			  <td width="120"><span class="style1">Total Transactions </span></td>
			  <?
			  if(CONFIG_CANCEL_DAILY_COMM == '1')
			  {
			  	?>
			  	<td><span class="style1">Cancelled Amount </span></td>
			  	<td><span class="style1">Cancelled Transactions </span></td>
			  	<?
				}
			  ?>
			  <td align="center">&nbsp;</td>
		  </tr>
		    <?
		    
		    //$transactionCount = count($contentsCancel);
		    //if(count($contentsTrans) > count($contentsCancel))
				
			$grandTotalAmount = 0;
			$grandFee = 0;
			$grandCount = 0;
			$grandSurcharges = 0;
			$grandGrossTotal = 0;
			$grandAgentComm = 0;
			
			
			$transactionCount = count($contentsTrans)+ count($contentsCancel);
			 $recordNormal = 0;
			 $recordCancel = 0;
		     for($i=0;$i < $transactionCount;$i++)
		     {
		     	
		     $totalCalculated = 0;
			 $calculatFee = 0;
			 $counter = 0;
			 $grossTotal = 0;
			 $agentComm = 0;
			
		     	        
		     	        
		     	$dateSelected = $contentsTrans[$recordNormal]["simpleDate"];
		     	$recordNormal++;
		     	
		     	if($dateSelected == '')
		     	{
		     		//echo("Cancel");
		     		$dateSelected = $contentsCancel[$recordCancel]["cancelDate"];
		     		$recordCancel++;
		     		$recordNormal--;
		    	}
		    	if($dateSelected == $contentsCancel[$recordCancel]["cancelDate"])
				{
				$recordCancel++;
				$i++;
				}


			//echo("Normal ".count($contentsTrans) ." And Cancelled". count($contentsCancel));
			
			for($data = 0; $data < count($contentsTrans); $data++)
			{
				if($dateSelected ==  $contentsTrans[$data]["simpleDate"])
				{
					$totalCalculated = $contentsTrans[$data]["totAmount"];  
					$grandTotalAmount = ($grandTotalAmount + $contentsTrans[$data]["totAmount"]);
					
					$calculatFee = $contentsTrans[$data]["totFee"];
					$grandFee = $grandFee + $contentsTrans[$data]["totFee"];
					
					 $counter += $contentsTrans[$data]["cnt"]; 
					 $grandCount = ($grandCount+$contentsTrans[$data]["cnt"]);
					 if(CONFIG_REPORTS_SURCHARG == '1')
					 {
					 	 $surcharges = $contentsTrans[$data]["extra1"] + $contentsTrans[$data]["extra2"] + $contentsTrans[$data]["extra3"] + $contentsTrans[$data]["extra4"];
					 	 $grandSurcharges = $surcharges + $grandSurcharges;
					 }
					 
					 $grossTotal = $contentsTrans[$data]["grossAmount"];  
					 $grandGrossTotal = $grandGrossTotal + $contentsTrans[$data]["grossAmount"];  
					 
					 $agentComm = $contentsTrans[$data]["agentComm"];  
					 $grandAgentComm = $grandAgentComm + $contentsTrans[$data]["agentComm"];  
					 
				}
			}
			if(CONFIG_CANCEL_REVERSE_COMM_COMM == '1')
			{
				
				for($data2=0;$data2 < count($cancelData2);$data2++)
				{
					if($dateSelected == $cancelData2[$data2]["cancelDate"])
					{
						$totalCalculated -=  $cancelData2[$data2]["totAmount"];
					    $grandTotalAmount -= $cancelData2[$data2]["totAmount"];
					    
					  //  $counter -= $contentsTrans[$data2]["cnt"]; 
					 //$grandCount -= $contentsTrans[$data2]["cnt"];
					 
					   $grossTotal -= $cancelData2[$data2]["totAmount"];  
					   $grandGrossTotal -=  $cancelData2[$data2]["totAmount"];  
					 
					    
					}
				}
				
				
				for($data3=0;$data3 < count($cancelData3);$data3++)
				{
					if($dateSelected == $cancelData3[$data3]["cancelDate"])
					{
						$totalCalculated -=  $cancelData3[$data3]["totAmount"];
					//echo("total<br>".$cancelData3[$data3]["totAmount"]."<br>".$cancelData3[$data3]["cnt"]."<br>");
					    $grandTotalAmount -= $cancelData3[$data3]["totAmount"];
					// echo("Grand<br>"."<br>");
					    $calculatFee -= $cancelData3[$data3]["totFee"];
					 // echo("Fee<br>".$cancelData3[$data3]["totFee"]."<br>");
					    $grandFee -= $cancelData3[$data3]["totFee"];
					//  echo("GrandFee<br>");
					   //$counter -= $contentsTrans[$data3]["cnt"]; 
					// $grandCount -= $contentsTrans[$data3]["cnt"];
					
					 $grossTotal -= $cancelData3[$data3]["grossAmount"];  
					 $grandGrossTotal -= $cancelData3[$data3]["grossAmount"];  
					 
					 $agentComm -= $cancelData3[$data3]["agentComm"];  
					 $grandAgentComm -= $cancelData3[$data3]["agentComm"];  
					 
					 if(CONFIG_REPORTS_SURCHARG == '1')
					 {
					 	 $surchargesTemp = $contentsTrans[$data]["extra1"] + $contentsTrans[$data]["extra2"] + $contentsTrans[$data]["extra3"] + $contentsTrans[$data]["extra4"];
					 	 $surcharges -= $surchargesTemp;
					 	 $grandSurcharges -= $surchargesTemp;
					 }
					 
					}
				}
			}
			
				
				
			if(CONFIG_EXCLUDE_CANCEL_DAILY_COMM == '1')
			{
				
				for($exclude = 0; $exclude < count($contentsExcludeCancel); $exclude++)
				{
						
					if($dateSelected == $contentsExcludeCancel[$exclude]["simpleDate"])
					{
						 $calculatFee += $contentsExcludeCancel[$exclude]["totFee"];
						 $grandFee += $contentsExcludeCancel[$exclude]["totFee"];
						
						 $counter += $contentsExcludeCancel[$exclude]["cnt"]; 
						 $grandCount += $contentsExcludeCancel[$exclude]["cnt"];
						 if(CONFIG_REPORTS_SURCHARG == '1')
						 {
						 	 $surcharges += $contentsExcludeCancel[$exclude]["extra1"] + $contentsExcludeCancel[$exclude]["extra2"] + $contentsExcludeCancel[$exclude]["extra3"] + $contentsExcludeCancel[$exclude]["extra4"];
						 	 $grandSurcharges += $grandSurcharges;
						 }
						 
						  ////As Fee is to be added still, only transaAmount is excluded
						 $grossTotal += $contentsTrans[$exclude]["totFee"]; 
						 $grandGrossTotal += $contentsTrans[$exclude]["totFee"];  
						 
						 $agentComm += $contentsTrans[$exclude]["agentComm"];  
						 $grandAgentComm += $contentsTrans[$exclude]["agentComm"];  
					}
				}	
			}	
			?>
				
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF" align="center"><strong><font color="#006699"><? echo $dateSelected ?></font></strong></td>
				  <td width="100" bgcolor="#FFFFFF" align="center"><? echo number_format($totalCalculated, 2, '.', ''); ?></td>
				  <td width="100"  align="center" bgcolor="#FFFFFF"> <? echo number_format($calculatFee, 2, '.', ''); ?></td>
				  <td width="100"  align="center" bgcolor="#FFFFFF"> <? echo number_format($agentComm, 2, '.', ''); ?></td>
				  <td width="100"  align="center" bgcolor="#FFFFFF"> <? echo number_format(($calculatFee - $agentComm), 2, '.', ''); ?></td>
				  <td width="50" align="center" bgcolor="#FFFFFF"><? echo number_format($surcharges, 2, '.', '');  ?>&nbsp;</td>
				  <?
				  	$grossTotal -= $agentComm ;
				  ?>
				  <td width="120" bgcolor="#FFFFFF"  align="center"><? echo number_format($grossTotal, 2, '.', ''); ?></td>
				  <td width="120" bgcolor="#FFFFFF"  align="center"><?=$counter?></td>
				  <?
				  if(CONFIG_CANCEL_DAILY_COMM == '1')
				  { //$contentsTrans[$i]["tDate"]
				  	for($j=0;$j < count($contentsCancel);$j++)
					{
						if($dateSelected == $contentsCancel [$j]["cancelDate"])
						{
					  	?>
					  	<td width="100" bgcolor="#FFFFFF"><? 
					  		// refund, sum(totalAmount
					  		
					  		echo $contentsCancel [$j]["cancelAmount"];
					  		$grandCancel += $contentsCancel [$j]["cancelAmount"];
					  		?> </td>
					  	<td width="100" bgcolor="#FFFFFF"><? echo $contentsCancel [$j]["cnt"];
					  		$grandCancelCount += $contentsCancel [$j]["cnt"];
					  		
					  		?> </td>
					  	<?
				  		}
					}
				  }
				  ?>
				  <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
				</tr>
				
				<?
				}
			
		
			
			?>
			<tr bgcolor="#FFFFFF">
			  <td>Total Days: <? echo $i;?></td>
			 
			  <td  align="center"><? echo number_format($grandTotalAmount, 2, '.', '');?></td>
			  <td width="100" align="center"><? echo  number_format($grandFee, 2, '.', '');?></td>
			  <td width="50" align="center"><? echo  number_format($grandAgentComm, 2, '.', '');?>&nbsp;</td>
			  <td width="50" align="center"><? echo  number_format(($grandFee - $grandAgentComm), 2, '.', '');?>&nbsp;</td>
			  <td width="50" align="center"><? echo  number_format($grandSurcharges, 2, '.', '');?>&nbsp;</td>
			  <?
			  	$grandGrossTotal -= $grandAgentComm ;
			  ?>
			  <td width="50" align="center"><? echo  number_format($grandGrossTotal, 2, '.', '');?>&nbsp;</td>
			  <td width="120"  align="center"><? echo $grandCount;?></td>
			  <?
			  if(CONFIG_CANCEL_DAILY_COMM == '1')
			  {
			  	?>
			  	<td align="center" bgcolor="#FFFFFF">&nbsp;<? echo  number_format($grandCancel, 2, '.', '');?> </span></td>
			  	<td align="center" bgcolor="#FFFFFF">&nbsp;<? echo  number_format($grandCancelCount, 2, '.', '');?> </span></td>
			  	<?
				}
			  ?>
			   <td align="center">&nbsp;</td>
			  </tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>