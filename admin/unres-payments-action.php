<?php
	session_start();
/**
* @package Payex
* @subpackage Reports
* Short Description
* This page perform the operations of unresolved payments 
* called from the nres-payments-ajax.php page 
*/		
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	
	if(CONFIG_CUSTOM_TRANSACTION == '1')
		$transactionPage = CONFIG_TRANSACTION_PAGE;
	else
		$transactionPage = "add-transaction.php";
//print_r($_REQUEST);
	$today = date("d/m/Y");
	if($_REQUEST["from"] == "")
	{
		$_REQUEST["from"] = $today;
	}
	if($_REQUEST["to"] == "")
	{
		$_REQUEST["to"] = $today;
	}
	
	if($_REQUEST["fromT"] == "")
	{
		$_REQUEST["fromT"] = $today;
	}
	if($_REQUEST["toT"] == "")
	{
		$_REQUEST["toT"] = $today;
	}
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx && $_REQUEST['getGrid']!='transList')
	{
		$sidx = 'importedOn DESC, id ';
	}elseif(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";
	
/* this function will remove empty values from give array*/
	function remove_array_empty_values($array, $remove_null_number = true)
	{
		$new_array = array();
		$null_exceptions = array();
		foreach ($array as $key => $value)
		{
			$value = trim($value);
			if($remove_null_number)
			{
				$null_exceptions[] = '0';
			}
			if(!in_array($value, $null_exceptions) && $value != "")
			{
				$new_array[] = $value;
			}
		}
		return $new_array;
	}
			
$tranactionSource = $_REQUEST["tranactionSource"];

if($_REQUEST["btnAction"] == "AddTransaction")
{
	
	$reference = $_REQUEST["reference"];
	$amount = $_REQUEST["amount"];
	$tranactionSourceAgent = $tranactionSource."agent";
	$agentId = '';
	$selectAgent = "select userID from admin where username = '".$tranactionSourceAgent."'";
	$agentResult = mysql_query($selectAgent);
	if($agentRow = mysql_fetch_assoc($agentResult))
	{
			$agentId = $agentRow["userID"];
	}
		$selectDistributor = "select userID from admin where username = '".$tranactionSource."'";
		$distributorResult = mysql_query($selectDistributor);
		if($row = mysql_fetch_assoc($distributorResult))
		{
			$insertQuery = "INSERT INTO transactions
			(customerID, benID, benAgentID, custAgentID,  exchangeID, refNumberIM, transAmount,
			 exchangeRate, localAmount, IMFee, totalAmount, addedBy, transDate, transStatus, transType, moneyPaid) values(
			 '','','".$row["userID"]."','".$agentId."','','".$reference."', '".$amount."', '1', '".$amount."', '0', '".$amount."', 
			 '".$_SESSION["loggedUserData"]["username"]."', '".date('Y-m-d H:i:s')."', 'Awaiting Payment', 'Bank Transfer','By Bank Transfer')";
			 
			 mysql_query($insertQuery);
		}
}

if($_REQUEST["btnAction"] == "Delete")
{
	$payIDArray		= $_REQUEST["payIDs"];
	for ($d=0;$d < count($payIDArray);$d++)
	{
		if(isset($payIDArray[$d]))
		{
			update("update barclayspayments set isDeleted = 'Y' where id = '".$payIDArray[$d]."'");
			$descript="Deleted record for Barclays Payment by ". $_SESSION["loggedUserData"]["username"];;
			activities($_SESSION["loginHistoryID"],"UPDATE",$payIDArray[$d],'barclayspayments',$descript);
			if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
				$query_pay_action = "
								INSERT INTO payment_action(payID,action_id)
											values('".$payIDArray[$d]."', '".$_SESSION["loggedUserData"]["userID"]."')";
				insertInto($query_pay_action);
			}				
			$err = "";
		}
	}

	if(trim($err) == "")
	{
		$err = "Payment(s) deleted successfully.";
	}
	// Number of iteration should be equal to the number of  transactions
		echo "<input type='hidden' name='payListDelete' id='payListDelete' value='".$err."'>";
		exit;
}
if($_REQUEST["btnAction"] == 'Cancel' && (strstr(CONFIG_CANCEL_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_CANCEL_TRANSACTION_UNRESOLVED=="1"))
{
	$err = "Operation not performed. Please login again.";
	$transIDArray	= $_REQUEST["transIDsCanc"];
	if(count($transIDArray)>0){
		$arguments = array("action" => $_REQUEST["btnAction"],"totTrans"=>count($transIDArray),"transIDArr" => $transIDArray,"refundFee"=>$_REQUEST["refundFee"],"username" => $username);
		//debug($arguments);
		cancellationRequest($arguments);
 	$err = "Transaction(s) cancelled successfully.";

	// Number of iteration should be equal to the number of  transactions
	}
//redirect("request_cancellation.php?msg=Y&action=". $_GET["action"]);
		echo "<input type='hidden' name='transListCancel' id='transListCancel' value='".$err."'>";
 exit;
}

if($_REQUEST["btnAction"] == "Resolve")
{

//\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\
	$successResolved = false;
	$transIDArray	= $_REQUEST["transIDs"];
	$payIDArray		= $_REQUEST["payIDs"];
	//debug($transIDArray);
	//debug($payIDArray,true);
	if(count($transIDArray)>0 && is_array($transIDArray)){
		for($s=0;$s<count($transIDArray);$s++)	{
			$errFlag = false;

			$transID = $transIDArray[$s];
			$transRS = selectFrom("SELECT transID,localAmount,totalAmount,transAmount,bankCharges,refNumberIM,benAgentID,custAgentID,customerID,AgentComm,currencyTo,currencyFrom,createdBy FROM transactions where transID='".$transID."'");
			$transID = $transRS["transID"];	
			$totalAmount = $transRS["totalAmount"];
			$transAmount = $transRS["transAmount"];
			$localAmount = $transRS["localAmount"];
			$bankCharges = $transRS["bankCharges"];
			$imReferenceNumber = $transRS["refNumberIM"];
			$imReferenceNumberV .= $imReferenceNumber.',';
			$benAgentID = $transRS["benAgentID"];
			$custAgentID = $transRS["custAgentID"];											
			$customerID = $transRS["customerID"];
			$agentComm = $transRS["AgentComm"];
			$currencyTo = $transRS["currencyTo"];
			$currencyFrom = $transRS["currencyFrom"];
			$transCreatedBy = $transRS["createdBy"];
			$modifyby = $agentID; // to keep track of logged user ID in modify column
			$amount = $totalAmount;
			$payinFlag = true;
			/*******
				#5084 - AMB Exchange
				This block below is to check either functionality is PayinBook or not Also
				if PayinBook then Customer's Ledgers should also be affected alongwith
				Payin Agent either All or assigned in CONFIG_PAYIN_AGENT_NUMBER.
			*******/
			if(CONFIG_PAYIN_CUST_AGENT == "1" && CONFIG_PAYIN_CUSTOMER=="1"){
				if(CONFIG_PAYIN_CUST_AGENT_ALL=="1"){
					$ledgerAgent = $custAgentID;
				}
				elseif(defined("CONFIG_PAYIN_AGENT_NUMBER") && CONFIG_PAYIN_AGENT_NUMBER!="0"  && CONFIG_PAYIN_CUST_AGENT_ALL!="1"){
					$ledgerAgent = CONFIG_PAYIN_AGENT_NUMBER;
				}
				$agentPayinContentsT = selectFrom("select userID from " . TBL_ADMIN_USERS . " where userID = '".$ledgerAgent."'");
				$agentPayinID = $agentPayinContentsT["userID"];
				if($agentPayinID=="" && $transCreatedBy!="CUSTOMER")
					$payinFlag = false;
			}
			elseif($transCreatedBy!="CUSTOMER"){
					$payinFlag = false;
			}
			if($_REQUEST["userTypes"]  == "customer" && $payinFlag)
			{
				if($transCreatedBy == "CUSTOMER" && isExist("select c_id from cm_customer where c_id ='".$customerID."'"))
				{
					$strQuery = "SELECT Balance,c_id FROM cm_customer where c_id = '".$customerID."'";
					$rstRow = selectFrom($strQuery);
					$Balance = $rstRow["Balance"]+$amount;	
					update("update cm_customer set Balance = '".$Balance."' where c_id = '".$customerID."'");
					$agentLedgerAmount = $amount;
					$tran_date = date("Y-m-d");
					
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$amount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>$imReferenceNumber,"customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"customer_account"
										 );
					$retInputArgsRevC = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
					
					insertInto("insert into  customer_account (customerID ,Date ,tranRefNo,payment_mode,Type ,amount,note ) 
								 values('".$customerID."','".$tran_date."','".$imReferenceNumber."','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$note."'
								 )");
									
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}else{
						if($Balance >= $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}								
				
					if($conditionFlag)// >= $transAmount)
					{	
						$tran_date = date("Y-m-d");	
						/**************************************************************
						 * The puporse of this config (CONFIG_VERIFY_TRANSACTION_ENABLED) when user resolve payment then it will go in processing status
						 * In processiing status ledger not affect.
						 * Added by Niaz Ahmad at 03-08-2009 @5256- Minascenter
											
						*****************************************************************///
						if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
						{													
							update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}elseif(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{
						
							update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$tran_date."' where transID = '".$transID."'");
	
							$checkBalance2 = selectFrom("select agentType, balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");
							
							if($checkBalance2["agentType"] == 'Sub')
							{
								updateSubAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}else{
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}
					  }
					}
				}
				elseif($transCreatedBy != "CUSTOMER"  && isExist("select customerID from customer where customerID ='".$customerID."'") && $payinFlag)
				{
					/*					
					$queryResolve = "update barclayspayments set isResolved = 'Y', username='$uname', paymentFrom = 'agent customer' where id = '".$_REQUEST["allIds"][$i]."'";
					update($queryResolve);
			
					$strQuery = "SELECT * FROM barclayspayments where username = '$uname' and id = '".$_REQUEST["allIds"][$i]."'";
					
					$Balance = 0;
					$nResultBarkleyTable = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
					while($rstRowBT = mysql_fetch_array($nResultBarkleyTable))	
					*/			
					$Balance = $amount;
					$rstRow = selectFrom("SELECT * FROM customer where customerID = '".$customerID."'");
					$tran_date = date("Y-m-d");
					$agentLedgerAmount = $amount;
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$agentLedgerAmount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>"","customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"agents_customer_account"
										 );
										 
					$retInputArgsRev = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
/*					if(CONFIG_UNRESOLVED_REVERSAL_ENTRY=="1"){
						$queryResolve = "SELECT id FROM barclayspayments WHERE isResolved = 'N' AND amount='-".$amount."' AND tlaCode='REV' ORDER BY id DESC ";
						$queryResolveRS = selectFrom($queryResolve);
						if($queryResolveRS["id"]!=""){
							update("update barclayspayments 
										set isResolved = 'Y', 
											username='', 
											paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."' 
										where id = '".$queryResolveRS["id"]."' ");
							insertInto("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','WITHDRAW','".$amount."','".$modifyby."'
									 )");
						}
					}*/
					update("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$modifyby."'
									 )");

					if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
					{													
				   	 update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
					}elseif(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{			
					
					if(CONFIG_PAYIN_CUST_AGENT == '1')
					{
						$agentPayinContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'");
						if($agentPayinContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
							$q = updateAgentAccount($agentPayinContents["parentID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}else{
							$q = updateAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}	
					}
					///////////Balance Condition//////////	
					// to Authorize trasaction in any case;
				
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
						}else{
							if($Balance >= $totalAmount)
							{
								$conditionFlag = 1;
							}else{
								$conditionFlag = 0;
						}
					}								
															
					///////$Balance >= $totalAmount///// Done on requirement of Express
														
					if($conditionFlag)
					{	
						/*$strQuery = "select * from transactions  where customerID = $CustomerID and transStatus  = 'Pending' and transAmount = $transAmount and createdBy != 'CUSTOMER'";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$rstRows = mysql_fetch_array($nResult);
						$imReferenceNumber = $rstRows["refNumberIM"];
						$totalAmount = $rstRows["totalAmount"];*/
						$date_time = date("Y-m-d H:i:s");
						if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{
							update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$date_time."' where transID = '".$transID."'");
						}
						/*$strQuery = "insert into  agent_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount ) 
										 values('".$CustomerID."','".$tran_date."','$imReferenceNumber','Payment by Bank','WITHDRAW','".$totalAmount."'
										 )";	
					
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());	
						*/																														
					}		
					else
					{}
				  }	
				}
				else
				{
					$err = "Error occured while processing some records\n(Payin Agent is not specified properly OR select Agent from Search Type dropdown\nand give Agent Name for Simple Agent transaction Reconcile.)";
					$errFlag = true;
				}
				
			}
			
			elseif($_REQUEST["userTypes"] == "agent" || $_REQUEST["userTypes"] == "distributor"  || !$payinFlag)
			{
				if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
				{													
					update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
				}
				else
				{
				$custAgentR = selectFrom("select username from " . TBL_ADMIN_USERS . " where userID='".$custAgentID."'");
				$uname = $custAgentR["username"];
				if($uname!="")
				{
					$type = "DEPOSIT";
					$description = "Barclays Payment";
					
					$contantagentID = selectFrom("select userID,balance,limitUsed,agentAccountLimit,isCorrespondent from " . TBL_ADMIN_USERS . " where username ='".$uname."'");
					$agentID = $contantagentID["userID"];
					$agentlimitUsed	= $contantagentID["limitUsed"];
					$agentLimit	= $contantagentID["agentAccountLimit"];
					$agentBalance	= $contantagentID["balance"];
					$paymentFrom = "agent";
					
					$Balance = $agentBalance + $amount;
					
					if(CONFIG_AnD_ENABLE == '1' && $contantagentID["isCorrespondent"] == 'Y')
					{
						$isAgentAnD = 'Y';
					}
					
					update("update " . TBL_ADMIN_USERS . " set balance  = '".$Balance."' where userID = '".$agentID."'");
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
					{
						/* Changing the localAmount to totalAmount as bug found via ticket #3829 */
						$amount = $totalAmount;
						$currencyFrom = $currencyTo;
					}else{
						$amount = $amount;
						$currencyFrom = $currencyFrom;
					}
					$agentLedgerAmount = $amount;
					
					/* #4703 - Opal
					 * agent ledger is not affected as Deposit [money in] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
					 * as it might need only affect on authorizing transaction.
					 * by AShahid
					 */
					if(CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION != "1"){
							if($isAgentAnD == 'Y')
							{
								$queryAgent = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, amount, type, modified_by, description,TransID,actAs,currency) values('".$agentID."','".getCountryTime(CONFIG_COUNTRY_CODE)."','".$agentLedgerAmount."','".$type."', '".$modifyby."','".$description."','".$transID."','Agent','".$currencyFrom."')";
								insertInto($queryAgent);
							}else{
								if($contantagentID["agentType"] == 'Sub')
								{
									updateSubAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
									updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
									
								}else{
									updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
								}
							}
					 }	
						$amountInHand = $agentBalance + $amount;
						if(CONFIG_AGENT_LIMIT == "1")
						{
						$agentValidBalance = $amountInHand + $agentLimit;///Agent Limit is also used
						}else{
						$agentValidBalance = $amountInHand;///Agent Limit is not used
						}
						
						///
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$agentLedgerAmount = $totalAmount - $agentComm;
						}else{
							$agentLedgerAmount = $totalAmount;
						}
						///
								
						//update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						//update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						//debug($agentValidBalance .">=". $agentLedgerAmount);
						
						/*
						  #4703: Opal - Item Wise Bank Reconciliation
						  in Opal not check whether balance is enough or not
						  Distributor ledgers are affected when transaction is Authorized
						  distributor ledger
						*/
						$enoughBalanceFlag = false;
						if(defined("CONFIG_PUT_AnD_ENTRY_IN_LEDGER_AT_AUTHORIZATION") && CONFIG_PUT_AnD_ENTRY_IN_LEDGER_AT_AUTHORIZATION == "1")
							$enoughBalanceFlag = true;
						
						if($agentValidBalance >= $agentLedgerAmount || $enoughBalanceFlag)
						{	//////////////////If Balance is enough
							
							$queryCust = "select payinBook  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
							$custContents = selectFrom($queryCust);
							
							if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
							{	
								$Status	= "Authorize";
								$tran_date = date("Y-m-d");	
								
								$amountInHand = $amountInHand - $agentLedgerAmount;			
								/* #4703 - Opal
								 * agent ledger is affected as [money out] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
								 * as it might not have been affected on creation of transaction under the same config.
								 * by AShahid
								 */
								if(CONFIG_LEDGER_AT_CREATION != "1" || CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1")
								{
									
									if($isAgentAnD == 'Y')
									{
										$insertQuery = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description,currency) values('".$agentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$currencyFrom."')";
									}else{
										$insertQuery = "insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description,note,currency) values('".$agentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$note."','".$currencyFrom."')";
									}
									$q=mysql_query($insertQuery);//////Insert into Agent Account
									update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '".$agentID."'");											
									agentSummaryAccount($agentID, "WITHDRAW", $agentLedgerAmount);	
								}
							////////////////////Auto Authorization//////////
							/******
								#5160
								Distributors ledgers are affected in different cases like here we checked
								that when CONFIG_AUTO_AHTHORIZE is ON then in some clients on the basis of this
								config Ledgers of distributors are WITHDRAWN.
							******/
							
							/* #4703 - Opal
							 * distributor ledger is affected as Deposit [money in] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
							 * by AShahid
							 */
							if(CONFIG_AUTO_AHTHORIZE == "1" || CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1")
							{
								$distType = "WITHDRAW";
								if(CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1" && $isAgentAnD == 'Y')
									$distType = "DEPOSIT";
									
									$checkBalance2 = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");						
									$agentLedgerAmount = $transAmount;
								if(CONFIG_AnD_ENABLE == '1' && $checkBalance2["isCorrespondent"] == 'Y')
								{
									$isBankAnD = 'Y';
								}
								$currentBalanceBank = $checkBalance2["balance"]	- $transAmount;				
								update("update " . TBL_ADMIN_USERS . " set balance  = '".$currentBalanceBank."' where userID = '".$benAgentID."'");											
								if($isBankAnD == 'Y')
								{
									insertInto("insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description, actAs,currency) values('".$benAgentID."', '".$tran_date."', '".$distType."', '".$agentLedgerAmount."', '".$modifyby."', '".$transID."', 'Transaction Authorized','Distributor','".$currencyTo."')");
								}else{
									$agentLedgerAmount = (DEST_CURR_IN_ACC_STMNTS == "1" ? $localAmount : $transAmount);
									insertInto("insert into bank_account (bankID, dated, type, amount, currency, modified_by, TransID, description) values('".$benAgentID."', '".$tran_date."', '".$distType."', '".$agentLedgerAmount."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $currencyTo : $currencyFrom)."', '".$modifyby."', '".$transID."', 'Transaction Authorized')");		
								}
								agentSummaryAccount($benAgentID, $distType, $agentLedgerAmount);
							}
						}
					//update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '$agentID'");
					}
					
					/////////////////////////
				}
				else
				{
					$err = "Error occured while processing some records";
					$errFlag = true;
				}
			  }	  // end processing trans status if condition
			}
			//echo("it is here...");
			//if(trim($err) == "")
			if(!$errFlag)
			{
				$err = "Payments resolved successfully.";
				$successResolved = true;
				//debug("update transactions set isResolved  = 'Y' where transID = '".$transID."'");
				update("update transactions set isResolved  = 'Y' where transID = '".$transID."'");
					
					
					
					
				if($enoughBalanceFlag)
				{
					if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
					{
						update("update transactions set transStatus  = 'Pending' where transID = '".$transID."'");
					}else{
						update("update transactions set transStatus  = 'Authorize' where transID = '".$transID."'");
						}
				}
				//redirect("unres-payments.php?type=" . $_GET["type"] . "&msg=" . urlencode($err));
			}			
		}
	}
	// Number of iteration should be equal to the number of  transactions
		//debug($_REQUEST["userTypes"]."-->".$transID."-->".$successResolved);	
		
		if($_REQUEST["userTypes"]!="" && $transID!="" && $successResolved)
		{
			$counter = 0;
			//debug($payIDArray);
			$strBatchumSql = "select Max(batchNumber) as mbn from payments_resolved";
			$arrBatchNumData = selectFrom($strBatchumSql);
			$loopCount = 0;
			$format = 0;
			
			$intbatchNumber = $arrBatchNumData["mbn"];
				$intbatchNumber++;
			
			//debug($intbatchNumber);
			
			//debug($transIDArray);
			//debug($payIDArray);
			
			$paymentCount = count($payIDArray);
			$transCount = count($transIDArray);
			//debug($paymentCount."-->".$transCount);
			
			$paymentUnique = array_unique($payIDArray);
			$transUnique = array_unique($transIDArray);
			//debug($paymentUnique."-->".$transUnique);
			
			$paymentUniqueCount = count($paymentUnique);
			$transUniqueCount = count($transUnique);
			//debug($paymentUniqueCount."-->".$transUniqueCount);
			
			
			if(count($payIDArray) == 1 && count($transIDArray) == 1)
				$format = 1;
			elseif($paymentUniqueCount == 1 && $transUniqueCount != 1)
				$format = 2;
			elseif($transUniqueCount == 1 && $paymentUniqueCount != 1)
				$format = 3;
			elseif($paymentUniqueCount !=1 && $transUniqueCount !=1)
				$format = 4;
			
			
						
			if(count($payIDArray) >= count($transIDArray))
				$loopCount = count($payIDArray);
			else	
				$loopCount = count($transIDArray);
				
			for($b=0; $b< $loopCount; $b++){
			
				$queryResolve = "UPDATE 
									barclayspayments 
								 SET 
								 	isResolved = 'Y',
									username='',
									paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."'
								WHERE 
									id = '".$payIDArray[$b]."'";
				//debug($queryResolve);
				update($queryResolve);
				
				
				if(empty($transIDArray[$b]))
					$transIDArray[$b] = $transIDArray[$b-1];
				
				if(empty($payIDArray[$b]))
					$payIDArray[$b] = $payIDArray[$b-1];	
				
				$transVal = selectFrom("select refNumberIM ,totalAmount  from transactions where transID = '".$transIDArray[$b]."'");
				//debug("select refNumberIM ,totalAmount  from transactions where transID = '".$transIDArray[$b]."'");
				//debug($transVal);
				
				$query_pay_resolved = "INSERT INTO 
												payments_resolved
												(
												 payID,
												 transID,
												 agentAmount,
												 resolvedBy,
												 refNumberIMs,
												 batchNumber,
												 format
												 )
												VALUES
												(
												'".$payIDArray[$b]."',
												'".$transIDArray[$b]."',
												'".$transVal["totalAmount"]."', 
												'".$_SESSION["loggedUserData"]["userID"]."',
												'".$transVal["refNumberIM"]."',
												'".$intbatchNumber."',
												'".$format."'
												)";
					
					//debug($query_pay_resolved);
					insertInto($query_pay_resolved);
				
				
				$descript="Resolved record for Barclays Payment for transaction ".$imReferenceNumber;
				activities($_SESSION["loginHistoryID"],"UPDATE", $payIDArray[$b],'barclayspayments',$descript);
			
				if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
					$query_pay_action = "
									INSERT INTO payment_action(payID,transID,action_id,agentAmount)
							    	values('".$payIDArray[$b]."', '".$transIDArray[$b]."', '".$_SESSION["loggedUserData"]["userID"]."','".$agentLedgerAmount."')";
					//debug($query_pay_action);
					insertInto($query_pay_action);
				}
			
				$counter = $counter+1;
			}
		}
		echo "<input type='hidden' name='payListResolve' id='payListResolve' value='".$err."'>";
		exit;
}
	
if($_REQUEST["type"] == 1)
{//tlaCode = 'REM'
	$query = "select * from barclayspayments where  isResolved='N' and isDeleted='N' ";
	
}elseif($_REQUEST["type"] == 2)
{
	$query = "select * from natwestpayments where isResolved='N' and isDeleted='N' ";
}
$queryTemp = $query;

//////////////Date Block //////////////////
if ($_REQUEST["from"]!="")
		$fromJS = $_REQUEST["from"];
if ($_REQUEST["to"]!="")
		$toJS = $_REQUEST["to"];


	if ($fromJS != "")
	{
		$dDateF = explode("/",$fromJS);
		if(count($dDateF) == 3)
			$fromJS = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($toJS != "")
	{
		$dDateT = explode("/",$toJS);
		if(count($dDateT) == 3)
			$toJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	
			if ($fromJS != "" || $toJS != "")
		{
			if ($fromJS != "" && $toJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00' and importedOn <= '$toJS 23:59:59') ";	
			}
			elseif ($fromJS != "")
			{
				$queryDated = " and (importedOn >= '$fromJS 00:00:00') ";	
			}
			elseif ($toJS != "")
			{
				$queryDated = " and (importedOn <= '$toJS 23:59:59') ";	
			}	
		}
	if ($queryDated != "")
	{
		$query .= $queryDated;
	}
	
	/////////////////////Date Block ended///////////////	
	$flagSearchPay = false;
if($_REQUEST["Submit"] == "SearchPay")
{
	$flagSearchPay = true;
	$cityFlag = false;
	/*******************************/
	/*print_r($_SESSION["lastQueryPay"]);
	echo("----------------------------- \r\n");*/
	//debug($_SESSION["lastQueryPay"]);
	//debug($_SESSION["lastQueryTrans"]);
	if($_REQUEST["suggest"] > 0 && sizeof($_SESSION["lastQueryTrans"]["amount"] > 0))
	{
		/*print_r($_SESSION["lastQueryTrans"]["amount"]);*/
			$suggestAmounts = implode(",",$_SESSION["lastQueryTrans"]["amount"]);
			$queryTemp2 = $queryTemp;
			if(!empty($suggestAmounts))
				$queryTemp2 .= " and amount in (".$suggestAmounts.")"; 
			//print_r($_SESSION);
			
		if(sizeof($_SESSION["lastQueryPay"]["id"]) > 0)
		{	
			$suggestIds = implode(",", $_SESSION["lastQueryPay"]["id"]);
			if(!empty($suggestIds))
				$queryTemp2 .= " and `id` in (".$suggestIds.")";
		}
	//	echo $queryTemp2;
		//echo("now what -----");/**/
		/*echo $queryTemp2;*/
			$resultTemp = mysql_query($queryTemp2);
			$tempResultIds = array();
			while($tempData = mysql_fetch_assoc($resultTemp))
			{
				/*print_r($tempData);*/
				$tempResultIds[] = $tempData["id"];
			}
			/*print_r($tempResultIds);*/
			$filteredIds = implode(",",$tempResultIds);
				if(!empty($filteredIds))
				{
					$queryTemp .= " and `id` in (".$filteredIds.")";
				/*echo("this is the tempQuery==>>".$queryTemp);*/
					if($_REQUEST["suggest"] == 1)
					{
						$tempArray = array();
						foreach($_SESSION["lastQueryTrans"]["reference"] as $refTemp)
						{
							$refTempArr = explode("-", $refTemp);
							if(count($refTempArr)>0){
								if($refTempArr[1] != '')
								{
									$tempArray[] = $refTempArr[1];
								}
							}
							else{
								$tempArray[] = $refTemp;
							}
						}
						//$compareDescription = $_SESSION["lastQueryTrans"]["reference"];
						$compareDescription = $tempArray;
						//$compareDescription = $_SESSION["lastQueryTrans"]["tracking_Ref"];
						//debug($compareDescription);
						$sugestionFieldName = 'refNumberIM';
					}
					if($_REQUEST["suggest"] == 2)
					{
						$compareDescription = $_SESSION["lastQueryTrans"]["custLast"];
						$sugestionFieldName = 'c.lastName';
					}
					if($_REQUEST["suggest"] == 3)
					{
						$compareDescription = $_SESSION["lastQueryTrans"]["custFirst"];
						$sugestionFieldName = 'c.firstName';
					}
					if($_REQUEST["suggest"] == 4)
					{
						//debug($_SESSION["lastQueryTrans"]);
						$compareDescription = $_SESSION["lastQueryTrans"]["city"];
						$sugestionFieldName = 'c.custBankeBranch';
						//debug($compareDescription);
						$cityFlag = true;
					
					}
					$queryTempDesc = '';
					for($k=0; $k < sizeof($compareDescription); $k++)
					{
						if($k > 0)
						{
								$queryTempDesc .= " OR ";
						}
						if($cityFlag)
						{
							$arrCity = explode(",",$compareDescription[$k]);
							//debug($arrCity);
							if(count($arrCity) == 1){
								$city_name = $arrCity[0];
							}elseif(count($arrCity) > 1 ){
							$bank_code = $arrCity[0];
							$city_name = $arrCity[1];
							}
							//debug($bank_code."-->".$city_name);
							
							if(!empty($bank_code) && !empty($city_name)){
								$queryTempDesc .= " description like '%".$city_name."%'";
								$queryTempDesc .= " AND accountNo like '".$bank_code."%'";
							}elseif(!empty($city_name)){
								$queryTempDesc .= " description like '%".$city_name."%'";
							}elseif(!empty($bank_code)){
								$queryTempDesc .= " accountNo like '".$bank_code."%'";
							}	
						}
						else
						{
							$queryTempDesc .= " description like '%".$compareDescription[$k]."%' ";
						}	
						//debug($queryTempDesc);
					}
					if(!empty($queryTempDesc))
						$queryTemp .= " and (".$queryTempDesc.")";
				}else{
					$flagSearchPay = false;
					}
					//debug($queryTemp);
		}else{
	/******************************/
			//echo("--Before-".$amount1JS."---");
			if ($_REQUEST["amount1"]!=""){
			//	echo("---".$_REQUEST["amount1"]."---");
				$amount1JS = $_REQUEST["amount1"];
			}
			
			//echo("--Before-".$amount1JS."---");
			
			if ($_REQUEST["desc"]!="")
				$descJS = $_REQUEST["desc"];
			//if ($_REQUEST["currencyPay"]!="")
				//$currencyP = $_REQUEST["currencyPay"];
			if ($_REQUEST["accountNo"]!="")
				$accountNo = $_REQUEST["accountNo"];
			
			if(!empty($_REQUEST["type"]))
			{
		
				if ($amount1JS != "")
				{
					$queryAmount = " and amount = $amount1JS ";	
				}
				if ($descJS != "")
				{
					$queryDesc = " and (description like '%$descJS%'  or unResReason like '%$descJS%')";	
				}	
			}
		
			if ($queryrefNumber != "")
			{
				$query .= $queryrefNumber;
			}
			if ($queryAmount != "")
			{
				$query .= $queryAmount;
			}
			if ($queryDesc != "")
			{
				$query .= $queryDesc;	
			}
			/*if ($currencyP != "")
			{
				$query .= " and currency = '".$currencyP."'";
			}*/
			if ($accountNo != "" && $accountNo!='null')
			{
				$query .= " and accountNo in (".$accountNo.")";
			}
			
	}
	
//debug($queryTemp);	
}
$strStatus  = '';
if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
{
	$fetchingStatus = 'Awaiting Payment';	
	$strStatus  .= '';
}else
{
	$fetchingStatus = 'Pending';	
	$strStatus  .= ' AND transType = "Bank Transfer" ';
}

$queryT = "select 
				benID,
				transID,
				totalAmount,
				refNumberIM,
				transDate,
				customerID,
				createdBy,
				senderBank,
				currencyFrom 
				
			FROM 
				". TBL_TRANSACTIONS." 
			WHERE 
				transStatus = '".$fetchingStatus."' AND moneyPaid='By Bank Transfer' AND isResolved='N' ".$strStatus;

if(!strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") && CONFIG_EDIT_TRANSACTION_UNRESOLVED!="1"){
	$queryT .= " AND benAgentID !=0 ";
}
//print_r($_REQUEST);
/////////////////////Date Block////////////////////
if ($_REQUEST["fromT"]!="")
		$fromTJS = $_REQUEST["fromT"];
if ($_REQUEST["toT"]!="")
		$toTJS = $_REQUEST["toT"];
	if ($fromTJS != "")
	{
		$dDateFT = explode("/",$fromTJS);
		if(count($dDateFT) == 3)
			$fromTJS = $dDateFT[2]."-".$dDateFT[1]."-".$dDateFT[0];
	}
	if ($toTJS != "")
	{
		$dDateT = explode("/",$toTJS);
		if(count($dDateT) == 3)
			$toTJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	if ($fromTJS != "" || $toTJS != "")
	{
		if ($fromTJS != "" && $toTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00' and transDate <= '$toTJS 23:59:59') ";	
		}
		elseif ($fromTJS != "")
		{
			$queryDatedT = " and (transDate >= '$fromTJS 00:00:00') ";	
		}
		elseif ($toTJS != "")
		{
			$queryDatedT = " and (transDate <= '$toTJS 23:59:59') ";	
		}	
	}

	if ($queryDatedT != "")
	{
		$queryT .= $queryDatedT;
	}
///////////////////Date Block ended /////////////////////////

	if($tranactionSource != "" && $tranactionSource != "payex")
	{
		 $selectDistributor = "select userID from admin where username = '".$tranactionSource."'";
		$distributorResult = mysql_query($selectDistributor);
		if($row = mysql_fetch_assoc($distributorResult))
		{
			$distId = $row["userID"];
			$queryT .= " and benAgentID = '".$distId."' ";
		}
		//$tranactionSource = $_REQUEST["tranactionSource"];	
	}else{
		$selectDistributor = "select userID from admin where username in ('unistream','anelik')";
		$distributorResult = mysql_query($selectDistributor);
		$distIds = array();
			while($row = mysql_fetch_assoc($distributorResult))
			{
				$distId[] = $row["userID"];
			}
			$idsString = implode(",", $distId);
			if(!empty($idsString))
				$queryT .= " and benAgentID not in (".$idsString.") ";
		}


if($_REQUEST["Submit"] == "SearchTrans")
{
//	echo("--Before-".$amountTJS."---");
	if ($_REQUEST["amountT"]!=""){
	//	echo("---".$_REQUEST["amountT"]."---");
		$amountTJS = $_REQUEST["amountT"];
	}
//	echo("--After-".$amountTJS."---");
	if ($_REQUEST["searchBy"]!="")
		$searchByTJS = $_REQUEST["searchBy"];
	if ($_REQUEST["searchName"]!="")
		$searchNameT = $_REQUEST["searchName"];
	/*if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];*/
	if ($_REQUEST["paymentMod"]!="")
		$paymentMod = $_REQUEST["paymentMod"];
	if($_REQUEST["destCountry"] != '')
	{
		$destCountry = $_REQUEST["destCountry"];
	}
	
	if ($searchNameT != "")
	{
		if($searchByTJS=="customer"){
			$querysearchNameT = " and transID IN (select transID from ".TBL_TRANSACTIONS." where refNumberIM='".$searchNameT."') "; 
		}
		elseif($searchByTJS=="agent"){
			if(!empty($searchNameT))
				$querysearchNameT = " and custAgentID IN (select userID from ".TBL_ADMIN_USERS." where username LIKE '".$searchNameT."%') "; 
		}
		elseif($searchByTJS=="distributor"){
			if(!empty($searchNameT))
				$querysearchNameT = " and benAgentID IN (select userID from ".TBL_ADMIN_USERS." where username LIKE '".$searchNameT."%') "; 
		}

	}
	if ($amountTJS != "")
	{
		$queryAmountT = " and totalAmount = $amountTJS ";	
	}

	if ($querysearchNameT != "")
	{
		$queryT .= $querysearchNameT;
	}
	if ($queryAmountT != "")
	{
		$queryT .= $queryAmountT;
	}
	/*if ($currencyT != "")
	{
		$queryT .= " and currencyFrom = '".$currencyT."'";
	}*/
	if ($paymentMod != "")
	{
		$queryT .= " and moneyPaid = '".$paymentMod."'";
	}
	if ($destCountry != "" && $destCountry != "null")
	{
		$queryT .= " and toCountry in (".$destCountry.") ";
	}

	//debug($_SESSION["lastQueryPay"]);
	$queryTSuggest = '';		
	if($_REQUEST["suggest"] != 0)
	{
		if(sizeof($_SESSION["lastQueryPay"]["amount"]) > 0)
		{
			// loop get ref numbers and get numeric part out of C-223344-1 
			// i.e 223344 here [ as this might be in description field as reference number (customer bumber) ]
			$tempRefNumberArray = $tempRefNumberData = array();
			foreach($_SESSION["lastQueryTrans"]["reference"] as $refTemp)
			{
				$refTempArr = explode("-", $refTemp);
				if(count($refTempArr)>0){
					if($refTempArr[1] != '')
					{
						$tempRefNumberArray[] = $refTempArr[1];
					}
				}
				else{
					$tempRefNumberArray[] = $refTemp;
				}
			}
			$tempRefNumberData = $tempRefNumberArray;
			
			$flag = false;
			for($i = 0; $i < sizeof($_SESSION["lastQueryPay"]["amount"]); $i++)
			{
					$amount = $_SESSION["lastQueryPay"]["amount"][$i];
					$tracking_Ref = $_SESSION["lastQueryTrans"]["tracking_Ref"];
					$tracking_Id = $_SESSION["lastQueryTrans"]["tracking_Id"];
					$suggestDescriptions = $_SESSION["lastQueryPay"]["description"][$i];
					$suggestDescriptions = str_replace(" ", ",",$suggestDescriptions);
					$suggestDescriptions = str_replace("&", "",$suggestDescriptions);
					$reverse3 = explode(",", $suggestDescriptions);
					$reverse2 = array();
					$_strBankSql = '';
					$remove_null_number = true;
					$operator = '';
					
					/* remove empty values from array*/
					
					$reverse3 = remove_array_empty_values($reverse3, $remove_null_number);
					
					if($_REQUEST["suggest"] == 4)
						$accountNo = substr($_SESSION["lastQueryPay"]["bankAccount"][$i],0,4);
						
						for($j= 0 ; $j < sizeof($reverse3); $j++)
						{
							$dbQuote = '"';
							if($j==0)
								$dbQuote = '';
							if( $j > 0 )
								 $operator = "OR";
							
							if($_REQUEST["suggest"] == 4)
							{
								if(!empty($reverse3[$j]))
									$_strBankSql .= $operator." custBankBranch LIKE ('".$accountNo.",".$reverse3[$j]."%')";
									//debug($_strBankSql);
							}	
								$reverse2[$j] = '"'.$reverse3[$j].$dbQuote;
								if(empty($dbQuote))
									$reverse2[$j] .= '|'.$amount.'"';
						}
						
						for($j= 0 ; $j < sizeof($reverse3)-1; $j++)
						{
								$reverse2[] = '"'.$reverse3[$j].' '.$reverse3[$j+1].'"';
						}
					$suggestDescriptions = implode(",", $reverse2);	
					//debug($suggestDescriptions);
					if($_REQUEST["suggest"] == 1)
					{
						$tempIDs = array();
						for($ref = 0; $ref < sizeof($tempRefNumberArray); $ref++)
						{
							if(strpos($suggestDescriptions, '"'.$tempRefNumberArray[$ref].'|'.$amount.'"') !== false)
							{
								$tempIDs[] = $tracking_Id[$ref];
							}
						}
						if($i != 0 && $flag == true)
						{
							if(sizeof($tempIDs) > 0)
							{
								$suggestId = implode(",", $tempIDs);
								$suggestIdClause = '';
								if(!empty($suggestId))
								{
									$suggestIdClause = " and transID in (".$suggestId.") ";
									$queryTSuggest .= " or (totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' ".$suggestIdClause.") ";
								}
							}
						}else{
							//$queryTSuggest .= "(totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' and refNumberIM in (".$suggestDescriptions.")) ";
							//$queryTSuggest .= "(totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' ) ";
							if(sizeof($tempIDs) > 0)
							{
								$suggestId = implode(",", $tempIDs);
								$suggestIdClause = '';
								if(!empty($suggestId))
								{
									$suggestIdClause = " and transID in (".$suggestId.") ";
									$queryTSuggest .= " (totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' ".$suggestIdClause.") ";
									$flag = true;
								}
							}
							
						}
					}
					
					if($_REQUEST["suggest"] == 2)
					{
						 /*"select t.transID from customerBank as b, transactions as t  where t.transID = b.transID and t.totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' and transStatus =  'Awaiting Payment'
AND isResolved =  'N' and b.custBankBranch in (".$suggestDescriptions.") ";	*/
						$customerQuery = "select t.transID from customer as c, transactions as t  where t.customerID = c.customerID and t.transStatus =  'Awaiting Payment' AND t.isResolved =  'N' and t.totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' ";
						if(!empty($suggestDescriptions))
							$customerQuery .= " and c.lastName in (".$suggestDescriptions.")";
					}
					if($_REQUEST["suggest"] == 3)
					{
						/*$customerQuery = "select customerID from customer where firstName in (".$suggestDescriptions.")";*/
						$customerQuery = "select t.transID from customer as c, transactions as t  where t.customerID = c.customerID and t.transStatus =  'Awaiting Payment' AND t.isResolved =  'N' and t.totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' ";
						if(!empty($suggestDescriptions))
							$customerQuery .= " and c.firstName in (".$suggestDescriptions.")";
					}
					
					if($_REQUEST["suggest"] == 2 || $_REQUEST["suggest"] == 3)
					{	
						$customerResult = mysql_query($customerQuery);
						$customerId = array();
						if($row = mysql_fetch_assoc($customerResult))
						{
							$customerId[] = $row["transID"]; 
						}
						if(sizeof($customerId) > 0)
						{
							$suggestbanks = implode(",",$customerId);
							if($i != 0 && $flag == true)
							{
								if(!empty($suggestbanks))
									$queryTSuggest .= " or ( transID in (".$suggestbanks.") ) ";
							}else{
									if(!empty($suggestbanks))
									{
										$queryTSuggest .= "( transID in (".$suggestbanks.") ) ";
										$flag = true;
									}
							}
							
							//$queryTSuggest .= " and customerID in (".$suggestcustomer.") ";
						}
					}
					
				if($_REQUEST["suggest"] == 4)
				{
					//debug($_strBankSql);
					$senderBankQuery   = "select t.transID from customerBank as b, transactions as t  where t.transID = b.transID and t.totalAmount = '".$_SESSION["lastQueryPay"]["amount"][$i]."' and transStatus =  'Awaiting Payment'
AND isResolved =  'N' ";	
					//debug($suggestDescriptions);
					if(!empty($_strBankSql))
					{
						//$senderBankQuery .= " and b.custBankBranch in (".$suggestDescriptions.")";
						$senderBankQuery .= " and (".$_strBankSql.")";
						//debug($senderBankQuery);
						
						$bankResult = mysql_query($senderBankQuery);
						$bankId = array();
						while($row = mysql_fetch_assoc($bankResult))
						{
							$bankArray[] = $row["transID"]; 
						}
						//debug($bankArray);
						if(sizeof($bankArray) > 0)
						{
							$suggestbanks = implode(",",$bankArray);
							//debug($suggestbanks);
							if($i != 0 && $flag == true)
							{
								if(!empty($suggestbanks))
									$queryTSuggest .= " or ( transID in (".$suggestbanks.")) ";
									//debug($queryTSuggest);
							}else{
									if(!empty($suggestbanks))
									{
										$queryTSuggest .= "( transID in (".$suggestbanks.")) ";
										$flag = true;
										//debug($queryTSuggest);
									}
							}
						//	echo("size is more than zero".sizeof($bankArray));
							
							//$queryTSuggest .= " and transID in (".$suggestbanks.") ";
						}
					}/*else{
						echo("its empty");
						}*/
				}
					
			}
			if(!empty($queryTSuggest))
				$queryT .= " and (".$queryTSuggest.") ";
		
		}
	
		//echo("-------------------");
	/*	debug($queryT);*/
	}
	//
	//debug($queryT);
}
//debug($queryT);
/*$contents = selectMultiRecords($query);
$allCount = countRecords($queryCnt);*/
	//$allCount = countRecords($queryCnt);
	//$count = $allCount;
	//debug($query);
if($_REQUEST["getGrid"] == "payList"){
	$flagStore = true;
	if($_REQUEST["suggest"] > 0)
	{
		$flagStore = false;
		$query = $queryTemp;
		$sidx = 'amount';
		$sord = 'ASC';
		/*debug($query);*/
	}
	//echo $query;
	//debug($_REQUEST);
	//debug($query);
	$total_pages = 0;
	$count = 0;
	
	if($flagSearchPay)
	{
		$result = mysql_query($query) or die(__LINE__.": ".mysql_query());
		$count = mysql_num_rows($result);
		
		
		
		if($count > 0)
		{
			$total_pages = ceil($count / $limit);
		} else {
			$total_pages = 0;
		}
		
		if($page > $total_pages)
		{
			$page = $total_pages;
		}
		
		$start = $limit * $page - $limit; // do not put $limit*($page - 1)
		
		/**
		 * A patch to handle -ve value in case the $count=0, 
		 * because in this case the $start is set to a -ve value 
		 * which causes query to break.
		 *
		 * @author: Waqas Bin Hasan
		 */
		if($start < 0)
		{
			$start = 0;
		}
		
		$query .= " order by $sidx $sord LIMIT $start , $limit";
		$result = mysql_query($query) or die(__LINE__.": ".mysql_error());
		//echo $query;
		//debug($query);
		//print($query);
	}
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;
	
	$i=0;
	
	//if($flagStore){
			$_SESSION["lastQueryPay"] = array();
			$_SESSION["lastQueryPay"]["amount"] = array();
			$_SESSION["lastQueryPay"]["description"] = array();
			$_SESSION["lastQueryPay"]["id"] = array();
			$_SESSION["lastQueryPay"]["bankAccount"] = array();
			//}
	$totalPayment = 0;
	while($row = mysql_fetch_array($result))
	{
		//if($flagStore)
		//{
			$_SESSION["lastQueryPay"]["amount"][] = $row["amount"];
			$_SESSION["lastQueryPay"]["description"][] = $row["description"];
			$_SESSION["lastQueryPay"]["id"][] = $row["id"];
			$_SESSION["lastQueryPay"]["bankAccount"][] = $row["accountNo"];
		//}
		$response->rows[$i]['id'] = $row["id"];
		$amountV = number_format($row["amount"],"2",".","");
		if($row["entryDate"]== "0000-00-00")
		$entryDate='--';
		else
		$entryDate=dateFormat($row["entryDate"], "2");
		$totalPayment += $amountV;
/*		$bankNameSql = "select bankName from bankDetails where transID = ".$row["transID"];
		$bankNameData = selectFrom($bankNameSql);*/
		
		$response->rows[$i]['cell'] = array(
									dateFormat($row["importedOn"], "2"),
									$row["accountNo"],
									$entryDate,
									$row["tlaCode"],
									number_format($row["amount"],"2",".",""),
									$row["description"]
								);
		$i++;
	}
	if($i>0){
		$response->rows[$i]['cell'] = array(
									'',
									'',
									"<strong>Total:</strong>",
									"<strong>".number_format($totalPayment,"2",".","")."</strong>",
									''
								);
	}	
	echo $response->encode($response); 

}	

if($_REQUEST["getGrid"] == "transList"){
	//echo $queryT;
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_query());
	$countT = mysql_num_rows($resultT);
	//unset($_SESSION["lastQueryTrans"]);
	//debug($_REQUEST);
	//debug($queryT);
	if($_REQUEST["suggest"] > 0)
	{
		$flagStore = false;
		$query = $queryTemp;
		$sidx = 'transAmount';
		$sord = 'ASC';
		/*debug($query);*/
	}	
	if($countT > 0)
	{
		$total_pages = ceil($countT / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryT .= " order by $sidx $sord LIMIT $start , $limit";
	$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_error());

	//debug($queryCnt);

	$response->page = $page;
	$response->total = (isset($total_pages)?$total_pages:0);
	$response->records = (isset($countT)?$countT:0);

	$i=0;
	if($countT > 0)
	{
		$_SESSION["lastQueryTrans"]["amount"] = array();
		$_SESSION["lastQueryTrans"]["reference"] = array();
		$_SESSION["lastQueryTrans"]["tracking_Ref"] = array();
		$_SESSION["lastQueryTrans"]["tracking_Id"] = array();
		$_SESSION["lastQueryTrans"]["custLast"] = array();
		$_SESSION["lastQueryTrans"]["custFirst"] = array();
		$_SESSION["lastQueryTrans"]["city"] = array();
	}

	while($rowT = mysql_fetch_array($resultT))
	{
		$response->rows[$i]['id'] = $rowT["transID"];

		if($rowT["createdBy"]!="CUSTOMER"){
			$custQ = "select accountName,firstName, lastName from ".TBL_CUSTOMER." where customerID = '".$rowT["customerID"]."'";
			$custRS = selectFrom($custQ);
			$custNumber = $custRS["accountName"];
			$custName = $custRS["firstName"] . " ". $custRS["lastName"];
			
			$benQ = "select firstName, lastName from ".TBL_BENEFICIARY." where benID = '".$rowT["benID"]."'";
			$benRS = selectFrom($benQ);
			$benName = $benRS["firstName"] . " ". $benRS["lastName"];
		}
	/*if($tranactionSource != "" && $tranactionSource != "payex")
	{*/
		//$senderBankRS   = selectFrom("select branchAddress from banks where bankId ='".$rowT["senderBank"]."'");
		$senderBankRS   = selectFrom("select custBankBranch from customerBank where transID ='".$rowT["transID"]."'");
		$senderBankName = $senderBankRS["custBankBranch"];
		$response->rows[$i]['cell'] = array(
									number_format($rowT["totalAmount"],"2",".",""),
									$senderBankName,
									$rowT["refNumberIM"],
									$custName,
									$benName,
									dateFormat($rowT["transDate"], "2")
								);
		$_SESSION["lastQueryTrans"]["amount"][] = "'".$rowT["totalAmount"]."'";
		$_SESSION["lastQueryTrans"]["reference"][] = $rowT["refNumberIM"];
		$_SESSION["lastQueryTrans"]["custLast"][] = $custRS["lastName"];
		$_SESSION["lastQueryTrans"]["custFirst"][] = $custRS["firstName"];
		$_SESSION["lastQueryTrans"]["city"][] = $senderBankName;
		
		$tempVal = $rowT["refNumberIM"];
		/*$refTempArr = explode("-", $tempVal);
		if($refTempArr[1] != '')
		{
			$tempVal = $refTempArr[1];
		}*/
		
		$_SESSION["lastQueryTrans"]["tracking_Ref"][] = $tempVal;
		$_SESSION["lastQueryTrans"]["tracking_Id"][] = $rowT["transID"];
		//debug($_SESSION["lastQueryTrans"]["amount"]);
		//debug($_SESSION["lastQueryTrans"]["tracking_Ref"]);
		//debug($_SESSION["lastQueryTrans"]);
		$linkVal = '';
		$urlTrans = '';
		if(strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_EDIT_TRANSACTION_UNRESOLVED=="1"){
			$extraParam = "?transID=".$rowT["transID"]."&back=unres-payments-action.php";
			$urlTrans = $transactionPage.$extraParam;
			$linkVal = "<a href='#' onClick='window.open (\"$urlTrans\",
\"mywindow\",\"scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=900,width=800\");' class='linkClass'>Update</a>";
			$response->rows[$i]['cell'][] = $linkVal;
		}
	/*}else{
		$response->rows[$i]['cell'] = array(
									number_format($rowT["totalAmount"],"2",".",""),
									$rowT["refNumberIM"],
									dateFormat($rowT["transDate"], "2")
								);
		}*/
		$i++;
	}
		echo $response->encode($response); 
}

switch($_REQUEST["action"]){

case 'delet_trans':

 
	  
	$payIDArray	= $_POST["users_val"];
	$payIDArray = explode(",", $payIDArray);
	for($i=0;$i<count($payIDArray);$i++){
 
		update("update barclayspayments set isDeleted = 'Y' where id='".$payIDArray[$i]."'");
	
		$amount = selectFrom("select amount from barclayspayments WHERE id=".$payIDArray[$i]."");	 
		$barcAmount= $amount[amount];
		
			if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
				$query_pay_action = "
								INSERT INTO payment_action(payID,action_id,agentAmount)
											values('".$payIDArray[$i]."', '".$_SESSION["loggedUserData"]["userID"]."','".$barcAmount."')";
				insertInto($query_pay_action);
			}				
		
	} 
 

}
?>