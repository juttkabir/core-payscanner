<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

if($_REQUEST["btnAction"] =="Export All")
{
	if ($offset == "") {
		$offset = 0;
	}
	$limit=50;

	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}

	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
	/**/
		$cntSql = stripslashes(stripslashes($_REQUEST["qc"]));
		$strNewtransIds = "";
		if(!empty($cntSql))
		{
			$cntSql = str_replace("count(*)", "transid", $cntSql);
			//debug($cntSql);
			$arrNewIds = selectMultiRecords($cntSql);
			foreach($arrNewIds as $ai)
				$strNewtransIds .= "'".$ai["transid"]."',";
			
			$strNewtransIds = substr($strNewtransIds,0,strlen($strNewtransIds)-1);
			//debug($strNewtransIds);
		}
	/**/
	
	$distributorInfo  = selectFrom("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where userID='$_REQUEST[banks]' and parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' order by agentCompany");

	$exportTransIDs = "'" . str_replace(",",  "','", $_REQUEST["transIDs"]) . "'";

	$query = "select * from ". TBL_TRANSACTIONS." where transID in (" . $exportTransIDs . ")  order by transDate ";//DESC LIMIT $offset , $limit";

	$joinedQuery = "select 
						transactions.*, 

						concat(beneficiary.firstName, ' ', beneficiary.lastName) as benName,
						beneficiary.phone as benPhone,
						beneficiary.Address as benAddress,
						beneficiary.City as benCity,
						beneficiary.IDNumber as benIDNumber,
						
						admin.name as agentName,
						
						concat(customer.firstName, ' ', customer.lastName) as custName,
						customer.accountName as custAccountName,
						customer.Address as custAddress,
						
						bankDetails.bankName,
						bankDetails.accNo,
						bankDetails.branchCode,
						
						cm_collection_point.cp_branch_address,
						cm_collection_point.cp_city,
						cm_collection_point.cp_corresspondent_name,
						concat(admin.name, '[', admin.username, ']') as bankNameForHomeDelivery

					from ". TBL_TRANSACTIONS." 
					
					LEFT JOIN beneficiary on transactions.benID = beneficiary.benID 
					LEFT JOIN customer on transactions.customerID = customer.customerID 
					LEFT JOIN bankDetails on transactions.transID = bankDetails.transID 
					LEFT JOIN cm_collection_point on transactions.collectionPointID = cm_collection_point.cp_id 
					LEFT JOIN admin on transactions.custAgentID = admin.userID
					
			  where transactions.transID in (" . $strNewtransIds . ")  order by transactions.transDate"; // DESC LIMIT $offset , $limit";
			
			  
			  /*
			  where transactions.transID in (" . $exportTransIDs . ")  order by transactions.transDate DESC";// LIMIT $offset , $limit";
			  LEFT JOIN admin on transactions.benAgentID = admin.userID
			  */
			  
	//debug($joinedQuery);
	$joinQueryTime = microtime();
	$searchedTransactions = selectMultiRecords($joinedQuery);
	$joinQueryTime = microtime() - $joinQueryTime;

	$otherQueryTime = 0;

	$queryTime = microtime();
	$contentsTrans = selectMultiRecords($query);
	$queryTime = microtime() - $queryTime;
	$otherQueryTime += $queryTime;

	foreach ( $contentsTrans as $key => $transactionDetail )
	{
		$contentsTrans[$key]["bankDetails"] = array();
		$contentsTrans[$key]["collectionPoint"] = array();
		$contentsTrans[$key]["homeDelivery"] = array();

		
		$queryTime = microtime();
		$beneficiaryContent = selectFrom("select firstName,middleName, lastName, phone, Address, City,IDType,IDNumber,IDexpirydate,IDissuedate,otherId_name,otherId,CPF from " . TBL_BENEFICIARY . " where benID='".$transactionDetail["benID"]."'");

		$queryTime = microtime() - $queryTime;
		$otherQueryTime += $queryTime;

		$contentsTrans[$key]["beneficiary"] = $beneficiaryContent;


		$queryTime = microtime();
		$customerContent = selectFrom("select firstName,middleName, lastName, Address, Zip,accountName, payinBook,dob,placeOfBirth from " . TBL_CUSTOMER . " where customerID='".$transactionDetail["customerID"]."'");

		$queryTime = microtime() - $queryTime;
		$otherQueryTime += $queryTime;
		
		$contentsTrans[$key]["customer"] = $customerContent;



		if( strtoupper($transactionDetail["transType"]) == strtoupper('Bank Transfer') )
		{
			$queryTime = microtime();			
			$bankDetails = selectFrom("select bankName, accNo, branchAddress, branchCode from ".TBL_BANK_DETAILS." where transID = '".$transactionDetail["transID"]."'");

			$queryTime = microtime() - $queryTime;
			$otherQueryTime += $queryTime;

			$contentsTrans[$key]["bankDetails"] = $bankDetails;
			
		}

		if ( strtoupper($transactionDetail["transType"]) == strtoupper('Pick Up') )
		{
			$queryTime = microtime();
			$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state, cp_corresspondent_name, cp_branch_no, cp_ida_id from " . TBL_COLLECTION . " where cp_id='".$transactionDetail["collectionPointID"]."'");

			$queryTime = microtime() - $queryTime;
			$otherQueryTime += $queryTime;

			$contentsTrans[$key]["collectionPoint"] = $collectionPoint;
		}
		
		if ( strtoupper($transactionDetail["transType"]) == strtoupper('Home Delivery') )
		{
			$queryTime = microtime();			
			$distributorInfo = selectFrom("select userID, username, name  from " . TBL_ADMIN_USERS . " where userID = '".$transactionDetail["benAgentID"]."'");	

			$queryTime = microtime() - $queryTime;
			$otherQueryTime += $queryTime;

			$contentsTrans[$key]["homeDelivery"]["bankName"] = $distributorInfo["name"]."[".$distributorInfo["username"]."]";
		}
	}
	
//	debug( "My Query Took: " . $joinQueryTime );
//	debug( "Old query took: " . $otherQueryTime );
}

?>
<html>
<head>
<title>Distributor Output File</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
body, table, td  { 
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size: 11px; 
}
-->
</style>
</head>
<body onLoad="print();">
<table width="90%" cellpadding="0" cellspacing="0" border="0" style="margin-left:10px;">
	<tr>
		<td>
			TO: <strong><?=strtoupper($distributorInfo["agentCompany"])?></strong>
		</td>
	</tr>
	<? 

	if($_REQUEST["chkComp2"] == "Y"){ 
	?>
	<tr>
	
		<td colspan="2" align="center">
		   <? echo date('d-m-Y'); ?><br />
			<strong><? echo COMPANY_NAME?></strong>&nbsp;&nbsp
			<strong><? echo COMPANY_ADDRESS?></strong>
			<hr />
		</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan="2" align="center">
			<br />
			<strong>Please pay as follows</strong>
			<hr />
		</td>
	</tr>
</table>
		<?
			
			if(!empty($_REQUEST["start_serail_number"]))
				$i = $_REQUEST["start_serail_number"];
			else
				$i = 1;
			
			$counter = 0;
			$localAmountTotal = 0;
			foreach ( $searchedTransactions as $transactionDetails )
			{
				$strPassportIdSql = "select id_number from user_id_types where user_id = '".$transactionDetails["benID"]."' and user_type='B' and id_type_id=(select id from id_types where title='Passport')";
				$arrBeneficiaryId = selectFrom($strPassportIdSql);
				$strBeneficiaryId = $arrBeneficiaryId["id_number"];
				$counter++;
				?>
					<table align="center" width="700" style="padding-left:10px;">
						<tr>
							<td valign="top">
								<table style="border-right:0px solid black; margin-right:5px;" width="400">
									<tr>
										<td valign="top"><?=$i?>:</td>
										<td colspan="2">&nbsp;</td>
									</tr>	
									<tr>
										<td width="30" valign="top">&nbsp;</td>
										<td width="192" valign="top"><div align="right"><strong>Receiver Name:</strong></div></td>
										<td width="162" valign="top"><?=$transactionDetails["benName"]; ?></td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top"><div align="right"><strong>Address:</strong></div></td>
										<td valign="top"><?=$transactionDetails["benAddress"]?></td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top"><div align="right"><strong>Telephone Number:</strong></div></td>
										<td valign="top"><?=$transactionDetails["benPhone"]?></td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top"><div align="right"><strong><?=CONFIG_USE_ID_TYPE_OF_BENEFICIARY?> ID Number:</strong></div></td>
										<td valign="top"><?=$strBeneficiaryId?></td>
									</tr>
									<?
										if ( strtoupper($transactionDetails["transType"]) == strtoupper("Bank Transfer") )
										{
											?>
											
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Account Number:</strong></div></td>
												<td valign="top"><?=$transactionDetails["accNo"];?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Bank Name:</strong></div></td>
												<td valign="top"><?=$transactionDetails["bankName"];?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Branch Code/Name:</strong></div></td>
												<td valign="top"><?=$transactionDetails["branchCode"];?></td>
											</tr>
											<?
										}
										else if ( strtoupper($transactionDetails["transType"]) == strtoupper("Pick up") )
										{
											?>
											
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Collection Point Name:</strong></div></td>
												<td valign="top"><?=$transactionDetails["cp_corresspondent_name"];?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Collection Point Address:</strong></div></td>
												<td valign="top"><?=$transactionDetails["cp_branch_address"];?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>City:</strong></div></td>
												<td valign="top"><?=$transactionDetails["cp_city"];?></td>
											</tr>
											<?
										}
										else if ( strtoupper($transactionDetails["transType"]) == strtoupper("Home Delivery") )
										{
											?>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Bank Name:</strong></div></td>
												<td valign="top"><?=$transactionDetails["bankNameForHomeDelivery"];?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>Delivery Address:</strong></div></td>
												<td valign="top"><?=$transactionDetails["benAddress"]?></td>
											</tr>
											<tr>
												<td valign="top"></td>
												<td valign="top"><div align="right"><strong>City:</strong></div></td>
												<td valign="top"><?=$transactionDetails["beneCity"]?></td>
											</tr>
											<?
										}
									?>
									
									<tr>
										<td valign="top"></td>
										<td valign="top"><div align="right"><strong>Amount:</strong></div></td>
										<td valign="top"><strong><?=$transactionDetails["currencyTo"];?> <?=$transactionDetails["localAmount"];?></strong></td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top" ><div align="right"><strong><?=MANUAL_CODE?>:</strong></div></td>
										<td valign="top"><strong><?=$transactionDetails["refNumber"];?></strong></td>
									</tr>
								
								</table>
							</td>
							<td valign="bottom">
								<table width="300" border="0">
									<tr>
										<td width="50%" valign="top"><div align="right"><strong>Sender Number:</strong></div></td>
										<td valign="top"><?=$transactionDetails["custAccountName"]; ?></td>
									</tr>
									<tr>
										<td valign="top"><div align="right"><strong>Name:</strong></div></td>
										<td valign="top"><?=$transactionDetails["custName"]; ?></td>
									</tr>
									<tr>
										<td valign="top"><div align="right"><strong>Address:</strong></div></td>
										<td valign="top"><?=$transactionDetails["custAddress"]; ?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<hr width="700" <? if($counter%4 == 0 && $counter != count($searchedTransactions)){?>style="page-break-after:always"<? } ?> />
				<?
				   if($counter%4 == 0)
				  	{
				    	echo date('d-m-Y'); 
				  	}
				$localAmountTotal+=$transactionDetails["localAmount"];
				$i++;
			}
		?>
		

		<center><strong>Total</strong>: <?=$localAmountTotal;?>
		<br />
	
		<input type="button" value="Print" onClick="this.style.display='none';print();" />
	</center>
</body>
</html>