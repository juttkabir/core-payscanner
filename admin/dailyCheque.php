<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

	$date_from			= $_REQUEST["date_from"];
	$date_to		 	= $_REQUEST["date_to"];
	$userId                = $_REQUEST["userId"];
	
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$userId                = (!empty($userId))?$userId:$_SESSION["loggedUserData"]["userID"];

	
	$strDateFilter = "";
	
	if(!empty($date_from))
	{
		$date = explode("/",$date_from);
		$strDateFilter = " AND paid_on >= '".date("Y-m-d 00:00:00",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	
	if(!empty($date_to))
	{
		$date = explode("/",$date_to);
		$strDateFilter = " AND paid_on <= '".date("Y-m-d 23:59:59",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
	
	if(empty($date_from) && empty($date_to))
	{
		$strDateFilter = " AND paid_on like '$currentDate%'";
	}

	
	$sql = "SELECT 
				COUNT(recId) AS cnt
			FROM 
				cash_book
			WHERE
				userId = $userId
				AND created like '$currentDate%'
			";
	$result = selectFrom($sql);
	$numRows = $result["cnt"];
	
	if($numRows > 0)
	{
		$latestUpdatesOn = $currentDate;
		$cmd = "UPDATE";
	} else {
		$sql = "SELECT
					DISTINCT DATE_FORMAT(created, '%Y-%m-%d') as created
				FROM
					cash_book
				ORDER BY
					created DESC
				LIMIT
					0,1
				";
		$result = selectFrom($sql);
		$latestUpdatesOn = $result["created"];
	}

	$sql = "SELECT 
				recId,
				openingBalance,
				closingBalance,
				banks,
				overShort
			FROM
				cash_book
			WHERE
				userId = $userId
				AND created like '$latestUpdatesOn%'
			";
	$result = selectFrom($sql);
	$id						= $result["recId"];

	if($numRows > 0)
	{
		$openingBalance 	= $result["openingBalance"];
		$closingBalance		= $result["closingBalance"];
	} else {
		$openingBalance		= $result["closingBalance"];
		$closingBalance		= 0;
	}
	
	$xBalance			= $openingBalance;
	
	activities($_SESSION["loginHistoryID"],"VIEW",0,"cheque_order","Cheque Account Ledger Viewed");

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cash Book</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#date_from").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});
	
	$("#date_to").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});
});

function updateBalance(b)
{
	var banks = parseFloat(document.getElementById("banks").value);
	var closingBalance = parseFloat(document.getElementById("closingBalance").value);
	var final = 0;
	
	closingBalance = closingBalance + b;
	final = closingBalance - banks;
	
	document.getElementById("closingBalance").value = final;
	document.getElementById("balanceAfterBank").innerHTML = final;
	document.getElementById("closingFinal").innerHTML = final;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
</head>

<body>

<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<? if($agentType == "admin") { ?>
		<tr>
			<td align="right"><b>Agents</b></td>
			<?
			/** #4747
				All user types are displayed in Drop down
				under name of user tyep
				by Aslam Shahid.
			**/
			$allUsersSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						";

			?>
			<td align="left">&nbsp;
				<select name="userId" id="userId">
					<option value=""> -Select User- </option>

					<optgroup label="Super Admin">
					<? 	
						$superAdminClause = " where adminType ='Supper' AND isMain='Y' AND agentStatus='Active' order by username";
						$superAdminRS = selectMultiRecords($allUsersSql.$superAdminClause);
					?>
					<? for($i=0; $i<sizeof($superAdminRS); $i++) { ?>
						<option value="<?=$superAdminRS[$i]["userID"]?>"><?=$superAdminRS[$i]["username"]."[".$superAdminRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Agents">
					<? 	
						$agentClause = " where adminType = 'Agent' AND parentID > 0 AND isCorrespondent != 'ONLY' AND agentStatus='Active' order by username";
						$agentsRS = selectMultiRecords($allUsersSql.$agentClause);
					?>
					<? for($i=0; $i<sizeof($agentsRS); $i++) { ?>
						<option value="<?=$agentsRS[$i]["userID"]?>"><?=$agentsRS[$i]["username"]."[".$agentsRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Distributors">
					<? 	
						$distClause = " where adminType='Agent' AND isCorrespondent ='ONLY' AND agentStatus='Active' order by username";
						$distRS = selectMultiRecords($allUsersSql.$distClause);
					?>
					<? for($i=0; $i<sizeof($distRS); $i++) { ?>
						<option value="<?=$distRS[$i]["userID"]?>"><?=$distRS[$i]["username"]."[".$distRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					
					<optgroup label="Admin Staff">
					<? 	
						$adminStaffClause = " where adminType = 'Admin' AND agentStatus='Active' order by username";
						$adminStaffRS = selectMultiRecords($allUsersSql.$adminStaffClause);
					?>
					<? for($i=0; $i<sizeof($adminStaffRS); $i++) { ?>
						<option value="<?=$adminStaffRS[$i]["userID"]?>"><?=$adminStaffRS[$i]["username"]."[".$adminStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>

					<optgroup label="Branch Manager">
					<? 	
						$branchManagerClause = " where adminType='Branch Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$branchManagerRS = selectMultiRecords($allUsersSql.$branchManagerClause);
					?>
					<? for($i=0; $i<sizeof($branchManagerRS); $i++) { ?>
						<option value="<?=$branchManagerRS[$i]["userID"]?>"><?=$branchManagerRS[$i]["username"]."[".$branchManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>

					<optgroup label="Admin Manager">
					<? 	
						$adminManagerClause = " where adminType='Admin Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$adminManagerRS = selectMultiRecords($allUsersSql.$adminManagerClause);
					?>
					<? for($i=0; $i<sizeof($adminManagerRS); $i++) { ?>
						<option value="<?=$adminManagerRS[$i]["userID"]?>"><?=$adminManagerRS[$i]["username"]."[".$adminManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Call Center Staff">
					<? 	
						$callCenterStaffClause = " where adminType='Call' AND isMain='N' AND agentStatus='Active' order by username";
						$callCenterStaffRS = selectMultiRecords($allUsersSql.$callCenterStaffClause);
					?>
					<? for($i=0; $i<sizeof($callCenterStaffRS); $i++) { ?>
						<option value="<?=$callCenterStaffRS[$i]["userID"]?>"><?=$callCenterStaffRS[$i]["username"]."[".$callCenterStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>					
					<optgroup label="Support Staff">
					<? 	
						$supportStaffClause = " where adminType ='Support' AND agentStatus='Active' order by username";
						$supportStaffRS = selectMultiRecords($allUsersSql.$supportStaffClause);
					?>
					<? for($i=0; $i<sizeof($supportStaffRS); $i++) { ?>
						<option value="<?=$supportStaffRS[$i]["userID"]?>"><?=$supportStaffRS[$i]["username"]."[".$supportStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
<?php /*?>					<optgroup label="MLRO">
					// #4747 - Minas Center - commented by Aslam Shahid
					// As MLRO is not in Minas Center
					
					<? 	
						$mlroClause = " where adminType ='MLRO' AND  isMain='N' AND  agentStatus='Active' order by username";
						$mlroRS = selectMultiRecords($allUsersSql.$mlroClause);
					?>
					<? for($i=0; $i<sizeof($mlroRS); $i++) { ?>
						<option value="<?=$mlroRS[$i]["userID"]?>"><?=$mlroRS[$i]["username"]."[".$mlroRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup><?php */?>
					
					<optgroup label="COLLECTOR">
					<? 	
						$collectorClause = " where adminType ='COLLECTOR' AND  isMain='N' AND  agentStatus='Active' order by username";
						$collectorRS = selectMultiRecords($allUsersSql.$collectorClause);
					?>
					<? for($i=0; $i<sizeof($collectorRS); $i++) { ?>
						<option value="<?=$collectorRS[$i]["userID"]?>"><?=$collectorRS[$i]["username"]."[".$collectorRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					
					<optgroup label="Distributor Manager">
					<? 	
						$distManagerClause = " where adminType ='SUPI Manager' AND  isMain='N' AND agentStatus='Active' order by username";
						$distManagerRS = selectMultiRecords($allUsersSql.$distManagerClause);
					?>
					<? for($i=0; $i<sizeof($distManagerRS); $i++) { ?>
						<option value="<?=$distManagerRS[$i]["userID"]?>"><?=$distManagerRS[$i]["username"]."[".$distManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					
<?php /*?>					<optgroup label="TELLOR">
					<? 	
						$tellorQuery = "select tellerID, loginName, name from " . TBL_TELLER . " order by loginName";
						$tellorRS = selectMultiRecords($tellorQuery);
					?>
					<? for($i=0; $i<sizeof($tellorRS); $i++) { ?>
						<option value="<?=$tellorRS[$i]["tellerID"]?>"><?=$tellorRS[$i]["loginName"]."[".$tellorRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup><?php */?>
<?php /*?>					
					<optgroup label="PAYING BOOK CUSTOMER">
					<? 	
						$payinCustQuery = "select userID, userName, name from " . TBL_TELLER . " order by userName";
						$payinCustRS = selectMultiRecords($payinCustQuery);
					?>
					<? for($i=0; $i<sizeof($payinCustRS); $i++) { ?>
						<option value="<?=$payinCustRS[$i]["userID"]?>"><?=$payinCustRS[$i]["userName"]."[".$payinCustRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup><?php */?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$userId; ?>");
			   </script>
			</td>
		</tr>
		<? } ?>
		<tr>
			<td align="left">
				<b>From Date:</b>
				&nbsp;
				<input type="text" name="date_from" id="date_from" value="<?=$_REQUEST["date_from"]?>" />
			</td>
			<td align="left">
				<b>To Date:</b>
				&nbsp;
				<input type="text" name="date_to" id="date_to" value="<?=$_REQUEST["date_to"]?>" />
			</td>
		</tr>
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2">
				<input type="submit" name="Submit" value="Submit" />
			</td>
		</tr>
	</table>
</form>

<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader"><?=SYSTEM?> (Cash Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="5%" class="columnTitle">Sr.#</td>
					<td width="50%" class="columnTitle">Particulars</td>
					<td width="10%" align="right" class="columnTitle">Dr. &pound; </td>
					<td width="10%" align="right" class="columnTitle">Cr. &pound;</td>
					<td width="10%" align="right" class="columnTitle">Balance &pound;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">Opening Balance b/f </td>
					<td align="right">-  </td>
					<td align="right">-</td>
					<td align="right">
						<?=$openingBalance?>	
					</td>
				</tr>
				<?
					$sql = "SELECT 
								t.refNumberIM, 
								t.totalAmount
							FROM 
								transactions t
							WHERE 
								t.transStatus IN('Authorize', 'Credited', 'Pending', 'Amended', 'Picked up', 'Processing', 'AwaitingCancellation')
								AND t.custAgentID = $userId
								AND t.transDate like '$currentDate%'
							";
					$result = SelectMultiRecords($sql);
					for($i=0; $i<sizeof($result); $i++)
					{
						//$xTransactionId			= $result[$i]["refNumberIM"];
						$xTransactionAmount		= $result[$i]["totalAmount"];
						$xBalance				+= $xTransactionAmount;
						$xTotalTransactions		+= $xTransactionAmount;
					}

					$sql = "SELECT 
								t.refNumberIM, 
								t.totalAmount 
							FROM 
								transactions t
							WHERE 
								t.transStatus IN('Cancelled', 'Failed', 'Rejected', 'Suspicious')
								AND t.custAgentID = $userId
								AND t.transDate like '$currentDate%'
							";
					$result = SelectMultiRecords($sql);
					
					for($i=0; $i<sizeof($result); $i++)
					{
						$xTransactionId			= $result[$i]["refNumberIM"];
						$xTransactionAmount		= $result[$i]["totalAmount"];
						$xBalance				-= $xTransactionAmount;
						$xTotalTransactions		-= $xTransactionAmount;
					}


					$sqlCheque = "SELECT 
								sum(paid_amount) as pa
							FROM 
								cheque_order
							WHERE 
								status = 'ID'
								AND cheque_currency = 'GBP'
								AND paid_by like '".$userId."%'
								".$strDateFilter."
							";
					$result = SelectFrom($sqlCheque);
					
					$xChequeAmount		= $result["pa"];
					$xBalance		    -= $xChequeAmount;
				?>

				
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">Total Transactions </td>
					<td align="right">0.00</td>
					<td align="right" class="heading"><?=number_format($xTotalTransactions, 2, ".", ",")?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading"><a href="chequeAccountDetail.php?userid=<?=$userId?>&df=<?=$_REQUEST["date_from"]?>&dt=<?=$_REQUEST["date_to"]?>">Total Cheque Order Paid</a></td>
					<td align="right" class="heading"><?=number_format($xChequeAmount, 2, ".", ",")?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">Closing Balance of the Day </td>
					<td align="right">-</td>
					<td align="right">-</td>
					<td align="right" class="netBalance" id="closingFinal"><?=number_format($xBalance, 2, ".", ",")?></td>
				</tr>
			</table>
			<br />
			</form>
		</td>
	</tr>
</table>
</body>
</html>
