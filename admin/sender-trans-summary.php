<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$reportName='sender-trans-summary.php';
$reportLabel='Sender Transaction Summary';
$reportFlag=true;
$executeQuery=false;
$filterValue=array();
$filterName=array();

$newOffset = $_REQUEST["newOffset"];

if ($offset == "") {
	$offset = 0;
}
//debug($_REQUEST);

//CONFIG_MAX_TRANSACTIONS

if ($limit == 0) {
	$limit = 15;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

if(!empty($_REQUEST["fromDate"]))
{
	$fD = explode("-",$_REQUEST["fromDate"]);
	if (count($fD) == 3)
	{
		$rfDay = $fD[2];
		$rfMonth = $fD[1];
		$rfYear = $fD[0];
	}
}
else
{
    $rfDay = $_REQUEST["fDay"];
	$rfMonth =$_REQUEST["fMonth"];
	$rfYear =$_REQUEST["fYear"];
}

if(!empty($_REQUEST["toDate"]))
{
	$tD = explode("-",$_REQUEST["toDate"]);
	if (count($tD) == 3)
	{
		$rtDay = $tD[2];
		$rtMonth = $tD[1];
		$rtYear = $tD[0];
	}
}
else
{
	$rtDay = $_REQUEST["tDay"];
	$rtMonth = $_REQUEST["tMonth"];
	$rtYear = $_REQUEST["tYear"];
}

$custName= $_REQUEST["custName"];
$custNumber= $_REQUEST["custNumber"];
$custAgent= $_REQUEST["custAgent"];


if($RID!=''){
	
	$reportFlag=false;
	$filterArray=array(); 
	 $value=array(); 
	 $finalArray=array();
		$compQuery = "select queryID,RID,filterName,filterValue from " . TBL_COMPLIANCE_QUERY_LIST ." where RID=".$RID."";
		$compRecord = selectMultiRecords($compQuery); 
		$compQueryMain = selectFrom("select note from " . TBL_COMPLIANCE_QUERY ." where RID=".$RID."");
				
		for($c=0; $c< count($compRecord) ; $c++)
		{
		
		 $filterArray[$c]= $compRecord[$c]["filterName"];
		 $value[$c]= $compRecord[$c]["filterValue"];
		 $finalArray[$filterArray[$c]]=$value[$c];
		 
		} 
	
	$Save=$finalArray["Save"];
	$Submit=$finalArray["Submit"];
	$custName= trim( $finalArray["custName"] );
	$custNumber= trim( $finalArray["custNumber"] );
	$custAgent= trim( $finalArray["custAgent"] );
	$fromDate=$finalArray["fromDate"];
	$toDate=$finalArray["toDate"];
 }	  

if($_REQUEST["Submit"]!= "" || $Save == 'Save Report')
{

	
	
	if($_REQUEST["Submit"]!= ""){
	$filterValue[]=$Submit;
	$filterName[]='Submit';
}elseif($Save == 'Save Report'){
	$filterValue[]=$Save;
	$filterName[]='Save';
	}

if ($_POST["note"] != "")
{
	$note = $_POST["note"];
}elseif ($_GET["note"] != "")
{
	$note = $_GET["note"];	
}
		
	if(!empty($rfYear) && !empty($rfMonth) && !empty($rfDay)){	
		 $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
		 $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	 }
	
$custIDQry = "SELECT customerID FROM " . TBL_CUSTOMER . " as c WHERE 1 ";
$custIDCnt = "SELECT count(customerID) FROM " . TBL_CUSTOMER . " as c WHERE 1 ";
$executeQuery=true;

}
	
if ($custName != "") {

		$filterValue[]=$custName;
	  $filterName[]='custName';
	 // searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";

			 /*************** Comments by Usman Ghani ********************/
			 // The function searchNameMultiTables was not generating appropriate query clause
			 // thats why I commented out this portion and wrote some new code below to generate appropriate query clause.
			 // $q=searchNameMultiTables($custName,$fn,$mn,$ln,$alis);  // This line has been commented out.
			 
			/****** start of new code to generate appropriate query clause ************/
				$q .= " CONCAT(c.firstName, ' ', c.lastName) like '%" . $custName . "%' ";
				/* $custNameParts = explode( " ", $custName );
				foreach( $custNameParts as $part )
				{
					 $q .= " c.firstName like '%" . $part . "%' or c.lastName like '%" . $part . "%' or ";
				}
				$q = substr( $q, 0, strlen($q) - 3 ); */
				$q = " and (" . $q . ")";
			/****** end of new code to generate appropriate query clause ************/
			
		   $custIDQry .= $q;
		   $custIDCnt .= $q;
		   $executeQuery=true;
}
if ($custNumber != "") {
		$custIDQry .= " AND c.accountName= '".$custNumber."' ";
		$custIDCnt .= " AND c.accountName= '".$custNumber."' ";
		
		$filterValue[]=$custNumber;
	  $filterName[]='custNumber';
	   $executeQuery=true;
	}
	
if ($fromDate != "" && $toDate!= "")
{
	$filterValue[]=$fromDate;
	$filterName[]='fromDate';

	$filterValue[]=$toDate;
	$filterName[]='toDate';

	$executeQuery=true;
}			
if($custAgent != ""){
    
			$custAgentQuery= selectFrom("select userID from " . TBL_ADMIN_USERS . " as u where  username= '".$custAgent."' ");

			if ( empty($custAgentQuery["userID"]) ) 
			    $executeQuery=false;
			else
				$executeQuery=true;
		}	

if($executeQuery==true)	{
		
		//echo "<br>".$custIDQry;
 	  $custIDRes = selectMultiRecords($custIDQry);
}
	




// customer id
	$numberOfCustIDs = count($custIDRes);
	if ($numberOfCustIDs > 0)
	{
		for ($i = 0; $i < $numberOfCustIDs; $i++)
		{
			$custIDs .= "'" . $custIDRes[$i]['customerID'] . "',";
		}
		$custIDs = substr($custIDs, 0, (strlen($custIDs) - 1));
		$executeQuery=true;
	}
	else
		$executeQuery=false;

	//echo"<br>--id--".$custIDs;

if ($_REQUEST["Submit"] == "Search" || $Save == 'Save Report') {
	
			
	$query = "SELECT count(transID), sum(totalAmount), transDate, customerID,custAgentID FROM ". TBL_TRANSACTIONS . "  WHERE 1 AND custAgentID=$userID AND transStatus != 'Cancelled'  ";
	$queryCnt = "SELECT count(distinct(customerID)) FROM ". TBL_TRANSACTIONS . "  WHERE 1 AND custAgentID=$userID AND transStatus != 'Cancelled' ";
	
	if ($custIDs != "") {
		$query .= " AND customerID IN (".$custIDs.") ";
		$queryCnt .= " AND customerID IN (".$custIDs.") ";
	}
	if($custAgent != "" && !empty($custAgentQuery["userID"]) ){
		$query .= " AND custAgentID= ".$custAgentQuery["userID"]."  ";
		$queryCnt .= " AND custAgentID = ".$custAgentQuery["userID"]."  ";
		
		}
	
	
	
	
	if ($fromDate != "" && $toDate!= "") {
	$query .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
	$queryCnt .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
}
  
	 $query .= " Group By customerID ORDER BY $sortBy DESC ";
  $query .= " LIMIT $offset , $limit";
  $allCount = countRecords($queryCnt);

	$contentsTrans = array();
	//if ($executeQuery)
	echo $query;
		$contentsTrans = selectMultiRecords($query);

if($Save == 'Save Report' && $reportFlag == true){
 	$Querry_Sqls="insert into ".TBL_COMPLIANCE_QUERY." (reportName,logedUser,logedUserID,queryDate,reportLabel,note) values ('".$reportName."','".$agentType."','".$loggedUserID."','".$date_time."','".$reportLabel."','".$note."')";
	insertInto($Querry_Sqls);
	$reportID = @mysql_insert_id();
	

	for($i=0;$i< count($filterName);$i++){
	$Querry_Sqls2="insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$filterName[$i]."','".$filterValue[$i]."')";
	insertInto($Querry_Sqls2); 
  }
	$msg="Report Has Been Saved";  
    
}
	
	
}
	
?>
<html>
<head>
	<title><?=__("Sender");?> Transaction Summary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
<script language="javascript">
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><?=__("Sender");?> Transaction Summary</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="sender-trans-summary.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap align="center">  
             
       <?=__("Sender");?> Name 
         <input type="text" name="custName" id="custName" value="<? echo $custName ?>">
       <?=__("Sender");?> Number <input type="text" name="custNumber" id="custName" value="<? echo $custNumber ?>">
       Agent Number <input type="text" name="custAgent" id="custName" value="<? echo $custAgent ?>">
      </td>
	</tr>
		     <tr>
       <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
								echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					
           <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
					
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
									echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
          	SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
								echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         		SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
          	SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
      </tr>
     <tr> <td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	 <!-- </form> -->
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
     <!-- <form action="sender-trans-summary.php?action=<? echo $_GET["action"]?>" method="post" name="trans"> -->


          <?
			if (count($contentsTrans) > 0){
		?>
       
       <tr>
	    	<td  bgcolor="#000000">
				
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">	
						<? if($msg!= '') { ?>
						<tr>
							  <td colspan="2" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></b></i><strong><? echo $msg; ?></strong></font></td>
						</tr>
						<? } ?>
	 					<tr>
							<td valign="center" align="right">Add Note/Comment</td>
							<td><textarea name="note" cols="40"><?=$compQueryMain["note"];?></textarea></td>
	        		
						</tr>  
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">	
	      	 <input type="submit" name="Save" value="Save Report">
	      </td>
	    </tr>
	       
      <tr>
      	<td  bgcolor="#000000">
					<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
				      <tr>
                  <td>
                    <?php if (count($allCount) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1"><?=__("Sender");?> Number</span></td>
			  <td width="100"><span class="style1"><?=__("Sender");?> Name </span></td>
			  <td width="100"><span class="style1">Registration Date </span></td>
			  <td width="100"><span class="style1">Number of Transactions </span></td>
		    <td width="100"><span class="style1">Amount Sent</span></td>
		    <? if(CONFIG_SENDER_TRANSACTION_SUMMARY_DOCUMENTS == "1") { ?>
		    <td width="100">&nbsp;</td>
			<? }?>
			 
			</tr>
		    <? 
		     $totalTrans=count($contentsTrans);
		    for($i=0;$i < count($contentsTrans);$i++)			{
		    
		 // echo"--id--".$custAgentQuery["userID"];
		     $custQuery = selectFrom("SELECT customerID,firstName,lastName,accountName,created FROM " . TBL_CUSTOMER . "  WHERE  customerID= '".$contentsTrans[$i]["customerID"]."' ") ;
	    //   echo "SELECT customerID,firstName,lastName,accountName,created FROM " . TBL_CUSTOMER . "  WHERE  customerID= '".$contentsTrans[$i]["customerID"]."' " ;      
		    

					
			?>
				
				<tr bgcolor="#FFFFFF">
					
				
				<td width="81" >
				  	<a href="#" onClick="javascript:window.open('sender-trans-summary-detail.php?customerID=<? echo $contentsTrans[$i]["customerID"]; ?>&custAgentID=<? echo $custAgentQuery["userID"]; ?> &toDate=<? echo $toDate;?>&fromDate=<? echo $fromDate; ?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $custQuery["accountName"];?></font></strong></a>
				</td>
				 
			
					  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $custQuery["firstName"]." ".$custQuery["lastName"]; ?></font></td>				  	
				   <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $custQuery["created"]; ?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["count(transID)"]; ?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo number_format($contentsTrans[$i]["sum(totalAmount)"],2,'.',','); ?></font></td>
					<? if(CONFIG_SENDER_TRANSACTION_SUMMARY_DOCUMENTS == "1") { 
					?>
							<td bgcolor="#FFFFFF" align="center">
								<a class="style2" onclick="javascript:window.open('viewDocument.php?customerID=<?=$contentsTrans[$i]["customerID"]?>', 'ViewDocuments', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="#">
									View Documents
								</a>
							</td>
							<? } ?>					  
				</tr>
				<?
			}
			
			} // greater than zero
			
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
					<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
				      <tr>
                  <td>
                    <?php if (count($allCount) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&custName=$custName&custNumber=$custNumber&custAgent=$custAgent&fromDate=$fromDate&toDate=$toDate&Submit=".$_REQUEST["Submit"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>