<? 
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();
	//$currentdate = date('d-m-Y  h:i:s A');
	$todayDate = date('Y-m-d');
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
    $print = $_REQUEST["print"];
	
	if(!empty($_REQUEST["date_from"]))
		$date_from	= $_REQUEST["date_from"];
	else
		$date_from = date('d/m/Y');	
		
	if(!empty($_REQUEST["date_to"]))
	 	$date_to = $_REQUEST["date_to"];
	else
		$date_to = date('d/m/Y');	
	
	$userId                = $_REQUEST["userId"];
	$account 	= $_REQUEST["account"];
	$currency 	= $_REQUEST["currency"];
	$transType 	= $_REQUEST["transType"];
	$transIDV 	= $_REQUEST["transID"];
	
    //debug($_REQUEST);
	$strFilter = "";
	$strBalanceFilter = "";
	if(!empty($date_from))
	{
		$date = explode("/",$date_from);
		$strFilter .= " AND created >= '".date("Y-m-d 00:00:00",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
		
		$strDateFrom = " AND dated < '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
	}
//	debug($strDateFrom);
	if(!empty($date_to))
	{
		$date2 = explode("/",$date_to);
		$strFilter .= " AND created <= '".date("Y-m-d 23:59:59",mktime(0,0,0,$date2[1],$date2[0],$date2[2]))."'";
		$strDateTo = " AND dated = '".date("Y-m-d",mktime(0,0,0,$date2[1],$date2[0],$date2[2]))."'";
	}
	if(!empty($userId))
	{
	 	$sqlUser = selectFrom("SELECT username From ".TBL_ADMIN_USERS." WHERE userID = '".$_REQUEST["userId"]."'");
		$username = $sqlUser["username"];
		//$strFilter.= "AND (crAccount = '".$username."' OR drAccount = '".$username."')";	
	}
  	
	if(!empty($account))
		$strFilter .= "AND (crAccount = '".$account."' OR drAccount = '".$account."')";
	
	if(!empty($transType))
		$strFilter .= " AND transType = '".$transType."' ";	
	if(!empty($transIDV))
		$strFilter .= " AND transID = '".$transIDV."' ";

   	if(!empty($_REQUEST["Submit"]))
	{
	$transType="TT";
		$strAccount = "SELECT 
							   id,
							   userID,
							   transID,
							   crAmount,
							   drAmount,
							   crAccount,
							   drAccount,
							   currency,
							   description,
							   status,
							   note,
							   created
						  FROM 
							 ".TBL_ACCOUNT_LEDGERS." 
						  WHERE 1 
						  AND transType='".$transType ."'
						  ".$strFilter;
		
				  
		$accountRs = selectMultiRecords($strAccount.' ORDER BY id asc');
		//debug($strAccount);
//		debug($accountRs);
		$strOpeningBalance = selectFrom("SELECT id, closing_balance, currency 
											FROM  ".TBL_ACCOUNT_SUMMARY." 
												WHERE 
													accountNumber = '".$account."'
													".$strDateFrom." ORDER BY id DESC
									");			
									
		$openingBalance = $strOpeningBalance["closing_balance"];
		//debug($openingBalance);
		
		if(!is_array($strOpeningBalance)){
		$strDateFrom = " AND dated < '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
			$strOpeningBalance = selectFrom("SELECT id, opening_balance, currency , closing_balance
											FROM  ".TBL_ACCOUNT_SUMMARY." 
												WHERE 
													accountNumber = '".$account."'
													".$strDateFrom." AND id = (
																		SELECT max( id )
																		FROM account_summary
																		WHERE accountNumber = '".$account."'
																		".$strDateFrom.")
												");			
									
		$openingBalance = $strOpeningBalance["closing_balance"];
	
		
		}
		//debug($openingBalance);
     	$strClosingBalance = selectFrom("SELECT id, opening_balance, closing_balance, currency 
										  	FROM  ".TBL_ACCOUNT_SUMMARY." 
												WHERE accountNumber = '".$account."' ".$strDateTo."	");	
								
		$closing_balance = $strClosingBalance["closing_balance"];
	//	debug($strOpeningBalance);
		$strQueryFilter = "SELECT id,balanceType,balance 
							FROM ".TBL_ACCOUNTS." 
								WHERE	accounNumber = '".$account."' 
										AND created >= '".date("Y-m-d 00:00:00",mktime(0,0,0,$date[1],$date[0],$date[2]))."' 
										AND created <= '".date("Y-m-d 23:59:59",mktime(0,0,0,$date2[1],$date2[0],$date2[2]))."'";
		$getStartingBalanceFilter = selectFrom($strQueryFilter);
		//debug($strQueryFilter);
		$strQueryAll = "SELECT id,balanceType,balance 
							FROM ".TBL_ACCOUNTS." 
								WHERE	accounNumber = '".$account."'";
		
		
		$getStartingBalance = selectFrom($strQueryAll);
		//debug($getStartingBalance);
		$openingCrBalance = 0;		
		$openingDrBalance = 0;
		//$openingBalance = 0;
		if($getStartingBalance["balanceType"] == 'Dr')
		{
			//if(count($accountRs)>0){
				$openingDrBalance = -$getStartingBalance["balance"];
				$openingBalance += $openingDrBalance;
				//debug($openingBalance);
			//}
		}
		else
		{
			//if(count($accountRs)>0){
				$openingCrBalance = $getStartingBalance["balance"];
				$openingBalance += $openingCrBalance;
				//debug($openingBalance);
			//}
		}
		//debug($openingBalance .'+'. $strClosingBalance['opening_balance']);
		/*if(empty($getStartingBalanceFilter['id']))
			$openingBalance = $openingBalance + $strClosingBalance['opening_balance'];*/
		/*	if($date_from == $date_to){
			$openingBalance = $openingBalance + $strClosingBalance['opening_balance'];
			}*/
	}	
	//debug($openingBalance);


/* #8794 - AMB Exchange
 * Transaction ID format on the basis of currency exchange [CE-1]and 
 * inter fund transfer [FT-1]. also it shows the search filter on view accounts page.
 * also account number is displayed which is affected against selected account for fund transfer
*/
$transTypeFlag = false;
if(CONFIG_ACCOUNT_LEDGER_TRANS_TYPE == 1)
	$transTypeFlag = true;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View Sender Accounts</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
	.ce{
		color:#0066FF;
		cursor:pointer;
	}
</style>
<script src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#date_from").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		
		$("#date_to").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
	
	});
	
	function updateBalance(b)
	{
		var banks = parseFloat(document.getElementById("banks").value);
		var closingBalance = parseFloat(document.getElementById("closingBalance").value);
		var final = 0;
		
		closingBalance = closingBalance + b;
		final = closingBalance - banks;
		
		document.getElementById("closingBalance").value = final;
		document.getElementById("balanceAfterBank").innerHTML = final;
		document.getElementById("closingFinal").innerHTML = final;
	}
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	
	function manageDisplay(rec)
	{
		if($("#right"+rec).html() == "[+]")
			$("#right"+rec).html("[-]");
		else
			$("#right"+rec).html("[+]");
		
		$(".sub"+rec).toggle();
	}
	
	<!--
	function popup(url) 
	{
		var width  = 1000;
		var height = 900;
		var left   = (screen.width  - width)/2;
		var top    = (screen.height - height)/2;
		var params = 'width='+width+', height='+height;
		params += ', top='+top+', left='+left;
		params += ', directories=no';
		params += ', location=no';
		params += ', menubar=no';
		params += ', resizable=yes';
		params += ', scrollbars=yes';
		params += ', status=no';
		params += ', toolbar=no';
		newwin=window.open(url,'windowname5', params);
		
		if (window.focus) {newwin.focus()}
		
		return false;
	}
</script>

</head>

<body>
<? 
if($print != "Y")
{
?>
	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="4">Search</td>
		</tr>
		
		<tr>
			<td align="left"><b>From Date:</b> </td>
				
				<td> <input type="text" name="date_from" id="date_from" value="<?=$_REQUEST["date_from"]?>" /></td>
			<td align="left">&nbsp;<b>To Date:</b></td>
				
				<td><input type="text" name="date_to" id="date_to" value="<?=$_REQUEST["date_to"]?>" />	</td>
		</tr>
		<? if($agentType == "admin2") { ?>
		<tr>
			<td align="left"><b>Agents</b></td>
			<?
			
			$allUsersSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						";

			?>
			<td align="left">
				<select name="userId" id="userId">
					<option value=""> -Select User- </option>

					<optgroup label="Super Admin">
					<? 	
						$superAdminClause = " where adminType ='Supper' AND isMain='Y' AND agentStatus='Active' order by username";
						$superAdminRS = selectMultiRecords($allUsersSql.$superAdminClause);
					?>
					<? for($i=0; $i<sizeof($superAdminRS); $i++) { ?>
						<option value="<?=$superAdminRS[$i]["userID"]?>"><?=$superAdminRS[$i]["username"]."[".$superAdminRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Agents">
					<? 	
						$agentClause = " where adminType = 'Agent' AND parentID > 0 AND isCorrespondent != 'ONLY' AND agentStatus='Active' order by username";
						$agentsRS = selectMultiRecords($allUsersSql.$agentClause);
					?>
					<? for($i=0; $i<sizeof($agentsRS); $i++) { ?>
						<option value="<?=$agentsRS[$i]["userID"]?>"><?=$agentsRS[$i]["username"]."[".$agentsRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Distributors">
					<? 	
						$distClause = " where adminType='Agent' AND isCorrespondent ='ONLY' AND agentStatus='Active' order by username";
						$distRS = selectMultiRecords($allUsersSql.$distClause);
					?>
					<? for($i=0; $i<sizeof($distRS); $i++) { ?>
						<option value="<?=$distRS[$i]["userID"]?>"><?=$distRS[$i]["username"]."[".$distRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					
					<optgroup label="Admin Staff">
					<? 	
						$adminStaffClause = " where adminType = 'Admin' AND agentStatus='Active' order by username";
						$adminStaffRS = selectMultiRecords($allUsersSql.$adminStaffClause);
					?>
					<? for($i=0; $i<sizeof($adminStaffRS); $i++) { ?>
						<option value="<?=$adminStaffRS[$i]["userID"]?>"><?=$adminStaffRS[$i]["username"]."[".$adminStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>

					<optgroup label="Branch Manager">
					<? 	
						$branchManagerClause = " where adminType='Branch Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$branchManagerRS = selectMultiRecords($allUsersSql.$branchManagerClause);
					?>
					<? for($i=0; $i<sizeof($branchManagerRS); $i++) { ?>
						<option value="<?=$branchManagerRS[$i]["userID"]?>"><?=$branchManagerRS[$i]["username"]."[".$branchManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>

					<optgroup label="Admin Manager">
					<? 	
						$adminManagerClause = " where adminType='Admin Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$adminManagerRS = selectMultiRecords($allUsersSql.$adminManagerClause);
					?>
					<? for($i=0; $i<sizeof($adminManagerRS); $i++) { ?>
						<option value="<?=$adminManagerRS[$i]["userID"]?>"><?=$adminManagerRS[$i]["username"]."[".$adminManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					<optgroup label="Call Center Staff">
					<? 	
						$callCenterStaffClause = " where adminType='Call' AND isMain='N' AND agentStatus='Active' order by username";
						$callCenterStaffRS = selectMultiRecords($allUsersSql.$callCenterStaffClause);
					?>
					<? for($i=0; $i<sizeof($callCenterStaffRS); $i++) { ?>
						<option value="<?=$callCenterStaffRS[$i]["userID"]?>"><?=$callCenterStaffRS[$i]["username"]."[".$callCenterStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>					
					<optgroup label="Support Staff">
					<? 	
						$supportStaffClause = " where adminType ='Support' AND agentStatus='Active' order by username";
						$supportStaffRS = selectMultiRecords($allUsersSql.$supportStaffClause);
					?>
					<? for($i=0; $i<sizeof($supportStaffRS); $i++) { ?>
						<option value="<?=$supportStaffRS[$i]["userID"]?>"><?=$supportStaffRS[$i]["username"]."[".$supportStaffRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
<?php /*?>					<optgroup label="MLRO">
					// #4747 - Minas Center - commented by Aslam Shahid
					// As MLRO is not in Minas Center
					
					<? 	
						$mlroClause = " where adminType ='MLRO' AND  isMain='N' AND  agentStatus='Active' order by username";
						$mlroRS = selectMultiRecords($allUsersSql.$mlroClause);
					?>
					<? for($i=0; $i<sizeof($mlroRS); $i++) { ?>
						<option value="<?=$mlroRS[$i]["userID"]?>"><?=$mlroRS[$i]["username"]."[".$mlroRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup><?php */?>
					
					<optgroup label="COLLECTOR">
					<? 	
						$collectorClause = " where adminType ='COLLECTOR' AND  isMain='N' AND  agentStatus='Active' order by username";
						$collectorRS = selectMultiRecords($allUsersSql.$collectorClause);
					?>
					<? for($i=0; $i<sizeof($collectorRS); $i++) { ?>
						<option value="<?=$collectorRS[$i]["userID"]?>"><?=$collectorRS[$i]["username"]."[".$collectorRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
					
					<optgroup label="Distributor Manager">
					<? 	
						$distManagerClause = " where adminType ='SUPI Manager' AND  isMain='N' AND agentStatus='Active' order by username";
						$distManagerRS = selectMultiRecords($allUsersSql.$distManagerClause);
					?>
					<? for($i=0; $i<sizeof($distManagerRS); $i++) { ?>
						<option value="<?=$distManagerRS[$i]["userID"]?>"><?=$distManagerRS[$i]["username"]."[".$distManagerRS[$i]["name"]."]" ?></option>
					<? }?>
					</optgroup>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$_REQUEST["userId"]; ?>");
			   </script>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<? } ?>
	
		<tr>
			<td align="left"><strong> Account </strong></td>
			<td align="left" >&nbsp;
				  <?
			   $sql = "SELECT accounNumber,accounType,accountName,currency FROM ".TBL_ACCOUNTS." WHERE status = 'AC' AND  CustID !='' ORDER BY accounNumber";
			   $accountData = mysql_query($sql) or die('invalid query'.mysql_error());
			   
			   ?>
				  <select name="account" id="account">
				    <option value="">--select account--</option>
			        <?
					
					while($row = mysql_fetch_array($accountData))
					{
					//debug ($row);
						$accounNumber = $row["accounNumber"];
						$accountName = $row["accountName"];
						$currency = $row["currency"];
						$userType = $row["userType"];
						echo "<option value='$accounNumber'>".$accounNumber."-->".$accountName."-->".$currency."</option>";
					}
		        ?>
			      </select>				
				  <script language="JavaScript">
					SelectOption(document.forms[0].account, "<?=$_REQUEST["account"]; ?>");
			      </script>
				 
				</td>
<?php
			//$args = array('flag'=>'getTransTypeDropdown');
//			$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
//			if(!empty($returnData['transTypeDropdown']))
//				$transTypeDropdown = $returnData['transTypeDropdown'];
//			if(!empty($transTypeDropdown)){
?>	
				<!--<td>
				<select name="transType" id="transType">
					<?=$transTypeDropdown?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].transType, "<?=$_REQUEST["transType"]; ?>");
			      </script>
				</td>-->
				<?php 
		//	}

			$args = array('flag'=>'getTransIDField','select'=>$transIDV);
			$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
			if(!empty($returnData['transIDHTML']))
				$strHTMLTransID = $returnData['transIDHTML'];
			if(!empty($strHTMLTransID)){
?>	
				<td>
				<?=$strHTMLTransID?>
				</td>
				<?php 
			}

				/*?><td align="lesft">&nbsp;Currency </td>
				<?
			   $sqlCurrency = "SELECT id,currency,accounNumber FROM ".TBL_ACCOUNTS." WHERE status = 'AC' ORDER BY currency";
			   $currencyData = mysql_query($sqlCurrency) or die('invalid query'.mysql_error());
			   ?>
				<td>
				  <select name="currency" id="currency">
				    <option value="">--select currency--</option>
			        <?
					while($rowCurrency = mysql_fetch_array($currencyData))
					{
					//debug ($row);
						$currency = $rowCurrency["currency"];
						echo "<option value='$currency'>".$currency."</option>";
					}
		        ?>
			      </select>				
				  <script language="JavaScript">
					SelectOption(document.forms[0].currency, "<?=$_REQUEST["currency"]; ?>");
			      </script>
				
			  </td>
<?php */?>		</tr>
		
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="4"><input type="submit" name="Submit" value="Submit" id="Submit"></td>
		</tr>
	</table>
	</form>
<?
}
else
{
?>
	<input type="hidden" name="userId" value="<?=$_SESSION["loggedUserData"]["userID"]?>" />
	<input type="hidden" name="Submit" value="Submit" id="submit"/>
<?
}	
?>
<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr><TD width="40%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td width="60%" align="left" class="reportHeader"><?=SYSTEM?> (Ledger Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
				  </span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%" class="columnTitle">Date</td>
					<td width="10%" class="columnTitle">Trans ID</td>
					<?php if($transTypeFlag){?>
						<td width="10%" class="columnTitle">Other Account</td>
					<?php }?>
					<td width="30%" class="columnTitle">Particulars</td>
 					
 					<?php
						$style ='style="display:none"';
						if ($_REQUEST["account"] == '11103')
							$style ='style="display:"';
						else
							$style ='style="display:none"';
 					?> 																 
					 
				 	<td width="20%" class="columnTitle" id="Note" <?=$style?>>Note</td>
					<td width="10%" class="columnTitle" id="GBP" <?=$style?>>GBP</td>
					
					<td width="15%" align="left" class="columnTitle">Dr. </td>
					<td width="15%" align="left" class="columnTitle">Cr. </td>
					<td width="15%" align="right" class="columnTitle">Balance</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<?php if($transTypeFlag){?>
						<td>&nbsp;</td>
					<?php }?>
					<td class="heading">Opening Balance b/f </td>
					<td width="10%" align="right" <?=$style?>>&nbsp;</td>
					<td width="10%" align="right" <?=$style?>>&nbsp;</td>
					<td align="left" class="heading">&nbsp;<?=abs($openingDrBalance)?></td>
					<td align="left" class="heading">&nbsp;<?=$openingCrBalance?></td>
					<td align="right" class="heading">&nbsp;<?=$openingBalance?></td>
				</tr> 
				<?
					$sumCr  = 0;
					$sumDr = 0;
					$closingBalance = 0;
					//debug($accountRs);
					for($i=0;$i < count($accountRs);$i++)
					{
						$strDebtColum = 'amountSell';
						$strCrdtColum = 'amountBuy';
						
						$fltDebt = '-';
						$fltCrdt = '-';
						$refNumberIM = '';
						
						$accountNumberOther = '';
						if(!empty($_REQUEST['account']))
						{ 
							if($_REQUEST['account'] == $accountRs[$i]["crAccount"]){
								$fltCrdt = $accountRs[$i]["crAmount"];
								$accountNumberOther = $accountRs[$i]["drAccount"];
							}
							else{
								$fltDebt = $accountRs[$i]["drAmount"];
								$accountNumberOther = $accountRs[$i]["crAccount"];
							}
						}
						if(!empty($_REQUEST['userId']))
						{ 
							if($username == $accountRs[$i]["crAccount"])
								$fltCrdt = $accountRs[$i]["crAmount"];
							else
								$fltDebt = $accountRs[$i]["drAmount"];
						}
						
						/*if(!empty($_REQUEST['currency'])){ 
							if($_REQUEST['currency'] == $accountRs[$i]["crCurrency"])
								$fltCrdt = $accountRs[$i]["crAmount"];
							else
								$fltDebt = $accountRs[$i]["drAmount"];
						}*/	
						//debug($accountRs[$i]["transID"]);		
						if(!empty($accountRs[$i]["transID"]))
						{
							if(empty($accountRs[$i]["fid"]) && defined('CONFIG_CURRENCY_EXCHANGE_ACCOUNT') && CONFIG_CURRENCY_EXCHANGE_ACCOUNT!='0'){
								$refNumberIM = $accountRs[$i]["transID"];
							}
							else{
								$transSql = selectFrom("select transID,refNumberIM,transAmount,transaction_notes FROM transactions WHERE transID='".$accountRs[$i]["transID"]."'");
								$refNumberIM = $transSql["refNumberIM"];
								$transAmount = $transSql["transAmount"];
								$transaction_notes = $transSql["transaction_notes"];
							}
						// debug ($transSql);
						}	
						$sumCr += $fltCrdt;
						$sumDr += $fltDebt;
						$trimmedTransID = $accountRs[$i]["transID"];
						if(strstr($accountRs[$i]["transID"],'CE-'))
							$trimmedTransID = substr($accountRs[$i]["transID"],3);// CE-
						
						$buysellCurrency = selectFrom("select buysellCurrency,buy_sell FROM ".TBL_CURR_EXCHANGE_ACCOUNT." WHERE id='".$trimmedTransID."'");
						//debug("select buysellCurrency,buy_sell FROM ".TBL_CURR_EXCHANGE_ACCOUNT." WHERE id='".$trimmedTransID."'");
						//debug($buysellCurrency);
						$currencyName = selectFrom("select currencyName FROM currencies WHERE cID ='".$buysellCurrency[0]."'");		
						
						
						
						if(preg_match("/Exchange Rate/i",$accountRs[$i]["description"]))
						{
							$descriptionV = $accountRs[$i]["description"];
						}
						if($accountRs[$i]["status"] == 'C')
							$descriptionV = "[Cancelled]";
						//debug($descriptionV);	
					?>
				<tr>
					<td align="left" class="heading">&nbsp;<?=date("Y-m-d",strtotime($accountRs[$i]["created"]))?></td>
					<td align="left" class="heading">&nbsp;<?=$refNumberIM?></td>
					<!--<td class="heading"><a href="view-ledgers-details.php?id=<?=$accountRs[$i]["id"]?>&userId=<?=$_REQUEST["userId"]?>&df=<?=$_REQUEST["date_from"]?>&dt=<?=$_REQUEST["date_to"]?>"><?=$accountRs[$i]["description"] ?></a></td>-->
					<?php if($transTypeFlag){?>
						<td class="heading">&nbsp;<a href="#" onClick="javascript:window.open('view-ledgers-other.php?id=<?=$accountRs[$i]["id"]; ?>','Other Account', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740,left=200')"><?=$accountNumberOther?></a></td>
					<?php }?>					
					<td class="heading">&nbsp;<?=$descriptionV?></td>
					<?
					$transaction_notes;
					$notes=explode("|",$transaction_notes);
					//debug ($notes["2"]);
					?>
				 	<td align="left" id="Note" class="heading"  <?=$style;?>>&nbsp;<?=$notes["2"]?></td>
					<td align="left" id="GBP" class="heading" <?=$style;?>>&nbsp;<?=number_format($transAmount, 2, ".", ",")?></td>					 				
					<td align="left" class="heading"><?=number_format($fltDebt, 2, ".", ",")?></td>
					<td align="left" class="heading"><?=number_format($fltCrdt, 2, ".", ",")?></td>
					<td align="left">&nbsp;</td>
					<!--	<input type="hidden" name="drbal" value="<? $fltDebt;?>">
						<input type="hidden" name="crbal" value="<? $fltCrdt;?>"> -->
				</tr>
				<? } ?>
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<?php if($transTypeFlag){?>
						<td>&nbsp;</td>
					<?php }?>
					<td class="heading">Closing Balance of the Day </td>
					<td <?=$style;?>>&nbsp;</td>
					<td <?=$style;?>>&nbsp;</td>
					<td align="left">-</td>
					<td align="left">-</td>
					<?
						//$sumDr +=$openingDrBalance;
						//$sumCr +=$openingCrBalance;
				/*		if($sumCr > $sumDr)
							$xBalance = $sumCr - $sumDr;
						else{
							$xBalance = $sumDr -$sumCr ;
						}	*/
						$xBalance = $sumDr -$sumCr ;
						
					?>
					<td align="right" class="netBalance" id="closingFinal">
					
					<?php
					//debug($xBalance." -- ".$sumCr." -- ".$sumDr);
					
					?>
					
					<?=number_format($openingBalance - $xBalance, 2, ".", ",")?></td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;
				<?
				if($_REQUEST["from"] == "chartAccount"){
				?>
				<input type="button" name="Submit3" value="Close" onclick="window.close();">
				<? } ?>
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<tr>
			</table>
			<br />
			</form>
		</td>
	</tr>
	<tr><td align="center"><input type="button" name="Submit2" value=" Print " onclick="print_doc(this)">
	<script type="text/javascript">
function print_doc(ob){
	ob.style.display = 'none';
	window.print();
var yes =confirm(' Do you want to print this page again. ?');		
			if(yes==true){
			print_doc(ob);
			}
			else{
				ob.style.display = 'block';
				}
	
	}
</script>

		</td>
	</tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
