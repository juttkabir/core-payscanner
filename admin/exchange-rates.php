<?
session_start();
include ("../include/config.php");
include ("security.php");
$query = "select * from " . TBL_EXCHANGE_RATES . " order by country ";
$curencies = selectMultiRecords($query);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="images/interface.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<script language="javascript" src="../javascript/functions.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
}
-->
</style></head>

<body>
<? 
$country = ($_GET["agentCountry"] == "" ? "United Kingdom" : $_GET["agentCountry"]);
$query = "select * from " . TBL_EXCHANGE_RATES . " where  country ='$country'";
$contentRate = selectFrom($query);
$countryRate =  $contentRate["primaryExchange"];

?>
<table  border="0" cellpadding="0" cellspacing="0">

<?
for($i=0;$i<count($curencies);$i++)
{
$margin = $curencies[$i]["marginPercentage"];
?>
  <tr>
    <td width="130" height="20" title="<? echo "Rate By: " . $curencies[$i]["sProvider"]?>"><? echo $curencies[$i]["country"]?></td>
    <td width="60" height="20"><? echo $curencies[$i]["currency"]?></td>
    <td width="50" height="20">
	<? 
	if(strtolower($curencies[$i]["country"]) == strtolower("United Kingdom"))
	{
		echo round($curencies[$i]["primaryExchange"] - (($curencies[$i]["primaryExchange"]) * $margin / 100 ) , 4);
	}
	else
	{
		if(strtolower($curencies[$i]["country"]) != strtolower($country))
		{
			echo round ( ($curencies[$i]["primaryExchange"] / $countryRate) - (($curencies[$i]["primaryExchange"] / $countryRate) * $margin) / 100   , 4);			
		}
		else
		{
			echo round(1 - (1*$margin) /  100 , 4);			
		}
	}
	?></td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#CCCCCC" height="1"></td>
  </tr>
  
<?
}
?>
</table>
</body>
</html>
