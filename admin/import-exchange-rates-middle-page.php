<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$flag="false";
$standAlone = array();
$groupedRates = array();
$userNames = array();
$isUserPresent = true;

	if ($_FILES["importFile"]["tmp_name"] != "") {
		$ext = strtolower(strrchr($_FILES["importFile"]["name"], "."));
		if ($ext == ".txt" || $ext == ".xls" || $ext == ".csv") {
			if ($ext == ".xls") {
				//require_once 'cm_xls/Excel/reader.php';
				/*
				$data = new Spreadsheet_Excel_Reader();
				$data -> setOutputEncoding('CP1251');
				$data -> read($_FILES["importFile"]["tmp_name"]);
				*/
				error_reporting(E_ALL ^ E_NOTICE);
				
				/**
				 * Implement the new Excel Reader as found bug in the PHP related to the memory limit
				 * @Ticket #4850
				 */
				require_once 'cm_xls/Excel/excel_reader2.php';
				$data = new Spreadsheet_Excel_Reader($_FILES["importFile"]["tmp_name"]);
				
				//debug($data, true);
				//debug($data->sheets[0]['cells']);
				//debug(count($data->sheets[0]['cells']), true);
				for ($i = 2; $i <= count($data->sheets[0]['cells']); $i++) {
					
				  $data1[1] = trim($data->sheets[0]['cells'][$i][1]); // Dated
					if ($data1[1] != "") {
						$date = explode("/", $data1[1]);
						if (count($date) == 3) {
							$day  = $date[0];
							$mon  = $date[1];
							$year =	$date[2];
							
							$data1[1] = $year . "-" . $mon . "-" . $day;
						}
					} else {
						$data1[1] = date("Y-m-d");	
					}
					$data1[2] = trim($data->sheets[0]['cells'][$i][2]); // Originating Country
					$data1[3] = trim($data->sheets[0]['cells'][$i][3]); // Originating Currency
					$data1[4] = trim($data->sheets[0]['cells'][$i][4]); // Destination Country
					$data1[5] = trim($data->sheets[0]['cells'][$i][5]); // Destination Currency
					$data1[6] = trim($data->sheets[0]['cells'][$i][6]); // Exchange Rate Type
					$data1[7] = trim($data->sheets[0]['cells'][$i][7]); // UserName
					if ($data1[7] != "") 
					{
						if($data1[7] == "bank24hr" && CONFIG_24HR_BANKING == "1")
						{
							
						}
						else
						{
							$qUserID  = "SELECT `userID` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$data1[7]."'";
							$userID   = selectFrom($qUserID);
							$userNames[$userID["userID"]] = $data1[7];
							$data1[7] = $userID["userID"]; // userID
						}
					}
					$data1[8] = trim($data->sheets[0]['cells'][$i][8]); // Basic Exchange Rate
					$data1[9] = trim($data->sheets[0]['cells'][$i][9]); // Margin Percentage
					$data1[10] = trim($data->sheets[0]['cells'][$i][10]); // Agent Margin
					$data1[11] = trim($data->sheets[0]['cells'][$i][11]); // Agent Margin Type; percent or fixed
					$data1[12] = trim($data->sheets[0]['cells'][$i][12]); // Group Id
					
					$data1[13] = trim($data->sheets[0]['cells'][$i][13]); // 24hrMarginType
					$data1[14] = trim($data->sheets[0]['cells'][$i][14]); // 24hrMarginValue
					
					// Setting default values
					//$data1[11] = (empty($data1[11]))?"percent":$data1[11];
					
					if(empty($data1[12])){
						$standAlone["date"][] = $data1[1]; 
						$standAlone["orgCountry"][] = $data1[2];
						$standAlone["orgCurrency"][] = $data1[3];
						$standAlone["destCountry"][] = $data1[4];
						$standAlone["destCurrency"][] = $data1[5];
						$standAlone["exrateType"][] = $data1[6];
						$standAlone["userID"][] = $data1[7];
						$standAlone["exrate"][] = $data1[8];
						$standAlone["margin"][] = $data1[9];
						$standAlone["agentMargin"][] = $data1[10];
						$standAlone["agentMarginType"][] = $data1[11];
						
						$standAlone["24hrMarginType"][] = ((empty($data1[13]) && $data1[7] == "bank24hr")?"lower":$data1[13]);
						$standAlone["24hrMarginValue"][] = (empty($data1[14])?"0":$data1[14]);
					}
					else
					{
						$groupedRates[$data1[12]]["date"][] = $data1[1];
						$groupedRates[$data1[12]]["orgCountry"][] = $data1[2];
						$groupedRates[$data1[12]]["orgCurrency"][] = $data1[3];
						$groupedRates[$data1[12]]["destCountry"][] = $data1[4];
						$groupedRates[$data1[12]]["destCurrency"][] = $data1[5];
						$groupedRates[$data1[12]]["exrateType"][] = $data1[6];
						$groupedRates[$data1[12]]["userID"][] = $data1[7];		
						$groupedRates[$data1[12]]["exrate"][] = $data1[8];
						$groupedRates[$data1[12]]["margin"][] = $data1[9];
						$groupedRates[$data1[12]]["agentMargin"][] = $data1[10];
						$groupedRates[$data1[12]]["agentMarginType"][] = $data1[11];
					}	
				}
				//debug($groupedRates);
				
				$_SESSION["standAlone"] = $standAlone;
				$_SESSION["groupedRates"] = $groupedRates;
				//debug($_SESSION["standAlone"], true);
			} /*else {
				$filename = $_FILES["importFile"]["tmp_name"];
				$fd = fopen ($filename, "r");
				$contents = fread ($fd, filesize ($filename));
				fclose ($fd);
				$allRows =  explode("\n", $contents);
				$fieldNames = explode("|", $allRows[0]);
				for ($i = 1 ; $i < count($allRows) ; $i++) {
					$data = explode("|", $allRows[$i]);
					
					$data1[1] = trim($data[0]);	// Dated
					if ($data1[1] != "") {
						$date = explode("/", $data1[1]);
						if (count($date) == 3) {
							$day  = $date[0];
							$mon  = $date[1];
							$year =	$date[2];
							
							$data1[1] = $year . "-" . $mon . "-" . $day;
						}
					} else {
						$data1[1] = date("Y-m-d");	
					}
					$data1[2] = trim($data[1]);	// Originating Country
					$data1[3] = trim($data[2]); // Originating Currency
					$data1[4] = trim($data[3]); // Destination Country
					$data1[5]	= trim($data[4]); // Destination Currency
					$data1[6] = trim($data[5]); // Exchange Rate Type
					$data1[7] = trim($data[6]); // UserName
					if ($data1[7] != "") {
						$qUserID  = "SELECT `userID` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$data1[7]."'";
						$userID   = selectFrom($qUserID);
						$data1[7] = $userID["userID"]; // userID
					}
					$data1[8] = trim($data[7]); // Basic Exchange Rate
					$data1[9] = trim($data[8]); // Margin Percentage
					
					
				}
			}*/
		} else {
			$msg = CAUTION_MARK . " File format must be .txt, .xls or .csv";
		}
	} else {
			$msg = CAUTION_MARK . " Please first browse for file.";
	}	
	
?>
<html>
<head>
	<title>View Exchange Rates To Import</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/datetimepicker.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">

<script language="javascript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="800" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">View Exchange Rates To Import</font></strong></td>
  </tr>
</table>
<form action="import-exchange-rates.php" method="post" name="trans">
	<input type="hidden" name="command" value="IMPORT" />
<table width="800" border="1" cellpadding="0" bordercolor="#666666">
	<!--tr>
  	<td  bgcolor="#000000" >
			<table  width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		    <tr>
		      <td>
		        <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
		        Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
		        of
		        <?=$allCount; ?>
		        <?php } ;?>
		      </td>
		      <?php if ($prv >= 0) { ?>
		      <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType";?>"><font color="#005b90">First</font></a>
		      </td>
		      <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType";?>"><font color="#005b90">Previous</font></a>
		      </td>
		      <?php } ?>
		      <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
					$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
		      <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType";?>"><font color="#005b90">Next</font></a>&nbsp;
		      </td>
		      <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType";?>"><font color="#005b90">Last</font></a>&nbsp;
		      </td>
		      <?php } ?>
		    </tr>
		  </table>
		</td>
	</tr-->

 	<!--<tr>
  	<td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
    are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
 	</tr>-->
	
	<tr>
  	<td nowrap bgcolor="#EFEFEF">
  		<table width="100%" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					<td><span class="style1">User</td>
					<td><span class="style1">Originating Country</span></td>
					<td><span class="style1">Originating Currency</span></td>
					<td><span class="style1">Destination Country</span></td>
					<td><span class="style1">Destination Currency</span></td>
					<td><span class="style1">Exchange Rate</span></td>
					<td><span class="style1">Margin</span></td>
					<td><span class="style1">Agent Margin (type)</span></td>
					<td><span class="style1">Exchange Rate Type</span></td>
			 	</tr>
			 	
			 	<?
			 	/**
			 	 * Displaying grouped rates first.
			 	 */
			 	?>
			 	<tr>
			 		<td colspan="8">Intermediate Exchange Rates (Grouped)</td>
				</tr>
				<?
			 	foreach($groupedRates  as $key => $group){
			 			for($i=0; $i<sizeof($group["orgCurrency"]); $i++)
			 			{
							if(strtolower($group["exrateType"][$i]) != strtolower("generic") && empty($group["userID"][$i]) && strtolower($group["exrateType"][$i]) != strtolower("bank24hr"))
							{
								$usrBgColor = "#FF0000";
								$isUserPresent = false;
							} else {
								$usrBgColor = "#FFFFFF";
							}
			 	?>
					 	<tr bgcolor="#FFFFFF">
						  <td width="75" bgcolor="<?=$usrBgColor?>"><?=$userNames[$group["userID"][$i]]?></td>
						  <td width="75" bgcolor="#FFFFFF"><?=$group["orgCountry"][$i]?></td>
				          <td width="75" bgcolor="#FFFFFF" ><?=$group["orgCurrency"][$i]?></td>
				          <td width="75" bgcolor="#FFFFFF" ><?=$group["destCountry"][$i]?></td>
		        		  <td width="75" bgcolor="#FFFFFF" ><?=$group["destCurrency"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF"><?=$group["exrate"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=$group["margin"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=(!empty($group["agentMargin"][$i]))?$group["agentMargin"][$i]."(".$group["agentMarginType"][$i].")":""?></td>
						  <td width="100" bgcolor="#FFFFFF"><?=$group["exrateType"][$i]?></td>
						</tr>
				<?
						}
				}
				
				/**
				 * Displaying stand alone rates.
				 */
				?>
				<tr>
			 		<td colspan="8">Standalone Exchange Rates</td>
				</tr>
				<?
				//debug($standAlone);
				for($i=0; $i<sizeof($standAlone["orgCurrency"]); $i++)
			 			{
							if(strtolower($standAlone["exrateType"][$i]) != strtolower("generic") && empty($standAlone["userID"][$i])  && strtolower($standAlone["exrateType"][$i]) != strtolower("bank24hr"))
							{
								$usrBgColor = "#FF0000";
								$isUserPresent = false;
							} else {
								$usrBgColor = "#FFFFFF";
							}
			 	?>
					 	<tr bgcolor="#FFFFFF">
						  <td width="75" bgcolor="<?=$usrBgColor?>"><?=$standAlone["userID"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF"><?=$standAlone["orgCountry"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=$standAlone["orgCurrency"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=$standAlone["destCountry"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=$standAlone["destCurrency"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF"><?=$standAlone["exrate"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=$standAlone["margin"][$i]?></td>
						  <td width="75" bgcolor="#FFFFFF" ><?=(!empty($standAlone["agentMargin"][$i]))?$standAlone["agentMargin"][$i]."(".$standAlone["agentMarginType"][$i].")":""?></td>
						  <td width="100" bgcolor="#FFFFFF"><?=$standAlone["exrateType"][$i]?></td>
						</tr>
				<?
						}
				?>
			</table>
		</td>
	</tr>
</table>
<br /><br />
<?
	if($isUserPresent)
	{
?>
		<center><input type="submit" value="Import" /></center>
<?
	} else {
		echo "<center>";
		echo "<font color='#ff0000'><b>";
		echo "You must provide User Name for the records highlighted with Red Colour.<br />";
		echo "</b></font>";
		echo "<a href=\"import-exchange-rates.php\" style=\"color:#0000FF\">Click here to go back to Import the file again.</a>";
		echo "</center>";
	}
?>
</form>
</body>
</html>