<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
//die("Unable to connect database error!");  
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];
global $nTotal;

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}

$nxt = $offset + $limit;
$prv = $offset - $limit;


if($_POST["Submit"] == 'Search' || $_GET[Submit] == 'Search')
{
	if($_POST["transRef"] != '')
		$transRef = $_POST["transRef"];
	elseif($_GET["transRef"] != '')
		$transRef = $_GET["transRef"];
	
	if($_POST["txtAgent"] != '')////For agent Name Selection  txtFilter
		$txtAgent = $_POST["txtAgent"];
	elseif($_GET["txtAgent"] != '')////For agent Name Selection  txtFilter
		$txtAgent = $_GET["txtAgent"];
	
	if($_POST["transStatus"] != '')
		$transStatus = $_POST["transStatus"];
	elseif($_GET["transStatus"] != '')
		$transStatus = $_GET["transStatus"];
	
	/*if($_POST["transDate"] != '')///transID
		$transDate = $_POST["transDate"];
	elseif($_GET["transDate"] != '')///transID
		$transDate = $_GET["transDate"];
	
	if($_POST["transDate2"] != '')
		$transDate2 = $_POST["transDate2"];
	elseif($_GET["transDate2"] != '')
		$transDate2 = $_GET["transDate2"];*/
		
	if($_POST["Submit"] != '')
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"] != '')
		$Submit = $_GET["Submit"];
	
}

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( a.userID = '".$agentID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as a ON a.userID = c.parentID  ";
	$acManagerFlag = true;
}

	if($Submit == "Search")
	{
			if($transStatus !="" || $transRef != '')
			{
				
					$query = "select * from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
					$queryCnt = "select count(*) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
			}

		
		if($txtAgent != '' && $transRef == '')
		{
			$query = "select * from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where t.addedBy = a.username ".$extraCondition;
			$queryCnt = "select count(*) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where t.addedBy = a.username ".$extraCondition;
			
			$query .= " and (a.username like '".$txtAgent."%')";
			$queryCnt .= " and (a.username like '".$txtAgent."%')";
		
		/*	
		$allCount = countRecords($queryCnt);	  
		$query .= " order by t.transDate DESC";    
		$query .= " LIMIT $offset , $limit";    
		
		$contentsTrans = selectMultiRecords($query);
	*/
		}

		if($transRef != '')
		{
			$query .= " and (t.refNumber like '".$transRef."' or t.refNumberIM like '".$transRef."')";
			$queryCnt .= " and (t.refNumber like '".$transRef."' or t.refNumberIM like '".$transRef."')";
			
		}
		/*if($transDate != '' && $transDate2 != '')
		{
			$dDate = explode("/",$transDate);
			if(count($dDate) == 3)
				$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
			else
				$dDate = $transateD;

			$dDate2 = explode("/",$transDate2);
			if(count($dDate2) == 3)
				$dDate2 = $dDate2[2]."-".$dDate2[1]."-".$dDate2[0];
			else
				$dDate2 = $transDate2;

			$query .= " and t.transDate between '$dDate 00:00:00' and '$dDate2 23:59:59'";
			$queryCnt .= " and t.transDate between '$dDate 00:00:00' and '$dDate2 23:59:59'";
			$transDate = $dDate;
			$transDate2 = $dDate2;
		}*/
		if($transStatus !="" && $transRef == '')
		{
			$query .= " and t.transStatus like '".$transStatus."'";
			$queryCnt .= " and t.transStatus like '".$transStatus."'";
		} 
		$allCount = countRecords($queryCnt);
			if(($transStatus !=""  || $txtAgent != '')&& $transRef == '')
			{
					  
				$query .= " order by t.transDate DESC";    
				$query .= " LIMIT $offset , $limit";    
			}

	if(!empty($query))
		$contentsTrans = selectMultiRecords($query);
			
}
//debug($query);
?>
<html>
<head>
	<title>Transactions Audit Logs</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
} 
function checkForm(theForm)
{
	
	if((theForm.txtAgent.value == "" || IsAllSpaces(theForm.txtAgent.value))&&(theForm.transRef.value == "" || IsAllSpaces(theForm.transRef.value))){
    	alert("Please provide Proper Filters.");
      theForm.txtAgent.focus();
        return false;
    }	
    return true;
}

function CheckAll()
{
	

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
  <tr>
    <td class="topbar"> View Transactions Audit</td>
  </tr>

  <tr>
    <td align="left"><br>
      <table border="1" cellpadding="5" bordercolor="#6699CC" align="center">
	  <form action="Transaction_Audit.php" method="post" name="Search" onSubmit="return checkForm(this);">
      <tr>
          <td nowrap bgcolor="#6699CC"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
        <td nowrap>
		  <!--From Date
		    		    					<input name="transDate" type="text" id="transDate"  value="<?=$transDate?>" readonly>&nbsp;<a href="javascript:show_calendar('Search.transDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		   &nbsp;To Date&nbsp;<input name="transDate2" type="text" id="transDate2"  value="<?=$transDate2?>" readonly>&nbsp;<a href="javascript:show_calendar('Search.transDate2');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		  <br> -->
		  Created By
		  <input name="txtAgent" type="text" value="<?=$txtAgent?>">
		  Transaction Number
		  <input name="transRef" type="text" value="<?=$transRef?>">
		  <br>
		Transaction Status <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">

		  <option value=""> - Status - </option>

		  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		   <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Hold">Holded</option>
		  <option value="unHold">Un Holded</option>
		  <option value="Failed">Failed</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus?>");</script>
            <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>

<?

if(count($contentsTrans) > 0)
{
//if ($allCount > 0){
?>

          <tr>
            <td colspan="4" bgcolor="#ffffff" align="left">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="150">
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  <td width="900">&nbsp;</td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 <td>&nbsp;</td>
                  <?php } ?>
                </tr>
              </table>
			  </td>

          </tr>

      <table width="700" border="1" cellpadding="0" bordercolor="#6699CC">
        <form action="Transaction_Audit.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
	<tr>
	  <td width="700" height="25" nowrap bgcolor="#6699CC"><span class="tab-u"><strong>&nbsp;Records to View</span></td>
	</tr>
	<tr>
          <td nowrap bgcolor="#EFEFEF">
          	<table width="700" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF">
                  <td><span class="style1"><b>Creation Date</span></td>
                  <td><span class="style1"><b> <?=$systemCode?> </span></td>
                  <td><span class="style1"><b><?=$manualCode?></span></td>
                  <td><span class="style1"><b>Inputted By</span></td>
                	<?
                /*	if($transStatus != '' && $transStatus != 'Pending')
                	{
                	?>
	                  <td width="100"><span class="style1"><b><?=$transStatus?> By </span></td>
	                  <td width="80"><span class="style1"><b><?=$transStatus?> Date</span></td>
	                  <td width="268" align="center"><span class="style1"><b>Accessed IP</span></td>
	                <?
	                }*/
	                ?>
                 
                </tr>
		<?
		}

      for($i=0;$i < count($contentsTrans);$i++)
			{
				
		?>

			

		  <!-- *************************************  -->
        

                <tr bgcolor="#FFFFFF">
                  <td bgcolor="#FFFFFF">&nbsp;<? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></td>
                  <td bgcolor="#FFFFFF">&nbsp;<? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td bgcolor="#FFFFFF">&nbsp;<? echo $contentsTrans[$i]["refNumber"]?></td>
                  <td bgcolor="#FFFFFF">&nbsp;<?  echo $contentsTrans[$i]["addedBy"]?></td>
                </tr>
                  <?
                  if($contentsTrans["transStatus"] != 'Pending')
                	{
                				//echo "<br>select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Verified'";
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Verified'");
                				if($activity["login_history_id"] != "")
                				{
                					           					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
                					
                					?>
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Verified By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Verified Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
			       								<td bgcolor="#FFFFFF"><? echo dateFormat($statusDated,"2"); ?></td>
			       								<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
			       								<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
		       								</tr>
                					<?
                				}
                				//echo "<br>select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Rejected'";
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Rejected'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
                					?>
                					
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Rejected By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Rejected Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
					       				</tr>
                					<?
                				}
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Suspended'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
                					
                				?>
                				
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Suspended By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Suspended Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
		                				<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
                				<?
                				}
                				
                					//echo "<br>select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction  is updated%'";
                					$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction  is updated%'");
                				if($activity["login_history_id"] != "")
                				{
                				
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
                					
                				?>
                				
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Updated By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Updation Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
		                				<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
                				<?
                				}
                				
                				//echo "<br>select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction is Cancelled%'";
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction is Cancelled%'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}
                					?>
                					
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Cancelled By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Cancellation Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
                					<?
                				}
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Awaiting Cancellation'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
										  		
										  		?>
										  		
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Awaiting Cancellation By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Awaiting Cancellation Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  		<?
                				}	
                				
                				//echo "<br>select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction is Authorized%'";
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like '%Transaction is Authorized%'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
										  		
										  		?>
										  		
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Authorized By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Authorized Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  		<?
                				}
                				  if(CONFIG_RETURN_CANCEL == "1"){ 
                						$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Cancelled - Returned'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
                					?>
                					
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Cancelled - Returned By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Cancelled - Returned Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
					       				</tr>
                					<?
                				}
                			}
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Holded'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
										  		
										  		?>
										  		
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Holded By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Holded Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  		<?
                				}
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Unholded'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
										  		$changedBy =  ucfirst($agentContent["username"]);
										  		
										  	?>
										  	
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Unholded By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Unholded Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  	<?
                				}
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Failed'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                				if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}
										  	?>
										  	
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Failed By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Failed Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  	<?
                				}
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Delivered'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}
										  	?>
										  	
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Delivered By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Delivered Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  	<?
                				}
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Out for Delivery'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}
										  	?>
										  	
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Out for Delivery By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Out for Delivery Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
										  	<?
                				}
                				
               					$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Picked up'");

                				if($activity["login_history_id"] != "")
                				{
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}
										  			?>
										  			
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Picked up By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Picked up Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
                				<?
                				}
                				
                				$activity = selectFrom("select login_history_id, activity_time, access_ip, login_type, user_id from " . TBL_LOGIN_HISTORY . " as h, ".TBL_LOGIN_ACTIVITIES." as a where h.history_id = a.login_history_id and a.action_for = '".$contentsTrans[$i]["transID"]."' and a.table_name like '".TBL_TRANSACTIONS."' and a.description like 'Transaction is Credited'");
                				if($activity["login_history_id"] != "")
                				{
                					
                					
                					
                					$statusDated = $activity["activity_time"];
                					$accessedIP = $activity["access_ip"];
                					if($activity["login_type"] != 'TELLER')
                					{
	                					$agentContent = selectFrom("select name, username, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["username"]);
										  		}else{
										  			$agentContent = selectFrom("select name, loginName from " . TBL_TELLER . " where tellerID='".$activity["user_id"]."'");
											  		$changedBy =  ucfirst($agentContent["loginName"]);
										  			}?>
										  			
                					<tr>
                						<td bgcolor="#FFFFFF"><span class="style1"><b>Credited By </span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Credited Date</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1"><b>Accessed IP</span></td>
	                  				<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>		
                					</tr>
                					<tr>
	                					<td bgcolor="#FFFFFF"><?=$changedBy?></td>
						       					<td bgcolor="#FFFFFF"><?  echo dateFormat($statusDated,"2");?></td>
						       					<td bgcolor="#FFFFFF"><?=$accessedIP?></td>
						       					<td bgcolor="#FFFFFF"><span class="style1">&nbsp;</span></td>	
						       				</tr>
                				<? }	
                		
                		
	                }
	                ?>
	                <tr>
	                	<td>&nbsp;</td>
	                </tr>
	                <tr>
	                	<td>&nbsp;</td>
	                </tr>
	                <?
			}
/*
if ($allCount > 0){
?>
           <tr>
            <td bgcolor="#ffffff"> <table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="150">
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  <td >&nbsp;</td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transRef=$transRef&txtAgent=$txtAgent&transStatus=$transStatus&transDate=$transDate&transDate2=$transDate2&Submit=$Submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>

				  <td >&nbsp;</td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>

<?
//}

}*/
?>
 </table>
 
 </td>
        </tr>
		</form>
    </table></td>
  </tr>
<? if ($allCount == 0){
?>
<tr align="center">
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">&nbsp;&nbsp;&nbsp;<h4>No data to view. Select Proper Filters to view Transactions. </h4></font>
				</td>
			</tr>
<?                
}
?>
</table>
</body>
</html>