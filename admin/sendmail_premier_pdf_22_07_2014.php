<?php
/** 
 * short description here
 * This page is used for generating pdf at run time.
 * This Page attach pdf with the email that sent to given address.
 * This page also sent the email to the given Email Address
 * @package Transaction Module
 * @subpackage Sending pdf attachment
 * @author Mirza Arslan Baig
 * @copyright HBS Tech. (Pvt) Ltd.
 */ 
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$nowTime = date("F j, Y");
	$transID = $_REQUEST["transid"];
	//debug($transID);
	$today = date("d-m-Y");
	$todayD= date("Y-m-d");
	$emailFlag = false;
	
	$mailto = $_REQUEST["emailto"];
	//debug ($_REQUEST, true);
	 /*
	 * 7897 - Premier FX
	 * Rename 'Create Transaction' text in the system.
	 * Enable  : string
	 * Disable : "0"
	*/
	
	$strTransLabel = 'Transaction';
	
	if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
		$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
td.myFormat
{
font-size:10px;	
}

td.Arial
{
font-size:10px;
font-family:"Gill Sans MT",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
background:#C0C0C0;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
background:#C0C0C0;
}
td.bottom
{
border-bottom: solid 1px #000000;  
font-size:10px;
}
td.bottomborder{
border-bottom: solid 1px #000000;
}
td.leftborder{
border-left: solid 1px #000000;
}

td.sign
{
font-size:10px;
}
td.style2
{
font-size:10px;
}
td.terms
{
font-size:12px;
font-family: Gill Sans MT;
}
</style>
</head>
</html>
<?php
$message="";
$nTransID = $_REQUEST['transid'];	


$nTransID=explode(",",$nTransID);


if(is_array($nTransID) && $nTransID!="")
{
	$countTrans = count($nTransID);
}
else
{
	$countTrans = 1;
}
//debug($nTransID);
for($t=0;$t<$countTrans;$t++)
{


	$strTransactions = "SELECT 
							tr.transID as transID,
							tr.custAgentID as tr_custAgentID,
							tr.benID as tr_benID,
							tr.customerID as tr_customerID,
							tr.addedBy as tr_addedBy,
							tr.currencyFrom as tr_currencyFrom,
							tr.transDate as tr_transDate,
							tr.valueDate as tr_valueDate,
							tr.localAmount as tr_localAmount,
							tr.currencyTo as tr_currencyTo,
							tr.transAmount as tr_transAmount,
							tr.exchangeRate as tr_exchangeRate,
							tr.refNumber as tr_refNumber,
							tr.tip as tr_tip,
							tr.transDetails as trans_details,
							
							cust.customerID as cust_customerID,
							cust.email as cust_email,
							cust.parentID as cust_accManagerID,
							cust.firstName as cust_firstName,
							cust.lastName as cust_lastName,
							cust.accountName as cust_accountName,
							
							ag.email as ag_email,
							ag.name as ag_name,
							
							accM.email as accM_email,
							accM.name as accM_name,
							
							bd.bankName as bd_bankName,
							bd.accNo as bd_accNo,
							bd.IBAN as bd_IBAN,
							bd.sortCode as bd_sortCode,
							bd.routingNumber as bd_routingNumber,
							bd.branchAddress as bd_branchAddress,
							bd.swiftCode as bd_swiftCode,
							bd.branchCode as bd_branchCode							
							
						FROM
							".TBL_TRANSACTIONS." as tr 
							LEFT JOIN ".TBL_CUSTOMER." as cust ON tr.customerID = cust.customerID  
							LEFT JOIN ".TBL_ADMIN_USERS." as ag ON tr.custAgentID = ag.userID 
							LEFT JOIN ".TBL_ADMIN_USERS." as accM ON cust.parentID = accM.userID 
							
							LEFT JOIN ".TBL_BANK_DETAILS." as bd ON tr.transID = bd.transID 
						WHERE 
							tr.transID = '".$nTransID[$t]."'"; 
						 //AND tr.benID != ''";
	$transContent = selectFrom($strTransactions);	
	//debug($strTransactions, true);
	$transactionID = $transContent['transID'];
	//debug($transID, true);
	$agentEmail = $transContent["ag_email"];

	$emailFlag = true;

	$customerID=$transContent['tr_customerID'];
	$custEmail = $transContent["cust_email"];

	if(!empty($mailto))
		$custEmail = $mailto;
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		$agentEmail = $accMgrEmail = $transContent["accM_email"];	
		$emailFlag = true;
	}			
	
	// accounts details against speficif sending currency
	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$transContent['tr_currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
	$trAccountrTrading = '';
	for($i=0;$i<count($fieldsCheckedArr);$i++)
	{
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr))
		{
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else
		{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		
		//$trAccountrTrading[] = array(array('name'=>strtoupper($fieldsLabel),'type'=>$accountRS[$fieldsValue]));
		$trAccountrTrading .= '<tr style="background-color:#fff;">
					<td>'.strtoupper($fieldsLabel).':</td>
					<td>'.$accountRS[$fieldsValue].'</td></tr>';
	}
  	//$bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$nTransID[$t]."'");	
  
	$tipV = '';
	
	if(CONFIG_HIDE_TIP_FIELD != "1"){
		$tipV = $transContent["tr_tip"];
	}	
	else{
		//$tipV = "BARC GB 22";
		$tipV = "";
	}
	
	$termsConditionStr = '';
	if(CONFIG_TRANS_CONDITION_ENABLED == '1')
	{
		if(defined("CONFIG_TRANS_COND"))
		{
			$termsConditionStr =  (CONFIG_TRANS_COND);
		}
		else
		{
			$termsConditionSql = selectFrom("SELECT company_terms_conditions FROM company_detail
													WHERE company_terms_conditions!='' 
													AND dated = (SELECT MAX(dated) FROM company_detail WHERE company_terms_conditions!='')");
			if(!empty($termsConditionSql["company_terms_conditions"]))
				$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
			//eval("$tremsConditions");
			eval("\$tremsConditions = \"$tremsConditions\";");
			$termsConditionStr =  stripslashes($tremsConditions);
		}
		if (defined('CONFIG_CONDITIONS')) { 
			$termsConditionStr = '';
		}  
	}else{
		$termsConditionStr = "I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.";
	//debug($termsConditionStr);
	} 
		  
}

/**
 * #9984 : Premier Exchange: CUSTOMER ADDING BENEFICIARY INSTRUCTIONS
 * short description
 * These @vars are changed for the pdf library
 */ 
	
	$benID = $transContent["tr_benID"];
	$bdBankName = $transContent["bd_bankName"];
	$bdBranchAddress = $transContent["bd_branchAddress"];
	$bdSwiftCode = $transContent["bd_swiftCode"];
	$bdIbanNum = $transContent["bd_IBAN"];
	$bdAccNum = $transContent["bd_accNo"];
	$bdSortCode = trim($transContent["bd_sortCode"]);
	$bdBranchCode = trim($transContent["bd_branchCode"]);
	$bdRoutingNumber = $transContent["bd_routingNumber"];
	
 
$clientData = '
	<table border="1" style="padding:2px;" width="500" >
		<tr>
			<td>CLIENT NAME:</td>
			<td>'.$transContent['cust_firstName']. " " . $transContent['cust_lastName'].'</td>
		</tr>
		<tr style="background-color:#ccc;" >
			<td>CLIENT NO.</td>
			<td>'.$transContent['cust_accountName'].'</td>
		</tr>
		<tr>
			<td>PFX DEALER</td>
			<td>'.$transContent['accM_name'].'</td>
		</tr>
	</table>';
	
$transactionData = '
		<table border="1" style="padding:2px;" width="500" >
			<tr>
				<td>TRADE DATE:</td>
				<td>'.dateFormat($transContent["tr_transDate"],2).'</td>
			</tr>
			<tr style="background-color:#ccc;" >
				<td>MATURITY DATE:</td>
				<td>'.dateFormat($transContent["tr_valueDate"],2).'</td>
			</tr>
			<tr>
				<td>YOU BUY:</td>
				<td>'.$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"].'</td>
			</tr>
			<tr style="background-color:#ccc;" >
				<td>AT A RATE OF:</td>
				<td>'.$transContent["tr_exchangeRate"].'</td>
			</tr>
			<tr>
				<td>YOU SELL:</td>
				<td>'.number_format($transContent["tr_transAmount"],2).' '.$transContent["tr_currencyFrom"].'</td>
			</tr>
			<tr style="background-color:#ccc;" >
				<td>TT Fee:</td>
				<td>ZERO</td>
			</tr>
			<tr>
				<td>REFERENCE:</td>
				<td>'.$transContent['tr_refNumber'].'</td>
			</tr>
		</table>';
	
if(!empty($transContent['tr_currencyFrom'])){
		$paymentsData = '
			<table border="1" style="padding:2px" width="500">
				<tr>
					<td>ACCOUNT NAME:</td>
					<td>'.$accountRS["accountName"].'</td>
				</tr>
				<tr style="background-color:#ccc;"  >
					<td>ACCOUNT NUMBER:</td>
					<td>'.$accountRS["accounNumber"].'</td>
				</tr>
				<tr>
					<td>BANK</td>
					<td>'.$accountRS["bankName"].'</td>
				</tr>
				<tr style="background-color:#ccc;" >
					<td>IBAN:</td>
					<td>'.$accountRS["IBAN"].'</td>
				</tr>
				<tr>
					<td>SORT CODE:</td>
					<td>'.$accountRS["sortCode"].'</td>
				</tr>
				<tr style="background-color:#ccc;">
					<td>Swift Code:</td>
					<td>'.$accountRS["swiftCode"].'</td>
				</tr>
		';
		$paymentsData .= $trAccountrTrading;
		$paymentsData .= '</table>';
}
$signatureData = '
	<table border="1" width="500" style="padding:2px">
		<tr>
			<td>NAME (Print):</td>
			<td>&nbsp;</td>
		</tr>
		<tr style="background-color:#ccc;" >
			<td>DATED:</td>
			<td>'.date("d/m/y",strtotime($transContent["tr_transDate"])).'</td>
		</tr>
		<tr>
			<td>PREMIER FX SIGN OFF:</td>
			<td>&nbsp;</td>
		</tr>
	</table>
';
/* debug($transContent["bd_bankName"]);
debug($transContent["bd_accNo"]);
debug($transContent["tr_benID"]); */

//if(!(empty($transContent["bd_bankName"]) && empty($transContent['bd_accNo']) && (($transContent['tr_benID'] != '0') || empty($transContent['tr_benID'])))){


/*
$accountsData = '
	<table border="1" width="100%" style="padding:2px;">
		<tr>
			<td>CLIENT NUMBER:</td>
			<td>'.$transContent["cust_accountName"].'</td>
		</tr>
		<tr  >
			<td>AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
			<td>'.$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"].'</td>
		</tr>
	</table>
	';
*/
//if(!(empty($transContent["bd_bankName"]) && empty($transContent['bd_accNo']) && (($transContent['tr_benID'] == '0') || !empty($transContent['tr_benID'])))){
	
	/**
	 * #10232: Premier Exchange: Module to set Country based rules on Bank Transfer 
	 * Short Description
	 * Generation of text Data according to the provided data
	 */
/*	
if(($transContent['tr_benID'] != '0') || !empty($transContent['tr_benID'])){
	$queryBenDetails = "SELECT firstName, middleName, lastName, Country FROM ".TBL_BENEFICIARY." WHERE benID = '{$transContent['tr_benID']}' LIMIT 1";
	$benDetails = selectFrom($queryBenDetails);
	$benData = '
		<table border="1" width="100%" style="padding:2px;">	
			<tr>
				<td>BENEFICIARY\'S FIRST NAME</td>
				<td>'.$benDetails["firstName"].'</td>
			</tr>
			<tr>
				<td>BENEFICIARY\'S MIDDLE NAME</td>
				<td>'.$benDetails["middleName"].'</td>
			</tr>
			<tr>
				<td>BENEFICIARY\'S LAST NAME</td>
				<td>'.$benDetails["lastName"].'</td>
			</tr>
			<tr>
				<td>BENEFICIARY\' COUNTRY</td>
				<td>'.$benDetails["Country"].'</td>
			</tr>
		</table>
	';
	
}
*/
$confirmedData = '
	<table width="100%" cellpadding="4" style="padding:2px;">
		<tr>
			<td >Your Name:</td>
			<td>&nbsp;</td>
		</tr>
		<tr style="background-color:#ccc;height:100px">
			<td>Your Contact Number(s):</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Your Signature:</td>
			<td>&nbsp;</td>
		</tr>
		<tr  >
			<td>DATED: DD/MM/YY</td>
			<td>'.date("d/m/y",time()).'</td>
		</tr>
	</table>
';
$companyAddress = '
	<table align="left" width="150" style="padding:2px;" cellpadding="0" cellspacing="0">
		<tr>
			<th align="left"><b>Premier FX</b></th>
		</tr>
		<tr  >
			<td align="left"><b>UK:</b> 55 Old Broad Street,</td>
		</tr>
		<tr>
			<td align="left">London, EC2M 1RF.</td>
		</tr>
		<tr>
			<td align="left">Tel: +44 (0)845 021 2370<br /></td>
		</tr>
		<tr>
			<td align="left"><b>Portugal:</b> Rua Sacadura Cabral,</td>
		</tr>
		<tr >
			<td align="left">Edificio Golfe, lA,</td>
		</tr>
		<tr>
			<td align="left">8135-144 Almancil, Algarve.</td>
		</tr>
		<tr >
			<td align="left">Tel: +351 289 358 511<br /></td>
		</tr>
		<tr>
			<td align="left"><b>Spain:</b>  La Rambla 13, 07003,</td>
		</tr>
		<tr >
			<td align="left">Palma de Mallorca</td>
		</tr>
		<tr>
			<td align="left">Tel: +34 971 576 724</td>
		</tr>
		<tr >
			<td align="left">Fax : +351 289 358 513</td>
		</tr>
		<tr>
			<td align="left">Email : info@premfx.com</td>
		</tr>
		<tr >
			<td align="left">Web : www.premfx.com</td>
		</tr>
	</table>
	';
/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/
	
	/**
	 * #9984: Premier Exchange: CUSTOMER ADDING BENEFICIARY INSTRUCTIONS
	 * short description
	 * This code is changed for thr new pdf library
	 */
	require_once('lib/tcpdf/config/lang/eng.php');
	require_once('lib/tcpdf/tcpdf.php');
	
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator('Premier FX');
	
	$pdf->SetAuthor('Premier FX');
	$pdf->SetTitle('Premier Exchange Deal Contract');
	$pdf->SetSubject('Deal Contract');
	
	$pdf->setPrintHeader(false);
	
		

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 8);

	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	//set some language-dependent strings
	$pdf->setLanguageArray($l);


	//$pdf->setFormDefaultProp(array('lineWidth'=>0, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));


	// add a page
	$pdf->AddPage();

	// set JPEG quality
	$pdf->setJPEGQuality(75);


	$x = 6;
	$y = 10;
	$w = 0;
	$h = 0;


	$pdf->Image('lib/tcpdf/images/logo.jpg', $x, $y, $w, $h, 'JPG', '', '', false, 300, '', false, false, 0, '', false, false);

	
	
	// IMPORTANT: disable font subsetting to allow users editing the document
	$pdf->setFontSubsetting(false);
	
	// set font
	$pdf->SetFont('helvetica', '', 10, '', false);	
	
	// set default form properties
	//$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 255), 'strokeColor'=>array(0, 0, 0)));

	$pdf->SetFont('helvetica', 'B', 14);
	$pdf->Ln(10);
	$pdf->Ln(6);
	$pdf->writeHTML('<b><u>DEAL CONTRACT</u></b>',true,false,false,false,'C');
	$pdf->Ln(8);
	$pdf->SetFont('helvetica', '', 12);
	$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $clientData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(6);
	
	$html = "<b>This Trade has now been executed in accordance with your instruction, which is legally binding in accordance with our Terms & Conditions.</b>";
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(6);
	
	$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $transactionData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(6);
	
	if(!empty($paymentsData)){
		$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>Where to instruct your Bank to send payment to us:</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
		$pdf->Ln(6);
		$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $paymentsData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	}
	
	$pdf->SetFontSize(10, true);
	$pdf->Ln(8);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>I confirm that the details set out above are correct and that I will transmit the amount due to the account specified. I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a GBP 25.00 per day late payment fee.</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	
	$pdf->Ln(10);
	$pdf->Ln(10);
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', 'SIGNATURE', $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
	$pdf->Ln(8);
	$pdf->Ln(10);
	$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $signatureData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(10);
	$pdf->Ln(8);
	$pdf->Ln(12);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b><u>REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</u></b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(4);
	$pdf->SetFontSize(9, true);
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $accountsData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(6);
	$pdf->SetFontSize(10, true);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b><u>Beneficiary Account Details</u></b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Cell(25,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
	$pdf->Ln(8);
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $benData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$clientNumber = $transContent["cust_accountName"];
	$localAmount = $transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"];
	/**
	 * #10232: Premier Exchange: Module to set Country based rules on Bank Transfer 
	 * Short Description
	 * generation of editable fields according to the information provided
	 */
	if(($transContent['tr_benID'] != '0') || !empty($transContent['tr_benID'])){
		
		
		$queryBenCountry = "SELECT Country FROM ".TBL_BENEFICIARY." WHERE benID = '{$transContent['tr_benID']}' LIMIT 1";
		$benCountry = selectFrom($queryBenCountry);
		//debug($benCountry, true);
		$urlLink = $_SERVER['HTTP_HOST'];
		$benficiaryCountry = $benCountry['Country'];
		
		$benAccountData = <<<EOD
			<style>
				table{
					font-size:11pt;
				}
				.txt{
					padding-left:10px;
				}
				.border{
					border:1px solid #000000;
				}
			</style>
			<form action="http://$urlLink/admin/save_bank_details_pdf.php" method="post" enctype="multipart/form-data">
			<table border="1" width="100%" style="padding:2px;">
			<tr>
				<td>CLIENT NUMBER:</td>
				<td>$clientNumber</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
				<td>$localAmount</td>
			</tr>
EOD;
	
		
	
	$queryRuleBankDetails = "SELECT country.countryName, countryRuleBankDetails.* FROM countries AS country INNER JOIN ".TBL_BEN_BANK_DETAILS_RULE." AS countryRuleBankDetails on countryRuleBankDetails.benCountry = country.countryId WHERE country.countryName = '{$benCountry['Country']}' AND countryRuleBankDetails.status = 'Enable' ORDER BY countryRuleBankDetails.updateDate DESC";
	$bankDetailsRule = selectFrom($queryRuleBankDetails);
	$flagForm = false;
		
	if(empty($bdBankName)){
		$flagForm = true;
		if($bankDetailsRule['accountName'] == 'Y'){
			$benAccountData .= <<<EOD
				<tr>
					<td width="50%"><label>BENEFICIARY ACCOUNT NAME* :</label></td>
					<td>
						<input name="bankName" type="text" size="41" class="border"/>
						<input name="reqBankName" value="Y" type="hidden"/>
					</td>
				</tr>
EOD;
		}
		elseif($bankDetailsRule['accountName'] == 'N'){
			$benAccountData .= <<<EOD
				<tr>
					<td width="50%"><label>BENEFICIARY'S ACCOUNT NAME :</label></td>
					<td class="border">
						<input name="bankName" type="text" size="41" class="border"/>
					</td>
				</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
			<tr>
				<td>BENEFICIARY�S ACCOUNT NAME:</td>
				<td>$bdBankName</td>
			</tr>
EOD;
	}
				
	if(empty($bdAccNum)){
		$flagForm = true;
		if($bankDetailsRule['accNo'] == 'Y'){
			$benAccountData .= <<<EOD
				<tr style="background-color:#ccc;">
					<td>
						<label>Account NO* :</label>
					</td>
					<td>
						<input name="accountNumber" type="text" size="41" class="border"/>
						<input name="reqAccountNumber" value="Y" type="hidden"/>
					</td>
				</tr>
EOD;
		}elseif($bankDetailsRule['accNo'] == 'N'){
			$benAccountData .= <<<EOD
				<tr style="background-color:#ccc;">
					<td>
						<label>Account NO :</label>
					</td>
					<td>
						<input name="accountNumber" type="text" size="41" class="border"/>
					</td>
				</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr style="background-color:#ccc;">
			<td>BENEFICIARY�S ACCOUNT NUMBER : </td>
			<td>$bdAccNum</td>
		</tr>
EOD;
	}
			
	if(empty($bdBranchCode)){
		$flagForm = true;
		if($bankDetailsRule['branchNameNumber'] == 'Y'){
		$benAccountData .= <<<EOD
		<tr style="background-color:#ccc;">
			<td>
				<label>Branch Name / Number* :</label>
			</td>
			<td>
				<input name="branchCode" type="text" size="41" class="border"/>
				<input name="reqBranchCode" type="hidden" value="Y"/>
			</td>
		</tr>
EOD;
		}elseif($bankDetailsRule['branchNameNumber'] == 'N'){
			$benAccountData .= <<<EOD
			<tr style="background-color:#ccc;">
				<td>
					<label>Branch Name / Number :</label>
				</td>
				<td>
					<input name="branchCode" type="text" size="41" class="border"/>
				</td>
			</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr style="background-color:#ccc;">
			<td>BRANCH NAME / NUMBER:</td>
			<td>$bdBranchCode</td>
		</tr>
EOD;
	}

		
	
	if(empty($bdBranchAddress)){
		$flagForm = true;
		if($bankDetailsRule['branchAddress'] == 'Y'){
			$benAccountData .= <<<EOD
			<tr>
				<td>
					<label>NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO* :</label><br /><br /><br />
				</td>
				<td>
					<textarea name="branchAddress" cols="35" rows="5" class="border"></textarea>
					<input name="reqBranchAddress" value="Y" type="hidden"/>
				</td>
			</tr>
EOD;
		}elseif($bankDetailsRule['branchAddress'] == 'N'){
			$benAccountData .= <<<EOD
			<tr>
				<td>
					<label>NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO : </label><br /><br /><br />
				</td>
				<td>
					<textarea name="branchAddress" cols="41" rows="5" class="border"></textarea>
				</td>
			</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr>
			<td>NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
			<td>$bdBranchAddress</td>
		</tr>
EOD;
	}
	
	if(empty($bdSwiftCode)){
		$flagForm = true;
		if($bankDetailsRule['swiftCode'] == 'Y'){
			$benAccountData .= <<<EOD
			<tr style="background-color:#ccc;">
				<td><label>SWIFT/BIC Code of your bank* :</label></td>
				<td>
					<input name="swiftCode" type="text" size="41" class="border"/>
					<input name="reqSwiftCode" value="Y" type="hidden"/>
				</td>
			</tr>
EOD;
		}elseif($bankDetailsRule['swiftCode'] == 'N'){
			$benAccountData .= <<<EOD
			<tr>
				<td><label>SWIFT/BIC Code of your bank :</label></td>
				<td>
					<input name="swiftCode" type="text" size="41" class="border"/>
				</td>
			</tr>
EOD;
		}
	}
	else{
		$benAccountData .= <<<EOD
		<tr style="background-color:#ccc;">
			<td>SWIFT/BIC CODE OF YOUR BANK:</td>
			<td>$bdSwiftCode</td>
		</tr>
EOD;
	}
	if(empty($bdSortCode)){
		$flagForm = true;
		if($bankDetailsRule['sortCode'] == 'Y'){
			$benAccountData .= <<<EOD
			<tr>
				<td>
					<label>SORT CODE* :</label>
				</td>
				<td>
					<input name="sortCode" size="41" class="border" type="text"/>
					<input name="reqSortCode" value="Y" type="hidden"/>
				</td>
			</tr>
EOD;
		}
		elseif($bankDetailsRule['sortCode'] == 'N'){
			$benAccountData .= <<<EOD
			<tr>
				<td>
					<label>SORT CODE :</label>
				</td>
				<td>
					<input name="sortCode" size="41" class="border" type="text"/>
				</td>
			</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr>
			<td>SORT CODE:</td>
			<td>$bdSortCode</td>
		</tr>
EOD;
	}
	
	if(empty($bdIbanNum)){
			$flagForm = true;
			if($bankDetailsRule['iban'] == 'Y'){
				$benAccountData .= <<<EOD
				<tr>
					<td>
						<label>BENEFICIARY'S IBAN* :</label>
						<br />
						<label style="font-size:9pt">(if the payment is in euros, a full iban is required to avoid a bank administration charge)</label>
					</td>
					<td>
						<textarea name="ibanNumber" cols="41" rows="3" class="border"></textarea>
						<input name="reqIbanNumber" value="Y" type="hidden"/>
					</td>
				</tr>
EOD;
		}elseif($bankDetailsRule['iban'] == 'N'){
			$benAccountData .= <<<EOD
				<tr>
					<td>
						<label>Beneficiary's IBAN :</label>
						<br />
						<label style="font-size:9pt">(if the payment is in euros, a full iban is required to avoid a bank administration charge)</label>
					</td>
					<td>
						<textarea name="ibanNumber" cols="41" rows="3" class="border"></textarea>
					</td>
				</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr>
			<td>BENEFICIARY�S IBAN :</td>
			<td>$bdIbanNum</td>
		</tr>
EOD;
	}
			
	
	
	
	if(empty($bdRoutingNumber)){
		$flagForm = true;
		if($bankDetailsRule['routingNumber'] == 'Y'){
			$benAccountData .= <<<EOD
			<tr style="background-color:#ccc;">
				<td>
					<label>ABA/ROUTING NUMBER (USA ONLY)* :</label>
				</td>
				<td class="border">
					<input name="routingNumber" type="text" size="41" class="border"/>
					<input name="reqRoutingNumber" value="Y" type="hidden"/>
				</td>
			</tr>
EOD;
		}elseif($bankDetailsRule['routingNumber'] == 'N'){
			$benAccountData .= <<<EOD
			<tr>
				<td>
					<label>ABA/ROUTING NUMBER (USA ONLY) :</label>
				</td>
				<td>
					<input name="routingNumber" type="text" size="41" class="border"/>
				</td>
			</tr>
EOD;
		}
	}else{
		$benAccountData .= <<<EOD
		<tr style="background-color:#ccc;" >
			<td>ABA/ROUTING NUMBER (USA ONLY) :</td>
			<td>$bdRoutingNumber</td>
		</tr>
EOD;
	}
	$benAccountData .= <<<EOD
	<tr style="background-color:#ccc;" >
		<td>WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
		<td>
			<textarea name="tip" cols="41" rows="2" class="border">$tipV</textarea>
		</td>
	</tr>
	</table>
EOD;
	$benAccountData .= <<<EOD
		<br />
		</br />
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td align="center">
EOD;
	//debug($flagForm);
	if($flagForm){
		$benAccountData .= <<<EOD
					<input type="hidden" name="benID" value="$benID" />
					<input type="hidden" name="benCountry" value="$benficiaryCountry"/>
EOD;
	}
	//debug($benAccountData);
	$benAccountData .= <<<EOD
					<input type="hidden" name="transID" value="$transactionID" />
					<input type="submit" name="save" value="Save"/>
					&nbsp;&nbsp;&nbsp;
					<input name="reset" type="reset" value="Reset"/>&nbsp;&nbsp;
					<input name="print" type="button" value="Print" onclick="print();"/>
					<br /><br />
				</td>
			</tr>
		</table>
	</form>
EOD;
//debug($benAccountData);
}
/*
	$benAccountData .= <<<EOD
	<tr >
		<td>WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
		<td><input name="tip" type="text" value="$tipV"/></td>
	</tr>
</table>
EOD;
*/

	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $benAccountData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	
	//debug($transContent);
	//if(empty($transContent["bd_bankName"]) && empty($transContent['bd_accNo']) && (($transContent['tr_benID'] != '0') || !empty($transContent['tr_benID'])))
		/**
		 * #10232: Premier Exchange: Module to set Country based rules on Bank Transfer 
		 * Short Description
		 * generation of editable fields according to the information provided
		 */
		/*
	{
		$queryBenCountry = "SELECT Country FROM ".TBL_BENEFICIARY." WHERE benID = '{$transContent['tr_benID']}' LIMIT 1";
		$benCountry = selectFrom($queryBenCountry);
		//debug($benCountry, true);
		$urlLink = $_SERVER['HTTP_HOST'];
		$benficiaryCountry = $benCountry['Country'];
		
		
		$queryRuleBankDetails = "SELECT country.countryName, countryRuleBankDetails.* FROM countries AS country INNER JOIN ".TBL_BEN_BANK_DETAILS_RULE." AS countryRuleBankDetails on countryRuleBankDetails.benCountry = country.countryId WHERE country.countryName = '{$benCountry['Country']}' AND countryRuleBankDetails.status = 'Enable' ORDER BY countryRuleBankDetails.updateDate DESC";
		$bankDetailsRule = selectFrom($queryRuleBankDetails);
		$flagForm = false;
		
		$html =  <<<EOD
			<style>
				table{
					font-size:11pt;
				}
				.txt{
					padding-left:10px;
				}
				.border{
					border:1px solid #000000;
				}
			</style>
			<form action="http://$urlLink/admin/save_bank_details_pdf.php" method="post" enctype="multipart/form-data">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
EOD;
			if(empty($bdBankName)){
				$flagForm = true;
				if($bankDetailsRule['accountName'] == 'Y'){
					$html .= <<<EOD
						<tr>
							<td width="50%"><label>Beneficiary Account Name* :</label></td>
							<td width="5%">&nbsp;</td>
							<td>
								<input name="bankName" value=" " type="text" size="41" class="border"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
EOD;
				}
				elseif($bankDetailsRule['accountName'] == 'N'){
					$html .= <<<EOD
						<tr>
							<td width="50%"><label>Beneficiary's Bank Name :</label></td>
							<td width="5%">&nbsp;</td>
							<td class="border">
								<input name="bankName" value=" " type="text" size="41" class="border"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
EOD;
				}
			}
				
			if(empty($bdAccNum)){
				$flagForm = true;
				if($bankDetailsRule['accNo'] == 'Y'){
					$html .= <<<EOD
						<tr>
							<td>
								<label>Account NO* :</label>
							</td>
							<td width="5%">&nbsp;</td>
							<td>
								<input name="accountNumber" value=" " type="text" size="41" class="border"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
EOD;
			}elseif($bankDetailsRule['accNo'] == 'N'){
					$html .= <<<EOD
						<tr>
							<td>
								<label>Account NO :</label>
							</td>
							<td width="5%">&nbsp;</td>
							<td>
								<input name="accountNumber" value=" " type="text" size="41" class="border"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
EOD;
		}
	}
			
			
			if(empty($bdIbanNum)){
				$flagForm = true;
				if($bankDetailsRule['iban'] == 'Y'){
					$html .= <<<EOD
					<tr>
						<td>
							<label>Beneficiary's IBAN* :</label>
							<br />
							<label style="font-size:9pt">(if the payment is in euros, a full iban is required to avoid a bank administration charge)</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="ibanNumber" value=" " type="text" size="41" class="border"/>
						</td>
					</tr>
EOD;
			}elseif($bankDetailsRule['iban'] == 'N'){
				$html .= <<<EOD
					<tr>
						<td>
							<label>Beneficiary's IBAN :</label>
							<br />
							<label style="font-size:9pt">(if the payment is in euros, a full iban is required to avoid a bank administration charge)</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="ibanNumber" value=" " type="text" size="41" class="border"/>
						</td>
					</tr>
EOD;
			}
		}
			
			if(empty($bdBranchCode)){
				$flagForm = true;
				if($bankDetailsRule['branchNameNumber'] == 'Y'){
				$html .= <<<EOD
				<tr>
					<td>
						<label>Branch Name / Number* :</label>
					</td>
					<td width="5%">&nbsp;</td>
					<td>
						<input name="branchCode" value=" " type="text" size="41" class="border"/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
EOD;
			}elseif($bankDetailsRule['branchNameNumber'] == 'N'){
				$html .= <<<EOD
				<tr>
					<td>
						<label>Branch Name / Number :</label>
					</td>
					<td width="5%">&nbsp;</td>
					<td>
						<input name="branchCode" value=" " type="text" size="41" class="border"/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
EOD;
			}
		}
			if(empty($bdBranchAddress)){
				$flagForm = true;
				if($bankDetailsRule['branchAddress'] == 'Y'){
					$html .= <<<EOD
					<tr>
						<td><label>Branch Address* :</label></td>
						<td width="5%">&nbsp;</td>
						<td>
							<textarea name="branchAddress" cols="35" rows="8" class="border"> </textarea>
						</td>
					</tr>
					<tr>
						<td height="130px">&nbsp;</td>
					</tr>
EOD;
				}elseif($bankDetailsRule['branchAddress'] == 'N'){
					$html .= <<<EOD
					<tr>
						<td><label>Branch Address :</label></td>
						<td width="5%">&nbsp;</td>
						<td>
							<textarea name="branchAddress" cols="35" rows="8" class="border"> </textarea>
						</td>
					</tr>
					<tr>
						<td height="130px">&nbsp;</td>
					</tr>
EOD;
				}
			}
			if(empty($bdSwiftCode)){
				$flagForm = true;
				if($bankDetailsRule['swiftCode'] == 'Y'){
					$html .= <<<EOD
					<tr>
						<td><label>SWIFT/BIC Code of the your bank* :</label></td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="swiftCode" value=" " type="text" size="41" class="border"/>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
EOD;
				}elseif($bankDetailsRule['swiftCode'] == 'N'){
					$html .= <<<EOD
					<tr>
						<td><label>SWIFT/BIC Code of the your bank :</label></td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="swiftCode" value=" " type="text" size="41" class="border"/>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
EOD;
				}
			}
			if(empty($bdSortCode)){
				$flagForm = true;
				if($bankDetailsRule['sortCode'] == 'Y'){
					$html .= <<<EOD
					<tr>
						<td>
							<label>Clearing code of the bank* :</label>
							<br />
							<label style="font-size:9pt">(i.e. sort code/aba routing number)</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="sortCode" type="text" value=" " size="41" class="border"/>
						</td>
					</tr>
EOD;
				}
				elseif($bankDetailsRule['sortCode'] == 'N'){
					$html .= <<<EOD
					<tr>
						<td>
							<label>Clearing code of the bank :</label>
							<br />
							<label style="font-size:9pt">(i.e. sort code/aba routing number)</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="sortCode" type="text" value=" " size="41" class="border"/>
						</td>
					</tr>
EOD;
				}
			}
			if(empty($bdRoutingNumber)){
				$flagForm = true;
				if($bankDetailsRule['routingNumber'] == 'Y'){
					$html .= <<<EOD
					<tr>
						<td>
							<label>Routing Number* :</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td class="border">
							<input name="routingNumber" type="text" value=" " size="41" class="border"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
EOD;
				}elseif($bankDetailsRule['routingNumber'] == 'N'){
					$html .= <<<EOD
					<tr>
						<td>
							<label>Routing Number :</label>
						</td>
						<td width="5%">&nbsp;</td>
						<td>
							<input name="routingNumber" type="text" value=" " size="41" class="border"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
EOD;
				}
			}
			$html .= <<<EOD
				<tr>
					<td colspan="3" align="center">
						<input type="hidden" name="transID" value="$transactionID" />
						<input type="hidden" name="benID" value="$benID" />
						<input type="hidden" name="benCountry" value="$benficiaryCountry"/>
						<input type="submit" name="save" value="Save"/>
						&nbsp;&nbsp;&nbsp;
						<input name="reset" type="reset" value="Reset"/>&nbsp;&nbsp;
						<input name="print" type="button" value="Print" onclick="print();"/>
						
					</td>
				</tr>
			</table>
		</form>
EOD;

		/*
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,'5', $txt = 'BENEFICIARY�S ACCOUNT NAME* :', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		
		$pdf->TextField('benAccountName', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(8);
		$pdf->Cell(120,$h = 5, $txt = 'BENEFICIARY�S IBAN / ACCOUNT NO* : ', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		$pdf->TextField('accountNumber', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = '(IF THE PAYMENT IS IN EUROS, A FULL IBAN IS ', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		$pdf->Ln(4);
		$pdf->Cell(120,$h = 4, $txt = 'REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE)', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'NAME AND ADDRESS OF THE BANK *', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->TextField('address', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = 'YOUR CURRENCY IS BEING TRANSFERRED TO:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'SWIFT/BIC CODE OF THE YOUR BANK:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		$pdf->TextField('swiftCode', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(8);
		$pdf->Cell(120,$h = 5, $txt = 'CLEARING CODE OF THE BANK:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->TextField('clearingCode', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = '(I.E. SORT CODE/ABA ROUTING NUMBER) ', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'WHAT REFERENCE, IF ANY,', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->TextField('reference', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->Cell(120,$h = 4, $txt = 'DO YOU WANT TO QUOTE ON YOUR TRANSFER: ', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		$pdf->Ln(6);
		
		
		$pdf->Cell(60,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->Button('print', 15, 7, 'Print', 'Print()', array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(204, 204, 204), 'strokeColor'=>array(255, 255, 255)));
		
		$pdf->Cell(20,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		// Reset Button
		$pdf->Button('reset', 15, 7, 'Reset', array('S'=>'ResetForm'), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(204, 204, 204), 'strokeColor'=>array(255, 255, 255)));
		$pdf->Cell(20,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		// Submit Button
		$pdf->Button('submit', 15, 7, 'Save', array('S'=>'SubmitForm', 'F'=>'http://localhost/test/pdf_test/tcpdf/printvars.php', 'Flags'=>array('ExportFormat')), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(204, 204, 204), 'strokeColor'=>array(255, 255, 255)));
		*/
		//if($flagForm)
			//$pdf->writeHTML($html,true, 0, true, 0);
	//}
	$pdf->SetFontSize(9, true);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>CUSTOMER DECLARATION AND DETAILS</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(4);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<div style="text-align:justify">I confirm that I will make arrangements to transfer the requisite amount of funds to pay for the currency ordered as per my instruction given in relation to this trade number and that cleared funds will be credited to Premier FX�s Client Account one (1) working day before the maturity date of this trade. Should I envisage any problem in keeping to this agreement, I will contact my personal dealer immediately. In the event that he or she is unavailable, I will speak to another available dealer of Premier FX to ensure that the Company is made aware of the situation. I also confirm that I am aware that any changes to this trade may incur additional charges.</div>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(2);
	//$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $confirmedData, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
	//$pdf->Ln(4);
	
	$pdf->SetFontSize(8, true);
	$pdf->Cell(130,$h = 4, ' ', $border = 0, $ln = 0, $align = 'L', $fill = false);
	$pdf->writeHTML($companyAddress, $ln=1, $fill=0, $reseth=true,$cell=false, $align='');
	

	$pdfcode = $pdf->Output('DEAL_CONTRACT.pdf', 'S');
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_DEAL_').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);

	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));
	
		
	$deal_contract_name = 'DEAL_CONTRACT.pdf';
	$deal_contract_path = $fPDFname;

/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  START EMAIL BODY ***************/
$timeStamp = time();
$num = md5( time() );
/* ticket # 10407 email template
*/
//incomplete deal contract
//doing work of ticket#10750
//$fullName = $transContent["cust_firstName"]." ".$transContent["cust_lastName"];
$fullName = $transContent["cust_firstName"];
$emailContent1 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='3' and email_templates.status = 'Enable' ");
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
$message11 = $emailContent1["body"];
$subject1 = $emailContent1["subject"];
$varEmail1 = array("{customer}","{logo}","{accountmanager}");
//$accM_name= explode(" ",$transContent["accM_name"]);

$contentEmail1 = array($fullName,$logoCompany,$transContent["accM_name"]);
$messageT1 = str_replace($varEmail1,$contentEmail1,$message11);

//complete deal contract
$emailContent2 = selectFrom("select * from email_templates left join events on email_templates.eventID=events.id where events.id='4' and email_templates.status = 'Enable' ");
$logoCompany = "<img border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg?v=1' />";
$message12 = $emailContent2["body"];
$subject2 = $emailContent2["subject"];
$varEmail2 = array("{customer}","{logo}","{accountmanager}");

$contentEmail2 = array($fullName,$logoCompany,$transContent["accM_name"]);
$messageT2 = str_replace($varEmail2,$contentEmail2,$message12);
/*
$messageT = "\n<br />Dear ". $transContent["cust_firstName"] .",\n<br /><br />Please find attached deal contract for trade done today.  Please check all details are correct and if possible please e-mail/fax back a SIGNED\ncopy to us.\nThank you very much for dealing with Premier FX. <br /><br />Regards,<br /> ";
*/
/**
 * #10287: Premier Exchange: Deal Contract Email Content
 * Email content body changed according to the beneficiary bank details provided
 */

if($transContent["trans_details"] == 'Yes'){
	$messageT = $messageT2;
	$subject = $subject2;
	/*$messageT = "\n<br />Dear ". $transContent["cust_firstName"].",\n<br /><br />Please find attached deal contract for trade done today. Please check all details are correct, and inform us if there are any discrepancies.\n<br /><br />The contract is for your records only and there is no need to return it to us.\n<br /><br />Thank you very much for dealing with Premier FX\n<br /><br />Regards ";*/
}else{
	$messageT = $messageT1;
	$subject = $subject1;
	/*$messageT = "\n<br />Dear ". $transContent["cust_firstName"] .",\n<br /><br />Please find attached deal contract for trade done today. Please complete the form with your beneficiary bank details and press save. This will return the details to us.\n<br /><br />Please print a copy for your records if required.\n<br /><br />Thank you very much for dealing with Premier FX\n<br /><br />Regards ";*/
}

if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1")
	$agentEmail = $transContent['accM_email'];

/*$messageT .= "\n<br />".$transContent["accM_name"]."<br />\n";
$messageT .= "<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>
						55 Old Broad Street,London, EC2M 1RX.\n<br>
						<font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370\n<br>\n<br>
						<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br>
						Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve.  \n<br>
						<font color='#F7A30B'>Tel:</font> +351 289 358 511\n<br>
						<font color='#F7A30B'>FAX:</font> +351 289 358 513\n<br>\n<br>
						<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>
						C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br>
						<font color='#F7A30B'>Tel:</font> +34 971 576 724 
						 \n <br> 
						 <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font>
					</td>
				</tr>
			</table>";*/
//debug($messageT, true);
/***************  END EMAIL BODY ***************/



// Prepare Email for Sender and Account Manger

//$subject = "Deal Contract";
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

$fromName  = SYSTEM;
$fromEmail = $fromEmail;

if(!empty($agentEmail)){
	$fromName  = $transContent["accM_name"];
	$fromEmail = $agentEmail;	
}
require_once("lib/phpmailer/class.phpmailer.php");
$sendermailer = new PHPMailer();
$agentmailer = new PHPMailer();
$sendermailer->FromName = $agentmailer->FromName = $transContent["accM_name"];
$sendermailer->From = $agentmailer->From = $transContent["accM_email"]; 
if(CONFIG_SEND_EMAIL_ON_TEST == "1")
{
	$sendermailer->AddAddress(CONFIG_TEST_EMAIL,'');
	$agentmailer->AddAddress(CONFIG_TEST_EMAIL,'');
}
else
{
	$sendermailer->AddAddress($custEmail,'');
	$agentmailer->AddAddress($agentEmail,'');
}
$agentmailer->Subject = $sendermailer->Subject = $subject;

if($transContent["trans_details"] == 'Yes' && CONFIG_SEND_EMAIL_ON_TEST != "1")
{
	//Add CC	
	//die($emailContent2['cc']);
	if($emailContent2['cc'])
	$sendermailer->AddAddress($emailContent2['cc'],'');
	
	//Add BCC
	if($emailContent2['bcc'])
	$sendermailer->AddBCC($emailContent2['bcc'],'');
}
else if (CONFIG_SEND_EMAIL_ON_TEST != "1")
{
	//Add CC	
	if($emailContent1['cc'])
	$sendermailer->AddAddress($emailContent1['cc'],'');
	
	//Add BCC
	if($emailContent1['bcc'])
	$sendermailer->AddBCC($emailContent1['bcc'],'');
	
	
	
	
}


$sendermailer->IsHTML(true);
$agentmailer->IsHTML(true);


// attachements
if($fpPDF){
	$sendermailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
	$agentmailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
}
else{
	$attachementInfo = '<br/>Deal Contract could not be attached with email';
}



// Body
$agentmailer->Body = $sendermailer->Body = $messageT;


	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		if($custEmail && CONFIG_EMAIL_STATUS == "1")
		{
			$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers);
		}
			//debug($agentmailer, true);
		if($accMgrEmail && CONFIG_EMAIL_STATUS == "1")
		{
			$accMgrEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
		}
	}
	else{
	if($custEmail && CONFIG_EMAIL_STATUS == "1")
		$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
	if($agentEmail && CONFIG_EMAIL_STATUS == "1")
		$agentEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
	}

$msg = "Email is not sent to customer and introducer (may not exist or email functionality is disabled)";



if ($custEmailFlag || $agentEmailFlag || $accMgrEmailFlag) {
	if($custEmailFlag && $agentEmailFlag){
		$msg = "Email has been sent to customer and introducer.";
	}
	else if($custEmailFlag && $accMgrEmailFlag){
		$msg = "Email has been sent to customer and Account Manager.";
	}
	else{
		if($custEmailFlag)
			$msg = "Email has been sent to customer.";
		elseif($agentEmailFlag)
			$msg = "Email has been sent to introducer.";
	}
	$msg.=$attachementInfo."<br/>";
}
?>

<div class='noPrint' align="center">
		<?=$msg?>&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?

if(sizeof($transContent) < 1){
	?>
	<div class='noPrint' align="center">
	No Record found
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?
	}

?>
