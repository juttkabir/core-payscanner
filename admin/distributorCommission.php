<?
	session_start();
	include ("../include/config.php");
	include ("security.php");

	/**
	 * Variables from Query String
	 */
	$cmd 					= $_REQUEST["cmd"];
	$id						= $_REQUEST["id"];
	$distributor 			= $_REQUEST["distributor"];
	$collectionPoint 		= $_REQUEST["collectionPoint"];
	$attachCollectionPoint	= $_REQUEST["attachCollectionPoint"];
	$fixedValue				= $_REQUEST["fixedValue"];
	$percentValue 			= $_REQUEST["percentValue"];

	/**
	 * Some pre data preparation.
	 */
	$fixedValue			= (!empty($fixedValue))?trim($fixedValue):"0";
	$percentValue	 	= (!empty($percentValue))?trim($percentValue):"0";
	
	/**
	 * Check for data validity
	 */
	$fixedValue		 	= (is_numeric($fixedValue))?$fixedValue:"0";
	$percentValue	 	= (is_numeric($percentValue))?$percentValue:"0";

	if($cmd == "ADD")
	{
		if(!empty($distributor) && !empty($fixedValue) && !empty($percentValue))
		{
			if($attachCollectionPoint == "Y" && !empty($collectionPoint))
			{
				$isCollectionPoint = " and collectionPointId=".$collectionPoint;
			} else {
				$isCollectionPoint = " and collectionPointId=0";
			}
			
			$sql = "SELECT
						id
					FROM
						commission
					WHERE
						userId = ".$distributor."
						".$isCollectionPoint."
					";
			$result = mysql_query($sql);
			$numrows = mysql_num_rows($result);
			
			if($numrows <= 0)
			{
				if($attachCollectionPoint == "Y" && !empty($collectionPoint))
				{
					$sql = "INSERT INTO
								commission
								(
									userId,
									collectionPointId,
									fixedValue,
									percentValue,
									created
								)
							VALUES
							(
								".$distributor.",
								".$collectionPoint.",
								".$fixedValue.",
								".$percentValue.",
								NOW()
							)
							";
				} else {
					$sql = "INSERT INTO
								commission
								(
									userId,
									fixedValue,
									percentValue,
									created
								)
							VALUES
							(
								".$distributor.",
								".$fixedValue.",
								".$percentValue.",
								NOW()
							)
							";
				}
				
				$result = mysql_query($sql);
				$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
				unset($cmd, $distributor, $collectionPoint, $attachCollectionPoint, $fixedValue, $percentValue);
			} else {
				$errMsg = "<span class='errorMessage'>Record already exists, please use a different distributor and/or collection point. Or you may edit the existing record to update the values.</span>";
			}
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "UPDATE")
	{
		if(!empty($id) && !empty($distributor) && !empty($fixedValue) && !empty($percentValue))
		{
			if($attachCollectionPoint == "Y" && !empty($collectionPoint))
			{
				$isCollectionPoint = " and collectionPointId=".$collectionPoint;
			}
			
			$sql = "SELECT
						id
					FROM
						commission
					WHERE
						userId = ".$distributor."
						".$isCollectionPoint."
					";
			$result = mysql_query($sql);
			$numrows = mysql_num_rows($result);
			
			if($numrows <= 0)
			{
				if($attachCollectionPoint != "Y" || empty($attachCollectionPoint))
				{
					$collectionPointId = 0;
				}
				
				$sql = "UPDATE 
							commission
						SET
							userId=".$distributor.",
							collectionPointId=".$collectionPoint.",
							fixedValue=".$fixedValue.",
							percentValue=".$percentValue.",
							updated=NOW()
						WHERE
							id=".$id."
						";
				
				$result = mysql_query($sql);
				$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
				unset($cmd, $distributor, $collectionPoint, $attachCollectionPoint, $fixedValue, $percentValue, $id);
			} else {
				$errMsg = "<span class='errorMessage'>Record already exists, please use a different distributor and/or collection point. Or you may edit the existing record to update the values.</span>";
			}
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "EDIT")
	{
		if(!empty($id))
		{
			$sql = "SELECT
						userId,
						collectionPointId,
						fixedValue,
						percentValue
					FROM
						commission
					WHERE
						id=".$id."
					";
			$result = mysql_query($sql);
			$numrows = mysql_num_rows($result);
			
			if($numrows > 0)
			{
				while($rs = mysql_fetch_array($result))
				{
					$distributor		= $rs["userId"];
					$collectionPoint	= $rs["collectionPointId"];
					$fixedValue			= $rs["fixedValue"];
					$percentValue		= $rs["percentValue"];
				}
				
				$cmd = "UPDATE";
			} else {
				$errMsg = "<span class='errorMessage'>No record found, please select a valid record.</span>";
			}
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for editing.</span>";
		}
	}
	
	if($cmd == "DELETE")
	{
		if(!empty($id))
		{
			$sql = "DELETE FROM
						commission
					WHERE
						id=".$id."
					";
			$result = mysql_query($sql);
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
			unset($cmd, $id);
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for deletion.</span>";
		}
	}
	
	if(empty($cmd))
	{
		$cmd = "ADD";
		$fixedValue = 0;
		$percentValue = 0;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Distributor Commission</title>
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.2.5.min.js"></script>
<script type="text/javascript" src="jquery.cluetip.js"></script>
<script type="text/javascript">
function loadCollectionPoints(distributorId)
{
	if($('#attachCollectionPoint').attr("checked") == true)
	{
		$.ajax
		({
			type: "GET",
			url: "loadCollectionPoints.php",
			data: "distributor="+distributorId,
			beforeSend: function(XMLHttpRequest)
			{
				$("#status").text("Loading...");
			},
			success: function(responseText)
			{
				$("#collectionPoint").html(responseText);
				$("#status").text("");
			}
		});
	}
}

/**
 * Calculates which-ever-is-higher commission.
 */
function calculateWIHC()
{
	var amount = parseFloat(prompt("Enter an amount...", 100));
	var fixedValue = parseFloat($("#fixedValue").val());
	var percentValue = parseFloat($("#percentValue").val());
	var newCommission = fixedValue;
	var tmpCommission = (amount * percentValue) / 100;
	
	if(tmpCommission > fixedValue)
	{
		newCommission = tmpCommission;
	}
	
	alert(
		"Amount: " + amount + "\n" +
		"Percent: " + tmpCommission + " (" + percentValue + "%)\n" +
		"Fixed: " + fixedValue + "\n\n" +
		"Commission: " + newCommission
		);
}
	
$(document).ready(function() {
	$('a').cluetip({splitTitle: '|'});
});
</script>
</head>
<body>
<table width="600" border="0" align="center" cellpadding="10" cellspacing="0" id="boundingBox">
	<tr>
		<td><p class="pageTitle">Distributor Commission</p>
			<p class="description">This distributor commission manager works on standard &quot;which-ever-is-higher&quot; logic. You will need to specify fixed and percentage values for a distributor's commission. For example, if fixed commission is $2 against a $200 transaction amount and it's calculated percentage based commission is $4 (2%), then system should charge the $4 amount rather than $2. Same is applied if transaction amount is $10, fixed commission is $2 and calculated percentage based commission is $0.2 (2%), then system should charge $2 - which is higher.</p>
			
			<?
				if(!empty($errMsg))
				{
					echo "<p>".$errMsg."</p>";
				}
			?>
			
		<form id="form1" name="form1" method="post" action="distributorCommission.php">
			<table width="100%" border="0" cellspacing="0" cellpadding="10">
				<tr>
					<td width="65%" valign="top"><p>Distributor <span class="mandatory">*</span> <br />
						<select name="distributor" id="distributor" title="Select a distributor." onchange="loadCollectionPoints(this.value)">
							<option value="">-- Select -- </option>
							<?
								$sql = "SELECT
											userID, username, name
										FROM
											admin
										WHERE
											isCorrespondent = 'ONLY'
											AND parentID > 0
											AND adminType = 'Agent'
										ORDER BY
											name
										";
								$result = SelectMultiRecords($sql);
								$numrows = sizeof($result);
								
								for($i=0; $i<$numrows; $i++)
								{
									if($distributor == $result[$i]["userID"])
									{
										$selected = " selected=\"selected\"";
									} else {
										$selected = "";
									}
									
									echo "<option value=\"".$result[$i]["userID"]."\"".$selected.">".$result[$i]["name"]." [".$result[$i]["username"]."]</option>";
								}
							?>
						</select>
						<a href="#" title="Distributor|Shows a list of available distributors. You can select one to enter the rates for.">[?]</a></p>
						<p>
							<input name="attachCollectionPoint" type="checkbox" id="attachCollectionPoint" title="Check on to link above selected distributor with a collection point." value="Y" onclick="loadCollectionPoints($('#distributor').val())" />
							Attach Distributor with Collection Point <a href="#" title="Attach Distributor with Collection Point|Check on this option to load a list of linked collection points, and you can select a collection point listed below.">[?]</a> </p>
						<blockquote>
							<p>Collection Point <br />	
									<select name="collectionPoint" id="collectionPoint" title="Select a collection point." class="dropDownList">
									</select> <a href="#" title="Collection Point|Shows a list of collection points linked with a distributor. If you checked the above option, then selected collection point will be attached with this rate information.">[?]</a> <span id="status" class="statusMessage">&nbsp;</span></p>
						</blockquote></td>
					<td width="35%" valign="top"><p>Fixed Rate <span class="mandatory">*</span> <br />
							<input name="fixedValue" type="text" id="fixedValue" title="Enter fixed commission rate." value="<?=$fixedValue?>" size="20" maxlength="6" class="inputBoxesNumerics" />
							<a href="#" title="Fixed Rate|Fixed value of the commission, which will be charged as minimum commission amount.">[?]</a></p>
						<p>Percentage <span class="mandatory">*</span><br />
							<input name="percentValue" type="text" id="percentValue" title="Enter percentage based commission rate." value="<?=$percentValue?>" size="20" maxlength="6" class="inputBoxesNumerics" />
						<a href="#" title="Percentage|Percentage based value of the commission, which will be charged if percentage based calculated amount is higher than the fixed commission amount.">[?]</a></p></td>
				</tr>
				<tr>
					<td valign="top">&nbsp;</td>
					<td valign="top">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top" class="buttonsToolbar"><input type="submit" value="Submit" title="Click to save the changes." />
						<input type="reset" value="Reset" title="Click to revert back current changes." />
						<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
						<input name="id" type="hidden" id="id" value="<?=$id?>" /></td>
					<td align="right" valign="top" class="buttonsToolbar"><input type="button" name="showDemo" value="Show Demo" title="Click this button to see an example calculation based on your entered values." onclick="calculateWIHC()" /> 
						<a href="#" title="Show Demo|Displays a demonstration of commission calculation, based on entered values. A dummy amount is used to simulate the calculations.">[?]</a> </td></tr>
			</table>
			</form>
			<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="25%" class="reportHeader">Distributor</td>
					<td width="25%" class="reportHeader">Collection Point </td>
					<td width="15%" align="right" class="reportHeader">Fixed Rate </td>
					<td width="15%" align="right" class="reportHeader">Percentage</td>
					<td width="20%" class="reportHeader">&nbsp;</td>
				</tr>
				
				<?
					$sql = "SELECT
								c.id,
								a.username,
								a.name,
								p.cp_corresspondent_name,
								p.cp_city,
								p.cp_country,
								c.fixedValue,
								c.percentValue
							FROM
								commission c
							LEFT JOIN 
								admin a 
							ON 
								c.userId = a.userID
							LEFT JOIN 
								cm_collection_point p 
							ON 
								c.collectionPointId = p.cp_id
							";
					
					$result = mysql_query($sql);
					$numrows = mysql_num_rows($result);
					
					while($rs = mysql_fetch_array($result))
					{
						$fId				= $rs["id"];
						$fUserName			= $rs["username"];
						$fName				= $rs["name"];
						$fCPName			= $rs["cp_corresspondent_name"];
						$fCPCity			= $rs["cp_city"];
						$fCPCountry			= $rs["cp_country"];
						$fFixedValue		= $rs["fixedValue"];
						$fPercentValue		= $rs["percentValue"];
				?>
						<tr>
							<td class="list"><?=$fName." [".$fUserName."]"?></td>
							<td class="list"><?=$fCPName.", ".$fCPCity.", ".$fCPCountry?></td>
							<td align="right" class="list"><?=number_format($fFixedValue, 2, ".", ",")?></td>
							<td align="right" class="list"><?=number_format($fPercentValue, 2, ".", ",")?></td>
							<td align="right" class="list">
								<a href="distributorCommission.php?id=<?=$fId?>&amp;cmd=DELETE" title="System|Click to delete the record">Delete</a>
							</td>
						</tr>
				<?
					}
				?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
