<?php

	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$nowTime = date("F j, Y");
	$transID = $_REQUEST["transid"];
	//debug($transID);
	$today = date("d-m-Y");
	$todayD= date("Y-m-d");
	$emailFlag = false;
	$mailto = $_REQUEST["emailto"];
	//debug ($_REQUEST, true);
	 /*
	 * 7897 - Premier FX
	 * Rename 'Create Transaction' text in the system.
	 * Enable  : string
	 * Disable : "0"
	*/
	
	$strTransLabel = 'Transaction';
	
	if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
		$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
td.myFormat
{
font-size:10px;	
}

td.Arial
{
font-size:10px;
font-family:"Gill Sans MT",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
background:#C0C0C0;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
background:#C0C0C0;
}
td.bottom
{
border-bottom: solid 1px #000000;  
font-size:10px;
}
td.bottomborder{
border-bottom: solid 1px #000000;
}
td.leftborder{
border-left: solid 1px #000000;
}

td.sign
{
font-size:10px;
}
td.style2
{
font-size:10px;
}
td.terms
{
font-size:12px;
font-family: Gill Sans MT;
}
</style>
</head>
</html>
<?php
$message="";
$nTransID = $_REQUEST['transid'];	


$nTransID=explode(",",$nTransID);


if(is_array($nTransID) && $nTransID!="")
{
	$countTrans = count($nTransID);
}
else
{
	$countTrans = 1;
}
//debug($nTransID);
for($t=0;$t<$countTrans;$t++)
{


	$strTransactions = "SELECT 
							tr.transID as transID,
							tr.custAgentID as tr_custAgentID,
							tr.benID as tr_benID,
							tr.customerID as tr_customerID,
							tr.addedBy as tr_addedBy,
							tr.currencyFrom as tr_currencyFrom,
							tr.transDate as tr_transDate,
							tr.valueDate as tr_valueDate,
							tr.localAmount as tr_localAmount,
							tr.currencyTo as tr_currencyTo,
							tr.transAmount as tr_transAmount,
							tr.exchangeRate as tr_exchangeRate,
							tr.refNumber as tr_refNumber,
							
							cust.customerID as cust_customerID,
							cust.email as cust_email,
							cust.parentID as cust_accManagerID,
							cust.firstName as cust_firstName,
							cust.lastName as cust_lastName,
							cust.accountName as cust_accountName,
							
							ag.email as ag_email,
							ag.name as ag_name,
							
							accM.email as accM_email,
							accM.name as accM_name,
							
							bd.bankName as bd_bankName,
							bd.accNo as bd_accNo,
							bd.IBAN as bd_IBAN,
							bd.sortCode as bd_sortCode,
							bd.routingNumber as bd_routingNumber,
							bd.branchAddress as bd_branchAddress,
							bd.swiftCode as bd_swiftCode
							
						FROM
							".TBL_TRANSACTIONS." as tr 
							LEFT JOIN ".TBL_CUSTOMER." as cust ON tr.customerID = cust.customerID  
							LEFT JOIN ".TBL_ADMIN_USERS." as ag ON tr.custAgentID = ag.userID 
							LEFT JOIN ".TBL_ADMIN_USERS." as accM ON cust.parentID = accM.userID 
							
							LEFT JOIN ".TBL_BANK_DETAILS." as bd ON tr.transID = bd.transID 
						WHERE 
							tr.transID = '".$nTransID[$t]."' 
						";
	$transContent = selectFrom($strTransactions);	
	//debug($strTransactions,true);
	$agentEmail = $transContent["ag_email"];

	$emailFlag = true;

	$customerID=$transContent['tr_customerID'];
	$custEmail = $transContent["cust_email"];

	if(!empty($mailto))
		$custEmail = $mailto;
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		$agentEmail = $accMgrEmail = $transContent["accM_email"];	
		$emailFlag = true;
	}			
	
	// accounts details against speficif sending currency
	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$transContent['tr_currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
	$trAccountrTrading = array();
	for($i=0;$i<count($fieldsCheckedArr);$i++)
	{
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr))
		{
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else
		{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		
		$trAccountrTrading[] = array(array('name'=>strtoupper($fieldsLabel),'type'=>$accountRS[$fieldsValue]));
	}
  	//$bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$nTransID[$t]."'");	
  
	$ibanOrAcc = '';
	$sortCodeV = '';
	$tipV = '';
	if($transContent["bd_IBAN"]!='' && $transContent["bd_accNo"]!=''){			
		$ibanOrAcc = "IBAN: ".$transContent["bd_IBAN"]." / ". "Account No. ".$transContent["bd_accNo"];
	}
	elseif($transContent["bd_IBAN"]!=''){
		$ibanOrAcc = "IBAN: ".$transContent["bd_IBAN"];
	}
	elseif($transContent["bd_accNo"]!=''){
		$ibanOrAcc = "Account No. ".$transContent["bd_accNo"];
	}	
	if($transContent["bd_sortCode"]!="" &&  $transContent["bd_routingNumber"]!="") {
		$sortCodeV = $transContent["bd_sortCode"]." / ".$transContent["bd_routingNumber"];			
	}
	elseif($transContent["bd_sortCode"]=""){
		$sortCodeV = $transContent["bd_routingNumber"];			
	}
	elseif($transContent["bd_routingNumber"]=""){
		$sortCodeV = $transContent["bd_sortCode"];
	}
	if(CONFIG_HIDE_TIP_FIELD != "1"){
		$tipV = $transContent["tr_tip"];
	}	
	else{
		$tipV = "BARC GB 22";
	}
	
	$termsConditionStr = '';
	if(CONFIG_TRANS_CONDITION_ENABLED == '1')
	{
		if(defined("CONFIG_TRANS_COND"))
		{
			$termsConditionStr =  (CONFIG_TRANS_COND);
		}
		else
		{
			$termsConditionSql = selectFrom("SELECT company_terms_conditions FROM company_detail
													WHERE company_terms_conditions!='' 
													AND dated = (SELECT MAX(dated) FROM company_detail WHERE company_terms_conditions!='')");
			if(!empty($termsConditionSql["company_terms_conditions"]))
				$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
			//eval("$tremsConditions");
			eval("\$tremsConditions = \"$tremsConditions\";");
			$termsConditionStr =  stripslashes($tremsConditions);
		}
		if (defined('CONFIG_CONDITIONS')) { 
			$termsConditionStr = '';
		}  
	}else{
		$termsConditionStr = "I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.";
	//debug($termsConditionStr);
	} 
		  
}


$clientData = array(
				 array('name'=>'CLIENT NAME:','data'=>$transContent["cust_firstName"] . " " . $transContent["cust_lastName"])
				,array('name'=>'CLIENT NO.','data'=>$transContent["cust_accountName"])
				,array('name'=>'DemoFX DEALER','data'=>$transContent['accM_name'])
			);	
$transactionData = array(

					 array('name'=>'TRADE DATE:','data'=>dateFormat($transContent['tr_transDate'],2))
					,array('name'=>'MATURITY DATE:','data'=>dateFormat($transContent['tr_valueDate'],2))
					,array('name'=>'YOU BUY:','data'=>$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"])
					,array('name'=>'AT A RATE OF:','data'=>$transContent['tr_exchangeRate'])
					,array('name'=>'YOU SELL:','data'=>number_format($transContent["tr_transAmount"],2).' '.$transContent["tr_currencyFrom"])
					,array('name'=>'TT Fee:','data'=>'ZERO')
					,array('name'=>'REFERENCE:','data'=>$transContent['tr_refNumber'])
				);	
if(!empty($transContent['tr_currencyFrom'])){
	$paymentsData = array(
					 array('name'=>'ACCOUNT NAME:','data'=>$accountRS["accountName"])
					,array('name'=>'ACCOUNT NUMBER:','data'=>$accountRS["accounNumber"])
					,array('name'=>'BANK:','data'=>$accountRS["bankName"])
					,array('name'=>'IBAN:','data'=>$accountRS["IBAN"])
					,array('name'=>'SORT CODE:','data'=>$accountRS["sortCode"])
					,array('name'=>'Swift Code:','data'=>$accountRS["swiftCode"])
				);	
	foreach($trAccountrTrading as $k=>$v)
		$paymentsData[] = $v;
}
$new_line = "\n";
$signatureData = array(
						 array('name'=>$new_line.'SIGNED','data'=>"")
						,array('name'=>$new_line.'NAME (Print):','data'=>"")
						,array('name'=>$new_line.'DATED:','data'=>$new_line.date("d/m/y",strtotime($transContent["tr_transDate"])))
						,array('name'=>$new_line.'PREMIER FX SIGN OFF:','data'=>"")
					);
$accountsData = array(
					 array('name'=>'CLIENT NUMBER:','data'=>$transContent["cust_accountName"])
					,array('name'=>'AMOUNT OF CURRENCY TO BE TRANSFERRED:',
							'data'=>$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"])
					,array('name'=>'BENEFICIARY’S ACCOUNT NAME:','data'=>$transContent["bd_bankName"])
					,array('name'=>'BENEFICIARY’S IBAN / ACCOUNT NO: IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE','data'=>$ibanOrAcc)
					,array('name'=>'NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:',
							'data'=>$transContent["bd_branchAddress"])
					,array('name'=>'SWIFT/BIC CODE OF THE YOUR BANK:',
							'data'=>$transContent["bd_swiftCode"])
					,array('name'=>'CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)','data'=>$sortCodeV)
					,array('name'=>'WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:','data'=>$tipV)
			);

$confirmedData = array(
						 array('name'=>$new_line.'Your Name:','data'=>"")
						,array('name'=>$new_line.'Your Contact Number(s):','data'=>"")
						,array('name'=>$new_line.'Your Signature:','data'=>"")
						,array('name'=>$new_line.'DATED: DD/MM/YY','data'=>$new_line.date("d/m/y",time()))
					);
					
/*
$companyAddressData = array(
						 array('data'=>'<b>DemoFX</b>')
						,array('data'=>'<b>UK:</b> Third Floor, 114 Rochestor Row,</b>')
						,array('data'=>'London, SW1P 1JQ.')
						,array('data'=>'Tel: + 44 (0) 2071931910')
						,array('data'=>' ')
						,array('data'=>'<b>Portugal:</b> Rua Sacadura Cabral,')
						,array('data'=>'Edificio Golfe, lA,')
						,array('data'=>' 8135-144 Almancil, Algarve.')
						,array('data'=>'Tel: +351 289 358 511')
						,array('data'=>' ')
						,array('data'=>'<b>Spain:</b> C/Rambla dels Duvs, 13-1,')
						,array('data'=>'07003, Mallorca.')
						,array('data'=>'Tel: +34 971 576 724')
						,array('data'=>'Fax : +351 289 358 513')
						,array('data'=>'Email : info@hbstech.co.uk')
						,array('data'=>'Web : www.hbstech.co.uk')
					);
*/
$companyAddressData = array(
						 array('data'=>'<b>DemoFX</b>')
						,array('data'=>'<b>UK:</b> Third Floor, 114 Rochestor Row,</b>')
						,array('data'=>'London, SW1P 1JQ.')
						,array('data'=>'Tel: + 44 (0) 2071931910')
						,array('data'=>'Email : info@hbstech.co.uk')
						,array('data'=>'Web : www.hbstech.co.uk')
					);



/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/

	include ('../pdfClass/class.ezpdf.php');
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,750,200);
	
	$pdf->ezSetMargins(0,0,80,80);
	for($i=0;$i<=4;$i++)
		$pdf->ezText('',12);
	
	$pdf->ezText('',12);
	// generate client's table
	$pdf->ezText('<b><u>DEAL CONTRACT</u></b>',12,array("justification" => 'center'));
	$pdf->ezText('',12);
	$pdf->ezTable($clientData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>1,'width'=>400)
					);	
					
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	// generate transaction detail's table
	$pdf->ezText('<b>This Trade has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.</b>',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	$pdf->ezTable($transactionData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>1,'width'=>400)
					);
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	// generate payment account detail's table
	if(!empty($paymentsData)){
		$pdf->ezText('<b>Where to instruct your Bank to send payment to us:</b>',12,array("justification" => 'center'));	
		$pdf->ezText('',12);
		$pdf->ezTable($paymentsData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>1,'width'=>400)
					);
	}	
	for($i=0;$i<=1;$i++)
		$pdf->ezText('',12);
	// generate signature table
	$pdf->ezText('<b>I confirm that the details set out above are correct and that I will transmit the amount due to the account specified. I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a GBP 25.00 per day late payment fee.</b>',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	
	
	$pdf->ezTable($signatureData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>0,'width'=>500,'height'=>1000)
					);
	for($i=0;$i<=8;$i++)
		$pdf->ezText('',12);



	// generate account's table

	$pdf->ezText('<b><u>REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</u></b>',12,array("justification" => 'center'));
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	$pdf->ezText('<b>Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</b>',12,array("justification" => 'center'));
	$pdf->ezText('',12);
	$pdf->ezTable($accountsData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>1,'width'=>500,'cols'=>array(
	'name'=>array('justification'=>'left','width'=>250)
	,'data'=>array('width'=>250))
));
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	
	$pdf->ezText('<b>CUSTOMER DECLARATION AND DETAILS<b>',12,array("justification" => 'center'));	
	$pdf->ezText('',12);
	$pdf->ezText('I confirm that I will make arrangements to transfer the requisite amount of funds to pay for the currency ordered as per my verbal instruction given in relation to this trade number and that cleared funds will be credited to Premier FX’s Client Account one (1) working day before the maturity date of this trade. Should I envisage any problem in keeping to this agreement, I will contact my personal dealer immediately. In the event that he or she is unavailable, I will speak to another available dealer of Premier FX to ensure that the Company is made aware of the situation. I also confirm that I am aware that any changes to this trade may incur additional charges.',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	$pdf->ezTable($confirmedData,array('name'=>'value','data'=>'column'),'',array('showHeadings'=>0,'shaded'=>1,'showLines'=>0,'width'=>500));
	$pdf->ezText('',10);
	$pdf->ezText('',10);


$pdf->ezTable($companyAddressData,array('data'=>'column'),'',array('xPos'=>'right','showHeadings'=>0,'shaded'=>0,'showLines'=>0,'width'=>200, 'fontSize' => 8));

	$pdf->ezText('',10);
	$pdfcode = $pdf->output();
	//$pdf->ezStream();
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_DEAL_').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));

	//$args = array('agentID'=>$transContent['tr_custAgentID'],'customerID'=>$transContent['tr_custAgentID'],'customerParentID'=>$_REQUEST["customerParentID"]);
	//$sfsData = attachPDFWelcomeLetterEmail($args);
		
	$deal_contract_name = 'DEAL_CONTRACT.pdf';
	$deal_contract_path = $fPDFname;
	//$deal_contract_name = $sfsData["file_name"];
	//$deal_contract_path = $sfsData["file_path"];

/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  START EMAIL BODY ***************/
$timeStamp = time();
$num = md5( time() );
$messageT = "\n<br />Dear ". $transContent["cust_firstName"] .",\n<br /><br />Please find attached deal contract for trade done today.  Please check all details are correct and if possible please e-mail/fax back a SIGNED\ncopy to us.\nThank you very much for dealing with Premier FX. <br /><br />Regards,<br /> ";

if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1")
	$agentEmail = $transContent['accM_email'];

$messageT .= "\n ".$transContent["accM_name"]."\n";
$messageT.="<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>55 Old Broad Street,London, EC2M 1RF. \n<br><font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370   \n<br>\n<br>
						<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br> Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve. \n<br> <font color='#F7A30B'>Tel:</font> +351 289 358 511  \n<br>\n<br>
						<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br><font color='#F7A30B'>Tel:</font> +34 971 576 724 
						<font color='#F7A30B'>FAX:</font> +351 289 358 513 \n<br> \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font></td>
				</tr>
			</table>";
			
/***************  END EMAIL BODY ***************/



// Prepare Email for Sender and Account Manger

$subject = "Deal Contract";
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

$fromName  = SYSTEM;
$fromEmail = $fromEmail;

if(!empty($agentEmail)){
	$fromName  = $transContent["accM_name"];
	$fromEmail = $agentEmail;	
}
require_once("lib/phpmailer/class.phpmailer.php");
$sendermailer = new PHPMailer();
$agentmailer = new PHPMailer();

$sendermailer->FromName = $agentmailer->FromName = $fromName;
$sendermailer->From = $agentmailer->From = $fromEmail; 
$sendermailer->AddAddress($custEmail,'');
$agentmailer->AddAddress($agentEmail,'');
$agentmailer->Subject = $sendermailer->Subject = $subject;
$sendermailer->IsHTML(true);
$agentmailer->IsHTML(true);

// attachements
if($fpPDF){
	$sendermailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
	$agentmailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
}
else{
	$attachementInfo = '<br/>Deal Contract could not be attached with email';
}

// Body
$agentmailer->Body = $sendermailer->Body = $messageT;


	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		if($custEmail)
			$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
		if($accMgrEmail)
			$accMgrEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
	}
	else{
	if($custEmail)
		$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
	if($agentEmail)
		$agentEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
	}

$msg = "Email is not sent to customer and introducer (may not exist or email functionality is disabled)";



if ($custEmailFlag || $agentEmailFlag || $accMgrEmailFlag) {
	if($custEmailFlag && $agentEmailFlag){
		$msg = "Email has been sent to customer and introducer.";
	}
	else if($custEmailFlag && $accMgrEmailFlag){
		$msg = "Email has been sent to customer and Account Manager.";
	}
	else{
		if($custEmailFlag)
			$msg = "Email has been sent to customer.";
		elseif($agentEmailFlag)
			$msg = "Email has been sent to introducer.";
	}
	$msg.=$attachementInfo."<br/>";
}
?>

<div class='noPrint' align="center">
		<?=$msg?>&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?

if(sizeof($transContent) < 1){
	?>
	<div class='noPrint' align="center">
	No Record found
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?
	}

?>

<?php

	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$nowTime = date("F j, Y");
	$transID = $_REQUEST["transid"];
	//debug($transID);
	$today = date("d-m-Y");
	$todayD= date("Y-m-d");
	$emailFlag = false;
	$mailto = $_REQUEST["emailto"];
	//debug ($_REQUEST, true);
	 /*
	 * 7897 - Premier FX
	 * Rename 'Create Transaction' text in the system.
	 * Enable  : string
	 * Disable : "0"
	*/
	
	$strTransLabel = 'Transaction';
	
	if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
		$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
td.myFormat
{
font-size:10px;	
}

td.Arial
{
font-size:10px;
font-family:"Gill Sans MT",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
background:#C0C0C0;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
background:#C0C0C0;
}
td.bottom
{
border-bottom: solid 1px #000000;  
font-size:10px;
}
td.bottomborder{
border-bottom: solid 1px #000000;
}
td.leftborder{
border-left: solid 1px #000000;
}

td.sign
{
font-size:10px;
}
td.style2
{
font-size:10px;
}
td.terms
{
font-size:12px;
font-family: Gill Sans MT;
}
</style>
</head>
</html>
<?php
$message="";
$nTransID = $_REQUEST['transid'];	


$nTransID=explode(",",$nTransID);


if(is_array($nTransID) && $nTransID!="")
{
	$countTrans = count($nTransID);
}
else
{
	$countTrans = 1;
}
//debug($nTransID);
for($t=0;$t<$countTrans;$t++)
{


	$strTransactions = "SELECT 
							tr.transID as transID,
							tr.custAgentID as tr_custAgentID,
							tr.benID as tr_benID,
							tr.customerID as tr_customerID,
							tr.addedBy as tr_addedBy,
							tr.currencyFrom as tr_currencyFrom,
							tr.transDate as tr_transDate,
							tr.valueDate as tr_valueDate,
							tr.localAmount as tr_localAmount,
							tr.currencyTo as tr_currencyTo,
							tr.transAmount as tr_transAmount,
							tr.exchangeRate as tr_exchangeRate,
							tr.refNumber as tr_refNumber,
							
							cust.customerID as cust_customerID,
							cust.email as cust_email,
							cust.parentID as cust_accManagerID,
							cust.firstName as cust_firstName,
							cust.lastName as cust_lastName,
							cust.accountName as cust_accountName,
							
							ag.email as ag_email,
							ag.name as ag_name,
							
							accM.email as accM_email,
							accM.name as accM_name,
							
							bd.bankName as bd_bankName,
							bd.accNo as bd_accNo,
							bd.IBAN as bd_IBAN,
							bd.sortCode as bd_sortCode,
							bd.routingNumber as bd_routingNumber,
							bd.branchAddress as bd_branchAddress,
							bd.swiftCode as bd_swiftCode
							
						FROM
							".TBL_TRANSACTIONS." as tr 
							LEFT JOIN ".TBL_CUSTOMER." as cust ON tr.customerID = cust.customerID  
							LEFT JOIN ".TBL_ADMIN_USERS." as ag ON tr.custAgentID = ag.userID 
							LEFT JOIN ".TBL_ADMIN_USERS." as accM ON cust.parentID = accM.userID 
							
							LEFT JOIN ".TBL_BANK_DETAILS." as bd ON tr.transID = bd.transID 
						WHERE 
							tr.transID = '".$nTransID[$t]."' 
						";
	$transContent = selectFrom($strTransactions);	
	//debug($strTransactions,true);
	$agentEmail = $transContent["ag_email"];

	$emailFlag = true;

	$customerID=$transContent['tr_customerID'];
	$custEmail = $transContent["cust_email"];

	if(!empty($mailto))
		$custEmail = $mailto;
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		$agentEmail = $accMgrEmail = $transContent["accM_email"];	
		$emailFlag = true;
	}			
	
	// accounts details against speficif sending currency
	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$transContent['tr_currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
	$trAccountrTrading = array();
	for($i=0;$i<count($fieldsCheckedArr);$i++)
	{
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr))
		{
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else
		{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		
		$trAccountrTrading[] = array(array('name'=>strtoupper($fieldsLabel),'type'=>$accountRS[$fieldsValue]));
	}
  	//$bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$nTransID[$t]."'");	
  
	$ibanOrAcc = '';
	$sortCodeV = '';
	$tipV = '';
	if($transContent["bd_IBAN"]!='' && $transContent["bd_accNo"]!=''){			
		$ibanOrAcc = "IBAN: ".$transContent["bd_IBAN"]." / ". "Account No. ".$transContent["bd_accNo"];
	}
	elseif($transContent["bd_IBAN"]!=''){
		$ibanOrAcc = "IBAN: ".$transContent["bd_IBAN"];
	}
	elseif($transContent["bd_accNo"]!=''){
		$ibanOrAcc = "Account No. ".$transContent["bd_accNo"];
	}	
	if($transContent["bd_sortCode"]!="" &&  $transContent["bd_routingNumber"]!="") {
		$sortCodeV = $transContent["bd_sortCode"]." / ".$transContent["bd_routingNumber"];			
	}
	elseif($transContent["bd_sortCode"]=""){
		$sortCodeV = $transContent["bd_routingNumber"];			
	}
	elseif($transContent["bd_routingNumber"]=""){
		$sortCodeV = $transContent["bd_sortCode"];
	}
	if(CONFIG_HIDE_TIP_FIELD != "1"){
		$tipV = $transContent["tr_tip"];
	}	
	else{
		$tipV = "BARC GB 22";
	}
	
	$termsConditionStr = '';
	if(CONFIG_TRANS_CONDITION_ENABLED == '1')
	{
		if(defined("CONFIG_TRANS_COND"))
		{
			$termsConditionStr =  (CONFIG_TRANS_COND);
		}
		else
		{
			$termsConditionSql = selectFrom("SELECT company_terms_conditions FROM company_detail
													WHERE company_terms_conditions!='' 
													AND dated = (SELECT MAX(dated) FROM company_detail WHERE company_terms_conditions!='')");
			if(!empty($termsConditionSql["company_terms_conditions"]))
				$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
			//eval("$tremsConditions");
			eval("\$tremsConditions = \"$tremsConditions\";");
			$termsConditionStr =  stripslashes($tremsConditions);
		}
		if (defined('CONFIG_CONDITIONS')) { 
			$termsConditionStr = '';
		}  
	}else{
		$termsConditionStr = "I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.";
	//debug($termsConditionStr);
	} 
		  
}


$clientData = '
	<table border="1" style="padding:2px;" width="400" >
		<tr>
			<td>CLIENT NAME:</td>
			<td>'.$transContent['cust_firstName']. " " . $transContent['cust_lastName'].'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>CLIENT NO.</td>
			<td>'.$transContent['cust_accountName'].'</td>
		</tr>
		<tr>
			<td>DEMOFX DEALER</td>
			<td>'.$transContent['accM_name'].'</td>
		</tr>
	</table>';
	
$transactionData = '
		<table border="1" style="padding:2px;" width="400" >
			<tr>
				<td>TRADE DATE:</td>
				<td>'.dateFormat($transContent["tr_transDate"],2).'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>MATURITY DATE:</td>
				<td>'.dateFormat($transContent["tr_valueDate"],2).'</td>
			</tr>
			<tr>
				<td>YOU BUY:</td>
				<td>'.$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"].'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>AT A RATE OF:</td>
				<td>'.$transContent["tr_exchangeRate"].'</td>
			</tr>
			<tr>
				<td>YOU SELL:</td>
				<td>'.number_format($transContent["tr_transAmount"],2).' '.$transContent["tr_currencyFrom"].'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>TT Fee:</td>
				<td>ZERO</td>
			</tr>
			<tr>
				<td>REFERENCE:</td>
				<td>'.$transContent['tr_refNumber'].'</td>
			</tr>
		</table>';
	
if(!empty($transContent['tr_currencyFrom'])){
	$paymentsData = '
		<table border="1" style="padding:2px" width="400">
			<tr>
				<td>ACCOUNT NAME:</td>
				<td>'.$accountRS["accountName"].'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>ACCOUNT NUMBER:</td>
				<td>'.$accountRS["accounNumber"].'</td>
			</tr>
			<tr>
				<td>BANK</td>
				<td>'.$accountRS["bankName"].'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>IBAN:</td>
				<td>'.$accountRS["IBAN"].'</td>
			</tr>
			<tr style="background-color:#ccc;">
				<td>SORT CODE:</td>
				<td>'.$accountRS["sortCode"].'</td>
			</tr>
			<tr>
				<td>Swift Code:</td>
				<td>'.$accountRS["swiftCode"].'</td>
			</tr>
		</table>
	';
	foreach($trAccountrTrading as $k=>$v)
		$paymentsData[] = $v;
}
$signatureData = '
	<table border="1" width="400" style="padding:2px">
		<tr>
			<td>NAME (Print):</td>
			<td>&nbsp;</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>DATED:</td>
			<td>'.date("d/m/y",strtotime($transContent["tr_transDate"])).'</td>
		</tr>
		<tr>
			<td>PREMIER FX SIGN OFF:</td>
			<td>&nbsp;</td>
		</tr>
	</table>
';
if($transContent["bd_bankName"] != ''){
	$accountsData = '
	<table border="1" width="100%" style="padding:2px;">
		<tr>
			<td>CLIENT NUMBER:</td>
			<td>'.$transContent["cust_accountName"].'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
			<td>'.$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"].'</td>
		</tr>
		<tr>
			<td>BENEFICIARY’S ACCOUNT NAME:</td>
			<td>'.$transContent["bd_bankName"].'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>BENEFICIARY’S IBAN / ACCOUNT NO: IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE</td>
			<td>'.$ibanOrAcc.'</td>
		</tr>
		<tr>
			<td>NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
			<td>'.$transContent["bd_branchAddress"].'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>SWIFT/BIC CODE OF THE YOUR BANK:</td>
			<td>'.$transContent["bd_swiftCode"].'</td>
		</tr>
		<tr>
			<td>CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)</td>
			<td>'.$sortCodeV.'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
			<td>'.$tipV.'</td>
		</tr>
	</table>
';
}
else{
	$accountsData = '
	<table border="1" width="100%" style="padding:2px;">
		<tr>
			<td>CLIENT NUMBER:</td>
			<td>'.$transContent["cust_accountName"].'</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
			<td>'.$transContent["tr_localAmount"].' '.$transContent["tr_currencyTo"].'</td>
		</tr>
	</table>';
}

$confirmedData = '
	<table width="100%" cellpadding="4" style="padding:2px;">
		<tr >
			<td >Your Name:</td>
			<td>&nbsp;</td>
		</tr>
		<tr style="background-color:#ccc;height:100px">
			<td>Your Contact Number(s):</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Your Signature:</td>
			<td>&nbsp;</td>
		</tr>
		<tr style="background-color:#ccc;">
			<td>DATED: DD/MM/YY</td>
			<td>'.date("d/m/y",time()).'</td>
		</tr>
	</table>
';
$companyAddress = '
	<table align="left" width="150" style="padding:2px;" cellpadding="0" cellspacing="0">
		<tr>
			<th align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Premier FX</b></th>
		</tr>
		<tr >
			<td align="left"><b>UK:</b> 55 Old Broad Street,</td>
		</tr>
		<tr>
			<td align="left">London, EC2M 1RF.</td>
		</tr>
		<tr >
			<td align="left">Tel: +44 (0)845 021 2370<br /></td>
		</tr>
		<tr>
			<td align="left"><b>Portugal:</b> Rua Sacadura Cabral,</td>
		</tr>
		<tr >
			<td align="left">Edificio Golfe, lA,</td>
		</tr>
		<tr>
			<td align="left">8135-144 Almancil, Algarve.</td>
		</tr>
		<tr >
			<td align="left">Tel: +351 289 358 511<br /></td>
		</tr>
		<tr>
			<td align="left"><b>Spain:</b> C/Rambla dels Duvs,</td>
		</tr>
		<tr >
			<td align="left">13-1, 07003, Mallorca.</td>
		</tr>
		<tr>
			<td align="left">Tel: +34 971 576 724</td>
		</tr>
		<tr >
			<td align="left">Fax : +351 289 358 513</td>
		</tr>
		<tr>
			<td align="left">Email : info@premfx.com</td>
		</tr>
		<tr >
			<td align="left">Web : www.premfx.com</td>
		</tr>
	</table>
	';


/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/

	require_once('lib/tcpdf/config/lang/eng.php');
	require_once('lib/tcpdf/tcpdf.php');
	
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator('Mirza Arslan Baig');
	
	$pdf->SetAuthor('Mirza Arslan Baig');
	$pdf->SetTitle('Premier Exchange Deal Contract');
	$pdf->SetSubject('Deal Contract');
	
	$pdf->setPrintHeader(false);
	
		

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 8);

	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	//set some language-dependent strings
	$pdf->setLanguageArray($l);


	$pdf->setFormDefaultProp(array('lineWidth'=>0, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));


	// add a page
	$pdf->AddPage();

	// set JPEG quality
	$pdf->setJPEGQuality(75);


	$x = 6;
	$y = 6;
	$w = 0;
	$h = 0;


	$pdf->Image('lib/tcpdf/images/logo.jpg', $x, $y, $w, $h, 'JPG', '', '', false, 300, '', false, false, 0, '', false, false);

	
	
	// IMPORTANT: disable font subsetting to allow users editing the document
	$pdf->setFontSubsetting(false);
	
	// set font
	$pdf->SetFont('helvetica', '', 10, '', false);	
	
	// set default form properties
	//$pdf->setFormDefaultProp(array('lineWidth'=>0, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

	$pdf->SetFont('helvetica', 'B', 14);
	$pdf->Ln(10);
	$pdf->Ln(8);
	$pdf->writeHTML('<b><u>DEAL CONTRACT</u></b>',true,false,false,false,'C');
	$pdf->Ln(6);
	$pdf->SetFont('helvetica', '', 12);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $clientData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(6);
	
	$html = "<b>This Trade has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.</b>";
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(6);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $transactionData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(6);
	
	if(!empty($paymentsData)){
		$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>Where to instruct your Bank to send payment to us:</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
		$pdf->Ln(6);
		
		$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $paymentsData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	}
	
	$pdf->SetFontSize(10, true);
	$pdf->Ln(8);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>I confirm that the details set out above are correct and that I will transmit the amount due to the account specified. I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a GBP 25.00 per day late payment fee.</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	
	$pdf->Ln(10);
	$pdf->Ln(10);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', 'SIGNATURE', $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
	$pdf->Ln(8);
	$pdf->Ln(10);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $signatureData, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(10);
	$pdf->Ln(10);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b><u>REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</u></b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(10);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(8);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $accountsData, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(8);
	
	if($transContent["bd_bankName"] == ''){
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,'5', $txt = 'BENEFICIARY’S ACCOUNT NAME:', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		
		$pdf->TextField('BenAccountName', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(8);
		$pdf->Cell(120,$h = 5, $txt = 'BENEFICIARY’S IBAN / ACCOUNT NO: ', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		$pdf->TextField('accountNumber', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = '(IF THE PAYMENT IS IN EUROS, A FULL IBAN IS ', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		$pdf->Ln(4);
		$pdf->Cell(120,$h = 4, $txt = 'REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE)', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'NAME AND ADDRESS OF THE BANK ', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->TextField('address', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = 'YOUR CURRENCY IS BEING TRANSFERRED TO:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'SWIFT/BIC CODE OF THE YOUR BANK:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'C');
		$pdf->TextField('swiftCode', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(8);
		$pdf->Cell(120,$h = 5, $txt = 'CLEARING CODE OF THE BANK:', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		$pdf->TextField('clearingCode', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		
		$pdf->Ln(4);
		$pdf->SetFontSize(8, true);
		$pdf->Cell(120,$h = 5, $txt = '(I.E. SORT CODE/ABA ROUTING NUMBER) ', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->Ln(8);
		$pdf->SetFontSize(10, true);
		$pdf->Cell(120,$h = 5, $txt = 'WHAT REFERENCE, IF ANY,', $border = 0, $ln = 0, $align = 'L', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->TextField('reference', 40, 5, array('borderStyle' => 'none','lineWidth' => 0));
		$pdf->Ln(4);
		$pdf->Cell(120,$h = 4, $txt = 'DO YOU WANT TO QUOTE ON YOUR TRANSFER: ', $border = 0, $ln = 0, $align = 'L', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'C');
		$pdf->Ln(6);
		
		
		$pdf->Cell(60,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		$pdf->Button('print', 15, 7, 'Print', 'Print()', array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(204, 204, 204), 'strokeColor'=>array(255, 255, 255)));
		
		$pdf->Cell(20,$h = 5, '', $border = 0, $ln = 0, $align = '', false, '', $stretch = 0, $ignore_min_height = true, $calign = 'T', $valign = 'T');
		
		// Reset Button
		$pdf->Button('reset', 15, 7, 'Reset', array('S'=>'ResetForm'), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(204, 204, 204), 'strokeColor'=>array(255, 255, 255)));

		// Submit Button
		//$pdf->Button('submit', 30, 10, 'Submit', array('S'=>'SubmitForm', 'F'=>'http://localhost/test/pdf_test/tcpdf/printvars.php', 'Flags'=>array('ExportFormat')), array('lineWidth'=>2, 'borderStyle'=>'beveled', 'fillColor'=>array(128, 196, 255), 'strokeColor'=>array(64, 64, 64)));
	}
	$pdf->Ln(12);
	$pdf->SetFontSize(10, true);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<b>CUSTOMER DECLARATION AND DETAILS</b>', $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
	$pdf->Ln(4);
	
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', '<div style="text-align:justify">I confirm that I will make arrangements to transfer the requisite amount of funds to pay for the currency ordered as per my verbal instruction given in relation to this trade number and that cleared funds will be credited to Premier FX’s Client Account one (1) working day before the maturity date of this trade. Should I envisage any problem in keeping to this agreement, I will contact my personal dealer immediately. In the event that he or she is unavailable, I will speak to another available dealer of Premier FX to ensure that the Company is made aware of the situation. I also confirm that I am aware that any changes to this trade may incur additional charges.</div>', $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
	$pdf->Ln(2);
	$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $confirmedData, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
	$pdf->Ln(6);
	
	$pdf->SetFontSize(8, true);
	$pdf->Cell(130,$h = 4, ' ', $border = 0, $ln = 0, $align = 'L', $fill = false);
	$pdf->writeHTML($companyAddress, $ln=1, $fill=0, $reseth=true,$cell=false, $align='');
	
	$js = <<<EOD
function CheckField(name,message) {
	var f = getField(name);
	if(f.value == '') {
		app.alert(message);
		f.setFocus();
		return false;
	}
	return true;
}
function Print() {
	if(!CheckField('BenAccountName','Beneficiary Name is mandatory')) {return;}
	print();
}
EOD;

// Add Javascript code
$pdf->IncludeJS($js);

	$pdfcode = $pdf->Output('DEAL_CONTRACT.pdf', 'S');
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_DEAL_').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);

	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));
	
		
	$deal_contract_name = 'DEAL_CONTRACT.pdf';
	$deal_contract_path = $fPDFname;

/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  START EMAIL BODY ***************/
$timeStamp = time();
$num = md5( time() );
$messageT = "\n<br />Dear ". $transContent["cust_firstName"] .",\n<br /><br />Please find attached deal contract for trade done today.  Please check all details are correct and if possible please e-mail/fax back a SIGNED\ncopy to us.\nThank you very much for dealing with DemoFX. <br /><br />Regards,<br /> ";

if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1")
	$agentEmail = $transContent['accM_email'];

$messageT .= "\n ".$transContent["accM_name"]."\n";
/*
$messageT.="<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>55 Old Broad Street,London, EC2M 1RF. \n<br><font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370   \n<br>\n<br>
						<font color='#F7A30B'> PORTUGAL OFFICE</font> \n<br> Rua Sacadura Cabral, Edificio Golfe lA, 8135-144 Almancil, Algarve. \n<br> <font color='#F7A30B'>Tel:</font> +351 289 358 511  \n<br>\n<br>
						<font size='2px'><font color='#F7A30B'>SPAIN OFFICE</font>\n<br>C/Rambla dels Duvs, 13-1, 07003, Mallorca. \n<br><font color='#F7A30B'>Tel:</font> +34 971 576 724 
						<font color='#F7A30B'>FAX:</font> +351 289 358 513 \n<br> \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font></td>
				</tr>
			</table>";
*/
$messageT.="<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'>
						<font size='2px'><font color='#F7A30B'>UK OFFICE</font>\n<br>Third Floor, 114 Rochestor Row, London SW1P 1JQ. \n<br><font color='#F7A30B'>Tel:</font> +44 (0)845 021 2370   \n<br>\n<br>
						<font color='#F7A30B'>FAX:</font> +351 289 358 513 \n<br> \n <br> <font color='#F7A30B'>Email:</font> info@hbstech.co.uk | www.hbstech.co.uk</font></td>
				</tr>
			</table>";


			
/***************  END EMAIL BODY ***************/



// Prepare Email for Sender and Account Manger

$subject = "Deal Contract";
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

$fromName  = SYSTEM;
$fromEmail = $fromEmail;

if(!empty($agentEmail)){
	$fromName  = $transContent["accM_name"];
	$fromEmail = $agentEmail;	
}
require_once("lib/phpmailer/class.phpmailer.php");
$sendermailer = new PHPMailer();
$agentmailer = new PHPMailer();

$sendermailer->FromName = $agentmailer->FromName = $fromName;
$sendermailer->From = $agentmailer->From = $fromEmail; 
$sendermailer->AddAddress($custEmail,'');
$agentmailer->AddAddress($agentEmail,'');
$agentmailer->Subject = $sendermailer->Subject = $subject;
$sendermailer->IsHTML(true);
$agentmailer->IsHTML(true);

// attachements
if($fpPDF){
	$sendermailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
	$agentmailer->AddAttachment($deal_contract_path, $deal_contract_name,"base64");
}
else{
	$attachementInfo = '<br/>Deal Contract could not be attached with email';
}

// Body
$agentmailer->Body = $sendermailer->Body = $messageT;


	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		if($custEmail)
			$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
		if($accMgrEmail)
			$accMgrEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
	}
	else{
	if($custEmail)
		$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
	if($agentEmail)
		$agentEmailFlag = $agentmailer->Send();//@mail($agentEmail, $subject, $messageT, $headers); 	
	}

$msg = "Email is not sent to customer and introducer (may not exist or email functionality is disabled)";



if ($custEmailFlag || $agentEmailFlag || $accMgrEmailFlag) {
	if($custEmailFlag && $agentEmailFlag){
		$msg = "Email has been sent to customer and introducer.";
	}
	else if($custEmailFlag && $accMgrEmailFlag){
		$msg = "Email has been sent to customer and Account Manager.";
	}
	else{
		if($custEmailFlag)
			$msg = "Email has been sent to customer.";
		elseif($agentEmailFlag)
			$msg = "Email has been sent to introducer.";
	}
	$msg.=$attachementInfo."<br/>";
}
?>

<div class='noPrint' align="center">
		<?=$msg?>&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?

if(sizeof($transContent) < 1){
	?>
	<div class='noPrint' align="center">
	No Record found
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?
	}

?>
