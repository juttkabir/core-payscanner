<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("ledgersUpdation.php");
include("connectOtherDataBase.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$clientCompany = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'
$isAgentAnD = 'N';
$isBankAnD = 'N';

$errMsg = "";
/*@5394-now transfer - currency routine by Niaz Ahmad*/
$root_path = $_SERVER['DOCUMENT_ROOT'];
if(CONFIG_DISTRIBUTION_DEAL_SHEET == "1")
	include("$root_path/deal_sheet/server.php");
	
/* If Transaction is to distribut to third part 
if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("LOCAL_PATH"))
	require LOCAL_PATH."api/etransact/soap_api.php";
*/

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";
$moneyPaid   = "";

if ($_POST["moneyPaid"] != "") {
	$moneyPaid = $_POST["moneyPaid"];
} else if ($_GET["moneyPaid"] != "") {
	$moneyPaid = $_GET["moneyPaid"];
}
if($_POST["transType"]!="")
		$transType = $_POST["transType"];
	elseif($_GET["transType"]!="") 
		$transType=$_GET["transType"];
			
if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];
		
		
if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
{
	if($_POST["transSource"]!="")
	{
		$transSource = $_POST["transSource"];
	
	}
	else if($_GET["transSource"]!="")
	{
		$transSource = $_GET["transSource"];
	
	}
}		
		
if ($_POST["nameType"] != "") {
	$nameType = $_POST["nameType"];
} else if ($_GET["nT"] != "") {
	$nameType = $_GET["nT"];
}
if($_POST["searchBy"]!="")
		$by = $_POST["searchBy"];
	elseif($_GET["searchBy"]!="") 
		$by = $_GET["searchBy"];
		
if($_POST["agentID"]!="")
		$agentName = $_POST["agentID"];
	elseif($_GET["agentID"]!="") 
		$agentName = $_GET["agentID"];		

$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$trnsid = $_POST["trnsid"];
$ttrans = $_POST["totTrans"];



$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0")
{
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}
/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
 	/**
	 *  @var $extraCondition type string
	 *  This varibale is used to define a condition 
	 */
	$extraCondition  = " and ( am.userID = '". $_SESSION["loggedUserData"]["userID"] ."' ) ";
}
 
if($_POST["btnAction"] != "")
{

	for ($i=0;$i< $ttrans;$i++)
	{
	 
		if(count($trnsid) >= 1)
		{
			if($trnsid[$i] != "")
			{
			
			if($_POST["btnAction"] != "")
			{
				 
				 /************************************/
				 
				/*if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT != '1')
				{
					$contentTrans = selectFrom("select benAgentID, refNumber from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
					$refData = generateDistRef($trnsid[$i]);
					
					
					$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used))");
				
					$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
					if($refNumRange["id"] != '')
					{
						$value = $refNumRange["rangeFrom"] + $refNumRange["used"] + 1;
					
						/**
						* if CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO, include the 
						* distributor IDs than include '-' (DASH) within its
						* distributor refrence number.
						* @Ticket #4108
						*/
						/*if(defined("CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO"))
						{
							if(strpos(CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO,$contentTrans["benAgentID"]) !== false)
								$refNum = $refNumRange["prefix"]."-".$value;
							else
								$refNum = $refNumRange["prefix"].$value;	
						}
						else
							$refNum = $refNumRange["prefix"].$value;*/
						
						/*$strManualReferenceNumber = $refNum; 
						
						
					}
					if(empty($strManualReferenceNumber))
						$strManualReferenceNumber = $contentTrans["refNumber"];
				}*/

				 
				 /************************************/
				 
						 
				 
				 /************************************/
				 
				if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT != '1')
				{
					$contentTrans = selectFrom("select benAgentID, refNumber from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
					$refData = generateDistRef($trnsid[$i]);
					$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used))");
				
					$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
					if($refNumRange["id"] != '')
					{
						$value = $refNumRange["rangeFrom"] + $refNumRange["used"] + 1;
					
						/**
						* if CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO, include the 
						* distributor IDs than include '-' (DASH) within its
						* distributor refrence number.
						* @Ticket #4108
						*/
						if(defined("CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO"))
						{
							if(strpos(CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO,$contentTrans["benAgentID"]) !== false)
								$refNum = $refNumRange["prefix"]."-".$value;
							else
								$refNum = $refNumRange["prefix"].$value;	
						}
						else
							$refNum = $refNumRange["prefix"].$value;
						
						$strManualReferenceNumber = $refNum;
						
					}
					if(empty($strManualReferenceNumber))
						$strManualReferenceNumber = $contentTrans["refNumber"];
				}

				 
				 /************************************/
				 
				 
				 
				 
				 $contentTrans = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$trnsid[$i]."'");
				
				 if(CONFIG_EXCLUDE_COMMISSION == '1')
					{
						$agentLedgerAmount = $contentTrans["totalAmount"] - $contentTrans["AgentComm"];
						
					}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
						$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
					$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
							if($agentPaymentMode == 'exclude')
							{
									$agentLedgerAmount = $contentTrans["totalAmount"] - $contentTrans["AgentComm"];
							
							}elseif($agentPaymentMode == 'include')
							{
								$agentLedgerAmount = $contentTrans["totalAmount"];
								
							}
	
	

	}else{
						$agentLedgerAmount = $contentTrans["totalAmount"];
					}
					
			
					
				 
				 $custID 	= $contentTrans["customerID"];
				 $benID 		= $contentTrans["benID"];
				 $custAgentID = $contentTrans["custAgentID"];
				 $benAgentID = $contentTrans["benAgentID"];
				 $imReferenceNumber = $contentTrans["refNumberIM"];
				 $manualRefrenceNumber = $contentTrans["refNumber"];
				 $collectionPointID = $contentTrans["collectionPointID"];
				 $type  = $contentTrans["transType"];
				 $transDate   = $contentTrans["transDate"];
				 $transDate = date("F j, Y", strtotime("$transDate"));
				  
				 $strAuthoriseDate = date("F j, Y", strtotime($contentTrans["authoriseDate"]));
				 
				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
				$adminEmail = $adminContents["email"];
				// Getting customer's Agent Email
				if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
				$custAgentEmail	= $agentContents["email"];
				}
				// getting BEneficiray agent Email
				if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
				{
				$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
				$benAgentEmail	= $agentContents["email"];
				}
				// getting beneficiary email
				$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
				$benEmail = $benContents["email"];
				// Getting Customer Email
				$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
				$custEmail = $custContents["email"];

if($_POST["btnAction"] == "Authorize")
{

if($contentTrans["createdBy"] == 'CUSTOMER')
{
			$subject = "Status Updated";

			$custContents = selectFrom("select * from cm_customer where c_id= '". $custID ."'");
			$custEmail = $custContents["c_name"];
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["FirstName"];
			$custMiddleName = $custContents["MiddleName"];
			$custLastName = $custContents["LastName"];
			$custCountry = $custContents["c_country"];
			$custCity = $custContents["c_city"];
			$custZip = $custContents["c_zip"];
			$custLoginName = $custContents["username"];
			$custEmail = $custContents["c_email"];
			$custPhone = $custContents["c_phone"];


			$benContents = selectFrom("select * from cm_beneficiary where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction is updated </p></td>
  </tr>
			
			
					  <tr>
						<td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
					  </tr>				
			
								  <tr>
						<td width='205'>&nbsp;</td>
						<td width='295'>&nbsp;</td>
					  </tr>
							  <tr>
						<td colspan='2'><p>Thank you for choosing $clientCompany as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
					  </tr>			
			
			
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Transaction Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
						
			
					  <tr>
						<td> Transaction Type:  ".$type." </td>
						<td> Status: $Status </td>
					  </tr>
					  <tr>
						<td> Transaction No:   $imReferenceNumber </td>
						<td> Transaction Date:  ".$transDate."  </td>
					  </tr>
					  <tr>
						<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
						<td> &nbsp;</td>
					  </tr>
					  <tr>
						<td><p>Authorised Date: ".date("F j, Y")."</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					
					
					  <tr>
						<td><p><strong>Beneficiary Detail: </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td colspan='2'>-----------------------------------------------------------------------------------</td>
					  </tr>
					  <tr>
						<td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Address:  $benAddress </td>
						<td> Postal / Zip Code:  $benZip </td>
					  </tr>
					  <tr>
						<td> Country:   $benCountry   </td>
						<td> Phone:   $benPhone   </td>
					  </tr>
					  <tr>
						<td><p>Email:  $benEmail   </p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>";			
			if(trim($type) == 'Bank Transfer')
			{
				$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benContents["Country"]."'");

				$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
				$rstRow = mysql_fetch_array($nResult);


				if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
				{
					$message .= "	<tr>
										<td>IBAN</td>
										<td>" . $rstRow["IBAN"] . "</td>
									</tr>";
				}
				else
				{
					$_SESSION["bankName"] = $rstRow["bankName"];
					$_SESSION["branchCode"] = $rstRow["branchCode"];
					$_SESSION["branchAddress"] = $rstRow["branchAddress"];
					$_SESSION["swiftCode"] = $rstRow["swiftCode"];
					$_SESSION["accNo"] = $rstRow["accNo"];
					$_SESSION["branchCity"] = $rstRow["BranchCity"];
					
					$message.="	<tr>
									<td><p><strong>Beneficiary Bank Details </strong></p></td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td> Bank Name:  ".$_SESSION['bankName']."  </td>
									<td> Acc Number:  ".$_SESSION['accNo']."  </td>
								</tr>
								<tr>
									<td> Branch Code:  ".$_SESSION['branchCode']."  </td>
									<td> Branch Address:  ".$_SESSION['branchAddress'].", ".$_SESSION['branchCity']."  </td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>";
					$_SESSION["bankName"] = "";
					$_SESSION["branchCode"] = "";
					$_SESSION["branchAddress"] = "";
					$_SESSION["swiftCode"] = "";
					$_SESSION["accNo"] = "";
				}
			}
			elseif(trim($type) == "Pick up")
			{
			
			$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
			$agentname = $cContents["cp_corresspondent_name"];
			$contactperson = $cContents["cp_corresspondent_name"];
			$company = $cContents["cp_branch_name"];
			$address = $cContents["cp_branch_address"];
			$country = $cContents["cp_country"];
			$city = $cContents["cp_city"];
			$phone = $cContents["cp_phone"];
			$tran_date = date("Y-m-d");
			$tran_date = date("F j, Y", strtotime("$tran_date"));
			
					$message .= "
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td><p><strong>Collection Point Details </strong></p></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td> Agent Name : $agentname </td>
						<td> Contact Person:  $contactperson </td>
					  </tr>
					  <tr>
						<td> Company:  $company </td>
						<td> Address:  $address </td>
					  </tr>
					  <tr>
						<td>Country:   $country</td>
						<td>City:  $city</td>
					  </tr>
					  <tr>
						<td>Phone:  $phone</td>
						<td>&nbsp;</td>
					  </tr> ";
}
$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
//$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used
$exRate 		= $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}

/*if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}*/


if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}
	
/*if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount  = '$totalamount',
												 localAmount = '$localamount',
												 exchangeRate  = '$exRate'
													 where
													 transID  = '". $trnsid[$i] ."'");
}*/

$message .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
";

}
else
{

			$subject = "Status Updated";

			$custContents = selectFrom("select * from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
			$custTitle = $custContents["Title"];
			$custFirstName = $custContents["firstName"];
			$custMiddleName = $custContents["middleName"];
			$custLastName = $custContents["lastName"];
			$custCountry = $custContents["Country"];
			$custCity = $custContents["City"];
			$custZip = $custContents["Zip"];
			$custEmail = $custContents["email"];
			$custPhone = $custContents["Phone"];
			$custStatus = $custContents["customerStatus"];


			$benContents = selectFrom("select * from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
			$benTitle = $benContents["Title"];
			$benFirstName = $benContents["firstName"];
			$benMiddleName = $benContents["middleName"];
			$benLastName = $benContents["lastName"];
			$benAddress  = $benContents["Address"];
			$benCountry = $benContents["Country"];
			$benCity = $benContents["City"];
			$benZip = $benContents["Zip"];
			$benLoginName = $benContents["username"];
			$benEmail = $benContents["email"];
			$benPhone = $benContents["Phone"];


			$AdmindContents = selectFrom("select * from admin where userID = '". $benAgentID ."'");
			$AdmindLoginName = $AdmindContents["username"];
			$AdmindName = $AdmindContents["name"];
			$AdmindEmail = $AdmindContents["email"];




			$Status = $_POST["btnAction"];

/***********************************************************/
$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'><p><strong>Subject</strong>: Your transaction status is updated </p></td>
  </tr>";
 $messageNew =" 
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $custTitle&nbsp;$custFirstName&nbsp;$custLastName  </p></td>
  </tr>";
  $messageRemain="
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>Thank you for choosing $clientCompany as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Transaction Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>

  <tr>
    <td> Transaction Type:  ".$type." </td>
    <td> Status: $Status </td>
  </tr>
  <tr>
    <td> Transaction No:   $imReferenceNumber </td>
    <td> Transaction Date:  ".$transDate."  </td>
  </tr>
    <tr>
    <td> Manual Reference No:   ".$strManualReferenceNumber ."</td> 
    <td> &nbsp; </td>
  </tr>
  <tr>
    <td><p>Authorised Date: ".date("F j, Y")."</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td><p><strong>Beneficiary Detail: </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td><p>Beneficiary Name:  $benTitle&nbsp;$benFirstName&nbsp;$benLastName</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Address:  $benAddress </td>
    <td> Postal / Zip Code:  $benZip </td>
  </tr>
  <tr>
    <td> Country:   $benCountry   </td>
    <td> Phone:   $benPhone   </td>
  </tr>
  <tr>
    <td><p>Email:  $benEmail   </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";

if(trim($type) == 'Bank Transfer')
{
	$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benCountry."'");

	$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	$rstRow = mysql_fetch_array($nResult);

	if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
	{
		$messageRemain .= "	<tr>
							<td>IBAN</td>
							<td>" . $rstRow["IBAN"] . "</td>
						</tr>";
	}
	else
	{
		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];

		$messageRemain.="  <tr>
					<td><p><strong>Beneficiary Bank Details </strong></p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td> Bank Name:  ".$_SESSION['bankName']."  </td>
					<td> Acc Number:  ".$_SESSION['accNo']."  </td>
				  </tr>
				  <tr>
					<td> Branch Code:  ".$_SESSION['branchCode']."  </td>
					<td> Branch Address:  ".$_SESSION['branchAddress']."  </td>
				  </tr>
				  <tr>
					<td><p>Swift Code:  ".$_SESSION['swiftCode']."  </p></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				";
		$_SESSION["bankName"] = "";
		$_SESSION["branchCode"] = "";
		$_SESSION["branchAddress"] = "";
		$_SESSION["swiftCode"] = "";
		$_SESSION["accNo"] = "";
	}
}
elseif(trim($type) == "Pick up")
{

$cContents = selectFrom("select * from cm_collection_point where cp_id  = '". $collectionPointID ."'");
$agentname = $cContents["cp_corresspondent_name"];
$contactperson = $cContents["cp_corresspondent_name"];
$company = $cContents["cp_branch_name"];
$address = $cContents["cp_branch_address"];
$country = $cContents["cp_country"];
$city = $cContents["cp_city"];
$phone = $cContents["cp_phone"];
$tran_date = date("Y-m-d");
$tran_date = date("F j, Y", strtotime("$tran_date"));

$messageRemain .= "
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Collection Point Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Agent Name : $agentname </td>
    <td> Contact Person:  $contactperson </td>
  </tr>
  <tr>
    <td> Company:  $company </td>
    <td> Address:  $address </td>
  </tr>
  <tr>
    <td>Country:   $country</td>
    <td>City:  $city</td>
  </tr>
  <tr>
    <td>Phone:  $phone</td>
    <td>&nbsp;</td>
  </tr> ";

}

$exchangerate = getExchangeRate($custCountry, $benCountry);

$exID 			= $exchangerate[0];
//$exRate 		= $exchangerate[1];
$currencyFrom 	= $exchangerate[2];
$currencyTo 	= $exchangerate[3];

///Exchange rate used 
$exRate = $contentTrans["exchangeRate"];

$amount = $contentTrans["transAmount"];
$fee = $contentTrans["IMFee"];
$localamount =($amount * $exRate);
$purpose = $contentTrans["transactionPurpose"];
$totalamount =  $amount + $fee;
$moneypaid = $contentTrans["moneyPaid"];
if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
{
	$cashCharges = $contentTrans["cashCharges"];
	$totalamount =  $totalamount +$cashCharges;
	}


/*if($amount > 500)
{
	$tempAmount = ($amount - 500);
	$nExtraCharges =  (($tempAmount * 0.50)/100);
	$charges = ($fee + $amount + $nExtraCharges);
}*/


//Distributor Commition logic

$benAgentID = $contentTrans["benAgentID"];

$packageQuery="select * from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
			$agentPackage = selectFrom($packageQuery);

			$package = $agentPackage["commPackage"];
			
			//
			switch ($package)
				  {
				  	case "001": // Fixed amount per transaction
					{
						$agentCommi = $agentPackage["agentCommission"];
						$commType = "Fixed Amount";
						break;
					}
				  	case "002": // percentage of total transaction amount
					{
						$agentCommi = ( $amount* $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of total Amount";
						break;
					}
				  	case "003": // percentage of IMFEE
					{
						$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
						$commType = "Percentage of Fee";
						break;
					}
					case "004":
					{
						$agentCommi = 0;
						$commType = "None";
						break;
					}
				}				  
				   
			//
		 $agentCommi;


// end




/*if($trnsid[$i] != "")
{
 update("update transactions set IMFee = '$fee',
												 totalAmount 	 = '$totalamount',
												 localAmount 	 = '$localamount',
												 exchangeRate 	 = '$exRate',
												 distributorComm = '$agentCommi'
													 where
													 transID  = '". $trnsid[$i] ."'");
}*/
$messageRemain .="
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Amount Details </strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'>-----------------------------------------------------------------------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>";
  
  $messageEnd="
  <tr>
    <td> Exchange Rate:  ".$exRate."</td>
    <td> Amount:  ".$amount." </td>
  </tr>
  <tr>
    <td> ".$systemPre." Fee:  ".$fee." </td>
    <td> Local Amount:  ".$localamount." </td>
  </tr>  <tr>
    <td> Transaction Purpose:  ".$purpose." </td>
    <td> Total Amount:  ".$totalamount." </td>
  </tr>  <tr>
    <td> Money Paid:  ".$moneypaid." </td>
    <td> Bank Charges:  ".$nExtraCharges." </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
    
  </tr>
</table>
";

}
/* #4990 Point-2
    Email Condition.
	If anyone of these configs is ON then Mail will be sent from transStatusChange function of ledgersUpdation.php
	As this file function is also called.
*/
$sendMailFrmStChangFunc = true;
if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER!="1"){
	$sendMailFrmStChangFunc = false;
}elseif(CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER!="1"){
	$sendMailFrmStChangFunc = false;
}
	if(!$sendMailFrmStChangFunc){
		// Added by Niaz Ahmad at 22-05-2009 @4979
		if(CONFIG_CUSTOMER_EMAIL_ADD_TRANS_DISABLED != "1"){
			 sendMail($custEmail, $subject, $message.$messageNew.$messageRemain.$messageEnd, $fromName, $fromEmail);			
		}
		if(CONFIG_BEN_EMAIL_CP == "1"){
				$messageNew =" 
			  <tr>
				<td colspan='2'><p><strong>Dear</strong>  $benTitle&nbsp;$benFirstName&nbsp;$benLastName  </p></td>
			  </tr>";
			  $messageEnd="
			  
			  <tr>
				<td> Money Paid:  ".$moneypaid."  </td>
				<td> Local Amount:  ".$localamount." </td>
			  </tr>  
			  <tr>
				<td> Transaction Purpose:  ".$purpose."</td>
				<td> &nbsp; </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			<tr>
				<td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
				
			  </tr>
			</table>
			";
				sendMail($benEmail, $subject, $message.$messageNew.$messageRemain.$messageEnd, $fromName, $fromEmail);	
		}
	}
}
/**********************************************************/
				
			}



			if($_POST["btnAction"] == "Authorize")
			{
													
										/********
										#2622 Generating distributor reference number from the convention  prefix + lower range where 
										lower range is increamented every time a new transaction for that distributor is authorized. 
										Once from the range 10 values are left the distributor/system will be mailed a message that he 
										needs to add new range.   
										*********/
										
										$flag = true;
										if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT != '1')
    								{
    								//$refData = generateDistRef($contentTrans["transID"]);
    									

    									
    									$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used))");

											$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);
											//debug($refNumRange);
											if($refNumRange["id"] != '')
											{
												/**
												 * Commented the code as bug found @Ticket #4112
												 * $value = $refNumRange["rangeFrom"] + $refNumRange["used"];
												 */
												$value = $refNumRange["used"];
												
											
												/**
												 * if CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO, include the 
												 * distributor IDs than include '-' (DASH) within its
												 * distributor refrence number.
												 * @Ticket #4108
												 */
												if(defined("CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO"))
												{
													if(strpos(CONFIG_INCLUDE_DASH_IN_DISTRIBUTOR_REF_NO,$contentTrans["benAgentID"]) !== false)
														$refNum = $refNumRange["prefix"]."-".$value;
													else
														$refNum = $refNumRange["prefix"].$value;	
												}
												else
													$refNum = $refNumRange["prefix"].$value;
												
												$used = $refNumRange["used"];
												$used++;
												
												/**
												 * Commented the code as bug found @Ticket #4112
												 * update("update ".TBL_RECEIPT_RANGE." set used = '".$used."' where agentID ='".$contentTrans["benAgentID"]."' and id ='".$refNumRange["id"]."' ");
												 */
								
															
												
													IF(CONFIG_DIST_MANUAL_REF_NUMBER == "1"){
														
																update("update ".TBL_TRANSACTIONS." set refNumber = '".$refNum."' where transID ='".$contentTrans["transID"]."'");
														
														}else{
														
																update("update ".TBL_TRANSACTIONS." set distRefNumber = '".$refNum."' where transID ='".$contentTrans["transID"]."'");
											}
												
												if($limitVal <= 10){
													$cond = selectFrom(" SELECT id from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and created > (SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$contentTrans["benAgentID"]."' and rangeTo >= (rangeFrom + used)) ");
													if($cond["id"] == ''){
														
														include ("distEmail.php");
													}
												}
											
											}else{
												
												$flag = false;
												$errMsg = "Transaction cannot be authorized. Add a new distributor reference number range. ";
											}
											
											if(CONFIG_DIST_MANUAL_REF_NUMBER != "1"){

    									$flag = $refData[0];
										}
      				
      						}
										
										if($flag){
						
											// Update agents account limit;
										//	update("update " . TBL_ADMIN_USERS . " set limitUsed  = (limitUsed  + '$agentLedgerAmount') where userID = '". $contentTrans["custAgentID"] ."'");
											update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $trnsid[$i] ."'");				
											
											if(CONFIG_DISTRIBUTION_DEAL_SHEET == "1")
										{
											$action = 'Authorize';
											$transID = $trnsid[$i];
											$inputArg = array("action" =>$action,"transID"=>$transID);
											//debug($inputArg);
											$response = transAuthorize($inputArg);
										 }			
											/*$today = date("Y-m-d");;  
											$insertQuery = "insert into bank_account values('','".$contentTrans["benAgentID"]."', '$today', 'WITHDRAW', '".$contentTrans["totalAmount"]."', '$username', '". $trnsid[$i] ."','Transaction Authorized')";
											$q=mysql_query($insertQuery);*/
											
											$today = date("Y-m-d");  
										
											//authorizeTrans($trnsid[$i], $username);
										
											//insertError("Transaction Authorized successfully.");
							
											//$descript = "Transaction is Authorized";
											//activities($_SESSION["loginHistoryID"],"UPDATION",$trnsid[$i],TBL_TRANSACTIONS,$descript);		 
											//Query for CashTrans job queue 
											
											transStatusChange($trnsid[$i], "Authorize", $username, '');
											
											/*
											if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("LOCAL_PATH"))
												sendApiTransaction($trnsid[$i]);
											*/
										
											
											$Query="INSERT INTO `uc_job_queue` 
											( `id` , `action` , `ident` , `status` , `error_code` , `error_text` , `timestamp` ) 
											VALUES (NULL , 'new_transaction ', $trnsid[$i], 'new', '', '', NOW( ) )";
											mysql_query($Query);
											
											if(CONFIG_SHARE_OTHER_NETWORK == '1' && CONFIG_REMOTE_LEDGER_AT_CREATE != '1')
											{
												
												
												$benAgentInfo = selectFrom("select fromServer from " . TBL_ADMIN_USERS . " where userID = '".$contentTrans["benAgentID"]."'");
												if($benAgentInfo["fromServer"] != '')
												{
													
													$jointClientComm = selectFrom("select * from ".TBL_JOINTCLIENT." where clientName = '".$benAgentInfo["fromServer"]."' and isEnabled = 'Y'");
													
													/*$checkSharedAgent = selectfrom("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$contentTrans["custAgentID"]."' and availableAs like 'Agent'");
													echo("select id, availableAt, availableAs from ".TBL_SHAREDUSERS." where availableAt = '".$jointClientComm["clientId"]."' and localServerId  = '".$contentTrans["custAgentID"]."' and availableAs like 'Agent'");													
													if($checkSharedAgent["id"] != '')
													{*/		
																$sharedTransComm = $contentTrans["IMFee"] - $contentTrans["AgentComm"];
															if(CONFIG_SHARE_FROM_TOTAL == '1')
															{													
																$clientComm = (($contentTrans["IMFee"] * ($jointClientComm["commissionShare"])) / 100);
															}else{
																$clientComm = (($sharedTransComm * ($jointClientComm["commissionShare"])) / 100);
																}
															$clientTotalAmount = $contentTrans["transAmount"] + $clientComm;
															$transactionDescription = "Transaction Authorized";
															$transID = $trnsid[$i];
															include("createOtherNetworkTrans.php");		
															dbConnect();
													/*}else{
														$errMsg = "Agent of Transaction ".$contentTrans["refNumberIM"]." is not available at ".$jointClientComm["clientName"]."";
														}*/
													

												}
													
											}
										}
			}
		}

		}
	}
//redirect("authorize-transactions.php?msg=Y&action=". $_GET["action"]);
}
//debug($_REQUEST);

$query = "select t.benID, transID , transDate , refNumber, refNumberIM, custAgentID, totalAmount, transType, transAmount, localAmount,transStatus,t.customerID,collectionPointID,createdBy,clientRef,senderBank,trans_source ".$custExtraFields." from ". TBL_TRANSACTIONS. " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
			$extraJoin."  
			WHERE 1 ".$extraCondition;

$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c 
             ON t.customerID = c.customerID ".
				$extraJoin."   
				WHERE 1 ".$extraCondition;


if($Submit == "Search")
{
	if($transID != "")
	{
		switch($by)
		{
			case 0:
			{		
				$query = "select t.transID,t.transType,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source ".$custExtraFields." from ". TBL_TRANSACTIONS. " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
			$extraJoin."  
			WHERE 1 ".$extraCondition;

				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c 
                             ON t.customerID = c.customerID ".
				$extraJoin."   
				WHERE 1 ".$extraCondition;
				
				$query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				$queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
				break;
			}
			case 1:
			{	
				$query = "select benID,transID,transDate,t.transType,refNumber,refNumberIM,custAgentID,totalAmount,transType,transAmount,localAmount,transStatus,t.customerID,collectionPointID,createdBy,t.clientRef,t.senderBank,t.trans_source ".$custExtraFields." from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where t.createdBy != 'CUSTOMER' ".$extraCondition;
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where t.createdBy != 'CUSTOMER' ".$extraCondition;
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

		
		
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$transID);
										$nameClause = "c.firstName LIKE '$name[0]' and c.lastName LIKE  '$name[1]' ";
									    $nameClauseOn = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
								    $nameClause = " c.".$nameType." LIKE '".$transID."%'";
									$nameClauseOn = " ".$nameType." LIKE '".$transID."%'";
									
									}
					
					
								$query .= " and ($nameClause) ";
								$queryCnt .= " and ($nameClause) ";
								
								$queryonline .= " and $nameClauseOn ";
								$queryonlineCnt .= " and $nameClauseOn ";
				
				
					}else{
		    	// searchNameMultiTables function start (Niaz)
       $fn="FirstName";
			 $mn="MiddleName";
			 $ln="LastName"; 		
			 $alis="cm";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
			  $queryonline .= $q;
				$queryonlineCnt .= $q;
				//$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
				//$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
					
				if($agentType == "Branch Manager"){
				$queryonline .= " and (t.custAgentParentID ='$agentID') ";
				$queryonlineCnt .= " and (t.custAgentParentID ='$agentID') ";
				}			
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
				
				$query .= $q;
				$queryCnt .= $q;
			}				
				//$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				//$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
				break;
			}
			case 2:
			{
				$query = "select t.transID,t.custAgentID,t.transType,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source ".$custExtraFields." from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
					$extraJoin."  
					where t.createdBy != 'CUSTOMER' ".$extraCondition;

				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
					$extraJoin." where t.createdBy != 'CUSTOMER' ".$extraCondition;
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy = 'CUSTOMER' ";

				
				
					if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$transID);
										$nameClause   = " b.firstName LIKE '$name[0]' and b.lastName LIKE  '$name[1]' ";
										$nameClauseOn = " firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
									$nameClause = " b.".$nameType." LIKE '".$transID."%'";
									$nameClauseOn = " ".$nameType." LIKE '".$transID."%'";
									
									}
					
					
								$query .= " and ($nameClause) ";
								$queryCnt .= " and ($nameClause) ";
								
								$queryonline .= " and $nameClauseOn ";
								$queryonlineCnt .= " and $nameClauseOn ";
				
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
				
				$queryonline .= $q;		
				$queryonlineCnt .=$q;
								
				//$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";		
				//$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
				
				if($agentType == "Branch Manager"){
				$queryonline .= " and (t.custAgentParentID ='$userID') ";
				$queryonlineCnt .= " and (t.custAgentParentID ='$userID') ";
				}
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
			 
				$query .= $q;
				$queryCnt .=$q;
			}
				//$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				//$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
				break;
			}
			case 3:
			{

				$query = "select t.transID,t.custAgentID,t.refNumber,t.transType,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source ".$custExtraFields." 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER'  ".$extraCondition; 

				$queryCnt = "select count(transID) from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER'  ".$extraCondition; 
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as ca where t.custAgentID = ca.userID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as ca where t.custAgentID = ca.userID and t.createdBy = 'CUSTOMER' ";

				
				$queryonline .= " and (ca.name like '".$transID."%')";		
				$queryonlineCnt .= " and (ca.name like '".$transID."%')";
				
				if($agentType == "Branch Manager"){
				$query .= " and (a.parentID ='$userID') ";
				$queryCnt .= " and (a.parentID ='$userID') ";
				}
				
				$query .= " and (a.name like '".$transID."%')";
				$queryCnt .=" and (a.name like '".$transID."%')";
				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["transID"]."' OR t.refNumber = '".$_POST["transID"]."' OR  t.refNumberIM = '".$_POST["transID"]."') ";
	}elseif($agentName != "")
			{
		$query = "select t.transID,t.transType,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID,t.clientRef,t.senderBank,t.createdBy,t.trans_source ".$custExtraFields." 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER'  ".$extraCondition; 


						$queryCnt = "select count(transID) 
						from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
						$extraJoin."  
						where t.createdBy != 'CUSTOMER'  ".$extraCondition; 
						if($agentName != "all")
						{
							$query .= " and (a.userID ='$agentName') ";
							$queryCnt .= " and (a.userID ='$agentName') ";
						}
					
						if($agentType == "Branch Manager")
						{
							$query .= " and (a.parentID ='$userID') ";
							$queryCnt .= " and (a.parentID ='$userID') ";
						}
				
			}
			
			
}

	if($transType != "")
	{
		$query .= " and (t.transType='".$transType."')";
		$queryCnt .= " and (t.transType='".$transType."')";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType."')";
		  $queryonlineCnt .= " and (t.transType='".$transType."')";
			}
		
	}
	
	if ($moneyPaid != "") {
		$query .= " and (t.moneyPaid='".$moneyPaid."')";
		$queryCnt .= " and (t.moneyPaid='".$moneyPaid."')";
		
		$queryonline .= " and (t.moneyPaid='".$moneyPaid."')";
		$queryonlineCnt .= " and (t.moneyPaid='".$moneyPaid."')";
	}


if($_GET["action"] == "authorize")
{
	/*
	if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
	{
		$query .= " and (t.transStatus = 'Processing') ";
		$queryCnt .= " and (t.transStatus = 'Processing') ";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transStatus='Processing')";
		  $queryonlineCnt .= " and (t.transStatus='Processing')";
			}
	}
	else
	{	
	*/
	$flagPending = true;
	if(defined("CONFIG_VERIFY_TRANSACTION_ENABLED")){
	if(CONFIG_VERIFY_TRANSACTION_ENABLED)
	{
		$query .= " and (t.transStatus = 'Processing') ";
		$queryCnt .= " and (t.transStatus = 'Processing') ";
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transStatus='Processing')";
		  $queryonlineCnt .= " and (t.transStatus='Processing')";
			}
		$flagPending = false;	
	}
	}
	
	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($transSource!= "")
		{
			if($transSource == "O")
			{
				$query.= " and (t.trans_source = 'O') ";
				$queryCnt.= " and (t.trans_source = 'O') ";
			}
			else if ($transSource == "P")
			{
				$query.= " and (t.trans_source != 'O') ";
				$queryCnt.= " and (t.trans_source != 'O') ";
			}
		}
	}
	
	if($flagPending){	
		$query .= " and (t.transStatus ='Pending') ";
		$queryCnt .= " and (t.transStatus ='Pending') ";
		
		if($transID != "" && $by != 0)
		{
			$queryonline .= " and (t.transStatus='Pending')";
		  $queryonlineCnt .= " and (t.transStatus='Pending')";
			}
	}
	//}
	if(CONFIG_CHEQUE_BASED_TRANSACTIONS == "1" || CONFIG_MULTI_LINKS_AT_MAIN_MENU == "1"){
		if(CONFIG_SHOW_TRANS_MONEY_PAID_BY_CHQ != "1")
		{
			$query .= " and (t.moneyPaid != 'By Cheque') and t.chequeNo = '' ";
			$queryCnt .= " and (t.moneyPaid != 'By Cheque') and t.chequeNo = '' ";
		}
	
	}
}

switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	if($transID != "")
		{	
		$queryonline .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	}
}

			if($agentType == "Branch Manager"){
				$query .= " and (t.custAgentParentID ='$userID') ";
				$queryCnt .= " and (t.custAgentParentID ='$userID') ";
				}
	if(CONFIG_USE_TRANSACTION_DETAILS == "1"){
		$query .= " and (transDetails !='No')";
		$queryCnt .= " and (transDetails !='No')";
	}
	if(CONFIG_VALUE_DATE_TRANSACTIONS == "1"){
		$queryCnt .= "  AND (valueDate != '0000-00-00 00:00:00' AND valueDate != '')  ";
		$query .= "  AND (valueDate != '0000-00-00 00:00:00' AND valueDate != '') ";	
	}
  $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $query .= " LIMIT $offset , $limit";
 

 if($_POST["Submit"] == "" || ($_POST["Submit"] != "" && $_POST["searchBy"] != "" && $_POST["transID"] != "") || $agentName != ""){
  	$contentsTrans = selectMultiRecords($query);
   
 print_r($query);
  $allCount = countRecords($queryCnt);


		
		if($transID != "" && $by != 0)
		{	
			
			
			$onlinecustomerCount = count(selectMultiRecords($queryonlineCnt ));
				
			$allCount = $allCount + $onlinecustomerCount;
			
			
			
					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
			    $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
	}
 
		
?>
<html>
<head>
	<title>Authorize Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--

function checkBoxCheck(field)
{
alert("Contact MLRO to authorize this transaction");

	document.getElementById(field).checked=false;
}


function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>


    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<script type="text/javascript" src="wz_tooltip.js"></script>	
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?> 
      Transactions</font></strong></td>
  </tr>
  <? if ($errMsg != "") {  ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo CAUTION_COLOR; ?>"><b><i><? echo CAUTION_MARK;?></i></b></font></td>
	<td width="635"><? echo "<font color='" . CAUTION_COLOR . "'><b>".$errMsg."</b><br></font>"; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? }?>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="authorize-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="transID" type="text" id="transID" value="<?=$transID?>">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
		<select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=__("Sender");?> Name</option>
			<option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
			<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Name</option>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
        <? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
		<br>
		<br>
		
		Search By Agent &nbsp; &nbsp;
		<select name="agentID" style="font-family:verdana; font-size: 11px">
                <option value="">- Select Agent-</option>
				<option value="all">All Agents</option>
                <?
//work by Mudassar Ticket #11425
				if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
                $agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' and agentStatus = 'Active' ";
					}
				else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
				$agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
				}
//work end by Mudassar Ticket #11425	
					
								if($agentType == "Branch Manager" || $agentType == "SUPA" || $agentType == "SUPAI")
								{
									$agentQuery .= " and parentID = '$userID'";				
									
								}
								$agentQuery .= " order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
								
								
					for ($i=0; $i < count($agents); $i++){
				?>
                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                <?
					}
				?>
             </select>
			 
			 <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$agentName?>");
                                </script>
<?php  //debug ($agentQuery); ?>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Mode -</option>
          <option value="By Cash" <? echo ($moneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
          <option value="By Cheque" <? echo ($moneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
          <option value="By Bank Transfer" <? echo ($moneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
		  </select>
		<?	}  ?>
		
	</td>
</tr>
		<? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
   		
  	</td></tr>
  	<? } ?>
		<tr>
			<td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="authorize-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
		 
			if ($allCount > 0){
				
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
              <td><span class="style1">Transaction Source</span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1"><?=__("Sender");?> Name </span></td>

			  <?
			  	if(CONFIG_SENDER_BANK_DETAILS == "1")
				{
			?>
					<td align="center" class="style1"><?=__("Sender");?> Bank</td>
				    <td><span class="style1">Branch Code</span></td>
				    <td><span class="style1">Branch Address</span></td>			 
			<?
				}
			  ?>

			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <? 
			  if(CONFIG_PICKUP_INFO == '1') 
			  { ?>
			  		<td width="20%"><span class="style1">Pick up Center - City </span></td>
		    <? } ?>
			<?
					if(CONFIG_REMARKS_ENABLED == "1")
					{
			?>
						<td width="74" align="center" class="style1"><?=CONFIG_REMARKS?></td>
			<?
					}
				?>
			  <td width="146" align="center"><? if ($_GET["action"] != "") { ?><input name="All" type="checkbox" id="All" onClick="CheckAll();"><? } ?></td>
		    <td width="74"><span class="style1">Enquiry </span></td>
			</tr>
		    <? 
			//debug($contentsTrans);
			for($i=0;$i<count($contentsTrans);$i++)
			{
				
				
				$fontColor = "";
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
																			
									}
									if($fontColor == "")
									{
										$fontColor = "#006699";
									}
									
									
						$toolText = ""; 
					  if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
					  {
					  		
							$rateLimitQuery = "select id, minRateAlert, maxRateAlert, isAlertProcessed from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentsTrans[$i]["transID"]."' and isAlertProcessed = 'N'";
							$rateLimitContent = selectFrom($rateLimitQuery);
							if($rateLimitContent["minRateAlert"] > 0 || $rateLimitContent["maxRateAlert"] > 0)
							{
								$toolText = "Waiting till Exchange Rate Min = ".$rateLimitContent["minRateAlert"]." and Max = ".$rateLimitContent["maxRateAlert"];
								$fontColor = "#CC0000";
							}
						}			
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" <? if($toolText != ""){ ?>onmouseover="Tip('<?=$toolText?>')"; onMouseOut="UnTip()";<? } ?> onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="<? echo $fontColor ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
                  <td width="75" bgcolor="#FFFFFF"><?php if($contentsTrans[$i]["trans_source"] == 'O'){ echo 'Online';}else{ echo 'Payex';}?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]) .  " " . getCurrencySign($contentsTrans[$i]["currencyFrom"])?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>

					<?
						if(CONFIG_SENDER_BANK_DETAILS == "1")
						{
							if(!empty($contentsTrans[$i]["senderBank"]))
							{
								$senderBank = selectFrom("select name,branchCode,branchAddress from ".TBL_BANKS." where bankId='".$contentsTrans[$i]["senderBank"]."'");
								
					?>
								<td bgcolor="#FFFFFF"><?=$senderBank["name"]?></td>
								<!--Edited code for adding new fields Branch Address and Branch Code -->
								
								<td bgcolor="#FFFFFF"><?=$senderBank["branchCode"]?></td>
								<td bgcolor="#FFFFFF"><?=$senderBank["branchAddress"]?></td>
							
								<!--End of Above Edited Code -->
					<?
							} else {
					?>
								<td bgcolor="#FFFFFF" colspan="3">&nbsp;</td>
					<?
							}
						}
					?>

				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName, customerStatus from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>

					<?
					
					
						if(CONFIG_SENDER_BANK_DETAILS == "1")
						{
							if(!empty($contentsTrans[$i]["senderBank"]))
							{
								$senderBank = selectFrom("select name,branchCode,branchAddress from ".TBL_BANKS." where bankId='".$contentsTrans[$i]["senderBank"]."'");
					?>
								<td bgcolor="#FFFFFF"><?=$senderBank["name"]?></td>
								<!--Edited code for adding new fields Branch Address and Branch Code -->
								
								<td bgcolor="#FFFFFF"><?=$senderBank["branchCode"]?></td>
								<td bgcolor="#FFFFFF"><?=$senderBank["branchAddress"]?></td>
							
								<!--End of Above Edited Code -->
					<?
							} else {
					?>
								<td bgcolor="#FFFFFF" colspan="3">&nbsp;</td>
					<?
							}
						}
					?>

				 		<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<? } ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
					  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
					  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else
				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
			    <? 
			    if(CONFIG_PICKUP_INFO == '1') 
			  	{
			  	 if($contentsTrans[$i]["transType"] == "Pick up")
			  	 {
			  	   $pickUpInfo = selectFrom("select cp_branch_name, cp_city from ".TBL_COLLECTION." where cp_id = '".$contentsTrans[$i]["collectionPointID"]."'");
			  	?>
			  	   <td width="100" bgcolor="#FFFFFF"><? echo $pickUpInfo["cp_branch_name"]. " - " .$pickUpInfo["cp_city"]?></td>
			  <? }else 
	         {?>
	          <td width="100" bgcolor="#FFFFFF" width="100">&nbsp; </td>
	        <? }
			   } ?>
			   
			  		 <?
						if(CONFIG_REMARKS_ENABLED == "1")
						{
					?>
							<td align="center" bgcolor="#FFFFFF"><?=$contentsTrans[$i]["clientRef"]?></td>
					<?
						}
					?>
					<?
					if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER =="1"){
											if ($customerContent['customerStatus']== 'Disable'){?>
											
									  <td align="center" bgcolor="#FFFFFF">  <? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId' id='$tempTransId' onClick=checkBoxCheck($tempTransId);>";?><? } ?></td>
									<? }elseif ($customerContent['customerStatus']== 'Enable' || $customerContent['customerStatus']==""){?>
											
										<td align="center" bgcolor="#FFFFFF">   <? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] id='$tempTransId' value='$tempTransId' >";?><? } ?></td>
									<? } 
					}else{?>
											<td align="center" bgcolor="#FFFFFF">  <? if ($_GET["action"] != "" && $_GET["action"] != "Recall") {  $tempTransId = $contentsTrans[$i]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId' id='$tempTransId' >";?><? } ?></td>
									
					<? } ?>
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>&authTrans=Y" class="style2">Enquiry</a></td>
					  
				</tr>
				<?
			}
	 	for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$j]["totalAmount"]) .  " " . getCurrencySign($onlinecustomer[$j]["currencyFrom"])?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");  
				   		debug($customerContent);
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
          <? 
			    if(CONFIG_PICKUP_INFO == '1') 
			  	{
			  		if( $onlinecustomer[$j]["transType"] == "Pick up")
			  		{
			  	  $pickUpInfo = selectFrom("select cp_branch_name, cp_city from ".TBL_COLLECTION." where cp_id = '".$onlinecustomer[$j]["collectionPointID"]."'");
			  	?>
			  	  <td width="100" bgcolor="#FFFFFF"><? echo $pickUpInfo["cp_branch_name"]. " - " .$pickUpInfo["cp_city"]?></td>
	          <?}else 
	          {?>
	          <td width="100" bgcolor="#FFFFFF">&nbsp; </td>
	        <?}
	      	}?>

				  <td align="center" bgcolor="#FFFFFF"><? if ($_GET["action"] != "") {  $tempTransId = $onlinecustomer[$j]["transID"]; echo "<input type=checkbox name=trnsid[] value='$tempTransId'>";?><? } ?></td>
				   <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$j]["transID"]?>&authTrans=Y" class="style2">Enquiry</a></td>
					  
				</tr>
				<?
			}
			?>
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center"><input type='hidden' name='totTrans' value='<? echo ($i + $j); ?>'>
			    <? if ($_GET["action"] != "") { ?><input name="btnAction" type="submit"  value="<? echo ucfirst($_GET["action"])?>"><? } ?></td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			</tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&Submit=$Submit&searchBy=$by&transID=$transID&transSource=$transSource&moneyPaid=$moneyPaid&agentID=$agentName&action=".$_GET["action"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.Or provide proper search fiters to search.
            </td>
          </tr>
          <?
			}
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>