<?php

	/**
	 *  @author     Aman Mukhtar
	 *  @copyright  Copyright (c) Horivert Business Solutions Pvt. Ltd. 
     *  Short Description
	 *	File used to enable the import functionality of MT103 in premiere fx
	 */
	 
	session_start();
	include ("../include/config.php");
	include ("security.php");
	
	/**
	 *	@var $agentType type string
	 *  This variable is used to store the agent type of the user 
	 */
	$agentType    = getAgentType();
	$arrOutputLog = array();

	//ignore $strTransactionStatusForMT103NewTransfers = "NewMT103";

    /**
	 * This condition checks whether any file is imported or not 
	 */
	if( !empty( $_FILES['uploadFile']['tmp_name'] ) )
	{
		$timeStamp = time();
	    /**
	     *  @var $tmpfname type string 
	     *  This varibale is used to store the path of the temp file created
	     */
		$tmpfname = tempnam("/tmp", "IMPORTMT103_".$timeStamp."_");
		/**
	     *  @var $fileName type string 
	     *  This varibale is used to store name of the file imported
	     */
		$fileName = $_FILES['uploadFile']['name'];
		/**
	     *  @var $tempFileName type string 
	     *  This varibale is used to store name of the temp file
	     */
		$tempFileName = $_FILES['uploadFile']['tmp_name'];
		/**
	     *  @var $fileSize type int 
	     *  This varibale is used to store the size of the file imported
	     */
		$fileSize = $_FILES['uploadFile']['size'];
		/**
	     *  @var $fileType type string 
	     *  This varibale is used to store the type of the file imported
	     */
		$fileType = $_FILES['uploadFile']['type'];
		/**
	     *  @var $fileSizeAllowed type int 
	     *  This varibale is used to store the max file size allowed 
	     */
		$fileSizeAllowed  = 102450;
		
		/**
	     *  @var $agentID type int 
	     *  This varibale is used to store the id of agent selected 
	     */
		$agentID = $_POST['agentID'];
		/**
	     *  @var $ext type string 
	     *  This varibale is used to store the extension of the 
		 *  imported file 
	     */
		$ext = strtolower(strrchr($fileName,"."));
	    /**
	     *  @var $strFileName type string 
	     *  This varibale is used to store name of the file imported 
	     */
		$strFileName = $fileName;
	
		/**
		 *  This condition checks that only text file is imported
		 */ 
		if ($fileType == 'text/plain')
		{
			if (is_uploaded_file($tempFileName))
			{
				move_uploaded_file($tempFileName, $tmpfname );
				/**
				 *  @var $file type file
				 *  This variable opens the file in reading mode
				 */
				$file 		 = fopen($tmpfname, "r") or exit("Unable to open file!");;
	
				$dataFields  = array();
				$arrFileRows = array();
				
				/**
				 *  @var $strFullFileData type string
				 *  This variable stores the content of the file imported
				 */
				$strFullFileData = "";
				$refHandle 		 = fopen($tmpfname, "rb");
				$strFullFileData = stream_get_contents($refHandle);
				fclose($refHandle);
	
			    /**
				 *  @var $strTransactionsFoundRegx type string
				 *  This variable stores the pattern which is to be 
				 *  searched in the file
				 */
				$strTransactionsFoundRegx = "/Text(.*)=(.*)Block 5:/isU"; 
				preg_match_all($strTransactionsFoundRegx, $strFullFileData, $arrFoundTransactions);
				
				$arrAllTransactionsFormated = array();
				$arrTransactionsParams      = array();
				$strGetTransParamRegx 		= "/:([A-Z0-9]{2,3}):(.*)\\n:/isU";
				foreach($arrFoundTransactions[2] as $strOneFoundTransaction)
				{
				    $strOneFoundTransaction = trim($strOneFoundTransaction);
					/**
					 * Matching the pattern
					 */
					preg_match_all("/(.*)\\n(.*)/isU", $strOneFoundTransaction, $arrParegMatches); 
					
					$arrNewFormatedMT103Transactions = array();
					/**
					 *  @var $intLineNumber type int   
					 *  This varibale stores the line number
					 */
					$intLineNumber = 0;
					
					foreach($arrParegMatches[1] as $strOneLineOfMTFormat)
					{
						/**
						 * Condition to compare/match the patterns
						 */
						if(!preg_match("/:([A-Z0-9]{2,3}):/isU", $strOneLineOfMTFormat))
						{
							$arrParegMatches[1][$intLastMatchedLineNo] .= " ".$strOneLineOfMTFormat;
							$arrParegMatches[1][$intLineNumber] = "";
						}
						else
						{
							$intLastMatchedLineNo = $intLineNumber;
						}	
						$intLineNumber++;	
					}
	
					foreach($arrParegMatches[1] as $strOneLineOfMTFormat)
					{
						if(!empty($strOneLineOfMTFormat))
							$arrNewFormatedMT103Transactions[] = $strOneLineOfMTFormat;
					}
						$arrAllTransactionsFormated[] = $arrNewFormatedMT103Transactions;
				}
				
				foreach($arrAllTransactionsFormated as $arrOneTr)
				{
					$arrRowToInsert = array();
					/**
					 * Following array will hold the information which will just be captured and inserted into
					 * table and will not go in transaction table. End user will change the information hisself
					 * in order to apply correct business logic on data
					 */
					$arrPreInsertNVP = array();
					foreach($arrOneTr as $strTranString)
					{
						preg_match("/:(.*):/isU", $strTranString, $arrIntermediateFindRet);
						$strMT103KeyTag   = strtoupper($arrIntermediateFindRet[1]);
						
						$strTransParamVal = str_replace($arrIntermediateFindRet[0], "", $strTranString);
						$strTransParamVal = trim($strTransParamVal);
						$arrPreInsertNVP[$strMT103KeyTag] = $strTransParamVal;
						
						
						switch($strMT103KeyTag)
						{
							
							case "20": //Transaction Reference
								$arrRowToInsert['TRANREF'] = $strTransParamVal;
								break;
							case "13C": //time indication
								break;
							case "23B": //Bank Operation Code
								$arrRowToInsert['SERIALNUMBER'] = $strTransParamVal;
								break;
							case "23E": //Instruction code
								break;
							case "26T": //Transaction Type Code
								$arrRowToInsert['TRANSTYPE'] = "Bank Transfer";
								break;
							case "32A": //Value Date | Currency Code | Payment Amount
								$arrRowToInsert['transDate'] = "20".substr($strTransParamVal,0,2)."-".substr($strTransParamVal,2,2)."-".substr($strTransParamVal,4,2);
								
								$arrRowToInsert['CURRENCYCODE'] = substr($strTransParamVal,6,3);
								$arrRowToInsert['AMOUNT'] = str_replace(",", ".", substr($strTransParamVal,9));
								break;
							case "33B":	//Currency / Instruction Amount
								break;
							case "36": 	//Exchange Rate
								$arrRowToInsert['EXCHANGERATE'] = $strTransParamVal;
								break;
							case "50A": //Ordering Customer (Debit Account)
							case "50K":
								$arrSenderParamsRegx = preg_split("/\\r/isU", $strTransParamVal);
								if(substr($arrSenderParamsRegx[0], 0, 1) == "/")
								{
									$arrRowToInsert['REMITTERACCOUNT'] = substr($arrSenderParamsRegx[0], 1);
									$arrRowToInsert['REMITTERNAME']    = trim($arrSenderParamsRegx[1]);
									$arrRowToInsert['REMITTERADDRESS'] = trim($arrSenderParamsRegx[2]);
									$arrRowToInsert['REMITTERCOUNTRY'] = trim($arrSenderParamsRegx[3]);
									
								}
								else
								{
									$arrRowToInsert['REMITTERNAME']    = trim($arrSenderParamsRegx[0]);
									$arrRowToInsert['REMITTERADDRESS'] = trim($arrSenderParamsRegx[1]);
									$arrRowToInsert['REMITTERCOUNTRY'] = trim($arrSenderParamsRegx[2]);
								}
								break;
							case "51A": //Sending Institution - Ignored
								break;
							case "52A": //Senders Correspondent - Ignored
								break;
							case "53A": //Senders's Correspondent
							case "53B":
							case "53D":
								$arrRowToInsert['REMITTERCORRESPONDENT']   = $strTransParamVal;
								break;
							case "54A": //Receiver's Correspondent
								$arrRowToInsert['BENEFICIARCORRESPONDENT'] = $strTransParamVal;
								break;
							case "55A": //Third Reimbursment Institution
								break;
							case "56A":	//Intermediary Institution
								break;
							case "57A": //SWIFTBIC 
							case "57B":
							case "57C":
							case "57D":
								$arrReceiverBanksRegx = preg_split("/\\r/isU", $strTransParamVal);								
								break;
							case "59":
							case "59A":
								$arrReceiverAccountInfoRegx = preg_split("/\\r/isU", $strTransParamVal);
								if(substr($arrReceiverAccountInfoRegx[0], 0, 1) == "/")
								{
									$arrRowToInsert['BENEFICIARYACCOUNTNUMBER'] = substr($arrReceiverAccountInfoRegx[0], 1);
									$arrRowToInsert['BENEFICIARYNAME'] 			= trim($arrReceiverAccountInfoRegx[1]);
									$arrRowToInsert['BENEFICIARYADDRSS'] 		= trim($arrReceiverAccountInfoRegx[2])." ".
																				  trim($arrReceiverAccountInfoRegx[3])." ".
																				  trim($arrReceiverAccountInfoRegx[4])." ".
																				  trim($arrReceiverAccountInfoRegx[5]);
											
								}
								else
								{
									$arrRowToInsert['BENEFICIARYNAME'] 	 =  trim($arrReceiverAccountInfoRegx[0]);
									$arrRowToInsert['BENEFICIARYADDRSS'] = 	trim($arrReceiverAccountInfoRegx[1])." ".
																			trim($arrReceiverAccountInfoRegx[2])." ".
																			trim($arrReceiverAccountInfoRegx[3])." ".
																			trim($arrReceiverAccountInfoRegx[4]);
								}
									
								break;
							case "70":	//Payment Reference - Reason
								break;
							case "71A":	//Chrging Instructions --        NEED Clarification
								break;
							case "71F":	//Sender Charges
								break;
							case "71G":	//Receiver Charges
								break;
							case "72":	//Deal Number and Rate
								break;		
							case "77B":	//Regulatory Reporting
								break;		
							case "77T":	//Envelope Contents
								break;		
							default:
								break;		
						}
					}
					
					/**
					 * Adjust the missing fields for Import Transaction
					 */
					$strFromCountry = "Pakistan";
					if(!empty($arrRowToInsert['REMITTERCOUNTRY']))
						$strFromCountry = $arrRowToInsert['REMITTERCOUNTRY'];
					if(empty($arrRowToInsert['TRANSTYPE']))
						$arrRowToInsert['TRANSTYPE'] = "Bank Transfer";
	
					$intDefaultCustomerAgentId = $agentID;
					
					/* Missing fileds adjustment ends here */	
					
					if(empty($arrRowToInsert['TRANREF']))
					{
						$arrOutputLog[] = array("L"=> "E", "M" => "Unable to locate Transfer Code <br />".$arrTransactions[$intRowCount]);
						continue;
					}
					
					//check if transaction with same serial number is already in database or not 
					$arrCheckDuplicate = selectFrom("SELECT count(transID) as cti FROM transactions WHERE refNumberIM = '".$arrRowToInsert['TRANREF']."'");
					if($arrCheckDuplicate['cti'] > 0)
					{
						$arrOutputLog[] = array("L"=> "W", "M" => "Transaction with TRANREF <b>".$arrRowToInsert['TRANREF']."</b> already in database.");
						continue;
					}
	
					$arrSenderNames 	= deComposeFullNameIntoParts($arrRowToInsert['REMITTERNAME']);
					$arrBeneficaryNames = deComposeFullNameIntoParts($arrRowToInsert['BENEFICIARYNAME']);
					
					//insert new customer
					
					$arrCustomerDataToInsert = array();
					$arrCustomerDataToInsert['agentID'] 	= $intDefaultCustomerAgentId; //default need to find out logic
					$arrCustomerDataToInsert['firstName']   = $arrSenderNames['firstName'];
					$arrCustomerDataToInsert['middleName']  = $arrSenderNames['middleName'];
					$arrCustomerDataToInsert['lastName']    = $arrSenderNames['lastName'];
					$arrCustomerDataToInsert['customerName']= $arrRowToInsert['REMITTERNAME'];
					$arrCustomerDataToInsert['country'] 	= $arrRowToInsert['REMITTERCOUNTRY'];
					$arrCustomerDataToInsert['address'] 	= $arrRowToInsert['REMITTERADDRESS'];
					$arrCustomerDataToInsert['accountName'] = $arrRowToInsert['REMITTERACCOUNT'];
					$arrCustomerDataToInsert['created'] 	= date("Y-m-d H:i:s");
					
				    $intCustomerId = dataInsertionOperation($arrCustomerDataToInsert, "customer");
					if(!$intCustomerId)
					{
						$arrOutputLog[] = array("L"=> "E", "M" => "Transaction with TRANREF ".$arrRowToInsert['TRANREF']." unable to import into database.");	
						continue;
					}
					
					activities($_SESSION['loginHistoryID'],"INSERTION",$intCustomerId,TBL_CUSTOMER,"New customer inserted via MT103 import routine.");
					
	
					
					$arrBeneficiaryDataToInsert = array();
					$arrBeneficiaryDataToInsert['customerID']      = $intCustomerId;
					$arrBeneficiaryDataToInsert['firstName']       = $arrBeneficaryNames["firstName"];
					$arrBeneficiaryDataToInsert['middleName'] 	   = $arrBeneficaryNames["middleName"];
					$arrBeneficiaryDataToInsert['lastName']   	   = $arrBeneficaryNames["lastName"];
					$arrBeneficiaryDataToInsert['beneficiaryName'] = $arrRowToInsert['BENEFICIARYNAME'];
					$arrBeneficiaryDataToInsert['address'] 		   = $arrRowToInsert['BENEFICIARYADDRSS'];
					$arrBeneficiaryDataToInsert['created'] 		   = date("Y-m-d");
	
					$intBeneficiaryId = dataInsertionOperation($arrBeneficiaryDataToInsert, "beneficiary");
					if(!$intBeneficiaryId)
					{
						$arrOutputLog[] = array("L"=> "E", "M" => "Transaction with TRANREF ".$arrRowToInsert['TRANREF']." unable to import into database.");	
						continue;
					}
					activities($_SESSION['loginHistoryID'],"INSERTION",$intBeneficiaryId,TBL_BENEFICIARY,"New beneficiary inserted via MT103 import routine.");
					
					
					$strFormatedTransactionData = $arrRowToInsert['transDate'];
					
					//import transaction table information
					$arrTransactionDataToInsert = array();
					$arrTransactionDataToInsert['custAgentID'] = $intDefaultCustomerAgentId;
					$arrTransactionDataToInsert['benAgentID']  = $intDefaultDistributorId;
					$arrTransactionDataToInsert['customerID']  = $intCustomerId;
					$arrTransactionDataToInsert['benID']       = $intBeneficiaryId;
					$arrTransactionDataToInsert['transDate']   = $strFormatedTransactionData;
					// ignore $arrTransactionDataToInsert['transStatus'] = $strTransactionStatusForMT103NewTransfers;
					$arrTransactionDataToInsert['transType']   = $arrRowToInsert['TRANSTYPE'];
					$arrTransactionDataToInsert['refNumberIM'] = $arrRowToInsert['TRANREF']; 
					$arrTransactionDataToInsert['IMFee'] 	   = '0'; 
					$arrTransactionDataToInsert['exchangeRate']= $arrRowToInsert['EXCHANGERATE']; 
					$arrTransactionDataToInsert['totalAmount'] = $arrRowToInsert['AMOUNT']; 
					$arrTransactionDataToInsert['transAmount'] = $arrRowToInsert['AMOUNT']; 
					$arrTransactionDataToInsert['localAmount'] = $arrRowToInsert['AMOUNT']; 
					$arrTransactionDataToInsert['addedBy'] 	   = $agentType; 
					$arrTransactionDataToInsert['remarks'] 	   = 'MT103 Import Routine'; 
					$arrTransactionDataToInsert['fromCountry'] = $strFromCountry; 
					$arrTransactionDataToInsert['toCountry']   = 'Pakistan'; 
					$arrTransactionDataToInsert['currencyFrom']= $arrRowToInsert['CURRENCYCODE']; 
					$arrTransactionDataToInsert['currencyTo']  = $arrRowToInsert['CURRENCYCODE']; 
					
					$arrTransactionDataToInsert['internalRemarks'] = $strFileName; //File name in which import contains
					$arrTransactionDataToInsert['restoreDate']     = date("Y-m-d H:i:s"); // on which time file is inserted into database
					$arrTransactionDataToInsert['sss'] = $intIBid; // Import batch number
					$arrTransactionDataToInsert['trans_source'] = 'M'; // Transaction come from MT103
				
					$intTransactionId = dataInsertionOperation($arrTransactionDataToInsert, "transactions");
					if(!$intTransactionId)
					{
						$arrOutputLog[] = array("L"=> "E", "M" => "Transaction with TRANREF ".$arrRowToInsert['TRANREF']." unable to import into database.");	
						continue;
					}
	
					$intImportCount++;
	
					activities($_SESSION['loginHistoryID'], "INSERTION", $intTransactionId, TBL_TRANSACTIONS, "Transaction imported via MT103 import routine.");
					
					// Insert full detail of the Tags and its values into extended table for further reference
					$arrTransExtendedInfo = array();
					$arrTransExtendedInfo['transID'] 	= $intTransactionId;
					$arrTransExtendedInfo['meta_data']  = serialize($arrPreInsertNVP);
					//dataInsertionOperation($arrTransExtendedInfo, "transactionExtended");
					
					
					//Get Bank Alfalah Code
					$strSelectBankName = "SELECT * FROM banks WHERE extra2 LIKE '08'";
					$arrSelectBankName = selectFrom($strSelectBankName);
					$strFullBranchAddress = $arrRowToInsert['BENEFICIARYBANKBRANCHADDRESS'];
					
					$arrBankDetailsToInsert = array();
					$arrBankDetailsToInsert['transID'] = $intTransactionId;
					$arrBankDetailsToInsert['benID'] = $intBeneficiaryId;
					//$arrBankDetailsToInsert['bankName'] = $arrSelectBankName['name'];
					$arrBankDetailsToInsert['accNo'] = $arrRowToInsert['BENEFICIARYACCOUNTNUMBER'];
					//$arrBankDetailsToInsert['branchCode'] = $arrRowToInsert['BENEFICIARYBRANCHCODE'];
					//$arrBankDetailsToInsert['branchAddress'] = trim($strFullBranchAddress);
					//$arrBankDetailsToInsert['originalBankId'] = $arrSelectBankName['bankId'];
					$arrBankDetailsToInsert['accountType'] = "Current";
					//debug($arrBankDetailsToInsert);
					
					
					$intBankDetailId = dataInsertionOperation($arrBankDetailsToInsert, "bankDetails");
	
					//debug($intBankDetailId);
					if(!empty($intBankDetailId))
						$arrOutputLog[] = array("L"=> "O", "M" => "Transaction with TRANREF <b>".$arrRowToInsert['TRANREF']."</b> imported into database successfully. Do not forget to edit transaction.");	
					else
						$arrOutputLog[] = array("L"=> "E", "M" => "Transaction with TRANREF ".$arrRowToInsert['TRANREF']." unable to import into database.");	

					
					
					// End of amin foreach loop
					
				}
	
			}
			else
				$arrOutputLog[] = array("L"=> "E", "M" => "Your file in not uploaded due to some error.");		
		}
		else
			$arrOutputLog[] = array("L"=> "E", "M" => "Your file in not uploaded due to some error.");	
	}
	else
		$arrOutputLog[] = array("L"=> "E", "M" => "Please select values to import.");	

	$strTitle = "Premiere FX Import MT013";

	if(empty($strTitle))
		$strTitle = "Premiere FX Import MT013";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$strTitle?></title>
<link href="images/interface.css" rev="stylesheet" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<style>
.heading {
	background-color:#6699cc;
	color:#ffffff;
	font-weight:bold;
	font-size:15px;
	font-family:Verdana, Arial, Helvetica, sans-serif;
}
.O {
	background-color:#009966;
	color:#FFFFFF;
	font-size:12px;
}
.E {
	background-color: #FF0000;
	color:#FFFFFF;
	font-size:12px;
	font-weight:bold;
}

.W {
	background-color: #FFFF33;
	color: #FF0000;
	font-size:12px;
	font-style:italic;
}


a {
	color:#0000FF;
}
</style>
<script language="javascript">

function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
</script> 

</head>
<body>

<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
	<tr>
    	<td class="heading">Import MT103</td>
	</tr>
	
	<?php 
		if(!empty($arrOutputLog))
		{
			foreach($arrOutputLog as $arrLog)
			{
				echo '<tr><td class="'.$arrLog['L'].'">'.$arrLog['M'].'</td></tr>';
			}
		}
	?>
	
	
	<tr>
		<td>
		<script type="text/javascript">
		function check(){
			if(document.getElementById('agentID').value == ""){
				alert("Please select an Agent First!");
				document.getElementById('agentID').focus();
				return;
			}
			else if(document.getElementById('uploadFile').value == ""){
				alert("Please select some file First!");
				document.getElementById('uploadFile').focus();
				return;
			}

			document.getElementById('import').submit();			
		}
		</script>
			<fieldset>
				<form name="import" id="import" action="" method="post" enctype="multipart/form-data">
					<table width="70%" border="0" align="center" cellpadding="5" cellspacing="1" style="background-color:#DFE6EA">
						<tr>
						<td align="right">Select Agent</td>
							<td>
								<select id="agentID" name="agentID" style="font-family:verdana; font-size: 11px;">
			  						<option value="">- Select Agent-</option>
										<?
											$agentTypeStr = "'Agent'";
											$userType = 'SUPA_SUBA'; // only for  Agent and distributor, in any other case userType is not needed
											$entyCondition = "";
											// default behaviour agentCompany [username]
											$format = 'agentCompany,username';
											$arguments = array(
																"returnType"=>'dd',
																"userType"=>$userType,
																"adminType"=>$agentTypeStr,
																"selectedUser"=>$_REQUEST["agentID"],
																"format"=>$format,
																"extraCondition"=>$entyCondition
															  );
											echo getUserDropdownList($arguments);
										?>
									</select>
							</td>
						</tr>
						<tr>
							<td width="25%" align="right">Select File</td>
							<td width="75%"><input name="uploadFile" type="file" id="uploadFile" size="70" /></td>
						</tr>
						
						<tr>
							<td align="center" colspan="2">
								<input name="Submit" type="button" class="flat" onclick="check()" value="Submit" />
							</td>
						</tr>
					</table>
				</form>
			</fieldset>
		</td>
	</tr>
	<tr><td valign="top"></td></tr>
</table>
	
</body>
</html>