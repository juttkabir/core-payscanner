<?
	session_start();
	$rootPath = $_SERVER['DOCUMENT_ROOT']."/admin/lib/";
	include ($_SERVER['DOCUMENT_ROOT']."/include/config.php");
	include ("security.php");
	include_once($rootPath."currency_exchange_functions.php");
	
	
	$agentType = getAgentType();
	// define page navigation parameters
	
	$limit=150;
	
	if ($offset == "")
		$offset = 0;
		
	if($limit == 0)
		$limit=200;
	
	
	if ($_REQUEST['newOffset'] != "") {
		$offset = $_REQUEST['newOffset'];
	}
	//debug($limit."-->".$offset);
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	$sortBy = $_REQUEST['sortBy'];
	if ($sortBy == "")
		$sortBy = "id";
	
	//debug($nxt."-->".$prv );
	$colSpans 		= 12;
	$roundAmountTo	= 2;
	$roundRateTo 	= 4;
	$filters = "";
	
	if(!empty($_REQUEST['buysellCurrency']))
		$buysellCurrency = $_REQUEST['buysellCurrency'];
	
	if($_REQUEST['fDay'] != "")
		$fDay = $_REQUEST['fDay'];
	
	if($_REQUEST['fMonth'] != "")
		$fMonth = $_REQUEST['fMonth'];
	
	if($_REQUEST['fYear'] != "")
		$fYear = $_REQUEST['fYear'];	
		
	if($_REQUEST['tDay'] != "")
		$tDay = $_REQUEST['tDay'];	
	
	if($_REQUEST['tMonth'] != "")
		$tMonth = $_REQUEST['tMonth'];	
	
	if($_REQUEST['tYear'] != "")
		$tYear = $_REQUEST['tYear'];	
	
	if($_REQUEST['Submit'] != "")
		$Submit = $_REQUEST['Submit'];	
		
	
	//debug($_REQUEST);
	$groupBy = " GROUP BY buysellCurrency";	
	if($_REQUEST['Submit']!="" || $_POST['Submit']!="")
	{
		$fromDate		= $_REQUEST['fYear'] . "-" . $_REQUEST['fMonth'] . "-" . $_REQUEST['fDay']." 00:00:00";
		$toDate 		= $_REQUEST['tYear'] . "-" . $_REQUEST['tMonth'] . "-" . $_REQUEST['tDay']." 23:59:59";
		$fromStrDate 	= date("Y-m-d H:i:s",strtotime($fromDate));
		$toStrDate 		= date("Y-m-d H:i:s",strtotime($toDate));
		
		$strDisplyFromDate	= date("d/m/Y",strtotime($fromDate));
		$strDisplyToDate	= date("d/m/Y",strtotime($toDate));
		
		$filtersDate 	= " and (created_at >= '$fromStrDate' and created_at <= '$toStrDate')";
		$filters 		.= " and (created_at >= '$fromStrDate' and created_at <= '$toStrDate')";
		//debug($filtersDate);
		$currencyFilter = '';
		if(!empty($_REQUEST['buysellCurrency']) && $_REQUEST['buysellCurrency'] !='all')
		$currencyFilter .= " and buysellCurrency = '".$_REQUEST['buysellCurrency']."'";
	
		$queryCurr = "  SELECT 
							buysellCurrency,sum(localAmount) as localAmount
						FROM 
							curr_exchange_account as cea
						WHERE
							transStatus != 'Cancelled'  ";							
		
		$whrClause = $currencyFilter.$filters.$groupBy;	
		$queryCurr .= $whrClause;
	
		//debug($queryCurr);
		$countRecords = SelectMultiRecords($queryCurr);
		$allCount = count($countRecords);
		
		//$queryCurr .= " LIMIT $offset , $limit";
		$currencyData = SelectMultiRecords($queryCurr);
		//debug($currencyData);
		
	}
	
	
	$currStockStr = "	SELECT 
							curr.currencyName,
							crt.buying_rate 
						FROM 
							currencies as curr,curr_exchange as crt 
						WHERE 
							curr.cID = crt.buysellCurrency  ".$currencyFilter.$groupBy." ORDER BY crt.id ASC";
							
	$currStockData = SelectMultiRecords($currStockStr);
	
	//debug($currStockStr);
	foreach($currStockData as $k=>$v){
		$currentStockInital = selectFrom("select * from currency_stock  where currency = '".$v['currencyName']."'");
		$foreingInitial = number_format(($currentStockInital['actualAmount']/$v['buying_rate']),2,'.','');
		$currencyDataInitial[$v['currencyName']][] = array(
													"created_at"	=> $currentStockInital['created_on'],
													"buy_sell" 		=> "B",
													"totalAmount" 	=> $currentStockInital['actualAmount'],
													"rate" 			=> $v['buying_rate'],
													"localAmount" 	=> $foreingInitial
												);
	}
	//debug($currencyDataInitial);
	$colHeads = array('Foreign Currency','GBP Sell','GBP Buy','Profit Sell (In GBP)','Sell Average Margin');
	
	/*** Currency Drop Down Query***/
	$sqlCurr = "SELECT 
					cID,
					currencyName
				FROM 
					currencies
				WHERE 
					cID IN
					(
					SELECT 
						buysellCurrency
					FROM 
						curr_exchange_account
					WHERE 1
					)";
												 	
	$sqlCurr .= " GROUP BY currencyName";
	$getBSCurrData1 = SelectMultiRecords($sqlCurr);

?>
<html>
<head>
<title>Stock Turn Over Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	
	$(document).ready(function(){
		
		$("#exportReport").change(function(){
			if($(this).val() != "")
			{
				$("#exportForm").submit();
			}
		});
		
		$("#exportBtn").click(function(){
			$("#exportReport").show();
		});
		
		$("#printBtn").click(function(){
			$("#filterRow").hide();
			$("#printRow").hide();
			window.print();
		});
		
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-4 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-4 years"))?>'
		});
		
	});
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	} 
	
</script>

<style type="text/css">
	.dp-choose-date{
		color: black;		
	}
	.error {
		color: red;
		font: 8pt verdana;
		font-weight:bold;
	}
</style>

</head>
<body>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr bgcolor="#DFE6EA">
    <td class="topbar" align="center">Stock Turn Over Report</td>
  </tr>
  <form name="search" method="post" action="currency_stock_turnover_report.php">
    <tr id="filterRow">
      <td>
	  	<fieldset>
        <table width="50%" border="0" align="center" cellpadding="5" cellspacing="1">
          <tr>
            <td align="center"><b>SEARCH FILTER</b></td>
          </tr>
          <tr bgcolor="#DFE6EA">
		  	<td align="center"><b>From </b>
            <?php 
				$month = date("m");
				$day = date("d");
				$year = date("Y");
			?>
              	<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                <?php
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
							$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
				?>
              	</select>
            	<script language="JavaScript">
					SelectOption(document.search.fDay, "<?=(!empty($_REQUEST['fDay'])?$_REQUEST['fDay']:"")?>");
				</script>
				  <SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
					<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
					<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
					<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
					<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
					<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
					<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
					<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
					<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
					<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
					<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
					<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
				  </SELECT>
					<script language="JavaScript">
						SelectOption(document.search.fMonth, "<?=(!empty($_REQUEST['fMonth'])?$_REQUEST['fMonth']:"")?>");
					</script>
				  <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
				  <?php
						$cYear=date("Y");
							for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
				  ?>
				  </SELECT>
				  <script language="JavaScript">
						SelectOption(document.search.fYear, "<?=(!empty($_REQUEST['fYear'])?$_REQUEST['fYear']:"")?>");
					</script>
				  <b>To </b>
				  <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				  <?php
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
							$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
					?>
				  </select>
					<script language="JavaScript">
						SelectOption(document.search.tDay, "<?=(!empty($_REQUEST['tDay'])?$_REQUEST['tDay']:"")?>");
					</script>
				  <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
					<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
					<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
					<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
					<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
					<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
					<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
					<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
					<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
					<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
					<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
					<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
				  </SELECT>
					<script language="JavaScript">
						SelectOption(document.search.tMonth, "<?=(!empty($_REQUEST['tMonth'])?$_REQUEST['tMonth']:"")?>");
					</script>
				  <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
					<?php
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
						{
							echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
						}
				  ?>
				  </SELECT>
				  <script language="JavaScript">
						SelectOption(document.search.tYear, "<?=(!empty($_REQUEST['tYear'])?$_REQUEST['tYear']:"")?>");
				</script>
				</td>
			</tr>
			<tr bgcolor="#DFE6EA">
				<td align="center"><b>Buy/Sell Currency</b>
              		<select name="buysellCurrency" style="font-family: verdana; font-size: 11px;">
                		<option value="">Select Currency</option>
                		<option value="all"> All </option>
                		<?php
						for ($i=0; $i < count($getBSCurrData1); $i++)
						{
							$getBSCurrName1 = $getBSCurrData1[$i]['currencyName'];		
							$getBSCurrID1   = $getBSCurrData1[$i]['cID'];
						?>
                			<OPTION value="<?=$getBSCurrID1; ?>"><?=$getBSCurrName1; ?></OPTION>
                		<?php
						}
						?>
              		</SELECT>
					<script language="JavaScript">
						SelectOption(document.search.buysellCurrency, "<?=$_REQUEST['buysellCurrency']?>");
					</script>
				</td>
			</tr>
			<tr bgcolor="#DFE6EA">
				<td align="center"><input type="submit" name="Submit" value="Process"></td>
			</tr>
        </table>
        </fieldset>
        <br>
      </td>
  </form>
  </tr>
  <tr>
    <td align="center"><table width="50%" border="0" cellspacing="1" cellpadding="1" align="center">
    <?php
		if ($allCount > 0)
		{
			$query_string = "&buysellCurrency=$buysellCurrency&Submit=$Submit&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear";
	?>
        <tr>
          <td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
              <tr>
                <td><?php if (count($currencyData) > 0) {;?>
                  Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
                  <?=$allCount; ?>
                  <?php } ;?>
                </td>
                <?php if ($prv >= 0) { ?>
                <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
                <?php } ?>
                <?php 
					  if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php } ?>
              </tr>
            </table></td>
        </tr>
        <tr bgcolor="#DFE6EA">
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[0]?>
            </strong></font></td>
          <td width="125"><font color="#005b90"><b>
            <?=$colHeads[1]?>
            </b></font></td>
          <td width="109"><font color="#005b90"><b>
            <?=$colHeads[2]?>
            </b></font></td>
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[3]?>
            </strong></font></td>
          <td width="109"><font color="#005b90"><strong>
            <?=$colHeads[4]?>
            </strong></font></td>
        </tr>
        <?php
			$grandBuy  			= 0;
			$grandSell 			= 0;
			$fltTotalTurnOver	= 0;
			$grandPL 	 		= 0;
			$grandMargin		= 0;
			$exportData 		= array();
			$exportTotalData	= array();
			$exportTotalTurnOver= array();
			
			for ($i=0; $i < count($currencyData); $i++)
			{
				
				$getCurr = selectFrom("select currencyName FROM currencies WHERE cID='".$currencyData[$i]['buysellCurrency']."'");
				$currencyName = $getCurr['currencyName'];
				$cID = $currencyData[$i]['buysellCurrency'];
				$whrClause 	= "AND buysellCurrency = '".$cID."' AND transStatus != 'Cancelled' ".$filtersDate;

				$buyAllStr 	= "Select localAmount,rate,totalAmount, buy_sell FROM curr_exchange_account WHERE 1 ".$whrClause;
				$buyAllData	= selectMultiRecords($buyAllStr);
				
				if(count($currencyDataInitial)>0){
					// merging array having first buy currency stock to display as first deal record.
					$buyAllData = array_merge($currencyDataInitial[$currencyName],$buyAllData);

				}
				
				$pl 			= 0;
				$buyCostInGBP  	= 0;
				$sellCostInGBP 	= 0;
				$margin 		= 0;
				
				$lastBuyRate = $currencyDataInitial[$currencyName][0]['rate'];
				$exportData['currencyName'][]	= $currencyName;
				//debug($buyAllData);
				foreach($buyAllData as $k=>$v){
					$arrInput = array(
							"buy_sell"    		=> $v['buy_sell'],	
							"totalAmount" 		=> $v['totalAmount'],
							"rate"        		=> $v['rate'],	
							"lastBuyRate" 		=> $lastBuyRate,
							"localAmount" 		=> $v['localAmount'],
							"roundRateLevel"  	=> $roundRateTo,
							"roundAmountLevel"	=> $roundAmountTo	
					);
					
					$arrCurrencyTotal = currency_totals($arrInput); 
					
					$sellCostInGBP	+= $arrCurrencyTotal['buyCostInGBP'];	// GBP amount
					$buyCostInGBP	+= $arrCurrencyTotal['sellCostInGBP'];	// GBP amount
					$pl				+= $arrCurrencyTotal['profit_loss'];
					$margin			+= $arrCurrencyTotal['margin'];
											
				}
				$margin = ($sellCostInGBP/$pl)*100;
				// Assign Data in sequence to be displayed on report and export
				$exportData[$currencyName][]	=	array('data'=>$sellCostInGBP,'dec'=>$roundAmountTo);	
				$exportData[$currencyName][]	=	array('data'=>$buyCostInGBP ,'dec'=>$roundAmountTo);
				$exportData[$currencyName][]	=	array('data'=>$pl ,'dec'=>$roundAmountTo);
				$exportData[$currencyName][]	= 	array('data'=>$margin ,'dec'=>$roundAmountTo);
				
				// Assign Data in sequence to be displayed on report and export
				
				$grandSell 			+= $sellCostInGBP;
				$grandBuy 			+= $buyCostInGBP;
				$grandPL 			+= $pl;
				$grandMargin		+= $margin;
				
		?>
				<tr valign="top" bgcolor="#eeeeee" title="Rate for : <? echo $getBSCurrName;?>">
				  <td><a href="#" onClick="javascript:window.open('currency_daily_deal_sheet_report_AMB.php?buysellCurrency=<? echo $cID;?>&Submit=Process&from_date=<?=$strDisplyFromDate?>&to_date=<?=$strDisplyToDate?>')"> <font color="#005b90"><b>
					<?=$currencyName?>
					</b></font></a> </td>
				  <td align="right"><?=stripslashes(number_format($sellCostInGBP,$roundAmountTo,'.',',')); ?></td>
				  <td align="right"><?=stripslashes(number_format($buyCostInGBP,$roundAmountTo,'.',',')); ?></td>
				  <td align="right"><?=stripslashes(number_format($pl,$roundAmountTo,'.',',')); ?></td>
				  <td align="right"><?=stripslashes(number_format($margin,$roundAmountTo,'.',',')); ?></td>
				</tr>
			<?php
			} //end for loop
			
			// Calculate Total Turn Over to display on report and export
			$fltTotalTurnOver	= ($grandSell+$grandBuy);
			// Assign Data in sequence to be displayed on report and export
			$exportTotalData[]	= array('data'=>'Grand Total');
			$exportTotalData[]	= array('data'=>$grandSell,'dec'=>$roundAmountTo);
			$exportTotalData[]	= array('data'=>$grandBuy,'dec'=>$roundAmountTo);
			$exportTotalData[]	= array('data'=>$grandPL,'dec'=>$roundAmountTo);
			$exportTotalData[]	= array('data'=>$grandMargin,'dec'=>$roundAmountTo);
			
			$exportTotalTurnOver[]	= array('data'=>'Total Turn Over');
			$exportTotalTurnOver[]	= array('data'=>$fltTotalTurnOver,'dec'=>$roundAmountTo);
		?>
        <tr bgcolor="#DFE6EA">
          <td><font color="#005b90"><strong>Grand Total</strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($grandSell,$roundAmountTo,'.',',')); ?>
            </strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($grandBuy,$roundAmountTo,'.',',')); ?>
            </strong></font></td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($grandPL,$roundAmountTo,'.',',')); ?>
            </strong></font> </td>
          <td align="right"><font color="#005b90"><strong>
            <?=stripslashes(number_format($grandMargin,$roundAmountTo,'.',',')); ?>
            </strong></font> </td>
        </tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="5" rowspan="2"><b>Total Turn Over = <?=$fltTotalTurnOver?></b></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
        <tr>
          <td colspan="<?=$colSpans?>" bgcolor="#000000"><table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
              <tr>
                <td><?php if (count($currencyData) > 0) {;?>
                  Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($currencyData));?></b> of
                  <?=$allCount; ?>
                  <?php } ;?>
                </td>
                <?php if ($prv >= 0) { ?>
                <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$query_string;?>"><font color="#005b90">First</font></a> </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$prv".$query_string;?>"><font color="#005b90">Previous</font></a> </td>
                <?php } ?>
                <?php 
					  if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                <td width="50" align="right"><a href="<?php print $PHP_SELF ."?newOffset=$nxt".$query_string;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$query_string;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php } ?>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="<?=$colSpans?>" align="center"></td>
        </tr>
        <tr id="printRow">
          <form action="export_currency_turnover_report.php" method="post" id="exportForm" name="exportForm" target="_blank">
          <td align="center" colspan="5"><input type="button" name="Submit2" value="Print This Report" id="printBtn" />
            <input type="hidden" name="exportData" id="exportData" value="<?=base64_encode(serialize($exportData))?>" />
            <input type="hidden" name="exportTotalData" id="exportTotalData" value="<?=base64_encode(serialize($exportTotalData))?>" />
			<input type="hidden" name="exportTotalTurnOver" id="exportTotalTurnOver" value="<?=base64_encode(serialize($exportTotalTurnOver))?>" />
            <input type="hidden" name="colHeads" id="colHeads" value="<?=base64_encode(serialize($colHeads))?>" />
            <input type="button" id="exportBtn" value="Export All" />
            <select name="exportReport" id="exportReport" style="display:none" >
              <option value="">Select Format</option>
              <option value="XLS">Excel</option>
              <option value="CSV">CSV</option>
              <option value="HTML">HTML</option>
            </select>
          </td>
          </form>
        </tr>
        <?php
		} else {
		?>
        <tr>
          <td colspan="5" align="center"> No Record found in the database. </td>
        </tr>
        <?php
		}
		?>
      </table>
	 </td>
  </tr>
</table>
</body>
</html>