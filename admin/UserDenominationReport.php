<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

/***********
	#5367 - Minas Center
			Bank Transactions also added in User Denomination Report.
			by Aslam Shahid.
***********/
	$bankTransFlag = false;
	if(CONFIG_BANK_TRANS_USER_DENOMINATION=="1"){
		$bankTransFlag = true;
	}
/***********
	#5367 - Minas Center
			Cheque Transactions also added in User Denomination Report.
			by Aslam Shahid.
***********/
	$chequeTransFlag = false;
	if(CONFIG_CHEQUE_TRANS_USER_DENOMINATION=="1"){
		$chequeTransFlag = true;
	}

/***********
	#5367 - Minas Center
			Total Calculated/Collected and Over/Short amounts are
			displayed in in 3 cards on user denomination report.
			by Aslam Shahid.
***********/
	$totalCalColOSFlag = false;
	if(CONFIG_CAL_COLL_OS_USER_DENOMINATION=="1"){
		$totalCalColOSFlag = true;
	}

/***********
	#5367 - Minas Center
			Saving User Denomination Temporarily and it will be editable until 
			saved permanently. Ledgers will be affected on saving permanently
			by Aslam Shahid.
***********/
	$saveTempFlag = false;
	if(CONFIG_SAVE_TEMP_USER_DENOMINATION=="1"){
		$saveTempFlag = true;
	}

/***********
	#5395 - Minas Center
			Hide total of Cash Transactions displayed
			by Aslam Shahid.
***********/
	$hideTotalGbpFlag = false;
	if(CONFIG_HIDE_CASH_TOTAL_USER_DENOMINATION=="1"){
		$hideTotalGbpFlag = true;
	}
/***********
	#5395 - Minas Center
			Show cheque created on user denomination report
			by Aslam Shahid.
***********/
	$showChequeCreatedFlag = false;
	if(CONFIG_CHEQUE_CREATED_USER_DENOMINATION=="1"){
		$showChequeCreatedFlag = true;
	}
/***********
	#5415 - Minas Center
			Show search filter to users specified in config if defined
			else to super admin only.
			by Aslam Shahid.
***********/
	$showSearchFilter = false;
	if(defined("CONFIG_SHOW_FILTERS_USER_DENOMINATION") && (strstr(CONFIG_SHOW_FILTERS_USER_DENOMINATION,$agentType.",") || strtoupper(CONFIG_SHOW_FILTERS_USER_DENOMINATION)=="ALL")){
		$showSearchFilter = true;
	}
	elseif($agentType=="admin"){
		$showSearchFilter = true;
	}
	
	/**
	 * Variables from Query String
	 */
	$cmd 				= $_REQUEST["cmd"];
	$print				= $_REQUEST["print"];
	$id 				= $_REQUEST["id"];
	$userId		    	= $_REQUEST["userId"];
	$currentDate		= $_REQUEST["currentDate"];
	$totalTransactions	= $_REQUEST["totalTransactions"];
	$totalExpense		= $_REQUEST["totalExpense"];
	
	$userId				= (!empty($userId))?$userId:$_SESSION["loggedUserData"]["userID"];
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$allowUpdate		= ($currentDate == date("Y-m-d", time()))?"Y":"N";

	if($cmd == "UPDATE")
	{
		if(!empty($id))
		{
			$ledgerConditionUpdate = false;
			if($saveTempFlag){
				$allowUpdate = "Y";
				if(strtoupper($_POST["Submit"])=="SAVE TEMPORARILY"){
					$allowUpdate = "Y";
					update("UPDATE CurrenryNotes SET saveTemp='Y' WHERE id='".$id."' AND TellerID = '".$userId."'");
				}
				elseif(strtoupper($_POST["Submit"])=="SUBMIT"){
					$allowUpdate = "N";
					update("UPDATE CurrenryNotes SET saveTemp='N' WHERE id='".$id."' AND TellerID = '".$userId."'");
					$ledgerConditionUpdate = true;
				}
			}
			/*****
				#5255 - Minas Center Point #7
						Ledger is maintained for user denomination entry
						to be shown in Admin Account Statement
						Admin Account Summary is maintained for Opening/Closing Balances.
						by Aslam Shahid
			*****/
			if(CONFIG_USER_DENOMINATION_ADMIN_LEDGER=="1"){
				$ledgerCurrencyNotesql = "SELECT 
							id,
							total,
							date
						FROM
							CurrenryNotes
						WHERE
							TellerID = '".$_REQUEST["userId"]."' AND id = '".$id."'";
				$resultLedger = selectFrom($ledgerCurrencyNotesql);
				$currecyToLedger = "GBP";
				$amountToLedgerOld = $resultLedger["total"];
				$amountToLedger = $_REQUEST["totalTillHd"];
				 if($resultLedger["id"]!=""){
					 if($saveTempFlag){
						/**
						 * Deposit entry in admin account
						 */
						if($ledgerConditionUpdate){
							maintainAdminStaffAccount($_REQUEST["userId"], $amountToLedger,"", "DEPOSIT", "","USER DENOMINATION DEPOSIT", $currecyToLedger,date("Y-m-d"));
						}
					 }
					 else{
						/**
						 * Withdraw entry in admin account
						 */
						maintainAdminStaffAccount($_REQUEST["userId"], $amountToLedgerOld,"", "WITHDRAW", "","USER DENOMINATION WITHDRAW", $currecyToLedger,date("Y-m-d"));
						/**
						 * Deposit entry in admin account
						 */
						maintainAdminStaffAccount($_REQUEST["userId"], $amountToLedger,"", "DEPOSIT", "","USER DENOMINATION DEPOSIT", $currecyToLedger,date("Y-m-d"));
					}
				}
			}
			if($saveTempFlag){
			
				$sql = "UPDATE
						CurrenryNotes
					SET
						currency_data = '".serialize($_REQUEST["data"])."',
						denomination_total = '".$_REQUEST["totalGbpHd"]."',
						total = '".$_REQUEST["totalTillHd"]."'
					WHERE
						id = '".$id."'
						AND TellerID = '".$userId."'
						";
			}
			else{
				$sql = "UPDATE
							CurrenryNotes
						SET
							currency_data = '".serialize($_REQUEST["data"])."',
							denomination_total = '".$_REQUEST["totalGbpHd"]."',
							total = '".$_REQUEST["totalTillHd"]."',
							date = now()
						WHERE
							id = $id
							AND TellerID = $userId
						";
			}
			update($sql);
			activities($_SESSION["loginHistoryID"], "Update", $id, "CurrenryNotes","Daily Expense report updated");
			
		} else {
			$errMsg = "";
		}
	}
	$extraFields = "";
	$extraValues = "";
	if($bankTransFlag){
		$extraFields = ",totalBankGbp";
		$extraValues = ",'".$_REQUEST["totalBankGbp"]."'";
	}
	if($chequeTransFlag){
		$extraFields .= ",totalChequeGbp";
		$extraValues .= ",'".$_REQUEST["totalChequeGbp"]."'";
	}
	if($saveTempFlag){
		$extraFields .= ",saveTemp";
		$extraValues .= ",'".$_REQUEST["saveTemp"]."'";
	}
	if($cmd == "ADD")
	{
		$sql = "INSERT INTO
					CurrenryNotes
					(
						Currency,
						TellerID,
						date,
						currency_data,
						denomination_total,
						total
						$extraFields
					)
				VALUES
					(
						'GBP',
						'".$_REQUEST["userId"]."',
						now(),
						'".serialize($_REQUEST["data"])."',
						'".$_REQUEST["totalGbpHd"]."',
						'".$_REQUEST["totalTillHd"]."'
						$extraValues
					)
				";
		insertInto($sql);
		$currencyLast = selectFrom("SELECT LAST_INSERT_ID() as currLastID");
		$currencyLastID = $currencyLast["currLastID"];

		$cmd = "UPDATE";
		$ledgerConditionAdd = true;
		if($saveTempFlag){
			$allowUpdate = "Y";
			if(strtoupper($_POST["Submit"])=="SAVE TEMPORARILY"){
				$allowUpdate = "Y";
				$ledgerConditionAdd = false;
				update("UPDATE CurrenryNotes SET saveTemp='Y' WHERE id='".$currencyLastID."'");
			}
			elseif(strtoupper($_POST["Submit"])=="SUBMIT"){
				$allowUpdate = "N";
				update("UPDATE CurrenryNotes SET saveTemp='N' WHERE id='".$currencyLastID."'");
			}
		}
		/*****
			#5255 - Minas Center Point #7
					Ledger is maintained for user denomination entry
					to be shown in Admin Account Statement
					Admin Account Summary is maintained for Opening/Closing Balances.
					by Aslam Shahid
		*****/
		if(CONFIG_USER_DENOMINATION_ADMIN_LEDGER=="1" && $ledgerCondition){
			$currecyToLedger = "GBP";
			$amountToLedger = $_REQUEST["totalTillHd"];
			/**
			 * Deposit entry in admin account
			 */
			 if(!empty($_REQUEST["userId"])){
				maintainAdminStaffAccount($_REQUEST["userId"], $amountToLedger,"", "DEPOSIT", "","USER DENOMINATION DEPOSIT", $currecyToLedger,date("Y-m-d"));
			}
		}
		activities($_SESSION["loginHistoryID"],"INSERTION",0,"CurrenryNotes","Daily Expense report created");
	}
	
	$sql = "SELECT 
				COUNT(id) AS cnt
			FROM 
				CurrenryNotes
			WHERE
				TellerID = $userId
				AND date like '$currentDate%'
			";
	$result = selectFrom($sql);
	$numRows = $result["cnt"];
	
	if($numRows > 0)
	{
		$latestUpdatesOn = $currentDate;
		$cmd = "UPDATE";
	} else {
		$sql = "SELECT
					DISTINCT DATE_FORMAT(date, '%Y-%m-%d') as created
				FROM
					CurrenryNotes
				ORDER BY
					date DESC
				LIMIT
					0,1
				";
		$result = selectFrom($sql);
		$latestUpdatesOn = $result["date"];
	}
	
	if(empty($latestUpdatesOn)){
		if($saveTempFlag){
			$latestUpdatesOn = date("Y-m-d",time());
		}
	}
	
	$sql = "SELECT 
				id,
				Currency,
				currency_data,
				denomination_total,
				total
				$extraFields
			FROM
				CurrenryNotes
			WHERE
				TellerID = $userId
				AND date like '$latestUpdatesOn%'
			";

	$result = selectFrom($sql);
	$id						= $result["id"];

	if($numRows > 0)
	{
		$currency_data 		= unserialize($result["currency_data"]);
		$closingBalance		= $result["closingBalance"];
	} else {
		$currency_data		= unserialize($result["currency_data"]);
		$closingBalance		= 0;
	}
	
	if(empty($cmd))
	{
		$cmd = "ADD";
	}
	if($saveTempFlag){
		$allowUpdate = "Y";
		if(strtoupper($result["saveTemp"])=="Y"){
			$cmd = "UPDATE";
			$allowUpdate = "Y";
		}
		elseif(strtoupper($result["saveTemp"])=="N"){
			$allowUpdate = "N";
		}
	}

	$arrLoogedInUserName = selectFrom("select username from admin where userid = '".$userId."'");
	$strUserName = $arrLoogedInUserName["username"];

/* Bank Transactions */ 
if($bankTransFlag){
	$strBankCashSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount
			FROM 
				transactions t
			WHERE 
					(t.custAgentID = '$userId' or t.addedBy = '".$strUserName."')
				AND t.transDate like '$latestUpdatesOn%'
				AND moneyPaid = 'By Bank Transfer'
			";
	$arrBankCash = SelectMultiRecords($strBankCashSql);
	$xTotalTransactions = 0;
	for($i=0; $i<sizeof($arrBankCash); $i++){
		$xTotalTransactions		+= $arrBankCash[$i]["totalAmount"];
	}
	$xTotalTransactions = number_format($xTotalTransactions,2,".",",");
}
/* Cash Transactions */ 
	$strCashSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount
			FROM 
				transactions t
			WHERE 
					(t.custAgentID = '$userId' or t.addedBy = '".$strUserName."')
				AND t.transDate like '$latestUpdatesOn%'
				AND moneyPaid = 'By Cash'
			";
	$arrBankCash = SelectMultiRecords($strCashSql);
	$xTotalTransactionsCash	= 0;
	for($i=0; $i<sizeof($arrBankCash); $i++){
		$xTotalTransactionsCash		+= $arrBankCash[$i]["totalAmount"];
	}
	$xTotalTransactionsCash = number_format($xTotalTransactionsCash,2,".",",");

/* Cheque Transactions */ 
	$strChequeSql = "SELECT 
				t.refNumberIM, 
				t.totalAmount
			FROM 
				transactions t
			WHERE 
					(t.custAgentID = '$userId' or t.addedBy = '".$strUserName."')
				AND t.transDate like '$latestUpdatesOn%'
				AND moneyPaid = 'By Cheque'
			";
	$arrBankCheque = SelectMultiRecords($strChequeSql);
	$xTotalTransactionsCheque	= 0;
	for($i=0; $i<sizeof($arrBankCheque); $i++){
		$xTotalTransactionsCheque	+= $arrBankCheque[$i]["totalAmount"];
	}
	$xTotalTransactionsCheque = number_format($xTotalTransactionsCheque,2,".",",");

/* Currency Exchange */ 
	$strCurrExchSql = "SELECT 
							sum(ce.localAmount) as localAmountGBP,
							sum(ce.totalAmount) as foreignAmount,
							cr.currencyName as currency 
						FROM 
							curr_exchange_account ce,currencies cr 
						WHERE 
							ce.buysellCurrency = cr.cID ";
	$strCurrExchSql .= " AND created_at LIKE '$latestUpdatesOn%' AND createdBy ='$userId' ";
	$strCurrExchSql .= " GROUP BY currency ORDER BY ce.id DESC ";
	$arrCurrExch = SelectMultiRecords($strCurrExchSql);

if($showChequeCreatedFlag){
	$strOrderCreatedSql = "SELECT 
						cheque_currency,
						sum(cheque_amount) as chequeAmount
					FROM
						cheque_order 
					WHERE 
						upper(cheque_currency)='GBP' AND status='NG' ";

	$strOrderCreatedSql .= " AND created_on LIKE '$latestUpdatesOn%' AND created_by LIKE '$userId|%' ";
	$strOrderCreatedSql .= " GROUP BY cheque_currency ORDER BY order_id DESC ";

	$xTotalOrderCreated = 0;
	$arrOrderCreated = selectFrom($strOrderCreatedSql);	
	if($arrOrderCreated["chequeAmount"]!="")
		$xTotalOrderCreated = $arrOrderCreated["chequeAmount"];
	$xTotalOrderCreated = number_format($xTotalOrderCreated,2,".",",");
}

	$strOrderSql = "SELECT 
						cheque_currency,
						sum(paid_amount) as paidAmount
					FROM
						cheque_order 
					WHERE 
						upper(cheque_currency)='GBP' AND status='ID' ";

	$strOrderSql .= " AND created_on LIKE '$latestUpdatesOn%' AND created_by LIKE '$userId|%' ";
	$strOrderSql .= " GROUP BY cheque_currency ORDER BY order_id DESC ";

	$xTotalOrder = 0;
	$arrOrder = selectFrom($strOrderSql);	
	if($arrOrder["paidAmount"]!="")
		$xTotalOrder = $arrOrder["paidAmount"];
	$xTotalOrder = number_format($xTotalOrder,2,".",",");
	
	/* This makes fields readonly if it is saved permanently (by A.Shahid) */	
	$readOnlyHtml = "";
	if($saveTempFlag && $allowUpdate!="Y"){
		$readOnlyHtml = "readonly";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>User Denomination Report</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<script src="javascript/jquery.js"></script>
<script type="text/javascript">

function denominationCal()
{

	var note50 = Number($("#note50").val());
	var note20 = Number($("#note20").val());
	var note10 = Number($("#note10").val());
	var note5  = Number($("#note5").val());
	var note2  = Number($("#note2").val());
	var note1  = Number($("#note1").val());
	
	var p50 = Number($("#p50").val());
	var p20 = Number($("#p20").val());
	var p10 = Number($("#p10").val());
	var p5  = Number($("#p5").val());
	var p2  = Number($("#p2").val());
	var p1  = Number($("#p1").val());
	
	
	
	var noteTotal = (note50 * 50) + (note20 * 20) + (note10 * 10) + (note5 * 5) + (note2 * 2) + (note1 * 1);
	var pTotal = (p50 * 0.50) + (p20 * 0.20) + (p10 * 0.10) + (p5 * 0.05) + (p2 * 0.02) + (p1 * 0.01);
	
	
	var total = noteTotal + pTotal;
	$("#totalGbp").html(total);
	$("#totalGbpHd").val(total);
	
	updateTill();
}


function updateTill()
{
var totalCurr = 0;
var curEUR=curUSD=curBRL=expanse=chequeAmountC=0;

<? if($totalCalColOSFlag){ ?>
	<? if(count($arrCurrExch)>0){
		for($c=0;$c<count($arrCurrExch);$c++){
?>
			totalCurr += Number(document.getElementById("currency<?=$arrCurrExch[$c]["currency"];?>").value);
<?		}
	   }
	}
	else{
?>
	curEUR = Number($("#currencyEur").val());
	curUSD = Number($("#currencyUsd").val());
	curBRL = Number($("#currencyBrl").val());
	expanse = Number($("#dailyExpanse").val());
<? }?>
	var chequeAmount = Number($("#chequeAmount").val());
<? if($showChequeCreatedFlag){ ?>
	chequeAmountC = Number($("#chequeAmountC").val());
<? }?>
	var denominationTotal = Number($("#totalGbp").html());

	var totalTill = curEUR + curUSD + curBRL + chequeAmount + chequeAmountC + denominationTotal + expanse + totalCurr;
<? if($showChequeCreatedFlag){ ?>
	var totalTill = curEUR + curUSD + curBRL + chequeAmountC - chequeAmount + denominationTotal + expanse;
<? }?>
<? if($bankTransFlag){ ?>	
	var totalBankGbp = Number($("#totalBankGbp").val());
	totalTill +=totalBankGbp;
<? }?>	

<? if($chequeTransFlag){?>	
	var totalChequeGbp = Number($("#totalChequeGbp").val());
	totalTill +=totalChequeGbp;
<? }?>	

	$("#totalTill").html(totalTill);
	$("#totalTillHd").val(totalTill);

<? if($totalCalColOSFlag){?>	
	<? if($bankTransFlag){ ?>
		$("#bankTransColl").html(Number($("#totalBankGbp").val()).toFixed(2));
		$("#bankTransOS").html(($("#bankTransColl").html()-$("#bankTransCal").html()).toFixed(2));
	<? }?>
	<? if($chequeTransFlag){?>	
		$("#bankChequeColl").html(Number($("#totalChequeGbp").val()).toFixed(2));
		$("#bankChequeOS").html(($("#bankChequeColl").html()-$("#bankChequeCal").html()).toFixed(2));
	<? }?>
	$("#bankCashColl").html(denominationTotal.toFixed(2));
	$("#bankCashOS").html(($("#bankCashColl").html()-$("#bankCashCal").html()).toFixed(2));

	<? if(count($arrCurrExch)>0){
		for($f=0;$f<count($arrCurrExch);$f++){
			$currName = strtoupper($arrCurrExch[$f]["currency"]);
?>
	$("#currency<?=$currName?>Coll").html(Number($("#currency<?=$currName;?>").val()).toFixed(2));
	$("#currency<?=$currName?>OS").html((Number($("#currency<?=$currName?>Coll").html())-$("#currency<?=$currName?>Cal").html()).toFixed(2));
<?		}
	   }
?>

	$("#orderPaidColl").html(chequeAmount.toFixed(2));
	$("#orderPaidOS").html(($("#orderPaidColl").html()-$("#orderPaidCal").html()).toFixed(2));

	<? if($showChequeCreatedFlag){ ?>
		$("#orderCreatedColl").html(chequeAmountC.toFixed(2));
		$("#orderCreatedOS").html(($("#orderCreatedColl").html()-$("#orderCreatedCal").html()).toFixed(2));
	<? }?>

<? }?>
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<style type="text/css">
	.windowTotal{
		font-family:Arial, Helvetica, sans-serif;
		background-color:gainsboro;
		margin:5px; 
		text-align:center;
 	}
	.windowTotal2{
		font-family:Arial, Helvetica, sans-serif;
		background-color:gainsboro;
		text-align:center;
		width:70px;
		float:right;
 	}
	.clear{
		height:10px;
		clear:both;
		height:2px;
	}
</style>
</head>

<body onload="denominationCal();">

<? if($showSearchFilter && $_REQUEST["from"]!="CashBookNew") { ?>
	<form action="" method="post" name="frmSearch">
	<table width="65%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<tr>
			<td align="right">Users</td>
			<?
			$agentSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
						ORDER BY
							username
						";
			$agentResult = SelectMultiRecords($agentSql);
			
			
			$strStaffQuery  = "
								select 
									userID,
									username, 
									agentCompany, 
									name, 
									agentContactPerson,
									adminType 
								from 
									".TBL_ADMIN_USERS." 
								where 
										adminType IN ('Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM')
									AND agentStatus='Active' 
								order by 
									adminType, 
									name	";
			$arrStaff = selectMultiRecords($strStaffQuery);

			
			
				
			?>
			<td>
				<select name="userId" id="userId">
					<option value="">- Select User -</option>
					<optgroup label="Agents">
						<? for($i=0; $i<sizeof($agentResult); $i++) { ?>
							<option value="<?=$agentResult[$i]["userID"]?>"><?=$agentResult[$i]["username"]."[".$agentResult[$i]["name"]."]" ?></option>
						<? } ?>
					</optgroup>
					<?
						for($i=0; $i < count($arrStaff); $i++)
						{
							if($arrStaff[$i]["adminType"] != $arrStaff[$i-1]["adminType"])
								echo "<optgroup label='".$arrStaff[$i]["adminType"]."'>";
							
							if($intUserId == $arrStaff[$i]["userID"]) { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" selected="selected"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } else { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } 

							if($arrStaff[$i]["adminType"] != $arrStaff[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$userId; ?>");
			   </script>
			</td>
		</tr>
		<? if(!empty($userId)) { ?>
		<tr>
			<td align="right">Date of Expense</td>
			<td>
				<?php
					$strDates = "select DISTINCT DATE_FORMAT(date, '%Y-%m-%d') as date from CurrenryNotes where TellerID = $userId order by date desc";
					//debug($strDates);
					$arrAllDates = array();
					$arrAllDatesQ = selectMultiRecords($strDates);
					for($arr=0;$arr<count($arrAllDatesQ);$arr++){
						$arrAllDates[]= $arrAllDatesQ[$arr]["date"];
					}
					$todayDate = date("Y-m-d",time());
					if($arrAllDates[0]!="") {
				?>
				<select name="currentDate" id="date">
					<?	if(!in_array($todayDate,$arrAllDates) && $saveTempFlag){ // This will show today date if it is not in system ?>
						<option value="<?=$todayDate;?>"><?=$todayDate;?></option>
					<? }?>
					<? 
						foreach($arrAllDates as $key => $val) 
						{ 
							$strSelect = "";
							if($currentDate == $val){
								 $strSelect = 'selected="selected"';
							}
					?>
						<option value="<?=$val?>" <?=$strSelect?>><?=$val?></option>
					<? } ?>
				</select>
				<? } elseif($saveTempFlag){ // This will show today date if there is no entry in system ?>
					<select name="currentDate" id="date">
							<option value="<?=$todayDate;?>"><?=$todayDate;;?></option>
					</select>
				<? }?>
			</td>
		</tr>
		<? } ?>
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2">
				<input type="submit" name="Submit" value="Submit" />
			</td>
		</tr>
	</table>
	</form>
<? } ?>
<? if($_REQUEST["from"]=="CashBookNew" && !empty($_REQUEST["userId"]) && !empty($_REQUEST["currentDate"])) { ?>
	<table width="65%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
		<?
			$agentSql = "SELECT 
							name,
							username
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							userID = '".$_REQUEST["userId"]."'
						";
			$agentResult = selectFrom($agentSql);
			$agentUsername = $agentResult["username"];
			$agentName = $agentResult["name"];
			?>

			<td align="right"  class='heading'>Agent : </td>
			<td width="58%" align="left"  class='heading'>
				<em><?=$agentUsername." (".$agentName.")"?></em>
			</td>
		</tr>
		</tr>
		<tr>
			<td align="right"  class='heading'>Date : </td>
			<td align="left"  class='heading'>
				<em><?=$_REQUEST["currentDate"]?></em>
			</td>
		</tr>
	</table>
<? } ?>
<br />
<table width="65%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
				<tr>
					<td align="center" class="reportHeader"><?=COMPANY_NAME?> - User Denomination Report<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
			<!-- Shahid -->
			<? if($bankTransFlag){?>
				<tr>
					<td class="heading">Bank Transactions</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="left" colspan="2">
					<input type="text" name="data[totalBankGbp]" id="totalBankGbp" size="20" onblur="updateTill();" 
					value="<?=$currency_data["totalBankGbp"]?>" <?=$readOnlyHtml?>/>
					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="5">&nbsp;</td>
			  </tr>
			<? }?>
				<tr>
					<td class="heading">Cash Transactions</td>
					<td align="right" class="heading">Notes</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;50&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note50]" id="note50" size="20" onblur="denominationCal();" value="<?=$currency_data["note50"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;20&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note20]" id="note20" size="20" onblur="denominationCal();" value="<?=$currency_data["note20"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;10&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note10]" id="note10" size="20" onblur="denominationCal();" value="<?=$currency_data["note10"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;5&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note5]" id="note5" size="20" onblur="denominationCal();" value="<?=$currency_data["note5"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right" class="heading">Coins</td>
					<td align="right" colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;2&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note2]" id="note2" size="20" onblur="denominationCal();" value="<?=$currency_data["note2"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">&pound;1&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[note1]" id="note1" size="20" onblur="denominationCal();" value="<?=$currency_data["note1"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">50p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p50]" id="p50" size="20" onblur="denominationCal();" value="<?=$currency_data["p50"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">20p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p20]" id="p20" size="20" onblur="denominationCal();" value="<?=$currency_data["p20"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">10p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p10]" id="p10" size="20" onblur="denominationCal();" value="<?=$currency_data["p10"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">5p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p5]" id="p5" size="20" onblur="denominationCal();" value="<?=$currency_data["p5"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">2p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p2]" id="p2" size="20" onblur="denominationCal();" value="<?=$currency_data["p2"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">1p&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[p1]" id="p1" size="20" onblur="denominationCal();" value="<?=$currency_data["p1"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="right">&nbsp;</td>
				</tr>
			<? if($hideTotalGbpFlag){?>
				<tr>
					<td align="right" colspan="5">
						<span id="totalGbp" style="display:none;">0.00</span>&nbsp;
					</td>
				</tr>
			<? }
				else{
			?>
				<tr>
					<td>&nbsp;</td>
					<td align="right" class="heading">Total GBP</td>
					<td align="left" colspan="2">
						<span id="totalGbp">0.00</span>					</td>
					<td align="right">&nbsp;</td>
				</tr>
			<? }?>
			<? if($chequeTransFlag){?>
				<tr>
                  <td class="heading">Cheque Transactions</td>
				  <td>&nbsp;</td>
				  <td align="right">&nbsp;</td>
				  <td align="right">&nbsp;</td>
				  <td align="right">&nbsp;</td>
			  </tr>
				<tr>
                  <td>&nbsp;</td>
				  <td align="right">&nbsp;</td>
				  <td align="left" colspan="2"><input type="text" name="data[totalChequeGbp]" id="totalChequeGbp" size="20" onblur="updateTill();" value="<?=$currency_data["totalChequeGbp"]?>" <?=$readOnlyHtml?>/>
				  </td>
				  <td align="right">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="5">&nbsp;</td>
			  </tr>
			<? }?>				

				<tr>
					<td class="heading">Currency Exchange</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
			<? if($totalCalColOSFlag){ ?>
				<? if(count($arrCurrExch)>0){
					for($n=0;$n<count($arrCurrExch);$n++){
						$currenyN = strtoupper($arrCurrExch[$n]["currency"]);
						if($currenyN!=""){
							$currExchAmount = ($currency_data[$currenyN]!=""?$currency_data[$currenyN]:0);
					?>
					<tr>
						<td>&nbsp;</td>
						<td align="right">Amount in <b><?=$currenyN?></b>&nbsp;&nbsp;&nbsp;</td>
						<td align="left" colspan="2">
							<input type="text" name="data[<?=$currenyN?>]" id="currency<?=$currenyN?>" size="20" onblur="updateTill();" 
							value="<?=$currExchAmount;?>" <?=$readOnlyHtml?>/>
						</td>
						<td align="right">&nbsp;</td>
					</tr>
						
				<? 		}
					 }
 			       }
				   else{
				?>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td align="left" colspan="2">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
			   <?	}?>
			<? }
				else{
			?>
				<tr>
					<td>&nbsp;</td>
					<td align="right">Conversion of GBP to <b>EUR</b>&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[EUR]" id="currencyEur" size="20" onblur="updateTill();" value="<?=$currency_data["EUR"]?>" />					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">Conversion of GBP to <b>USD</b>&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[USD]" id="currencyUsd" size="20" onblur="updateTill();" value="<?=$currency_data["USD"]?>" />					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="right">Conversion of GBP to <b>BRL</b>&nbsp;&nbsp;&nbsp;</td>
					<td align="left" colspan="2">
						<input type="text" name="data[BRL]" id="currencyBrl" size="20" onblur="updateTill();" value="<?=$currency_data["BRL"]?>" />					</td>
					<td align="right">&nbsp;</td>
				</tr>
			<? }?>	
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
				<tr>
					<td class="heading">Cheque Cashing</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<? if($showChequeCreatedFlag){?>
				<tr>
					<td>&nbsp;</td>
					<td align="right"><b>Cheque Created &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; QTY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><br/>
						<input type="text" name="data[qtyC]" size="5" value="<?=$currency_data["qtyC"]?>" <?=$readOnlyHtml?>/>
						</td>
				  <td align="left" colspan="2"><strong>Total</strong><br/>
					  <input type="text" name="data[chequeAmountC]" id="chequeAmountC" size="20" onblur="updateTill();" value="<?=$currency_data["chequeAmountC"]?>" <?=$readOnlyHtml?>/>					
					  </center></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? } ?>

				<? if($totalCalColOSFlag){?>
				<tr>
					<td>&nbsp;</td>
					<td align="right"><b>Cheque Paid &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; QTY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><br/>
						<input type="text" name="data[qty]" size="5" value="<?=$currency_data["qty"]?>" <?=$readOnlyHtml?>/>
						</td>
				  <td align="left" colspan="2"><strong>Total</strong><br/>
					  <input type="text" name="data[chequeAmount]" id="chequeAmount" size="20" onblur="updateTill();" value="<?=$currency_data["chequeAmount"]?>" <?=$readOnlyHtml?>/>					
					  </center></td>
					<td align="right">&nbsp;</td>
				</tr>
				<? }
					else{?>
				<tr>
					<td>&nbsp;</td>
					<td align="right"><b>QTY</b>
						<input type="text" name="data[qty]" size="5" value="<?=$currency_data["qty"]?>" <?=$readOnlyHtml?>/>
						</td>
					<td align="left" colspan="2">
						<input type="text" name="data[chequeAmount]" id="chequeAmount" size="20" onblur="updateTill();" value="<?=$currency_data["chequeAmount"]?>" <?=$readOnlyHtml?>/>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<? }?>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
				<? if(!$totalCalColOSFlag){ ?>
					<tr>
						<td class="heading">Daily Expense</td>
						<td>&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="left" colspan="2">
							<input type="text" name="data[expense]" id="dailyExpanse" size="20" onblur="updateTill();" value="<?=$currency_data["expense"]?>" <?=$readOnlyHtml?>/>					</td>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="5">&nbsp;</td>
					</tr>
				<? }?>
				<tr>
					<td>&nbsp;</td>
					<td align="right"><b>Total Till</b></td>
					<td align="left" colspan="2">
						<span id="totalTill">0.00</span>					</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
		<? if($totalCalColOSFlag){ ?>
				<tr>
					<td class="heading" colspan="5" align="center">
					<center>
						<table width="33%" cellspacing="0" cellpadding="0" border="1" align="left">
							<tbody>
								<tr>
									<td colspan="3">
										<table width="100%" cellspacing="0" cellpadding="2" border="0" align="left">
											<th align="center" colspan="2" bgcolor="#CCCCCC">Total Calculated</th>
									<? if($bankTransFlag){?>
											<tr>
												<td width="50%" class="heading">Bank Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankTransCal"><?=$xTotalTransactions?></div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Cash (GBP)</td>
												<td class="heading"><div class="windowTotal" id="bankCashCal"><?=$xTotalTransactionsCash;?></div></td>
											</tr>
									<? if($chequeTransFlag){?>
											<tr>
												<td width="50%" class="heading">Cheque Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankChequeCal"><?=$xTotalTransactionsCheque;?></div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Currency</td>
												<td class="heading">
												<? if(count($arrCurrExch)>0){
													for($i=0;$i<count($arrCurrExch);$i++){
														$foreignCurrExchAmount = ($arrCurrExch[$i]["foreignAmount"]!=""?$arrCurrExch[$i]["foreignAmount"]:0);
														$totalCurrExchAmount = ($arrCurrExch[$i]["localAmountGBP"]!=""?$arrCurrExch[$i]["localAmountGBP"]:0);
												?>
													 <div style='float:left;'><?=strtoupper($arrCurrExch[$i]["currency"]);?>&nbsp;</div>
													 <div class="windowTotal2" id="currency<?=strtoupper($arrCurrExch[$i]["currency"]);?>Cal">
													 <?=number_format($foreignCurrExchAmount,2,".",",");?></div>
													<div class="clear">&nbsp;</div>
												<? 	}
												   }
												   else{
												?>
													 <div class="windowTotal">0.00</div>
											   <?	}?>
											   </td>
											</tr>
										<? if($showChequeCreatedFlag){ ?>
											<tr>
												<td width="50%" class="heading">Cheque Created</td>
												<td class="heading"><div class="windowTotal" id="orderCreatedCal"><?=$xTotalOrderCreated;?></div></td>
											</tr>
										<? }?>
											<tr>
												<td width="50%" class="heading">Cheque Paid</td>
												<td class="heading"><div class="windowTotal" id="orderPaidCal"><?=$xTotalOrder;?></div></td>
											</tr>
										</table>									</td>
								</tr>
							</tbody>
						</table>
						<table width="33%" cellspacing="0" cellpadding="0" border="1" align="left">
							<tbody>
								<tr>
									<td colspan="3">
										<table width="100%" cellspacing="0" cellpadding="2" border="0" align="left">
											<th align="center" colspan="2" bgcolor="#CCCCCC">Total Collected</th>
									<? if($bankTransFlag){?>
											<tr>
												<td width="50%" class="heading">Bank Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankTransColl">0.00</div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Cash (GBP)</td>
												<td class="heading"><div class="windowTotal" id="bankCashColl">0.00</div></td>
											</tr>
									<? if($chequeTransFlag){?>
											<tr>
												<td width="50%" class="heading">Cheque Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankChequeColl">0.00</div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Currency</td>
												<td class="heading">
												<? if(count($arrCurrExch)>0){
													 for($i=0;$i<count($arrCurrExch);$i++){
														$currencyCol = strtoupper($arrCurrExch[$i]["currency"]);
														if($currencyCol!=""){
															$currExchAmountCol = ($currency_data[$currencyCol]!=""?$currency_data[$currencyCol]:0);
															$currExchForeign = ($arrCurrExch[$i]["foreignAmount"]!=""?$arrCurrExch[$i]["foreignAmount"]:0);
												?>
															 <div style='float:left;'><?=$currencyCol;?>&nbsp;</div>
															 <div class="windowTotal2" id="currency<?=$currencyCol;?>Coll">0.00</div>
 															<div class="clear">&nbsp;</div>
 												<? 		}
 													 }
												   }
												   else{
												?>
													 <div class="windowTotal">0.00</div>
											   <?	}?>												</td>
											</tr>
										<? if($showChequeCreatedFlag){ ?>
											<tr>
												<td width="50%" class="heading">Cheque Created</td>
												<td class="heading"><div class="windowTotal" id="orderCreatedColl">0.00</div></td>
											</tr>
										<? }?>
											<tr>
												<td width="50%" class="heading">Cheque Paid</td>
												<td class="heading"><div class="windowTotal" id="orderPaidColl">0.00</div></td>
											</tr>
										</table>									</td>
								</tr>
							</tbody>
						</table>
						<table width="33%" cellspacing="0" cellpadding="0" border="1" align="left">
							<tbody>
								<tr>
									<td colspan="3">
										<table width="100%" cellspacing="0" cellpadding="2" border="0" align="left">
											<th align="center" colspan="2" bgcolor="#CCCCCC">Over/Short</th>
									<? if($bankTransFlag){?>
											<tr>
												<td width="50%" class="heading">Bank Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankTransOS">0.00</div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Cash (GBP)</td>
												<td class="heading"><div class="windowTotal" id="bankCashOS">0.00</div></td>
											</tr>
									<? if($chequeTransFlag){?>
											<tr>
												<td width="50%" class="heading">Cheque Transactions</td>
												<td class="heading"><div class="windowTotal" id="bankChequeOS">0.00</div></td>
											</tr>
									<? }?>
											<tr>
												<td width="50%" class="heading" >Currency</td>
												<td class="heading">
												<? if(count($arrCurrExch)>0){
													for($i=0;$i<count($arrCurrExch);$i++){
												?>
													 <div style='float:left;'><?=$arrCurrExch[$i]["currency"];?>&nbsp;</div>
													 <div class="windowTotal2" id="currency<?=$arrCurrExch[$i]["currency"]?>OS">0.00</div>
													<div class="clear">&nbsp;</div>
												<? 	}
												   }
												   else{
												?>
													 <div class="windowTotal">0.00</div>
											   <?	}?>												</td>
											</tr>
										<? if($showChequeCreatedFlag){ ?>
											<tr>
												<td width="50%" class="heading">Cheque Created</td>
												<td class="heading"><div class="windowTotal" id="orderCreatedOS">0.00</div></td>
											</tr>
										<? }?>
											<tr>
												<td width="50%" class="heading">Cheque Paid</td>
												<td class="heading"><div class="windowTotal" id="orderPaidOS">0.00</div></td>
											</tr>
										</table>									</td>
								</tr>
							</tbody>
						</table>
					 </center>					</td>
				</tr>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
		<? }?>
				<tr>
					<td class="heading" colspan="5">
						Notes:<br />
						<center>
							<textarea name="data[notes]" cols="50" rows="8" wrap="soft" <?=$readOnlyHtml?>><?=$currency_data["notes"]?></textarea>
						</center>					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
				<tr>
					<td width="50%" align="right" class="reportToolbar">
						<input name="id" type="hidden" id="id" value="<?=$id?>" />
						<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
						<input name="userId" type="hidden" id="userId" value="<?=$userId?>" />
						<input name="totalGbpHd" type="hidden" id="totalGbpHd" value="" />
						<input name="totalTillHd" type="hidden" id="totalTillHd" value="" />
						<? if($saveTempFlag){?>
							<input name="currentDate" type="hidden" id="currentDate" value="<?=$latestUpdatesOn;?>" />
						<? }?>
						<? if($allowUpdate == "Y") { ?>
							<? if($saveTempFlag){?>
								<div style="float:left; display:inline;"><input type="submit" name="Submit" value="Save Temporarily" /></div>
							<? }?>
							<input type="submit" name="Submit" value="Submit" />
							<input type="reset" name="Submit2" value="Reset" />
						<? } ?>
					</td>
				</tr>
			</form>
		</td>
	</tr>
</table>
</body>
</html>
