<?php
include ("../include/config.php");
include ("security.php");
/**
 * @var $agent \Payex\Model\Agent
 */
$agent = $app->getAgentRepository()->getById($_SESSION['loggedUserData']['userID']);
$agentType = getAgentType();
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$str_footer = '';
if(defined("CONFIG_ADD_REGULATIONS_COMPANY"))
	$str_footer = CONFIG_ADD_REGULATIONS_COMPANY;

$str_footer = $app->getUiHelper()->getFooterCompanyRegNumberString($agent->getAgentCompanyRegNumber());

?>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<TABLE cellSpacing=0 cellPadding=0 width="100%"  border=0 bgcolor="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? CONFIG_CUSTOM_COLOR1 : "#666666") ?>" height="100%">
<TBODY>
<TR>
	<TD>
		<TABLE width="100%" cellSpacing=0 cellPadding=0 border=0>
                        <TBODY>
                        <TR>
                        <?php if (empty($str_footer)) {?>
                        	<TD vAlign=bottom width=87> </TD>
                        <?php }?>
                            <TD vAlign=bottom align=center>
								<TABLE cellSpacing=0 cellPadding=0 border=0>
                                <TBODY>
                                <TR>

                      <TD >&nbsp;</TD>

                      <TD class="nrm01" align="center"><NOBR><FONT color=#ceedff>
                 		<?	if (CONFIG_ADD_SHORTCUT == "1" && CONFIG_LEFT_FUNCTION == "1") {  ?>
                      		<a href="shortcuts.php" target="mainFrame">My Shortcuts</a>
                      	|
                  	<?	}  ?>
                      		<a href="feedback.php" target="mainFrame">Client Feedback</a>
                        | <a href="faqs-list.php?sid=<?=$sid?>" target="mainFrame">FAQs</a>
                        | <a href="chpass.php?sid=<?=$sid?>" target="mainFrame">Change Password</a>
                        <?
                       // echo $agentType." Type is";

                        if($agentType == 'admin'|| $agentType == 'SUPI'|| $agentType == 'SUPA')
                        {
	                        ?>
	                        | <a href="upload-logo.php" target="mainFrame">Upload
	                        Company Logo</a>
	                        <?
                    	}
                        ?>
                         | <A
                                href="signout.php" target="_parent" class=link01>Logout</A></FONT></NOBR>
                       <?php if (!empty($str_footer)) {
                           $str_footer = '<font color="#ffffff"><br>' . $str_footer . '</font>';
                       }
                       echo $str_footer;
                       ?>
                        <font color="#ffffff"><br>
                        </font><font color="#ffffff">&copy; Copyright 2004-<?=date(Y)?>
                        <!--<a href="javascript:;" class="style3" onClick="window.open('http://payscanner.com','','scrollbars=yes,toolbar=yes,location=yes,directories=no,status=yes,menubar=yes,resizable=yes,height=420,width=740')">Payex</a>-->
                        <? echo $company;?> powered by
                        <a href="javascript:;" class="style3" onClick=" window.open('http://payscanner.com','', 'scrollbars=yes,toolbar=yes,location=yes,directories=no,status=yes,menubar=yes,resizable=yes,height=420,width=740')">Payscanner</a> <? echo releaseNumber(); ?>
                        </font></TD>

                                  </TR>

                                  </TBODY>
                                </TABLE></TD>
                            </TR>
                          </TBODY>
                        </TABLE></TD>
    </TR>
  </TBODY>
</TABLE>
