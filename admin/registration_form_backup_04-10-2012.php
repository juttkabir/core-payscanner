<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>

		<title>Private Client Registration Form - Premier FX Online Currency Transfers, Algarve, Portugal</title>

		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading, Premier FX, Algarve, Portugal, www.premfx.com" />

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<meta http-equiv="imagetoolbar" content="no">

		<link href="css/style.css" rel="stylesheet" type="text/css">

		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="jquery.js"></script>

		<script type="text/javascript" src="./javascript/jquery.validate.js"></script>

		<script type="text/javascript" src="./javascript/date.js"></script>

		<script type="text/javascript" src="./javascript/jquery.datePicker.js"></script>

		<script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>

		<script type="text/javascript" src="./javascript/ajaxupload.js"></script>
		
	</head>

	<body>

				<div id="wait" style="position:absolute;height:390%;width:100%;background:#222;opacity:0.7;display:none;">

					<div style="margin: 0 auto;position: relative;top: 50%;width: 300px;font-size:48px;color:#fff;font-family:arial">

						LOADING...

					</div>

				</div>

				<div id="body">

					<div id="main">

						<div id="page">

							<div id="cntnt">

								<div id="hdr">

				<div id="login">

					<a href="#nl"><img src="images/premier/formImages/shim.gif" style="height:33px; width:140px;"></a>

				</div>

				<ul>

					<li><a href="http://www.premfx.com/index.php" target="_parent">Home</a></li>

					<li><a href="http://www.premfx.com/news.php" target="_parent">News</a></li>

					<li><a href="http://www.premfx.com/testimonials.php" target="_parent">Testimonials</a></li>

					<li><a href="http://www.premfx.com/casestudies.php" target="_parent">Case studies</a></li>

					<li><a href="http://www.premfx.com/faqs.php" target="_parent">FAQs</a></li>

				</ul>

			</div>

			<div id="logo_nav">

				<img id="logo" src="images/premier/formImages/ui/logo_def.png">

				<ul id="nav">

					<li class="l"></li>

					<li class="btn">

						<div>

							<a href="http://www.premfx.com/index.php" target="_parent"><img src="images/premier/formImages/ui/btn_home.gif"></a>

						</div>

					</li>

					<li>

						<a href="http://www.premfx.com/private_home.php" target="_parent">Private Clients</a>

						<ul id="sub_nav">

							<li class="top"><div><img src="images/premier/formImages/ui/dd/top_arr.png"></div></li>

							<li><div><p><a href="http://www.premfx.com/private_home.php" target="_parent">Overview</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/private_stepbystep.php" target="_parent">Step by step guide</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/private_regpayment.php" target="_parent">Regular payment plans</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/private_overseasprop.php" target="_parent">Overseas property</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/private_emigration.php" target="_parent">Emigration</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/private_typeofcontract.php" target="_parent">Types of contract</a></p></div></li>

							<li><div><p><a href="http://clients.premfx.com/private_registration.php" target="_parent">Open an account</a></p></div></li>

							<li class="btm"><div>&nbsp;</div></li>

						</ul>

					</li>

					<li><a href="http://www.premfx.com/corporate_home.php" target="_parent">Corporate Clients</a>

						<ul id="sub_nav">

							<li class="top"><div><img src="images/premier/formImages/ui/dd/top_arr.png"></div></li>

							<li><div><p><a href="http://www.premfx.com/corporate_home.php" target="_parent">Overview</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/corporate_stepbystep.php" target="_parent">Step by step guide</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/corporate_openaccount.php" target="_parent">Open an account</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/corporate_typeofcontract.php" target="_parent">Types of contract</a></p></div></li>

							<li class="btm"><div>&nbsp;</div></li>

						</ul>

					</li>

					<li><a href="http://www.premfx.com/introducing_home.php" target="_parent">Introducing Agents</a></li>

					<li><a href="http://www.premfx.com/aboutus_home.php" target="_parent">About Us</a>

						<ul id="sub_nav">

							<li class="top"><div><img src="images/premier/formImages/ui/dd/top_arr.png"></div></li>

							<li><div><p><a href="http://www.premfx.com/aboutus_home.php" target="_parent">Overview</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/aboutus_regulation.php" target="_parent">Regulation and compliance</a></p></div></li>

							<li><div><p><a href="http://www.premfx.com/aboutus_security.php" target="_parent">Security</a></p></div></li>

							<li class="btm"><div>&nbsp;</div></li>

						</ul></li>

					<li><a href="http://www.premfx.com/contactus.php" target="_parent">Contact us</a></li>

					<li class="r"></li>

				</ul>

			</div>					<div>

									<div id="box_sml" class="l">

										<div class="top">

											<img src="images/premier/formImages/shim.gif">

										</div>

										<div class="cntnt">

											<h1><a href="http://www.premfx.com/private_home.php" target="_parent">Private Clients</a></h1>

			<ul id="l_nav">

				<li><a href="http://www.premfx.com/private_home.php" target="_parent">Overview</a></li>

				<li><a href="http://www.premfx.com/private_stepbystep.php" target="_parent">Step by step guide</a></li>

				<li><a href="http://www.premfx.com/private_regpayment.php" target="_parent">Regular payment plans</a></li>

				<li><a href="http://www.premfx.com/private_overseasprop.php" target="_parent">Overseas property</a></li>

				<li><a href="http://www.premfx.com/private_emigration.php" target="_parent">Emigration</a></li>

				<li><a href="http://www.premfx.com/private_typeofcontract.php" target="_parent">Types of contract</a></li>

				<li><a href="http://www.premfx.com/private_openaccount.php" target="_parent">Open an account</a></li>

			</ul>								<hr>

											<div>

												<div style="background:url(images/premier/formImages/ico_phone_sml_l.png) left top no-repeat; margin:0px 5px 0px -10px; padding:0px 0px 0px 50px;">

													<h2 style="font-size:14px; margin:0px 0px 5px 0px;">United Kingdom*</h2>

													<h2 style="font-size:13px; color:#9d9d98; margin:0px 0px 10px 0px;">+44 (0)845 021 2370</h2>

													<h2 style="font-size:14px; margin:0px 0px 5px 0px;">Portugal</h2>

													<h2 style="font-size:13px; color:#9d9d98; margin:0px 0px 10px 0px;">+351 289 358 511</h2>

													<h2 style="font-size:14px; margin:0px 0px 5px 0px;">Spain</h2>

													<h2 style="font-size:13px; color:#9d9d98; margin:0px 0px 10px 0px;">+34 971 576 724</h2>

												</div>

												<p style="font-size:12px; margin:0px;">* UK calls charged at local rate</p>

											</div>

										</div>

										<div class="btm">

											<img src="images/premier/formImages/shim.gif">

										</div>

										<div style="margin:5px;">

											<h1 class="nm" style="margin:0px 0px 20px 0px;">What our customers say about us...</h1>

											<h2 style="margin-bottom:5px;">Confidence</h2>

											<p style="margin-bottom:5px;">"I am far more confident with my transactions now, knowing that you are dealing with them"</p>

											<p class="ylw" style="font-weight:700; margin-bottom:30px;">C King - Bristol</p>



											<h2 style="margin-bottom:5px;">Very simple</h2>

											<p style="margin-bottom:5px;">"They made a subject I never quite understood very simple and have saved me money as well. A very professional service".</p>

											<p class="ylw" style="font-weight:700; margin-bottom:30px;">A Monk - Bowden, Cheshire</p>



											<h2 style="margin-bottom:5px;">First class</h2>

											<p style="margin-bottom:5px;">"A painless, efficient service. Premier FX's attitude and professionalism was first class".</p>

											<p class="ylw" style="font-weight:700; margin-bottom:30px;">Mr &amp; Mrs P Gordon - Dubai</p>

										</div>

									</div>

									<div class="body_text l">

										<div style="margin:0px 0px 30px 0px; width:680px;">

											<div style="background:url(images/premier/formImages/hdr/private.png) left top no-repeat; height:170px; left:-5px; margin:0px 0px 30px 0px; position:relative; padding:20px 0px 0px 25px; width:665px;">

												<div style="width:360px;">

													<h1>We take the mystery out of currency exchange for you</h1>

													<h3 class="gry">Saving you time, money, and stress.</h3>

													<h3 class="gry">And get you competitive exchange rates...</h3>

													<h3>...better than banks</h3>

												</div>

											</div>

										</div>

										<div class="l" style="width:680px;">

											<h1>Open an account: private client</h1>

											<p>Please complete the form below and upload one form of identification to comply with UK money laundering regulations, we require either:</p>

											<ul>

											<li>a valid Passport</li>

											<li>or a valid National Identity Document</li>

											</ul>

											<div class="enq_form">

												<p>Fields marked with a <sup>*</sup> are required and must be completed.</p>

												<form name="senderRegistrationForm" id="senderRegistrationForm" action="" method="post" style="overflow:hidden;">

													<div class="row">

														<div class="col c10">

															<label>Title<sup>&nbsp;</sup></label>

															<select name="title">

																<option value="Mr.">Mr.</option>

																<option value="Mrs">Mrs</option>

																<option value="Miss.">Miss.</option>

																<option value="Dr.">Dr.</option>

																<option value="Prof.">Prof.</option>

																<option value="Eng.">Eng.</option>

																<option value="Herr">Herr</option>

																<option value="Frau">Frau</option>

																<option value="Ing.">Ing.</option>

																<option value="Mag.">Mag.</option>

																<option value="Sr.">Sr.</option>

																<option value="Sra.">Sra.</option>

																<option value="Srta.">Srta.</option>

															</select>

														</div>

														<div class="col c45">

															<label>Forename <sup>*</sup></label>

															<input name="forename" type="text" id="forename" />

														</div>

														<div class="col nom c45">

															<label>Surname <sup>*</sup></label>

															<input name="surname" type="text" id="surname" />

														</div>

													</div>

													<div class="row">

														<div class="col c55">

															<label>Email <sup>*</sup></label>

															<input name="email" type="text" id="email" class="required email"/>

															<div id="emailValidator" style="clear:both"></div>

															<input type="hidden" name="ipAddress" value="95.210.228.178"/>

														</div>

														<div class="col c30">

															<label>Date of Birth <sup>*</sup></label>

															<select name="dobDay" id="dobDay">

																	<option value="01">01</option>

																	<option value="02">02</option>

																	<option value="03">03</option>

																	<option value="04">04</option>

																	<option value="05">05</option>

																	<option value="06">06</option>

																	<option value="07">07</option>

																	<option value="08">08</option>

																	<option value="09">09</option>

																	<option value="10">10</option>

																	<option value="11">11</option>

																	<option value="12">12</option>

																	<option value="13">13</option>

																	<option value="14">14</option>

																	<option value="15">15</option>

																	<option value="16">16</option>

																	<option value="17">17</option>

																	<option value="18">18</option>

																	<option value="19">19</option>

																	<option value="20">20</option>

																	<option value="21">21</option>

																	<option value="22">22</option>

																	<option value="23">23</option>

																	<option value="24">24</option>

																	<option value="25">25</option>

																	<option value="26">26</option>

																	<option value="27">27</option>

																	<option value="28">28</option>

																	<option value="29">29</option>

																	<option value="30">30</option>

																	<option value="31">31</option>

															</select>

															<select name="dobMonth" id="dobMonth">

																<option value="01">Jan</option>

																<option value="02">Feb</option>

																<option value="03">Mar</option>

																<option value="04">Apr</option>

																<option value="05">May</option>

																<option value="06">Jun</option>

																<option value="07">Jul</option>

																<option value="08">Aug</option>

																<option value="09">Sep</option>

																<option value="10">Oct</option>

																<option value="11">Nov</option>

																<option value="12">Dec</option>

															</select>

															<select name="dobYear" id="dobYear">

																<option value="1929">1929</option>

																<option value="1930">1930</option>

																<option value="1931">1931</option>

																<option value="1932">1932</option>

																<option value="1933">1933</option>

																<option value="1934">1934</option>

																<option value="1935">1935</option>

																<option value="1936">1936</option>

																<option value="1937">1937</option>

																<option value="1938">1938</option>

																<option value="1939">1939</option>

																<option value="1940">1940</option>

																<option value="1941">1941</option>

																<option value="1942">1942</option>

																<option value="1943">1943</option>

																<option value="1944">1944</option>

																<option value="1945">1945</option>

																<option value="1946">1946</option>

																<option value="1947">1947</option>

																<option value="1948">1948</option>

																<option value="1949">1949</option>

																<option value="1950">1950</option>

																<option value="1951">1951</option>

																<option value="1952">1952</option>

																<option value="1953">1953</option>

																<option value="1954">1954</option>

																<option value="1955">1955</option>

																<option value="1956">1956</option>

																<option value="1957">1957</option>

																<option value="1958">1958</option>

																<option value="1959">1959</option>

																<option value="1960">1960</option>

																<option value="1961">1961</option>

																<option value="1962">1962</option>

																<option value="1963">1963</option>

																<option value="1964">1964</option>

																<option value="1965">1965</option>

																<option value="1966">1966</option>

																<option value="1967">1967</option>

																<option value="1968">1968</option>

																<option value="1969">1969</option>

																<option value="1970" selected="">1970</option>

																<option value="1971">1971</option>

																<option value="1972">1972</option>

																<option value="1973">1973</option>

																<option value="1974">1974</option>

																<option value="1975">1975</option>

																<option value="1976">1976</option>

																<option value="1977">1977</option>

																<option value="1978">1978</option>

																<option value="1979">1979</option>

																<option value="1980">1980</option>

																<option value="1981">1981</option>

																<option value="1982">1982</option>

																<option value="1983">1983</option>

																<option value="1984">1984</option>

																<option value="1985">1985</option>

																<option value="1986">1986</option>

																<option value="1987">1987</option>

																<option value="1988">1988</option>

																<option value="1989">1989</option>

																<option value="1990">1990</option>

																<option value="1991">1991</option>

																<option value="1992">1992</option>

																<option value="1993">1993</option>

																<option value="1994">1994</option>

																<option value="1995">1995</option>

																<option value="1996">1996</option>

																<option value="1997">1997</option>

															</select>

														</div>

														<div class="col c15">

															<label>Gender<sup>&nbsp;</sup></label>

															<select name="gender">

																<option value="male">Male</option>

																<option value="female">Female</option>

															</select>

														</div>

													</div>

													<div class="row">

														<div class="col c50">

															<label>Country of Birth<sup>&nbsp;</sup></label>

															<select name="birthCountry" id="birthCountry">

																<option value="United Kingdom">United Kingdom</option>

																<option value="Portugal">Portugal</option>

																<option value="Germany">Germany</option>

																<option value="USA">USA</option>

																<option value="Canada">Canada</option>

																<option value="Afghanistan">Afghanistan</option>

																<option value="Albania">Albania</option>

																<option value="Algeria">Algeria</option>

																<option value="Andorra">Andorra</option>

																<option value="Angola">Angola</option>

																<option value="Anguilla">Anguilla</option>

																<option value="Antarctica">Antarctica</option>

																<option value="Antigua and Barbuda">Antigua and Barbuda</option>

																<option value="Argentina">Argentina</option>

																<option value="Armenia">Armenia</option>

																<option value="Aruba">Aruba</option>

																<option value="Australia">Australia</option>

																<option value="Austria">Austria</option>

																<option value="Azerbaijan">Azerbaijan</option>

																<option value="Bahamas">Bahamas</option>

																<option value="Bahrain">Bahrain</option>

																<option value="Bangladesh">Bangladesh</option>

																<option value="Barbados">Barbados</option>

																<option value="Belarus">Belarus</option>

																<option value="Belgium">Belgium</option>

																<option value="Belize">Belize</option>

																<option value="Benin">Benin</option>

																<option value="Bermuda">Bermuda</option>

																<option value="Bhutan">Bhutan</option>

																<option value="Bolivia">Bolivia</option>

																<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>

																<option value="Botswana">Botswana</option>

																<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>

																<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>

																<option value="British Virgin Islands">British Virgin Islands</option>

																<option value="Brunei">Brunei</option>

																<option value="Bulgaria">Bulgaria</option>

																<option value="Burkina Faso">Burkina Faso</option>

																<option value="Burundi">Burundi</option>

																<option value="Cambodia">Cambodia</option>

																<option value="Cameroon">Cameroon</option>

																<option value="Canada">Canada</option>

																<option value="Cape Verde">Cape Verde</option>

																<option value="Cayman Islands">Cayman Islands</option>

																<option value="Central African Republic">Central African Republic</option>

																<option value="Chad">Chad</option>

																<option value="Chile">Chile</option>

																<option value="China">China</option>

																<option value="Christmas Island">Christmas Island</option>

																<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>

																<option value="Colombia">Colombia</option>

																<option value="Comoros">Comoros</option>

																<option value="Congo">Congo</option>

																<option value="Cook Islands">Cook Islands</option>

																<option value="Costa Rica">Costa Rica</option>

																<option value="Cote D'Ivoire">Cote D'Ivoire</option>

																<option value="Croatia">Croatia</option>

																<option value="Cuba">Cuba</option>

																<option value="Cyprus">Cyprus</option>

																<option value="Czech Republic">Czech Republic</option>

																<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>

																<option value="Denmark">Denmark</option>

																<option value="Djiboutio">Djiboutio</option>

																<option value="Dominica">Dominica</option>

																<option value="Dominican Republic">Dominican Republic</option>

																<option value="East Timor">East Timor</option>

																<option value="Ecuador">Ecuador</option>

																<option value="Egypt">Egypt</option>

																<option value="El Salvador">El Salvador</option>

																<option value="Equatorial Guinea">Equatorial Guinea</option>

																<option value="Eritrea">Eritrea</option>

																<option value="Escocia">Escocia</option>

																<option value="Estonia">Estonia</option>

																<option value="Ethiopia">Ethiopia</option>

																<option value="Falkland Islands">Falkland Islands</option>

																<option value="Faroe Islands">Faroe Islands</option>

																<option value="Fiji">Fiji</option>

																<option value="Finland">Finland</option>

																<option value="France">France</option>

																<option value="French Guiana">French Guiana</option>

																<option value="French Polynesia">French Polynesia</option>

																<option value="Gabon">Gabon</option>

																<option value="Gambia">Gambia</option>

																<option value="Georgia">Georgia</option>

																<option value="Germany">Germany</option>

																<option value="Ghana">Ghana</option>

																<option value="Gibraltar">Gibraltar</option>

																<option value="Greece">Greece</option>

																<option value="Greenland">Greenland</option>

																<option value="Grenada">Grenada</option>

																<option value="Guadaloupe">Guadaloupe</option>

																<option value="Guam">Guam</option>

																<option value="Guatemala">Guatemala</option>

																<option value="Guinea">Guinea</option>

																<option value="Guinea-Bissau">Guinea-Bissau</option>

																<option value="Guyana">Guyana</option>

																<option value="Haiti">Haiti</option>

																<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>

																<option value="Honduras">Honduras</option>

																<option value="Hong Kong">Hong Kong</option>

																<option value="Hungary">Hungary</option>

																<option value="Iceland">Iceland</option>

																<option value="India">India</option>

																<option value="Indonesia">Indonesia</option>

																<option value="Iran">Iran</option>

																<option value="Iraq">Iraq</option>

																<option value="Ireland">Ireland</option>

																<option value="Israel">Israel</option>

																<option value="Italy">Italy</option>

																<option value="Jamaica">Jamaica</option>

																<option value="Japan">Japan</option>

																<option value="Jordan">Jordan</option>

																<option value="Kazakhstan">Kazakhstan</option>

																<option value="Kenya">Kenya</option>

																<option value="Kiribati">Kiribati</option>

																<option value="Kitts and Nevis">Kitts and Nevis</option>

																<option value="Kuwait">Kuwait</option>

																<option value="Kyrgyzstan">Kyrgyzstan</option>

																<option value="Laos">Laos</option>

																<option value="Latvia">Latvia</option>

																<option value="Lebanon">Lebanon</option>

																<option value="Lesotho">Lesotho</option>

																<option value="Liberia">Liberia</option>

																<option value="Libya">Libya</option>

																<option value="Liechtenstein">Liechtenstein</option>

																<option value="Lithuania">Lithuania</option>

																<option value="Luxembourg">Luxembourg</option>

																<option value="Macau">Macau</option>

																<option value="Macedonia">Macedonia</option>

																<option value="Madagascar">Madagascar</option>

																<option value="Malawi">Malawi</option>

																<option value="Malaysia">Malaysia</option>

																<option value="Maldives">Maldives</option>

																<option value="Mali">Mali</option>

																<option value="Malta">Malta</option>

																<option value="Marinique">Marinique</option>

																<option value="Marshall Islands">Marshall Islands</option>

																<option value="Mauritania">Mauritania</option>

																<option value="Mauritius">Mauritius</option>

																<option value="Mayotte">Mayotte</option>

																<option value="Mexico">Mexico</option>

																<option value="Micronesia">Micronesia</option>

																<option value="Moldova">Moldova</option>

																<option value="Monaco">Monaco</option>

																<option value="Mongolia">Mongolia</option>

																<option value="Monserrat">Monserrat</option>

																<option value="Morocco">Morocco</option>

																<option value="Mozambique">Mozambique</option>

																<option value="Myanmar">Myanmar</option>

																<option value="Namibia">Namibia</option>

																<option value="Nauru">Nauru</option>

																<option value="Nepal">Nepal</option>

																<option value="Netherlands">Netherlands</option>

																<option value="Netherlands Antilles">Netherlands Antilles</option>

																<option value="New Caledonia">New Caledonia</option>

																<option value="New Zealand">New Zealand</option>

																<option value="Nicaragua">Nicaragua</option>

																<option value="Niger">Niger</option>

																<option value="Nigeria">Nigeria</option>

																<option value="Niue">Niue</option>

																<option value="Norfolk Island">Norfolk Island</option>

																<option value="North Korea">North Korea</option>

																<option value="Northern Mariana Islands">Northern Mariana Islands</option>

																<option value="Norway">Norway</option><option value="Oman">Oman</option>

																<option value="Pakistan">Pakistan</option>

																<option value="Palau">Palau</option>

																<option value="Panama">Panama</option>

																<option value="Papua New Guinea">Papua New Guinea</option>

																<option value="Paraguay">Paraguay</option>

																<option value="Peru">Peru</option>

																<option value="Philippines">Philippines</option>

																<option value="Pitcairn Island">Pitcairn Island</option>

																<option value="Poland">Poland</option>

																<option value="Puerto Rico">Puerto Rico</option>

																<option value="Qatar">Qatar</option>

																<option value="Reunion">Reunion</option>

																<option value="Romania">Romania</option>

																<option value="Russia">Russia</option>

																<option value="Rwanda">Rwanda</option>

																<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>

																<option value="Saint Helena">Saint Helena</option>

																<option value="Saint Lucia">Saint Lucia</option>

																<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>

																<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>

																<option value="Samoa-American">Samoa-American</option>

																<option value="Samoa-Western">Samoa-Western</option>

																<option value="San Marino">San Marino</option>

																<option value="Sao Tome and Principe">Sao Tome and Principe</option>

																<option value="Saudi Arabia">Saudi Arabia</option>

																<option value="Senegal">Senegal</option>

																<option value="Seychelles">Seychelles</option>

																<option value="Sierra Leone">Sierra Leone</option>

																<option value="Singapore">Singapore</option>

																<option value="Slovakie">Slovakie</option>

																<option value="Slovenia">Slovenia</option>

																<option value="Solomon Islands">Solomon Islands</option>

																<option value="Somalia">Somalia</option>

																<option value="South Africa">South Africa</option>

																<option value="South Korea">South Korea</option>

																<option value="Spain">Spain</option>

																<option value="Sri Lanka">Sri Lanka</option>

																<option value="Sudan">Sudan</option>

																<option value="Suriname">Suriname</option>

																<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>

																<option value="Swaziland">Swaziland</option>

																<option value="Sweden">Sweden</option>

																<option value="Switzerland">Switzerland</option>

																<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>

																<option value="Tajikistan">Tajikistan</option>

																<option value="Tanzania">Tanzania</option>

																<option value="Thailand">Thailand</option>

																<option value="Togo">Togo</option>

																<option value="Tokelau">Tokelau</option>

																<option value="Tonga">Tonga</option>

																<option value="Trinidad and Tobago">Trinidad and Tobago</option>

																<option value="Tunisia">Tunisia</option>

																<option value="Turkemenistan">Turkemenistan</option>

																<option value="Turkey">Turkey</option>

																<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>

																<option value="Tuvalu">Tuvalu</option>

																<option value="Uganda">Uganda</option>

																<option value="Ukraine">Ukraine</option>

																<option value="United Araba Emirates">United Araba Emirates</option>

																<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>

																<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>

																<option value="Vatican City">Vatican City</option>

																<option value="Venezuela">Venezuela</option>

																<option value="Vietnam">Vietnam</option>

																<option value="Virgin Islands">Virgin Islands</option>

																<option value="Wallis and Futuna">Wallis and Futuna</option>

																<option value="Western Sahara">Western Sahara</option>

																<option value="Yemen">Yemen</option>

																<option value="Yugoslavia">Yugoslavia</option>

																<option value="Zambia">Zambia</option>

																<option value="Zimbabwe">Zimbabwe</option>

															</select>

														</div>

													</div>

													<div class="row">

														<div class="col c50">

															<label>Telephone - Landline <sup>*</sup></label>

															<input name="landline" type="text" id="landline" />

														</div>

														<div class="col c50">

															<label>Telephone - Mobile<sup>&nbsp;</sup></label>

															<input name="mobile" type="text" id="mobile"/>

														</div>

													</div>

													<div class="shim"></div>

													<div class="row">

														<fieldset>

															<legend>Automatic Address Lookup</legend>

															<div class="row">

																<p>Enter your United Kingdom postcode and click the 'Search for address' button</p>

																<div class="col c33">

																	<label>Post code <sup>*</sup></label>

																	<input id="postCodeSearch"  type="text" onKeyPress='enterToSearch(event);' onFocus="clearAddress();"/>

																</div>

																<div class="col c33">

																	<label>&nbsp;<sup>&nbsp;</sup></label>

																	<input type="button" id="searchAddress" value="Search for address" class="button" />

																</div>

															</div>

															<div class="row">

																<div class="col c50" id="addressContainer">

																	<label>Suggested addresses<br /><small>please choose your address from the list below</small></label>

																	<div id="suggesstions"></div>

																</div>

															</div>

														</fieldset>

														<fieldset>

															<legend>Address</legend>

															<div class="row">

																<p>If your address cannot be found using the automatic lookup above, please enter your address here.</p>

																<div class="col c20">

																	<label>Building Number<sup>&nbsp;</sup></label>

																	<input name="buildingNumber" id="buildingNumber" type="text" onKeyPress='enterToSearch(event);'/>

																</div>

																<div class="col c10">&nbsp;</div>

																<div class="col c60">

																	<label>Building Name<sup>&nbsp;</sup></label>

																	<input name="buildingName" id="buildingName" type="text" onKeyPress='enterToSearch(event);'/>

																</div>

															</div>

															<div class="row">

																<div class="col c50">

																	<label>Street<sup>&nbsp;</sup></label>

																	<input name="street" id="street" type="text" onKeyPress='enterToSearch(event);'/>

																</div>

															</div>

															<div class="row">

																<div class="col c33">

																	<label>Town<sup>&nbsp;</sup></label>

																	<input name="town" id="town" type="text" onKeyPress='enterToSearch(event);'/>

																</div>

															</div>

															<div class="row">

																<div class="col c50">

																	<label>Region<sup>&nbsp;</sup></label>

																	<input name="province" id="province" type="text" onKeyPress='enterToSearch(event);'/>

																</div>

															</div>

															<div class="row">

																<div class="col c33">

																	<label>Postal code<sup>&nbsp;</sup></label>

																	<input name="postcode" id="postcode"  type="text" />

																</div>

															</div>

															<div class="row">

																<div class="col c50">

																	<label>Country<sup>&nbsp;</sup></label>

																	<select name="residenceCountry" id="residenceCountry">

																		<option value="United Kingdom">United Kingdom</option>

																		<option value="Portugal">Portugal</option>

																		<option value="Germany">Germany</option>

																		<option value="USA">USA</option>

																		<option value="Canada">Canada</option>

																		<option value="Afghanistan">Afghanistan</option>

																		<option value="Albania">Albania</option>

																		<option value="Algeria">Algeria</option>

																		<option value="Andorra">Andorra</option>

																		<option value="Angola">Angola</option>

																		<option value="Anguilla">Anguilla</option>

																		<option value="Antarctica">Antarctica</option>

																		<option value="Antigua and Barbuda">Antigua and Barbuda</option>

																		<option value="Argentina">Argentina</option>

																		<option value="Armenia">Armenia</option>

																		<option value="Aruba">Aruba</option>

																		<option value="Australia">Australia</option>

																		<option value="Austria">Austria</option>

																		<option value="Azerbaijan">Azerbaijan</option>

																		<option value="Bahamas">Bahamas</option>

																		<option value="Bahrain">Bahrain</option>

																		<option value="Bangladesh">Bangladesh</option>

																		<option value="Barbados">Barbados</option>

																		<option value="Belarus">Belarus</option>

																		<option value="Belgium">Belgium</option>

																		<option value="Belize">Belize</option>

																		<option value="Benin">Benin</option>

																		<option value="Bermuda">Bermuda</option>

																		<option value="Bhutan">Bhutan</option>

																		<option value="Bolivia">Bolivia</option>

																		<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>

																		<option value="Botswana">Botswana</option>

																		<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>

																		<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>

																		<option value="British Virgin Islands">British Virgin Islands</option>

																		<option value="Brunei">Brunei</option>

																		<option value="Bulgaria">Bulgaria</option>

																		<option value="Burkina Faso">Burkina Faso</option>

																		<option value="Burundi">Burundi</option>

																		<option value="Cambodia">Cambodia</option>

																		<option value="Cameroon">Cameroon</option>

																		<option value="Canada">Canada</option>

																		<option value="Cape Verde">Cape Verde</option>

																		<option value="Cayman Islands">Cayman Islands</option>

																		<option value="Central African Republic">Central African Republic</option>

																		<option value="Chad">Chad</option>

																		<option value="Chile">Chile</option>

																		<option value="China">China</option>

																		<option value="Christmas Island">Christmas Island</option>

																		<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>

																		<option value="Colombia">Colombia</option>

																		<option value="Comoros">Comoros</option>

																		<option value="Congo">Congo</option>

																		<option value="Cook Islands">Cook Islands</option>

																		<option value="Costa Rica">Costa Rica</option>

																		<option value="Cote D'Ivoire">Cote D'Ivoire</option>

																		<option value="Croatia">Croatia</option>

																		<option value="Cuba">Cuba</option>

																		<option value="Cyprus">Cyprus</option>

																		<option value="Czech Republic">Czech Republic</option>

																		<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>

																		<option value="Denmark">Denmark</option>

																		<option value="Djiboutio">Djiboutio</option>

																		<option value="Dominica">Dominica</option>

																		<option value="Dominican Republic">Dominican Republic</option>

																		<option value="East Timor">East Timor</option>

																		<option value="Ecuador">Ecuador</option>

																		<option value="Egypt">Egypt</option>

																		<option value="El Salvador">El Salvador</option>

																		<option value="Equatorial Guinea">Equatorial Guinea</option>

																		<option value="Eritrea">Eritrea</option>

																		<option value="Escocia">Escocia</option>

																		<option value="Estonia">Estonia</option>

																		<option value="Ethiopia">Ethiopia</option>

																		<option value="Falkland Islands">Falkland Islands</option>

																		<option value="Faroe Islands">Faroe Islands</option>

																		<option value="Fiji">Fiji</option>

																		<option value="Finland">Finland</option>

																		<option value="France">France</option>

																		<option value="French Guiana">French Guiana</option>

																		<option value="French Polynesia">French Polynesia</option>

																		<option value="Gabon">Gabon</option>

																		<option value="Gambia">Gambia</option>

																		<option value="Georgia">Georgia</option>

																		<option value="Germany">Germany</option>

																		<option value="Ghana">Ghana</option>

																		<option value="Gibraltar">Gibraltar</option>

																		<option value="Greece">Greece</option>

																		<option value="Greenland">Greenland</option>

																		<option value="Grenada">Grenada</option>

																		<option value="Guadaloupe">Guadaloupe</option>

																		<option value="Guam">Guam</option>

																		<option value="Guatemala">Guatemala</option>

																		<option value="Guinea">Guinea</option>

																		<option value="Guinea-Bissau">Guinea-Bissau</option>

																		<option value="Guyana">Guyana</option>

																		<option value="Haiti">Haiti</option>

																		<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>

																		<option value="Honduras">Honduras</option>

																		<option value="Hong Kong">Hong Kong</option>

																		<option value="Hungary">Hungary</option>

																		<option value="Iceland">Iceland</option>

																		<option value="India">India</option>

																		<option value="Indonesia">Indonesia</option>

																		<option value="Iran">Iran</option>

																		<option value="Iraq">Iraq</option>

																		<option value="Ireland">Ireland</option>

																		<option value="Israel">Israel</option>

																		<option value="Italy">Italy</option>

																		<option value="Jamaica">Jamaica</option>

																		<option value="Japan">Japan</option>

																		<option value="Jordan">Jordan</option>

																		<option value="Kazakhstan">Kazakhstan</option>

																		<option value="Kenya">Kenya</option>

																		<option value="Kiribati">Kiribati</option>

																		<option value="Kitts and Nevis">Kitts and Nevis</option>

																		<option value="Kuwait">Kuwait</option>

																		<option value="Kyrgyzstan">Kyrgyzstan</option>

																		<option value="Laos">Laos</option>

																		<option value="Latvia">Latvia</option>

																		<option value="Lebanon">Lebanon</option>

																		<option value="Lesotho">Lesotho</option>

																		<option value="Liberia">Liberia</option>

																		<option value="Libya">Libya</option>

																		<option value="Liechtenstein">Liechtenstein</option>

																		<option value="Lithuania">Lithuania</option>

																		<option value="Luxembourg">Luxembourg</option>

																		<option value="Macau">Macau</option>

																		<option value="Macedonia">Macedonia</option>

																		<option value="Madagascar">Madagascar</option>

																		<option value="Malawi">Malawi</option>

																		<option value="Malaysia">Malaysia</option>

																		<option value="Maldives">Maldives</option>

																		<option value="Mali">Mali</option>

																		<option value="Malta">Malta</option>

																		<option value="Marinique">Marinique</option>

																		<option value="Marshall Islands">Marshall Islands</option>

																		<option value="Mauritania">Mauritania</option>

																		<option value="Mauritius">Mauritius</option>

																		<option value="Mayotte">Mayotte</option>

																		<option value="Mexico">Mexico</option>

																		<option value="Micronesia">Micronesia</option>

																		<option value="Moldova">Moldova</option>

																		<option value="Monaco">Monaco</option>

																		<option value="Mongolia">Mongolia</option>

																		<option value="Monserrat">Monserrat</option>

																		<option value="Morocco">Morocco</option>

																		<option value="Mozambique">Mozambique</option>

																		<option value="Myanmar">Myanmar</option>

																		<option value="Namibia">Namibia</option>

																		<option value="Nauru">Nauru</option>

																		<option value="Nepal">Nepal</option>

																		<option value="Netherlands">Netherlands</option>

																		<option value="Netherlands Antilles">Netherlands Antilles</option>

																		<option value="New Caledonia">New Caledonia</option>

																		<option value="New Zealand">New Zealand</option>

																		<option value="Nicaragua">Nicaragua</option>

																		<option value="Niger">Niger</option>

																		<option value="Nigeria">Nigeria</option>

																		<option value="Niue">Niue</option>

																		<option value="Norfolk Island">Norfolk Island</option>

																		<option value="North Korea">North Korea</option>

																		<option value="Northern Mariana Islands">Northern Mariana Islands</option>

																		<option value="Norway">Norway</option><option value="Oman">Oman</option>

																		<option value="Pakistan">Pakistan</option>

																		<option value="Palau">Palau</option>

																		<option value="Panama">Panama</option>

																		<option value="Papua New Guinea">Papua New Guinea</option>

																		<option value="Paraguay">Paraguay</option>

																		<option value="Peru">Peru</option>

																		<option value="Philippines">Philippines</option>

																		<option value="Pitcairn Island">Pitcairn Island</option>

																		<option value="Poland">Poland</option>

																		<option value="Puerto Rico">Puerto Rico</option>

																		<option value="Qatar">Qatar</option>

																		<option value="Reunion">Reunion</option>

																		<option value="Romania">Romania</option>

																		<option value="Russia">Russia</option>

																		<option value="Rwanda">Rwanda</option>

																		<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>

																		<option value="Saint Helena">Saint Helena</option>

																		<option value="Saint Lucia">Saint Lucia</option>

																		<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>

																		<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>

																		<option value="Samoa-American">Samoa-American</option>

																		<option value="Samoa-Western">Samoa-Western</option>

																		<option value="San Marino">San Marino</option>

																		<option value="Sao Tome and Principe">Sao Tome and Principe</option>

																		<option value="Saudi Arabia">Saudi Arabia</option>

																		<option value="Senegal">Senegal</option>

																		<option value="Seychelles">Seychelles</option>

																		<option value="Sierra Leone">Sierra Leone</option>

																		<option value="Singapore">Singapore</option>

																		<option value="Slovakie">Slovakie</option>

																		<option value="Slovenia">Slovenia</option>

																		<option value="Solomon Islands">Solomon Islands</option>

																		<option value="Somalia">Somalia</option>

																		<option value="South Africa">South Africa</option>

																		<option value="South Korea">South Korea</option>

																		<option value="Spain">Spain</option>

																		<option value="Sri Lanka">Sri Lanka</option>

																		<option value="Sudan">Sudan</option>

																		<option value="Suriname">Suriname</option>

																		<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>

																		<option value="Swaziland">Swaziland</option>

																		<option value="Sweden">Sweden</option>

																		<option value="Switzerland">Switzerland</option>

																		<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>

																		<option value="Tajikistan">Tajikistan</option>

																		<option value="Tanzania">Tanzania</option>

																		<option value="Thailand">Thailand</option>

																		<option value="Togo">Togo</option>

																		<option value="Tokelau">Tokelau</option>

																		<option value="Tonga">Tonga</option>

																		<option value="Trinidad and Tobago">Trinidad and Tobago</option>

																		<option value="Tunisia">Tunisia</option>

																		<option value="Turkemenistan">Turkemenistan</option>

																		<option value="Turkey">Turkey</option>

																		<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>

																		<option value="Tuvalu">Tuvalu</option>

																		<option value="Uganda">Uganda</option>

																		<option value="Ukraine">Ukraine</option>

																		<option value="United Araba Emirates">United Araba Emirates</option>

																		<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>

																		<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>

																		<option value="Vatican City">Vatican City</option>

																		<option value="Venezuela">Venezuela</option>

																		<option value="Vietnam">Vietnam</option>

																		<option value="Virgin Islands">Virgin Islands</option>

																		<option value="Wallis and Futuna">Wallis and Futuna</option>

																		<option value="Western Sahara">Western Sahara</option>

																		<option value="Yemen">Yemen</option>

																		<option value="Yugoslavia">Yugoslavia</option>

																		<option value="Zambia">Zambia</option>

																		<option value="Zimbabwe">Zimbabwe</option>

																	</select>

																</div>

															</div>

															

														</fieldset>				

													</div>

													<div class="row">

														<p>Please provide either your passport <strong>or</strong> national identity card details</p>

													</div>

													<div class="tab">
														<div class="row">
															<fieldset>
																<legend>Passport Details</legend>
																<div class="row">
																	<div class="col c55">
																		<label>Country of issue <sup>*</sup></label>
																		<select name="passportCountry" id="passportCountry" onChange="passportMask();">
																			<option value="" selected='selected'>-- Select Country of Issue --</option>
																			<option value="United Kingdom">United Kingdom</option>
																			<option value="Portugal">Portugal</option>
																			<option value="USA">USA</option>
																			<option value="Australia">Australia</option>
																			<option value="Spain">Spain</option>
																		</select>
																		<div class="row">
																			<img src="images/premier/formImages/uk_passport.jpg" alt="UK Specimin Passport" width="280px"/><br />
																		</div>
																	</div>
																	<div class="col c45">
																		<label>Passport Upload </label>
																		<a href="javascript:void();" id="file" class="button">Select file</a>
																		<input type="file" name="passportDoc" id="passportDoc" style="visibility:hidden;width:25px;" />
																		<p>Please upload a scanned copy of your passport, the maximum file size is 2Mb. If you are having difficulty uploading the file please post a copy or e-mail us at <a href="mailto:info@premfx.com">info@premfx.com</a> as soon as possible.</p>
																	</div>
																</div>
																<div class="row">	
																	<div class="col c80">
																		<label>Passport Number<br /><small>Please enter your passport number as highlighted above.</small></label>
																		<input name="passportNumber" type="text" size="62" id="passportNumber" disabled="true"/>
																		<div id="passportAvailability"></div>
																	</div>
																</div>
																<div class="col c55">
																	<div class="row">
																		<label>Passport Expiry Date <sup>*</sup></label>
																		<input name="passportExpiry" id="passportExpiry" value="" readonly="readonly" />
																	</div>
																	<div class="row">
																		<label>Issue Date</label>
																		<input name="passportIssue" id="passportIssue" type="text" readonly="readonly" />		
																	</div>
																</div>
															</fieldset>
														</div>
													</div>

													<div class="tab">

														<div class="row">

															<fieldset>

																<legend>National Identity Card Details</legend>

																<div class="row">

																	<div class="col c50">

																		<label>Line1</label>

																		<input name="line1" type="text" id="line1" style="text-transform:uppercase"/>

																	</div>

																	<div class="col c50">

																		<label>Line2</label>

																		<input name="line2" id="line2" type="text" style="text-transform:uppercase"/>

																	</div>

																</div>

																<div class="row">

																	<div class="col c50">

																		<label>Line3</label>

																		<input name="line3" id="line3" type="text" maxlength="30" style="text-transform:uppercase;"/>

																		<div id="NICAvailability"></div>

																	</div>

																	<div class="col c50">

																		<label>Expiry Date <sup>*</sup></label>

																		<input name="idExpiry" id="idExpiry" type="text" readonly="readonly"/>

																	</div>

																</div>

																<div class="row">

																	<div class="col c50">

																		<label>Country of nationality</label>

																		<select name="nationalityCountry" id="nationalityCountry">

																			<option value="" selected='selected'>-- Select Originating Country --</option>

																			<option value="United Kingdom">United Kingdom</option>

																			<option value="Portugal">Portugal</option>

																			<option value="Germany">Germany</option>

																			<option value="USA">USA</option>

																			<option value="Canada">Canada</option>

																			<option value="Afghanistan">Afghanistan</option>

																			<option value="Albania">Albania</option>

																			<option value="Algeria">Algeria</option>

																			<option value="Andorra">Andorra</option>

																			<option value="Angola">Angola</option>

																			<option value="Anguilla">Anguilla</option>

																			<option value="Antarctica">Antarctica</option>

																			<option value="Antigua and Barbuda">Antigua and Barbuda</option>

																			<option value="Argentina">Argentina</option>

																			<option value="Armenia">Armenia</option>

																			<option value="Aruba">Aruba</option>

																			<option value="Australia">Australia</option>

																			<option value="Austria">Austria</option>

																			<option value="Azerbaijan">Azerbaijan</option>

																			<option value="Bahamas">Bahamas</option>

																			<option value="Bahrain">Bahrain</option>

																			<option value="Bangladesh">Bangladesh</option>

																			<option value="Barbados">Barbados</option>

																			<option value="Belarus">Belarus</option>

																			<option value="Belgium">Belgium</option>

																			<option value="Belize">Belize</option>

																			<option value="Benin">Benin</option>

																			<option value="Bermuda">Bermuda</option>

																			<option value="Bhutan">Bhutan</option>

																			<option value="Bolivia">Bolivia</option>

																			<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>

																			<option value="Botswana">Botswana</option>

																			<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>

																			<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>

																			<option value="British Virgin Islands">British Virgin Islands</option>

																			<option value="Brunei">Brunei</option>

																			<option value="Bulgaria">Bulgaria</option>

																			<option value="Burkina Faso">Burkina Faso</option>

																			<option value="Burundi">Burundi</option>

																			<option value="Cambodia">Cambodia</option>

																			<option value="Cameroon">Cameroon</option>

																			<option value="Canada">Canada</option>

																			<option value="Cape Verde">Cape Verde</option>

																			<option value="Cayman Islands">Cayman Islands</option>

																			<option value="Central African Republic">Central African Republic</option>

																			<option value="Chad">Chad</option>

																			<option value="Chile">Chile</option>

																			<option value="China">China</option>

																			<option value="Christmas Island">Christmas Island</option>

																			<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>

																			<option value="Colombia">Colombia</option>

																			<option value="Comoros">Comoros</option>

																			<option value="Congo">Congo</option>

																			<option value="Cook Islands">Cook Islands</option>

																			<option value="Costa Rica">Costa Rica</option>

																			<option value="Cote D'Ivoire">Cote D'Ivoire</option>

																			<option value="Croatia">Croatia</option>

																			<option value="Cuba">Cuba</option>

																			<option value="Cyprus">Cyprus</option>

																			<option value="Czech Republic">Czech Republic</option>

																			<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>

																			<option value="Denmark">Denmark</option>

																			<option value="Djiboutio">Djiboutio</option>

																			<option value="Dominica">Dominica</option>

																			<option value="Dominican Republic">Dominican Republic</option>

																			<option value="East Timor">East Timor</option>

																			<option value="Ecuador">Ecuador</option>

																			<option value="Egypt">Egypt</option>

																			<option value="El Salvador">El Salvador</option>

																			<option value="Equatorial Guinea">Equatorial Guinea</option>

																			<option value="Eritrea">Eritrea</option>

																			<option value="Escocia">Escocia</option>

																			<option value="Estonia">Estonia</option>

																			<option value="Ethiopia">Ethiopia</option>

																			<option value="Falkland Islands">Falkland Islands</option>

																			<option value="Faroe Islands">Faroe Islands</option>

																			<option value="Fiji">Fiji</option>

																			<option value="Finland">Finland</option>

																			<option value="France">France</option>

																			<option value="French Guiana">French Guiana</option>

																			<option value="French Polynesia">French Polynesia</option>

																			<option value="Gabon">Gabon</option>

																			<option value="Gambia">Gambia</option>

																			<option value="Georgia">Georgia</option>

																			<option value="Germany">Germany</option>

																			<option value="Ghana">Ghana</option>

																			<option value="Gibraltar">Gibraltar</option>

																			<option value="Greece">Greece</option>

																			<option value="Greenland">Greenland</option>

																			<option value="Grenada">Grenada</option>

																			<option value="Guadaloupe">Guadaloupe</option>

																			<option value="Guam">Guam</option>

																			<option value="Guatemala">Guatemala</option>

																			<option value="Guinea">Guinea</option>

																			<option value="Guinea-Bissau">Guinea-Bissau</option>

																			<option value="Guyana">Guyana</option>

																			<option value="Haiti">Haiti</option>

																			<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>

																			<option value="Honduras">Honduras</option>

																			<option value="Hong Kong">Hong Kong</option>

																			<option value="Hungary">Hungary</option>

																			<option value="Iceland">Iceland</option>

																			<option value="India">India</option>

																			<option value="Indonesia">Indonesia</option>

																			<option value="Iran">Iran</option>

																			<option value="Iraq">Iraq</option>

																			<option value="Ireland">Ireland</option>

																			<option value="Israel">Israel</option>

																			<option value="Italy">Italy</option>

																			<option value="Jamaica">Jamaica</option>

																			<option value="Japan">Japan</option>

																			<option value="Jordan">Jordan</option>

																			<option value="Kazakhstan">Kazakhstan</option>

																			<option value="Kenya">Kenya</option>

																			<option value="Kiribati">Kiribati</option>

																			<option value="Kitts and Nevis">Kitts and Nevis</option>

																			<option value="Kuwait">Kuwait</option>

																			<option value="Kyrgyzstan">Kyrgyzstan</option>

																			<option value="Laos">Laos</option>

																			<option value="Latvia">Latvia</option>

																			<option value="Lebanon">Lebanon</option>

																			<option value="Lesotho">Lesotho</option>

																			<option value="Liberia">Liberia</option>

																			<option value="Libya">Libya</option>

																			<option value="Liechtenstein">Liechtenstein</option>

																			<option value="Lithuania">Lithuania</option>

																			<option value="Luxembourg">Luxembourg</option>

																			<option value="Macau">Macau</option>

																			<option value="Macedonia">Macedonia</option>

																			<option value="Madagascar">Madagascar</option>

																			<option value="Malawi">Malawi</option>

																			<option value="Malaysia">Malaysia</option>

																			<option value="Maldives">Maldives</option>

																			<option value="Mali">Mali</option>

																			<option value="Malta">Malta</option>

																			<option value="Marinique">Marinique</option>

																			<option value="Marshall Islands">Marshall Islands</option>

																			<option value="Mauritania">Mauritania</option>

																			<option value="Mauritius">Mauritius</option>

																			<option value="Mayotte">Mayotte</option>

																			<option value="Mexico">Mexico</option>

																			<option value="Micronesia">Micronesia</option>

																			<option value="Moldova">Moldova</option>

																			<option value="Monaco">Monaco</option>

																			<option value="Mongolia">Mongolia</option>

																			<option value="Monserrat">Monserrat</option>

																			<option value="Morocco">Morocco</option>

																			<option value="Mozambique">Mozambique</option>

																			<option value="Myanmar">Myanmar</option>

																			<option value="Namibia">Namibia</option>

																			<option value="Nauru">Nauru</option>

																			<option value="Nepal">Nepal</option>

																			<option value="Netherlands">Netherlands</option>

																			<option value="Netherlands Antilles">Netherlands Antilles</option>

																			<option value="New Caledonia">New Caledonia</option>

																			<option value="New Zealand">New Zealand</option>

																			<option value="Nicaragua">Nicaragua</option>

																			<option value="Niger">Niger</option>

																			<option value="Nigeria">Nigeria</option>

																			<option value="Niue">Niue</option>

																			<option value="Norfolk Island">Norfolk Island</option>

																			<option value="North Korea">North Korea</option>

																			<option value="Northern Mariana Islands">Northern Mariana Islands</option>

																			<option value="Norway">Norway</option><option value="Oman">Oman</option>

																			<option value="Pakistan">Pakistan</option>

																			<option value="Palau">Palau</option>

																			<option value="Panama">Panama</option>

																			<option value="Papua New Guinea">Papua New Guinea</option>

																			<option value="Paraguay">Paraguay</option>

																			<option value="Peru">Peru</option>

																			<option value="Philippines">Philippines</option>

																			<option value="Pitcairn Island">Pitcairn Island</option>

																			<option value="Poland">Poland</option>

																			<option value="Puerto Rico">Puerto Rico</option>

																			<option value="Qatar">Qatar</option>

																			<option value="Reunion">Reunion</option>

																			<option value="Romania">Romania</option>

																			<option value="Russia">Russia</option>

																			<option value="Rwanda">Rwanda</option>

																			<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>

																			<option value="Saint Helena">Saint Helena</option>

																			<option value="Saint Lucia">Saint Lucia</option>

																			<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>

																			<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>

																			<option value="Samoa-American">Samoa-American</option>

																			<option value="Samoa-Western">Samoa-Western</option>

																			<option value="San Marino">San Marino</option>

																			<option value="Sao Tome and Principe">Sao Tome and Principe</option>

																			<option value="Saudi Arabia">Saudi Arabia</option>

																			<option value="Senegal">Senegal</option>

																			<option value="Seychelles">Seychelles</option>

																			<option value="Sierra Leone">Sierra Leone</option>

																			<option value="Singapore">Singapore</option>

																			<option value="Slovakie">Slovakie</option>

																			<option value="Slovenia">Slovenia</option>

																			<option value="Solomon Islands">Solomon Islands</option>

																			<option value="Somalia">Somalia</option>

																			<option value="South Africa">South Africa</option>

																			<option value="South Korea">South Korea</option>

																			<option value="Spain">Spain</option>

																			<option value="Sri Lanka">Sri Lanka</option>

																			<option value="Sudan">Sudan</option>

																			<option value="Suriname">Suriname</option>

																			<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>

																			<option value="Swaziland">Swaziland</option>

																			<option value="Sweden">Sweden</option>

																			<option value="Switzerland">Switzerland</option>

																			<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>

																			<option value="Tajikistan">Tajikistan</option>

																			<option value="Tanzania">Tanzania</option>

																			<option value="Thailand">Thailand</option>

																			<option value="Togo">Togo</option>

																			<option value="Tokelau">Tokelau</option>

																			<option value="Tonga">Tonga</option>

																			<option value="Trinidad and Tobago">Trinidad and Tobago</option>

																			<option value="Tunisia">Tunisia</option>

																			<option value="Turkemenistan">Turkemenistan</option>

																			<option value="Turkey">Turkey</option>

																			<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>

																			<option value="Tuvalu">Tuvalu</option>

																			<option value="Uganda">Uganda</option>

																			<option value="Ukraine">Ukraine</option>

																			<option value="United Araba Emirates">United Araba Emirates</option>

																			<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>

																			<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>

																			<option value="Vatican City">Vatican City</option>

																			<option value="Venezuela">Venezuela</option>

																			<option value="Vietnam">Vietnam</option>

																			<option value="Virgin Islands">Virgin Islands</option>

																			<option value="Wallis and Futuna">Wallis and Futuna</option>

																			<option value="Western Sahara">Western Sahara</option>

																			<option value="Yemen">Yemen</option>

																			<option value="Yugoslavia">Yugoslavia</option>

																			<option value="Zambia">Zambia</option>

																			<option value="Zimbabwe">Zimbabwe</option>

																		</select>

																	</div>

																	<div class="col c50">

																		<label>Country of issue</label>

																		<select name="issueCountry" id="issueCountry">

																			<option value="" selected='selected'>-- Select Country of Issue --</option>

																			<option value="United Kingdom">United Kingdom</option>

																			<option value="Portugal">Portugal</option>

																			<option value="Germany">Germany</option>

																			<option value="USA">USA</option>

																			<option value="Canada">Canada</option>

																			<option value="Afghanistan">Afghanistan</option>

																			<option value="Albania">Albania</option>

																			<option value="Algeria">Algeria</option>

																			<option value="Andorra">Andorra</option>

																			<option value="Angola">Angola</option>

																			<option value="Anguilla">Anguilla</option>

																			<option value="Antarctica">Antarctica</option>

																			<option value="Antigua and Barbuda">Antigua and Barbuda</option>

																			<option value="Argentina">Argentina</option>

																			<option value="Armenia">Armenia</option>

																			<option value="Aruba">Aruba</option>

																			<option value="Australia">Australia</option>

																			<option value="Austria">Austria</option>

																			<option value="Azerbaijan">Azerbaijan</option>

																			<option value="Bahamas">Bahamas</option>

																			<option value="Bahrain">Bahrain</option>

																			<option value="Bangladesh">Bangladesh</option>

																			<option value="Barbados">Barbados</option>

																			<option value="Belarus">Belarus</option>

																			<option value="Belgium">Belgium</option>

																			<option value="Belize">Belize</option>

																			<option value="Benin">Benin</option>

																			<option value="Bermuda">Bermuda</option>

																			<option value="Bhutan">Bhutan</option>

																			<option value="Bolivia">Bolivia</option>

																			<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>

																			<option value="Botswana">Botswana</option>

																			<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>

																			<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>

																			<option value="British Virgin Islands">British Virgin Islands</option>

																			<option value="Brunei">Brunei</option>

																			<option value="Bulgaria">Bulgaria</option>

																			<option value="Burkina Faso">Burkina Faso</option>

																			<option value="Burundi">Burundi</option>

																			<option value="Cambodia">Cambodia</option>

																			<option value="Cameroon">Cameroon</option>

																			<option value="Canada">Canada</option>

																			<option value="Cape Verde">Cape Verde</option>

																			<option value="Cayman Islands">Cayman Islands</option>

																			<option value="Central African Republic">Central African Republic</option>

																			<option value="Chad">Chad</option>

																			<option value="Chile">Chile</option>

																			<option value="China">China</option>

																			<option value="Christmas Island">Christmas Island</option>

																			<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>

																			<option value="Colombia">Colombia</option>

																			<option value="Comoros">Comoros</option>

																			<option value="Congo">Congo</option>

																			<option value="Cook Islands">Cook Islands</option>

																			<option value="Costa Rica">Costa Rica</option>

																			<option value="Cote D'Ivoire">Cote D'Ivoire</option>

																			<option value="Croatia">Croatia</option>

																			<option value="Cuba">Cuba</option>

																			<option value="Cyprus">Cyprus</option>

																			<option value="Czech Republic">Czech Republic</option>

																			<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>

																			<option value="Denmark">Denmark</option>

																			<option value="Djiboutio">Djiboutio</option>

																			<option value="Dominica">Dominica</option>

																			<option value="Dominican Republic">Dominican Republic</option>

																			<option value="East Timor">East Timor</option>

																			<option value="Ecuador">Ecuador</option>

																			<option value="Egypt">Egypt</option>

																			<option value="El Salvador">El Salvador</option>

																			<option value="Equatorial Guinea">Equatorial Guinea</option>

																			<option value="Eritrea">Eritrea</option>

																			<option value="Escocia">Escocia</option>

																			<option value="Estonia">Estonia</option>

																			<option value="Ethiopia">Ethiopia</option>

																			<option value="Falkland Islands">Falkland Islands</option>

																			<option value="Faroe Islands">Faroe Islands</option>

																			<option value="Fiji">Fiji</option>

																			<option value="Finland">Finland</option>

																			<option value="France">France</option>

																			<option value="French Guiana">French Guiana</option>

																			<option value="French Polynesia">French Polynesia</option>

																			<option value="Gabon">Gabon</option>

																			<option value="Gambia">Gambia</option>

																			<option value="Georgia">Georgia</option>

																			<option value="Germany">Germany</option>

																			<option value="Ghana">Ghana</option>

																			<option value="Gibraltar">Gibraltar</option>

																			<option value="Greece">Greece</option>

																			<option value="Greenland">Greenland</option>

																			<option value="Grenada">Grenada</option>

																			<option value="Guadaloupe">Guadaloupe</option>

																			<option value="Guam">Guam</option>

																			<option value="Guatemala">Guatemala</option>

																			<option value="Guinea">Guinea</option>

																			<option value="Guinea-Bissau">Guinea-Bissau</option>

																			<option value="Guyana">Guyana</option>

																			<option value="Haiti">Haiti</option>

																			<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>

																			<option value="Honduras">Honduras</option>

																			<option value="Hong Kong">Hong Kong</option>

																			<option value="Hungary">Hungary</option>

																			<option value="Iceland">Iceland</option>

																			<option value="India">India</option>

																			<option value="Indonesia">Indonesia</option>

																			<option value="Iran">Iran</option>

																			<option value="Iraq">Iraq</option>

																			<option value="Ireland">Ireland</option>

																			<option value="Israel">Israel</option>

																			<option value="Italy">Italy</option>

																			<option value="Jamaica">Jamaica</option>

																			<option value="Japan">Japan</option>

																			<option value="Jordan">Jordan</option>

																			<option value="Kazakhstan">Kazakhstan</option>

																			<option value="Kenya">Kenya</option>

																			<option value="Kiribati">Kiribati</option>

																			<option value="Kitts and Nevis">Kitts and Nevis</option>

																			<option value="Kuwait">Kuwait</option>

																			<option value="Kyrgyzstan">Kyrgyzstan</option>

																			<option value="Laos">Laos</option>

																			<option value="Latvia">Latvia</option>

																			<option value="Lebanon">Lebanon</option>

																			<option value="Lesotho">Lesotho</option>

																			<option value="Liberia">Liberia</option>

																			<option value="Libya">Libya</option>

																			<option value="Liechtenstein">Liechtenstein</option>

																			<option value="Lithuania">Lithuania</option>

																			<option value="Luxembourg">Luxembourg</option>

																			<option value="Macau">Macau</option>

																			<option value="Macedonia">Macedonia</option>

																			<option value="Madagascar">Madagascar</option>

																			<option value="Malawi">Malawi</option>

																			<option value="Malaysia">Malaysia</option>

																			<option value="Maldives">Maldives</option>

																			<option value="Mali">Mali</option>

																			<option value="Malta">Malta</option>

																			<option value="Marinique">Marinique</option>

																			<option value="Marshall Islands">Marshall Islands</option>

																			<option value="Mauritania">Mauritania</option>

																			<option value="Mauritius">Mauritius</option>

																			<option value="Mayotte">Mayotte</option>

																			<option value="Mexico">Mexico</option>

																			<option value="Micronesia">Micronesia</option>

																			<option value="Moldova">Moldova</option>

																			<option value="Monaco">Monaco</option>

																			<option value="Mongolia">Mongolia</option>

																			<option value="Monserrat">Monserrat</option>

																			<option value="Morocco">Morocco</option>

																			<option value="Mozambique">Mozambique</option>

																			<option value="Myanmar">Myanmar</option>

																			<option value="Namibia">Namibia</option>

																			<option value="Nauru">Nauru</option>

																			<option value="Nepal">Nepal</option>

																			<option value="Netherlands">Netherlands</option>

																			<option value="Netherlands Antilles">Netherlands Antilles</option>

																			<option value="New Caledonia">New Caledonia</option>

																			<option value="New Zealand">New Zealand</option>

																			<option value="Nicaragua">Nicaragua</option>

																			<option value="Niger">Niger</option>

																			<option value="Nigeria">Nigeria</option>

																			<option value="Niue">Niue</option>

																			<option value="Norfolk Island">Norfolk Island</option>

																			<option value="North Korea">North Korea</option>

																			<option value="Northern Mariana Islands">Northern Mariana Islands</option>

																			<option value="Norway">Norway</option><option value="Oman">Oman</option>

																			<option value="Pakistan">Pakistan</option>

																			<option value="Palau">Palau</option>

																			<option value="Panama">Panama</option>

																			<option value="Papua New Guinea">Papua New Guinea</option>

																			<option value="Paraguay">Paraguay</option>

																			<option value="Peru">Peru</option>

																			<option value="Philippines">Philippines</option>

																			<option value="Pitcairn Island">Pitcairn Island</option>

																			<option value="Poland">Poland</option>

																			<option value="Puerto Rico">Puerto Rico</option>

																			<option value="Qatar">Qatar</option>

																			<option value="Reunion">Reunion</option>

																			<option value="Romania">Romania</option>

																			<option value="Russia">Russia</option>

																			<option value="Rwanda">Rwanda</option>

																			<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>

																			<option value="Saint Helena">Saint Helena</option>

																			<option value="Saint Lucia">Saint Lucia</option>

																			<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>

																			<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>

																			<option value="Samoa-American">Samoa-American</option>

																			<option value="Samoa-Western">Samoa-Western</option>

																			<option value="San Marino">San Marino</option>

																			<option value="Sao Tome and Principe">Sao Tome and Principe</option>

																			<option value="Saudi Arabia">Saudi Arabia</option>

																			<option value="Senegal">Senegal</option>

																			<option value="Seychelles">Seychelles</option>

																			<option value="Sierra Leone">Sierra Leone</option>

																			<option value="Singapore">Singapore</option>

																			<option value="Slovakie">Slovakie</option>

																			<option value="Slovenia">Slovenia</option>

																			<option value="Solomon Islands">Solomon Islands</option>

																			<option value="Somalia">Somalia</option>

																			<option value="South Africa">South Africa</option>

																			<option value="South Korea">South Korea</option>

																			<option value="Spain">Spain</option>

																			<option value="Sri Lanka">Sri Lanka</option>

																			<option value="Sudan">Sudan</option>

																			<option value="Suriname">Suriname</option>

																			<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>

																			<option value="Swaziland">Swaziland</option>

																			<option value="Sweden">Sweden</option>

																			<option value="Switzerland">Switzerland</option>

																			<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>

																			<option value="Tajikistan">Tajikistan</option>

																			<option value="Tanzania">Tanzania</option>

																			<option value="Thailand">Thailand</option>

																			<option value="Togo">Togo</option>

																			<option value="Tokelau">Tokelau</option>

																			<option value="Tonga">Tonga</option>

																			<option value="Trinidad and Tobago">Trinidad and Tobago</option>

																			<option value="Tunisia">Tunisia</option>

																			<option value="Turkemenistan">Turkemenistan</option>

																			<option value="Turkey">Turkey</option>

																			<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>

																			<option value="Tuvalu">Tuvalu</option>

																			<option value="Uganda">Uganda</option>

																			<option value="Ukraine">Ukraine</option>

																			<option value="United Araba Emirates">United Araba Emirates</option>

																			<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>

																			<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>

																			<option value="Vatican City">Vatican City</option>

																			<option value="Venezuela">Venezuela</option>

																			<option value="Vietnam">Vietnam</option>

																			<option value="Virgin Islands">Virgin Islands</option>

																			<option value="Wallis and Futuna">Wallis and Futuna</option>

																			<option value="Western Sahara">Western Sahara</option>

																			<option value="Yemen">Yemen</option>

																			<option value="Yugoslavia">Yugoslavia</option>

																			<option value="Zambia">Zambia</option>

																			<option value="Zimbabwe">Zimbabwe</option>

																		</select>

																	</div>

																</div>

															</fieldset>

														</div>

													</div>

													<div class="row">

														<div class="col c50">

															<label>How did you hear about us?</label>

															<textarea name="heardAboutUs" cols="50" rows="2"></textarea>

														</div>

														<div class="col c50">

															<label>&nbsp;<sup>&nbsp;</sup></label>

															<input type="submit" name="register" value="Register" id="register" class="button right" />

														</div>

													</div>

													<div id="msg" style="text-align:center"></div>

												</form>

											</div>

											<div class="shim"><img src="images/premier/formImages/shim.gif" /></div>

											<p>&nbsp;</p>

											<h3 class="ylw">If you do not wish to register online:</h3>

											<p><a href="http://www.premfx.com/downloads/AccountOpening_Private.doc"><strong class="ylw">Print and complete this form</strong></a>, then fax/send it to us at any of the addresses on our contact us page together with a copy of your passport and a recent utility bill.</p>

											<p>If you are acting on behalf of a business please refer to the information in our <a href="http://www.premfx.com/corporate_home.php" target="_parent"><strong class="ylw">corporate clients section</strong></a> or go to our <a href="http://www.premfx.com/downloads/AccountOpeningForm_Private.doc"><strong class="ylw">corporate client application form</strong></a>.</p>

											<p>&nbsp;</p>

										</div>

										<hr>

										<div style="margin:0px 0px 40px 0px; width:680px;">

											<img src="images/premier/formImages/ico_phone_sml_l.png" width="45" height="60" class="l" style="margin:0px 20px 0px 0px;">

											<div class="r" style="margin:0px 0px 0px 0px; width:615px;">

												<h1>If you experience any difficulties using our form,</h1>

												<h3 class="gry" style="line-height:25px;">please contact us, and we will help you.<br>

												Email us on <a href="mailto:info@premfx.com" class="ylw"><strong>info@premfx.com</strong></a> or call<br>

												<strong class="ylw">UK 0845 021 2370</strong> / <strong class="ylw">Portugal +351 289 358 511</strong> / <strong class="ylw">Spain +34 971 576 724</strong></h3>

											</div>

											<div class="shim"><img src="images/premier/formImages/shim.gif" /></div>

									  </div>

									</div>

									<div class="shim"><img src="images/premier/formImages/shim.gif" /></div>

									<a name="nl"></a>

			<div style="padding:0px 40px 40px 40px;">

				<div id="nl">

					<p class="l">SIGN UP TO OUR NEWSLETTER</p>

					<input class="l" type="text" id="nl_email" name="nl_email" value="Enter your email address here...">

					<div id="nl_signup" class="btn r">

						<p>CONTINUE</p>

						<img src="images/premier/formImages/ui/btn_nl.png" style="width:115px; height:60px;">

					</div>

				</div>

			</div>					</div>

							</div>

						</div>

						<div id="ftr">

				<div id="cntnt">

					<div id="sitemap">

						<ul>

							<li class="hdr"><a href="http://www.premfx.com/private_home.php" target="_parent">Private individuals</a></li>

							<li><a href="http://www.premfx.com/private_stepbystep.php" target="_parent">Step by step guide</a></li>

							<li><a href="http://www.premfx.com/private_regpayment.php" target="_parent">Regular payment plans</a></li>

							<li><a href="http://www.premfx.com/private_overseasprop.php" target="_parent">Overseas property</a></li>

							<li><a href="http://www.premfx.com/private_emigration.php" target="_parent">Emigration</a></li>

							<li><a href="http://www.premfx.com/private_typeofcontract.php" target="_parent">Types of contract</a></li>

							<li><a href="http://www.premfx.com/private_openaccount.php" target="_parent">Open an account</a></li>

						</ul>

						<ul>

							<li class="hdr"><a href="http://www.premfx.com/corporate_home.php" target="_parent">Corporate Clients</a></li>

							<li><a href="http://www.premfx.com/corporate_stepbystep.php" target="_parent">Step by step guide</a></li>

							<li><a href="http://www.premfx.com/corporate_openaccount.php" target="_parent">Open an account</a></li>

							<li><a href="http://www.premfx.com/corporate_typeofcontract.php" target="_parent">Types of contract</a></li>

						</ul>

						<ul>

							<li class="hdr"><a href="http://www.premfx.com/introducing_home.php" target="_parent">Introducing agents</a></li>

							<li></li>

							<li class="hdr"><a href="http://www.premfx.com/aboutus_home.php" target="_parent">About us</a></li>

							<li><a href="http://www.premfx.com/aboutus_regulation.php" target="_parent">Regulation and compliance</a></li>

							<li><a href="http://www.premfx.com/aboutus_security.php" target="_parent">Security</a></li>

							<li></li>

						</ul>

						<ul>

							<li class="hdr"><a href="http://www.premfx.com/contactus.php" target="_parent">Contact us</a></li>

							<li class="hdr"><a href="http://www.premfx.com/news.php" target="_parent">News</a></li>

							<li class="hdr"><a href="http://www.premfx.com/testimonials.php" target="_parent">Testimonials</a></li>

							<li class="hdr"><a href="http://www.premfx.com/casestudies.php" target="_parent">Case studies</a></li>

							<li class="hdr"><a href="http://www.premfx.com/faqs.php" target="_parent">FAQs</a></li>

						</ul>

						<div class="shim"><img src="images/premier/formImages/shim.gif" /></div>

					</div>

					<div id="credits">

									<p class="l">&copy; Premier FX 2011-2012. All rights reserved&nbsp;|&nbsp;<a href="http://www.premfx.com/" target="_parent">Terms &amp; Conditions</a></p>

						<div class="btn r" style="margin:16px 0px 0px 0px;overflow:hidden;height:24px;">

							<a href="http://www.iperium.com" target="_blank" alt="powered by iperium" title="powered by iperium"><img src="images/premier/formImages/ui/ip_btn.png"></a>

						</div>

					</div>

				</div>

			</div>

			<div id="cntnt_btm">

				<img src="images/premier/formImages/shim.gif" style="height:10px; width:10px;">

			</div>		</div>

				</div>

			<script>

			var fileValidateFlag = 0;

			$(function() {

				$('#addressContainer').hide();

				var button = $('#file');

				var testUpload = new AjaxUpload(button, {

					action: 'registration_form_conf.php',

					name: 'passportDoc',

					// for single upload this should have 0 (zero appended)

					autoSubmit: false,

					allowedExtensions: ['.jpg', '.png', '.gif', 'jpeg'],

					sizeLimit: 2048,

					// max size  

					onChange: function(file, ext) {

						if ((ext && /^(jpg|png|jpeg|gif)$/i.test(ext))) {

							fileValidateFlag = 1;

							//button.text("Selected");

							button.css({

								"background-color": "#CCCCCC",

								"border": "1px solid #000000",

								"font-weight": "bold"

							});

							button.html('Document Selected');

						} else {

							alert('Please choose a standard image file to upload, JPG or JPEG, PNG or GIF');

							button.css({

								"background-color": "#ECB83A",

								"border": "1px solid #E5A000",

								"font-weight": "normal"

							});

							button.html('Select Document');

						}

					},

					onSubmit: function(file, ext) {

						// If you want to allow uploading only 1 file at time,

						// you can disable upload button

						this.disable();

						$('#wait').fadeIn("slow");

					},

					onComplete: function(file, response) {

						// enable upload button

						this.enable();

						//this.disable();

						$('#emailValidator').html(' ');

						$('#wait').fadeOut("fast");

						alert(response);

					}

				});





				$(document).ready(



				function() {





					// Date Picker

					Date.format = 'dd/mm/yyyy';


					$('#passportExpiry').datePicker({

						clickInput: true,

						createButton: false

					});

					$('#passportIssue').datePicker({

						clickInput: true,

						createButton: false,

						endDate: (new Date()).asString()

					});

					$('#idExpiry').datePicker({

						clickInput: true,

						createButton: false

					});

					$('#passportIssue').dpSetStartDate(' ');





					// Masking

					$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');

					$('#line1').mask('**-***-*********-9-***************');

					$('#line2').mask('999999-9-*-999999-9-aaa-***********');





					// Form Validation 

					$("#senderRegistrationForm").validate({

						rules: {



							forename: {

								required: true

							},

							surname: {

								required: true

							},

							postcode: {

								required: true

							},

							passportNumber: {

								required: function() {

									if ($('#line1').val() == '') return true;

									else return false;

								}

								//,minlength: 37

							},

							line1: {

								required: function() {

									if ($('#passportNumber').val() == '') return true;

									else return false;

								}

							},

							line2: {

								required: function() {

									if ($('#line1').val() == '') return false;

									else return true;

								}

							},

							line3: {

								required: function() {

									if ($('#line2').val() == '' || $('#line1').val() == '') return false;

									else return true;

								}

							},

							idExpiry: {

								required: function() {

									if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;

									else return false;

								}

							},

							passportExpiry: {

								required: function() {

									if ($('#passportNumber').val() != '') return true;

									else return false;

								}

							}

						},

						messages: {

							passportNumber: "Please provide your Passport or National Identity card number",

							line1: "Please provide your Passport or National Identity card number",

							line2: "Please enter the remaining part of your National Identity card number",

							line3: "Please enter the remaining part of your National Identity card number",

							passportExpiry: "Please enter the expiry date of your Passport"

						},

						submitHandler: function() {

							register();

							return false;

						}

					});



					// Validating Email availability checks

					$("#email").blur(



					function() {

						var email = $('#email').val();



						if (email != '') {

							$.ajax({

								url: "registration_form_conf.php",

								data: {

									email: email,

									chkEmailID: '1'

								},

								type: "POST",

								cache: false,

								success: function(data) {

									$('#emailValidator').html(data);

								}

							});

						}

					});



					// Check if the passport availability checks

					$('#passportNumber').blur(



					function() {

						setTimeout(function() {

							passportAvailabilty();

						}, 100);

					});



					function passportAvailabilty() {

						var passport = $('#passportNumber').val();

						if (passport != '') {

							$.ajax({

								url: "registration_form_conf.php",

								data: {

									passportNum: passport,

									chkPassport: '1'

								},

								type: "POST",

								cache: false,

								success: function(data) {

									$('#passportAvailability').html(data);

								}

							});

						}

					}





					// ID card availability checks

					$('#line3').blur(



					function() {

						var idLine1 = $('#line1').val();

						var idLine2 = $('#line2').val();

						var idLine3 = $('#line3').val();

						if (idLine1 != '' && idLine2 != '' && idLine3 != '') {

							var idCard = idLine1 + idLine2 + idLine3;

						} else return false;

						$.ajax({

							url: "registration_form_conf.php",

							data: {

								idCardNumber: idCard,

								chkIDCard: '1'

							},

							type: "POST",

							cache: false,

							success: function(data) {

								$('#NICAvailability').html(data);

							}

						});

					});





					// Ajax Registration of Sender





					function register() {

						//Passport Issue and Expiry Date

						var passportIssue = new Date($('#passportIssue').val());

						var passportExpiry = new Date($('#idExpiry').val());

						if (passportIssue != '') {

							if (passportIssue >= passportExpiry) {

								alert("Your Passport issued date must be before the expiry date.");

								return false;

							}

						}

						// Registration Request

						var data = $('#senderRegistrationForm').serialize();

						//alert(data);

						data += "&register=Register";

						$('#wait').fadeIn("fast");

						$.ajax({

							url: "registration_form_conf.php",

							data: data,

							type: "POST",

							cache: false,

							success: function(msg) {

								if (msg.search(/Sender is registered successfully/i) >= 0) {

									if (fileValidateFlag != 0) {

										fileValidateFlag = 0;

										testUpload.submit();

									}

									resetFormData();

								}

								$('#wait').fadeOut("fast");

								$('#msg').html(msg);

							}

						});

					}



					function resetFormData() {

						$('#addressContainer').fadeOut('fast');

						document.forms[0].reset();

						$('#file').css({

							"background-color": "#ECB83A",

							"border": "1px solid #E5A000",

							"font-weight": "normal"

						});

						$('#file').html('Select Document');

					}



					// Trigger the search address function

					$('#searchAddress').click(



					function() {

						searchAddress();

					});



				});

			});



			// Populate the Address in the fields





			function getAddressData(ele) {

				var value = ele.value;

				var arrAddress = value.split('/');

				var buildingNumber = $.trim(arrAddress[0]);

				var buildingName = $.trim(arrAddress[1]);

				var subBuilding = $.trim(arrAddress[2]);

				var street = $.trim(arrAddress[3]);

				var subStreet = $.trim(arrAddress[4]);

				var town = $.trim(arrAddress[5]);

				//var postcode = $.trim(arrAddress[6]);

				var organization = $.trim(arrAddress[7]);

				var buildingNumberVal = '';

				var buildingNameVal = '';

				var streetValue = '';
				
				var postCode = $('#postCodeSearch').val();

				if (buildingNumber != '') buildingNumberVal += buildingNumber;

				if (buildingName != '') buildingNameVal += buildingName;

				if (subBuilding != '') buildingNameVal += ' ' + subBuilding;



				if (street != '') streetValue += street;

				if (subStreet != '') streetValue += ' ' + subStreet;



				$('#buildingNumber').val(buildingNumberVal);

				$('#buildingName').val(buildingNameVal);

				$('#street').val(streetValue);

				$('#town').val(town);
				$('#postcode').val(postCode);

			}



			// if Press Enter on any field in the address area trigger the search address function





			function enterToSearch(e) {

				if (e.which) {

					keyCode = e.which;

					if (keyCode == 13) {

						e.preventDefault();

						searchAddress();

					}

				}

			}



			// Calls the API for suggessted address





			function searchAddress() {

				$('#residenceCountry').val('United Kingdom');

				$('#addressContainer').fadeOut('fast');

				postcode = $.trim($('#postCodeSearch').val());

				buildingNumber = $.trim($('#buildingNumber').val());

				street = $.trim($('#street').val());

				town = $.trim($('#town').val());

				if (postcode == '') {

					alert("Enter a postcode to search for your address");

					$('#postCodeSearch').focus();

					return;

				}

				$('#wait').fadeIn("fast");

				$.ajax({
					url: "http://<?php echo $_SERVER['HTTP_HOST']; ?>/api/gbgroup/addresslookupCus.php",

					data: {

						postcode: postcode,

						buildingNumber: buildingNumber,

						street: street,

						town: town

					},

					type: "POST",

					cache: false,

					success: function(data) {

						//alert(data.match(/option/i));

						$('#wait').fadeOut("slow");

						if (data.search(/option/i) >= 0) {

							$('#addressContainer').fadeIn('fast');

							$('#suggesstions').html(data);

						} else {

							$('#addressContainer').fadeIn('fast');

							$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');

						}

					}

				});

			}



			// Clear the address section





			function clearAddress() {

				$('#buildingNumber').val('');

				$('#buildingName').val('');

				$('#street').val('');

				$('#town').val('');

				$('#province').val('');

			}



			function passportMask() {

				passportCountry = $('#passportCountry').val();

				switch (passportCountry) {

				case 'United Kingdom':

					$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<<<<<<<<<<<<<<-9-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'Portugal':

					$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-**********<<<<-9-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'USA':

					$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<<<<<<<<<<<<<<-9-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'Australia':

					$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-<*********<<<<-9-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'Spain':

					$('#passportNumber').unmask().mask('999999999-9-aaa-9999999-a-9999999-***********<<<-9-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				default:

					alert('Please select a country and enter your passport number');

					break;

				}



			}



			</script>
	</body>

</html>

