<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	
	
	$loggedUserID = $_SESSION["loggedUserData"]["userID"];
	$agentType = getAgentType();
	if($agentType == "Admin")
		$agentType = "AdminS";
	/**
	 * function calculateChequeOrderFee
	 * To calculate the cheque order fee, the following function defines the logic
	 *
	 * @param float $fltAmount		The amount against which the fee required to be calculated
	 * @param int $intCompany		The company of the cheque from which it is issues
	 * @return int 	$inFeeId		The id of the which is selected, from conditions
	 * Calculation logic is as below
	 * 
	 * 1) Look for comany based fee with data range (latest one)
	 * 2) Simple company based (latest one)
	 * 3) Non company based fee, on range of amount (latest one)
	 * 4) Non company based fee without range (latest one)
	 * All of these will be searched on the latest updated date
	 * 
	 */
	function calculateChequeOrderFee($fltAmount, $intCompany)
	{
		/* query to look for company based fee having the amount range also */
		$strFeeSql[] = "select 
							fee_id 
					  from 
					  		cheque_order_fee
					  where
					  		type = 'C' and
							type_value = '".$intCompany."' and
							amount_from <= '".$fltAmount."' and
							amount_upto >= '".$fltAmount."'
					 order by
					 		updated_on desc limit 1
						";
						
		/* query to look for company based fee without the specific range */
		$strFeeSql[] = "select 
							fee_id 
					  from 
					  		cheque_order_fee
					  where
					  		type = 'C' and
							type_value = '".$intCompany."' and
							amount_from <= '".$fltAmount."' and
							amount_upto = '0.00'
					 order by
					 		updated_on desc limit 1
						";
		/* Query to look for simple about the amount range */
		$strFeeSql[] = "select 
							fee_id 
					  from 
					  		cheque_order_fee
					  where
					  		type != 'C' and
							amount_from <= '".$fltAmount."' and
							amount_upto >= '".$fltAmount."'
					 order by
					 		updated_on desc limit 1
						";
		/* this query will executed at last which will select the fee withour any range */
		$strFeeSql[] = "select 
							fee_id 
					  from 
					  		cheque_order_fee
					  where
					  		type != 'C' and
							amount_from <= '".$fltAmount."' 
					 order by
					 		updated_on desc limit 1
						";

		
		
		//debug($strFeeSql);
		for($i=0; $i<count($strFeeSql);$i++)
		{
			$intReturnFeeId = checkRecord($strFeeSql[$i]);
			if($intReturnFeeId)
				return $intReturnFeeId;
		}		
		return false;
	}
	
	
	function checkRecord($strSql)
	{
		//debug($strSql);
		$arrCheckResultSet = selectFrom($strSql);
		//debug($arrCheckResultSet);
		if(!empty($arrCheckResultSet["fee_id"]))
			return $arrCheckResultSet["fee_id"];
		else
			return false;
	}
	
	if($_REQUEST["fee"] == "get")
	{
		include ("JSON.php");
		$response = new Services_JSON();

		if(!empty($_REQUEST["amount"]) && !empty($_REQUEST["company"]))
		{
			$feeId = calculateChequeOrderFee($_REQUEST["amount"], $_REQUEST["company"]);
			if($feeId)
			{
				$arrFullFeeData = selectFrom("select fee_id, type, fee from cheque_order_fee where fee_id='".$feeId."'");
				$fee = 0;
				if($arrFullFeeData["type"] == "P")
					$fee = $_REQUEST["amount"] * $arrFullFeeData["fee"] / 100;
				elseif($arrFullFeeData["type"] == "C")
					$fee = $arrFullFeeData["fee"];
				else
					$fee = $arrFullFeeData["fee"];

			if(CONFIG_ROUND_FEE_ENABLED_CHEQ_CASH=="1"){
				if(defined("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH") && is_numeric(CONFIG_ROUND_FEE_TO_CHEQ_CASH)){
					$fee = round($fee,CONFIG_ROUND_FEE_TO_CHEQ_CASH);
				}
			}
				$arrFullFeeData["falf"] = "$fee";
				
				echo $response->encode($arrFullFeeData); 
				
				exit;
			}
		}
		/* Any problem in the communication, so print E for error indication */
		echo "E";
		exit;
	}

	
	if($_REQUEST["co"] == "a")
	{

		
		do {
			$strCheqRef = strtoupper(uniqid());
			$strCheqRef = substr($strCheqRef, 3);
			
			$arrSameRefNo = selectFrom("select order_id from cheque_order where cheque_ref='".$strCheqRef."'");
			
		}while(!empty($arrSameRefNo["order_id"]));
		
		
		
		$strCreatedBy = $_SESSION["loggedUserData"]["userID"]."|".getAgentType();
		$strNote = "Cheque Order Created:".time();
		$strStatus = "NG";
		
		
		if(empty($_REQUEST["fi"]))
			$strFeeVal = "NULL";
		else
			$strFeeVal = "'".$_REQUEST["fi"]."'";
		
		if(empty($_REQUEST["manual_rate_reason"]))
			$strReasonNotesVal = "NULL";
		else
			$strReasonNotesVal = "'".$_REQUEST["manual_rate_reason"]."'";
		
		if(!empty($_REQUEST["issue_date"]))
		{
			$arrCID = explode("/", $_REQUEST["issue_date"]);
			$strChequeData = date("Y-m-d", mktime(0,0,0,$arrCID[1],$arrCID[0],$arrCID[2]));
		}
		
		$strOrderInsertSql = "insert into cheque_order
							(customer_id, company_id, cheque_ref, cheque_no, 
							 account_no, bank_name, branch, cheque_date, 
							 cheque_amount, cheque_currency, fee,  
							 paid_amount, status, note, created_by, created_on,
							 fee_id, manual_fee_reason, estimated_date
							)
							values 
							('".$_REQUEST["cusdi"]."', '".$_REQUEST["cmpdi"]."', '".$strCheqRef."', '".$_REQUEST["cheque_no"]."', 
							 '".$_REQUEST["account_no"]."', '".$_REQUEST["bank_name"]."', '".$_REQUEST["branch"]."', '".$strChequeData."', 
							 '".$_REQUEST["cheque_amount"]."', '".$_REQUEST["cheque_currency"]."', '".$_REQUEST["cheque_fee"]."', 
							 '".$_REQUEST["amount_paid"]."', '".$strStatus."', '".$strNote."', '".$strCreatedBy."', '".date("Y-m-d H:i:s")."',
							  ".$strFeeVal.", ".$strReasonNotesVal.", '".$_REQUEST["estimated_date"]."'
							)";
		
		if(insertInto($strOrderInsertSql))
		{
			$newOrderId = mysql_insert_id();
			/*****
				#5342 - Minas Center
						Ledger is maintained for cheque cashing module upon creation.
						PREVIOUSLY IT WAS MAINTAINED UPON CLEARING (#5258)
						to be shown in Admin Account Statement
						by Aslam Shahid
			*****/
			if(CONFIG_CHEQUE_CASHING_ADMIN_LEDGER=="1"){
				if($strStatus == "NG"){
					$chequeNumber = "";
					if($_REQUEST["cheque_no"]!="")
						$chequeNumber = "Cheque Number : ".$_REQUEST["cheque_no"];
						/**
						 * Make Withdraw entry on the ledger of the admin when Cheque Cleared.
						 */
					maintainAdminStaffAccount($_SESSION["loggedUserData"]["userID"],$_REQUEST["amount_paid"],"", "WITHDRAW",$chequeNumber,"CHEQUE ORDER CREATED", $_REQUEST["cheque_currency"],date("Y-m-d"));
				}
			}
			echo $newOrderId;
			activities($_SESSION["loginHistoryID"],"INSERTION",$newOrderId,"cheque_order",$strCheqRef.":New Cheque Order Created");
		}
		else
			echo "E";
		exit;
	}


	if($_REQUEST["get"] == "gd")
	{
		//debug($_REQUEST);	
		/* 	#5083
		 	this is to control if request is from commission report.
			parameter $_REQUEST["mode"] comes from chequeCommissionReport.php
		 	by Aslam Shahid
		 */
		 $chequeOrderType = "cheque_order";
		 $chequeOrderDesc = "Searching on Cheque Orders via report";
		if($_REQUEST["mode"]=="commission"){
			$chequeOrderDesc = "Searching on Cheque Commission via report";
		}
		activities($_SESSION["loginHistoryID"],"SELECT",0,$chequeOrderType,$chequeOrderDesc);
		
		include ("JSON.php");

		$response = new Services_JSON();
		$page = $_REQUEST['page']; // get the requested page 
		$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
		$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
		$sord = $_REQUEST['sord']; // get the direction 
		
		if(!$sidx)
			$sidx = 1;

		$strSql = "select order_id, o.cheque_currency, cheque_ref, o.company_id, cheque_no, created_on, estimated_date, paid_on, cheque_amount, paid_amount, status, name, created_by 
					from cheque_order o, company c
					where c.company_id = o.company_id ";
		if($_REQUEST["mode"]=="commission"){
		$strSql = "select created_by, SUM(cheque_amount) as cheque_amount, SUM(paid_amount) as paid_amount,SUM(fee) as fee
					from cheque_order o, company c
					where c.company_id = o.company_id ";
		}		
		
		if($_REQUEST["Submit"] == "Search")
		{
			$strFilters = "";
			
			if(!empty($_REQUEST["from_date"]))
			{
				$date = explode("/",$_REQUEST["from_date"]);
				$strFilters .= " and created_on >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
			}
			
			if(!empty($_REQUEST["to_date"]))
			{
				$date = explode("/",$_REQUEST["to_date"]);
				$strFilters .= " and created_on <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
			}
			
			if(!empty($_REQUEST["cheque_ref"]))
				$strFilters .= " and cheque_ref = '".$_REQUEST["cheque_ref"]."'";
			
			if(!empty($_REQUEST["name"]))
				$strFilters .= " and name like '%".$_REQUEST["name"]."%'";
				
			if(!empty($_REQUEST["order_status_filter"]))
				$strFilters .= " and status = '".$_REQUEST["order_status_filter"]."'";

			$strSql .=  $strFilters;
		}
		if($_REQUEST["mode"]=="commission"){
			if(!empty($_REQUEST["creator"])){
				$creatorQ = "select userID from admin where username='".$_REQUEST["creator"]."'";
				$arrCreatorData = selectFrom($creatorQ);
				$strSql .= " and created_by LIKE '".$arrCreatorData["userID"]."|%'";
			}
			$strSql .=" GROUP BY created_by";
		}
		$result = mysql_query($strSql) or die(__LINE__.": ".mysql_query());
		$count = mysql_num_rows($result);

		if($count > 0)
		{
			$total_pages = ceil($count / $limit);
		} else {
			$total_pages = 0;
		}
		
		if($page > $total_pages)
		{
			$page = $total_pages;
		}
		
		$start = $limit * $page - $limit; 
		
		if($start < 0)
		{
			$start = 0;
		}
		
		$strSql .= " order by $sidx $sord LIMIT $start , $limit";
		$result = mysql_query($strSql) or die(__LINE__.": ".mysql_error());
	
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $count;
		if($_REQUEST["mode"]=="commission"){
			$totalPaid = 0;
			$totalCommission = 0;
			$totalAmount = 0;
		}
		$i=0;
		while($arrOrderData = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$creator = explode("|",$arrOrderData["created_by"]);
			$creatorQuery = "select username from admin where userid='".$creator[0]."'";
			if(!empty($_REQUEST["creator"])){
				$creatorQuery .=" AND userid='".$creator[0]."'";
			}
			$arrCreatorDetail = selectFrom($creatorQuery);
			
			$strEstimatedDate = "";
			if($arrOrderData["paid_on"] != "0000-00-00 00:00:00" && !empty($arrOrderData["paid_on"]))
				$strEstimatedDate = date("d/m/Y",strtotime($arrOrderData["paid_on"]));
			
			$response->rows[$i]['id'] = $arrOrderData["order_id"];
			// Report for Cheque Reports
			if($_REQUEST["mode"]!="commission"){
				$response->rows[$i]['cell'] = array(
													$arrOrderData["cheque_ref"],
													$arrOrderData["cheque_no"],
													$arrOrderData["name"],
													$arrOrderData["created_on"],
													$arrOrderData["estimated_date"],
													$strEstimatedDate,
													$arrOrderData["cheque_amount"]." ".$arrOrderData["cheque_currency"],
													$arrOrderData["paid_amount"]." ".$arrOrderData["cheque_currency"],
													$arrChequeStatues[$arrOrderData["status"]],
													$arrCreatorDetail["username"]
												);
			}
			// #5083 - Report for Cheque Commissions
			elseif($_REQUEST["mode"]=="commission"){
				$response->rows[$i]['cell'] = array(
													$arrCreatorDetail["username"],
													$arrOrderData["paid_amount"]." ".$arrOrderData["cheque_currency"],
													$arrOrderData["fee"]." ".$arrOrderData["cheque_currency"],
													$arrOrderData["cheque_amount"]." ".$arrOrderData["cheque_currency"],
												);
				$totalPaid += $arrOrderData["paid_amount"];
				$totalCommission += $arrOrderData["fee"];
				$totalAmount += $arrOrderData["cheque_amount"];
			}
			$i++;
		}	
		if($_REQUEST["mode"]=="commission"){
		$response->rows[$i]['cell'] = array(
											"<strong><font style='font-size:16px; color:#000099; text-align:right'>Total Records (".$i.")</font></strong>",
											"<strong><font style='font-size:16px; color:#000099;'>".$totalPaid." ".$arrOrderData["cheque_currency"]."</font></strong>",
											"<strong><font style='font-size:16px; color:#000099;'>".$totalCommission." ".$arrOrderData["cheque_currency"]."</font></strong>",
											"<strong><font style='font-size:16px; color:#000099;'>".$totalAmount." ".$arrOrderData["cheque_currency"]."</font></strong>",
										);
		}
		echo $response->encode($response);
		exit;
	}
	
	
	
	if($_REQUEST["data"] == "order")
	{
	
		//debug($_REQUEST);
		
		if(!empty($_REQUEST["odi"]))
		{
			$arrOrder = selectFrom("select o.*, name from cheque_order o, company c where order_id = '".$_REQUEST["odi"]."' and c.company_id = o.company_id");		
			
			activities($_SESSION["loginHistoryID"],"SELECT",$_REQUEST["odi"],"cheque_order","Open the mini detail of the cheque order.");
			
		?>
		<script type="text/javascript">
		function checkFormOrder(){
			var orderStatusID = document.getElementById("order_status");
			var orderNotesID = document.getElementById("order_note");
		
			<? if(CONFIG_NOTE_STATUS_MANDATORY_CHEQUE_CASHING=="1"){?>
				if(orderStatusID!=null && orderNotesID!=null){
					var orderStatus = orderStatusID.value;
					var orderNotes = orderNotesID.value;
					if(orderStatus=="ST" && orderNotes==""){
						alert("Please Enter the Notes.")				
						return false;
					}
				}
		<? }?>
		}
		</script>
			<form name="edit_order" id="edit_order" action="<?=$_SERVER['PHP_SELF']?>" method="post">
				<table width="100%">
					<tr>
						<td colspan="2" align="center" width="100%" style="color:#666666; font-size:18px; font:Arial, Helvetica, sans-serif; font-weight:bold">
							CHEQUE ORDER: <i><?=$arrOrder["cheque_ref"]?></i> [Cheque Reference No.]
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" width="100%">
							<a href="#" onclick='window.open("/admin/chequeOrderFullDetail.php?eodi="+<?=$_REQUEST["odi"]?>,"CustomerReceipt","location=0,scrollbars=1,width=800,height=700");'>(Show Full Detail)</a>
							&nbsp;&nbsp;
							<a href="javascript:void(0)" onclick='window.open("/admin/chequeReceipt.php?oi=<?=$_REQUEST["odi"]?>","CustomerSmallReceipt","location=0,scrollbars=1,width=300,height=500");'>(Show Customer Receipt)</a>
						</td>
					</tr>

					<tr>
						<td align="center" width="65%">
							<table width="100%">
								<tr>
									<td>Branch<font color="red">*</font></td>
									<td><input type="text" name="branch_name" id="branch_name" value="<?=$arrOrder["branch"]?>" /></td>
									<td>Estimated Payment Date<font color="red">*</font></td>
									<td><input type="text" name="estimated_date" id="estimated_date" size="11" value="<?=$arrOrder["estimated_date"]?>" readonly="" /></td>
								</tr>
								<tr>
									<td>Account<font color="red">*</font></td>
									<td><input type="text" name="account_no" id="account_no" value="<?=$arrOrder["account_no"]?>" /></td>
									<td>Payment Date</td>
									<td><?=$arrOrder["paid_on"] == "0000-00-00 00:00:00"?"Not Yet Paid":$arrOrder["paid_on"]?></td>
								</tr>
								<tr>
									<td>Cheque Value</td>
									<td><?=$arrOrder["cheque_amount"]." ".$arrOrder["cheque_currency"]?></td>
									<td>Value to Pay</td>
									<td><?=$arrOrder["paid_amount"]." ".$arrOrder["cheque_currency"]?></td>
								</tr>
								<tr>
									<td>Status<font color="red">*</font></td>
									<td>
										<? if ($arrOrder["status"] != "ID") { 
											//$bolOptionNotDisplayed = true;
										?>
										<select name="order_status" id="order_status">
											<? 
											foreach($arrChequeStatues as $key => $val) 
											{ 
												if(stristr($arrChequeStatusRight[$key],$agentType) !== false)
												{							
													//$bolOptionNotDisplayed = false;					
													$strSelected = "";
													$feedOptions = true;
													/*#4580
														If transaction selected has Pending status then dont show Paid status in DropDown
														by ashahid
													*/
													if(defined("CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_PENDING_CQ") && CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_PENDING_CQ=="1"){
														if($arrOrder["status"]=="NG" && $key=="ID"){
															$feedOptions = false;
														}
													}
													/*#5027
														If transaction selected has Hold status then dont show 
														Hold, Cancel Request,Cancel,Return status in DropDown
														by ashahid
													*/
													if(defined("CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_STATUSES_CQ") && strstr(CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_STATUSES_CQ,$arrOrder["status"]) && CONFIG_DROPDOWN_PAID_STATUS_DISABLE_FOR_STATUSES_CQ!="0" && $key=="ID"){
														$feedOptions = false;
													}
													if($key == $arrOrder["status"])
														$strSelected = 'selected="selected"';
													if($feedOptions){
											?>
														<option value="<?=$key?>" <?=$strSelected?>><?=$val?></option>
											<? 
													}
												} 
											}
											?>
										</select>
										<? }else{ ?>
											<b>Paid</b>
										<? } ?>
										
										<? /* 
											if($arrOrder["status"] != "ID" && $bolOptionNotDisplayed)
												echo "<b>".$arrChequeStatues[$arrOrder["status"]]."</b>"; 
											*/
										?>
									</td>
									<td>Fee</td>
									<td><?=$arrOrder["fee"]." ".$arrOrder["cheque_currency"]?></td>
								</tr>
							</table>
						</td>

						<td width="35%">
							<b>Order Notes:</b> <br />
							<textarea name="order_note" id="order_note" cols="40" rows="4" wrap="soft"><?=stripslashes($arrOrder["order_note"])?></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="hidden" name="eodi" id="eodi" value="<?=$_REQUEST["odi"]?>" />
							<input type="submit" name="update_order" id="update_order" value="Update Order" onclick="return checkFormOrder();" />
						</td>
					</tr>
				</table>
			</form>
		<?	
		
		}
		exit;
	}
	
	
	if(!empty($_REQUEST["eodi"]))
	{
		//debug($_REQUEST);	
		
		$arrOrder = selectFrom("select note from cheque_order where order_id = '".$_REQUEST["eodi"]."'");		
		
		$strNote = $arrOrder["note"].",updated by:".$_SESSION["loggedUserData"]["userID"]."|".getAgentType()."|".time()."|".$_REQUEST["order_status"];
		
		
		if(!empty($_REQUEST["order_status"]))
		{
			$strStatusSqlEntry = " status = '".$_REQUEST["order_status"]."',	";
			
			if($_REQUEST["order_status"] == "AR")
				$strStatusValue = ", cleared_on = '".date("Y-m-d H:i:s")."' ";
			elseif($_REQUEST["order_status"] == "ID")
				$strStatusValue = ", paid_on = '".date("Y-m-d H:i:s")."', paid_by = '".$_SESSION["loggedUserData"]["userID"]."|".getAgentType()."' ";
		}

		$strUpdateSql = "update 
							cheque_order
						 set
						 	branch = '".$_REQUEST["branch_name"]."',
							estimated_date = '".$_REQUEST["estimated_date"]."',
							account_no = '".$_REQUEST["account_no"]."',
							".$strStatusSqlEntry."	
							order_note = '".addslashes($_REQUEST["order_note"])."',
							updated_on = '".date("Y-m-d H:i:s")."',
							note = '".$strNote."'
							".$strStatusValue."
						 where
						 	order_id = '".$_REQUEST["eodi"]."'
							";
		if(update($strUpdateSql))
		{
			/* If order is paid than enter the related account entry into cheque_account */
			if($_REQUEST["order_status"] == "ID")
			{
				$strInsertSql = "insert into cheque_account 
								 (datetime, order_id, note)
								 values
								 (".CURRENT_TIMESTAMP.", '".$_REQUEST["eodi"]."', 'Cheque Order Paid')
								";				
				//insertInto($strInsertSql);

			}
			/*****
				#5258 - Minas Center
						Ledger is maintained for cheque cashing module upon paid
						to be shown in Admin Account Statement
						Admin Account Summary is maintained for Opening/Closing Balances.
						by Aslam Shahid
			*****/
			if(CONFIG_CHEQUE_CASHING_ADMIN_LEDGER=="1"){
				if($_REQUEST["order_status"] == "ID"){
					$arrOrderLedger = selectFrom("select order_id,cheque_currency,paid_amount,created_by,cheque_no from cheque_order where order_id = '".$_REQUEST["eodi"]."'");		
					$currecyToLedger = $arrOrderLedger["cheque_currency"];
					$amountToLedger = $arrOrderLedger["paid_amount"];
					$createdBy = explode("|",$arrOrderLedger["created_by"]);
					$adminID = $createdBy[0];
					$chequeNumber = "";
					if($arrOrderLedger["cheque_no"]!="")
						$chequeNumber = "Cheque Number : ".$arrOrderLedger["cheque_no"];
						/**
						 * Make Deposit entry on the ledger of the admin when Cheque Paid.
						 */
						maintainAdminStaffAccount($adminID, $amountToLedger,"", "DEPOSIT", $chequeNumber,"CHEQUE ORDER PAID", $currecyToLedger,date("Y-m-d"));
				}
			}
			echo "S";
			
			activities($_SESSION["loginHistoryID"],"UPDATE",$_REQUEST["eodi"],"cheque_order","Cheque Order Updated To:".$arrChequeStatues[$_REQUEST["order_status"]]);
			
		}
		else
			echo "E";
		exit;
	}
?>