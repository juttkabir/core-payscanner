<?
session_start();
include ("../include/config.php");
$date_time = date('d/m/Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}
if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}
$buttonValue = $_GET["transSend"];	
	/**
		When selected multiple transactions from Quick Menu then All transactions are shown
		in line breaks.
		by Aslam Shahid.
	   
	  */	
	if(isset($_REQUEST["checkbox"]) && sizeof($_REQUEST["checkbox"]) > 0)
	{
		for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
		{
			$transID = $_REQUEST["checkbox"][$i];
			$queryTransaction[$i] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			
			if($queryTransaction[$i]["custAgentID"] != '')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[$i]["custAgentID"] . "'";
				$custAgent=$queryTransaction[$i]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}	
		
			$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[$i]["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[$i]["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
		}
			$horizontalLine = true;
	} 
	else{	
		
			if($transID!='')
			{
				$queryTransaction[0] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction[0]["custAgentID"]!='')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[0]["custAgentID"] . "'";
				$custAgent=$queryTransaction[0]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}
		$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[0]["customerID"] . "'";
		$customerContent = selectFrom($queryCust);
		$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[0]["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
}				
		
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
body{
color:#000066;
}
.border {
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
}

.labelFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
.linkFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color:#000000;
}

.tddataFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-variant:normal;
}


.headingFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	
}
.tddataFontsmall {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	vertical-align: baseline;
	}
	

.italics {	
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			}	

.tdsize {
			 height:50px;
			 vertical-align:top;
		   }

			
.tditalics {
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			height:30px;
		   }

			
		}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<?
	for($i=0; $i<sizeof($queryTransaction); $i++)
	{
?>
<table width="100%" border="0"  >
		<tr>
		<td>
		<table width="650" border="0"  align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td width="170" class="headingFont">
			<img src="images/amb_reciept.png" border="0">			</td>
			<td width="391" class="headingFont">
				<div id="ambHeader" style="float:left;"align="center">
					<font size="5">AMB EXCHANGE</font><br/>
					<span style="font-size:11px;">
					87,Edgware Road, Marble Arch, London, W2 2HX<br/>
					Tel: 0207 723 3622, Fax: 0207 723 2984<br/>
					E-mail: info@ambexchange.co.uk, Web: www.ambexchange.co.uk
					</span><br/><br/>
					<span style="font-size:14px;">
					REMITTANCE APPLICATION FORM<br/>
					INSTANT CASH
					</span>
				</div>
			</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table width="650" border="1"  align="center" cellpadding="3" cellspacing="0" style=" border:1px; color:#333333;">
		<tr>
			<td class="tditalics" style="border:none;" colspan="3">Reference # :&nbsp;<span style="font-size: 16px; font-weight:normal;;"><?=$queryTransaction[$i]["refNumberIM"]?></span></td>
			<td width="87" valign="top" class="tditalics" style="border:none;" align="right"> Date:- </td>
			<td width="135" valign="top" class="tddataFont" style="border:none;"><? 
			$transDate = $queryTransaction[$i]["transDate"];
			echo date('d/m/Y H:i:s A',strtotime($transDate));
			?></td>
		</tr>
		<tr>
			<td width="127" class="tditalics">BEN NAME :- </td>
			<td colspan="2" valign="top" class="tddataFont"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
			<td valign="top" class="tditalics">CURRENCY  :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]["currencyTo"];?></td>
		</tr>
		<tr>
			<td colspan="3" class="tditalics">&nbsp;</td>
			<td valign="top" class="tditalics">FC AMOUNT   :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]["localAmount"];?></td>
		</tr>
		<tr>
			<td class="tditalics">Telephone :-</td>
			<td colspan="2" valign="top" class="tddataFont"><?=($benificiaryContent["Phone"]!="" ? $benificiaryContent["Phone"]:"&nbsp;")?></td>
			<td valign="top" class="tditalics">RATE    :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]['exchangeRate'];?></td>
		</tr>
		<tr>
			<td class="tditalics">City :-</td>
			<td width="92" valign="top" class="tddataFont"><? echo $benificiaryContent["City"]?></td>
			<td width="167" valign="top" class="tddataFont"><span class="tditalics">Country :- &nbsp;&nbsp;<? echo $benificiaryContent["Country"]?></span></td>
			<td valign="top" class="tditalics">STG   :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]['transAmount']."&nbsp;".$queryTransaction[$i]["currencyFrom"];?></td>
		</tr>
		<tr>
			<td class="tditalics">APPLICANT :-</td>
			<td colspan="2" valign="top" class="tddataFont"><? echo $customerContent["firstName"] . "&nbsp;" . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
			<td valign="top" class="tditalics">COMM    :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]['IMFee']+$queryTransaction[$i]["bankCharges"];?></td>
		</tr>
		<tr>
			<td class="tditalics">ADDRESS  :-</td>
			<td colspan="2" valign="top" class="tddataFont"><? echo $customerContent["Address"] . "&nbsp;" . $customerContent["Address1"]?></td>
			<td valign="top" class="tditalics">TOTAL    :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]["totalAmount"]." ".$queryTransaction[$i]["currencyFrom"]?></td>
		</tr>
		<tr>
			<td colspan="5" class="tditalics">&nbsp;</td>
			</tr>
		<tr>
			<td class="tditalics">COMPANY NAME : - </td>
			<td colspan="4" valign="top" class="tddataFont">AMB EXCHNANGE</td>
			</tr>
		<tr>
			<td class="tditalics">Telephone :-</td>
			<td colspan="4" valign="top" class="tddataFont"><?=($customerContent["Phone"]!="" ? $customerContent["Phone"]:"&nbsp;")?></td>
		</tr>
		<tr>
			<td class="tditalics">
			<?
			if($queryTransaction[$i]["transType"] == "Bank Transfer"){
				echo "Bank Details";
			}
			elseif($queryTransaction[$i]["transType"] == "Pick up"){
				echo "Pick up Details";
			}
			?>
			 :-</td>
			<td colspan="4" valign="top" class="tddataFont">
					<? 
		if($queryTransaction[$i]["transType"] == "Bank Transfer")
		 {
				$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
				$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
				$queryDistributorBank = "select agentCompany,agentPhone  from " .TBL_ADMIN_USERS. " where  userID  ='" .$queryTransaction[$i]["benAgentID"]. "'";
				$queryExecuteDistributorBank = selectFrom($queryDistributorBank);	
				
								
				if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
				{
					 echo "<strong>IBAN : </strong>".$benBankDetails["IBAN"];
					 echo "<strong>".($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;")."/strong";
					 echo ($benificiaryContent["Country"] == "GEORGIA" ? $_SESSION["ibanRemarks"] : "&nbsp;");
				 }
				 else
				 {
				  ?>
		  		  <span class="tddataFont"> 
				  <?
				  		//debug($benBankDetails);
						echo "<strong>Name :</strong> ".$benBankDetails["bankName"]."&nbsp;";
						//echo $benBankDetails["accNo"]; 
						//echo $benBankDetails["branchCode"];
						
						if(CONFIG_CPF_ENABLED == '1')
						{
						echo  "<strong>CPF Number :</strong> ";
						echo $benificiaryContent["CPF"];
						}		  
												
						echo " <strong>Branch Address :</strong> ".$benBankDetails["branchAddress"]; 
						
						if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
						{
							echo "<br/><strong>Account Type :</strong> ".$benBankDetails["accountType"];
						}
				 ?>		
				</span>		
				<?			
				}
				
		} 
		if($queryTransaction[$i]["transType"] == "Pick up")
		{
		  
				$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction[$i]["collectionPointID"] . "'";
				$cpContent = selectFrom($queryCust);	
		
				$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $cpContent["cp_ida_id"] . "'";
				$queryExecute = selectFrom($queryDistributor);		
				if($cpContent["cp_id"] != "")
				{
				?>	
					<span class="tddataFont">
					<? 
					if (CONFIG_SHOW_DIST_BANK_NAME=="1")
						print "<strong>Distributor Name : </strong> ".$queryExecute["name"]." (".$cpContent["cp_corresspondent_name"].")";
					else
					   	print "<strong>Collection Point Name : </strong> ".$cpContent["cp_corresspondent_name"];
					?>
				    <!--// commented no included in reciept echo $cpContent["cp_contact_person_name"]-->
					  <? echo "<br><strong>Address : </strong> ".$cpContent["cp_branch_address"]." ".$cpContent["cp_city"].", ".$cpContent["cp_country"]?>
					 <? echo "<br/><strong>Phone : </strong>".$cpContent["cp_phone"]."&nbsp;"?>
					 
					 </span>
			  <?
				}
		}
		?>
			</td>
			</tr>
		<tr>
			<td colspan="5" class="headingFont" align="center" style="border-left:none;border-right:none; font-size:18px">TERMS AND CONDITIONS</td>
			</tr>
		<tr>
			<td colspan="5" class="tdsize">
			<?
			$termsConditionSql = selectFrom("SELECT
										company_terms_conditions
								 FROM 
								 		company_detail
								 WHERE 
								 		company_terms_conditions!='' AND 
										dated = (
												 SELECT MAX(dated)
												 FROM 
												 	company_detail 
												WHERE 
													company_terms_conditions!=''
												 )");
			if(!empty($termsConditionSql["company_terms_conditions"]))
					$tremsConditions = $termsConditionSql["company_terms_conditions"];
			
		?>
		<p style="padding:5px;">
		<? echo $tremsConditions; ?>
		</p>
<span id="customerSign" style="float:right; padding-right:80px;" class="tditalics">Customer's Sign : ...........................................</span>			</td>
			</tr>
		<tr>
			<td colspan="5"  class="headingFont" style="padding:2px;" align="center"><span class="headingFont" style="padding:2px;">INSTANT TRANSFERS TO : PAKISTAN,INDIA,DUBAI,HONG KONG,MIDDLE EAST AND FAR EAST</span></td>
		</tr>
		<tr>
			<td colspan="5" class="tditalics" style="border-bottom:none; border-left:none; border-right:none;">
			<span id="approvedBy">Approved By:</span>
			<span id="cashier" style="padding-left:130px;">Cashier:</span>
			<span id="manager" style="padding-left:130px;">Manager:</span>			</td>
			</tr>
</table>
</td>
</tr>
	<tr>
	<td>
	 	<? if($horizontalLine) { ?>
		 <hr noshade="noshade" size="1" />
	<?
	   }
	 } // end for loop
	 ?>			
   <table align="center" width="650" border="0" cellpadding="0"  cellspacing="0">
	<tr>
	<td>
	<div class='noPrint' align="right">
	<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create Transaction</a>
	</div>
	</td>
	</tr>

</table>
</td>
</tr>
</table>
</body>
</html>