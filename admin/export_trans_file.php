<?php
session_start();
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$currentDate = date('d/m/Y');

$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$date_time = date('d-m-Y-h-i-s-A');
$amount2 = 0;
$flagExport = true;
if($_GET["fileID"] != ""){
	$fileID = $_GET["fileID"];
}elseif($_POST["fileID"] != ""){
	$fileID = $_GET["fileID"];
}

if($_GET["fileFormat"] != ""){
	$fileFormat = $_GET["fileFormat"];
}elseif($_POST["fileFormat"] != ""){
	$fileFormat = $_GET["fileFormat"];
}

$condition = "";
$value = "";
if(CONFIG_EXPORT_TRANS_OLD == '1')
{
	if ($_GET['old'] == 'Y') {
		$condition = " and is_exported = 'Y' ";
		$value = "old";
	} else {
		$condition = " and is_exported = '' ";
		$value = "current";
	}
}

//$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");


if(CONFIG_SEPARATE_LINK_FOR_BOTH_TRANSACTIONS == 1 && !empty($_REQUEST["ttype"]))
{
	if($_REQUEST["ttype"] == 1)
		$condition .= " and transType = 'Pick up' ";
	elseif($_REQUEST["ttype"] == 2)
		$condition .= " and transType = 'Bank Transfer' ";
}


if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	
	$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
		
	$condition .= " and transStatus = 'Authorize' ".$FileConditionQuery["conditions"]." ";
}else{
	
	 $condition .= " and benAgentID = '100289'";
	}

//$condition .= " and transStatus = 'Authorize'";

 $queryCnt = "select COUNT(*) from ". TBL_TRANSACTIONS." where 1 $condition ";
$countTrans = countRecords($queryCnt);

$disticntCurrency = selectMultiRecords ("select DISTINCT(currencyTo) from ".TBL_TRANSACTIONS." where 1 $condition");
$curr = explode(',',$disticntCurrency);




if ($countTrans <= 0) {
	if ($value != "") {
		insertError("There is no ".$value." transaction to export.");
	} else {
		insertError("There is no new transaction to export.");
	}

	redirect("main.php?msg=Y");
}

if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
} else {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and File_Name = 'export_trans_file.php'");
}

$lineBreak = $fileFormatQuery["lineBreak"];
$fieldSpacing = $fileFormatQuery["fieldSpacing"];
if ($fieldSpacing == 'tab') {
	$fieldSpace = "\t";	
} else {
	$fieldSpace = " ";
}
$fileFormat = $fileFormatQuery["Format"];
$fileID = $fileFormatQuery["id"];
$fileNamed = $username."_".$date_time."_IN";

	if ($fileFormat == "txt") {
		$appType = "txt";
	} else if ($fileFormat == "csv") {
		$appType = "csv";
	} else {
		$appType = "x-msexcel";	
	}
	
	header ("Content-type: application/$appType");
	header ("Content-Disposition: attachment; filename=$fileNamed.$fileFormat"); 
	header ("Content-Description: PHP/INTERBASE Generated Data");
	
 $query = "select * from ". TBL_TRANSACTIONS." where 1 $condition ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	$query .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";

$contentTrans = selectMultiRecords($query);

if ($fileFormat == "xls")
{

	
	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	
if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
	$data = "<table width='900' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	
	
	
	$data .="<tr>";
	
			$data .= "<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["heading"]."</font>\n";	
		
		
			if($labelQuery["address"] == 'Y'){
		
			$data .= "<font face='Verdana' color='#000000' size='2'><b>".COMPANY_ADDR."</font></td>\n";
		}
		
		
		
		$data .="</tr>";
		$data .="<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label1"]."</font></td>\n</tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label2"]." ".$currentDate." ".$labelQuery["label3"]."</font></td>\n</tr>
						<tr><td>&nbsp;</td></tr>
						</table>";
	
		
	}
	$data .= "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	if($fileFormatQuery["showLable"]== "Y"){	///if Labels are enabled
		$data .="<tr>";
	}
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	
		
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
		//if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME == $fileFormatQuery["FileLable"]){
		if($fileFormatQuery["showLable"]== "Y"){
			/////If Labels are enbled
			$data .= "<td><font face='Verdana' color='#000000' size='2'><b>".$fieldQuery[$j]["Lable"]."</font></td>\n";	
		}
	//}
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
		
	if($fieldQuery[$j]["isPattern"]== 'Y'){		
	
	   $patternValue[$j] = $fieldQuery[$j]["patternType"];
   }else{
   	  $patternValue[$j] = '';
   	 }			
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
	

		
		//$data .="</tr>";
	}
	if($fileFormatQuery["showLable"]== "Y"){////If Labels are enabled	
		$data .="</tr>";
	}

	
	for($i=0;$i < count($contentTrans);$i++)
	{
		$flagExport = true;
		if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT == '1' && CONFIG_REF_IN_EXTRA_INFO  != '1')
		{
			$refData = generateDistRef($contentTrans[$i]["transID"]);
			$flagExport = $refData[0];
			$distRefNum = $refData[1];
		}
		if(CONFIG_TRANS_EXTRA_INFO == '1')
		{
			if ($_GET['old'] != 'Y')
			{
				updateExtraInfo($contentTrans[$i]["transID"]);	
			}
			$transExtendContent = selectFrom("select * from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentTrans[$i]["transID"]."'");
		}
		 $exchangeData = getMultipleExchangeRates($contentTrans[$i]["fromCountry"], $contentTrans[$i]["toCountry"],$contentTrans[$i]["currencyTo"], $contentTrans[$i]["benAgentID"], 0 ,'' , 'USD',$contentTrans[$i]["transType"],$contentTrans[$i]["moneyPaid"], $contentTrans[$i]["custAgentID"]);
		 $exID = $exchangeData[0];
		$exRate = $exchangeData[1];
		$contentTrans[$i]["localAmount"];	
	  //$usdAmount =  number_format(round($contentTrans[$i]["localAmount"]/$exRate,2),2,'','');	
	  	if(!empty($exRate))
			$usdAmount =  round($contentTrans[$i]["localAmount"]/$exRate,2);	
		
		if($flagExport)
		{
			if (CONFIG_EXPORT_TRANS_OLD == '1') {
				update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
			}
			$refNumberIM = $contentTrans[$i]["refNumberIM"];
			$transDate = $contentTrans[$i]["transDate"];
			$companyCode = $contentTrans[$i]["refNumber"];
			$customerID = $contentTrans[$i]["customerID"];
			$customerName = "";
			$beneName = "";
			if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
			{		  
			   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
			   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
			   
			   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
			   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			   $accountName = $customerContent["username"];
			   $beneAddress = $beneContent["Address"];
			   $beneCity = $beneContent["City"];
			   $beneState = $beneContent["State"];
			   $benePhone = $beneContent["Phone"];
			   $beneCPF = $beneContent["CPF"];
			}else
				  {
			  	$customerContent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
			  	$beneContent = selectFrom("select * from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
			  	
			  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
			  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			  	$accountName = $customerContent["accountName"];
			  	$beneAddress = $beneContent["Address"];
			  	$beneAddress1 = $beneContent["Address1"];
			    $beneCity = $beneContent["City"];
			    $beneState = $beneContent["State"];
			    $benePhone = $beneContent["Phone"];
			    $beneMobile = $beneContent["Mobile"];
			    $beneCPF = $beneContent["CPF"];
			  }
			$gbpAmount = $contentTrans[$i]["transAmount"];
			$exchRate = $contentTrans[$i]["exchangeRate"];
			$foriegnAmount = $contentTrans[$i]["localAmount"];
			$toCountry = $contentTrans[$i]["toCountry"];
			$currencyFrom = $contentTrans[$i]["currencyFrom"];
			$currencyTo = $contentTrans[$i]["currencyTo"];
			$type = $contentTrans[$i]["transType"];
			$transTotal += $gbpAmount;
			$localTotal += $foriegnAmount;
			$collection = '';
			$bankName = '';						
			$account = '';						
			$brCode = '';						
			$brAddress = '';			
			if($contentTrans[$i]["transType"]=='Pick up')
			{
			$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
			$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
			$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
			}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
				{
					if($contentTrans[$i]["createdBy"] == "CUSTOMER")
						{
							$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
						
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];
							$accountType = $bankDetails["accountType"];						
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
							}else{
							$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
							
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];			
							$accountType = $bankDetails["accountType"];		
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
						}
						$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
						$bankNumber	= $bankNumQuery["bankCode"];				
				}

				$agnetDetails = selectFrom("select userID,username,agentCommission from ".TBL_ADMIN_USERS." where userID = '".$contentTrans[$i]["custAgentID"]."'");
			
		$data .= " <tr>";
			for($k = 0; $k < count($dataVariable); $k ++)
			{
				$value = "";
				$field = explode(',',$dataVariable[$k]); // this it to break a string on any index of the array[array containing database fields]
			
	      if($fixedValue[$k] != '')
	      {
	      	$value .= $fixedValue[$k]; 

	      }elseif($patternValue[$k]!= '' ){
				      if($patternValue[$k] == 'DISTRIBUITOR  SUB REF'){
					     $value .= $distRefNum;
				       }elseif($patternValue[$k] == 'DOLAR AMOUNT'){
				      	 $value .= $usdAmount;
				      	}elseif($patternValue[$k] == 'SETTLEMENT_EXCHANGE_RATE'){
				      			$value .= $exRate;
				      		}
				}else{

									

				/*
	values for the fields are pulled from the respective tables in the database
	
	*/
						if($dataTable[$k] == 'transactions')
							{
							
								for($ex = 0; $ex < count($field); $ex++){
									
										if($ex > 0){
												$value .= " ";
											}								
									
								//}
									if($field[$ex] == "localAmount")
									{
										if($contentTrans[$i]["toCountry"] == "United Kingdom" &&  CONFIG_TOCOUNTRY_UK == '1'){
											$amount2 +=  $contentTrans[$i]["localAmount"];
										}else{
											$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
											$amount +=  $contentTrans[$i]["localAmount"];
											$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
										}
									}
										$value .= $contentTrans[$i][$field[$ex]];
										
																
								}
												
							}elseif($dataTable[$k] == 'beneficiary'){
								for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$beneContent[$field[$ex]];
											
								}elseif($dataTable[$k] == 'customer'){
								
								for($ex = 0; $ex < count($field); $ex++){
								      
								       /*	if($field[$ex] == 'Phone'){
												
												$value .=$usdAmount;
												}*/
								      
											$value .= " ".$customerContent[$field[$ex]];					
								  }
							}elseif($dataTable[$k] == 'bankDetails'){
									
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
												$value .= $bankDetails[$field[$ex]];					
									}
								}elseif($dataTable[$k] == 'admin'){
									for($ex = 0; $ex < count($field); $ex++){
									  	$value .= $agnetDetails[$field[$ex]];
									  }
									}elseif($dataTable[$k] == 'cm_collection_point'){
									  	for($ex = 0; $ex < count($field); $ex++){
									  			if($contentTrans[$i]["transType"] == 'Bank Transfer'){
												$value .= "None ";
												} else{
									  		$value .= $collectionPoint[$field[$ex]];
									  	 }
									  }
									}elseif($dataTable[$k] == 'transactionExtended'){
										for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$transExtendContent[$field[$ex]];
											
								}
									
							if($value == "")
							{
									$value = "None";
								}
												
					}	
						if($dataVariable[$k] == 'transDate')
						{
							if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
							}
							
						}	
										$data .="<td><font color='#000000' size='1' face='Verdana'>&nbsp;$value</font></td>";
				}
				$data .="</tr>";
			
		
		}
	}
	if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME != $fileFormatQuery["FileLable"]){
		$comm = 0;		
		$distEnd = 0;	
		$comm2 = 0;
		$moneyOwed = 0;		
		$data .="<tr><td colspan='6'>&nbsp;</td></tr>";
					
		$z=0;
				foreach($currencyArray as $keys => $values){
					$comm = $values * 0.008;
					$distEnd = $values + 	$comm;	
					$comm2 = $amount2 * 0.005;		
					$moneyOwed = $amount2 + $comm2;
					
					
					if($z > 0){
				
						$data .="	<tr><td colspan='6'>&nbsp;</td></tr>";
						
						}	
						
						
	$data .="<td><font color='#000000' size='1' face='Verdana'>Total for ".$keys."</font></td>
					<td>&nbsp;</td>
					<td><font color='#000000' size='1' face='Verdana'>'".$values."'</font></td>
			<td><font color='#000000' size='1' face='Verdana'>'".$keys."'</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Incoming Transfer</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".$amount2."</font></td>";
			}else{
				$data .="<td>&nbsp;</td>
				<td>&nbsp;</td>";
			} 
			
		$data .="	
			</tr>
			<tr><td><font color='#000000' size='1' face='Verdana'>Calculation of commission for opal</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>". number_format($comm,2,'.','')."</font></td>
			<td><font color='#000000' size='1' face='Verdana'>".$keys."</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Opal Commission</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".number_format($comm2,2,'.','')."</font></td>";
			}else{
					$data .="<td>&nbsp;</td>
					<td>&nbsp;</td>";
			}
			
			
		$data .="	
			</tr>
			<tr><td><font color='#000000' size='1' face='Verdana'>Money to be given to the distribution end</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>".number_format($distEnd,2,'.','')."</font></td>
			<td><font color='#000000' size='1' face='Verdana'>".$keys."</font></td>";
			if($z == 0 && CONFIG_TOCOUNTRY_UK == '1'){
				$data .="<td><font color='#000000' size='1' face='Verdana'>Money Owed</font></td>
							<td><font color='#000000' size='1' face='Verdana'>&nbsp;".number_format($moneyOwed,2,'.','')."</font></td>";
			}else{
				$data .="<td>&nbsp;</td>
				<td>&nbsp;</td>";
			}
			
		$data .="	
			
			</tr>";
	
	if(CONFIG_TOCOUNTRY_UK == '1'){
		if($keys == 'USD'){
			$netDebt = $distEnd - $moneyOwed;
			$data .="<tr>
			<td><font color='#000000' size='1' face='Verdana'>Net Debt</font></td>
			<td>&nbsp;</td>
			<td align='left'><font color='#000000' size='1' face='Verdana'>".number_format($netDebt,2,'.','')."</font></td>
			<td>&nbsp;</td>
			</tr>";
		}
	}
			$z++;
		
			}
			
		}
	
$data.="</table>";
}
elseif ($fileFormat == "csv")
{

	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
		
		$data .= $labelQuery["heading"]."$fieldSpace";	
		$data .= "\r\n";
		$data .= COMPANY_ADDR;
		$data .= $labelQuery["label1"]."$fieldSpace";	
		$data .= "\r\n";
		$data .= $labelQuery["label2"]."$fieldSpace".$currentDate. $labelQuery["label3"];	
		$data .= "\r\n";
	
		}

	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	$flagToNextLine = false;
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
	//if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME == $fileFormatQuery["FileLable"]){
		
		if($fileFormatQuery["showLable"]== "Y"){
			$data .= $fieldQuery[$j]["Lable"]."$fieldSpace";	
			$flagToNextLine = true;
		}	
	
	//}
	
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
	
			
		if($fieldQuery[$j]["isPattern"]== 'Y'){
		$patternValue[$j] = $fieldQuery[$j]["patternType"];
	}else{
		$patternValue[$j] = '';
		}
	
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		
	}
//if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME == $fileFormatQuery["FileLable"])
{	
			if($fileFormatQuery["showLable"]== "Y")
			{
			$data .= "\n\r";
	}
}
	
	
	
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		
		
		$flagExport = true;
		if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT == '1' && CONFIG_REF_IN_EXTRA_INFO  != '1')
		{
			$refData = generateDistRef($contentTrans[$i]["transID"]);
			$flagExport = $refData[0];
			$distRefNum = $refData[1];
		}

		if(CONFIG_TRANS_EXTRA_INFO == '1')
		{
			if ($_GET['old'] != 'Y')
			{
				updateExtraInfo($contentTrans[$i]["transID"]);	
			}
			$transExtendContent = selectFrom("select * from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentTrans[$i]["transID"]."'");
		}
	   $exchangeData = getMultipleExchangeRates($contentTrans[$i]["fromCountry"], $contentTrans[$i]["toCountry"],$contentTrans[$i]["currencyTo"], $contentTrans[$i]["benAgentID"], 0 ,'' , 'USD',$contentTrans[$i]["transType"],$contentTrans[$i]["moneyPaid"], $contentTrans[$i]["custAgentID"]);
		 $exID = $exchangeData[0];
		 $exRate = $exchangeData[1];
		 $contentTrans[$i]["localAmount"];	
	  //$usdAmount =  number_format(round($contentTrans[$i]["localAmount"]/$exRate,2),2,'','');	
	  $usdAmount =  round($contentTrans[$i]["localAmount"]/$exRate,2);		
		

		if($flagExport)
		{
			if (CONFIG_EXPORT_TRANS_OLD == '1') {
				update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
			}
			$refNumberIM = $contentTrans[$i]["refNumberIM"];
			$transDate = dateFormat($date,"3");
			$companyCode = $contentTrans[$i]["refNumber"];
			$customerID = $contentTrans[$i]["customerID"];
			$customerName = "";
			$beneName = "";
			if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
			{		  
			   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
			   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
			   
			   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
			   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			   $accountName = $customerContent["username"];
			   $beneAddress = $beneContent["Address"];
			   $beneCity = $beneContent["City"];
			   $beneState = $beneContent["State"];
			   $benePhone = $beneContent["Phone"];
			   $beneCPF = $beneContent["CPF"];
			}else
				  {
			  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
			  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
			  	
			  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
			  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			  	$accountName = $customerContent["accountName"];
			  	$beneAddress = $beneContent["Address"];
			  	$beneAddress1 = $beneContent["Address1"];
			    $beneCity = $beneContent["City"];
			    $beneState = $beneContent["State"];
			    $benePhone = $beneContent["Phone"];
			    $beneMobile = $beneContent["Mobile"];
			    $beneCPF = $beneContent["CPF"];
			  }
			$gbpAmount = $contentTrans[$i]["transAmount"];
			$exchRate = $contentTrans[$i]["exchangeRate"];
			$foriegnAmount = $contentTrans[$i]["localAmount"];
			$currencyFrom = $contentTrans[$i]["currencyFrom"];
			$currencyTo = $contentTrans[$i]["currencyTo"];
			$toCountry = $contentTrans[$i]["toCountry"];
			$type = $contentTrans[$i]["transType"];
			$transTotal += $gbpAmount;
			$localTotal += $foriegnAmount;
			$collection = '';
			$bankName = '';						
			$account = '';						
			$brCode = '';						
			$brAddress = '';			
			if($contentTrans[$i]["transType"]=='Pick up')
			{
			$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
			$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
			$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
			}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
				{
					if($contentTrans[$i]["createdBy"] == "CUSTOMER")
						{
							$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
						
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];
							$accountType = $bankDetails["accountType"];						
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
							}else{
							$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
							
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];
							$accountType = $bankDetails["accountType"];						
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
						}	
						$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
						$bankNumber	= $bankNumQuery["bankCode"];				
				}
		 { 	
			
				for($k = 0; $k < count($dataVariable); $k ++)
			{
				$value = "";
				$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
			
	      if($fixedValue[$k] != '')
	      {
	      	$value .= $fixedValue[$k]; 
	      }elseif($patternValue[$k]!= '' ){
				      if($patternValue[$k] == 'DISTRIBUITOR  SUB REF'){
					     $value .= $distRefNum;
				       }elseif($patternValue[$k] == 'DOLAR AMOUNT'){
				      	 $value .= $usdAmount;
				      	}elseif($patternValue[$k] == 'SETTLEMENT_EXCHANGE_RATE'){
				      			$value .= $exRate;
				      		}
		      }else{
			

									
	/*
	values for the fields are pulled from the respective tables in the database
	
	*/
				
						if($dataTable[$k] == 'transactions')
							{
								for($ex = 0; $ex < count($field); $ex++)
								{
									if($ex > 0 && $fieldQuery["fieldSperator"]!= "None"){
											$value .= " ";
										}
										
										if($field[$ex] == "localAmount")
										{
											if($contentTrans[$i]["toCountry"] == "United Kingdom" &&  CONFIG_TOCOUNTRY_UK == '1'){
												$amount2 +=  $contentTrans[$i]["localAmount"];
											}else{
												$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
												$amount +=  $contentTrans[$i]["localAmount"];
												$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
											}
										}
										    $value .= $contentTrans[$i][$field[$ex]];

									    //}
									    /*
									    Due to confision, i added one that was in excel format and was a bit more logical, so need to confirm some ticket related to Brazil export and confirm, this was was done by Javeria in supervision of Riffat
									    */
									/*if($field[$ex] == "totalAmount")
									{
										$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
										$amount +=  $contentTrans[$i]["totalAmount"];
										$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
									}*/
								
																

								}	
							
							}elseif($dataTable[$k] == 'beneficiary'){
																				 
								for($ex = 0; $ex < count($field); $ex++)
								{
									if($ex > 0){
											$value .= " ";
										}
											$value .= $beneContent[$field[$ex]];
								}
																			 
							}elseif($dataTable[$k] == 'customer'){
								
								for($ex = 0; $ex < count($field); $ex++)
								{
									if($ex > 0){
											$value .= " ";
										}
											$value .= $customerContent[$field[$ex]];					
								}
							}elseif($dataTable[$k] == 'bankDetails'){
									
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
												$value .= $bankDetails[$field[$ex]];					
									}
								}elseif($dataTable[$k] == 'admin'){
									
									 for($ex = 0; $ex < count($field); $ex++){
									  	$value .= $agnetDetails[$field[$ex]];
									  }
									}elseif($dataTable[$k] == 'cm_collection_point'){
									  	
									  	for($ex = 0; $ex < count($field); $ex++){
									  			if($contentTrans[$i]["transType"] == 'Bank Transfer'){
												$value .= "None ";
												} else{
									  		$value .= $collectionPoint[$field[$ex]];
									  	 }
									  }
									}elseif($dataTable[$k] == 'transactionExtended'){
										for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$transExtendContent[$field[$ex]];
											
								}
							if($value == "")
							{
									$value = "None";
								}
					}	
						if($dataVariable[$k] == 'transDate')
						{
							if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
							}
						}
			
			/* values are now displayed here*/
			$data .= "$value,";
				
		}
		
			
		}
		$data .= "\n\r";
		
		
			
		if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME != $fileFormatQuery["FileLable"]){
			
			$z=0;
			$data .= "\n\r";
					foreach($currencyArray as $keys => $values){
					
						$data .= "$values";
						
						}
			
			
			
			}	
		}
	}
}
elseif ($fileFormat == "txt")
{
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		if($fileFormatQuery["showLable"]== "Y"){
		$data .= $fieldQuery[$j]["Lable"]."$fieldSpace";	
	}
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
	
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
	if($fieldQuery[$j]["isPattern"]== 'Y'){		
	
	   $patternValue[$j] = $fieldQuery[$j]["patternType"];
   }else{
   	  $patternValue[$j] = '';
   	 }				
		
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
	}
	
	
//	$data .= "$lineBreak\n\r";
if($fileFormatQuery["showLable"]== "Y"){
	$data .= "\n";
}
	for($i=0;$i < count($contentTrans);$i++)
	{
		$flagExport = true;
		if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT == '1' && CONFIG_REF_IN_EXTRA_INFO  != '1')
		{
			$refData = generateDistRef($contentTrans[$i]["transID"]);
			$flagExport = $refData[0];
			$distRefNum = $refData[1];
		}

		if(CONFIG_TRANS_EXTRA_INFO == '1')
		{
			if ($_GET['old'] != 'Y')
			{
				updateExtraInfo($contentTrans[$i]["transID"]);	
			}
			$transExtendContent = selectFrom("select * from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentTrans[$i]["transID"]."'");
		}
		 $exchangeData = getMultipleExchangeRates($contentTrans[$i]["fromCountry"], $contentTrans[$i]["toCountry"],$contentTrans[$i]["currencyTo"], $contentTrans[$i]["benAgentID"], 0 ,'' , 'USD',$contentTrans[$i]["transType"],$contentTrans[$i]["moneyPaid"], $contentTrans[$i]["custAgentID"]);
		 $exID = $exchangeData[0];
		 $exRate = $exchangeData[1];
		 $contentTrans[$i]["localAmount"];	
	  //$usdAmount =  number_format(round($contentTrans[$i]["localAmount"]/$exRate,2),2,'','');	
		if(!empty($exRate))
			$usdAmount =  round($contentTrans[$i]["localAmount"]/$exRate,2);
		

		if($flagExport)
		{	
			
				if (CONFIG_EXPORT_TRANS_OLD == '1') {
					update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
				}
				$refNumberIM = $contentTrans[$i]["refNumberIM"];
				$transDate = dateFormat($date,"3");
				$companyCode = $contentTrans[$i]["refNumber"];
				$customerID = $contentTrans[$i]["customerID"];
				$customerName = "";
				$beneName = "";
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
				{		  
				   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
				   
				   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
				   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
				   $accountName = $customerContent["username"];
				   $beneAddress = $beneContent["Address"];
				   $beneCity = $beneContent["City"];
				   $beneState = $beneContent["State"];
				   $benePhone = $beneContent["Phone"];
				   $beneCPF = $beneContent["CPF"];
				}else
					  {
				  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook,Address,City from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName, Address,Address1,City, State, Phone,Mobile, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
				  	
				  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
				  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
				  	$accountName = $customerContent["accountName"];
				  	$beneAddress = $beneContent["Address"];
				  	$beneAddress1 = $beneContent["Address1"];
				    $beneCity = $beneContent["City"];
				    $beneState = $beneContent["State"];
				    $benePhone = $beneContent["Phone"];
				    $beneMobile = $beneContent["Mobile"];
				    $beneCPF = $beneContent["CPF"];
				  }
				$gbpAmount = $contentTrans[$i]["transAmount"];
				$exchRate = $contentTrans[$i]["exchangeRate"];
				$foriegnAmount = $contentTrans[$i]["localAmount"];
				$toCountry = $contentTrans[$i]["toCountry"];
				$currencyFrom = $contentTrans[$i]["currencyFrom"];
				$currencyTo = $contentTrans[$i]["currencyTo"];
				$type = $contentTrans[$i]["transType"];
				$transTotal += $gbpAmount;
				$localTotal += $foriegnAmount;
				$collection = '';
				$bankName = '';						
				$account = '';						
				$brCode = '';						
				$brAddress = '';			
				if($contentTrans[$i]["transType"]=='Pick up')
				{
					
				$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
				$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
				$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
				}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
					{
						if($contentTrans[$i]["createdBy"] == "CUSTOMER")
							{
								$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
							
								$bankName = $bankDetails["bankName"];						
								$account = $bankDetails["accNo"];	
								$accountType = $bankDetails["accountType"];					
								$brCode = $bankDetails["branchCode"];						
								$brAddress = $bankDetails["branchAddress"];
								$IBAN = $bankDetails["IBAN"];
								$Remarks = $bankDetails["Remarks"];
								}else{
									
								$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
															
								$bankName = $bankDetails["bankName"];						
								$account = $bankDetails["accNo"];		
								$accountType = $bankDetails["accountType"];				
								$brCode = $bankDetails["branchCode"];						
								$brAddress = $bankDetails["branchAddress"];
								$IBAN = $bankDetails["IBAN"];
								$Remarks = $bankDetails["Remarks"];
							}		
							$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
							$bankNumber	= $bankNumQuery["bankCode"];				
					}
		
			{
					for($k = 0; $k < count($dataVariable); $k ++)
				{
					$value = "";
					$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
			      
			   if($fixedValue[$k] != '')
	      {
	      	$value .= $fixedValue[$k]; 

	      }elseif($patternValue[$k]!= '' ){
				      if($patternValue[$k] == 'DISTRIBUITOR  SUB REF'){
					     $value .= $distRefNum;
				       }elseif($patternValue[$k] == 'DOLAR AMOUNT'){
				      	 $value .= $usdAmount;
				      	}elseif($patternValue[$k] == 'SETTLEMENT_EXCHANGE_RATE'){
				      			$value .= $exRate;
				      		}
	      

	      }else{
			
										
		/*
		values for the fields are pulled from the respective tables in the database
		
		*/
						
							if($dataTable[$k] == 'transactions')
								{
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0 && $fieldQuery[$k]["fieldSperator"]!= "None"){
												$value .= " ";
											}
										
											
											
											if($field[$ex] == "localAmount")
											{
												if($contentTrans[$i]["toCountry"] == "United Kingdom" &&  CONFIG_TOCOUNTRY_UK == '1'){
													$amount2 +=  $contentTrans[$i]["localAmount"];
												}else{
													$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
													$amount +=  $contentTrans[$i]["localAmount"];
													$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
												}
											}	
											$value .= $contentTrans[$i][$field[$ex]];
									  
									 
																					
									}
									
													
								}elseif($dataTable[$k] == 'beneficiary'){
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
												$value .= $beneContent[$field[$ex]];
									}			
									}elseif($dataTable[$k] == 'customer'){
									
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
												$value .= $customerContent[$field[$ex]];					
									}
								}elseif($dataTable[$k] == 'bankDetails'){
									
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
											if($contentTrans[$i]["transType"] == 'Pick up'){
												$value .= "None ";
												} else{
												$value .= $bankDetails[$field[$ex]];					
											}
										
									}

								}elseif($dataTable[$k] == 'admin'){
									
									 for($ex = 0; $ex < count($field); $ex++){
									  	$value .= $agnetDetails[$field[$ex]];
									  }
									}elseif($dataTable[$k] == 'cm_collection_point'){
									  	
									  	for($ex = 0; $ex < count($field); $ex++){
									  			if($contentTrans[$i]["transType"] == 'Bank Transfer'){
												$value .= "None ";
												} else{
									  		$value .= $collectionPoint[$field[$ex]];
									  	 }
									  }
									}elseif($dataTable[$k] == 'transactionExtended'){
										for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$transExtendContent[$field[$ex]];
											
								}

								if($value == "")
								{
										$value = "None";
									}
									
						}	
							if($dataVariable[$k] == 'transDate')
							{
								if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
								}
							}	
				
				/* values are now displayed here*/
				$data .= $value."$fieldSpace";
				
				
			}
			}
			
			$data .= "$lineBreak";
			$data .= "\n";
		}
	}
}
if(!$flagExport)
{
		$data .= $distRefNum;
}
if(CONFIG_TRANS_EXTRA_INFO == '1')
	{
		/*condition is needed to be confirmed*/
		update("update ".TBL_TRANSACTION_EXTENDED." set isExported = 'Y' where isExported = 'N'");
	}		
				
echo $data;

?>
