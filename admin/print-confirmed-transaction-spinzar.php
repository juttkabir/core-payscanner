<?
session_start();
include ("../include/config.php");

/*
require_once "../include/set_env.php";
require_once "../include/DataObjects/Config.php";
$config_logo = &DataObjects_Config::category_array('logo');
$width = $config_logo['width'];
$height = $config_logo['height'];
$path = $config_logo['file_name'];
*/
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];
$today = date("d-m-Y");
if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

$buttonValue = $_GET["transSend"];	
	
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body <? if(CONFIG_SKIP_CONFIRM == '1'&& $buttonValue != ""){ ?> onLoad="print()"<? } ?>>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <? if ($_GET["msg"] == "Y"){ ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center"><div class='noPrint'><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></div></td>
                <td width="635"><div class='noPrint'><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></div></td>
              </tr>
            </table></td>
          </tr>
		  <?
		  }
		  ?>
  
    <tr>
      <td align="center"><table width="650" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <?
			
			if($transID!='')
			{
				$queryTransaction = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction["custAgentID"]!='')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction["custAgentID"] . "'";
				$custAgent=$queryTransaction["custAgentID"];
			
			


			$senderAgentContent = selectFrom($querysenderAgent);
			
			$custAgentParentID = $senderAgentContent["parentID"];
		}
		

	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo CONFIG_LOGO?>" width="<? echo CONFIG_LOGO_WIDTH?>" height="<? echo CONFIG_LOGO_HEIGHT?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table border="0" align="center">
              <tr>
                <td align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
              </tr>
            </table>
	<?
	}
	?>
            
         			<font color="#005b90">Transaction Date: </font><? echo $today;?>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Agent's Details</legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Company Name</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentCompany"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
                <td width="100" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
              </tr>
            
              <tr>
                <td width="150" align="right"><font color="#005b90">MSB No.</font></td>
                <td width="200"><? echo $senderAgentContent["agentMSBNumber"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Sender Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
		
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Full Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender's Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                 <tr>
                  <td width="200" align="right"><font color="#005b90">Date of Birth</font></td>
                  <?
                 $dateFormat = explode("-",$customerContent["dob"]);
                 $year = $dateFormat[0];
                 $month = $dateFormat[1];
                 $days = $dateFormat[2];
                 $spinzar_date = $days."-".$month."-".$year;
                 
                  
                  ?>
                  <td colspan="3"><? echo $spinzar_date;?></td>
                </tr>
                 <tr>
                  <td width="150" align="right"><font color="#005b90">Sender's ID</font></td>
                  <td colspan="200"><? echo $customerContent["IDType"];?></td>
                  <td width="150" align="right"><font color="#005b90">ID Number</font></td>
                  <td colspan="200"><? echo $customerContent["IDNumber"];?></td>
                </tr>
                <tr>
                  <td width="250" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">City</font></td>
                  <td width="200"><? echo $customerContent["City"]?></td>
                </tr>
                <tr>
                	<td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["Email"];?></td>
                </tr>
               
          			<? 
          			if ($customerContent["remarks"] != "") {  ?>
      						<tr>
        						<td width="150" align="right"><font color="#005b90">Remarks</font></td>
         						<td colspan="3"><? echo $customerContent["remarks"] ?></td>
      						</tr>
    				<?	}  ?>
                
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 
			
		/*	$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_SESSION["agentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$benAgentParentID = $senderAgentContent["parentID"];
			if($senderAgentContent["userID"] != "")
			{*/
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90" class="style2">Beneficiary Name</font></td>
                        <td width="200"><strong><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></strong></td>
                       
                         <td width="75" align="right"><font color="#005b90">S/O</font></td>
                        <td width="200"><? echo $benificiaryContent["SOF"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Destination</font></td>
                        <td width="200"><? echo $benificiaryContent["City"]?></td>
                        
                      </tr>
                      
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]; ?></td>
                      </tr>
                <? if(CONFIG_IDTYPE_PASSWORD == 1){ ?>
				              <tr>
				                <td width="150" align="right"><font color="#005b90">ID Type</font></td>
				                <td width="200"><? echo $benificiaryContent["otherId_name"]?></td>
				                <td width="100" align="right"><font color="#005b90">ID Number</font></td>
				                <td width="200"><? echo $benificiaryContent["otherId"]?></td>
				              </tr>
              
              	<? } ?>
                      <?
				}
			  if($queryTransaction["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
					?>
                      <tr>
                        <td colspan="2"><span class="style2">Beneficiary Bank Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
												/*if($_SESSION["benCountry"] == "United States")
												{
													echo "ABA Number*";
												}
												else*/if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												
								         	echo $benBankDetails["ABACPF"];
								                          
												}
												?> &nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
		<td width="100" height="20"  align="right">&nbsp; </td>
						<td width="200" height="20">&nbsp;  </td>
                      </tr>
                      <tr>
                      	<?
                      if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
                      {?>
                      	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
                        <td width="200"><?=$benBankDetails["accountType"]?> </td>
                      <? 
                      }else{
                      ?>	
                        <td width="150" align="right">&nbsp;  </td>
                        <td width="200">&nbsp;  </td>
                      <?
                      }
                      ?>
                        <td width="100" align="right">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <?  //echo $_SESSION["IBAN"]; color="#005b90">IBAN Number</font font color="#005b90"Swift Code font echo $_SESSION["swiftCode"];
  			}
			  if($queryTransaction["transType"] == "Pick up")
			  {
			  
			$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction["collectionPointID"] . "'";
			$senderAgentContent = selectFrom($queryCust);	
			$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);		
			if($senderAgentContent["cp_id"] != "")
			{
		?>
                      <tr>
                        <td colspan="2"><span class="style2">Collection Point Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>		
                        <tr>
                        <td width="150" align="right"><font class="style2">Collection point name</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_corresspondent_name"]?> </strong></td>
                        <td width="100" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_contact_person_name"]?></strong></td>
                      </tr>
                      <tr>
                       <!-- Add by Niaz Ahmad against ticket # 2558 at 12-10-2007 (only for Beaconcrest)-->
                        <? if(CONFIG_CHANGE_LABEL_RECEIPT == "1"){ ?>
                        <td width="150" align="right"><font class="style2">Paying Agent</font></td>
                        <? } else { ?>
                         <td width="150" align="right"><font class="style2">Distributor</font></td>
                        <? } ?>
                        <td width="200" colspan="3"><? echo $queryExecute["name"]?></td>
                      </tr> 
                      <tr>
                        <td width="150" align="right"><font class="style2">Branch Address</font></td>
                        <td colspan="3"><strong><? echo $senderAgentContent["cp_branch_address"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font class="style2">City</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_city"]?></strong></td>
                        <td width="100" align="right"><font class="style2">Country</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font class="style2">Phone</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
                        <td width="100" align="right"><font class="style2">Fax</font></td>
                        <td width="200"><strong><? echo $senderAgentContent["cp_fax"]?></strong></td>
                      </tr> 
                      <?
			  }
			  
			  
			  }
  ?>
            </table>
              </fieldset></td>
              </tr>
         
<?

//}
?>		  
         		 
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><strong><font color="#005b90"><? echo $manualCode;?></font></strong></td>
                <td width="200"><strong><? echo $queryTransaction["refNumber"]?></strong></td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" ><? echo $queryTransaction["transAmount"]?> 
         <?
       	if(CONFIG_ENABLE_DOMESTIC != '1')
       	{
       	?>       	
                	in  <? echo $queryTransaction["currencyFrom"]?>
        <?
      	}
        ?>        	
                	
                	</td>
              </tr>
       <?
       if(CONFIG_ENABLE_DOMESTIC != '1')
       {
       ?>          
			  <tr>
                <td width="150" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200"><? echo $queryTransaction["exchangeRate"]?>                  </td>
                <td width="100" align="right"><font class="style2">Local Amount</font></td>
                <td width="200"><strong><? echo $queryTransaction["localAmount"]?>  in  <? echo $queryTransaction["currencyTo"];?>  </strong></td>
			  </tr>
			  <?
			}
			?>
			  <?
				$dis =  $_SESSION["chDiscount"];
				
			  
			  if ($dis!="on") 
			  {
			   ?>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200"> <? echo $queryTransaction["IMFee"]?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $queryTransaction["totalAmount"]?>                </td>
              </tr>
			  <? }
			  
			  else
			  {
			   ?>
			   <tr>
                <td width="150" align="right"><font color="#005b90"> <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?> Discount</font></td>
                <td width="200"><? echo $_SESSION["discount"];?> </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $queryTransaction["totalAmount"]?>                </td>
              </tr>			   
			 <? }?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200"><? if($queryTransaction["transactionPurpose"] == "Other") {echo $queryTransaction["other_pur"];} else{echo $queryTransaction["transactionPurpose"];} ?>
                </td>
                <? if(CONFIG_REMOVE_FUNDS_SOURCES != "1")
               {?> 
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="200"><? echo $queryTransaction["fundSources"]?>
                </td>
             <? }?>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200"><? echo $queryTransaction["moneyPaid"]?>
                <td width="150" align="right"><font color="#005b90">Branch Money Paid In</font></td>
                <td width="200"><? echo $queryTransaction["branchName"]?>	
                </td>
              </tr>
              <tr>
        <? if(CONFIG_REMOVE_ADMIN_CHARGES != "1")
           { ?>           
				<td width="100" align="right"><font color="#005b90">Admin Charges</font></td>
                <td width="200">
                	
        	<? 
        	if ($queryTransaction["transType"]=="Bank Transfer")
						{		
							echo $queryTransaction["bankCharges"];
							
						}?>
				</td>
				<? }?>
      </tr>
				<?
					 if(CONFIG_CASH_PAID_CHARGES_ENABLED)
							  {
							  	if($queryTransaction["moneyPaid"]=='By Cash')
							  	{?>
								  <tr>
								  <td align="right"><font color="#005b90"> Cash Handling Charges </font></td>
								  <td><? echo CONFIG_CASH_PAID_CHARGES;?></td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>			  
								  
								  </tr>
					  <? 
					  			}
					  		} ?>
	
						 <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <tr>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td><?=$queryTransaction["outCurrCharge"]?></td>
					  <td><font color="#005b90"> Discount Request </font></td>
					  <td><?=$queryTransaction["discountRequest"]?></td>			  
					  
					  </tr>
					  <? } ?>
                
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $queryTransaction["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <tr><!-- Added by Niaz Ahmad againist ticket # 2533 at 09-10-2007 -->
                <? if(CONFIG_SHOW_REFERENCE_CODE_BOX == "1"){ ?>
                   <td width="150" height="20" align="right"><strong><font color="#005b90"><? echo $systemPre; ?> Transaction No. </font></strong></td>
                <td width="200" height="20"><table border="1"><tr><td width="100" height="20" align="center"><strong><? echo $queryTransaction["refNumberIM"];?></strong></td></tr></table></td>
                 <? } else {?>
                <td width="150" height="20" align="right"><strong><font color="#005b90"><? echo $systemPre; ?> Transaction No. </font></strong></td>
                <td width="200" height="20"><strong><? echo $queryTransaction["refNumberIM"];?></strong></td>
                  <? } ?>
                <td width="150" height="20" align="right"><font color="#005b90">&nbsp;
		</font></td>
                <td width="200" height="20">&nbsp;
		</td>
              </tr>
               <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font></td>
                <td colspan="3" align="left"><? echo $queryTransaction["tip"]?></td>		
               
              </tr>
               <? if(CONFIG_IDTYPE_PASSWORD == '1' && $benificiaryContent["otherId"]!= "")
					  {
					  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Password</font></td>
                <td width="200" height="20"><? echo $queryTransaction["benIDPassword"];?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <? } ?>
            </table>
            </fieldset>
              </td>
          </tr>
          <tr>
            <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%"> _______________________________<br>
                  
                Agent Official Signature</td>
                <td width="50%" align="right"> _______________________________<br>
                  
                Sender Signature </td>
              </tr>
               <tr>
                  <td><strong>10 CHURCH ROAD ACTON W3 8PP</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><strong>Tel: 020 8993 4236 Fax: 020 8993 2472</strong></td>
               </tr>
               
             <!-- <tr>
              	<td >
              		Customer Service Number <? echo CONFIG_INVOICE_FOOTER; ?>
              	</td>
              </tr> -->
              <tr>
              	<td height="40" colspan="3">
                        	<? if(CONFIG_TRANS_CONDITION_ENABLED==1)
                        	{
                        		echo(CONFIG_TRANS_COND);
                        	 }else{
                        	 ?>
                        	I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.
                        	<? } ?>                        	</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center">
		<div class='noPrint'>
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="<?=$returnPage?>?create=Y" class="style2">Go to Create Transaction</a>
<?
/* if(CONFIG_SKIP_CONFIRM == '1'){ ?>
			&nbsp;&nbsp;&nbsp;<a href="<?=$returnPage?>?transID=<? echo $transID?>&back=edit-transactions.php" class="style2">Edit Transaction</a>
<? }*/
?>		
		</div>
			</td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>