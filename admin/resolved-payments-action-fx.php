<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	
	include ("../include/config.php");
	include ("security.php");
	
	if(CONFIG_CUSTOM_TRANSACTION == '1')
		$transactionPage = CONFIG_TRANSACTION_PAGE;
	else
		$transactionPage = "add-transaction.php";
//print_r($_REQUEST);
	$today = date("d/m/Y");
	if($_REQUEST["from"] == "")
	{
		$_REQUEST["from"] = $today;
	}
	if($_REQUEST["to"] == "")
	{
		$_REQUEST["to"] = $today;
	}
	
	if($_REQUEST["fromT"] == "")
	{
		$_REQUEST["fromT"] = $today;
	}
	if($_REQUEST["toT"] == "")
	{
		$_REQUEST["toT"] = $today;
	}
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx && $_REQUEST['getGrid']!='transList')
	{
		$sidx = 'importedOn DESC, id ';
	}elseif(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";
$tranactionSource = $_REQUEST["tranactionSource"];
	/*function merege_entries($arrFirstParam, $arrSecParam, $strKey)
	{
		//index merge in case of same transaction 
		$arrTransIdIndexes = array(
			'importedOn', 'payment_desc', 'bankAccount', 'amount', 'action_date'
		);
	
		$arrPayIdIndexes = array(
			'totalAmount', 'refNumberIMs', 'custName', 'benName', 'action_date', 'resolvedBy','cityName'
		);
		


		$arrToReturn = array();
		
		if($strKey == 'transID')
		{
			foreach($arrFirstParam as $key => $val)
			{
				if(in_array($key, $arrTransIdIndexes))
					$arrToReturn[$key] = $val;
				else
					$arrToReturn[$key] = $val."<BR />".$arrSecParam[$key];
			}
		}
		
		if($strKey == 'payID')
		{
			foreach($arrFirstParam as $key => $val)
			{
				if(in_array($key, $arrPayIdIndexes))
					$arrToReturn[$key] = $val;
				else
					$arrToReturn[$key] = $val."<BR />".$arrSecParam[$key];
			}
			//debug($arrToReturn);
		}


		if($strKey == 'FULL')
		{
			foreach($arrFirstParam as $key => $val)
			{
				$arrToReturn[$key] = $val."<BR />".$arrSecParam[$key];
			}
		}
		

		//debug($arrToReturn);

		return $arrToReturn;
	}*/


	

if($_REQUEST["btnAction"] == "Resolve")
{

//\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\/\/\/\/\\/\/\/\/\/\/\
	$successResolved = false;
	$transIDArray	= $_REQUEST["transIDs"];
	$payIDArray		= $_REQUEST["payIDs"];
	//debug($transIDArray);
	//debug($payIDArray,true);
	if(count($transIDArray)>0 && is_array($transIDArray)){
		for($s=0;$s<count($transIDArray);$s++)	{
			$errFlag = false;

			$transID = $transIDArray[$s];
			$transRS = selectFrom("SELECT transID,localAmount,totalAmount,transAmount,bankCharges,refNumberIM,benAgentID,custAgentID,customerID,AgentComm,currencyTo,currencyFrom,createdBy FROM transactions where transID='".$transID."'");
			$transID = $transRS["transID"];	
			$totalAmount = $transRS["totalAmount"];
			$transAmount = $transRS["transAmount"];
			$localAmount = $transRS["localAmount"];
			$bankCharges = $transRS["bankCharges"];
			$imReferenceNumber = $transRS["refNumberIM"];
			$benAgentID = $transRS["benAgentID"];
			$custAgentID = $transRS["custAgentID"];											
			$customerID = $transRS["customerID"];
			$agentComm = $transRS["AgentComm"];
			$currencyTo = $transRS["currencyTo"];
			$currencyFrom = $transRS["currencyFrom"];
			$transCreatedBy = $transRS["createdBy"];
			$modifyby = $agentID; // to keep track of logged user ID in modify column
			$amount = $totalAmount;
			$payinFlag = true;
			/*******
				#5084 - AMB Exchange
				This block below is to check either functionality is PayinBook or not Also
				if PayinBook then Customer's Ledgers should also be affected alongwith
				Payin Agent either All or assigned in CONFIG_PAYIN_AGENT_NUMBER.
			*******/
			if(CONFIG_PAYIN_CUST_AGENT == "1" && CONFIG_PAYIN_CUSTOMER=="1"){
				if(CONFIG_PAYIN_CUST_AGENT_ALL=="1"){
					$ledgerAgent = $custAgentID;
				}
				elseif(defined("CONFIG_PAYIN_AGENT_NUMBER") && CONFIG_PAYIN_AGENT_NUMBER!="0"  && CONFIG_PAYIN_CUST_AGENT_ALL!="1"){
					$ledgerAgent = CONFIG_PAYIN_AGENT_NUMBER;
				}
				$agentPayinContentsT = selectFrom("select userID from " . TBL_ADMIN_USERS . " where userID = '".$ledgerAgent."'");
				$agentPayinID = $agentPayinContentsT["userID"];
				if($agentPayinID=="" && $transCreatedBy!="CUSTOMER")
					$payinFlag = false;
			}
			elseif($transCreatedBy!="CUSTOMER"){
					$payinFlag = false;
			}
			if($_REQUEST["userTypes"]  == "customer" && $payinFlag)
			{
				if($transCreatedBy == "CUSTOMER" && isExist("select c_id from cm_customer where c_id ='".$customerID."'"))
				{
					$strQuery = "SELECT Balance,c_id FROM cm_customer where c_id = '".$customerID."'";
					$rstRow = selectFrom($strQuery);
					$Balance = $rstRow["Balance"]+$amount;	
					update("update cm_customer set Balance = '".$Balance."' where c_id = '".$customerID."'");
					$agentLedgerAmount = $amount;
					$tran_date = date("Y-m-d");
					
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$amount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>$imReferenceNumber,"customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"customer_account"
										 );
					$retInputArgsRevC = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
					
					insertInto("insert into  customer_account (customerID ,Date ,tranRefNo,payment_mode,Type ,amount,note ) 
								 values('".$customerID."','".$tran_date."','".$imReferenceNumber."','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$note."'
								 )");
									
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}else{
						if($Balance >= $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
					}								
				
					if($conditionFlag)// >= $transAmount)
					{	
						$tran_date = date("Y-m-d");	
						/**************************************************************
						 * The puporse of this config (CONFIG_VERIFY_TRANSACTION_ENABLED) when user resolve payment then it will go in processing status
						 * In processiing status ledger not affect.
						 * Added by Niaz Ahmad at 03-08-2009 @5256- Minascenter
											
						*****************************************************************///
						if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
						{													
							update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}elseif(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{
						
							update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$tran_date."' where transID = '".$transID."'");
	
							$checkBalance2 = selectFrom("select agentType, balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");
							
							if($checkBalance2["agentType"] == 'Sub')
							{
								updateSubAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}else{
								updateAgentAccount($benAgentID, $transAmount, $transID, "WITHDRAW", "Transaction Authorized", "Distributor");
							}
					  }
					}
				}
				elseif($transCreatedBy != "CUSTOMER"  && isExist("select customerID from customer where customerID ='".$customerID."'") && $payinFlag)
				{
					/*					
					$queryResolve = "update barclayspayments set isResolved = 'Y', username='$uname', paymentFrom = 'agent customer' where id = '".$_REQUEST["allIds"][$i]."'";
					update($queryResolve);
			
					$strQuery = "SELECT * FROM barclayspayments where username = '$uname' and id = '".$_REQUEST["allIds"][$i]."'";
					
					$Balance = 0;
					$nResultBarkleyTable = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
					while($rstRowBT = mysql_fetch_array($nResultBarkleyTable))	
					*/			
					$Balance = $amount;
					$rstRow = selectFrom("SELECT * FROM customer where customerID = '".$customerID."'");
					$tran_date = date("Y-m-d");
					$agentLedgerAmount = $amount;
					/**
					 * #5677
					 * Reconciling REV(Reveersal entry into customer ledger if exists with same amount having "-" sign)
					 * Enable = "1"
					 * Disable = "0"
					 * by Aslam Shahid 
					 */
					$inputArgsRev = array("userTypes"=>$_REQUEST["userTypes"],"amount"=>$agentLedgerAmount,"transDate"=>$tran_date,"type"=>"WITHDRAW",
										  "transRefNo"=>"","customerID"=>$customerID,"modifiedBy"=>$modifyby,"note"=>$note,
										  "tableCustomer"=>"agents_customer_account"
										 );
										 
					$retInputArgsRev = gateway("CONFIG_UNRESOLVED_REVERSAL_ENTRY",$inputArgsRev,CONFIG_UNRESOLVED_REVERSAL_ENTRY);
/*					if(CONFIG_UNRESOLVED_REVERSAL_ENTRY=="1"){
						$queryResolve = "SELECT id FROM barclayspayments WHERE isResolved = 'N' AND amount='-".$amount."' AND tlaCode='REV' ORDER BY id DESC ";
						$queryResolveRS = selectFrom($queryResolve);
						if($queryResolveRS["id"]!=""){
							update("update barclayspayments 
										set isResolved = 'Y', 
											username='', 
											paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."' 
										where id = '".$queryResolveRS["id"]."' ");
							insertInto("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','WITHDRAW','".$amount."','".$modifyby."'
									 )");
						}
					}*/
					update("insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
									 values('".$customerID."','".$tran_date."','','Payment by Bank','DEPOSIT','".$agentLedgerAmount."','".$modifyby."'
									 )");

					if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
					{													
				   	 update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
					}elseif(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{			
					
					if(CONFIG_PAYIN_CUST_AGENT == '1')
					{
						$agentPayinContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'");
						if($agentPayinContents["agentType"] == 'Sub')
						{
							updateSubAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
							$q = updateAgentAccount($agentPayinContents["parentID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}else{
							$q = updateAgentAccount($agentPayinContents["userID"], $agentLedgerAmount, $transID, 'DEPOSIT', "Barclays Payment", "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
						}	
					}
					///////////Balance Condition//////////	
					// to Authorize trasaction in any case;
				
					if(CONFIG_EXACT_BALANCE == "1")
					{
						if($Balance == $totalAmount)
						{
							$conditionFlag = 1;
						}else{
							$conditionFlag = 0;
						}
						}else{
							if($Balance >= $totalAmount)
							{
								$conditionFlag = 1;
							}else{
								$conditionFlag = 0;
						}
					}								
															
					///////$Balance >= $totalAmount///// Done on requirement of Express
														
					if($conditionFlag)
					{	
						/*$strQuery = "select * from transactions  where customerID = $CustomerID and transStatus  = 'Pending' and transAmount = $transAmount and createdBy != 'CUSTOMER'";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$rstRows = mysql_fetch_array($nResult);
						$imReferenceNumber = $rstRows["refNumberIM"];
						$totalAmount = $rstRows["totalAmount"];*/
						$date_time = date("Y-m-d H:i:s");
						if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
						{
								update("update transactions set transStatus  = 'Pending' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
						}else{
							update("update transactions set transStatus  = 'Authorize' , authoriseDate = '".$date_time."' where transID = '".$transID."'");
						}
						/*$strQuery = "insert into  agent_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount ) 
										 values('".$CustomerID."','".$tran_date."','$imReferenceNumber','Payment by Bank','WITHDRAW','".$totalAmount."'
										 )";	
					
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());	
						*/																														
					}		
					else
					{}
				  }	
				}
				else
				{
					$err = "Error occured while processing some records\n(Payin Agent is not specified properly OR select Agent from Search Type dropdown\nand give Agent Name for Simple Agent transaction Reconcile.)";
					$errFlag = true;
				}
				
			}
			
			elseif($_REQUEST["userTypes"] == "agent" || $_REQUEST["userTypes"] == "distributor"  || !$payinFlag)
			{
				if(CONFIG_VERIFY_TRANSACTION_ENABLED == "1")
				{													
					update("update transactions set transStatus  = 'Processing' , verificationDate = '".$tran_date."' where transID = '".$transID."'");
				}
				else
				{
				$custAgentR = selectFrom("select username from " . TBL_ADMIN_USERS . " where userID='".$custAgentID."'");
				$uname = $custAgentR["username"];
				if($uname!="")
				{
					$type = "DEPOSIT";
					$description = "Barclays Payment";
					
					$contantagentID = selectFrom("select userID,balance,limitUsed,agentAccountLimit,isCorrespondent from " . TBL_ADMIN_USERS . " where username ='".$uname."'");
					$agentID = $contantagentID["userID"];
					$agentlimitUsed	= $contantagentID["limitUsed"];
					$agentLimit	= $contantagentID["agentAccountLimit"];
					$agentBalance	= $contantagentID["balance"];
					$paymentFrom = "agent";
					
					$Balance = $agentBalance + $amount;
					
					if(CONFIG_AnD_ENABLE == '1' && $contantagentID["isCorrespondent"] == 'Y')
					{
						$isAgentAnD = 'Y';
					}
					
					update("update " . TBL_ADMIN_USERS . " set balance  = '".$Balance."' where userID = '".$agentID."'");
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD != "1")
					{
						/* Changing the localAmount to totalAmount as bug found via ticket #3829 */
						$amount = $totalAmount;
						$currencyFrom = $currencyTo;
					}else{
						$amount = $amount;
						$currencyFrom = $currencyFrom;
					}
					$agentLedgerAmount = $amount;
					
					/* #4703 - Opal
					 * agent ledger is not affected as Deposit [money in] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
					 * as it might need only affect on authorizing transaction.
					 * by AShahid
					 */
					if(CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION != "1"){
							if($isAgentAnD == 'Y')
							{
								$queryAgent = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, amount, type, modified_by, description,TransID,actAs,currency) values('".$agentID."','".getCountryTime(CONFIG_COUNTRY_CODE)."','".$agentLedgerAmount."','".$type."', '".$modifyby."','".$description."','".$transID."','Agent','".$currencyFrom."')";
								insertInto($queryAgent);
							}else{
								if($contantagentID["agentType"] == 'Sub')
								{
									updateSubAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
									updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
									
								}else{
									updateAgentAccount($agentID, $agentLedgerAmount, $transID, $type, $description, "Agent",$note,$currencyFrom);
								}
							}
					 }	
						$amountInHand = $agentBalance + $amount;
						if(CONFIG_AGENT_LIMIT == "1")
						{
						$agentValidBalance = $amountInHand + $agentLimit;///Agent Limit is also used
						}else{
						$agentValidBalance = $amountInHand;///Agent Limit is not used
						}
						
						///
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$agentLedgerAmount = $totalAmount - $agentComm;
						}else{
							$agentLedgerAmount = $totalAmount;
						}
						///
								
						//update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						//update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='".$uname."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$transID."'");
						//debug($agentValidBalance .">=". $agentLedgerAmount);
						
						/*
						  #4703: Opal - Item Wise Bank Reconciliation
						  in Opal not check whether balance is enough or not
						  Distributor ledgers are affected when transaction is Authorized
						  distributor ledger
						*/
						$enoughBalanceFlag = false;
						if(defined("CONFIG_PUT_AnD_ENTRY_IN_LEDGER_AT_AUTHORIZATION") && CONFIG_PUT_AnD_ENTRY_IN_LEDGER_AT_AUTHORIZATION == "1")
							$enoughBalanceFlag = true;
						
						if($agentValidBalance >= $agentLedgerAmount || $enoughBalanceFlag)
						{	//////////////////If Balance is enough
							
							$queryCust = "select payinBook  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
							$custContents = selectFrom($queryCust);
							
							if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
							{	
								$Status	= "Authorize";
								$tran_date = date("Y-m-d");	
								
								$amountInHand = $amountInHand - $agentLedgerAmount;			
								/* #4703 - Opal
								 * agent ledger is affected as [money out] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
								 * as it might not have been affected on creation of transaction under the same config.
								 * by AShahid
								 */
								if(CONFIG_LEDGER_AT_CREATION != "1" || CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1")
								{
									
									if($isAgentAnD == 'Y')
									{
										$insertQuery = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description,currency) values('".$agentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$currencyFrom."')";
									}else{
										$insertQuery = "insert into agent_account (agentID, dated, type, amount, modified_by, TransID, description,note,currency) values('".$agentID."', '".$tran_date."', 'WITHDRAW', '".$agentLedgerAmount."', '".$modifyby."', '". $transID."', 'Transaction Authorized','".$note."','".$currencyFrom."')";
									}
									$q=mysql_query($insertQuery);//////Insert into Agent Account
									update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '".$agentID."'");											
									agentSummaryAccount($agentID, "WITHDRAW", $agentLedgerAmount);	
								}
							////////////////////Auto Authorization//////////
							/******
								#5160
								Distributors ledgers are affected in different cases like here we checked
								that when CONFIG_AUTO_AHTHORIZE is ON then in some clients on the basis of this
								config Ledgers of distributors are WITHDRAWN.
							******/
							
							/* #4703 - Opal
							 * distributor ledger is affected as Deposit [money in] for CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION
							 * by AShahid
							 */
							if(CONFIG_AUTO_AHTHORIZE == "1" || CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1")
							{
								$distType = "WITHDRAW";
								if(CONFIG_DONT_PUT_AnD_ENTRY_IN_LEDGER_AT_CREATION=="1" && $isAgentAnD == 'Y')
									$distType = "DEPOSIT";
									
									$checkBalance2 = selectFrom("select balance, isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$benAgentID."'");						
									$agentLedgerAmount = $transAmount;
								if(CONFIG_AnD_ENABLE == '1' && $checkBalance2["isCorrespondent"] == 'Y')
								{
									$isBankAnD = 'Y';
								}
								$currentBalanceBank = $checkBalance2["balance"]	- $transAmount;				
								update("update " . TBL_ADMIN_USERS . " set balance  = '".$currentBalanceBank."' where userID = '".$benAgentID."'");											
								if($isBankAnD == 'Y')
								{
									insertInto("insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description, actAs,currency) values('".$benAgentID."', '".$tran_date."', '".$distType."', '".$agentLedgerAmount."', '".$modifyby."', '".$transID."', 'Transaction Authorized','Distributor','".$currencyTo."')");
								}else{
									$agentLedgerAmount = (DEST_CURR_IN_ACC_STMNTS == "1" ? $localAmount : $transAmount);
									insertInto("insert into bank_account (bankID, dated, type, amount, currency, modified_by, TransID, description) values('".$benAgentID."', '".$tran_date."', '".$distType."', '".$agentLedgerAmount."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $currencyTo : $currencyFrom)."', '".$modifyby."', '".$transID."', 'Transaction Authorized')");		
								}
								agentSummaryAccount($benAgentID, $distType, $agentLedgerAmount);
							}
						}
					//update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '$agentID'");
					}
					
					/////////////////////////
				}
				else
				{
					$err = "Error occured while processing some records";
					$errFlag = true;
				}
			  }	  // end processing trans status if condition
			}
			//echo("it is here...");
			//if(trim($err) == "")
			if(!$errFlag)
			{
				$err = "Payments resolved successfully.";
				$successResolved = true;
				update("update transactions set isResolved  = 'Y' where transID = '".$transID."'");
					
					
					
					
				if($enoughBalanceFlag)
				{
					if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
					{
						update("update transactions set transStatus  = 'Pending' where transID = '".$transID."'");
					}else{
						update("update transactions set transStatus  = 'Authorize' where transID = '".$transID."'");
						}
				}
				//redirect("unres-payments.php?type=" . $_GET["type"] . "&msg=" . urlencode($err));
			}			
		}
	}
	// Number of iteration should be equal to the number of  transactions
			
		if($_REQUEST["userTypes"]!="" && $transID!="" && $successResolved){
			$counter = 0;
			for($b=0;$b<count($payIDArray);$b++){
				$queryResolve = "update barclayspayments set isResolved = 'Y', username='', paymentFrom = '".($_REQUEST["userTypes"]!="agent"?"customer":"agent")."' where id = '".$payIDArray[$b]."' ";
				update($queryResolve);
				
				$query_pay_resolved = "
									INSERT INTO payments_resolved(payID,transID,agentAmount)
												values('".$payIDArray[$b]."', '".$transIDArray[$b]."','".$agentLedgerAmount."')";
					insertInto($query_pay_resolved);
				
				
				$descript="Resolved record for Barclays Payment for transaction ".$imReferenceNumber;
				activities($_SESSION["loginHistoryID"],"UPDATE", $payIDArray[$b],'barclayspayments',$descript);
				if(CONFIG_IMPORT_PAYMENT_AUDIT=="1"){
					$query_pay_action = "
									INSERT INTO payment_action(payID,transID,action_id,agentAmount)
												values('".$payIDArray[$b]."', '".$transIDArray[$b]."', '".$_SESSION["loggedUserData"]["userID"]."','".$agentLedgerAmount."')";
					insertInto($query_pay_action);
				}
			
				$counter = $counter+1;
			}
		}
		echo "<input type='hidden' name='payListResolve' id='payListResolve' value='".$err."'>";
		exit;
}
	
if($_REQUEST["type"] == 1)
{//tlaCode = 'REM'
	$query = "select * from barclayspayments where  isResolved='N' and isDeleted='N' ";
	
}elseif($_REQUEST["type"] == 2)
{
	$query = "select * from natwestpayments where isResolved='N' and isDeleted='N' ";
}
$queryTemp = $query;

//////////////Date Block //////////////////

	
	/////////////////////Date Block ended///////////////	

if(CONFIG_TRANSACTION_STATUS_AWAITING_PAYEMNT == '1')
{
	$fetchingStatus = 'Awaiting Payment';	
}else
{
	$fetchingStatus = 'Pending';	
}

$queryT = "select 

				pa.batchNumber as batchNumber,
				tr.transID as transID,
				pa.payID as payID,
				
				bar.id as bar_id,
				bar.importedOn,
				bar.currency,
				bar.amount,
				bar.entryDate,
				bar.description as payment_desc,
				bar.tlaCode,
				bar.accountNo as bankAccount,
				bar.isDeleted as isDeleted,
				bar.isResolved as isResolved,
				
				pa.agentAmount as payment_amount,
				pa.action_date as action_date,
				pa.refNumberIMs as refNumberIMs,
				pa.format as format,
				
				ad.username as resolvedBy,
				
				tr.refNumberIM as refNumberIM,
				tr.benID as benID,
				
				tr.totalAmount as totalAmount,
				tr.transDate as transDate,
				tr.customerID as customerID,
				tr.createdBy as createdBy,
				tr.currencyFrom as currencyFrom ";


$queryT .= "FROM
				payments_resolved AS pa
				INNER JOIN barclayspayments AS bar ON bar.id = pa.payID
				LEFT JOIN transactions AS tr ON tr.transID = pa.transID
				LEFT JOIN admin AS ad ON ad.userID = pa.resolvedBy 
				
			WHERE
				1 and pa.status = 'Y'
			";
//echo $queryT;
//print_r($_REQUEST);
/////////////////////Date Block////////////////////

if(!empty($_REQUEST["searchDateType"]))
	$date_field = $_REQUEST["searchDateType"];
else
	$date_field = 'action_date';


if ($_REQUEST["fromT"]!="")
		$fromTJS = $_REQUEST["fromT"];
if ($_REQUEST["toT"]!="")
		$toTJS = $_REQUEST["toT"];
	if ($fromTJS != "")
	{
		$dDateFT = explode("/",$fromTJS);
		if(count($dDateFT) == 3)
			$fromTJS = $dDateFT[2]."-".$dDateFT[1]."-".$dDateFT[0];
	}
	if ($toTJS != "")
	{
		$dDateT = explode("/",$toTJS);
		if(count($dDateT) == 3)
			$toTJS = $dDateT[2]."-".$dDateT[1]."-".$dDateT[0];
	}
	if ($fromTJS != "" || $toTJS != "")
	{
		if ($fromTJS != "" && $toTJS != "")
		{
			$queryDatedT = " and (".$date_field." >= '$fromTJS 00:00:00' and ".$date_field." <= '$toTJS 23:59:59') ";	
		}
		elseif ($fromTJS != "")
		{
			$queryDatedT = " and (".$date_field." >= '$fromTJS 00:00:00') ";	
		}
		elseif ($toTJS != "")
		{
			$queryDatedT = " and (".$date_field." <= '$toTJS 23:59:59') ";	
		}	
	}
	if ($queryDatedT != "")
	{
		$queryT .= $queryDatedT;
	}
///////////////////Date Block ended /////////////////////////

	if($tranactionSource != "" && $tranactionSource != "payex")
	{
		 $selectDistributor = "select userID from admin where username = '".$tranactionSource."'";
		$distributorResult = mysql_query($selectDistributor);
		if($row = mysql_fetch_assoc($distributorResult))
		{
			$distId = $row["userID"];
			$queryT .= " and benAgentID = '".$distId."' ";
		}
		//$tranactionSource = $_REQUEST["tranactionSource"];	
	}else{
		$selectDistributor = "select userID from admin where username in('unistream','anelik')";
		$distributorResult = mysql_query($selectDistributor);
		$distIds = array();
			while($row = mysql_fetch_assoc($distributorResult))
			{
				$distId[] = $row["userID"];
			}
			if(count($distId) > 0)
			{
				$idsString = implode(",", $distId);
				$queryT .= " and benAgentID not in (".$idsString.") ";
			}
		}

		
if($_REQUEST["Submit"] == "SearchTrans")
{
//	echo("--Before-".$amountTJS."---");
	if ($_REQUEST["amountT"]!=""){
	//	echo("---".$_REQUEST["amountT"]."---");
		$amountTJS = $_REQUEST["amountT"];
	}
//	echo("--After-".$amountTJS."---");
	if ($_REQUEST["searchBy"]!="")
		$searchByTJS = $_REQUEST["searchBy"];
	if ($_REQUEST["searchName"]!="")
		$searchNameT = $_REQUEST["searchName"];
	/*if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];*/
	if ($_REQUEST["paymentMod"]!="")
		$paymentMod = $_REQUEST["paymentMod"];
	if($_REQUEST["destCountry"] != '')
	{
		$destCountry = $_REQUEST["destCountry"];
	}
	
	if($_REQUEST["accountNo"] != ""  && $_REQUEST["accountNo"] != "null")
	{
		$accountNo = $_REQUEST["accountNo"];
	}
	
	if ($searchNameT != "")
	{
		if($searchByTJS=="customer"){
			$querysearchNameT = " and tr.transID IN (select transID from ".TBL_TRANSACTIONS." where refNumberIM='".$searchNameT."') "; 
		}
		elseif($searchByTJS=="agent"){
			if(!empty($searchNameT))
				$querysearchNameT = " and custAgentID IN (select userID from ".TBL_ADMIN_USERS." where username LIKE '".$searchNameT."%') "; 
		}
		elseif($searchByTJS=="distributor"){
			if(!empty($searchNameT))
				$querysearchNameT = " and benAgentID IN (select userID from ".TBL_ADMIN_USERS." where username LIKE '".$searchNameT."%') "; 
		}
		

	}
	if ($amountTJS != "")
	{
		$queryAmountT = " and totalAmount = $amountTJS ";	
	}
	if($accountNo != "" && $accountNo != "null")
	{
		$queryT .= " and bar.accountNo in (".$accountNo.")";;
	}
	if ($querysearchNameT != "")
	{
		$queryT .= $querysearchNameT;
	}
	if ($queryAmountT != "")
	{
		$queryT .= $queryAmountT;
	}
	if ($paymentMod != "")
	{
		$queryT .= " and moneyPaid = '".$paymentMod."'";
	}
	if ($destCountry != "" && $destCountry != "null")
	{
		$queryT .= " and toCountry in(".$destCountry.") ";
	}
	//print_r($_SESSION["lastQueryPay"]);
	
	//
}
function getBatchNumber($rowT)
{
	//debug_print_backtrace();
	$returnArray = array();
	$counter = 0;
	/* put batches into array */
	for($k =0;$k < count($rowT);$k++)
	{
		if($k == 0){
			$oldB = $rowT[$k]["batchNumber"];
			$samiCounter = 0;
			$returnArray[$counter][$samiCounter] = $rowT[$k];
		}
		else if($k >0 && $oldB == $rowT[$k]["batchNumber"])
		{
			$samiCounter ++;
			$returnArray[$counter][$samiCounter] = $rowT[$k];
		//do some code
		}
		else
		{
			$counter ++;
			$oldB = $rowT[$k]["batchNumber"];
			$samiCounter = 0;
			$returnArray[$counter][$samiCounter] = $rowT[$k];
		}
	
	}
	//debug($returnArray);
	return  $returnArray;
	
}
if($_REQUEST["getGrid"] == "transList"){
   //$queryT .= " order by batchNumber, transid, payid $sord ";
     //$queryT .= " order by $sidx $sord ";
    //debug($queryT);
	//$resultT = mysql_query($queryT) or die(__LINE__.": ".mysql_query());
	$rowT = selectMultiRecords($queryT, MYSQL_ASSOC);
	//$countT = mysql_num_rows($resultT);
	
	//debug($rowT);
	//while($rowT = mysql_fetch_array($resultT))
	$strDes ='';
	$strBAc ='';
	$batchArray =array();
	$counter = 0;
	$formatArray = array();

	//$batchArray = getBatchNumber($rowT);
	//debug($formatArray);
	
	$countT = count($rowT);
	//debug($countT);
	
	if($countT > 0)
	{
		$total_pages = ceil($countT / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryT .= " order by batchNumber, transid, payid $sord LIMIT $start , $limit";			
	
	//echo $queryT;
	//debug($queryT);
	///debug($queryT);
	$rowT = selectMultiRecords($queryT, MYSQL_ASSOC);
	
	$batchArray = getBatchNumber($rowT);
	//debug($batchArray);
	
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $countT;

	$arrNewFormatedArray = array();
	$arrMultipleJoins = array();
	
	$arrAlreadySolvedTransaction = array();
	$arrAlreadySolvedPayments = array();
	$arrResolvedBatch = array();
	$seprator = "<BR />";
	$transactions_counter =0;
	$payments_counter =0;
	
	for($j =0;$j < count($batchArray);$j++)
	{
		$arrFormat4 = array();
		$strCustName = '';
		$strBenName ='';
		$strCityName ='';
		$grandTotalAmount = 0;
		$grandPaymentAmount = 0;
		$strGrandTotalAmount =0;
		$strGrandPaymentAmount = 0;

		for($l =0;$l < count($batchArray[$j]);$l++)
		{
		
			//$arrNewFormatedArray[] = 
			
			//debug(count($batchArray[$j]));
			if($batchArray[$j][$l]["createdBy"]!="CUSTOMER")
			{
				$custQ = "select accountName,firstName, lastName from ".TBL_CUSTOMER." where customerID = '".$batchArray[$j][$l]["customerID"]."'";
				$custRS = selectFrom($custQ);
				$custNumber = $custRS["accountName"];
				$custName = $custRS["firstName"] . " ". $custRS["lastName"];
				$benQ = "select firstName, lastName from ".TBL_BENEFICIARY." where benID = '".$batchArray[$j][$l]["benID"]."'";
				$benRS = selectFrom($benQ);
				$benName = $benRS["firstName"] . " ". $benRS["lastName"];
			}
			$senderBankRS   = selectFrom("select custBankBranch from customerBank where transID ='".$batchArray[$j][$l]["transID"]."'");
			$cityName = $senderBankRS["custBankBranch"];
			
			$grandTotalAmount = $batchArray[$j][$l]["totalAmount"];
			$grandPaymentAmount= $batchArray[$j][$l]["amount"];
			
			//debug($grandTotalAmount);
			
			if($l == 0)
			{
				$transID  = $batchArray[$j][$l]["transID"];
				$batchNumber  = $batchArray[$j][$l]["batchNumber"];
				$strImportedOn  = dateFormat($batchArray[$j][$l]["importedOn"], "2");	
				$strDescription = 	str_replace("  ","",$batchArray[$j][$l]["payment_desc"]);
				$strBankAccount = $batchArray[$j][$l]["bankAccount"];
				$strPayment_amount      = number_format($batchArray[$j][$l]["amount"],"2",".","");
				$strEntryDate   = dateFormat($batchArray[$j][$l]["entryDate"], "2");
				$strTotalAmount = number_format($batchArray[$j][$l]["totalAmount"],"2",".","");
				$strRefNumberIM = $batchArray[$j][$l]["refNumberIMs"];
				$strAction_date = dateFormat($batchArray[$j][$l]["action_date"], "2");
				$strResolvedBy  = $batchArray[$j][$l]["resolvedBy"];
				$format = $batchArray[$j][$l]["format"];
				$strCustName = $custRS["firstName"] . " ". $custRS["lastName"];
				$strBenName =$benRS["firstName"] . " ". $benRS["lastName"];
				$strCityName = $senderBankRS["custBankBranch"];
				$strGrandTotalAmount = $grandTotalAmount;
				$strGrandPaymentAmount = $grandPaymentAmount;
				
				$arrFormat4['P'][] = $batchArray[$j][$l]["payID"];
				$arrFormat4['T'][] = $batchArray[$j][$l]["transID"];
				//debug($transactions_counter);
				$transactions_counter++;
				$payments_counter++;
			}
			//debug($format);
			if($format == 1 && $l == 0)
			{
				/*Case #1  Resolve One Payment With One transaction*/
				/*Resolve Multiple Payments With One Transaction*/
				$dataArray = array(
									"transID" => $transID,
									"batchNumber" => $batchNumber,
									"importedOn" => $strImportedOn,
									"payment_desc" => $strDescription,	
									"bankAccount" => $strBankAccount,
									"amount" => $strPayment_amount,	
									"entryDate" => $strEntryDate,
									"cityName" => $strCityName,	
									"totalAmount" => $strTotalAmount,
									"refNumberIM" => $strRefNumberIM,
									"custName" => $strCustName,	
									"benName" => $benName,	
									"action_date" => $strAction_date,
									"resolvedBy" => $strResolvedBy,
									"grandTransTotal" =>$strGrandTotalAmount,
									"grandPaymentTotal" => $strGrandPaymentAmount
			
				);
				//$formatArray[$j] = $dataArray;
				$arrNewFormatedArray[] = $dataArray;
				//debug($transactions_counter);
				//$arrResolvedBatch[] = $batchArray[$j][$l]['batchNumber'];
				
			}
			elseif($format == 2 && $l  > 0)
			{
					/*Resolve One Payments With Multiple Transaction*/
					$transID  = $batchArray[$j][$l]["transID"];
					$payID  = $batchArray[$j][$l]["payID"];
					$strCityName .= $seprator.$cityName;
					$strTotalAmount .= $seprator.number_format($batchArray[$j][$l]["totalAmount"],"2",".","");
					$strRefNumberIM .= $seprator.$batchArray[$j][$l]["refNumberIMs"];
					$strCustName .= $seprator.$custName;
					$strBenName  .= $seprator.$benName;
					$strAction_date .= $seprator.dateFormat($batchArray[$j][$l]["action_date"], "2");
					//$strResolvedBy .= $seprator.$batchArray[$j][$l]["resolvedBy"];
					$strGrandTotalAmount +=  $grandTotalAmount;
					$strGrandPaymentAmount +=  $grandPaymentAmount;
					$transactions_counter++;
					//debug($grandTotalAmount);
					//debug($transactions_counter);
			}
			elseif($format == 3 &&  $l > 0)
			{
				/*Resolve Multiple Payments With One Transaction*/
			
					$transID  = $batchArray[$j][$l]["transID"];
					$payID  = $batchArray[$j][$l]["payID"];
					$strImportedOn .= $seprator.dateFormat($batchArray[$j][$l]["importedOn"], "2");
					$strDescription .= $seprator.str_replace("  ","",$batchArray[$j][$l]["payment_desc"]);
					$strBankAccount .= $seprator.$batchArray[$j][$l]["bankAccount"];
					$strPayment_amount .= $seprator.number_format($batchArray[$j][$l]["amount"],"2",".","");
					$strEntryDate .= $seprator.dateFormat($batchArray[$j][$l]["action_date"], "2");
					//$grandPaymentAmount +=  number_format($batchArray[$j][$l]["totalAmount"],"2",".","");
					//$grandPaymentAmount +=  number_format($batchArray[$j][$l]["amount"],"2",".","");
					$payments_counter++;
					//debug($transactions_counter);
			}
			elseif($format == 4 &&  $l > 0)
			{
				if(!in_array($batchArray[$j][$l]["transID"], $arrFormat4['T']))
				{
					$transID  = $batchArray[$j][$l]["transID"];
					$payID  .= $seprator.$batchArray[$j][$l]["payID"];

					/* trans id*/
					$strCityName .= $seprator.$cityName;
					$strTotalAmount .= $seprator.number_format($batchArray[$j][$l]["totalAmount"],"2",".","");
					$strRefNumberIM .= $seprator.$batchArray[$j][$l]["refNumberIMs"];
					$strCustName .= $seprator.$custName;
					$strBenName .= $seprator.$benName;
					$strAction_date .= $seprator.dateFormat($batchArray[$j][$l]["action_date"], "2");
					//$strResolvedBy .= $seprator.$batchArray[$j][$l]["resolvedBy"];
					$arrFormat4['T'][] = $batchArray[$j][$l]["transID"];
					$strGrandTotalAmount +=  $grandTotalAmount;
					//$strGrandPaymentAmount +=  $grandPaymentAmount;
					$transactions_counter++;
					//debug($transactions_counter);
				}
				
				if(!in_array($batchArray[$j][$l]["payID"], $arrFormat4['P']))
				{
					$transID  = $batchArray[$j][$l]["transID"];
					$payID  .= $seprator.$batchArray[$j][$l]["payID"];
					/* pay id part */
					$strImportedOn .= $seprator.dateFormat($batchArray[$j][$l]["importedOn"], "2");
					$strDescription .= $seprator.str_replace("  ","",$batchArray[$j][$l]["payment_desc"]);
					$strBankAccount .= $seprator.$batchArray[$j][$l]["bankAccount"];
					$strPayment_amount .= $seprator.number_format($batchArray[$j][$l]["amount"],"2",".","");
					$strEntryDate .= $seprator.dateFormat($batchArray[$j][$l]["action_date"], "2");
					//$strGrandTotalAmount +=  $grandTotalAmount;
					$strGrandPaymentAmount +=  $grandPaymentAmount;
					$arrFormat4['P'][] = $batchArray[$j][$l]["payID"];
					$payments_counter++;
				}

			}
				//debug($transactions_counter);
		
		} // end inner for loop
		
		$dataArray = array(
							"transID" => $transID,
							"batchNumber" => $batchNumber,
							"importedOn" => $strImportedOn,
							"payment_desc" => $strDescription,	
							"bankAccount" => $strBankAccount,
							"amount" => $strPayment_amount,	
							"entryDate" => $strEntryDate,
							"cityName" => $strCityName,	
							"totalAmount" => $strTotalAmount,
							"refNumberIM" => $strRefNumberIM,
							"custName" => $strCustName,	
							"benName" => $strBenName,	
							"action_date" => $strAction_date,
							"resolvedBy" => $strResolvedBy,
							"grandTransTotal" => $strGrandTotalAmount,
							"grandPaymentTotal" => $strGrandPaymentAmount,
							"transactions_counter" => $transactions_counter,
							"payments_counter" => $payments_counter
		);
		//$formatArray[$j] = $dataArray;
		$arrNewFormatedArray[$j] = $dataArray;
		
		//debug($arrResolvedBatch);

	} // end outer for loop

//debug($arrNewFormatedArray);

	//debug($transactions_counter."-->".$payments_counter);
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 *
	 * @author: Waqas Bin Hasan
	 */
	
	//$query .= " order by $sidx $sord LIMIT $start , $limit";
	//$result = mysql_query($query) or die(__LINE__.": ".mysql_error());
	
	//debug($limit."-->".$page."-->".$total_pages."-->".$countT);


	$i=0;
	$totalAmount = 0;
	$totalPayment = 0;
	$j = 0;
	if($_REQUEST["returnType"] == "export")
	{
		$start = 0;
		$limit = $countT;
		
					$data = "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'> <tr>
					<td><font face='Verdana' color='#000000' size='2'><b>Payment Import Date</font></td>
					<td><font face='Verdana' color='#000000' size='2'><b>DESCRIPTION</font></b></td>
					<td><font face='Verdana' color='#000000' size='2'><b>BANK ACCOUNT</font></b></td>
					<td><font face='Verdana' color='#000000' size='2'><b>PAYMENT TOTAL AMOUNT</font></td>
					<td><font face='Verdana' color='#000000' size='2'><b>ORIGINAL STATEMENT DATE</font></td>
					<td><font face='Verdana' color='#000000' size='2'><b>CITY</font></b></td>
					<td><font face='Verdana' color='#000000' size='2'><b>TRANSACTION TOTAL AMOUNT</font></td>
					<td><font face='Verdana' color='#000000' size='2'><b>REFERENCE CODE</font></b></td>
					<td><font face='Verdana' color='#000000' size='2'><b>SENDER NAME</font></b></td>		
					<td><font face='Verdana' color='#000000' size='2'><b>BENEFICIARY NAME</font></b></td>	
					<td><font face='Verdana' color='#000000' size='2'><b>RESOLVED DATE</font></b></td>
					<td><font face='Verdana' color='#000000' size='2'><b>RESOLVED BY</font></b></td>
					</tr>";	
		}
	
	$formatArray = $arrNewFormatedArray; 
	//debug(count($formatArray));
	for($k =0;$k < count($formatArray);$k++)
	{
		/*if($start <= $i && $i < ($start+$limit))
		{*/
				$response->rows[$k]['id'] = $formatArray[$k]["transID"];
		
				$strImportDate = $formatArray[$k]["importedOn"];
				$strDes= $formatArray[$k]["payment_desc"];
				$strBAc= $formatArray[$k]["bankAccount"];
				$strAmount= $formatArray[$k]["amount"];
				$strOriginDate = $formatArray[$k]["entryDate"];
				$strCity= $formatArray[$k]["cityName"];
				$strTransAmount= $formatArray[$k]["totalAmount"];
				$strRefCode= $formatArray[$k]["refNumberIM"];
				$strSenderName= $formatArray[$k]["custName"];
				$strBenName= $formatArray[$k]["benName"];
				$strResolveDate= $formatArray[$k]["action_date"];
				$strResolveBy= $formatArray[$k]["resolvedBy"];
				$grandTransTotal= $formatArray[$k]["grandTransTotal"];
				$trans_counter= $formatArray[$k]["transactions_counter"];
				$payments_counter= $formatArray[$k]["payments_counter"];
				
				//debug ($strDes);
				
				if($_REQUEST["returnType"] == "export")
				{
						$data .= "
					<tr>
						<td>".$strImportDate."</td>
						<td>".$strDes."</td>
						<td>".$strBAc."</td>
						<td>".$strAmount."</td>
						<td>".$strOriginDate."</td>
						<td>".$strCity."</td>
						<td>".$strTransAmount."</td>
						<td>".$strRefCode."</td>
						<td>".$strSenderName."</td>		
						<td>".$strBenName."</td>	
						<td>".$strResolveDate."</td>
						<td>".$strResolveBy."</td>
					</tr>";	
				}else{
						$response->rows[$k]['cell'] = array(
									$strImportDate,
									$strDes,
									$strBAc,
									$strAmount,
									$strOriginDate,
									$strCity,
									$strTransAmount,
									$strRefCode,
									$strSenderName,
									$strBenName,
									$strResolveDate,
									$strResolveBy
								);		
					}			
								
					$j++;				
			//}						
								//debug($strTransAmount);
								$totalAmount += $grandTransTotal;
								$totalPayment+= $strAmount;
								//debug($totalAmount);
									
		$linkVal = '';
		$urlTrans = '';
	
	
		$i++;
	}
	if(empty($trans_counter))
		$trans_counter = 0;
	if(empty($payments_counter))
		$payments_counter = 0;
	//debug($arrAllData);
		$response->rows[$j]['cell'] = array(
									'',
									'<strong>Total Payments</strong>',
									"<strong>".$payments_counter."</strong>",
									'',
									'',
									'',
									"<strong>".number_format($totalAmount,"2",".","")."&nbsp;</strong>",
									'',
									'',
									'<strong>Total Transactions</strong>',
									"<strong>".$trans_counter."</strong>",
									''
								);
								
	if($_REQUEST["returnType"] == "export")
	{
		$data .= "
					<tr>
						<td> </td>
						<td><strong>Total Payments</strong> </td>
						<td>$payments_counter</td>
						<td> </td>		
						<td> </td>	
						<td> </td>	
						<td><strong>".number_format($totalAmount,"2",".","")."&nbsp;</strong></td>
						<td> </td>
						<td> </td>
						<td><strong>Total Transactions:</strong> </td>
						<td>$trans_counter</td>
						<td> </td>
					</tr>";	
		
		echo($data);
	}else{						
		echo $response->encode($response); 
	}
}	
?>