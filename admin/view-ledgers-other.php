<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();
	//$currentdate = date('d-m-Y  h:i:s A');
	$todayDate = date('Y-m-d');
	
	$id = $_REQUEST["id"];
	
	$strOtherAccount = "SELECT 
							   id,
							   transID,
							   crAmount,
							   drAmount,
							   crAccount,
							   drAccount,
							   currency,
							   description,
							   status,
							   note,
							   created
						  FROM 
							 ".TBL_ACCOUNT_LEDGERS." 
						  WHERE 1  
						  AND  id = '".$id."'
						  ";
						  
	$accountRs = selectFrom($strOtherAccount.' ORDER BY id asc');
	$created = date("Y-m-d",strtotime($accountRs["created"]));
	$transID = $accountRs["transID"];
	$accountsData[]= array("accountNumber"=>$accountRs["drAccount"],"drAmount"=>$accountRs["drAmount"],"transID"=>$transID,"created"=>$created,"type"=>"Dr");
	$accountsData[]= array("accountNumber"=>$accountRs["crAccount"],"crAmount"=>$accountRs["crAmount"],"transID"=>$transID,"created"=>$created,"type"=>"Cr");

	$totalDrAmt = 0;
	$totalCrAmt = 0;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View Accounts</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
	.ce{
		color:#0066FF;
		cursor:pointer;
	}
</style>
<script src="javascript/jquery.js"></script>
</head>

<body>
<table width="100%" border="0" align="center" cellspacing="5" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr><TD width="40%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td width="60%" align="left" class="reportHeader"><?=SYSTEM?> (Ledger Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($todayDate))?><br />
					<br />
				  </span></td></tr>
			</table>
			<br />
			<table width="82%" border="0" align="center" cellpadding="5" cellspacing="1">
				<tr>
					<td width="22%" align="center"  class="columnTitle">Date</td>
					<td width="27%" align="center"  class="columnTitle">Trans ID</td>
					<td width="22%" align="center"  class="columnTitle">Account Number</td>
					<td width="15%" align="center"  class="columnTitle">Dr. </td>
					<td width="14%" align="center"  class="columnTitle">Cr. </td>
				</tr>
		<?php foreach($accountsData as $k=>$v){?>
				<tr>
					<?php 
						$totalDrAmt += number_format($v["drAmount"], 2, ".", "");
						$totalCrAmt += number_format($v["crAmount"], 2, ".", "");
					?>
					<td width="22%" class="columnTitle"><?=$v["created"]?></td>
					<td width="27%" class="columnTitle"><?=$v["transID"]?></td>
					<td width="22%" class="columnTitle"><?=$v["accountNumber"]?></td>
					<td width="15%" align="right" class="columnTitle"><?=number_format($v["drAmount"], 2, ".", ",")?></td>
					<td width="14%" align="right" class="columnTitle"><?=number_format($v["crAmount"], 2, ".", ",")?></td>
				</tr> 
		<?php }?>
			<tr style="font-style:italic">
				<td colspan="3" align="right" class="columnTitle">Total</td>
				<td width="15%" align="right" class="columnTitle"><?=number_format($totalDrAmt, 2, ".", ",")?></td>
				<td width="14%" align="right" class="columnTitle"><?=number_format($totalCrAmt, 2, ".", ",")?></td>
			</tr> 
		  </table>
			<br />
		</td>
	</tr>
</table>
</body>
</html>
