<?
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$today = date('Y-m-d');
	$agentType=getAgentType();
	$loggedUName = $_SESSION["loggedUserData"]["username"];
	$returnPage = 'add-transaction.php';


	$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
	and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";
	
	$cumulativeRule = selectFrom($cumulativeRuleQuery);
	
	$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
	and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
	$currentRule = selectFrom($currentRuleQuery);


	/***************************************************
	*    Code from express-confirm-transaction.php     *
	***************************************************/


	$refNumber = str_replace(" ","",$_POST["refNumber"]);
	$_SESSION["refNumber"] = $refNumber;
	$_POST["refNumber"] = $refNumber;

	if (CONFIG_MANUAL_CODE_WITH_RANGE == "1" && $_SESSION["compManualCode"] != "") 
	{
		if ($_POST["refNumber"] == "") 
		{
			insertError("Please provide " . $manualCode);
			redirect($backUrl);
		}
		$qDate = "SELECT MIN(created) AS created FROM ".TBL_RECEIPT_RANGE." WHERE agentID = '".$_POST["custAgentID"]."' AND used != 'Used'";
		$qDRes = mysql_query($qDate) or die("Query Error: " . $qDate . "<br>" . mysql_error());
		$qDRow = mysql_fetch_array($qDRes);
		if ($qDRow["created"] == "") 
		{
			insertError("Please first add agent's receipt book range");
			redirect($backUrl);
		} 
		else 
		{
			$qRanVal = "SELECT * FROM ".TBL_RECEIPT_RANGE." WHERE `agentID` = '".$_POST["custAgentID"]."' AND used != 'Used' AND `created` = '".$qDRow["created"]."'";
			$rValRes = mysql_query($qRanVal) or die("Query Error: " . $qRanVal . "<br>" . mysql_error());
			$rValRow = mysql_fetch_array($rValRes);
			$usedVal = ($rValRow["used"] == "0" ? $rValRow["rangeFrom"] : $rValRow["used"]);
			
			if ($rValRow["used"] != "0") 
			{
				$usedVal++;
			}	
			$nextRangeVal = $usedVal;
			$_SESSION["refNumber"] = $_SESSION["refNumber"] . "-" . $nextRangeVal;
		}
	}
	
	/****    CODE FROM express-confirm-transaction.php    ********/
	
	
	if(CONFIG_TRANS_ROUND_LEVEL != "")
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	else
		$roundLevel = 4;
	
	if($_GET["transID"] != "")
	{
		$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$_GET["transID"]."'");
		$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$_GET["transID"]."'");
	} 
	else 
	{
		if(isset($_REQUEST["checkbox"]) && sizeof($_REQUEST["checkbox"]) > 0)
		{
			for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
			{
				$transID = $_REQUEST["checkbox"][$i];
			}
			$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$transID."'");
			$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$transID."'");
		}
	}
		
	if($_POST["customerID"]!= "")
		$customerID = $_POST["customerID"];
	else
		$customerID = $transactionQuery["customerID"];
	
	if($_POST["benID"]!= "")
		$beneficiaryID = $_POST["benID"];
	else
		$beneficiaryID = $transactionQuery["benID"];
	
	
	if($_POST["customerAgent"]!= "")
		$custAgentID = $_POST["customerAgent"];
	else
		$custAgentID = $transactionQuery["custAgentID"];
	
	$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$custAgentID."'");
	if($_POST["totalAmount"]!= "")
		$totalAmount= number_format($_POST["totalAmount"],2);
	else
		$totalAmount=number_format($transactionQuery["totalAmount"],2);	
		
	if($_POST["transAmount"]!= ""){
		$transAmount= number_format($_POST["transAmount"],2);
	}
	else{
		$transAmount=number_format($transactionQuery["transAmount"],2);	
	}
		
	if($_POST["localAmount"]!= "")
		$localAmount= $_POST["localAmount"];
	else
		$localAmount=$transactionQuery["localAmount"];	
		
	
	if($_POST["moneyPaid"]!= "")
		$moneyPaid= $_POST["moneyPaid"];
	else
		$moneyPaid=$transactionQuery["moneyPaid"];	
		
		
	if($_POST["transactionPurpose"]!= "")
		$transactionPurpose= $_POST["transactionPurpose"];
	else
		$transactionPurpose=$transactionQuery["transactionPurpose"];	
	
	
	$discount=$transactionQuery["discounted_amount"];	
	
	if($_POST["exchangeRate"]!= "")
		$exchangeRate= $_POST["exchangeRate"];
	else
		$exchangeRate=$transactionQuery["exchangeRate"];	
	
	
	if($_POST["IMFee"]!= "")
		$IMFee= $_POST["IMFee"];
	else
		$IMFee=$transactionQuery["IMFee"];	
	
	
	
	if($_POST["bankName"]!= "")
		$bankName= $_POST["bankName"];
	else
		$bankName=$bankQuery["bankName"];	
	
	if($_POST["branchCode"]!= "")
		$branchCode= $_POST["branchCode"];
	else
		$branchCode=$bankQuery["branchCode"];	
	
	if($_POST["branchAddress"]!= "")
		$branchAddress= $_POST["branchAddress"];
	else
		$branchAddress=$bankQuery["branchAddress"];	
	
	
	if($_POST["swiftCode"]!= "")
		$swiftCode= $_POST["swiftCode"];
	else
		$swiftCode=$bankQuery["swiftCode"];	
	
	
	if($_POST["accNo"]!= "")
		$accNo= $_POST["accNo"];
	else
		$accNo=$bankQuery["accNo"];	
	
	
	if($_POST["currencyTo"]!= "")
		$currencyTo= $_POST["currencyTo"];
	else
		$currencyTo=$transactionQuery["currencyTo"];	
		
		
		
	if($_POST["currencyFrom"]!= "")
		$currencyFrom= $_POST["currencyFrom"];
	else
		$currencyFrom=$transactionQuery["currencyFrom"];	
		
		
	if($_POST["benCountry"]!= "")
		$ToCountry= $_POST["benCountry"];
	else
		$ToCountry=$transactionQuery["toCountry"];	
		
		
	if($_POST["transType"] != "")
		$transactionType = $_POST["transType"];
	else
		$transactionType = $transactionQuery["transType"];	
				
	if($_POST["bankCharges"] != "")
		$bankCharges = $_POST["bankCharges"];
	else
		$bankCharges = $transactionQuery["bankCharges"];	
			
	if($_POST["outCurrCharges"] != "")
		$outCurrCharges = $_POST["outCurrCharges"];
	else
		$outCurrCharges = $transactionQuery["outCurrCharges"];	
	
	if($_POST["adminCharges"] != "")
		$adminCharges = $_POST["adminCharges"];
	else
		$adminCharges = $transactionQuery["adminCharges"];	
			
			
	if($_POST["cashCharges"] != "")
		$cashCharges = $_POST["cashCharges"];
	else
		$cashCharges = $transactionQuery["cashCharges"];	
			
	$serviceCharges = $bankCharges + 	$outCurrCharges + $adminCharges;
				
	if($_POST["refNumber"] != "")
		$refNumber=$_POST["refNumber"];
	else
		$refNumber=$transactionQuery["refNumber"];
		
	$discount=$transactionQuery["discounted_amount"];	
		
	$refNumberIM=$transactionQuery["refNumberIM"];

/**
 * data retrieved to display on top of 
 * reciept from agent of Admin Staff
 * who has made a transaction.
 * @Ticket #4453
 */
	
	$printCompany = $strAddressQuery["agentCompany"];
	$printAddress = $strAddressQuery["agentAddress"];
	$printAddress2 = $strAddressQuery["agentAddress2"];
	$printCountry = $strAddressQuery["agentCountry"];
	$printCity = $strAddressQuery["agentCity"];
	$printFax = $strAddressQuery["agentFax"];
	$printPhone = $strAddressQuery["agentPhone"];

	$printSenderNo = $CustomerData["accountName"];
	$agentUserName = $strAddressQuery["username"];
	$agentName = $strAddressQuery["name"];
	$custAgentParentID = $strAddressQuery["parentID"];
	
	$distributorId = $transactionQuery["benAgentID"];
	
	$distributorNameQuery = selectFrom("select name from ". TBL_ADMIN_USERS." where userID ='".$distributorId."'");
	$distributorName = $distributorNameQuery["name"];
	
	$currencyNameQuery = selectFrom("select description from ". TBL_CURRENCY." where currencyName ='".$currencyTo."'");
	$currencyName = $currencyNameQuery["description"];
	
	
	$fontColor = "#800000";
	$queryCust = "select *  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
	$customerContent = selectFrom($queryCust);
	
	$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
	$benificiaryContent = selectFrom($queryBen);
	
	
	$queryCust = "select *  from cm_collection_point as c, admin a where c.cp_ida_id = a.userID And cp_id  ='" . $_SESSION["collectionPointID"] . "'and a.agentStatus = 'Active'";
	$senderAgentContent = selectFrom($queryCust);

	
	// This code is added by Niaz Ahmad against #1727 at 08-01-2008
	if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1")
	{
	
		$senderAccumulativeAmount = 0;
				   
		$to = getCountryTime(CONFIG_COUNTRY_CODE);
			
		$month = substr($to,5,2);
		$year = substr($to,0,4);
		$day = substr($to,8,2);
		$noOfDays = CONFIG_NO_OF_DAYS;
		
		$fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));
		
			
		$from = $fromDate." 00:00:00"; 
		$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");

		$senderAccumulativeAmount += $senderTransAmount["transAmount"];
		 
		if($_POST["transID"] == "")
			$senderAccumulativeAmount += $_POST["transAmount"];
			
		$dateTime = explode(" ",$transactionQuery["transDate"]);
		$date = $dateTime[0];
		$time = $dateTime[1];
		
	}	
	if($agentType == "SUPA" || $agentType == "SUBA")
	{
		if($agentType == "SUBA")
		{
			$arrAgentDataSql = selectFrom("select * from admin where userID = '".$_SESSION["loggedUserData"]["parentID"]."' ");	
			$agentCompany = $arrAgentDataSql["agentCompany"];
			$agentAddress = $arrAgentDataSql["agentAddress"]; 
			$agentAddress2 = $arrAgentDataSql["agentAddress2"];

			$agentCity = $arrAgentDataSql["agentCity"];
			$agentCountry = $arrAgentDataSql["agentCountry"];
			
			$agentPhone = $arrAgentDataSql["agentPhone"];
			$agentFax = $arrAgentDataSql["agentFax"];	
		}
		else
		{
			$agentCompany = $_SESSION["loggedUserData"]["agentCompany"];
			$agentAddress = $_SESSION["loggedUserData"]["agentAddress"]; 
			$agentAddress2 = $_SESSION["loggedUserData"]["agentAddress2"];
			$agentCity = $_SESSION["loggedUserData"]["agentCity"];
			$agentCountry = $_SESSION["loggedUserData"]["agentCountry"];
			$agentPhone = $_SESSION["loggedUserData"]["agentPhone"];
			$agentFax = $_SESSION["loggedUserData"]["agentFax"];	
		}
		
	}
/**
 * On reciept of transaction. Address of
 * Agent is displayed now for Admin Staff.
 * @Ticket #4453
 */
	elseif($agentType == "Admin" && $strAddressQuery!=""){

$staffAgentDataSqlInner  = "(select linked_Agent from admin where userID='".$_SESSION["loggedUserData"]["userID"]."' )";
$staffAgentDataSqlInnerData  =selectFrom($staffAgentDataSqlInner);
$staffAgentName          = split(",",$staffAgentDataSqlInnerData['linked_Agent']);
$staffAgentDataSql = selectFrom("select * from admin where username = '".$staffAgentName[0]."'");

		$agentCompany = $printCompany;
		$agentAddress = $printAddress;
		$agentAddress2 = $printAddress2;

		$agentCity = $printCity;
		$agentCountry = $printCountry;
		
		$agentPhone = $printPhone;
		$agentFax = $printFax;
	}
	else
	{
		$agentCompany = "GLOBAL EXCHANGE LTD";
		$agentAddress = "54 Ealing Road"; 
		$agentCity = "Wembley HA0 4TQ";
		$agentPhone = "0208 902 3366";
		$agentFax = "0208 902 9922";
	}

/**
 * On the confirm and the print receipt client want to show 
 * the Passport ID number of the beneficiary
 * @Ticket #4425
 */
if(defined("CONFIG_USE_ID_TYPE_OF_BENEFICIARY"))
{
	$strPassportIdSql = "select id_number from user_id_types where user_id = '".$beneficiaryID."' and user_type='B' and id_type_id=(select id from id_types where title='".CONFIG_USE_ID_TYPE_OF_BENEFICIARY."')";
	$arrBeneficiaryId = selectFrom($strPassportIdSql);
	$strBeneficiaryId = $arrBeneficiaryId["id_number"];
}
if(!empty($distributorId))
{
	$arrDistributorData = selectFrom("select name from admin where userid='".$distributorId."'");
}

?>
<html>
<head>
	<title>Transaction Confirmation</title>
	
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
	<link href="styles/displaying.css" rel="stylesheet" type="text/css" media="print">


<style type="text/css">
<!--
.boldFont {
	font-weight: bold;
}

.headingFont {
	font-weight: bold;
	font-size: 14px;
}


.leftAlign
{
	text-align:right;
}
-->
</style>
</head>

<body>
	<? if($_REQUEST["re"] == "y") {?>
		<!--div style="position: absolute; left: 390px; top: 35px; height: 20px; width: 75px; padding: 2px; border: dashed; border-width:1px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#999999; text-align:center; vertical-align:middle">
			<b>RE-PRINT</b>
		</div-->  
	<? }else{ ?>
		<!--div style="position: absolute; left: 390px; top: 35px; height: 20px; width: 60px; padding: 2px; border: dashed; border-width:1px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#999999; text-align:center; vertical-align:middle">
			<b>PRINT</b>
		</div-->  
	<? } ?>
	<table align="center" cellpadding="0" cellspacing="0" border="0" width="690">
	<tr>
	<td width="30" align="center" valign="middle">
		<img width="30" src="images/globalexchange/customer-copy.GIF" />
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		<br /><br /><br /><br /><br /><br />
		<img width="30" src="images/connectplus/office-copy.jpg" />	</td>
	<td width="660" align="center"><table width="660" border="0" cellspacing="3" cellpadding="2" bordercolor="#333333">
      <tr bgcolor="#502727">
        <td  colspan="4" height="30"><div align="right"> <span class="headingFont"> <font color="#FFFFFF">
            &nbsp;<?=$agentCompany?>
        </font> </span>. </div></td>
      </tr>
      <tr>
        <td colspan="3" align="right"><div align="left"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></div></td>
        <td align="right"><?php 
						echo $agentAddress.(!empty($agentAddress2)?", ".$agentAddress2:"").", ";
						echo $agentCity.(!empty($agentCountry)?", ".$agentCountry:"");
						echo "&nbsp;&nbsp;Phone no: ".$agentPhone." Fax: ".$agentFax;
					?></td>
      </tr>
      <tr>
        <td class="leftAlign">TRAN NO.</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$transactionQuery["refNumberIM"]?>&nbsp;
        </b></td>
        <td rowspan="3" class="leftAlign" valign="top"> Receiver <br>
          Address</td>
        <td rowspan="3" valign="top" style="border-width:1px; border-color:#333333; border-style:solid">
		
		<b><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["lastName"]; ?></b><br />
		<? echo $benificiaryContent["Address"].", ".$benificiaryContent["Address1"];?> <br>
          <? echo $benificiaryContent["City"].", ".$benificiaryContent["State"].", <br/ >".$benificiaryContent["Zip"].", ".$benificiaryContent["Country"];?>&nbsp; </td>
      </tr>
      <tr>
        <td class="leftAlign">Date / Time </td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $date." ".$time; ?></td>
        </tr>
      <tr>
        <td class="leftAlign">Customer No</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["accountName"];?>&nbsp;</td>
        </tr>
      <tr>
        <td rowspan="2" class="leftAlign" valign="top">Name<br>
          Address</td>
        <td rowspan="2" valign="top"style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["firstName"] ." ".$customerContent["lastName"];?><br>
          <? echo $customerContent["Address"].", ".$customerContent["Address1"];?>&nbsp;</td>
        <td height="25" class="leftAlign">Tel Number</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $benificiaryContent["Phone"]?>&nbsp;</td>
      </tr>
      <tr>
        <td height="24" valign="top" class="leftAlign"><?=CONFIG_USE_ID_TYPE_OF_BENEFICIARY?>
&nbsp;ID Number</td>
        <td valign="top" style="border-width:1px; border-color:#333333; border-style:solid"><?=$strBeneficiaryId?>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top" class="leftAlign">Telephone</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["Phone"]?>&nbsp;</td>
        <td rowspan="2" valign="top" class="leftAlign">Bank Name<br>
          Address</td>
        <td rowspan="2" valign="top" style="border-width:1px; border-color:#333333; border-style:solid"><?
					if($transactionQuery["transType"] == "Bank Transfer")
						echo $bankQuery["bankName"];
					else
						echo $senderAgentContent["cp_corresspondent_name"];
				?>
          <br>          <?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						echo $bankQuery["branchCode"].", <br />";
						echo $bankQuery["branchAddress"].".<br />";
					}
					else
					{
						echo $senderAgentContent["cp_branch_address"];
						echo ",<br />".$senderAgentContent["cp_city"].", <br />".$senderAgentContent["cp_state"];
						echo ", ".$senderAgentContent["cp_country"].".";
					}
					?>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top" class="leftAlign">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td class="leftAlign" valign="top">LKR(FC)</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$localAmount." ".$currencyTo?>&nbsp;
        </b></td>
        <td class="leftAlign" valign="top">Account No </td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						
						echo  $bankQuery["accNo"];
					}
					else
					{
						echo "";
					}
					?>&nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Rate</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $transactionQuery["exchangeRate"]; ?></td>
        <td class="leftAlign">Swift/BIC/BSB</td>
        <td align="left" style="border-width:1px; border-color:#333333; border-style:solid">
		      <?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						
						echo $bankQuery["swiftCode"];
					}
					else
					{
							echo "";
					}
					?>&nbsp;
		</td>
      </tr>
      <tr>
        <td height="25" class="leftAlign">Amount &pound;</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><?=$transAmount?>&nbsp;</td>
        <td class="leftAlign">Collect At</td>
		<td style="border-width:1px; border-color:#333333; border-style:solid">
		  <?
					if($transactionQuery["transType"] == "Bank Transfer")
					echo $bankQuery["bankName"];
					else
					echo $senderAgentContent["cp_corresspondent_name"];
					?>&nbsp;
		</td>
      </tr>
      <tr>
        <td class="leftAlign">Charges</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $transactionQuery["IMFee"]; ?></td>
        <td class="leftAlign">Reason</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$transactionQuery["transactionPurpose"]?>
          &nbsp;</b></td>
      </tr>
      <tr>
        <td class="leftAlign">Total Amount</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=number_format($transactionQuery["totalAmount"],2)?>
          GBP</b></td>
        <td class="leftAlign"> Funds Code</td>
          <td width="107" style="border-width:1px; border-color:#333333; border-style:solid"><b><?=$transactionQuery["refNumber"]?></b>&nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Recieved &pound;</td>
        <td><b style="border-width:1px; border-color:#333333; border-style:solid">&nbsp;
              <?=$transactionQuery["recievedAmount"]?>
          &nbsp;</b> in <b style="border-width:1px; border-color:#333333; border-style:solid"> &nbsp;
            <?=$transactionQuery["moneyPaid"]?>
            &nbsp;</b> </td>
        <td class="leftAlign">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Outstanding</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?
				 
				 $transOutstanding = $transactionQuery["totalAmount"] - $transactionQuery["recievedAmount"]; 
				  echo number_format($transOutstanding,2,'.',',');
				 
				 ?>
          GBP</b></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     
      <tr>
        <td colspan="4" align="center"><font size="2">Declaration</font></td>
      </tr>
      <tr>
        <td height="23">Authorise signature</td>
        <td>_____________________  <br />
          Global Exchange Ltd.</td>
        <td>Customer Signature</td>
        <td>&nbsp;_____________________</td>
      </tr>
    </table>
	  <!--------  Customer Copy    ---------->
      <br />
      <br />
		<table width="660" border="0" cellspacing="3" cellpadding="2" class="print" bordercolor="#333333" style="border-style:solid">
          <tr bgcolor="#502727">
            <td  colspan="4" height="30"><div align="right"> <span class="headingFont"> <font color="#FFFFFF">
              <?=$agentCompany?>
            </font> </span>. </div></td>
          </tr>
          <tr>
            <td colspan="3" align="right"><div align="left"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></div></td>
        <td align="right"><?php 
						echo $agentAddress.(!empty($agentAddress2)?", ".$agentAddress2:"").", ";
						echo $agentCity.(!empty($agentCountry)?", ".$agentCountry:"");
						echo "&nbsp;&nbsp;Phone no: ".$agentPhone." Fax: ".$agentFax;
					?></td>
      </tr>
      <tr>
        <td class="leftAlign">TRAN NO.</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$transactionQuery["refNumberIM"]?>&nbsp;
        </b></td>
        <td rowspan="3" class="leftAlign" valign="top"> Receiver <br>
          Address</td>
        <td rowspan="3" valign="top" style="border-width:1px; border-color:#333333; border-style:solid">
		
		<b><? echo $benificiaryContent["firstName"]." ".$benificiaryContent["lastName"]; ?></b><br />
		<? echo $benificiaryContent["Address"].", ".$benificiaryContent["Address1"];?> <br>
          <? echo $benificiaryContent["City"].", ".$benificiaryContent["State"].", <br/ >".$benificiaryContent["Zip"].", ".$benificiaryContent["Country"];?> &nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Date / Time </td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $date." ".$time; ?>&nbsp;</td>
        </tr>
      <tr>
        <td class="leftAlign">Customer No</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["accountName"];?></td>
        </tr>
      <tr>
        <td rowspan="2" class="leftAlign" valign="top">Name<br>
          Address</td>
        <td rowspan="2" valign="top"style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["firstName"] ." ".$customerContent["lastName"];?><br>
          <? echo $customerContent["Address"].", ".$customerContent["Address1"];?></td>
        <td height="25" class="leftAlign">Tel Number</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $benificiaryContent["Phone"]?>&nbsp;</td>
      </tr>
      <tr>
        <td height="24" valign="top" class="leftAlign"><?=CONFIG_USE_ID_TYPE_OF_BENEFICIARY?>
&nbsp;ID Number</td>
        <td valign="top" style="border-width:1px; border-color:#333333; border-style:solid"><?=$strBeneficiaryId?>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top" class="leftAlign">Telephone</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $customerContent["Phone"]?>&nbsp;</td>
        <td rowspan="2" valign="top" class="leftAlign">Bank Name<br>
          Address</td>
        <td rowspan="2" valign="top" style="border-width:1px; border-color:#333333; border-style:solid"><?
					if($transactionQuery["transType"] == "Bank Transfer")
						echo $bankQuery["bankName"];
					else
						echo $senderAgentContent["cp_corresspondent_name"];
				?>
          <br>          <?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						echo $bankQuery["branchCode"].", <br />";
						echo $bankQuery["branchAddress"].".<br />";
					}
					else
					{
						echo $senderAgentContent["cp_branch_address"];
						echo ",<br />".$senderAgentContent["cp_city"].", <br />".$senderAgentContent["cp_state"];
						echo ", ".$senderAgentContent["cp_country"].".";
					}
					?>&nbsp;</td>
      </tr>
      <tr>
        <td valign="top" class="leftAlign">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td class="leftAlign" valign="top">LKR(FC)</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$localAmount." ".$currencyTo?>
        </b></td>
        <td class="leftAlign" valign="top">Account No </td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						
						echo  $bankQuery["accNo"];
					}
					else
					{
						echo "";
					}
					?></td>
      </tr>
      <tr>
        <td class="leftAlign">Rate</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $transactionQuery["exchangeRate"]; ?></td>
        <td class="leftAlign">Swift/BIC/BSB</td>
        <td align="left" style="border-width:1px; border-color:#333333; border-style:solid">
		      <?php 
					if($transactionQuery["transType"] == "Bank Transfer")
					{
						
						echo $bankQuery["swiftCode"];
					}
					else
					{
							echo "";
					}
					?>	    &nbsp;</td>
      </tr>
      <tr>
        <td height="25" class="leftAlign">Amount &pound;</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><?=$transAmount?></td>
        <td class="leftAlign">Collect At</td>
		<td style="border-width:1px; border-color:#333333; border-style:solid">
		  <?
					if($transactionQuery["transType"] == "Bank Transfer")
					echo $bankQuery["bankName"];
					else
					echo $senderAgentContent["cp_corresspondent_name"];
					?>		</td>
      </tr>
      <tr>
        <td class="leftAlign">Charges</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><? echo $transactionQuery["IMFee"]; ?></td>
        <td class="leftAlign">Reason</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=$transactionQuery["transactionPurpose"]?>
          &nbsp;</b></td>
      </tr>
      <tr>
        <td class="leftAlign">Total Amount</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?=number_format($transactionQuery["totalAmount"],2)?>
          GBP</b></td>
        <td class="leftAlign"> Funds Code</td>
      <td width="107" style="border-width:1px; border-color:#333333; border-style:solid"><b><?=$transactionQuery["refNumber"]?></b>&nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Recieved &pound;</td>
        <td><b style="border-width:1px; border-color:#333333; border-style:solid">&nbsp;
              <?=$transactionQuery["recievedAmount"]?>
          &nbsp;</b> in <b style="border-width:1px; border-color:#333333; border-style:solid"> &nbsp;
            <?=$transactionQuery["moneyPaid"]?>
            &nbsp;</b> </td>
        <td class="leftAlign">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="leftAlign">Outstanding</td>
        <td style="border-width:1px; border-color:#333333; border-style:solid"><b>
          <?
				 
				 $transOutstanding = $transactionQuery["totalAmount"] - $transactionQuery["recievedAmount"]; 
				  echo number_format($transOutstanding,2,'.',',');
				 
				 ?>
          GBP</b></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     
      <tr>
        <td colspan="4" align="center"><font size="2">Declaration</font></td>
      </tr>
      <tr>
        <td height="23">Authorise signature</td>
        <td>_____________________  <br />
          Global Exchange Ltd.</td>
        <td>Customer Signature</td>
        <td>&nbsp;_____________________</td>
          </tr>
        </table>
		<div class='noPrint'>
			<table align="center">
				<tr>
					<td align="center">
						<input type="button" value="PRINT TRANSACTION" onClick="print();" />					</td>
				</tr>
			</table>
    </div>	</td></tr></table>
    
</body>
</html>
