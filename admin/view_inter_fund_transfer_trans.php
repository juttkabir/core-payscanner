<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$modifyby = $_SESSION["loggedUserData"]["userID"];

$currencyies = selectMultiRecords("SELECT DISTINCT(currencySell) as sending_currency  FROM ".TBL_ACCOUNT_DETAILS);
$Recvcurrencyies = selectMultiRecords("SELECT DISTINCT(currencyBuy) as receiving_currency  FROM ".TBL_ACCOUNT_DETAILS);


$strLabel = 'Inter Fund Transfer Transaction Report';

$linkedAgent = array();

$strSql = "SELECT 
				id,
				accounNumber,
				accounType,
				accountName,
				currency 
			FROM 
				accounts 
			WHERE 
				status = 'AC' ";  

/*$strSql = "SELECT 
				id,
				accounNumber,
				accounType,
				accountName,
				currency 
			FROM 
				accounts 
			WHERE 1 "; */			
										
$src = selectMultiRecords($strSql." AND CustID = ''");
$AmbAcc	= selectMultiRecords($strSql." AND CustID = ''");				
?>
<html>
<head>
<title><?=$strLabel?></title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>

<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var subgridimgpath = 'javascript/jqGrid/themes/green/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	var comSubGrid = 'comSubGrid';
	jQuery(document).ready(function(){
		$("#loading").ajaxStart(function(){
		   $("#confirmFlag").val("");
		 });

		var lastSel;
		var maxRows = 20;
		jQuery("#comMainList").jqGrid({
			url:"viewFTtrans_ajax.php?getGrid=intComReport&nd="+new Date().getTime()+"&q=1",
			datatype: "json",
			height: 300,
			width: 900,
			colNames:['Sending Account', 'Amount','Currency', 'Total transactions'],
			colModel:[				
				{name:'sendingAccount',index:'sendingAccount', width:100},
				{name:'totTransAm',index:'totTransAm', width:100, align:"right"},
				{name:'currency',index:'currency', width:100},
				{name:'cntTrans',index:'cntTrans', width:70, align:'center'}
				
			],
			imgpath:gridimgpath,
			rowNum:maxRows,
			rowList: [20,50,100],
			pager: jQuery('#pagernav'),
			sortname: 'sendingAccount',
			viewrecords: true,
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			multiselect: false,
			forceFit: true,
			shrinkToFit: true,
			subGrid: true,
			caption: "<?=$strLabel?>",
			subGridRowExpanded: function(subgrid_id, row_id) {
				// we pass two parameters
				// subgrid_id is a id of the div tag created whitin a table data
				// the id of this elemenet is a combination of the "sg_" + id of the row
				// the row_id is the id of the row
				// If we wan to pass additinal parameters to the url we can use
				// a method getRowData(row_id) - which returns associative array in type name-value
				// here we can easy construct the flowing
				$("#comMainGridID").val(row_id);
				var subgrid_table_id, pager_id;
				subgrid_table_id = comSubGrid;//subgrid_id+"_t";
				//alert(subgrid_table_id);
				pager_id = "p_"+subgrid_table_id;
				$("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
				
				var from = jQuery("#from").val();
				var to = jQuery("#to").val();
				var transNumber = jQuery("#transNumber").val();
				transNumber = transNumber.replace("#","");
				var SenderID = '';
				var RecvAcc  = '';
				if(jQuery("#SenderID").val() != undefined)
				var	SenderID = jQuery("#SenderID").val();
				if(jQuery("#RecvAcc").val() != undefined)
				var	RecvAcc = jQuery("#RecvAcc").val();
				var currencyTrans = jQuery("#currencyTrans").val();
				var RecvcurrencyTrans = jQuery("#RecvcurrencyTrans").val();
				var comMainGridID = jQuery("#comMainGridID").val();
				
				var extraParamsSubGrid = "&from="+from+"&to="+to+"&transNumber="+transNumber+"&SenderID="+SenderID+"&RecvAcc="+RecvAcc+"&currencyTrans="+currencyTrans+"&RecvcurrencyTrans="+RecvcurrencyTrans+"&Submit=SearchTrans";
							
				jQuery("#"+subgrid_table_id).jqGrid({
					url:"viewFTtrans_ajax.php?getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+row_id+extraParamsSubGrid,
					datatype: "json",
					height: 100,
					width: 700,

					colNames:['Transaction Number', 'Receiving Account', 
							  'Date', 'Sending Amount', 'Sending Currency', 
							  'Rate','Receiving Amount', 'Receiving Currency','Status'
							 ],
					
					colModel:[
						
						{name:'TransNumberT',index:'TransNumberT', width:30, align:"right"},
						{name:'receiving_account',index:'receiving_account', width:30, align:"right"},
						{name:'TransDate',index:'TransDate', width:40, align:'center'},
						{name:'totTransAmT',index:'totTransAmT', width:40, align:'right'},
						{name:'sending_currency',index:'sending_currency', width:40, align:'left'},
						{name:'exchangeRate',index:'exchangeRate', width:40, align:'right'},
						{name:'totRecvAm',index:'totRecvAm', width:40, align:'right'},
						{name:'receiving_currency',index:'receiving_currency', width:40, align:'left'},
						{name:'status',index:'status', width:40, align:'left'}												
					],
					imgpath:subgridimgpath, 
					rowNum:maxRows,
					rowList: [20,50,100],
					pager: pager_id,
					//sortname: 'num',
					multiselect: true
				});
				jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false})
				
				//alert('*');
			},
			subGridRowColapsed: function(subgrid_id, row_id) {
				// this function is called before removing the data
				var subgrid_table_id;
				subgrid_table_id = comSubGrid;//subgrid_id+"_t";
				jQuery("#"+subgrid_table_id).remove();
				$("#comMainGridID").val('');
			}
		});
		jQuery("#comMainList").jqGrid('navGrid','#pagernav',{add:false,edit:false,del:false});
		jQuery('a').cluetip({splitTitle: '|'});

	
		jQuery("#btnCancel").click( function(){
			//var confirmFlag = $("#confirmFlag").val();
			//if(confirmFlag == ""){
				var confirmMsg = confirm('Are you sure to Proceed?');
				$("#confirmFlag").val("Y");
				if($(this).val()=="Cancel" && confirmMsg==true){
					var allTransIDs = jQuery("#"+comSubGrid).getGridParam('selarrrow');
					
					if(allTransIDs!=''){
						//var allTransIDs = $("#allTransIDsV").val();
						var allTransIdsArr = allTransIDs;
						//$("#allTransIDsV").val(allTransIdsArr);
						$("#payListCancelData").load("viewFTtrans_ajax.php", { 'transIDs[]': allTransIdsArr,'btnCancel': 'Cancel'},
						function(){
							
						});
					}
					
					gridReload('comMainList');
				}
			
		});
		
		jQuery("#btnUnHold").click( function(){
			//var confirmFlag = $("#confirmFlag").val();
			//if(confirmFlag == ""){
				var confirmMsg = confirm('Are you sure to Proceed?');
				$("#confirmFlag").val("Y");
				if($(this).val()=="UnHold" && confirmMsg==true){
					var allTransIDs = jQuery("#"+comSubGrid).getGridParam('selarrrow');
					
					if(allTransIDs!=''){
						//var allTransIDs = $("#allTransIDsV").val();
						var allTransIdsArr = allTransIDs;
						//$("#allTransIDsV").val(allTransIdsArr);
						$("#payListHoldData").load("viewFTtrans_ajax.php", { 'transIDs[]': allTransIdsArr,'btnUnHold': 'UnHold'},
						function(){
							
						});
					}
					
					gridReload('comMainList');
				}
			
		});
		
		
		
		
	});

	function gridReload(grid)
	{
		var theUrl = "viewFTtrans_ajax.php";
		
		if(grid=='comMainList' || grid=='comSubGrid'){
			var extraParam;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var transNumber = jQuery("#transNumber").val();
			//transNumber = transNumber.replace(/[^a-zA-Z 0-9]+/g,'');
			transNumber = transNumber.replace("#","");
			var SenderID = '';
			var RecvAcc  ='';
			if(jQuery("#SenderID").val() != undefined)
			var	SenderID = jQuery("#SenderID").val();
			
			if(jQuery("#RecvAcc").val() != undefined)
			var	RecvAcc = jQuery("#RecvAcc").val();
			
			var currencyTrans = jQuery("#currencyTrans").val();
			var RecvcurrencyTrans = jQuery("#RecvcurrencyTrans").val();
			var comMainGridID = jQuery("#comMainGridID").val();

			if(grid=='comMainList'){
				extraParam = "?from="+from+"&to="+to+"&transNumber="+transNumber+"&SenderID="+SenderID+"&RecvAcc="+RecvAcc+"&currencyTrans="+currencyTrans+"&RecvcurrencyTrans="+RecvcurrencyTrans+"&Submit=SearchTrans&getGrid=intComReport";
			}
			else{
				extraParam = "?from="+from+"&to="+to+"&transNumber="+transNumber+"&SenderID="+SenderID+"&RecvAcc="+RecvAcc+"&currencyTrans="+currencyTrans+"&RecvcurrencyTrans="+RecvcurrencyTrans+"&Submit=SearchTrans&getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+comMainGridID;
			}
			jQuery("#"+grid).setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
<div id="loading"></div>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF"><?=$strLabel?></font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
    
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="from" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    </td>
                </tr>
				
				
				
				
		<tr>
  <td nowrap="" align="center">
  
  <fieldset>
    <legend><b>Transaction</b></legend>
    <lable>Transaction Number : </lable>
    <input name="transNumber" id="transNumber" />
   
     <br/>
    </fieldset>
  
  
  
  
  <fieldset>
    <legend><b>Sending</b></legend>
    <lable>Account # : </lable>
    <select name="SenderID" id="SenderID">
		<option value="">ALL</option>
     <?php 
	  for ($i=0; $i < count($src); $i++){
					
					?>
					<option value="<?=$src[$i]["accounNumber"]; ?>"><? echo $src[$i]["accountName"] . " [" . $src[$i]["accounNumber"] . "]" ; ?></option>	
					<?
				
		
			} ?>
    </select>
   
    <lable>Currency : </lable>
		<select name="currencyTrans" id="currencyTrans">
			<option value="">ALL</option>
			<? if(count($currencyies)>0){?>
				<? for($cp=0;$cp<count($currencyies);$cp++){?>
				<option value="<?=$currencyies[$cp]["sending_currency"]?>">
				<?=$currencyies[$cp]["sending_currency"]?>
				</option>
				<? }?>
			<? }?>
		</select>
    <br/>
    </fieldset>
    <fieldset>
    <legend><b>Receiving</b></legend>
    <lable>Account # : </lable>
    <select name="RecvAcc" id="RecvAcc">
		<option value="">ALL</option>
      <?php 
	  for ($i=0; $i < count($AmbAcc); $i++){
				?>
					<option value="<?=$AmbAcc[$i]["accounNumber"]; ?>"><? echo $AmbAcc[$i]["accountName"] . " [" . $AmbAcc[$i]["accounNumber"] . "]" ; ?></option>	
					<?
				
		
			} ?>
    </select>
    <lable>Currency</lable>
   		<select name="RecvcurrencyTrans" id="RecvcurrencyTrans">
			<option value="">ALL</option>
			<? if(count($Recvcurrencyies)>0){?>
				<? for($cp=0;$cp<count($Recvcurrencyies);$cp++){?>
				<option value="<?=$Recvcurrencyies[$cp]["receiving_currency"]?>">
				<?=$Recvcurrencyies[$cp]["receiving_currency"]?>
				</option>
				<? }?>
			<? }?>
		</select>
    <br/>
    </fieldset></td>
</tr>

	  <tr>
         <td nowrap align="center">
	
		  	  
          &nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit" value=" Search " onClick="gridReload('comMainList')">
		<input type="reset" name="reset" value="Reset">		 </td>
    </table>  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="comMainList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListCancelData" style="visibility:hidden;"></div>
				<div id="payListHoldData" style="visibility:hidden;"></div>	</td>
    </tr>
	<?php if($agentType != 'SUPA' && $agentType != 'SUBA'){?>
	  <tr bgcolor="#FFFFFF">
	   <td align="center">
		<input name="btnCancel" id="btnCancel" type="button"  value="Cancel">
		<!--<input name="btnUnHold" id="btnUnHold" type="button"  value="UnHold">-->
		<input type='hidden' name='comMainGridID' id='comMainGridID' value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="confirmFlag" id="confirmFlag" value="">
		</td>
	  </tr>
	<?php }?>
</table>
	</td>
  </tr>
</table>
</form>
</body>
</html>