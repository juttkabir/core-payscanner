<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$parentID = $_SESSION["loggedUserData"]["userID"];


if ($offset == "")
	$offset = 0;
$limit=50;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

$arrAllConfData = selectFrom("Select 
									id,
									customer_account_days_limit 
								from 
									users_configuration 
								where 
									isEnable = 'Y'     AND
									updated_on =(select max(updated_on) from users_configuration where isEnable = 'Y')
									");
		if(!empty($arrAllConfData["id"]))
			$daysLimit = $arrAllConfData["customer_account_days_limit"];
			
		//$current_date_time = date('Y-m-d h:i:s');
	    $current_date_time = date('Y-m-d');
		

/*function date_diff2($d1, $d2){
$d1 = (is_string($d1) ? strtotime($d1) : $d1);
$d2 = (is_string($d2) ? strtotime($d2) : $d2);
$diff_secs = abs($d1 - $d2);
$base_year = min(date("Y", $d1), date("Y", $d2));
$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
return array(
"years" => date("Y", $diff) - $base_year,
"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
"months" => date("n", $diff) - 1,
"days_total" => floor($diff_secs / (3600 * 24)),
"days" => date("j", $diff) - 1,
"hours_total" => floor($diff_secs / 3600),
"hours" => date("G", $diff),
"minutes_total" => floor($diff_secs / 60),
"minutes" => (int) date("i", $diff),
"seconds_total" => $diff_secs,
"seconds" => (int) date("s", $diff)
);
}*/						
		$arrcustId = array();
		$strBalanceSql = "Select customerID from ".TBL_CUSTOMER."  where balance > 0";
		$cusBalance = selectMultiRecords($strBalanceSql);
	
		for($k=0 ; $k< count($cusBalance) ; $k++)
		{
		$strAccountSql = selectFrom("SELECT 
								caID,
								customerID,
								Date
								
						  FROM 
								agents_customer_account 
						  WHERE 
								customerID = '".$cusBalance[$k][customerID]."' AND
								Date = (select max(Date) from agents_customer_account where 
								customerID = '".$cusBalance[$k][customerID]."')
								
								");
	 
		if(!empty($strAccountSql["caID"]))			 
			$daysDiff = date_diff($strAccountSql["Date"],$current_date_time);	
		
		if($daysDiff >= $daysLimit)
			{
				$arrcustId[] = $strAccountSql["customerID"];
				
			}
		}
		
		$strcustid = implode(",",$arrcustId);
		
	   if(!empty($strcustid)){
                $queryCust = "Select     
	                                customerID,
									balance,
									Title,
									firstName,
									middleName,
									lastName,
									accountName,
									Address,
									City,
									Country
								from 
									".TBL_CUSTOMER."
								where 
									customerID IN(".$strcustid.")
									";
									
				$strQueryCnt = "Select     
	                                count(customerID)
								from 
									".TBL_CUSTOMER."
								where 
									customerID IN(".$strcustid.")
									";					

$allCount = countRecords($strQueryCnt);

$queryCust .= " LIMIT $offset , $limit";
$contentsCust = selectMultiRecords($queryCust);

}

?>
<html>
<head>
	<title>Search Customers</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
.link{
color: #3366CC;
font:Arial, Helvetica, sans-serif;
font-weight:bold;
text-decoration:none;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 <form action="<?=$_SERVER['PHP_SELF']?>" method="get" name="trans">
  <tr>
    <td class="topbar"><strong><font color="#3366CC" size="2">
      Customers List </font></strong></td>
  </tr>
  
  <tr>
    <td align="center">
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
       
	    <tr>
            <td height="25" nowrap bgcolor="333333">
<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
            <tr>
		
              <td><?php if (count($contentsCust) > 0) {;?>
      Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsCust));?></b> of
      <?=$allCount; ?>
      <?php } ;?>
              </td>
              <?php if ($prv >= 0) { ?>
              <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a> </td>
              <?php } ?>
              <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
              <?php } ?>
            </tr>
          </table></td>
        </tr>
		<?
		if(count($contentsCust) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Customer Name </span></td>
			  <td><span class="style1">Address</span></td>
			  <td><span class="style1">Balance</span></td>
			  <td><span class="style1">Customer Account</span></td>
			  <td><span class="style1">Create Transaction</span></td>			
			</tr>
		    <? for($i=0;$i<count($contentsCust);$i++)
			{
				?>
				<tr bgcolor="#FFFFFF">
				  <td width="200"><strong><font color="#006699"><? echo $contentsCust[$i]["Title"] .  " " . $contentsCust[$i]["firstName"] . " " . $contentsCust[$i]["middleName"] . " " . $contentsCust[$i]["lastName"]?></font></strong></td>
				  <td width="158"><? echo $contentsCust[$i]["Address"] . " " . $contentsCust[$i]["city"] . " " . $contentsCust[$i]["Country"]?></td>
				  <td width="125"><? echo $contentsCust[$i]["balance"]?></td>
				  <td width="125"><a class="link" href="AgentsCustomerAccount.php?customerID=<?=$contentsCust[$i]["customerID"]?>&custID=<?=$contentsCust[$i]["accountName"]?>&search=search">Deposit</a>
				  </td>		
				    <td width="125"><a class="link" href="add-transaction.php?create=Y&customerID=<?=$contentsCust[$i]["customerID"]?>">Create Transaction</a>
				  
				  </td>		
				 
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			   <td>&nbsp;</td>
			    <td>&nbsp;</td>
			 </tr>
			<?
		}else{// greater than zero
			?>
			<tr bgcolor="#FFFFFF">
			  <td colspan="4">&nbsp; No Record to View.</td>
			</tr>
			<? } 
			?>
          </table></td>
        </tr>
		
      </table></td>
  </tr>
</form>
</table>
</body>
</html>
