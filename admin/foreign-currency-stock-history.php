<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$userName  = $_SESSION["loggedUserData"]["name"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$date_time = date('d-m-Y  h:i:s A');
	
	/**
	* Fireign Currency Stock History Report 
	* AMB: #9363 
	* Created by Kalim ul Haq
	* Jan 18, 2012
	**/
	
	/*** Currency Dropdown Menu Query Start ***/
	$strCurrencyQuery	= "SELECT
								cID, 
								currencyName 
							FROM 
								".TBL_CURRENCY." 
							WHERE 
								cID IN 
								(
									SELECT 
										buysellCurrency 
									FROM 
										curr_exchange 
									ORDER BY id DESC
								)
								OR
								cID IN 
								(
									SELECT 
										operationCurrency 
									FROM 
										curr_exchange 
									ORDER BY id DESC
								)
							 GROUP BY
							 	currencyName";
								
	$arrCurrenciesRows = SelectMultiRecords($strCurrencyQuery);
	/*** Currency Dropdown Menu Query END ***/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>FC Stock History</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<!-- jqGrid JS Start -->
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script> 
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<!--<script src="javascript/jqGrid/js/inlineedit.js" type="text/javascript"></script>-->
<!-- jqGrid JS Start -->

<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>

<script>
$(document).ready(function() {

	/* vaildations of the fields start */

	$("#search_fc_stock_history").validate({
		rules: {
			currency:"required"
		},
		messages: {
			currency: "<br />Please Select Foreign Currency"
		},
		submitHandler: function(form) {
			//Control Form Submition logic
		}
	});
	
	/* Validation ends */
	

	/***  Grid For FC Stock History ***/	
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';  
	$("#transList").jqGrid({
		url:'add-currency-exchange-conf.php?get=getStockHistoryGrid',
		datatype: "json",
		height: 400, 
		width: 1000,
		colNames:[
			'Batch ID',
			'Date In',
			'Amount',
			'Used Amount',
			'Available Amount',
			'Buying Rate',
			'Purchased From',
			'Status',
			'Stock Closing Date'
		],
		colModel:[
			{name:'id',					index:'id', 				width:15, 	align:"center"	},
			{name:'dateIN',				index:'created_date', 		width:30, 	align:"left"	},
			{name:'fltAmount',			index:'amount', 			width:30, 	align:"right"	},
			{name:'fltUsedAmount',		index:'used_amount', 		width:30, 	align:"right"	},
			{name:'fltAvailableAmount',	index:'available_amount', 	width:30, 	align:"right"	},
			{name:'fltBuyingRate',		index:'buying_rate', 		width:30, 	align:"right"	},
			{name:'strPurchasedFrom',	index:'purshased_from', 	width:30,	align:"left"	},
			{name:'strStatus',			index:'status', 			width:30, 	align:"left"	},
			{name:'dateClosing',		index:'closing_date', 		width:30, 	align:"left"	}
		],
		rowNum: 20,
		rowList: [5,10,20,50,100],
		imgpath: gridimgpath,
		pager: jQuery('#pager'),
		sortname: 'id',
		//viewrecords: true,
		sortorder: "desc",
		loadonce: false,
		loadtext: "Loading, please wait...",
		loadui: "block",
		forceFit: true,
		shrinkToFit: true,
		multiselect:true,
		cellEdit: false,
		//cellurl: 'currency-revaluation-conf.php?get=editRate',
		caption: "Foreign Currency Stock History",
		afterSaveCell:function(rowid, cellname, value, iRow, iCol){
			// event triggered after saving certain editable field value
		}		
		,onSelectRow:function(id){
			// write logic would be triggered on selecting certain row
		}
		,ondblClickRow:function(id){
			// write logic would be triggered on double clicking certain row
		}
	});	
	
	jQuery('a').cluetip({splitTitle: '|'});
	$("#from_date").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});
	$("#to_date").datePicker({
		startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
	});
	
	$("#btnStockHistorySearch").click(function() {
		gridReload();
	});
	/* Export FC Stock History Report */
	
	$("#exportBtn").click(function(){
			$("#exporType").removeAttr("style");
		});
	
	$("#exporType").change(function() {
	
		var strExpType	= $(this).val();
		var intCurrID	= $("#currency").val();
		var from_date	= $("#from_date").val();
		var to_date		= $("#to_date").val();
		
		if(strExpType != "")
		{			
			var strExportUrl = "fc-stock-history-export.php?exportType="+strExpType+"&currencyID="+intCurrID+"&from_date="+from_date+"&to_date="+to_date;
			window.open(strExportUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
		}
	});
});
	
function gridReload(extraParams)
{
	var intCurrID	= $("#currency").val();
	var from_date	= $("#from_date").val();
	var to_date		= $("#to_date").val(); 
	var theUrl="add-currency-exchange-conf.php?get=getStockHistoryGrid&currencyID="+intCurrID+"&from_date="+from_date+"&to_date="+to_date+"&Submit=Search";
	if(extraParams)
		theUrl = theUrl+extraParams;
	$("#transList").setGridParam({
		url: theUrl,
		page:1
	}).trigger("reloadGrid");
}
</script>


<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="javascript/jqGrid/themes/basic/grid.css" title="basic" media="screen" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="coffee" href="javascript/jqGrid/themes/coffee/grid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<!-- jqGrid CSS End -->
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style type="text/css">
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form id="search_fc_stock_history" name="search_fc_stock_history" action="" method="post">
<table width="50%" align="center" border="0" cellpadding="5">
	<tr bgcolor="#DFE6EA">
		<th colspan="2"><h2>FC Stock History</h2></th>
	</tr>
	<tr bgcolor="#EDEDED">
		<td align="center" colspan="2"><font color="red">*</font> Compulsory Fields.</td>
	</tr>
	<tr bgcolor="#EDEDED">
		<td align="right">Currency :&nbsp;<font color="red">*</font></td>
		<td><select name="currency" id="currency" <?=$disabledCurr;?>>
			<option value="">- Select Currency -</option>
			<?php
			if(count($arrCurrenciesRows)>0)
			{
				foreach($arrCurrenciesRows as $key => $val)
				{?>
					<option value="<?=$val["cID"]?>"><?=$val["currencyName"]?></option>
				<?php
				} 
			}?>
			</select>
		</td>
	</tr>
	
	<tr bgcolor="#EDEDED">
		<td>Date In From :&nbsp;
			<input type="text" name="from_date" id="from_date" readonly="" />
		</td>
		<td>Date In To :&nbsp;
			<input type="text" name="to_date" id="to_date" readonly="" />
		</td>
	</tr>
	
	<tr bgcolor="#EDEDED">
		<td align="right"><input type="submit" id="btnStockHistorySearch" name="submit" title="" value="Search" /></td>
		<td align="left"><input type="reset" id="resetFields" name="reset" title="" value="Clear Fields" /></td>
	</tr>
	<tr bgcolor="#EDEDED">
		<td colspan="2" align="center">
			<input type="button" name="exportBtn" id="exportBtn" value="Export" />
			<select name="exporType" id="exporType" style="display:none">
				<option value="">Select Format</option>
				<option value="XLS">Excel</option>
				<option value="CSV">CSV</option>
				<option value="HTML">HTML</option>
			</select>
		</td>
	</tr> 
	
	<tr bgcolor="#EDEDED">
		<td colspan="2">&nbsp;</td>
	</tr>
	
</table>
</form>
<table width="90%" align="center" border="0">
	<tr>
		<td>
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="100%" style="margin:auto">
			</table>
			<div id="pager" class="scroll" style="text-align:center;"></div>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
			
</body>
</html>
