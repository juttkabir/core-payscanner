<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d H:i:s');
include ("security.php");
$agentType = getAgentType();

extract(getHttpVars());
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$userID  = $_SESSION["loggedUserData"]["userID"];

if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=100;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}


if($Submit == "Search" && $customHours !='')
{
	
	$customHours = trim($customHours);
			$strTransSql = "select 
						transID, 
						transDate, 
						refNumberIM, 
						refNumber, 
						transStatus, 
						totalAmount, 
						transType
	
					from
						 ". TBL_TRANSACTIONS . " 
					where 
						 (transStatus = 'Authorize'	 OR transStatus = 'Pending') AND
						 (DATE_ADD(authoriseDate,INTERVAL ".$customHours." HOUR) < '".$date_time."' OR
						  DATE_ADD(transDate,INTERVAL ".$customHours." HOUR) < '".$date_time."')
					  ";

		$strTransCnt = "select 
						count(transID)
					from ". TBL_TRANSACTIONS . " 
				    where 	 
						(transStatus = 'Authorize'	 OR transStatus = 'Pending') AND
						 (DATE_ADD(authoriseDate,INTERVAL ".$customHours." HOUR) < '".$date_time."' OR
						  DATE_ADD(transDate,INTERVAL ".$customHours." HOUR) < '".$date_time."')
					  ";


	$allCount = countRecords($strTransCnt);
	$strTransSql .= " LIMIT $offset , $limit";
	$contentsTrans = selectMultiRecords($strTransSql);
}
	
?>
<html>
<head>
	<title>View Transaction Status Alerts</title>
<script language="javascript" src="./javascript/functions.js"></script>
<!-- <script language="javascript" src="./javascript/compliance_ajax.js"></script> -->
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
	color:#005b90;
	font-weight:bold;
}
-->
    </style>
    
 <script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

	function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   -->
</script>   
</head>
<body>
	
<table width="100%" border="0" cellspacing="1" cellpadding="5">
    <form action="trans-status-alert.php" method="post" name="Search" >
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">View Transaction Status Alerts </font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table width="450" border="1" cellpadding="5" bordercolor="#666666">
	
      
      <tr>
         <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
      </tr>
      
      <tr> 
   	  <td align="center" >
	        Hours 
	          <input type="text" name="customHours" value="<?=$customHours?>" > 
                     
       </td>
      </tr>  
        
    
     <tr> <td align="center">
		 <input type="submit" name="Submit" value="Search"> 
		</td>
    </tr>
	   
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666" id="txthint">
      


          <?
			if (count($contentsTrans) > 0){
		?>
    
      <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
         
         <? if($msg!= '') { ?>
			    <tr>
				  <td colspan="8" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></i></b><strong><? echo $msg; ?></strong></font></td>
			    </tr>
			   <? } ?>	
                <tr>
                  <td>
                    <?php if ($allCount > 0) {
                    
                    	?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ?>
                  </td>
                  
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>

    
    
         
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
		<tr bgcolor="#FFFFFF">
		<td width="85"><strong><font color="#006699">Date</font></strong></td> 
	    <td width="85"><span class="style1"><strong><font color="#006699"><? echo $systemCode;?></font></strong></span></td> 	
	   <td><span class="style1"><strong><font color="#006699"><? echo $manualCode;?></font></strong></span></td>
      <td><span class="style1"><strong><font color="#006699">Status</font></strong></span></td>    
      <td><span class="style1"><strong><font color="#006699">Amount</font></strong></span></td>
      <td><span class="style1"><strong><font color="#006699">Type</font></strong></span></td>
    </tr>
		    <? 
		    
		    for($i=0;$i < count($contentsTrans);$i++){
		    
			?>
				
				<tr bgcolor="#FFFFFF">
				
				 <td> &nbsp; 
            <?  //echo $contentsTrans[$i]["transDate"]; ?>
			<a href="#" class="links" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>',
			'TranDetails','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
			<? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></a>
          </td>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
          </td>
         <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumber"]; ?>
          </td>
        <td width="100"> &nbsp; 
            <?  echo $contentsTrans[$i]["transStatus"]; ?>
          </td>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["totalAmount"]; ?>
          </td>
         
        <td> &nbsp; 
            <?  echo $contentsTrans[$i]["transType"]; ?>
          </td>
		</tr>
			
		<?
			}
			
		} // greater than zero
			
		?>
          </table></td>
        </tr>

     <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset + count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&amount=$amount&amountCriteria=$amountCriteria&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
          

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No data found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		
      </table></td>
  </tr>
</form>
</table>
</body>
</html>