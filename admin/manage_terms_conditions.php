<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
    $date_time = date('Y-m-d  h:i:s');
	$strMainCaption = "Manage Company Terms & Conditions"; 

	$cmd = $_REQUEST["cmd"];

	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into company_detail 
							(
							company_terms_conditions,
							dated,userID
							)
							 values
								(
								'".$_REQUEST["txtTermCondition"]."',
								'".$date_time."',
								".$_REQUEST["agent"]."
								)";
							
				if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$cID = "";
			
		}
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["cID"]))
			{
				$strUpdateSql = "update company_detail 
								 set
									company_terms_conditions = '".$_REQUEST["txtTermCondition"]."',
									dated = '".$date_time."'
								 where
									id = '".$_REQUEST["cID"]."'	"; 
									
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$cID = "";
				$cmd = "";
						
			}	
		}
		
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["cID"]))
			{
				$strGetDataSql = "select * from company_detail where id='".$_REQUEST["cID"]."'";
				$arrCompanyData = selectFrom($strGetDataSql);
													
				$txtTermCondition = $arrCompanyData["company_terms_conditions"];
										
				$cmd = "UPDATE";
				$cID = $_REQUEST["cID"];
				}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["cID"]))
			{
				$strDelSql = "delete from company_detail where id='".$_REQUEST["cID"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}

	if(empty($cmd))
		$cmd = "ADD";
	
	/* The default amount in "amount from" field should be '1' */
	if(empty($amount_from))
		$amount_from = 1;
	
	/* Fetching the list of secret questions to display at the bottom */	
	if($agentType == 'SUPA' || $agentType == 'SUBA'){
		$arrAllCompanyData = selectMultiRecords("Select * from company_detail where userID = $userID order by id");
	}
	else{
		$arrAllCompanyData = selectMultiRecords("Select * from company_detail order by id");
	}

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Company Terms & Conditions</title>
<script language="javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.cluetip.js"></script>
<script>
$(document).ready(function() {
	$("#companyForm").validate({
		rules: {
			txtTermCondition: {
				required: true
			}
		},
		messages: {
			txtTermCondition: {
				required: "<br />Please provide company terms & conditions."
			}
		}
	});
	
  
			
  $('img').cluetip({splitTitle:'|'});
});
	

function disableFields(strFieldName, chrVal) 
{ 
	var rad_val = '';	
	
	if(chrVal == "Y")
		document.getElementById(strFieldName).disabled = false;
	else
		document.getElementById(strFieldName).disabled = true;
	
/*	for (var i=0; i < companyForm.companyValue.length; i++)
   {
   if (companyForm.companyValue[i].checked)
      {
      	rad_val = companyForm.companyValue[i].value;
      }
	 */
   }
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
.style1 {color: #FF0000}
</style>
</head>
<body>
<form name="companyForm" id="companyForm" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="550" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
			 All fields mark with <font color="red">*</font> are compulsory Fields.</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td colspan="4" valign="top" class="tdDefination">
		   <table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" id="addressTble">
		<tr>
			<td>
				Select Agent
			</td>
			<td>
				<select name="agent" id="agent" style="font-family:verdana; font-size: 11px; width:226">
					<option value="">- Select One -</option>
					<?
					if($agentType=='SUPA'|| $agentType == 'SUBA')
					{
						$agents = selectMultiRecords("select userID, agentCompany, username from ".TBL_ADMIN_USERS." where (userID = $userID OR parentID = $userID) and adminType = 'Agent' and (agentType='Supper' OR agentType = 'Sub') and isCorrespondent = 'N' and agentStatus = 'Active' order by agentCompany");
					}

					else
					{
						$agents = selectMultiRecords("select userID, agentCompany, username from ".TBL_ADMIN_USERS." where parentID > 0 and adminType = 'Agent' and (agentType='Supper' OR agentType = 'Sub') and isCorrespondent = 'N' and agentStatus = 'Active' order by agentCompany");
					}
					for ($i=0; $i < count($agents); $i++){
						if($agents[$i]["userID"] == $userID)
						{
							?>
							<option value="<? echo $agents[$i]["userID"]; ?>" selected>
								<? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
							</option>
							<?
						}
						else
						{
							?>
							<option value="<? echo $agents[$i]["userID"]; ?>">
								<? echo $agents[$i]["username"]." [".$agents[$i]["agentCompany"]."]"; ?>
							</option>
							<?
						}
					}
					?>
				</select>
			</td>
		</tr>
		   <tr bgcolor="#ededed">

		   	<td valign="top" class="tdDefination">Terms And Conditions<span class="style1"> *</span> </td>
		    <td align="left">
			<? 
			$text = 'write here your company Terms & Conditions. For Bold text write like &lt;b&gt;AMB&lt;/b&gt;';
			$text2 =htmlspecialchars($text);
			//$htmlTag= html_entity_decode($text2);
			?>
			<textarea name="txtTermCondition" cols="50" rows="8" id="txtTermCondition"><?=$txtTermCondition?></textarea>
			<img src="images/info.gif" border="0" title="Terms And Conditions |<?=$text2?>" />
			</td>
		   </tr>
<?php /*?>
			<tr bgcolor="#ededed">
			<td class="tdDefination">Company Name </td>
		    <td align="left">
			
			<input type="text" name="txtCompanyName" id="txtCompanyName" size="40" value="<?=$txtCompanyName?>" />			</td>
		
				  
	    </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Company Address </td>
		  <td align="left">
		   <input type="text" name="txtCompanyAddress1" id="txtCompanyAddress1" size="40" value="<?=$txtCompanyAddress1?>" />		  </td>
	     
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Company Timings</td>
		  <td align="left">
		     <input type="text" name="txtCompanyAddress2" id="txtCompanyAddress2" size="40" value="<?=$txtCompanyAddress2?>" />		  </td>
	     
	  </tr>
		
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Country</td>
		  <td align="left">
		  <input type="text" name="txtCompanyCountry" id="txtCompanyCountry" size="40" value="<?=$txtCompanyCountry?>" />		  </td>
	     
	  </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Phone</td>
		  <td align="left">
		  <input type="text" name="txtCompanyPhone1" id="txtCompanyPhone1" size="40" value="<?=$txtCompanyPhone1?>" />		  </td>
	      
	  </tr>
		
		   <tr bgcolor="#ededed">
			<td class="tdDefination">Advertisement Heading </td>
		    <td align="left">
			<input type="text" name="txtHeading" id="txtHeading" size="40" value="<?=$txtHeading?>" />			</td>
		
		
		   
	    </tr>
		<tr bgcolor="#ededed">
		  <td class="tdDefination">Advertisement Message </td>
		  <td align="left"><input type="text" name="txtMessage" id="txtMessage" size="40" value="<?=$txtMessage?>" /></td>
	    
	  </tr><?php */?>
	 
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="cID" value="<?=$cID?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($cID)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>			</td>
		</tr>
  </table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;Company Detail &nbsp;
	</legend>
	<table width="100%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="60%">Terms & Conditions</th>
			<th width="20%">Actions</th>
		</tr>
		<?php
			foreach($arrAllCompanyData as $companyVal)
			{
				
					
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=stripcslashes($companyVal["company_terms_conditions"])?></td>
		<td valign="top">
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$companyVal["id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit Company Terms & Conditions| You can edit the record by clicking on this thumbnail." />				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$companyVal["id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove Company Terms & Conditions|By clicking on this the record will be no longer available." />				</a>		  </td>
		</tr>
				
		<?
			}
		?>
</table>
</fieldset>

</body>
</html>