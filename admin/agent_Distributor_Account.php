<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];


session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];
if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
	$_SESSION["closingBalance"] = "";
	$_SESSION["openingBalance"] = "";
	$_SESSION["currDate"] = "";	

}

if($_POST["agentID"]!="")
	$_SESSION["agentID2"] =$_POST["agentID"];
	elseif($_GET["agentID"]!="")
		$_SESSION["agentID2"]=$_GET["agentID"];
	else 
		$_SESSION["agentID2"] = "";
		
if($_REQUEST["currency"]!="")
	$_SESSION["currency"] =$_REQUEST["currency"];

if ($offset == "")
	$offset = 0;
$limit=10;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

   	  	
      	  	
      	  
?>
<html>
<head>
	<title>Agent Account</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
function exportAgentDistAccount(){
	var exportFormat 	= document.getElementById("exportFormat").value;
	var allCountRows 	= document.getElementById("allCountRows").value;
	var fromDate 		= document.getElementById("fromDate").value;
	var toDate 			= document.getElementById("toDate").value;
	var currency		= document.getElementById("currency").value;
	var slctID 			= document.getElementById("agentID").value;
	if(exportFormat!="" && allCountRows>0){
		window.open('exportAgentDistributorAccount.php?agentID='+slctID+'&fromDate='+fromDate+'&toDate='+toDate+'&exportType='+exportFormat+'&allCountRows='+allCountRows+'&currency='+currency+'','Export A&D Account Statement', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420');
	}
	else if(exportFormat==""){
		alert("Please Select export type");
		return false;	
	}
	else{
		alert("Please First Select filters and Search.");
		return false;
	}
}
function checkForm(){
	var currency = document.getElementById("currency").value;
	if(currency==""){
		alert("Please select a currency");
		return false;
	}
}
function hideDivs(){
document.getElementById("printDiv").style.display="none";
document.getElementById("serchDiv").style.display="none";
document.getElementById("depostiWithdrawRow").style.display="none";
document.location.reload();
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="1" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">A&D Account Statement</td>
  </tr>
	<tr>
      <td width="563">
		<form name="searchForm" action="agent_Distributor_Account.php" method="post" name="Search"  onSubmit="return checkForm();">
  			<div align="center" id="serchDiv">From 
        		<? 
				$month = date("m");
		
				$day = date("d");
				$year = date("Y");
				?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                	<?
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
		  			?>
                </select>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        		</script>
		
				<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         			SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        		</script>
                
				<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{	
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
		  		?>
                </SELECT>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        		</script>
                
				To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  	<?
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}	
		  			?>
                </select>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        		</script>
		 		
				<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         			SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        		</script>
               
			    <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                	<?
					$cYear=date("Y");
					for ($Year=2004;$Year<=$cYear;$Year++)
					{
						echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
					}
		  			?>
                </SELECT>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        		</script>
                
				<br>
                &nbsp;&nbsp; 
                <?    
				if($agentType == "admin" || $agentType == "Admin" || $agentType == "Admin Manager" || $agentType == "COLLECTOR" || $agentType == "Branch Manager")//101
				{
				?>
                <select id="agentID" name="agentID" style="font-family:verdana; font-size: 11px">
                    <option value="">- Select One-</option>
                    <?
		                $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent = 'Y'";
					
						if($agentType == "Branch Manager")
						{
							$agentQuery .= " and parentID = '$changedBy'";				
									
						}
						$agentQuery .= "order by agentCompany";
						$agents = selectMultiRecords($agentQuery);
						for ($i=0; $i < count($agents); $i++){
						?>
                    <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                    <?
						}
						?>
                </select>
                <script language="JavaScript">
			         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                    </script>
                 <? 
				 } 
				 ?>
          
          		<? 
          
          		if($agentType == "SUPAI")
          		{
          			$_SESSION["agentID2"] = $_SESSION["loggedUserData"]["userID"];
          		}
          
          
          		if(CONFIG_ADD_CURRENCY_DROPDOWN == "1")
          		{
          		?>       
          			<br><br>
								Currency&nbsp;<font color="#FF0000">*</font> :
								<select id="currency" name="currency" style="font-family:verdana; font-size: 11px; width:130">
		          			<option value="">- Select Currency -</option>
          				<?
          	  			$queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES;
 	                	$DestCurrencyData = selectMultiRecords($queryDestCurrency);
 	                    
										$queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES;
										$OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
											
										for($a = 0; $a < count($DestCurrencyData); $a++)
										{
										 	$allCurrency[$a] = $DestCurrencyData[$a]['currency'];
											$y++;
										}
															
										for($b = 0; $b < count($OriginCurrencyData); $b++)
										{
												$allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
												$y++;
										}
										$uniqueCurrency = array_unique($allCurrency);
				 	                
				         		$uniqueCurrency = array_values ($uniqueCurrency);
										
				   	   			for($k = 0; $k < count($uniqueCurrency); $k++)
				   	   			{
										?>
											<option value="<?=trim($uniqueCurrency[$k])?>"><?=trim($uniqueCurrency[$k])?></option>
										<? 
										} 
										?>
					</select>
           
             		<script language="JavaScript">
         	    		SelectOption(document.forms[0].currency, "<?=$_REQUEST["currency"]; ?>");
             		</script>
						  <? 
							} 
							?>
	            <br>
					
					<input type="submit" name="Submit" value="Search">
					<br/><br/>

            </div>
				<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
			$Balance="";
			//if($_REQUEST["Submit"]=="search")
				$slctID = $_REQUEST["agentID"];
			
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
		if ($slctID !="")
		{
			/* make currency filter field mandatory for searching */			
			if(CONFIG_ADD_CURRENCY_DROPDOWN == "1")
			{
				$accountQuery = "Select * from ".TBL_AnD_ACCOUNT." where agentID = $slctID";
				$accountQuery .= " and $queryDate";				
				$accountQuery .= " and status != 'Provisional' ";

				$prevDateQuery = "Select dated from ".TBL_AnD_ACCOUNT." where agentID = $slctID";
				$accountQuery .= " and status != 'Provisional' ";
				$prevDateQuery .= " and $queryDate";
						
				$accountQueryCnt = "Select count(*) from ".TBL_AnD_ACCOUNT." where agentID = $slctID";
				$accountQueryCnt .= " and $queryDate";				
				$accountQueryCnt .= " and status != 'Provisional' ";
				
				/* adding the currency criteria */
				if($_REQUEST["currency"]!=""){
					$accountQuery .= " and currency = '".$_REQUEST["currency"]."' ";
					$accountQueryCnt .= " and currency = '".$_REQUEST["currency"]."' ";	
					$prevDateQuery .= " and currency ='".$_REQUEST["currency"]."' ";
				}
				$allCount = countRecords($accountQueryCnt);
				$accountQuery .= " order by aaID LIMIT $offset , $limit"; 
				$contentsAcc = selectMultiRecords($accountQuery);
			}
		}
		else
		{
			$slctID = $changedBy;
			$accountQuery="Select * from ".TBL_AnD_ACCOUNT." where agentID = $changedBy";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= " and status != 'Provisional' ";
				
			//$contentsAcc = selectMultiRecords($accountQuery);
		
			$accountQueryCnt = "Select count(*) from ".TBL_AnD_ACCOUNT." where agentID = $changedBy";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
			$prevDateQuery .= "Select dated from ".TBL_AnD_ACCOUNT." where agentID = $changedBy";
			$prevDateQuery .= " and $queryDate";
			$prevDateQuery .= " and status != 'Provisional' ";
			
			if(CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
			 	if($_SESSION["currency"]!="")	{
			 		$accountQuery .= " and currency = '".$_SESSION["currency"]."' ";
			 		$accountQueryCnt .= " and currency = '".$_SESSION["currency"]."' ";	
					$prevDateQuery .= " and currency = '".$_SESSION["currency"]."' ";	
				}
			}
			$allCount = countRecords($accountQueryCnt);
			$accountQuery .= " order by aaID"; 
			$accountQuery .= " LIMIT $offset , $limit"; 
			$contentsAcc = selectMultiRecords($accountQuery);
			}
			$currDate1 = $currDate2 = $currDate;
			if(CONFIG_SWAP_CLOSING_TO_OPENING=="1"){
				// this is to assign date Query String in pagination value on top of records as it is not in loop of records.
				$currDate1 = $currDate = $contentsAcc[count($contentsAcc)-1]["dated"];
				if($_GET["newOffset"]>$limit){
					$prevDateQuery .= " ORDER BY aaID LIMIT ".($offset-$limit-1)." , 1";
					$prevDateData = selectMultiRecords($prevDateQuery);
					$currDate2 = $prevDateData[0]["dated"];
				}
//				$lastaaID = $contentsAcc[count($contentsAcc)-1]["aaID"];
//				debug((ceil($allCount / $limit)-1)*$limit+$limit);
			}
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr id="paginationRow">
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) { ?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first&currency=$_SESSION[currency]";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate2&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre&currency=$_SESSION[currency]";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate1&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next&currency=$_SESSION[currency]";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&currDate=$currDate&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last&currency=".$_REQUEST["currency"];?>"><font color="#005b90">Last</font></a>&nbsp; </td>
				  <?php } ?>
				</tr>
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			<td>
				<? if(CONFIG_AGENT_STATEMENT_CUMULATIVE == "1") {?>
				<table>
					<tr>
						
						<td width="300">
							Cumulative Total <? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate);  
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>			</td>
			
			<td>
				<? if ($allAmount < 0 ){ ?>
				<font color="red"><strong>A&D is to pay the money </strong></font>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<strong> A&D Credit Balance  </strong>
			<? } ?>			</td>
		</tr>
			</table>
			<? } ?>			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <? if(CONFIG_AGENT_STATEMENT_BALANCE == '1') { ?>
					  	<td align="center"><span class="style1">Opening Balance</span></td>
					  <? } ?>
					  <td align="left" ><span class="style1">Modified By</span></td>
					  <td align="left" ><span class="style1">Money In</span></td>
					  <td align="left" ><span class="style1">Money Out</span></td>
					  <td align="left" ><span class="style1">Transaction</span></td>
					  <td align="left" ><span class="style1">Description</span></td>
					  <td align="left" ><span class="style1">Note</span></td>	
					  <td align="left" ><span class="style1">Status</span></td>
					  <? if(CONFIG_AGENT_STATEMENT_BALANCE == '1') { ?>
			  		  	<td align="center"><span class="style1">Closing Balance</span></td>
					  <? } ?>
				</tr>
				<? 
				
/*				$preDate = "";
				$oldDate = "";
				$openingBalanceSwap = "";
				if(CONFIG_SWAP_CLOSING_TO_OPENING!="1"){
					if($_SESSION["currDate"] != "")
						$preDate = $_SESSION["currDate"];
					if($_SESSION["closingBalance"] != "")	
						$closingBalance = $_SESSION["closingBalance"];
					if($_SESSION["openingBalance"] != "")	
						$openingBalance = $_SESSION["openingBalance"];
				}*/
				
				if(CONFIG_SWAP_CLOSING_TO_OPENING=="1"){
					$preDate = "";
					$openingBalanceSwap = "";
				}
				else{
					if($_SESSION["currDate"] != "")
						$preDate = $_SESSION["currDate"];
					if($_SESSION["closingBalance"] != "")	
						$closingBalance = $_SESSION["closingBalance"];
					if($_SESSION["openingBalance"] != "")	
						$openingBalance = $_SESSION["openingBalance"];
				}
				for($i=0;$i < count($contentsAcc);$i++)
				{
					$currDate1 = $currDate2 = $currDate;
					if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
					{
						$currDate = $contentsAcc[$i]["dated"];
						if($currDate != $preDate && CONFIG_SWAP_CLOSING_TO_OPENING!="1")
						{
							if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
							}
							else if($_REQUEST["currency"]!=""){
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency = '".$_REQUEST["currency"]."' ");
							}
							else{
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
							}
							$FirstDated = $datesFrom["datedFrom"];
							$openingBalance = 0;
							
							if($FirstDated != "")
							{
								$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
								$account1 .= " and dated = '$FirstDated'";				
								$account1 .= " and  user_id = '".$slctID."' ";
								if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='')
									$account1 .= " and  currency='".$settleCurr."' ";		
								if($_REQUEST["currency"]!="")
									$account1 .=" and currency = '".$_REQUEST["currency"]."' ";								

								$contents1 = selectFrom($account1);
								
								if($currDate == $FirstDated){
									$openingBalance = $contents1["opening_balance"];
								}
								else{
									$openingBalance =  $contents1["closing_balance"];
							
								}
							}
							
							
							$closingBalance = $openingBalance;
							$preDate = $currDate;
							$_SESSION["openingBalance"] = $openingBalance;
							//$_SESSION["settleOpeningBalance"] = $settleOpeningBalance;
							$_SESSION["currDate"] = $currDate;
		
						}
					/* # 4635 - To show opening balances of same currencies. and swaping closing of previous date
					 to opening of next date otherwise keeping same(ashahid)
					 */
/*					else if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING == "1"){
							if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
								$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$currDate."' and currency='".$settleCurr."'  order by id");
							}
							else if($_REQUEST["currency"]!=""){
								$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$contentsAcc[$i]["dated"]."' and currency = '".$_REQUEST["currency"]."' order by id");
								//debug("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated = '".$currDate."' and currency = '".$_REQUEST["currency"]."' order by id");
							}
							else{
								$summaryFirst = selectFrom("select id as firsID from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated ='".$currDate."' order by id");
							}
							$firsID = $summaryFirst["firsID"];
							if($_REQUEST["currency"]!="" && $firsID!=""){
								$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." 
														where id = '".$firsID."'" ; 
								$contents1 = selectFrom($account1);
								if($preDate == $currDate){
									$openingBalance = $openingBalanceSwap;
									$closingBalance = $closingBalance;
									//debug($oldDate ."=". $currentDate);
								}
								else if($preDate < $currDate && $preDate!=""){
									$openingBalance =  $closingBalance;
									$closingBalance = $openingBalance;
									//debug($oldDate ."<". $currentDate);
								}
								else{
									$openingBalance = $contents1["opening_balance"];
									$closingBalance = $openingBalance;								
								}
								$openingBalanceSwap = $openingBalance;
								$preDate = $currDate;
							}

						}
					}*/
					else if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING == "1"){
							$currDate = $contentsAcc[$i]["dated"];
								//$openingBalance = 0;
								$openingBalanceSwap = $closingBalance;
								
								if($i==0){
									// Total Transfer Withdraw Amount before first date.
									$summaryWithdrawSQL = "SELECT sum(amount) as openingAmountWith FROM ".TBL_AnD_ACCOUNT." WHERE agentID = '$slctID' and aaID<".$contentsAcc[$i]["aaID"];	
									//$summaryWithdrawSQL .= " AND (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') AND type='WITHDRAW'";
									$summaryWithdrawSQL .= " AND type='WITHDRAW' ";
									$summaryFirstWithdraw = selectFrom($summaryWithdrawSQL);
									// Total Transfer Deposit Amount before first date.
									$summaryDepositSQL = "SELECT sum(amount) as openingAmountDep FROM ".TBL_AnD_ACCOUNT." WHERE agentID = '$slctID' and aaID<".$contentsAcc[$i]["aaID"];
									//$summaryDepositSQL .= " AND (note LIKE 'INTER AnD%' OR note LIKE 'INTER AGENT%') AND type='DEPOSIT'";
									$summaryDepositSQL .= " AND type='DEPOSIT'";
									$summaryFirstDeposit = selectFrom($summaryDepositSQL);
									// Difference of Total Withdraw and Deposit is Opening for First date.
									$openingBalance =  $summaryFirstDeposit["openingAmountDep"]-$summaryFirstWithdraw["openingAmountWith"];
									$closingBalance = $openingBalance;
									$openingBalanceSwap = $openingBalance;
									if($currDate==$_GET["currDate"]){ // this logic is to retain correct the opening balance if date is same on next page.

										// Re-calculates opening balance in case next page first record has same date as previous page last record
										$summaryCompositWithSQL = "SELECT sum(amount) as compositWith FROM ".TBL_AnD_ACCOUNT." WHERE agentID = '$slctID' ";
										$summaryCompositWithSQL .= " and dated<'".$currDate."' AND type='WITHDRAW' ";
										$summaryCompositWith = selectFrom($summaryCompositWithSQL);
										$summaryCompositDepSQL = "SELECT sum(amount) as compositDep FROM ".TBL_AnD_ACCOUNT." WHERE agentID = '$slctID' ";
										$summaryCompositDepSQL .= " and dated<'".$currDate."' AND type='DEPOSIT' ";
										$summaryCompositDep = selectFrom($summaryCompositDepSQL);
										$openingBalance = $summaryCompositDep["compositDep"]-$summaryCompositWith["compositWith"];

/*										// TO exclude uncleared amounts
										$unclearedWithSQL = "SELECT sum(amount) as unclearedWith FROM " . TBL_AGENT_ACCOUNT . " WHERE agentID = '$slctID' ";
										$unclearedWithSQL .= " and dated='".$currDate."' AND chequeStatus='UNCLEARED' AND type='WITHDRAW' and aaID<".$contentsAcc[$i]["aaID"];
										$unclearedDepSQL .= $transManualQ;
										$unclearedWith = selectFrom($unclearedWithSQL);
										$unclearedDepSQL = "SELECT sum(amount) as unclearedDep FROM " . TBL_AGENT_ACCOUNT . " WHERE agentID = '$slctID' ";
										$unclearedDepSQL .= " and dated='".$currDate."' AND chequeStatus='UNCLEARED' AND type='DEPOSIT' and aaID<".$contentsAcc[$i]["aaID"];
										$unclearedDepSQL .= $transManualQ;
										$unclearedDep = selectFrom($unclearedDepSQL);
										$closingBalanceUncleared = $unclearedDep["unclearedDep"]-$unclearedWith["unclearedWith"];
										if($closingBalanceUncleared!="")
											$closingBalance=$openingBalance+$closingBalanceUncleared;*/
										$openingBalanceSwap = $closingBalance;
									}
								}
								else{
									if($preDate==$currDate){
										$openingBalance =  $openingBalance;
									}
									else{
										$openingBalance =  $closingBalance;	
									}
								}
								$closingBalance =  $openingBalanceSwap;
								$currentAmount = $contentsAcc[$i]["amount"];
								$preDate = $currDate;
							}
					}
				//$BeneficiryID = $contentsBen[$i]["benID"];
				$currSending = $contentsAcc[$i]["currency"];
				
					?>
				<tr bgcolor="#FFFFFF" title="<?=$contentsAcc[$i]["aaID"]?>">
					<td width="200" align="left"><strong><font color="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					
					<? if(CONFIG_AGENT_STATEMENT_BALANCE == '1') { 
					?>
					
						<td align="center"><? echo number_format($openingBalance,2,'.',','); ?></td> 
					<? } ?>
					
					<td width="200" align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo $contentsAcc[$i]["amount"];
						if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$contentsAcc[$i]["currency"];}
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo $contentsAcc[$i]["amount"];
						if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$contentsAcc[$i]["currency"];}
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?></td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["description"]; ?>				</td>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["note"]; ?>				</td>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["status"]; ?>				</td>
				<?
				if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
				{
						if($contentsAcc[$i]["type"] == "WITHDRAW")
							$closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
						elseif($contentsAcc[$i]["type"] == "DEPOSIT")
							$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
						$_SESSION["closingBalance"] = $closingBalance;

					
				?>
					<td align="center"><? echo(number_format($closingBalance,2,'.',',')); ?></td>
 			    <? } ?>
			</tr>
					
					
					
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
					 <td colspan="10">&nbsp;</td>
				</tr>
				<? if(defined("CONFIG_DISPLAY_TOTAL_RUNNING_AnD_STATEMENT") && CONFIG_DISPLAY_TOTAL_RUNNING_AnD_STATEMENT=="1"){ ?>
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',',');if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></td>
					<td colspan="5">&nbsp;</td>
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					echo number_format($_SESSION["grandTotal"],2,'.',',');if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;}
					?>					  </td>
				  	<td colspan="8">&nbsp;</td>
                </tr>
				<? }?>
            <tr bgcolor="#FFFFFF">
              <td colspan="10">&nbsp;</td>
            </tr>
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>				</td>
			</tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
			<table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					<td width="93" align="left"><span class="style1">Date</span></td>
					<td width="215" align="left"><span class="style1">Modified By</span></td>
					<td width="174" align="left"><span class="style1">Money In</span></td>
					<td width="281" align="left">Money Out</td>
					<td width="120"  align="left"><span class="style1">Transaction</span></td>
					<td width="96" align="left"><span class="style1">Description</span></td>
					<td align="left" ><span class="style1">Status</span></td>
				</tr>	
				<tr bgcolor="#FFFFFF">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="left">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>		   </td>
		  </tr>
			<?
			}

			/*	@4345
				Modified and Added CONFIG_ACCOUNT_MANAGER_DEPOSIT to display for Account Manager(Admin Staff/Basic User)

			*/
if($agentType == "admin" || ( CONFIG_ADMIN_MANAGER_DEPOSIT == "1" && $agentType == "Admin Manager") || ( CONFIG_ACCOUNT_MANAGER_DEPOSIT == "1" && $agentType == "Admin"))
			{
				if($slctID!=''){
				$agentID2actAs = selectFrom("select settlementCurrencies from ".TBL_ADMIN_USERS." where userID='".$_SESSION["agentID2"]."'");
				if($agentID2actAs["settlementCurrencies"]!=""){
					$actAsArr = explode("|",$agentID2actAs["settlementCurrencies"]);
					$actAsArrAgent 		 = $actAsArr[0];
					$actAsArrDistributor = $actAsArr[1];
					if($_REQUEST["currency"]==$actAsArrAgent)
						$actAsString = "Agent";
					else if($_REQUEST["currency"]==$actAsArrDistributor)					
						$actAsString = "Distributor";
					
				}
			?>
			
			<tr id="depostiWithdrawRow">
		  		<td height="15" bgcolor="#c0c0c0">
		  				<span class="child">
		  						<a HREF="add_Agent_Dist_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>&currencySending=<?=$_REQUEST["currency"]?>&actAsString=<?=$actAsString?>" target="mainFrame" class="tblItem style4">
		  								<font color="#990000">Deposit/Withdraw Money</font>		  						</a>		  				</span>		  		</td>
	  	</tr>
	<? } 
}
	?>
       </table>
	   
	<?	
	 }
	 else
	 {
		$_SESSION["grandTotal"]="";
		$_SESSION["CurrentTotal"]="";
	
	
		$_SESSION["fMonth"]="";
		$_SESSION["fDay"]="";
		$_SESSION["fYear"]="";
		
		$_SESSION["tMonth"]="";
		$_SESSION["tDay"]="";
		$_SESSION["tYear"]="";
	 }///End of If Search
	 ?>	 </td>
	</tr>
	<input type="hidden" id="fromDate" name="fromDate" value="<?=$fromDate?>" >
	<input type="hidden" id="toDate" name="toDate" value="<?=$toDate?>" >
	<input type="hidden" id="allCountRows" name="allCountRows" value="<?=$allCount?>" >

	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center" id="printDiv">
<br/><br/>
			Select Format <font color="#FF0000">*</font> : 
				<select name="exportFormat" id="exportFormat">
					<option value="">Select an Option</option>				
					<option value="xls">EXCEL</option>
					<option value="csv">CSV</option>
					<option value="html">HTML</option>
				</select>
			<input name="btnExport" id="btnExport" type="button"  value="Export" onClick="exportAgentDistAccount();">
             		<script language="JavaScript">
         	    		SelectOption(document.forms[0].exportFormat, "<?=$_REQUEST["exportFormat"]; ?>");
             		</script>
<br/><br/>
					<input type="button" name="Submit2" value="Print This Report" onClick="hideDivs();print()">
				</div>			</td>
		  </tr>
		  <? }?>
	</form>	</td>
	</tr>
</table>
</body>
</html>