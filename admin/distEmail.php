<?
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

				$fromName = SUPPORT_NAME;
				$fromEmail = SUPPORT_EMAIL;
				// getting Admin Email address
				$adminsContents = selectFrom("select name, email from " . TBL_ADMIN_USERS . " where userID= '".$userID."'");
				$adminsName = $adminsContents["name"];
				$adminsEmail = $adminsContents["email"];
				$distInfo = selectFrom("select username, agentCompany from " . TBL_ADMIN_USERS . " where userID = '".$contentTrans["benAgentID"]."'");
				$distName = $distInfo["agentCompany"]."[".$distInfo["username"]."]";
				$subject2 = "Define new distributor's reference number range";
		
if($rangeName == "")		
	$rangeName = "Reference Number";
/***********************************************************/
$message2 = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td colspan='2'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p><strong>Dear</strong>  $adminsName  </p></td>
  </tr>
  <tr>
    <td width='205'>&nbsp;</td>
    <td width='295'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan='2'><p>The ".$rangeName." range for the distributor $distName is about to 
    									expire so please define a new distributor's referenece number range. 
    									Otherwise you won't be able to authorize transactions for this distributor.</p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Thank You</td>
    <td>&nbsp;</td>
  </tr>
 <tr>
    <td>$fromName</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
  </tr>
</table>
";


sendMail($adminsEmail, $subject2, $message2, $fromName, $fromEmail);

?>