<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$transID = (int) $_GET["transID"];
//include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
global $isCorrespondent;
global $adminType;

$agentType = getAgentType();

if($agentType == "TELLER")
{
	$userID = $_SESSION["loggedUserData"]["cp_id"];
	$loginName = $_SESSION["loggedUserData"]["loginName"];
}
else{
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$isCorrespondent  = $_SESSION["loggedUserData"]["isCorrespondent"];
	$adminType  = $_SESSION["loggedUserData"]["adminType"];
		$loginName = $_SESSION["loggedUserData"]["username"];
}
$get=$_GET["act"];
$today = date("Y-m-d");

if($_POST["Submit"] != "")
{
	if($_POST["Submit"] == "Submit")
	{
				if( $_POST["changeStatusTo"]  == "Failed")////Deposit Agent		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Failed')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Failed','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						// insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Failed')");
				}
				if( $_POST["changeStatusTo"]  == "Suspicious")
				{
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Suspicious', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', suspeciousDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				}
				if( $_POST["changeStatusTo"]  == "Picked up")
				{
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						if($checkQues["answer"] != $_POST["answer"])
						{
							$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
							
							}
					}else{
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
						}
					
				}
				if( $_POST["changeStatusTo"]  == "Credited")
				{
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						if($checkQues["answer"] != $_POST["answer"])
						{
							$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
							
							}
					}else{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					}
				}
				if( $_POST["changeStatusTo"]  == "Sent By Courier")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Delivered")///Withdraw Bank		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Out for Delivery")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryOutDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Rejected")///Deposit Agent//Cancelled		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, transAmount, customerID, createdBy, refNumberIM, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Rejected')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Rejected','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Rejected')");
					
						}
				if( $_POST["changeStatusTo"]  == "Cancelled")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Cancelled')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Cancelled')");
				}
				include ("email.php");
		
	}
	
	redirect("samba-trans-IDA-search.php?act=$get&message=$mess");
}
if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
}
else
{
	redirect("samba-trans-IDA-search.php?act=$get");
}
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../styles/admin.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  	<tr>
		<td>
				<? 
				echo("[".$_GET["quer"]."]");
					if($_GET["back"]=="release-trans")
					{?>
					<a class="style2" href="release-trans.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
					<? }elseif($_GET["back"]=="cancel_Transactions"){?>
				 <a class="style2" href="cancel_Transactions.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
				 	<? } ?>
		</td>
	</tr>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
         
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#000000">
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#D9D9FF" ><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              	 <td width="100%" align="center" colspan="3">
            	<strong><?=$company?><br>
            Pick-Up Reciept </strong>
            </td>
              </tr>
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Date</font></td>
              	<td width="200">
              		<? echo $today; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Transaction No</font></td>
              	<td width="200">
              		<? echo $contentTrans["refNumberIM"]; ?>
              	</td>
            </tr>
            <tr bgcolor="#ffffff">
              	  <td width="100" align="left">&nbsp;</td>
              	<td width="250">
              		&nbsp;
              	</td>
              	 <td width="150" align="left"><font color="#005b90"><? echo $manualCode;?></font></td>
              	<td width="200">
              		<? echo($contentTrans["refNumber"]); ?>
              	</td>           	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
              	
            </tr>
            <? 
			if($createdBy != 'CUSTOMER')
			{
			/////Customer Details//////
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			
			$custId = $customerContent["customerID"];
			$fullNameCust = $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"];
			$addressCust =  $customerContent["Address"]." ".$customerContent["Address1"];
			$zipCust = $customerContent["Zip"];
			$phoneCust = $customerContent["Phone"];
			
			///////Beneficiary Details/////////
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, IDNumber  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			
			$benID = $benificiaryContent["benID"];
			$fullNameBen = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
			$phoneBen = $benificiaryContent["Phone"];
			$destCodeBen = $benificiaryContent["City"];
			$destCityBen = $benificiaryContent["City"];
			$idTypeBen = $benificiaryContent["IDType"];
			$idNoBen = $benificiaryContent["IDNumber"];
			
			}elseif($createdBy == 'CUSTOMER')
			{
			/////Customer Details//////
			$queryCust = "select c_id, c_name, Title, FirstName, LastName, MiddleName, c_address, c_address2, c_city, c_state, c_zip, c_country, c_phone, c_email  from ".TBL_CM_CUSTOMER." where c_id ='" . $contentTrans[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			
			$custId = $customerContent["c_id"];
			$fullNameCust = $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"];
			$addressCust =  $customerContent["c_address"]." ".$customerContent["c_address2"];
			$zipCust = $customerContent["c_zip"];
			$phoneCust = $customerContent["c_phone"];
			
			///////Beneficiary Details/////////
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, NICNumber from  ".TBL_CM_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			
			$benID = $benificiaryContent["benID"];
			$fullNameBen = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
			$phoneBen = $benificiaryContent["Phone"];
			$destCodeBen = $benificiaryContent["City"];
			$destCityBen = $benificiaryContent["City"];
			$idTypeBen = $benificiaryContent["IDType"];
			$idNoBen = $benificiaryContent["NICNumber"];
			
			}
			if($custId != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
              	<td width="200" colspan="3">
              		<? echo $fullNameCust; ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="200" colspan="3">
              		<? echo $addressCust; ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Postal Code / Zip</font></td>
              	<td width="200">
              		<? echo $zipCust; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200">
              		<? echo $phoneCust; ?>
              	</td>
            </tr>
            <?
        }
            ?>
            
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
             </tr>
              <? 
		
			
			if($benID != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Beneficiary Name</font></td>
              	<td width="200">
              		<? echo $fullNameBen; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200">
              		<? echo $phoneBen; ?>
              	</td>
            </tr>
             
          
             
               <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Remittance Details</strong></font></td>
              	
            </tr>
            
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Destination Code</font></td>
              	<td width="200">
              		<? echo $destCodeBen; ?> Office
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Destination</font></td>
              	<td width="200">
              		<? echo $destCityBen; ?> Office
              	</td>
            </tr> 
            
               <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Transfer Currency</font></td>
              	<td width="200">
              		<? echo $contentTrans["currencyFrom"]?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Amt Transfer</font></td>
              	<td width="200">
              		<? echo($contentTrans["transAmount"]);?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Paid In</font></td>
              	<td width="200">
              		<? echo $contentTrans["currencyTo"]?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Exchange Rate</font></td>
              	<td width="200">
              		<? echo $contentTrans["exchangeRate"]?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Commission</font></td>
              	<td width="200">
              		<? echo $contentTrans["IMFee"]?> 
              	</td>
              	 <td width="150" align="left"><font color="#005b90">In <?=$contentTrans["currencyTo"]?></font></td>
              	<td width="200">
              		<? echo($contentTrans["localAmount"]); ?> 
              	</td>
            </tr> 
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong></strong></font></td>
              	
            </tr>        
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">ID Description</font></td>
              	<td width="200">
              		<? echo $idTypeBen; ?>&nbsp;
              	</td>
              	 <td width="150" align="left"><font color="#005b90">ID Serial no</font></td>
              	<td width="200">
              		<? echo $idNoBen; ?>&nbsp;
              	</td>
            </tr> 
            <?
        }            
            ?>
           
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Cashier Number</font></td>
              	<td width="200">&nbsp;<? echo($loginName); ?>
              		
              	</td>
              	 <td width="150" align="left"><font color="#005b90"><i>Signature</i></font></td>
              	<td width="200">&nbsp;
              		
              	</td>
            </tr>             
             
            </table>
          
            <br>
            </fieldset></td>
           
          </tr>         
         <? /* ?> 
           <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#000000">
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#D9D9FF" ><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              	 <td width="100%" align="center" colspan="3">
            	<strong>Express Money House<br>
            Pick-Up Reciept </strong>
            </td>
              </tr>
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Date</font></td>
              	<td width="200">
              		<? echo $today; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Transaction No</font></td>
              	<td width="200">
              		<? echo $contentTrans["refNumberIM"]; ?>
              	</td>
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
              	
            </tr>
            <? 
		
			if($custId != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
              	<td width="200" colspan="3">
              		<? echo $fullNameCust; ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="200" colspan="3">
              		<? echo $addressCust; ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Postal Code / Zip</font></td>
              	<td width="200">
              		<? echo $zipCust; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200">
              		<? echo $phoneCust; ?>
              	</td>
            </tr>
            <?
        }
            ?>
            
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
             </tr>
              <? 
		
			
			if($benID != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Beneficiary Name</font></td>
              	<td width="200">
              		<? echo $fullNameBen; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200">
              		<? echo $phoneBen; ?>
              	</td>
            </tr>
             
          
             
               <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Remittance Details</strong></font></td>
              	
            </tr>
            
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Destination Code</font></td>
              	<td width="200">
              		<? echo $destCodeBen; ?> Office
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Destination</font></td>
              	<td width="200">
              		<? echo $destCityBen; ?> Office
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
            	 <td width="150" align="left"><font color="#005b90">Transfer Currency</font></td>
            	<td width="200">
            		<? echo $contentTrans["currencyFrom"]?>
            	</td>
            	 <td width="150" align="left"><font color="#005b90">Amt Transfer</font></td>
            	<td width="200">
            		<? echo $contentTrans["transAmount"]?> 
            	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Paid In</font></td>
              	<td width="200">
              		<? echo $contentTrans["currencyTo"]?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Exchange Rate</font></td>
              	<td width="200">
              		<? echo $contentTrans["exchangeRate"]?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Commission</font></td>
              	<td width="200">
              		<? echo $contentTrans["IMFee"]?> 
              	</td>
              	 <td width="150" align="left"><font color="#005b90">In Cedis</font></td>
              	<td width="200">
              		<? echo $contentTrans["localAmount"]?> 
              	</td>
            </tr> 
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong></strong></font></td>
              	
            </tr>        
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">ID Description</font></td>
              	<td width="200">
              		<? echo $idTypeBen; ?>&nbsp;
              	</td>
              	 <td width="150" align="left"><font color="#005b90">ID Serial no</font></td>
              	<td width="200">
              		<? echo $idNoBen; ?>&nbsp;
              	</td>
            </tr> 
            <?
        }            
            ?>
           
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Cashier Name</font></td>
              	<td width="200">&nbsp;
              		
              	</td>
              	 <td width="150" align="left"><font color="#005b90"><i>Signature</i></font></td>
              	<td width="200">&nbsp;
              		
              	</td>
            </tr>             
             
            </table>
          
            <br>
            </fieldset></td>
           
          </tr>         
          <? */?>
         
              <tr>
               
				  <td align="center">
				         
                            <input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
                        
				  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>