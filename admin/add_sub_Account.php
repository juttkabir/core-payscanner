<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
include ("javaScript.php");
///////////////////////History is maintained via method named 'activities'

$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$_SESSION["agentID2"];
if ($_GET["agent_Account"] !="" ) {
	$_SESSION["agentID2"] = $_GET["agent_Account"];
}

if($_GET["currencySending"]!="")
	$_SESSION["currencySending"]=$_GET["currencySending"];
	
$condition = true;
	
if ($_POST["Submit"] == "Save") {
	if (CONFIG_BACKDATING_PAYMENTS == "1") {
		if ($_POST["dDate"] != "") {
			$dDate = explode("/", $_POST["dDate"]);
			$today = $dDate[2] . "-" . $dDate[1] . "-" . $dDate[0];
			if ($today > date("Y-m-d")) {
				$condition = false;	
			}
		} else {
			$today = date("Y-m-d");
		}
	} else {
		$today = date("Y-m-d");
	}
}
?>
<html>
<head>
<title>Add into Account</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>

<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr>
    <td class="topbar">Sub A&D Account Statement</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="add_sub_Account.php">
    <tr> 
      <td width="39%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Amount &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="money"></td>
    </tr>
     <tr> 
      <td><div align="right"><font color="#005b90">Note &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="note"></td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Type &nbsp;&nbsp;</font></div></td>
      <td><select name="type">
      			<option value="Deposit">Deposit</option>
      			<option value="Withdraw">Withdraw</option>
      		</select>
      	</td>
    </tr>
    <?	if (CONFIG_BACKDATING_PAYMENTS == "1") {  ?>
    <tr> 
      <td><div align="right"><font color="#005b90">Date &nbsp;&nbsp;</font></div></td>
      <td>
      	<input type="text" name="dDate" readonly>&nbsp;
      	<a href="javascript:show_calendar('form1.dDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
      </td>
    </tr>
    <?	}  ?>
    
     <? if(CONFIG_ADD_CURRENCY_DROPDOWN == "1"){?> 
      <tr> 
      <td><div align="right"><font color="#005b90">Sending Currency &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="currencyFrom" value="<?=$_SESSION["currencySending"]?>" readonly></td>
    </tr>
     <? } ?>        
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Save"></td>
    </tr>
	 <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if ($_POST["Submit"] == "Save") {
	if ($condition) {	
		if ($agentType == "COLLECTOR") {
			$status	= "Provisional";
		} else {
			$status	= "Verified";	
		}
		if ($_POST["type"] == "Deposit") {
			$type = "DEPOSIT";
			$desc = "Manually Deposited";
		} else if ($_POST["type"] == "Withdraw") {
			$type = "WITHDRAW";
			$desc = "Manually Withdrawn";
		}
		
		$res = updateSubAgentAccount($_SESSION["agentID2"], $_POST["money"], '', $type, $desc, 'Agent',$_SESSION["currencySending"],$_POST["note"],$today);
		
if($res)
	{?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>">You 
        have successfully <? echo ($_POST["type"] == "Deposit" ? "deposited" : "withdrawn"); ?> 
        <? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
        </font></div></td>
  	  <td bgcolor="#CCCCCC" align="left">&nbsp;<!--font color="#990000"> &nbsp; Now Balance 
	  is <? $balanceCal=$amountInHand;//agentBalance($_SESSION["agentID2"]); 
	  echo number_format($balanceCal,2,'.',',');?></font--> </td>
  </tr>
  <? }
} else {
	?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo CAUTION_COLOR ?>">
      	Enter Date should not be greater than today Date.
      	</font></div></td>
  	  <td bgcolor="#CCCCCC" align="left">&nbsp;</td>
  </tr>
	<?
}	
} 
?>
	<tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr> 
      
    <td height="19"><a href="sub_Account.php?agentID=<?=$_SESSION["agentID2"]?>&search=search&currencySending=<?=$_SESSION["currencySending"]?>" class="style2" ><font color="#000000">Go 
      Back to Sub A&D Account Summary</font></a></td>
      
    <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>