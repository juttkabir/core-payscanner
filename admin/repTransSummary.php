<?php
session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//include ("javaScript.php");
if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 2;
}
//debug($_REQUEST);

$agentType = getAgentType();


//print_r($agentType);
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];
$role=$_SESSION["loggedUserData"]["role"];	



$where_sql='';



include("connectOtherDataBase.php");
/////////////Defining action array against #9906: Bankalflah: Enhancement on search filter on View Transaction page
if(CONFIG_ACTIONDATE_COL=="1")
$StatusArr=array( 
				
				"Pending" 					=> "transDate",
				"Processing" 				=> "transDate",
				"Recalled"					=> "recalledDate",
				"Rejected"					=> "rejectDate",
				"Suspended"					=> "suspendDate",
				"amended"					=> "backToPendingDate",
				"Hold"						=> "holdDate",
				"Cancelled"					=> "cancelDate",
				"Cancelled - Returned"		=> "returnDate",
				"AwaitingCancellation"		=> "cancelDate",
				"Authorize"					=> "authoriseDate",
				"Failed"					=> "failedDate",
				"Delivered"					=> "deliveryDate",
				"Out for Delivery"			=> "deliveryOutDate",
				"Picked up"					=> "deliveryDate",
				"Credited"					=> "deliveryDate"



);

if(defined("CONFIG_RTS_TRANS") && CONFIG_RTS_TRANS !="0"){
	//echo "CONFIG WORKING";
}
 
if(defined("CONFIG_DISPLAY_DATE_FORMAT") && CONFIG_DISPLAY_DATE_FORMAT !="0" && is_numeric(CONFIG_DISPLAY_DATE_FORMAT))
{
	$dfFlag = CONFIG_DISPLAY_DATE_FORMAT;
} else {
	
	$dfFlag="2";
}
if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
     $entity_country =getLoggedUserEntity($userID);
/*debug($userID);	 
debug($entity_country);*/
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";
$moneyPaid   = "";
$fMonth      = "";
$fDay        = "";
$fYear       = "";
$tMonth		 = "";
$tDay		 = "";
$tYear   	 = "";
$fhr         = ""; 
$fmin        = "";
$fsec        = "";
$thr         = "";
$tmin        = "";
$tsec        = "";

if($_POST["transType"]!="")
	$transType = $_POST["transType"];
elseif($_GET["transType"]!="") 
	$transType=$_GET["transType"];
		
if($_POST["transStatus"]!="")
	$transStatus = $_POST["transStatus"];
elseif($_GET["transStatus"]!="")
	$transStatus = $_GET["transStatus"];

if($_POST["Submit"]!="")
	$Submit = $_POST["Submit"];
elseif($_GET["Submit"]!="") 
	$Submit=$_GET["Submit"];
	
if($_POST["transID"]!="")
	$transID = trim($_POST["transID"]);
elseif($_GET["transID"]!="")
	$transID = trim($_GET["transID"]);

//echo $transID;

if($_POST["searchBy"]!="")
	$by = $_POST["searchBy"];
elseif($_GET["searchBy"]!="") 
	$by = $_GET["searchBy"];
	
if ($_POST["moneyPaid"] != "") {
	$moneyPaid = $_POST["moneyPaid"];
} else if ($_GET["moneyPaid"] != "") {
	$moneyPaid = $_GET["moneyPaid"];
}


if ($_POST["nameType"] != "") {
	$nameType = $_POST["nameType"];
} else if ($_GET["nT"] != "") {
	$nameType = $_GET["nT"];
}


if ($_POST["agentNameType"] != "") {
	$agentNameType = $_POST["agentNameType"];
} else if ($_GET["agentNameType"] != "") {
	$agentNameType = $_GET["agentNameType"];
}



if ($_POST["referenceNumberType"] != "") {
	$referenceNumberType = $_POST["referenceNumberType"];
} else if ($_GET["referenceNumberType"] != "") {
	$referenceNumberType = $_GET["referenceNumberType"];
}
/*
if($_POST["fMonth"] != ""){
	$fMonth=$_POST["fMonth"];
}elseif($_GET["fMonth"] != ""){
	$fMonth=$_GET["fMonth"];
}else{
	$fMonth = date("m");
	
	}
if($_POST["fDay"] != ""){
	$fDay=$_POST["fDay"];
}elseif($_GET["fDay"] != ""){
	$fDay=$_GET["fDay"];
}else{
	$fDay = date("d");
	}
	
if($_POST["fYear"] != ""){
	$fYear=$_POST["fYear"];
}elseif($_GET["fYear"] != ""){
	$fYear=$_GET["fYear"];
}else{
	$fYear = date("Y");
	}
		
if($_POST["tMonth"] != ""){
	$tMonth=$_POST["tMonth"];
}elseif($_GET["tMonth"] != ""){
	$tMonth=$_GET["tMonth"];
}else{
	$tMonth = date("m");
	}

if($_POST["tDay"] != ""){
	$tDay=$_POST["tDay"];
}elseif($_GET["tDay"] != ""){
	$tDay=$_GET["tDay"];
}else{
	$tDay = date("d");
	}
	
if($_POST["tYear"] != ""){
	$tYear=$_POST["tYear"];
}elseif($_GET["tYear"] != ""){
	$tYear=$_GET["tYear"];
}else{
	$tYear = date("Y");
	
	}

if($_POST["fhr"] != ""){
	$fhr=$_POST["fhr"];
}elseif($_GET["fhr"] != ""){
	$fhr=$_GET["fhr"];
}else{
	$fhr = "00";
	
	}
if($_POST["fmin"] != ""){
	$fmin=$_POST["fmin"];
}elseif($_GET["fmin"] != ""){
	$fmin=$_GET["fmin"];
}else{
	$fmin = "00";
	}
	
if($_POST["fsec"] != ""){
	$fsec=$_POST["fsec"];
}elseif($_GET["fsec"] != ""){
	$fsec=$_GET["fsec"];
}else{
	$fsec = "00";
	}

if($_POST["thr"] != ""){
	$thr=$_POST["thr"];
}elseif($_GET["thr"] != ""){
	$thr=$_GET["thr"];
}else{
	$thr = "23";
	}
	
if($_POST["tmin"] != ""){
	$tmin=$_POST["tmin"];
}elseif($_GET["tmin"] != ""){
	$tmin=$_GET["tmin"];
}else{
	$tmin = "59";
	}
	
if($_POST["tsec"] != ""){
	$tsec=$_POST["tsec"];
}elseif($_GET["tsec"] != ""){
	$tsec=$_GET["tsec"];
}else{
	$tsec = "59";
	}
*//*
$strfromDate=$_POST['fromDate'];
debug($_POST['fromDate']);
sscanf($strfromDate, '%d-%d-%d', $d, $m, $Y);
$fromDate== date("Y-m-d", strtotime("$Y-$m-$d"));
*/

if(!empty($_GET['fDate'])){
		$_POST['fromDate']	=$_GET['fDate'];

}
if(!empty($_GET['tDate'])){
		$_POST['toDate']=$_GET['tDate'];

}
if(!empty($_POST['fromDate']))
{
	if(empty($_GET['fDate'])){
	$strfromDate=explode('/',$_POST['fromDate']);
	$fromDate = $strfromDate[2]. "-" . $strfromDate[1] . "-" . $strfromDate[0];
	}
	else
		$fromDate=$_GET['fDate'];
	
}	
else{
	$fromDate=date("Y-m-d");
}

if(!empty($_POST['toDate'])){

	if(empty($_GET['tDate'])){
		$strToDate=explode('/',$_POST['toDate']);
		$toDate = $strToDate[2]. "-" . $strToDate[1] . "-" . $strToDate[0];
	}
	else
		$toDate=$_GET['tDate'];
	}
else{
	$toDate=date("Y-m-d");
}	
	$fromTime =  "00:00:00";
	 $toTime = "23:59:59";

$chequeAmountField = "";
if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
{
	$chequeAmountField = "chequeAmount,";
}	

	
if(CONFIG_ACTIONDATE_COL=="1"){
	if(empty($transStatus))
		$ActionStatus="t.".$StatusArr["Pending"]." as actionDate,";
	else
		$ActionStatus="t.".$StatusArr[$transStatus]." as actionDate,";
}

//$query = "select t.benID,transID,transDate,totalAmount,custAgentID," . $chequeAmountField . " t.benAgentID,transType,transAmount,localAmount,refNumber,refNumberIM,transStatus,t.customerID,t.collectionPointID,createdBy,addedBy, clientRef,t.custDocumentProvided,Account_lmt_exceeded from ". TBL_TRANSACTIONS . " as t where 1";
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1";


				
				
				
				
								
				/*$query = "select t.benID,t.transID,t.transDate,t.refNumber,t.refNumberIM,t.custAgentID, t.benAgentID,t.totalAmount," . $chequeAmountField . " t.transType,t.customerID, t.currencyTo,t.toCountry,t.transAmount,t.IMFee, t.localAmount, t.transStatus, t.fromCountry, t.exchangeRate, t.currencyFrom, t.currencyTo, t.AgentComm, t.customerID, t.collectionPointID, t.createdBy, t.addedBy, t.custDocumentProvided ,".$ActionStatus.", tbd.bankName,tbd.accNo,tbd.branchCode from ". TBL_TRANSACTIONS . " as t, ".TBL_BANK_DETAILS." as tbd where 1 and t.transID=tbd.transID ";
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1 ";*/
				$query = "select t.benID,t.transID,t.transDate,t.sss,t.refNumber,t.refNumberIM,t.custAgentID, t.benAgentID,t.totalAmount," . $chequeAmountField . " t.transType,t.customerID, t.currencyTo,t.toCountry,t.transAmount,t.IMFee, t.trans_source,t.localAmount, t.transStatus, t.fromCountry, t.exchangeRate, t.currencyFrom, t.currencyTo, t.AgentComm, t.customerID, t.collectionPointID, t.createdBy, t.addedBy, t.custDocumentProvided ,".$ActionStatus." tbd.bankName,tbd.accNo,tbd.branchCode, c.parentID from (". TBL_TRANSACTIONS . " as t LEFT JOIN ".TBL_BANK_DETAILS." as tbd  ON  t.transID=tbd.transID) join ".TBL_CUSTOMER." as c on t.customerID=c.customerID where 1  ";
				
				$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t join ".TBL_CUSTOMER." as c on t.customerID=c.customerID  where 1 ";
				$queryGroup = "select count(vwTrans.currencyTo) as totalGroup,sum(vwTrans.totalAmount) as grandAmount, sum(vwTrans.transAmount) as totalTransAmount, sum(vwTrans.localAmount) as totalLocalAmount, sum(vwTrans.IMFee) as IMFee, vwTrans.currencyTo,vwTrans.currencyFrom from ". TBL_TRANSACTIONS . " as vwTrans join ".TBL_CUSTOMER." as c on vwTrans.customerID=c.customerID  where 1" ; 
				
		 
//if(defined("CONFIG_RTS_TRANS") && CONFIG_RTS_TRANS !="0"){
	//$query.=" And t.transStatus!='Cancelled'";
	//$queryCnt.=" And t.transStatus!='Cancelled'";
	$queryGroupSendingCurrencyCancelled=$queryGroup;
	$queryGroup.=" And vwTrans.transStatus!='Cancelled'";
	$queryGroupSendingCurrencyCancelled.=" And vwTrans.transStatus='Cancelled'";

//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType");

if($Submit == "Search")
{


	

	// if(!empty($_POST['agentName']) && $_POST['agentName']!='all'){
		
			// $query .=" and t.custAgentID='".$_POST['agentName']."'";
			// $queryCnt .=" and t.custAgentID='".$_POST['agentName']."'";
			// $queryGroup .=" and vwTrans.custAgentID='".$_POST['agentName']."'";
			// $queryGroupSendingCurrencyCancelled .=" and vwTrans.custAgentID='".$_POST['agentName']."'";
	
	// }
	if(!empty($_POST['bankID'])  && $_POST['bankID']!='all' ){
		
			$query .=" and t.benAgentID='".$_POST['bankID']."'";
			$queryCnt .=" and t.benAgentID='".$_POST['bankID']."'"; 
			$queryGroup .=" and vwTrans.benAgentID='".$_POST['bankID']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.benAgentID='".$_POST['bankID']."'";
	
	}
	
	if(!empty($_POST['paymentMode'])){
		
			$query .=" and t.moneyPaid='".$_POST['paymentMode']."'";
			$queryCnt .=" and t.moneyPaid='".$_POST['paymentMode']."'";
			$queryGroup .=" and vwTrans.moneyPaid='".$_POST['paymentMode']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.moneyPaid='".$_POST['paymentMode']."'";
	
	}
	if(!empty($_POST['transactionType'])){
		
			$query .=" and t.transType='".$_POST['transactionType']."'";
			$queryCnt .=" and t.transType='".$_POST['transactionType']."'";
			$queryGroup .=" and vwTrans.transType='".$_POST['transactionType']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.transType='".$_POST['transactionType']."'";
		
	}
	if(!empty($_POST['transactionStatus'])){
		
			$query .=" and t.transStatus='".$_POST['transactionStatus']."'";
			$queryCnt  .=" and t.transStatus='".$_POST['transactionStatus']."'";
			$queryGroup  .=" and vwTrans.transStatus='".$_POST['transactionStatus']."'";
			$queryGroupSendingCurrencyCancelled  .=" and vwTrans.transStatus='".$_POST['transactionStatus']."'";
		
	}
	if(!empty($_POST['amount']) && !empty($_POST['amountCriteria']) && !empty($_POST['amountType']) ){
		$amountType = $_POST["amountType"];
		$amountCriteria = $_POST["amountCriteria"];
		$amount = $_POST["amount"];
		
 	 			if($amountType == "sent") {
 	 				$type = "transAmount";
 	 			}elseif($amountType == "foriegn"){
 	 				$type = "localAmount";
 	 			}elseif($amountType == "afterCommission"){
 	 				$type = "totalAmount";
 	 			}
		if($amountCriteria == "BETWEEN" || $amountCriteria == "NOT BETWEEN"){
					
 					$amountArray = split('-',$amount);
			$query .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ";
			if(!empty($amountArray[1])){
				$query .=$amountArray[1].")" ;
			}
			ELSE
				$query .=$amountArray[0].")" ;
 	$queryCurrency .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND " ;
			if(!empty($amountArray[1])){
				$queryCurrency .=$amountArray[1].")" ;
			}
			ELSE
				$queryCurrency .=$amountArray[0].")" ;
	
 	 $queryGroup .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND " ;
			if(!empty($amountArray[1])){
				$queryGroup .=$amountArray[1].")" ;
			}
			ELSE
				$queryGroup .=$amountArray[0].")" ;
			
	$queryCnt .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ";
			if(!empty($amountArray[1])){
				$queryCnt .=$amountArray[1].")" ;
			}
			ELSE
				$queryCnt .=$amountArray[0].")" ;
				
			$queryGroupSendingCurrencyCancelled .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ";
			if(!empty($amountArray[1])){
				$queryGroupSendingCurrencyCancelled .=$amountArray[1].")" ;
			}
			ELSE
				$queryGroupSendingCurrencyCancelled .=$amountArray[0].")" ;	
 	      }
		  else if($amountCriteria!=''){
			
 	      	$query .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	        $queryCurrency .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	        $queryGroup .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
			$queryCnt .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
			$queryGroupSendingCurrencyCancelled .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	      }
				
 				
			
	
	
	}
	
	if(!empty($_POST['supAgentID']) && $_POST['supAgentID']!='all'){
			$query .=" and c.parentID='".$_POST['supAgentID']."'";
			$queryCnt .=" and c.parentID='".$_POST['supAgentID']."'";
			$queryGroup .=" and c.parentID='".$_POST['supAgentID']."'";
			$queryGroupSendingCurrencyCancelled .=" and c.parentID='".$_POST['supAgentID']."'";
	
	}
	if(!empty($_POST['Tocurrency'])){
			$query .=" and t.currencyTo='".$_POST['Tocurrency']."'";
			$queryCnt .=" and t.currencyTo='".$_POST['Tocurrency']."'";
			$queryGroup .=" and vwTrans.currencyTo='".$_POST['Tocurrency']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.currencyTo='".$_POST['Tocurrency']."'";
	
	}
	if(!empty($_POST['Tocountry'])){
			$query .=" and t.toCountry='".$_POST['Tocountry']."'";
			$queryCnt .=" and t.toCountry='".$_POST['Tocountry']."'";
			$queryGroup .=" and vwTrans.toCountry='".$_POST['Tocountry']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.toCountry='".$_POST['Tocountry']."'";
	
	}
	
	// if(!empty($_POST['cashier'])){
			// $query .=" and t.addedBy='".$_POST['cashier']."'";
			// $queryCnt .=" and t.addedBy='".$_POST['cashier']."'";
			// $queryGroup .=" and vwTrans.addedBy='".$_POST['cashier']."'";
			// $queryGroupSendingCurrencyCancelled .=" and vwTrans.addedBy='".$_POST['cashier']."'";
	
	// }
	
	if(!empty($_POST['transSource'])){
			$query .=" and t.trans_source='".$_POST['transSource']."'";
			$queryCnt .=" and t.trans_source='".$_POST['transSource']."'";
			$queryGroup .=" and vwTrans.trans_source='".$_POST['transSource']."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.trans_source='".$_POST['transSource']."'";
	
	}
	//debug($query);
	
	
}

if($agentType=='SUPA' ||  $agentType=='SUPI' )
{


			$query .=" and t.custAgentID='".$agentID."'";
			$queryCnt .=" and t.custAgentID='".$agentID."'";
			$queryGroup .=" and vwTrans.custAgentID='".$agentID."'";
			$queryGroupSendingCurrencyCancelled .=" and vwTrans.custAgentID='".$agentID."'";
}

//debug($agentType);
//debug($role);
//debug($transType);
//debug($_POST["searchBy"]);
	
	
	
		
	$queryDate = "(t.transDate BETWEEN '$fromDate $fromTime' AND '$toDate $toTime')";		
	$query .=  " and $queryDate";				
	$queryCnt .=  " and $queryDate";
	$queryDate = "( vwTrans.transDate BETWEEN '$fromDate $fromTime' AND '$toDate $toTime')";
	$queryGroup .=  " and $queryDate";
	$queryGroupSendingCurrencyCancelled .=  " and $queryDate";
	//debug($query);
	//debug($queryCnt);
	$queryonline .= " and (transDate >= '$fromDate $fromTime' and transDate <= '$toDate $toTime')";	
	$queryonline.=  " and $queryDate";
	//debug($queryonline);
//debug($StatusArr[$transStatus]);
	
	 
	
	
	if($by != 0 || $by == ""){
  $query .= " order by t.transDate DESC";
  
}
$queryGroup .= " group by currencyFrom,currencyTo order by currencyTo ASC";

// print_r( $query);
// echo '<br>';
// echo '<br>';
// echo '<br>';
// print_r($queryGroup);
$contentsTransGroup = selectMultiRecords($queryGroup);
if($by == "" && $transID != ""){
	
	$errorMessage="Select Proper filters to Search";
	
	
	}else{
		
		
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $exportQuery = $query;
 $exportQueryGroup=$queryGroup;
 $query .= " LIMIT $offset , $limit";
//debug($queryCnt);
//debug($query);
$contentsTrans = selectMultiRecords($query);
//debug(count($contentsTrans));
 $allCount = countRecords($queryCnt);
 $queryGroupEnd = " group by currencyFrom,currencyTo order by refNumberIM DESC";
 $queryGroupSendingCurrencyCancelled.=" group by currencyFrom,currencyTo order by refNumberIM DESC";
 $contentsTransCancelled = selectMultiRecords($queryGroupSendingCurrencyCancelled);
// debug($allCount);
}

		
		if($transID != "" && $by != 0)
		{	
			
			
				$onlinecustomerCount = countRecords($queryonlineCnt );
				$allCount = $allCount + $onlinecustomerCount;
				$other = $limit;
			 if($other > count($contentsTrans))
			 {
			
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
	 			 $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
//debug($fromDate);
 $fromDateReport =date("d/m/Y",strtotime($fromDate));
  $toDateReport   =date("d/m/Y",strtotime($toDate));
//debug($query);
?>
<html>
<head>
	<title>Report Transaction Summary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/datetimepicker.js"></script>
<script src="./javascript/jquery.min.js" type="text/javascript"></script>
<!-- AJAX Upload script itself doesn't have any dependencies-->
<script type="text/javascript" src="javascript/ajaxupload.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	$(document).ready(function(){
		/* Example 1 */
		$('.button').each(function(index){
			var button = $(this), interval;
			new AjaxUpload(button, {
				action: 'uploadCustomerImage-conf.php', 
				name: 'file0', // for single upload this should have 0 (zero appended)
				data: {
					customerID : $("#transID"+index).val(), // this is transaction id sent to uploadCustomerImage-conf.phg page
					referenceNo0 : $("#refNumberIM"+index).val(), // this is reference number of transaction (for single upload this should have 0 (zero appended))
					page : "repTransSummary.php",
					mode : "Add",
					userType : "T",
					action : "ajax"
				},
				onSubmit : function(file, ext){
					// change button text, when user selects file			
					button.text('Uploading');
					// If you want to allow uploading only 1 file at time,
					// you can disable upload button
					this.disable();
					
					// Uploding -> Uploading. -> Uploading...
					interval = window.setInterval(function(){
						var text = button.text();
						if (text.length <15){
							button.text(text + '.');					
						} else {
							button.text('Uploading');				
						}
					}, 200);
				},
				onComplete: function(file, response){
					button.text('Upload');
					window.clearInterval(interval);
					// enable upload button
					this.enable();
					alert(response);
					// add file to the list
					//$('<li></li>').appendTo('#example1 .files').text(file);						
				}
			});
		});
	});
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm()
{
	if(document.forms[0].DateType.value == "actionDate" && document.forms[0].transStatus.value == ""){
	alert("Please put some value of status to be searched.");
	 return false;	
	}
	if(document.forms[0].searchBy.value != "" && document.forms[0].transID.value == "")
	{
				alert("Please put some value to be searched.");
        document.forms[0].transID.focus();
        return false;	
	}	
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

<script language="JavaScript">
<!--

function nameTypeCheck(){
	
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
				document.getElementById('nameTypeRow').style.display = '';
			}else{
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			if(document.getElementById('searchBy').value == '4'){
				document.getElementById('agentTypeRow').style.display = '';
			}else{
				document.getElementById('agentTypeRow').style.display = 'none';
				}
				if(document.getElementById('searchBy').value == '0'){
				document.getElementById('referenceTypeRow').style.display = '';
			}else{
				document.getElementById('referenceTypeRow').style.display = 'none';
				}
}

-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
.classRed {color: #FF0000; font-weight: bold; }
.classOrange {color: #FFCC33; font-weight: bold; }
div.button {
	background-color:#000;
	color:#CDFFFF;
	font-size:14px;
	font-weight:bold;
	height:22px;
	padding-top:3px;
	text-align:center; 
	width:110px;
}


-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
	<script type="text/javascript" src="wz_tooltip.js"></script>
			  <script type="text/javascript" src="javaScript.js"></script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><?=__("Report Transaction Summary") ?></font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <form action="repTransSummary.php" method="post" name="Search">
    <table border="1" bordercolor="#666666"  id="searchfilterhide">
     <tr>
     <td  valign="top">
    <table border="1" bordercolor="#666666">
 	            <tr id="searchFilter" ><td  valign="top"><table border="0" cellpadding="5" bordercolor="#666666" width="100%" >
 	     
 	      <tr>
 	            <td width="100%" colspan= "4" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters
 	              </strong></span></td>
 	        </tr>
 	        <tr>
 	        <td align="center" nowrap colspan="4">
 	        From <input name="fromDate" type="text" id="fromDate"  value="<?=$fromDateReport?>" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
		
        To	<input name="toDate" type="text" id="toDate"  value="<?=$toDateReport?>" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
 	        
 	       
 	      </td></tr>
		   <tr>
 	    		<td colspan="4" align="center">
           		<font color="#FF0000" size=1>Note: If amount Criteria selected is "BETWEEN or NOT BETWEEN" then amount should be entered as 2000-5000 otherwise enter a single value as 2000 </font>
          </td>
 	    	</tr>
 	    	<tr>
 	    		<td colspan="4" align="center">Sending/Foreign Amount
 	    			<input name="amount" type="text" value="<?=$amount?>" onKeyPress="return allowNumber(event)">
		  			<select name="amountCriteria" id="amountCriteria">
            		<option value="">-Amount Criteria- </option>
            		<!--option value="=">Equal To</option-->
            		<option value="<">Less Than</option>
            		<option value=">">Greater Than</option>
            		<option value="<=">Less Than or Equal To</option>
            		<option value=">=">Greater Than or Equal To</option>
            		<option value="!=">Not Equal To</option>
            		<option value="BETWEEN">BETWEEN</option>
            		<option value="NOT BETWEEN">NOT BETWEEN</option>
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].amountCriteria, "<?=$amountCriteria; ?>");</script>	
							
						<select name="amountType" style="font-family:verdana; font-size: 11px;">
		    				<option value="">-Select Amount- </option>
	    					<option value="sent">Amount Sent</option>
	    					<option value="foriegn">Foriegn Amount</option>
	    					<option value="afterCommission">Amount After Commission</option>
        			</select>
      				<script language="JavaScript">SelectOption(document.forms[0].amountType, "<?=$amountType; ?>");</script>
           </td>
            
 	    	</tr>
 	        <?
 	        //if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
 	        /*if($condition)
 	        {*/
 	        ?>
			 <? 
			 
			 $adminStaffList		=	"'Admin','Call','COLLECTOR','Collector','Branch Manager','Admin Manager','Support','SUPI Manager','MLRO'";
			 /**
			  * #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
			  * Short Description
			  * This array is changed for retain the index after search
			  */
				
				
//work  by Mudassar Ticket #11425	
			   if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){	
					$agentStatusQuery = selectFrom("select agentStatus from ".TBL_ADMIN_USERS." where agentStatus='Active' ");
					
				$argumentsArr	=	array("userType"=>"populateAdminStaff","adminStaffList"=>$adminStaffList,"allOption"=>"Y", "selectedID"=>$_REQUEST['supAgentID'], "agentStatusQuery"=>$agentStatusQuery);	
					
					}
					
	else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
	
			$argumentsArr	=	array("userType"=>"populateAdminStaff","adminStaffList"=>$adminStaffList,"allOption"=>"Y", "selectedID"=>$_REQUEST['supAgentID']);
					}
							
	//work end by Mudassar Ticket #11425
				
				
				
				
				//debug($argumentsArr);
				if(!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))
				{
			 ?>
					 <tr>
						<td>Admin Staff</td>
						<td><?php echo gateway("CONFIG_POPULATE_ADMINSTAFF_DROPDOWN", $argumentsArr, "1");?></td>
					 </tr>
				<?php 
				}?>
				 
				 <tr>
				<?  if($agentType!='SUPA' && $agentType!='SUPI'){ ?>  	 	<td> <?=__("Agent"); ?> Name  </td> <?php } ?>
					<?php 
						/**
						 * # 7888: Premeir FX - Admin Staff and Correspoding Agents
						 * Short Description
						 * This Config Enable / Disable the Agent Staff Field and Corresponding Field and if any admin staff is loign his sub agents are shown
						 */
						if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1" && (!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))){
					?>
					<td>
						<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          				<option value="all">All</option>
 	          			</select>
					<?php 
						}
						elseif($agentType == 'Admin' || $agentType == "Admin Manager" || $agentType == "Branch Manager" ){
					?>
						<td>
							<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          					<option value="">- Select Agent-</option>
								<!--
									#9898: Premier Exchange:RTS is not working / Filter Values are not Retained
									option all has no value
									Give this option 'all' value for retain this value in pagination and inner pages
								-->
	         					<option value="all">All</option>
 	            			<?
								/**
								 * #7888: Premier Exchange
								 * Short Description
								 * @var $assocAgentsIDs is for the associated users ids
								 * @array $arrAgentsID of Associated IDs
								 * @var $intNumberAgents is the counter for associated agents
								 */
								/*$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
								if($agentType == "Branch Manager")
 	                        	{
									$agentQuery .= " and parentID = '$parentID'";                   
 	                            }
 	                            $agentQuery .= "order by agentCompany";*/
								$arrInput = array("userID" => $parentID);
								$assocAgentsIDs = linkAgentList($arrInput);
								//debug($assocAgentsIDs);
								$arrAgentsID = explode(",", $assocAgentsIDs);
								$intNumberAgents = count($arrAgentsID);
								for($x = 0; $x < $intNumberAgents; $x ++){
								
//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){							
									$agentQuery = "SELECT userID, name, username, agentStatus, agentCompany FROM ".TBL_ADMIN_USERS." WHERE agentStatus = 'Active' AND userID = ".$arrAgentsID[$x];
									
			}
									
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){								
		$agentQuery = "SELECT userID, name, username, agentCompany FROM ".TBL_ADMIN_USERS." WHERE userID = ".$arrAgentsID[$x];
						}
//work end by Mudassar Ticket #11425
									//debug($agentQuery);
									$assocAgent = selectFrom($agentQuery);
									echo "<option value='".$assocAgent['userID']."'>".$assocAgent['agentCompany']." [".$assocAgent['username']."] </option>";
								}
								
 	                	/*$agents = selectMultiRecords($agentQuery);
 	                	for ($i=0; $i < count($agents); $i++){
 	            		?>
 	            			<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            		<?
 	                	}*/
 	            		?>
 	          			</select>
					<?php 
						}
						else
						{
						
					
						
							/**
							 * # 7888: Premier Exchange
							 * Short Description
							 * This @condition is for super Admin without Config 'CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS'
							 */
					?>
									<?  if($agentType!='SUPA' && $agentType!='SUPI'){ ?>  <td>
		
							<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          					<option value="">- Select Agent-</option>
								<option value="all">All</option>
								
					<?php 
					
					
					//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){							
									
			$agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' ";												
			}
									
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){								
		$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
						}
//work end by Mudassar Ticket #11425
					
					
								
								//debug($agentQuery);
								if($agentType == "Branch Manager")
 	                        	{
									$agentQuery .= " and parentID = '$parentID'";                   
 	                            }
 	                            $agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
		 	                	for ($i=0; $i < count($agents); $i++){
 	    		        		?>
 	            				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            				<?
 	                			}
						
					?>
						</select>
						<?php }else{ ?>
						<input type="hidden" name="agentName" id="agentName" value="<?=$userID?>"> 
						<?php }  ?>
 	          			<script language="JavaScript">
 	        				SelectOption(document.Search.agentName, "<?=$_SESSION["agentName"]; ?>");
 	            		</script>
					
						
 	            	</td> 	<?php  } ?>
 	            <? //debug($agentQuery); ?>
 	            <!-- currency -->
 	             <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
 	          <td>
 	            <?
 	                $queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES." where 1 and currency!=''";
 	                $DestCurrencyData = selectMultiRecords($queryDestCurrency);
 	                    
 	                 $queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES." where 1 and currencyOrigin=''";
 	                 $OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
 	                    
 	                    for($a = 0; $a < count($DestCurrencyData); $a++)
 	                {
 	                 $allCurrency[$a] = $DestCurrencyData[$a]['currency'];
 	                            
 	                    $y++;
 	                
 	                }
 	            
 	                for($b = 0; $b < count($OriginCurrencyData); $b++)
 	                {
 	                        $allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
 	                    
 	                $y++;
 	                }
 	                $uniqueCurrency = array_unique($allCurrency);
 	                
 	        
 	            $uniqueCurrency = array_values ($uniqueCurrency);
 	        	//debug($_REQUEST['Tocurrency']);
 	                ?>  
 	                    Currency </td><td>
 	                    <select name="Tocurrency" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value="">-Select Currency-</option>
 	               
 	                   
 	                    <?
 	                    
 	                    for($c = 0; $c < count($uniqueCurrency); $c++)
 	                    {?>
 	                         <option value="<?=$uniqueCurrency[$c]?>" <?php if($_REQUEST['Tocurrency'] == $uniqueCurrency[$c]) echo "selected";?> ><?=$uniqueCurrency[$c]?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	       
 	       
 	        <script language="JavaScript">SelectOption(document.forms[0].Tocurrency, "<?=$Tocurrency?>");
 	          </script>  <? } ?>
 	           <!-- currency  --> 
 	           <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
 	            <td>
 	            	Bank Name </td><td>
 	              <select name="bankID" style="font-family:verdana; font-size: 11px">
 	                <option value="">- Select Bank-</option>
 	                	<? if($agentType != "SUPI Manager") {?>
 	                  <option value="all">All Banks</option>
 	                
 	                 <?
 	                   }
 	                    
						//work by Mudassar Ticket #11425
						if ($agentType == "SUPA" || $agentType == "SUBA"){
							$selected=selectMultiRecords("select linked_Distributor from admin where userID='$userID' or  parentID = '$userID'");
							$row=$selected[0];
							$array=explode(", ", $row["linked_Distributor"]);
							$userID="";
							for($i=0;$i<count($array);$i++){
								$row=$array[$i];
								if ($i==count($array)-1)
									$userID.="'$row'";
								else
									$userID.="'$row', ";
							}
							$banks = selectMultiRecords("select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where userID in ($userID)");
						}
		else if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){

				$banks = selectMultiRecords("select userID,username, agentCompany, name,  agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' AND agentStatus='Active' order by agentCompany");		
						
				}		
				else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
				$banks = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
				
				}
							
//work end by Mudassar Ticket #11425	
						
						
						
					
 	                    for ($i=0; $i < count($banks); $i++){
 	                ?>
 	                <option value="<?=$banks[$i]["userID"]; ?>"><? echo($banks[$i]["agentCompany"]." [".$banks[$i]["username"]."]"); ?></option>
 	                <?
 	                  }  }
 	                ?>
 	             </select>         
 	             <script language="JavaScript">
 	        SelectOption(document.Search.bankID, "<?=$_SESSION["bankID"]; ?>");
 	            </script>
 	        </td>  </tr>
 	        <tr><td>
 	            <?
 	                $queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES." where 1 ";
 	                    $DestCurrencyData = selectMultiRecords($queryDestCurrency);
 	                    
 	                 $queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES." where 1 ";
 	                    $OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
 	                    
 	                    for($a = 0; $a < count($DestCurrencyData); $a++)
 	                {
 	                 $allCurrency[$a] = $DestCurrencyData[$a]['currency'];
 	                            
 	                    $y++;
 	                
 	                }
 	            
 	                for($b = 0; $b < count($OriginCurrencyData); $b++)
 	                {
 	                        $allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
 	                    
 	                $y++;
 	                }
 	                $uniqueCurrency = array_unique($allCurrency);
 	                
 	        
 	            $uniqueCurrency = array_values ($uniqueCurrency);
				 	            
 	                ?>  <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
 	                    Currency </td><td>
 	                    <select name="Tocurrency" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value="">-Select Currency-</option>
 	               
 	                   
 	                    <?
 	                    $selected = "selected='selected'";
 	                    for($c = 0; $c < count($uniqueCurrency); $c++)
 	                    {
						?>
 	                         <option value="<?=$uniqueCurrency[$c]?>" <?php if($_REQUEST['Tocurrency'] == $uniqueCurrency[$c]){ echo $selected; } ?> ><?=$uniqueCurrency[$c]?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	       <script language="JavaScript">
		   /**
		    * #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
			* Short Description
			* This code is changed for the retain of the value of field after search
		   */
		   //SelectOption(document.forms[0].Tocurrency, "<?=$Tocurrency?>");
 	            </script><? } ?>
 	           
 	        </td>
 	        <? $agentType; if( $agentType!="SUPI" && $agentType!="SUPAI" ){ ?>
 	            <td>
 	            Destination Country </td><td align="left">
 	            <select name="Tocountry" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value=""> - Select Country - </option>
 	                <?
 	                    $queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType Like '%destination%' ";
 	                    $countryData = selectMultiRecords($queryCountry);
 	                    for($k = 0; $k < count($countryData); $k++)
 	                    {?>
 	                         <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>   
 	                    <?
 	                  }}
 	                ?>
 	        </select>
 	        <script language="JavaScript">SelectOption(document.forms[0].Tocountry, "<?=$Tocountry?>");
 	            </script>
 	           
 	                   <br><br><?
					   
					   /**
					    * #7888: Premier FX- account manager performance RTS
						* Short Description
						* This Query is Changed to Get the Agents Except Who is logged in
						*/
			//$cashierType =  "select name,username,adminType from ".TBL_ADMIN_USERS." where adminType IN ('call','Agent','admin') ";

//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){							
									
		 $cashierType =  "select name,username, agentStatus, adminType from ".TBL_ADMIN_USERS." where agentStatus = 'Active' AND adminType IN ('call','Agent','admin') AND userID != '$parentID'";

			
			}
									
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){								
		 $cashierType =  "select name,username,adminType from ".TBL_ADMIN_USERS." where adminType IN ('call','Agent','admin') AND userID != '$parentID'";
						}
//work end by Mudassar Ticket #11425

 	                    $cashierTypeQuery= selectMultiRecords($cashierType);
 	              
//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){							
									
		$cashierTypeTeller =  "select loginName,name,is_active from ".TBL_TELLER." where 1 AND is_active = 'Active' ";
			
			}
									
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){								
		$cashierTypeTeller =  "select loginName,name from ".TBL_TELLER." where 1 ";
						}
//work end by Mudassar Ticket #11425


				  
 	                
 	                    $cashierTypeQueryTeller= selectMultiRecords($cashierTypeTeller);
 	                
 	                for($k = 0; $k < count($cashierTypeQuery); $k++)
 	                {
 	                     $cashierAdminType[$k] = $cashierTypeQuery[$k]['adminType'];
 	                      
 	                     $cashierUsername[$k]= $cashierTypeQuery[$k]['name'];
 	                      $cashierName[$k]= $cashierTypeQuery[$k]['username'];
 	                    // $cashierName[$k]= $cashierTypeQuery[$k]['username'];
 	                    
 	                    
 	                    $x++;
 	                
 	                }
 	            
 	                for($j = 0; $j < count($cashierTypeQueryTeller); $j++)
 	                {
 	                    $cashierUsername[$x]= $cashierTypeQueryTeller[$j]['name'];
 	                    $cashierAdminType[$x] = 'Teller';
 	                $x++;
 	                }
 	                        
 	            
 	                        
 	                ?>
 	            </td></tr>
 	            <tr><td>
 	        Payment Mode </td><td>
 	        <select name="paymentMode" style="font-family:verdana; font-size: 11px;">
 	                <option value="">-Select Transaction Type-</option>
 	                <?
 	                    $queryMoneyPaid = "select distinct(moneyPaid) from ".TBL_TRANSACTIONS." where 1 and moneyPaid!='' ";
 	                    $queryData = selectMultiRecords($queryMoneyPaid);
 	                    for($k = 0; $k < count($queryData); $k++)
 	                    {?>
 	                         <option value="<?=$queryData[$k]['moneyPaid']?>"><?=$queryData[$k]['moneyPaid']?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	        <script language="JavaScript">SelectOption(document.forms[0].paymentMode, "<?=$paymentMode?>");
 	            </script>
 	       
 	    </td></tr>
 	    <tr>
 	    	<td>Transaction Type </td>
			<td><select name="transactionType" style="font-family:verdana; font-size: 11px;">
 	                <option value="">-Select Transaction Type-</option>
					<option value="Pick up">Pick up</option>   
					<option value="Bank Transfer">Bank Transfer</option>  
					<option value="Home Delivery">Home Delivery</option>       
 	        	</select>
 	       		<script language="JavaScript">SelectOption(document.forms[0].transactionType, "<?=$transactionType?>");</script>
 	    	</td>
			<td>Transaction Status</td>
			<td>
				<?php
					$transStatusQuery = "SELECT DISTINCT(transStatus) From transactions WHERE 1"; 
					$transStatuArr = SelectMultiRecords($transStatusQuery);
				?> 
				<select name="transactionStatus">
					<option value="">- Select -</option>
					<?php 
					foreach($transStatuArr as $key=>$val)
						echo "<option value='".$val["transStatus"]."'>".$val["transStatus"]."</option>";
					?>
				</select>
				<script language="JavaScript">SelectOption(document.forms[0].transactionStatus, "<?=$transactionStatus?>");</script>
			</td>
 	    	
 	    </tr>
		<tr>
 	    	<td>Transaction Source</td>
			<td><? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
 	    	</td>
		</tr>
 	    <tr>
		<td colspan= "4" align="center"><input type="submit" name="Submit" value="Search"></td>
		</tr>
 	     
 	   </tr>
	</td>
 </table>
</table>
    </table>
   </form>
      <br>
      <br>
      <table width="100%" border="1" cellpadding="0" bordercolor="#666666">
      <form action="repTransSummary.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
		  //debug($allCount);
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000" >
			<table  width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    <?=__("Showing") ?> <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                   <?=__("of") ?>
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&DateType=".$_REQUEST["DateType"]."&moneyPaid=$moneyPaid&fDate=$fromDate&tDate=$toDate&nT=$nameType";?>"><font color="#005b90"><?=__("First") ?></font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&DateType=".$_REQUEST["DateType"]."&moneyPaid=$moneyPaid&fDate=$fromDate&tDate=$toDate&nT=$nameType";?>"><font color="#005b90"><?=__("Previous") ?></font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&DateType=".$_REQUEST["DateType"]."&moneyPaid=$moneyPaid&fDate=$fromDate&tDate=$toDate&nT=$nameType";?>"><font color="#005b90">
<?=__("Next")?></font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&DateType=".$_REQUEST["DateType"]."&moneyPaid=$moneyPaid&fDate=$fromDate&tDate=$toDate&nT=$nameType";?>"><font color="#005b90">
<?=__("Last")?></font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
           <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;<?=__("There are")?> <? echo (count($contentsTrans) + count($onlinecustomer))?> <?=__("records to View") ?></span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
      <td nowrap bgcolor="#EFEFEF"><table width="100%" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			
			<td><span class="style1"><? echo __($manualCode); ?></span></td>
			
			<td><span class="style1"><? echo __($systemCode); ?></span></td>
			<td><span class="style1"><?=__("Status") ?></span></td>
			<td><span class="style1"><?=__("Creation Date") ?></span></td>
			  <td><span class="style1"><? echo __("Transaction Source"); ?></span></td>
			
			  <td><span class="style1"><?=__("Customer Name") ?> </span></td>
			 
			  <td><span class="style1"><?=__("Beneficiary Name") ?> </span></td>
			  <td><span class="style1"><?=__("Amount Sent") ?> </span></td>
			  <td><span class="style1"><?=__("Originating Currency Type") ?> </span></td>
			  <td><span class="style1"><?=__("Exchange Rate ") ?> </span></td>
			  <td><span class="style1"><?=__("Foriegn Amount") ?> </span></td>
			  <td><span class="style1"><?=__("Destination Currency Type") ?> </span></td>
			 
			  <td><span class="style1"><?=__("Amount before Commission") ?> </span></td>
			  <td><span class="style1"><?=__("Commission") ?> </span></td>
			  <td><span class="style1"><?=__("Amount After Commission") ?> </span></td>
			  <td><span class="style1"><?=__("Admin Staff Name") ?> </span></td>
			  <td>	 <span class="style1"><?=__("Introducer Name ") ?> </span></td>
			  
			 	
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
						
						
						if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$classRed = "classRed"; 
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$classRed = "style1";
														$custStatus = "Enable";
										
										}
																			
									}elseif(CONFIG_COLOR_OF_LINK == '1'){ 
							$senderRemarks = selectFrom("select remarks from ".TBL_CUSTOMER." where customerID='".$contentsTrans[$i]["customerID"]."'");
				 			$enquiry = selectFrom("select cmpID from ".TBL_COMPLAINTS." where transID='".$contentsTrans[$i]["transID"]."' and status = 'New'");
				 			
				 			if(($enquiry["cmpID"] != "")&&($contentsTrans[$i]["tip"] != "" || $contentsTrans[$i]["internalRemarks"] != "" || $senderRemarks["remarks"] != "")){
				  			$classRed = "classRed"; 
				  		}elseif($enquiry["cmpID"] != ""){
						  	$classRed = "classRed"; 
						  }elseif($contentsTrans[$i]["tip"] != "" || $contentsTrans[$i]["internalRemarks"] != "" || $senderRemarks["remarks"] != ""){
						  	$fontColor = "#FFF00";
						  }else{
						  	$classRed = "style1"; 
						  }
						
						}else{
				  			$classRed = "style1";
				  	} 
				  	$toolText = ""; 
				  if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
				  {
				  		
						$rateLimitQuery = "select id, minRateAlert, maxRateAlert, isAlertProcessed from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentsTrans[$i]["transID"]."' and isAlertProcessed = 'N'";
						$rateLimitContent = selectFrom($rateLimitQuery);
						if($rateLimitContent["minRateAlert"] > 0 || $rateLimitContent["maxRateAlert"] > 0)
						{
							$toolText = "Waiting till Exchange Rate Min = ".$rateLimitContent["minRateAlert"]." and Max = ".$rateLimitContent["maxRateAlert"];
							$classRed = "classRed"; 
						}
					}
					if($contentsTrans[$i]["transStatus"] == 'Hold')
					 	$classRed = 'classRed';
					else if($contentsTrans[$i]["Account_lmt_exceeded"] == 'Y')
					{$classRed = 'classOrange';}
					else
						$classRed = 'style1'; 
				?>

				<tr bgcolor="#FFFFFF">
				
				  <td width="100" bgcolor="#FFFFFF" class="<?=$classRed;?>"><a href="#" class="<?=$classRed;?>" <? if($toolText != ""){ ?>onmouseover="Tip('<?=$toolText?>')"; onMouseOut="UnTip()";<? } ?> onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><? echo $contentsTrans[$i]["refNumberIM"]; ?>
				  	</a></td>
					
				<?php// if(CONFIG_ACTIONDATE_COL =="1" && $_REQUEST["DateType"]=="actionDate" && ($transStatus!=="Pending" && !empty($transStatus))){
				  ?> 
				  <td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  <td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  
				  <td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $contentsTrans[$i]["transDate"]?></td>
				 <?php if($contentsTrans[$i]["trans_source"] == "O")
						   {
							$showTransSource = "Online";
						   }
						   else
						   {
							$showTransSource = "Payex";
						   
						   }
						   ?>
						    <td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $showTransSource?></td> 
				  
				 
				  <?
				 $customerContent = selectFrom("select FirstName, LastName,Phone,Mobile,Address from customer where customerID ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName,customerID from beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
?>
				  <td width="100" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo  ucfirst($customerContent["FirstName"]). " " .ucfirst($customerContent["LastName"]).""?></td>
				
				  
				  	<td width="100" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo ucfirst($beneContent["firstName"]). " " .ucfirst($beneContent["lastName"]) .""?></td>
				
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					 number_format($contentsTrans[$i]["transAmount"],2,",",".");?></td>
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $contentsTrans[$i]["currencyFrom"]?></td>
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					number_format($contentsTrans[$i]["exchangeRate"],2,",",".")?></td>
					
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					number_format($contentsTrans[$i]["localAmount"],2,",",".")?></td>
					
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $contentsTrans[$i]["currencyTo"]?></td>
					
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					number_format($contentsTrans[$i]["transAmount"],2,",",".")?></td>
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					number_format($contentsTrans[$i]["IMFee"],2,",",".")?></td>
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo 
					number_format($contentsTrans[$i]["totalAmount"],2,",",".")?></td>
					
					<?
						$collectionPoint=selectFrom("select cp_branch_name as c_name,cp_branch_no as c_branch from cm_collection_point where cp_id='".$contentsTrans[$i]["collectionPointID"]."'");
						
						$agentName = selectFrom("select name,username,agentCompany from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
						
						$distName = selectFrom("select name from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["benAgentID"]."'");
						
					
					?>
					  <?php 
					 	/**
						 * #7888 : Premeir FX : Adding Column Admin Staff
						 * Short Description
						 * This CONFIG is for the addition of the column Admin Staff Name
						 */
						if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "0"){
							$queryAdminStaff = "SELECT cust.parentID, admins.username, admins.name FROM ".TBL_CUSTOMER." AS cust, ".TBL_ADMIN_USERS." AS admins WHERE cust.customerID = '".$contentsTrans[$i]["customerID"]."' AND admins.userID = cust.parentID LIMIT 1";
							$adminStaffName = selectFrom($queryAdminStaff);
					 ?>
						<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>">
							<?php 
								echo $adminStaffName['name']." [".$adminStaffName['username']."]";
							?>
						</td>
					 <?php 
					 	}
				 	?>
					
					<td width="75" bgcolor="#FFFFFF" class="<?=$classRed;?>"><? echo $agentName['agentCompany']." [".$agentName['username']."]";?></td>
					
					
				  	                                                           
				  
					
				  
				
				</tr>
				<?
			}
			?>
			
			<? for($i=0;$i < count($onlinecustomer);$i++)
			{
				?>
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($onlinecustomer[$i]["totalAmount"],$roundLevel,'.',',') .  " " . $onlinecustomer[$i]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  
				  
				  </td>
				  
				  
				
				
				 
           <!-- Added by Niaz Ahmad againist ticket # 2531 at 09-10-2007 -->
           <? if(CONFIG_SHOW_COLLECTIONPOINTS == "1") {
              $collectionPoint = selectFrom("select cp_id,cp_corresspondent_name from " . TBL_COLLECTION . " where cp_id='".$contentsTrans[$i]["collectionPointID"]."' and '".$contentsTrans[$i]["transType"]."' = 'Pick up' "); 
				     ?> 				      
				     <td width="100" bgcolor="#FFFFFF" title="<? echo  $collectionPoint["cp_corresspondent_name"]; ?>">
				     <? echo $collectionPoint["cp_corresspondent_name"];
             } ?>
            </td>   
           
				  <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&viewTrans=Y" class="style2">Enquiry</a></td>
			  </tr>
				<?
			}
			}
			?>
		<tr bgcolor="#CCCCCC">
 	                <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	             	<td colspan="13">&nbsp;</td>
 	             	<?
					/**
					 * # 7888: Premeir Exchange - Addition of Admin Staff Name Column
					 * Short Description
					 * This @condition is for HTML layout after addition of column Admin Staff Name
					 */
 	            }elseif(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
 	           	?>
					<td colspan="18">&nbsp;</td>
				<?php 
				}
				else{	
					?>
 	            	<td colspan="17">&nbsp;</td> 	            	
 	            	<?
 	            	}
 	             	?>
 	                </tr>
 	            <tr>
                  <td  align="left">&nbsp;</td>
				 </tr>
				 
				 
				 
				 <?
					$totalCancelLess 			= 0;
					$totalTransAmountGrouped	= 0;
					$totalIMFee 				= 0;
					$totalGrandAmountLess 		= 0;
					$totalCurrencyArr			= array();
					$totalCurrencyArr1          = array();
					$totalIMFeeArr              =array();
					$cumulativeArrTotal=array();
					
					
					
					//debug($queryGroupSendingCurrencyCancelled);
					for($s=0;$s < count($contentsTransGroup);$s++)
					{
						//debug($contentsTransGroup[$s]["totalGroup"]);
						//$totalCurrencyArr[] = $contentsTransGroup[$s]["currencyFrom"];
						//$totalCurrencyArr1[]=$contentsTransGroup[$s]["currencyTo"];
						$totalIMFeeArr[]=$contentsTransGroup[$s]["IMFee"];
					//	$commulativeArr[]= $contentsTransGroup[$s]["totalTransAmount"];
						//debug($totalCurrencyArr);
						$totalCancelLess += $contentsTransGroup[$s]["totalGroup"];
						$totalTransAmountGrouped += $contentsTransGroup[$s]["totalTransAmount"];
						$totalIMFee += $contentsTransGroup[$s]["IMFee"];
						$totalGrandAmountLess += $contentsTransGroup[$s]["grandAmount"];
					}
					//debug($contentsTransGroup);
					if(count($contentsTransGroup)>0){?>
						<?php
						for($bottom=0;$bottom < count($contentsTransGroup);$bottom++)
						{
						?>
						<tr bgcolor="#CCCCCC"  style="text-align: right;">
						 <td width="75">&nbsp;</td>
						 <td width="75">&nbsp;</td>
						 <td width="71">&nbsp;</td> 
						 <td width="81">&nbsp;</td>
						 <td width="50">&nbsp;</td>
						 <td width="83">&nbsp;</td><td width="83">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="67"><?
				 echo number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',');
										  
					/*if ((isset $contentsTransGroup[$bottom]["currencyFrom"]))
					{
					
					$commulativeArr[$bottom]+=	$contentsTransGroup[$bottom]["totalTransAmount"];
					}	
					*/
					
					if(isset( $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]))
					{
					 $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]= $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]] + $contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
					else {
						
						$totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]=$contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
						 		
								// debug($commulativeArr);
										  ?></td>
						 <? } ?>
						 <td align="left"><?=getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"])?></td>
						
						 <td width="71">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');
									


        if(isset( $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]))
					{
					 $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]= $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]] + $contentsTransGroup[$bottom]["totalLocalAmount"];
					}
					else {
						
						$totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]=$contentsTransGroup[$bottom][  	 
							"totalLocalAmount"];
					}
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td align="left">&nbsp;<? echo ($contentsTransGroup[$bottom]["currencyTo"]);
				
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="100">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]); ?></td>
						 
						 <? } ?>
						 <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="71"></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71"><? 
										echo number_format($contentsTransGroup[$bottom]["IMFee"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]);

					
										?></td>
						 <td width="100"><? 
										echo number_format($contentsTransGroup[$bottom]["grandAmount"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]); 
										?></td>
						 <?
									  }
										?>
						 <td width="71">&nbsp;</td>
						 <!-- Net Amount for distributor only -->
						 <? if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="80">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');   
									  ?></td>
						 <? } ?>
						 <!-- Net Amount for distributor only   -->
						 <td width="78">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						</tr>
						<?
								}
						if(is_array($totalCurrencyArr))
							//$totalCurrencyArr = array_unique($totalCurrencyArr);
						
						if(count($totalCurrencyArr)> 0){
							//debug($totalCurrencyArr);
						//	debug($queryGroup);
							$totalCurrency = $totalCurrencyArr[0];
							?>
							<tr  style="text-align: right;">
								 <td  align="left" colspan="5">&nbsp;<b><font color="#006699">Total for Transactions (excluding Cancelled): &nbsp;<i><?=$totalCancelLess?></i></font></b></td>
								 <!--<td  align="right" colspan="2"><b><font color="#006699">Total Amount(s) :&nbsp;</font></b></td>
								 <td><?=number_format($totalTransAmountGrouped,2,'.',',');?></td>
								 <td align="left"><?=$totalCurrency?></td>
								 <td colspan='3'>&nbsp;</td>
								 <td><?=number_format($totalTransAmountGrouped,2,'.',',');?> <?=$totalCurrency?></td>
								 <td><?=number_format($totalIMFee,2,'.',',');?> <?=$totalCurrency?></td>
								 <td><?=number_format($totalGrandAmountLess,2,'.',',');?> <?=$totalCurrency?></td>-->
								</tr>
							<?
						}	
					}
				// debug($commulativeArr);
				
					$totalTransAmountCanc 		= 0;
					$totalTransIMFeeCanc 		= 0;
					$totalTransGrandAmountCanc 	= 0;
					$totalCurrencyArrCanc		= array();
					//debug($contentsTransCancelled);					
					for($c=0;$c < count($contentsTransCancelled);$c++){
					
						$totalCurrencyArrCanc[]	= $contentsTransCancelled[$c]["currencyFrom"];
						$totalTransAmountCanc += $contentsTransCancelled[$c]["totalTransAmount"];
						$totalTransIMFeeCanc += $contentsTransCancelled[$c]["IMFee"];
						$totalTransGrandAmountCanc += $contentsTransCancelled[$c]["grandAmount"];
					}		
					
					if(count($contentsTransCancelled)>0){
				
						
						for($cancel=0;$cancel < count($contentsTransCancelled);$cancel++)
						{
						?>
						<tr bgcolor="#CCCCCC"  style="color:#FF0000;text-align: right;" >
						 <td width="75">&nbsp;</td>
						 <td width="75">&nbsp;</td>
						 <td width="71">&nbsp;</td>
						 <td width="81">&nbsp;</td>
						 <td width="50">&nbsp;</td>
						 <td width="83">&nbsp;</td>
						 <td width="83">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="67"><?
										  echo number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',');
										  
					
										  
										//  debug($totalCurrencyArrCanc);
										  ?></td>
						 <? } ?>
						 <td  align="left"><?=getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"])?></td>
						 <td width="71">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');
									  
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td align="left">&nbsp;<? echo ($contentsTransCancelled[$cancel]["currencyTo"]);
				
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="100">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); ?></td>
						 <? } ?>
						 <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="71"></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71"><? 
										echo number_format($contentsTransCancelled[$cancel]["IMFee"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); 
										?></td>
						 <td width="100"><? 
										echo number_format($contentsTransCancelled[$cancel]["grandAmount"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); 
										?></td>
						 <?
									  }
										?>
						 <td width="71">&nbsp;</td>
						 <!-- Net Amount for distributor only -->
						 <? if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="80">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');   
									  ?></td>
						 <? } ?>
						 <!-- Net Amount for distributor only   -->
						 <td width="78">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						</tr>
						<?
								}
						if(is_array($totalCurrencyArrCanc))
							$totalCurrencyArrCanc = array_unique($totalCurrencyArrCanc);
							$totalCurrencyCanc = $totalCurrencyArrCanc[0];
							?>
							<tr style="color:#FF0000;text-align: right;">
								 <td  align="left" colspan="5">&nbsp;<b><font color="#006699">Total for Transactions [Cancelled]:&nbsp;<i style='padding-left:20px;'><?=$allCount-$totalCancelLess?></i></font></b></td>
								<!-- <td  align="right" colspan="2"><b><font color="#006699">Total Amount(s) :&nbsp;</font></b></td>
								 <td><?=number_format($totalTransAmountCanc,2,'.',',');?></td>
								 <td align="left"><?=$totalCurrencyCanc?></td>
								 <td colspan='3'>&nbsp;</td>
								 <td><?=number_format($totalTransAmountCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>
								 <td><?=number_format($totalTransIMFeeCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>
								 <td><?=number_format($totalTransGrandAmountCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>-->
								</tr>
								
							<?
							//}
					} ?>
				 
 	          </table>
			 
			 <table width="100%" border="0" bordercolor="#EFEFEF">


<tr bgcolor="#CCCCCC" align="right">
<td width="75"></td>
<td width="75"></td>
<td width="71"></td>
<td width="81"></td>
<td width="50"></td>
<td width="71">
<?foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
	
}?></td>


<td width="81"><?//print_r($totalCurrencyArr);

?></td>
<!--<td width="67"></td>-->

<td width="140"><?//print_r($totalCurrencyArr1);
foreach ($totalCurrencyArr1 as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}
?></td>

<td width="86"><?//print_r($totalCurrencyArr);
foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}

?></td>

<td width="86"><?//print_r($totalCurrencyArr);
foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}

?></td>
<td width="132" align="center"></td>
<td width="86"></td>


</tr>
</br>
</br>
<tr><b><font color="#006699">Cumulative Sum Of Transactions [Not Cancelled]</font></b></tr>
		</table>
 	              </td>
 	        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {?>
                    <?=__("Showing") ?> <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                   <?=__("of")?> 
                    <?=$allCount; ?>
                    <?php }?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType";?>"><font color="#005b90"><?=__("First") ?> </font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType";?>"><font color="#005b90"><?=__("Previous") ?></font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
				
									<?
										/**
										 * Fixing the navigation issue as when click on next button it disturb the searched query
										 * @Ticket# 3531
										 *
										 * Updations in link: 
										 * Just add &agentNameType=$agentNameType to the end of the link
										 */
									?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType";?>"><font color="#005b90"><?=__("Next") ?></font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType";?>"><font color="#005b90"><?=__("Last") ?></font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
 
          <?
			} else {
		?>
          <tr>
            <td  align="center"> <? if($errorMessage == ""  ){?>
            	 No Transaction found in the database.
            	 <? }elseif($errorMessage != "") {
            	 			echo $errorMessage;
            	 	}
            	 	
            	 	?>
            	 
            </td>
          </tr>
          <?
			}
			
		?>
		<script language="JavaScript">
		
		function exportcheck()
		{
		
		var allcount = <? echo $allCount ?>;
		//alert(allcount);
		
		if (allcount > 10000 ){
			alert("Maximum transction records should be less than 10,000"); 
			return false;
			                  }
							 
		else {
		document.trans.action='ExportRTS.php'; 
		document.trans.submit();
		     }
		}
		</script>
		<tr> 
		
            <td colspan="13"  align="center"> <input type="hidden" name="query"
 value="<?=base64_encode(serialize($exportQuery))?>" />
                        <input type="hidden" name="queryGroup" value="<?= base64_encode(serialize($exportQueryGroup))?>" />
                        <input type="hidden" name="queryCurrency" value="<?= base64_encode(serialize($queryCnt))?>" />
                        <input type="hidden" name="transStatus" value="<?=base64_encode(serialize($transStatus))?>" />
                        <input type="hidden" name="DateType" value="<?=base64_encode(serialize($_REQUEST["DateType"]))?>" />
                        <input type="hidden" name="queryGroupSendingCurrencyCancelled" id="queryGroupSendingCurrencyCancelled" value="<?=base64_encode(serialize($queryGroupSendingCurrencyCancelled))?>" />
                        <input type="button" onClick="exportcheck()" value="Export_to_Excel"/>
            	 
            </form>
			
		  </td>
          </tr>
		
      </table></td>
  </tr>

</table>
</body>
</html>
