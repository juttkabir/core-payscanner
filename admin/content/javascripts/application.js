jQuery.easing.easeOutQuart = function (x, t, b, c, d) {return -c * ((t=t/d-1)*t*t*t - 1) + b;};

$(document).ready(function()
{ 
  
  $('#menu .description').hide();

  $("#menu li ul li").hover(function()
  {
    $('#menu .description').hide();
    $(this).children('.description').fadeIn();

  },function()
  {
    $('#menu .description').hide();
   $(this).children('.description').fadeOut();
  });
  
  
  var slideshow = $(".slideshow");
  if(slideshow.html() != null) if(slideshow.find('li').size() > 1) slideshow.crossfade({interval: 10000});
  
  
  var ticker = $(".ticker");
  if(ticker.html() != null)
  { 
    ticker.before("<div id='tickerInfo'></div>");
    
    ticker.wrapAll("<div class='ticker'></div>")
    ticker.removeAttr("class");
    ticker.attr("class","container");
    
    var container = ticker;
    container.find(".info").hide();
    
    var containerWidth = 0;
    container.find("li").each(function(i){containerWidth += $(this, i).outerWidth() + 5});
    container.width(containerWidth);
    
    var duration = container.outerWidth()/.04;
    
    animateTicker();
  }
  
  function animateTicker()
  {
    container.css("left",500);
    container.animate({left:-container.outerWidth()}, duration, "linear",animateTicker);
    
    var enterFrame = setInterval(check,100);
    var currentTick = 0;
    function check()
    {
      position = $(".ticker").position().left - container.position().left;
      original = container.find("li").eq(currentTick).position().left;
      if(position >= original)
      {
        $("#tickerInfo").html(container.find("li").eq(currentTick).find(".info").html());
        if(currentTick + 1 < container.find("li").length) currentTick +=1; 
      }
    }
  }
  
  $(".icons li").bind("mouseenter",showContent);
  
  function showContent()
  {
    $.scrollTo($(this),{duration:"fast",offset:-50,easing:"easeOutQuart"});
    content = $(this).find(".content");
    content.hide();
    content.show("drop",{ direction: "up"},$(this).unbind("mouseenter"));
    $(this).bind("mouseleave",hideContent);
  }
  
  function hideContent()
  {
    content = $(this).find(".content");
    content.fadeOut("fast");
    $(this).bind("mouseenter",showContent);
  }
  
  $(".list").find("li").toggle(function()
  {
    content = $(this).find("p");
    content.hide();
    content.slideDown('slow');
    
  },function()
  {
    content = $(this).find("p");
    content.slideUp('slow');
  })
  
  var callMeBack = $('#call-me-back');
  if(callMeBack.html() != null)
  {
    var validation  = callMeBack.validate();
    callMeBack.find("#name").rules("add", {minilength: 5});
    callMeBack.find("#phone").rules("add", {minlength:11, digits: true});
    
    callMeBack.find('input:first').focus();
  }
  
  
  $('.new .description p:first').css('font-weight','bold').css('font-size','110%');
  
});