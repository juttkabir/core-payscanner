(function($){ $.fn. crossfade = function(options)
{
  /*@params
    @element
    @items
    @start
    @nav
    @controls
    @duration
    @fadeOutDuration
    @interval
    @fixed
    @onBefore
  */
  var defaults  = {
                    items:'li',
                    start: 0,
                    nav: false,
                    controls: false,
                    duration: 1000,
                    fadeOutDuration: 2000,
                    fixed:true,
                    interval:false
                  };
                  
                  
  var options   = $.extend(defaults, options);
  var element   = this;
  var items     = element.children(options.items);
  var length    = items.length - 1;
  var active    = options.start;
  var prev      = options.start;
  
  //element.css('position','relative');
  items.css('position','relative');
  
  if(options.fixed) items.css('position','absolute');
  
  if(options.nav)
  {
    var nav=$(options.nav);
    nav.children().css('cursor','pointer');
    nav.children().click(function(event)
    {
      node = nav.find('li').index($(event.currentTarget));
      if(node <= length)
      {
        active = node;
        showItem();
      }
    })
  }
  else
  {
    var nav = false;
  }
  
  if(options.controls)
  {
    items.each(function()
    {
      $(this).append("<div class='controls group'></div>");
      
      if(items.index($(this))!=0)
      {
        var label = options.controls.prev || "Prev"
        $(this).find('.controls').append("<span class='prev'>"+label+"</span>");
        $(this).find('.controls .prev').click(function(event){active-=1; showItem()});
      }
      if(items.index($(this))!=length)
      {
        var label = options.controls.next || "Next"
        $(this).find('.controls').append("<span class='next'>"+label+"</span>");
        $(this).find('.controls .next').click(function(event){active+=1; showItem()});
      }
    })
  }
  
  
  if(options.interval) setInterval(showItem,options.interval);

  
  showItem(true);
  
  function showItem(firstRun)
  {
    if(firstRun || options.onBefore == undefined || options.onBefore(element,items.eq(prev),prev,active,nav) == true)
    {
      items.hide();
      items.eq(prev).css("z-index",0);
      items.eq(active).css("z-index",1);
      items.eq(prev).show();
      if(!firstRun){animate(items.eq(prev),'hide');}
      if(options.fixed) element.height(items.eq(active).height());
      animate(items.eq(active),'show');
      prev = active;
      
      if(options.nav)
      {
        $(options.nav).find('li').removeClass('active');
        $(options.nav).find('li').eq(active).addClass('active');
      }
      
      if(options.interval) active != items.size() -1 ? active+=1 : active=0;
    }
  }
  
  function animate(element, animation)
  {
    if(options.duration > 0)
    {
      if(animation=='hide') element.fadeOut(options.fadeOutDuration);
      if(animation=='show') element.fadeIn(options.duration);
    }
    else
    {
      if(animation=='hide') element.hide();
      if(animation=='show') element.show();
    }
  }
  
};
  
 })(jQuery)