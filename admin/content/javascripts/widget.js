$(document).ready(function()
{
  if($('.widget').html() != null)
  {
    getValue('#from',$('#from'));
    getValue('#to',$('#to'));
    $('#left').focus();
    if($.browser.msie && $.browser.version != 6)
    {
      $('.widget select').data("origWidth", $('.widget select').css("width")).css("width", "auto");
    }
  }
  
});

function getValue(target,obj)
{
  setTimeout(function()
  {
    var elem = $(target).find('option[selected]');
    obj.prev('span').text(elem.val() + ' ' + elem.text().substring(elem.text().indexOf('(')+1,elem.text().lastIndexOf(')')));
  },500)
}


if($('.widget').html() != null)
{
  $('#disclaimer').toggle(function(){$(this).find('p').slideDown()},function(){$(this).find('p').slideUp()}).css('cursor','pointer').find('p').hide();
  
  var widget = $('.widget');
  
  widget.find('select').each(function()
  {
    $(this).wrapAll("<div class='selector'></div>").before($(document.createElement("span")));
  })
  
  widget.find('select').click(function()
  {
    value = this.options[this.selectedIndex].innerHTML;
    value = $(this).val() + " " + value.substring(value.indexOf('(')+1,value.lastIndexOf(')'));
    $(this).prev('span').text(value);
  })
  
  var convertInterval = 0;
  var spinner = $('.spinner').hide();
  
  $('#left').keypress(function(e)
  {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
      return false;
    }
    else
    {
      spinner.fadeIn().css('left',260);
      clearInterval(convertInterval);
      convertInterval = setInterval(function()
      {
        convertIt( $('#left').val(), $('#from').val(), $('#to').val(), $('#right'));
        clearInterval(convertInterval);
        spinner.fadeOut();
      },2000);
    }

  })
  
  $('#right').keyup(function(e)
  {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
      return false;
    }
    else
    {
      spinner.fadeIn().css('left',118);
      clearInterval(convertInterval);
      convertInterval = setInterval(function()
      {
        convertIt( $('#right').val(), $('#to').val(), $('#from').val(), $('#left'));
        clearInterval(convertInterval);
        spinner.fadeOut();
      },2000);
    }
  })  
}