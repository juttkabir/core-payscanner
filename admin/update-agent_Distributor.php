<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType2 = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}elseif($_POST["userID"] != ""){
	$userID = $_POST["userID"];
	
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$CountryGet = $_GET["Country"];
}elseif($_POST["CountryGet"] != "")
{
	$CountryGet = $_POST["CountryGet"];
}

if ($_GET["agentStatus"] != ""){
	$agentStatusGet = $_GET["agentStatus"];
}elseif($_POST["agentStatusGet"] != ""){
	
	$agentStatusGet = $_POST["agentStatusGet"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$type = $_GET["type"];
}
if ($_GET["ida"] != ""){
	$ida = $_GET["ida"];
}

if($_GET["userID"] != "")
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
else
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$userID."'");
	
if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1"){
	$settlementCurrencies = explode("|",$agentDetails["settlementCurrencies"]);
	$strAsAgent 		= $settlementCurrencies[0];
	$strAsDistributor 	= $settlementCurrencies[1];
}

/*	#5441 - AMB Exchange
	Operator ID is added in AnD form.
	to be displayed in Western Union Output export file.
	by Aslam Shahid
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1"){
	$operatorFlag = true;
}
/*	#4966 
	Company Type dropdown added on agents form and Trading field also added.
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
<?	if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {  ?>
	if(theForm.manualAgentNum.value == "" || IsAllSpaces(theForm.manualAgentNum.value)){
  	alert("Please provide the AnD's Login Name.");
      theForm.manualAgentNum.focus();
      return false;
  }
<?	}  ?>
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	/*if(theForm.postcode.value == "" || IsAllSpaces(theForm.postcode.value)){
    	alert("Please provide the agent's Post Code.");
        theForm.postcode.focus();
        return false;
    }*/
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	<?
		/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
		if ( defined("CONFIG_POST_CODE_MENDATORY")
			&& CONFIG_POST_CODE_MENDATORY == 1 )
		{
			?>
				if ( theForm.postcode.value == "" )
				{
					alert("Please provide post code.");
					theForm.postcode.focus();
					return false;
				}
			<?
		}
		/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
	?>
	/*
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
   /*
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the agent's email address.");
        theForm.email.focus();
        return false;
    } else {
	}
	if(theForm.agentCompDirector.value == "" || IsAllSpaces(theForm.agentCompDirector.value)){
    	alert("Please provide the agent company director's name.");
        theForm.agentCompDirector.focus();
        return false;
    }
	if(theForm.agentBank.value == "" || IsAllSpaces(theForm.agentBank.value)){
    	alert("Please provide the agent's Bank name.");
        theForm.agentBank.focus();
        return false;
    }
	if(theForm.agentAccountName.value == "" || IsAllSpaces(theForm.agentAccountName.value)){
    	alert("Please provide the agent's bank account name.");
        theForm.agentAccountName.focus();
        return false;
    }
	if(theForm.agentAccounNumber.value == "" || IsAllSpaces(theForm.agentAccounNumber.value)){
    	alert("Please provide the agent's bank account number.");
        theForm.agentAccounNumber.focus();
        return false;
    }
	if(theForm.agentCurrency.options.selectedIndex == 0){
    	alert("Please select agent's currency.");
        theForm.agentCurrency.focus();
        return false;
    }
	if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
    	alert("Please provide the agent's account limit.");
        theForm.agentAccountLimit.focus();
        return false;
    }
	if(theForm.agentCommission.value == "" || IsAllSpaces(theForm.agentCommission.value)){
    	alert("Please provide the agent's commision percentage.");
        theForm.agentCommission.focus();
        return false;
    }*/
	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value)){
    	alert("Please select atleast one country.");
        theForm.IDAcountry.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
function processCompanyType(){
	var compTypeID = document.getElementById("companyType");
	if(compTypeID!=null){
		if(compTypeID.value!="MSB"){
			var compVal = "3rd Party";
		}
		else{
			var compVal = "MSB";
		}
		var s = 1;
		while(document.getElementById("companyLabel_"+s)!=null){
			document.getElementById("companyLabel_"+s).innerHTML = compVal;
			s++;
		}
	}
}
	// end of javascript -->
	</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update Super A&D</font></strong></td>
  </tr>
  <form action="add-agent_distributor-conf.php?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>&type=<?=$type;?>&ida=<?=$ida;?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
  <input type="hidden" name="userID" value="<?=$agentDetails["userID"]; ?>">
   <input type="hidden" name="CountryGet" value="<?=$CountryGet; ?>">
   <input type="hidden" name="agentStatusGet" value="<?=$agentStatusGet; ?>">
  <tr>
    <td align="center">
		<table width="448" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update Super A&D</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		  <tr bgcolor="#EEEEEE"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["msg"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["msg"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td><? echo "<font color='" . ($_GET["msg"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td></tr></table></td></tr><? } ?>
              	<tr bgcolor="#ededed">
			<td height="19" colspan="2"><a class="style2" href="agent_Distributor-list.php?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$CountryGet;?>&agentStatus=<?=$agentStatusGet;?>&type=<?=$type;?>&ida=<?=$ida;?>">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
	<?	
	if($agentType2 != 'Branch Manager'){
		//$nAgentType = $_GET["type"];
		if(IS_BRANCH_MANAGER){
		?>
		
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>
             
              <? if(CONFIG_CUSTOMER_CATEGORY == '1'){ echo CONFIG_ADMIN_STAFF_LABEL; }else{?>
              Select Branch Manager
              <? } ?>
              
              </strong></font></td>
            <td>
          
              <select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226" >
                <option value="">- Select One -</option>
                <?
					$agents = selectMultiRecords("select userID,name,username, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Branch Manager'");
					for ($i=0; $i < count($agents); $i++){
				if($agents[$i]["userID"] == $agentDetails["parentID"])
				{
				?>
                <option value="<?=$agents[$i]["userID"]; ?>" selected>
                <? echo $agents[$i]["username"]." d[".$agents[$i]["name"]."]" ?>
                </option>
                <?
				}
				else
				{					
				?>
                <option value="<? echo $agents[$i]["userID"]; ?>">
                <? echo $agents[$i]["username"]." h[".$agents[$i]["name"]."]"; ?>
                </option>
                <?
			}
					}
				?>
              </select>
            </td>
          </tr>
          <?
        }
		}
		
		?>		
		
		  <?	if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {  
		  		$loginName = "AnD Login Name";
		  ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><?=$loginName?><font color="#ff0000">*</font></strong></font></td>
            <td width="268">
            	<input type="text" name="manualAgentNum" value="<?=stripslashes($agentDetails["username"]); ?>" size="35" maxlength="255">
            	<input type="hidden" name="manualAgentNum2" value="<?=stripslashes($agentDetails["username"]); ?>">
            </td>
        </tr>
      <?	}  ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong> Name*</strong></font></td>
            <td width="268"><input type="text" name="agentCompany" value="<?=stripslashes($agentDetails["agentCompany"]); ?>" size="35" maxlength="255"></td>
        </tr>
		  <? if($companyTypeFlag){?>
          <tr bgcolor="#ededed"> 
            <td width="156">
				<font color="#005b90"><strong>Company Type* </strong></font>
             </td>
            <td width="361">
              <SELECT name="companyType" id="companyType" style="font-family:verdana; font-size: 11px" onChange="processCompanyType();"> 
                <OPTION value="MSB">MSB</OPTION>
                <OPTION value="3rd party">3rd Party</OPTION>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].companyType, "<?=$agentDetails["companyType"]; ?>");
                                </script>
            </td>
          </tr>
		  <? }?>
		  <? if($tradingNameFlag){?>
          <tr bgcolor="#ededed" id="tradingDIV"> 
            <td width="156"><font color="#005b90">
            	<strong>
                 Trading Name 
              	</strong>
              </font>
             </td>
            <td width="361">
              <input type="text" name="tradingName" value="<?=stripslashes($agentDetails["tradingName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
		 <? }?> 
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong> Contact Person*</strong></font></td>
            <td><input type="text" name="agentContactPerson" value="<?=stripslashes($agentDetails["agentContactPerson"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 1*</b></font></td>
            <td><input type="text" name="agentAddress" value="<?=stripslashes($agentDetails["agentAddress"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><input type="text" name="agentAddress2" value="<?=stripslashes($agentDetails["agentAddress2"]); ?>" size="35" maxlength="255"></td>
        </tr>
		<tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Post Code </b></font>
				<?
					/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
					if ( defined("CONFIG_POST_CODE_MENDATORY")
						&& CONFIG_POST_CODE_MENDATORY == 1 )
					{
					?>
						<strong>*</strong>
					<?
					}
					/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
				?>
			</td>
            <td><input type="text" name="postcode" value="<?=stripslashes($agentDetails["postCode"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><? echo $agentDetails["correspondent"];?><b>Country*</b></font></td>
            <td><input type="hidden" value="<? echo $agentDetails["agentCountry"];?>" name="oldCountry"><SELECT name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action = 'update-agent_Distributor.php?userID=<?=$userID?>'; document.forms[0].submit();">
                <OPTION value="">- Select Country-</OPTION>
				<!--option value="United States">United States</option>
				<option value="Canada">Canada  correspondent </option-->
				<?
							
				 
					$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 order by countryName");
								
						//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<? if($_POST["Country"] == "") echo $agentDetails["agentCountry"]; else echo $_POST["Country"]; ?>");
                                </script></td>
        </tr>
		
       <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>City</b></font></td>
            <td> 
              <input type="text" name="City" value="<?=stripslashes($agentDetails["agentCity"]);?>" size="35" maxlength="255">
            </td>
          </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td><input type="text" name="agentPhone" value="<?=stripslashes($agentDetails["agentPhone"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><input type="text" name="agentFax" value="<?=stripslashes($agentDetails["agentFax"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Email</strong></font></td>
            <td><input type="text" name="email" value="<?=stripslashes($agentDetails["email"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td><input type="text" name="agentURL" value="<?=stripslashes($agentDetails["agentURL"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><label id="companyLabel_1"><?=(stripslashes($agentDetails["companyType"])!="" && $companyTypeFlag ? stripslashes($agentDetails["companyType"]) : "MSB"); ?></label> Number</strong></font>
			<? if(CONFIG_NON_MANDATORY_BANK_NAME_AT_COLLECTION_POINT != "1"){ ?>
			<br><font size="1">OR (State Bank license or permission from Ministry 
              of Finance)</font>
			  <? } ?>
			  </td>
            <td><input type="text" name="agentMSBNumber" value="<?=stripslashes($agentDetails["agentMSBNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><label id="companyLabel_2"><?=(stripslashes($agentDetails["companyType"])!="" && $companyTypeFlag ? stripslashes($agentDetails["companyType"]) : "MSB"); ?></label> Expiry</strong></font></td>
            <td>
					<SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].msbDay, "<? echo substr($agentDetails["agentMCBExpiry"], 8,2);?>");
          		</script>
          		<SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbMonth, "<? echo substr($agentDetails["agentMCBExpiry"], 5,2); ?>");
          </script>
          		
          		 <SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbYear, "<?echo substr($agentDetails["agentMCBExpiry"], 0,4); ?>");
          </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Registration Number</strong></font></td>
            <td><input type="text" name="agentCompRegNumber" value="<?=stripslashes($agentDetails["agentCompRegNumber"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Director</strong></font></td>
            <td><input type="text" name="agentCompDirector" value="<?=stripslashes($agentDetails["agentCompDirector"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td><input type="text" name="agentDirectorAdd" value="<?=stripslashes($agentDetails["agentDirectorAdd"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong>Director Proof ID</strong></font></td>
            <td valign="top"><input type="radio" name="agentProofID" value="Passport" <? if ($agentDetails["agentProofID"] == "Passport" || $agentDetails["agentProofID"] == "") echo "checked"; ?>>Passport <br><input type="radio" name="agentProofID" value="Driving License" <? if ($agentDetails["agentProofID"] == "Driving License") echo "checked"; ?>>Driving License <br><input type="radio" name="agentProofID" value="Other" <? if ($agentDetails["agentProofID"] == "Other") echo "checked"; ?>>Other please specify <input type="text" name="otherProofID" value="<?=$agentDetails[otherProofID]; ?>" size="15"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td><SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idMonth, "<? echo substr($agentDetails["agentIDExpiry"], 5,2); ?>");
          </script>
					<SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].idDay, "<?echo substr($agentDetails["agentIDExpiry"], 8,2);?>");
          		</script> <SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idYear, "<?echo substr($agentDetails["agentIDExpiry"], 0,4);?>");
          </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong> Document Provided</strong></font></td>
            <td><input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($agentDetails["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
<label id="companyLabel_3"><?=(stripslashes($agentDetails["companyType"])!="" && $companyTypeFlag ? stripslashes($agentDetails["companyType"]) : "MSB"); ?></label> License <br>
<input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($agentDetails["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
Company Registration Certificate <br>
<input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($agentDetails["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
Utility Bill <br>
<input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($agentDetails["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
Director Proof of ID</td>
        </tr>
	<? if($operatorFlag){?>
		<tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Operator ID</strong></font></td>
            <td><input type="text" name="OperatorID" value="<?=stripslashes($agentDetails["OperatorID"]); ?>" size="35" maxlength="255"></td>
        </tr>
	<? }?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Bank</strong></font></td>
            <td><input type="text" name="agentBank" value="<?=stripslashes($agentDetails["agentBank"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Name</strong></font></td>
            <td><input type="text" name="agentAccountName" value="<?=stripslashes($agentDetails["agentAccountName"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Number</strong></font></td>
            <td><input type="text" name="agentAccounNumber" value="<?=stripslashes($agentDetails["agentAccounNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Branch Code</strong></font></td>
            <td><input type="text" name="agentBranchCode" value="<?=stripslashes($agentDetails["agentBranchCode"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Type</strong></font></td>
            <td><select name="agentAccountType">
			<option value="Current">Current</option>
			<option value="Saving">Saving</option>
			<option value="Other">Other</option>
			</select><script language="JavaScript">
						         			SelectOption(document.forms[0].agentAccountType, "<? echo $agentDetails["agentAccountType"]; ?>");
						          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Currency</strong></font></td>
            <td><SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
            	<?
						 						  
					$strQuery = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ";
					$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
					while($rstRow = mysql_fetch_array($nResult))
					{
						$currency = $rstRow["currencyName"];	
						$description = $rstRow["description"];
						$erID  = $rstRow["cID"];							
						if($selectedCurrency == $currency)
						{
							echo "<option value ='$currency' selected> ".$currency." --> ".$description."</option>";			
						}
						else
						{
						    echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
						  }
					}
		  ?>				
                  </SELECT><script language="JavaScript">
	         			SelectOption(document.forms[0].agentCurrency, "<? echo $agentDetails["agentCurrency"]; ?>");
	          	  </script></td>
        </tr>
        <? if (CONFIG_AGENT_LIMIT == 1){ ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Limit</strong></font></td>
            <td><input type="text" name="agentAccountLimit" value="<?=stripslashes($agentDetails["agentAccountLimit"]); ?>" size="35" maxlength="15"></td>
        </tr>
      <? } ?>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Commission package</strong>(As Agent)</font></td>
          <td><select name="commPackage" id="commPackage">
            <option value="001">Fixed Amount</option>
            <option value="002">Percentage of Transaction Amount</option>
            <option value="003">Percentage of Fee</option>
			<option value="004">None</option>
          </select><script language="JavaScript">
						         			SelectOption(document.forms[0].commPackage, "<? echo $agentDetails["commPackage"]; ?>");
						          	  </script>
            </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Commission Percentage/Fee</strong>(As Agent)</font></td>
            <td><input type="text" name="agentCommission" value="<?=stripslashes($agentDetails["agentCommission"]); ?>" size="35" maxlength="6"></td>
        </tr>
        <!-------->
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Commission package</strong>(As Distributor)</font></td>
          <td><select name="commPackageAnD" id="commPackageAnD">
            <option value="001">Fixed Amount</option>
            <option value="002">Percentage of Transaction Amount</option>
            <option value="003">Percentage of Fee</option>
			<option value="004">None</option>
          </select><script language="JavaScript">
     			SelectOption(document.forms[0].commPackageAnD, "<? echo $agentDetails["commPackageAnDDist"]; ?>");
      	  </script>
            </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Commission Percentage/Fee</strong>(As Distributor)</font></td>
            <td><input type="text" name="CommissionAnD" value="<?=stripslashes($agentDetails["commAnDDist"]); ?>" size="35" maxlength="6"></td>
        </tr>
		
          <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?> 
			<tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency</strong>(As Agent)</font><font color="red">*</font></td>
            <td>
              <select name="settlementCurrencyAsAgent" id="settlementCurrencyAsAgent">
			  <option value="">-- Select Currency --</option>
			  <?
				$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		
				while($rstRow = mysql_fetch_array($nResult))
				{
					$country = $rstRow["country"];
					$currency = $rstRow["currencyName"];	
					$description = $rstRow["description"];
					$erID  = $rstRow["cID"];							
					
				 echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
				}
			  ?>				
				</select>
              </select>
              <script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsAgent, "<?=$strAsAgent?>");</script>
            </td>
          </tr>
			<tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency</strong>(As Distributor)</font><font color="red">*</font></td>
            <td>
              <select name="settlementCurrencyAsDistributor" id="settlementCurrencyAsDistributor">
			  <option value="">-- Select Currency --</option>
			  <?
				$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
				$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		
				while($rstRow = mysql_fetch_array($nResult))
				{
					$country = $rstRow["country"];
					$currency = $rstRow["currencyName"];	
					$description = $rstRow["description"];
					$erID  = $rstRow["cID"];							
					
				 	echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
				}
			  ?>				
				</select>
              </select>
              <script language="JavaScript">SelectOption(document.forms[0].settlementCurrencyAsDistributor, "<?=$strAsDistributor?>");</script>
            </td>
          </tr>
		  <? }?>
		
        <!-------------->
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Status</strong></font></td>
            <td><input type="radio" name="agentStatus" value="New" <? if ($agentDetails["agentStatus"] == "" || $agentDetails["agentStatus"] == "New") echo "checked"; ?>>New <input type="radio" name="agentStatus" value="Active" <? if ($agentDetails["agentStatus"] == "Active") echo "checked"; ?>>Active <input type="radio" name="agentStatus" value="Disabled" <? if ($agentDetails["agentStatus"] == "Disabled") echo "checked"; ?>>Disabled <input type="radio" name="agentStatus" value="Suspended" <? if ($agentDetails["agentStatus"] == "Suspended") echo "checked"; ?>>Suspended</td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Correspondent</strong></font></td>
          <td><select name="correspondent" id="correspondent">
                <option value="Y" selected>Both Agent and Distributor</option><!--Can distribute transactions-->
          	</select>
          	<script language="JavaScript">SelectOption(document.forms[0].correspondent, "<? echo $agentDetails["isCorrespondent"]; ?>");
          	</script>
          </td>       
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Select country to Assign distribution/Send money*</strong></font></td>
          <td>Hold Ctrl key for multiple selection<br>
            <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
             <?
            
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
	                	
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
               
					//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
              <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($agentDetails["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>>
              <?=$countires[$i]["countryName"]; ?>
              </OPTION>
              <?
					}
				?>
              </SELECT>
          </td></tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Company Logo </strong></font></td>
          <td><input type="file" name="logo"></td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Authorized for the services</strong></font></td>
          <td>
		    Hold Ctrl key for multiple selection<br>
		    <SELECT name="authorizedFor[]" size="4" multiple style="font-family:verdana; font-size: 11px" >
            <?
			if(trim($agentDetails["authorizedFor"]) == "")
			{?>
				<option value="Pick up" selected>Pick up</option>
			<?
			}
			else
			{
			?>
			<option value="Pick up" <? echo (strstr($agentDetails["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick up</option>
            <?
			}
			?>
			<option value="Bank Transfer" <? echo (strstr($agentDetails["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank Transfer</option>
			<option value="Home Delivery" <? echo (strstr($agentDetails["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home Delivery</option>
          </SELECT>
		  </td>
        </tr>
		   
        <!--
					Start of code @ticket #3497 point 4
					The at source and at end of month options already exists for agent added for AnD 
					-->	 					
 					<? 
 					if(CONFIG_AGENT_PAYMENT_MODE == "1"){ 
 					?>
						<tr bgcolor="#ededed"> 
            	<td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            	<td>
            		<select name="agentPaymentMode" id="agentPaymentMode">
              	<? 
              	if(CONFIG_PAYMENT_MODE_OPTION == "1"){ 
              	?>
              		<option value="exclude" <? echo ($agentDetails["paymentMode"] == "exclude" ? "selected"  : "")?>>At Source</option>
                <? 
                }else{ 
                ?>
	                <option value="include" <? echo ($agentDetails["paymentMode"] == "include" ? "selected"  : "")?>>At End Of Month</option>
	                <option value="exclude" <? echo ($agentDetails["paymentMode"] == "exclude" ? "selected"  : "")?>>At Source</option>
               	<? 
               	} 
               	?>
              	</select>
              	<script language="JavaScript">
         					SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
              	</script>
            	</td>
          	</tr>
          <? 
          }else{ 
          ?>
          	<tr bgcolor="#ededed"> 
	            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
	            <td>
	            	<select name="agentPaymentMode" id="agentPaymentMode">
		            	<option value="include" <? echo ($agentDetails["paymentMode"] == "include" ? "selected"  : "")?>>At End Of Month</option>  
    						</select>
              	<script language="JavaScript">
         					SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
                </script>
            	</td>
          	</tr>
          
          <? 
          } 
          ?>
          <!--
          End of code @ticket #3497 
          -->  
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Access From IP</strong></font></td>
          <td>For multiple please separate by comma.
              <input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255" value="<?=stripslashes($agentDetails["accessFromIP"]); ?>"></td>
        </tr>
		  <!--  #4996 point 2 AMB upload document functionality-->
		  <? if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1") { ?> 
		  <tr bgcolor="#ededed"> 
           <td width="37%"align="right"><font color="#005b90"><b>Upload Documents&nbsp;</b></font></td>
           	<td width="37%"><input type="checkbox" name="uploadImage" value="Y" <?=($_SESSION["uploadImage"]=="Y")?'checked="checked"':''?>>
           		<input type="hidden" name="mode" value="Add">
           		</td>
           				</tr>
         <? 
		    }
		 ?>
               <?
         if(CONFIG_DEFAULT_DISTRIBUTOR == '1')
         {
         	?>

         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Default Distributor</strong></font>
					</td>
					<td> 
						<select name="defaultDistrib" id="defaultDistrib">
              			<option value="N" <? echo($agentDetails["defaultDistrib"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($agentDetails["defaultDistrib"]=="Y" ? "selected"  : "")?>>Yes</option>	
						</select>
					</td>
				</tr>
        <?
         	}
         	?> 
   
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="hidden" name="usern" value="<?=stripslashes($agentDetails["username"]); ?>">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>