<?php

session_start();

include ("../include/config.php");
include ("security.php");

$filters = "";
$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */
if(!empty($_REQUEST["date_report"]))
{
	$date_report	= $_REQUEST["date_report"];
	$queryDated = " and dated <= '$date_report' ";
	$qrCreationDate=" and created <= '$date_report 23:59:59'";	
}
$exportType = $_REQUEST["exportType"];

	$curr_exch_acc = '';
	if(defined("CONFIG_CURRENCY_EXCHANGE_ACCOUNT"))
		$curr_exch_acc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
	// get total GBP of stock amount	
	$total_stock_amount = number_format(getTotalStockAmount(),2,'.',',');
	
if($exportType!=""){
	
  $strAccountChart = "SELECT 
							   id,
							   accountName,
							   accountNumber,
							   description,
							   currency 
						  FROM 
							 accounts_chart
						  WHERE 
						  	status = 'AC'
						  ";
			       //  debug($strAccountChart);
					$accountChartRs = selectMultiRecords($strAccountChart);
}
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "xls")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=ChartAccount-".time().".xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strNumColumnStart = "<td style='mso-number-format:\"\@\"' align='right'>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
		$strColspanStart = "<td colspan='5'>";
		$str3ColspanStart = "<td colspan='3'>";
		$str2ColspanStart = "<td colspan='2'>";
		$strColspanEnd = "</td>";
	}
	elseif($exportType == "csv")
	{
		header("Content-Disposition: attachment; filename=ChartAccount-".time().".csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColspanStart = "<td colspan='5'>";
		$strColspanEnd = "</td>";
		$strColumnStart = "<td>";
		$strNumColumnStart= "<td style='mso-number-format:\"\@\"' align='right'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Export Chart Of Account </title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
	$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;
	
	$strFullHtml .= $strRowStart;

	$strFullHtml .= $strColumnStart.$strBoldStart."A/C".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."CONTROL NAME".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."CUR".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Dr [Operational]".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Cr [Operational]".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Dr [Foreign]".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Cr [Foreign]".$strBoldClose.$strColumnEnd;

			
	$strFullHtml .= $strRowEnd;
	
	$strFullHtml .= $strRowStart;

	for($i=0;$i < count($accountChartRs);$i++)
	{
		 $strAccount = "SELECT 
									   id,
									   accounNumber,
									   accountName,
									   currency,
									   balanceType,
									   balance  
								  FROM 
									 accounts
								  WHERE 
									status = 'AC' AND
									accounType = '".$accountChartRs[$i]["accountNumber"]."' ".$qrCreationDate."";
			        // debug($strAccount);
					 $accountRs = selectMultiRecords($strAccount);
		// Data Rows Starts
		$strFullHtml .= $strColumnStart.$strBoldStart.$accountChartRs[$i]["accountNumber"].$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart.$accountChartRs[$i]["accountName"].$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart.$accountChartRs[$i]["currency"].$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strColumnStart."".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		
		if(count($accountRs) > 0 ) { 
			/*$strFullHtml .= $strRowStart;
			$strFullHtml .= $strColspanStart."".$strColspanEnd;
			$strFullHtml .= $strRowEnd;*/
			
			for($k=0; $k < count($accountRs); $k++){ 
				$crBalance	= "0";
				$drBalance 	= "0";
				$crBalance1 = "0";
				$drBalance1 = "0";
				
				$closingBalance = selectFrom("SELECT 
													id,
													closing_balance,
													dated,
													accountNumber  
												FROM 
													".TBL_ACCOUNT_SUMMARY." 
												WHERE
													accountNumber = '".$accountRs[$k]["accounNumber"]."' AND
													dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
													WHERE accountNumber = '".$accountRs[$k]["accounNumber"]."' ".$queryDated.")
												 ");
			
				/*
				If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
				If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
				*/
				/*if(strstr($closingBalance["closing_balance"],'-'))
				  $drBalance = $closingBalance["closing_balance"];
				 else
				  $crBalance = $closingBalance["closing_balance"];*/
				if($accountRs[$k]['balanceType'] == 'Dr')
					$drBalance =$closingBalance["closing_balance"] - $accountRs[$k]['balance'];
				elseif($accountRs[$k]['balanceType'] == 'Cr')
					$crBalance =$closingBalance["closing_balance"] + $accountRs[$k]['balance'];	  
				 
				if($accountRs[$k]["currency"] == "GBP"){	  
					if($drBalance < 0)
						$drSum += $drBalance;
					if($drBalance > 0)	
						$crSum += $drBalance;
					if($crBalance < 0)
						$drSum += $crBalance;
					if($crBalance > 0)	
						$crSum += $crBalance;	
				}else{ 
					if($drBalance < 0)
						$drSumF += $drBalance;
					if($drBalance > 0)	
						$crSumF += $drBalance;
					if($crBalance < 0)
						$drSumF += $crBalance;
					if($crBalance > 0)	
						$crSumF += $crBalance;
					
					$strCurrency=$accountRs[$k]["currency"];	
					$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
					$buying_rate_arr_id = selectFrom($cur_sql); 
					$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
					//debug($cur_sql);
					$buying_rate_arr = selectFrom($cur_sql); 
					//debug($buying_rate_arr);
					$buying_rate=$buying_rate_arr[0];
				}
				$show_total_amount = '';
				if($accountRs[$k]["accounNumber"] == $curr_exch_acc)
					$show_total_amount = '<strong>Stock='.$total_stock_amount.'<strong>';
					
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColumnStart.$accountRs[$k]["accounNumber"].$strColumnEnd;
				$strFullHtml .= $strColumnStart.$accountRs[$k]["accountName"].$show_total_amount.$strColumnEnd;
				$strFullHtml .= $strColumnStart.$accountRs[$k]["currency"].$strColumnEnd;
				
				
				if($accountRs[$k]["currency"] == "GBP"){
					if($drBalance<0 ){
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance), 2, ".", "").$strColumnEnd;
					}
					else if($crBalance<0){
						$strFullHtml .= $strNumColumnStart.number_format($crBalance, 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}
				}else{
					if($drBalance<0 ){
						$drBalance1=$drBalance/$buying_rate;
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance1), 2, ".", "").$strColumnEnd;
					}
					else if($crBalance<0){
						$crBalance1=$crBalance/$buying_rate;
						$strFullHtml .= $strNumColumnStart.number_format($crBalance1, 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}
					 
				}				
				
				/*if($drBalance<0 ){
					$strFullHtml .= $strColumnStart.number_format($drBalance, 2, ".", "").$strColumnEnd;
					//echo number_format(abs($drBalance), 2, ".", "");
				}
				else if($crBalance<0){
					$strFullHtml .= $strColumnStart.number_format($crBalance, 2, ".", "").$strColumnEnd;
					//echo number_format(abs($crBalance), 2, ".", "");
				}
				else {
					$strFullHtml .= $strColumnStart.$strColumnEnd;
					//echo number_format(abs($crBalance), 2, ".", "");
				}	*/
				
				
				
				if($accountRs[$k]["currency"] == "GBP"){
					if($drBalance>0 ){
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance), 2, ".", "").$strColumnEnd;
					}
					else if($drBalance == 0 && $crBalance>0 ){
						$strFullHtml .= $strNumColumnStart.number_format($crBalance, 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}
				}else{
					if($drBalance>0 ){
						$drBalance1=$drBalance/$buying_rate;
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance1), 2, ".", "").$strColumnEnd;
					}
					else if($drBalance == 0 && $crBalance>0 ){
						$crBalance1=$crBalance/$buying_rate;
						$strFullHtml .= $strNumColumnStart.number_format($crBalance1, 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}
					 if($drBalance < 0)
						$drSum += $drBalance1;
					 if($drBalance > 0)	
						$crSum += $drBalance1;
					 if($crBalance < 0)
						$drSum += $crBalance1;
					 if($crBalance > 0)	
						$crSum += $crBalance1;
				}	
				
				if($accountRs[$k]["currency"] != "GBP"){
					if($drBalance<0 ){
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance), 2, ".", "").$strColumnEnd;
					}
					else if($crBalance<0){
						$strFullHtml .= $strNumColumnStart.number_format(abs($crBalance), 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}					

				}

				if($accountRs[$k]["currency"] != "GBP"){
					if($drBalance>0 ){
						$strFullHtml .= $strNumColumnStart.number_format(abs($drBalance), 2, ".", "").$strColumnEnd;
					}
					else if($drBalance == 0 && $crBalance>0 ){
						$strFullHtml .= $strNumColumnStart.number_format($crBalance, 2, ".", "").$strColumnEnd;
					}else {
						$strFullHtml .= $strColumnStart."0.00".$strColumnEnd;
						//echo number_format(abs($crBalance), 2, ".", "");
					}
				}				
				
				/*if($drBalance>0 ){
					$strFullHtml .= $strColumnStart.number_format($drBalance, 2, ".", "").$strColumnEnd;
					//echo number_format(abs($drBalance), 2, ".", "");
				}
				else if($drBalance == 0 && $crBalance>0 ){
					$strFullHtml .= $strColumnStart.number_format($crBalance, 2, ".", "").$strColumnEnd;
					//echo number_format($crBalance, 2, ".", "");
					
				}
				else {
					$strFullHtml .= $strColumnStart.$strColumnEnd;
					//echo number_format(abs($crBalance), 2, ".", "");
				}	*/
				
				$strFullHtml .= $strRowEnd;
			} // end for inner loop
		
		} // end if condition
				/*$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColspanStart."".$strColspanEnd;
				$strFullHtml .= $strRowEnd;*/
// Data Row Ends
 } // end out for loop

// Breakline Starts
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColspanStart."".$strColspanEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strRowEnd;
				
				
				$strFullHtml .= $strRowStart;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strColumnStart.$strBoldStart." TOTAL".$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format($drSum,2, ".", "").$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format($crSum,2, ".", "").$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format($drSumF,2, ".", "").$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strNumColumnStart.$strBoldStart.number_format($crSumF,2, ".", "").$strBoldClose.$strColumnEnd;
				$strFullHtml .= $strRowEnd;
// Breakline Ends

// Total Runnig Row starts
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>
