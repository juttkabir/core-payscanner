<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];

$limit=20;
if ($offset == "") {
	$offset = 0;
}
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}

$nxt = $offset + $limit;
$prv = $offset - $limit;

$today_date = date('Y-m-d');

$userName	= trim($_REQUEST["userName"]);
$userType	= $_REQUEST["userType"];
$IDType		= $_REQUEST["IDType"];
$alertType	= $_REQUEST["alertType"];
$orderBy	= $_REQUEST["orderBy"];

$strWhrClause = "";

if($_REQUEST["Submit"] == "Search")
{
	$strAlertQuery_1	= " SELECT 
								MAX(day) 
							FROM 
								".TBL_EMAIL_ALERT." 
							WHERE
								id_type_id	= '".$IDType."'	AND
								userType	= '".$userType."'	AND
								isEnable 	= 'Y' 				AND 
								alertType	= '".$alertType."'
							";

	$arrAlertData_1 = selectFrom($strAlertQuery_1);

	//print_r($arrAlertData_1);

	$intDays	= $arrAlertData_1[0];






	if($alertType == "Future Expiry")
	{
		if($intDays == ''){

			$commingExpiryDate = date("Y-m-d",strtotime($today_date . ' + ' . $intDays[0]. ' days'));
			$strWhrClause .= " AND (uid.expiry_date >= '".$commingExpiryDate."')";
			//debug($commingExpiryDate);
		}else {


			$commingExpiryDate = date("Y-m-d");
			$strWhrClause .= " AND (uid.expiry_date >= '".$commingExpiryDate."')";
			//	debug($commingExpiryDate);

		}

		$strWhrClauseName .="AND (uid.expiry_date > '".$today_date."')";



		$commingExpiryDate = date("Y-m-d",strtotime($intDays." day"));
		$strWhrClause .= " AND (uid.expiry_date > '".$today_date."')";

	}
	elseif($alertType == "Expired Ids")
	{

		$strWhrClauseName .="AND (uid.expiry_date < '".$today_date."')";
		$expiredDate = date("Y-m-d",strtotime("-".$intDays." day"));
		$strWhrClause .= " AND (uid.expiry_date <= '".$today_date."' )";

		//$strWhrClause .= " AND (uid.expiry_date <= '".$today_date."')";
	}

	if(!empty($userName))
		$strWhrClause .= " AND (c.firstName LIKE '%".$userName."%' OR c.middleName LIKE '%".$userName."%' OR c.lastName LIKE '%".$userName."%' OR c.customerName LIKE '%".$userName."%')";
	if(!empty($alertType))
		$strWhrClause .= " AND em.alertType = '".$alertType."'";

	if(!empty($IDType))
	{
		$strWhrClause .= " AND em.id_type_id = '".$IDType."'";

		if($IDType != '-1')
			$strWhrClause .= " AND uid.id_type_id = em.id_type_id";
	}
	else
		$strWhrClause .= "";

	if($IDType != '-1'){
		if($agentType == "SUPA" || $agentType == "SUBA"){
			$strIdExpired	= "SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							 c.agentID=$agentID  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							".$strWhrClause."
							
						";

		}

		else {

			$strIdExpired = "SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							  
							uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							" . $strWhrClause . "
							
						";
		}




	}else if($IDType == "-1"){
		if($agentType == "SUPA" || $agentType == "SUBA"){
			$strIdExpired	= " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							c.agentID=$agentID  
							AND c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = '".$alertType."'
							".$strWhrClauseName."
						";



		}
		else {

			$strIdExpired = " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							 c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = '" . $alertType . "'
							" . $strWhrClauseName . "
						";
		}


	}

	if(!empty($userName))
		$strIdExpired .= " AND (c.firstName LIKE '%".$userName."%' OR c.middleName LIKE '%".$userName."%' OR c.lastName LIKE '%".$userName."%' OR c.customerName LIKE '%".$userName."%')";
	if(!empty($orderBy))
		$strIdExpired .= " ORDER BY ".$orderBy;
	else
		$strIdExpired .= " ORDER BY expiry_date";

	$strIdExpiredCnt	= $strIdExpired;
	$strIdExpired 		.= " LIMIT $offset , $limit";

	$arrIdExpiredData 	= selectMultiRecords($strIdExpired);
	$allCount = count(selectMultiRecords($strIdExpiredCnt));
}
if($_REQUEST["Submit"] != "Search") {
	$commingExpiryDate = date("Y-m-d");

	if ($agentType == "SUPA" || $agentType == "SUBA") {

		$strIdExpired = " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							c.agentID=$agentID  
							AND c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = 'Expired Ids'
							AND (uid.expiry_date < '" . $commingExpiryDate . "');
						";

		$strIdFutureExpiry = " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							c.agentID=$agentID  
							AND c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = 'Future Expiry'
							AND (uid.expiry_date >= '" . $commingExpiryDate . "')						";


	} else {

		$strIdExpired = " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							 c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = 'Expired Ids'
							AND (uid.expiry_date < '" . $commingExpiryDate . "')						";


		$strIdFutureExpiry = " SELECT
							uid.id_type_id,
							uid.user_id, 
							uid.expiry_date,
							
							em.alertType,
							em.userType,
							em.report,
							em.message,
							em.id_type_id,
							c.customerID, 					
							c.firstName,
							c.lastName,
							c.middleName,
							c.accountName,
							c.customerName,
							c.customerType
						FROM 
						customer as c 
						INNER JOIN user_id_types AS uid ON uid.user_id = c.customerID
						INNER JOIN emailAlert AS em ON em.id_type_id = uid.id_type_id	
						WHERE 
							 c.customerID	= uid.user_id  
							AND uid.user_type	= 'C' 
							AND em.userType		= 'Customer'
							AND em.isEnable 	= 'Y'  
							AND em.alertType    = 'Future Expiry'
							AND (uid.expiry_date >= '" . $commingExpiryDate . "')						";

	}
	$arrIdExpiredData = selectMultiRecords($strIdExpired);
	$arrIdFutureExpiredData = selectMultiRecords($strIdFutureExpiry);
//	echo $strIdFutureExpiry;
}

?>

<html>
<head>
	<title>Alert Controller</title>
	<script language="javascript" src="../javascript/functions.js"></script>
	<script language="javascript" src="./javascript/functions.js"></script>
	<script type="text/javascript" src="./javascript/jquery.js"></script>
	<script type="text/javascript" src="./javascript/jquery.validate.js"></script>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
		function SelectOption(OptionListName, ListVal)
		{
			for (i=0; i < OptionListName.length; i++)
			{
				if (OptionListName.options[i].value == ListVal)
				{
					OptionListName.selectedIndex = i;
					break;
				}
			}
		}

		$(document).ready(function(){

			$.validator.addMethod("noSpecialChars", function(value, element){
				return this.optional(element) || /^[a-zA-Z -]+$/i.test(value);
			});

			$("#searchForm").validate({
				rules: {
					userName: "noSpecialChars",
					alertType: "required",
					IDType: "required",
					userType: "required"
				},
				messages: {
					userName: "<br>Name must contain only letters.",
					alertType: "<br/>Please select Alert Type.",
					IDType: "<br/>Please select ID Type.",
					userType: "<br/>Please select User Type."
				}
			});

			var a=0;
			// No continuous spaces
			$('input.nospace').keydown(function(e) {
				if(e.keyCode == 32){
					a++;
					if(a > 1)
						return false;
				}
				else
					a=0;
			});

			$("#printBtn").click(function(){
				$("#btnRow").hide();
				$(".actionCol").hide();
				$("#searchTable").hide()
				print();
			});
		});
	</script>
	<style type="text/css">
		.style2 {
			color: #6699CC;
			font-weight: bold;
		}
		#searchTable
		{
			background-color:#ededed;
			border:1px solid #000099;
		}
		#searchTable tr td
		{
			border-top:2px solid #FFFFFF;
		}
		label.error {
			color: red;
			padding-left: 5px;
			font-weight:bold;
		}
	</style>
</head>
<body>
<div>
	<form id="searchForm" action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<table width="50%" id="searchTable" align="center" border="0" cellpadding="5">
			<tr>
				<th align="left" nowrap colspan="4" bgcolor="#6699cc"><span class="tab-u">Search Alert</span></th>
			</tr>
			<tr>
				<td align="right">User Name</td>
				<td><input type="text" id="userName" class="nospace" name="userName" value="<?=$userName?>" /></td>
				<td align="right">User Type <font color="#ff0000">*</font></td>
				<td><select id="userType" name="userType" style="font-family:verdana; font-size: 11px">
						<!--<option value="">- Select -</option>
                        <option value="All">All</option>-->
						<option value="Customer">Customer</option>
						<!--<option value="Beneficiary">Beneficiary</option>
                        <option value="Agent">Agent</option>-->
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].userType, "<?=$userType?>");</script>
				</td>
			</tr>
			<tr>
				<td align="right">ID Type <font color="#ff0000">*</font></td>
				<td><select id="IDType" name="IDType" style="font-family:verdana; font-size: 11px">
						<option value="">- Select -</option>
						<option value="-1">All</option>
						<?php
						if(CONFIG_DB_ID_TYPES == "1")
						{
							$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
							foreach($arrIDTypeData as $key=>$val)
							{
								?>
								<option value="<?=$val["id"];?>"><?=$val["title"];?></option>
								<?php
							}
						}
						else
						{?>
							<option value="Driving License">Driving License</option>
							<option value="Passport">Passport</option>
							<option value="ID Card">ID Card</option>
							<option value="Other ID">Other ID</option>
							<option value="MSB">MSB Expiry</option>
							<?php
						}
						?>
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].IDType, "<?=$IDType?>");</script>
				</td>
				<td align="right">Alert Type <font color="#ff0000">*</font></td>
				<td><select id="alertType" name="alertType" style="font-family:verdana; font-size: 11px">
						<option value="">- Select -</option>
						<option value="Expired Ids"  >Expired Ids</option>
						<option value="Future Expiry" >Future Expiry</option>
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].alertType, "<?=$alertType?>");</script>
				</td>

			</tr>
			<tr>
				<td colspan="4" align="center">
					<input type="hidden" name="alertID" value="<?=$_REQUEST["alertID"]?>" />
					<input type="submit" name="Submit" value="Search">
				</td>
			</tr>
		</table>
	</form>

	<fieldset style="margin:10px 10px; border:1px solid #000099">
		<!--<legend class="style2">Alert Status </legend>-->
		<br>
		<form action="exportAlertController.php" method="post">
			<?php
			if(count($arrIdExpiredData) > 0)
			{
				$strQueryStrint = "&userName=".$userName."&userType=".$userType."&IDType=".$IDType."&alertType=".$alertType."&Submit=Search";

				if($_REQUEST["Submit"] == "Search"){
?>
				<table align="center" width="970" cellpadding="2" cellspacing="0" border="0">
					<tr>
						<td>Showing <strong><?php print ($offset+1).' - '.($offset+count($arrIdExpiredData));?></strong> of <strong><?=$allCount; ?></strong></td>
						<?php if ($prv >= 0)
						{?>
							<td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=0".$strQueryStrint;?>"><font color="#005b90">First</font></a></td>
							<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv".$strQueryStrint;?>"><font color="#005b90">Previous</font></a></td>
							<?php
						}
						if ( ($nxt > 0) && ($nxt < $allCount) )
						{
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
							?>
							<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt".$strQueryStrint;?>"><font color="#005b90">Next</font></a>&nbsp;</td>
							<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$strQueryStrint;?>"><font color="#005b90">Last</font></a>&nbsp;</td>
							<?php
						} ?>
					</tr>
				</table>
		<?php } else	{?>
					<table align="center" width="970" cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td>Showing <strong><?php print ($offset+1).' - '.($offset+count($arrIdExpiredData)+count($arrIdFutureExpiredData));?></strong> of <strong><?=($offset+count($arrIdExpiredData)+count($arrIdFutureExpiredData)); ?></strong></td>
							<?php if ($prv >= 0)
							{?>
								<td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=0".$strQueryStrint;?>"><font color="#005b90">First</font></a></td>
								<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv".$strQueryStrint;?>"><font color="#005b90">Previous</font></a></td>
								<?php
							}
							if ( ($nxt > 0) && ($nxt < $allCount) )
							{
								$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
								<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt".$strQueryStrint;?>"><font color="#005b90">Next</font></a>&nbsp;</td>
								<td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$strQueryStrint;?>"><font color="#005b90">Last</font></a>&nbsp;</td>
								<?php
							} ?>
						</tr>
					</table>
					<?php } ?>
				<table width="970" border="0" align="center" cellpadding="5" cellspacing="1">
					<tr>
						<td width="170" bgcolor="#CCEEFF" ><strong>User Name</strong></td>
						<td width="130" bgcolor="#CCEEFF" ><strong>Reference Number</strong></td>
						<td width="100" bgcolor="#CCEEFF" ><strong>User Type</strong></td>
						<td width="80" bgcolor="#CCEEFF"  ><strong>ID Type</strong></td>
						<td width="100" bgcolor="#CCEEFF" ><strong>Alert Type</strong></td>
						<td width="100" bgcolor="#CCEEFF" ><strong>Expiry date </strong></td>
						<td width="240" bgcolor="#CCEEFF" ><strong>Message</strong></td>
						<td width="50" bgcolor="#CCEEFF" class="actionCol">&nbsp;</td>
					</tr>
					<?
					foreach($arrIdExpiredData as $key=>$val)
					{
						if($val["customerType"] == "company")
							$strEditPage = "add-company.php?from=alertPage";
						else
							$strEditPage = "add-customer.php?pageID=alertPage";

						$strIDtypeName = selectFrom("SELECT title FROM ".TBL_ID_TYPES." WHERE id = '".$val["id_type_id"]."'");

						?>

						<tr>
							<td bgcolor="#DFE6EA" ><?=($val["firstName"]." ".$val["middleName"]." ".$val["lastName"]);?></td>
							<td bgcolor="#DFE6EA" ><?=$val["accountName"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["userType"];?></td>
							<td bgcolor="#DFE6EA" ><?=$strIDtypeName["title"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["alertType"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["expiry_date"];?></td>
							<?php
								$strMessage = "select message from emailAlert";
								if(date('Y-m-d') >= $val['expiry_date']){
									$strMessage .= " where id = 3 and isEnable = 'Y'";
									$queryMessage = selectFrom($strMessage);
									$message = $queryMessage["message"];
								} else {
									$strMessage .= " where id = 4 and isEnable = 'Y'";
									$queryMessage = selectFrom($strMessage);
									$day = selectFrom("select DATEDIFF('".$val['expiry_date']."', SYSDATE()) AS day");
									$message = str_replace("{day}", $day["day"], $queryMessage["message"]);
								}
								echo '<td bgcolor="#DFE6EA" >'.$message.'</td>';
							?>
							<td bgcolor="#DFE6EA" class="actionCol"><a <a href="<?=$strEditPage?>&customerID=<?=$val["customerID"];?>&from=search-edit-cust.php" target="_blank" class="style2">Edit</a></td>

						</tr>
						<?php
					}
					//////////////////////////////////////////////////
					foreach($arrIdFutureExpiredData as $key=>$val)
					{
						if($val["customerType"] == "company")
							$strEditPage = "add-company.php?from=alertPage";
						else
							$strEditPage = "add-customer.php?pageID=alertPage";

						$strIDtypeName = selectFrom("SELECT title FROM ".TBL_ID_TYPES." WHERE id = '".$val["id_type_id"]."'");

						?>

						<tr>
							<td bgcolor="#DFE6EA" ><?=($val["firstName"]." ".$val["middleName"]." ".$val["lastName"]);?></td>
							<td bgcolor="#DFE6EA" ><?=$val["accountName"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["userType"];?></td>
							<td bgcolor="#DFE6EA" ><?=$strIDtypeName["title"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["alertType"];?></td>
							<td bgcolor="#DFE6EA" ><?=$val["expiry_date"];?></td>
							<?php
							$strMessage = "select message from emailAlert";
							if(date('Y-m-d') >= $val['expiry_date']){
								$strMessage .= " where id = 3 and isEnable = 'Y'";
								$queryMessage = selectFrom($strMessage);
								$message = $queryMessage["message"];
							} else {
								$strMessage .= " where id = 4 and isEnable = 'Y'";
								$queryMessage = selectFrom($strMessage);
								$day = selectFrom("select DATEDIFF('".$val['expiry_date']."', SYSDATE()) AS day");
								$message = str_replace("{day}", $day["day"], $queryMessage["message"]);
							}
							echo '<td bgcolor="#DFE6EA" >'.$message.'</td>';
							?>
							<td bgcolor="#DFE6EA" class="actionCol"><a <a href="<?=$strEditPage?>&customerID=<?=$val["customerID"];?>&from=search-edit-cust.php" target="_blank" class="style2">Edit</a></td>

						</tr>
						<?php
					}
					//////////////////////////////////////////////////////////////////////////////////////
					?>
					<tr id="btnRow">
						<td bgcolor="#DFE6EA" colspan="4" align="right">
							<input type="button" id="printBtn" name="Submit" value="Print">
						</td>
						<td bgcolor="#DFE6EA" colspan="4" align="left">
							<!--	<input type="hidden" name="exportQuery" value="<? //$strIdExpiredCnt?>" />  --><input type="hidden" name="exportQuery" value="<? echo base64_encode($strIdExpiredCnt);?>" />
							<select name="exportType">
								<option value="XLS">XLS</option>
								<option value="CSV">CSV</option>
								<option value="HTML">HTML</option>
							</select>
							<input type="submit" name="Submit" value="Export All">
						</td>
					</tr>
				</table>
				<?php
			}
			else
			{ ?>
				<div align="center" style="background-color:#DFE6EA"><strong>No Data Found.</strong></div>
				<?php
			}
			?>
		</form>
		<br>
	</fieldset>
</div>
</body>
</html>