<?
/**
 * @package compliance
 * @subpackage Manage User Profiling
 * Short description.
 * This File is used to manage the compliance rule of user profiling
 */	
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
if($_REQUEST["oper"] == "del")
{
	if(!empty($_REQUEST["id"]))
	{
		$id	= $_REQUEST["id"];
		$strDeleteQuery	= "DELETE FROM compliance_user_profile WHERE id='".$id."'";
			mysql_query($strDeleteQuery);
		
	}
}
//debug ($agentType);

$sql = "SELECT * FROM compliance_user_profile WHERE 1 ";
if($_POST["Submit"] =="Search")
	{
		
			$customerType	= $_REQUEST["customerType"];
			$monitoringCriteria	= $_REQUEST["monitoringCriteria"];
			$riskType	= $_REQUEST["riskType"];
			$status	  	= $_REQUEST["status"];
			$whrClause	= "";
	
			if(!empty($customerType))
			{
				//if($customerType == "All")
				//	$customerType = -1;
				$whrClause .= " AND customerType = '".$customerType."'";
			}
			if(!empty($monitoringCriteria))
				$whrClause .= " AND monitoringCriteria = '".$monitoringCriteria."'";
			if(!empty($riskType))
				$whrClause .= " AND riskType = '".$riskType."'";
			if (!empty($status))
				$whrClause .= " AND status = '".$status."'";
				
		}
$parentID = $_SESSION["loggedUserData"]["userID"];
$user=$agentType;
//debug ($user);
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;


//if($_POST["Submit"] == "Search"){
//$queryCnt .= $dated;
if(!empty($whrClause) && !empty($sql))
			$sql	= $sql." ".$whrClause;
		
			
$sqlRule = $sql."  LIMIT $offset , $limit";
//debug($queryCust);
if(!empty($sql)){
$contentsRule = selectMultiRecords($sqlRule);
$countSql = selectMultiRecords($sql);
$intRuleCount = count($countSql);
}

?>

<html>
<head>
	<title>Search Compliance User Profile Rule</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style4 {color: #005b90; font-weight: bold; }
#searchTable
{
	background-color:#ededed;
	border:1px solid #000000;
}
#searchTable tr td
{
	border-top:1px solid #FFFFFF;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body >
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 
  <tr>
    <td align="center"><br>
    
	<form name="frmSearch" id="frmSearch" action="" method="post">
	<table width="100" border="0" align="center">
		<tr>
			<td>
				<table id="searchTable" align="center" border="0" cellpadding="5" bordercolor="#666666">
					<tr>
						<th align="left"colspan="4" bgcolor="#6699cc"><span class="tab-u">Search Rule</span></th>
					</tr>
					<tr>
						<td nowrap align="right">Customer Type</td>
						<td><select name="customerType" id="customerType" >
						<option value="">- Select -</option>
						<?php
						foreach ($arrCustomerType as $key => $value){
						?>
							<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
						<?php } ?>
						</select>
						<script language="JavaScript">SelectOption(document.forms[0].customerType, "<?=$_REQUEST['customerType']; ?>");</script>
						</td>
						<td nowrap align="right">Monitoring Criteria</td>
						<td><select name="monitoringCriteria" id="monitoringCriteria" >
						<option value="">- Select -</option>
						<?php
						foreach ($arrMonitoringCriteria as $key => $value){
						?>
							<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
						<?php } ?>
						</select>
						<script language="JavaScript">SelectOption(document.forms[0].monitoringCriteria, "<?=$_REQUEST['monitoringCriteria']; ?>");</script>
						</td>
					</tr>
					<tr>
						<td nowrap align="right">Risk Type</td>
						<td><select name="riskType" id="riskType" >
						<option value="">- Select -</option>
							<?php
						foreach ($arrRiskType as $key => $value){
						?>
							<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
						<?php } ?>
						</select>	
						<script language="JavaScript">SelectOption(document.forms[0].riskType, "<?=$_REQUEST['riskType']; ?>");</script>
						</td>
						<td nowrap align="right">Status</td>
						<td><select id="status" name="status" style="WIDTH: 140px;HEIGHT: 18px; ">
								<option value="">- Select -</option>
								<option value="Enable">Enable</option>						
								<option value="Disable">Disable</option>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].status, "<?=$_REQUEST['status']; ?>");</script>
						</td>
					</tr>
					<tr>
						<td colspan="4" nowrap align="center">
							<input type="submit" name="Submit" value="Search" >
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		
	</table>
</form>
      <br>
	  <br>      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="" method="post" name="trans">

	    <tr id="rowHeader">
            <td height="25" nowrap bgcolor="333333">
<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
            <tr>
              <td><?php if (count($contentsRule) > 0) {;?>
      Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsRule));?></b> of
      <?=$intRuleCount; ?>
      <?php } ;?>
              </td>
              <?php if ($prv >= 0) { ?>
              <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&customerType=$customerType&Submit=Search&monitoringCriteria=$monitoringCriteria&riskType=".$riskType."&status=".$status;?>"><font color="#005b90">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&customerType=$customerType&Submit=Search&monitoringCriteria=$monitoringCriteria&riskType=".$riskType."&status=".$status;?>"><font color="#005b90">Previous</font></a> </td>
              <?php } ?>
              <?php 
					if ( ($nxt > 0) && ($nxt < $intRuleCount) ) {
						$alloffset = (ceil($intRuleCount / $limit) - 1) * $limit;
				?>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&customerType=$customerType&Submit=Search&monitoringCriteria=$monitoringCriteria&riskType=".$riskType."&status=".$status;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&customerType=$customerType&Submit=Search&monitoringCriteria=$monitoringCriteria&riskType=".$riskType."&status=".$status;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
              <?php } ?>
            </tr>
          </table></td>
        </tr>
		
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
				<td><span class="style1">Customer Type </span></td>
				<td><span class="style1">Monitoring Criteria</span></td>
				<td><span class="style1">Orignating Currency</span></td>
				<td><span class="style1">Destination Currency</span></td>
				<td><span class="style1">Amount Criteria</span></td>
				<td><span class="style1">Amount Value</span></td>
				<td><span class="style1">Period</span></td>
				<td><span class="style1">Apply On</span></td>
				<td><span class="style1">Points</span></td>
				<td><span class="style1">Risk Type</span></td>
				<td><span class="style1">Status</span></td>
				<td><span class="style1" id="editTitle">Edit</span></td>
				<td><span class="style1" id="deleteTitle">Delete</span></td>
			</tr>
		    <? for($i = 0; $i < count($contentsRule); $i++)
			{
				?>
				<tr bgcolor="#FFFFFF">
				<td width="200"><?php echo $contentsRule[$i]['customerType']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['monitoringCriteria']; ?></td>  
				<td width="200"><?php echo $contentsRule[$i]['orignatingCurrency']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['destinationCurrency']; ?></td> 
				<td width="200"><?php echo $contentsRule[$i]['amountCriteria']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['value']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['period']; ?></td>  
				<td width="200"><?php echo $contentsRule[$i]['applyOn']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['points']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['riskType']; ?></td>
				<td width="200"><?php echo $contentsRule[$i]['status']; ?></td>   
				<td width="125" align="center" id="editVlaue">
				  		  <a href="javascript:;" class="style3" onClick=" window.open('add-compliance-user-profiling.php?id=<?php echo $contentsRule[$i]['id']; ?>&oper=edit',null,'scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')">Edit</a> </td>
						  <td width="200" id="deleteVlaue"> <a href="manage-compliance-user-profiling.php?id=<?php echo $contentsRule[$i]['id']; ?>&oper=del" onClick="return confirm('Are you sure you want to delete?');"><strong><font color="##3366CC">Delete</font></strong></a></td>   
				  		
				</tr>
				<?php
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			 
			</tr>
			<tr bgcolor="#FFFFFF" id="rowFooter">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  
            <td align="center"> <input type="hidden" value="<?php echo addslashes($sql);?>" name="query">
			<input type="hidden" value="" name="transStatus">
			<input type="hidden" value="" name="DateType">
			<input type="button" value="Export to Excel" onClick="document.trans.action='export_complience_user_profile.php?act=export'; document.trans.submit();"></td><td align="center">
			<input type="button" name="Submit2" id="printBtn" value="Print" onClick="document.trans.action='export_complience_user_profile.php?act=print'; document.trans.submit();" />
            </td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
          </tr>
			 </table></td>
        </tr>
		</form>
      </table></td>
  </tr>
</table>
</body>
</html>
