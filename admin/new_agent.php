<?php
session_start();
/**
 * @package: Payex
 * @subpackage: Transactions
 * @Author: Aslam Shahid
 */
 
include ("../include/config.php");
include ("security.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript" src="./javascript/functions.js"></script>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<LINK href="images/interface_admin.css" type=text/css rel=stylesheet>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->


	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" >
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<!-- Include the above in your HEAD tag -->


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--  <script src="content/javascripts/jquery.js" type="text/javascript">.</script>-->
<!--  <script src="jquery.form.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery-ui-1.7.2.custom.min.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.scrollTo.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.serialScroll.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.validate.min.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.li-scroller.1.0.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.livequery.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/jquery.fancybox-1.2.6.pack.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/crossfader.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/swfobject.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/date.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/application.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/widget.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/forms.js" type="text/javascript">.</script>-->
<!--  <script src="content/javascripts/client.js" type="text/javascript">.</script>-->
<title>Add Introducer</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="GBP, EUR, USD, GBPEUR, GBPUSD,EURUSD foreign exchange specialists, foreign currency, foreign exchange brokers, foreign currencies, money transfer, forex, fx, transfer money abroad, dollars, euros, sterling, expats, online foreign exchange, currency brokers, overseas property, Corporate, business, Exchange rate, best rates of exchange, Currency converter, emigrating, currency market, money market, trading, import, export, margin, one off transfer, save money, forward contracts, spot, market order, rate watch" />
<link rel="alternate" type="application/rss+xml" title="Blog" href="/rss" />
<link href="content/stylesheets/application.css" rel="stylesheet" type="text/css" media="screen" />
<link href="content/stylesheets/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
<link href="content/stylesheets/jquery.fancybox-1.2.6.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
$(document).ready(function(){
    var options = { 
        //target:        '#output1',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
 
        // other available options: 
        ,url:       "add-agent-conf.php"         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //,clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 
 
    // bind form using 'ajaxForm' 

	 $('#registration').ajaxForm(options);
 });
 // pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    //alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
	$("#showMessage").html('');
	$("#showMessage").hide();
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
	//alert($('#error_flag').val() + 'submitted');
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    //alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.'); 
//	alert(responseText);
///alert(responseText);
	$("#showMessage").show();
	$("#showMessage").html(responseText);
//	alert("Introducer has been saved successfully.");
} 
</script>
<link rel="stylesheet" href="css/new-agent.css" />
</head>
<body>
 <div class="alert introducer">Add Introducer</div>
<div class="wrapper">
  <div class="full">
    <?php
          if(isset($_GET['userID']))
          {
              ?>
              <div class="alert alert-success">
                  <?php
                    $query=selectFrom("SELECT * from admin WHERE userID = ".$_GET['userID']);
                    $username=$query["username"];
                    $password=$query["password"];
                    echo "Introducer added successfully. The login name is $username and password is $password";
                  ?></div>
              <?php
          }
      ?>
<!--   <ul id="nav" class="tabs group">-->
      <!--<li class="first active">Welcome</li>-->
<!--	  <div class="alert alert-success">Add Introducer</div>-->
      <!--<li>Transaction Information</li>-->
<!--      <li class="last">Agreement</li>-->
<!--    </ul>-->
	
	
    <form id="registration" name="registration" action="new_agent-action.php?source=new_agent.php" method="post" enctype="multipart/form-data" style="background-color: #fff; border-color: #fff">
      <!--<fieldset class="group">
      <legend><strong>Create Transaction</strong> </legend>
      <p> Here you can provide any information prior to creating transaction. </p>
      <p> <b>You can download company policy document. For that </b><a href="#">click here</a></p>
      </fieldset>-->
      <fieldset class="group">
<!--      <legend><b>Add Introducer </b></legend>-->
      <p class="alert alert introducer"><em><b id="showMessage">This will be updated with notices returned by the system.</b></em> </p>
      <br>
      <div class="group"> </div>
      <fieldset class="individual group " style="width:900px; margin-right:-150px;background-color=#d4edda">
      <div class="fa fa-user "> </div>
      <legend>New Indivudual</legend>
      <div class="float">
		
<?php
	$arguments = array("flag"=>"populateUsers","selectedID"=>$_REQUEST["supAgentID"]);
	$populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN",$arguments,CONFIG_POPULATE_USERS_DROPDOWN);
	if(!empty($populateUser)){
		echo $populateUser;
	}
	else{
		$agents = selectMultiRecords("select userID,name,username, agentCompany,agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Branch Manager'");
		$selectOption = '<select class="required form-control" id="supAgentID" name="supAgentID">';
		$selectOption .= '<option value="">- Select Branch Manager -</option>';
		for ($i=0; $i < count($agents); $i++)
			$selectOption .= '<option value="'.$agents[$i]["userID"].'">'.$agents[$i]["name"]." [".$agents[$i]["username"].']</option>'; 
		$selectOption.='</select>';
		echo $selectOption;
	}
?>
<br>
<br>
	   <select class="required form-control" id="agentUserType" name="agentUserType"><option value="">Account Type</option>
			<option value="Introducer">Introducer</option>
			<option value="Management Company">Management Company</option>
		</select>
		<br>
<?		if(defined("CONFIG_SHOW_INTRODUCER_FIELD") && (strstr(CONFIG_SHOW_INTRODUCER_FIELD,$agentType.",") || CONFIG_SHOW_INTRODUCER_FIELD=="1")){ ?>

        <input class="required form-control" id="source" name="source" type="text" value="" placeholder="Source" />
		
		<? } ?>
		<br>
        <input class="required form-control" id="manualAgentNum" name="manualAgentNum" type="text" value="" placeholder=" *Introducer Login Name" />
       <br>
        <input class="required form-control" id="agentCompany" name="agentCompany" type="text" value="" placeholder="*Company Name" />
		    


			<label> <em></em>Company Logo</label>
		<input type="file" name="logo">
		
	
		  
		  
<br>
        <input class="required form-control" id="agentContactPerson" name="agentContactPerson" type="text" value="" placeholder="*Contact Person"/>
        <!--                <label> <em>*</em>Date of Birth - dd/mm/yyyy</label>
                <input class="required birthday" id="Birthday" name="Birthday" type="text" value="" />-->
       <br>
        <input class="form-control" id="email" name="email" type="text" value="" placeholder="Email Address" />
        <!--                <input class="required email" id="email" name="email" type="text" value="" />-->
<!--        <label> MSB Number</label>
        <input id="agentMSBNumber" name="agentMSBNumber" type="text" value="" />-->
        <br>
        <input class="form-control" id="agentCompDirector" name="agentCompDirector" type="text" value="" placeholder="Company Director" />
        <br>
        <input class="form-control" id="agentDirectorAdd" name="agentDirectorAdd" type="text" value="" placeholder="Director Address" />
      </div>
      <div class="float">
<br>
        <input class="required form-control" id="agentAddress" name="agentAddress" type="text" value="" placeholder="*Address line 1" />
      <br>
        <input class="form-control" id="agentAddress2" name="agentAddress2" type="text" value="" placeholder=" Address line 2" />
        <br>
        <input class="form-control" id="postcode" name="postcode" type="text" value="" placeholder="Post Code" />
        <!--                <input class="required postcode form-control" id="postcode" name="postcode" type="text" value="" />-->
        <br>
        <input class="form-control" id="City" name="City" type="text" value="" placeholder=" City/Town" />
        <br>
        <select class="required country-selector form-control" id="Country" name="Country">
          <option value="">*Country</option>
		<?php
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
				$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
								
					for ($i=0; $i < count($countires); $i++){
		?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>">
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
        </SELECT>
     <br>
        <input class="required phone form-control" id="agentPhone" name="agentPhone" type="text" value="" placeholder="*Telephone Number" />
   <br>
        <input  class="form-control" id="mobile" name="mobile" type="text" value="" placeholder="Mobile Number" />
       <br>
        <input class="phone form-control" id="agentFax" name="agentFax" type="text" value="" placeholder="Fax" />
      </div>
      <div class="float last">
<!--		<SELECT name="IDAcountry[]" size="4" MULTIPLE id="IDAcountry" >
			<OPTION VALUE="Portugal">Portugal</OPTION>
		</SELECT>-->
		<input type="hidden" name="IDAcountry[]" id="IDAcountry" value="Portugal" />
<!--        <label> Authorised for the services</label>
		<SELECT  class="required country-selector" name="authorizedFor[]" size="4" id="servicesDiv">
        	<OPTION VALUE="Bank Transfer">Bank Transfer</OPTION>	  
		</SELECT>-->
		<input type="hidden" name="authorizedFor[]" size="4" id="servicesDiv" value="Bank Transfer" />
		<br>
        <select class="required country-selector form-control" id="agentCurrency" name="agentCurrency">
		<OPTION  SELECTED>Account Currency</option>
                <OPTION VALUE="USD" >US Dollar</option>
                <OPTION VALUE="AFA">Afghanistan Afghani</option>
                <OPTION VALUE="ALL">Albanian Lek</option>
                <OPTION VALUE="DZD">Algerian Dinar</option>
                <OPTION VALUE="ADF">Andorran Franc</option>
                <OPTION VALUE="ADP">Andorran Peseta</option>
                <OPTION VALUE="AON">Angolan New Kwanza</option>
                <OPTION VALUE="ARS">Argentine Peso</option>
                <OPTION VALUE="AWG">Aruban Florin</option>
                <OPTION VALUE="AUD">Australian Dollar</option>
                <OPTION VALUE="ATS">Austrian Schilling</option>
                <OPTION VALUE="BSD">Bahamian Dollar </option>
                <OPTION VALUE="BHD">Bahraini Dinar</option>
                <OPTION VALUE="BDT">Bangladeshi Taka</option>
                <OPTION VALUE="BBD">Barbados Dollar</option>
                <OPTION VALUE="BEF">Belgian Franc</option>
                <OPTION VALUE="BZD">Belize Dollar</option>
                <OPTION VALUE="BMD">Bermudian Dollar</option>
                <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                <OPTION VALUE="BOB">Bolivian Boliviano</option>
                <OPTION VALUE="BWP">Botswana Pula</option>
                <OPTION VALUE="BRL">Brazilian Real</option>
                <OPTION VALUE="GBP">British Pound</option>
                <OPTION VALUE="BND">Brunei Dollar</option>
                <OPTION VALUE="BGL">Bulgarian Lev</option>
                <OPTION VALUE="BIF">Burundi Franc</option>
                <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                <OPTION VALUE="XAF">CFA Franc BEAC</option>
                <OPTION VALUE="XPF">CFP Franc</option>
                <OPTION VALUE="KHR">Cambodian Riel</option>
                <OPTION VALUE="CAD">Canadian Dollar</option>
                <OPTION VALUE="CVE">Cape Verde Escudo</option>
                <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                <OPTION VALUE="CLP">Chilean Peso</option>
                <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                <OPTION VALUE="COP">Colombian Peso</option>
                <OPTION VALUE="KMF">Comoros Franc</option>
                <OPTION VALUE="CRC">Costa Rican Colon</option>
                <OPTION VALUE="HRK">Croatian Kuna</option>
                <OPTION VALUE="CUP">Cuban Peso</option>
                <OPTION VALUE="CYP">Cyprus Pound</option>
                <OPTION VALUE="CZK">Czech Koruna</option>
                <OPTION VALUE="DKK">Danish Krone</option>
                <OPTION VALUE="DJF">Djibouti Franc</option>
                <OPTION VALUE="DOP">Dominican R. Peso</option>
                <OPTION VALUE="NLG">Dutch Guilder</option>
                <OPTION VALUE="XEU">ECU</option>
                <OPTION VALUE="XCD">East Caribbean Dollar</option>
                <OPTION VALUE="ECS">Ecuador Sucre</option>
                <OPTION VALUE="EGP">Egyptian Pound</option>
                <OPTION VALUE="SVC">El Salvador Colon</option>
                <OPTION VALUE="EEK">Estonian Kroon</option>
                <OPTION VALUE="ETB">Ethiopian Birr</option>
                <OPTION VALUE="EUR">Euro</option>
                <OPTION VALUE="FKP">Falkland Islands Pound</option>
                <OPTION VALUE="FJD">Fiji Dollar</option>
                <OPTION VALUE="FIM">Finnish Markka</option>
                <OPTION VALUE="FRF">French Franc</option>
                <OPTION VALUE="GMD">Gambian Dalasi</option>
                <OPTION VALUE="DEM">German Mark</option>
                <OPTION VALUE="GHC">Ghanaian Cedi</option>
                <OPTION VALUE="GIP">Gibraltar Pound</option>
                <OPTION VALUE="XAU">Gold (oz.)</option>
                <OPTION VALUE="GRD">Greek Drachma</option>
                <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                <OPTION VALUE="GNF">Guinea Franc</option>
                <OPTION VALUE="GYD">Guyanese Dollar</option>
                <OPTION VALUE="HTG">Haitian Gourde</option>
                <OPTION VALUE="HNL">Honduran Lempira</option>
                <OPTION VALUE="HKD">Hong Kong Dollar</option>
                <OPTION VALUE="HUF">Hungarian Forint</option>
                <OPTION VALUE="ISK">Iceland Krona</option>
                <OPTION VALUE="INR">Indian Rupee</option>
                <OPTION VALUE="IDR">Indonesian Rupiah</option>
                <OPTION VALUE="IRR">Iranian Rial</option>
                <OPTION VALUE="IQD">Iraqi Dinar</option>
                <OPTION VALUE="IEP">Irish Punt</option>
                <OPTION VALUE="ILS">Israeli New Shekel</option>
                <OPTION VALUE="ITL">Italian Lira</option>
                <OPTION VALUE="JMD">Jamaican Dollar</option>
                <OPTION VALUE="JPY">Japanese Yen</option>
                <OPTION VALUE="JOD">Jordanian Dinar</option>
                <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                <OPTION VALUE="KES">Kenyan Shilling</option>
                <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                <OPTION VALUE="LAK">Lao Kip</option>
                <OPTION VALUE="LVL">Latvian Lats</option>
                <OPTION VALUE="LBP">Lebanese Pound</option>
                <OPTION VALUE="LSL">Lesotho Loti</option>
                <OPTION VALUE="LRD">Liberian Dollar</option>
                <OPTION VALUE="LYD">Libyan Dinar</option>
                <OPTION VALUE="LTL">Lithuanian Litas</option>
                <OPTION VALUE="LUF">Luxembourg Franc</option>
                <OPTION VALUE="MOP">Macau Pataca</option>
                <OPTION VALUE="MGF">Malagasy Franc</option>
                <OPTION VALUE="MWK">Malawi Kwacha</option>
                <OPTION VALUE="MYR">Malaysian Ringgit</option>
                <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                <OPTION VALUE="MTL">Maltese Lira</option>
                <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                <OPTION VALUE="MUR">Mauritius Rupee</option>
                <OPTION VALUE="MXN">Mexican Peso</option>
                <OPTION VALUE="MNT">Mongolian Tugrik</option>
                <OPTION VALUE="MAD">Moroccan Dirham</option>
                <OPTION VALUE="MZM">Mozambique Metical</option>
                <OPTION VALUE="MMK">Myanmar Kyat</option>
                <OPTION VALUE="ANG">NL Antillian Guilder</option>
                <OPTION VALUE="NAD">Namibia Dollar</option>
                <OPTION VALUE="NPR">Nepalese Rupee</option>
                <OPTION VALUE="NZD">New Zealand Dollar</option>
                <OPTION VALUE="NIO">Nicaraguan Cordoba Oro</option>
                <OPTION VALUE="NGN">Nigerian Naira</option>
                <OPTION VALUE="KPW">North Korean Won</option>
                <OPTION VALUE="NOK">Norwegian Kroner</option>
                <OPTION VALUE="OMR">Omani Rial</option>
                <OPTION VALUE="PKR">Pakistan Rupee</option>
                <OPTION VALUE="XPD">Palladium (oz.)</option>
                <OPTION VALUE="PAB">Panamanian Balboa</option>
                <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                <OPTION VALUE="PYG">Paraguay Guarani</option>
                <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                <OPTION VALUE="PHP">Philippine Peso</option>
                <OPTION VALUE="XPT">Platinum (oz.)</option>
                <OPTION VALUE="PLN">Polish Zloty</option>
                <OPTION VALUE="PTE">Portuguese Escudo</option>
                <OPTION VALUE="QAR">Qatari Rial</option>
                <OPTION VALUE="ROL">Romanian Lei</option>
                <OPTION VALUE="RUB">Russian Rouble</option>
                <OPTION VALUE="WST">Samoan Tala</option>
                <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                <OPTION VALUE="SAR">Saudi Riyal</option>
                <OPTION VALUE="SCR">Seychelles Rupee</option>
                <OPTION VALUE="SLL">Sierra Leone Leone</option>
                <OPTION VALUE="XAG">Silver (oz.)</option>
                <OPTION VALUE="SGD">Singapore Dollar</option>
                <OPTION VALUE="SKK">Slovak Koruna</option>
                <OPTION VALUE="SIT">Slovenian Tolar</option>
                <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                <OPTION VALUE="SOS">Somali Shilling</option>
                <OPTION VALUE="ZAR">South African Rand</option>
                <OPTION VALUE="KRW">South-Korean Won</option>
                <OPTION VALUE="ESP">Spanish Peseta</option>
                <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                <OPTION VALUE="SHP">St. Helena Pound</option>
                <OPTION VALUE="SDD">Sudanese Dinar</option>
                <OPTION VALUE="SDP">Sudanese Pound</option>
                <OPTION VALUE="SRG">Suriname Guilder</option>
                <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                <OPTION VALUE="SEK">Swedish Krona</option>
                <OPTION VALUE="CHF">Swiss Franc</option>
                <OPTION VALUE="SYP">Syrian Pound</option>
                <OPTION VALUE="TWD">Taiwan Dollar</option>
                <OPTION VALUE="TZS">Tanzanian Shilling</option>
                <OPTION VALUE="THB">Thai Baht</option>
                <OPTION VALUE="TOP">Tonga Pa'anga</option>
                <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                <OPTION VALUE="TND">Tunisian Dinar</option>
                <OPTION VALUE="TRL">Turkish Lira</option>
                <OPTION VALUE="AED">UAE Dirham</option>
                <OPTION VALUE="UGS">Uganda Shilling</option>
                <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                <option value="GBP">United Kingdom Pounds</option>
                <OPTION VALUE="UYP">Uruguayan Peso</option>
                <OPTION VALUE="VUV">Vanuatu Vatu</option>
                <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                <OPTION VALUE="VND">Vietnamese Dong</option>
                <OPTION VALUE="YUN">Yugoslav Dinar</option>
                <OPTION VALUE="ZMK">Zambian Kwacha</option>
                <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
        </select>
<!--        <label> Introducer Account Limit </label>
        <input id="agentAccountLimit" name="agentAccountLimit" type="text" value="" />-->
<?php		
		if(CONFIG_USER_COMMISSION_MANAGEMENT == '1' && ($_GET["ida"] != "Y" || CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')){
        }else{
        ?>
          
          <br>
		  <select class="form-control" name="commPackage" id="commPackage">
		  <option selected >Commission package</option>
			<option value="001">Fixed Amount</option>
			<option value="002">Percentage of Transaction Amount</option>
			<option value="003">Percentage of Fee</option>
			<option value="004">None</option>
		  </select>
			<br>
			<input class="form-control" id="agentCommission" name="agentCommission" type="text" value="" placeholder="Commission Percentage/Fee" />
            </td>
          </tr>
       <? }?>

<?php /*?>        <label> Correspondent </label>
        <select class="required country-selector" id="correspondent" name="correspondent">
           <option value="N">Agent</option>
        </select>
<?php */?>
<br>


          <!--			//////////////////////////////////////////////////////////////////-->
          <div bgcolor="#ededed">
              <div>
                  <font color="#005b90"><strong>Select services  </strong></font>
              </div>
              
              <div>
                  <SELECT name="services[]" size="4" multiple id="services" style="font-family:verdana; font-size: 11px">
                      <option value="W">Website</option>
                      <option value="O">Online</option>
                      <option value="BO">Back Office</option>
                      <option value="G">Gateway</option>
                  </SELECT>
              </div>

          </div>
          <!--			////////////////////////////////////////////-->


          <label> Status </label>
		<input type="hidden" id="correspondent" name="correspondent" value="N">
		
        <input type="radio" value="New" name="agentStatus" style="width:10px;">
        New<br>
        <input type="radio"  checked="" value="Active" name="agentStatus" style="width:10px;">
        Active<br>
        <input type="radio" value="Disabled" name="agentStatus" style="width:10px;">
        Disabled<br>
        <input type="radio" value="Suspended" name="agentStatus" style="width:10px;">
        Suspended<br>
                
                <div class="documentInformation">
                  <label class="documentNumber">Upload Document</label>
                  <input class="" id="file" name="file" type="file" />
                </div>
      </div>
		<div class="inlineCheckbox" style="margin-left:-150px">
			<label for="newsletter">Send an Email to Introducer.</label>
			<input id="newsletter" name="sendAgentEmail" type="checkbox" value="true" class="sendEmail"/>
			<input name="emailFlag" type="hidden" value="true" />
		</div>
		<?php 
		if(defined("CONFIG_NEW_INTRODUCER_ASSOCIATION") && CONFIG_NEW_INTRODUCER_ASSOCIATION==1)
		{ ?>
		
			<div class="inlineCheckbox" style="margin-left:-150px">
			<label for="IsByDefault">By Default [Introducer Selection]</label>
			<input style="margin-left: -2px; margin-top: 0px" id="IsByDefault" name="IsByDefault" type="checkbox" value="true" class="ByDefaultSelected" />
		
			</div> 
<?php   }
		?>	
		
          <input class="btn submit-button" type="submit" value="Submit" />
		  <input type="hidden" id="error_flag" name="error_flag" value="" />

      </fieldset>
      <!-- <a href="#" class="newIndividual">Add new Individual</a>-->
      </fieldset>
<!--      <fieldset class="group">
      <legend><b>Transaction Information</b></legend>
            <div class="float">
<label> <em>*</em>Transaction Type</label>
              <select class="required" id="Activity" name="Activity"><option value="">Please Select</option>
<option value="Bank Transfer">Bank Transfer</option>
<option value="Pick Up">Pick Up</option>
<option value="Home Delivery">Home Delivery</option>
<option value="ATM Card">ATM Card</option>
</select>

              <label> <em>*</em>Banke Name</label>

              <input class="required" id="Company_Name" name="Name" value="" type="text">
              
              <label>Branch Address</label>
              <input id="Company_Trade" name="Trade" value="" type="text">
              
              <label> <em>*</em>Branch Name</label>
              <input class="required" id="Company_Registration" name="Registration" value="" type="text">
              
              <label>Account Number</label>

              <input id="Company_VAT" name="VAT" value="" type="text">
            </div>
            <div class="float">
             <label> <em>*</em>Select Distributor</label>
              <select class="required" id="Activity1" name="Activity1"><option value="">Please Select</option>
<option value="123">ASLAM-123</option>
<option value="124">SHAHID-123</option>
</select>
             <label> <em>*</em>Money Paid</label>
              <select class="required" id="Activity2" name="Activity2"><option value="">Please Select</option>
<option value="123">By Cash</option>
<option value="124">By Cheque</option>
<option value="124">By Bank Transfer</option>
</select>
             <label> <em>*</em>Purpose of Transaction</label>
              <select class="required" id="Activity3" name="Activity3"><option value="">Please Select</option>
<option value="123">Business</option>
<option value="124">Education</option>
</select>
            </div>
            <div class="float last">
              <label> <em>*</em>Transaction Amount</label>
              <input class="required postcode" id="Company_Postcode" name="Postcode" value="" type="text">
              
              <label> <em>*</em>Exchange Rate</label>
              <input class="required" id="Company_City" name="City" value="" type="text">
              <label> <em>*</em>Commission/Fee</label>
              <input class="required" id="Company_City" name="City" value="" type="text">
              <label> <em>*</em>Local Amount</label>
              <input class="required" id="Company_City" name="City" value="" type="text">
              <div class="compulsoryAdvice">* Compulsory Fields</div>
            </div>
      </fieldset>
      <fieldset id="tacFieldset" class="group">
                 <legend>Terms and Conditions</legend>
            <strong>Terms and Conditions description can be displayed here.</strong>
      <div class="group">
        <p id="indication"><b></b></p>
                      <div id="tacFieldsetDiv">
                <h3>Interpretation:</h3>
                <h4>
                  In these terms and conditions, unless the context otherwise requires:                </h4>
                <ul>
                  <li>condition number 1.</li>
                  <li>condition number 2.</li>
                  <li>condition number 3.</li>
                </ul>
                <ol>
                  <li>
                    <h3>Instructions</h3>
                    <ol>
                      <li>Instruction number 1</li>
                      <li>Instruction number 2</li>
                    </ol>
                  </li>
                </ol>
              </div>
              <div class="left text">
                <p style="font-size11px;">
                  You need to accept terms and conditions to proceed.</p>
              </div>
        <div class="right">
                          
                <div class="inlineCheckbox">
                  <label>
                     <em>*</em>YES, I Accept the <a href='#'>Terms and Conditions</a></label>
                  <input class="required" id="agreement" name="Agreement" type="checkbox" value="true" /><input name="Agreement" type="hidden" value="false" />
                </div>
                <div class="inlineCheckbox">
                  <label for="newsletter">
                    Do you want to receive email updates.</label>
                  <input id="newsletter" name="Unsubscribe" type="checkbox" value="true" /><input name="Unsubscribe" type="hidden" value="false" />
                </div>
          <input class="submit" type="submit" value="Submit" />
		  <input type="hidden" id="error_flag" name="error_flag" value="" />
        </div>
      </div>
      </fieldset>-->
    </form>
  </div>
  <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
      </script>
  <script type="text/javascript">
        try {
          var pageTracker = _gat._getTracker("UA-12966809-1");
          pageTracker._trackPageview();
        }
        catch(err) {}
        
      </script>
</div>
</body>
</html>