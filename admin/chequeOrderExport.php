<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	
	$strSql = "select 
					order_id, 
					o.cheque_currency, 
					cheque_ref, 
					o.company_id, 
					cheque_no, 
					created_on, 
					estimated_date, 
					paid_on, 
					cheque_amount, 
					paid_amount, 
					status, 
					name, 
					created_by, 
					cust.firstname, 
					cust.middlename, 
					cust.lastname
				from 
					cheque_order o, 
					company c, customer cust
				where 
					c.company_id = o.company_id and 
					cust.customerid = o.customer_id
					";
		
		
	if($_REQUEST["Submit"] == "Search")
	{
		$strFilters = "";
		
		if(!empty($_REQUEST["from_date"]))
		{
			$date = explode("/",$_REQUEST["from_date"]);
			$strFilters .= " and created_on >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
		}
		
		if(!empty($_REQUEST["to_date"]))
		{
			$date = explode("/",$_REQUEST["to_date"]);
			$strFilters .= " and created_on <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
		}
		
		if(!empty($_REQUEST["cheque_ref"]))
			$strFilters .= " and cheque_ref = '".$_REQUEST["cheque_ref"]."'";
		
		if(!empty($_REQUEST["name"]))
			$strFilters .= " and name like '%".$_REQUEST["name"]."%'";
			
		if(!empty($_REQUEST["order_status_filter"]))
			$strFilters .= " and status = '".$_REQUEST["order_status_filter"]."'";
		
		
		$strSql .=  $strFilters;
	}
	
	$arrOrderData = selectMultiRecords($strSql);
	
	//debug($arrOrderData);
	
	
	$exportType = $_REQUEST["exportType"];
	
	
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($exportType == "XLS")
	{
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=ChequeOrder.".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=ChequeOrder.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Cheque Orders</title>
							
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Cheque Refrence".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Cheque No.".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Client".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Date In".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Estimated Date".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Paid On".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Value".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Amount Paid".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Created By".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;


	$strFullHtml .= $strRowStart;
	foreach($arrOrderData as $key => $val)
	{
		$strPaidOn = "";
		if($val["paid_on"] != "0000-00-00 00:00:00")
			$strPaidOn = date("d/m/Y",strtotime($val["paid_on"]));
		
		
		if(!empty($val["created_by"]))
		{
			$creator = explode("|",$val["created_by"]);
			$arrCreatorDetail = selectFrom("select username from admin where userid='".$creator[0]."'");
		}
		
		if(!empty($val["paid_by"]))
		{
			$paidBy = explode("|",$val["paid_by"]);
			$arrPaidByDetail = selectFrom("select username from admin where userid='".$paidBy[0]."'");
		}
		
		
		if($exportType == "CSV")
			$strFullHtml .= "";
		else
			$strFullHtml .= "<tr class='".$val["status"]."'>";
			
		$strFullHtml .= $strColumnStart.$val["cheque_ref"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["cheque_no"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["name"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["created_on"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["estimated_date"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strPaidOn.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["cheque_amount"]." ".$val["cheque_currency"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["paid_amount"].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$arrChequeStatues[$val["status"]].$strColumnEnd;
		$strFullHtml .= $strColumnStart.$arrCreatorDetail["username"].$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		
	}
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;
	
	
	
	echo $strFullHtml;
?>