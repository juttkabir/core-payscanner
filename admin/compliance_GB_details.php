<?php 
	/**
	 * @package Online Register Users
	 * @subpackage GB Group Compliance
	 * This page is for show individual sender compliance according to GB Group
	 * @author Mirza Arslan Baig
	 */
	
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	$senderID = $_GET['custID'];

	if(isset($_POST['exportExcel'])){
		$exportQuery = $_POST['exportQuery'];
		$complianceGB = selectMultiRecords($exportQuery);
		header("Content-type: application/x-msexcel"); 
		header("Content-Disposition: attachment; filename=GB_Compliance_List.xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		$strMainStart = '<table border="1" align="center" cellspacing="0" width="900">';
		$strMainEnd = '</table>';
		$strRowStart = '<tr>';
		$strRowEnd = '</tr>';
		$strColStart = '<td>&nbsp;';
		$strColStartSr = '<td align="left">';
		$strColEnd = '</td>';
		
		$strHeadCols = "<th>Serial No.</th>";
		$strHeadCols .= "<th>Authentication ID</th>";
		$strHeadCols .= "<th>IP Address</th>";
		$strHeadCols .= "<th>Customer Reference Number</th>";
		$strHeadCols .= "<th>Document Type</th>";
		$strHeadCols .= "<th>ID Number</th>";
		$strHeadCols .= "<th>Expiry Date</th>";
		$strHeadCols .= "<th>Country</th>";
		$strHeadCols .= "<th>Result</th>";
		$strHeadCols .= "<th>Score</th>";
		$strHeadCols .= "<th>Verified By</th>";
		$strHeadCols .= "<th>Verified Date</th>";
		$strExportData = $strMainStart.$strHeadCols;
		$queryIDInfo = "SELECT * FROM user_id_types WHERE user_id = '$senderID'";
		$idResults = mysql_query($queryIDInfo);
		for($x = 0; $x < count($complianceGB); $x++){
			$idType = mysql_fetch_array($idResults);
			$strExportData .= $strRowStart.$strColStartSr. ($x+1)."."; $strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['authenticationID'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['ipAddress'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['accountName'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['documentType'].$strColEnd;
			$strExportData .= $strColStart.$idResults['id_number'].$strColEnd;
			$strExportData .= $strColStart.$idType['expiry_date'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['country'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['result'].$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['score'].$strColEnd;
			$strExportData .= $strColStart."GB Group".$strColEnd;
			$strExportData .= $strColStart.$complianceGB[$x]['verificationDate'].$strColEnd.$strRowEnd;;
		}
		$strExportData .= $strMainEnd;
		echo $strExportData;
		exit();
	}
	$queryComplianceGB = "SELECT *, cust.accountName, cust.country, cust.ipAddress FROM compliance_GB as GBComp INNER JOIN ".TBL_CUSTOMER." as cust ON GBComp.customerID = cust.customerID WHERE GBComp.customerID = '$senderID'";
	$complianceGB = selectMultiRecords($queryComplianceGB);
	
?>
<html>
	<head>
		<title>GB Compliance Details</title>
		<link href="images/interface.css" rel="stylesheet" type="text/css" />
		<style>
			.style1{
				font-weight:bold;
				font-size:10px;
			}
			table tr td{
				font-size:10px;
			}
		</style>
		
	</head>
	<body>
		<table cellpadding="1" cellspacing="5" width="100%">
			<tr>
				<td bgcolor="#C0C0C0">
					<strong>
						<font size="2" color="#fff">
							GB Compliance Details
						</font>
					</strong>
				</td>
			</tr>
			<tr>
				<td align="center">
				<br />
					<table cellpadding="2" cellspacing="0" border="1">
						<tr style="background:#eee">
							<td align="center" width="40" class="style1">Serial No.</td>
							<td align="center" width="240" class="style1">Authentication ID</td>
							<td align="center" width="200" class="style1">IP Address</td>
							<td align="center" width="130" class="style1">Customer Reference Number</td>
							<td align="center" width="70" class="style1">Document Type</td>
							<td align="center" width="70" class="style1">ID Number</td>
							<td align="center" width="70" class="style1">Expiry Date</td>
							<td align="center" width="70" class="style1">Country</td>
							<td align="center" width="60" class="style1">Result</td>
							<td align="center" width="50" class="style1">Score</td>
							<td align="center" width="70" class="style1">Verified By</td>
							<td align="center" width="125" class="style1">Verified Date</td>
						</tr>
						<?php 
							$queryIDInfo = "SELECT * FROM user_id_types WHERE user_id = '$senderID'";
							$idResults = mysql_query($queryIDInfo);
							for($x = 0; $x < count($complianceGB); $x++){
								$idType = mysql_fetch_array($idResults);
						?>
						<tr>
							<td align="center"><?php echo ($x+1)."."; ?></td>
							<td><?php echo $complianceGB[$x]['authenticationID']; ?></td>
							<td><?php echo $complianceGB[$x]['ipAddress']; ?></td>
							<td><?php echo $complianceGB[$x]['accountName']; ?></td>
							<td><?php echo $complianceGB[$x]['documentType']; ?></td>
							<td><?php echo $idType['id_number']; ?></td>
							<td><?php echo $idType['expiry_date']; ?></td>
							<td><?php echo $complianceGB[$x]['country']; ?></td>
							<td><?php echo $complianceGB[$x]['result']; ?></td>
							<td><?php echo $complianceGB[$x]['score']; ?></td>
							<td>GB Group</td>
							<td><?php echo $complianceGB[$x]['verificationDate']; ?></td>
						</tr>
						<?php
							}
						?>
						<tr>
							<td colspan="12" align="center">
								<form action="" name="action" method="post">
									<input type="button" onClick="print();" value="Print"/>
									<input type="hidden" name="exportQuery" id="exportQuery" value="<?=$queryComplianceGB?>" />
									<input name="exportExcel" id="exportExcel" value="Export" type="submit"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
