<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
require_once './cm_xls/Excel/reader.php';

//$agentType = getAgentType();

$_SESSION["agentID2"] = "";
if($_POST["agentID"]!="")
	$_SESSION["agentID2"] =$_POST["agentID"];
	elseif($_GET["agentID"]!="")
		$_SESSION["agentID2"]=$_GET["agentID"];
	 $_SESSION["agentID2"];
$success = "";
if($_POST["Submit"] != "")
{
	$csvFile = $_FILES["csvFile"]["tmp_name"];
	if ($csvFile=='none')
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	}
	$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));
	if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" )
	{
		
		if (is_uploaded_file($csvFile))
		{
			$Pict="$username-" . date("Y-m-d") . $ext;
			//move_uploaded_file($csvFile, "../uploads/" . $Pict );

			// Parsing file and inserting into DB
			//$filename= "../uploads/$Pict";
			$filename = $csvFile;
			$data = new Spreadsheet_Excel_Reader();
			// Set output Encoding.
			$data->setOutputEncoding('CP1251');
			$data->read($filename);
			error_reporting(E_ALL ^ E_NOTICE);
			$tran_date = date("Y-m-d");
			$updateCounter = 0;
			$insertCounter = 0;
			$insertMsg = "";
			$updateMsg = "";
			for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
			{
				// Code added by Usman Ghani against #3540: Connect Plus - Import Collection Point Issues
				foreach( $data->sheets[0]['cells'][$i] as $index => $value )
				{
					$data->sheets[0]['cells'][$i][$index] = trim( $value );
				}
				// End of code against #3540: Connect Plus - Import Collection Point Issues
	
				$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //Country
				$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //City
				$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //Address1
				$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);	// Address2
				$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	//State
				
				$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);  //Phone Number
				$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);	// Collection point name
				$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	// Branch No
				$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);	// Correspondent Name
				$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);  //Opening hours
				$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //Weekend Opening hours
				$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Fax No.
				//$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //Distributor ID (cp_ida_id)
				
			
				
				//$password = createCode();
				/**if($data1[1] != "" || $data1[8] != "") check is applied to restrict empty data from being imported ***/
				if($data1[1] != "" || $data1[8] != ""){
					if($_POST["agentID"] != "")
					{
						$branchNumber = selectFrom("select cp_id,cp_branch_no, cp_ida_id from ".TBL_COLLECTION." where cp_ida_id='".$_POST["agentID"]."' and cp_branch_no = '".$data1[8]."'");
						
						if($branchNumber["cp_id"] !='' ){
							
							$updatCP= "update ".TBL_COLLECTION." 
											set 
												cp_country = '".$data1[1]."', cp_city =  '".$data1[2]."', cp_branch_address = '".$data1[3]."', cp_branch_address2 = '".$data1[4]."', cp_state = '".$data1[5]."', 
					    cp_phone = '".$data1[6]."', cp_branch_name = '".$data1[7]."', cp_branch_no = '".$data1[8]."',cp_corresspondent_name = '".$data1[9]."', cp_opening_hours = '".$data1[10]."', cp_weekend_opening_hours = '".$data1[11]."', cp_fax = '".$data1[12]."',cp_active = 'Active' , cp_ida_id = '".$_POST["agentID"]."'
							where cp_ida_id='".$_POST["agentID"]."' and cp_branch_no = '".$data1[8]."' ";
					
							update($updatCP);
							$updateCounter ++;
							/*$insertMsg = " $updateCounter Collection Points are Updated Successfully ";*/
						}else{
						
						
					    $Querry_Sqls = "INSERT INTO ".TBL_COLLECTION." 
					   (cp_country,cp_city, cp_branch_address, cp_branch_address2, cp_state, 
					   cp_phone, cp_branch_name, cp_branch_no,cp_corresspondent_name, cp_opening_hours, cp_weekend_opening_hours,cp_fax,cp_active, cp_ida_id) VALUES 
					   ('".$data1[1]."','".$data1[2]."', '".$data1[3]."', '".$data1[4]."', '".$data1[5]."',
					   '".$data1[6]."', '".$data1[7]."', '".$data1[8]."', '".$data1[9]."', '".$data1[10]."','".$data1[11]."','".$data1[12]."','Active', '".$_POST["agentID"]."')";
					 
						  insertInto($Querry_Sqls);
							$benID = @mysql_insert_id();
							
							$insertCounter ++;
							/*$updateMsg = " $insertCounter Collection Points are Imported Successfully ";*/
				  	}
				
		    	}
				}
			}
			$insertMsg = " $updateCounter Collection Points are Updated Successfully ";
			$updateMsg = " $insertCounter Collection Points are Imported Successfully ";
			$msg = $insertMsg."<br />".$updateMsg;
			$success = "Y";
			
			//exit;
				// END
		}
		else
		{
			$msg = CAUTION_MARK . " Your file is not uploaded due to some error.";
		}
	}
	else
	{
		$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
	}	
}


// $logoContent = selectFrom("select logo from " . TBL_ADMIN_USERS. " where username='$username'");

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
	
function checkForm(theForm) {
	
	if(theForm.agentID.value == "" || IsAllSpaces(theForm.agentID.value)){
    	alert("Please Select Distributor.");
        theForm.agentID.focus();
        return false;
    }

}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
   function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Import Collection Points </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0">
  <tr>
    <td  valign="top"><fieldset>
    <legend class="style2">Import Collection Points </legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="addComplaint" action="import-collection-point-updated.php" method="post" enctype="multipart/form-data" onSubmit="return checkForm(this);">
			 
			  <tr align="center" class="tab-r">
			    <td colspan="2">Please follow the instructions on the Readme.txt file.</td>
			    </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="#" onClick="javascript:window.open('import-readme-cp.php?page=custben', 'ReadMe', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=250,width=650,top=200,left=170')" class="style2">
				Readme.txt
			  </a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="xls/sample-collection-point.xls" class="style2">Sample Template</a>	
				</td>
			  </tr>
			  
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2"><font color="#ff0000">*</font>
	       <select name="agentID" style="font-family:verdana; font-size: 11px" <? if (DEST_CURR_IN_ACC_STMNTS == "1") { ?>onchange="document.Search.submit();"<? } ?>>
				<option value="">- Select Distributor -</option>
				<!--option value="All">- All Banks -</option-->
				<?
						$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent'  and isCorrespondent = 'ONLY' order by agentCompany");
						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
						}
					?>
			  </select>
			  <script language="JavaScript">
				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
									</script>		
			
				</td>
			  </tr>
			  
			  <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Excel File </td>
			    <td width="75%" bgcolor="#DFE6EA">			      <input name="csvFile" type="file" id="csvFile" size="15"></td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
		      <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			  	<td colspan="2"><font color=<? echo ($success != "" ? SUCCESS_COLOR : "#CC0000") ?> ><strong><? echo $msg ?></strong></font><br></td>
			  </tr>
			  <tr>
			  <?
			  }
			  ?>
			</table>
		    <br>
		     
        </fieldset></td>
    <td width="431" valign="top">
	<? if ($str != "")
	{
	?>
	
	<fieldset>
    <legend class="style2">Import Collection Points Results </legend>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
      <form name="addComplaint" action="import-collection-point-updated.php" method="post" enctype="multipart/form-data">
        <tr>
          <td valign="top" bgcolor="#DFE6EA"><? echo $str?></td>
          </tr>
      </form>
    </table>
    <br>
    </fieldset>
	<?
	}
	?>
	</td>
  </tr>
  
</table>

	</td>
  </tr>
</table>
</body>
</html>
