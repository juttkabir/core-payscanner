<?php  
/*if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
    session_cache_limiter("public");
}*/
session_start();
include ("../include/config.php");
include ("security.php");
$userID  = $_SESSION["loggedUserData"]["userID"];
if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "csv")	
{
	/* Export version is csv */
	//echo '##1!te';
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	
	header('Content-type: application/octet-stream');
	header("Content-disposition: filename=transactionsExportedCSV.csv");
	
	
	//$strDbSql = "select dispatchNumber from dispatch_book where recId=".$_REQUEST["recId"];
	//$arrDbSql = selectFrom($strDbSql);
	
	
	if(!empty($_REQUEST["exportspb"]) && $_REQUEST["exportspb"] == "t" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId, 
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.transID IN (".$arrAllTransactions.") and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
	}
	else
	{
	
		if($_REQUEST["transTypeFlag"]){
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId,  
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID  
				order by 
					tbd.bankName asc";
					//$arrDbSql["dispatchNumber"]
		}
		if($_REQUEST["excludeDistFlag"] && $_REQUEST["pickupFlag"] && !$_REQUEST["transTypeFlag"]){			
		$strSql = "select 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.benAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					cp.cp_corresspondent_name,
					cp.cp_city
				from 
					transactions as tr
					LEFT JOIN cm_collection_point as cp ON tr.collectionPointID = cp.cp_id
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
				where
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					transType = '".trim($_REQUEST["transType"])."'
					
				order by tr.transID	asc
					";
				//debug($strSql);	
		}
	}
	$fullRS = SelectMultiRecords($strSql);
	//debug($strSql);
	//debug($fullRS);

	$strCsvOutput = "";
	

	//$strCsvOutput .= "Transaction Id,Dispatch Date,Sender Name,Sender Document Number,Beneficiary Name,Beneficiary City,CPF/CPNJ,Bank Id,Bank Name,Branch Number,Account Number,Bank Account Type,Local Amount\r\n";
	
	$cpfseperator = array("-","/",".");
	for($i=0; $i < count($fullRS); $i++)
	{
		
		$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"];
		
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"];
		
		$strNewFormatTime = strtotime($fullRS[$i]["dispatchDate"]);
		
		$strCsvOutput .= substr($fullRS[$i]["refNumberIM"],0,20).",";
		$strCsvOutput .= substr(date("m/d/Y",$strNewFormatTime),0,20).",";
		$strCsvOutput .= trim(substr($senderFullName,0,40)).",";
		$strCsvOutput .= substr($fullRS[$i]["IDNumber"],0,40).",";
		$strCsvOutput .= trim(substr($beneficiaryFullName,0,40)).",";
		
		$strCsvOutput .= substr($fullRS[$i]["City"],0,30).",";
		if($_REQUEST["transTypeFlag"]){
		$strCsvOutput .= substr(str_replace($cpfseperator,"",$fullRS[$i]["CPF"]),0,14).",";
		$strCsvOutput .= substr($fullRS[$i]["originalBankId"],0,3).",";
		$strCsvOutput .= substr($fullRS[$i]["bankName"],0,60).",";
		
		$strCsvOutput .= substr(str_pad(str_replace("-","",$fullRS[$i]["branchCode"]),5,"0"),0,5).",";
		
		$strCsvOutput .= substr($fullRS[$i]["accNo"],0,20).",";
		$strCsvOutput .= ($fullRS[$i]["accountType"] == "Savings"?"CP":"CC").",";
        }
		//$strCsvOutput .= substr(round($fullRS[$i]["localAmount"],2),0,10);
		$strCsvOutput .= substr(number_format($fullRS[$i]["localAmount"], 2, ".", ","),0,10);
		
		$strCsvOutput .= "\r\n";
	}
	
	echo $strCsvOutput;
	/* End of csv version */
}elseif(isset($_REQUEST["format"]) && $_REQUEST["format"] == "xls")	
{
	/* default export version will be excel */
	
	header('Content-Type: application/vnd.ms-excel');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	header('Content-Type: image/gif');
	
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=dispatchedTransactions.xls"); 
	header ("Content-Description: PHP/MYSQL Generated Data");
	
if(!empty($_REQUEST["recId"])){
	$strDbSql = "select dispatchNumber,created from dispatch_book where recId=".$_REQUEST["recId"];
	$arrDbSql = selectFrom($strDbSql);
	}
	if($_REQUEST["transTypeFlag"]){
	$strSql = "
				select 
					tbd.bankName, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate, 
					tr.localAmount, 
					tr.benAgentID,
					tr.exchangeRate,
					tr.dispatchDate, 
					ben.firstName, 
					ben.middleName,
					ben.lastName, 
					ben.Country, 
					ben.City,
					ben.Phone,
					ben.IDType, 
					ben.IDNumber, 
					ben.userType, 
					ben.CPF, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben 
				where 
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and 
					tr.dispatchNumber = '".$arrDbSql["dispatchNumber"]."' order by tbd.bankName asc";
					
			}		
		if($_REQUEST["excludeDistFlag"] && $_REQUEST["pickupFlag"] && !$_REQUEST["transTypeFlag"]){	
		$strSql = "select 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate, 
					tr.localAmount, 
					tr.benAgentID,
					tr.exchangeRate,
					tr.dispatchDate, 
					ben.firstName, 
					ben.middleName,
					ben.lastName, 
					ben.Country, 
					ben.City,
					ben.Phone,
					ben.IDType, 
					ben.IDNumber, 
					ben.userType, 
					ben.CPF,
					cp.cp_corresspondent_name,
					cp.cp_city
				from 
					transactions as tr
					LEFT JOIN cm_collection_point as cp ON tr.collectionPointID = cp.cp_id
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
				where
					tr.dispatchNumber = '".$arrDbSql["dispatchNumber"]."' 
					
				order by tr.transID	asc
					";
				//debug($strSql);	
		
		}
	
	$fullRS = SelectMultiRecords($strSql);
	if(!empty($_REQUEST["distributors"])){
		$strDistributorSql = selectFrom("
					   select
					   		userID,
							username,
							name, 
							agentContactPerson,
							agentCity,
							agentCountry,
							balance,
							agentCompany,
							agentAddress  
						from
							admin
						where
							userID = '".$_REQUEST["distributors"]."'");
	
	$distCompanyName = $strDistributorSql["agentCompany"];
	$distContactPerson = $strDistributorSql["agentContactPerson"];
	$distCity = $strDistributorSql["agentCity"];
	$distCountry = $strDistributorSql["agentCountry"];
	$agentAddress = $strDistributorSql["agentAddress"];
	$distBalance = $strDistributorSql["balance"];
	}
	
	if(!empty($_REQUEST["distributors"])){
	$today = date("Y-m-d");
	$datesLast= selectFrom("select Max(dated) as dated from ".TBL_ACCOUNT_SUMMARY." where user_id ='".$_REQUEST["distributors"]."' ");
	if(!empty($datesLast["dated"]))	
         $agentContents = selectFrom("select id,user_id, dated, opening_balance, closing_balance,currency  from " . TBL_ACCOUNT_SUMMARY . "  where user_id = '".$_REQUEST["distributors"]."' and dated = '".$datesLast["dated"]."'");
		
	    $openingBalance = $agentContents["opening_balance"];
		
		if(empty($openingBalance))
		   $openingBalance = 0;
}
//	ob_start();


			$myFile = "images/amb_dispatch.gif";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh,filesize($myFile));
			fclose($fh);
			//echo $theData;

$url =$_SERVER['HTTP_HOST'];
$remote_url = $_SERVER['REMOTE_ADDR'];
//debug($url ."------>". $remote_url);	
$path ="/var/sandbox/AMB/admin/images/amb_dispatch.gif";
$path2 = "http://amb.payex.horivert.com/admin/images/amb_dispatch.gif";
//debug($path2);
$mimeType = image_type_to_mime_type("images/amb_dispatch.gif");
//debug($mimeType );
?>

<table>
<tr>
  <td>&nbsp;</td>
 <td>
	<table>
	    <tr><td>
		<!--<img src="images/amb_dispatch.gif" />
		<img src="data:[mime-type],[data]" alt="" /> 
		<img alt="Red square" src="data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%10%00%00%00%10%08%02%00%00%00%90%91h6%00%00%00%19IDAT(%91c%BCd%AB%C2%40%0A%60%22I%F5%A8%86Q%0DCJ%03%00%DE%B5%01S%07%88%8FG%00%00%00%00IEND%AEB%60%82" />
		<img src="data:image/gif,images/amb_dispatch.gif" alt="AMB" />
		<img alt="AMB" src="data:<?=$mimeType?>,<?=$theData?>" />-->
		</td></tr>
		 <tr><td>&nbsp;</td></tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><font size="3"><strong><u><?=$distCompanyName;?></u><strong></font></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><font size="2"><strong><u><?=$agentAddress?></u><strong></font></td>
			<td><font size="3"><strong>DT:<?=$arrDbSql["created"];?><strong></font></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><font size="2"><strong>Kind Attn: <?=$distContactPerson;?></strong></font></td>
			<td><font size="3"><strong>FAX NO:<?=$arrDbSql["dispatchNumber"];?> </strong></font></td>
		</tr>
		<tr>
			<td colspan="4">Please Debit our account and pay on Proper identification to the following Beneficiaries details below. </td>
		</tr>
	</table>	</td></tr>
<tr>
  <td>&nbsp;</td>
 <td>	
		<table border="1" align="center">	
			<tr>
				<th width="10">S.No</th>
				<th>City</th>
				<th>PKR Amount </th>
				<th>Beneficiary Name </th>
				<th>Telephone</th>
				<th>B/O</th>
			</tr>
	<?
		for($i=0; $i < count($fullRS); $i++)
		{
			
			$totalAmount += $fullRS[$i]["localAmount"];
			$exchangeRate = $fullRS[$i]["exchangeRate"];
			$totalTrasactions += 0;
			$sr_no +=1;
		?>	
			<tr>
				<td width="10"><strong><?=$sr_no?></strong></td>
				<td><font size="2"><strong><?=$fullRS[$i]["City"]?></strong></font></td>
				<td><font size="2"><strong><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></strong></font></td>
				<td align="right"><font size="2"><strong><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?>	</strong></font></td>
				<td><font size="2"><strong><?=$fullRS[$i]["Phone"]?></strong></font></td>
				<td><font size="2"><strong>AMB</strong></font></td>
			</tr>
	<?
	 }
	?>
  </table></td></tr>
<tr>
  <td>&nbsp;</td>
<td>&nbsp;</td>
</tr>	
<tr>
  <td>&nbsp;</td> 
<td>
	<table border="2" align="center">
		<tr>
			<td><strong>O/ Balance</strong></td>
			<td><strong>DEBIT</strong></td>
			<td><strong><?=$openingBalance?></strong></td>
		</tr>
		<tr>
			<td><strong>MESSAGE <?=$arrDbSql["dispatchNumber"];?></strong></td>
			<td><strong>DEBIT</strong></td>
			<td><strong><?=$totalAmount?></strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><strong>CL/BALANCE</strong></td>
			<td><strong>DEBIT</strong></td>
			<td><strong><?=$openingBalance+$totalAmount;?></strong></td>
		</tr>
  </table></td></tr>	
 
 <tr>
   <td>&nbsp;</td>
   <td>	
	 <table>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>THANKS</strong></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><strong>RATE:<?="   ".$exchangeRate;?></strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>REGARDS,</strong></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			   <strong>
			   <?
				$userSql = selectFrom("select userID,username,name from admin where userID = '".$userID."' ");
				echo $userSql["name"];
				?>
			  </strong>
			</td>
		</tr>
		<tr>
			<td>
		       
			</td>
		 </tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>AMB EXCHANGE.</strong></td>
		</tr>
  </table> </td></tr>
 </table>	
<?
/* End of Excel export version */
}elseif(isset($_REQUEST["format"]) && $_REQUEST["format"] == "txt")	
{
/* Export version is txt */
	//echo '##1!te';
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	header('Content-type: plain/text');
	header("Content-disposition: Attachment; filename=MT103.txt");
	
		
	if(!empty($_REQUEST["exportspb"]) && $_REQUEST["exportspb"] == "t" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					tr.transDate,
					tr.transAmount,
					tr.transaction_notes,
					tr.currencyFrom,
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId, 
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					tbd.swiftCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.transID IN (".$arrAllTransactions.") and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID					
				order by 
					tbd.bankName asc";
		
		//debug($strSql);			
	}
	else
	{
	
		if($_REQUEST["transTypeFlag"]){
		$strSql = "
				select 
					tr.refNumberIM, 
					tr.dispatchDate, 
					tr.localAmount, 
					tr.transDate,
					tr.transAmount,
					tr.currencyFrom,
					tr.transaction_notes,
					ben.Title as bt,
					ben.firstName as bf, 
					ben.middleName as bm,
					ben.lastName as bl, 
					ben.City, 
					ben.CPF, 
					tbd.originalBankId,  
					tbd.bankName, 
					tbd.accNo, 
					tbd.accountType, 
					tbd.branchCode,
					tbd.swiftCode,
					cust.Title,
					cust.firstName, 
					cust.middleName,
					cust.lastName,
					cust.IDNumber 
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben, 
					customer as cust
				where 
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					tbd.transID = tr.transID and 
					tr.benID = ben.benID and
					tr.customerID = cust.customerID  
				order by 
					tbd.bankName asc";
					//$arrDbSql["dispatchNumber"]
					//debug($strSql);	
		}
		if($_REQUEST["excludeDistFlag"] && $_REQUEST["pickupFlag"] && !$_REQUEST["transTypeFlag"]){			
		$strSql = "select 
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.currencyFrom,
					tr.transAmount,
					tr.localAmount,
					tr.benAgentID,
					tr.transaction_notes,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					cp.cp_corresspondent_name,
					cp.cp_city
				from 
					transactions as tr
					LEFT JOIN cm_collection_point as cp ON tr.collectionPointID = cp.cp_id
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
				where
					tr.dispatchNumber = '".$_REQUEST["recId"]."' and
					transType = '".trim($_REQUEST["transType"])."'
					
				order by tr.transID	asc
					";
				//debug($strSql);	
		}
	}
	$fullRS = SelectMultiRecords($strSql);
	//debug($strSql);
	//debug($fullRS);

	$strCsvOutput = "";

	
	//$cpfseperator = array("-","/",".");
	$transseperator = "-";
	$lineseperator = "\r\n";
	
	for($i=0; $i < count($fullRS); $i++)
	{
		
	$_arrNote = explode("|",$fullRS[$i]["transaction_notes"]);
	//debug($_arrNote);
	//debug($fullRS[$i]["transDate"]."-->".$fullRS[$i]["currencyFrom"]."-->".$fullRS[$i]["transAmount"]."-->".$fullRS[$i]["transaction_notes"]);
	
		$beneficiaryFullName = $fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"];
		$senderFullName = $fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"];
		
		$strCsvOutput .=":20:".substr($fullRS[$i]["refNumberIM"],0,16).$lineseperator;
		$strCsvOutput .=":32A:".substr(date("ymd",strtotime($fullRS[$i]["transDate"])),0,6).substr($fullRS[$i]["currencyFrom"],0,3).
		substr(str_replace(".",",",$fullRS[$i]["transAmount"]),0,14).$lineseperator;
		$strCsvOutput .=":50k:".$lineseperator;
		$strCsvOutput .="XYZ PLC".$lineseperator;
		$strCsvOutput .="20 SMITH ST".$lineseperator;
		$strCsvOutput .="BOURNEMOUTH".$lineseperator;
		$strCsvOutput .="GB".$lineseperator;
		$strCsvOutput .=":57A:"."//".substr($fullRS[$i]["swiftCode"],0,11).$lineseperator;
		$strCsvOutput .=":59:"."/".substr($fullRS[$i]["accNo"],0,34).$lineseperator;
		$strCsvOutput .=substr($beneficiaryFullName,0,40).$lineseperator;
		$strCsvOutput .=":70:".substr($_arrNote[2],0,35).$lineseperator;
		$strCsvOutput .= "-".$lineseperator;
	}
	//debug($strCsvOutput);
	echo $strCsvOutput;
	/* End of txt version */
}
?>
