<?php
/*
PHP Version: 5.x
Date / Time: 26-Jan-2007
Developed by Gohar Sultan
Contact me at contact2gohar@gmail.com
*/
class DynamicForms{
	var $ClassVar;
	var $Help;			//Public variable, Boolean for display internal process
	var $ViewLog;
	var $SetTag;
	var $OutPut;
	function DataClass($bool){
	// Autoload on any class start
		$return = false;
		if($bool){
			$DClass = new DataClass;
			$DClass->Help = $this->Help;
			$DClass->ViewLog = $this->ViewLog;
			$Dclass->ClassVar = $this->ClassVar;
			$return = $DClass;
		}
		return $return;
	}
	function SetPrefix($separator,$string){		// It sets HTML form field explode prefix and table/field name postions.
		$DClass = $this->DataClass(true);
		$return = $DClass->SetPrefix($separator,$string);
		$this->ClassVar['setprefix'] = $return;
		return $return;
	}
	function MysqlConnect($host, $user, $password, $db){
			$DClass = $this->DataClass(true);
			$this->ClassVar["db_identifier"] = $DClass->MysqlConnect($host, $user, $password, $db);
			$this->ClassVar["database"] = $db;
		return $this->ClassVar["db_identifier"];
	}
	function display($detail, $var,$val){  // print var as per type
		
		$detail = explode('::+',$detail);
		for($i = 0; $i < sizeof($detail); $i++){
			if(!isset($detail[$i]) || empty($detail[$i]) || is_null($detail[$i])){
				$detail[$i] = "Not Available";
			}
		}
		if($val){
			echo "<br><strong>Function:</strong> $detail[0] <br><strong>Description:</strong> $detail[1] <br> <strong>Example:</strong> $detail[2] <br>";
			if(is_array($var)){echo $caption.'<br><strong>Current Returns:</strong> <font color="#990000">'; print_r($var); echo '</font><br><font size="2px">For more information: <a href="http://www.gohar.googlepages.com">Synopsis</a><br></font><br>';}else{echo $caption.'<br><strong>Current Returns:</strong> <font color="#990000">'.$var.'</font><br><font size="2px">For more information: <a href="http://www.gohar.googlepages.com">Synopsis</a><br></font><br>';}
		}
		return $var;
	}
	function InsertForm($table){
		$DClass = $this->DataClass(true);
		$result = mysql_query($DClass->Select($table, '*',""),$this->ClassVar["db_identifier"]);
		if(@mysql_num_fields($result)!=true){echo "Fatal Error!";exit();}
		$num=mysql_num_fields($result);
		$numrows = mysql_num_rows($result);
			if(isset($this->SetTag)){
				$settags = $this->SetTag;
				$tags = array_keys($settags);
			}
			for($i=0;$i<=$num;$i++){		
				if(mysql_field_name($result,$i) != ""){
					if(in_array(mysql_field_name($result,$i),$tags)){
						$fieldnames[$table.'_'.mysql_field_name($result,$i)] = $settags[mysql_field_name($result,$i)];
					}else{				
						$fieldnames[$i] = $table.'_'.mysql_field_name($result,$i);
					}
					
				}
			}
			//$fieldnames[$i++] = 'submit';
			if(!isset($fieldnames['submit'])){
				$fieldnames['submit'] = array(type=>submit,value=>'Add New');
			}

				$fieldarray = $this->Fields($fieldnames,false);
				$formfields = array_keys($fieldarray);
			for($n=0;$n<=$num;$n++){
					$values[$x] = '<strong>'.mysql_field_name($result,$n).'</strong>';
				$x++;
					$values[$x] = $fieldarray[$formfields[$n]];
				$x++;
			}

			$values[0] = "";
			$values[1] = "<h1>New Form</h1>";
		$htmtable= $this->Wrapper($num+1,2, array(border=>1,width=>'500',align=>center), $values);
		$formstart = '<form name="insert_form" method="post" action="'.$_SERVER['PHP_SELF'].'?formstate=submit">';
		 $formend = '</form>';
		 if(isset($_GET['formstate'])){	
		 	$DClass->Insert($_POST);
			$DClass->Render();
			if($this->ClassVar['querystatus']){echo "Form has been submitted";}else{echo "Could not process";}
		 }
		 
		 $return = $formstart.$htmtable.$formend;
		 $this->OutPut['InsertForm'] = $return;
		 return $return;
	}
	 function GetDBInfo(){ // Gets information for internal functions
		$db = $this->ClassVar["database"];
		$resulttables = mysql_list_tables($db);
		$numoftable = mysql_num_rows($resulttables);
				for($n=0; $n<=$numoftable; $n++){
					$tablename = mysql_tablename($resulttables,$n);
					$resultfields = mysql_list_fields($db, $tablename);
					$numoffields = mysql_num_fields($resultfields);
					if($tablename != ""){
						for($x=0; $x<=$numoffields; $x++){
							$fieldname = mysql_field_name($resultfields,$x);
							$fieldtype = mysql_field_type($resultfields,$x);
								if($db != "" && $tablename != "" && $fieldname != ""){
									$dbout[$tablename][$x] = $fieldname;
								}
						}
					}
				}
		$return = $dbout;
		$this->DataClass(true);
		$this->display('GetDBInfo( dbidentifier [resource] )::+ This function return multidimetional array with database, table names and field names. ::+ ClassInstance->GetDBInfo( dbidentifier );', $return, $this->Help);
		return $return;
	}
	function UpdateForm($table,$wherearray){
		$dbinfo = $this->GetDBInfo();
		$fieldsarray = $dbinfo[$table];
		//print_r($fieldsarray);
		$DClass = $this->DataClass(true);
		if(!$this->ClassVar['setprefix']){
			$arrayprefix = $this->SetPrefix("_","table_field"); //Default form field prefix
		}else{
			$arrayprefix = $this->ClassVar['setprefix'];
		}
		if(isset($this->SetTag)){$settags = $this->SetTag;$tags = array_keys($settags);} // Checks if SetTag is Set
//			print_r($fieldsarray);
			
			
			for($i=0; $i<sizeof($fieldsarray);$i++){
					$fieldnames[$arrayprefix['strprefix'].$arrayprefix['separator'].$arrayprefix['table'].$arrayprefix['separator'].$fieldsarray[$i]] = array(value => mysql_result($result,0,$fieldsarray[$i]));;
			}
			if(isset($settags['submit'])){$fieldnames['submit'] = $settags['submit'];}else{$fieldnames['submit'] = array(type=>submit,value=>'Update');}
			print_r($fieldnames);
			/// WORK HALT ON THIS POINT
			// need to redo the prefix evaluator 01/Feb/07
/*
				$formfieldarray = $this->Fields($fieldsarray,false);
				$formfields = array_keys($formfieldarray);
			for($n=0;$n<=$num;$n++){
					$values[$x] = '<strong>'.$fieldsarray[$n].'</strong>';
				$x++;
					$values[$x] = $formfieldarray[$formfields[$n]];
				$x++;
			}
				print_r($values);
			$values[0] = "";
			$values[1] = "<h1>Edit Form</h1>";
		$htmtable= $this->Wrapper($num+1,2, array(border=>1,width=>'500',align=>center), $values);
		$formstart = '<form name="update_form" method="post" action="'.$_SERVER['PHP_SELF'].'?formstate=submit">';
		 $formend = '</form>';
		 
		 
		 if(isset($_GET['formstate'])){	  //This code will run on Submit
		 	$DClass->Update($_POST,$wherearray);
			$DClass->Render();
			echo "Form has been submitted";
		 }
		 
		 $return = $formstart.$htmtable.$formend;
		 $this->OutPut['UpdateForm'] = $return;
*/
		//$arrayprefix['strprefix']
		//echo $arrayprefix['table'];
		//$this->ClassVar['field_pos'] = array_search('field',$strarray);
		return $return;
	
	}
	function UpdateFormv0($table,$wherearray){
		$DClass = $this->DataClass(true);
		$result = mysql_query($DClass->Select($table, '*',$wherearray),$this->ClassVar["db_identifier"]);
		if(@mysql_num_fields($result)!=true){echo "Fatal Error!";exit();}
		$num=mysql_num_fields($result);
		$numrows = mysql_num_rows($result);
		
			if(isset($this->SetTag)){
				$settags = $this->SetTag;
				$tags = array_keys($settags);
			}
			for($i=0;$i<=$num;$i++){		
				if(mysql_field_name($result,$i) != ""){
					if(in_array(mysql_field_name($result,$i),$tags)){
						$fieldnames[$table.'_'.mysql_field_name($result,$i)] = $settags[mysql_field_name($result,$i)];
					}else{				
						$fieldnames[$table.'_'.mysql_field_name($result,$i)] = array(value => mysql_result($result,0,mysql_field_name($result,$i)));;
					}
					
				}
			}
			if(isset($settags['submit'])){
				$fieldnames['submit'] = $settags['submit'];
			}else{
				$fieldnames['submit'] = array(type=>submit,value=>'Update');
			}

				$fieldarray = $this->Fields($fieldnames,false);
				$formfields = array_keys($fieldarray);
			for($n=0;$n<=$num;$n++){
					$values[$x] = '<strong>'.mysql_field_name($result,$n).'</strong>';
				$x++;
					$values[$x] = $fieldarray[$formfields[$n]];
				$x++;
			}

			$values[0] = "";
			$values[1] = "<h1>Edit Form</h1>";
		$htmtable= $this->Wrapper($num+1,2, array(border=>1,width=>'500',align=>center), $values);
		$formstart = '<form name="update_form" method="post" action="'.$_SERVER['PHP_SELF'].'?formstate=submit">';
		 $formend = '</form>';
		 if(isset($_GET['formstate'])){	
		 	$DClass->Update($_POST,$wherearray);
			$DClass->Render();
			echo "Form has been submitted";
		 }
		 
		 $return = $formstart.$htmtable.$formend;
		 $this->OutPut['UpdateForm'] = $return;
		 return $return;
	}
	function Fields($array,$display){	
		if(is_array($array)){
			$array_keys = array_keys($array);
			for($i=0; $i<sizeof($array_keys); $i++){
			// Field name $array_keys[$i]
			//echo $array_keys[$i]."<br>";
				if(!is_array($array[$array_keys[$i]])){
					//
					$tagarray['name'] = $array[$array_keys[$i]];
				}else{
					$tagarray['name'] = $array_keys[$i];
					$arrayelement = $array[$array_keys[$i]];
					$arrayelementkeys = array_keys($arrayelement);
						for($n=0; $n<sizeof($arrayelementkeys); $n++){
							//tag $arrayelementkeys[$n]
							$tagarray[$arrayelementkeys[$n]] = $arrayelement[$arrayelementkeys[$n]];
							//echo $arrayelement[$arrayelementkeys[$n]];
						}						
				}
				if(!isset($tagarray['id'])){$tagarray['id'] = "field$i";}
							$formfield = $this->TagEval($tagarray);	
							$render .= $formfield;	
							if($tagarray['name'] != ""){
								$arrreturn[$tagarray['name']] = $formfield;
							}
							unset($tagarray);
				}

			}
		if($display){echo $render;}
		$return = $arrreturn;
		$this->DataClass(true);
		$this->display('Fields( array [form fields], bool [true / false] optional )::+ This function returns HTML rendered form from an Array, and Array values are field names.Use echo() / print() to view ::+ ClassInstance->Fields(array(firstname, lastname, country => array(type=>select, options => array(pakistan = PK, India => IND, UnitedStates => US),email, phone)) , true);', $return, $this->Help);

		return $return;
	}
function Wrapper($numoftr,$numoftd, $tabletags, $valarray){
	$tabletags['type'] = 'table';
	$val=0;
	for($i=0;$i<$numoftr;$i++){
		for($n=0;$n<$numoftd;$n++){
			$tds .= $this->TagEval(array(type=>td, value=>$valarray[$val]));
			$val++;
		}
		$output .= $this->TagEval(array(type=>tr, value=>$tds));
		unset($tds);
	}
	$tabletags['value'] = $output;
	$return = $this->TagEval($tabletags);
		$this->DataClass(true);
		$this->display('Wrapper( integer [Number of tr], integer [Per tr number of td], array [additional table tags], array [values in td])::+ This function is a wrapper for any type of html table renderings. Returns wrapped data ::+ ClassInstance->Wrapper(7,2, array(border=>3,width=>500),array(a,b,d,s,x,e,4,3));', $return, $this->Help);

	return $return;
}
function TagEval($array){
	$HTMtagarray = array(textarea,select,table,tr,td,div,span,options);
	$SetTag = $array;
	if(in_array($SetTag['type'],$HTMtagarray)){
		$fieldtag="\n<";
	}else{
		$fieldtag="\n\t<input ";
	}
$Tags = array_keys($SetTag);
for($i=0; $i<sizeof($Tags); $i++){
	if($Tags[$i] != 'type'){
		//echo $Tags[$i].' == '.$SetTag[$Tags[$i]]."<br>";
		if(!in_array($SetTag['type'],$HTMtagarray) && $Tags[$i] != 'value'){
			if(!is_array($SetTag[$Tags[$i]])){
				$fieldtags .= ' '.$Tags[$i].' ="'.$SetTag[$Tags[$i]].'"';
			}
		}elseif($Tags[$i] != 'value'){
				$fieldtags .= ' '.$Tags[$i].' ="'.$SetTag[$Tags[$i]].'"';
		}
	}else{
		if(in_array($SetTag['type'],$HTMtagarray)){
			 $fieldtype = $SetTag[$Tags[$i]];
		}else{
			$fieldtype = $Tags[$i].'="'.$SetTag[$Tags[$i]].'"';
		}
	}
	if($SetTag['type'] == 'select' && $Tags[$i] == 'options' && is_array($SetTag[$Tags[$i]])){
		$optionsarray = $SetTag[$Tags[$i]];
		 $optionsarray_keys= array_keys($optionsarray);
		for($n=0; $n<sizeof($optionsarray_keys); $n++){
			$option = '<option value="'.$optionsarray[$optionsarray_keys[$n]].'">'.$optionsarray_keys[$n].'</option>';
			$options .= $option; 
		}

		$SetTag['value'] = $options;
	}
}
	$fieldtag .= $fieldtype.$fieldtags;
	if(in_array($SetTag['type'],$HTMtagarray)){
		$fieldtag.=">".$SetTag['value']."\n</".$SetTag['type'].">";
	}else{
		$fieldtag.=' value="'.$SetTag['value'].'">';
	}
		$this->DataClass(true);
		$this->display('TagEval( array [field tags] )::+ It generates Html form fields and resloves its tags in array ::+ ClassInstance->TagEval(array(firstname, lastname, country => array(type=>select, options => array(pakistan = PK, India => IND, UnitedStates => US),email, phone)));', $return, $this->Help);
	return $fieldtag;
}

}
class DataClass{
	var $ClassVar;	
	var $Reserve;					//Public variable
	var $Help;			//Public variable, Boolean for display internal process
	var $CascadeOrder = array();
	var $ViewLog;
	function display($detail, $var,$val){  // print var as per type
		$detail = explode('::+',$detail);
		for($i = 0; $i < sizeof($detail); $i++){
			if(!isset($detail[$i]) || empty($detail[$i]) || is_null($detail[$i])){
				$detail[$i] = "Not Available";
			}
		}
		if($val){
			echo "<br><strong>Function:</strong> $detail[0] <br><strong>Description:</strong> $detail[1] <br> <strong>Example:</strong> $detail[2] <br>";
			if(is_array($var)){echo $caption.'<br><strong>Current Returns:</strong> <font color="#990000">'; print_r($var); echo '</font><br><font size="2px">For more information: <a href="http://www.gohar.googlepages.com">Synopsis</a><br></font><br>';}else{echo $caption.'<br><strong>Current Returns:</strong> <font color="#990000">'.$var.'</font><br><font size="2px">For more information: <a href="http://www.gohar.googlepages.com">Synopsis</a><br></font><br>';}
		}
		return $var;
	}
	function MysqlConnect($host, $user, $password, $db){
		$this->ClassVar["db_identifier"] = mysql_connect($host,$user,$password);
		mysql_select_db($db) or die( "Unable to select database");
		$this->display('MysqlConnect( host, username, password )::+ This function establish a MySql database connection and returns its link_identifier to ClassVar["db_identifier"], <br /> <strong>IMPORTANT:</strong> System will not respond on mysql Errors / Messages without this function ::+ ClassInstance->MysqlConnect("localhost", "username", "password", "dbname")', $this->ClassVar["db_identifier"], $this->Help);
		if($db != ""){$this->ClassVar["database"] = $db;}else{echo "Invalid Database name.";exit();}
		return $this->ClassVar["db_identifier"];
	}
	function IfExists ($table, $wherearray){
		$result = mysql_query($this->Select($table, '*', $wherearray));
		$num=mysql_numrows($result);
		if($num == 0){ $return = false;}else{$return = true;}
		$this->display('IfExists( string [tablename], array / string [Where Clause])::+This function Returns Ture, if the value exists is db, and false on not found.::+ClassInstance->IfExists( tablename, array(id => 2))', $return, $this->Help);
		return $return;
	}
	function GetValue($query, $fieldname){
			$result=mysql_query($query);
			$num=mysql_num_rows($result);
			if ($nump=="0") {
				$return = false;
			}else {
					$return = mysql_result($result,0,$fieldname);
			}
			//Log Writing
	$Log .= '<font color="#FF0000">'.mysql_error($this->ClassVar["db_identifier"]).'</font><br>';
	$this->ViewLog = $Log.'<font size="2px">For more information: <a href="http://www.mysql.org">MySql</a><br></font>';

		$this->display('GetValue( string [sql query] , string [field name])::+This function gets the desired field value from db,  and writing Log on Error. Log is viewable by ClassInstance->ViewLog ::+ ClassInstance->GetValue()', $return, $this->Help);
			return $return;
	
	}
	function SetPrefix($separator,$string){		// It sets HTML form field explode prefix and table/field name postions.
		if(!$return){$return='';}
		$strarray = explode($separator,$string);
		$this->ClassVar['separator'] = $separator;
		$this->ClassVar['table_pos'] = array_search('table',$strarray);
		$this->ClassVar['field_pos'] = array_search('field',$strarray);
		if(!$this->ClassVar['separator']){$this->ClassVar['separator'] = "_";}		//Defaults
		if(!$this->ClassVar['table_pos']){$this->ClassVar['table_pos'] = 0;}
		if(!$this->ClassVar['field_pos']){$this->ClassVar['field_pos'] = 1;}
		$array['table'] = $this->ClassVar['table_pos'];
		$array['field'] = $this->ClassVar['field_pos'];
		$array['separator'] = $this->ClassVar['separator'];
			for($i=0;$i<sizeof($strarray);$i++){
				if($strarray[$i] != 'table'){
					if($strarray[$i] != 'field'){
						$strprefix .= $strarray[$i]. $separator;
					}
				}
			}
			$array['strprefix'] = $strprefix;
		$return = $this->display('SetPrefix( string separator, string html form field name)::+Using this function you can define prefix method of you HTML form fields. <br>keyword: table for table name <br> field for table field name ::+ClassInstance->SetPrefix( string separator, string html form field name)', $array, $this->Help);
		return $array;
	}
	function Select($table, $array, $wherearray){
		//$query = "SELECT * FROM $tbl WHERE $FieldName1='$Val1' && $FieldName2='$Val2'";
		//for($i=0; $i<)
		if(is_array($array)){
				for($i=0; $i<sizeof($array); $i++){
					if($i != sizeof($array)-1){
						$fields .= $array[$i].',';
					}else{
						$fields .= $array[$i];			
					}
				}
		}else{
			$fields = $array;
		}
		if(is_array($wherearray)){
			$warray = array_keys($wherearray);
			for($i=0; $i<sizeof($warray); $i++){
				if($i != sizeof($warray)-1){
					$where .= $warray[$i]." = '".$wherearray[$warray[$i]]."', ";
				}else{
					$where .= $warray[$i]." = '".$wherearray[$warray[$i]]."' ";
				}
			}
		}else{
			$where = $wherearray;
		}
		if($wherearray != ""){
			$query = "SELECT $fields FROM $table WHERE $where";
		}else{
			$query = "SELECT $fields FROM $table";
		}
		$return = $query;
		$this->display("Select( string [tablename] , array / string [fieldnames] , Array hash / string [Where Clause] )::+This function generates MySql Select query dynamically.::+ ClassInstance->Select(tablename , '*', array(id => 2))", $return, $this->Help);
	$this->ClassVar['query'][$table] = $return;
	return $return;
	}
	function Insert($Method){	// This function builds INSERT queries for single / multiple tables / Cascade Inserts
		$reswords = array("Submit","PHPSESSID");
		if(!isset($this->Reserve)){$this->Reserve = $reswords;}
		$this->display('Reserve = array ::+This function sets the Reserved words array ', $reswords, $this->Help);
		if(!isset($Method)){
			$Method=$_POST;
		}
		$TotalValues = array_keys($Method);
	   
		$TotalValues=array_diff($TotalValues,$reswords);
		sort($TotalValues);
		$i=0;	$n=0;									    //QLOGIC
	   while($i<sizeof($TotalValues)){
		 $FString=explode($this->ClassVar['separator'],$TotalValues[$i]);
			 if(substr_count($TotalValues[$i],$this->ClassVar['separator']) != 0){		// Checks if Array length is not equal to 0, after explode
				$TableNameArr[$FString[$this->ClassVar['table_pos']]][$n]=$FString[$this->ClassVar['field_pos']];
				 $Method[$FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]] = $Method[$TotalValues[$i]];
			//	echo $FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]." ==> ".$Method[$TotalValues[$i]]."<br>";
				//echo $FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]."<br>";
				
				$n++;
			 }
		$i++;
	   }
	   $ArrKeys=array_keys($TableNameArr);
	//	print_r($Method);
		$a=0;
		while($a<count($TableNameArr)){
			sort($TableNameArr[$ArrKeys[$a]]);
				$QValues = "";
				$QFields = "";
				if(count($TableNameArr[$ArrKeys[$a]]) == 1){
				  $Arr = array_keys($TableNameArr[$ArrKeys[$a]]);
				  $p=0;
				  while($p<=$Arr[0]){
					if($TableNameArr[$ArrKeys[$a]][$p] != ""){
						$Tmp=$TableNameArr[$ArrKeys[$a]][$p];
							unset($TableNameArr[$ArrKeys[$a]][$p]);
							$TableNameArr[$ArrKeys[$a]][0]=$Tmp;
					}
					$p++;
					}
				}
//	print_r($TableNameArr[$ArrKeys[$a]]);
			$x=0;
			while($x<=sizeof($TableNameArr[$ArrKeys[$a]])){
//			echo "<br> --  ".$ArrKeys[$a]."  ==>  ".$TableNameArr[$ArrKeys[$a]][$x]." ==> ".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."<br>";
				if($x != sizeof($TableNameArr[$ArrKeys[$a]])-1){
					if($TableNameArr[$ArrKeys[$a]][$x] != ""){
						$QFields = $QFields." ".$TableNameArr[$ArrKeys[$a]][$x]." ,";
						$QValues = $QValues." '".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."' ,";
					}
				}else{
					if($TableNameArr[$ArrKeys[$a]][$x] != ""){
						$QFields = $QFields." ".$TableNameArr[$ArrKeys[$a]][$x];
						$QValues = $QValues." '".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."' ";
					}
				}
			$x++;
			}
		$FinalQuery = "INSERT INTO ".$ArrKeys[$a]." ( ".$QFields." ) VALUES ( ".$QValues." )";
		$this->ClassVar['query'][$ArrKeys[$a]] = $FinalQuery;
		$return.=$FinalQuery."\n";
	$a++;
	}
	$this->display('Insert( Method )::+This function generates MySql Insert queries dynamically. We need to define the Method of Posted Values.<br><strong>Default Method:</strong> POST::+ ClassInstance->Insert($_POST)', $return, $this->Help);
		return $return;
	
	}
	
		function Update($Method, $Record){	// This function builds Update queries for single / multiple tables / Cascade Updates
		if(!$this->ClassVar['separator']){$this->ClassVar['separator'] = "_";}		//Defaults
		if(!$this->ClassVar['table_pos']){$this->ClassVar['table_pos'] = 0;}
		if(!$this->ClassVar['field_pos']){$this->ClassVar['field_pos'] = 1;}
		$reswords = array("Submit","PHPSESSID");
		if(!isset($this->Reserve)){$this->Reserve = $reswords;}
		$this->display('Reserve = array ::+This function sets the Reserved words array ', $reswords, $this->Help);
		if(!isset($Method)){
			$Method=$_POST;
		}
		$TotalValues = array_keys($Method);
	   
		$TotalValues=array_diff($TotalValues,$reswords);
		sort($TotalValues);
		$i=0;	$n=0;									    //QLOGIC
	   while($i<sizeof($TotalValues)){
		 $FString=explode($this->ClassVar['separator'],$TotalValues[$i]);
			 if(substr_count($TotalValues[$i],$this->ClassVar['separator']) != 0){		// Checks if Array length is not equal to 0, after explode
				$TableNameArr[$FString[$this->ClassVar['table_pos']]][$n]=$FString[$this->ClassVar['field_pos']];
				 $Method[$FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]] = $Method[$TotalValues[$i]];
			//	echo $FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]." ==> ".$Method[$TotalValues[$i]]."<br>";
				//echo $FString[$this->ClassVar['table_pos']].'_'.$FString[$this->ClassVar['field_pos']]."<br>";
				
				$n++;
			 }
		$i++;
	   }
	   $ArrKeys=array_keys($TableNameArr);
	//	print_r($Method);
		$a=0;
		while($a<count($TableNameArr)){
			sort($TableNameArr[$ArrKeys[$a]]);
				$Qoutput ="";
				if(count($TableNameArr[$ArrKeys[$a]]) == 1){
				  $Arr = array_keys($TableNameArr[$ArrKeys[$a]]);
				  $p=0;
				  while($p<=$Arr[0]){
					if($TableNameArr[$ArrKeys[$a]][$p] != ""){
						$Tmp=$TableNameArr[$ArrKeys[$a]][$p];
							unset($TableNameArr[$ArrKeys[$a]][$p]);
							$TableNameArr[$ArrKeys[$a]][0]=$Tmp;
					}
					$p++;
					}
				}
//	print_r($TableNameArr[$ArrKeys[$a]]);
			$x=0;
			while($x<=sizeof($TableNameArr[$ArrKeys[$a]])){
//			echo "<br> --  ".$ArrKeys[$a]."  ==>  ".$TableNameArr[$ArrKeys[$a]][$x]." ==> ".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."<br>";
				if($x != sizeof($TableNameArr[$ArrKeys[$a]])-1){
					if($TableNameArr[$ArrKeys[$a]][$x] != ""){
						$Qoutput = $Qoutput." ".$TableNameArr[$ArrKeys[$a]][$x]." = '".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."' ,";
					}
				}else{
					if($TableNameArr[$ArrKeys[$a]][$x] != ""){
						$Qoutput = $Qoutput." ".$TableNameArr[$ArrKeys[$a]][$x]." = '".$Method[$ArrKeys[$a]."_".$TableNameArr[$ArrKeys[$a]][$x]]."' ";
					}
				}
			$x++;
			}
			$fieldnames=array_keys($Record);
			unset($where);
			
			if(sizeof($fieldnames) == 1){
				$where .= " ".$fieldnames[0]." = '".$Record[$fieldnames[0]]."' ";
			}else{
				for($n=0; $n<sizeof($fieldnames); $n++){
					if($n != sizeof($fieldnames)-1){
						$where .= " ".$fieldnames[$n]." = '".$Record[$fieldnames[$n]]."' AND";
					}else{
						$where .= " ".$fieldnames[$n]." = '".$Record[$fieldnames[$n]]."' ";
					}
				}
			}
		$FinalQuery = "UPDATE ".$ArrKeys[$a]." SET ".$Qoutput." WHERE ".$where;
		$this->ClassVar['query'][$ArrKeys[$a]] = $FinalQuery;
//		echo $ArrKeys[$a];
		$return.=$FinalQuery."\n";
	$a++;
	}
	$this->display('Update( Method, Record id )::+This function generates MySql Update queries dynamically. We need to define the Method of Posted Values and send a is value.<br><strong>Default Method:</strong> POST::+ ClassInstance->Update($_POST , 1)', $return, $this->Help);
		return $return;
	
	}
	function Delete($table,$array){
		$fieldnames=array_keys($array);
			if(sizeof($fieldnames) == 1){
				$where .= " ".$fieldnames[0]." = '".$array[$fieldnames[0]]."' ";
			}else{
				for($n=0; $n<sizeof($fieldnames); $n++){
					if($n != sizeof($fieldnames)-1){
						$where .= " ".$fieldnames[$n]." = '".$array[$fieldnames[$n]]."' AND";
					}else{
						$where .= " ".$fieldnames[$n]." = '".$array[$fieldnames[$n]]."' ";
					}


				}
			}

		$query="DELETE FROM $table WHERE ".$where;
		$this->display('Delete( tablename, array )::+This function generates MySql Delete query dynamically. Params are table name and array, array have field name as array key and value as array value::+ ClassInstance->Delete(tablename , array(id => 2))', $query, $this->Help);
		$this->ClassVar['query'][$table] = $query;
	return $query;
	}
	function Render(){
		$NumofQuries = array_keys($this->ClassVar['query']);
		for($i=0; $i<=sizeof($NumofQuries); $i++){
			if($this->CascadeOrder){
					//echo $this->ClassVar['insert'][$this->CascadeOrder[$i]]."<br>";
					$queryresult = mysql_query($this->ClassVar['query'][$this->CascadeOrder[$i]]);				
			}else{
					//echo $this->ClassVar['query'][$NumofQuries[$i]]."<br>";
					$queryresult = mysql_query($this->ClassVar['query'][$NumofQuries[$i]]);				
			}
//print($this->ClassVar['query']);
	//echo $this->ClassVar['query']['person'];
			//Log Writing
			$Log .= '<font color="#FF0000">'.mysql_error($this->ClassVar["db_identifier"]).'</font><br>';
			$this->ViewLog = $Log.'<font size="2px">For more information: <a href="http://www.mysql.org">MySql</a><br></font>';
			
		}
		$this->display('Render()::+This function execute the quires in the Query Queue and writing Log on Error. Log is viewable by ClassInstance->ViewLog ::+ ClassInstance->Render()', "Returns Resource id if Sucessfull", $this->Help);
		$return = $queryresult;
		$this->ClassVar['querystatus'] = $queryresult;
		return $return;
	}
}


?>