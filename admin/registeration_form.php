<?php 
/**
 * @package Payex
 * @subpackage Sender Registration
 * This page is used for registration Sender from the Premeir Exchange Website
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Customer Registeration</title>
<style>
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed,
	figure, figcaption, footer, header, hgroup,
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
	}
	/* HTML5 display-role reset for older browsers */
	article, aside, details, figcaption, figure,
	footer, header, hgroup, menu, nav, section {
	display: block;
	}
	body {
	line-height: 1;
	}
	ol, ul {
	list-style: none;
	}
	blockquote, q {
	quotes: none;
	}
	blockquote:before, blockquote:after,
	q:before, q:after {
	content: '';
	content: none;
	}
	table {
	border-collapse: collapse;
	border-spacing: 0;
	}
	*{
		color: #9D9D98;
		font-family: 'PT Sans',Tahoma,Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;
		font-weight: 400;
		letter-spacing: 0;
		line-height: 20px;
		outline: medium none;
		resize: none;
	}
	.wrapper{
		width:960px;
		margin:0 auto;
	}
	.head1{
		color: #E6A300;
		font-family: 'Cabin';
		font-size: 20px;
		font-weight: 400;
		line-height: 20px;
		margin: 15px 0;
		font-size:24px;
	}
	.form_container{
		border: 1px solid #CCCCCC;
		padding: 19px 150px;
		overflow:hidden;
	}
	.form_container input{
	 	background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		height: 28px;
		line-height: 28px;
		margin: 0;
		padding: 0 7px;
	}
	.form_container p{
		color: #E6A300;
		line-height: 9px;
		margin: 0;
		padding: 0 0 10px;
	}
	.form_container select{
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		height: 30px;
		line-height: 30px;
		margin: 0;
		padding: 4px;
	}
	label.error{
		color:#FF4B4B;
		font-size:11px;
		font-weight:bold;
		display:block;
		margin-top:5px;
		font-family:arial;
	}
	.row{
		padding:5px 0;
		overflow:hidden;
		margin:10px 0;
	}
</style>
<link href="css/datePicker.css" rel="stylesheet" type="text/css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="javascript/jquery.validate.js" type="text/javascript"></script>
<script src="javascript/date.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
<script>
	$(document).ready(		
		function(){
			// Date Picker
			Date.format = 'yyyy-mm-dd';
			$('#dob').datePicker({clickInput:true,createButton:false});
			$('#dob').dpSetStartDate('1960-01-01');
			
			$('#passportExpiry').datePicker({clickInput:true,createButton:false});
			//$('#passportExpiry').dpSetStartDate('');
			
			$("#senderRegistrationForm").validate({
				rules:{
					landline: {
						required:true,
						minlength: 11,
						digits: true
					},
					postcode: {
						required:true
					},
					passportExpiry:{
						required:true
					}
				},
				submitHandler: function(){
					register();
					return false;
				}
			});
			$("#email").blur(
				function(){
					var email = $('#email').val();
					$.ajax({
						url: "registration_process_pdf.php",
						data: {
							email: email,
							chkEmailID: '1'
						},
						type: "POST",
						cache: false,
						success: function(data){
							$('#emailValidator').html(data);
						}
					})
				}
			);
			
			function register(){
				if($('#passportNum1').val() == '' || $('#passportNum1').length < 10){
					alert('Enter Valid Passport Number.');
					$('#passportNum1').focus();
					return false;
				}
				else if($('#passportNum2').length < 3){
					alert('Enter Valid Passport Number.');
					$('#passportNum2').focus();
					return false;
					}
					else if($('#passportNum3').val() == '' || $('#passportNum3').length < 7){
							alert('Enter Valid Passport Number.');
							$('#passportNum3').focus();
							return false;
						}
						else if($('#passportNum4').val() == ''){
								alert('Enter Valid Passpoert Number.');
								$('#passportNum4').focus();
								return false;
							}
							else if($('#passportNum5').val() == '' || $('#passportNum5').length < 7){
									alert('Enter Valid Passport Number.');
									$('#passportNum5').focus();
									return false;
								}
								else if($('#passportNum6').val() == '' || $('#passportNum6').length < 14){
										alert('Enter Valid Passport Number.');
										$('#passportNum6').focus();
										return false;
									}
									else if($('#passportNum7').val() == '' || $('#passportNum7').lehgth < 2){
										alert('Enter Valid Passport Number.');
										$('#passportNum7').focus();
										return false;
									}
				var data = $('#senderRegistrationForm').serialize();
				data += "&register=Register";
				$.ajax({
					url: "registration_process_pdf.php",
					data: data,
					type: "POST",
					cache: false,
					success: function(msg){
						if(msg == 'Email has been sent to sender')
							$('#senderRegistrationForm').reset();
						$('#msg').html(msg);
					}
				});
			}	
			
			/*$('#senderRegistrationForm').click(
				function(){
					var data = $('#senderRegistrationForm').serialize();
					$.ajax({
						url: "registration_process_pdf.php",
						data: data,
						type: "POST",
						cache: false,
						success: function(msg){
							$('#msg').html(msg);
						}
					});
				}
			);*/
			
			
			$('#searchAddress').click(
				function(){
					searchAddress();
				}
			);
			
			/*$('#passportNum4').blur(
				function(){
					passport = $(this).val();
					$.ajax({
						url: 'http://arsbai09.dev.hbstech.co.uk/api/gbgroup/passportcheck.php',
						data: {
							number: passport
						},
						type: 'POST',
						cache: false,
						success: function(data){
							alert(data);
						}
					})
				}
			);*/
			
		}
	);
	
	function getAddressData(ele)
	{
	//window.location="http://arsbai09.dev.hbstech.co.uk/api/gbgroup/addresslookupCus.php?a="+ele.value;
		var value = ele.value;
		var arrAddress = value.split('/');
		var buildingNumber = $.trim(arrAddress[0]);
		var buildingName = $.trim(arrAddress[1]);
		var subBuilding = $.trim(arrAddress[2]);
		var street = $.trim(arrAddress[3]);
		var subStreet = $.trim(arrAddress[4]);
		var town = $.trim(arrAddress[5]);
		var postcode = $.trim(arrAddress[6]);
		var organization = $.trim(arrAddress[7]);
		var buildingNumberVal = '';
		var buildingNameVal = '';
		var streetValue = '';
		if(buildingNumber != '')
			buildingNumberVal += buildingNumber;
		if(buildingName != '')
			buildingNameVal += buildingName;
		if(subBuilding != '')
			buildingNameVal += ' '+subBuilding;
		
		if(street != '')
			streetValue += street;
		if(subStreet != '')
			streetValue += ' '+subStreet;
			
		$('#buildingNumber').val(buildingNumberVal);
		$('#buildingName').val(buildingNameVal);
		$('#street').val(streetValue);
		$('#town').val(town);
		$('#postcode').val(postcode);
	}
	
	function enterToSearch(e){
		if(e.which){
			keyCode = e.which;
			if(keyCode == 13){
				e.preventDefault();
				searchAddress();
			}
		}
	}
	
	function searchAddress(){
		$('#addressContainer').fadeOut('fast');
		postcode = $.trim($('#postcode').val());
		buildingNumber = $.trim($('#buildingNumber').val());
		street = $.trim($('#street').val());
		town = $.trim($('#town').val());
		if(postcode == ''){
			alert("Enter postcode to search address!!");
			$('#postcode').focus();
			return;
		}
		/*$('#buildingNumber').val('');
		$('#street').val('');
		$('#town').val('');*/
		
		//country = $('#residenceCountry').val();
		$.ajax({
			url: "http://premierexchange.test.hbstech.co.uk/api/gbgroup/addresslookupCus.php",
			data: {
				postcode: postcode,
				buildingNumber: buildingNumber,
				street: street,
				town: town
			},
			type: "POST",
			cache: false,
			success: function(data){
				//alert(data.match(/option/i));
				if(data.match(/option/i)){
					$('#addressContainer').fadeIn('fast');
					$('#suggesstions').html(data);
				}
			}
		});
	}
	
</script>
</head>
<body>
	<div class="wrapper">
		<h1 class="head1">Create an Account</h1>
		<div class="form_container">
			<form name="senderRegistrationForm" id="senderRegistrationForm" action="" method="post" >
				<div class="row">
					<div style="float:left;">
						<p>Title</p>
						<select name="title" style="height:30px; width:96px;">
							<option value="Mr.">Mr.</option>
							<option value="Mrs">Mrs</option>
							<option value="Miss.">Miss.</option>
							<option value="Dr.">Dr.</option>
							<option value="Prof.">Prof.</option>
							<option value="Eng.">Eng.</option>
							<option value="Herr">Herr</option>
							<option value="Frau">Frau</option>
							<option value="Ing.">Ing.</option>
							<option value="Mag.">Mag.</option>
							<option value="Sr.">Sr.</option>
							<option value="Sra.">Sra.</option>
							<option value="Srta.">Srta.</option>
						</select>
					</div>
					<div style="float:left;margin-left:20px;">
						<p>Forename <em>*</em></p>
						<input name="forename" type="text" style="width:178px;" id="forename" class="required"/>
					</div>
					<div style="float:left;margin-left:20px;">
						<p>SurName <em>*</em></p>
						<input name="surname" type="text" style="width:294px;" id="surname" class="required"/>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px">
						<p>Date of Birth<em>*</em></p>
						<input name="dob" id="dob" type="text" style="width:275px;margin-right:5px;" readonly="readonly" class="required"/>
					</div>
					<div>
						<p>Gender</p>
						<select name="gender" style="width:250">
							<option value="male">Male</option>
							<option value="female">Female</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div style="float:left;margin-right:20px">
						<p>Country of Birth</p>
						<select style="width:310px;" name="birthCountry" id="birthCountry">
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="Germany">Germany</option>
							<option value="USA">USA</option>
							<option value="Canada">Canada</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'Ivoire">Cote D'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
							<option value="Denmark">Denmark</option>
							<option value="Djiboutio">Djiboutio</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Escocia">Escocia</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadaloupe">Guadaloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kitts and Nevis">Kitts and Nevis</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marinique">Marinique</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia">Micronesia</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Monserrat">Monserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option><option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Island">Pitcairn Island</option>
							<option value="Poland">Poland</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa-American">Samoa-American</option>
							<option value="Samoa-Western">Samoa-Western</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakie">Slovakie</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkemenistan">Turkemenistan</option>
							<option value="Turkey">Turkey</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Araba Emirates">United Araba Emirates</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Virgin Islands">Virgin Islands</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Yugoslavia">Yugoslavia</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
					<div style="float:left">
						<p>Country of Residence</p>
						<select style="width:310px;" name="residenceCountry" id="residenceCountry">
							<option value="United Kingdom">United Kingdom</option>
							<option value="Portugal">Portugal</option>
							<option value="Germany">Germany</option>
							<option value="USA">USA</option>
							<option value="Canada">Canada</option>
							<option value="Afghanistan">Afghanistan</option>
							<option value="Albania">Albania</option>
							<option value="Algeria">Algeria</option>
							<option value="Andorra">Andorra</option>
							<option value="Angola">Angola</option>
							<option value="Anguilla">Anguilla</option>
							<option value="Antarctica">Antarctica</option>
							<option value="Antigua and Barbuda">Antigua and Barbuda</option>
							<option value="Argentina">Argentina</option>
							<option value="Armenia">Armenia</option>
							<option value="Aruba">Aruba</option>
							<option value="Australia">Australia</option>
							<option value="Austria">Austria</option>
							<option value="Azerbaijan">Azerbaijan</option>
							<option value="Bahamas">Bahamas</option>
							<option value="Bahrain">Bahrain</option>
							<option value="Bangladesh">Bangladesh</option>
							<option value="Barbados">Barbados</option>
							<option value="Belarus">Belarus</option>
							<option value="Belgium">Belgium</option>
							<option value="Belize">Belize</option>
							<option value="Benin">Benin</option>
							<option value="Bermuda">Bermuda</option>
							<option value="Bhutan">Bhutan</option>
							<option value="Bolivia">Bolivia</option>
							<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
							<option value="Botswana">Botswana</option>
							<option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option>
							<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
							<option value="British Virgin Islands">British Virgin Islands</option>
							<option value="Brunei">Brunei</option>
							<option value="Bulgaria">Bulgaria</option>
							<option value="Burkina Faso">Burkina Faso</option>
							<option value="Burundi">Burundi</option>
							<option value="Cambodia">Cambodia</option>
							<option value="Cameroon">Cameroon</option>
							<option value="Canada">Canada</option>
							<option value="Cape Verde">Cape Verde</option>
							<option value="Cayman Islands">Cayman Islands</option>
							<option value="Central African Republic">Central African Republic</option>
							<option value="Chad">Chad</option>
							<option value="Chile">Chile</option>
							<option value="China">China</option>
							<option value="Christmas Island">Christmas Island</option>
							<option value="Cocos Keeling Islands">Cocos Keeling Islands</option>
							<option value="Colombia">Colombia</option>
							<option value="Comoros">Comoros</option>
							<option value="Congo">Congo</option>
							<option value="Cook Islands">Cook Islands</option>
							<option value="Costa Rica">Costa Rica</option>
							<option value="Cote D'Ivoire">Cote D'Ivoire</option>
							<option value="Croatia">Croatia</option>
							<option value="Cuba">Cuba</option>
							<option value="Cyprus">Cyprus</option>
							<option value="Czech Republic">Czech Republic</option>
							<option value="Democratic Republic of the Cong">Democratic Republic of the Cong</option>
							<option value="Denmark">Denmark</option>
							<option value="Djiboutio">Djiboutio</option>
							<option value="Dominica">Dominica</option>
							<option value="Dominican Republic">Dominican Republic</option>
							<option value="East Timor">East Timor</option>
							<option value="Ecuador">Ecuador</option>
							<option value="Egypt">Egypt</option>
							<option value="El Salvador">El Salvador</option>
							<option value="Equatorial Guinea">Equatorial Guinea</option>
							<option value="Eritrea">Eritrea</option>
							<option value="Escocia">Escocia</option>
							<option value="Estonia">Estonia</option>
							<option value="Ethiopia">Ethiopia</option>
							<option value="Falkland Islands">Falkland Islands</option>
							<option value="Faroe Islands">Faroe Islands</option>
							<option value="Fiji">Fiji</option>
							<option value="Finland">Finland</option>
							<option value="France">France</option>
							<option value="French Guiana">French Guiana</option>
							<option value="French Polynesia">French Polynesia</option>
							<option value="Gabon">Gabon</option>
							<option value="Gambia">Gambia</option>
							<option value="Georgia">Georgia</option>
							<option value="Germany">Germany</option>
							<option value="Ghana">Ghana</option>
							<option value="Gibraltar">Gibraltar</option>
							<option value="Greece">Greece</option>
							<option value="Greenland">Greenland</option>
							<option value="Grenada">Grenada</option>
							<option value="Guadaloupe">Guadaloupe</option>
							<option value="Guam">Guam</option>
							<option value="Guatemala">Guatemala</option>
							<option value="Guinea">Guinea</option>
							<option value="Guinea-Bissau">Guinea-Bissau</option>
							<option value="Guyana">Guyana</option>
							<option value="Haiti">Haiti</option>
							<option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
							<option value="Honduras">Honduras</option>
							<option value="Hong Kong">Hong Kong</option>
							<option value="Hungary">Hungary</option>
							<option value="Iceland">Iceland</option>
							<option value="India">India</option>
							<option value="Indonesia">Indonesia</option>
							<option value="Iran">Iran</option>
							<option value="Iraq">Iraq</option>
							<option value="Ireland">Ireland</option>
							<option value="Israel">Israel</option>
							<option value="Italy">Italy</option>
							<option value="Jamaica">Jamaica</option>
							<option value="Japan">Japan</option>
							<option value="Jordan">Jordan</option>
							<option value="Kazakhstan">Kazakhstan</option>
							<option value="Kenya">Kenya</option>
							<option value="Kiribati">Kiribati</option>
							<option value="Kitts and Nevis">Kitts and Nevis</option>
							<option value="Kuwait">Kuwait</option>
							<option value="Kyrgyzstan">Kyrgyzstan</option>
							<option value="Laos">Laos</option>
							<option value="Latvia">Latvia</option>
							<option value="Lebanon">Lebanon</option>
							<option value="Lesotho">Lesotho</option>
							<option value="Liberia">Liberia</option>
							<option value="Libya">Libya</option>
							<option value="Liechtenstein">Liechtenstein</option>
							<option value="Lithuania">Lithuania</option>
							<option value="Luxembourg">Luxembourg</option>
							<option value="Macau">Macau</option>
							<option value="Macedonia">Macedonia</option>
							<option value="Madagascar">Madagascar</option>
							<option value="Malawi">Malawi</option>
							<option value="Malaysia">Malaysia</option>
							<option value="Maldives">Maldives</option>
							<option value="Mali">Mali</option>
							<option value="Malta">Malta</option>
							<option value="Marinique">Marinique</option>
							<option value="Marshall Islands">Marshall Islands</option>
							<option value="Mauritania">Mauritania</option>
							<option value="Mauritius">Mauritius</option>
							<option value="Mayotte">Mayotte</option>
							<option value="Mexico">Mexico</option>
							<option value="Micronesia">Micronesia</option>
							<option value="Moldova">Moldova</option>
							<option value="Monaco">Monaco</option>
							<option value="Mongolia">Mongolia</option>
							<option value="Monserrat">Monserrat</option>
							<option value="Morocco">Morocco</option>
							<option value="Mozambique">Mozambique</option>
							<option value="Myanmar">Myanmar</option>
							<option value="Namibia">Namibia</option>
							<option value="Nauru">Nauru</option>
							<option value="Nepal">Nepal</option>
							<option value="Netherlands">Netherlands</option>
							<option value="Netherlands Antilles">Netherlands Antilles</option>
							<option value="New Caledonia">New Caledonia</option>
							<option value="New Zealand">New Zealand</option>
							<option value="Nicaragua">Nicaragua</option>
							<option value="Niger">Niger</option>
							<option value="Nigeria">Nigeria</option>
							<option value="Niue">Niue</option>
							<option value="Norfolk Island">Norfolk Island</option>
							<option value="North Korea">North Korea</option>
							<option value="Northern Mariana Islands">Northern Mariana Islands</option>
							<option value="Norway">Norway</option><option value="Oman">Oman</option>
							<option value="Pakistan">Pakistan</option>
							<option value="Palau">Palau</option>
							<option value="Panama">Panama</option>
							<option value="Papua New Guinea">Papua New Guinea</option>
							<option value="Paraguay">Paraguay</option>
							<option value="Peru">Peru</option>
							<option value="Philippines">Philippines</option>
							<option value="Pitcairn Island">Pitcairn Island</option>
							<option value="Poland">Poland</option>
							<option value="Puerto Rico">Puerto Rico</option>
							<option value="Qatar">Qatar</option>
							<option value="Reunion">Reunion</option>
							<option value="Romania">Romania</option>
							<option value="Russia">Russia</option>
							<option value="Rwanda">Rwanda</option>
							<option value="S. Georgia and the S. Sandwich Is.">S. Georgia and the S. Sandwich Is.</option>
							<option value="Saint Helena">Saint Helena</option>
							<option value="Saint Lucia">Saint Lucia</option>
							<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
							<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
							<option value="Samoa-American">Samoa-American</option>
							<option value="Samoa-Western">Samoa-Western</option>
							<option value="San Marino">San Marino</option>
							<option value="Sao Tome and Principe">Sao Tome and Principe</option>
							<option value="Saudi Arabia">Saudi Arabia</option>
							<option value="Senegal">Senegal</option>
							<option value="Seychelles">Seychelles</option>
							<option value="Sierra Leone">Sierra Leone</option>
							<option value="Singapore">Singapore</option>
							<option value="Slovakie">Slovakie</option>
							<option value="Slovenia">Slovenia</option>
							<option value="Solomon Islands">Solomon Islands</option>
							<option value="Somalia">Somalia</option>
							<option value="South Africa">South Africa</option>
							<option value="South Korea">South Korea</option>
							<option value="Spain">Spain</option>
							<option value="Sri Lanka">Sri Lanka</option>
							<option value="Sudan">Sudan</option>
							<option value="Suriname">Suriname</option>
							<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
							<option value="Swaziland">Swaziland</option>
							<option value="Sweden">Sweden</option>
							<option value="Switzerland">Switzerland</option>
							<option value="Syria">Syria</option><option value="Taiwan">Taiwan</option>
							<option value="Tajikistan">Tajikistan</option>
							<option value="Tanzania">Tanzania</option>
							<option value="Thailand">Thailand</option>
							<option value="Togo">Togo</option>
							<option value="Tokelau">Tokelau</option>
							<option value="Tonga">Tonga</option>
							<option value="Trinidad and Tobago">Trinidad and Tobago</option>
							<option value="Tunisia">Tunisia</option>
							<option value="Turkemenistan">Turkemenistan</option>
							<option value="Turkey">Turkey</option>
							<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
							<option value="Tuvalu">Tuvalu</option>
							<option value="Uganda">Uganda</option>
							<option value="Ukraine">Ukraine</option>
							<option value="United Araba Emirates">United Araba Emirates</option>
							<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
							<option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option>
							<option value="Vatican City">Vatican City</option>
							<option value="Venezuela">Venezuela</option>
							<option value="Vietnam">Vietnam</option>
							<option value="Virgin Islands">Virgin Islands</option>
							<option value="Wallis and Futuna">Wallis and Futuna</option>
							<option value="Western Sahara">Western Sahara</option>
							<option value="Yemen">Yemen</option>
							<option value="Yugoslavia">Yugoslavia</option>
							<option value="Zambia">Zambia</option>
							<option value="Zimbabwe">Zimbabwe</option>
						</select>
					</div>
				</div>
				<fieldset style="border:1px solid #ccc;padding:0 8px;">
					<legend style="color: #E6A300;">Address</legend>
					<div class="row">
						<div style="float:left;margin-right:30px">
							<p>Postcode / Zipcode<em>*</em></p>
							<input name="postcode" id="postcode"  type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left;margin-right:30px;">
							<p>Building No.</p>
							<input name="buildingNumber" id="buildingNumber" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left;margin-right:30px;">
							<p>Building Name</p>
							<input name="buildingName" id="buildingName" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
					</div>
					<div class="row">
						<div style="float:left;margin-right:30px">
							<p>Street</p>
							<input name="street" id="street" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left;margin-right:30px">
							<p>Town</p>
							<input name="town" id="town" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
						<div style="float:left">
							<p>Province</p>
							<input name="province" id="province" type="text" style="width:150px;" onKeyPress='enterToSearch(event);'/>
						</div>
					</div>
					<div>
						<div style="padding:0 0 10px 0;margin:5px 100px 5px 0;display:none;float:left;" id="addressContainer">
							<p>Suggessted Address</p>
							<div id="suggesstions">
								
							</div>
						</div>
						<div style="float:right;margin:0 20px 20px 0;">
							<input type="button" id="searchAddress" value="Search Address" style="background:#ECB83A;color:#fff;border:1px solid #E5A000;" />
						</div>
						
					</div>
				</fieldset>				
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>Telephone - Landline<em>*</em></p>
						<input name="landline" type="text" style="width:294px;" id="landline" />
					</div>
					<div style="float:left;">
						<p>Telephone - Mobile</p>
						<input name="mobile" type="text" style="width:294px;"/>
					</div>
				</div>
				<fieldset style="border:1px solid #ccc;padding:0 8px;">
				<legend style="color:#E6A300">Passport</legend>
				<div class="row">
				<div style="float:left;margin-right:20px">
					<p>Passport Number<em>*</em></p>
					<div class="display:block;overflow:hidden;padding:5px;">
						<input name="passportNum1" type="text" size="8" id="passportNum1" maxlength="10" style="padding:0 2px"/>
						<em>-</em>
						<input name="passportNum2" id="passportNum2" value="" type="text" maxlength="3" style="padding:0 2px;width:25px;text-transform:uppercase"/>
						<em>-</em>
						<input name="passportNum3" id="passportNum3" value="" type="text" maxlength="7" style="padding:0;width:50px"/>
						<em>-</em>
						<input name="passportNum4" id="passportNum4" type="text" maxlength="1" style="padding:0 2px;width:15px;text-transform:uppercase"/>
						<em>-</em>
						<input name="passportNum5" id="passportNum5" size="4" type="text" maxlength="7" style="padding:0 2px;"/>
						<em>-</em>
						<input name="passportNum6" id="passportNum6" size="10" type="text" maxlength="14" style="padding:0 2px;"/>
						<input name="passportNum7" id="passportNum7" type="text" maxlength="2" style="padding:0 2px;width:15px;"/>
					</div>
				</div>
				<div style='float:left'>
					<p>Passport Expiry Date<em>*</em></p>
					<input name="passportExpiry" id="passportExpiry" value="" readonly="readonly" style="margin-right:5px;width:140px"/>
				</div>
				</div>
				</fieldset>
				<div class="row">
					<div style="float:left;margin-right:20px;">
						<p>National ID Card<em>*</em></p>
						<input name="nic" type="text" style="width:294px;" id="nic" class="required digits"/>
					</div>
					<div>
						<p>Email<em>*</em></p>
						<input style="width:300px;" name="email" type="text" id="email" class="required email"/>
						<div id="emailValidator" style="clear:both"></div>
						<input type="hidden" name="ipAddress" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>"/>
					</div>
				</div>
				<div style="margin-top:20px;overflow:hidden;padding:5px 0;text-align:center">
					<div>
						<input type="submit" name="register" value="Register" id="register" style="background:#ECB83A;border:1px solid #E5A000;color:#fff;font-size:14px;cursor:pointer;padding-bottom:5px"/>
					</div>
				</div>
				<div id="msg" style="text-align:center">
					
				</div>
			</form>
		</div>
	</div>
</body>
</html>
