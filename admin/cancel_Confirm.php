<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("ledgersUpdation.php"); 
//include ("double_entry_functions.php");
include ("double-entry-functions.php");
//double_entry_functions.php
include("connectOtherDataBase.php");

$transID = (int) $_GET["transID"];
///////////////////////History is maintained via method named 'activities'

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
global $isCorrespondent;
global $adminType;
$isCorrespondent  = $_SESSION["loggedUserData"]["isCorrespondent"];
$adminType  = $_SESSION["loggedUserData"]["adminType"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$get=$_GET["act"];
$today = date("Y-m-d");

$isAgentAnD = 'N';
$isBankAnD = 'N';


if(defined("CONFIG_API_TRANSACTIONS") && CONFIG_API_TRANSACTIONS == "CMT")
	require(LOCAL_DIR."api/gopi/api_functions.php");

if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("(LOCAL_DIR"))
	require LOCAL_PATH."api/etransact/soap_api.php";

function callApiCancelReturn($sftp, $intTransId)
{
	//debug_print_backtrace();
	$strXmlResponse = '';
	if(defined("CONFIG_API_TRANSACTIONS") && !empty($intTransId))
	{
		$sqlGetTransactionForResponse = "select transStatus, transType, transID, refNumber, refNumberIM, custAgentID, benAgentID, partner_id, localAmount, totalAmount, currencyFrom, currencyTo from transactions where from_server = '".CONFIG_API_TRANSACTIONS."' and is_parsed = 'Y' and transid='".$intTransId."' and holdedBy = '".CONFIG_API_TRANSACTIONS."'";
		
		$arrTransactionDet = selectFrom($sqlGetTransactionForResponse);
		
		//debug($arrTransactionDet);
		
		if(!empty($arrTransactionDet))
		{
			$strTransStatus = $arrTransactionDet["transStatus"];
		
			if($_REQUEST["changeStatusTo"] == "reject_paid") //transaction is already paid
				$intResposeCode = "30001";
			elseif($strTransStatus == "Authorize") // Cancel request is rejected (Reason other than paid)
				$intResposeCode = "30004";
			else //if($strTransStatus == "Cancelled") // transaction is confirmed to cancel
				$intResposeCode = "30000";
			
			$strXmlResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
								<TMTPayoutUpdateRequestResponse xmlns=\"tmt\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
									<Transaction>
										<tmtTransactionNumber>".$arrTransactionDet["refNumber"]."</tmtTransactionNumber>
										<updateType>01</updateType>
										<responseCode>".$intResposeCode."</responseCode>
										<processDateTime>".date("Y-m-d\TH:i:s")."</processDateTime>
										<partnerId>".$arrTransactionDet["partner_id"]."</partnerId>
									</Transaction>
								</TMTPayoutUpdateRequestResponse>
								";
			
			$strFileName = $arrTransactionDet["partner_id"]."_TMTURR_".date("dmYHis").".xml";
			
	
			//do{
				$bolFileNotWritten = false;
				if(!writeFileOnSftp($sftp, $strFileName, $strXmlResponse))
				{
					/* If file not written than try it again with a sllep time of 10 seconds */
					$bolFileNotWritten = true;
					echo "Unable to write '$strFileName' file, on client system, trying it again in next 10 seconds.<br />";
					//debug("Unable to write '$strFileName' file, on client system, trying it again in next 10 seconds.<br />", true);
					//sleep(10);
				}
			//}while($bolFileNotWritten);
			
			//debug(htmlentities($strXmlResponse), true);
		}
	}
}



if($_POST["Submit"] != "")
{
$remarks = $_POST["remarks"];

	if($_POST["Submit"] == "Submit")
	{
				$refund = $_POST["refundFee"];
				if (!$refund){
					$refundFees = "No";
				}else{ 
					$refundFees = "Yes";
					}
					
				$tranStat = selectFrom("select transStatus, transID, totalAmount, custAgentID, benAgentID from ". TBL_TRANSACTIONS." where transID='". $_GET["transID"] ."' and transStatus !='Cancelled'");	
				//echo "select transStatus, transID, totalAmount, custAgentID, benAgentID from ". TBL_TRANSACTIONS." where transID='". $_GET["transID"] ."' and transStatus !='Cancelled'";
		if($tranStat["transID"] != ""){	
				if($_POST["changeStatusTo"]  == "Cancelled")		
				{
				 
					if(CONFIG_RETURN_CANCEL == '1')
					{
						$updateTrans = "update " . TBL_TRANSACTIONS . " set transStatus= 'Cancelled', cancelledBy = '".$username."',
				cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
				refundFee = '".$refundFees."',
				remarks		= '".$remarks."',
				cancelReason='".$remarks."' 
				where transID='".$tranStat["transID"]."'";
				
				update($updateTrans);							
						$printingMessage = "Transaction is Cancelled ";
						activities($_SESSION["loginHistoryID"],"UPDATION",$tranStat["transID"],TBL_TRANSACTIONS,$printingMessage);
						if(CONFIG_DIST_REF_NUMBER == "1" && CONFIG_REUSE_DIST_REF_NUMBER == "1")
						{
							removeDistRef($tranStat["transID"], "Transaction is Cancelled");
						}
					}else
					{
					
						 $tranStat = selectFrom("select transStatus, transID, totalAmount, custAgentID, benAgentID from ". TBL_TRANSACTIONS." where transID='". $_GET["transID"] ."' and transStatus !='Cancelled'");
						 $date = date("Y-m-d");
			 $transDetail = selectFrom ("SELECT * FROM ". TBL_TRANSACTIONS." where transID='".$tranStat["transID"] ."' ");
				$customerID = $transDetail["customerID"];
				$totalAmount = $transDetail["totalAmount"];
				
				$strQuery = "insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
									 values(".$customerID.",'".$date."','Transaction is Cancelled','DEPOSIT','".$totalAmount."','".$tranStat["transID"]."'
									 )";
									 debug($strQuery );
							  mysql_query($strQuery)or die("Invalid query 1: " . mysql_error());
							
		/************agent_company_account************/
				$strQuery = "insert into ".TBL_AGENT_COMPANY_ACCOUNT."( Date ,payment_mode,Type ,amount,tranRefNo) 
		 values( '".$date."','Transaction is Cancelled','WITHDRAW','".$totalAmount."','".$tranStat["transID"]."' )";
		 
		 debug($strQuery );
		 mysql_query($strQuery)or die("Invalid query 2: " . mysql_error());
					
					
						 
						 
						 
						 
					
					transStatusChange($_GET["transID"], "Cancelled", $username, $_POST["remarks"], $refundFees);
					
					if(CONFIG_SHARE_OTHER_NETWORK == '1')
					{
						
						
						$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
												
						if($remoteTransInfo["remoteTrans"] != '')
						{
							
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
							if($jointClient["clientId"] != '')
							{
								$otherClient = new connectOtherDataBase();
								$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
								
								
								$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and transStatus != 'Cancelled'");		
								
								if($otherTransaction["transID"] != '')
								{						
									if($refundFees == "No"){
										$balanceRefund = $otherTransaction["totalAmount"] - $otherTransaction["IMFee"];
									}else{
										$balanceRefund = $otherTransaction["totalAmount"];
										}
									
									$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
													transStatus = 'Cancelled', 
													cancelledBy = '".$username."',
													cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													refundFee = '".$refundFees."',
													remarks		= '".$_POST["remarks"]."',cancelReason='".$_POST["remarks"]."'
													where transID = '".$otherTransaction["transID"]."'
													");	
										
									$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
								
								
									if($remoteCustAgent["agentType"] == 'Sub')
									{
										$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $balanceRefund, $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $balanceRefund, $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}else{
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $balanceRefund, $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}				
									if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
									{
										$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
											
											if($remoteBenAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}else{
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Cancelled', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}	
									}
								}
								
							$otherClient->closeConnection();		
							
							dbConnect();
							}
							
						}
							
							
						
					}
					
				/**************************************************************************	
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', refundFee = '$refundFees', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
						
						$descript = "Transaction is Cancelled";
						activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);	
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, imfee, transAmount, localAmount, benAgentID, AgentComm, currencyTo, currencyFrom from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$imfee = $amount["imfee"];
					$agentComm = $amount["AgentComm"];
					if(CONFIG_EXCLUDE_COMMISSION == '1')
					{
						$balance = $amount["totalAmount"] - $agentComm;
					}else{
						$balance = $amount["totalAmount"];
						}
					if (!$refund){
					$balance = $balance - $imfee;
					}
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
							
							if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1' && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD)
								$currecyToLedger = $amount["currencyTo"];
							else
								$currecyToLedger = $amount["currencyFrom"];
							
							
							$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$agent'");
							

							
							$currentBalance = $agentContents["balance"];
							
							$currentBalance = $balance + $currentBalance;
							
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
							////////////////Either to insert into Agent or A&D Account////////
							
							////////////////Either to insert into Agent or A&D Account////////
							if($agentContents["agentType"]=="Sub")
							{
								$agent=$agentContents["parentID"];
								$q = updateAgentAccount($agent, $balance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Agent");
									 updateSubAgentAccount($amount["custAgentID"], $balance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Agent", $currecyToLedger);
							}else{
								$agent=$amount["custAgentID"];
								$q = updateAgentAccount($agent, $balance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Agent", $currecyToLedger);
							}
							//////
							
							
														
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1"){
							
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
					    values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$_GET["transID"]."','".$_SESSION["loggedUserData"]["userID"]."')");
				 			
							$balance = $amount["totalAmount"];
							$newBalance = $payin["balance"] + $balance ; 
							$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
							update($update_Balance);
							}
						
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$balance = $amount["totalAmount"];
						$strQuery = "insert into  customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);						 
						 
						    if(CONFIG_ONLINE_AGENT == '1')
							{	
								
								$descript = "Transaction is Cancelled";
								updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent');	
							}	
					}
					if(CONFIG_POST_PAID != '1')
					{
						$benAgentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$bank'");
						$currentBalance = $benAgentContents["balance"];
						
						
						/////////
						if($benAgentContents["agentType"]=="Sub")
						{
							$agent=$benAgentContents["parentID"];
							$q = updateAgentAccount($agent, $transBalance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Distributor");
								 updateSubAgentAccount($benAgentContents["userID"], $transBalance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Distributor");
						}else{
							$agent=$benAgentContents["userID"];
							$q = updateAgentAccount($agent, $transBalance, $_GET["transID"], "DEPOSIT", "Transaction Cancelled", "Distributor");
						}
						/////////
						
						$currentBalance = $transBalance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
							
						

					
					}**************************************************************/
					
				}
				}
				if( $_POST["changeStatusTo"]  == "Reject Cancel")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' , trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."',cancelReason='".$remarks."',cancelReason = '".$_POST["remarks"]."' where transID='". $_GET["transID"]."'");
					
					$descript = "Transaction is not Cancelled".
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);	
					
					if(CONFIG_SHARE_OTHER_NETWORK == '1')
					{
						
						
						$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
												
						if($remoteTransInfo["remoteTrans"] != '')
						{
							
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
							if($jointClient["clientId"] != '')
							{
								$otherClient = new connectOtherDataBase();
								$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
								$otherClient-> update("update ".$jointClient["dataBaseName"].".transactions set transStatus= 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' , trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."',cancelReason='".$_POST["remarks"]."' where transID='". $remoteTransInfo["remoteTrans"]."'");
					 
								$otherClient->closeConnection();		
							
							dbConnect();
							}
						}
					}
					

				}
				
				
				if( $_REQUEST["changeStatusTo"]  == "reject_paid")
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' , trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."',cancelReason='".$_POST["remarks"]."' where transID='". $_GET["transID"]."'");
					
					$descript = "Transaction is not Cancelled".
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);	
				}
				
				
				if(defined("CONFIG_API_TRANSACTIONS") && CONFIG_API_TRANSACTIONS == "CMT")
					callApiCancelReturn($sftp, $_REQUEST["transID"]);
				
				if(CONFIG_MIDDLE_TIER_CLIENT_ID == "3" && defined("(LOCAL_DIR"))
					cancelApiTransaction($_REQUEST["transID"]);

				include ("email.php");
			}
			if($tranStat["transID"]!=""){
				sendCustomMail($tranStat["transID"]);
			}
		
	}
	//debug("",true);
	redirect("cancel_Transactions.php?act=cancel");
}
if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
}
else
{
	redirect("cancel_Transactions.php?act=$get");
}
if($createdBy != 'CUSTOMER')
{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  	<tr>
		<td>
				<? 
					if($_GET["back"]=="cancel_Transactions"){?>
				 <a class="style2" href="cancel_Transactions.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
				 	<? } ?>
		</td>
	</tr>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0">
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="196" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="153" align="right"><font color="#005b90">Status</font></td>
                <td width="181"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90"><? echo $systemCode; ?> </font></td>
                <td width="196" height="20"><? echo $contentTrans["refNumberIM"]?>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90"><? echo $manualCode; ?> </font> </td>
                <td width="181" height="20"><? echo $contentTrans["refNumber"]; ?></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="196" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="181" height="20"><? 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$authoriseDate"));				
				?></td>
              </tr>			  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table width="140" border="0" align="center">
              <tr>
                <td width="323" height="80" colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table border="0">
                <? 
						if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentTrans["transID"]."' and generatedLocally = 'N'");   
            }   
						if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentTrans["customerID"]."'");		
							$benificiaryContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentTrans["benID"]."'");		
									$otherClient->closeConnection();
											dbConnect();									
						}else{
			
						$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
						$customerContent = selectFrom($queryCust);
						
						$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
						$benificiaryContent = selectFrom($queryBen);
					}
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="224"><? echo $customerContent["Email"]?></td>
                </tr>
			<?
				}
				else{
				?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">&nbsp;</font></td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>				
				<?
				}
			  }
			  ?>
              </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Beneficiary Agent Details</legend>
            <table border="0">
              <? 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["benAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			if($senderAgentContent["userID"] != "")
			{
		?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                <td><? echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><? echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Company</font></td>
                <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="222"><? echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="222"><? echo $senderAgentContent["email"]?></td>
              </tr>
              <?
			  }
			  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
            <table border="0" bordercolor="#006600">
              <? 
		
		
		
			
			if($benificiaryContent["benID"] != "")
			{
		?>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Beneficiary 
                  Name</font></td>
                <td height="20" colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                <td width="196" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Address</font></td>
                <td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                <td width="196" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Phone"]?></td>
                <td width="138" align="right"><font color="#005b90">Email</font></td>
                <td width="196"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from bankDetails where transID='".$transID."'");
					?>
              <tr> 
                <td width="156" height="20" class="style2">Beneficiary Bank Details </td>
                <td width="186" height="20">&nbsp;</td>
                <td width="138" height="20">&nbsp;</td>
                <td width="196" height="20">&nbsp;</td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">
					<?php
						/**
						  * #10301: Premier Exchange:Swift Code missing on view transaction details
						  * Short Description
						  * Change to change labels according to the Deal Contract Page
						  */
						if(CONFIG_FOR_CHANGE_LABELS_BANK_DETAILS == '1')
							echo "Beneficiary Account Name";
						else
							echo "Bank Name";
					?>
				</font></td>
                <td width="186" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                <td width="138" align="right"><font color="#005b90">Acc Number</font></td>
                <td width="196"><? echo $benBankDetails["accNo"]; ?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">
					<?php 
						/**
						  * #10301: Premier Exchange:Swift Code missing on view transaction details
						  * Short Description
						  * Change to change labels according to the Deal Contract Page
						  */
						if(CONFIG_FOR_CHANGE_LABELS_BANK_DETAILS == '1')
							echo "Branch Name/Number";
						else
							echo "Branch Code";
					?>
				</font></td>
                <td width="186" height="20"><? echo $benBankDetails["branchCode"]; ?> 
                </td>
                <td width="138" align="right"><font color="#005b90"> 
                  <?
				if($benificiaryContent["Country"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($benificiaryContent["Country"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                  </font></td>
                <td width="196">
                  <?
				if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
				{
				?>
                  <? echo $benBankDetails["ABACPF"]; ?> 
                  <?
				}
				?>
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Branch 
                  Address</font> </td>
                <td width="186" height="20"><? echo $benBankDetails["branchAddress"]; ?> 
                </td>
                <td width="138" height="20"  align="right"><font color="#005b90">
					<?php 
						/**
						  * #10301: Premier Exchange:Swift Code missing on view transaction details
						  * Short Description
						  * Change to change labels according to the Deal Contract Page
						  */
						if(CONFIG_FOR_CHANGE_LABELS_BANK_DETAILS == '1')
							echo "Swift Code";
					?>
                 </font></td>
                <td width="196" height="20">
					<?php 
						/**
						  * #10301: Premier Exchange:Swift Code missing on view transaction details
						  * Short Description
						  * Change to change labels according to the Deal Contract Page
						  */
						if(CONFIG_FOR_CHANGE_LABELS_BANK_DETAILS == '1')
							echo $benBankDetails['swiftCode'];
					?>
                </td>
              </tr>
               <? if(CONFIG_ADD_BEN_BANK_FIELDS_ON_CREATE_TRANSACTION == "1"){ ?>
						  <tr>
							<td width="150" align="right"><font color="#005b90">Sort Code</font> </td>
							<td width="200"><? echo $benBankDetails["sortCode"]; ?></td>
							<td width="100" height="20"  align="right"><font color="#005b90">Routing Number</font></td>
							<td width="200" height="20"><? echo $benBankDetails["routingNumber"]; ?></td>
						  </tr>
						  <tr>
							<td width="150" align="right"><font color="#005b90">IBAN</font> </td>
							<td width="200"><? echo $benBankDetails["IBAN"];?></td>
							<td width="100" height="20"  align="right">
								
							</td>
							<td width="200" height="20">
								
							</td>
						  </tr>
					 <? } ?>
              <?
  }
  			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
              <tr> 
                <td colspan="4" class="style2">Collection Point Details </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Agent 
                  Name</font></td>
                <td width="186" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                <td width="138" align="right"><font color="#005b90">Address</font></td>
                <td width="196"  valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?> 
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Contact 
                  Person </font></td>
                <td width="186" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?> 
                </td>
                <td width="138" align="right"><font color="#005b90">Branch Code</font></td>
				<td width="196" colspan="3"><? echo $senderAgentContent["cp_ria_branch_code"]?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Compnay 
                  </font> </td>
                <td width="186" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?> 
                </td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $senderAgentContent["cp_country"]?></td>
              </tr>
              <tr> 
                <td width="156" align="right"><font color="#005b90">Phone</font></td>
                <td width="186"><? echo $senderAgentContent["cp_phone"]?></td>
                <td width="138" height="20" align="right"><font color="#005b90">City</font></td>
                <td width="196" height="20">
                  <?  echo $senderAgentContent["cp_city"]; ?>
                </td>
              </tr>
              <?
  }
  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><font color="#005b90"><? echo $manualCode; ?></font></font></td>
                <td height="20" width="200"><? echo $contentTrans["refNumber"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="225" height="20" ><? echo $contentTrans["transAmount"]?>    


				            </td>
              </tr>
			  <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200" height="20"><? echo $contentTrans["exchangeRate"]?>                  </td>
                <td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="225" height="20"><? 
	
																	
				 echo $contentTrans["localAmount"]?>
								<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>

				                </td>
			  </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				  
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200" height="20"><? echo $contentTrans["IMFee"]?>                </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="225" height="20"><? echo $contentTrans["totalAmount"]?>                </td>
              </tr>
			  <?
			  }			  
			  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200" height="20"><? echo $contentTrans["transactionPurpose"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="225"><? echo $contentTrans["fundSources"]?>
                </td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200" height="20"><? echo $contentTrans["moneyPaid"]?>
                </td>
     <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                                <td width="225">&nbsp;<!-- echo $contentTrans["bankCharges"]-->
                </td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?

//$sambaCheck = selectFrom("select * from sambaida_check");
//if($sambaCheck["IDA_id"] != $userID)
{	
$get=$_GET["act"];
if($_GET["act"]=="")
	{
	$_GET["act"]="amend";
	}
	if($_GET["act"]=="amend" || $_GET["act"]=="cancel")
	{
?>		  
		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
			<form name="form1" method="post" action="cancel_Confirm.php?transID=<? echo $_GET["transID"]?>">
              <tr>
                <td width="150" height="20" align="right">Change</td>
                <td height="20" align="left">
					<select name="changeStatusTo" class="flat" id="changeStatusTo">
						<option value="Reject Cancel">Reject Cancel</option>
						<option value="Cancelled">Confirm Cancel</option>
						<? if(defined("CONFIG_API_TRANSACTIONS") && CONFIG_API_TRANSACTIONS == "CMT") { ?>
							<option value="reject_paid">Reject Cancel As Already Paid</option>
						<? } ?>
					</select>
					<script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right">Refund <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></td>
                <td height="20" align="left">
					<select name="refundFee" class="flat" id="refundFee">
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
					<script language="JavaScript">SelectOption(document.form1.refundFee, "<?=$_POST["refundFee"]; ?>");</script></td>
              </tr>
				      <tr>
                <td width="150" height="20" align="right" valign="top">Your Remarks </td>
                <td width="196"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			        <tr align="center">
                  <td height="20" colspan="2"> <input name="Submit" type="submit" class="flat" value="Submit"></td>
				  <td width="327">
				          <div align="left">
                            <input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
                          </div>
				  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
        </table></td>
    </tr>
<?
	}
}
?>
</table>
</body>
</html>
<?
}
else
{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>

<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? echo $systemCode; ?> </font></td>
                <td width="200" height="20"><? echo $contentTrans["refNumberIM"]?>
                </td>
                <td width="100" height="20" align="right"><font color="#005b90"><?  echo $manualCode; ?></font></td>
                <td width="200" height="20"><? echo $contentTrans["refNumber"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="150" height="20"><? 
				 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
				{
					if($authoriseDate != '0000-00-00 00:00:00')
						echo date("F j, Y", strtotime("$authoriseDate"));				
				}
				?></td>
              </tr>				  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table width="277" border="0" align="center">
              <tr>
                <td width="323" height="80" colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table width="650" border="0">
                <? 
		
			$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right">&nbsp;</td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["c_country"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  			  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Beneficiary Name</font></td>
                        <td height="20" colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Address</font></td>
                        <td height="20" colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200" height="20">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200" height="20"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
					?>
                      <tr>
                        <td colspan="4" class="style2">Beneficiary Bank Details </td>
                        
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				if($benificiaryContent["Country"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($benificiaryContent["Country"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                          </font></td>
                        <td width="200"><?
				if($benificiaryContent["Country"] == "United States" || $benificiaryContent["Country"] == "Brazil")
				{
				?>
                          <? echo $benBankDetails["ABACPF"]; ?>
                          <?
				}
				?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
		<td width="100" height="20"  align="right"><!--font color="#005b90">IBAN Number</font--> &nbsp;</td>
						<td width="200" height="20">&nbsp; <? //echo $benBankDetails["IBAN"]; ?></td>

                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><!--font color="#005b90">Swift Code</font--></td>
                        <td width="200" height="20"> &nbsp;<? // echo $benBankDetails["swiftCode"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><? echo $senderAgentContent["cp_branch_address"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
						<td width="150" align="right"><font color="#005b90">Branch Code</font></td>
						<td width="200" colspan="3"><? echo $senderAgentContent["cp_ria_branch_code"]?></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Compnay </font> </td>
                        <td width="200" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?>                        </td>
						<td width="100" align="right"><font color="#005b90">Country</font></td>
						<td width="200"><? echo $senderAgentContent["cp_country"]?></td>                       
                      </tr>
					  <tr> 
						<td width="150" align="right"><font color="#005b90">Phone</font></td>
						<td width="200"><? echo $senderAgentContent["cp_phone"]?></td>
					    <td width="100" height="20" align="right"><font color="#005b90">City</font></td>
                        <td width="200" height="20"><?  echo $senderAgentContent["cp_city"]; ?></td>
					  </tr>
					  
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo $contentTrans["transAmount"]?>                </td>
              </tr>
			  <tr>
			    <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
			    <td height="20"><? echo $contentTrans["IMFee"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200" height="20"><? echo $contentTrans["localAmount"]?>     				<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>
           </td>
			  </tr>
              <tr>
                <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200" height="20"><? echo $contentTrans["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
                <td width="100" align="right"><!--font color="#005b90">Bank Charges</font-->&nbsp;</td>
                                <td width="200"><? //echo $contentTrans["bankCharges"]?>&nbsp;
                </td>
              </tr>
			  
              <tr>
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?

//$sambaCheck = selectFrom("select * from sambaida_check");
//if($sambaCheck["IDA_id"] != $userID)
{
	$get=$_GET["act"];
	if($_GET["act"]=="")
	{
	$_GET["act"]="amend";
	}
//	if($_GET["act"]=="amend")
	{
?>		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
			<form name="form1" method="post" action="cancel_Confirm.php?transID=<? echo $_GET["transID"]?>">
              <tr>
                <td width="150" height="20" align="right">Change</td>
                <td height="20" align="left">
					<select name="changeStatusTo" class="flat" id="changeStatusTo">
						  <option value="Reject Cancel">Reject Cancel</option>
						  <option value="Cancelled">Confirm Cancel</option>
						  <? if(defined("CONFIG_API_TRANSACTIONS") && CONFIG_API_TRANSACTIONS == "CMT") { ?>
						  	<option value="reject_paid">Reject Cancel As Already Paid</option>
						  <? } ?>
                	</select>
					
					<script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right">Refund <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></td>
                <td height="20" align="left"><select name="refundFee" class="flat" id="refundFee">
							  <option value="1">Yes</option>
							  <option value="0">No</option>
							  
                </select><script language="JavaScript">SelectOption(document.form1.refundFee, "<?=$_POST["refundFee"]; ?>");</script></td>
              </tr>
				      <tr>
                <td width="150" height="20" align="right" valign="top">Your Remarks </td>
                <td width="213"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			  
              <tr align="right">
                <td height="20" colspan="2">
                  <input name="Submit" type="submit" class="flat" value="Submit">
                  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
		  
		  
        </table></td>
    </tr>
<?
  }
		
	
}
?>
</table>

</body>
</html>
<?
}
?>