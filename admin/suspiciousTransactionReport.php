<?php
//session_start();
include ("../include/config.php");
include ("security.php");

$date_time = date('d-m-Y  h:i:s A');
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

if(isset($_POST["Search"]))
{
    // From Date
    $fDay 	= $_POST["fDay"];
    $fMonth = $_POST["fMonth"];
    $fYear 	= $_POST["fYear"];

    $fromDate = $fYear."-".$fMonth."-".$fDay;

    // To Date
    $tDay = $_POST["tDay"];
    $tMonth = $_POST["tMonth"];
    $tYear = $_POST["tYear"];

    $toDate = $tYear."-".$tMonth."-".$tDay;
    $SenCurrency=$_REQUEST["SenCurrency"];
    $Monitioring=$_REQUEST["Monitioring"];

    $SearchType=$_REQUEST["SearchType"];
    //$Agent=$_REQUEST["Agent"];
    $Risk=$_REQUEST["Risk"];
    $Points=$_REQUEST["Points"];
//		$agentName=$_REQUEST["agentName"];
    $Custtype=$_REQUEST["Custtype"];
    $xmlFileName = "suspiciousTransactionReport_xml.php?type=gridrpt&fdate=".$fromDate."&tdate=".$toDate."&SenCurrency=".$SenCurrency."&SearchType=".$SearchType."&Points=".$Points."&Risk=".$Risk."&Custtype=".$Custtype."&Monitioring=".$Monitioring;
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Suspicious Transaction Report</title>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <link rel="STYLESHEET" type="text/css" href="/dhtmlgrid/dhtmlxgrid.css">
    <link rel="stylesheet" type="text/css" href="/dhtmlgrid/skins/dhtmlxgrid_dhx_skyblue.css">

    <script src="/dhtmlgrid/jquery.js" type="text/javascript"></script>
    <script src="/dhtmlgrid/dhtmlxcommon.js"></script>
    <script src="/dhtmlgrid/dhtmlxgrid.js"></script>
    <script src="/dhtmlgrid/dhtmlxgridcell.js"></script>
    <script src="/dhtmlgrid/ext/dhtmlxgrid_srnd.js"></script>
    <script src="/dhtmlgrid/ext/dhtmlxgrid_filter.js"></script>
    <script src="/dhtmlgrid/dhtmlxdataprocessor.js"></script>
    <script src="/dhtmlgrid/ext/dhtmlxgrid_math.js"></script>
    <script src="/dhtmlgrid/ext/dhtmlxgrid_nxml.js"></script>

    <script language="javascript">


        /** Function for loading options at the run time.
         * this function loads values on run time depending on the Monitoring Dropdown
         **/


        function loadsearch(monitor)
        {
            if(monitor=='Amount')
            {
                $('#SearchType')
                    .find('option')
                    .remove()
                ;
                var myOptions = {
                    All				  : 'All',
                    'Over 6 million'	  : 'Over 6 million',
                    'Between 4.5 & 5.9 million'  : 'Between 4.5 & 5.9 million',
                    'Between 3 & 4.4 million'  : 'Between 3 & 4.4 million'
                };
                var mySelect = $('#SearchType');
                $.each(myOptions, function(val, text) {
                    mySelect.append(
                        $('<option></option>').val(val).html(text)
                    );
                });

            }
            else if (monitor=='Beneficiary')
            {
                $('#SearchType')
                    .find('option')
                    .remove()
                ;
                var myOptions = {
                    All		  : 'All',
                    '20 or more'  : '20 or more',
                    '15-19' 	  : '15-19',
                    '10-14'  	  : '10-14'
                };
                var mySelect = $('#SearchType');
                $.each(myOptions, function(val, text) {
                    mySelect.append(
                        $('<option></option>').val(val).html(text)
                    );
                });
            }
            else if (monitor=='Frequency')
            {
                $('#SearchType')
                    .find('option')
                    .remove()
                ;

                var myOptions = {
                    All 		  : 'All',
                    '48 or more'  : '48 or more',
                    '36-47'  	  : '36-47',
                    '24-35'  	  : '24-35'
                };
                var mySelect = $('#SearchType');
                $.each(myOptions, function(val, text) {
                    mySelect.append(
                        $('<option></option>').val(val).html(text)
                    );
                });
            }
            else if (monitor=='No. of Transactions')
            {
                $('#SearchType')
                    .find('option')
                    .remove()
                ;
                var myOptions = {
                    All		 		 : 'All',
                    '12 times or more (1-2 millions)' 	: '12 times or more (1-2 millions)',
                    '6-11 times or more (1-2 millions)'   : '6-11 times or more (1-2 millions)',
                    'less than 6 times or more (1-2 millions)' 	 : 'less than 6 times or more (1-2 millions)'
                };
                var mySelect = $('#SearchType');
                $.each(myOptions, function(val, text) {
                    mySelect.append(
                        $('<option></option>').val(val).html(text)
                    );
                });
            }
            else if (monitor=='All')
            {
                $('#SearchType')
                    .find('option')
                    .remove()
                ;
                var myOptions = {
                    All		 		 : 'All'
                };
                var mySelect = $('#SearchType');
                $.each(myOptions, function(val, text) {
                    mySelect.append(
                        $('<option></option>').val(val).html(text)
                    );
                });
            }
        }

        function checkForm(theForm)
        {


            /*if(theForm.SenCurrency.value == "" || IsAllSpaces(theForm.SenCurrency.value)){
             alert("Please select Sending currency.");
             theForm.SenCurrency.focus();
             return false;
             }*/


            //if(theForm.PayOutCurrency.value == "" || IsAllSpaces(theForm.PayOutCurrency.value)){
//				alert("Please select Payout currency.");
//				theForm.PayOutCurrency.focus();
//				return false;
//			}


            return true;
        }
        function SelectOption(OptionListName, ListVal)
        {
            for (i=0; i < OptionListName.length; i++)
            {
                if (OptionListName.options[i].value == ListVal)
                {
                    OptionListName.selectedIndex = i;
                    break;
                }
            }
        }



    </script>

</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
    <tr>
        <td align="center" bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Suspicious Transaction Report</font></strong></td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <form action="" method="post" name="Search" onSubmit="return checkForm(this);">
                <table border="1" cellpadding="5" bordercolor="#666666" width="94%">
                    <tr>
                        <td nowrap bgcolor="#c0c0c0" colspan="2"><span class="tab-u"><strong>Search</strong></span></td>
                    </tr>
                    <tr>

                        <td align="center" nowrap >

                            <table width="80%" >
                                <tr>
                                    <td width="1%">
                                        <b>From</b>
                                    </td>
                                    <td width="3%">&nbsp;

                                    </td>

                                    <?php
                                    $month = date("M");
                                    $day = date("d");
                                    $year = date("Y");
                                    ?>
                                    <td width="4%" style="padding:0">
                                        <select name="fDay"  size="1" id="fDay" style="width:90px;font-family:verdana; font-size: 11px">
                                            <?php
                                            for ($Day=1;$Day<32;$Day++)
                                            {
                                                if ($Day<10)
                                                    $Day="0".$Day;
                                                echo "<option value='$Day' ".($day == $Day ? "selected" : "").">$Day</option>\n";
                                            }
                                            ?>
                                        </select>

                                        <script language="JavaScript">
                                            SelectOption(document.Search.fDay, "<?php echo $fDay?>");
                                        </script>

                                        <SELECT name="fMonth" size="1" id="fMonth" style="width:89px;font-family:verdana; font-size: 11px">
                                            <OPTION value="01" <?php echo ($month =="01" ? "selected" : "")?> >January</OPTION>
                                            <OPTION value="02" <?php echo ($month =="02" ? "selected" : "")?>>Febrary</OPTION>
                                            <OPTION value="03" <?php echo ($month =="03" ? "selected" : "")?>>March</OPTION>
                                            <OPTION value="04" <?php echo ($month =="04" ? "selected" : "")?>>April</OPTION>
                                            <OPTION value="05" <?php echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                                            <OPTION value="06" <?php echo ($month =="06" ? "selected" : "")?>>June</OPTION>
                                            <OPTION value="07" <?php echo ($month =="07" ? "selected" : "")?>>July</OPTION>
                                            <OPTION value="08" <?php echo ($month =="08" ? "selected" : "")?>>August</OPTION>
                                            <OPTION value="09" <?php echo ($month =="09" ? "selected" : "")?>>September</OPTION>
                                            <OPTION value="10" <?php echo ($month =="10" ? "selected" : "")?>>October</OPTION>
                                            <OPTION value="11" <?php echo ($month =="11" ? "selected" : "")?>>November</OPTION>
                                            <OPTION value="12" <?php echo ($month =="12" ? "selected" : "")?>>December</OPTION>
                                        </SELECT>

                                        <script language="JavaScript">
                                            SelectOption(document.Search.fMonth, "<?php echo $fMonth?>");
                                        </script>
                                        <SELECT name="fYear" size="1" id="fYear" style="width:71px;font-family:verdana; font-size: 11px">
                                            <?php
                                            $cYear=date("Y");
                                            for ($Year=2004;$Year<=$cYear;$Year++)
                                            {
                                                echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
                                            }
                                            ?>
                                        </SELECT>
                                        &nbsp;
                                    </td>
                                    <script language="JavaScript">
                                        SelectOption(document.Search.fYear, "<?php echo $fYear?>");
                                    </script>
                                    <td width="1%">&nbsp;

                                    </td>

                                    <td width="1%" align="center">
                                        <b>To</b>
                                    </td>
                                    <td width="3%">&nbsp;

                                    </td>
                                    <td width="4%">
                                        <SELECT name="tDay" size="1" id="tDay" style="width:90px;font-family:verdana; font-size: 11px">
                                            <?php
                                            for ($Day=1;$Day<32;$Day++)
                                            {
                                                if ($Day<10)
                                                    $Day="0".$Day;
                                                echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
                                            }
                                            ?>
                                        </select>

                                        <script language="JavaScript">
                                            SelectOption(document.Search.tDay, "<?php echo $tDay?>");
                                        </script>


                                        <SELECT name="tMonth" size="1" id="tMonth" style="width:89px;font-family:verdana; font-size: 11px">
                                            <OPTION value="01" <?php echo ($month =="01" ? "selected" : "")?>>January</OPTION>
                                            <OPTION value="02" <?php echo ($month =="02" ? "selected" : "")?>>Febrary</OPTION>
                                            <OPTION value="03" <?php echo ($month =="03" ? "selected" : "")?>>March</OPTION>
                                            <OPTION value="04" <?php echo ($month =="04" ? "selected" : "")?>>April</OPTION>
                                            <OPTION value="05" <?php echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                                            <OPTION value="06" <?php echo ($month =="06" ? "selected" : "")?>>June</OPTION>
                                            <OPTION value="07" <?php echo ($month =="07" ? "selected" : "")?>>July</OPTION>
                                            <OPTION value="08" <?php echo ($month =="08" ? "selected" : "")?>>August</OPTION>
                                            <OPTION value="09" <?php echo ($month =="09" ? "selected" : "")?>>September</OPTION>
                                            <OPTION value="10" <?php echo ($month =="10" ? "selected" : "")?>>October</OPTION>
                                            <OPTION value="11" <?php echo ($month =="11" ? "selected" : "")?>>November</OPTION>
                                            <OPTION value="12" <?php echo ($month =="12" ? "selected" : "")?>>December</OPTION>
                                        </SELECT>

                                        <script language="JavaScript">
                                            SelectOption(document.Search.tMonth, "<?php echo $tMonth?>");
                                        </script>

                                        <SELECT name="tYear" size="1" id="tYear" style="width:71px;font-family:verdana; font-size: 11px">
                                            <?php
                                            $cYear=date("Y");
                                            for ($Year=2004; $Year<=$cYear; $Year++)
                                            {
                                                echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
                                            }
                                            ?>
                                        </SELECT>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;

                                    </td>
                                </tr>
                                <script language="JavaScript">
                                    SelectOption(document.Search.tYear, "<?php echo $tYear?>");
                                </script>
                                &nbsp;&nbsp;
                                <tr>
                                    <td width="5%" >
                                        <b>Customer Type
                                        </b>
                                    </td>
                                    <td width="2%">&nbsp;

                                    </td>
                                    <td width="20%">
                                        <select name="Custtype" size="1" id="Custtype" style="width:257px;font-family:verdana; font-size: 11px">
                                            <OPTION value="All">All</OPTION>
                                            <OPTION value="Individual">Individual</OPTION>
                                            <OPTION value="Corporate">Corporate</OPTION>
                                        </select>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;

                                    </td>
                                </tr>
                                <script language="JavaScript">
                                    SelectOption(document.Search.Custtype, "<?php echo $_REQUEST["Custtype"]?>");
                                </script>

                                <br />
                                <br />





                                <tr>
                                    <td width="1%" >
                                        <b>Monitoring Criteria</b>
                                    </td>
                                    <td width="2%">&nbsp;

                                    </td>
                                    <td width="20%">


                                        <select name="Monitioring" onchange="loadsearch(this.value);" style="width:257px;font-family:verdana; font-size: 11px; width:130">

                                            <option  value="All">All</option>
                                            <option  value="Amount">Amount</option>
                                            <option  value="Frequency">Frequency</option>
                                            <option  value="Beneficiary">Beneficiary</option>
                                            <option  value="No. of Transactions">No. of Transactions</option>
                                        </select>
                                    </td>
                                    <script language="JavaScript">SelectOption(document.forms[0].Monitioring, "<?=$_REQUEST["Monitioring"]?>");
                                    </script>
                                    <td width="1%">&nbsp;

                                    </td>
                                    &nbsp;&nbsp;
                                    <td width="1%" align="center">
                                        <b>Search By</b>
                                    </td>
                                    <td width="2%">&nbsp;

                                    </td>

                                    <!--<font color="#FF0000">*</font>-->
                                    <td width="20%">
                                        <select name="SearchType" id="SearchType" style="width:257px;font-family:verdana; font-size: 11px; width:130">

                                            <option value="All">All</option>
                                            <!--<option value="Over6">Over 6 million </option>
                                            <option value="Bt4">Between 4.5 & 5.9 million</option>
                                            <option value="Bt3">Between 3 & 4.4 million</option> -->

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;

                                    </td>
                                </tr>
                                <script language="JavaScript">SelectOption(document.forms[0].SearchType, "<?=$_REQUEST["SearchType"]?>");
                                </script>

                                &nbsp;&nbsp;

                                <br />
                                <br />
                                <tr>
                                    <td width="1%" align="center">
                                        <b>Originating Currency</b>
                                    </td>
                                    <td width="2%">&nbsp;
                                    </td>
                                    <!--<font color="#FF0000">*</font>-->
                                    <td width="20%">
                                        <select name="SenCurrency" style="width:257px;font-family:verdana; font-size: 11px; width:130">

                                            <option value="">-Select Currency-</option>
                                            <?php
                                            $queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
                                            $currencyData = selectMultiRecords($queryCurrency);
                                            for($k = 0; $k < count($currencyData); $k++)
                                            {
                                                if($currencyData[$k]['currencyName']!="ALL")
                                                {
                                                    echo "<option ";
                                                    echo " value=".$currencyData[$k]['currencyName'].">".$currencyData[$k]['currencyName']."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;

                                    </td>
                                </tr>
                                <script language="JavaScript">SelectOption(document.forms[0].SenCurrency, "<?=$_REQUEST["SenCurrency"]?>");
                                </script>






                                <tr>
                                    <td width="2%">
                                        <b>Points</b>
                                    </td>
                                    <td width="2%">&nbsp;

                                    </td>

                                    <td width="20%">

                                        <input type="text" name="Points" size="43"/>
                                        <!--<SELECT name="Points" id="Points" style="width:257px;font-family:verdana; font-size: 11px">

                                                <option value="">&nbsp;</option>
                                            <option value="10">10</option>
                                            <option value="10">6</option>
                                            <option value="10">4</option>
                                        </SELECT>-->

                                    </td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;

                                    </td>
                                </tr>
                                <script language="JavaScript">SelectOption(document.forms[0].Points, "<?=$_REQUEST["Points"]?>");
                                </script>
                                &nbsp;&nbsp;
                                <tr>
                                    <td width="2%">
                                        <b>	Risk Type</b>
                                    </td>
                                    <td width="2%">&nbsp;
                                    </td>
                                    <td width="20%">
                                        <SELECT name="Risk" id="Risk" style=" width:257px;font-family:verdana; font-size: 11px">
                                            <option value="All">All</option>
                                            <option value="High">High</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Low">Low</option>
                                        </SELECT>
                                    </td>
                                </tr>

                            </table>
                            <script language="JavaScript">SelectOption(document.forms[0].Risk, "<?=$_REQUEST["Risk"]?>");
                            </script>


                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <input type="submit" name="Search" value="Search">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <div id="editTransaction" style="height:240px; text-align:center; overflow:hidden;"></div>
            <?php
            if(isset($_POST["Search"]))
            {
            ?>
            <script language="javascript">

                function calculateFooterValues(stage) {

                    //alert(editTransGrid.cells(3, 6).getValue());
                    //alert(editTransGrid.getRowsNum());

                    //if (stage && stage != 2)
//				return true;
//				var arQ = document.getElementById("ar_q");
//
//				arQ.innerHTML = sumColumn(5);
//
//
//				var nrQ = document.getElementById("nr_q");
//				nrQ.innerHTML = sumColumn(6);
//
//				var srQ = document.getElementById("sr_q");
//				srQ.innerHTML = sumColumn(7);
//
//				var brQ = document.getElementById("br_q");
//				brQ.innerHTML = sumColumn(8);
//
//				var crQ = document.getElementById("cr_q");
//				crQ.innerHTML = sumColumn(15);

                    return true;
                }
                function sumColumn(ind) {

                    var out = 0;
                    var myVar=0;
                    var NumOfRows=editTransGrid.getRowsNum()+1;
                    
                    for (var i = 1; i < NumOfRows ; i++) {

                        myVar=editTransGrid.cells(i, ind).getValue().replace(",", "");


                        out += parseFloat(myVar);
                        //console.log(editTransGrid.cells(i, ind).getValue());
                    }

                    out = addCommas(out);

                    return out;
                }




                editTransGrid = new dhtmlXGridObject('editTransaction');
                editTransGrid.setImagePath("/dhtmlgrid/imgs/");


                //editTransGrid.setHeader("&nbsp;,&nbsp;,&nbsp;,&nbsp;,#cspan,#cspan,#cspan,#cspan,#cspan,CURRENCY OF TRANSFER,#cspan,#cspan,#cspan,#cspan,#cspan,SETTLEMENT CURRENCY,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;");

                editTransGrid.setHeader("Sr.,Customer Type,Monitoring Criteria,Customer Name,Customer Number,Search Criteria,Aggregated Amount,Total Points,Risk Type,Created On");

                //editTransGrid.attachHeader("&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,#select_filter,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,#select_filter,&nbsp;,#select_filter,&nbsp;,&nbsp;,&nbsp;,&nbsp;,#select_filter,&nbsp;,&nbsp;,&nbsp;,&nbsp;,#select_filter,#select_filter,#select_filter");

                editTransGrid.attachHeader("&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;,&nbsp;");
                editTransGrid.setInitWidths("45,90,110,105,110,90,120,80,70,100");
                editTransGrid.setColAlign("center,center,center,center,center,center,center,center,center,center");
                editTransGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");

                editTransGrid.setNumberFormat("0,000.00",7,".",",");


                //editTransGrid.setColSorting("str,str,str,str,str,str,int,int,int,int,str,int,str,str,int,str,str,str,str,str,str,str,str,str,str,str,");
                editTransGrid.setSkin("dhx_skyblue");

                editTransGrid.init();
                //editTransGrid.attachFooter("Total,&nbsp;,&nbsp;,&nbsp;,&nbsp;,<div id='ar_q'>0</div>,<div id='nr_q'>0</div>,<div id='sr_q'>0</div>,<div id='br_q'>0</div>", ["text-align:left;"] );

                editTransGrid.enableSmartRendering(true);
                //editTransGrid.loadXML("<?php echo $xmlFileName; ?>",calculateFooterValues);
                editTransGrid.loadXML("<?php echo $xmlFileName; ?>");
                //editTransGrid.setNumberFormat("0,000.00",7,".",",");


                function addCommas(nStr)
                {
                    nStr = nStr * 100 / 100;

                    nStr = parseFloat(nStr);
                    nStr = nStr.toFixed(2);


                    nStr += '';
                    x = nStr.split('.');
                    //console.log(x);
                    x1 = x[0];

                    var intDecimalPart = x[1];

                    x2 = x.length > 1 ? '.' + intDecimalPart : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }

                    var finalResult = x1 + x2;
                    return finalResult;
                }

            </script>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle">
            <form action="" method="post" name="frmExport" id="frmExport">
                <input type="hidden" id="tmp" value="" />
            </form>
        </td>
    </tr>
    <tr id="PrintBtn">
        <td colspan="16">
            <div align="center">

                <input type="button" name="Submit2" value="       Print       " onClick="exportFile('excel','printExcel');"> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;

                <span style="color:#0000FF; font-size:12px; font-weight:bold;">
			<input  type="button" src="images/excel.png" name="expExcel" id="expExcel" value="       Export       " onclick="exportFile('excel');" /> &nbsp; &nbsp;
			</span>


            </div></td>
    </tr>
</table>
<script>

    function exportFile(value,printExcel)
    {
        if(value == 'excel' && printExcel=='printExcel')
        {
            document.forms["frmExport"].action = "suspiciousTransactionReport_xml.php?printFile=1&type=excelrpt&fdate=<?php echo $fromDate; ?>&tdate=<?php echo $toDate; ?>&SenCurrency=<?php echo $SenCurrency; ?>&SearchType=<?php echo $SearchType; ?>&Risk=<?php echo $Risk;?>&Points=<?php echo $Points;?>&Custtype=<?php echo $Custtype;?>";
            document.forms["frmExport"].submit();
        }
        else if(value == 'excel' && printExcel!='printExcel')
        {
            document.forms["frmExport"].action = "suspiciousTransactionReport_xml.php?type=excelrpt&fdate=<?php echo $fromDate; ?>&tdate=<?php echo $toDate; ?>&SenCurrency=<?php echo $SenCurrency; ?>&SearchType=<?php echo $SearchType; ?>&Risk=<?php echo $Risk;?>&Points=<?php echo $Points;?>&Custtype=<?php echo $Custtype;?>";
            document.forms["frmExport"].submit();
        }

    }







</script>
<?php
}
?>
</body>
</html>
