<?php

	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$nowTime = date("F j, Y");
	$transID = $_REQUEST["transid"];
	//debug($transID);
	$today = date("d-m-Y");
	$todayD= date("Y-m-d");
	$emailFlag = false;
	$mailto = $_REQUEST["emailto"];
	//debug ($_REQUEST, true);
	 /*
	 * 7897 - Premier FX
	 * Rename 'Create Transaction' text in the system.
	 * Enable  : string
	 * Disable : "0"
	*/
	
	$strTransLabel = 'Transaction';
	
	if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
		$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
td.myFormat
{
font-size:10px;	
}

td.Arial
{
font-size:10px;
font-family:"Gill Sans MT",Times,serif;
border-bottom: solid 1px #000000;
border-right: solid 1px #000000;
}
table.double
{
border:double;
background:#C0C0C0;
}
.noborder
{
font-size:11px;
}
table.single
{
border-style:solid;
border-width:1px;
background:#C0C0C0;
}
td.bottom
{
border-bottom: solid 1px #000000;  
font-size:10px;
}
td.bottomborder{
border-bottom: solid 1px #000000;
}
td.leftborder{
border-left: solid 1px #000000;
}

td.sign
{
font-size:10px;
}
td.style2
{
font-size:10px;
}
td.terms
{
font-size:12px;
font-family: Gill Sans MT;
}
</style>
</head>
</html>
<?php
$message="";
$nTransID = $_REQUEST['transid'];	


$nTransID=explode(",",$nTransID);


if(is_array($nTransID) && $nTransID!="")
{
	$countTrans = count($nTransID);
}
else
{
	$countTrans = 1;
}
//debug($nTransID);
for($t=0;$t<$countTrans;$t++)
{



	$queryTrans="select * from transactions where transID =".$nTransID[$t]."";
	$transContent = selectFrom($queryTrans); 
//debug($queryTrans);
	//debug ($transContent);		
	$agentsql="SELECT name,email FROM admin WHERE userID  = '".$transContent['custAgentID']."'";
	$custAgentContents=selectFrom($agentsql);		
	$agentEmail = $custAgentContents["email"];

	//	debug ($custAgentContents);
	$emailFlag = true;

	//debug ($transContent);
	$customerID=$transContent['customerID'];
	$queryCust = "select customerID, Title, accountName, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, email,dob,IDType,IDNumber, remarks  from customer where customerID ='" . $customerID . "'";
	//debug($queryCust);
	$customerContent = selectFrom($queryCust); 
	$custEmail = $customerContent["email"];

	if(!empty($mailto))
		$custEmail = $mailto;

	$benID=$transContent['benID'];

	/****************/
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		$accountMgr = selectFrom("select parentID from customer where customerID='".$customerID."'");				
		$custAccMgrCont = selectFrom("SELECT name,email FROM admin WHERE userID  = '".$accountMgr['parentID']."'");				
		$accMgrEmail = $custAccMgrCont["email"];	
		$emailFlag = true;
	}			
	/***********************/
				
	$customerID=$transContent['customerID'];
	$queryCust = "select customerID, Title, accountName, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from customer where customerID ='" . $customerID . "'";
	$customerContent = selectFrom($queryCust); 
	//			debug ($customerContent);
	
	$sql="select * from admin where userID= '" . $custAgentContents["userID"] . "'";
	//debug ($sql);
	$userContent = selectFrom($sql);
	//debug ($userContent);
	$queryusername= " select name from admin where username = '".$transContent['addedBy']."'";
	$usernameCont= selectFrom($queryusername);
	
	//debug ($refContent);
	$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from beneficiary where benID ='" . $transContent["benID"] . "'";
	
	//debug ($queryBen); 
	$benificiaryContent = selectFrom($queryBen);
	//debug ($benificiaryContent);

	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$transContent['currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);
		
	$trAccountrTrading = '<table border="0" width="100%" align="center" class="double">';
	for($i=0;$i<count($fieldsCheckedArr);$i++)
	{
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr))
		{
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else
		{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;
		
		$trAccountrTrading .= '	 <tr>
								<td class="Arial" width="20%">'.strtoupper($fieldsLabel).':</td>
								<td width="80%" class="bottom">'.$accountRS[$fieldsValue].'&nbsp;</td>
							  </tr>';	
	}
	$trAccountrTrading = '</table>';
  	$bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$nTransID[$t]."'");	
  
	$ibanOrAcc = '';
	$sortCodeV = '';
	$tipV = '';
	if($bankdetailsQuery["IBAN"]!='' && $bankdetailsQuery["accNo"]!=''){			
		$ibanOrAcc = "IBAN: ".$bankdetailsQuery["IBAN"]." / ". "Account No. ".$bankdetailsQuery["accNo"];
	}
	elseif($bankdetailsQuery["IBAN"]!=''){
		$ibanOrAcc = "IBAN: ".$bankdetailsQuery["IBAN"];
	}
	elseif($bankdetailsQuery["accNo"]!=''){
		$ibanOrAcc = "Account No. ".$bankdetailsQuery["accNo"];
	}	
	if($bankdetailsQuery["sortCode"]!="" &&  $bankdetailsQuery["routingNumber"]!="") {
		$sortCodeV = $bankdetailsQuery["sortCode"]." / ".$bankdetailsQuery["routingNumber"];			
	}
	elseif($bankdetailsQuery["sortCode"]=""){
		$sortCodeV = $bankdetailsQuery["routingNumber"];			
	}
	elseif($bankdetailsQuery["routingNumber"]=""){
		$sortCodeV = $bankdetailsQuery["sortCode"];
	}
	if(CONFIG_HIDE_TIP_FIELD != "1"){
		$tipV = $transContent["tip"];
	}	
	else{
		$tipV = "BARC GB 22";
	}
	
	$termsConditionStr = '';
	if(CONFIG_TRANS_CONDITION_ENABLED == '1')
	{
		if(defined("CONFIG_TRANS_COND"))
		{
			$termsConditionStr =  '<td class = "myFormat" align="center">'. (CONFIG_TRANS_COND).'</td>';						
			//debug($termsConditionStr);
		}
		else
		{
			$termsConditionSql = selectFrom("SELECT company_terms_conditions FROM company_detail
													WHERE company_terms_conditions!='' 
													AND dated = (SELECT MAX(dated) FROM company_detail WHERE company_terms_conditions!='')");
			if(!empty($termsConditionSql["company_terms_conditions"]))
				$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
			//eval("$tremsConditions");
			eval("\$tremsConditions = \"$tremsConditions\";");
			$termsConditionStr =  stripslashes($tremsConditions);
			//debug($termsConditionStr);
			//echo $tremsConditions;
		}
		if (defined('CONFIG_CONDITIONS')) { 
			$termsConditionStr = '';
		}  
	}else{
		$termsConditionStr = "I accept that I haven't used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.";
	//debug($termsConditionStr);
	} 
	
	
	
//debug($nTransID[$t]);	
$message .='
<table border="0" width="70%" class="" align="center" style="font-size:10px;">

<tr>
     <td><img height="80" border="0" width="190" src="http://'.$_SERVER['HTTP_HOST'].'/admin/images/premier/logo.jpg?v=1" alt="" id="Picture2"></td>
	 <tr>
     <td><h2 align="Center" style="font-style:Gill Sans MT;"><u>DEAL CONTRACT</u></h5></td>
</tr>
</tr>
<tr>
	<td>
	
	<table border="1" width="100%" align="center" class="double" bordercolor="#000000">
	   
         <tr border="1">
            <td style="padding-left:30px;" width="40%" class="Arial" bgcolor="#C0C0C0">CLIENT NAME:</td>
            <td width="80%" align="center" class="bottom" bgcolor="#C0C0C0">'. $customerContent["firstName"] . " " . $customerContent["lastName"] . '&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial"  style="padding-left:30px;" width="40%" bgcolor="#C0C0C0">CLIENT NO.</td>
            <td align="center" class="bottom" bgcolor="#C0C0C0">'. $customerContent["accountName"].'&nbsp;</td>
          </tr>
          <tr>
            <td style="padding-left:30px;" bgcolor="#C0C0C0">DEMOFX DEALER</td>
            <td align="center" class="bottom" bgcolor="#C0C0C0">'.$custAccMgrCont['name'].'&nbsp;</td>
          </tr>
        </table>
        <table border="0" width="100%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">This Trade has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.
			</td>
			</tr>
			<tr>
			<td>
              <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
                <tr>
                  <td class="Arial" width="30%" bgcolor="#C0C0C0">TRADE DATE:</td>
                  <td class="bottom" bgcolor="#C0C0C0">'.dateFormat($transContent['transDate'],2) .'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">MATURITY DATE:</td>
                  <td class="bottom" bgcolor="#C0C0C0">'.dateFormat($transContent['valueDate'],2).'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">YOU BUY:</td>
                  <td width="80%" class="bottom" bgcolor="#C0C0C0">'.$transContent["localAmount"]. " ". $transContent["currencyTo"].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">AT A RATE OF:</td>
                  <td class="bottom" bgcolor="#C0C0C0">'. $transContent['exchangeRate'].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">YOU SELL:</td>
                  <td class="bottom" bgcolor="#C0C0C0">'. number_format($transContent["transAmount"],2).' '.$transContent["currencyFrom"].'&nbsp;</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">TT Fee:</td>
                  <td class="bottom" bgcolor="#C0C0C0">ZERO</td>
                </tr>
                <tr>
                  <td class="Arial" bgcolor="#C0C0C0">REFERENCE:</td>
                  <td class="bottom" bgcolor="#C0C0C0">'. $transContent['refNumber'].'
                    &nbsp;</td>
                </tr>
              </table>
			 </td>
          </tr>
        </table>
        <table border="0" width="100%" align="center">
          <tr>
            <td align="center" colspan="2" class="noborder">Where to instruct your Bank to send payment to us:</td>
          </tr>
        </table>
		<table border="1" width="100%" align="center" class="double" bordercolor="#000000">
';
if(!empty($transContent['currencyFrom'])){
	$message .='
		<tr>
            <td class="Arial" width="40%" bgcolor="#C0C0C0">ACCOUNT NAME:</td>
            <td class="bottom" bgcolor="#C0C0C0">'.$accountRS["accountName"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial"  bgcolor="#C0C0C0">ACCOUNT NUMBER:</td>
            <td class="bottom" bgcolor="#C0C0C0">'.$accountRS["accounNumber"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial" bgcolor="#C0C0C0">BANK:</td>
            <td class="bottom" bgcolor="#C0C0C0">'.$accountRS["bankName"].'&nbsp;</td>
          </tr>
		  <tr>
          <td class="Arial" bgcolor="#C0C0C0">IBAN:</td>
            <td class="bottom" bgcolor="#C0C0C0">'.$accountRS["IBAN"].'&nbsp;</td>
          </tr>
          <tr>
            <td class="Arial" bgcolor="#C0C0C0">SORT CODE:</td>
            <td  class="bottom" bgcolor="#C0C0C0">'.$accountRS["sortCode"].'&nbsp;</td>
          <tr>
            <td class="Arial" bgcolor="#C0C0C0">Swift Code:</td>
            <td class="bottom" bgcolor="#C0C0C0">'.$accountRS["swiftCode"].'&nbsp;</td>
          </tr></table>
	';
	$message .= $trAccountrTrading;
}
	$message .='
	<table widh="100%" border="0" bordercolor="#FF0000" align="center">
		<tr>
		<td>
			<table border="0" width="80%" align="center" bordercolor="#000000">
			  <tr>
				<td colspan="2">I confirm that the details set out above are correct and that I will transmit the amount due to the account specified.  I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a &pound;25.00 per day late payment fee.</td>
			  </tr>
			  </table>
			  <table border="1" width="80%" class="single" align="center">
			  <tr>
				<td>
				  <table border="0" width="100%" class="single" align="center">
				  <tr>
					<td width="40%" bgcolor="#C0C0C0"><br/>SIGNED</td>
					<td class="" bgcolor="#C0C0C0">&nbsp;</td>
				  </tr>
				  <tr>
					<td bgcolor="#C0C0C0">NAME (Print):</td>
					<td class="" bgcolor="#C0C0C0">&nbsp;</td>
				  </tr>
				  <tr>
					<td bgcolor="#C0C0C0">DATED:</td>
					<td class="" bgcolor="#C0C0C0"><br/>'.date("d/m/y",strtotime($transContent["transDate"])).'&nbsp;</td>
				  </tr>
				 
			</table>
				</td>
			  </tr>
			</table>
			 <table border="1" width="80%" class="single" align="center">
				  <tr>
					<td width="40%" style="font-family:Gill Sans MT;" bgcolor="#C0C0C0"><br/>PREMIER FX SIGN OFF:</td>
					<td bgcolor="#C0C0C0" width="100%">&nbsp;<td>
			</tr>
			</table>
		</td>
				</tr>
		</table>';
		
	$message .='
<table border="0" width="100%" align="center" class="">
           <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="center" style="font-weight:bold;font-size:14px;font-family:Gill Sans MT; text-decoration:underline;">REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</td>
          </tr>
          <tr>
            <td colspan="2" align="center">Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</td>
          </tr>
          </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
		  <tr>
            <td width="70%" bgcolor="#C0C0C0">CLIENT NUMBER:</td>
            <td class="leftborder" bgcolor="#C0C0C0">'. $customerContent["accountName"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$transContent["localAmount"].' '.$transContent["currencyTo"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">BENEFICIARY&rsquo;S ACCOUNT NAME:</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$bankdetailsQuery["bankName"].'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>		  
          <td width="70%" style="font-style:Gill Sans MT" bgcolor="#C0C0C0">BENEFICIARY&rsquo;S IBAN / ACCOUNT NO:
IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE
		</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$ibanOrAcc.'&nbsp;</td>
		</tr>
		</table>
		<table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$bankdetailsQuery["branchAddress"].'&nbsp;</td>
		</tr>
		</table>
		<table border="1" width="100%" align="center" class="double" bordercolor="#000000">		
          <tr>
            <td width="70%" bgcolor="#C0C0C0">SWIFT/BIC CODE OF THE YOUR BANK:</td>
            <td class="leftborder" bgcolor="#C0C0C0">
			'.$bankdetailsQuery["swiftCode"].'&nbsp;
			</td>
          </tr>
		  </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$sortCodeV.'&nbsp;</td>
          </tr>
		  </table>
		  <table border="1" width="100%" align="center" class="double" bordercolor="#000000">
          <tr>
            <td width="70%" bgcolor="#C0C0C0">WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
            <td class="leftborder" bgcolor="#C0C0C0">'.$tipV.'&nbsp;</td>
          </tr>
        </table>';
		//debug($termsConditionStr);
		//$message.=$termsConditionStr;
		//$message.=$termsConditionStr;
		$TRANSACTION_DATE = dateFormat($today,1);
		$message .='<table border="0" width="100%" align="center">
<tr>
<td align="center" style="font-weight:bold;font-size:14px;font-family:Gill Sans MT; text-decoration:underline;">CUSTOMER DECLARATION AND DETAILS</td>
</tr>
</table>
';

$TRANSACTION_DATE = dateFormat($todayD,1);
if(CONFIG_TRANS_CONDITION_ENABLED == '1')
                        	{
                        		if(defined("CONFIG_TRANS_COND"))
								{
									$message .= '<td class = "myFormat" align="center">'. (CONFIG_TRANS_COND).'</td>';						
								}
								else
								{
										$termsConditionSql = selectFrom("SELECT
											company_terms_conditions
										 FROM 
											company_detail
										 WHERE 
											company_terms_conditions!='' AND 
											dated = (
													 SELECT MAX(dated)
													 FROM 
														company_detail 
													WHERE 
														company_terms_conditions!=''
													 )");
									$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
								if(!empty($termsConditionSql["company_terms_conditions"]))
									$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
																		//eval("$tremsConditions");
									eval("\$tremsConditions = \"$tremsConditions\";");
									//debug($termsConditionSql);
									$message .= stripslashes($tremsConditions);
					//echo $tremsConditions;
								}
       if (defined('CONFIG_CONDITIONS')) {
/*		$receiptContents .= '
          <A HREF="#" onClick="javascript:window.open('.CONFIG_CONDITIONS'\', 'Conditions', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')" CLASS="style2">Full Terms and Conditions</A>*/
		}
      }
	  else
	  {
									$message .= 'I accept that I haven\'t used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.';
      }
	  
	  	
//debug($nTransID[$t]);	
//debug($message);	 
}
/*	<tr>
          <td widh="100%" align="center">'.$termsConditionStr.' */
	//debug($termsConditionStr);


	//debug($message);
	//debug("1", true);
$timeStamp = time();//htmlEmailFormat
//$touchR  = touch($timeStamp.".html");
$tmpfname = tempnam("/tmp", "DEAL_".$customerContent["firstName"]."_");
$fpOpen = fopen($tmpfname, 'w+');
if($fpOpen){
	//debug($fpOpen);
	fwrite($fpOpen, $message);
	fclose($fpOpen);
	//include("lib/html_to_doc.inc.php");
	//$htmltodoc= new HTML_TO_DOC();
	//$htmltodoc->createDoc($timeStamp.".html","test");
}
else{
	$attachementInfo = '<br/>Deal Contract could not be attached with email';
}
/*$fp = fopen("test.doc", 'w+');
fwrite($fp, $message);
fclose($fp);*/
//$htmltodoc->createDocFromURL("http://yahoo.com","test");
$subject = "Deal Contract";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$num = md5( time() );
$messageT = "\n<br />Dear ". $customerContent["firstName"] .",\n<br /><br />Please find attached deal contract for trade done today.  Please check all details are correct and if possible please e-mail/fax back a SIGNED\ncopy to us.\nThank you very much for dealing with DemoFX. <br /><br />Regards,<br /> ";
/*$agentIDQuery=selectFrom(" select agentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom(" select username from admin where userID = '".$agentIDQuery["agentID"]."'");
$messageT .= $agentUserName["username"]."\n<br>";
*/
$agentIDQuery=selectFrom("select parentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom("select name,email from admin where userID = '".$agentIDQuery["parentID"]."'");

if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1")
	$agentEmail = $agentUserName['email'];

$messageT .= "\n ".$agentUserName["name"]."\n";
$messageT.="<table>
				<tr>
					<td align='left' width='30%'>
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg?v=1' />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width='20%'>&nbsp;</td>
					<td align='left'><font size='2px'><font color='#F7A30B'>LONDON OFFICE</font>\n<br>Third Floor, 114 Rochestor Row,London SW1P 1JQ, \n<br> <font color='#F7A30B'>Tel:</font> UK + 44 (0) 2071931910 |  \n<br> <font color='#F7A30B'>Email:</font> info@hbstech.co.uk | www.hbstech.co.uk</font></td>
				</tr>
			</table>";
$strresume = $tmpfname;//$timeStamp.".html";
$fp = fopen($strresume, "rb");
$file = fread($fp, 102460);
// MAIL HEADERS with attachment
//echo $message;
//debug($strresume);
$file = chunk_split(base64_encode($file));
$num = md5(time());

	//Normal headers
$fromHeader = SYSTEM.' <'.$fromEmail.'> ';
if(!empty($agentEmail))
	$fromHeader = $agentUserName["name"].' <'.$agentEmail.'> ';
$headers .= 'From: '.$fromHeader."\r\n";
$headers  .= "MIME-Version: 1.0"."\r\n";
$headers  .= "Content-Type: multipart/mixed; ";
$headers  .= "boundary=".$num."\r\n";
$headers  .= "--$num\r\n";

	// This two steps to help avoid spam   

$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

	// With message
   
$headers .= "Content-Type: text/html; charset=iso-8859-1\r\n";
$headers .= "Content-Transfer-Encoding: 8bit\r\n";
$headers .= "".$messageT."\n";
$headers .= "--".$num."\n"; 

//debug($message);
//debug($messageT, true);
	// Attachment headers
if($fpOpen){
	$headers  .= "Content-Type: application/msword";
	$headers  .= "name=\"attachment.doc\"r\n";
	$headers  .= "Content-Transfer-Encoding: base64\r\n";
	$headers  .= "Content-Disposition: attachment; ";
	$headers  .= "filename=\"DealContract.html\"\r\n\n";
	$headers  .= "".$file."\r\n";
	$headers  .= "--".$num."--";
}
//\/\/\/\/\/\//\/\/\/\/\/\//\/\/\ A.Shahid Start /\/\/\//\/\/\/\/\/\//\/\/\/\/\/\/
/*ini_set("include_path", "../../phpmailer/");
require_once("class.phpmailer.php");
$mailer = new PHPMailer();
$mailer->IsSMTP();
$mailer->Host = 'smtp.mail.yahoo.com';
$mailer->SMTPAuth = true;

$mailer->Username = 'maslamshahid7';
$mailer->Password = '';
$mailer->FromName = $agentUserName["name"];
$mailer->From = 'maslamshahid7@yahoo.com'; 
$mailer->AddAddress($custEmail,$customerContent["firstName"]);
$mailer->Subject = $subject;
$mailer->IsHTML(true);
//$mailer->AddStringAttachment($string, $filename, $encoding, $type);
$mailer->AddCustomHeader('From: <'.$fromHeader.'>');
$mailer->AddAttachment($strresume, "DealContract.html","base64");
//debug($mailer->AddAttachment($strresume, "DealContract.html","base64","application/msword"));
$mailer->Body = $messageT;
//debug($headers);

$custEmailFlag 		= $mailer->Send();
$accMgrEmailFlag 	= $mailer->Send();*/
//\/\/\/\/\/\//\/\/\/\/\/\//\/\/\ A.Shahid End /\/\/\//\/\/\/\/\/\//\/\/\/\/\/\/
//echo $message;
//if(CONFIG_STOP_WHOLE_EMAIL_SYSTEM!="1"){
//debug(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR);
	if(CONFIG_EMAIL_SEND_TO_CUST_ACCTMGR=="1"){
		if($custEmail)
			$custEmailFlag = @mail($custEmail, $subject, $messageT, $headers); 
		if($accMgrEmail)
			$accMgrEmailFlag = @mail($agentEmail, $subject, $messageT, $headers); 	
	}
	else{
	if($custEmail)
		$custEmailFlag = @mail($custEmail, $subject, $messageT, $headers); 
	if($agentEmail)
		$agentEmailFlag = @mail($agentEmail, $subject, $messageT, $headers); 	
	}
//}

//debug($custEmail .'<br/><br/>'. $subject .'<br/><br/>'. $messageT.'<br/><br/>'. $headers);
//debug('<br/><br/>'. $custEmailFlag);
$msg = "Email is not sent to customer and introducer (may not exist or email functionality is disabled)";



if ($custEmailFlag || $agentEmailFlag || $accMgrEmailFlag) {
	if($custEmailFlag && $agentEmailFlag){
		$msg = "Email has been sent to customer and introducer.";
	}
	else if($custEmailFlag && $accMgrEmailFlag){
		$msg = "Email has been sent to customer and Account Manager.";
	}
	else{
		if($custEmailFlag)
			$msg = "Email has been sent to customer.";
		elseif($agentEmailFlag)
			$msg = "Email has been sent to introducer.";
	}
	$msg.=$attachementInfo."<br/>";
}
?>

<div class='noPrint' align="center">
		<?=$msg?>&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?

if(sizeof($transContent) < 1){
	?>
	<div class='noPrint' align="center">
	No Record found
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
	</div>
	<?
	}

?>


