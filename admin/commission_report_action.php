<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	include ("../include/config.php");
	include ("security.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();

	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";


if($_REQUEST["btnAction"] == "Paid"){
	$transIDArray		= $_REQUEST["transIDs"];
	for ($d=0;$d < count($transIDArray);$d++)
	{
		if(isset($transIDArray[$d]))
		{
			update("update ".TBL_TRANSACTIONS." set agentComDuePaid = AgentComm where transID = '".$transIDArray[$d]."'");
			$err = "Commission Paid successfully.";
			
		}
	}

	if(trim($err) == "")
		$err = "Error occured while performing this action";
	// Number of iteration should be equal to the number of  transactions
		echo "<input type='hidden' name='payListPaid' id='payListPaid' value='".$err."'>";
		exit;
}

if($_REQUEST["Submit"] == "SearchTrans")
{
	if ($_REQUEST["from"]!="")
		$from = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$to = $_REQUEST["to"];
	if ($_REQUEST["custAgentID"]!="")
		$custAgentID = $_REQUEST["custAgentID"];
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];

	if ($from != "")
	{
		$dDateF = explode("/",$from);
		if(count($dDateF) == 3)
			$from = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($to != "")
	{
		$dDate = explode("/",$to);
		if(count($dDate) == 3)
			$to = $dDate[2]."-".$dDate[1]."-".$dDate[0];
	}
	if ($from != "" || $to != "")
	{
		if ($from != "" && $to != "")
		{
			$queryDated = " and (tr.transDate >= '$from 00:00:00' and tr.transDate <= '$to 23:59:59') ";	
		}
		elseif ($from != "")
		{
			$queryDated = " and (tr.transDate >= '$from 00:00:00') ";	
		}
		elseif ($to != "")
		{
			$queryDated = " and (tr.transDate <= '$to 23:59:59') ";	
		}	
	}
	if ($queryDated != "")
	{
		$queryClause .= $queryDated;
	}
	if ($custAgentID != "")
	{
		$queryClause .= " AND tr.custAgentID = '$custAgentID' ";
	}
	if ($currencyT != "")
	{
		$queryClause .= " AND tr.currencyFrom = '$currencyT' ";
	}
}

$extraQuery = "";
if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && ($agentType != 'admin' && $agentType != 'Branch Manager' /*&& $agentType != 'Admin Manager'*/))
{
	
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$userID."'");
	
	$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
	//$query .= " and username in '".$linkedAgent."'";
}
$queryCust = "select userID,username from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
if($agentType == "Branch Manager"){
	$queryCust .= " and parentID = '$userID'";						
}
$queryCust .=" order by agentCompany";
$agents = selectMultiRecords($queryCust);
if (empty($custAgentID))
{
	if($agentType == 'SUPA' || $agentType == 'SUBA'){
	$extraQuery .= " AND tr.custAgentID = '$agentID' ";
	}
	else if(count($agents)>0){
		$linkedAgentIDs = array();
		$linkedAgentIDstr = "";
		if(count($linkedAgent)>0){
			for($s=0;$s<count($agents);$s++){
				$flag = false;
				if(in_array($agents[$s]["username"], $linkedAgent) && CONFIG_ADMIN_ASSOCIATE_AGENT == '1')
					$flag = true;	
				if((CONFIG_ADMIN_ASSOCIATE_AGENT != '1') || (strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false))
					$flag = true;
				if($flag)
					$linkedAgentIDArr[] = $agents[$s]['userID'];
			}
			if(!empty($linkedAgentIDArr))
				$linkedAgentIDstr = implode("','",$linkedAgentIDArr);
		}
		if(!empty($linkedAgentIDstr) || $agentType != 'admin')
			$extraQuery .= " AND tr.custAgentID IN ('$linkedAgentIDstr') ";
	}
}

if($_REQUEST["getGrid"] == "intComReport"){
	$queryCom = "SELECT 
					cust.customerID, 
					cust.firstName as custFirstName, 
					cust.lastName as custLastName, 
					count( tr.transID ) AS cntTrans, 
					sum( tr.transAmount ) AS totTransAm, 
					sum( tr.IMFee ) AS totIMFee, 
					tr.currencyFrom as currencyFrom,
					sum(tr.AgentComm) as transAgentComm,
					sum(tr.agentComDuePaid) as agentComDuePaid
				from 
					".TBL_CUSTOMER." as cust  
					JOIN ".TBL_TRANSACTIONS." as tr ON  cust.customerID = tr.customerID  
					";
	if(!empty($queryClause))
		$queryCom .= $queryClause;

	if(!empty($extraQuery))
		$queryCom .= $extraQuery;
	$queryCom .= " GROUP BY cust.customerID, tr.currencyFrom ";
	//echo $queryCom;
	//exit;
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($resultCom);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryCom .= " order by $sidx $sord LIMIT $start , $limit";
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_error());

	//echo $queryCom;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultCom))
	{	
	
	$notPaidYet=$row["transAgentComm"]-$row["agentComDuePaid"];
	
		$transAm = number_format($row["totTransAm"],2,'.',',');
		$transCommDue = number_format($row["transAgentComm"],2,'.',',');
		$notPaidYet = number_format($notPaidYet,2,'.',',');	
		$transCommDuePaid = number_format($row["agentComDuePaid"],2,'.',',');						
		
		$response->rows[$i]['id'] = $row["customerID"].'_'.$row["currencyFrom"];
		$response->rows[$i]['cell'] = array(
									$row["custFirstName"].' '.$row["custLastName"],
									$row["currencyFrom"],
									$row["cntTrans"],
									$transAm,
									$transCommDue,
									$notPaidYet,
									$transCommDuePaid
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

if($_REQUEST["getGrid"] == "intComReportSubGrid"){
	$ridArr = explode('_',$_REQUEST["rid"]);
	$custID = $ridArr[0];
	$currFrom = $ridArr[1];
	$queryComSub = "SELECT 
					tr.transID as transIDT,
					tr.transDate AS transDateT, 
					tr.transAmount AS totTransAmT,
					tr.localAmount AS  destAmT,
					tr.refNumberIM AS refNumT,
					tr.IMFee  AS totIMFeeT, 
					tr.transDate  AS transDateT, 
					tr.currencyFrom as currencyFromT,
					tr.AgentComm as transAgentCommT,
					cust.customerID as customerIDT, 
					cust.firstName as custFirstNameT, 
					cust.lastName as custLastNameT,
					tr.agentComDuePaid as agentComDuePaidT
				from 
					".TBL_CUSTOMER." as cust  
					JOIN ".TBL_TRANSACTIONS." as tr ON  cust.customerID = tr.customerID 
					";
	$queryComSub .= " AND cust.customerID = '$custID' AND tr.currencyFrom = '$currFrom'";
	//$queryComSub .= " AND cust.customerID = '$custID' ";
	//echo $queryComSub;
	//exit;
	if(!empty($queryClause))
		$queryComSub .= $queryClause;

	if(!empty($extraQuery))
		$queryComSub .= $extraQuery;

	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_query());
	//print($queryComSub);
	$count = mysql_num_rows($resultComSub);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryComSub .= " order by $sidx $sord LIMIT $start , $limit";
	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_error());

	//echo $queryCom;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultComSub))
	{
		$transAm = number_format($row["totTransAmT"],2,'.',',');
		$transCommDue = number_format($row["transAgentCommT"],2,'.',',');
		$transCommDuePaid = number_format($row["agentComDuePaidT"],2,'.',',');
		$RemAmount=$transCommDue-$transCommDuePaid;
		$RemAmount=number_format($RemAmount,2,'.',',');
		$response->rows[$i]['id'] = $row["transIDT"];
		$response->rows[$i]['cell'] = array(
									$row["custFirstNameT"].' '.$row["custLastNameT"],
									$row["currencyFromT"],
									$row["refNumT"],
									$row["transDateT"],
									$transAm,
									$row["destAmT"],
									$transCommDue,
									$RemAmount,
									$transCommDuePaid
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

?>