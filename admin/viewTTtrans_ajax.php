<?
	session_start();
	
	
	include ("../include/config.php");
	include ("security.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();
	$created = date('Y-m-d  H:i:s');
	$createdBy = $_SESSION["loggedUserData"]["userID"];

	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$sord = $_REQUEST['sord']; // get the direction 
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	//TT Transaction status array
	$status_arr=array("1"=>"Pending","2"=>"Hold","3"=>"Cancelled","4"=>"Incomplete");
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";


	if(!empty($_REQUEST["btnCancel"])){
		$transIDArray		= $_REQUEST["transIDs"];
		$status_new         =3;
		
		for ($d=0;$d < count($transIDArray);$d++)
		{
			if(isset($transIDArray[$d]))
			{
				
					$TransData = selectFrom("select * from ".TBL_TT_TRANSACTIONS." where id = '".$transIDArray[$d]."'");
					if($TransData["status"]!="3" && $TransData["status"]!="2")
					{
						//debug ($TransData["status"]);
						update("update ".TBL_TT_TRANSACTIONS." set status = '".$status_new."' where id = '".$transIDArray[$d]."'");
						//$err = "Commission Cancel successfully.";
						//debug("update ".TBL_TT_TRANSACTIONS." set status = '".$status_new."' where id = '".$transIDArray[$d]."'");
						//Updating ledger
						
						
						$type = 'TT';
						$args = array('flag'=>'getTransID','type'=>$type,'CancelTrans'=>$transIDArray[$d]);
						$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);				
						if(!empty($returnData['transID'])){
							$transID = $returnData['transID'];
							$transType = $type;
						}
						
						$arguments = array(
											'flagTradingLedger'=>true,
											'transID'=>$transID,
											'amount'=>$TransData["receiving_amount"], //ramount
											'ramount'=>$TransData["sending_amount"], //amount
										
											'sendingAc'=>$TransData["receiving_account_number"], //receivingAc
											'receivingAc'=>$TransData["sender_account_number"], //sendingAc
											
											'sendingCurrency'=>$TransData["receiving_currency"], //receivingCurrency
											'receivingCurrency'=>$TransData["sending_currency"], //sendingCurrency
											 
											'createdBy'=>$createdBy,
											'created'=>$created,
											'extraFields'=>array('transType'=>$transType),
											'status'=>'C');
						
						$acountLF = accountLedger($arguments);
						/*echo '<script>
							
									alert("Transaction Successfully Cancelled");
								</script>';	*/
					}
					else
					{
							/*echo '<script>
							
									alert("Transaction Can not be cancelled");
								</script>';	*/
					}
				}
			
		}
	
		if(trim($err) == "")
			$err = "Error occured while performing this action";
		// Number of iteration should be equal to the number of  transactions
			echo "<input type='hidden' name='payListCancel' id='payListCancel' value='".$err."'>";
			exit;
	}
	
	if(!empty($_REQUEST["btnUnHold"])){
		$transIDArray		= $_REQUEST["transIDs"];
		$status_new         =1;
		$PrevEntry="N";
		for ($d=0;$d < count($transIDArray);$d++)
		{
			if(isset($transIDArray[$d]))
			{
				
				$TransData = selectFrom("select * from ".TBL_TT_TRANSACTIONS." where id = '".$transIDArray[$d]."'");
				if($TransData["status"]=="2")
				{
					if(empty($TransData["sender_account_number"]) || empty($TransData["receiving_account_number"]) || ($TransData["sending_amount"]<=0) || ($TransData["receiving_amount"]<=0) || empty($TransData["exchange_rate"]))
					{
						$statusUp=4;
						update("update ".TBL_TT_TRANSACTIONS." set status = '".$statusUp."' where id = '".$transIDArray[$d]."'");
					
					}
					else
					{	
						//Fetch 'prevAmountDetail' to send proper cancel entery... it got record of prev amount  details with which ledgers were updated last time for this trans
						$prevAmountDetail=$TransData['prevAmountDetail'];
					//	debug("update ".TBL_TT_TRANSACTIONS." set status = '".$status_new."', prevAmountDetail='' where id = '".$transIDArray[$d]."'",true);
//						exit;
						update("update ".TBL_TT_TRANSACTIONS." set status = '".$status_new."' , prevAmountDetail='' where id = '".$transIDArray[$d]."'");
						
						
						//Updating Ledger
						
						$type = 'TT';
						$args = array('flag'=>'getTransID','type'=>$type,'CancelTrans'=>$transIDArray[$d]); //sending CancelTrans parameter so that ledger of this specific transaction is updated
						$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
						if(!empty($returnData['transID'])){
							$transID = $returnData['transID'];
							$transType = $type;
						}
						
					
						if(!empty($TransData["sending_currency"]) || !empty($TransData["receiving_currency"]))
							$noteToInsert = "( Exchange Rate: 1 ".$TransData["sending_currency"]." = ".$TransData["exchange_rate"]." ".$TransData["receiving_currency"]." ) ";
					/** First send a reverse entry in ledger for specific transaction then update
					**  qrPrevEntry is to check whether any previous entry of this transaction is found in ledger or not 
					**  if previous entry is found only then reverse entry will be made.
					**/
						$qrPrevEntry=selectFrom("SELECT 
													id 
											  FROM
													account_ledgers
											  WHERE
													transID='".$transID."'
											");
						
						//debug($qrPrevEntry,true);
						if(!empty($qrPrevEntry))
							$PrevEntry="Y";	
							
						if($PrevEntry=="Y" && !empty($prevAmountDetail))
						{
							$prevAmountDetail=unserialize($prevAmountDetail);
							/*debug($prevAmountDetail,true);
							exit;
							 [sAcc] => 56987
    [rAcc] => 89745666
    [sAmt] => 20.0000
    [rAmt] => 40.0000
    [sCurr] => EUR
    [rCurr] => GBP
    [exRate] => 2.00000000*/

							$arguments = array(
												'flagTradingLedger'=>true,
												'transID'=>$transID,
												'amount'=>$prevAmountDetail["rAmt"], //ramount
												'ramount'=>$prevAmountDetail["sAmt"], //amount
											
												'sendingAc'=>$prevAmountDetail["rAcc"], //receivingAc
												'receivingAc'=>$prevAmountDetail["sAcc"], //sendingAc
												
												'sendingCurrency'=>$prevAmountDetail["rCurr"], //receivingCurrency
												'receivingCurrency'=>$prevAmountDetail["sCurr"], //sendingCurrency
												 
												'createdBy'=>$createdBy,
												'created'=>$created,
												'extraFields'=>array('transType'=>$transType),
												'status'=>'C');	
							$acountLF = accountLedger($arguments);
						}
						if(($PrevEntry=="Y" && !empty($prevAmountDetail)) || ($PrevEntry=="N"))
						{
							$arguments = array(
												'flagTradingLedger'=>true,
												'transID'=>$transID,
												'amount'=>$TransData["sending_amount"], 
												'ramount'=>$TransData["receiving_amount"],
												'sendingAc'=>$TransData["sender_account_number"],
												'receivingAc'=>$TransData["receiving_account_number"],
												'sendingCurrency'=>$TransData["sending_currency"],
												'receivingCurrency'=>$TransData["receiving_currency"],
												'note'=>addslashes($noteToInsert),
												'createdBy'=>$createdBy,
												'created'=>$created,
												'extraFields'=>array('transType'=>$transType)
											);
											/*debug($arguments,true);
											exit;*/
							$acountLF = accountLedger($arguments);
						}
					}
					/*echo '<script>
							
							alert("Transaction Unhold Successfully");
						</script>';	*/
				
				}
				else
				{
					/*echo '<script>
							
									alert("Transaction can not be unHold !!! Only Hold Transaction can be Unhold");
								</script>';	*/
				}
				
			}
		}
	
		if(trim($err) == "")
			$err = "Error occured while performing this action";
		// Number of iteration should be equal to the number of  transactions
			echo "<input type='hidden' name='payListHoldData' id='payListHoldData' value='".$err."'>";
			exit;
	}
	


if($_REQUEST["Submit"] == "SearchTrans")
{

	if ($_REQUEST["from"]!="")
		$from = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$to = $_REQUEST["to"];
	if ($_REQUEST["SenderID"]!="")
		$SenderID = $_REQUEST["SenderID"];
	if(!empty($_REQUEST["RecvAcc"]))
		$RecvAcc  = $_REQUEST["RecvAcc"];
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];
	if ($_REQUEST["RecvcurrencyTrans"]!="")
		$RecvcurrencyTrans = $_REQUEST["RecvcurrencyTrans"];
	if ($_REQUEST["ttStatus"]!="")
		$ttStatus = $_REQUEST["ttStatus"];
	if ($_REQUEST["ttId"]!="")
		$ttId = htmlspecialchars($_REQUEST["ttId"],ENT_QUOTES);
	
	
	if ($from != "")
	{
		$dDateF = explode("/",$from);
		if(count($dDateF) == 3)
			$from = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($to != "")
	{
		$dDate = explode("/",$to);
		if(count($dDate) == 3)
			$to = $dDate[2]."-".$dDate[1]."-".$dDate[0];
	}
	if ($from != "" || $to != "")
	{
		if ($from != "" && $to != "")
		{
			$queryDated = " and (tr.created >= '$from 00:00:00' and tr.created <= '$to 23:59:59') ";	
		}
		elseif ($from != "")
		{
			$queryDated = " and (tr.created >= '$from 00:00:00') ";	
		}
		elseif ($to != "")
		{
			$queryDated = " and (tr.created <= '$to 23:59:59') ";	
		}	
	}
	if ($queryDated != "")
	{
		$queryClause .= $queryDated;
	}
	if (!empty($SenderID))
	{
		$queryClause .= " AND tr.sender_account_number = '$SenderID' ";
	}
	if(!empty($RecvAcc))
	
		$queryClause .= " AND tr.receiving_account_number = '$RecvAcc' ";
		
	if (!empty($currencyT))
	{
	
		$queryClause .= " AND tr.sending_currency = '$currencyT' ";
	}
	
	if(!empty($RecvcurrencyTrans))
	{
		$queryClause .= " AND tr.receiving_currency = '$RecvcurrencyTrans' ";

	}
	
	
	if(!empty($ttStatus))
	{
		
		$queryClause .= " AND tr.status = '$ttStatus' ";

	}
	
	
	
			
	if(!empty($ttId))
	{
		$idTrans=explode("-",$ttId);
		$ttId=$idTrans[1];
		$queryClause .= " AND tr.id = '$ttId' ";

	}
	
}

$extraQuery = "";




if($_REQUEST["getGrid"] == "intComReport"){
	$queryCom = "SELECT 
					cust.customerID, 
					cust.firstName as custFirstName, 
					cust.lastName as custLastName, 
					count( tr.id ) AS cntTrans, 
					sum( tr.sending_amount ) AS totTransAm, 
					sum( tr.receiving_amount ) AS totRecvAm,
					tr.sending_currency,
					tr.receiving_currency
					
					
				from 
					".TBL_CUSTOMER." as cust  
					JOIN ".TBL_TT_TRANSACTIONS." as tr ON  cust.customerID = tr.customer_Id  
					";
	
	if(!empty($queryClause))
		$queryCom .= $queryClause;
	$queryComCancl =$queryCom;
	if(empty($ttStatus) || $ttStatus!=3)
	{
		$queryCom .= " AND tr.status != '3' ";
	}
	if(!empty($extraQuery)){
		$queryCom .= $extraQuery;
		$queryComCancl .=$extraQuery;
	}
	//$queryCom .= " GROUP BY cust.customerID, tr.sending_currency ";
	$queryCom .= " GROUP BY cust.customerID,tr.sending_currency,tr.receiving_currency";
	
	//echo $queryCom;
	//exit;
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($resultCom);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	//$queryCom .= " order by $sidx $sord LIMIT $start , $limit";
	$queryCom .= " order by firstName LIMIT $start , $limit";
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_error());

	//echo $queryCom;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultCom))
	{	
	
	
	
		$transAm = number_format($row["totTransAm"],2,'.',',');
		$RecvAm = number_format($row["totRecvAm"],2,'.',',');
		
							
		
		$response->rows[$i]['id'] = $row["customerID"].'_'.$row["sending_currency"].'_'.$row["receiving_currency"];
		$response->rows[$i]['cell'] = array(
									$row["custFirstName"].' '.$row["custLastName"],
									$row["sending_currency"],
									$row["receiving_currency"],
									$row["cntTrans"],
									$transAm,
									$RecvAm
									
									
									
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

if($_REQUEST["getGrid"] == "intComReportSubGrid"){
	$ridArr = explode('_',$_REQUEST["rid"]);
	$custID = $ridArr[0];
	$currFrom = $ridArr[1];
	$currTo = $ridArr[2];
	//print_r($ridArr);
	echo $queryComSub;
	$queryComSub = "SELECT 
					tr.id as id,
					tr.created AS transDateT, 
					tr.sending_amount AS totTransAmT,
					tr.receiving_amount AS totRecvAm,
					
					
					
					tr.sending_currency as sending_currencyT,
					tr.receiving_currency as receiving_currencyT,
					tr.status,
					tr.internalDetails,
					cust.customerID as customerIDT, 
					cust.firstName as custFirstNameT, 
					cust.lastName as custLastNameT
					
				from 
					".TBL_CUSTOMER." as cust  
					JOIN ".TBL_TT_TRANSACTIONS." as tr ON  cust.customerID = tr.customer_Id 
					";
	//$queryComSub .= " AND cust.customerID = '$custID' AND tr.sending_currency = '$currFrom'";
	$queryComSub .= " AND cust.customerID = '$custID' ";
	
	$queryComSub .= " AND tr.sending_currency = '$currFrom' ";

	$queryComSub .= " AND tr.receiving_currency = '$currTo' ";

	//echo $queryComSub;
	//exit;
	if(!empty($queryClause))
		$queryComSub .= $queryClause;

	if(!empty($extraQuery))
		$queryComSub .= $extraQuery;

	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_query());
	//print($queryComSub);
	$count = mysql_num_rows($resultComSub);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryComSub .= " order by $sidx $sord LIMIT $start , $limit";
	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_error());
	
	
	
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	/*print_r($queryComSub);
	exit;*/
	while($row = mysql_fetch_array($resultComSub))
	{
		$transAm = number_format($row["totTransAmT"],2,'.',',');
		$totRecvAm = number_format($row["totRecvAm"],2,'.',',');
		$TransID=$row["id"];
		
		if($row["status"]==3 || $row["status"]==2)
			//$editTransID="<a href='TTtransfer.php?flagUp=update&TTtransId=$TransID&src=view_TT_Trans.php' target='_blank'>"."TT-".$TransID."</a>";
			$editTransID="TT-".$TransID;
		else
			$editTransID="<a href='TTtransfer.php?flagUp=update&TTtransId=$TransID&src=view_TT_Trans.php'>"."TT-".$TransID."</a>";
		$arrComplianceDetails=unserialize($row["internalDetails"]);
		
		$strComplianceMsg='';
		foreach($arrComplianceDetails as $keyCompl => $valCompl)
		{
			foreach($valCompl as $keyType => $valType)
			{
				
					$strComplianceMsg .=$valType["M"]."<br />";
				
				
			}
		}//debug($strComplianceMsg);exit;
		if($row["status"]==2)
			$strMsg=$strComplianceMsg;
		else
			$strMsg='N\A';
		$response->rows[$i]['id'] = $row["id"];
		$response->rows[$i]['cell'] = array(
									$row["custFirstNameT"].' '.$row["custLastNameT"],
									$editTransID,
									$row["transDateT"],
									$transAm,
									$row["sending_currencyT"],
									$totRecvAm,
									$row["receiving_currencyT"],
									$status_arr[$row["status"]],
									$strMsg
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

?>