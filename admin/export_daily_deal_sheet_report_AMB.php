<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	$rootPath = $_SERVER['DOCUMENT_ROOT']."/admin/lib/";
	include_once($rootPath."currency_exchange_functions.php");
	$systemCode = SYSTEM_CODE;
    $manualCode = MANUAL_CODE;
	//debug($_REQUEST,true);
	$roundAmountTo= "2";
	$roundRateTo = "4";
	
	$colHeads = '';
	if(!empty($_REQUEST["colHeads"])){
		$colHeads = unserialize(base64_decode($_REQUEST["colHeads"]));
	}	
	if(!empty($_REQUEST["filters"])){
		$filters = $_REQUEST["filters"];
	}
	if(!empty($_REQUEST["round_amount_to"])){
		$roundAmountTo = $_REQUEST["round_amount_to"];
	}
	if(!empty($_REQUEST["round_rate_to"])){
		$roundRateTo = $_REQUEST["round_rate_to"];
	}
	
	$queryCurr = "	SELECT 
						*
					FROM 
						currency_stock_history 
					WHERE 1  
				";
									 
	$queryCurr 		.= $filters;	
	$queryCurr 		.= " ORDER BY  id ASC";
	$currencyData 	= SelectMultiRecords($queryCurr);
	
	$exportType 		= $_REQUEST["exportReport"]; // export type
	$strExportedData 	= "";
	$strDelimeter 		= "";
	$strHtmlOpening 	= "";
	$strHtmlClosing 	= "";
	$strBoldStart 		= "";
	$strBoldClose 		= "";
	
	if($exportType == "XLS")
	{
	
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=daily_deal_sheet.".$exportType); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td align='right' style='mso-number-format:\"\@\"'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
		$strHeadColumnStart	=	"<th>";
		$strHeadColumnEnd	=	"</th>";
	}
	elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=daily_deal_sheet.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "\"";
		$strColumnEnd   = "\",";
		$strHeadColumnStart	=	"\"";
		$strHeadColumnEnd	=	"\",";

	}
	else
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td align='right' width='100'>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
		$strHeadColumnStart	=	"<th>";
		$strHeadColumnEnd	=	"</th>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Daily Deal Sheet Report</title>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	// column headings
	foreach($colHeads as $k=>$v)
		$strFullHtml .= $strHeadColumnStart.$v.$strHeadColumnEnd;

	$strFullHtml .= $strRowEnd;

	$totalTrans =0;
	$totalLocal =0;
	foreach($currencyData as $keyBuy => $valBuy)
	{
	 	
		$fltBuyRate		= $valBuy["buying_rate"];
		$localAmount	= ($valBuy["amount"] / $valBuy["buying_rate"]); // Calculate Local Amount
		$localAmount	= number_format($localAmount,$roundAmountTo,'.',',');
		$arrInput = array(
						"buy_sell"    		=> "B",
						"totalAmount" 		=> $valBuy["amount"],
						"rate"        		=> $fltBuyRate,	
						"lastBuyRate" 		=> $fltBuyRate,
						"localAmount" 		=> $localAmount,
						"roundRateLevel"  	=> $roundRateTo,
						"roundAmountLevel"	=> $roundAmountTo
		);
		$arrCurrencyTotal = currency_totals($arrInput);
		 
		/*if($exportType == "CSV"){
			foreach($arrCurrencyTotal as $k=>$v)
				$arrCurrencyTotal[$k] = str_replace(',','',$v);
		}*/
		$strBatchID		= $valBuy["batch_id"];
		$dateCreated 	= date("d/m/Y",strtotime($valBuy["created_at"]));
		$buyAmount 		= $arrCurrencyTotal["buyAmount"];		// foreign currency amount
		$buyRate 		= $arrCurrencyTotal["buyRate"];
		$buyCostInGBP 	= $arrCurrencyTotal["buyCostInGBP"];	// GBP amount
		
		// Total Buy
		$grandBuy 		+= $buyAmount;
		//$averageBuyRate 	= $currInitialBuyingRate;
		$grandBuyGBP 	+= $buyCostInGBP;
	   	
		$strFullHtml .= $strRowStart;
		$strFullHtml .= $strColumnStart.$strBatchID." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$dateCreated." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$buyAmount." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$buyRate." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$buyCostInGBP." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart." ".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
		
		$sellCurrancyData = SelectMultiRecords("SELECT * FROM currency_stock_history_used WHERE from_batch_id='".$valBuy['batch_id']."'");
		foreach($sellCurrancyData as $keySell=>$valSell)
		{
			$localAmountSell= ($valSell["amount"] / $valSell["sell_rate"]);
			$localAmountSell= number_format($localAmountSell,$roundAmountTo,'.',',');
			$arrInputSell	= array(
									"buy_sell"			=> "S",
									"totalAmount"		=> $valSell["amount"], 
									"rate"				=> $valSell["sell_rate"],
									"lastBuyRate" 		=> $fltBuyRate,
									"localAmount" 		=> $localAmountSell,
									"roundRateLevel"  	=> $roundRateTo,
									"roundAmountLevel"	=> $roundAmountTo
								);
								
			$arrCurrencyTotalSell = currency_totals($arrInputSell);
			// Sell Stock
			$sellAmount 	= $arrCurrencyTotalSell["sellAmount"];		// foreign currency amount
			$sellRate 		= $arrCurrencyTotalSell["sellRate"];
			$sellCostInGBP	= $arrCurrencyTotalSell["sellCostInGBP"];	// GBP amount
			
			// Profit/Loss & Margin 
			$pl		= $arrCurrencyTotalSell["profit_loss"];
			$margin = $arrCurrencyTotalSell["margin"];
			
			// Total Sell
			$grandSell 			+= $sellAmount;
			$grandSellGBP		+= $sellCostInGBP;
			$grandProfitLoss	+= $pl;
			
			$strFullHtml .= $strRowStart;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$valSell["Date_out"]." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$sellAmount." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$sellRate." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$sellCostInGBP." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$pl ." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart.$margin." ".$strColumnEnd;
			$strFullHtml .= $strColumnStart." ".$strColumnEnd;
			$strFullHtml .= $strRowEnd;
		}
	}
	
	/* get grand totals*/
	//$getGrandTotals = currency_grand_totals($currencyData);
	
	//$grandBuy = $getGrandTotals["grandBuy"];
	//$averageBuyRate = $getGrandTotals["averageBuyRate"];
	//$grandBuyGBP = $getGrandTotals["grandBuyGBP"];
	//$grandSell = $getGrandTotals["grandSell"];
	//$averageSellRate = $getGrandTotals["averageSellRate"];
	//$grandSellGBP = $getGrandTotals["grandSellGBP"];
	//$grandProfitLoss = $getGrandTotals["grandProfitLoss"];
	//$grandMargin = $getGrandTotals["grandMargin"];
	//$cashInHand = $getGrandTotals["cashInHand"];
	$cashInHand	= $grandBuy - $grandSell;
	$cashInHand	= number_format($cashInHand,$roundAmountTo,'.',',');
	/*if($exportType == "CSV"){
		foreach($getGrandTotals as $k=>$v)
			$getGrandTotals[$k] = str_replace(',','',$v);
	}*/
		
	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Grand Total"." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandBuy." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandBuyGBP." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandSell." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart." ".$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandSellGBP." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandProfitLoss." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$grandMargin ." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart.$cashInHand." ".$strBoldStart.$strColumnEnd;
	$strFullHtml .= $strRowEnd;   	   
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlClosing;

	echo $strFullHtml;
?>