<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("ledgersUpdation.php");
include("connectOtherDataBase.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$clientCompany = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
///////////////////////History is maintained via method named 'activities'
$isAgentAnD = 'N';
$isBankAnD = 'N';

$errMsg = "";

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$nameType = "";
$transType   = "";
$Submit      = "";
$searchVal     = "";
$by          = "";
$qryString   = "";

if($_POST["transType"]!=""){
	$transType = $_POST["transType"];
}
elseif($_GET["transType"]!="") {
	$transType=$_GET["transType"];
}
$qryString = "&transType=".$transType;
			
if($_POST["Submit"]!=""){
		$Submit = $_POST["Submit"];
	}elseif($_GET["Submit"]!="") {
		$Submit=$_GET["Submit"];
	}
	$qryString .= "&Submit=".$Submit;			
	if($_POST["searchVal"]!=""){
		$searchVal = $_POST["searchVal"];
	}elseif($_GET["searchVal"]!=""){
		$searchVal = $_GET["searchVal"];
	}
	$qryString .= "&searchVal=".$searchVal;
	
if($_POST["searchBy"]!=""){
		$by = $_POST["searchBy"];
	}elseif($_GET["searchBy"]!="") {
		$by = $_GET["searchBy"];
	}
	$qryString .= "&searchBy=".$by;

if($_POST["nameType"]!=""){
		$nameType = $_POST["nameType"];
	}elseif($_GET["nameType"]!="") {
		$nameType = $_GET["nameType"];
	}
$qryString .= "&nameType=".$nameType;		
/*if($_POST["agentID"]!="")
		$agentName = $_POST["agentID"];
	elseif($_GET["agentID"]!="") 
		$agentName = $_GET["agentID"];		
*/
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

$query = "select benID,transID,transDate,refNumber,refNumberIM,custAgentID,totalAmount,transType,transAmount,moneyPaid,localAmount,transStatus,customerID,collectionPointID,createdBy from ". TBL_TRANSACTIONS . " as t where 1";
$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t where 1";

if($Submit == "Search")
{
	
	if($searchVal != "")
	{
		switch($by)
		{
			case 0:
			{		
				$query = "select t.transID,t.transType,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.moneyPaid,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t where 1 ";
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t where 1 ";
				
				$query .= " and (t.refNumber = '".$searchVal."' OR t.refNumberIM = '".$searchVal."') ";
				$queryCnt .= " and (t.refNumber = '".$searchVal."' OR t.refNumberIM = '".$searchVal."') ";
				break;
			}
			case 1:
			{
				
				
				$query = "select benID,transID,transDate,t.transType,refNumber,refNumberIM,custAgentID,totalAmount,transType,transAmount,t.moneyPaid,localAmount,transStatus,t.customerID,collectionPointID,createdBy from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, ". TBL_CUSTOMER ." as c where t.customerID = c.customerID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
		
		
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$searchVal);
										$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
									$nameClause = " ".$nameType." LIKE '".$searchVal."%'";
									
									}
					
					
								$query .= " and $nameClause ";
								$queryCnt .= " and $nameClause ";
								
								$queryonline .= " and $nameClause ";
								$queryonlineCnt .= " and $nameClause ";
				
				
					}else{
		    	// searchNameMultiTables function start (Niaz)
       $fn="FirstName";
			 $mn="MiddleName";
			 $ln="LastName"; 		
			 $alis="cm";
			 $q=searchNameMultiTables($searchVal,$fn,$mn,$ln,$alis);
			 
			  $queryonline .= $q;
				$queryonlineCnt .= $q;
				//$queryonline .= " and (cm.FirstName like '".$searchVal."%' OR cm.LastName like '".$searchVal."%')";
				//$queryonlineCnt .= " and (cm.FirstName like '".$searchVal."%' OR cm.LastName like '".$searchVal."%')";
					
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$agentID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
				}			
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($searchVal,$fn,$mn,$ln,$alis);
				
				$query .= $q;
				$queryCnt .= $q;
			}				
				//$query .= "  and (c.firstName like '".$searchVal."%' OR c.lastName like '".$searchVal."%')";
				//$queryCnt .= "  and (c.firstName like '".$searchVal."%' OR c.lastName like '".$searchVal."%')";
				break;
			}
			/*case 2:
			{
				$query = "select t.transID,t.custAgentID,t.transType,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as cb where t.benID = cb.benID and t.createdBy = 'CUSTOMER' ";
				
				
					if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$searchVal);
										$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
									$nameClause = " ".$_POST['nameType']." LIKE '".$searchVal."%'";
									
									}
					
					
								$query .= " and $nameClause ";
								$queryCnt .= " and $nameClause ";
								
								$queryonline .= " and $nameClause ";
								$queryonlineCnt .= " and $nameClause ";
				
				
					}else{
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="cb";
			 $q=searchNameMultiTables($searchVal,$fn,$mn,$ln,$alis);
				
				$queryonline .= $q;		
				$queryonlineCnt .=$q;
								
				//$queryonline .= " and (cb.firstName like '".$searchVal."%' OR cb.lastName like '".$searchVal."%')";		
				//$queryonlineCnt .= " and (cb.firstName like '".$searchVal."%' OR cb.lastName like '".$searchVal."%')";
				
				if($agentType == "Branch Manager"){
				$queryonline .= " and t.custAgentParentID ='$userID' ";
				$queryonlineCnt .= " and t.custAgentParentID ='$userID' ";
				}
				
				// searchNameMultiTables function start (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($searchVal,$fn,$mn,$ln,$alis);
			 
				$query .= $q;
				$queryCnt .=$q;
			}
				//$query .= " and (b.firstName like '".$searchVal."%' OR b.lastName like '".$searchVal."%')";
				//$queryCnt .=" and (b.firstName like '".$searchVal."%' OR b.lastName like '".$searchVal."%')";
				break;
			}*/
			case 3:
			{
				$query = "select t.transID,t.custAgentID,t.refNumber,t.transType,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.moneyPaid,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID and t.createdBy != 'CUSTOMER' ";
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID and t.createdBy != 'CUSTOMER' ";
							
				$queryonline = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as ca where t.custAgentID = ca.userID and t.createdBy = 'CUSTOMER' ";
				$queryonlineCnt = "select t.transID,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as ca where t.custAgentID = ca.userID and t.createdBy = 'CUSTOMER' ";
				
				$queryonline .= " and (ca.name like '".$searchVal."%')";		
				$queryonlineCnt .= " and (ca.name like '".$searchVal."%')";
				
				if($agentType == "Branch Manager"){
				$query .= " and a.parentID ='$userID' ";
				$queryCnt .= " and t.parentID ='$userID' ";
				}
				
				$query .= " and (a.name like '".$searchVal."%')";
				$queryCnt .=" and (a.name like '".$searchVal."%')";
				break;
			}
		}
//		$query .= " and (t.transID='".$_POST["searchVal"]."' OR t.refNumber = '".$_POST["searchVal"]."' OR  t.refNumberIM = '".$_POST["searchVal"]."') ";
	//	$queryCnt .= " and (t.transID='".$_POST["searchVal"]."' OR t.refNumber = '".$_POST["searchVal"]."' OR  t.refNumberIM = '".$_POST["searchVal"]."') ";
			}
			/******** COMMENTING AGENT SEARCH THROUGH AGENT LIST OPTION
			elseif($agentName != "")
			{
						$query = "select t.transID,t.transType,t.custAgentID,t.refNumber,t.totalAmount,t.refNumberIM,t.transDate,t.benID,t.customerID,t.transStatus,t.transAmount,t.benAgentID,t.collectionPointID from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID ";
						$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " as t, ". TBL_ADMIN_USERS ." as a where t.custAgentID = a.userID ";
						if($agentName != "all")
						{
							$query .= " and a.userID ='$agentName' ";
							$queryCnt .= " and a.userID ='$agentName' ";
						}
					
						if($agentType == "Branch Manager")
						{
							$query .= " and a.parentID ='$userID' ";
							$queryCnt .= " and a.parentID ='$userID' ";
						}
				
				
			}****/
			
			
}

	if($transType != "")
	{
		$query .= " and (t.transType='".$transType."')";
		$queryCnt .= " and (t.transType='".$transType."')";
		if($searchVal != "" && $by != 0)
		{
			$queryonline .= " and (t.transType='".$transType."')";
		  $queryonlineCnt .= " and (t.transType='".$transType."')";
			}
		
	}
	
		$query .= " and (t.moneyPaid='On Credit')";
		$queryCnt .= " and (t.moneyPaid='On Credit')";
		
		$queryonline .= " and (t.moneyPaid='On Credit')";
		$queryonlineCnt .= " and (t.moneyPaid='On Credit')";
	




switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	if($searchVal != "")
		{	
		$queryonline .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	}
}

			if($agentType == "Branch Manager"){
				$query .= " and t.custAgentParentID ='$userID' ";
				$queryCnt .= " and t.custAgentParentID ='$userID' ";
				}
  $query .= " order by t.transDate DESC";

//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $query .= " LIMIT $offset , $limit";

 if($_POST["Submit"] == "" || ($_POST["Submit"] != "" && $_POST["searchBy"] != "" && $_POST["searchVal"] != "") || $agentName != ""){
 	
  	$contentsTrans = selectMultiRecords($query);
  	
	
 
  $allCount = countRecords($queryCnt);


		
		if($searchVal != "" && $by != 0)
		{	
			
			
			$onlinecustomerCount = count(selectMultiRecords($queryonlineCnt ));
				
			$allCount = $allCount + $onlinecustomerCount;
			
			
			
					$other = $offset + $limit;
			 if($other > count($contentsTrans))
			 {
				if($offset < count($contentsTrans))
				{
					$offset2 = 0;
					$limit2 = $offset + $limit - count($contentsTrans);	
				}elseif($offset >= count($contentsTrans))
				{
					$offset2 = $offset - $countOnlineRec;
					$limit2 = $limit;
				}
				$queryonline .= " order by t.transDate DESC";
			  $queryonline .= " LIMIT $offset2 , $limit2";
				$onlinecustomer = selectMultiRecords($queryonline);
			 }
			
	  }
	}

		
?>
<html>
<head>
	<title>Money Paid On Credit Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>


    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body onload="nameTypeCheck();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Money Paid On Credit Transactions</font></strong></td>
  </tr>
  <? if ($errMsg != "") {  ?>
		<tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo CAUTION_COLOR; ?>"><b><i><? echo CAUTION_MARK;?></i></b></font></td>
	<td width="635"><? echo "<font color='" . CAUTION_COLOR . "'><b>".$errMsg."</b><br></font>"; ?>
		  </td>
		 </tr>
		</table>
	 </td>
	</tr>
		<? }?>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="moneypaid_oncredit_trans.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap> Search Transactions 
              <input name="searchVal" type="text" id="searchVal" value="<?=$searchVal?>">
		  <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Type - </option>
          <option value="Pick up">Pick up</option>
          <option value="Bank Transfer">Bank Transfer</option>
          <option value="Home Delivery">Home Delivery</option>
        </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
		<select name="searchBy" id="searchBy" onchange="nameTypeCheck();">
			<option value=""> - Search By - </option>
			<option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
			<option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
			<!-- <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option> -->
			<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Name</option> 
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");</script>
		<br>
		
		<!--
		Search By Agent &nbsp; &nbsp;
		<select name="agentID" style="font-family:verdana; font-size: 11px">
                <option value="">- Select Agent-</option>
				<option value="all">All Agents</option>
                <?
                $agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					
								if($agentType == "Branch Manager" || $agentType == "SUPA" || $agentType == "SUPAI")
								{
									$agentQuery .= " and parentID = '$userID'";				
									
								}
								$agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                <?
					}
				?>
             </select>
			 <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$agentName?>");
                                </script>
			-->
	</td>
</tr>
		<? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
   		
  	</td></tr>
  	<?} ?>
		<tr>
			<td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> transactions with money paid on credit option.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td><span class="style1">Money Paid</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
			  <? 
			  if(CONFIG_PICKUP_INFO == '1') 
			  { ?>
			  		<td width="20%"><span class="style1">Pick up Center - City </span></td>
		    <?} ?>
		    <td><span class="style1"></span></td>
			</tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				
				
				$fontColor = "";
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
																			
									}
									if($fontColor == "")
									{
										$fontColor = "#006699";
									}
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','moneyPaidOnCredit', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="<? echo $fontColor ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["moneyPaid"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName, customerStatus from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
			    <? 
			    if(CONFIG_PICKUP_INFO == '1') 
			  	{
			  	 if($contentsTrans[$i]["transType"] == "Pick up")
			  	 {
			  	   $pickUpInfo = selectFrom("select cp_branch_name, cp_city from ".TBL_COLLECTION." where cp_id = '".$contentsTrans[$i]["collectionPointID"]."'");
			  	?>
			  	   <td width="100" bgcolor="#FFFFFF"><? echo $pickUpInfo["cp_branch_name"]. " - " .$pickUpInfo["cp_city"]?></td>
			  <? }else 
	         {?>
	          <td width="100" bgcolor="#FFFFFF" width="100">&nbsp; </td>
	        <?}
			   }?>
					<td align="center" bgcolor="#FFFFFF"><a href="change_trans_moneypaid_status.php?transID=<? echo $contentsTrans[$i]["transID"]?><?=$qryString?>"><strong><font color="#006699"> Change Money Paid Status </font></strong></a> </td> 
				</tr>
				<?
			}
			?>
			
			<? for($j=0;$j< count($onlinecustomer); $j++)
			{
				$i++;
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','moneyPaidOnCredit', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$j]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$j]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$j]["totalAmount"] .  " " . $onlinecustomer[$j]["currencyFrom"]?></td>
				  <? if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$j]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$j]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$j]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($onlinecustomer[$j]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$j]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$j]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
          <? 
			    if(CONFIG_PICKUP_INFO == '1') 
			  	{
			  		if( $onlinecustomer[$j]["transType"] == "Pick up")
			  		{
			  	  $pickUpInfo = selectFrom("select cp_branch_name, cp_city from ".TBL_COLLECTION." where cp_id = '".$onlinecustomer[$j]["collectionPointID"]."'");
			  	?>
			  	  <td width="100" bgcolor="#FFFFFF"><? echo $pickUpInfo["cp_branch_name"]. " - " .$pickUpInfo["cp_city"]?></td>
	          <?}else 
	          {?>
	          <td width="100" bgcolor="#FFFFFF"> &nbsp;</td>
	        <?}
	      	}?>
					<td align="center" bgcolor="#FFFFFF"><a href="change_trans_moneypaid_status.php?transID=<? echo $onlinecustomer[$j]["transID"]?>"><strong><font color="#006699"> Change Money Paid Status </font></strong></a> </td> 
				</tr>
				<?
			}
			?>
			
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
            </td>
          </tr>

          <?
			} else {
		?>
          <tr>
            <td  align="center"> No Transaction found in the database.Or provide proper search fiters to search.
            </td>
          </tr>
          <?
			}
		?>
		  </table></td>
  </tr>

</table>
</body>
</html>
