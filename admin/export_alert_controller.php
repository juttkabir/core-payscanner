<?php

	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	$systemCode = SYSTEM_CODE;
    $manualCode = MANUAL_CODE;
	
	$id_type_id	= $_REQUEST["id_type_id"];
	$alertType	= $_REQUEST["alertType"];
	$userType	= $_REQUEST["userType"];
	$status	  	= $_REQUEST["status"];
	
	$strAlertQuery	= "SELECT * FROM ".TBL_EMAIL_ALERT." WHERE 1 ";
	
	if(!empty($id_type_id))
		$strAlertQuery .= " AND id_type_id = '".$id_type_id."'";
	if(!empty($alertType))
		$strAlertQuery .= " AND alertType = '".$alertType."'";
	if(!empty($userType))
		$strAlertQuery .= " AND userType = '".$userType."'";
	if (!empty($status))
		$strAlertQuery .= " AND isEnable = '".$status."'";
		
	$strAlertQuery	.= " ORDER BY id_type_id ASC";
	
	$arrAlertData	= selectMultiRecords($strAlertQuery);
	
	//$exportType = $_REQUEST["exportType"];
	$strExportedData = "";
	$strDelimeter = "";
	$strHtmlOpening = "";
	$strHtmlClosing = "";
	$strBoldStart = "";
	$strBoldClose = "";
	
	if($_REQUEST["act"] == "export")
	{
	
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=Manage_Alert.xls"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "<table border='1'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";
	}
	/*elseif($exportType == "CSV")
	{

		header("Content-Disposition: attachment; filename=Manage_Alert.csv"); 
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Content-Type: application/vnd.ms-excel');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 

		$strMainStart   = "";
		$strMainEnd     = "";
		$strRowStart    = "";
		$strRowEnd      = "\r\n";
		$strColumnStart = "";
		$strColumnEnd   = ",";

	}*/
	if($_REQUEST["act"] == "print")
	{
		$strMainStart   = "<table align='center' border='1' width='100%'>";
		$strMainEnd     = "</table>";
		$strRowStart    = "<tr>";
		$strRowEnd      = "</tr>";
		$strColumnStart = "<td>";
		$strColumnEnd   = "</td>";
		$strBoldStart 	= "<b>";
		$strBoldClose 	= "</b>";

		
		$strHtmlOpening = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
							<title>Manage Alert</title>
							<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
							<script language="javascript" type="text/javascript">
								jQuery(document).ready(function(){
									$("#printBtn").click(function(){
										$("#printBtn").hide();
										print();	
									});
								});
							</script>
							<style> 
							td {
								font-family: verdana;
								font-size: 12px;
							}
							 .NG td{
							 	color: #9900FF;
							 }
							 .AR td{
							 	color: #009933;
							 }
							 .ID td{
							 	color: #666666;
							 }
							 .LD td{
							 	color: #00CCFF;
							 }
							 .ST td{
							 	color: #FF9900;
							 }
							 .EL td{
							 	color: #FF0000;
							 }
							 .RN td{
							 	color: #004433;
							 }
							</style> 
							</head>
							<body>
							';
		
		$strHtmlClosing = '</body></html>';
		$strHtmlButton = '<br /><div style="text-align:center;"><input type="button" id="printBtn" value="Print"></div>';
	}

	$strFullHtml = "";
	
	$strFullHtml .= $strHtmlOpening;
		
	$strFullHtml .= $strMainStart;

	$strFullHtml .= $strRowStart;
	$strFullHtml .= $strColumnStart.$strBoldStart."Sr.".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."ID Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."User Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Alert Type".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Days".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Status".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strColumnStart.$strBoldStart."Message".$strBoldClose.$strColumnEnd;
	$strFullHtml .= $strRowEnd;
	
	$sr = 0;
	foreach($arrAlertData as $key => $val)
	{	   
		$sr++;
		if($val["isEnable"] == "Y")
			$status = "Enable";
		if($val["isEnable"] == "N")
			$status = "Disable";
		
		$strFullHtml .= $strRowStart;
		$strFullHtml .= $strColumnStart.$sr." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["report"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["userType"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["alertType"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["day"]." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$status." ".$strColumnEnd;
		$strFullHtml .= $strColumnStart.$val["message"]." ".$strColumnEnd;
		$strFullHtml .= $strRowEnd;
	}
	   	   
	$strFullHtml .= $strMainEnd ;
	$strFullHtml .= $strHtmlButton;
	$strFullHtml .= $strHtmlClosing;
	echo $strFullHtml;
?>
