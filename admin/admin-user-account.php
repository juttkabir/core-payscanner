<?
session_start();
include ("../include/config.php");
include ("security.php");
include ("calculateBalance.php");

$agentType = getAgentType();

$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

$intUserId = $_REQUEST["agentID"];
$strSubmit = $_REQUEST["Submit"];

if($intUserId != "")
	$_SESSION["agentID2"] = $intUserId;
else 
	$_SESSION["agentID2"] = "";

$today = date("Y-m-d");
$msg = "";

if(isset($_REQUEST["addManual"]))
{
	
	if($_REQUEST["amountType"] == "DEPOSIT")
	{
				
		$insertQuery="insert into agent_account 
					(agentID, dated, type, amount, modified_by, description, note,currency) 
			values  ('".$_SESSION["agentID2"]."', '$today', 'DEPOSIT', '".$_REQUEST["amount"]."', '$changedBy', 'Manually Deposited','".$_REQUEST["description"]."','".$_REQUEST["currency"]."')";
				
						
		$bolReturn = insertInto($insertQuery);
		$insertedID = @mysql_insert_id();

		$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
		$agentBalance	= $contantagentID["balance"];
		$agentBalance += $_REQUEST["amount"];
		update("update " . TBL_ADMIN_USERS . " set balance  = $agentBalance where userID = '".$_SESSION["agentID2"]."'");
		
		$descript = "Amount ".$_REQUEST["amount"]." is added into Admin Staff account";
		activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agent_account",$descript);
		/*****
			#5258 - Minas Center
					Admin Account Summary is maintained for Opening/Closing Balances.
					by Aslam Shahid
		*****/
		adminAccountSummary($_SESSION["agentID2"], "DEPOSIT", $_REQUEST["amount"],$_REQUEST["currency"]);
	} 
	elseif ($_REQUEST["amountType"] == "WITHDRAW") 
	{
		
		$insertQuery="insert into agent_account 
				  (agentID, dated, type, amount, modified_by, description, note,currency) 
			values('".$_SESSION["agentID2"]."', '$today', 'WITHDRAW', '".$_REQUEST["amount"]."', '$changedBy', 'Manually Withdrawn','".$_REQUEST["description"]."','".$_REQUEST["currency"]."')";

		$bolReturn = insertInto($insertQuery);
		$insertedID = @mysql_insert_id();
		
		$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
		$agentBalance	= $contantagentID["balance"];
		$agentBalance -= $_REQUEST["amount"];
		update("update " . TBL_ADMIN_USERS . " set balance  = $agentBalance where userID = '".$_SESSION["agentID2"]."'");
		
		$descript = "Amount ".$_REQUEST["amount"]." is withdrawn from Admin Staff account";
		activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agent_account",$descript);
		/*****
			#5258 - Minas Center
					Admin Account Summary is maintained for Opening/Closing Balances.
					by Aslam Shahid
		*****/
		adminAccountSummary($_SESSION["agentID2"], "WITHDRAW", $_REQUEST["amount"],$_REQUEST["currency"]);
	}
	
	if($bolReturn)
		$msg = "You have sucessfully ".$_REQUEST["amountType"]." ".$_REQUEST["amount"]." ".$_REQUEST["currency"]." into admin staff account.|successMessage";
	else
		$msg = "Could not perform the action, please try again.|errorMessage";
	
	$strSubmit = "Search";
}

//debug($insertQuery);


if($strSubmit == "Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_REQUEST["fMonth"];
	$_SESSION["fDay"]=$_REQUEST["fDay"];
	$_SESSION["fYear"]=$_REQUEST["fYear"];
	
	$_SESSION["tMonth"]=$_REQUEST["tMonth"];
	$_SESSION["tDay"]=$_REQUEST["tDay"];
	$_SESSION["tYear"]=$_REQUEST["tYear"];
	
	$_SESSION["closingBalance"] = "";
	$_SESSION["openingBalance"] = "";
	$_SESSION["currDate"] = "";	
	
}


if ($offset == "")
	$offset = 0;
$limit=20;
if ($_REQUEST["newOffset"] != "") {
	$offset = $_REQUEST["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

$condition = false;
if($agentType == "admin") 
{
	$condition = true;
	$intUserId = $_REQUEST["agentID"];
}
else
	$intUserId = $_SESSION["loggedUserData"]["userID"];

?>
<html>
<head>
	<title>Admin User Account Statement</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function clearCheque(recId, un)
{
	var confirmClear = confirm("Are you sure you want to clear this amount ?");
	if(!confirmClear){
		return true;
	}
	$.ajax
	({
		type: "GET",
		url: "clearCheque.php",
		data: "uc=" + un,
		beforeSend: function(XMLHttpRequest)
		{
			$("#status"+recId).text("Processing...");
		},
		success: function(responseText)
		{
			$("#latestCleared").val(recId);
			$("#status"+recId).html(responseText);
			$("#Submit").click();
		}
	});
}


function submitForm()
{
	if($("#amount").val() == "" || $("#amount").val() <= 0)
	{
		alert("Please provide valid amount to deposit or withdraw.");
		$("#amount").focus();
		return false;
	}
	
	if($("#amountType").val() == "")
	{
		alert("Please select type of amount (deposit or withdraw).");
		$("#amountType").focus();
		return false;
	}
	if($("#currency").val() == "")
	{
		alert("Please provide amount currency.");
		$("#currency").focus();
		return false;
	}

	return confirm('Are you sure to '+ $('#amountType').val() +' of '+ $('#amount').val() + ' with admin staff account?');
	
}


</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
.boundingTable {
	border: solid 2px #666666;
}

.inputBoxesNumerics {
	text-align: right;
	width: 70px;
	font-size: 11px;
}

.inputBoxesText {
	width: 255px;
	font-size: 11px;
}

.inputBoxesCustom {
	font-size: 11px;
}

.dropDownBoxes {
	font-size: 11px;
}

.reportToolbar {
	background-color: #EEEEEE;
}

.errorMessage {
	color: #FF0000;
	font-weight: bold;
	font-size: 12px;
}

.successMessage {
	color: #009900;
	font-weight: bold;
	font-size: 12px;
}

.fieldComments {
	color: #999999;
}

.tdTitle { 
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:14px;
	color:#0033CC;
	padding-left:4px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">Admin Staff Account Statement</td>
  </tr>
	<tr>
      <td width="100%">
	<form action="" method="post" name="Search" id="Search">
  		<div align="center" id="searchTable">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
					SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
				</script>
			 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
				</script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
               <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			   ?>
               </SELECT>
                <script language="JavaScript">
					SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
				</script>
                <br>
                &nbsp;&nbsp; 
                <? 
					if($condition) 
					{ 
						$agentQuery  = "
										select 
											userID,
											username, 
											agentCompany, 
											name, 
											agentContactPerson,
											adminType 
										from 
											".TBL_ADMIN_USERS." 
										where 
											    adminType IN ('Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM')
											AND agentStatus='Active' 
										order by 
											adminType, 
											name	";
						//debug($agentQuery);
						$arrStaff = selectMultiRecords($agentQuery);
						//debug($arrStaff);
				?>
				<br />
				<select name="agentID" style="font-family:verdana; font-size: 11px">
					<option value="">- Select Admin User -</option>
                	<?
						for($i=0; $i < count($arrStaff); $i++)
						{
							if($arrStaff[$i]["adminType"] != $arrStaff[$i-1]["adminType"])
								echo "<optgroup label='".$arrStaff[$i]["adminType"]."'>";
							
							if($intUserId == $arrStaff[$i]["userID"]) { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" selected="selected"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } else { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>"><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } 

							if($arrStaff[$i]["adminType"] != $arrStaff[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
                </select>
                 <? } ?>
				<br />				 
				<input type="hidden" id="latestCleared" name="latestCleared" value="" />
                <input type="submit" name="Submit" id="Submit" value="Search" /> 
              </div>
        
		<tr>
		<td>
		
      <?
	 if(!empty($strSubmit))
	 {
	 	
	 	$Balance="";
		if($strSubmit == "Search")
			$slctID = $intUserId;
	
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			

		if ($slctID !=""){
			 $settlementCurr  = selectFrom("select settlementCurrency from admin  where 1 and userID=".$slctID." ");	
       		 $settleCurr=$settlementCurr["settlementCurrency"];  
			
			$accountQuery = "Select * from agent_account where agentID = $slctID";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= " and status != 'Provisional' ";
				
			$accountQueryCnt = "Select count(*) from agent_account where agentID = $slctID";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') {
				$accountQuery .= " and currency ='".$settleCurr."' ";
				$accountQueryCnt .= " and currency ='".$settleCurr."' ";
		 	 }
			 
			 if(CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM == "true")
			 {
			 	$chequeNumber = trim($_REQUEST["chequeNumber"]);
			 
			 	if(!empty($chequeNumber))
				{
					$accountQuery .= " and chequeNumber ='".$chequeNumber."' ";
					$accountQueryCnt .= " and chequeNumber ='".$chequeNumber."' ";
				}
			 }
			 

			$allCount = countRecords($accountQueryCnt);

			$accountQuery .= "order by aaID";
	  	$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}else{
		
			$slctID = $changedBy;
			$accountQuery="Select * from agent_account where agentID = $changedBy";
			$accountQuery .= " and $queryDate";				
			$accountQuery .= " and status != 'Provisional' ";
				
				
			$accountQueryCnt = "Select count(*) from agent_account where agentID = $changedBy";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
				$accountQuery .= " and currency = '".$settleCurr."' ";
				$accountQueryCnt .= " and currency = '".$settleCurr."' ";
		  	}
			
			if(CONFIG_ENABLE_FILTER_ON_SEARCH_BY_CHEQUE_NUM == "true")
			 {
				$chequeNumber = trim($_REQUEST["chequeNumber"]);
			 
				if(!empty($chequeNumber))
				{
					$accountQuery .= " and chequeNumber ='".$chequeNumber."' ";
					$accountQueryCnt .= " and chequeNumber ='".$chequeNumber."' ";
				}
			 }
			 

			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= "order by aaID";
		    $accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}
		//debug($accountQuery);
		

		?>	
			
		  <table width="95%" border="1" cellpadding="0" bordercolor="#666666" align="center">
		  	<? if(!empty($msg)) 
			   { 
			   		$parts = explode("|", $msg);
			?>
					<tr>
						<td align="center" class="<?=$parts[1]?>">
							<?=$parts[0]?>
						</td>
					</tr>
			<? 
			  } 
			
			 $strExtraUrlParams = "&fDay=".$_REQUEST["fDay"]."&fMonth=".$_REQUEST["fMonth"]."&fYear=".$_REQUEST["fYear"].
			 					  "&tDay=".$_REQUEST["tDay"]."&tMonth=".$_REQUEST["tMonth"]."&tYear=".$_REQUEST["tYear"]."&agentID=".$intUserId."&Submit=Search";
			
			
			?>
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF" id="pagination">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount?>
					<?php } ?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?=$PHP_SELF."?newOffset=0&total=first".$strExtraUrlParams?>"><font color="#005b90">First</font></a></td>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$prv&total=pre".$strExtraUrlParams?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?=$PHP_SELF."?newOffset=$nxt&total=next".$strExtraUrlParams?>"><font color="#005b90">Next</font></a>&nbsp;</td>
				  <td width="50" align="right">&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			<td>&nbsp;
			<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
				<table>
					<tr>
						<td width="40%">
							<? 
							$agentBalanceFirst = selectFrom("select opening_balance, closing_balance, dated from " . TBL_ACCOUNT_SUMMARY . " where user_id ='".$slctID."' and dated = (select MIN(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id ='".$slctID."' and dated >= '".$fromDate."')");
							$agentBalanceSecond = selectFrom("select opening_balance, closing_balance, dated from " . TBL_ACCOUNT_SUMMARY . " where user_id ='".$slctID."' and dated = (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id ='".$slctID."' and dated <= '".$toDate."')");
							
							$cumulativeOpeningBalance = $agentBalanceFirst["opening_balance"];
							$cumulativeClosingBalance = $agentBalanceSecond["closing_balance"]; 
							$cumulativeRunning = $cumulativeClosingBalance - $cumulativeOpeningBalance;
							number_format($cumulativeRunning,2,'.',',');
							
							
							?>
			</td>
			<td>
				<? if ($allAmount < 0 ){ ?>
				<font color="red"><strong>The agent has to pay the money </strong></font>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<strong> Agent Credit Balance  </strong>
			<? } ?>
			</td>
			</tr>
			</table>
			<?
		}
		?>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="100%" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF" valign="baseline">
					  <td align="left" width="8%"><span class="style1">Date</span></td>
					  <td align="left"  width="5%"><span class="style1">Modified By</span></td>
					  <? 
					  if(CONFIG_ADMIN_STATEMENT_BALANCE == '1')
					  {
					  ?>
					  <td align="left"  width="10%"><span class="style1">Opening Balance</span></td>
					  <?
						}
					  ?>
					  
					  <td align="left" width="8%"><span class="style1">Money In</span></td>
					  <td align="left" width="8%"><span class="style1">Money Out</span></td>
					   
						<td align="left" width="8%"><span class="style1"><? echo $systemCode; ?></span></td>
						<td align="left" width="10%"><span class="style1"><? echo $manualCode; ?></span></td>
						<td align="left" width="15%"><span class="style1">Description</span></td>
						<td align="left" width="15%"><span class="style1">Note</span></td>	
						<? 
							if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
							{ 
						?>
								<td align="left" width="8%"><span class="style1">Status</span></td>
						<?
							}
							
							//if(CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT == "true")
							//{
						?>
								<td align="left" width="8%"><span class="style1">Ref #</span></td>
								<td align="left" width="8%"><span class="style1">Transfer Status</span></td>
						<?
							//}
							
							if(CONFIG_ADMIN_STATEMENT_BALANCE == '1')
							{
						?>
					  			<td align="left" width="10%"><span class="style1">Closing Balance</span></td>
						<?
					  		}
							
							if(CONFIG_AGENT_STATEMENT_FIELDS != '1')
							{
						?>
							   <td>&nbsp;</td> 
							   <td>&nbsp;</td> 
						<?
           					}
						?>
					</tr>
					
				<? 
				
				$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					
					///////////////////Opening and Closing////////////////
					
						if(CONFIG_ADMIN_STATEMENT_BALANCE == '1')
						{
							
							$currDate = $contentsAcc[$i]["dated"];
							if($currDate != $preDate)
							{
									if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
	 								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
							  }else{
							  	$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
							  	}
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								
								 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
								  $account1 .= " and  user_id = '".$slctID."' ";	
								  if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') {
								  $account1 .= " and  currency='".$settleCurr."' ";		
								  }
								  			
									$contents1 = selectFrom($account1);
									
									if($currDate == $FirstDated){
										 $openingBalance =  $contents1["opening_balance"];
								  
									}else{
										$openingBalance =  $contents1["closing_balance"];
								
									}
								}
									
								
								 $closingBalance = $openingBalance;
								
								 $preDate = $currDate;
								
							}
								
						}
						$_SESSION["currDate"] = $currDate;
						

					?>
				<tr bgcolor="#FFFFFF">
					<td align="left"><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
				  <? 
				  if(CONFIG_AGENT_STATEMENT_FIELDS == '1')
          {
          ?>
					<td align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
						$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; 
					?>
					</td>
				 <? 
				  }
					  if(CONFIG_ADMIN_STATEMENT_BALANCE == '1')
					  {
					  ?>
					      <td align="left"><? echo number_format($openingBalance,2,'.',','); ?></td> 
					  
					  <?
					  
					 }


					$currentAmount = 0;
					if($contentsAcc[$i]["TransID"] > 0 && CONFIG_BALANCE_FROM_TRANSACTION == '1'){

							$trans = selectFrom("SELECT refNumberIM, refNumber, totalAmount, AgentComm, transAmount, refundFee,currencyFrom
							FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
					
							
							if($contentsAcc[$i]["type"] == "DEPOSIT")
									{
										if($trans["refundFee"] == 'No')
										{
											$currentAmount = $trans["transAmount"];
										}else{
											$currentAmount = $trans["totalAmount"] - $trans["AgentComm"];
											}
										$Balance = $Balance+$currentAmount;
									}	
									
									if($contentsAcc[$i]["type"] == "WITHDRAW")
									{
										$currentAmount = $trans["totalAmount"] - $trans["AgentComm"];
										$Balance = $Balance-$currentAmount;
									}
							
							
						}else{
						
									if($contentsAcc[$i]["type"] == "DEPOSIT")
									{
										$currentAmount = $contentsAcc[$i]["amount"];
									
										$Balance = $Balance+$currentAmount;
									}	
									
									if($contentsAcc[$i]["type"] == "WITHDRAW")
									{
										$currentAmount = $contentsAcc[$i]["amount"];
										$Balance = $Balance-$currentAmount;
									}
							
							 }	
							
					?>
					<td align="right">
					<?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
							 echo customNumberFormat($currentAmount)." ".$contentsAcc[$i]["currency"];
						}else{
							 echo $currentAmount." ".$contentsAcc[$i]["currency"];
						}
					}
					?>
					</td>
					<td align="right">
					<? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
							 echo customNumberFormat($currentAmount)." ".$contentsAcc[$i]["currency"];
						}else{
							 echo customNumberFormat($currentAmount)." ".$contentsAcc[$i]["currency"];
						}
					}
					?>
					</td>
					      
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM, refNumber FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumber"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<? }?>
				<td>
					<? echo $contentsAcc[$i]["description"]; ?>&nbsp;
				</td>
				<td>
					<? echo $contentsAcc[$i]["note"]; ?>&nbsp;
				</td>
				<?  if(CONFIG_AGENT_STATEMENT_FIELDS == '1') {  ?>
				<td>
					<? echo $contentsAcc[$i]["status"]; ?>&nbsp;
				</td>
				<?
			  }
			  
			  	//if(CONFIG_DISPLAY_CHEQUE_FIELDS_ON_AGENT_ACCT_STMT == "true")
				//{
			$latestClearedFormat = "";
			if($contentsAcc[$i]["aaID"] == $_POST["latestCleared"])
				$latestClearedFormat = "style='background-color:lightgreen;'";
			?>
					<td>&nbsp;<?=$contentsAcc[$i]["chequeNumber"]?></td>
					<td <?=$latestClearedFormat?>>&nbsp;
						<?
							if($contentsAcc[$i]["chequeStatus"] == "UNCLEARED")
							{
			
								/** 
								 * Only Admin can clear the cheques 
								 * @Ticket #4017
								 */								 						 
								if($agentType == "admin")
									echo "<span id='status".$contentsAcc[$i]["aaID"]."'><a href='#' style='{color:#0000ff}' title='Click to CLEAR the transfer.' onClick=\"javacript: clearCheque(".$contentsAcc[$i]["aaID"].", '".$contentsAcc[$i]["chequeNumber"]."');\">".$contentsAcc[$i]["chequeStatus"]."</a></span>";
								else
									echo $contentsAcc[$i]["chequeStatus"];
							} 
							else {
								echo $contentsAcc[$i]["chequeStatus"];
							}
						?>
					</td>
			<?
				//}
			  
   			 if(CONFIG_ADMIN_STATEMENT_BALANCE == '1')
				{
				
					if($contentsAcc[$i]["chequeStatus"] == "CLEARED" || empty($contentsAcc[$i]["chequeStatus"]))
					{
						if($contentsAcc[$i]["type"] == "WITHDRAW")
						{
							 
						 $closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
						
						}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
						{
							$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
							
						}
					}
					?>
					<td align="left"><? echo(number_format($closingBalance,2,'.',',')); ?></td>
					 				  
		  <? } ?>

		</tr>
		<? } ?>
			<tr bgcolor="#FFFFFF">
			   <td colspan="13">&nbsp;</td>
			</tr>

		<?	
		if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
		{
		?>
				<tr bgcolor="#FFFFFF">
					<td colspan="3" align="left"><b>Page Total Amount</b></td>
					
					<td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
						if($_GET["total"]=="first")
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$Balance;
						}elseif($_GET["total"]=="pre"){
							$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
							$_SESSION["CurrentTotal"]=$Balance;
						}elseif($_GET["total"]=="next"){
							$_SESSION["grandTotal"] += $Balance;
							$_SESSION["CurrentTotal"]= $Balance;
						}else
							{
							$_SESSION["grandTotal"] = $Balance;
							$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
							}
						echo number_format($_SESSION["grandTotal"],2,'.',',');
						?>
					 </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>	
                </tr>
                <?
            }
            ?>
			</table>		
            <? } else { ?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
			<? } ?>

	  		<? if($agentType == "admin") { ?>
			<tr bgcolor="#FFFFFF">
              <td colspan="15" align="left">
				  		<a href="#" onClick="$('#manual-row').show();" style="color:#0066FF">Manual Deposit/Withdraw into admin staff account?</a>
					&nbsp;
			  </td>
            </tr>
			<? } ?>
       </table>
	   
	<?	
		 } 
		 else 
		 {
			$_SESSION["grandTotal"]="";
			$_SESSION["CurrentTotal"]="";
	 
	
			$_SESSION["fMonth"]="";
			$_SESSION["fDay"]="";
			$_SESSION["fYear"]="";
			
			$_SESSION["tMonth"]="";
			$_SESSION["tDay"]="";
			$_SESSION["tYear"]="";
		}
	?>
		</td>
	</tr>
	<? if($slctID!=''){?>
		<tr id="actionBtn">
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
			</td>
		  </tr>
	<? }?>
	</form>
	</td>
	</tr>
	
	
	<tr id="manual-row" style="display:none">
	<td align="center">
		<form action="" name="dw" method="post" onSubmit="return submitForm();">
		<table width="70%" align="center" cellpadding="2" cellspacing="0" border="0" class="boundingTable">
			<tr bgcolor="#CCCCCC">
				<td colspan="2"><h3>Deposit / Withdraw into Admin Staff Account</h3></td>
			</tr>
			<tr>
				<td align="right" class="tdTitle">Amount&nbsp;<font color="red">*</font></td>
				<td><input type="text" name="amount" id="amount" maxlength="8" size="20" class="inputBoxesNumerics" /></td>
			</tr>
			<tr valign="top">
				<td align="right" class="tdTitle">Amount Description</td>
				<td><textarea name="description" id="description" rows="5" cols="50" wrap="soft"></textarea></td>
			</tr>
			<tr valign="top">
				<td align="right" class="tdTitle">Amount Type&nbsp;<font color="red">*</font></td>
				<td>
					<select name="amountType" id="amountType">
						<option value="DEPOSIT">Deposit</option>
						<option value="WITHDRAW">Withdraw</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right" class="tdTitle">Amount Currency&nbsp;<font color="red">*</font></td>
				<td>
					<?
						$strCurrencySql = "select
										cn.currency,
										sr.fromCountryId
									from
										services as sr,
										countries as cn
									where
										cn.countryId = sr.fromCountryId limit 1
								";
					
						$sendingAgentData = selectMultiRecords( $strCurrencySql );
					?>
					<select name="currency" id="currency">
						<? 
							foreach ($sendingAgentData as $uniqueCurrency )
							{
								$uniqueCurrency = $uniqueCurrency["currency"];
								$selected = "";
								if( $_REQUEST["currency"] == $uniqueCurrency )
									$selected = "selected='selected'";
								?>
									<option <?=$selected;?> value="<?=trim($uniqueCurrency)?>" ><?=trim($uniqueCurrency); ?></option>
								<?
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<input type="hidden" name="agentID" value="<?=$intUserId?>" />
					<input type="hidden" name="fDay" value="<?=$_REQUEST["fDay"]?>" />
					<input type="hidden" name="fMonth" value="<?=$_REQUEST["fMonth"]?>" />
					<input type="hidden" name="fYear" value="<?=$_REQUEST["fYear"]?>" />
					
					<input type="hidden" name="tDay" value="<?=$_REQUEST["tDay"]?>" />
					<input type="hidden" name="tMonth" value="<?=$_REQUEST["tMonth"]?>" />
					<input type="hidden" name="tYear" value="<?=$_REQUEST["tYear"]?>" />
					
					<input type="submit" name="addManual" id="addManual" value="Submit" />
					&nbsp;&nbsp;
					<input type="reset" value="Cancel Action" onClick="$('#manual-row').hide();" />
				</td>
			</tr>
		</table>
		</form>
	</td>
	</tr>
	
</table>
</body>
</html>