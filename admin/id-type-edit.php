<?
	// Usman Ghani
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	//$parentID = $_SESSION["loggedUserData"]["userID"];
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
$serviceTypeVal = $_REQUEST["useService"];
	if ( !empty($_REQUEST["id"]) )
	{
		// Fetch already availble ID Type
		$q = "select * from id_types where id = '" . $_REQUEST["id"] . "'";
		$idType = selectFrom($q);
		if ( empty($idType) )
			$errorMessage = "Invalid id provided.";
	}
	else
	{
		$errorMessage = "Please provide an id.";
	}

	// Handle form submit request
	if( !empty($_REQUEST["id_type_id"]) && !empty($_REQUEST["submitBtn"]))
	{
		if ( !empty($_REQUEST["title"]) )
		{
		    if(defined("CONFIG_MULTI_ID_MANDATORY_SEPARATE") && CONFIG_MULTI_ID_MANDATORY_SEPARATE=="1"){
				$mandCustFieldTypes = ",mandatorySender";
				$mandBenFieldTypes = "mandatoyBeneficiary";
				$custFieldValues = "'".$_REQUEST["mandatorySender"]."',"; 
				$benFieldValues = "'".$_REQUEST["mandatoyBeneficiary"]."'";
				$mandBenCustUpdateQ = $mandCustFieldTypes ."=". $custFieldValues. $mandBenFieldTypes ."=". $benFieldValues ;
		     }
			 if(CONFIG_SENDER_AS_COMPANY == "1"){
			 	$senderAsCompanyFieldTypes = ",show_on_sender_as_company";
				$senderAsCompanyFieldValues = "'".(!empty($_REQUEST["showOnSenderAsCompany"]) ? "Y":"N")."',"; 
				$senderAsCompanyUpdateSql = $senderAsCompanyFieldTypes ."=".$senderAsCompanyFieldValues;
			 }
			  if(CONFIG_BENEFICIARY_AS_COMPANY == "1"){
			 	$benAsCompanyFieldTypes = "show_on_beneficiary_as_company";
				$benAsCompanyFieldValues = "'".(!empty($_REQUEST["showOnBeneficiaryAsCompany"]) ? "Y":"N")."'"; 
				$benAsCompanyUpdateSql = $benAsCompanyFieldTypes ."=".$benAsCompanyFieldValues;
			 }
			 
			$q = "update id_types "
				  . " set "
						. " title='" . $_REQUEST["title"] . "', "
						. " mandatory='" . $_REQUEST["mandatory"] . "', "
						. " show_on_sender_page='" . ( !empty($_REQUEST["showOnSenderPage"]) ? "Y" : "N" ) . "', "
						. " show_on_ben_page='" . ( !empty($_REQUEST["showOnBeneficiaryPage"]) ? "Y" : "N" ) . "' "
						.$mandBenCustUpdateQ
						.$senderAsCompanyUpdateSql
						.$benAsCompanyUpdateSql
				  . " where "
				  		. " id='" . $_REQUEST["id_type_id"] . "'";
			if($countryBasedFlag){
				$serviceId = selectFrom("select sim from service_ID_Mandatory where  serviceAvailable='" . $serviceTypeVal . "' AND multiple_id='".$_REQUEST["id_type_id"]."'");
				if($serviceId["sim"]=="" && $serviceTypeVal!=""){
					insertInto("insert into service_ID_Mandatory(
																	serviceAvailable,
																	custMandatory,
																	benMandatory,
																	multiple_id
																) 
														 values(
																	'".$serviceTypeVal."',
																	'".(!empty($_REQUEST["mandatorySender"]) ? "Y" : "N" )."',
																	'".(!empty($_REQUEST["mandatoyBeneficiary"]) ? "Y" : "N" )."',
																	'".$_REQUEST["id_type_id"]."'
																)");
				}
				elseif($serviceTypeVal!=""){
					update("update service_ID_Mandatory "
						  . " set "
								. " custMandatory='" . ( !empty($_REQUEST["mandatorySender"]) ? "Y" : "N" ) . "', "
								. " benMandatory='" . ( !empty($_REQUEST["mandatoyBeneficiary"]) ? "Y" : "N" ) . "' "
						  . " where "
								. " serviceAvailable='" . $serviceTypeVal . "' AND multiple_id='".$_REQUEST["id_type_id"]."'");
				}
			}
			update( $q );			
			$done = true;
			//debug($q,true);
		}
		else
		{
			$formTitleError = "<span class='formError'>Title is required</span>";
		}
	}
	if($countryBasedFlag){
		$contentsService = selectMultiRecords("select DISTINCT(serviceAvailable) from " . TBL_SERVICE_NEW. " order by serviceAvailable");	
		if($serviceTypeVal==""){
			$serviceTypeVal  = $contentsService[0]["serviceAvailable"];
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage ID Types</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="css/general.css" rel="stylesheet" type="text/css">

<? // include ("javaScript.php"); ?>
<script type="text/javascript">
	<?
		if ( isset($done) )
		{
			?>
				//alert(window.opener.document.location);
				//window.opener.document.location.reload();
				window.opener.document.location = window.opener.document.location;
				window.close();
			<?
		}
	?>

	function checkForm(theForm)
	{
		if ( trim( theForm.title.value ) == "" )
		{
			alert("Please provide a title.");
			theForm.title.focus();
			return false;
		}
		if ( /^\d+$/.test( theForm.title.value ) )
		{
			alert("Please provide a valid title.");
			theForm.title.focus();
			return false;
		}
		return true;
	}

	function trim(stringToTrim)
	{
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
	function submitOnChange()
	{
		document.getElementById("idType").action='id-type-edit.php';
		document.getElementById("idType").submit();
	}
</script>

</head>
<body>
<?
	if ( isset($errorMessage) )
	{
		echo $errorMessage;
	}
	else
	{
		?>
		<form name="idType" id="idType" method="post" onsubmit="return checkForm(this);">
			<table border="1" cellspacing="1" cellpadding="5" align="center">
				<tr>
					<td class="tableHeading" colspan="2">Edit ID Type</td>
				</tr>
				<tr>
					<td class="fieldLable">
						ID Title
					</td>
					<td>
						<input type="hidden" name="id_type_id" value="<?=$idType["id"];?>" />
						<input type="hidden" name="id" id="id" value="<?=$idType["id"];?>" />
						<input type="text" name="title" value="<?=$idType["title"];?>" /> <?=( !empty($formTitleError) ? "<br />" . $formTitleError : ""); ?>
					</td>
				</tr>
				<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE !="1"){  ?>
				<tr>
					<td class="fieldLable">
						Mandatory
					</td>
					<td>
						<input type="radio" name="mandatory" value="Y" <?=($idType["mandatory"] == "Y" ? "checked='checked'" : "");?> />&nbsp;Yes&nbsp;&nbsp;
						<input type="radio" name="mandatory" value="N" <?=($idType["mandatory"] == "N" ? "checked='checked'" : "");?> />&nbsp;No
					</td>
				</tr>
				<? } ?>
				<? if($countryBasedFlag && count($contentsService)>0){?>
				<tr>
				<td valign="top" class="fieldLable">Service Type</td>
					<td>

							<select name="useService" id="useService"  onChange="submitOnChange();">
							<? for($sA=0;$sA<count($contentsService);$sA++){?>
								<option value="<?=$contentsService[$sA]["serviceAvailable"]?>" <?=$selectedSer?>><?=$contentsService[$sA]["serviceAvailable"]?></option>
							<? }?>
							</select>
										<script language="JavaScript">SelectOption(document.forms[0].useService, "<?=$serviceTypeVal?>");</script>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td valign="top" class="fieldLable">Available on</td>
					<td>
						<input type="checkbox" value="Y" name="showOnSenderPage" <?=($idType["show_on_sender_page"] == "Y" ? "checked='checked'" : "");?> /> 
						Add Sender Page
						<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE == "1"){  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?
							$mandarotyCustCheck = ($idType["mandatorySender"] == "Y" ? "checked='checked'" : "");
							$mandarotyBenCheck = ($idType["mandatoryBeneficiary"] == "Y" ? "checked='checked'" : "");
							if($countryBasedFlag){
								$serviceMandQ = "select * from service_ID_Mandatory where multiple_id ='".$idType["id"]."' and serviceAvailable='".$serviceTypeVal."'";
								$serviceMandR = selectFrom($serviceMandQ);
								$mandarotyCustCheck = ($serviceMandR["custMandatory"] == "Y" ? "checked='checked'" : "");
								$mandarotyBenCheck = ($serviceMandR["benMandatory"] == "Y" ? "checked='checked'" : "");
							}
						 ?>
						<font color="#005b90"><strong>Mandatory</strong></font>&nbsp;&nbsp;
						 <input type="checkbox" value="Y" name="mandatorySender" <?=$mandarotyCustCheck;?>/>
						<? }?>
					
						<br />
						<input type="checkbox" value="Y" name="showOnBeneficiaryPage" <?=($idType["show_on_ben_page"] == "Y" ? "checked='checked'" : "");?> /> Add Beneficiary Page
						<? if(CONFIG_MULTI_ID_MANDATORY_SEPARATE == "1"){  ?>&nbsp; <font color="#005b90"><strong>Mandatory</strong></font> &nbsp; 
						<input type="checkbox" value="Y" name="mandatoyBeneficiary" <?=$mandarotyBenCheck;?> />
						<? } ?>
						<? if(CONFIG_SENDER_AS_COMPANY == "1") { ?>
						<br />
						<input type="checkbox" value="Y" name="showOnSenderAsCompany" <?=($idType["show_on_sender_as_company"] == "Y" ? "checked='checked'" : "");?> /> 
								Add Sender As Company 
						<? } ?>
							  
						<? if(CONFIG_BENEFICIARY_AS_COMPANY == "1") { ?>
						<br />
					   <input type="checkbox" value="Y" name="showOnBeneficiaryAsCompany" <?=($idType["show_on_beneficiary_as_company"] == "Y" ? "checked='checked'" : "");?> /> Add Beneficiary As Compnay
							
						   <? } ?>
					</td>
				</tr>
				<tr>
				<tr>
					<td>&nbsp;
					</td>
					<td>
						<input name="submitBtn" type="submit" value="Save" />&nbsp;
						<input type="button" value="Cancel" onClick="window.close();" />
						<? if ( isset($done) ) { ?>
							<input type="hidden" name="isEdit" value="y" />
						<? } ?>
					</td>
				</tr>
			</table>
		</form>
		<?
	}
?>
	
</body>
</html>