<?php

use Payex\Helper\AgentHelper;
use Payex\Repository\AgentRepository;

require '../src/bootstrap.php';

/**
	 * @package : online registered users
	 * @subpackage: Send Online Link To Customers
	 * @author: M. Awais Umer
	 */
	//ini_set("display_errors","on");
	session_start();

    $userId = $_SESSION['loggedUserData']['userID'];
    $agentRepository = new AgentRepository($app->getDb());
    $agentHelper = new AgentHelper();
    $agent = $agentRepository->getById($userId);

	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();
	$limit = 50;
	if(isset($_GET['offset']))
		$offset = $_GET['offset'];
	else
		$offset = 0;
	$next = $offset + $limit;
	$prev = $offset - $limit;
	$strCondition = '';
	
	if(isset($_REQUEST['query'])){
	if($_REQUEST['query'] != ''){
	$queryforname = $_REQUEST['query'];
	}
	
	if($_POST['query'] != ''){
	$queryforname = $_POST['query'];
	}
	$counter =0;
	switch($queryforname){
	
	case querInNumDescendingOrder:
	
	/**making session variable and 
	*storing registering it 
	**/	
	if($_REQUEST['orderPointer'] ==''){
	$_SESSION['orderPointer']='customer.accountName ASC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.customerName ASC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.accountName ASC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.customerName DESC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.accountName DESC';
	$orderPointer = $_SESSION['orderPointer']; 
	}
	else if($_REQUEST['orderPointer'] == 'customer.accountName ASC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.accountName DESC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.accountName DESC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.accountName ASC';
	$orderPointer = $_SESSION['orderPointer']; 
	}
	
	if(isset($_REQUEST['customerName']) && !empty($_REQUEST['customerName'])){
			$strCustomerName .= trim($_REQUEST['customerName']);
			$strCondition = " AND customer.customerName LIKE '$strCustomerName%'";
		}
		
		if(($_REQUEST['customerNumber']) && !empty($_REQUEST['customerNumber'])){
			$strCustomerNumber = trim($_REQUEST['customerNumber']);
			$strCondition .= " AND customer.accountName = '$strCustomerNumber'";
		}
		if(($_REQUEST['fYear']) && ($_REQUEST['fMonth']) && ($_REQUEST['fDay'])){
			$strFromDate = $_REQUEST['fYear']."-".$_REQUEST['fMonth']."-".$_REQUEST['fDay']." 00:00:00";
			$strToDate = $_REQUEST['tYear']."-".$_REQUEST['tMonth']."-".$_REQUEST['tDay']." 23:59:59";
		}
		
		$strCondition .= " AND (customer.created BETWEEN '$strFromDate' AND '$strToDate')";
		$strQueryTotal = "SELECT count(customer.customerID) AS records FROM ".TBL_CUSTOMER." WHERE password != '' AND email != '' AND customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition";
		$arrTotalRecords = selectFrom($strQueryTotal);
		
		$strQuery = "SELECT customer.accountName AS accountName, customer.password AS password, customer.email, customer.customerName AS customerName,customer.customerStatus AS customerStatus ,  customer.customerID AS customerID, admin.name AS accountManager FROM ".TBL_CUSTOMER." left join admin on customer.parentID=admin.userID WHERE customer.password != '' AND customer.email != '' AND customer.customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition order by ".$orderPointer." ";
		$strQueryLimit = " LIMIT $offset, $limit ";
		$strQuery .= $strQueryLimit;
		
		$arrCustomer = selectMultiRecords($strQuery);
		$intTotalCustomers = $arrTotalRecords['records'];
		$intCustomers = count($arrCustomer);
	
	break;
	 
	case querInNameDescendingOrder:
	
	/**making session variable and 
	*storing registering it 
	**/
	if($_REQUEST['orderPointer'] ==''){
	$_SESSION['orderPointer']='customer.customerName ASC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.customerName ASC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.customerName DESC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.customerName DESC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.customerName ASC';
	$orderPointer = $_SESSION['orderPointer']; 
	}
	else if($_REQUEST['orderPointer'] == 'customer.accountName ASC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.customerName ASC';
	$orderPointer = $_SESSION['orderPointer'];
	}
	else if($_REQUEST['orderPointer'] == 'customer.accountName DESC'){
	unset($_SESSION['orderPointer']);
	$_SESSION['orderPointer'] ='customer.customerName DESC';
	$orderPointer = $_SESSION['orderPointer']; 
	}
	
	if(isset($_REQUEST['customerName']) && !empty($_REQUEST['customerName'])){
			$strCustomerName .= trim($_REQUEST['customerName']);
			$strCondition = " AND customer.customerName LIKE '$strCustomerName%'";
		}
		
		if(($_REQUEST['customerNumber']) && !empty($_REQUEST['customerNumber'])){
			$strCustomerNumber = trim($_REQUEST['customerNumber']);
			$strCondition .= " AND customer.accountName = '$strCustomerNumber'";
		}
		if(($_REQUEST['fYear']) && ($_REQUEST['fMonth']) && ($_REQUEST['fDay'])){
			$strFromDate = $_REQUEST['fYear']."-".$_REQUEST['fMonth']."-".$_REQUEST['fDay']." 00:00:00";
			$strToDate = $_REQUEST['tYear']."-".$_REQUEST['tMonth']."-".$_REQUEST['tDay']." 23:59:59";
		}
		
		$strCondition .= " AND (customer.created BETWEEN '$strFromDate' AND '$strToDate')";
		$strQueryTotal = "SELECT count(customer.customerID) AS records FROM ".TBL_CUSTOMER." WHERE email != '' AND customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition";
		
		$arrTotalRecords = selectFrom($strQueryTotal);
		
		$strQuery = "SELECT customer.accountName AS accountName, customer.password AS password, customer.email, customer.customerName AS customerName,customer.customerStatus AS customerStatus ,  customer.customerID AS customerID, admin.name AS accountManager FROM ".TBL_CUSTOMER." left join admin on customer.parentID=admin.userID WHERE customer.email != '' AND customer.customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition order by ".$orderPointer." " ;
		$strQueryLimit = " LIMIT $offset, $limit";
		$strQuery .= $strQueryLimit;
		
		$arrCustomer = selectMultiRecords($strQuery);
		$intTotalCustomers = $arrTotalRecords['records'];
		$intCustomers = count($arrCustomer);
	
	break;
	
	default:
	
	}
	}
    // if (isset($_REQUEST['search'])) {

        /**making session variable and
         *storing registering it
         **/

        if ($_REQUEST['orderPointer'] == '') {
            $_SESSION['orderPointer'] = 'customer.customerName ASC';
            $orderPointer = $_SESSION['orderPointer'];
        } else
            if ($_REQUEST['orderPointer'] == 'customer.customerName ASC') {
                $orderPointer = $_REQUEST['orderPointer'];
            } else if ($_REQUEST['orderPointer'] == 'customer.customerName DESC') {
                $orderPointer = $_REQUEST['orderPointer'];
            } else if ($_REQUEST['orderPointer'] == 'customer.accountName ASC') {
                $orderPointer = $_REQUEST['orderPointer'];
            } else if ($_REQUEST['orderPointer'] == 'customer.accountName DESC') {
                $orderPointer = $_REQUEST['orderPointer'];
            }


        if (isset($_POST['customerName']) && !empty($_POST['customerName'])) {
            $strCustomerName .= trim($_POST['customerName']);
            $strCondition = " AND customer.customerName LIKE '$strCustomerName%'";
        } elseif (isset($_GET['customerName']) && !empty($_GET['customerName'])) {
            $strCustomerName .= trim($_GET['customerName']);
            $strCondition = " AND customer.customerName LIKE '$strCustomerName%'";
        }

        if ($agentHelper->isAgent($agent)) {
            $strCondition .= " AND agentID = '{$agent->getId()}'";
        }

        if (isset($_POST['customerNumber']) && !empty($_POST['customerNumber'])) {
            $strCustomerNumber = trim($_POST['customerNumber']);
            $strCondition .= " AND customer.accountName = '$strCustomerNumber'";
        } elseif (isset($_GET['customerNumber']) && !empty($_GET['customerNumber'])) {
            $strCustomerNumber = trim($_GET['customerNumber']);
            $strCondition .= " AND customer.accountName = '$strCustomerNumber'";
        }
        if (isset($_POST['fYear']) || isset($_POST['fMonth']) || isset($_POST['fDay'])) {
            $strFromDate = $_POST['fYear'] . "-" . $_POST['fMonth'] . "-" . $_POST['fDay'] . " 00:00:00";
            $strToDate = $_POST['tYear'] . "-" . $_POST['tMonth'] . "-" . $_POST['tDay'] . " 23:59:59";
        } else {
			$strFromDate =  date("Y")."-"."01"."-01 00:00:00";
	        $strToDate =  date("Y")."-".date("m")."-".date("d")." 23:59:59";
            // $strFromDate = $_GET['fYear'] . "-" . $_GET['fMonth'] . "-" . $_GET['fDay'] . " 00:00:00";
            // $strToDate = $_GET['tYear'] . "-" . $_GET['tMonth'] . "-" . $_GET['tDay'] . " 23:59:59";
        }
        $strCondition .= " AND (customer.created BETWEEN '$strFromDate' AND '$strToDate')";
        $strQueryTotal = "SELECT count(customer.customerID) AS records FROM " . TBL_CUSTOMER . " WHERE email != '' AND customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition";
        $arrTotalRecords = selectFrom($strQueryTotal);

        $strQuery = "SELECT customer.accountName AS accountName, customer.password AS password, customer.email, customer.customerName AS customerName,customer.customerStatus AS customerStatus ,  customer.customerID AS customerID, admin.name AS accountManager FROM " . TBL_CUSTOMER . " left join admin on customer.parentID=admin.userID WHERE customer.email != '' AND customer.customerStatus = 'Enable' AND customer.is_sent = 'N' $strCondition ORDER BY " . $orderPointer . " ";
        $strQueryLimit = " LIMIT $offset, $limit";
        $strQuery .= $strQueryLimit;

        $arrCustomer = selectMultiRecords($strQuery);
        $intTotalCustomers = $arrTotalRecords['records'];
        $intCustomers = count($arrCustomer);
    //}
?>
<html>
	<head>
		<title>Send Online Link To Customers</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="css/style_admin.css"/>
		<script src="javascript/jquery-1.7.2.min.js"></script>
		<script src="javascript/bootstrap.js"></script>
		<script>
			
			var counter = 0;
			
			$(document).ready(
				function(){
					function checkAll(){
						$('input:checkbox').each(
							function(){
								$('input:checkbox').attr('checked', true);
							}
						);
					}
					
					function uncheckAll(){
						$('input:checkbox').each(
							function(){
								$('input:checkbox').attr('checked', false);
							}
						);
					}
					
					$('#all').change(
						function(){
							if($('#all').is(':checked'))
								checkAll();
							else
								uncheckAll();
						}
					);
					
					function SelectOption(OptionListName, ListVal){
						for (i=0; i < OptionListName.length; i++)
						{
							if (OptionListName.options[i].value == ListVal)
							{
								OptionListName.selectedIndex = i;
								break;
							}
						}
					}
					SelectOption(document.forms[0].tDay, "<?php echo date("d");?>");
					SelectOption(document.forms[0].tMonth, "<?php echo date("m");?>");
					SelectOption(document.forms[0].tYear, "<?php echo date("Y");?>");
					
				}
			);
			
			
		</script>
	</head>
	<body>
		<div class="container">
			<h3 class="well-small align_center well">Send Online Link To Customers</h3>
			<div class="form_cont">
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="search">
					<table class="table table-striped short_table">
						<tr>
							<th>From Date:</th>
							<td>
								<select name="fDay" id="fDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="fMonth" id="fMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="fYear" id="fYear" class="input-small">
									<!--<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>-->
										<?php 
$currentYear = date("Y");
$years = range ($currentYear, 2005);
foreach ($years as $value) {
echo "<option value=\"$value\">$value</option>\n";
}

?>
								</select>
							</td>
						</tr>
						<tr>
							<th>To Date:</th>
							<td>
								<select name="tDay" id="tDay" class="input-mini">
									<?php 
										for($d = 1; $d <= 31; $d++){
											$strDay = str_pad($d, 2, "0", STR_PAD_LEFT);
											echo "<option value='$strDay'>$strDay</option>";
										}
									?>
								</select>
								<select name="tMonth" id="tMonth" class="input-small">
									<option value='01'>Jan</option>
									<option value='02'>Feb</option>
									<option value='03'>Mar</option>
									<option value='04'>Apr</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>Aug</option>
									<option value='09'>Sep</option>
									<option value='10'>Oct</option>
									<option value='11'>Nov</option>
									<option value='12'>Dec</option>
								</select>
								<select name="tYear" id="tYear" class="input-small">
									<!--<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>-->	<?php 
$currentYear = date("Y");
$years = range ($currentYear, 2005);
foreach ($years as $value) {
echo "<option value=\"$value\">$value</option>\n";
}

?>
								</select>
							</td>
						</tr>
						<tr>
							<th>Customer Name:</th>
							<td>
								<input name="customerName" id="customerName" type="text" class="input-medium"/>
							</td>
						</tr>
						<tr>
							<th>Customer Number: </th>
							<td><input name="customerNumber" id="customerNumber" type="text" class="input-medium"/></td>
						</tr>
						<tr>
							<td colspan="2" class="align_center">
								<input name="search" id="search" type="submit" class="btn btn-primary" value="Search"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<?php 
				if($intCustomers > 0){
			?>
			<table class="table table-striped ">
				<tr>
					<th width="80%">
						Showing <?php echo ($offset+1)." - ".($offset+$intCustomers)." of ".$intTotalCustomers; ?>
					</th>
					<th class="align-right">
						<?php 
							if($prev >= 0 ){
						?>
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?offset=0&fDay=<?php echo $_REQUEST['fDay'];?>&fMonth=<?php echo $_REQUEST['fMonth'];?>&fYear=<?php echo $_REQUEST['fYear']?>&tDay=<?php echo $_REQUEST['tDay'];?>&tMonth=<?php echo $_REQUEST['tMonth'];?>&tYear=<?php echo $_REQUEST['tYear'];?>&customerName=<?php echo $_REQUEST['customerName'];?>&customerNumber=<?php echo $_REQUEST['customerNumber']; ?>&search=search&orderPointer=<?echo $orderPointer;?>">First</a>
						&nbsp;
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?offset=<?php echo $prev; ?>&fDay=<?php echo $_REQUEST['fDay'];?>&fMonth=<?php echo $_REQUEST['fMonth'];?>&fYear=<?php echo $_REQUEST['fYear']?>&tDay=<?php echo $_REQUEST['tDay'];?>&tMonth=<?php echo $_REQUEST['tMonth'];?>&tYear=<?php echo $_REQUEST['tYear'];?>&customerName=<?php echo $_REQUEST['customerName'];?>&customerNumber=<?php echo $_REQUEST['customerNumber']; ?>&search=search&orderPointer=<?echo $orderPointer;?>">Previous</a>
						<?php 
							}
							if($next < $intTotalCustomers && $next > 0){
								$allOffset = (ceil($intTotalCustomers/$limit) - 1) * $limit;
						?>
						&nbsp;
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?offset=<?php echo $next; ?>&fDay=<?php echo $_REQUEST['fDay'];?>&fMonth=<?php echo $_REQUEST['fMonth'];?>&fYear=<?php echo $_REQUEST['fYear']?>&tDay=<?php echo $_REQUEST['tDay'];?>&tMonth=<?php echo $_REQUEST['tMonth'];?>&tYear=<?php echo $_REQUEST['tYear'];?>&customerName=<?php echo $_REQUEST['customerName'];?>&customerNumber=<?php echo $_REQUEST['customerNumber']; ?>&search=search&orderPointer=<?echo $orderPointer;?>">Next</a>
						&nbsp;
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?offset=<?php echo $allOffset; ?>&fDay=<?php echo $_REQUEST['fDay'];?>&fMonth=<?php echo $_REQUEST['fMonth'];?>&fYear=<?php echo $_REQUEST['fYear']?>&tDay=<?php echo $_REQUEST['tDay'];?>&tMonth=<?php echo $_REQUEST['tMonth'];?>&tYear=<?php echo $_REQUEST['tYear'];?>&customerName=<?php echo $_REQUEST['customerName'];?>&customerNumber=<?php echo $_REQUEST['customerNumber']; ?>&search=search&orderPointer=<?echo $orderPointer;?>">Last</a>
						<?php 
							}
						?>
					</th>
				</tr>
			</table>
			<br />
			<form action="send_online_link_customer_process.php" name="issuePassowrd" method="post">
				<table class="table table-striped table-bordered">
					<tr>
						<th><input name="all" id="all" type="checkbox"/></th>

<th><a onclick='return counter();' href="<?php echo $_SERVER['PHP_SELF']; ?>?query=querInNumDescendingOrder&customerName=<?echo $_POST['customerName'];?>&customerNumber=<?echo $_REQUEST['customerNumber'];?>&fYear=<?echo $_REQUEST['fYear'] ;?>&fMonth=<? echo $_REQUEST['fMonth'];?>&fDay=<?echo $_REQUEST['fDay'];?>&tYear=<?echo $_REQUEST['tYear'];?>&tMonth=<?echo $_REQUEST['tMonth']; ?>&tDay=<?echo $_REQUEST['tDay'];?>&orderPointer=<?echo $orderPointer;?> ">Customer Number</a></th>

<th><a href="<?php echo $_SERVER['PHP_SELF']; ?>?query=querInNameDescendingOrder&customerName=<?echo $_POST['customerName'];?>&customerNumber=<?echo $_REQUEST['customerNumber'];?>&fYear=<?echo 
$_REQUEST['fYear'] ;?>&fMonth=<? echo $_REQUEST['fMonth'];?>&fDay=<?echo $_REQUEST['fDay'];?>&tYear=<?echo $_REQUEST['tYear'];?>&tMonth=<?echo $_REQUEST['tMonth']; ?>&tDay=<?echo $_REQUEST['tDay'];?> &orderPointer=<?echo $orderPointer;?>">Name</th>
						  
						<th>Email</th>
                        <th>Account Manager</th>
                        <th>Status</th>
						<th>Send Link</th>
					</tr>
					<?php 
						for($x = 0; $x < $intCustomers; $x++){
					?>
					<tr>
						<td><input name="customer[<?php echo $x; ?>]" id="customer[<?php echo $x; ?>]" type="checkbox" value="<?php echo $arrCustomer[$x]['customerID']; ?>"/></td>
						<td><?php echo $arrCustomer[$x]['accountName']; ?></td>
						<td><?php echo $arrCustomer[$x]['customerName']; ?></td>
						<td><?php echo $arrCustomer[$x]['email']; ?></td>
                        <td><?php echo $arrCustomer[$x]['accountManager']; ?></td>
                        <td><?php echo $arrCustomer[$x]['customerStatus']; ?></td>
						<td><a href="send_online_link_customer_process.php?customer=<?php echo $arrCustomer[$x]['customerID']; ?>" onClick="return confirm('Are you sure to send link to this customer?');">Send Link</a></td>
					</tr>
					<?php 
						}
					?>
					<tr>
						<td colspan="6" class="align_center">
							<input name="bulkIssuePassword" id="bulkIssuePassword" type="submit" class="btn-danger btn" value="Send"/>
						</td>
					</tr>
				</table>
			</form>
			<?php 
				}elseif(isset($_REQUEST['search'])){
			?>
			<table class="table table-striped table-bordered">
				<tr>
					<th class="align_center">There are no records to view.</th>
				</tr>
			</table>
			<?php 
				}
			?>
		</div>
	</body>
</html>