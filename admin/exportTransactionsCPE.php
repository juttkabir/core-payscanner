<?php
session_start();
include "../include/config.php";
include "security.php";

if (!empty($_REQUEST["recId"])) {
	$strDistributorData = selectFrom("select benAgentId from transactions where dispatchNumber = '" . $_REQUEST["recId"] . "'");
	$distributors = $strDistributorData["benAgentId"];
	if (empty($distributors)) {
		die("No Distributor Found!");
	}

} elseif (empty($_REQUEST["searchTrans"]) || empty($_REQUEST["at"])) {
	die("Invalid command!");
}

$sqlDistributorData = selectFrom("Select username, name from admin where userid = '$distributors'");

if (!isset($arrDistributorOutputFileFormat[$distributors])) {
	die("Invalid distributor!");
}

if ($arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "XLS") {
	header("Content-type: application/x-msexcel");
	$strMainStart = '<table border="1">';
	$strMainEnd = "</table>";
	$strRowStart = "<tr>";
	$strRowEnd = "</tr>";
	$strColumnStart = "<td style='mso-number-format:\@'>";
	$strColumnEnd = "</td>";
} elseif ($arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "TXT") {
	header('Content-type: application/octet-stream');
	$strMainStart = "";
	$strMainEnd = "";
	$strRowStart = "";
	$strRowEnd = "\r\n";
	$strColumnStart = "";
	$strColumnEnd = "\t";
} elseif ($arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "CSV") {
	header('Content-type: application/octet-stream');
	$strMainStart = "";
	$strMainEnd = "";
	$strRowStart = "";
	$strRowEnd = "\r\n";
	$strColumnStart = "";
	$strColumnEnd = ",";
} else {
	die("UNKNOWN FILE FORMAT!");
}

header("Content-disposition: filename=" . $sqlDistributorData["username"] . "-" . date("dmYHis") . "." . $arrDistributorOutputFileFormat[$distributors]["FORMAT"]);
header("Content-Description: PHP/MYSQL Generated Data");
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$extraFields = "";

if (CONFIG_CUSTOMIZED_FIELDS_AMB_EXCHANGE == "1") {
	$extraFields .= ",tr.transAmount,tr.custAgentID";
}
$strFieldsToFetchFromDb = "
					tr.refNumberIM,
					tr.refNumber,
					DATE_FORMAT(tr.transDate, '%d/%m/%Y') as transDate,
					DATE_FORMAT(tr.transDate, '%m/%d/%Y') as transDateMDY,
					concat(tr.localAmount,' ',tr.currencyTo) as localAmount,
					tr.question,
					tr.answer,
					tr.benIDPassword,
					tr.benID,
					tr.tip,
					tr.transType as transType,
					tr.currencyTo as currencyTo,
					tr.totalAmount as totalAmount,
					tr.localAmount as localAmount,
					tr.collectionPointID,
					tr.imfee as fee,
					concat(ben.firstName,' ',ben.middleName,' ',ben.lastName) as beneficiaryName,
					ben.IDType as beneficiaryIDType,
					ben.IDNumber as beneficiaryIDNumber,
					ben.Phone as beneficiaryPhone,
					ben.firstName as benFName,
					ben.middleName as benMName,
					ben.lastName as benLName,
					concat(ben.SOF_Type,' ',ben.SOF) as benFamilyRelation,
					ben.email  as benEmail,
					ben.Address  as benAddress,
					ben.Phone  as benPhone1,
					ben.Mobile  as benMobile,
					ben.Zip  as benZip,
					ben.Country as beneficiaryAddress,
					concat(cust.firstName,' ',cust.middleName,' ',cust.lastName) as senderName,
					cust.country as senderAddress,
					cust.Phone as senderTelephone,
					cust.firstName as senderFName,
					cust.middleName as senderMName,
					cust.lastName as senderLName,
					cust.email as senderEmail,
					cust.Address as senderAddress1,
					cust.Phone as senderPhone1,
					cust.Mobile as senderMobile,
					cust.zip as senderZip,
					cust.dob as senderDOB,
					tbd.accNo,
					tbd.bankName,
					tbd.branchCode,
					tbd.accNo as accountNumber
					" . $extraFields . "
			";

if (!empty($_REQUEST["recId"])) {
	$strSql = "
				select
					$strFieldsToFetchFromDb
				from
					transactions as tr
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where
					tr.dispatchNumber = '" . $_REQUEST["recId"] . "'
				order by
					tr.transDate ";
}

if (strtolower($_REQUEST["searchTrans"]) == "old" && !empty($_REQUEST["at"])) {
	$arrAllTransactions = substr($_REQUEST["at"], 0, strlen($_REQUEST["at"]) - 1);

	$strSql = "
				select
					$strFieldsToFetchFromDb
				from
					transactions as tr
					LEFT JOIN beneficiary as ben ON tr.benID = ben.benID
					LEFT JOIN customer as cust ON tr.customerID = cust.customerID
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where
					tr.transID IN (" . $arrAllTransactions . ")
				order by
					tr.transDate ";

}

//debug($arrDistributorOutputFileFormat);
if (!empty($strSql)) {
	$fullRS = SelectMultiRecords($strSql);
}

//debug($strSql);
//debug($fullRS);

if (CONFIG_EXPORT_TRANSACTION_CUSTOMIZE == "1") {
	for ($m = 0; $m < count($fullRS); $m++) {
		if ($fullRS[$m]["transType"] == "Bank Transfer") {
			$transType = "ACC";
			$accNumber = "Account #";
			$note = $fullRS[$m]["bankName"] . ' ' . $fullRS[$m]["branchCode"];
			$fullRS[$m]["transType"] = $transType;
			$fullRS[$m]["beneficiaryNote"] = $note;
			$fullRS[$m]["accountNumber"] = $fullRS[$m]["accNo"];
			$fullRS[$m]["accNo"] = $note;
			$fullRS[$m]["collectionPointID"] = "";

		} elseif ($fullRS[$m]["transType"] == "Pick up") {
			$transType = "CPK";
			$accNumber = "Cash Pickup";
			$note = $fullRS[$m]["benFName"] . ' ' . $fullRS[$m]["benLName"] . ' ' . $fullRS[$m]["benFamilyRelation"];
			$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address,cp_branch_no  from " . TBL_COLLECTION . " where cp_id='" . $fullRS[$m]["collectionPointID"] . "'");
			$collectionPointBen = $collectionPoint["cp_branch_no"];
			$fullRS[$m]["transType"] = $transType;
			$fullRS[$m]["collectionPointID"] = $collectionPointBen;
			$fullRS[$m]["beneficiaryNote"] = $note;
			$fullRS[$m]["accountNumber"] = "Cash Pickup";
		}

		if ($fullRS[$m]["currencyTo"] == "USD") {
			$currencyTo = "D";
			$fullRS[$m]["currencyTo"] = $currencyTo;
		} elseif ($fullRS[$m]["currencyTo"] == "PKR") {
			$currencyTo = "N";
			$fullRS[$m]["currencyTo"] = $currencyTo;
		}
		$fullRS[$m]["accNo"] = "XXX";

		if (CONFIG_CUSTOMIZED_FIELDS_AMB_EXCHANGE == "1") {
			if (CONFIG_OPERATOR_ID_AnD == "1") {
				$sqlAgentData = selectFrom("SELECT OperatorID from " . TBL_ADMIN_USERS . " where userID = '" . $fullRS[$m]["custAgentID"] . "'");
			}
			$countryCodeData = selectFrom("SELECT countryCode from " . TBL_COUNTRIES . " where upper(countryName) = '" . strtoupper($fullRS[$m]["beneficiaryAddress"]) . "'");
			$fullRS[$m]["srNo"] = ($m + 1) . " ";
			$fullRS[$m]["operatorID"] = $sqlAgentData["OperatorID"];
			$fullRS[$m]["countryCode"] = $countryCodeData["countryCode"];
			$fullRS[$m]["receiversFullName"] = $fullRS[$m]["benFName"] . ' ' . $fullRS[$m]["benLName"];
			$fullRS[$m]["sendersFullName"] = $fullRS[$m]["senderFName"] . ' ' . $fullRS[$m]["senderLName"];
			$fullRS[$m]["cashHardcode"] = "Cash";
		}
	}
}

if (isset($arrDistributorOutputFileFormat[$distributors]["FIELDS"]["beneficiaryIDNumber"])
	|| isset($arrDistributorOutputFileFormat[$distributors]["FIELDS"]["beneficiaryIDType"])) {

	for ($k = 0; $k < count($fullRS); $k++) {
		$strManadatoryIdType = "";
		$strManadatoryIdValue = "";

		$strMultiIdsSql = "
						select
							it.title,
							ut.id_number,
							it.mandatory
						from
							user_id_types as ut,
							id_types as it
						where
							ut.user_id = '" . $fullRS[$k]["benID"] . "'
							AND ut.user_type = 'B'
							AND ut.id_type_id = it.id
							";
		$arrMultiIds = SelectMultiRecords($strMultiIdsSql);
		//debug($strMultiIdsSql);
		//debug($arrMultiIds);
		/* If beneficiary have some multiple IDs */
		if (!empty($arrMultiIds)) {
			/* Search in resultset for any of the mandatory ID */
			foreach ($arrMultiIds as $intMIK => $arrMIV) {
				if ($arrMIV["mandatory"] == "Y") {
					$strManadatoryIdType = $arrMIV["title"];
					$strManadatoryIdValue = $arrMIV["id_number"];
				}
			}

			/* If no mandatory Id found than check for any of the available Id on First Found basis */
			if (empty($strManadatoryIdValue)) {
				$strManadatoryIdType = $arrMultiIds[0]["title"];
				$strManadatoryIdValue = $arrMultiIds[0]["id_number"];
			}

			/* Now assigned the Resultant ID to the main result set */
			$fullRS[$k]["beneficiaryIDType"] = $strManadatoryIdType;
			$fullRS[$k]["beneficiaryIDNumber"] = $strManadatoryIdValue;
		}
	}
}

$strOutput = $strMainStart;
//debug($arrDistributorOutputFileFormat[$distributors], true);

/* If want to show the fields caption at the top of the output file or not */
if ($arrDistributorOutputFileFormat[$distributors]["FIELDCAPTION"] == "1") {
	if (CONFIG_CUSTOMIZED_FIELDS_AMB_EXCHANGE == "1") {
		$strOutput .= $strRowStart;
		$strOutput .= $strColumnStart . "ORIGIN (SEND) LOG SHEET" . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . "DATE" . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . date("j-M-y") . $strColumnEnd;
		for ($d1 = 0; $d1 < 5; $d1++) {
			$strOutput .= $strColumnStart . " " . $strColumnEnd;
		}
		$strOutput .= $strRowEnd;
		$strOutput .= $strRowStart;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . "MONTH" . $strColumnEnd;
		$strOutput .= $strColumnStart . strtoupper(date("F.Y")) . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		for ($d2 = 0; $d2 < 5; $d2++) {
			$strOutput .= $strColumnStart . " " . $strColumnEnd;
		}
		$strOutput .= $strRowEnd;
		$strOutput .= $strRowStart;
		$i = 0;
		$week = 0;
		if (date("w", mktime(0, 0, 0, date(n), 1, date(Y))) <= "6") {
			$week++;
		}

		while ($i <= date("j")) {
			if (date("w", mktime(0, 0, 0, date(n), $i, date(Y))) == "7") {
				$week++;
			}

			$i++;
		}
		$strOutput .= $strColumnStart . "Wk " . $week . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . date("l") . $strColumnEnd;
		$strOutput .= $strColumnStart . " " . $strColumnEnd;
		$strOutput .= $strColumnStart . "TERMINAL ID: Z027" . $strColumnEnd;
		for ($d3 = 0; $d3 < 5; $d3++) {
			$strOutput .= $strColumnStart . " " . $strColumnEnd;
		}

		$strOutput .= $strRowEnd;
	}
	$strOutput .= $strRowStart;
	foreach ($arrDistributorOutputFileFormat[$distributors]["FIELDS"] as $outputKey => $outputVal) {
		if (is_array($outputVal)) {
			$strOutput .= $strColumnStart . $outputVal["TITLE"] . $strColumnEnd;
		} else {
			$strOutput .= $strColumnStart . $outputVal . $strColumnEnd;
		}

	}
	$strOutput .= $strRowEnd;
}

/* values of the transaction according to the bank output file formate */
for ($i = 0; $i < count($fullRS); $i++) {
	$strOutput .= $strRowStart;
	$j = 1;
	foreach ($arrDistributorOutputFileFormat[$distributors]["FIELDS"] as $outputKey => $outputVal) {
		if (is_array($outputVal)) {
			$strOutput .= $strColumnStart . $outputVal["FIXEDVAL"] . $strColumnEnd;
		} else {
			$strOutput .= $strColumnStart . $fullRS[$i][$outputKey] . $strColumnEnd;
		}

		/* Removing the last tab character at the end of the row */
		if ($arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "TXT" && count($arrDistributorOutputFileFormat[$distributors]["FIELDS"]) == $j) {
			$strOutput = substr($strOutput, 0, strlen($strOutput) - 1);
		}

		$j++;
	}

	$strOutput .= $strRowEnd;
}
$strOutput .= $strMainEnd;

if ($arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "CSV" || $arrDistributorOutputFileFormat[$distributors]["FORMAT"] == "TXT") {
	$strOutput = substr($strOutput, 0, strlen($strOutput) - 2);
}

echo $strOutput;
//debug(htmlentities($strOutput));
/* End of output */
?>