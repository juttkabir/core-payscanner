<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
include ("javaScript.php");
$currdate=date('Y-m-d h:i:s');
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
     $entity_country =getLoggedUserEntity($userID);
	 
$totalAmount="";

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("agrandTotal");
session_register("aCurrentTotal");

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

session_register("agrandTotal");
session_register("aCurrentTotal");

if($_POST["Submit"] =="Search")
{
	$_SESSION["agrandTotal"]="";
	$_SESSION["aCurrentTotal"]="";
/*
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	*/
	$_SESSION["abank"] = "";

}

if($_POST["agentID"]!="")
	$_SESSION["abank"] =$_POST["agentID"];
		
if(!empty($_REQUEST["currency"]))
	$currency = $_REQUEST["currency"];		

if ($offset == "")
	$offset = 0;
$limit=50;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

/*************Making of Condition*************/
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
/*************Making of Condition*************/		

if($_POST["Submit"] == "Search" || $_GET["search"] == "search")
{
	if($_POST["agentID"]!=""){
		$Balance = "";
		$_SESSION["abank"] = $_POST["agentID"];
		if($agentType == "SUPA"){
			$_SESSION["abank"] = $_SESSION["loggedUserData"]["userID"];
		}	
	}
	else{ 
		$_SESSION["abank"] = $_GET["agentID"];
	}
		
	if($_SESSION["abank"]=="all"){
		$accountQuery = "SELECT 
							distinct t.agentID 
						 FROM 
							agent_account AS t, 
							".TBL_ADMIN_USERS." AS a 
						 WHERE 
							t.agentID = a.userID" ;
	}elseif($_SESSION["abank"]!=""){
		$accountQuery = "SELECT 
							distinct t.agentID 
						 FROM 
							agent_account AS t, 
							".TBL_ADMIN_USERS." AS a 
						 WHERE 
							t.agentID = a.userID and  
							agentID = '".$_SESSION["abank"]."'
						";
	}else{
		$accountQuery = "SELECT 
							distinct t.agentID 
						FROM 
							agent_account as t, 
							".TBL_ADMIN_USERS." AS a 
						WHERE 
							t.agentID = a.userID AND  
							agentID = '".$_SESSION["loggedUserData"]["userID"]."'
						";
	}
	//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	
	//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	
	
if($_GET["fromDate"]!="")
$_POST["fromDate"] = $_GET["fromDate"] ;
if($_GET["toDate"]!="")
 $_POST["toDate"] = $_GET["toDate"] ;

$fromDatevalue=$_POST["fromDate"];
  $toDatevalue=$_POST["toDate"];
  
  
  
  if(!empty($fromDatevalue)){
  $fromDatevalue = str_replace('/', '-', $fromDatevalue);
  $fromDate= date("Y-m-d",strtotime($fromDatevalue));
 }
 else
  $fromDate=$currdate;
 if(!empty($toDatevalue)){
  $toDatevalue = str_replace('/', '-', $toDatevalue);
  $toDate= date("Y-m-d",strtotime($toDatevalue));
 }
 else
  $toDate=$currdate;
	   
  $fromDateReport=date("m-d-Y",strtotime($fromDate));
  $toDateReport=date("m-d-Y",strtotime($toDate));
	
	$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
	$accountQuery .=  " and $queryDate";
	$accountQuery .= " and status != 'Provisional' ";
	if($agentType == "Branch Manager"){
		$accountQuery .= " and a.parentID = '$changedBy'";								
	}
	
	$userEntity  = $_SESSION["loggedUserData"]["entityId"];
	if($entity_country != ''){	
		$accountQuery .= " and a.entityId = '$userEntity'";								
	}
	$accountQuery .=" AND a.adminType = 'Agent' AND a.isCorrespondent = 'N' ";
	$contentsAcc = selectMultiRecords($accountQuery);
	$allCount = count($contentsAcc);
	$allAmount=0;

	for($k=0;$k < $allCount;$k++)
	{
		$ids=$contentsAcc[$k]["agentID"];
		$cumulativeOpeningBalance = 0;
		$cumulativeClosingBalance = 0;
		$running = 0;
		$agentBalanceFirstQ	= "	SELECT 
									opening_balance, 
									closing_balance, 
									dated
								FROM 
									".TBL_ACCOUNT_SUMMARY."
								WHERE 1
									AND user_id	= '".$ids."' 
									AND currency= '".$currency."'
									AND dated	= (	SELECT 
														MIN(dated) 
													FROM 
														".TBL_ACCOUNT_SUMMARY." 
													WHERE 
														user_id		= '".$ids."' 
														AND dated	>= '".$fromDate."'
												  )
								";
										
		$agentBalanceSecondQ	= "	SELECT 
									opening_balance, 
									closing_balance, 
									dated
								FROM 
									".TBL_ACCOUNT_SUMMARY."
								WHERE 1
									AND user_id	= '".$ids."' 
									AND currency= '".$currency."'
									AND dated	= (	SELECT 
														MAX(dated) 
													FROM 
														".TBL_ACCOUNT_SUMMARY." 
													WHERE 
														user_id		= '".$ids."' 
														AND dated	<= '".$toDate."'
												  )
								";
		
		$agentBalanceFirst	= selectFrom($agentBalanceFirstQ);
		$agentBalanceSecond	= selectFrom($agentBalanceSecondQ);
		$cumulativeOpeningBalance = $agentBalanceFirst["opening_balance"];
		$cumulativeClosingBalance = $agentBalanceSecond["closing_balance"];
		$running	= $cumulativeClosingBalance - $cumulativeOpeningBalance;
		$allAmount += $running;
	}
	$exportQuery = $accountQuery;
	$accountQuery .= " LIMIT $offset , $limit";
	$contentsAcc = selectMultiRecords($accountQuery);
}
?>
	
<html>
<head>
<title>Agent Account</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="./javascript/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#paginationTable").hide();
		$("#actionBtnRow").hide();
		print();
		$("#searchTable").show();
		$("#paginationTable").show();
		$("#actionBtnRow").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm)
{	
	if(theForm.currency.value==""){
		alert("Please select the currency.");
		return false;
	}
	return true;
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
#searchTable,.displayTable,#paginationTable{ 
	border:1px solid #666666;
}
input,select{padding:3px;}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td class="topbar">Agent Account Balance</td>
	</tr>
	<tr>
	<td align="center">
		<form action="Agent_account_statement.php" method="post" name="Search" onSubmit="return checkForm(this)">
		<table id="searchTable" cellpadding="5" width="500px" align="center">
			<tr>
				<td colspan="2" bgcolor="#C0C0C0" style="border:1px solid #333333"><span class="tab-u"><strong><?=__("Search Filters") ?></strong></span></td>
			</tr>
			<tr>
				<td width="150"></td>
				<td width="350"></td>
			</tr>
			<tr>
				<td align="center" colspan="2">From Date 
                  	<input name="fromDate" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    
               </td>
			</tr> 
			<?php	
			//if($agentType=="admin" || $agentType == "Admin Manager"  || $agentType == "Branch Manager")
			if($condition)
			{
				$userEntity  = $_SESSION["loggedUserData"]["entityId"];
				if($entity_country != ''){	
					$extraCondition = "and entityId = '$userEntity'";
				}
			?>
			<tr>
				<td align="right">Agent </td>
				<td align="left"> 
					<select name="agentID" style="font-family:verdana; font-size: 11px; width:300px">
							<option value="">- Select Agent -</option>
							<option value="all">All Agents</option>
							<?php
							$agentTypeStr = "'Agent'";
							$userType = 'SUPA_SUBA'; // only for  Agent and distributor, in any other case userType is not needed- default behaviour agentCompany [username]
							$format = 'username,agentCompany,agentStatus';
							$status = "AND agentStatus = 'Active'";
							$orderBy = " concat(agentStatus),adminType,agentType,agentCompany,username ";
							
	//work by Mudassar Ticket #11425
				if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){						
							$arguments= array("returnType"=>'dd',"userType"=>$userType,"adminType"=>$agentTypeStr,"selectedUser"=>$_REQUEST["agentID"],"format"=>$format,"extraCondition"=>$extraCondition,"status"=>$status,"orderBy"=>$orderBy,"separateStatus"=>true);		
				}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){			
							$arguments= array("returnType"=>'dd',"userType"=>$userType,"adminType"=>$agentTypeStr,"selectedUser"=>$_REQUEST["agentID"],"format"=>$format,"extraCondition"=>$extraCondition,"orderBy"=>$orderBy,"separateStatus"=>true);	
					}
//work end by Mudassar Ticket #11425					
							echo getUserDropdownList($arguments);
						?>
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].agentID, "<?=$_SESSION["abank"]; ?>");</script>
				</td>
			</tr>
			<?php
			} ?>
			<tr>
				<td align="right">Currency <font color="#FF0000">*</font></td>
				<td align="left">
					<?php
					$ledgerCurrency = "select distinct currency from ".TBL_AGENT_ACCOUNT. " WHERE currency!='' ";
					$ledgerCurrencyData = selectMultiRecords($ledgerCurrency);
					?>
					<select name="currency" id="currency">
						<option value="">- Select Currency -</option>
						<?php
						foreach($ledgerCurrencyData as $key=>$val)
						{
							echo "<option value='".$val["currency"]."'>".$val["currency"]."</option>";
						}?>
					</select>
					<script language="JavaScript">SelectOption(document.forms[0].currency, "<?=$currency;?>");</script>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="Submit" value="Search"></td>
			</tr>
		</table>
		</form>
	</td>
</tr>
	<?php
	$values = ("Agent Name,Balance Amount,Agent Status");      
	if(count($contentsAcc) > 0)
	{
	?>
	<tr>
		<td align="center">
		<table width="715" align="center" cellspacing="3" class="displayTable">
			<tr>
				<td align="center">
					<table id="paginationTable" width="715" cellpadding="2" align="center">
						<tr> 
							<td width="500" align="left"><?php if (count($contentsAcc) > 0) {;?> Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of <?=$allCount; ?><?php } ;?></td>
							<?php if ($prv >= 0) { ?>
							<td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&agentID=".$_SESSION["abank"]."&currency=".$currency."&search=search&total=first&fromDate=".$fromDate."&toDate=".$toDate;?>"><font color="#005b90">First</font></a></td>
							<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&agentID=".$_SESSION["abank"]."&currency=".$currency."&search=search&total=pre&fromDate=".$fromDate."&toDate=".$toDate;?>"><font color="#005b90">Previous</font></a> </td>
							<?php } ?>
							<?php 
							if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
							?>
							<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&agentID=".$_SESSION["abank"]."&currency=".$currency."&search=search&total=next&fromDate=".$fromDate."&toDate=".$toDate;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
							<td width="50" align="right"><!--<a href="<?php //print $PHP_SELF . "?agentID=".$slctID."&currency=".$currency."&search=search&total=last";?>"><font color="#005b90">Last</font></a>-->&nbsp; </td>
							<?php } ?>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="25" bgcolor="#C0C0C0" class="tab-u" align="center">Report Date<br /> 
					<?php echo "From ".$fromDateReport." To ".$toDateReport;?>
				</td>
			</tr>
			<tr>
				<th height="25" bgcolor="#C0C0C0"><span class="tab-u">Agent Account Current Balance</span></th>
			</tr>
			<tr>
				<td bgcolor="#EFEFEF"> 
					<table border="0" bordercolor="#EFEFEF" class="displayTable" cellpadding="5">
						<tr bgcolor="#DDDDDD"> 
							<td class="style3" align="left">Cumulative Total</td>
							<td class="style3" align="right"><?=number_format($allAmount,2,'.',',');?></td>
							<td class="style3" align="right">&nbsp;</td>                      
						<tr> 
						<tr bgcolor="#D0D0D0"> 
							<th width="166" align="left"  class="style1">Agent Name</th>
							<th width="175" align="right" class="style1">Balance Amount</th>
							<th width="446" align="left"  class="style1">Agent Status</th>
						</tr>
						<?php
						for($i=0;$i<count($contentsAcc);$i++)
						{
							$q = selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["agentID"]."'");
							
							$id = $contentsAcc[$i]["agentID"];
							//debug($id);
							$Bal = agentBalanceDate($id, $fromDate, $toDate,$currency);
							$Balance += $Bal;
							?>
							<tr bgcolor="#FFFFFF"> 
								<td><?=$q["name"];?> </td>
								<td align="right"><?=number_format($Bal,2,'.',',');?> </td>
								<td align="left"><?=$q["agentStatus"]; ?> </td>
							</tr>
						<?php
						}?>
						<tr bgcolor="#DDDDDD"> 
							<td align="left" class="style3">Current Balance</td>
							<td align="right" class="style3"><?=number_format($Balance,2,'.',','); ?></td>
							<td>&nbsp;</td>
						</tr>
						<tr bgcolor="#DDDDDD">
							<td align="left" class="style3">Running Total</td>
							<td align="right" class="style3">
								<?php
									if($_GET["total"]=="first"){
										$_SESSION["agrandTotal"] = $Balance;
										$_SESSION["aCurrentTotal"]=$Balance;
									}elseif($_GET["total"]=="pre"){
										$_SESSION["agrandTotal"] -= $_SESSION["aCurrentTotal"];			
										$_SESSION["aCurrentTotal"]=$Balance;
									}elseif($_GET["total"]=="next"){
										$_SESSION["agrandTotal"] += $Balance;
										$_SESSION["aCurrentTotal"]= $Balance;
									}else{
										$_SESSION["agrandTotal"] = $Balance;
										$_SESSION["aCurrentTotal"]=$_SESSION["agrandTotal"];
									}
									echo number_format($_SESSION["agrandTotal"],2,'.',',');
								?>	
							</td>
							<td align="left">&nbsp;</td>	
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr id="actionBtnRow">
		<td align="center">
		<form method="post" action="<?=ADMIN_ACCOUNT_URL?>export_excel.php" name="export"> 
			<input type="button" name="Submit2" value="Print This Report" id="printReport">
			<input type="submit" name="export" value="Generate Excel">
			<input type="hidden" name="totalFields" value="<?=$values?>">
			<input type="hidden" name="query" value="<?=$exportQuery?>">
			<input type="hidden" name="transDate" value="<?=$toDate?>">
			<input type="hidden" name="accountDate" value="<?=$fromDate?>">
			<input type="hidden" name="currency" value="<?=$currency?>" />
            <input type="hidden" name="pageUrl" value="<?=$_SERVER['PHP_SELF']?>" />
			<input type="hidden" name="reportName" value="Agent Account Balance">
			<input type="hidden" name="reportFrom" value="export_Agent_account_statement.php">
		</form>
		</td>
	</tr>
	<?php
	}
	else{?>
	<tr bgcolor="#FFFFCC">
		<td align="center"><font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent.</font></td>
	</tr>
	<?php
	}?>
</table>
</body>
</html>
