<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$agentType = getAgentType();
if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI")
{
	$queryCust = "select *  from ".TBL_ADMIN_USERS." where username ='$username'";
	$senderAgentContent = selectFrom($queryCust);	
	$_POST["custAgentID"] = $senderAgentContent["userID"];
}
debug($_REQUEST);
$_SESSION["moneyPaid"] 			= $_POST["moneyPaid"];
$_SESSION["transactionPurpose"] = $_POST["transactionPurpose"];
$_SESSION["fundSources"] 		= $_POST["fundSources"];
$_SESSION["refNumber"] 			= $_POST["refNumber"];
$_SESSION["benAgentID"] 		= $_POST["agentID"];


$_SESSION["bankName"] 		= $_POST["bankName"];
$_SESSION["branchCode"] 	= $_POST["branchCode"];
$_SESSION["branchAddress"] 	= trim($_POST["branchAddress"]);
$_SESSION["swiftCode"] 		= $_POST["swiftCode"];
$_SESSION["accNo"] 			= $_POST["accNo"];
$_SESSION["ABACPF"] 		= $_POST["ABACPF"];
$_SESSION["IBAN"] 			= $_POST["IBAN"];

if($_POST["transID"] != "")
{
	$backUrl = "add-transaction.php?msg=Y&transID=" . $_POST["transID"];
}
else
{
	$backUrl = "add-transaction.php?msg=Y";
}
if(trim($_POST["transType"]) == "")
{
	insertError(TE3);	
	redirect($backUrl);
}
if(trim($_POST["customerID"]) == "")
{
	insertError(TE4);	
	redirect($backUrl);
}
if(trim($_POST["agentID"]) == "" && trim($_POST["transType"]) == "Pick up")
{
	insertError(TE5);	
	redirect($backUrl);
}

if(trim($_POST["benID"]) == "")
{
	insertError(TE6);	
	redirect($backUrl);
}

// Validity cheks for Bank Details of Beneficiary.
if(trim($_POST["transType"]) == "Bank Transfer")
{
	if(trim($_POST["bankName"]) == "")
	{
		insertError(TE14);	
		redirect($backUrl);
	}
	if(trim($_POST["accNo"]) == "")
	{
		insertError(TE18);	
		redirect($backUrl);
	}
/*
	if(trim($_POST["branchCode"]) == "")
	{
		insertError(TE15);	
		redirect($backUrl);
	}*/

	if(trim($_POST["branchAddress"]) == "")
	{
		insertError(TE16);	
		redirect($backUrl);
	}
/*
	if(trim($_POST["swiftCode"]) == "")
	{
		insertError(TE17);	
		redirect($backUrl);
	}
*/

	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "United States")
	{
		insertError(TE19);	
		redirect($backUrl);
	}
	
	if(trim($_POST["ABACPF"]) == "" && $_POST["benCountry"] == "Brazil")
	{
		insertError(TE20);	
		redirect($backUrl);
	}

	if(ibanCountries($_POST["benCountry"]) && trim($_POST["IBAN"]) == "")
	{
		insertError(TE21);	
		redirect($backUrl);
	}
}

if(trim($_POST["refNumber"]) == "")
{
	insertError(TE7);	
	redirect($backUrl);
}
if(trim($_POST["transAmount"]) == "" || $_POST["transAmount"] <= 0)
{
	insertError(TE8);	
	redirect($backUrl);
}

if(trim($_POST["exchangeRate"]) == "" || $_POST["exchangeRate"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["exchangeID"]) == "")
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["localAmount"]) == "" || $_POST["localAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["IMFee"]) == "" || $_POST["IMFee"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["totalAmount"]) == "" || $_POST["totalAmount"] <= 0)
{
	insertError(TE9);	
	redirect($backUrl);
}

if(trim($_POST["transactionPurpose"]) == "")
{
	insertError(TE10);	
	redirect($backUrl);
}

if(trim($_POST["fundSources"]) == "")
{
	insertError(TE11);	
	redirect($backUrl);
}

if(trim($_POST["moneyPaid"]) == "")
{
	insertError(TE12);	
	redirect($backUrl);
}

if(trim($_POST["Declaration"]) != "Y")
{
	insertError(TE13);	
	redirect($backUrl);
}
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><font color="#FFFFFF" size="2">Confirm Transaction.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td align="right"><a href="add-transaction.php" class="style2">Change Information</a></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <br>
            <?
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["custAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$custAgentParentID = $senderAgentContent["parentID"];
	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo $width?>" height="<? echo $height?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table width="277" border="0" align="center">
              <tr>
                <td width="323" height="80" colspan="4" align="center" bgcolor="#D9D9FF"><img src="images/logo.jpg" width="326" height="287"></td>
              </tr>
            </table>
	<?
	}
	?>
            <br>
			<font color="#005b90">Transaction Date: </font><? echo date("F j, Y")?><br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Agent Details</legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Company Name</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentCompany"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
                <td width="100" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">MSB No.</font></td>
                <td width="200"><? echo $senderAgentContent["agentMSBNumber"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Customer Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
		
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $_POST[customerID] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["Email"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["agentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$benAgentParentID = $senderAgentContent["parentID"];
			if($senderAgentContent["userID"] != "")
			{
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email from ".TBL_BENEFICIARY." where benID ='" . $_POST["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Benificiary Name</font></td>
                        <td colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]?></td>
                      </tr>
                      <?
				}
			  if($_POST["transType"] == "Bank Transfer")
			  {
		  			//$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
					?>
                      <tr>
                        <td colspan="2"><span class="style5">Benificiary Bank Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200"><? echo $_POST["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $_POST["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200"><? echo $_POST["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				if($_POST["benCountry"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($_POST["benCountry"] == "Brazil")
				{
					echo  "CPF Number*";
				}
				?>
                          </font></td>
                        <td width="200"><?
				if($_POST["benCountry"] == "United States" || $_POST["benCountry"] == "Brazil")
				{
				?>
                          <? echo $_POST["ABACPF"]; ?>
                          <?
				}
				?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200"><? echo $_POST["branchAddress"]; ?>                        </td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Swift Code</font></td>
                        <td width="200"><? echo $_POST["swiftCode"]; ?>                        </td>
                        <td width="100" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" title="For european Countries only">&nbsp;</td>
                      </tr>
                      <?
  }
  ?>
            </table>
              </fieldset></td>
              </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Payment Collection point </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><font color="#005b90">Branch Name </font></td>
                <td><? echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><? echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Bank Name </font></td>
                <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
              <?
			  }
			  ?>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><font color="#005b90"><font color="#005b90">Ref. Number</font></font></td>
                <td width="200"><? echo $_POST["refNumber"]?> </td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" ><? echo $_POST["transAmount"]?> in  <? echo $_POST["currencyFrom"]?></td>
              </tr>
			  <tr>
                <td width="150" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200"><? echo $_POST["exchangeRate"]?>                  </td>
                <td width="100" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200"><? echo $_POST["localAmount"]?>  in  <? echo $_POST["currencyTo"]?>              </td>
			  </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? echo $systemPre;?> Fee</font></td>
                <td width="200"><? echo $_POST["IMFee"]?>                </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $_POST["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200"><? echo $_POST["transactionPurpose"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="200"><? echo $_POST["fundSources"]?>
                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200"><? echo $_POST["moneyPaid"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $_POST["transType"]?></td>
				<td width="100" align="right">&nbsp;</td>
                <td width="200">&nbsp;</td>
              </tr>
				<? if($_POST["transType"] == "ATM Card") { ?>
					<tr>
						<td width="150" height="20" align="right"><font color="#005b90">Card Name</font></td>
						<td width="200" height="20"><?=$_REQUEST["cardName"]?></td>
						<td width="100" align="right"><font color="#005b90">Card No</font></td>
						<td width="200"><?=$_REQUEST["cardNo"]?></td>
					  </tr>
					  <tr>
						<td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
						<td width="200" height="20"><?=$_REQUEST["bankName"]?></td>
						<td colspan="2">&nbsp;</td>
					  </tr>	
				<? } ?>
              

              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? echo $systemCode;?> </font></td>
                <td width="200" height="20">
				<? 
						$query = "select count(transID) as cnt from ". TBL_TRANSACTIONS." where custAgentID = '".$_POST["custAgentID"]."'";
						$nextAutoID = countRecords($query);
						$nextAutoID = $nextAutoID + 1;
						$cntChars = strlen($nextAutoID);
						if($cntChars == 1)
							$leadingZeros = "00000";
						elseif($cntChars == 2)
							$leadingZeros = "0000";
						elseif($cntChars == 3)
							$leadingZeros = "000";		
						elseif($cntChars == 4)
							$leadingZeros = "00";		
						elseif($cntChars == 5)
							$leadingZeros = "0";		
						if($agentType == "admin" || $agentType == "Call")
						{
							$senderAgent = selectFrom("select username from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."'");
							$imReferenceNumber = strtoupper($senderAgent["username"]) . $leadingZeros . $nextAutoID ;
						}
						else
						{
							$imReferenceNumber = strtoupper($username) . $leadingZeros . $nextAutoID ;
						}

				echo $imReferenceNumber?> </td>
                <td width="100" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
            </table>
            </fieldset>
              </td>
          </tr>
          <tr>
            <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%"> _______________________________<br>
                  <br>
                Agent Official Signature</td>
                <td width="50%" align="right"> _______________________________<br>
                  <br>
                Customer Signature </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center">
				  <form name="addTrans" action="add-transaction-conf.php" method="post">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  				  
				  
	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;
	  ?>


			<input type="submit" name="Submit" value="Confirm Order">
            <input type="button" name="Submit" value="Print this Receipt" onClick="print()">
                  </form>			
			</td>
          </tr>
          <tr>
            <td align="right"><a href="add-transaction.php?transID=<? echo $_POST["transID"]?>" class="style2">Change Information</a></td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>