<?
include ("../include/config.php");
include ("security.php");
session_start();
?>
<html>
<head>
	<title>Update City Service</title>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
			function checkForm()
			{
				if(document.updateCitySrv.country.value == "")
				{
					alert("Please select the Country.");
					updateCitySrv.country.focus();
					return false;	
				}	
				if(document.updateCitySrv.state.value == "")
				{
					alert("Please provide the State Name.");
					updateCitySrv.state.focus();
					return false;	
				}	
				if(document.updateCitySrv.city.value == "")
				{
					alert("Please provide the City Name.");
					updateCitySrv.city.focus();
					return false;	
				}	
			}	
		</script>
		<style type="text/css">
.style2 {
	color: #6699CC;
	font-weight: bold;
}
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <form action="update-city-services-conf.php" method="post" name="updateCitySrv" onSubmit="return checkForm();">
  <tr>
      <td align="center"> 
        <table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update this form according to your Need.</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		  <tr bgcolor="EEEEEE"> 
            <td colspan="2"> 
          </td></tr>
         <tr bgcolor="#ededed">
			<td colspan="2"><a class="style2" href="city-services.php?msg='y'">Go Back</a></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
				  <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>Country Name*:</strong></font></td>
            <td width="210"><SELECT name="country" style="font-family:verdana; font-size: 11px">
            <OPTION value="">-Select Country-</OPTION>
            <? 
						$countryContents = selectMultiRecords("select * from ".TBL_COUNTRY." where countryType like '%destination%'");
						for ($i=0;$i<count($countryContents);$i++){
							?>
						<option value="<? echo $countryContents[$i]["countryName"]?>" <? echo ($countryContents[$i]["countryName"]==$_SESSION["countryName"] ? "selected" : "")?>><? echo $countryContents[$i]["countryName"]?></option>
					<? } ?>
				</SELECT>
              </td>
        </tr>
				
        <tr bgcolor="#ededed">
            <td width="185"><font color="#005b90"><strong>State Name*:</strong></font></td>
            <td width="210"><input type="text" name="state" size="17" value="<? echo $_SESSION["state"]?>">
		          	  </td>
        </tr>
	      <tr bgcolor="#ededed"> 
	      	<td><font color="#005b90"><strong>City Name*:</strong></font></td>
	         <td><input type="text" name="city" size="17" value="<? echo $_SESSION["city"]?>">
	         </td>
	       </tr>  
	       
	       			<tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Distributor Name:</strong></font></td>
		       			<td>
		       				<select name="distributor" style="font-family:verdana; font-size: 11px">
									<option value="0">- Select One -</option>
									<? 
									$queryDist = selectMultiRecords("select * from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent != 'N'  and isCorrespondent != '' and authorizedFor like '%Home Delivery%'");
									for ($i=0;$i<count($queryDist);$i++)
									{
										?>
										<option value="<? echo $queryDist[$i]["userID"]?>" <? echo ($_SESSION["distID"]==$queryDist[$i]["userID"] ? "selected" : "")?>><? echo $queryDist[$i]["agentCompany"]." [".$queryDist[$i]["name"]."]"?></option>
									<? } ?>
								  </select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><font color="#FF0000">
								  <? echo ($_SESSION["ass"]!="" ? $_SESSION["ass"] : "")?>
								</font></i>
		       			</td>
	       			</tr>
	       <tr bgcolor="#ededed">
	       				<td><font color="#005b90"><strong>Home Delivery Service:</strong></font></td>
		       			<td>
		       				<select name="Active" style="font-family:verdana; font-size: 11px">
									<option value="No" <? echo ($_SESSION["enDis"]=="Disabled" ? "selected" : "")?>><? echo "Disabled"?></option>
									<option value="Yes" <? echo ($_SESSION["enDis"]=="Enabled" ? "selected" : "")?>><? echo "Enabled"?></option>
								  </select>
		       			</td>
	       			</tr>
	       
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" value=" Update ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>