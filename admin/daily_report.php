<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
extract(getHttpVars());

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$reportName='sender-trans-summary.php';
$reportLabel='Sender Transaction Summary';
$reportFlag=true;
$executeQuery=false;
$filterValue=array();
$filterName=array();

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$userID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType !="SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}

    $rfDay = $fDay;
	  $rfMonth =$fMonth;
	  $rfYear =$fYear;
	
	  $rtDay=$tDay;
	  $rtMonth =$tMonth;
	  $rtYear =	$tYear;

  $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
  $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	
	
/*	if($fromDate!='' && $toDate!=''){
		$fD = explode("-",$fromDate);
		if (count($fD) == 3)
		{
				$rfDay = $fD[2];
				$rfMonth = $fD[1];
				$rfYear = $fD[0];
		}
				$tD = explode("-",$toDate);
		if (count($tD) == 3)
		{
				$rtDay = $tD[2];
				$rtMonth = $tD[1];
				$rtYear = $tD[0];
		}
	}	
		 $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
  	 $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	
*/


	
if ($Submit == "Search") {
	
			
	/* $query = "SELECT t.transID , t.customerID , t.custAgentID , t.benID , t.benAgentID , t.transAmount , t.exchangeRate , t.localAmount , t.transDate , t.transStatus , t.transType , t.refNumberIM , t.collectionPointID , t.moneyPaid , t.branchName from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition; */
	$query = "SELECT t.transID , t.customerID , t.custAgentID , t.benID , t.benAgentID , t.transAmount , t.exchangeRate , t.localAmount , t.transDate , t.transStatus , t.transType , t.refNumberIM , t.	currencyFrom, t.currencyTo , t.collectionPointID , t.moneyPaid , t.branchName from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
	$queryCnt = "SELECT count(transID) from ". TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 ".$extraCondition;
	
	 if($agentName!=''){
  	$query .= " and custAgentID = '".$agentName."'";
  	$queryCnt .= " and custAgentID = '".$agentName."'";
  	
  	}		
   if($status!=''){
  	$query .= " and transStatus = '".$status."'";
  	$queryCnt .= " and transStatus = '".$status."'";
  	
  	}			
	if($collectionPoint!=''){
  	$query .= " and collectionPointID = '".$collectionPoint."'";
  	$queryCnt .= " and collectionPointID = '".$collectionPoint."'";
  	}			
	
	if ($fromDate != "" && $toDate!= "") {
	$query .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
	$queryCnt .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
}
  
	$query .= " ORDER BY $sortBy DESC ";
  $query .= " LIMIT $offset , $limit";
  $contentsTrans = selectMultiRecords($query);
  $allCount = countRecords($queryCnt);
  	$queryForExport=$query; 
   //$allCount = count($contentsTrans);
  
	
}
	$strQueryString	= "&Submit=".$Submit."&fDay=".$fDay."&fMonth=".$fMonth."&fYear=".$fYear."&tDay=".$tDay."&tMonth=".$tMonth."&tYear=".$tYear."&agentName=".$agentName."&collectionPoint=".$collectionPoint."&status=".$status."";
?>
<html>
<head>
	<title>Daily Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
<script language="javascript">
	
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}	
function export_report()
{
	var query_out=document.getElementById("query_out").value;
	var reportUrl="<?=$_SERVER['PHP_SELF']?>";
	
	window.location = "daily_report_export.php?query_out="+query_out+"&reportUrl="+reportUrl;
}
</script>    
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Daily Report</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="daily_report.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
       
		     <tr>
       <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
								echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					
           <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
					
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
									echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
          	SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
								echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         		SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
          	SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
        <br>
        <?  
		if($condition)
		{
		?>
				   <?=__("Agent")?> Name  
				  <select name="agentName" style="font-family:verdana; font-size: 11px;">
			  <option value="">- Select Agent-</option>
			  <!-- <option value="all">All</option> -->
				<?
//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
				$agentQuery = "select userID,username, agentCompany, name, agentContactPerson , 
							agentStatus from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' ";
				}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
		$agentQuery = "select userID,username, agentCompany, name, agentContactPerson , 
							agentStatus from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
		
		}
	//work end by Mudassar Ticket #11425	
				if($agentType == "Branch Manager")
				{
					$agentQuery .= " and parentID = '$parentID' ";
				}
				$agentQuery .=" order by agentCompany";
				
						$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
					}
				?>
				</select>      
			 <script language="JavaScript">
			 SelectOption(document.Search.agentName, "<?=$agentName?>");
				</script>
			      
		<? }?> <!--End Admin If -->
		
		<br>
		
		  
		   Collection Points <select name="collectionPoint"  style="font-family:verdana; font-size: 11px; width:200">
      <option value=""> -   Collection Points    - </option> 
      <?
      $cpQuery=selectMultiRecords("select DISTINCT cp_corresspondent_name,cp_branch_address,cp_city,cp_id,cp_ida_id from cm_collection_point where cp_corresspondent_name!= ''");
      
       for($j=0;$j< count($cpQuery); $j++){
	   	
	   ?>
	    <option value="<? echo $cpQuery[$j]['cp_id']?>"> <? echo $cpQuery[$j]['cp_branch_address']." [".$cpQuery[$j]['cp_city']."]"; ?> </option>	
	   <?	} ?>
	  </select>
	  <script language="JavaScript">
			SelectOption(document.Search.collectionPoint, "<?=$collectionPoint?>");
		</script>
	   <br>
	   Select Status 
	   	<select name="status" style="font-family:verdana; font-size: 11px;">
		  <option value="">- Select Status-</option>
			<option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
		  <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  </select>   
				<script language="JavaScript">
			SelectOption(document.Search.status, "<?=$status?>");
				</script>
      </tr>
       
     <tr> <td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	 <!-- </form> -->
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
     <!-- <form action="sender-trans-summary.php?action=<? echo $_GET["action"]?>" method="post" name="trans"> -->


          <?
			if (count($contentsTrans) > 0){
		?>
          <tr>
            <td  bgcolor="">
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
				<? if($msg!= '') { ?>
			<tr>
				  <td colspan="8" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></b></i><strong><? echo $msg; ?></strong></font></td>
			</tr>
			<? } ?>	
                <tr>
                  <td>
                    <?php   
					if (count($allCount) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                <!--  <td >
                     <input type="submit" name="Save" value="Save Report">
                                   	
                    </td> -->
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0".$strQueryString;?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv".$strQueryString;?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt".$strQueryString;?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$strQueryString;?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
				<td width="100"><span class="style1"><? echo $systemCode;?></span></td>
			  <td width="100"><span class="style1">Pounds</span></td>
			  <td width="100"><span class="style1">Currency Type</span></td>
			  <td width="100"><span class="style1">FX Rate</span></td>
			  <td width="100"><span class="style1">Local Currency</span></td>
			  <td width="100"><span class="style1">Currency Type</span></td>
			  <td width="100"><span class="style1">Transaction Status</span></td>
			  <td width="100"><span class="style1"><?=__("Sender")?> Name</span></td>
		    <td width="100"><span class="style1">Receiver Name</span></td>
			  <td width="100"><span class="style1">Father's Name</span></td>
			  <td width="100"><span class="style1">Branch Name</span></td>
			  <td width="100"><span class="style1">Place Of Transmission</span></td>
			  <td width="100"><span class="style1">Money Paid</span></td>
			</tr>
		    <? 
		     $totalTrans=count($contentsTrans);
		    
		    $totalTrans =0;
		    $totalLocal =0;
		    
		    for($i=0;$i < count($contentsTrans);$i++)			{
		    
		   
	  	?>
				
				<tr bgcolor="#FFFFFF">
					   
					   <td>
				      &nbsp; <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
				     </td> 
					 <td>
              &nbsp; <?  echo number_format($contentsTrans[$i]["transAmount"],2,'.',',');
                        $totalTrans = $totalTrans + $contentsTrans[$i]["transAmount"];
              
              ?>
           </td>
		   <td>
				      &nbsp; <?  echo $contentsTrans[$i]["currencyFrom"]; ?>
				    </td> 
           <td>
				      &nbsp; <?  echo $contentsTrans[$i]["exchangeRate"]; ?>
				    </td> 
					  <td>
				      &nbsp; <?  echo number_format($contentsTrans[$i]["localAmount"],2,'.',','); 
				           $totalLocal = $totalLocal + $contentsTrans[$i]["localAmount"];
				       ?>
				    </td> 
					<td>
				      &nbsp; <?  echo $contentsTrans[$i]["currencyTo"]; ?>
				    </td> 
					
					  <td>
				      &nbsp; <?  echo $contentsTrans[$i]["transStatus"]; ?>
				    </td> 
					  <td>
           &nbsp; <? 	if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsTrans[$i]["customerID"]."'"); echo $CollCustomer["firstName"];
           						}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
           						{
           							$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'"); echo $CollCustomer["FirstName"];
           						}
           				?>
          </td>
          <td>
                 &nbsp; <? if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName,middleName,lastName, Phone from beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); echo $collben["firstName"]." ".$collben["middleName"];
           				}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName,middleName,lastName, Phone from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); echo $collben["firstName"]." ".$collben["middleName"];
           				}
           		 ?>
           </td> 
           
            <td>
				      &nbsp; <?  echo $collben["lastName"]; ?>
				    </td> 
				   <? 
				   
				 /*  if($contentsTrans[$i]["transType"] == "Bank Transfer"){
 					
 						$bankDetails = selectFrom("select bankID,transID,bankName,branchAddress from ".TBL_BANK_DETAILS." where transID = '".$contentsTrans[$i]["transID"]."'");
 						$branchName = $bankDetails["bankName"];
 					}else{
 						$branchName = '';
 						}*/
 					?>
 					<td>
           &nbsp; <?  echo $contentsTrans[$i]["branchName"]; ?>
          </td> 
 					<?
          if($contentsTrans[$i]["transType"] == "Pick up"){
          	
          	$pickup = selectFrom("select cp_id,cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_city from ".TBL_COLLECTION." where cp_id = '".$contentsTrans[$i]["collectionPointID"]."'");
          	$pickupLocation = $pickup["cp_city"]." ".$pickup["cp_branch_address"] ;
 					}else{
 						$pickupLocation = '';
 						}
 					
          ?>
           <td>
           &nbsp; <?  echo $pickupLocation; ?>
          </td> 
         <td>
				      &nbsp; <?  echo $contentsTrans[$i]["moneyPaid"]; ?>
				 </td> 
				</tr>
			
				<?
			}
			?>
				<tr bgcolor="#CCCCCC">
					 <td>Total</td>
					 <td><? echo number_format($totalTrans,2,'.',',');?></td>
					  <td>&nbsp;</td> 
					 <td><? echo number_format($totalLocal,2,'.',',');?></td> 
					
					 <td>&nbsp;</td>
					 <td>&nbsp;</td> 
					 <td>&nbsp;</td>
					 <td>&nbsp;</td> 
					 <td>&nbsp;</td>
					 <td>&nbsp;</td>  
					 <td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>   
				</tr>
			<?	
			} // greater than zero
			
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="" colspan="2"> 
            	<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                    <td><?php  
					if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b> of
                    <?=$allCount; ?>
                    <?php } ;?></td>
                    <?php if ($prv >= 0) { ?>
                    <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$strQueryString;?>"><font color="#005b90">First</font></a></td>
                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv".$strQueryString;?>"><font color="#005b90">Previous</font></a></td>
                    <?php } ?>
                    <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt".$strQueryString;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$strQueryString;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                    <?php } ?>
                  </tr>
                <tr>
                    <td bgcolor="#999999" colspan="5"><center>
                        <input type="button" name="exportBankGe" id="exportBankGe" value="Export" onClick="export_report();">
                        <input type="hidden" value="<?=base64_encode($queryForExport)?>" name="query_out" id="query_out">
                      </center></td>
                  </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
