<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
//include_once ("secureAllPages.php");
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$modifyby = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$currencyies = selectMultiRecords("SELECT DISTINCT(currencyFrom) as currencyName FROM ".TBL_TRANSACTIONS);

$strLabel = 'Introducer Commission Report';

$linkedAgent = array();
if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && ($agentType != 'admin' && $agentType != 'Branch Manager' /*&& $agentType != 'Admin Manager'*/))
{
	
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$parentID."'");
	
	$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
	//$query .= " and username in '".$linkedAgent."'";
}
//work by Mudassar Ticket #11425
if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){	
		$queryCust = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' ";
		}
else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
		$queryCust = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
		}
if($agentType == "Branch Manager"){
	$queryCust .= " and parentID = '$parentID'";						
}
$queryCust .=" order by agentCompany";
$agents = selectMultiRecords($queryCust);
					
?>
<html>
<head>
<title><?=$strLabel?></title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>

<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var subgridimgpath = 'javascript/jqGrid/themes/green/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	var comSubGrid = 'comSubGrid';
	jQuery(document).ready(function(){
		$("#loading").ajaxStart(function(){
		   $("#confirmFlag").val("");
		 });

		var lastSel;
		var maxRows = 20;
		jQuery("#comMainList").jqGrid({
			url:"commission_report_action.php?getGrid=intComReport&nd="+new Date().getTime()+"&q=1",
			datatype: "json",
			height: 300,
			width: 900,
			colNames:['Customer Name','Currency Transacted', 'Total Transactions', 'Total Transaction Amount','Commission','Commission Due','Amount Paid'],
			colModel:[
				{name:'custFirstName',index:'custFirstName', width:100},
				{name:'currencyFrom',index:'currencyFrom', width:70, align:'center'},
				{name:'cntTrans',index:'cntTrans', width:70, align:'center'},
				{name:'totTransAm',index:'totTransAm', width:100, align:"right"},
				{name:'transComm',index:'transComm', width:60, align:"right", sortable:false},
				{name:'transCommDue',index:'transCommDue', width:60,align:"right", sortable:false},
				{name:'amountDue',index:'amountDue', width:150, sortable:false}
			],
			imgpath:gridimgpath,
			rowNum:maxRows,
			rowList: [20,50,100],
			pager: jQuery('#pagernav'),
			sortname: 'custFirstName',
			viewrecords: true,
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			multiselect: false,
			forceFit: true,
			shrinkToFit: true,
			subGrid: true,
			caption: "<?=$strLabel?>",
			
			subGridRowExpanded:function(subgrid_id,row_id){
		//	alert(subgrid_id);
		//	alert(row_id);
				// we pass two parameters
				// subgrid_id is a id of the div tag created whitin a table data
				// the id of this elemenet is a combination of the "sg_" + id of the row
				// the row_id is the id of the row
				// If we wan to pass additinal parameters to the url we can use
				// a method getRowData(row_id) - which returns associative array in type name-value
				// here we can easy construct the flowing
				$("#comMainGridID").val(row_id);
				var subgrid_table_id;
				var pager_id;
				subgrid_table_id =subgrid_id+"_t"; //comSubGrid;
				//alert(subgrid_table_id);
				pager_id = "p_"+subgrid_table_id;
				$("#"+subgrid_id).html("<table id="+subgrid_table_id+" class='scroll'></table><div id="+pager_id+" class='scroll'></div>");
				var from = jQuery("#from").val();
				var to = jQuery("#to").val();
				var custAgentID = '';
				if(jQuery("#custAgentID").val() != undefined)
				var	custAgentID = jQuery("#custAgentID").val();
				var currencyTrans = jQuery("#currencyTrans").val();
				var comMainGridID = jQuery("#comMainGridID").val();
				//alert(comMainGridID);
				comSubGrid=subgrid_table_id;
				var extraParamsSubGrid = "&from="+from+"&to="+to+"&custAgentID="+custAgentID+"&currencyTrans="+currencyTrans+"&Submit=SearchTrans";
				
				jQuery("#"+subgrid_table_id).jqGrid({
				
				
					url:"commission_report_action.php?getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+row_id+extraParamsSubGrid,
					datatype: "json",
					height: 100,
					width: 700,
					colNames:['Client Name','Currency','Transaction Number','Date', 'Transaction Amount', 'Receiving Amount', 'Commission','Commission Due','Amount Paid'],
					colModel:[
						{name:'custFirstNameT',index:'custFirstNameT', width:80},
						{name:'currencyFromT',index:'currencyFromT', width:30, align:'center'},
						{name:'TransNumberT',index:'TransNumberT', width:30, align:"right"},
						{name:'DestAmT',index:'DestAmT', width:40, align:'center'},
						{name:'cntTransT',index:'cntTransT', width:40, align:'center'},
						{name:'totTransAmT',index:'totTransAmT', width:60, align:"right"},
						{name:'transCommT',index:'transCommT', width:30, align:"right", sortable:false},
						{name:'transCommDueT',index:'transCommDueT', width:50,align:"right", sortable:false},
						{name:'amountDueT',index:'amountDueT', width:60, sortable:false}
					],
					imgpath:subgridimgpath, 
					rowNum:maxRows,
					rowList: [20,50,100],
					pager: pager_id,
					//sortname: 'num',
					multiselect: true
				});
				
				jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false})
				
				//alert('*');
			},
			subGridRowColapsed: function(subgrid_id, row_id) {
				// this function is called before removing the data
				var subgrid_table_id;
				subgrid_table_id = comSubGrid;//subgrid_id+"_t";
				jQuery("#"+subgrid_table_id).remove();
				$("#comMainGridID").val('');
			}
		});
		jQuery("#comMainList").jqGrid('navGrid','#pagernav',{add:false,edit:false,del:false});
		jQuery('a').cluetip({splitTitle: '|'});

	
		jQuery("#btnAction").click( function(){
			//var confirmFlag = $("#confirmFlag").val();
			//if(confirmFlag == ""){
				var confirmMsg = confirm('Are you sure to Proceed?');
				$("#confirmFlag").val("Y");
				if($(this).val()=="Paid" && confirmMsg==true){
					var allTransIDs = jQuery("#"+comSubGrid).getGridParam('selarrrow');
					//alert('transIDs='+allTransIDs);
					if(allTransIDs!=''){
						//var allTransIDs = $("#allTransIDsV").val();
						var allTransIdsArr = allTransIDs;
						//$("#allTransIDsV").val(allTransIdsArr);
						$("#payListPaidData").load("commission_report_action.php", { 'transIDs[]': allTransIdsArr,'btnAction': 'Paid'},
						function(){
							//alert($("#payListPaid").val());
						});
					}
					/*else{
						alert("Please Select Row(s)");
						$("#allTransIDsV").val('');
						return false;
					}*/
					//gridReload('comSubGrid');
					gridReload('comMainList');
				}
			//}
		});
	});

	function gridReload(grid)
	{
		var theUrl = "commission_report_action.php";
		
		if(grid=='comMainList' || grid=='comSubGrid'){
			var extraParam;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var custAgentID = '';
			if(jQuery("#custAgentID").val() != undefined)
			var	custAgentID = jQuery("#custAgentID").val();
			var currencyTrans = jQuery("#currencyTrans").val();
			var comMainGridID = jQuery("#comMainGridID").val();

			if(grid=='comMainList'){
				extraParam = "?from="+from+"&to="+to+"&custAgentID="+custAgentID+"&currencyTrans="+currencyTrans+"&Submit=SearchTrans&getGrid=intComReport";
			}
			else{
				extraParam = "?from="+from+"&to="+to+"&custAgentID="+custAgentID+"&currencyTrans="+currencyTrans+"&Submit=SearchTrans&getGrid=intComReportSubGrid&nd="+new Date().getTime()+"&q=2&rid="+comMainGridID;
			}
			jQuery("#"+grid).setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
<div id="loading"></div>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF"><?=$strLabel?></font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
    
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="fromDate" type="text" id="from" value="<?=$from?>" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" value="<?=$to?>" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->	
</td>						
              </tr>
	  <tr>
         <td nowrap align="center">
	 <?php 
	 if(($agentType != 'SUPA' && $agentType != 'SUBA')){ ?>
			<select name="custAgentID" id="custAgentID">
         	<option value="">- Select Introducer -</option>
		 <?php
			for ($i=0; $i < count($agents); $i++){
				$flag = False;
				if(in_array($agents[$i]["username"], $linkedAgent) && CONFIG_ADMIN_ASSOCIATE_AGENT == '1')		
				{
					$flag = True;	
				}
				if((CONFIG_ADMIN_ASSOCIATE_AGENT != '1') || (strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false)){
					
					$flag = True;
					}

				if($flag)
				{		
					?>
					<option value="<?=$agents[$i]["userID"]; ?>"><?  echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
					<?
				}
		
			}
			
		?>
	  </select>
		  &nbsp;&nbsp;&nbsp;
	 <?php } ?>
		  Select Currency
          <select name="currencyTrans" id="currencyTrans">
           <option value="">Select Currency</option>
           <? if(count($currencyies)>0){?>
           <? for($cp=0;$cp<count($currencyies);$cp++){?>
           <option value="<?=$currencyies[$cp]["currencyName"]?>">
            <?=$currencyies[$cp]["currencyName"]?>
           </option>
           <? }?>
           <? }?>
          </select>		  
          &nbsp;&nbsp;&nbsp;
		<input type="button" name="Submit" value=" Search " onClick="gridReload('comMainList')">
		<input type="reset" name="reset" value="Reset">		 </td>
    </table>  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="comMainList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListPaidData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>	</td>
    </tr>
	<?php if($agentType != 'SUPA' && $agentType != 'SUBA'){?>
	  <tr bgcolor="#FFFFFF">
	   <td align="center">
		<input name="btnAction" id="btnAction" type="button"  value="Paid">
		<input type='hidden' name='comMainGridID' id='comMainGridID' value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="confirmFlag" id="confirmFlag" value="">
		</td>
	  </tr>
	<?php }?>
</table>
	</td>
  </tr>
</table>
</form>
</body>
</html>