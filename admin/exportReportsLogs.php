<?php
	/** Export Reports Viewed History (Reports Logs)
	 *  @pakage Report
	 *  @subpaakge Report View History
	 *  @author Kalim ul Haq
	 *  copyrigth(c) HBS Technology (Pvt.) Ltd.
	 */
	session_start(); 
	include("../include/config.php");
	include("security.php");
	ini_set("memory_limit","1024M");
	$date_time	= date('d-m-Y  h:i:s');
	$systemCode = SYSTEM_CODE;
	$company 	= COMPANY_NAME;
	$systemPre 	= SYSTEM_PRE; 
	$manualCode = MANUAL_CODE;
	$agentType 	= getAgentType();
	$parentID 	= $_SESSION["loggedUserData"]["userID"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	if(!empty($_REQUEST["pageUrl"]))
		maintainReportLogs($_REQUEST["pageUrl"],'E');
	
	if($_REQUEST["Submit"] == "Export")
	{
		if(!empty($_REQUEST["fromDate"]))
			$fromDate	= $_REQUEST["fromDate"];
		else
			$fromDate	= $currDate;
			
		if(!empty($_REQUEST["toDate"]))
			$toDate	= $_REQUEST["toDate"];
	
		// Query to fatch data from DB
		$strQuery	= "SELECT
							lh.login_time,
							lh.login_name,
							lh.access_ip,
							lh.login_type,
							rh.id,
							rh.report_title,
							rh.count,
							rh.actionCount
						FROM
							viewReportHistory as rh
							LEFT JOIN " .TBL_LOGIN_HISTORY . " as lh
							ON rh.login_history_id = lh.history_id
						WHERE 1" ;
						
		if(!empty($_REQUEST["userID"]) && $_REQUEST["userID"] != "all")
			$strQueryWhere .= " AND lh.user_id='".$_REQUEST["userID"]."' ";
			
		$strQueryWhere .= " AND (login_time >= '$fromDate 00:00:00' and login_time <= '$toDate 23:59:59') ";				
		
		$strQuery	.= $strQueryWhere;
		$strQuery	.= " ORDER BY lh.login_time DESC";
		$contentsTrans = selectMultiRecords($strQuery);
	}
	
	//export format
	header("Content-type: application/x-msexcel"); 
	header("Content-Disposition: attachment; filename=Reports_Viewed_History.xls" ); 
	header("Content-Description: PHP/INTERBASE Generated Data" );
	 
	$strMainStart 	= '<table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#000000">';
	$strMainEnd 	= '</table>';
	$strStrongStart	= '<strong>';
	$strStrongEnd	= '</strong>';
	$strRowStart	= '<tr>';
	$strRowEnd		= '</tr>';
	$strHeadingStart= '<th>';
	$strHeadingEnd	= '</th>';
	$strCellStart	= '<td>';
	$strCellEnd		= '</td>';
	$strCellColSpan	= '<td colspan="6">';
	$strTextCell	= '<td align="left" style=\'mso-number-format:"\@"\'>';
	$strAmountCell	= '<td align="right" style=\'mso-number-format:"\#\,\#\#0\.00"\'>';
	$strRateCell	= '<td align="right" style=\'mso-number-format:"\#\,\#\#0\.0000"\'>';
	$strDateTimeCell= '<td align="left" style=\'mso-number-format:"dd\-mm\-yyyy\ hh\:mm\:ss"\'>';
	$strCountCell	= '<td align="center" style=\'mso-number-format:"0"\'>';
	
	$strExportData	= '';
	
	$strExportData	.= $strMainStart;
	
	$strExportData	.= $strRowStart;
	$strExportData	.= $strCellColSpan.$strStrongStart.'Reports Log'.$strStrongEnd.$strCellEnd;
	$strExportData	.= $strRowEnd;
	
	if(count($contentsTrans) > 0)
	{
		$strExportData	.= $strRowStart;
		$strExportData	.= $strHeadingStart.'Login Time'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'User Name'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'User Type'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'IP Address'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Link Name'.$strHeadingEnd;
		$strExportData	.= $strHeadingStart.'Count'.$strHeadingEnd;
		$strExportData	.= $strRowEnd;
		
		foreach($contentsTrans as $key=>$val)
		{
			$strExportData	.= $strRowStart;
			$strExportData	.= $strDateTimeCell.$val["login_time"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["login_name"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["login_type"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["access_ip"].$strCellEnd;
			$strExportData	.= $strTextCell.$val["report_title"].$strCellEnd;
			$strExportData	.= $strCountCell.$val["count"].$strCellEnd;
			$strExportData	.= $strRowEnd;
			
			$totalCount		+= $val["count"];
		}
		
		$strExportData	.= $strRowStart;
		$strExportData	.= $strCellColSpan.$strStrongStart.'Total:'.$strStrongEnd.$strCellEnd;
		$strExportData	.= $strRowEnd;
		
		$strExportData	.= $strRowStart;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strTextCell.' '.$strCellEnd;
		$strExportData	.= $strCountCell.$strStrongStart.$totalCount.$strStrongEnd.$strCellEnd;
		$strExportData	.= $strRowEnd;
	}
	else
	{
		$strExportData	.= $strRowStart;
		$strExportData	.= $strTextCell.'No History Found!'.$strCellEnd;
		$strExportData	.= $strRowEnd;
	}
	
	$strExportData	.= $strMainEnd;
	
	echo $strExportData;
