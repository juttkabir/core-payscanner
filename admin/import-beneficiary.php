<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
require_once './cm_xls/Excel/reader.php';

if($_POST["Submit"] != "")
{
	if ($_FILES["csvFile"]["size"] <= 0)
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	} else {
		$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));
		
		if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" )
		{
			if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
			{
				$Pict="$username-" . date("Y-m-d") . $ext;
	
				// Parsing file and inserting into DB
				$filename = $_FILES["csvFile"]["tmp_name"];
				$data = new Spreadsheet_Excel_Reader();

				// Set output Encoding.
				$data->setOutputEncoding('CP1251');
				$data->read($filename);
				error_reporting(E_ALL ^ E_NOTICE);
				$tran_date = date("Y-m-d");
				$counter = 0;
				$updated = 0;
				$unlinked = 0;
				$total = $data->sheets[0]['numRows'] - 1;
				$unlinkedCustomers = array();
				
				/**
				 * Storing the beneficiary bank details as well as its personal data
				 * @Ticket# 3321
				 */
				if(CONFIG_BEN_BANK_DETAILS == "1")
				{
					
					for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
					{
						
						$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //Sender Id
						$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //Beneficiary ID - Auto Generated number
						$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //Title*
						$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);  //firstName
						$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	// middleName*
						$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);	//lastName
						$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);  //Address
						$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	// Address1
						$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);	// Country*
						$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);  //City
						$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //Zip
						$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //State
						$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //Phone*
						$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //Mobile
						$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //email
						$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //IDType
						$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);  //IDnumber
						$data1[18]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][18]);  //IDExpiry
						$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //otherId
						$data1[20]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][20]);  // otherId_name
						$data1[21]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][21]);  // Bank ID
						$data1[22]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][22]);  // branch code/number => Sort code
						$data1[23]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][23]);  // Account number
						$data1[24]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][24]);  // Account Type
						$data1[25]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][25]);  // Bank City 
						$data1[26]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][26]);  // SWIFT Code
						$data1[27]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][27]);  // IBAN Code
						 
						if(!empty($data1[1]) && !empty($data1[2]))	// && !empty($data1[4]) && !empty($data1[8]) && !empty($data1[10]) && !empty($data1[13]) for the time being
						{
							// Check for Sender's existance - customerID
							$sql = "select customerID from customer where customerID=".$data1[1];
							$result = mysql_query($sql) or die(mysql_error());
							$numrows = mysql_num_rows($result);
							
							if($numrows > 0)
							{
								if($data1[16] == "CPF" || $data1[16] == "CPNJ")
								{
									if($data1[16] == "CPF")
									{
										//$userType = "Private User";
										$customValue1 = "Private User";
									} else {
										//$userType = "Business User";
										$customValue1 = "Business User";
									}
									
									$customField1 = "userType";
									$customField2 = "CPF";
									$customValue2 = $data1[17];
								} else {
									$customField1 = "IDType";
									$customField2 = "IDNumber";
									$customValue1 = $data1[16];
									$customValue2 = $data1[17];
								}
							
								// Check for Beneficiary existance for update or insert
								$sql2 = "select benID from beneficiary where benID=".$data1[2];
								$result2 = mysql_query($sql2) or die(mysql_error());
								$numrows2 = mysql_num_rows($result2);
								
								if($numrows2 > 0)	// update
								{
									$updated++;
									$Querry_Sqls = "UPDATE ".TBL_BENEFICIARY." 
													SET 
														Title='".$data1[3]."', firstName='".$data1[4]."', middleName='".$data1[5]."', lastName='".$data1[6]."', Address='".$data1[7]."',
														Address1='".$data1[8]."', Country='".$data1[9]."', City='".$data1[10]."', Zip='".$data1[11]."', State='".$data1[12]."', 
														Phone='".$data1[13]."', Mobile='".$data1[14]."', email='".$data1[15]."', IDexpirydate='" .$data1[18]. "', $customField1='".$customValue1."',
														$customField2='".$customValue2."', customerID='" . $data1[1] . "', otherId='".$data1[19]."', otherid_name='".$data1[20]."', 
														IBAN='".$data1[27]."' 
													WHERE
														benID=".$data1[2]."
													";
								} else {			// insert
									$counter++;
									$Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." 
													(
														benID, 
														Title, firstName, middleName, lastName, Address, Address1, Country, City, Zip, State, 
														Phone, Mobile, email, IDexpirydate, $customField1, $customField2, customerID, otherId, otherid_name, 
														created, IBAN
													) 
													VALUES 
													(
														".$data1[2].", 
														'".$data1[3]."', '".$data1[4]."', '".$data1[5]."', '".$data1[6]."', '".$data1[7]."',
														'".$data1[8]."', '".$data1[9]."', '".$data1[10]."', '".$data1[11]."', '".$data1[12]."', 
														'".$data1[13]."', '".$data1[14]."', '".$data1[15]."', '" .$data1[18]. "', '".$customValue1."',
														'".$customValue2."', '" . $data1[1] . "', '".$data1[19]."', '".$data1[20]."', NOW(), 
														'".$data1[27]."'
													)";
								}
								
								$result3 = mysql_query($Querry_Sqls) or die(mysql_error());
							} else {
								$unlinkedCustomers[] = $data1;
								$unlinked++;
							}
							
							/**
							 * Used to keep data relationship in banks table
							 */
							//$benID = @mysql_insert_id();
							$benID = $data1[2];
							
							/**
							 * simplfying Account type, using just first half of account type
							 */
							$accType = explode(" ",$data1[24]);
							$data1[24] = $accType[0];
							
							
							/**
							 * Getting the bank having the same name of bank
							 */
							$verifyExisting = "select id from ".TBL_BANKS." where name = '".$data1[21]."'";
							$exResultSet = selectFrom($verifyExisting);
							/**
							 * If existing banks has exactly 1 instance
							 * else we will insert the new bank
							 */
							//if(count($exResultSet) == 1)
							if(isExist($verifyExisting))
							{
								
								$beneficiaryBankSql = "insert into ".TBL_BEN_BANK_DETAILS." (bankId, benId, accountNo, accountType, branchCode, branchAddress, swiftCode)
																				values(".$exResultSet["id"].",$benID, '".$data1[23]."','".$data1[24]."','".$data1[22]."','".$data1[25]."','".$data1[26]."')";
								
								insertInto($beneficiaryBankSql);
							}
							else
							{
								/**
								 * Did not found any bank matched
								 *
								 * Ading new bank
								 */
								
								$newBankSql = "insert into ".TBL_BANKS." (bankId, name, country)
																values('', '".$data1[21]."', '')";
								insertInto($newBankSql);
								$lastBankId = @mysql_insert_id();
								
								$benBankQry = "Insert into ".TBL_BEN_BANK_DETAILS. " (benId, bankId, accountNo, accountType, branchCode, branchAddress, swiftCode)
												values ($benID, $lastBankId, '".$data1[23]."','".$data1[24]."','".$data1[22]."','".$data1[25]."','".$data1[26]."')";
								insertInto($benBankQry);
								
							}

							/**
							 * Keeping the record count
							 */
							//$counter ++;
						
						}
					
					}
					$msg = "$total Records Scanned.<br>$counter New Beneficiaries Imported Successfully.<br>$updated Found Duplicate & Updated.<br>$unlinked Found Unlinked & Ignored.";
					
					
					
				}
				else
				{
				
				
				for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
				{
					
					$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //Title
					$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //firstName*
					$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //middleName
					$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);	// lastName*
					$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	//Address
					
					$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);  //Address1
					$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);	// Country*
					$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	// City*
					$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);  //Zip
					$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);  //State
					$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //Phone*
					$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Mobile
					$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //email
					$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //DOB
					
					
					$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //IDType
					$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //IDnumber
					$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);  //IDExpiry
					$data1[18]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][18]);  //otherId
					
					$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //otherId_name
					
					
					//$password = createCode();
					
					if($data1[19] != "")
					{
						$customerNumber = selectFrom("select accountName, customerID from ".TBL_CUSTOMER." where accountName='".$data1[19]."'");
						$customerNumber_value = $customerNumber["customerID"];
						
						}
					
				 $data1[17].	$Querry_Sqls = "INSERT INTO ".TBL_BENEFICIARY." 
					(Title, firstName, middleName, lastName, Address, 
					Address1, Country, City, Zip, State, Phone, Mobile, email, IDexpirydate,IDType, 
					IDNumber, customerID, otherId, otherid_name, created,dob) VALUES 
					('".$data1[1]."','".$data1[2]."', '".$data1[3]."', '".$data1[4]."', '".$data1[5]."',
					 '".$data1[6]."', '".$data1[7]."', '".$data1[8]."', '".$data1[9]."', '".$data1[10]."', '".$data1[11]."', '".$data1[12]."', '".$data1[13]."', '" .$data1[17]. "', '".$data1[15]."',
					 '".$data1[16]."', '" . $customerNumber_value . "', '".$data1[18]."', '".$data1[19]."','".$tran_date."','".$data1[14]."')";
					 
					 insertInto($Querry_Sqls);
					$benID = @mysql_insert_id();
					
					$counter ++;
				
		
				
				}
				$msg = " $counter Beneficiaries are Imported Successfully ";
				
				//exit;
					// END
					/**
					 * End of the config condition
					 */
				}
			}
			else
			{
				$msg = CAUTION_MARK . " Your file is not uploaded due to some error.";
			}
		}
		else
		{
			$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
		}
	}
}


// $logoContent = selectFrom("select logo from " . TBL_ADMIN_USERS. " where username='$username'");

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript">
function checkForm(theForm) {
	if(theForm.transID.value == "" || IsAllSpaces(theForm.transID.value)){
    	alert("Please provide transaction ID.");
        theForm.transID.focus();
        return false;
    }
	if(theForm.subject.value == "" || IsAllSpaces(theForm.subject.value)){
    	alert("Please provide the subject for complaint.");
        theForm.subject.focus();
        return false;
    }
	if(theForm.details.value == "" || IsAllSpaces(theForm.details.value)){
    	alert("Please provide the details of complaint.");
        theForm.details.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Import Beneficiaries </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0">
  <tr>
    <td  valign="top"><fieldset>
    <legend class="style2">Import Beneficiaries </legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="addComplaint" action="import-beneficiary.php" method="post" enctype="multipart/form-data">
			 
			  <tr align="center" class="tab-r">
			    <td colspan="2">Please follow the instructions on the Readme.txt file.</td>
			    </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="#" onClick="javascript:window.open('import-readme.php?page=custben', 'ReadMe', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=250,width=650,top=200,left=170')" class="style2">
				Readme.txt
			  </a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				
				<?
					if ( defined("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE")
						 && CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE == 1
						 && defined("CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME") )
					{
						$benTemplateFileName = CONFIG_CUSTOM_IMPORT_BENEFICIARY_TEMPLATE_FILENAME;
					}
					else
					{
						$benTemplateFileName = "sampleTempBen.xls";
					}
				?>
				<a href="xls/<?=$benTemplateFileName;?>" class="style2">Sample Template</a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Excel File </td>
			    <td width="75%" bgcolor="#DFE6EA">			      <input name="csvFile" type="file" id="csvFile" size="15"></td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
		      <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			  	<? if( $msg!= " $counter Beneficiaries are Imported Successfully " ) { ?>
			    <td colspan="2"><font color="#CC0000"><strong><? echo $msg ?></strong></font><br></td>
			    <? } else { ?>
			      <td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg ?></font></b><br></td>
			      <? } ?>
			    </tr>
			  <tr>
			  <?
			  }
			  ?>
			</table>
		    <br>
		     
        </fieldset></td>
    <td width="431" valign="top">
	<? if ($str != "")
	{
	?>
	
	<fieldset>
    <legend class="style2">Import Beneficiaries Results </legend>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
      <form name="addComplaint" action="import-beneficiary.php" method="post" enctype="multipart/form-data">
        <tr>
          <td valign="top" bgcolor="#DFE6EA"><? echo $str?></td>
          </tr>
      </form>
    </table>
    <br>
    </fieldset>
	<?
	}
	?>
	</td>
  </tr>
  
</table>

	</td>
  </tr>
</table>
</body>
</html>