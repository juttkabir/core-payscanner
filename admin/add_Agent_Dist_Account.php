<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
///////////////////////History is maintained via method named 'activities'
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
/*if(CONFIG_DOUBLE_ENTRY == '1')
	include("double-entry-functions.php");*/
$backUrl = "agent_Distributor_Account.php";
$_SESSION["agentID2"];
if($_GET["agent_Account"]!="")
	$_SESSION["agentID2"] = $_GET["agent_Account"];
	

if($_GET["currencySending"]!="")
	$_SESSION["currencySending"]=$_GET["currencySending"];
if($_REQUEST["actAsString"]!="")
	$actAsString = $_REQUEST["actAsString"];
else
	$actAsString = "Agent";

if(CONFIG_DOUBLE_ENTRY == '1')
{
$backUrl = "agent_Distributor_Account_new.php";
	include("double-entry-functions.php");
if($_REQUEST["action"] == "getAccount")
	{
		$strOption = '';
		$condition = '';
		$mode = $_REQUEST["mode"];
		$userID = $_REQUEST["userID"];
		if(!empty($mode))
		{
			if($mode == "By Cash"){
			  $condition = '100';
			  }elseif($mode == "By Bank Transfer"){
			   $condition = '1001';
			  }
			 
			$strAccount = "SELECT 
							   id,
							   accounNumber,
							   accountName,
							   currency
						  FROM 
						  	 accounts 
						  WHERE 
						    depositFlag = 'Y' AND
							accounType = '".$condition."' AND
							currency = '".$_SESSION["currencySending"]."' AND
							status = 'AC'";
		//debug($strAccGetDataSql);					
		$accountRs = mysql_query($strAccount) or die('invalid query'.mysql_error());	
		while($rowAccount = mysql_fetch_array($accountRs,MYSQL_ASSOC))
					{
					$Selected = '';
					$id = $rowAccount["id"];
					$accounNumber = $rowAccount["accounNumber"];
					$accountName = $rowAccount["accountName"];
					$currency = $rowAccount["currency"];
					/* if($accountSell == $accounNumber)
						 	$drSelected = 'selected="selected"';*/
				        $strOption.= "<option value='$accounNumber' $Selected >".$accounNumber."-->".$accountName."-->".$currency."</option>";
					}	
		/* Any problem in the communication, so print E for error indication */
		//debug($strOption);
		
	}
	echo $strOption;//$response->encode($arrFullFeeData); 
	exit;
}	
}

?>
<html>
<head>
<title>Add into Account</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>

<script>
	$(document).ready(function() {

		$("#form1").validate({
			rules: {
				accountNo: "required",
				currency: "required"
			},
			messages: {
				accountNo: "Please enter account number",
				currency: "Please select the currency from dropdown"
				
			}
			
		});
		getAccountFn();
			
		$("#closeWindow").click(function(){
			close();
		});
		
		$("#moneyPaid").change(function(){
			var mode = $("#moneyPaid").val();
			if(mode != '')
				getAccountFn();
			else
				$("#accountNo").html("&nbsp;");
				
		});
		
	});
	
	function getAccountFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add_Agent_Dist_Account.php",
		   data: "action=getAccount&mode="+$("#moneyPaid").val()+"&userID="+$("#aid").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
			$("#accountNo").html("&nbsp;");
		   },
		   success: function(msg){
		   if(msg !='')
				$("#accountNo").html(msg);
			else
				$("#accountNo").html("<option>--select account--</option>");	
		   }
		}); 
	}	 
	
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
</script>

</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="add_Agent_Dist_Account.php">
  <input name="aid" type="hidden" id="aid" value="<?=$_SESSION["agentID2"];?>" />
    <tr> 
      <td width="39%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Add Amount &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="money"></td>
    </tr>
     <tr> 
      <td><div align="right"><font color="#005b90">Add Note &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="note"></td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Type &nbsp;&nbsp;</font></div></td>
      <td><select name="type">
      			<option value="Deposit">Deposit</option>
      			<option value="Withdraw">Withdraw</option>
      		</select>
      	</td>
    </tr>
	
	<? if(CONFIG_DOUBLE_ENTRY == '1'){?>
	<tr>
		<td><div align="right"><font color="#005b90">Money Paid &nbsp;&nbsp;</font></div></td>
		<td>
			<select name="moneyPaid" id="moneyPaid">
			<option value="">--select one--</option>
				<option value="By Cash">By Cash</option>
				<option value="By Bank Transfer">By Bank Transfer</option> 
			</select>
		</td>
	</tr>
	<tr id="accountRow">
		<td><div align="right"><font color="#005b90">Account Number &nbsp;&nbsp;</font></div></td>
		<td>
			<select name="accountNo" id="accountNo">
				<option value="">--select Account--</option> 
			</select>
		</td>
	</tr>
	<? } ?>
      <? if(CONFIG_ADD_CURRENCY_DROPDOWN == "1"){?> 
      <tr> 
      <td><div align="right"><font color="#005b90">Currency &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="currencySending" value="<?=$_SESSION["currencySending"]?>" readonly>
	  <input type="hidden" name="actAsString" value="<?=$actAsString?>" readonly></td>
    </tr>
     <? } ?>        
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Save"></td>
    </tr>
	 <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if($_POST["Submit"] =="Save")
{
	
if ($_POST["type"] == "Deposit") {
			$type = "DEPOSIT";
			$desc = "Manually Deposited";
		} else if ($_POST["type"] == "Withdraw") {
			$type = "WITHDRAW";
			$desc = "Manually Withdrawn";
		}
	
$today = date("Y-m-d"); 
$newField = '';
$newValue = '';
if(CONFIG_DOUBLE_ENTRY == '1') {
$newField = ",modeOfPayment";
$moneyPaid = $_POST["moneyPaid"];
$newValue = ",'$moneyPaid'";
}
$insertQuery="insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description, status, note,currency,actAs ".$newField.") values('".$_SESSION["agentID2"]."', '$today', '$type', '".$_POST["money"]."', '$changedBy', '', '$desc','Verified','".$_POST["note"]."','".$_SESSION["currencySending"]."','".$actAsString."' ".$newValue.")";
//debug($insertQuery);
$q=mysql_query($insertQuery);
if(CONFIG_DOUBLE_ENTRY == '1'){
$aaID = @mysql_insert_id();
$created = date('Y-m-d  h:i:s');
$param = array(
	 "userID"=>$_SESSION["agentID2"],
	 "amount"=> $_POST["money"],
	  "accountNumber"=> $_POST["accountNo"],
	 "currency"=> $_SESSION["currencySending"],
	 "description"=>$desc,
	 "moneyPaid" => $_POST["moneyPaid"],
	 "type" => $type,
	 "created"=>$created,
	 "createdBy"=>$changedBy,
	 "id" => $aaID,
	 "tableName"=>TBL_AnD_ACCOUNT,
	 "action"=>'MANUAL_DEPOSIT'
	 );
$response = updateLedger($param);
//debug($response,true);
}

$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
$agentBalance	= $contantagentID["balance"];
$agentBalance += $_POST["money"];
update("update " . TBL_ADMIN_USERS . " set balance  = $agentBalance where userID = '".$_SESSION["agentID2"]."'");


$insertedID = @mysql_insert_id();
$descript = "Amount ".$_POST["money"]." is ".$type." into Agents account";
agentSummaryAccount($_SESSION["agentID2"], $type, $_POST["money"],$_SESSION["currencySending"]);
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_AnD_ACCOUNT,$descript);
	
	////////////////////Auto Authorization//////////
if(CONFIG_AUTO_AHTHORIZE == "1")
{
	$contantagentID = selectFrom("select * from " . TBL_ADMIN_USERS . " where userID ='".$_SESSION["agentID2"]."'");
	$agentID = $contantagentID["userID"];
	$agentBalance	= $contantagentID["balance"];
	$agentLimit	= $contantagentID["agentAccountLimit"];
	
	
	$amountInHand = $agentBalance;
	$contantTrans = selectMultiRecords("select * from ".TBL_TRANSACTIONS." where custAgentID = '".$_SESSION["agentID2"]."' and transStatus = 'Pending' order by transDate ");
	//echo("select * from ".TBL_TRANSACTIONS." where custAgentID = '".$_SESSION["agentID2"]."' and transStatus = 'Pending' order by transDate ");
	for($i = 0; $i < count($contantTrans); $i++)
	{
	//	echo("Amount in Hand $amountInHand    Transaction Amount ".$contantTrans[$i]["totalAmount"]);
	if(CONFIG_AGENT_LIMIT == "1")
	{
		$agentValidBalance = $amountInHand + $agentLimit;///Agent Limit is also used
	}else{
		$agentValidBalance = $amountInHand;///Agent Limit is not used
		}
	//echo("Valid Amount   $agentValidBalance    Transaction Amount ".$contantTrans[$i]["totalAmount"]);
		if($agentValidBalance >= $contantTrans[$i]["totalAmount"])
		{	//////////////////If Balance is enough
			
			$queryCust = "select payinBook  from ".TBL_CUSTOMER." where customerID ='" . $contantTrans[$i]["customerID"] . "'";
			$custContents = selectFrom($queryCust);
				
			if($custContents["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")///////If Not a payin book customer
			{	
				
				$Status	= "Authorize";
				$tran_date = date("Y-m-d");
				
				if(CONFIG_LEDGER_AT_CREATION != "1")
				{
					$insertQuery = "insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, TransID, description,currency) values('$agentID', '$tran_date', 'WITHDRAW', '".$contantTrans[$i]["totalAmount"]."', '$changedBy', '". $contantTrans[$i]["transID"]."', 'Transaction Authorized','".$_SESSION["currencySending"]."')";
					$q=mysql_query($insertQuery);//////Insert into Agent Account
					 update("update " . TBL_ADMIN_USERS . " set balance  = $amountInHand where userID = '$agentID'");
					 
					agentSummaryAccount($agentID, "WITHDRAW", $contantTrans[$i]["totalAmount"]); 
				}
				$amountInHand = $amountInHand - $contantTrans[$i]["totalAmount"];			
				
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy ='$username', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$contantTrans[$i]["transID"]."'");				
				//echo("Trans ID ---".$contantTrans[$i]["transID"]);
				

							/////////////////////Added to audit trial/////////
									$descript = "Transaction is Authorized";
									activities($_SESSION["loginHistoryID"],"UPDATION",$contantTrans[$i]["transID"],TBL_TRANSACTIONS,$descript);	
							//////////////////////////////////				 
				 
				$contentBenID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$contantTrans[$i]["benAgentID"]."'");
				$benBalance	= $contentBenID["balance"];
				$benBalance -= $contantTrans[$i]["transAmount"];
				update("update " . TBL_ADMIN_USERS . " set balance  = $benBalance where userID = '".$contantTrans[$i]["benAgentID"]."'");

				insertInto("insert into bank_account (bankID, dated, type, amount, currency, modified_by, TransID, description) values('".$contantTrans[$i]["benAgentID"]."', '$tran_date', 'WITHDRAW', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contantTrans[$i]["localAmount"] : $contantTrans[$i]["transAmount"])."', '".(DEST_CURR_IN_ACC_STMNTS == "1" ? $contantTrans[$i]["currencyTo"] : $contantTrans[$i]["currencyFrom"])."', '$changedBy', '".$contantTrans[$i]["transID"]."', 'Transaction Authorized')");		
									agentSummaryAccount($contantTrans[$i]["benAgentID"], "WITHDRAW", $contantTrans[$i]["transAmount"]); 

			}
		}
	}

	
	
}

////////////////////////
	
	
	
	
if($q)
	{?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>">You 
        have successfully added 
        <? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
        </font></div></td>
  	  <td bgcolor="#CCCCCC" align="left">&nbsp;<!--font color="#990000"> &nbsp; Now Balance 
	  is <? $balanceCal=$amountInHand;//agentBalance($_SESSION["agentID2"]); 
	  echo number_format($balanceCal,2,'.',',');?></font--> </td>
  </tr>
  <? }
}	 
?>
	<tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>
	<tr> 
    <td height="19">&nbsp;&nbsp;&nbsp;
    		<a href="<?=$backUrl?>?agentID=<?=$_SESSION["agentID2"]?>&search=search&currency=<?=$_SESSION["currencySending"]?>" class="style2" >
    			<font color="#000000">Back to A &amp; D Account Statement</font>
    		</a>
    </td>
    <td>&nbsp;</td>
	</tr>
</table>
</body>
</html>