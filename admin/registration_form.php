<?

if(isset($_GET['type'])){
$register_type =base64_decode( urldecode( $_GET['type'] ) );
}else{
$register_type="";
}


if(isset($_GET['cur_type'])){
$cust_type = $_GET['cur_type'];

}else{
$cust_type = "USD";

}

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<? include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<html>

	<head>

		<title>Private Client Registration Form - Payscanner Online Currency Transfers</title>

		<meta name="description" content="Our services are tailored to help expatriates living abroad, businesses or their clients, and those who own or plan to buy overseas assets such as property." /><meta name="keywords" content="currency exchange specialists, FX, foreign exchange, forex, money transfers, sterling, euro, eurozone, exchange rates, currency planning, currency rates, currency trading, forward trading" />

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<meta http-equiv="imagetoolbar" content="no">




<link href="css/style_new.css" rel="stylesheet" type="text/css">
<link href="css/style_responsive.css" rel="stylesheet" type="text/css">
<link href="css/stylenew.css" rel="stylesheet" type="text/css">

		<link href="css/datePicker.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="jquery.js"></script>

		<script type="text/javascript" src="./javascript/jquery.validate.js"></script>

		<script type="text/javascript" src="./javascript/date.js"></script>

		<script type="text/javascript" src="./javascript/jquery.datePicker.js"></script>

		<script type="text/javascript" src="./javascript/jquery.maskedinput.min.js"></script>

		<script type="text/javascript" src="./javascript/ajaxupload.js"></script>

		<!-- Google Analytics -->
		<?php include '_ga.inc.php';?>

	</head>

	<body>

<div id="wait" style="position:fixed;height:100%;width:100%;background:#222;opacity:0.7;display:none;z-index:999">

					<div style="margin: 0 auto;position: relative;top: 50%;font-size:24px;color:#fff;font-family:arial;text-align:center">
						<img src="images/premier/formImages/loading.gif" alt=""/>
						<span id="loadingMessage" style="white-space:nowrap;"> Loading...</span>
					</div>

				</div>


<!-- main container-->
<div class="container">
<div class="upper_container">
<div class="header">
<div class="logo">
<a href="http://payscanner.com"><img id="logo" src="images/spi_payscanner_com/logo.png"></a>
</div>
</div>
</div>
<!-- lower container -->
<div class="lower_container">
<div class="body_area">
<div class="logout_area">
<h2 class="heading">Open an account</h2>
<div class="logout">

</div>
</div>
<form name="senderRegistrationForm" id="senderRegistrationForm" action="" method="post" style="overflow:hidden;">

<input type="hidden" value="<?=$register_type?>" id="register_type" name="register_type" />
<div class="content_area">

<div class="content_area_center">
<p class="content_heading">Open an account: private client</p>
<p class="content_subheading">Please complete the form below and upload one form of identification to comply with UK money laundering regulations, we require </p>
<ul class="faqs_list">
<li><a href="#" style="text-decoration:none;cursor:default">a valid Passport</a></li>
<!--<li><a href="#" style="text-decoration:none;cursor:default">or a valid National Identity Document</a></li>-->
</ul>





<!-- table area for input fields ends  -->
</div>
<!-- content area left-->
<div id="emailDiv" class="content_area_left">


<div class="field_wrapper reg_title">
<label class="form_label ">Title :</label><br>
<select name="title" class="select_mini " >
<option value="Mr">Mr.</option>

<option value="Mrs">Mrs</option>

<option value="Miss">Miss.</option>

<option value="Dr">Dr.</option>

<option value="Prof">Prof.</option>

<option value="Eng">Eng.</option>

<option value="Herr">Herr</option>

<option value="Frau">Frau</option>

<option value="Ing">Ing.</option>

<option value="Mag">Mag.</option>

<option value="Sr">Sr.</option>

<option value="Sra">Sra.</option>

<option value="Srta">Srta.</option>
</select>


</div>


<div class="field_wrapper reg_forname">
<label class="form_label ">Forename:<span style="color:red">*</span></label><br>
<input name="forename" class="form_fields " type="text" id="forename" />
</div>



<div class="field_wrapper">
<label class="form_label">Email:<span style="color:red">*</span></label><br>
<input name="email" type="text" id="email" class="required email error form_fields"/>
<span id="emailValidator" class="please_do"></span>
<input type="hidden" name="ipAddress" value="95.210.228.178"/>

</div>

<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label">Country of Birth :<span style="color:red">*</span></label><br>

<select name="birthCountry" id="birthCountry" class="select_field hideOnDuplicateEmail">
<? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
$custCountries = array_filter($custCountries);
for ($k = 0; $k < count($custCountries); $k++) {
$selected="";
if (trim($custCountries[$k]) == "United Kingdom"){
$selected = "selected";    }  ?>
<option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
<?
echo trim($custCountries[$k]) ?>
</option>
<?
} ?>
</select>


</div>

<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label  hideOnDuplicateEmail">Telephone - Landline:<span style="color:red">*</span></label><br>
<input name="landline" type="text" class="form_fields  hideOnDuplicateEmail" id="landline"  onkeypress="return isNumber(event)" />
</div>

</div>

<!-- content area right-->
<div class="content_area_right">

<div class="field_wrapper" >
<label class="form_label">Surname:<span style="color:red">*</span></label><br>
<input name="surname" type="text" id="surname" class="form_fields">
</div>


<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label">Date of Birth:<span style="color:red">*</span></label><br>

<select name="dobDay" class="select_mini hideOnDuplicateEmail" id="dobDay">

<option value="01">01</option>

<option value="02">02</option>

<option value="03">03</option>

<option value="04">04</option>

<option value="05">05</option>

<option value="06">06</option>

<option value="07">07</option>

<option value="08">08</option>

<option value="09">09</option>

<option value="10">10</option>

<option value="11">11</option>

<option value="12">12</option>

<option value="13">13</option>

<option value="14">14</option>

<option value="15">15</option>

<option value="16">16</option>

<option value="17">17</option>

<option value="18">18</option>

<option value="19">19</option>

<option value="20">20</option>

<option value="21">21</option>

<option value="22">22</option>

<option value="23">23</option>

<option value="24">24</option>

<option value="25">25</option>

<option value="26">26</option>

<option value="27">27</option>

<option value="28">28</option>

<option value="29">29</option>

<option value="30">30</option>

<option value="31">31</option>

</select>

<select class="select_mini hideOnDuplicateEmail" name="dobMonth" id="dobMonth">

<option value="01">Jan</option>

<option value="02">Feb</option>

<option value="03">Mar</option>

<option value="04">Apr</option>

<option value="05">May</option>

<option value="06">Jun</option>

<option value="07">Jul</option>

<option value="08">Aug</option>

<option value="09">Sep</option>

<option value="10">Oct</option>

<option value="11">Nov</option>

<option value="12">Dec</option>


</select>


<select name="dobYear" class="select_mini" id="dobYear">

<option value="1929">1929</option>

<option value="1930">1930</option>

<option value="1931">1931</option>

<option value="1932">1932</option>

<option value="1933">1933</option>

<option value="1934">1934</option>

<option value="1935">1935</option>

<option value="1936">1936</option>

<option value="1937">1937</option>

<option value="1938">1938</option>

<option value="1939">1939</option>

<option value="1940">1940</option>

<option value="1941">1941</option>

<option value="1942">1942</option>

<option value="1943">1943</option>

<option value="1944">1944</option>

<option value="1945">1945</option>

<option value="1946">1946</option>

<option value="1947">1947</option>

<option value="1948">1948</option>

<option value="1949">1949</option>

<option value="1950">1950</option>

<option value="1951">1951</option>

<option value="1952">1952</option>

<option value="1953">1953</option>

<option value="1954">1954</option>

<option value="1955">1955</option>

<option value="1956">1956</option>

<option value="1957">1957</option>

<option value="1958">1958</option>

<option value="1959">1959</option>

<option value="1960">1960</option>

<option value="1961">1961</option>

<option value="1962">1962</option>

<option value="1963">1963</option>

<option value="1964">1964</option>

<option value="1965">1965</option>

<option value="1966">1966</option>

<option value="1967">1967</option>

<option value="1968">1968</option>

<option value="1969">1969</option>

<option value="1970" selected="">1970</option>

<option value="1971">1971</option>

<option value="1972">1972</option>

<option value="1973">1973</option>

<option value="1974">1974</option>

<option value="1975">1975</option>

<option value="1976">1976</option>

<option value="1977">1977</option>

<option value="1978">1978</option>

<option value="1979">1979</option>

<option value="1980">1980</option>

<option value="1981">1981</option>

<option value="1982">1982</option>

<option value="1983">1983</option>

<option value="1984">1984</option>

<option value="1985">1985</option>

<option value="1986">1986</option>

<option value="1987">1987</option>

<option value="1988">1988</option>

<option value="1989">1989</option>

<option value="1990">1990</option>

<option value="1991">1991</option>

<option value="1992">1992</option>

<option value="1993">1993</option>

<option value="1994">1994</option>

<option value="1995">1995</option>

<option value="1996">1996</option>

<option value="1997">1997</option>

</select>

</div>

<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label">Gender:</label><br>

<select class="select_mini hideOnDuplicateEmail"  name="gender">
<option value="male">Male</option>
<option value="female">Female</option>
</select>
</div>

<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label">Telephone - Mobile:<span style="color:red">*</span></label><br>
<input name="mobile" type="text" id="mobile" class="form_fields  hideOnDuplicateEmail"  onkeypress="return isNumber(event)"/>
</div>


</div>


</div>
<!-- content area ends here-->


<!-- content area 2-->
<div class="content_area hideDivOnDuplicateEmail" >
<div class="content_area_center">
<h1 class="heading_beneficiary">Automatic Address Lookup</h1>
<p class="content_subheading">Enter your United Kingdom postcode and click the 'Search for address' button</p>
</div>

<!-- content area left-->
<div class="content_area_left hideDivOnDuplicateEmail">

<div class="field_wrapper">
<label class="form_label">Post code:<span style="color:red">*</span></label><br>
<input id="postCodeSearch"  class="form_fields  hideOnDuplicateEmail" type="text" onKeyPress='enterToSearch(event);' onFocus="clearAddress();"/>
</div>


<div class="field_wrapper">
<input type="button" id="searchAddress" value="Search for address" class="submit_btn" />
</div>

<div class="field_wrapper" id="addressContainer">

<label class="form_label">Suggested addresses<br /><small>please choose your address from the list below</small></label>
<div id="suggesstions"></div>
</div>

</div>

<!-- content area right-->
<div class="content_area_right">
</div>

</div>
<!-- content area ends here 2-->



<!-- content area 3-->
<div class="content_area hideDivOnDuplicateEmail">

<div class="content_area_center">
<p class="content_heading">Address</p>
<p class="content_subheading">If your address cannot be found using the automatic lookup above, please enter your address here.</p>
</div>


<!-- content area left-->
<div class="content_area_left">


<div class="field_wrapper">
<label class="form_label">Building Number:</label><br>
<input name="buildingNumber" id="buildingNumber" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
</div>

<div class="field_wrapper">
<label class="form_label">Street:</label><br>
<input name="street" id="street" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
</div>

<div class="field_wrapper">
<label class="form_label">Town:</label><br>
<input name="town" id="town" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
</div>

<div class="field_wrapper">
<label class="form_label">Region:</label><br>

<input name="province" id="province" type="text" class="form_fields  hideOnDuplicateEmail" onKeyPress='enterToSearch(event);'/>
</div>





</div>

<!-- content area right-->
<div class="content_area_right">

<div class="field_wrapper">
<label class="form_label">Postal code:<span style="color:red">*</span></label><br>

<input  name="postcode" type="text" id="postcode"  class="form_fields  hideOnDuplicateEmail" />

</div>

<div class="field_wrapper">
<label class="form_label">Country:</label><br>
<select name="residenceCountry" class="select_field" id="residenceCountry">
<? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
$custCountries = array_filter($custCountries);
for ($k = 0; $k < count($custCountries); $k++) {
$selected="";
if (trim($custCountries[$k]) == "United Kingdom"){
$selected = "selected";         }    ?>
<option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
<?
echo trim($custCountries[$k]) ?>
</option>
<?
} ?>
</select>
</div>

<div class="field_wrapper">
<label class="form_label">Building Name:</label><br>
<input name="buildingName" id="buildingName" class="form_fields  hideOnDuplicateEmail" type="text" onKeyPress='enterToSearch(event);'/>
</div>
</div>
</div>
<!-- content area ends here 3-->



<!-- content area 4-->
<div class="content_area  hideDivOnDuplicateEmail">
<div class="content_area_center">
<h1 class="heading_beneficiary">Passport Details
</h1>
</div>

<!-- content area left-->
<div id="passportDiv" class="content_area_left hideDivOnDuplicateEmail">

<div class="field_wrapper">
<label class="form_label">Country of issue :<span style="color:red">*</span></label><br>

<select name="passportCountry" id="passportCountry" class="select_field" >


	<? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
	$custCountries = array_filter($custCountries);
	for ($k = 0; $k < count($custCountries); $k++) {
		$selected="";
		if (trim($custCountries[$k]) == "United Kingdom"){
			$selected = "selected";         }    ?>
		<option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
			<?
			echo trim($custCountries[$k]) ?>
		</option>
		<?
	} ?>


	

<!--	-->
<!--<option value="" selected='selected'>-- Select Country of Issue --</option>-->
<!--<option value="United Kingdom">United Kingdom</option>-->
<!--<option value="Portugal">Portugal</option>-->
<!--<option value="USA">USA</option>-->
<!--<option value="Australia">Australia</option>-->
<!--<option value="Spain">Spain</option>-->
<!--<option value="Ireland">Ireland</option>-->
<!--<option value="Netherlands">Netherlands</option>-->
<!--<option value="Canada">Canada</option>-->

</select>

</div>


<div class="field_wrapper">

<img src="images/premier/formImages/uk_passport.jpg" alt="UK Specimin Passport" width="280px"/>
</div>

<div class="field_wrapper">
<label class="form_label">Passport Number:<span style="color:red">*</span></label><br>
<input name="passportNumber" type="text" size="62" class="form_fields  hideOnDuplicateEmail passport_fields" id="passportNumber"   />

<span class="please_do" style="color:grey">Please enter your passport number as highlighted above.</span>
<span class="please_do" id="passportAvailability" ></span>
</div>

<div class="field_wrapper">
<label class="form_label   ">Issue Date:</label><br>
<input name="passportIssue" id="passportIssue" class="form_fields  hideOnDuplicateEmail" type="text" readonly />
</div>

<div class="field_wrapper">
<label class="form_label">Passport Expiry Date:<span style="color:red">*</span></label><br>
<input name="passportExpiry" id="passportExpiry" class="hideOnDuplicateEmail form_fields" value="" readonly />

</div>

</div>

<!-- content area right-->
<div class="content_area_right ">



<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label ">Passport Upload </label><br><br>
<!--<a href="javascript:void(0);" class="submit_btn" id="file" class="button" >Select file</a>-->
<button class="submit_btn" id="file">Select file</button>
<br>

<input type="file" class="submit_btn" name="passportDoc" id="passportDoc" style="visibility:hidden;" />
<span class="please_do" style="color:grey;display:inline-block;margin-top: -33px;">Please upload a scanned copy of your passport, the maximum file size is 2Mb. If you are having difficulty uploading the file please post a copy or e-mail us at <a href="mailto:hello@payscanner.com">hello@payscanner.com</a> as soon as possible</span><br>
<div class="please_do" id="fileUploader">
<img src="images/premier/formImages/loading.gif"/> Uploading ...
</div>
</div>



</div>
</div>
<!-- content area ends here 5-->
<?php
if (!empty($register_type) && strtolower($register_type) == 'prepaid') {
	$display = "display:block";

} else {

	$display = "display:none";

}
?>
<!-- content area 2-->
<div class="content_area ">
<div class="content_area_center hideDivOnDuplicateEmailh" style="<?=$display?>">
<!--<h1 class="heading_beneficiary">National Identity Card Details</h1>-->
<!--<h1 class="heading_beneficiary">Initial Load for Prepaid Card</h1>-->
</div>

<!-- content area left-->
<div class="content_area_left">


<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
<label class="form_label">Line1:</label><br>
<input name="line1" type="text" id="line1" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase" />
</div>



<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
<label class="form_label">Line3:</label><br>

<input name="line3" id="line3" type="text" maxlength="30" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase;" /><br>
<div class="please_do" id="NICAvailability"></div>
</div>
<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">

<label class="form_label">Country of nationality:</label><br>

<select name="nationalityCountry" class="select_field  hideOnDuplicateEmail" id="nationalityCountry">
<? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
$custCountries = array_filter($custCountries);
for ($k = 0; $k < count($custCountries); $k++) {
$selected="";
if (trim($custCountries[$k]) == "United Kingdom"){
$selected = "selected";    }  ?>
<option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
<?
echo trim($custCountries[$k]) ?>
</option>
<?
} ?>
</select>
</div>

	<div class="field_wrapper" id="initial_load_row" style="<?=$display?>">
	<!--<label class="form_label">Initial Load:<span style="color:red">*</span></label><br>-->
	<input name="initial_load" type="hidden" id="initial_load" class="form_fields" value="0.00" style="text-transform:uppercase" />
	</div>

<div class="field_wrapper hideDivOnDuplicateEmail">
<label class="form_label">How did you hear about us?</label>
<textarea name="heardAboutUs" class="form_fields  hideOnDuplicateEmail" cols="50" rows="2" style="height:60px;"></textarea>
</div>

</div>

<!-- content area right-->
<div class="content_area_right ">
<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
<label class="form_label">Line2:</label><br>

<input name="line2" id="line2" type="text" class="form_fields  hideOnDuplicateEmail" style="text-transform:uppercase" />
</div>


<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
<label class="form_label">Expiry Date:<span style="color:red">*</span></label><br>
<input name="idExpiry" id="idExpiry" class="form_fields  hideOnDuplicateEmail" type="text" readonly/>
</div>

<div class="field_wrapper hideDivOnDuplicateEmailh" style="display:none">
<label class="form_label">Country of issue:</label><br>
<select name="issueCountry" class="select_field  hideOnDuplicateEmail" id="issueCountry">
<? $custCountries = explode(",", CONFIG_CUST_COUNTRIES_LIST);
$custCountries = array_filter($custCountries);
for ($k = 0; $k < count($custCountries); $k++) {
$selected="";
if (trim($custCountries[$k]) == "United Kingdom"){
$selected = "selected";    }  ?>
<option value="<? echo trim($custCountries[$k]) ?>" <? echo $selected ?>   >
<?
echo trim($custCountries[$k]) ?>
</option>
<?
} ?>
</select>
</div>
<?php
//if(!empty($register_type) && strtolower($register_type) == 'prepaid')
{
	?>
		<div class="field_wrapper" id="currency_row" style="<?=$display?>">
		<label class="form_label"><span style="color:red"></span></label><br>
		<input type="hidden" name="card_issue_currency" class="select_field" id="card_issue_currency" value="<?php echo $cust_type; ?>">
		<!--<select name="card_issue_currency" class="select_field" id="card_issue_currency" >
			<option value="">---SELECT---</option>

			<option value="EUR">EUR</option>
			<option value="USD">USD</option>
		</select>-->
	</div>
<?php
}
?>





<div class="field_wrapper">

<input type="submit" name="register" value="Register" id="register" class="submit_btn" />
</div>


</div>

<div class="error_msg" id="msg" style="display:none;"></div>
</div>


<!-- content area ends here 5-->

</form>
</div>

<!-- footer area -->
<!--<?php include '../premier_fx_online_module/footer.php';?>-->
<?php include '../online/footer.php';?>
</div>



</div>
</body>
	<script>


	function removerules(){


	}
			var succssMsg = '';
			var fileValidateFlag = 0;

			$(function() {

				$('#addressContainer').hide();

				var button = $('#file');

				var fileUpload = new AjaxUpload(button, {

					action: 'registration_form_conf.php',

					name: 'passportDoc',

					// for single upload this should have 0 (zero appended)

					autoSubmit: false,

					allowedExtensions: ['.jpg', '.png', '.gif', '.jpeg', '.pdf'],

					// max size

					onChange: function(file, ext) {

						if ((ext && /^(jpg|png|jpeg|gif|pdf)$/i.test(ext))) {
							var fileSizeBytes = this._input.files[0].size;
							if(fileSizeBytes < 2097152){
								fileValidateFlag = 1;

								//button.text("Selected");

								button.css({

									"background": "#CCCCCC",

									"font-weight": "bold"

								});

								button.html('Document Selected');
							}else{
								alert("file is too large, maximum file size is 2MB");
							}

						} else {

							alert('Please choose a standard image file to upload, JPG or JPEG or PNG or GIF or PDF');

							button.css({

								"background": "#ECB83A",

								"font-weight": "normal"

							});

							button.html('Select Document');

						}

					},

					onSubmit: function(file, ext) {

						// If you want to allow uploading only 1 file at time,

						// you can disable upload button

						this.disable();

						$('#wait').fadeOut("slow");
						$('#wait').fadeIn("slow");
						$('#fileUploader').fadeIn("slow");

					},

					onComplete: function(file, response) {

						// enable upload button

						this.enable();

						//this.disable();

						$('#emailValidator').html(' ');

						$('#wait').fadeOut("fast");
						$('#fileUploader').fadeOut("slow");

						alert(response);
						$('#msg').show();
						$('#msg').html(succssMsg);

					}

				});





				$(document).ready(function() {


				$('#register').click(function(){


					 if($( "#initial_load_row" ).is(":visible") == false){

						$("#card_issue_currency").rules("remove");
						$("#initial_load").rules("remove");

					}

				});


					// Date Picker

					Date.format = 'dd/mm/yyyy';


					$('#passportExpiry').datePicker({

						clickInput: true,

						createButton: false

					});

					$('#passportIssue').datePicker({

						clickInput: true,

						createButton: false,

						endDate: (new Date()).asString()

					});

					$('#idExpiry').datePicker({

						clickInput: true,

						createButton: false

					});

					$('#passportIssue').dpSetStartDate(' ');





					// Masking

					//$('#passportNumber').mask('999999999-9-aaa-9999999-a-9999999-**************-9-9');

					//$('#line1').mask('**-***-*********-9-***************');

					//$('#line2').mask('999999-9-*-999999-9-aaa-***********');





					// Form Validation

					$("#senderRegistrationForm").validate({

						rules: {




							forename: {

								required: true

							}

							,
							/*
							password: {

								required: true, minlength: 6

							}

							,
							cpassword: {

								required: true, equalTo: "#password", minlength: 6



							}

							,
*/
							surname: {

								required: true

							},

							postcode: {

								required: true

							},

							passportNumber: {

								required: function() {

									if ($('#line1').val() == '') return true;

									else return false;

								}

								//,minlength: 37

							},

							/* line1: {

								required: function() {

									if ($('#passportNumber').val() == '') return true;

									else return false;

								}

							}, */

							/* line2: {

								required: function() {

									if ($('#line1').val() == '') return false;

									else return true;

								}

							},

							line3: {

								required: function() {

									if ($('#line2').val() == '' || $('#line1').val() == '') return false;

									else return true;

								}

							},

							idExpiry: {

								required: function() {

									if ($('#line1').val() != '' && $('#line2').val() != '' && $('#line3').val() != '') return true;

									else return false;

								}

							}, */

							passportExpiry: {
                            required: true
								// required: function() {

									// if ($('#passportNumber').val() != '') return true;

									// else return false;

								// }

							},
							initial_load: {
								required: false
							},
							card_issue_currency: {
								required: false
							},

						},

						messages: {

							passportNumber: "Please provide your Passport or National Identity card number",

							/* line1: "Please provide your Passport or National Identity card number",

							line2: "Please enter the remaining part of your National Identity card number",

							line3: "Please enter the remaining part of your National Identity card number",
 */
							passportExpiry: "Please enter the expiry date of your Passport",

							password: "Password should be of atleast 6 characters",

							cpassword: "Confirm password does not match",

						},

						submitHandler: function() {

							register();

							return false;

						}

					});



					// Validating Email availability checks

					$("#email").blur(



		function() {

			var email = $('#email').val();



			if (email != '') {

				$.ajax({

					url: "registration_form_conf.php",

					data: {

						email: email,

						chkEmailID: '1'

					},

					type: "POST",

					cache: false,

					success: function(data) {

					existingCurr = [];
					Currencies = [];


					emaildup = data.split('|');


					if(emaildup[2] !=undefined){
						prepaidCurrency = emaildup[2].split(',');

						ExistingCurrency =  document.getElementById("card_issue_currency");

					for(i=0;i<ExistingCurrency.length; i++){
						existingCurr.push(ExistingCurrency.options[i].text);
						}
					$("#card_issue_currency").empty();
						for(j=0;j<existingCurr.length;j++){

					if($.inArray(existingCurr[j],prepaidCurrency) == -1){
						$("#card_issue_currency").append('<option value="'+existingCurr[j]+'">'+existingCurr[j]+'</option>');
												}

						}
					}else{


					$("#card_issue_currency").empty();

					$("#card_issue_currency").append('<option value="---SELECT---">---SELECT---</option><option value="USD">USD</option><option value="EUR">EUR</option>');


					}

					//console.log(Currencies);
					if($.trim(emaildup[0]) != ''){

					$('.hideDivOnDuplicateEmail').css('display','none');
					//$("#passportNumber").rules("remove", "required");
					//$(".hideOnDuplicateEmail").rules("remove", "required");
					//$("#passportExpiry").rules("remove", "required");

			/* 		$("#postcode").rules("remove", "required");

					$("#line1").rules("remove", "required"); */


					$('#senderRegistrationForm').find('.hideOnDuplicateEmail').each( function (index,element) {



						 $(element).rules('remove', 'required');
					});


					if($.trim(emaildup[1])  == 'PC'){

					$("#currency_row").css("display","none");

					$("#initial_load_row").css("display","none");

					$("#card_issue_currency").rules("remove", "required");

					$("#initial_load").rules("remove", "required");

					}else if($.trim(emaildup[1])  == 'TC'){


			/* 		$("#currency_row").css("display","block");

					$("#initial_load_row").css("display","block");

					$("#card_issue_currency").rules("add", "required");

					$("#initial_load").rules("add", "required"); */

					}


					}else{

					if($('#register_type').val()  == 'prepaid'){

					$("#currency_row").css("display","block");

					$("#initial_load_row").css("display","block");

					//$("#card_issue_currency").rules("add", "required");

					$("#initial_load").rules("add", "required");

					}

					$('.hideDivOnDuplicateEmail').css('display','block');
	// $('#senderRegistrationForm').find('.hideOnDuplicateEmail').each(function (index,element) {
	// console.log(element.id);
	// if(element.id !="passportExpiry"){
		// $(element).rules('remove', 'required');
		// }

					// });


					}

					$('#emailValidator').html($.trim(emaildup[0]) );

					}

				});

			}

		});



					// Check if the passport availability checks
					$('#passportNumber').blur(



					function() {

						setTimeout(function() {

							passportAvailabilty();

						}, 100);

					});



					function passportAvailabilty() {

						var passport = $('#passportNumber').val();
						//alert(passport);

						if (passport != '') {

							$.ajax({

								url: "registration_form_conf.php",

								data: {

									passportNum: passport,

									chkPassport: '1'

								},

								type: "POST",

								cache: false,

								success: function(data) {

								$('#passportAvailability').html(data);
								}

							});

						}

					}





					// ID card availability checks

					$('#line3').blur(



					function() {

						var idLine1 = $('#line1').val();

						var idLine2 = $('#line2').val();

						var idLine3 = $('#line3').val();

						if (idLine1 != '' && idLine2 != '' && idLine3 != '') {

							var idCard = idLine1 + idLine2 + idLine3;

						} else return false;

						$.ajax({

							url: "registration_form_conf.php",

							data: {

								idCardNumber: idCard,

								chkIDCard: '1'

							},

							type: "POST",

							cache: false,

							success: function(data) {

								$('#NICAvailability').html(data);

							}

						});

					});




/**
* function checkForServices()
* against ticket 12761
* To ask for the other services
*/
				function checkForServices(msg){
				//alert($.trim(msg)+"   "+ $("#initial_load").val());
				var confirmResult = false;

				if($.trim(msg) == 'PC'){

				confirmResult = confirm("You are already registered as Prepaid customer. Do you want to register as Trading Customer ?");

			//	confirmResult = true;

				}else if($.trim(msg) == 'TC'){



				confirmResult = confirm("You are already registered as Trading customer. Do you want to register as Prepaid Customer?");

				// if( (confirmResult == true )  == false ){

				// //$("#currency_row").css("display","block");

				// //$("#initial_load_row").css("display","block");


				// // $("#card_issue_currency").rules("add", "required");
				 // //$("#initial_load").rules("add", "required");
				 // //$("#initial_load").val('');
				 // confirmResult = false;
				// }

				}else if ($.trim(msg) == 'PC,TC'){

			// alert("here both pc and tc");
			// return false;
				// if($('#card_issue_currency option').size()>1){

			confirmResult = confirm('Do you want to proceed?')

			//$("#currency_row").css("display","block");
				// if($('#card_issue_currency').val() =='---SELECT---'){
					// //alert("Please choose currency.");
					// return false;
					// }else{
					// confirmResult = confirm('Do you want another currency for prepaid?');
					// }

					// }else{
					// alert("You have registered with all currencies.");
					// return false;
					// }


				$('.hideDivOnDuplicateEmail').css('display','block');
				$(".hideOnDuplicateEmail").rules("add","required");

				}else if ($.trim(msg) == ''){

				confirmResult = true;

				}

				return confirmResult;
			}



			function initialLoadLimit(){

			// if($("#initial_load_row").is("visible") == false){

			// return true;

			// }

		 initialLoad = $("#initial_load").val();

		 receivingCurrency = $("#card_issue_currency").val();

				if(receivingCurrency == 'EUR')
			{
				if(initialLoad < 20)
				{
					alert('You can not send money less than 20');
					return false ;
				}else if(initialLoad > 500){
					alert('You can not send money more than500');
					return false ;
				}
			}else if(receivingCurrency == 'USD')
			{
				if(initialLoad < 20)
				{
					alert('You can not send money less than 20');
					return false ;
				}else if(initialLoad > 500){
					alert('You can not send money more than 500');
					return false ;
				}
			}else if(receivingCurrency == 'GBP')
			{
				if(initialLoad < 20)
				{
					alert('You can not send money less than 20');
					return false ;
				}else if(initialLoad > 400){
					alert('You can not send money more than 400');
					return false ;
				}
			}
			return true;

			}
					// Ajax Registration of Sender





					function register() {



						// if(initialLoadLimit() == false){
						// return false;
						// }

						//Passport Issue and Expiry Date

						var passportIssue = new Date($('#passportIssue').val());

						var passportExpiry = new Date($('#idExpiry').val());

						if (passportIssue != '') {

							if (passportIssue >= passportExpiry) {

								alert("Your Passport issued date must be before the expiry date.");

								return false;

							}

						}

						// Registration Request

						var data = $('#senderRegistrationForm').serialize();

						var formData = $('#senderRegistrationForm').serialize();

						data += "&register=Register";
						data += "&slog=yes";
						data +="&cardType=<? echo $register_type; ?>";

						$('#wait').fadeIn("fast");
						$("#loadingMessage").text('Submitting your registration');
						$.ajax({

							url: "registration_form_conf.php",

							data: data,

							type: "POST",

							cache: false,

							success: function(msg) {

						servicesFlag = checkForServices(msg);
						 //alert(servicesFlag);

						 //console.log("in registration= "+$.trim(msg));
						if(servicesFlag == true){
						updateRegisterType(formData);
						// if($.trim(msg) == 'PC,TC'){
							// addOtherCurrency(formData);
							// }else{
						// updateRegisterType(formData);
							// }
						}

						if (msg.search(/Sender is registered successfully/i) >= 0) {

									if (fileValidateFlag != 0) {
										succssMsg = msg;
										$("#loadingMessage").text('Please wait for the confirmation of your passport upload.');
										var plog = "yes";
										var chk = "1";

										$.ajax({

										url: "registration_form_conf.php",

										data: {

											plog: plog,
											chk: chk


										},

										type: "POST",

										cache: false,
										success: function(msg) {

										}});


										fileValidateFlag = 0;

										fileUpload.submit();

									}else{
										$('#msg').show();
										$('#msg').html(msg);
										$('#wait').fadeOut("fast");
									}
									resetFormData();

								}else{

								if($.trim(msg) != "TC" && $.trim(msg) != "PC" && $.trim(msg) != "PC,TC"){

									$('#msg').show();
									$('#msg').html(msg);
									$('#wait').fadeOut("fast");

								}else{
									$('#msg').show();
									$('#msg').html("");
									$('#wait').fadeOut("fast");
								}

								}

							}

						});

					}

					function addOtherCurrency(formData){

						data = formData;

					data += '&rtype=addOtherCurrency';

					$.ajax({

						url: "registration_form_conf.php",

						data: data,

						type: "POST",

						cache: false,

						success: function(msg){


						$('#msg').html('Currency Updated.');

						resetFormData();
							}

						});



						}

					function  updateRegisterType(formData){


					data = formData;

					data += '&rtype=updateRegisterType';

					$.ajax({

						url: "registration_form_conf.php",

						data: data,

						type: "POST",

						cache: false,

						success: function(msg){
						$('#msg').css("color","green")
						$('#msg').css("font-style","italic")
						$('#msg').html(msg);

						resetFormData();
							}

						});



					}


					function resetFormData() {

						$('#addressContainer').fadeOut('fast');

						document.forms[0].reset();

						$('#file').css({

							"background-color": "#ECB83A",

							"border": "1px solid #E5A000",

							"font-weight": "normal"

						});

						$('#file').html('Select Document');
						//$('#senderRegistrationForm')[0].reset();
					}



					// Trigger the search address function

					$('#searchAddress').click(



					function() {

						searchAddress();

					});



				});

			});



			// Populate the Address in the fields





			function getAddressData(ele) {

				var value = ele.value;

				var arrAddress = value.split('/');

				var buildingNumber = $.trim(arrAddress[0]);

				var buildingName = $.trim(arrAddress[1]);

				var subBuilding = $.trim(arrAddress[2]);

				var street = $.trim(arrAddress[3]);

				var subStreet = $.trim(arrAddress[4]);

				var town = $.trim(arrAddress[5]);

				//var postcode = $.trim(arrAddress[6]);

				var organization = $.trim(arrAddress[7]);

				var buildingNumberVal = '';

				var buildingNameVal = '';

				var streetValue = '';

				var postCode = $('#postCodeSearch').val();

				if (buildingNumber != '') buildingNumberVal += buildingNumber;

				if (buildingName != '') buildingNameVal += buildingName;

				if (subBuilding != '') buildingNameVal += ' ' + subBuilding;



				if (street != '') streetValue += street;

				if (subStreet != '') streetValue += ' ' + subStreet;



				$('#buildingNumber').val(buildingNumberVal);

				$('#buildingName').val(buildingNameVal);

				$('#street').val(streetValue);

				$('#town').val(town);
				$('#postcode').val(postCode);

			}



			// if Press Enter on any field in the address area trigger the search address function





			function enterToSearch(e) {

				if (e.which) {

					keyCode = e.which;

					if (keyCode == 13) {

						e.preventDefault();

						searchAddress();

					}

				}

			}



			// Calls the API for suggessted address

function removeValidation(id){

	var texts =$('#'+id).next('.error').html('');

}




			function searchAddress() {

				$('#residenceCountry').val('United Kingdom');

				$('#addressContainer').fadeOut('fast');

				postcode = $.trim($('#postCodeSearch').val());

				buildingNumber = $.trim($('#buildingNumber').val());

				street = $.trim($('#street').val());

				town = $.trim($('#town').val());

				if (postcode == '') {

					alert("Enter a postcode to search for your address");

					$('#postCodeSearch').focus();

					return;

				}
				$("#loadingMessage").text('Searching Address...');
				$('#wait').fadeIn("fast");

				$.ajax({
					url: "https://<?php echo $_SERVER['HTTP_HOST']; ?>/api/gbgroup/addresslookupCus.php",

					data: {

						postcode: postcode,

						buildingNumber: buildingNumber,

						street: street,

						town: town

					},

					type: "POST",

					cache: false,

					success: function(data) {

						//alert(data.match(/option/i));

						$('#wait').fadeOut("slow");

						if (data.search(/option/i) >= 0) {

							$('#addressContainer').fadeIn('fast');

							$('#suggesstions').html(data);

						} else {

							$('#addressContainer').fadeIn('fast');

							$('#suggesstions').html('<i style="color:#FF4B4B;font-family:arial;font-size:11px">Sorry there was no address found against your postal code</i>');

						}

					}

				});

			}



			// Clear the address section





			function clearAddress() {

				$('#buildingNumber').val('');

				$('#buildingName').val('');

				$('#street').val('');

				$('#town').val('');

				$('#province').val('');

			}



			function passportMask() {

				passportCountry = $('#passportCountry').val();

				//alert(passportCountry);

				switch (passportCountry) {


				case 'United Kingdom':


					 $('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-<<<<<<<<<<<<<<-*-9');

					 $('#passportNumber').removeAttr("disabled");

					break;

				case 'Portugal':



					 $('#passportNumber').unmask().mask('*******<<-9-***-9999999-a-9999999-**********<<<<-*-9');
					$('#passportNumber').removeAttr("disabled");

					break;

				case 'USA':



					$('#passportNumber').unmask().mask('*********-9-***-9999999-a-9999999-*********<****-*-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'Australia':



					 $('#passportNumber').unmask().mask('********<-9-***-9999999-a-9999999-<*********<<<<-*-9');

					$('#passportNumber').removeAttr("disabled");

					break;

				case 'Spain':



				 	$('#passportNumber').unmask().mask('********<-9-aaa-9999999-a-9999999-***********<<<-*-9');

					$('#passportNumber').removeAttr("disabled");

					break;

					case 'Ireland':


				 /* 	$('#passportNumber').unmask().mask('a999999<<9aaa9999999a9999999<<<<<<<<<<<<<<<9'); */
					$('#passportNumber').unmask().mask('aa99999999aaa9999999a9999999<<<<<<<<<<<<<<<9');

					$('#passportNumber').removeAttr("disabled");

					break;

					case 'Netherlands':

				 	/* $('#passportNumber').unmask().mask('a999aa9a99a<<9999999a9999999<<<<<<<<<<<<<<<9'); */

					$('#passportNumber').unmask().mask('aa9999a999aaa9999999a9999999999999999<<<<<99');

					$('#passportNumber').removeAttr("disabled");

					break;

					case 'Canada':

				 	/* $('#passportNumber').unmask().mask('a999aa9a99a<<9999999a9999999<<<<<<<<<<<<<<<9'); */

					$('#passportNumber').unmask().mask('aa999999<9aaa9999999a9999999<<<<<<<<<<<<<<99');

					$('#passportNumber').removeAttr("disabled");

					break;

				default:

					alert('Please select a country and enter your passport number');

					break;

				}


			}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

			</script>
</body>
</html>

