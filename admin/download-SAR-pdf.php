<?php
session_start();

include "lib/tcpdf/tcpdf.php";
include ("../include/config.php");
include ("security.php");
//$userID = $_SESSION["loggedUserData"]["userID"];
//$customerID = $_GET["customerID"];
if (isset($_GET['id'])) {
    $viewReport = selectFrom("SELECT id,name,reportingInstitution,created_on,suspectID,tableForSuspect,remarks,activityFrom,activityTo,amountInvolved,activityCategory,finServices,activityType FROM savedSARs WHERE id =" . $_GET['id']);
    $adminRecord = selectFrom("SELECT name , agentAddress , agentCity , agentCountry , postCode , agentAccounNumber  FROM admin WHERE userID =" . $viewReport["reportingInstitution"]);
    $custRecord = selectFrom("SELECT customerID , lastName , firstName , middleName , Address , City , State , Phone , dob FROM customer WHERE customerID =" . $viewReport["suspectID"]);
    $uidtypeRecord = selectFrom("SELECT id_number , issued_by FROM user_id_types WHERE user_id =" . $viewReport["suspectID"]);
    $sarRecord = selectMultiRecords("SELECT id, activityName FROM suspiciousActivityList WHERE status='Active'");
    $idTypeTitle=selectMultiRecords("SELECT title FROM id_types,user_id_types WHERE id_types.id=user_id_types.user_id AND user_id_types.user_id=".$viewReport["suspectID"]);

}
else{
    redirect("saved-SAR-reports.php");
}


$tcpdf= new TCPDF('p','mm','A4');
$tcpdf->AddPage();
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, 'Reporting Financial Institution Information', 0, 1, 'C');
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', '', 12);
$name='SPI NAME : '.$adminRecord["name"];
$tcpdf->Cell(35, 5, $name);
$tcpdf->Ln(10);
$address='SPI ADDRESS : '.$adminRecord["agentAddress"];
$tcpdf->Cell(35, 5, $address);
$tcpdf->Ln(10);
$city='SPI CITY : '.$adminRecord["agentCity"];
$tcpdf->Cell(35, 5, $city);
$tcpdf->Ln(10);
$country='SPI COUNTRY : '.$adminRecord["agentCountry"];
$tcpdf->Cell(35, 5, $country);
$tcpdf->Ln(10);
$zipCode='SPI ZIP CODE : '.$adminRecord["postCode"];
$tcpdf->Cell(35, 5, $zipCode);
$tcpdf->Ln(10);
$address='SPI ADDRESS : '.$adminRecord["agentAddress"];
$tcpdf->Cell(35, 5, $address);
$tcpdf->Ln(10);
$tcpdf->CheckBox('newsletter', 5, false, array(), array(), 'OK');
$textNarrative='Multiple Branches Include Information in Narrative';
$tcpdf->Cell(55, 5, $textNarrative);
$tcpdf->Ln(10);
$city='SPI CITY : '.$adminRecord["agentCity"];
$tcpdf->Cell(35, 5, $city);
$tcpdf->Ln(10);
$country='SPI COUNTRY : '.$adminRecord["agentCountry"];
$tcpdf->Cell(35, 5, $country);
$tcpdf->Ln(10);
$zipCode='SPI ZIP CODE : '.$adminRecord["postCode"];
$tcpdf->Cell(35, 5, $zipCode);
$tcpdf->Ln(10);
$institueClosed='If institute closed, date closed?';
$tcpdf->Cell(65, 5, $institueClosed);
$tcpdf->TextField('date', 30, 5, array(), array('v'=>date('Y-m-d'), 'dv'=>date('Y-m-d')));
$tcpdf->Ln(10);
$accountNumber='Account number if any : '.$adminRecord["agentAccounNumber"];
$tcpdf->Cell(75, 5, $accountNumber);
$tcpdf->Cell(35, 5, 'Closed?');
$tcpdf->RadioButton('', 5, array(), array(), '', false);
$tcpdf->Cell(35, 5, 'YES');
$tcpdf->RadioButton('', 5, array(), array(), '', false);
$tcpdf->Cell(35, 5, 'NO');
$tcpdf->Ln(15);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, 'Customer/Sender Information', 0, 1, 'C');
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', '', 12);
$lastName='Last Name or Name of Entry : '.$custRecord["lastName"];
$tcpdf->Cell(35, 5, $lastName);
$tcpdf->Ln(10);
$firstName='First Name : '.$custRecord["lastName"];
$tcpdf->Cell(35, 5, $firstName);
$tcpdf->Ln(10);
$middleName='Middle Name : '.$custRecord["middleName"];
$tcpdf->Cell(35, 5, $middleName);
$tcpdf->Ln(10);
$custAddress='Address : '.$custRecord["Address"];
$tcpdf->Cell(35, 5, $custAddress);
$tcpdf->Ln(10);
$custCity='City : '.$custRecord["City"];
$tcpdf->Cell(35, 5, $custCity);
$tcpdf->Ln(10);
$accountNumber='Govt issued identification : ';
$tcpdf->Cell(75, 5, $accountNumber);
$tcpdf->Ln(7);
foreach ($idTypeTitle AS $GovtIdentity) {
//    echo '<input type="radio" name="GOVT.1" value="' . $GovtIdentity["title"] . '" style="margin-left: 24px;margin-right: 9px; margin-top:3px; width: 20px" disabled >' . $GovtIdentity["title"];
    $tcpdf->RadioButton('', 5, array(), array(), '', false);
    $tcpdf->Cell(55, 5, $GovtIdentity["title"]);
    $tcpdf->Ln(10);
}
$userNumber='Number : '.$uidtypeRecord["id_number"];
$tcpdf->Cell(35, 5, $userNumber);
$tcpdf->Ln(10);
$custstateNumber='Issuing State/Country : '.$custRecord["State"];
$tcpdf->Cell(35, 5, $custstateNumber);
$tcpdf->Ln(10);
$ssn='SSN,EIN or TIN :';
$tcpdf->Cell(35, 5, $ssn);
$tcpdf->Ln(10);
$custDOB='Date of Birth : '.$custRecord["dob"];
$tcpdf->Cell(35, 5, $custDOB);
$tcpdf->Ln(10);
$custPhone='Telephone Number : '.$custRecord["Phone"];
$tcpdf->Cell(35, 5, $custPhone);
$tcpdf->Ln(15);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, 'Date or date range of suspicious activity:', 0, 1, 'C');
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', '', 12);
$activityFrom='From : '.$viewReport["activityFrom"];
$tcpdf->Cell(35, 5, $activityFrom);
$tcpdf->Ln(10);
$activityTo='To : '.$viewReport["activityTo"];
$tcpdf->Cell(35, 5, $activityTo);
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, 'Total amount involved in suspicious activity', 0, 1, 'C');
$tcpdf->SetFont('helvetica', '', 12);
$tcpdf->CheckBox('newsletter', 5, false, array(), array(), 'OK');
$amountUnknown='Amount Unknown';
$tcpdf->Cell(55, 5, $amountUnknown);
$tcpdf->Ln(10);
$amount='Amount :'.$viewReport["amountInvolved"];
$tcpdf->Cell(55, 5, $amount);
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, 'Category of suspicious activity (Check all that apply)', 0, 1, 'C');
$tcpdf->SetFont('helvetica', '', 12);
$arrActivityCategory = explode(",", trim($viewReport["activityCategory"]));
foreach ($arrActivityCategory as $activityCategory) {
    $tcpdf->CheckBox('newsletter', 5, true, array(), array(), 'OK');
    $tcpdf->Cell(55, 5, $activityCategory);
}
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, ' Financial services involved in the suspicious ', 0, 1, 'C');
$tcpdf->Cell(0, 5, 'activity and character of the suspicious activity. ', 0, 1, 'C');
$tcpdf->Cell(0, 5, 'Including unusual use (Check all that apply)', 0, 1, 'C');

$tcpdf->SetFont('helvetica', '', 12);
$arrFinServices = explode(",", trim($viewReport["finServices"]));
foreach ($arrFinServices as $finServices) {
    $tcpdf->CheckBox('newsletter', 5, true, array(), array(), 'OK');
    $tcpdf->Cell(55, 5, $finServices);
}
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, ' Check all of the following that apply', 0, 1, 'C');
$tcpdf->SetFont('helvetica', '', 12);
if ($viewReport["activityType"] != "") {
$arrActivityType = explode(",", trim($viewReport["activityType"]));
foreach ($arrActivityType as $activityType) {
    $activityType = selectFrom("select activityName from suspiciousActivityList where id = $activityType and status = 'Active'");
    $tcpdf->CheckBox('newsletter', 5, true, array(), array(), 'OK');
    $tcpdf->Cell(55, 5, $activityType["activityName"]);
    $tcpdf->Ln(7);
}
}
$tcpdf->Ln(10);
$tcpdf->SetFont('helvetica', 'BI', 18);
$tcpdf->Cell(0, 5, ' Remarks', 0, 1, 'C');
$tcpdf->SetFont('helvetica', '', 12);
$tcpdf->Ln(5);
$remarks=$viewReport["remarks"];
$tcpdf->Cell(55, 5, $viewReport["remarks"]);
$tcpdf->Ln(19);


$tcpdf->Output('SAR Report.pdf');
?>