<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " destCountry";
	
$submit   = "";
$origCountry  = "";
$destCountry  = "";
$customer = "";
$feeType  = "";

if($_POST["Submit"] != "")
{	
		$origCountry = $_POST["origCountry"];
		$destCountry = $_POST["destCountry"];
		$customer = $_POST["customer"];	
		$chargeType = $_POST["chargeType"];	
		$chargeBasedOn = $_POST["feeBasedOn"];
	$submit = $_POST["Submit"];
}elseif($_GET["Submit"]!= "")
{
	if($_GET["origCountry"] != "")
		$origCountry = $_GET["origCountry"];
	if($_GET["destCountry"] != "")
		$destCountry = $_GET["destCountry"];
	if($_GET["customer"] != "")
		$customer = $_GET["customer"];	
	if($_GET["chargeType"] != "")
		$chargeType = $_GET["chargeType"];		
	if($_GET["chargeBasedOn"] != "")
		$chargeBasedOn = $_GET["chargeBasedOn"];
	
	$submit = $_GET["Submit"];
}
	
	if($submit != "")
	{
		//Added By Niaz Ahmad #2538
		
		$query = "select * from ".TBL_BANK_CHARGES . " where 1 and chargesStatus != 'Disable'";
		$queryCnt = "select count(*) from ".TBL_BANK_CHARGES . " where 1 and chargesStatus != 'Disable'";
	  
		
		if($origCountry != "")
		{
			$query .= " and origCountry ='". $origCountry."' ";		
			$queryCnt .= " and origCountry ='". $origCountry."' ";
		}
		if($destCountry != "")
		{
			$query .= " and destCountry ='". $destCountry."' ";		
			$queryCnt .= " and destCountry ='". $destCountry."' ";
		}
		if($chargeType != "")
		{
			$query .= " and chargeType ='". $chargeType."' ";		
			$queryCnt .= " and chargeType ='". $chargeType."' ";
		}
		/*if (CONFIG_FEE_BASED_DROPDOWN == "1")
		{
			
			$query .= " and feeBasedOn= 'transactionType' ";
	  }*/
	}else
	{
		$query	 = "select * from ".TBL_BANK_CHARGES." where 1 and chargesStatus != 'Disable'";
		$queryCnt = "select count(*) from ".TBL_BANK_CHARGES." where 1 and chargesStatus != 'Disable'";
	}
//$fees = SelectMultiRecords($SQL_Qry);
$allCount = countRecords($queryCnt );
if ($sortBy !="")
	 $query .=  " order by $sortBy ASC ";
  $query .= " LIMIT $offset , $limit";
//echo $SQL_Qry;
$charges = SelectMultiRecords($query);
?>
<html>
<head>
	<title>Manage Fee/Commission</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
		
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($charges)+1; ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete selected Record(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No Record(s) selected for deletion.")
		}
		return a;
	}
	
	function CheckAll()
	{
	
		var m = document.lists;
		var len = m.elements.length;
		if (document.lists.All.checked==true)
	    {
		    for (var i = 0; i < len; i++)
			 {
		      	m.elements[i].checked = true;
		     }
		}
		else{
		      for (var i = 0; i < len; i++)
			  {
		       	m.elements[i].checked=false;
		      }
		    }
	}


	
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($charges);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($charges);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($charges);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($charges);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	// end of javascript -->
	</script>
    <style type="text/css">
<!--
.style2 {color: #6699CC;
	font-weight: bold;
}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Manage Charges</td>
  </tr>
  <tr>
    <td align="center">
		<table width="50%"  border="0">
          <tr>
            <td><fieldset>
              <!--legend class="style2">Search By Country </legend-->
              			  
              <table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
                <form name="search" action="charges-list.php" method="post">
                	                	<tr bgcolor="#ededed">
									<td width="185"><font color="#005b90"><strong>Originating Country</strong></font></td>
									<td width="210">
                      <SELECT name="origCountry" id="origCountry" class=flat>
                        <OPTION value="">- Select Country - </OPTION>
                       <?
						                		$countryTypes = " and  countryType like '%origin%' ";
						            
						            if(CONFIG_COUNTRY_SERVICES_ENABLED){
													$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = fromCountryId  $countryTypes order by countryName");
												}
											else
												{
													$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
												}
											for ($i=0; $i < count($countires); $i++){
										?>
						            <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo ($countires[$i]["countryName"] == $origCountry ? "selected" : "")?>>
						            <?=$countires[$i]["countryName"]; ?>
						            </OPTION>
						            <?
											}
										?>
                       
                      </SELECT>
                    
</font></td>
                </tr>
                	<tr bgcolor="#ededed">
									<td width="185"><font color="#005b90"><strong>Destination Country</strong></font></td>
									<td width="210">
                      <SELECT name="destCountry" id="destCountry" class=flat>
                        <OPTION value="">- Select Country - </OPTION>
                       <?
						            //if(CONFIG_ENABLE_ORIGIN == "1"){
						                	
						                		$countryTypes = " and  countryType like '%destination%' ";
							                //}else{
						                		//$countryTypes = " ";
						                	//}
						            if(CONFIG_COUNTRY_SERVICES_ENABLED){
													$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
												}
											else
												{
													$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
												}
											for ($i=0; $i < count($countires); $i++){
										?>
						            <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo ($countires[$i]["countryName"] == $destCountry ? "selected" : "")?>>
						            <?=$countires[$i]["countryName"]; ?>
						            </OPTION>
						            <?
											}
										?>
                       
                      </SELECT>
                    
</font></td>
                </tr>
                       			
        			
        		<tr bgcolor="#ededed">
            	<td width="285"><font color="#005b90"><strong>Charges Type</strong></font></td>
            	<td width="210">
            	<SELECT name="chargeType" id="chargeType" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Charges Type -</OPTION>
								<option value="fixed" <? echo ($chargeType=="fixed" ? "selected" : "")?>>Fixed Charges</option>
								<option value="percent" <? echo ($chargeType=="percent" ? "selected" : "")?>>Percentage of Amount</option>
							</SELECT>
              <script language="JavaScript">
		         			SelectOption(document.forms[0].chargeType, "<?=$chargeType?>");
		       	  </script></td>
		    
        		</tr>
        			
        			
        			<tr>
        				<td>
        					&nbsp;
        				</td>
        				<td>
        					<input type="submit" name="Submit" value="Submit" class="flat">
        				</td>
        			</tr>
        		</form>
            </table>
                  <br>
            </fieldset></td>
          </tr>
        </table>
		<br>
		  <form name="lists" action="delete-charges.php" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">

		<table width="700" border="0" cellspacing="1" cellpadding="1" align="center">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="11" bgcolor="#000000"> 
							<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td colspan="3"> 
                    <?php if (count($charges) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($charges));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&chargeType=$chargeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&chargeType=$chargeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&chargeType=$chargeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&chargeType=$chargeType&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
			  		</td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
           <? if( CONFIG_DELETE_BANK_CHARGES =="1"){ ?>
            <td width="50"><font color="#005b90"><strong>Delete</strong></font></td>
            <? } ?>
            <td width="159"><font color="#005b90"><strong>Originating Country</strong></font></td>
            <td width="150" align="right"><font color="#005b90"><b>Originating Currency</b></font></td>
            <td width="159"><font color="#005b90"><strong>Destination Country</strong></font></td>
            <td width="150" align="right"><font color="#005b90"><b>Destination Currency</b></font></td>
            <td width="143" align="right"><font color="#005b90"><strong>Lower Amount Range</strong></font></td>
            <td width="148" align="right"><font color="#005b90"><b>Upper Amount Range</b></font></td>
            <td width="68" align="right"><font color="#005b90"><b>Charges</b></font></td>
            <td width="68" align="right"><font color="#005b90"><b>Charges Type</b></font></td>
            <td width="150" align="right"><font color="#005b90"><b>Charges For</b></font></td>
            <td width="150" align="right"><font color="#005b90"><b>Distributor</b></font></td>
          
            
          </tr>
          <?
			for ($i=0; $i < count($charges); $i++){
				
					    $chargeBasedOn=$charges[$i]["chargeBasedOn"];
            	
      	?>
          <tr valign="top" bgcolor="#eeeeee"> 
            
            <? if( CONFIG_DELETE_BANK_CHARGES =="1"){ ?>
	            <td align="center" ><input type="checkbox" name="chargeID[<?=$i;?>]" value="<?=$charges[$i]["chargeID"]; ?>"></td>
	            <? } ?>
	          	
	          	<td><a href="update-bank-charges.php?chargeID=<?=$charges[$i]["chargeID"];?>&origCountry=<?=$origCountry;?>&destCountry=<?=$destCountry;?>&chargeBasedOn=<?=$chargeBasedOn;?>&chargeType=<?=$chargeType;?>&Submit=<?=$submit;?>"><font color="#005b90"><b><?=stripslashes($charges[$i]["origCountry"]); ?></b></font></a></td>
	          	
	            <td><font color="#005b90"><b><?=stripslashes($charges[$i]["currencyOrigin"]); ?></b></font></td>
	           
	            <td><font color="#005b90"><b><?=stripslashes($charges[$i]["destCountry"]); ?></b></font></a></td>
	            
	            <td><font color="#005b90"><b><?=stripslashes($charges[$i]["currencyDest"]); ?></b></font></td>
	           
            	
         
          	
            	
            <td align="right"><? echo stripslashes($charges[$i]["amountRangeFrom"]); ?> </td>
            <td align="right"><?=stripslashes($charges[$i]["amountRangeTo"]); ?></td>
            <td align="right"><? echo(stripslashes($charges[$i]["Charges"])); ?></td>
            <td align="right"><?
            	 if($charges[$i]["chargeType"]=="percent")
            	{
            		echo("Percentage of Amount");
            		}else{
            			echo("Fixed Value");
            		}
            		
            	 ?>
            	 </td>
            	 
            <td align="right"><? echo $chargeBasedOn; ?> </td> 
            <td align="right"><? 
            	if($chargeBasedOn == 'Distributor')
            	{
	            	$agentName = selectFrom("select userID, username, name from ".TBL_ADMIN_USERS." where userID = '".$charges[$i]["agentNo"]."'");
	            	echo $agentName["name"]."[".$agentName["username"]."]"; 
            	}
            	?> </td>
            
            	 
          </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="11" bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td colspan="3"> 
                    <?php if (count($charges) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($charges));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&customer=$customer&origCountry=$origCountry&destCountry=$destCountry&Submit=$submit&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
         <? if( CONFIG_DELETE_BANK_CHARGES =="1"){ ?>
          <tr> 
            <td colspan="5" align="center"> <input type="submit" value="Delete Charges"> 
            </td>
          </tr>
          <? } ?>  
          <?
			} else {
		?>
          <tr> 
            <td colspan="9" align="center"> No Records found in the database. 
            </td>
          </tr>
          <?
			}
		?>
        </table></form>
	</td>
  </tr>

</table>
</body>
</html>
