<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$agentType = getAgentType();
	$custCompFlag = true;
	if(defined("CONFIG_CUST_NON_MANDATORY_USERS_CURR") && (strstr(CONFIG_CUST_NON_MANDATORY_USERS_CURR,$agentType.",") || CONFIG_CUST_NON_MANDATORY_USERS_CURR=="1")){
		$custCompFlag = false;
	}
	/* #7106 - AMB Exchange
	 * Currency Exchange Fee Non Mandatory
	 * by Aslam Shahid.
	*/
	$feeCompFlag = true;
	if(defined("CONFIG_FEE_NON_MANDATORY_USERS_CURR") && (strstr(CONFIG_FEE_NON_MANDATORY_USERS_CURR,$agentType.",") || CONFIG_FEE_NON_MANDATORY_USERS_CURR=="1")){
		$feeCompFlag = false;
	}
	/* #5781 - AMB Exchange
	 * Currency Exchange Fee module added
	 * Also works for manual fee.
	 * by Aslam Shahid.
	*/
	$currencyFeeFlag = false;
	if(defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && (strstr(CONFIG_CURRENCY_EXCHANGE_FEE_USERS,$agentType.",") || CONFIG_CURRENCY_EXCHANGE_FEE_USERS=="1")){
		$currencyFeeFlag = true;
	}
	$currencyManulFeeFlag = false;
	if(defined("CONFIG_CURRENCY_EXCHANGE_MANUAL_FEE_USERS") && 
		(strstr(CONFIG_CURRENCY_EXCHANGE_MANUAL_FEE_USERS,$agentType.",") || CONFIG_CURRENCY_EXCHANGE_MANUAL_FEE_USERS=="1")){
		$currencyManulFeeFlag = true;
	}

	$date_time = date('d-m-Y  h:i:s A');
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$currHeading = "Create Currency Exchange Transaction.";
	$currDefault = "";
	$currDataArr = SelectMultiRecords("select cID,country,currencyName,count(currencyName) as inter_curr 
											from currencies 
											where cID IN 
										(select buysellCurrency 
											from curr_exchange order by id DESC) 
											group by currencyName");
	$buy_rate = "0.0";
	$sell_rate = "0.0";
	$buySellCurrName = "";
	$firstCurrency = $currDataArr[0]['cID'];
	$totalCurrencyName = "GBP";
	$totalCurrencyID = 246;
	if(!empty($firstCurrency)){
		$currRateArr 	 = selectFrom("select * from curr_exchange where buysellCurrency = ".$firstCurrency." order by id DESC");
		$buy_rate = round($currRateArr['buying_rate'],4);
		$sell_rate = round($currRateArr['selling_rate'],4);
		$buySellCurrName = $currDataArr[0]['currencyName'];
	}
	// Currency Fee calculation
		if($currencyFeeFlag){
			$fee="0";
			$feeType = "F";
			$feeId = calculateCurrencyExchangeFee('CE',$totalCurrencyID, $firstCurrency);
			if($feeId)
			{
				$arrFullFeeData = selectFrom("select fee_id, type, fee from currency_exchange_fee where fee_id='".$feeId."'");
				$fee = $arrFullFeeData["fee"];
				$feeType = $arrFullFeeData["type"];
				if(CONFIG_ROUND_FEE_ENABLED_CHEQ_CASH=="1"){
					if(defined("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH") && is_numeric(CONFIG_ROUND_FEE_TO_CHEQ_CASH)){
						$fee = round($fee,CONFIG_ROUND_FEE_TO_CHEQ_CASH);
					}
				}
			}
		}

		//debug($feeId." * ".$fee);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Create Currency Exchange</title>

<!--<script language="javascript" src="jquery.js"></script>-->
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<!-- jqGrid JS Start -->
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script> 
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<!--<script src="javascript/jqGrid/js/inlineedit.js" type="text/javascript"></script>-->
<!-- jqGrid JS Start -->

<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>

<script>
function check(inp)
{
	if(inp.checked == true){
		ResetValues();
		document.getElementById('manual_rate').style.display = 'table-cell';
	}
	else if(inp.checked == false){
		ResetValues();
		document.getElementById('manual_rate').style.display = 'none';
	}
	$("#buy_rate").val('');
	$("#buy_sell_total_amount").val('');
	$("#sell_rate").val('');
	$("#totalAmountBought").val("");
	$("#buy_sell_amount").val("");
}
function ResetValues(){
	$("#buy_rate").val("");
	$("#buy_rate").attr("readonly",true);					
	$("#sell_rate").val("");
	$("#sell_rate").attr("readonly",true);
	$("#totalAmountBought").val("");
	$("#buy_sell_amount").val("");
	$("#buy_sell_total_amount").val('');
	document.getElementById('manualExchangeRate1').checked =true;
	document.getElementById('curr_amount').innerHTML ="";
	document.getElementById('gbp_amount').innerHTML ="";
	document.getElementById('buysellCurrency').options[0].selected=true;
}
function getCurrAmt(val){
	$.get("get-currency-exchange-amount_AMB.php?cur_id="+val,function(data){
	var res = data.split('|');
	var currency = res[1];
	var amount = res[0];
	var gbp_amount =res[2];
	document.getElementById('curr_amount').innerHTML =currency +" Current Stock: "+amount;
	document.getElementById('gbp_amount').innerHTML ="GBP Current Stock: "+gbp_amount;
	$("#buySellCurrLable1").val(currency);
	$("#buySellCurrLable2").val(currency);
	$("#buySellCurrLable3").val(currency);
	if( document.getElementById('system_rate').checked == false){
		$("#buy_rate").val('');
		$("#buy_sell_total_amount").val('');
		$("#sell_rate").val('');
		//alert(data);
		$("#totalAmountBought").val("");
		$("#buy_sell_amount").val("");
	}
	});

}
	var customerId = '';
	<?
	if(isset($_GET['customerID']))
	{
	?>
		customerId = <?=$_GET['customerID']?>;
	<?
	}
	?>
	$(document).ready(function(){
		if(customerId != '')
		{
			
			$("#customerID").val(customerId);
				$("#customer_input").val('');
				$("#customerDetails").load("searchCustomer-ajax.php?cusdi="+customerId+"&getCustomerDetails=get");
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").show();	
			
		}
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#submitCurrExch").attr("disabled",true);
	/*	   $("#storeBuyForm").attr("disabled",true);
		   $("#storeSellForm").attr("disabled",true);*/
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#submitCurrExch").attr("disabled",false);
/*		   $("#storeBuyForm").attr("disabled",false);
		   $("#storeSellForm").attr("disabled",false);*/
		});
		$("#customer_input").autocomplete('searchCustomer-ajax.php', {
				mustMatch: true,
				maxItemsToShow : -1 
			}
		);
		$("#customer_input").result(function(event, data, formatted) {
			if (data)
			{
				//alert(data);
				$("#customerID").val(data[1]);
				$("#customer_input").val(data[0]);
				$("#customerDetails").load("searchCustomer-ajax.php?cusdi="+$("#customerID").val()+"&getCustomerDetails=get");
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").show();
			}
		});

		var tr_type = '';
		$("#storeBuyForm").click(function () {
			ResetValues();
			// trigger change event of currency to re-load stock history grid
			<?php if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1"){ ?>
				$("#buysellCurrency").trigger("change");
			<?php }?>
			$("#storeBuyForm").attr("disabled",true);
			$("#storeSellForm").attr("disabled",false);
			tr_type = "buy";
			$("#buy_sell_label").html("Buy");
			$("#buy_sell_curr_label").html("Buying");
			if($("input[@name='manualExchangeRate']:checked").val()!="Y"){
				$("#buy_rate").attr("readonly",true);
				$("#sell_rate").attr("readonly",true);
			}
			$("#totalAmountBought").val("");
			$("#buy_sell_amount").val("");
			$("#showCurrencyDetails").show();
			$("#buy_rate").val('');
			$("#buy_sell_total_amount").val('');
			$("#sell_rate").val('');

		});
		$("#storeSellForm").click(function () {
			ResetValues();
			// trigger change event of currency to re-load stock history grid
			<?php if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1"){ ?>
				$("#buysellCurrency").trigger("change");
			<?php } ?>
			$("#storeSellForm").attr("disabled",true);
			$("#storeBuyForm").attr("disabled",false);
			tr_type = "sell";
			$("#buy_sell_label").html("Sell");
			$("#buy_sell_curr_label").html("Selling");
			if($("input[@name='manualExchangeRate']:checked").val()!="Y"){
				$("#buy_rate").attr("readonly",true);
				$("#sell_rate").attr("readonly",true);
			}
			$("#buy_rate").val('');
			$("#buy_sell_total_amount").val('');
			$("#sell_rate").val('');
			$("#totalAmountBought").val("");
			$("#buy_sell_amount").val("");
			<?php if($currencyFeeFlag){?>
				$("#buy_sell_total_amount").val("");
				//$("#currency_fee").val("");
			<?php }?>
			$("#showCurrencyDetails").show();
		});
		
		$("#showHideCustomerDetailBtn").click(function () {
		 
			if($("#showHideCustomerDetailBtn").html() == "[-]")
			{
				$("#customerDetails").hide();
				$("#showHideCustomerDetailBtn").html("[+]");
			}
			else
			{
				$("#customerDetails").show();
				$("#showHideCustomerDetailBtn").html("[-]");
			}
		});
		$("#newCustomerBtn").click(function(){
			window.open ("/admin/add-customer.php","AddCustomer","location=1,scrollbars=1,width=900,height=450"); 
		});
		
		$('img').cluetip({splitTitle:'|'});
		
		
		/* vaildations of the fields start */

		$("#order_form").validate({
			rules: {
				<? if($custCompFlag){?>
				customer_input: "required",
				buysellCurrency: {
					required: true
				},
				<? }?>
				buy_sell_amount: {
					required: true,
					number: true,
					min   :0.0000001
				},
				totalAmountBought: {
					required: true,
					number: true,
					min   :0.0000001,
					max: function(){
						if($("#_li"))
							return $("#_li").val();
						else
							return true;
					}
				},
				buy_rate: {
					number: true,
					min   :0.1
				},
				sell_rate: {
					//required: true,
					number: true,
					min   :0.1
				}
				/* fee config */
			<?php if($currencyFeeFlag){?>
				,currency_fee: {
					required: true,
					number: true,
					min: 0//,
					//min: "#amount_from"+1
				},
				buy_sell_total_amount: "required"
			 <?php }?>
			},
			messages: {
				<? if($custCompFlag){?>
				customer_input: "<br />Please select the customer for this Currency Exchange",
				<? }?>
				buysellCurrency: "<br />Select the currency to Buy/Sell&nbsp;",
				buy_sell_amount: {
					required: "<br />Please enter amount of Currency to buy",
					number: "<br />Please provide the valid amount.",
					min: "<br />Amount sould be greater than 0"
				},
				totalAmountBought: {
					required: "<br />Please enter total amount.",
					number: "<br />Please provide the valid amount.",
					min: "<br />Amount sould be greater than 0.",
					max: "<br />This amount is greater than your allowed limit!"
				},
				buy_rate: {
					required: "<br />Please enter manual Buying rate",
					number: "<br />Please provide the valid amount.",
					min: "<br />Amount sould be greater than 0"
				},
				sell_rate: {
					required: "<br />Please enter manual Buying rate",
					number: "<br />Please provide the valid amount.",
					min: "<br />Amount sould be greater than 0."
				}
			<?php if($currencyFeeFlag){?>
				,currency_fee: { 
					required: "<br />Please enter the Fee.",
					number: "<br />This is not valid fee.",
					min: "<br />Fee can not be less than 0."//,
					//min: "<br />Value should be greater than "+ $("#amount_from").val()
				},
				buy_sell_total_amount: "<br />Total amount is required. Please provide correct Fee/Buying/Selling amount"
			<?php }?>
			},
			submitHandler: function(form) {
			
				<?php 
				if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1") 
				{?>
					if(tr_type == "sell")
					{
						var fltTotalBoughtAmnt		= $("#totalAmountBought").val();
						var fltAvailableAmountSum	= 0;
						var sids = jQuery("#transList").getGridParam('selarrrow'); //Get selected rows IDs
						var sids = sids.toString();
						
						if(sids){
							$("#transIds").val(sids);					
							var allTransIds	= new Array(); //array to store all selected rows IDs
							allTransIds	= sids.split(",");
							// Get Sum of Available Amount of selected rows	
							for(var p=0;p<allTransIds.length;p++)
							{
								var amountObj			= jQuery("#transList").getRowData(allTransIds[p]);
								fltAvailableAmountSum	= fltAvailableAmountSum + parseFloat(amountObj.fltAvailableAmount);
							}
							
							if(fltTotalBoughtAmnt > fltAvailableAmountSum)
							{
								alert("Selected stock history Amount(s) should be equal or greater than Amount to Buy.");
								return false;
							}	
						}else{
							alert("Please select one or more records from stock history.");
							return false;
						}
					}
				<?php 
				}?>	
		
				var buySellCurrencyV = $("#buySellCurrLable3").val();
				var confirmSubmit = confirm("Are you sure to Proceed ?");
				if(confirmSubmit==false){
					return false;
				}
				$("#storeBuyForm").attr("disabled",true);
				$("#storeSellForm").attr("disabled",true);
				
				
				$("#order_form").ajaxSubmit(function(data) { 
					if(data != "E")
					{
						/*if(document.getElementById('system_rate').checked == false){
						if($("#buy_rate").val() != ''){
								$.get("add-currency-exchange-conf.php",{action:'add',buying_rate:$("#buy_rate").val(),buysellCurrency:$("#buysellCurrency").val(),operationCurrency:$("#operationalCurrency").val()},function(data){
								alert(data);					
								});
							}
						else if($("#sell_rate").val() != ''){
								$.get("add-currency-exchange-conf.php",{action:'add',selling_rate:$("#sell_rate").val(),buysellCurrency:$("#buysellCurrency").val(),operationCurrency:$("#operationalCurrency").val()},function(data){
								alert(data);					
								});
							}
						}*/
						if(data == 'stockError'){
							alert("Please create stock for the currency ["+buySellCurrencyV+"] before creating transaction");
						}
						else{
							alert("Currency Exchange has been created");
							var resData  = data.split("|");
							var rid      = resData[0];
							var opCurrId = resData[1];
							var bsCurId  = resData[2];
							//$("#order_form").clearForm();
							window.open ("/admin/buySellCurrReceipt.php?rid="+rid+"&opcId="+opCurrId+"&bscId="+bsCurId,"Currency Exchange Receipt","location=0,scrollbars=1,width=350,height=550"); 
							$("#showCurrencyDetails").hide();
							$("#totalAmountBought").val("");
							$("#showHideCompanyDetailBtn").hide();
							$("#customerDetails").hide();
							$("#customerDetails").html("");
							$("#customer_input").val("");
							$("#showHideCustomerDetailBtn").hide();
							$("#buy_sell_amount").val("");
							$('#storeBuyForm').removeAttr("disabled");
							$('#storeSellForm').removeAttr("disabled");
							$("#customerID").val("");
							var $radManual = $("input[@name='manualExchangeRate']");
							$radManual.filter("[value=N]").attr("checked", true);
							getRates();
							<?php if($currencyFeeFlag){?>
								$("#buy_sell_total_amount").val("");
								//$("#currency_fee").val("");
								feeDataFn();
								//calculateCurrencyAmount(2);
							<?php }?>
						}
					}
					else
						alert("The record could not be saved due to some error."); 
				});
		   }
		});
		
		/* Validation ends */
		$("#buy_sell_amount").blur(function(){
			
			if($("#buy_sell_amount").val()!="" && document.getElementById('system_rate').checked == true ){
				calculateCurrencyAmount(1);
			}
			else if($("#buy_sell_amount").val()!="" && tr_type == 'buy' ){
				if($("#totalAmountBought").val()!=""){
					var totlal_bought =$("#totalAmountBought").val();
					var local_buy =$("#buy_sell_amount").val();	
					var res =totlal_bought/local_buy;
					var fee =$("#currency_fee").val();	
					var res2 = parseFloat(fee) + parseFloat(local_buy);
					$("#buy_rate").val(res);
					//$("#buy_sell_total_amount").val(res2);
					$("#buy_rate").val(parseFloat($("#buy_rate").val()).toFixed(2));
					if(document.getElementById('currency_fee').getAttribute("readonly") != null){
									feeDataFn();
							}
					else{
							$("#buy_sell_total_amount").val(res2);
							}		
					//$("#sell_rate").val(1);
				}
				else{
					//alert("Please Enter the total Amount to calculate Buy Rate");
					}
				}	
			else if($("#buy_sell_amount").val()!="" && tr_type == 'sell' ){
					if($("#totalAmountBought").val()!=""){
						var totlal_bought =$("#totalAmountBought").val();
						var local_buy =$("#buy_sell_amount").val();	
						var res =totlal_bought/local_buy;
						var fee =$("#currency_fee").val();	
						var res2 =parseFloat(fee) + parseFloat(local_buy);
						$("#sell_rate").val(res);
						//$("#buy_sell_total_amount").val(res2);
						$("#sell_rate").val(parseFloat($("#sell_rate").val()).toFixed(2));
						if(document.getElementById('currency_fee').getAttribute("readonly") != null){
									feeDataFn();
							}
						else{
							$("#buy_sell_total_amount").val(res2);
							}	
						//$("#buy_rate").val(1);
						}
					else{
						//alert("Please Enter the total Amount to calculate Buy Rate");
						}
					}
		});
		$("#totalAmountBought").blur(function(){
			if($("#totalAmountBought").val()!="" && document.getElementById('system_rate').checked == true){
				calculateCurrencyAmount(2);
			}
			else if($("#totalAmountBought").val()!="" && tr_type == 'buy'){
					if($("#buy_sell_amount").val()!=""){
							var totlal_bought =$("#totalAmountBought").val();
							var local_buy =$("#buy_sell_amount").val();	
							var res =totlal_bought/local_buy;
							var fee =$("#currency_fee").val();	
							var res2 = parseFloat(fee) + parseFloat(local_buy);
							$("#buy_rate").val(res);
							//$("#buy_sell_total_amount").val(res2);
							$("#buy_rate").val(parseFloat($("#buy_rate").val()).toFixed(2));
							if(document.getElementById('currency_fee').getAttribute("readonly") != null){
									feeDataFn();
							}
							else{
								$("#buy_sell_total_amount").val(res2);
							}
							//$("#sell_rate").val(1);
						}
					else{
							//alert("Please Enter the local Amount to calculate Buy Rate");
						}	
				}
			else if($("#totalAmountBought").val()!="" && tr_type == 'sell' ){
					if($("#buy_sell_amount").val()!=""){
							var totlal_bought =$("#totalAmountBought").val();
							var local_buy =$("#buy_sell_amount").val();	
							var res =totlal_bought/local_buy;
							var fee =$("#currency_fee").val();	
							var res2 = parseFloat(fee) + parseFloat(local_buy);
							$("#sell_rate").val(res);
							$("#sell_rate").val(parseFloat($("#sell_rate").val()).toFixed(2));
							if(document.getElementById('currency_fee').getAttribute("readonly") != null){
									feeDataFn();
							}
							else{
							$("#buy_sell_total_amount").val(res2);
							}
							
							//$("#buy_rate").val(1);
						}
					else{
						//	alert("Please Enter the local Amount to calculate Buy Rate");
						}
						
				}	
				
		});
		$("input[@name='manualExchangeRate']").change(function(){
			processManualExhCheck();
		});
		$("#buy_rate").blur(function(){
			if($("input[@name='manualExchangeRate']:checked").val()=="Y" && $("#storeBuySellForm").val()=="B"){
				calculateCurrencyAmount(2);
			}
		});
		$("#sell_rate").blur(function(){
			if($("input[@name='manualExchangeRate']:checked").val()=="Y" && $("#storeBuySellForm").val()=="S"){
				calculateCurrencyAmount(2);
			}
		});
	<?php if($currencyFeeFlag){?>
		$("#calculateFeeBtn").click(function(){
			if($("#buysellCurrency").valid())
				feeDataFn();
		});
		
		$("#manualFeeBtn").click(function(){
			$("#fi").val("");
			$("#feeType").val("F");
			//$("#totalAmountBought").val("");
			$("#currency_fee").attr("readonly",false);
			//$("#currency_fee").val("");
			//$("#manualRateReasonRow").show();
			//$("#buy_sell_amount").val("");
			//$("#buy_sell_total_amount").val("");
		});
		$("#currency_fee").blur(function(){
			if($("#currency_fee").val()!="" && $("#buy_sell_amount").val()!="")
				calculateCurrencyAmount(2);
		});
	<?php }?>
		
			$("#buysellCurrency").change(function() {
				gridReload();
			});
			
		<?php if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1"){ ?>			
				var gridimgpath = 'javascript/jqGrid/themes/basic/images';  
				$("#transList").jqGrid({
					url:'add-currency-exchange-conf.php?get=getStockHistoryGrid',
					datatype: "json",
					height: 200, 
					width: 900,
					colNames:[
						'Batch ID',
						'Date In',
						'Amount',
						'Used Amount',
						'Available Amount',
						'Buying Rate',
						'Purchased From',
						'Status',
						'Stock Closing Date'
					],
					colModel:[
						{name:'batch_id',			index:'batch_id', 			width:20, 	align:"center"	},
						{name:'dateIN',				index:'created_date', 		width:30, 	align:"left"	},
						{name:'fltAmount',			index:'amount', 			width:30, 	align:"right"	},
						{name:'fltUsedAmount',		index:'used_amount', 		width:30, 	align:"right"	},
						{name:'fltAvailableAmount',	index:'available_amount', 	width:30, 	align:"right"	},
						{name:'fltBuyingRate',		index:'buying_rate', 		width:30, 	align:"right"	},
						{name:'strPurchasedFrom',	index:'purshased_from', 	width:30,	align:"left"	},
						{name:'strStatus',			index:'status', 			width:20, 	align:"left"	},
						{name:'dateClosing',		index:'closing_date', 		width:30, 	align:"left"	}
					],
					rowNum: 20,
					rowList: [5,10,20,50,100],
					imgpath: gridimgpath,
					pager: jQuery('#pager'),
					sortname: 'id',
					//viewrecords: true,
					sortorder: "desc",
					loadonce: false,
					loadtext: "Loading, please wait...",
					loadui: "block",
					forceFit: true,
					shrinkToFit: true,
					multiselect:true,
					cellEdit: false,
					//cellurl: 'currency-revaluation-conf.php?get=editRate',
					caption: "Foreign Currency Stock History",
					afterSaveCell:function(rowid, cellname, value, iRow, iCol){
						// event triggered after saving certain editable field value
					}		
					,onSelectRow:function(id){
						// write logic would be triggered on selecting certain row
					}
					,ondblClickRow:function(id){
						// write logic would be triggered on double clicking certain row
					}
				});	
			<?php } ?>
			jQuery('a').cluetip({splitTitle: '|'});
			$("#from_date").datePicker({
				startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
			});
			$("#to_date").datePicker({
				startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
			});
		});
	
		function gridReload(extraParams)
		{
			var intCurrID = $("#buysellCurrency").val();
			var theUrl="add-currency-exchange-conf.php?get=getStockHistoryGrid&currencyID="+intCurrID;
			if(extraParams)
				theUrl = theUrl+extraParams;
			$("#transList").setGridParam({
				url: theUrl,
				page:1
			}).trigger("reloadGrid");
		}
	
	
	
	function processManualExhCheck(){
		if($("input[@name='manualExchangeRate']:checked").val()=="Y"){
			$("#buy_rate").attr("readonly",false);					
			$("#sell_rate").attr("readonly",false);
		}
		else{
			$("#buy_rate").attr("readonly",true);					
			$("#sell_rate").attr("readonly",true);
			getRates();
		}
	}
	function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
	  //var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	  // Output the result to the form field (change for your purposes)
	  if(rlength == 0)
	  	rlength = 2;
	  var multiple = Math.pow(10, rlength);
      var newnumber = Math.round(rnum * multiple) / multiple;
	  return newnumber; 
	}
	function calculateCurrencyAmount(id)
	{
		var roundDecimal = 4;
		<? 
			if(CONFIG_ROUND_NUMBER_ENABLED_CURR_EXCH=="1"){
				if(defined("CONFIG_ROUND_NUMBER_TO_CURR_EXCH") && is_numeric(CONFIG_ROUND_NUMBER_TO_CURR_EXCH)){
		?>
			roundDecimal = <?=CONFIG_ROUND_NUMBER_TO_CURR_EXCH?>;
		<?
				}
			}
		?>
		var br = parseFloat($("#buy_rate").val());
		var sr = parseFloat($("#sell_rate").val());
		if(document.getElementById('system_rate').checked == true && br>0 && sr>0){
			if(id==1){
				var amount = parseFloat($("#buy_sell_amount").val());

				if($("#storeBuySellForm").val()=="B"){
					var amount_buy_currency = roundNumber((amount * br),roundDecimal);

					if(!isNaN(amount_buy_currency)){
						$("#totalAmountBought").val(amount_buy_currency);
					}
				}
				else{
					var amount_sell_currency = roundNumber((amount * sr),roundDecimal);

					if(!isNaN(amount_sell_currency)){
						$("#totalAmountBought").val(amount_sell_currency);
					}
				}	
			}
			else if(id==2){
				var amount = parseFloat($("#totalAmountBought").val());
				if($("#storeBuySellForm").val()=="S"){
					var amount_sell_currency = roundNumber((amount / sr),roundDecimal);
					if(!isNaN(amount_sell_currency)){
						$("#buy_sell_amount").val(amount_sell_currency);
					}
				}
				else{
					var amount_buy_currency = roundNumber((amount / br),roundDecimal);
					if(!isNaN(amount_buy_currency)){
						$("#buy_sell_amount").val(amount_buy_currency);
					}
				}
			}
			<?php if($currencyFeeFlag){?>
					var totalAmount = amount+parseFloat($("#currency_fee").val());
					if($("#buy_sell_total_amount"))
						calculateFinalAmount()
			<?php }?>
		}
		else if(document.getElementById('system_rate').checked == false && (br>0 || sr>0)){
			if(id==1){
				var amount = parseFloat($("#buy_sell_amount").val());
				if($("#storeBuySellForm").val()=="B"){
					var amount_buy_currency = roundNumber((amount * br),roundDecimal);
					if(!isNaN(amount_buy_currency)){
						//$("#totalAmountBought").val(amount_buy_currency);
					}
				}
				else{
					var amount_sell_currency = roundNumber((amount * sr),roundDecimal);
					if(!isNaN(amount_sell_currency)){
						//$("#totalAmountBought").val(amount_sell_currency);
					}
				}	
			}
			else if(id==2){
				var amount = parseFloat($("#totalAmountBought").val());
				if($("#storeBuySellForm").val()=="S"){
					var amount_sell_currency = roundNumber((amount / sr),roundDecimal);
					if(!isNaN(amount_sell_currency)){
						//$("#buy_sell_amount").val(amount_sell_currency);
					}
				}
				else{
					var amount_buy_currency = roundNumber((amount / br),roundDecimal);
					if(!isNaN(amount_buy_currency)){
						//$("#buy_sell_amount").val(amount_buy_currency);
					}
				}
			}
			<?php if($currencyFeeFlag){?>
					var totalAmount = amount+parseFloat($("#currency_fee").val());
					if($("#buy_sell_total_amount"))
						calculateFinalAmount()
			<?php }?>
		}
		else if($("input[@name='manualExchangeRate']:checked").val()!="N"){
		//else if($("#manualExchangeRate").is(':checked') == false){
			alert("Buying and Selling rates are not Valid.\nPlease add valid rates to proceed.");
			$("#buy_rate").val("0.0");
			$("#buy_rate").attr("readonly",true);					
			$("#sell_rate").val("0.0");
			$("#sell_rate").attr("readonly",true);
			$("#totalAmountBought").val("");
			$("#buy_sell_amount").val("");
		<?php if($currencyFeeFlag){?>
			$("#buy_sell_total_amount").val("");
			//$("#currency_fee").val("");
		<?php }?>
			return false;
		}
	}
	function getRates(){
		//document.getElementById('manualExchangeRate1').checked =true;
		$("#manualExchangeRate1").attr('checked',true);
		var data = {
				rate:'get',
				currID:$("#buysellCurrency").val(),
				createdBy:'<? echo $userID?>'
			};
			
		$.ajax({
				url: 'add-currency-exchange-conf.php',
				async: false,
				dataType: 'json',
				data: data,
				success: function(funReturn) {
				//alert(funReturn);
					if(document.getElementById('system_rate').checked == true && funReturn.buying_rate != "" && funReturn.selling_rate != "" )
					{
						$("#buy_rate").val(funReturn.buying_rate);
						$("#sell_rate").val(funReturn.selling_rate);
						$("#buySellCurrLable1").val(funReturn.buySellCurrName);
						$("#buySellCurrLable2").val(funReturn.buySellCurrName);
						$("#buySellCurrLable3").val(funReturn.buySellCurrName);
						$("#buy_rate").attr("readonly",true);					
						$("#sell_rate").attr("readonly",true);
						$("#buySellCurrLable1").attr("readonly",true);
						$("#buySellCurrLable2").attr("readonly",true);
						$("#buySellCurrLable3").attr("readonly",true);
						$("#buy_sell_amount").val("");
						$("#totalAmountBought").val("");
					<?php if($currencyFeeFlag){?>
						//$("#currency_fee").val("");
						$("#buy_sell_total_amount").val("");
					<?php }?>
	
					}
					else if(document.getElementById('system_rate').checked == false && (funReturn.buying_rate != "" || funReturn.selling_rate != "" ))
					{
						$("#buy_rate").val(funReturn.buying_rate);
						$("#sell_rate").val(funReturn.selling_rate);
						$("#buySellCurrLable1").val(funReturn.buySellCurrName);
						$("#buySellCurrLable2").val(funReturn.buySellCurrName);
						$("#buySellCurrLable3").val(funReturn.buySellCurrName);
						$("#buy_rate").attr("readonly",true);					
						$("#sell_rate").attr("readonly",true);
						$("#buySellCurrLable1").attr("readonly",true);
						$("#buySellCurrLable2").attr("readonly",true);
						$("#buySellCurrLable3").attr("readonly",true);
						$("#buy_sell_amount").val("");
						$("#totalAmountBought").val("");
					<?php if($currencyFeeFlag){?>
						//$("#currency_fee").val("");
						$("#buy_sell_total_amount").val("");
					<?php }?>
	
					}
					else
					{
						alert("There is error in getting Record for this Currency\nplease check rates properly in database.");
						$("#buy_rate").val("0.0");
						$("#buy_rate").attr("readonly",true);		
						$("#sell_rate").val("0.0");
						$("#sell_rate").attr("readonly",true);
						$("#buySellCurrLable1").attr("readonly",true);
						$("#buySellCurrLable1").val("");
						$("#buySellCurrLable2").attr("readonly",true);
						$("#buySellCurrLable2").val("");
						$("#buySellCurrLable3").attr("readonly",true);
						$("#buySellCurrLable3").val("");
						$("#totalAmountBought").val("");
						$("#buy_sell_amount").val("");
					<?php if($currencyFeeFlag){?>					
						$("#buy_sell_total_amount").val("");
						//$("#currency_fee").val("");
					<?php }?>
					}
				}
						  
			});
	////////
		/*$.getJSON("add-currency-exchange-conf.php?rate=get&currID="+$("#buysellCurrency").val()+"&createdBy="+<? echo $userID?>, 
			function(json)
			{
				if(document.getElementById('system_rate').checked == true && json.buying_rate != "" && json.selling_rate != "" )
				{
					$("#buy_rate").val(json.buying_rate);
					$("#sell_rate").val(json.selling_rate);
					$("#buySellCurrLable1").val(json.buySellCurrName);
					$("#buySellCurrLable2").val(json.buySellCurrName);
					$("#buySellCurrLable3").val(json.buySellCurrName);
					$("#buy_rate").attr("readonly",true);					
					$("#sell_rate").attr("readonly",true);
					$("#buySellCurrLable1").attr("readonly",true);
					$("#buySellCurrLable2").attr("readonly",true);
					$("#buySellCurrLable3").attr("readonly",true);
					$("#buy_sell_amount").val("");
					$("#totalAmountBought").val("");
				<?php if($currencyFeeFlag){?>
					//$("#currency_fee").val("");
					$("#buy_sell_total_amount").val("");
				<?php }?>

				}
				else if(document.getElementById('system_rate').checked == false && (json.buying_rate != "" || json.selling_rate != "" ))
				{
					$("#buy_rate").val(json.buying_rate);
					$("#sell_rate").val(json.selling_rate);
					$("#buySellCurrLable1").val(json.buySellCurrName);
					$("#buySellCurrLable2").val(json.buySellCurrName);
					$("#buySellCurrLable3").val(json.buySellCurrName);
					$("#buy_rate").attr("readonly",true);					
					$("#sell_rate").attr("readonly",true);
					$("#buySellCurrLable1").attr("readonly",true);
					$("#buySellCurrLable2").attr("readonly",true);
					$("#buySellCurrLable3").attr("readonly",true);
					$("#buy_sell_amount").val("");
					$("#totalAmountBought").val("");
				<?php if($currencyFeeFlag){?>
					//$("#currency_fee").val("");
					$("#buy_sell_total_amount").val("");
				<?php }?>

				}
				else
				{
					alert("There is error in getting Record for this Currency\nplease check rates properly in database.");
					$("#buy_rate").val("0.0");
					$("#buy_rate").attr("readonly",true);		
					$("#sell_rate").val("0.0");
					$("#sell_rate").attr("readonly",true);
					$("#buySellCurrLable1").attr("readonly",true);
					$("#buySellCurrLable1").val("");
					$("#buySellCurrLable2").attr("readonly",true);
					$("#buySellCurrLable2").val("");
					$("#buySellCurrLable3").attr("readonly",true);
					$("#buySellCurrLable3").val("");
					$("#totalAmountBought").val("");
					$("#buy_sell_amount").val("");
				<?php if($currencyFeeFlag){?>					
					$("#buy_sell_total_amount").val("");
					//$("#currency_fee").val("");
				<?php }?>
				}
			}
		);*/
	}
<?php if($currencyFeeFlag){?>
	function feeDataFn()
	{
		$.ajax({
		   type: "GET",
		   url: "add-currency-exchange-conf.php",
		   data: "fee=get&currencyF="+$("#operationalCurrency").val()+"&currencyT="+$("#buysellCurrency").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
		   },
		   success: function(msg){
		   var dataMsg = msg.split("|");
			if(dataMsg[0])
			{
				$("#fi").val(dataMsg[0]);
				$("#currency_fee").val(dataMsg[1]);
				$("#currency_fee").attr("readonly",true);
				$("#feeType").val(dataMsg[2]);
				if(dataMsg[2] == 'P')
						$("#currency_fee").val(dataMsg[1]);			
			}
			else
			{
				$("#currency_fee").val("0");
				$("#feeType").val("F");
			}
			calculateCurrencyAmount(2);
		   }
		}); 
	}	
	function feeDataFn2nd()
	{
		$.ajax({
		   type: "GET",
		   url: "add-currency-exchange-conf.php",
		   data: "fee=get&currencyF="+$("#operationalCurrency").val()+"&currencyT="+$("#buysellCurrency").val(),
		   beforeSend: function(XMLHttpRequest){
			//document.getElementById('validate').disabled = true;
		   },
		   success: function(msg){
		   var dataMsg = msg.split("|");
			if(dataMsg[0])
			{
				$("#fi").val(dataMsg[0]);
				$("#currency_fee").val(dataMsg[1]);
				$("#currency_fee").attr("readonly",true);
				$("#feeType").val(dataMsg[2]);
				if(dataMsg[2] == 'P')
						$("#currency_fee").val(dataMsg[1]);			
			}
			else
			{
				$("#currency_fee").val("0");
				$("#feeType").val("F");
			}
		   }
		}); 
	}	
	function calculateFinalAmount()
	{
		var roundDecimal = 4;
		<? 
			if(CONFIG_ROUND_NUMBER_ENABLED_CHEQ_CASH=="1"){
				if(defined("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH") && is_numeric(CONFIG_ROUND_NUMBER_TO_CHEQ_CASH)){
		?>
			roundDecimal = <?=CONFIG_ROUND_NUMBER_TO_CHEQ_CASH?>;
		<?
				}
			}
		?>
		
		var fee = parseFloat($("#currency_fee").val());
		var amount = parseFloat($("#buy_sell_amount").val());
		var feeType = $("#feeType").val();
		
		var buy_sell_total_amount;
		// calculates amoun on the basis of fee type
		if(feeType=="P"){
			buy_sell_total_amount = parseFloat(amount+((fee*amount) / 100));
		}
		else{
			buy_sell_total_amount = parseFloat(amount + fee);
		}

		if(isNaN(buy_sell_total_amount))
			buy_sell_total_amount="";
		else
			buy_sell_total_amount = roundNumber(buy_sell_total_amount,roundDecimal);
		$("#buy_sell_total_amount").val(buy_sell_total_amount);
	}
<?php }?>
function setFocus(focusObj){
	$("#"+focusObj).focus();
}


</script>
<!-- jqGrid CSS Start -->
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="javascript/jqGrid/themes/basic/grid.css" title="basic" media="screen" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="coffee" href="javascript/jqGrid/themes/coffee/grid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<!-- jqGrid CSS End -->
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ font-family:Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.links {
	background-color:#FFFFFF; 
	font-size:10px; 
	text-decoration:none;
}
</style>
</head>
<body onLoad="setFocus('customer_input');">
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<form name="order_form" id="order_form" action="add-currency-exchange-conf.php" method="post">
	<input type="hidden" id="transIds" name="transIds" value="" />
	<table width="90%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
			<td colspan="5" align="center" bgcolor="#DFE6EA">
				<h2><?=$currHeading?></h2>
			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="5" align="center" class="tdDefination" style="text-align:center">
				All fields marked with <font color="red">*</font> are compulsory Fields.
			</td>
		</tr>


<!--
		<tr bgcolor="#ededed">
			<td colspan="4" align="right">
			<a href="javascript:void(0)" id="newCompanyBtn" class="links">[ Add New Company ]</a> 			</td>
		</tr>		
		<tr bgcolor="#ededed">
			<td width="32%" class="tdDefination"><b>Search Company by Name:&nbsp;</b><font color="red">*</font></td>
			<td width="68%" colspan="3" align="left">
				<input type="text" name="company_name" id="company_name" size="30" maxlength="150" />
				<input type="hidden" name="cmpdi" id="cmpdi" value="" />
				&nbsp;
				<a href="javascript:void(0)" id="showHideCompanyDetailBtn" style="display:none; text-decoration:none" title="Show Hide Company Details">[-]</a>			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" id="companyDetails" style="display:none">&nbsp;</td>
		</tr>
 -->
		<tr bgcolor="#ededed">
			<td colspan="5" align="right">
				<a href="javascript:void(0)" id="newCustomerBtn" class="links">[ Add New Sender ]</a>			</td>
		</tr>		
		<tr bgcolor="#ededed">
			<td width="37%" class="tdDefination"><b>Search sender by Name :&nbsp;</b><? if($custCompFlag){?><font color="red">*</font><? }?></td>
			<td colspan="4" align="left">
				<input type="text" name="customer_input" id="customer_input" size="30" maxlength="150" />
				<input type="hidden" name="customerID" id="customerID" value="" />
				&nbsp;
			<a href="javascript:void(0)" id="showHideCustomerDetailBtn" style="display:none; text-decoration:none" title="Show Hide Customer Details">[-]</a>			</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" id="customerDetails" style="display:none">&nbsp;</td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" class="tdDefination" style="padding-right:35px;"><input type="button" id="storeBuyForm" name="storeBuyForm" value="Buy Now" onclick="javascript:document.getElementById('storeBuySellForm').value='B';"/></td>
			<td width="50%" align="left" style="padding-left:35px;"><input type="button" id="storeSellForm" name="storeSellForm" value="Sell Now"  onclick="javascript:document.getElementById('storeBuySellForm').value='S';"/><span style="padding-left:35px;font-weight:bold;">Use System Exchange Rates&nbsp;<input type="checkbox" id="system_rate" onclick="check(this);"  /></span></td>
		</tr>
		<tr  bgcolor="#ededed">
		<table  width="90%" border="0" cellspacing="1" cellpadding="2" align="center" id="showCurrencyDetails" style="display:none;">
		<tr bgcolor="#ededed">
			<td colspan="3" align="left">
				<fieldset>
					<legend>Currency information</legend>
					<table align="left" width="100%">
						<?php 
						if(CONFIG_MANUAL_RATE_USERS_CURR == "1" || strstr(CONFIG_MANUAL_RATE_USERS_CURR, $agentType)){?>
						<tr bgcolor="#ededed" >
							<td width="25%" height="46" class="tdDefination"><b>Buy/Sell Currency :&nbsp;</b><font color="red">*</font></td>
							<td align="left" width="25%">
								<?php if($currencyFeeFlag){?>
									<input type="hidden" name="operationalCurrency" id="operationalCurrency" value="<?php echo $totalCurrencyID;?>" />
								<?php }?>
								<select name="buysellCurrency" id="buysellCurrency" style="width:150px" onchange="if(document.getElementById('system_rate').checked == true){if(this.value!=''){getRates();getCurrAmt(this.value);}<?php if($currencyFeeFlag){?>feeDataFn();<?php }?>}else{if(this.value!=''){getCurrAmt(this.value); feeDataFn2nd();}}" <? if(count($currDataArr)<1) {echo "disabled='disabled'";}?> >
									<option value="" >Select Currency</option>
									<?php
									if(!empty($currDataArr)){
										for($s=0;$s<count($currDataArr);$s++)
										{
											$strCurrencySelectedOption = '';
											if($currDefault == strtoupper($currDataArr[$s]["currencyName"]))
											$strCurrencySelectedOption = 'selected="selected"';
											if($currDataArr[$s]["inter_curr"]>1)
											$countryName = "(INTERNATIONAL)";
											else
											$countryName = "(".strtoupper($currDataArr[$s]["country"]).")";
											?>
											<option value="<?=$currDataArr[$s]["cID"]?>" <?=$strCurrencySelectedOption?>>
												<?=strtoupper($currDataArr[$s]["currencyName"])//." for ".$countryName?>
											</option>
										<?php
										} 
									}else{?>
										<option value="">No Currency Rate Defined</option>
									<?php
									} ?>
								</select>&nbsp;
								<img src="images/info.gif" title="Note about Currency Selection | If you see this select option disabled.Then it means there is no rate defined for any currency." /><br />
							</td>
							<td id="manual_rate" class="tdDefination" colspan="2" align="left" style="display:none"><b>Manual Exchange Rate : <img src="images/info.gif" title="Manual Exchange Rate | If you check 'Yes' then you will be able to use Manual Exchange Rate by inputting values." /></b>
								Yes:<input type="radio" name="manualExchangeRate" id="manualExchangeRate" value="Y">
								No:<input type="radio" name="manualExchangeRate" id="manualExchangeRate1" value="N" checked="checked">
							</td>
						</tr>
						<?php
						}?>
						
						
						
						<?php 
						if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1")
						{?>
							<tr bgcolor="#ededed">
								<td colspan="4" align="center">
									<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="100%" style="margin:auto">
									</table>
									<div id="pager" class="scroll" style="text-align:center;"></div>		
								</td>
							</tr>
						<?php
						}?>
						
						
						
						<tr>
							<td colspan="2" align="center">
								<b><div id="curr_amount"></div><br  />
								<div id="gbp_amount"></div></b>
							</td>
							<td colspan="2"></td>
						</tr>
						<tr bgcolor="#ededed">
							<td width="25%" height="48" class="tdDefination">
								<b>Amount to <label id="buy_sell_label">Buy</label> :</b>&nbsp;<font color="red">*</font>
							</td>
							<td align="left" width="25%">
								<input type="text" name="totalAmountBought" id="totalAmountBought" size="20" maxlength="7" />
								<input name="buySellCurrLable3" id="buySellCurrLable3" type="text" style="border:0px; color:#990033; background-color:#ededed; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$buySellCurrName?>" />
							</td>
							<td width="25%" class="tdDefination"><b>Rate to Buy : </b></td>
							<td align="left" width="25%" style="padding-left:10px;">
								<input style="color:#990000; font-weight:bold;" type="text" name="buy_rate" id="buy_rate" size="10" value="" readonly="" />							
								<input name="buySellCurrLable1" id="buySellCurrLable1" type="text" style="border:0px; color:#990033; background-color:#ededed; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$buySellCurrName?>" />
							</td>
						</tr>						
						<tr bgcolor="#ededed" valign="top">
							<td width="25%" class="tdDefination">
								<b>Local <label id="buy_sell_curr_label">Buying</label> Currency :</b>&nbsp;<font color="red">*</font></td>
							<td align="left" width="25%">
								<input type="text" name="buy_sell_amount" id="buy_sell_amount" size="20" maxlength="150" value="" />
								<input name="totalCurrencyLable" id="totalCurrencyLable" type="text" style="border:0px; color:#339966; background-color:#ededed; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$totalCurrencyName?>" />
								<!-- <font style="color:#339966; font-weight:bold; font-size:14px;">GBP</font> -->
							</td>
							<td width="25%" valign="top" class="tdDefination" style="padding-top:5px;"><b>Rate to Sell :</b></td>
							<td align="left" width="25%" valign="top" style="padding-left:10px;">
								<input style="font-weight:bold;" type="text" name="sell_rate" id="sell_rate" size="10" value="" readonly="" />							
								<input name="buySellCurrLable2" id="buySellCurrLable2" type="text" style="border:0px; color:#990033; background-color:#ededed; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly  value="<?=$buySellCurrName?>" />
							</td>
						</tr>
						<?php 
						if($currencyFeeFlag){?>
						<tr bgcolor="#ededed">
							<td width="25%" height="46" class="tdDefination"><b>Fee:&nbsp;<? if($feeCompFlag){?><font color="red">*</font><? }?>  <img src="images/info.gif" title="Currency Exchange Fee | You can use currency exchange fee after adding from currency exchange fee in currency exchange module." /></b></td>
							<td width="25%" align="left"><input type="text" name="currency_fee" id="currency_fee" size="10" maxlength="7" readonly="readonly" value="<?php echo $fee;?>" />
								<input type="hidden" name="fi" id="fi" value="<?php echo $feeId;?>" />
								<input type="hidden" name="feeType" id="feeType" value="<?php echo $feeType;?>" />
								<?php if($currencyManulFeeFlag){?>
								<span style="padding-left:10px;">
								<a href="javascript:void(0)" id="calculateFeeBtn" class="links">[Predefined Fee]</a> <img src="images/info.gif" title="Note about Predefined Fee | This brings the Predefined fee for selected Currency." />
								|
								<a href="javascript:void(0)" id="manualFeeBtn" class="links">[Manual Fee]</a>
								</span>
								<?php }?>
							</td>
							<td width="25%" class="tdDefination" style="vertical-align:top; padding-top:10px;"><b>Total Local Amount : </b></td>
							<td width="25%" align="left" width="25%" style="padding-left:10px;">
								<input style="font-weight:bold;" type="text" name="buy_sell_total_amount" id="buy_sell_total_amount" value="" size="10" maxlength="150"  readonly="readonly" />							
								<input name="totalCurrencyLable1" id="totalCurrencyLable1" type="text" style="border:0px; color:#339966; background-color:#ededed; font-weight:bold; font-size:14px;text-align:right;" size="3" readonly="readonly"  value="<?=$totalCurrencyName?>" />
							</td>
						</tr>
						<?php
						}?>
						<tr>
							<td align="left" class="tdDefination" colspan="4">
								<?php
								if(CONFIG_CURR_EXCH_STOCK_ACCOUNT=="1"){?>
									<strong><a href="currencyExchangeStock.php" target="_blank">Stock list</a></strong>
								<?php
								}?>
								&nbsp;
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>		
		<tr bgcolor="#ededed">
			<td colspan="5" align="center">
				<input type="submit" id="submitCurrExch" name="submitCurrExch" value=" Process " />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" onclick="ResetValues()" value="Clear Fields" />
				<input type="hidden" id="action" name="action" value="addBuySell" />
				<input type="hidden" id="storeBuySellForm" name="storeBuySellForm" value="B" />
				<input type="hidden" name="co" value="a" />
				<? if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1" && !empty($_SESSION["loggedUserData"]["adminLimitCurrExchange"])) { ?>
					<input type="hidden" name="_li" id="_li" value="<?=$_SESSION["loggedUserData"]["adminLimitCurrExchange"]?>" />
				<? } ?>

			</td>
		</tr>
		</table>
		</tr>
	</table>
</form>
</body>
</html>
