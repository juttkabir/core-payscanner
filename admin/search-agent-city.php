<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$locationString = "Location ID";
if(defined("CONFIG_COLLECTION_LOCATION_ID_TITLE") && CONFIG_COLLECTION_LOCATION_ID_TITLE!="0"){
	$locationString = CONFIG_COLLECTION_LOCATION_ID_TITLE;
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}
	
if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

$agentCity = ($_GET["agentCity"] != "" ? $_GET["agentCity"] : $_POST["agentName"]);
$qryString .= ($_GET["agentCity"] != "" ? "&agentCity=" . $_GET["agentCity"] : "&agentCity=" . $_POST["agentName"]);

$agentCountry = ($_GET["benCountry"] != "" ? $_GET["benCountry"] : $_POST["benCountry"]);
$qryString .= ($_GET["benCountry"] != "" ? "&benCountry=" . $_GET["benCountry"] : "&benCountry=" . $_POST["benCountry"]."&r=".$_GET["r"]);

$qryString .= "&transID=". $_GET["transID"];

$query = "select * from cm_collection_point as c, admin a ";///changed Wild Card
$queryCnt = "select count(*) from cm_collection_point as c, admin a ";///changed Wild Card



if(CONFIG_CP_SEARCH_VIA_COUNTRY =='1'){
	$query .= " where c.cp_ida_id = a.userID And (cp_city  like '$agentCity%' OR cp_country like '$agentCity%')" ;
	$queryCnt .= " where c.cp_ida_id = a.userID And (cp_city   like '$agentCity%' OR cp_country like '$agentCity%')";
}else{
	$query .= " where c.cp_ida_id = a.userID And cp_city  like '$agentCity%'" ;
	$queryCnt .= " where  c.cp_ida_id = a.userID And cp_city  like '$agentCity%'";
}


$query .= "  and cp_country like '$agentCountry' and cp_active='Active'";
$queryCnt .= " and cp_country like '$agentCountry' and cp_active='Active'";

	/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
	if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
		&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
	{
		if ( strstr($agentType, "SUPA") || strstr($agentType, "SUBA") )
		{
			// Limit the collection points to associated distributors if an agent is accessing this page.
			$userID = $_SESSION["loggedUserData"]["userID"];
			if ( !empty( $userID ) )
			{
				
				// Select associated distributors
				$assocDistQuery = "select associated_distributors from admin where userID='" . $userID . "'";
				$userData = selectFrom( $assocDistQuery );

				if ( !empty($userData["associated_distributors"]) )
				{
					// If it is not empty, it means that there are some distributors associated with this agent.
					// So, only those collection points should be shown which are related to those associated distributors.
					// If $userData["associated_distrbutors"] is found to be empty, it means that there is no particular assoication of this
					// agent with any distributor. All the collection will be shown in this case as it will be treated as the current agent is associated with
					// all distributors.
					$assocDistClause = "";
					$assocDistIDs = explode( ",", $userData["associated_distributors"] );
					foreach( $assocDistIDs as $distID )
					{
						$assocDistClause .= "'" . $distID . "', ";
					}

					$assocDistClause = substr( $assocDistClause, 0, strlen( $assocDistClause ) - 2 );
					$assocDistClause = " and cp_ida_id in(" . $assocDistClause . ") ";
					$query .= $assocDistClause;
					$queryCnt .= $assocDistClause;
				}
				
			}
			
		}
	}
	/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/

//$query .= "  and a.agentStatus = 'Active'";
//$queryCnt .= " and a.agentStatus = 'Active'";

//echo $query;
//$contentsAgent = selectMultiRecords($query);

//$allCount = count($contentsAgent);
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
$query .= " and serviceType ='".$_REQUEST["serviceType"]."' ";
$queryCnt .= " and serviceType ='".$_REQUEST["serviceType"]."'";


}
//debug($query );
$allCount = countRecords($queryCnt);
$query .= " LIMIT $offset , $limit";

$contentsAgent = selectMultiRecords($query);
//echo count($contentsAgent) ;

//echo $query .  count($contentsTrans);
?>
<html>
<head>
	<title>Search Collection point</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}

function closeWin() {
	window.close();	
}

-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Search Results 
      Collection Points </font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
	  <? if ($_GET["msg"] == "Y")
	  {
	  ?>
      <table width="650" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
        <tr>
          <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
          <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; ?></td>
        </tr>
      </table>
	  <?
	  }
	  ?>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
   <? if(count($contentsAgent) > 0) { ?>
	    <tr>
            <td height="25" nowrap bgcolor="#333333">
<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
            <tr>
              <td><?php if (count($contentsAgent) > 0) {;?>
      Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAgent));?></b> of
      <?=$allCount; ?>
      <?php } ;?>
              </td>
              <?php if ($prv >= 0) { ?>
              <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a> </td>
              <?php } ?>
              <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
              <?php } ?>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="760" border="0" bordercolor="#EEEEEE">
        <tr bgcolor="#FFFFFF">
			  <td width="136" bgcolor="#FFFFFF"><span class="style1">Bank Name </span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1"><?=$locationString?></span></td>
			  <? if(CONFIG_DIST_ON_COLLECTION_POINT_SERACH == "1") { ?>
			  <td width="136" bgcolor="#FFFFFF"><span class="style1">Distributor Alias</span></td>
			  <? 
			  
			  if($agentType != "SUPA" && $agentType != "SUBA"){ ?>
			  <td width="136" bgcolor="#FFFFFF"><span class="style1">Distributor </span></td>
			  <? }
			  
			  } ?>


			  <td width="136" bgcolor="#FFFFFF"><span class="style1">Collection Point Name </span></td>
			  <td width="250"><span class="style1">Branch Address </span></td>
			  <td width="77"><span class="style1">City </span></td>
			  <td width="82"><span class="style1">Country </span></td>
			  <td width="85" align="center"><span class="style1">Status</span></td>
			  <td width="81" align="center">&nbsp;</td>
			  
		  </tr>
		    <? for($i=0;$i < count($contentsAgent);$i++)
			{
				?>
				<tr bgcolor="#FFFFFF">
				  <td width="136" bgcolor="#FFFFFF"><? echo ucfirst($contentsAgent[$i]["cp_corresspondent_name"])?></td>
				  <td width="100"><?=$contentsAgent[$i]["cp_branch_no"]?></td>	  
				  <?   if(CONFIG_DIST_ON_COLLECTION_POINT_SERACH == "1") {
				  $distibutorData = selectFrom("select name,distAlias from admin where userID = '" .$contentsAgent[$i]["cp_ida_id"]."'"); ?>
				  <td width="136" bgcolor="#FFFFFF"><? echo ucfirst($distibutorData["distAlias"])?></td>
				  <? if($agentType != "SUPA" && $agentType != "SUBA"){ ?>
				  <td width="136" bgcolor="#FFFFFF"><? echo ucfirst($distibutorData["name"])?></td>
				  <? }} ?>


				  <td width="250"><? echo  $contentsAgent[$i]["cp_branch_name"]?></td>
				  <td width="250"><? echo  $contentsAgent[$i]["cp_branch_address"]?></td>
				  <td width="77"><? echo  $contentsAgent[$i]["cp_city"]?></td>
				  <td width="82"><? echo  $contentsAgent[$i]["cp_country"]?></td>
				  <td align="center"><? if($contentsAgent[$i]["cp_active"] != "Active") { ?><font color='#990000'><? } else {?><font color='#000000'><?}?><? echo ucfirst($contentsAgent[$i]["cp_active"])?></font></td>
             	
				<? if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){ ?>
				 <td align="center"><? if($contentsAgent[$i]["agentStatus"]== "Active"){ if($contentsAgent[$i]["cp_active"] == "Active") { ?><a href="#" onClick="javascript:opener.document.location = '<? echo $_GET["from"]?>?from=popUp&agentCity=<? echo $_GET["agentCity"]?>&benAgentID=<? echo $contentsAgent[$i]["cp_ida_id"]?>&transID=<? echo $_GET["transID"]?>&benID=<?=$_GET["benID"]?>&serviceType=<? echo $_GET["serviceType"]?>&customerID=<? echo $_GET["customerID"]?>&collectionPointID=<? echo $contentsAgent[$i]["cp_id"]?>'; closeWin();" class="style3">Select</a><? } }else{ ?><font readonly>Select<i>(distributor is Disabled)</i></font><? } ?></td>
				  <? }else{ ?>
				   <td align="center"><? if($contentsAgent[$i]["agentStatus"]== "Active"){ if($contentsAgent[$i]["cp_active"] == "Active") { ?><a href="#" onClick="javascript:opener.document.location = '<?=$returnPage?>?benAgentID=<? echo $contentsAgent[$i]["cp_id"]?>&transID=<? echo $_GET["transID"]?>&exchangeRate=<? echo $_GET["exchangeRate"]?>&calculateBy=<? echo $_GET["calculateBy"]?>&collectionPointID=<? echo $contentsAgent[$i]["cp_id"]?>&focusID=exchangeRate'; closeWin();" class="style3">Select</a><? } }else{ ?><font readonly>Select<i>(distributor is Disabled)</i></font><? } ?></td> 
				   <? } ?>
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="283" align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			   <td align="center">&nbsp;</td>
			    <td align="center">&nbsp;</td>
			     <td align="center">&nbsp;</td>
			    <td align="center">&nbsp;</td>
			    
			  </tr>
			<?
		} else {
			?>
			<tr bgcolor="#FFFFFF">
			    <td align="center">There is no collection point with this name. Please try again</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td>&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
			    <td align="center"><input type="button" value="Close Window" onClick="closeWin();"></td>
			</tr>
			<? } ?>
          </table></td>
        </tr>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>