<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();


if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}
//Beneficiary Country
//exRate
//$customerContent["Country"]
//$_SESSION["currencyTo"]
// $dist,
// $dDate,for Back Dated 
//$_SESSION["currencyFrom"]
//,$_SESSION["transType"]
//fee
//CalculateAmount From

//return Values
//exrate
//transAmount
//localAmount
//fee
//totalAmount
//moneyPaid

$benCountry = $_GET['benCountry'];
$custCountry = $_GET['custCountry'];
$exRate = $_GET['exRate'];
$currencyTo = $_GET['currencyTo'];
$dist = $_GET['dist'];
$dDate = "";
$currencyFrom = $_GET['currencyFrom'];
$transType = $_GET['transType'];
$fee = $_GET['fee'];
$transAmount = $_GET['amount'];
$amountFlag = $_GET['amountFlag'];
$localAmount = $_GET['amountLocal'];
$totalAmount = $_GET['amountTotal'];
$moneyPaid = $_GET['moneyPaid'];
$customerAgentID = $_GET['customerAgentID'];
$bankCharges = $_GET['bankChargesID'];
$discount =  $_GET['discountID']; 
$inclusive = $_GET['inclusive'];
$agentOwnRate = $_GET['agentOwnRate']; //
$bankingType = $_GET['bankingType']; //
$manualCommissionAjaxV = $_GET['manualCommissionAJAX'];
$customerID = $_GET['customerID'];
function currencyValue2($currencyTo,$currencyFrom)
{
	$compareCurrency = false;
	if(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_TO')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $currencyTo.",")|| CONFIG_TRANS_ROUND_CURRENCY == " ")
			$compareCurrency = true;	
	}elseif(CONFIG_TRANS_ROUND_CURRENCY_TYPE == 'CURRENCY_FROM')
	{
		if(strstr(CONFIG_TRANS_ROUND_CURRENCY, $currencyFrom.",")|| CONFIG_TRANS_ROUND_CURRENCY == " ")
			$compareCurrency = true;	
	}else
	{
		if((strstr(CONFIG_TRANS_ROUND_CURRENCY, $currencyTo.",")) || (strstr(CONFIG_TRANS_ROUND_CURRENCY, $currencyFrom.",")))
			$compareCurrency = true;	
	}
	return $compareCurrency;	
}


function RoundVal2($val){
	
	if(CONFIG_TRANS_ROUND_LEVEL != "")
	{
		$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
	}else{
		$roundLevel = 4;
		}
		
		
	$compareCurrency2 = currencyValue2($currencyTo,$currencyFrom);
	
    if((strstr(CONFIG_ROUND_NUM_FOR, $moneyPaid.",")) && $compareCurrency2){
        if(CONFIG_TRANS_ROUND_NUMBER == 1){
      	$ArrVal = explode(".",$val);
       	$return=$ArrVal[0];
    }else{
    	
        $return=round($val,$roundLevel);
        }
    }else{
    	
      	$return=round($val,$roundLevel);        
    }
return $return;	
}


	


if($inclusive != 'Yes')
{
 	if ((CONFIG_MANUAL_FEE_RATE == "1" && strstr(CONFIG_BEN_COUNTRY, strtoupper($benCountry))) || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $agentOwnRate == 'Y'))
 	{
 			$exID = 0;
 			if ($exRate > 0)
 			{
 				$_SESSION["exchangeRate"] = $exRate;
 			}
			else{
				$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $dist,$transAmount , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
				$exID 		  = $exchangeData[0];
				$exRate 	  = $exchangeData[1];	
				$currencyFrom = $exchangeData[2];
				$currencyTo   = $exchangeData[3];
			}
 	}else if(CONFIG_MULTI_RATE_PATTERN == '1')
 	{
 		$exID = 0;
 		$exRate = getMultiPatternRate($custCountry, $benCountry, $currencyTo, $dist, 0 , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
 		$currencyFrom = $currencyFrom;
		$currencyTo 	= $currencyTo;
 	}else{
 		$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $dist, 0 , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
	
	$exID 		  = $exchangeData[0];
	$exRate 	  = $exchangeData[1];	
	$currencyFrom = $exchangeData[2];
	$currencyTo   = $exchangeData[3];
	
	//$_SESSION["currencyFrom"] = $currencyFrom;
}
		
if($amountFlag == "trans")
{
	if($transAmount != "")
	{
	 $localAmount = $transAmount * $exRate;
	 $compareCurrency2 = currencyValue2($currencyTo,$currencyFrom);
	 	if(CONFIG_ROUND_NUMBER_ENABLED == '1' && strstr(CONFIG_ROUND_NUM_FOR, $moneyPaid.",") && $compareCurrency2)
		{
			$localAmount = round($localAmount);		
		}
		
	// $_SESSION["localAmount"] = $localAmount;
	}
	
	////Following Block is for Batch Transactions
	
	/*if ($_POST["t_amount"] != "")
	{
		$_SESSION["amount_transactions"] = $_POST["t_amount"];
	}
	if ($_POST["l_amount"] != "")
	{
		$_SESSION["amount_left"] = $_POST["l_amount"];
	}*/
}

/******
	#5211 - Minas Center
			CONFIG_TRANS_ROUND_NUMBER is parent.
			if CONFIG_ROUND_NUM_FOR is defined like define("CONFIG_ROUND_NUM_FOR", "By Bank Transfer,By Cash,By Cheque,")
			then control should not enter into this block as disturbs trans amount and converts back to decimals.
			This work is done because it came from OPAL (there define("CONFIG_ROUND_NUM_FOR", "inclusive, exclusive,")) from ticket #1772
			by Aslam Shahid
******/
$roundNumForFlag = true;
if(CONFIG_BY_CASH_ROUND_NUM_FOR=="1"){
	$roundNumForFlag = false;
}

$compareCurrency2 = currencyValue2($currencyTo,$currencyFrom);///it is used for #1772 calculations
if($amountFlag == "local" || (CONFIG_TRANS_ROUND_NUMBER == '1' && $moneyPaid == 'By Cash' && $compareCurrency2 && $roundNumForFlag))
{				
	
	if($localAmount != "")
	{
		//////This condition is based on opal requirement to calculate amount on rounded value based on cash payment
		if(CONFIG_TRANS_ROUND_NUMBER == '1' && $moneyPaid == 'By Cash' && $compareCurrency2)
  		{
  			$localAmount= round($localAmount);
  		}
  		////////////////////#1772 Ticket
  		
  			$transAmount = $localAmount / $exRate;
  		
	
		//$_SESSION["transAmount"] = $transAmount;
	}
	/*
		Block for Batch transaction to be done
	if ($_POST["t_amount"] != "")
	{
		$_SESSION["amount_transactions"] = $_POST["t_amount"];
	}
	if ($_POST["l_amount"] != "")
	{
		$_SESSION["amount_left"] = $_POST["l_amount"];
	}*/
}
	
	$imFee = 0;
	if ($discount > 0){ 
	$imFee = $discount;
	}elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 ){
		$imFee = 0 ;
			 //discount_request
		}elseif(CONFIG_MANUAL_FEE_RATE == "1" && strstr(CONFIG_BEN_COUNTRY, strtoupper($benCountry)))
	{
		if ($fee != "")
 		{
 			$imFee	= $fee;
 			
 		}
	}
	else
	{
		if(CONFIG_PAYIN_CUSTOMER != '1')
		{
			if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
			{
				$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $transType, $currencyFrom, $currencyTo);	
				
			}
			if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
			{
				$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $dist, $transType, $currencyFrom, $currencyTo);	
			
			}
			if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
			{
			 $imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $customerAgentID, $transType, $currencyFrom, $currencyTo);	///
			
			}
			if($imFee <= 0)
			{
				$imFee = imFee($transAmount,  $custCountry, $benCountry, $transType, $currencyFrom, $currencyTo,$_SESSION["collectionPointID"],$dist);	
				
			}
				
		}else
		{	
			if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
			{
				if($customerContent["payinBook"] != "" )
					$imFee = imFeeAgentPayin($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $transType, $currencyFrom, $currencyTo);	
				else
					$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $transType, $currencyFrom, $currencyTo);	
				
			}
			if(CONFIG_FEE_DISTRIBUTOR == 1 && $imFee <= 0)
			{
				if($customerContent["payinBook"] != "" )
					$imFee = imFeeAgentPayin($transAmount,  $custCountry, $benCountry, $dist, $transType, $currencyFrom, $currencyTo);	
				else
			 		$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $dist, $transType, $currencyFrom, $currencyTo);	
			 		
			}
			if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1) && $imFee <= 0)
			{
				if($customerContent["payinBook"] != "" )
					$imFee = imFeeAgentPayin($transAmount,  $custCountry, $benCountry, $customerAgentID, $transType, $currencyFrom, $currencyTo);	
				else
			 		$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $customerAgentID, $transType, $currencyFrom, $currencyTo);	
			 		
		 	}
			if($imFee == 0 && $manualCommissionAjaxV!="Y")
			{		//echo("In the condition");
				if($customerContent["payinBook"] != "" )
					$imFee = imFeePayin($transAmount,  $custCountry, $benCountry, $transType, $currencyFrom, $currencyTo);	
				else
			 		$imFee = imFee($transAmount,  $custCountry, $benCountry, $transType, $currencyFrom, $currencyTo,$_SESSION["collectionPointID"],$dist,$customerID);
		 	}
			else{
			
			//code by anjum
			if((CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)){
				$imFee =$imFee;
			}else{
				$imFee = $fee;
			}
			//code by anjum
			
			
			}
			
			
  	}
	}			
//echo("at line 2157 transID  ");
//	echo("Distributor is $dist and Fee is $imFee");
	 $totalAmount =  $transAmount;
	/* $localAmount = $transAmount * $exRate;
		if(CONFIG_ROUND_NUMBER_ENABLED == '1')
		{
			$localAmount = round($localAmount);		
		}
		*/
	
 	$imFeetemp = ($imFee);
	
	if(CONFIG_CURRENCY_CHARGES == '1')
	{
		$local = "No";
		//$_SESSION["currencyCharge"]="";
		$strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$currencyTo.",%'";
		$currencies = selectMultiRecords($strCurrency);
		for ($c=0; $c < count($currencies); $c++)
		{
			if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benCountry))
			$local = "Yes";
		}
		
		if($local=="No")
		{
			///////Out Currency Charges are working on Per 100 Amount
			$tempCharge = $transAmount/100.01;
			$tempCharge2 = (int)$tempCharge;
			$tempCharge2 = $tempCharge2 + 1;
			$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
			$currencyCharge = $tempCharge2; 
			$totalAmount = $totalAmount + $currencyCharge;
		}
		
	}
	
	if($moneyPaid == 'By Cash')
	{
	
			$totalAmount = ( $totalAmount +  $imFee);
			$imFeetemp = ($imFee);		
			if(CONFIG_CASH_PAID_CHARGES_ENABLED)
			{
				$totalAmount = ( $totalAmount +  CONFIG_CASH_PAID_CHARGES);
				}			
							
	}
	elseif($moneyPaid == 'By Cheque')
	{
		$totalAmount = ($totalAmount +  $imFee);
		$imFeetemp = ($imFee);
				
	}
	else//if($_SESSION["moneyPaid"] == 'By Bank Transfer')
	{		 
  
		$totalAmount = ($totalAmount + $imFee);
		$imFeetemp = ($imFee);
	}
	if($transType=="Bank Transfer")
	{		 
		if(CONFIG_CUSTOM_BANK_CHARGE == '1')
		{
			if(CONFIG_CUSTOM_BANK_CHARGE_CURRENCY == 'FROM')
			{
				$bankChargeAmount = $transAmount;	
			}else{
				$bankChargeAmount = $localAmount;	
				}
			$bankCharges = getBankCharges($custCountry, $benCountry, $currencyFrom, $currencyTo, $bankChargeAmount, $dist);
		}
		
		$totalAmount = ( $totalAmount + $bankCharges);
		
	}
	if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
		  /**
		   * Override the rounding through the function
		   * Updating as mentioned by Ticket# 3268
		   */
		  //echo ($transAmount > 0 ? RoundVal($transAmount) : "");
		  /**
		   * New Statment
		   */
		  $transAmount = ($transAmount > 0 ? round($transAmount,0) : "");
	  }else{
		  $transAmount = ($transAmount > 0 ? round($transAmount, 2) : "");
		  
	  }
	  if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM'){
		  $localAmount = ($localAmount > 0 ? round($localAmount,0) : "");
	  }
	  else{
		  $localAmount = ($localAmount > 0 ? round($localAmount, 0) : "");
	  }
	
 /* if($transAmount != "" && $_SESSION["chDiscount"]=="")
  {
  	///!$imFee && 
  	if(CONFIG_ZERO_FEE != '1' && $imFee <= 0)
	{
  ?>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
          <tr>
            <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
            <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".TE2."</b</font>"; ?></td>
          </tr>
        </table>                    
        <?
    }
  }*/


}

/******************
This Portion is for INCLUSIVE OF FEE
******************/
/**
	#5047
	This code (below else structure) added for Ajax based calculation of amount in case of Inclusive of Fee.
	It will automatically fetch Exchange rate, Fee either manual or not.
**/

else{
 
	
	$msgWrongTotal = "";
	/////Condition OF Calculation from Total Starts Here///////////
	$totalAmountTemp = $totalAmount;
	/////////////////Bank Charges///////////////
	
	if($totalAmount > 0)
	{
	
		if($transType=="Bank Transfer")
		{
			if(CONFIG_CUSTOM_BANK_CHARGE == '1')
				{
					$bankCharges = getBankCharges($custCountry, $benCountry, $currencyFrom, $currencyTo, $totalAmountTemp, $dist);
				}
					$totalAmountTemp = ( $totalAmountTemp - $bankCharges);
		}
			
		/////////////////////////////Cash Charges
		if($moneyPaid == 'By Cash')
		{
	 		if(CONFIG_CASH_PAID_CHARGES_ENABLED == "1")
			{
				$totalAmountTemp = ( $totalAmountTemp -  CONFIG_CASH_PAID_CHARGES);
			}			
								
		}
		
		////////////////////////////Outer Currency Charges////////////
		
		if(CONFIG_CURRENCY_CHARGES == '1')
		{
			$local = "No";
			 $strCurrency = "Select * from ".TBL_COUNTRY." where  currency like '%".$currencyTo.",%'";
			$currencies = selectMultiRecords($strCurrency);
			for ($c=0; $c < count($currencies); $c++)
			{
				if(strtoupper($currencies[$c]["countryName"]) == strtoupper($benCountry))
				$local = "Yes";
			}
			
			if($local=="No")
			{
				///////Out Currency Charges are working on Per 100 Amount
				$tempCharge = $totalAmountTemp/100.01;
				$tempCharge2 = (int)$tempCharge;
				$tempCharge2 = $tempCharge2 + 1;
				$tempCharge2 = $tempCharge2 * $countryContent["outCurrCharges"];
				$currencyCharge = $tempCharge2; 
				$totalAmountTemp = $totalAmountTemp - $currencyCharge;
			}
			
		}
		///////////////////Calculation of Fee//////////////////
/*			if ($discount != ""){ 
					$imFee = $discount;
				}elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0){
					$imFee = 0 ;
						 //discount_request
					}
					elseif(CONFIG_MANUAL_FEE_RATE == "1" && strstr(CONFIG_BEN_COUNTRY, strtoupper($benCountry)))
					{
						
						if ($fee != "")
				 		{
				 			$imFee	= $fee;
				 		}
					}
*/
		///////////////////Calculation of Fee Starts for $fromTotalConfig == "1" //////////////////
		/*********
			#5047 NT
			These lines of code should be brought here form add-transaction.php fee logic for $fromTotalConfig=="1"..
		*********/
					   $bolExecuteIMFeeCondition = false;
					   if(CONFIG_MANUAL_FEE_RATE == "1" && strstr(CONFIG_BEN_COUNTRY, strtoupper($benCountry)))
					   {
						 //@4812 faith exchange zero commission
							if(CONFIG_ZERO_FEE == "1"){
								$bolExecuteIMFeeCondition = true;
							}
							else
							{
								if(!empty($fee)){
									$bolExecuteIMFeeCondition = true;
								}
							}
					   }
					   
					   
						if(CONFIG_FEE_BASED_TRANSTYPE == "1" && defined("CONFIG_COMMISSION_CALCULATED_ON"))
						{
							$amountType=$_SESSION[CONFIG_COMMISSION_CALCULATED_ON];
							$feetransType=$transType;
												
						}
						else
						{
							$amountType=$transAmount;
							$feetransType=$transType;
						}  
					   
						$imFee = 0;
						if ($discount!="")
						{ 
							$imFee = $discount;
						}
						elseif(CONFIG_TOTAL_FEE_DISCOUNT == '1' && (!strstr(CONFIG_DISCOUNT_EXCEPT_USER, $agentType)) && $_SESSION["discount_request"] > 0 )
						{
							$imFee = 0 ;
							 //discount_request
						}
						//elseif(CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1" && !empty($fee))
						elseif($bolExecuteIMFeeCondition)
						{
							if(empty($_REQUEST["transID"]) && $manualCommissionAjaxV!="Y")
							{
								$fee = "";
								
							}
							
							if($fee != "")
							{
								$imFee	= $fee;
							}
							else
							{
								if(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1){
									$imFee = imFeeAgent($amountType,  $custCountry, $benCountry, $customerAgentID, $feetransType, $currencyFrom, $currencyTo);	
								}
								 
								if($imFee <= 0){
									$imFee = imFee($amountType, $custCountry, $benCountry,$feetransType, $currencyFrom, $currencyTo,$_SESSION["collectionPointID"],$dist);	
								}
							}
							
						}
						else
						{
							if(CONFIG_PAYIN_CUSTOMER != '1')
							{
								if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
								{
									$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $feetransType, $currencyFrom, $currencyTo);	
									
								}
								
								if(CONFIG_FEE_DISTRIBUTOR == 1)
								{
									$imFee = imFeeAgent($amountType,  $custCountry, $benCountry, $dist, $feetransType, $currencyFrom, $currencyTo);	
									
								}elseif(CONFIG_FEE_AGENT_DENOMINATOR == 1 || CONFIG_FEE_AGENT == 1)
								{
								 $imFee = imFeeAgent($amountType,  $custCountry, $benCountry, $customerAgentID, $feetransType, $currencyFrom, $currencyTo);	
								 
								}
								
								if($imFee <= 0)
								{ 
									
									
									$imFee = imFee($amountType, $custCountry, $benCountry,$feetransType, $currencyFrom, $currencyTo,$_SESSION["collectionPointID"],$dist);	
									
								}
									
							}else
							{	
								if(CONFIG_FEE_BASED_COLLECTION_POINT == 1 && !empty($_SESSION["collectionPointID"]))
								{
									if($customerContent["payinBook"] != "" )
										$imFee = imFeeAgentPayin($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $feetransType, $currencyFrom, $currencyTo);	
									else
										$imFee = imFeeAgent($transAmount,  $custCountry, $benCountry, $_SESSION["collectionPointID"], $feetransType, $currencyFrom, $currencyTo);	
								}
								
								if(CONFIG_BACKDATED_FEE == '1'){
									$feeQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID='".$_GET["transID"]."'");
									if($_GET["transID"] != "" && $transAmount == $feeQuery["transAmount"]){
										$imFee = $feeQuery["IMFee"];
									}
								}elseif(CONFIG_FEE_DISTRIBUTOR == '1')
								{
									
									
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeeAgentPayin($amountType,  $custCountry, $benCountry, $dist,$feetransType, $currencyFrom, $currencyTo);	
									
									}else{
										$imFee = imFeeAgent($amountType,  $custCountry, $benCountry, $dist, $feetransType, $currencyFrom, $currencyTo);	
									
									}
											 
								}elseif(CONFIG_FEE_AGENT_DENOMINATOR == '1' || CONFIG_FEE_AGENT == '1')
								{
									
									
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeeAgentPayin($amountType,  $custCountry, $benCountry, $customerAgentID, $feetransType, $currencyFrom, $currencyTo);	
									
									}
									else{ 
										$imFee = imFeeAgent($amountType,  $custCountry, $benCountry, $customerAgentID, $feetransType, $currencyFrom, $currencyTo);	
									
									}
							
								}
								if($imFee == 0)
								{	
														
									if($customerContent["payinBook"] != "" )
									{
										$imFee = imFeePayin($amountType,  $custCountry, $benCountry, $feetransType, $currencyFrom, $currencyTo);	
										
									}
									else
									{
										$imFee = imFee($amountType,  $custCountry, $benCountry, $feetransType, $currencyFrom, $currencyTo,$_SESSION["collectionPointID"],$dist);
										
									}
								}
						
						}
					 }

		///////////////////Calculation of Fee Ends for $fromTotalConfig == "1" //////////////////

					if($customerContent["payinBook"] != "" )
					{
						$payinBook = true;
					}else{
						$payinBook = false;
						}
			
				$FeeData = CalculateFromTotal($imFee, $totalAmountTemp , $custCountry, $benCountry, $dist, $customerAgentID,$payinBook, $transType, $currencyFrom, $currencyTo);
					
				 $transAmount = $FeeData[0];
				 $imFee = $FeeData[1];
				
				/*
				if ($transAmount != "" && $imFee != "") {
				 	$validTranAmount = selectFrom("select amountRangeFrom from " . TBL_IMFEE . " where Fee = '".$imFee."' and destCountry='".$benCountry."' and origCountry='".$custCountry."'");
				 	if ($transAmount < $validTranAmount["amountRangeFrom"]) {
				 		unset($transAmount);
				 		unset($imFee);
				 		$msgWrongTotal = "The Total Amount you entered is invalid. Please consult the fee table.";
				 	}
				}
				*/
				//echo("at line 2334  values are ".$transAmount."  and  ".$transAmount);
				//$transAmount = $transAmount;
				//echo("at line 2336  values are ".$transAmount."  and  ".$transAmount);
				
				///////////////////////Calculation of Exchange Rates//////////
		/*********
			#5047 NT
			These lines of code should be brought here form add-transaction.php exchange rate logic for $fromTotalConfig=="1".
		*********/
				if(CONFIG_EXCH_RATE_NOT_EDIT == '1' && $_GET["transID"] != "")
				{
					
					$exRateUsed = selectFrom("select rateUsedID, rateWithMargin, currencyFrom , currencyTo from ".TBL_EXCH_RATE_USED." where transID  = '".$_GET["transID"]."'");
					
					$exRate = $exRateUsed["rateWithMargin"];
					$currencyFrom = $exRateUsed["currencyFrom"];
				$currencyTo 	= $exRateUsed["currencyTo"];
					
				}elseif ((CONFIG_MANUAL_FEE_RATE == "1" && strstr(CONFIG_BEN_COUNTRY, strtoupper($benCountry)))  || (CONFIG_AGENT_OWN_MANUAL_RATE == '1' && $manualCommissionAjaxV == 'Y'))
			 	{
						$bolGetRate = true;
			 			$exID = 0;
			 			//debug($_REQUEST);
						if(defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE") && CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
						{
							if($manualCommissionAjaxV=="Y"){
								$bolGetRate = false;
							}
							else//if(empty($_REQUEST["enterOwnRate"]) || $_REQUEST["enterOwnRate"]=="N")
							{
								$bolGetRate = true;
							}
								
							if(!empty($_GET["transID"]))
							{
								if($agentOwnRate=="Y")
									$bolGetRate = false;
								else
									$bolGetRate = true;
							}
						}
				
			 			if ($exRate	 != "" && $exRate	 != 0)
			 			{
							$exRate	= $exRate;
			 			}
			 			else
			 			{
			 				//$exRate = 0;
							if(CONFIG_24HR_BANKING == "1" && $bankingType == "bank24hr")
							{
		 	
					 			$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $bankingType, 0 , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
								$exID 			  = $exchangeData[0];
								$exRate 		  = $exchangeData[1];	
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];	
						 		$currencyFrom = $currencyFrom;
						 	}
							elseif($bolGetRate)
							{	
				 				$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $dist, $transAmount , $dDate, $currencyFrom,$transType,$moneyPaid,$customerAgentID);
								
								$exID 			  = $exchangeData[0];
								
								
								/* Check the denomination based exchange rate */
								$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
								if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
								{
									if($arrExchangeRate["isDenomination"] == "Y")
									{
										$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$transAmount."' and amountTo >= '".$transAmount."'");
										if(is_array($arrDenominationBaseRate))
										{
											$exRate = $arrDenominationBaseRate["exchangeRate"];
										}
										else
										{
											/* error .. not define proper exchange rate */
										}
										
									}
									else	
									{
										$exRate = $exchangeData[1];	
									}	
								}
								else
								{
									$exRate  = $exchangeData[1];	
								}
								
								/**
								 * Get the exchange rate DB value only in case if the session value is empty
								 * @Ticket# 3524
								 */
								if(!defined("CONFIG_DENOMINATION_BASED_EXCHANGE_RATE")) 
									if(empty($exRate))
										$exRate = $exchangeData[1];	
								/* End of # 3524 */
									
								$currencyFrom = $exchangeData[2];
								$currencyTo 	= $exchangeData[3];
								
								$currencyFrom = $currencyFrom;
			 				}
			 				
			 			}
			 	}elseif(CONFIG_MULTI_RATE_PATTERN == '1')
			 	{
			 		$exID = 0;
			 		$exRate = getMultiPatternRate($custCountry, $benCountry, $currencyTo, $dist, 0, $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID, 'Yes');
			 		$currencyFrom = $currencyFrom;
					$currencyTo 	= $currencyTo;
			 	}elseif(CONFIG_24HR_BANKING == "1" && $bankingType == "bank24hr"){
		 	
		 			$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $bankingType, 0 , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
					$exID 			  = $exchangeData[0];
					$exRate 		  = $exchangeData[1];	
					$currencyFrom = $exchangeData[2];
					$currencyTo 	= $exchangeData[3];	
			
			 	}else{
			 	
			
			 		$exchangeData = getMultipleExchangeRates($custCountry, $benCountry,$currencyTo, $dist, $transAmount , $dDate, $currencyFrom,$transType,$moneyPaid, $customerAgentID);
				
				$exID 	 = $exchangeData[0];
				//$currencyFrom = $currencyFrom;
				$currencyFrom = $exchangeData[2];
				$currencyTo 	= $exchangeData[3];

				$arrExchangeRate = selectFrom("select * from exchangerate where erID='".$exID."'");			
				
				//debug($arrExchangeRate);

								
				if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1" && $arrExchangeRate["isDenomination"] == "Y")
				{
								
					if($arrExchangeRate["isDenomination"] == "Y")
					{
						$arrDenominationBaseRate = selectFrom("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$transAmount."' and amountTo >= '".$transAmount."'");
						
						//debug("select * from denominationBasedRate where parentRateId='".$arrExchangeRate["erID"]."' and amountFrom <= '".$transAmount."' and amountTo >= '".$transAmount."'");
						
						//debug($arrDenominationBaseRate);
						
						if(is_array($arrDenominationBaseRate))
						{
							$exRate = $arrDenominationBaseRate["exchangeRate"];
						}
						else
						{
							/* error .. not define proper exchange rate */
						}
						
					}
					else	
					{
						$exRate = $exchangeData[1];	
					}	
				}
				else
				{
					$exRate  = $exchangeData[1];	
				}
				$currencyFrom = $currencyFrom;
			}
				///////////////////	Calculating Local Amount
				if($transAmount != "")
				{
				 $localAmount = $transAmount * $exRate;
				 
				 	if(CONFIG_ROUND_NUMBER_ENABLED == '1')
					{
						$localAmount = round($localAmount);		
					}
				}
				if ($msgWrongTotal != "") {
					$localAmount = "";
					$_SESSION["localAmount"] = "";
				}
			}
		
}
//$amounts = $_GET['custCountry'];
$transAmount = round($transAmount,$roundLevel);
$localAmount = round($localAmount,$roundLevel);
$totalAmount = round($totalAmount,$roundLevel);
$imFee = round($imFee,$roundLevel);

$return_value = "$transAmount,$localAmount,$totalAmount,$imFee,$exRate,$currencyCharge,$exID";

echo $return_value;
?>