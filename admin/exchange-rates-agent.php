<?
session_start();
include ("../include/flags.php");
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();
$loggedUserID = $_SESSION["loggedUserData"]["userID"];


/**
 * Adding more filter to search the data
 * @Ticket# 3426
 */
$filters = "";
if(CONFIG_EXCHANGE_RATE_FILTERS && $_GET["Submit"] && $agentType == "admin")
{
	/**
	 * Date filter
	 */
	$fromDate = $_GET["fYear"] . "-" . $_GET["fMonth"] . "-" . $_GET["fDay"];
	$toDate = $_GET["tYear"] . "-" . $_GET["tMonth"] . "-" . $_GET["tDay"];
	$filters .= " and (dated >= '$fromDate' and dated <= '$toDate')";

	
	if(!empty($_GET["basedOn"]))
	{
		$filters .= " and rateFor = '$_GET[basedOn]' ";
	}
	if(!empty($_GET["country"]))
	{
		$filters .= " and countryOrigin = '$_GET[country]' ";
	}
	if(!empty($_GET["destCountry"]))
	{
		$filters .= " and country = '$_GET[destCountry]' ";
	}
	
	//echo "<br>".$filters."<br>";
}


/**
 * Following code under if/else clause has been indented by Usman Ghani to make it readable and easily understandable.
 */
 
// this cconfig added by Niaz Ahmad #2331 at 29-11-2007
if(CONFIG_AGENT_OWN_GENERIC_RATE== "1")
{
	// sub agnets  can view generic and his supper agent exchange rate(familyexpress)
	if($agentType == 'SUBA' || $agentType == 'SUBAI')
	{
		$subAgentQuery = selectFrom("select parentID from " . TBL_ADMIN_USERS . " where userID= ".$loggedUserID ." ");
		//echo "select parentID from " . TBL_ADMIN_USERS . " where userID= ".$loggedUserID ." ";
		 $superAgentID=$subAgentQuery["parentID"];
		$query = "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' and (rateFor ='agent' or rateFor ='generic') and (rateValue = ".$superAgentID ." or rateValue = '') order by dated DESC ";
		//echo "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' and (rateFor ='agent' or rateFor ='generic') and (rateValue = ".$superAgentID ." or rateValue = '') order by dated DESC ";
	}
	elseif($agentType == 'SUPA' || $agentType == 'SUPAI' )
	{
		//supper agnets  can view generic and his own exchange rate(familyexpress)
		$query = "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' and (rateFor ='agent' or rateFor ='generic') and (rateValue = ".$loggedUserID ." or rateValue = '') order by dated DESC ";
		//echo "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' and (rateFor ='agent' or rateFor ='generic') and (rateValue = ".$loggedUserID ." or rateValue = '') order by dated DESC ";
  	}
	else
	{
		// supper admin can view all exchange rate (core functionality) 
		$query = "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' order by dated DESC ";
		//echo  "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country' order by dated DESC "; 
	}
}
else
{
	if(CONFIG_SETTLEMENT_COMMISSION == "1")
	{
		$query = "select *  from " . TBL_EXCHANGE_RATES . " as e, ".TBL_RELATED_EXCHANGE_RATES." as r where country != 'Any Country' and e.erID = r.erID ";
	}
	else
	{
		$query = "select *  from " . TBL_EXCHANGE_RATES . " where country != 'Any Country'  ";
	}

	if($agentType == 'SUPA' || $agentType == 'SUPAI')
	{
		$contentAgent = selectFrom("select agentCountry,userID from " . TBL_ADMIN_USERS . " where username = '$username'");
		$country = ($_GET["currency"] == "GBP" || $contentAgent["agentCountry"] == "" ? "United Kingdom" : $contentAgent["agentCountry"]);
		$query .= " and countryOrigin = '$country'";
		if(CONFIG_GENERIC_RATE_FOR_AnD == "1")
			$query .= " and rateFor = 'generic'";
		elseif(CONFIG_GENERIC_RATE_FOR_AGENT == "1")
			$query .= " and rateFor = 'generic'";
		elseif(CONFIG_ALL_TYPES_OF_RATE == "1")
			$query .= " and rateFor != ''";
		else
			$query .= " and ( rateValue = '".$contentAgent['userID']."' or rateFor = 'generic') ";
	}

	if(CONFIG_EXCHANGE_RATE_FILTERS)
 		$query .= $filters;
	$query .= " and isActive !='N' order by dated DESC";
}
//debug( $query );
$curencies = selectMultiRecords($query);


/**
 * Code added to show agent->distributor exchange rates.
 * @Ticket: #3425: Connect Plus - Agent Exchange Rate Filters.
 * @Author: Usman Ghani.
 */
if ( defined("CONFIG_SHOW_AGENT_DISTRIBUTOR_RATE_TO_CERTAIN_USERS")
	 && CONFIG_SHOW_AGENT_DISTRIBUTOR_RATE_TO_CERTAIN_USERS == 1
	 && strstr(CONFIG_USERS_TO_SEE_AGENT_DISTRIBUTOR_RATES, $agentType) )
{
	$myQuery = "select *  from " . TBL_EXCHANGE_RATES . " as e, ".TBL_RELATED_EXCHANGE_RATES." as r where country != 'Any Country' and e.erID = r.erID and rateFor = 'distributor' and isActive !='N' order by dated DESC";
	$rawExRates = selectMultiRecords( $myQuery );

	foreach( $rawExRates as $exRate )
	{
		$chkQuery = "select * from " . TBL_EXCHANGE_RATES . " as e where country != 'Any Country' and rateFor = 'agent' and rateValue = '".$contentAgent['userID']."' and relatedID = '" . $exRate["relatedID"] . "'";
		$chkData = selectFrom( $chkQuery );
		if ( !empty( $chkData ) )
		{
			$curencies[] = $exRate;
		}
	}
}
/**
 * End of Usman's code against #3425: Connect Plus - Agent Exchange Rate Filters.
 */

$contentAgent = selectFrom("select agentCountry from " . TBL_ADMIN_USERS . " where username = '$username'");
$country = ($_GET["currency"] == "GBP" || $contentAgent["agentCountry"] == "" ? "United Kingdom" : $contentAgent["agentCountry"]);

/*$query = "select * from " . TBL_EXCHANGE_RATES . " where  country ='$country' and country != 'Any Country'";
$contentRate = selectFrom($query);
$countryRate =  $contentRate["primaryExchange"]."rate";
$dated =  $contentRate["dated"];
$dated = explode("-",$dated);
//$dated[2]."".$dated[1]."".$dated[0];
$date2 = explode(" ",$dated[2]);
$dated2 =  $date2[0]."/".$dated[1]. "/".$dated[0];
$unixdate = strtotime ($dated2);*/
$ukdate = date("j F, Y");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="images/interface.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script>
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
}
.style2 {color: #6699CC;
	font-weight: bold;
}
-->
</style></head>

<body>

<table width="100%" border="0" cellpadding="5" cellspacing="1" bordercolor="#666666">
  <tr>
    <td class="topbar">Exchange Rates</td>
  </tr>
  <tr>
    <td><table width="80%"  border="0" bordercolor="#666666">
        <tr>
          <td><fieldset>
            <legend class="style2" title="Exchange rates updated Last on <? echo $ukdate;?>">Exchange 
            rates updated Last on <? echo $ukdate;?></legend>
            <legend class="style2"></legend>
            <br>
            <table border="0" align="center" cellpadding="5" cellspacing="1">
              <tr>
                <td width="100%" colspan="4" bgcolor="#DFE6EA" align="center">
					<form name="search" method="get" action="exchange-rates-agent.php">
                <?
                	if(!CONFIG_EXCHANGE_RATE_FILTERS) {
                ?>
                	<select name="currency" id="currency">
                  	<option value="GBP">GBP</option>
									  <? if($contentAgent["agentCountry"] != "United Kingdom") { ?>
									  <option value="<? echo $contentAgent["agentCountry"]?>"><? echo $contentAgent["agentCountry"] ?></option>
					         	<? } ?>
									</select>
								<?
									}
								?>
					<?
						/**
						 * Displaying the search filters for super admin only
						 * @Ticket# 3426
						 */ 
						if(CONFIG_EXCHANGE_RATE_FILTERS && $agentType == "admin")
						{
					?>
									
										<b>Originating Country </b>
										<?
											$countiresRs = "select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%origin%' order by countryName";
										?>
									   	<select name="country" style="font-family: verdana; font-size: 11px;">
									   		<option value="">Select Country</option>
									   <?
												$countires = selectMultiRecords($countiresRs);
												for ($i=0; $i < count($countires); $i++){
										 ?>
												<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
										 <?
													}
										 ?>
												</SELECT>
									             <script language="JavaScript">
									   				SelectOption(document.search.country, "<?=$_GET["country"]?>");
								    	  </script>
								    	  		&nbsp;&nbsp;
										<b>Destination Country</b> 
										<?
											$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%destination%' ";
										?>
									   	<select name="destCountry" style="font-family: verdana; font-size: 11px;">
									   		<option value="">Select Country</option>
									   <?
											  $countiresQuery .=" order by countryName ";
												$countires = selectMultiRecords($countiresQuery);
												for ($i=0; $i < count($countires); $i++){
										 ?>
												<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
										 <?
													}
										 ?>
												</SELECT>
								               <script language="JavaScript">
								     				SelectOption(document.search.destCountry, "<?=$_GET["destCountry"]?>");
								      	  </script>

				<br /><br />
				
		<b>From </b>
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.search.fDay, "<?=(!empty($_GET["fDay"])?$_GET["fDay"]:"")?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.search.fMonth, "<?=(!empty($_GET["fMonth"])?$_GET["fMonth"]:"")?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.search.fYear, "<?=(!empty($_GET["fYear"])?$_GET["fYear"]:"")?>");
        </script>
        <b>To	</b>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.search.tDay, "<?=(!empty($_GET["tDay"])?$_GET["tDay"]:"")?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.search.tMonth, "<?=(!empty($_GET["tMonth"])?$_GET["tMonth"]:"")?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
        SelectOption(document.search.tYear, "<?=(!empty($_GET["tYear"])?$_GET["tYear"]:"")?>");
        </script>
        <br /><br />
		
											&nbsp;&nbsp;
															 <b> Based On </b>
				  <select name="basedOn" style="font-family: verdana; font-size: 11px;">
							  <option value="">All</option>
			  				<option value="generic" <? if($_GET["basedOn"] == "generic") echo "selected"; ?>>Generic</option>
								<option value="agent"  <? if($_GET["basedOn"] == "agent") echo "selected"; ?>>Agent</option>
								<option value="distributor" <? if($_GET["basedOn"] == "distributor") echo "selected"; ?>>Distributor</option>
					</select>      
					<br />
			 	      <br />
                <input type="submit" name="Submit" value="Process">
				</form>
		<?
			/**
			 * Closing of the Search filter interface
			 */
			
			}
		?>
				</td>
			</tr>
    	</table>
            <br>
            <table width="90%" border="0" align="center" cellpadding="5" cellspacing="1" bordercolor="#666666">
              <tr>
			<td colspan="8" align="center">
			<b><strong><font color="#000000"> [ The 
      display exchange rates are for guide only ]</font></strong></b>
			</td>
			</tr>
			<tr>
			<td width="59%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Originating Country</strong></font></td>
			<td width="59%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Originating Currency</strong></font></td>
			<td width="59%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Destination Country</strong></font></td>
			<td width="59%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Destination Currency</strong></font></td>
			<td width="28%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Date</strong></font></td>
			<td width="19%" bgcolor="#DFE6EA"><font color='#005b90'><strong>Rate</strong></font></td>
			<td bgcolor="#DFE6EA"><font color="#005b90"><b>Based On</b></font></td>
      <td bgcolor="#DFE6EA"><font color="#005b90"><b>Value</b></font></td>
			</tr>
			<?
			for($i=0;$i < count($curencies);$i++)
			{
			
			$margin = $curencies[$i]["marginPercentage"];
			//if($curencies[$i-1]["country"] != $curencies[$i]["country"] || $curencies[$i-1]["countryOrigin"] != $curencies[$i]["countryOrigin"])
			//{
			//echo $countryRate."country rate";
			?>			  
				<td width="59%" bgcolor="#DFE6EA"><? echo displayFlag($curencies[$i]["countryOrigin"], './'); echo " <font color='#005b90'><strong>" . $curencies[$i]["countryOrigin"] . "</strong></font>"?> </td>
				<td width="22%" bgcolor="#DFE6EA"><font color='#005b90'><? echo $curencies[$i]["currencyOrigin"]?></font></td>
			  <td width="59%" bgcolor="#DFE6EA"><? echo displayFlag($curencies[$i]["country"], './'); echo " <font color='#005b90'><strong>" . $curencies[$i]["country"] . "</strong></font>"?> </td>
			  <td width="22%" bgcolor="#DFE6EA"><font color='#005b90'><? echo $curencies[$i]["currency"]?></font></td>
             <!--<td width="59%" bgcolor="#DFE6EA"><font color='#005b90'>&nbsp;&nbsp;&nbsp;<? //echo ucfirst(strtolower($curencies[$i]["sProvider"]))?></font></td>-->
        <td width="22%" bgcolor="#DFE6EA"><font color='#005b90'><? echo dateFormat($curencies[$i]["dated"],"2")?></font></td>
        
        <td width="19%" bgcolor="#DFE6EA"><font color='#005b90'><? 
					if($curencies[$i]["marginType"] == 'fixed')
            	{
            		 $calculatedRate = $curencies[$i]["primaryExchange"] - $curencies[$i]["marginPercentage"];
            	}
            	else{
            		 $calculatedRate = round($curencies[$i]["primaryExchange"] - ($curencies[$i]["primaryExchange"] * $curencies[$i]["marginPercentage"])/100, 4);
            		}
            		
            		if(CONFIG_AGENT_RATE_MARGIN == "1" && strtoupper($curencies[$i]["rateFor"]) == 'AGENT'){
            			
				            			if($curencies[$i]["agentMarginType"] == 'fixed')
				            	{
				            		 $calculatedRate = $calculatedRate - $curencies[$i]["marginPercentage"];
				            	}else{
				            		 $calculatedRate = round($calculatedRate - ($calculatedRate * $curencies[$i]["agentMarginValue"])/100, 4);
				            		}
            			
            			
            			
            			}
					if(CONFIG_24HR_BANKING == "1"){
						$exRate24hr = 0.0;
						if($curencies[$i]["rateFor"] == "bank24hr"){
							if($curencies[$i]["_24hrMarginType"] == 'lower'){
											$exRate24hr = $curencies[$i]["primaryExchange"] - $curencies[$i]["_24hrMarginValue"];
										}elseif($curencies[$i]["_24hrMarginType"] == 'upper'){
											$exRate24hr = $curencies[$i]["primaryExchange"] + $curencies[$i]["_24hrMarginValue"];
										}
										if($curencies[$i]["marginType"] == 'fixed')
										{
											$exRate24hr = ($exRate24hr - $curencies[$i]["marginPercentage"]);
										}else{
											$exRate24hr = ($exRate24hr - ($exRate24hr * $curencies[$i]["marginPercentage"]) / 100);
										}
									$calculatedRate = $exRate24hr;
									}
					}
					echo $calculatedRate;
					
	?></font></td>
	<td bgcolor="#DFE6EA"><font color='#005b90'><?=strtoupper(stripslashes($curencies[$i]["rateFor"])); ?></font></td>
            
            <td bgcolor="#DFE6EA"><font color='#005b90'><? 
            		if(strtoupper(stripslashes($curencies[$i]["rateFor"])) == "DISTRIBUTOR" || strtoupper(stripslashes($curencies[$i]["rateFor"])) == "AGENT")
            		{
            			$agentName = selectFrom("select userID,username, name from ".TBL_ADMIN_USERS." where userID = '".$curencies[$i]["rateValue"]."'");
            			echo($agentName["name"]." [".$agentName["username"]."]");
            			}else{
            				 echo(stripslashes($curencies[$i]["rateValue"]));
            				 } 
            			?></font>
            </td>
              </tr>
			  <?
			  }
			  ?>
			  
			  <? if(count($curencies) < 1) { ?>
			  		<tr><td width="59%" bgcolor="#DFE6EA" colspan="8" align="center">No record to display. Please refine your search.</td></tr>
				<? } ?>
            </table>
            <br>
          </fieldset></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
