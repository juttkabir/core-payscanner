<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$currencyies = selectMultiRecords("SELECT DISTINCT(currencyName) FROM currencies");

?>
<html>
<head>
<title>Unresolved Payments for Item-Wise Reconciliation</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />  
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	jQuery(document).ready(function(){
		var lastSel;
		var maxRows = 20;
		jQuery("#payList").jqGrid({
			url:'unres-payments-action-fx-new.php?type=<?=$_GET["type"];?>&getGrid=payList&nd='+new Date().getTime(),
			datatype: "json",
			height: 500, 
			colNames:[
				'Date', 
				'Amount',
				'Remaining Amount',
				'Currency',
				'CODE', 
				'Description'
			],
			colModel:[
				{name:'importedOn',index:'importedOn', width:60, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'amount',index:'amount', width:50, align:"center"},
				{name:'remAmount',index:'remAmount', width:50, align:"center"},
				{name:'currencyPay',index:'currencyPay', width:50, align:"left"},
				{name:'tlaCode',index:'tlaCode', width:40, align:"center"},
				{name:'description',index:'description', width:300, align:"left"}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [20,40,60],
			pager: jQuery('#pagernav'),
//			sortname: 'importedOn',
			viewrecords: true,
			multiselect:true,
			sortorder: "ASC",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Unresolved payments"
		});

		jQuery("#transList").jqGrid({
			url:'unres-payments-action-fx-new.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			height: 500, 
			colNames:[
				'Date', 
				'Reference Code', 
				'Total Amount',
				'Remaining Amount',
				'Currency Sold',
				'Currency Bought',
				'Sender #',
				'Sender Name'
			],
			colModel:[
				{name:'transDate',index:'transDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center"},
				{name:'totalAmount',index:'totalAmount', width:80, align:"center"},
				{name:'remTransAmount',index:'remTransAmount', width:80, align:"center"},
				{name:'currencyTrans',index:'currencyTrans', width:50, align:"left"},
				{name:'currencyTo',index:'currencyTo', width:50, align:"left"},
				{name:'accountName',index:'accountName', width:50, align:"center"},
				{name:'name',index:'name', width:80, align:"left"},
				
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [20,40,60],
			pager: jQuery('#pagernavT'),
//			sortname: 'transDate',
			viewrecords: true,
			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions"
		}); 
		
		jQuery("#btnActionDel").click( function(){
			if($(this).val()=="Delete"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				if(payIDs!=''){
					payIDs = payIDs.toString();
					var confirmDel = confirm("Are you sure to delete selected Payment Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allPayIdsArr = payIDs.split(",");
					$("#payListDeleteData").load("unres-payments-action-fx-new.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'btnAction': 'Delete'},
						 function(){
						alert($("#payListDelete").val());
						//gridReload('payList');
					});
				}
				else{
					alert("Please Select Payment(s) to delete");
					$("#allPayIdsV").val('');
					return false;
				}
				$("#allPayIdsV").val('');
				gridReload('deletePayList');
			}
		});		
		jQuery("#btnAction").click( function(){
			if($(this).val()=="Resolve"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				var transIDs = jQuery("#transList").getGridParam('selarrrow');
				if(payIDs!='' && transIDs!=''){
					if(reconcileCheck(payIDs,transIDs)==false){
						
						return false;
					}
					var allPayIds = $("#allPayIdsV").val();
					var allPayIdsArr = allPayIds.split(",");
					var allTransIDs = $("#allTransIDsV").val();
					var allTransIdsArr = allTransIDs.split(",");
					var userType = $("#userType").val();
					//alert(allTransIDs);
					$("#payListResolveData").load("unres-payments-action-fx-new.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'transIDs[]': allTransIdsArr,'btnAction': 'Resolve','userTypes': userType},
						 function(){
						alert($("#payListResolve").val());
						gridReload('bothList');
					});
				}
				else{
					alert("Please Select Rows from both sides");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}

				$("#userType").val("customer");
				gridReload('bothList');
			}
		});
		jQuery('a').cluetip({splitTitle: '|'});
	});
	function deletePayments(pids){
		var amountPay   =0;
		var pids = pids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
	}
    function reconcileCheck(pids,tids){
		var amountPay   =0;
		var amountTrans =0;
		var pids = pids.toString();
		var tids = tids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
		var allPayIds	=new Array();
		var allTransIds	=new Array();
		var allPayCurr = new Array();
		var allTransCurr = new Array();		
		if(pids!=null)
			allPayIds   = pids.split(",");
		if(tids!=null)
			allTransIds = tids.split(",");
		
		for(var p=0;p<allPayIds.length;p++){
			var payG	= jQuery("#payList").getRowData(allPayIds[p]);
			amountPay	= amountPay + parseFloat(payG.amount.split(',').join(''));
			allPayCurr[p]  = payG.currencyPay;
		}
		for(var t=0;t<allTransIds.length;t++){
			var transG	= jQuery("#transList").getRowData(allTransIds[t]);
			amountTrans = amountTrans + parseFloat(transG.totalAmount.split(',').join(''));
			allTransCurr[t]  = transG.currencyTrans;
		}
		for(var c1=0;c1<allTransCurr.length;c1++){
			for(var c2=0;c2<allPayCurr.length;c2++){
				var transCurr = allTransCurr[c1];
				var payCurr = allPayCurr[c2];
				if(transCurr.toString()!=payCurr.toString()){
					alert("Currencies do not match from both sides.Please select same currency Amounts.");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}
			}
		}
		if(amountPay==amountTrans || amountPay!=amountTrans){
											
					$.ajax({
						url: 'unres-payments-action-fx-new.php',
                         data: {
                         ajax: true,
                         getCustomersList:'true',
						Id:allPayCurr,
						pay:amountPay,
						trans:amountTrans,
						payIDs:pids,
						transIDs:tids
                         },
                         async: false,
                         type: 'POST',
                         success: function(data, textStatus, XMLHttpRequest){
                                                    
                                                    $("#newDiv").html(data);
                                                    //$("#rules").show();
                                               }		
                                        });
			/*alert("Amounts should match.\nTotal Payment Amounts = "+amountPay+"\nTotal Transaction Amount = "+amountTrans+"\nPlease Select again.");
			$("#allPayIdsV").val('');
			$("#allTransIDsV").val('');*/
			return false;
		}
	}
	
	function gridReload(grid)
	{
		var theUrl = "unres-payments-action-fx-new.php";
		
		if(grid=='payList' || grid=='bothList' || grid=='deletePayList'){
			var extraParam="?type="+<?=$_GET["type"]?>;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var amount1 = jQuery("#amount1").val();
			var desc = jQuery("#desc").val();
			var currencyPay1 = jQuery("#currencyPay").val();
			if(grid=='payList'){
				extraParam = extraParam + "&from="+from+"&to="+to+"&desc="+desc+"&amount1="+amount1+"&currencyPay="+currencyPay1+"&Submit=SearchPay&getGrid=payList";
			}
			else{
				extraParam = extraParam + "&getGrid=payList";
			}
			jQuery("#payList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
		if(grid=='transList' || grid=='bothList'){
			var extraParam='';
			var fromT = jQuery("#fromT").val();
			var toT = jQuery("#toT").val();
			var amountT = jQuery("#amountT").val();
			var searchBy = jQuery("#searchType").val();
			var searchName = jQuery("#searchName").val();
			var currencyTrans1 = jQuery("#currencyTrans").val();
			var paymentMod = jQuery("#paymentMod").val();
			if(grid=='transList'){
				//$("#userType").val(searchBy);
				extraParam = "?fromT="+fromT+"&toT="+toT+"&amountT="+amountT+"&searchBy="+searchBy+"&searchName="+searchName+"&currencyTrans="+currencyTrans1+"&paymentMod="+paymentMod+"&Submit=SearchTrans&getGrid=transList";
			}
			else{
				extraParam = "?getGrid=transList";
			}
			
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}
	}
	function reconciledOptions(id){
		var ret 	= jQuery("#payListList").getRowData(id);
		var recOptIds  = jQuery("#recOptIds").val();
		alert('Before recOptIds'+recOptIds);
		var recArr = new Array();
		if(recOptIds!=''){
			recArr = recOptIds.split(',');
			//alert('id='+id);
			//alert('Length of Array = '+recArr.length +' index[0]='+recArr[0]+' index[1]='+recArr[1]);
			//alert('Exists = '+recArr.indexOf(id));
			if(recArr.indexOf(id)==-1){
				recArr.push(id);
				recArr.join(',');
				jQuery("#recOptIds").val(recArr);
			}
		}
		else{
			jQuery("#recOptIds").val(id);
		}
		alert('After recOptIds='+jQuery("#recOptIds").val());
		//+" Reconcile="+ret.mark+" Username="+ret.username+" UserType="+ret.userType+" Notes="+ret.notes
	}
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }

-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
	<form name="frmSearch" id="frmSearch">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF">Unresolved  Barclays payment</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
     <tr>
    	<td><a href="unresolved-payments-fx.php" class="style2">Go Back to Unresolved Payments List </a></td>
    </tr>
    
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Payments Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="from" type="text" id="from" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
		    		    </td>
                </tr>
        <tr>
            <td nowrap align="center"><!--input name="refNumber" type="text" id="refNumber" value=<?=$refNumber?>-->
		  				Amount 
              <input name="amount1" type="text" id="amount1" >
              <!--input name="amount1" type="text" id="amount1" value=<?=$amount1?>-->
			  &nbsp;&nbsp;&nbsp;
              Description 
              <input name="desc" type="text" id="desc" >
              <!--input name="desc" type="text" id="desc" value=<?=$desc?>-->
		</td>
      </tr>
	  <tr>
         <td nowrap align="center">
              Select Currency
              <select name="currencyPay" id="currencyPay">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($cp=0;$cp<count($currencyies);$cp++){?>
					  <option value="<?=$currencyies[$cp]["currencyName"]?>"><?=$currencyies[$cp]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
				<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">		 
		 </td>
		</tr>
		 <tr><td align="center"><input type="button" name="Submit" value="Search Payments" onClick="gridReload('payList')">
		</td></tr>
    </table>
  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListResolveData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">
		<input name="btnAction" id="btnAction" type="button"  value="Resolve">
		<input type='hidden' name='totTrans' id='totTrans'>
		<input type='hidden' name='recOptIds' id='recOptIds' value="">
		<input type="hidden" name="allPayIdsV" id="allPayIdsV" value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="userType" id="userType" value="customer">		
		<input name="btnActionDel" id="btnActionDel" type="button"  value="Delete"></td>
		
	  </tr>
	  <div id='newDiv' name='newDiv'></div>
</table>
	</td>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;margin-top:20px;" >
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Transactions Search Filter</strong></span></td>
        </tr>
        <tr>
		  <td nowrap align="center">From Date 
				<input name="fromT" type="text" id="fromT" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.fromT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
				<input name="toT" type="text" id="toT" readonly >
				&nbsp;<a href="javascript:show_calendar('frmSearch.toT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
				<!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
			</td>
        </tr>
        <tr>
	      <td nowrap align="center"> 
              Search Type
              <select name="searchType" id="searchType">
				  <option value="customer">Transaction</option>
				  <option value="agent">Agent</option>
				  <option value="distributor">Distributor</option>
				  <option value="sender" selected>Sender</option>
			  </select>
			  &nbsp;&nbsp;&nbsp;
           	  Reference Number/Name 
              <input name="searchName" type="text" id="searchName" >
		</td>
      </tr>
      <tr>
	      <td nowrap align="center"> 
              Payment Mode
              <select name="paymentMod" id="paymentMod">
           <option value="">Select One</option>   	
				  <option value="By Cash">By Cash</option>
				  <option value="By Cheque">By Cheque</option>
				  <option value="By Bank Transfer">By Bank Transfer</option>
			  </select>
		</td>
      </tr>
	  <tr>
		  <td nowrap="nowrap" align="center">
	  	  Amount 
            <input name="amountT" type="text" id="amountT" >
			&nbsp;&nbsp;&nbsp;
              Select Currency
              <select name="currencyTrans" id="currencyTrans">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($ct=0;$ct<count($currencyies);$ct++){?>
					  <option value="<?=$currencyies[$ct]["currencyName"]?>"><?=$currencyies[$ct]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
			<input type="button" name="Submit" value="Search Transactions" onClick="gridReload('transList')">
			<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">
		  </td>
	  </tr>
    </table>
  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="transList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">&nbsp;</td>
	  </tr>
	  <?php
		$msg=$_GET['msg'];
		if(!empty($msg))
			echo "<script>alert('You must check atleast one payment/transaction ');</script>";
	  ?>
</table>

</td>
  </tr>
</table>
</form>
</body>
</html>
