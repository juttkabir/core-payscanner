<?php
	session_start();
	include ("../include/config.php");
	include ("security.php");
	include ("calculateBalance.php");

	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_REQUEST["pageUrl"],'E');
	
	header('Content-Type: application/vnd.ms-excel');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=distriburtorStamtmentReport.xls"); 
	header ("Content-Description: PHP/MYSQL Generated Data");

	if(!empty($_POST["da"]))
	{
		$strSqlForExport = stripslashes($_POST["da"]);
		$arrDataForExport = selectMultiRecords($strSqlForExport);
		

		
		?>
		
        <tr> 
            <td> 
			  <table width="100%">
                <tr> 
                  <th align="center">Date</th>
                  <th align="center"><?=$systemCode?></th>
                  <th align="center">Fax Number</th>
				  <th align="center">Beneficiary Name</th>
				  <th align="center">Amount Received</th>
				  <th align="center">Payout Amount</th>
                  <th align="center">Balance</th>
				  <th align="center">Exchange Rate</th>
				  <th align="center">Sending Amount</th>
				  <th align="center">Description</th>
                </tr>
                <? 
				
				$intRunningBalance = distributorOpeningBalance($_REQUEST["slctID"],$_REQUEST["destcurrency"],$_REQUEST["fromDate"]);
				
				for($i=0;$i<count($arrDataForExport);$i++)
				{
					/**
					 * Fetching the associated transaction and beneficiary details
					 */
					if(!empty($arrDataForExport[$i]["TransID"]))
					{
						$strTransactionSql = "
										select 
											t.refNumberIM,
											t.exchangeRate,
											t.localAmount,
											t.transAmount,
											t.currencyFrom,
											t.currencyTo,
											t.transDate,
											t.fromCountry,
											t.toCountry,
											t.faxNumber,
											b.firstName,
											b.lastName
										from
											beneficiary as b,
											transactions as t
										where
											    b.benID = t.benID
											and t.transID = ".$arrDataForExport[$i]["TransID"];
						
						$arrTransactionData = selectFrom($strTransactionSql);
						
						$faxNumber = $arrTransactionData["faxNumber"];
						$refNumber = $arrTransactionData["refNumberIM"];
						$name = $arrTransactionData["firstName"]." ".$arrTransactionData["lastName"];
						$localAmount = $arrTransactionData["localAmount"];
						$exchangeRate = $arrTransactionData["exchangeRate"];
						$transAmount = $arrTransactionData["transAmount"];
					}
					else
					{
						$faxNumber = "&nbsp;";
						$refNumber = "&nbsp;";
						$name = "&nbsp;";
						$localAmount = "&nbsp;";
						$exchangeRate = "&nbsp;";
						$transAmount = "&nbsp;";
					}
				
					$cumulativeCurr=$arrDataForExport[$i]["currency"];
				
				?>
                <tr> 
                  <td align="center">
					<? 
						$authoriseDate = $arrDataForExport[$i]["dated"];
						echo date("d-m-y", strtotime("$authoriseDate"));
					?>
				  </td>
            <?  
             if(!empty($arrDataForExport[$i]["TransID"])) 
			 {
			?>
         	<td align="center">
				<?=$arrTransactionData["refNumberIM"]?>
			</td>      
			<? }else{?>
				<td>&nbsp;</td>
			<? }?>
			<td align="center"><?=$faxNumber?></td>
			<td align="center"><?=$name?></td>
			<td>
			<?  
				if($arrDataForExport[$i]["type"] == "DEPOSIT")
				{
					if (DEST_CURR_IN_ACC_STMNTS == "1") 
						echo $arrDataForExport[$i]["currency"] . " " ;
			
					echo $arrDataForExport[$i]["amount"];
					$Balance = $Balance+$arrDataForExport[$i]["amount"];
				}
			?>
			</td>
			<td align="center">
				<?
					if($arrDataForExport[$i]["type"] == "WITHDRAW")
						echo $arrDataForExport[$i]["amount"]." ".$arrDataForExport[$i]["currency"];
				?>
			</td>
			<? 
				$currDate = $arrDataForExport[$i]["dated"];
				
				if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
				}else{
					$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
				}
				$FirstDated = $datesFrom["datedFrom"];
				
				$openingBalance = 0;
				
				
				if($FirstDated != "")
				{
					$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
					$account1 .= " and dated = '$FirstDated'";				
					$account1 .= " and  user_id = '".$slctID."' ";	
					if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') 
					{
						$account1 .= " and  currency='".$settleCurr."' ";		
					}
					
					$contents1 = selectFrom($account1);
					
					if($currDate != $FirstDated){
						$openingBalance =  $contents1["opening_balance"];
					}else{
						$openingBalance =  $contents1["closing_balance"];
					}
				}
	
				$closingBalance = $openingBalance;
				
				$preDate = $currDate;
				
				/* Running balance functionality*/
				if($arrDataForExport[$i]["type"] == "DEPOSIT")
					$intRunningBalance = $intRunningBalance + $arrDataForExport[$i]["amount"];
				elseif($arrDataForExport[$i]["type"] == "WITHDRAW")
					$intRunningBalance = $intRunningBalance - $arrDataForExport[$i]["amount"];
					
          		/* Closing balance funcionality */
				if($arrDataForExport[$i]["type"] == "WITHDRAW")
					$closingBalance =  $closingBalance - $arrDataForExport[$i]["amount"];
				
				elseif($arrDataForExport[$i]["type"] == "DEPOSIT")
					$closingBalance =  $closingBalance + $arrDataForExport[$i]["amount"];
				
			  ?>
			  <td align="center"><?=$intRunningBalance." ".$arrDataForExport[$i]["currency"]?></td>
			  
			  <td align="center"><?=$exchangeRate?></td>	
			  <td align="center"><?=$transAmount?></td>	
			  <td align="center"><?=$arrDataForExport[$i]["description"]?></td>	
            </tr>
			<? 
	  	  } 
	}
?>
