<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;

	
	$currencyFilterFlag = false;
	if(CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE=="1"){
		$currencyFilterFlag = true;	
	}
	if(!empty($_GET["RID"]) && CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS == '1')
	{
		$reportQSql = "select RID from ".TBL_COMPLIANCE_QUERY." where note = '".trim($_GET["RID"])."' ";
		$reportQSqlData = selectFrom($reportQSql);
		$openSaveReportSql = "select 
					cpql.RID as RID,
					cpql.filterName as filterName,
					cpql.filterValue as filterValue
				from ".TBL_COMPLIANCE_QUERY_LIST." as cpql 
				where RID='".$reportQSqlData['RID']."'";
		//debug($openSaveReportSql);
		$openSaveReportSqlData = SelectMultiRecords($openSaveReportSql);
		//debug($openSaveReportSqlData);
		$strParam = '';
		for($i=0;$i<count($openSaveReportSqlData);$i++){
			//debug($openSaveReportSqlData[$i]['filterName'].'='.$openSaveReportSqlData[$i]['filterValue']);
			//debug($openSaveReportSqlData[$i]['filterName'].'='.$openSaveReportSqlData[$i]['filterValue']);
			$strParam .= '&'.$openSaveReportSqlData[$i]['filterName'].'='.$openSaveReportSqlData[$i]['filterValue'];
			if($openSaveReportSqlData[$i]['filterName'] == 'from_date')
				$from_date = $openSaveReportSqlData[$i]['filterValue'];
			else if($openSaveReportSqlData[$i]['filterName'] == 'to_date')
				$to_date = $openSaveReportSqlData[$i]['filterValue'];
			else if($openSaveReportSqlData[$i]['filterName'] == 'createdBy')
				$createdBy = $openSaveReportSqlData[$i]['filterValue'];
			else if($openSaveReportSqlData[$i]['filterName'] == 'clientName')
				$clientName = $openSaveReportSqlData[$i]['filterValue'];
			else if($openSaveReportSqlData[$i]['filterName'] == 'type_of_exchange')
				$type_of_exchange = $openSaveReportSqlData[$i]['filterValue'];
			else if($openSaveReportSqlData[$i]['filterName'] == 'currency')
				$currency = $openSaveReportSqlData[$i]['filterValue'];
		}
		//debug($strParam);
	}	
	$strCaption = "Currency Exchange Report";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Currency Exchange Reports</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var selectedId = "";
	var extraParams = "";
	var lAmt = 0;
	var tAmt = 0;
	var tlbAmt = 0;
	var tlsAmt = 0;
	$(document).ready(function(){
	
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		});
	
		$("#transList").jqGrid({
			url:'add-currency-exchange-conf.php?get=getceGrid&q=2&nd='+new Date().getTime()+'<?=$strParam?>',
			postData: {
				StateId: '1234'//function() { return jQuery("#StateId option:selected").val(); },
			},
			datatype: "json",
			height: 400, 
			width: 900,
			colNames:[
				'Rate ID', 
				'Client Name',
				'Date In',
				'Type of Exchange',
				'Currency Buy/Sell',
				'Local Amount',
				'Rate',
				'Amount',
				'Created By'
			],
			colModel:[
				{name:'id',index:'id', width:55, align:"center"},
				{name:'firstname',index:'firstname', width:100},
				{name:'created_at',index:'created_at', width:120, align:"center"},
				{name:'buy_sell',index:'buy_sell', width:100, align:"center"},
				{name:'currency',index:'currency', width:120, align:"center"},
				{name:'localAmount',index:'localAmount', width:90,align:"center"},
				{name:'rate',index:'rate', width:80, align:"center"},
				{name:'totalAmount',index:'totalAmount', width:80, align:"center"},
				{name:'created_by',index:'created_by', width:150},
			],
			rowNum: 20,
			rowList: [5,10,20,50,150,300],
			imgpath: gridimgpath,
			pager: jQuery('#pager'),
			sortname: 'id',
			viewrecords: true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			<? if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1" || $currencyFilterFlag){ ?>
			multiselect:true,
			<? }?>
			caption: "<?=$strCaption?>",
			<? 
			/*	#4693
				Cancel Transaction functionality added.
				Config handles cancelling a transaction and also
				viewing Cancelled transactions on its Report page.
				by Aslam Shahid
			*/
			if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1" || $currencyFilterFlag){
			?>
				ondblClickRow:function(id){
						loadReciept(id);
				}
				<? if($currencyFilterFlag){?>
					,onSelectRow:function(id){
						if(jQuery("#currency").val()!="")
								updateTotals(id);
					}
				<? }?>
			<? }else{ ?>
				onSelectRow:function(id){
						loadReciept(id);
				}
			<? }?>
			<? 
			if($currencyFilterFlag){
			?>
				,onSelectAll:function(ids)
							{
								var idsArray = new Array();
								var s_ids = new String();
								//alert(ids);
								if(ids != "")
								{
									resetTotals();
									
									s_ids = ids.toString();
									idsArray = s_ids.split(",");
									//alert(jQuery("#currency").val());
								//	if(jQuery("#currency").val()!=""){
										for(var i=0; i<idsArray.length; i++)
										{
											updateTotals(idsArray[i]);
										}
								//	}
								} else {
									resetTotals();
								}
							}
			<?
			}
			?>
			,loadComplete:function(data){
			var grid = $("#transList");
			 // grid.resetSelection();
			  var ids = grid.getDataIDs();
			  s_ids = ids.toString();
              idsArray = s_ids.split(",");
			  /*for (var i=0, il=ids.length; i < il; i++ ) 
			   		grid.setSelection(ids[i], false);*/
			resetTotals();	
			//alert(idsArray.length);	
			for(var i=0; i<idsArray.length; i++)
				{
				//alert(i);
					grid.setSelection(ids[i], false);
					if(jQuery("#currency").val()=="")
						updateTotals(idsArray[i]);
				}	
			  grid.resetSelection();
			
			}
		});
		jQuery('a').cluetip({splitTitle: '|'});
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});

		$("#search_buysell").click(function(){
			gridReload();
		});

		$("#saveReport").click(function(){
			var sids = jQuery("#transList").getGridParam('selarrrow');
			extraParams = "&saveCurrExchReport=y&reportName=<?=$strCaption?>";
			gridReload();
		});
		$("#exportBtn").click(function(){
			$("#exporCurrRow").show();
		});

		$("#exporCurrRow").change(function(){
			if($(this).val() != "")
			{
				var strUrl = "buySellCurrExport.php?exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()
							+"&createdBy="+$("#createdBy").val()+
							"&clientName="+$("#clientName").val()+"&type_of_exchange="+$("#type_of_exchange").val()+"&Submit=Search";
				<?
				if($currencyFilterFlag){
				?>
					strUrl += "&currency="+$("#currency").val();	
				<?
				}
				?>	
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
		$("#cancel_trans").click(function(){
			var sids = jQuery("#transList").getGridParam('selarrrow');
			if(sids == "")
			{
				alert("Please select one or more transactions.");
			} else {
				extraParams = "&cancelTrans=Y&transIds=" + sids;
				gridReload();
			}
		});

	});
	
	function gridReload()
	{
		var date_from = $("#from_date").val();
		var to_date = $("#to_date").val();
		var createdBy = $("#createdBy").val();
		var clientName = $("#clientName").val();
		var type_of_exchange = $("#type_of_exchange").val();
		var currency = $("#currency").val();
		var extraAppUrl = "&from_date="+date_from+"&to_date="+to_date+"&createdBy="+createdBy+"&clientName="+clientName+"&type_of_exchange="+type_of_exchange;
		var theUrl = "add-currency-exchange-conf.php?get=getceGrid"+extraAppUrl+"&Submit=Search";
		<?
		if($currencyFilterFlag){
		?>
			theUrl += "&currency="+$("#currency").val();	
			resetTotals();
		<?
		}
		?>	
		if(extraParams != "")
		{
			theUrl = theUrl + extraParams;
			extraParams = "";
		}
		$("#transList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
		
	}


	function loadReciept(id)
	{
		if(id != "")
		{
			var url = "/admin/buySellCurrReceipt.php?rid="+id;
			window.open (url,"Currency Exchange Receipt","location=0,menubar=0,resizable=0,status=1,toolbar=1,scrollbars=1,width=350,height=550"); 
		}
		
	}
	function resetTotals()
	{
/*		var ret = jQuery("#transList").getRowData(rowId);
		var fCurrency = ret.currency;*/
		lAmt = 0;
		tAmt = 0;
		tlbAmt = 0;
		tlsAmt = 0;
		jQuery("#lfa").text(lAmt.toFixed(4));
		jQuery("#tfa").text(tAmt.toFixed(4));
		jQuery("#tlba").text(tlbAmt.toFixed(4));
		jQuery("#tlsa").text(tlsAmt.toFixed(4));
	}
	
	function updateTotals(rowId)
	{
		var ret = jQuery("#transList").getRowData(rowId);
		//alert(ret.localAmount);
		var fCurrency = ret.currency;
		if(jQuery("#jqg_"+rowId).attr("checked"))
		{
			lAmt += parseFloat(ret.localAmount);
			tAmt += parseFloat(ret.totalAmount);
		}
		else{
			lAmt = lAmt - parseFloat(ret.localAmount);
			tAmt = tAmt - parseFloat(ret.totalAmount);
		}
		if(jQuery("#currency").val()!=""){
			jQuery("#lfa").text(lAmt.toFixed(4) + " GBP");
			jQuery("#tfa").text(tAmt.toFixed(4) + " " + fCurrency);
		}
		updateBuySellTotals(rowId); // This function displays total Buy and Sell GBP amounts separately.
	}
	function updateBuySellTotals(rowId)
	{
		var ret = jQuery("#transList").getRowData(rowId);
		var buy_sell = ret.buy_sell;
		if(jQuery("#jqg_"+rowId).attr("checked"))
		{
			if(buy_sell == 'Buy')
				tlbAmt += parseFloat(ret.localAmount);
			if(buy_sell == 'Sell')
				tlsAmt += parseFloat(ret.localAmount);
		}
		else{
			if(buy_sell == 'Buy')
				tlbAmt = tlbAmt - parseFloat(ret.localAmount);
			if(buy_sell == 'Sell')
				tlsAmt = tlsAmt - parseFloat(ret.localAmount);
		}
		
		jQuery("#tlba").text(tlbAmt.toFixed(4) + " GBP");
		jQuery("#tlsa").text(tlsAmt.toFixed(4) + " GBP");
	}
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
				break;
			}
		}
	}
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
/*a.dp-choose-date {
	float: left;
	width: 16px;
	height: 16px;
	padding: 0;
	margin: 5px 3px 0;
	display: block;
	text-indent: -2000px;
	overflow: hidden;
	background: url(images/calendar.jpg) no-repeat; 
}
a.dp-choose-date.dp-disabled {
	background-position: 0 -20px;
	cursor: default;
}*/
.error {
	color: red;
	font: 6pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" bgcolor="#DFE6EA">
			<strong>Search Currency Exchanges</strong>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
			<form name="search_buysell" id="search_buysell" action="" method="post">
				Date In From :&nbsp;
				<input type="text" name="from_date" id="from_date" readonly="" value="<?=$from_date?>"/>
				&nbsp;&nbsp;
				Date In To :&nbsp;
				<input type="text" name="to_date" id="to_date" readonly="" value="<?=$to_date?>"/>
				<br/><br/>
				&nbsp;&nbsp;&nbsp;
				Teller Name :&nbsp;<input type="text" name="createdBy" id="createdBy" value="<?=$createdBy?>"/>
				&nbsp;&nbsp;
				Client Name :&nbsp;<input type="text" name="clientName" id="clientName" value="<?=$clientName?>"/>
				<br /><br />
				Type of Exchange:&nbsp;&nbsp;
				<select name="type_of_exchange" id="type_of_exchange">
					<option value="">All</option>
					<option value="B">Bought</option>
					<option value="S">Sold</option>
				</select>
				 <script language="JavaScript">
					SelectOption(document.search_buysell.type_of_exchange, "<?=$type_of_exchange?>");
				</script>
				<? if($currencyFilterFlag){ ?>
					&nbsp;&nbsp;
					Currency:&nbsp;&nbsp;
					<select name="currency" id="currency">
					  <option value="">All</option>
					  <?
						$strQuery = "SELECT buysellCurrency, currencyName from currencies curr,curr_exchange_account 
										ca where curr.cID=ca.buysellCurrency 
									group by buysellCurrency ";
						$currDataArr = selectMultiRecords($strQuery);
						for($i=0;$i<count($currDataArr);$i++){
							$currencyID = $currDataArr[$i]["buysellCurrency"];
							$currencyN = $currDataArr[$i]["currencyName"];
					  ?>
					  <option value="<?=$currencyID?>"><?=$currencyN?></option>                  
					  <? }?>
					</select>
					 <script language="JavaScript">
						SelectOption(document.search_buysell.currency, "<?=$currency?>");
					</script>
				<? } ?>
<br />
<br />
				<input type="button" id="search_buysell" name="search_buysell" value="Search" style="font-weight:bold" />
				&nbsp;&nbsp;<input type="reset" value="Clear All Filters" />
				&nbsp;&nbsp;<input type="button" id="exportBtn" value="Export Currency Exchanges" />
				&nbsp;
				<select name="exporCurrRow" id="exporCurrRow" style="display:none">
					<option value="">Select Format</option>
					<option value="XLS">Excel</option>
					<option value="CSV">CSV</option>
					<option value="HTML">HTML</option>
				</select>
			</form>
		</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="80%">
			</table>
			<div id="pager" class="scroll" style="text-align:center;"></div>		
		</td>
    </tr>
	<? if($currencyFilterFlag){ ?>
		<tr>
			<td colspan="2">
				Local Amount = <strong><span id="lfa">0.0000</span></strong>&nbsp;&nbsp;&nbsp;&nbsp;Total Amount = <strong><span id="tfa">0.0000</span></strong>&nbsp;&nbsp;&nbsp;&nbsp;Total Sell = <strong><span id="tlba">0.0000</span></strong>&nbsp;&nbsp;&nbsp;&nbsp;Total Buy = <strong><span id="tlsa">0.0000</span></strong>&nbsp;&nbsp;<img src="images/info.gif" width="12" height="12" border="0" title="Amounts | Local and Total Amount will be calculated after searching Curreny based records and selecting records after that."/></a>
			</td>
		</tr>
	<? }?>
	<tr>
		<td colspan="2" align="center">
		<? if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1"){ ?>
			<input type="button" id="cancel_trans" name="cancel_trans" value=" Cancel Selected Transactions " />&nbsp;&nbsp;&nbsp;
		<? }?>
		<? if(CONFIG_SAVED_CURRENCY_EXCHANGE_REPORTS == '1'){ ?>
			<input type="button" id="saveReport" name="saveReport" value=" Save Report " />
		<? }?>
		</td>
	</tr>
</table>
</body>
</html>
