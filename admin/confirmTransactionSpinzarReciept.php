<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date("d-m-Y"); //date('Y-m-d');
//$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentType=getAgentType();
$loggedUName = $_SESSION["loggedUserData"]["username"];
$returnPage = 'add-transaction.php';

$cumulativeRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Accumulative Transaction' and applyAt = 'Confirm Transaction')";

$cumulativeRule = selectFrom($cumulativeRuleQuery);

$currentRuleQuery = " select * from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction' 
and dated = (select MAX(dated) from ".TBL_COMPLIANCE_LOGIC." where applyTrans = 'Current Transaction' and applyAt = 'Confirm Transaction')";
$currentRule = selectFrom($currentRuleQuery);

if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
	}

if($_GET["transID"] != ""){
		
		$transactionQuery = selectFrom("select * from ". TBL_TRANSACTIONS." where transID = '".$_GET["transID"]."'");
		$bankQuery = selectFrom("select * from ". TBL_BANK_DETAILS." where transID = '".$_GET["transID"]."'");
	}
	
if($_POST["customerID"]!= ""){
	$customerID = $_POST["customerID"];
	}else{
	$customerID = $transactionQuery["customerID"];
}

if($_POST["benID"]!= ""){
	$beneficiaryID = $_POST["benID"];
	}else{
	$beneficiaryID = $transactionQuery["benID"];
}


if($_POST["customerAgent"]!= ""){
	$custAgentID = $_POST["customerAgent"];
	}else{
	$custAgentID = $transactionQuery["custAgentID"];
}

		$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$custAgentID."'");


if($_POST["totalAmount"]!= ""){
$totalAmount= $_POST["totalAmount"];
}else{
$totalAmount=$transactionQuery["totalAmount"];	
	}
	
	
if($_POST["transAmount"]!= ""){
$transAmount= $_POST["transAmount"];
}else{
$transAmount=$transactionQuery["transAmount"];	
	}	


	if($_POST["localAmount"]!= ""){
$localAmount= $_POST["localAmount"];
}else{
$localAmount=$transactionQuery["localAmount"];	
	}
	



if($_POST["moneyPaid"]!= ""){
$moneyPaid= $_POST["moneyPaid"];
}else{
$moneyPaid=$transactionQuery["moneyPaid"];	
	}
	
	
if($_POST["transactionPurpose"]!= ""){
$transactionPurpose= $_POST["transactionPurpose"];
}else{
$transactionPurpose=$transactionQuery["transactionPurpose"];	
	}



$discount=$transactionQuery["discounted_amount"];	



if($_POST["exchangeRate"]!= ""){
$exchangeRate= $_POST["exchangeRate"];
}else{
$exchangeRate=$transactionQuery["exchangeRate"];	
	}


if($_POST["IMFee"]!= ""){
$IMFee= $_POST["IMFee"];
}else{
$IMFee=$transactionQuery["IMFee"];	
	}



if($_POST["bankName"]!= ""){
$bankName= $_POST["bankName"];
}else{
$bankName=$bankQuery["bankName"];	
	}

if($_POST["branchCode"]!= ""){
$branchCode= $_POST["branchCode"];
}else{
$branchCode=$bankQuery["branchCode"];	
	}

if($_POST["branchAddress"]!= ""){
$branchAddress= $_POST["branchAddress"];
}else{
$branchAddress=$bankQuery["branchAddress"];	
	}


if($_POST["swiftCode"]!= ""){
$swiftCode= $_POST["swiftCode"];
}else{
$swiftCode=$bankQuery["swiftCode"];	
	}



if($_POST["accNo"]!= ""){
$accNo= $_POST["accNo"];
}else{
$accNo=$bankQuery["accNo"];	
	}




if($_POST["currencyTo"]!= ""){
$currencyTo= $_POST["currencyTo"];
}else{
$currencyTo=$transactionQuery["currencyTo"];	
	}
	
	
	
if($_POST["currencyFrom"]!= ""){
$currencyFrom= $_POST["currencyFrom"];
}else{
$currencyFrom=$transactionQuery["currencyFrom"];	
	}
	
	
	if($_POST["benCountry"]!= ""){
$ToCountry= $_POST["benCountry"];
}else{
$ToCountry=$transactionQuery["toCountry"];	
	}
	
	
	if($_POST["transType"] != ""){
		
		$transactionType = $_POST["transType"];
		} else{
			$transactionType = $transactionQuery["transType"];	
			}
			

if($_POST["bankCharges"] != ""){
		
		$bankCharges = $_POST["bankCharges"];
		} else{
			$bankCharges = $transactionQuery["bankCharges"];	
			}
			
if($_POST["refNumber"] != ""){			
	
			$refNumber=$_POST["refNumber"];
	
}else{
			$refNumber=$transactionQuery["refNumber"];
		}
	
	if($_POST["remarks"] != ""){			
	
			$remarks=$_POST["remarks"];
	
}else{
			$remarks=$transactionQuery["remarks"];
		}
	
	
	$discount=$transactionQuery["discounted_amount"];	
	
$refNumberIM=$transactionQuery["refNumberIM"];


$printAddress = $strAddressQuery["agentAddress"];
$printPhone = $strAddressQuery["agentPhone"];
$printSenderNo = $CustomerData["accountName"];
$agentUserName = $strAddressQuery["username"];
$agentName = $strAddressQuery["name"];
$custAgentParentID = $strAddressQuery["parentID"];

$distributorId = $transactionQuery["benAgentID"];

$distributorNameQuery = selectFrom("select name from ". TBL_ADMIN_USERS." where userID ='".$distributorId."'");
$distributorName = $distributorNameQuery["name"];

$currencyNameQuery = selectFrom("select description from ". TBL_CURRENCY." where currencyName ='".$currencyTo."'");
$currencyName = $currencyNameQuery["description"];


$fontColor = "#800000";
	
if(CONFIG_SENDER_ACCUMULATIVE_AMOUNT == "1"){

$senderAccumulativeAmount = 0;
//$senderTransAmount = selectMultiRecords("select accumulativeAmount from ".TBL_CUSTOMER." where customerID = '".$_POST["customerID"]."'");


           
            		 $to = getCountryTime(CONFIG_COUNTRY_CODE);
									
									$month = substr($to,5,2);
									$year = substr($to,0,4);
									$day = substr($to,8,2);
									$noOfDays = CONFIG_NO_OF_DAYS;
									
									$fromDate = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d")-$noOfDays ,   date("Y")));

									
									$from = $fromDate." 00:00:00"; 
									$senderTransAmount = selectFrom("select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'");
								// echo "select  sum(transAmount) as transAmount from ".TBL_TRANSACTIONS." where customerID = '".$_POST["customerID"]."' and transDate between '$from' and '$to' AND transStatus != 'Cancelled'";
 $senderAccumulativeAmount += $senderTransAmount["transAmount"];
 

	if($_POST["transID"] == "")
	{
 				$senderAccumulativeAmount += $_POST["transAmount"];
	}
}

?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<style type="text/css">
<!--
.style1 {font-size: small}

-->
</style>
    </style>
    
 <script language="javascript">

	function showDetails() {
		 
			alert("disable");
			document.addTrans.Submit.Disabled= "disabled";
	}
	

function checkForm(theForm,strval){
	
		var minAmount = 0;
		var maxAmount = 0;
	var accumulativeAmount = 0;
	var ruleFlag=false;
	accumulativeAmount = <? echo $senderAccumulativeAmount ?>;
			
		
	var transSender = '';
	if(strval == 'Yes')
	{
		transSender="transSend";
		}else{
			transSender="NoSend";
			}
	var amountToCompareTrans; 
	var transAmount=<?=$_POST["transAmount"]?>;		

// cummulative amount/////////
	<?
	if($cumulativeRule["matchCriteria"] == "BETWEEN"){
		
		
		  $betweenAmount= explode("-",$cumulativeRule["amount"]);
		    $fromAmount=$betweenAmount[0];   
		    $toAmount=$betweenAmount[1]; 
		
 }else{
 	?>
 		    amountToCompare = <?=$cumulativeRule["amount"]?>;	
 <?	}?>
  	
  	////////////current transaction amount///////
  	
  		<?
	if($currentRule["matchCriteria"] == "BETWEEN"){
				
		  $betweenAmountCurrent= explode("-",$currentRule["amount"]);
		  $fromAmountCurrent=$betweenAmountCurrent[0];   
		  $toAmountCurrent=$betweenAmountCurrent[1]; 
		
 }else{
 	?>
 		    amountToCompareTrans = <?=$currentRule["amount"]?>;	
 <?	}?>

		
		<?
	 if($cumulativeRule["matchCriteria"] != "BETWEEN"){	
	 	?>
		 if(accumulativeAmount  <?=$cumulativeRule["matchCriteria"]?> amountToCompare)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();
        ruleFlag=false;
      }

    }else{ 

    	   ruleFlag=true;
    } 
 <? } ?>
 ///////BETWEEN Accumulative Amount/////////////////
 <?
 if($cumulativeRule["matchCriteria"] == "BETWEEN"){
 	?>
 if(<?=$fromAmount?> <= accumulativeAmount && accumulativeAmount <= <?=$toAmount?>)
	   {

				
	   	if(confirm("<?=$cumulativeRule["message"]?>"))
    	{
    	    
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		 
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	addTrans.localAmount.focus();
         ruleFlag=false;
      }

    }else{ 

    	    ruleFlag=true;
    } 
<? }  ?>

 ///Second Rule Compare transAmount with compliance rule amount  /////////////////////////////////
 <?
 if($currentRule["matchCriteria"] != "BETWEEN"){
 ?>
 if(transAmount  <?=$currentRule["matchCriteria"]?> amountToCompareTrans)
	   {

				
	   	if(confirm("<?=$currentRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
    
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   				
      	addTrans.localAmount.focus();
        ruleFlag=false;
      }

    }else{ 

    	     ruleFlag=true; 
    } 
   <? } ?> 
 <?
  ///////BETWEEN Current Transaction/////////////////
 if($currentRule["matchCriteria"] == "BETWEEN"){
 	?>
 if(<?=$fromAmountCurrent?> <= transAmount && transAmount <= <?=$toAmountCurrent?>)
	   {

				
	   	if(confirm("<?=$currentRule["message"]?>"))
    	{
    	
    			document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
    		
                
      }else{ 
             
    				document.addTrans.action='add-transaction.php?transID=<? echo $_POST["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   					
      	   addTrans.localAmount.focus();
           ruleFlag=false;
      }

    }else{ 

    	  	   ruleFlag=true;

    } 
<? }  ?>
   
   if(ruleFlag){
   	document.addTrans.action='add-transaction-conf.php?transID=<? echo $_GET["transID"]?>&focusID=transSend&transSend='+transSender; document.addTrans.submit();
   	}
	
	
}		
	
	</script>
    
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="40%" border="0" cellspacing="1">
  <tr>
    <th scope="col"><div align="left">
    	<?=printLogoOnReciept($_SESSION["loggedUserData"]["userID"],$loggedUName)?>
    </th>
    <th class="style1" scope="col">&nbsp;</th>
  </tr>
  <tr>
    <td><span class="style1">Date</span></td>
    <td><? echo  $today ;?></td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td colspan="2"><div align="center" class="style1" ><strong>Sender</strong></div></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1"></span></td>
  </tr>
  <tr>
    <td><span class="style1">Ref No </span></td>
    <td class="style1"><? echo  $refNumberIM ;?></td>
  </tr>
  <tr>
  	<? 
			$queryCust = "select *  from ".TBL_CUSTOMER." where customerID ='" . $customerID . "'";
			$customerContent = selectFrom($queryCust);
			
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $beneficiaryID . "'";
			$benificiaryContent = selectFrom($queryBen);
			
		?>
    <td><span class="style1">Full name </span></td>
    <td class="style1"><? echo $customerContent["firstName"] ." ".  $customerContent["lastName"];?></td>
  </tr>
  <tr>
    <td><span class="style1">Date of Birth </span></td>
   
    <td class="style1"><? /*echo $customerContent["dob"];*/ echo date("d-m-Y", strtotime($customerContent["dob"]));?></td>
  </tr>
  <tr>
    <td><span class="style1">Contact No </span></td>
    <td class="style1"><? echo $customerContent["Phone"];?></td>
  </tr>
  <tr>
  	<? if ($customerContent["IDType"] == "Other ID"){ 
  							
  							$IDNoType =  $customerContent["otherId"];
  							$IDNo =  $customerContent["otherId_name"];
  		 }else{ 
  		 	
  		 					$IDNoType =  $customerContent["IDType"];
  		 					$IDNo =  $customerContent["IDNumber"];
  		 				}
  		 	
  		 	?>
    <td><span class="style1">Sender's ID </span></td>
    <td class="style1"><? echo $IDNoType; ?></td>
  </tr>
  <tr>
    <td><span class="style1">ID Number </span></td>
    <td class="style1"><? echo $IDNo; ?></td>
  </tr>
  <tr>
    <td><span class="style1">Sender's Address </span></td>
    <td class="style1"><? echo $customerContent["Address"];?></td>
  </tr>
  
  <tr>
    <td><span class="style1"></span></td>
    <td class="style1">&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td colspan="2"><div align="center" class="style1"><strong>Reciever</strong></div></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1"></span></td>
  </tr>
  <tr>
    <td><span class="style1">Full name </span></td>
    <td class="style1"><? echo $benificiaryContent["firstName"]." ".  $benificiaryContent["lastName"];?></td>
  </tr>
  <tr>
    <td><span class="style1">S/O</span></td>
    <td class="style1"><? echo $benificiaryContent["SOF"]?></td>
  </tr>
  <tr>
    <td><span class="style1">Destination</span></td>
    <td class="style1"><? echo $benificiaryContent["City"]; ?></td>
  </tr>
  <tr>
    <td><span class="style1">Remarks</span></td>
    <td class="style1"><? echo $remarks; ?></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1"></span></td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td colspan="2"><div align="center" class="style1"><strong>Payment</strong></div></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1"></span></td>
  </tr>
  <tr>
    <td><span class="style1">Amount Sent </span></td>
    <td class="style1"><? echo number_format($transAmount,$roundLevel,'.',','); ?></td>
  </tr>
  <tr>
    <td><span class="style1">Transfer Fee </span></td>
    <td class="style1"><? echo $IMFee; ?></td>
  </tr>
  <tr>
    <td><span class="style1">Total Collected </span></td>
    <td class="style1"><? echo number_format($totalAmount,$roundLevel,'.',','); ?></td>
  </tr>
  <tr>
    <td colspan="2"><span class="style1"></span></td>
  </tr>
  <tr>
    <td><span class="style1">Exchange Rate </span></td>
    <td class="style1"><? echo $exchangeRate; ?></td>
  </tr>
  <tr>
    <td><span class="style1">Recieved Amount </span></td>
    <td class="style1"><? echo number_format($localAmount,$roundLevel,'.',','); ?></td>
  </tr>
  <tr>
    <td><span class="style1">Transaction Type </span></td>
    <td class="style1"><? echo $transactionType; ?></td>
  </tr>
  <tr>
    <td><span class="style1"></span></td>
    <td class="style1">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#CCCCCC"><div align="center" class="style1"><strong>Address</strong></div></td>
  </tr>
  <? if($transactionType== 'Pick up'){
	              			$queryColl = "select *  from ".TBL_COLLECTION." where cp_id ='".$_SESSION["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);
										
											?>
  <tr>
    <td colspan="2">
    	<table width="100%" border="0" cellspacing="1">
      <tr>
        <td><? echo $destinCollection["cp_contact_person_name"];?></td>
      </tr>
      <tr>
        <td class="style1"><? echo $destinCollection["cp_branch_address"];?></td>
      </tr>
      <tr>
        <td class="style1"><? echo $destinCollection["cp_city"];?></td>
      </tr>
      <tr>
        <td class="style1"><? echo $destinCollection["cp_phone"];?></td>
      </tr>
    </table></td>
    
  </tr>
  
<? }elseif($transactionType== 'Bank Transfer'){ ?>
  <tr>
    <td><span class="style1">Bank Name</span></td>
    <td class="style1"><? echo $distributorName;?></td>
  </tr>
<? } ?>

<tr>
   <td>&nbsp;</td>	
</tr>
 <tr>
	   <td><strong>Agent Signature</strong>&nbsp;---------------------- </td>
</tr>
<tr>
   <td>&nbsp;</td>	
</tr>
 <tr>
	   <td><strong>Sender Signature</strong>&nbsp;---------------------- </td>
	  
 </tr>
 <tr>
 <td colspan="2"><strong><hr color="black" noshade></strong></td>
 </tr>
 <tr>
   <td><strong>10 CHURCH ROAD ACTON W3 8PP</strong></td>
 </tr>
<tr>
   <td><strong>Tel: 020 8993 4236 Fax: 020 8993 2472</strong></td>
 </tr> 
</table>
<form name="addTrans" action="add-transaction-conf.php?r=<?=$_GET["r"]?>" method="post">
        <table width="100%" border="0" cellspacing="1">
        	<tr><td align="center"> 	
		<div class='noPrint'>
			
           <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
				  <input type="hidden" name="imReferenceNumber" value="<?=$refNumberIM;?>">
				  <input type="hidden" name="refNumber" value="<?=$_SESSION["refNumber"];?>">
				  <input type="hidden" name="mydDate" value="<?=$nowTime;?>">
					<input name="distribut" type="hidden" value="<? echo $_POST["distribut"];?>">	
					<input name="transID" type="hidden" value="<? echo $_POST["transID"];?>">					            
           
          	  <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;//window.open('printing_reciept.php?transID=< echo $imReferenceNumber; >', 'searchBen', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')

	  ?>
	  		
			<? if($success=='Y'){
				
				include "mailRecieptRichAlmond.php";
				$To = $customerContent["Email"];
				
			 $Subject = "New Transaction created";
			 $Name = $customerContent["firstName"];
		 $From = SUPPORT_EMAIL;
				sendMail($To,$Subject,$data,$From, $From);
				?>
	
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
			
		</div>
	</td>
		<? }else{ ?>
	<input type="Submit" name="confirmOrder" value="Confirm Order" id="once" onclick="javascript:document.getElementById('once').disabled=true;document.addTrans.submit();<? if(CONFIG_COMPLIANCE_PROMPT_CONFIRM== "1"){ ?> checkForm(addTrans,'yes')<? } ?>" >

<a href="<?=$returnPage?>?transID=<? echo $_POST["transID"]?>&from=conf" class="style2">Change Information</a>
</td>
		<? } ?>
		</tr>
			</table>
         </form>

</body>
</html>