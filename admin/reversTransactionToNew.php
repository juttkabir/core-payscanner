<?php
session_start();
include ("../include/config.php");
include ("security.php");

//debug($_REQUEST);
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$currentDate = date('d/m/Y');

$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$date_time = date('d-m-Y-h-i-s-A');
$amount2 = 0;
$flagExport = true;


if(!empty($_REQUEST["reverse"]) && $_REQUEST["reverse"] == "Reverse To New")
{
	if(!empty($_REQUEST["trnsid"]))
	{
		foreach($_REQUEST["trnsid"] as $tKey => $tVal)
		{
			$strUpdateTransactionSql = "update ".TBL_TRANSACTIONS." set is_exported = '' where transID='".$tVal."'";
			//debug($strUpdateTransactionSql);
			update($strUpdateTransactionSql);
		}
	}
}


if($_REQUEST["fileID"] != "")
	$fileID = $_REQUEST["fileID"];



$condition = "";
$value = "";
if(CONFIG_EXPORT_TRANS_OLD == '1')
{
	$condition = " and is_exported = 'Y' ";
	$value = "old";
}

if(CONFIG_SEPARATE_LINK_FOR_BOTH_TRANSACTIONS == 1 && !empty($_REQUEST["ttype"]))
{
	if($_REQUEST["ttype"] == 1)
		$condition .= " and transType = 'Pick up' ";
	elseif($_REQUEST["ttype"] == 2)
		$condition .= " and transType = 'Bank Transfer' ";
}


if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	
	$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
		
	$condition .= " and transStatus = 'Authorize' ".$FileConditionQuery["conditions"]." ";
}
else
{
	if(!empty($_REQUEST["user"]))
		$condition .= " and benAgentID = '".$_REQUEST["user"]."'";
	else
		$condition .= " and benAgentID = '100289'";
}

//$condition .= " and transStatus = 'Authorize'";

 $queryCnt = "select COUNT(*) from ". TBL_TRANSACTIONS." where 1 $condition ";
$countTrans = countRecords($queryCnt);

$disticntCurrency = selectMultiRecords ("select DISTINCT(currencyTo) from ".TBL_TRANSACTIONS." where 1 $condition");
$curr = explode(',',$disticntCurrency);



/*
if ($countTrans <= 0) {
	if ($value != "") {
		insertError("There is no ".$value." transaction to export.");
	} else {
		insertError("There is no new transaction to export.");
	}

	redirect("main.php?msg=Y");
}
*/
if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
} else {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and File_Name = 'export_trans_file.php'");
}

$lineBreak = $fileFormatQuery["lineBreak"];
$fieldSpacing = $fileFormatQuery["fieldSpacing"];
if ($fieldSpacing == 'tab') {
	$fieldSpace = "\t";	
} else {
	$fieldSpace = " ";
}
$fileFormat = $fileFormatQuery["Format"];
$fileID = $fileFormatQuery["id"];
$fileNamed = $username."_".$date_time."_IN";

	if ($fileFormat == "txt") {
		$appType = "txt";
	} else if ($fileFormat == "csv") {
		$appType = "csv";
	} else {
		$appType = "x-msexcel";	
	}
 $query = "select * from ". TBL_TRANSACTIONS." where 1 $condition ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	$query .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";

$contentTrans = selectMultiRecords($query);
//debug($query);
//debug($contentTrans);

$fileFormat = "xls";

if ($fileFormat == "xls")
{

	
	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	$data .= "<form action='reversTransactionToNew.php' method='post'>";
if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){/*
	$data = "<table width='900' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	
	
	
	$data .="<tr>";
	
			$data .= "<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["heading"]."</font>\n";	
		
		
			if($labelQuery["address"] == 'Y'){
		
			$data .= "<font face='Verdana' color='#000000' size='2'><b>".COMPANY_ADDR."</font></td>\n";
		}
		
		
		
		$data .="</tr>";
		$data .="<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label1"]."</font></td>\n</tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label2"]." ".$currentDate." ".$labelQuery["label3"]."</font></td>\n</tr>
						<tr><td>&nbsp;</td></tr>
						</table>";
	
		
	*/}
	$data .= "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	if($fileFormatQuery["showLable"]== "Y"){	///if Labels are enabled
		$data .="<tr><td>&nbsp;</td>";
	}
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	
	//debug($fieldQuery);	
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
		//if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME == $fileFormatQuery["FileLable"]){
		if($fileFormatQuery["showLable"]== "Y"){
			/////If Labels are enbled
			$data .= "<td><font face='Verdana' color='#000000' size='2'><b>".$fieldQuery[$j]["Lable"]."</font></td>\n";	
		}
	//}
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
		
	if($fieldQuery[$j]["isPattern"]== 'Y'){		
	
	   $patternValue[$j] = $fieldQuery[$j]["patternType"];
   }else{
   	  $patternValue[$j] = '';
   	 }			
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
	

		
		//$data .="</tr>";
	}
	if($fileFormatQuery["showLable"]== "Y"){////If Labels are enabled	
		$data .="</tr>";
	}

	
	for($i=0;$i < count($contentTrans);$i++)
	{
		
		//debug($contentTrans[$i]["transID"]);
		$flagExport = true;
		if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_GENERATE_DIST_REF_ON_EXPORT == '1' && CONFIG_REF_IN_EXTRA_INFO  != '1')
		{
			$refData = generateDistRef($contentTrans[$i]["transID"]);
			$flagExport = $refData[0];
			$distRefNum = $refData[1];
		}
		if(CONFIG_TRANS_EXTRA_INFO == '1')
		{
			/*
			if ($_GET['old'] != 'Y')
			{
				updateExtraInfo($contentTrans[$i]["transID"]);	
			}
			*/
			$transExtendContent = selectFrom("select * from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentTrans[$i]["transID"]."'");
		}
		 $exchangeData = getMultipleExchangeRates($contentTrans[$i]["fromCountry"], $contentTrans[$i]["toCountry"],$contentTrans[$i]["currencyTo"], $contentTrans[$i]["benAgentID"], 0 ,'' , 'USD',$contentTrans[$i]["transType"],$contentTrans[$i]["moneyPaid"], $contentTrans[$i]["custAgentID"]);
		 $exID = $exchangeData[0];
		$exRate = $exchangeData[1];
		$contentTrans[$i]["localAmount"];	
	  //$usdAmount =  number_format(round($contentTrans[$i]["localAmount"]/$exRate,2),2,'','');	
	  	
		if(!empty($exRate))
			$usdAmount =  round($contentTrans[$i]["localAmount"]/$exRate,2);	
		
		if($flagExport)
		{
			if (CONFIG_EXPORT_TRANS_OLD == '1') {
				update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
			}
			$refNumberIM = $contentTrans[$i]["refNumberIM"];
			$transDate = $contentTrans[$i]["transDate"];
			$companyCode = $contentTrans[$i]["refNumber"];
			$customerID = $contentTrans[$i]["customerID"];
			$customerName = "";
			$beneName = "";
			if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
			{		  
			   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
			   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
			   
			   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
			   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			   $accountName = $customerContent["username"];
			   $beneAddress = $beneContent["Address"];
			   $beneCity = $beneContent["City"];
			   $beneState = $beneContent["State"];
			   $benePhone = $beneContent["Phone"];
			   $beneCPF = $beneContent["CPF"];
			}else
				  {
			  	$customerContent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
			  	$beneContent = selectFrom("select * from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
			  	
			  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
			  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
			  	$accountName = $customerContent["accountName"];
			  	$beneAddress = $beneContent["Address"];
			  	$beneAddress1 = $beneContent["Address1"];
			    $beneCity = $beneContent["City"];
			    $beneState = $beneContent["State"];
			    $benePhone = $beneContent["Phone"];
			    $beneMobile = $beneContent["Mobile"];
			    $beneCPF = $beneContent["CPF"];
			  }
			$gbpAmount = $contentTrans[$i]["transAmount"];
			$exchRate = $contentTrans[$i]["exchangeRate"];
			$foriegnAmount = $contentTrans[$i]["localAmount"];
			$toCountry = $contentTrans[$i]["toCountry"];
			$currencyFrom = $contentTrans[$i]["currencyFrom"];
			$currencyTo = $contentTrans[$i]["currencyTo"];
			$type = $contentTrans[$i]["transType"];
			$transTotal += $gbpAmount;
			$localTotal += $foriegnAmount;
			$collection = '';
			$bankName = '';						
			$account = '';						
			$brCode = '';						
			$brAddress = '';			
			if($contentTrans[$i]["transType"]=='Pick up')
			{
			$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
			$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
			$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
			}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
				{
					if($contentTrans[$i]["createdBy"] == "CUSTOMER")
						{
							$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
						
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];
							$accountType = $bankDetails["accountType"];						
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
							}else{
							$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
							
							$bankName = $bankDetails["bankName"];						
							$account = $bankDetails["accNo"];			
							$accountType = $bankDetails["accountType"];		
							$brCode = $bankDetails["branchCode"];						
							$brAddress = $bankDetails["branchAddress"];
							$IBAN = $bankDetails["IBAN"];
							$Remarks = $bankDetails["Remarks"];
						}
						$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
						$bankNumber	= $bankNumQuery["bankCode"];				
				}

				$agnetDetails = selectFrom("select userID,username,agentCommission from ".TBL_ADMIN_USERS." where userID = '".$contentTrans[$i]["custAgentID"]."'");
			
		$data .= " <tr>";
		$data .= "<td><input type=checkbox name=trnsid[] value='".$contentTrans[$i]["transID"]."'></td>";
			for($k = 0; $k < count($dataVariable); $k ++)
			{
				$value = "";
				$field = explode(',',$dataVariable[$k]); // this it to break a string on any index of the array[array containing database fields]
			
	      if($fixedValue[$k] != '')
	      {
	      	$value .= $fixedValue[$k]; 

	      }elseif($patternValue[$k]!= '' ){
				      if($patternValue[$k] == 'DISTRIBUITOR  SUB REF'){
					     $value .= $distRefNum;
				       }elseif($patternValue[$k] == 'DOLAR AMOUNT'){
				      	 $value .= $usdAmount;
				      	}elseif($patternValue[$k] == 'SETTLEMENT_EXCHANGE_RATE'){
				      			$value .= $exRate;
				      		}
				}else{

									

				/*
	values for the fields are pulled from the respective tables in the database
	
	*/
						if($dataTable[$k] == 'transactions')
							{
							
								for($ex = 0; $ex < count($field); $ex++){
									
										if($ex > 0){
												$value .= " ";
											}								
									
								//}
									if($field[$ex] == "localAmount")
									{
										if($contentTrans[$i]["toCountry"] == "United Kingdom" &&  CONFIG_TOCOUNTRY_UK == '1'){
											$amount2 +=  $contentTrans[$i]["localAmount"];
										}else{
											$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
											$amount +=  $contentTrans[$i]["localAmount"];
											$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
										}
									}
										$value .= $contentTrans[$i][$field[$ex]];
										
																
								}
												
							}elseif($dataTable[$k] == 'beneficiary'){
								for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$beneContent[$field[$ex]];
											
								}elseif($dataTable[$k] == 'customer'){
								
								for($ex = 0; $ex < count($field); $ex++){
								      
								       /*	if($field[$ex] == 'Phone'){
												
												$value .=$usdAmount;
												}*/
								      
											$value .= " ".$customerContent[$field[$ex]];					
								  }
							}elseif($dataTable[$k] == 'bankDetails'){
									
									for($ex = 0; $ex < count($field); $ex++)
									{
										if($ex > 0){
												$value .= " ";
											}
									
												$value .= $bankDetails[$field[$ex]];					
									}
								}elseif($dataTable[$k] == 'admin'){
									for($ex = 0; $ex < count($field); $ex++){
									  	$value .= $agnetDetails[$field[$ex]];
									  }
									}elseif($dataTable[$k] == 'cm_collection_point'){
									  	for($ex = 0; $ex < count($field); $ex++){
									  			if($contentTrans[$i]["transType"] == 'Bank Transfer'){
												$value .= "None ";
												} else{
									  		$value .= $collectionPoint[$field[$ex]];
									  	 }
									  }
									}elseif($dataTable[$k] == 'transactionExtended'){
										for($ex = 0; $ex < count($field); $ex++)
											$value .= " ".$transExtendContent[$field[$ex]];
											
								}
									
							if($value == "")
							{
									$value = "None";
								}
												
					}	
						if($dataVariable[$k] == 'transDate')
						{
							if($fileFormatQuery["FileLable"] == "Transactions For Georgia"){
								$value = dateFormat($value, 4);
							}elseif($fileFormatQuery["FileLable"] == "Transactions For Lithuania"){
								$value = dateFormat($value, 3);
							}else{
								$value = dateFormat($value, 1);
							}
							
						}	
										$data .="<td><font color='#000000' size='1' face='Verdana'>&nbsp;$value</font></td>";
				}
				$data .="</tr>";
		}

	}
	if(count($contentTrans) < 1)
		$data .="<td align='center' colspan='".(count($fieldQuery)+1)."'><font color='red' size='1' face='Verdana'>No Records to display.</font></td>";
			
	$data .= "<input type='hidden' name='old' value='".$_REQUEST["old"]."' />";
	$data .= "<input type='hidden' name='fileID' value='".$_REQUEST["fileID"]."' />";
	$data .= "<input type='hidden' name='mFile' value='".$_REQUEST["mFile"]."' />";
	$data .= "<input type='hidden' name='user' value='".$_REQUEST["user"]."' />";
	$data .= "<input type='hidden' name='ttype' value='".$_REQUEST["ttype"]."' />";
	$data .= "<input type='submit' name='reverse' Value='Reverse To New' /></table></form>";
	}			
echo $data;

?>
<input type="button" name="export" value="Export All Transactions" onClick="document.location='<?=$_REQUEST["mFile"]?>?fileID=<?=$_REQUEST["fileID"]?>&old=<?=$_REQUEST["old"]?>&ttype=<?=$_REQUEST["ttype"]?>'" />
