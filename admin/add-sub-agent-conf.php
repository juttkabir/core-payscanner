<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
$agentType2 = getAgentType();
$parentID2 = $_SESSION["loggedUserData"]["userID"];


define("PIC_SIZE","51200");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/*	#4966 - AMB Exchange
	Operator ID is added in AnD form.
	to be displayed in Western Union Output export file.
	by Aslam Shahid
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1" && $_REQUEST["ida"]=="ND" ){
	$operatorFlag = true;
}
$agentIDNumberFlag = false;
if(defined("CONFIG_SUB_AGENT_DIST_ID_NUMBER") && CONFIG_SUB_AGENT_DIST_ID_NUMBER=="1"){
	$agentIDNumberFlag = true;
}

/*	#4966 
	Company Type dropdown added on agents form and Trading field also added.
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$typeV = $_GET["type"];
}
if ($_GET["ida"] != ""){
	$ida = $_GET["ida"];
}

if ($_POST["notes"] != ""){
	$notes = $_SESSION["notes"] = $_POST["notes"];
}
$pageID = $_POST["pageID"];
if ($_REQUEST["mode"] != ""){
	$mode = $_REQUEST["mode"];
}
$currencyOrigin = (isset($_REQUEST["currencyOrigin"])) ? $_REQUEST["currencyOrigin"] : "";


switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_COUNTRY." where countryName='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}
$IDAcountry = (is_array($_POST["IDAcountry"]) ? implode(",", $_POST["IDAcountry"]) :"");
$authorizedFor = (is_array($_POST["authorizedFor"]) ? implode(",", $_POST["authorizedFor"]) :"");

$backDays = "";


if ($_POST["rights"] == 'Backdated') {
	if ($agentType == 'admin') {
		if ($_POST['backDays'] == "") {
			$backDays	= '1';
		} else {
			$backDays = $_POST['backDays'];
		}
	}
}

	$supAgentID = $_POST["supAgentID"];



	$isOnline = $_POST["isOnline"];
	
foreach ($_POST as $k=>$v) 
{
	$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
	$str .= "$k\t\t:$v<br>";
}
/*
if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y") { 
     if (trim($_POST["manualAgentNum"]) == "") { 
        insertError(AG42) ; 
        redirect($backURL) ;     
     } else { 
       if($_POST["userID"] == "") { 
       		$qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `username` = '".trim($_POST["manualAgentNum"])."'" ; 
       }else{ 
        	$qUserName .= " and userID != '".$_POST["userID"]."'";
   	   }
        $qUserRes = selectFrom($qUserName) ; 
        if ($qUserRes["username"] != "") { 
            insertError(AG43) ; 
            redirect($backURL) ;     
        }
        
   		if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$agentCode."'")) {
			insertError(AG43);
			redirect($backURL);
		}
		if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$agentCode."'")) {
			insertError(AG43);
			redirect($backURL);
		}
		if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$agentCode."'")) {
			insertError(AG43);
			redirect($backURL);
		}

         
     } 
} 






	
/*echo $supAgentID;
echo "<br>";

exit();*/
//echo  implode(",", $_POST["IDAcountry"]);
//echo implode(" ", $arr );
//exit;
if ($_POST["userID"] == ""){

	
	
	////$_GET["ida"] == "Y"
	
	//$_SESSION["IDAcountry"] = implode(",", $_POST["IDAcountry"]);
	if($_POST["correspondent"] == "ONLY")
	{
		$backURL = "add-sub-agent.php?msg=Y&type=$typeV&ida=$ida&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
	}else{
		$backURL = "add-sub-agent.php?msg=Y&type=$typeV&ida=$ida&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country$Country&agentStatus=$agentStatus";
		}
} else {
	
	if($_POST["correspondent"] == "ONLY")
	{
		$backURL = "update-sub-agent.php?userID=$_POST[userID]&msg=Y&ida=$ida&type=$typeV&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
	}else{
		$backURL = "update-sub-agent.php?userID=$_POST[userID]&msg=Y&ida=$ida&type=$typeV&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$Country&agentStatus=$agentStatus";
		}
} 
if (trim($_POST["agentCompany"]) == ""){
	insertError(AG1);
	redirect($backURL);
}
if (trim($_POST["agentContactPerson"]) == ""){
	insertError(AG2);
	redirect($backURL);
}
if (trim($_POST["agentAddress"]) == ""){
	insertError(AG3);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(AG4);
	redirect($backURL);
}


if(CONFIG_PAYIN_AGENT == '1' && $ida!= 'Y')
	{

if (trim($_POST["ch_payinBook"]) != "" && trim($_POST["payinBook"]) == ""){
	

		insertError(AG46);
		redirect($backURL);

}



if (trim($_POST["payinBook"]) != ""){
	
	if (trim($_POST["ch_payinBook"]) == ""){	
	insertError(AG47);
	redirect($backURL);
	}	
	
		if (!is_numeric($_POST["payinBook"])){
		insertError(AG48);
		redirect($backURL);
		}
		
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		
		if ($payinBookNumber < CONFIG_AGENT_PAYIN_NUMBER ){	
		insertError(AG49." ".CONFIG_AGENT_PAYIN_NUMBER);
		redirect($backURL);
		}
		
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber > CONFIG_AGENT_PAYIN_NUMBER ){	
		insertError(AG50." ".CONFIG_AGENT_PAYIN_NUMBER);
		redirect($backURL);
		}
	
}	
	
		if ($_POST["payinBook"] != ""){
			if (isExist("select payinBook from ".TBL_CUSTOMER." where payinBook = '".$_POST["payinBook"]."' ")){
				insertError(AG52);
				
				redirect($backURL);
			}
		}
	

}

/*
if (trim($_POST["City"]) == ""){
	insertError(AG5);
	redirect($backURL);
}
if (trim($_POST["agentZip"]) == ""){
	insertError(AG6);
	redirect($backURL);
}*/
if (trim($_POST["agentPhone"]) == ""){
	insertError(AG7);
	redirect($backURL);
}


if (CONFIG_FREETEXT_IDFIELDS == '1') {
	$maxFutureYear = date("Y") + CONFIG_IDEXPIRY_YEAR_DELTA;
	if ($_POST["dDate"] != "") {
		$dDate = explode("-", $_POST["dDate"]);
		
		// Pass an array of date into this function and it returns true or false...
		if (isValidDate($dDate)) {
			if ($dDate[2] <= $maxFutureYear) {
				$idExpiryDate = $dDate[2] . "-" . $dDate[1]	. "-" . $dDate[0];
				if ($idExpiryDate <= date("Y-m-d")) {
					insertError(AG44);
					redirect($backURL);
				}
			} else {
				insertError("ID expiry year should not be greater than " . $maxFutureYear . ". If so, then contact to your admin.");
				redirect($backURL);
			}
		} else {
			insertError("Please provide format of ID expiry like: DD-MM-YYYY");
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";	
	}
} else if (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {
	if ($_POST["dDate"] != "") {
		$dDate = explode("/", $_POST["dDate"]);
		$idExpiryDate = $dDate[2] . "-" . $dDate[1]	. "-" . $dDate[0];
		if ($idExpiryDate <= date("Y-m-d")) {
			insertError(AG44);
			redirect($backURL);
		}
	} else {
		$idExpiryDate = "";	
	}
} else {
	$idExpiryDate = $_POST["idYear"] . "-" . $_POST["idMonth"] . "-" . $_POST["idDay"];
}

if ($logo_name != "")
{
	if ($logo_size > PIC_SIZE)
	{
		insertError("Image size must be less than 50K");
		redirect($backURL);
	}
	if (extension($logo_name)!=1)
	{
		insertError("Invalid file format. Only gif, jpg and png are allowed");
		redirect($backURL);
	}
}


$agent_type = 'sub';

$uploadImageSubVar = $_POST["uploadImage"];
if ($_POST["userID"] == ""){

	$password = createCode();
	$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		if((CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] == "") || (CONFIG_MANUAL_SUB_AnD_NUMBER=="1" && $_GET["ida"] == "ND")){
			$agentCode = trim($_POST["manualAgentNum"]) ; 
			if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE lower(accountName) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE lower(accountName) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
		} else { 
		  $agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov 
		} 

		/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
		$assocDistDBField = "";
		$csvAssocDistIDs = "";

		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1
			&& !empty($_REQUEST["assocDistributors"]) )
		{
			$assocDistDBField = "associated_distributors";
			$csvAssocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );
			$assocDistUpdateClause = " associated_distributors='" . $csvAssocDistIDs . "', ";
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/

		///////////////////////////////////
		if (isset($_POST["associatedAgent"])) {
			$commaSeperatedUserIDs = "";
			for ($i=0;$i<count($_POST["associatedAgent"]);$i++) {
				$associatedAgentValue=$_POST["associatedAgent"][$i];
				$array = explode("(", $associatedAgentValue);
				$name = $array[0];
				$userIDs = selectFrom("select userID from admin where name like '%$name%'");
				if ($i===count($_POST["associatedAgent"])-1)
					$commaSeperatedUserIDs .= $userIDs["userID"];
				else
					$commaSeperatedUserIDs .= $userIDs["userID"].", ";
			}

			$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." 
		(linked_Agent, username, password, changedPwd, name, email, created, 
		rights, backDays, parentID,	agentNumber, agentCompany, 
		agentContactPerson, agentAddress, agentAddress2, agentCity, 
		agentZip, agentCountry,	agentCountryCode, agentPhone, agentFax, 
		agentURL, agentMSBNumber,agentHouseNumber, agentMCBExpiry, agentCompRegNumber, 
		agentCompDirector, agentDirectorAdd, agentProofID, agentIDExpiry, 
		agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit,  " . (!empty($assocDistDBField) ? $assocDistDBField . ", " : "") . " 
		commPackage ,agentCommission, agentStatus, isCorrespondent, IDAcountry,
		 agentType, accessFromIP, authorizedFor, postCode,payinBook, isOnline,paymentMode,settlementCurrency,hasClave,distAlias,notes) VALUES 
		('$commaSeperatedUserIDs', '$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 
		'".$_POST["rights"]."', '".$backDays."', '".$supAgentID."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		'".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', 
		'".$_POST["agentZip"]."', '".$_POST["Country"]."', '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		'".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."','".checkValues($_POST["agentHouseNumber"])."', '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		'".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', '".checkValues($_POST["agentProofID"])."', '".$idExpiryDate."', 
		'".$_POST["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', '".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', '".checkValues($_POST["agentAccountLimit"])."',
		" . (!empty($csvAssocDistIDs) ? " '" . $csvAssocDistIDs . "', " : "") . "
		'".checkValues($_POST["commPackage"])."', '".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."',	'".$IDAcountry."',
		'$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($authorizedFor)."','".$_POST["postcode"]."','".$_POST["payinBook"]."', '".$_POST["isOnline"]."', '".$_POST["agentPaymentMode"]."','".$currencyOrigin."','".$_POST["hasClave"]."','".$_POST["distAlias"]."','".$notes."')";

			insertInto($Querry_Sqls);
			$userID = @mysql_insert_id();

			$userIDs = explode(", ", $commaSeperatedUserIDs);
			for ($i=0;$i<count($userIDs);$i++) {
				$associatedAgentValue = $userIDs[$i];
				$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
				$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
				if ($linkedDistributor == "") {
					$linkedDistributor .= $userID;
				} else {
					$linkedDistributor .= ", ".$userID;
				}
				$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
				update($strOverwriteSql);
			}
		} else if (isset($_POST["associatedDistributor"])) {
			$commaSeperatedUserIDs = "";
			for ($i=0;$i<count($_POST["associatedDistributor"]);$i++){
				$associatedDistributorValue=$_POST["associatedDistributor"][$i];
				$array = explode("(", $associatedDistributorValue);
				$name = $array[0];
				$userIDs = selectFrom("select userID from admin where name like '%$name%'");
				if ($i===count($_POST["associatedDistributor"])-1)
					$commaSeperatedUserIDs .= $userIDs["userID"];
				else
					$commaSeperatedUserIDs .= $userIDs["userID"].", ";
			}

			$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." 
		(linked_Distributor, username, password, changedPwd, name, email, created, 
		rights, backDays, parentID,	agentNumber, agentCompany, 
		agentContactPerson, agentAddress, agentAddress2, agentCity, 
		agentZip, agentCountry,	agentCountryCode, agentPhone, agentFax, 
		agentURL, agentMSBNumber,agentHouseNumber, agentMCBExpiry, agentCompRegNumber, 
		agentCompDirector, agentDirectorAdd, agentProofID, agentIDExpiry, 
		agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit,  " . (!empty($assocDistDBField) ? $assocDistDBField . ", " : "") . " 
		commPackage ,agentCommission, agentStatus, isCorrespondent, IDAcountry,
		 agentType, accessFromIP, authorizedFor, postCode,payinBook, isOnline,paymentMode,settlementCurrency,hasClave,distAlias,notes) VALUES 
		('$commaSeperatedUserIDs', '$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 
		'".$_POST["rights"]."', '".$backDays."', '".$supAgentID."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		'".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', 
		'".$_POST["agentZip"]."', '".$_POST["Country"]."', '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		'".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."','".checkValues($_POST["agentHouseNumber"])."', '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
		'".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', '".checkValues($_POST["agentProofID"])."', '".$idExpiryDate."', 
		'".$_POST["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', '".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		'".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', '".checkValues($_POST["agentAccountLimit"])."',
		" . (!empty($csvAssocDistIDs) ? " '" . $csvAssocDistIDs . "', " : "") . "
		'".checkValues($_POST["commPackage"])."', '".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."',	'".$IDAcountry."',
		'$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($authorizedFor)."','".$_POST["postcode"]."','".$_POST["payinBook"]."', '".$_POST["isOnline"]."', '".$_POST["agentPaymentMode"]."','".$currencyOrigin."','".$_POST["hasClave"]."','".$_POST["distAlias"]."','".$notes."')";

			insertInto($Querry_Sqls);
			$userID = @mysql_insert_id();

			$userIDs = explode(", ", $commaSeperatedUserIDs);
			for ($i=0;$i<count($userIDs);$i++){
				$associatedDistributorValue = $userIDs[$i];
				$existingLinkedAgents = selectFrom("select linked_Agent from admin where userID=".$associatedDistributorValue);
				$linkedAgent = $existingLinkedAgents["linked_Agent"];
				if ($linkedAgent == "") {
					$linkedAgent .= $userID;
				} else {
					$linkedAgent .= ", ".$userID;
				}
				$strOverwriteSql = "update admin set linked_Agent = '".$linkedAgent."' where userID=".$associatedDistributorValue;
				update($strOverwriteSql);
			}
		}
		///////////////////////////////////

	//}

	insertInto($Querry_Sqls);
	/*	#4996
		This case handled for BIGINT values
		by Aslam Shahid.
	*/
	$arrLastInsertId = selectFrom("select LAST_INSERT_ID() as li");
	$insertedID = $arrLastInsertId["li"];
	$userID = $insertedID;
	// Added by Niaz Ahmad at 11-05-2009 @4926 Faith Exchange
	if(defined("CONFIG_DISTRIBUTOR_BANK_CHARGES") && CONFIG_DISTRIBUTOR_BANK_CHARGES=="1"){
	update("update ".TBL_ADMIN_USERS." set bankCharges = '".$_POST["distributorBankcharges"]."' where userID='".$insertedID."'");
	}
	if($agentIDNumberFlag){
		update("update ".TBL_ADMIN_USERS." set agentIDNumber = '".$_POST["agentIDNumber"]."' where userID='".$insertedID."'");
	}
	if(!empty($insertedID) && isset($_POST["companyType"]) && $companyTypeFlag){
			update("update " . TBL_ADMIN_USERS. " set companyType='".$_POST["companyType"]."' where userID='".$insertedID."'");
	}
	if(!empty($insertedID) && isset($_POST["tradingName"]) && $tradingNameFlag){
			update("update " . TBL_ADMIN_USERS. " set tradingName='".$_POST["tradingName"]."' where userID='".$insertedID."'");
	}
	if(!empty($insertedID) && isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where userID='".$insertedID."'");
	}
	// PICTURE UPLOAD CODE
	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict = strtolower($agentCode.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($agentCode . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='$agentCode'");
		}
	}


	 if($_REQUEST["ida"] == "ND" && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1")
	 {
		/* Settlement curriencies of A&D */
		$strAgentDistributorSettlementCurrencies = $_REQUEST["settlementCurrencyAsAgent"]."|".$_REQUEST["settlementCurrencyAsDistributor"];
	 	
		$updateSettlementCurrecnySql = "update ".TBL_ADMIN_USERS." set settlementCurrencies = '".$strAgentDistributorSettlementCurrencies."' where username='$agentCode'";
		update($updateSettlementCurrecnySql);
	 }

	
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Admin Account creation at $company";
	$message = "Congratulations! ".$_POST["name"].",\n\n
		Your agent account has been created at $company agent module. Your login information is as follows:\n\n
		Login: ".$agentCode."\n\n
		Password: ".$password."\n\n
		Name: ".$_POST["agentContactPerson"]."\n\n
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a>\n\n

		$company Support";
	sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail);
	
	////To record in History
	$descript = "Agent is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);
	
	
	if($_POST["supAagentID"] != "")
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Super",'Sub', eregi_replace("Agent",'Distributor', AG16)). " The login name is <b>$agentCode</b> and password is <b>$password</b>");		
		}
		else
		{
			insertError(eregi_replace("Super",'Sub', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}
	else
	{
		if($_POST["correspondent"] == "ONLY")
		{
			insertError(eregi_replace("Agent",'Distributor', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");		
		}
		else
		{
			insertError(AG16 . " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		}
	}

		insertError( ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'Distributor', AG16) : AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		insertError(str_replace("Super",'Sub', AG16). " The login name is <b>$agentCode</b> and password is <b>$password</b>");
		$backURL .= "&success=Y";
} else {
	
	if(CONFIG_NO_MANUAL_USERNAME != '1'){
		
		
							if (trim($_POST["oldCountry"]) != trim($_POST["Country"]) && CONFIG_MANUAL_AGENT_NUMBER != "1" && CONFIG_MANUAL_SUB_AnD_NUMBER!="1"){
							$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
							 
					 		  $agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov 
								if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE `username` = '".$agentCode."'")) {
									insertError(AG43);
									redirect($backURL);
								}
								if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".$agentCode."'")) {
									insertError(AG43);
									redirect($backURL);
								}
								if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".$agentCode."'")) {
									insertError(AG43);
									redirect($backURL);
								}
								if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".$agentCode."'")) {
									insertError(AG43);
									redirect($backURL);
								}
								$agentAcountUpdate = "username='$agentCode', agentNumber='".($agentNumber[0]+1)."', ";
					 		}
 		}else{
		
		
			
 				
 				$DistData = selectFrom("select username,PPType,subagentNum,subagentNum from " . TBL_ADMIN_USERS . " WHERE userID = '".$_POST["userID"]."' ");
 			$agentAcountUpdate = "username= '".$DistData['username']."',"; 
		
		
		}
		
	
 	
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");

	/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
	$assocDistUpdateClause = "";

	if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
		&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1
		&& !empty($_REQUEST["assocDistributors"]) )
	{
		$csvAssocDistIDs = implode( ",", $_REQUEST["assocDistributors"] );
		$assocDistUpdateClause = " associated_distributors='" . $csvAssocDistIDs . "', ";
	}
	/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/

    $Querry_Sqls = "update ".TBL_ADMIN_USERS." set $agentAcountUpdate 
	 name='".$_POST["agentContactPerson"]."', 
	 email='".checkValues($_POST["email"])."', 
	 agentCompany='".checkValues($_POST["agentCompany"])."', 
	 agentContactPerson='".checkValues($_POST["agentContactPerson"])."', 
	 agentAddress='".checkValues($_POST["agentAddress"])."', 
	 agentAddress2='".checkValues($_POST["agentAddress2"])."',	 
	 agentCity='".$_POST["City"]."', 
	 rights='".$_POST["rights"]."',
	 backDays='".$backDays."',
	 agentZip='".$_POST["agentZip"]."', 
	 agentCountry='".$_POST["Country"]."', 
	 agentCountryCode='$ccode', 
	 agentPhone='".checkValues($_POST["agentPhone"])."', 
	 agentFax='".checkValues($_POST["agentFax"])."', 
	 agentURL='".checkValues($_POST["agentURL"])."', 
	 agentMSBNumber='".checkValues($_POST["agentMSBNumber"])."', 
	 agentHouseNumber='".checkValues($_POST["agentHouseNumber"])."', 	 
	 agentMCBExpiry='".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
	 agentCompRegNumber='".checkValues($_POST["agentCompRegNumber"])."', 
	 agentCompDirector='".checkValues($_POST["agentCompDirector"])."', 
	 agentDirectorAdd='".checkValues($_POST["agentDirectorAdd"])."', 
	 agentProofID='".checkValues($_POST["agentProofID"])."', 
	 agentIDExpiry='".$idExpiryDate."', 
	 agentDocumentProvided='".$_POST["agentDocumentProvided"]."', 
	 agentBank='".checkValues($_POST["agentBank"])."', 
	 agentAccountName='".checkValues($_POST["agentAccountName"])."', 
	 agentAccounNumber='".checkValues($_POST["agentAccounNumber"])."', 
	 agentBranchCode='".checkValues($_POST["agentBranchCode"])."', 
	 agentAccountType='".$_POST["agentAccountType"]."', 
	 agentCurrency='".$_POST["agentCurrency"]."', 
	 agentAccountLimit='".checkValues($_POST["agentAccountLimit"])."',
	 " . $assocDistUpdateClause	// By Usman Ghani against #3428: Connect Plus - Select Distributors for Agents
	 . " commPackage = '".$_POST["commPackage"]."', 
	 agentCommission='".checkValues($_POST["agentCommission"])."', 
	 agentStatus='".$_POST["agentStatus"]."', 
	 isCorrespondent = '".$_POST["correspondent"]."',
	 IDAcountry = '".$IDAcountry."',
	 accessFromIP = '".checkValues($_POST["accessFromIP"])."',
	 authorizedFor = '".checkValues($authorizedFor)."',
	 postCode='".checkValues($_POST["postcode"])."',
	 isOnline = '".$_POST["isOnline"]."',
	 PPType = '".$DistData['PPType']."',
	 subagentNum = '".$DistData['subagentNum']."',
	 settlementCurrency = '".$currencyOrigin."',
	 hasClave = '".$_POST["hasClave"]."',
	 distAlias = '".$_POST["distAlias"]."',
	 notes = '".$_POST["notes"]."',
	 paymentMode = '".$_POST["agentPaymentMode"]."'
	 where userID='".$_POST["userID"]."'";

	
	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict=strtolower($username.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($_POST["usern"] . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='".$_POST["usern"]."'");
		}
	}

	update($Querry_Sqls);
	
	 if($_REQUEST["ida"] == "ND" && CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1")
	 {
		/* Settlement curriencies of A&D */
		$strAgentDistributorSettlementCurrencies = $_REQUEST["settlementCurrencyAsAgent"]."|".$_REQUEST["settlementCurrencyAsDistributor"];

		$updateSettlementCurrecnySql = "update admin set settlementCurrencies = '".$strAgentDistributorSettlementCurrencies."' where username='".$_POST["usern"]."'";
		update($updateSettlementCurrecnySql);
	 }

	////To record in History
	$descript ="Agent is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);

// Added by Niaz Ahmad at 11-05-2009 @4926 Faith Exchange	
if(defined("CONFIG_DISTRIBUTOR_BANK_CHARGES") && CONFIG_DISTRIBUTOR_BANK_CHARGES=="1"){
update("update ".TBL_ADMIN_USERS." set bankCharges = '".$_POST["distributorBankcharges"]."' where userID='".$_POST["userID"]."'");
}
	if(!empty($_POST["userID"]) && isset($_POST["companyType"]) && $companyTypeFlag){
			update("update " . TBL_ADMIN_USERS. " set companyType='".$_POST["companyType"]."' where userID='".$_POST["userID"]."'");
	}
	if(!empty($_POST["userID"]) && isset($_POST["tradingName"]) && $tradingNameFlag){
			update("update " . TBL_ADMIN_USERS. " set tradingName='".$_POST["tradingName"]."' where userID='".$_POST["userID"]."'");
	}
	if($agentIDNumberFlag){
		update("update ".TBL_ADMIN_USERS." set agentIDNumber = '".$_POST["agentIDNumber"]."' where userID='".$_POST["userID"]."'");
	}	
	if(!empty($_POST["userID"]) && isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where userID='".$_POST["userID"]."'");
	}
	//echo("\n\n Query is updated");
	insertError(AG17);
	$backURL .= "&success=Y";
}
if(CONFIG_UPLOAD_DOCUMENT_ADD_SUB_AGENT	== "1"){
	if($uploadImageSubVar  == "Y"){
	   $backURL = "uploadCustomerImage.php?type=".$typeV."&from=add-sub-agent&mode=".$mode."&pageID=add_sub_agent&ida=".$ida."";
		 if(!empty($insertedID)){
			$backURL .="&customerID=".$insertedID;
		 }else{
		  $backURL .="&customerID=".$_REQUEST["userID"];
		}
	}
}

//$userID = "";
//for ($i=0; $i<count($_POST["associatedAgent"]);$i++)
//{
//	$associatedAgentValue=$_POST["associatedAgent"][$i];
//	$array = explode("(", $associatedAgentValue);
//	$name = $array[0];
//	$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//	if ($i===count($_POST["associatedAgent"])-1)
//		$userID .= $userIDs["userID"];
//	else
//		$userID .= $userIDs["userID"].", ";
//}
//$existingIDA = selectFrom("select userID from admin where userID = (select max(userID) from admin)");
//$strOverwriteSql = "update admin set linked_Agent = '$userID' where userID=".$existingIDA["userID"];
//update($strOverwriteSql);
//
//$userIDs = explode(", ", $userID);
//for ($i = 0; $i < count($userIDs) - 1; $i++)
//{
//	$associatedAgentValue = $userIDs[$i];
//	$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
//	$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
//	$linkedDistributor .= ", ".$existingIDA["userID"];
//	$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
//	update($strOverwriteSql);
//}
//
//$userID = "";
//for ($i=0; $i<count($_POST["associatedDistributor"]);$i++)
//{
//	$associatedDistributorValue=$_POST["associatedDistributor"][$i];
//	$array = explode("(", $associatedDistributorValue);
//	$name = $array[0];
//	$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//	if ($i===count($_POST["associatedDistributor"])-1)
//		$userID .= $userIDs["userID"];
//	else
//		$userID .= $userIDs["userID"].", ";
//}
//$existingIDA = selectFrom("select userID from admin where userID = (select max(userID) from admin)");
//$strOverwriteSql = "update admin set linked_Distributor = '$userID' where userID=".$existingIDA["userID"];
//update($strOverwriteSql);
//
//$userIDs = explode(", ", $userID);
//for ($i = 0; $i < count($userIDs) - 1; $i++)
//{
//	$associatedDistributorValue = $userIDs[$i];
//	$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
//	$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
//	$linkedDistributor .= ", ".$existingIDA["userID"];
//	$strOverwriteSql = "update admin set linked_Agent = '".$linkedDistributor."' where userID=".$associatedDistributorValue;
//	update($strOverwriteSql);
//}
//
//if (isset($_GET['userID'])){
//	$userID = "";
//	for ($i=0;$i<count($_POST["associatedAgent"]);$i++)
//	{
//		$associatedAgentValue=$_POST["associatedAgent"][$i];
//		$array = explode("(", $associatedAgentValue);
//		$name = $array[0];
//		$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//		if ($i===count($_POST["associatedAgent"])-1)
//			$userID .= $userIDs["userID"];
//		else
//			$userID .= $userIDs["userID"].", ";
//	}
//	$strOverwriteSql = "update admin set linked_Agent = '$userID' where userID=".$_GET["userID"];
//	update($strOverwriteSql);
//
//	$userID = "";
//	for ($i=0;$i<count($_POST["associatedDistributor"]);$i++){
//		$associatedDistributorValue=$_POST["associatedDistributor"][$i];
//		$array = explode("(", $associatedDistributorValue);
//		$name = $array[0];
//		$userIDs = selectFrom("select userID from admin where name like '%$name%'");
//		if ($i===count($_POST["associatedDistributor"])-1)
//			$userID .= $userIDs["userID"];
//		else
//			$userID .= $userIDs["userID"].", ";
//	}
//	$strOverwriteSql = "update admin set linked_Distributor = '$userID' where userID=".$_GET["userID"];
//	update($strOverwriteSql);
//}
///////////////////////////////////
if (isset($_GET['userID'])) {
	if (isset($_POST["associatedAgent"])) {
		$userID = "";
		$arrayUserID="";
		for ($i = 0; $i < count($_POST["associatedAgent"]); $i++) {
			$associatedAgentValue = $_POST["associatedAgent"][$i];
			$array = explode("(", $associatedAgentValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i === count($_POST["associatedAgent"]) - 1)
				$userID .= $userIDs["userID"];
			else
				$userID .= $userIDs["userID"] . ", ";
			$arrayUserID[$i] = $userIDs["userID"];
		}
		$strOverwriteSql = "update admin set linked_Agent = '$userID' where userID=" . $_GET["userID"];
		update($strOverwriteSql);

		for ($i=0;$i<count($arrayUserID);$i++) {
			$associatedAgentValue = $arrayUserID[$i];
			$existingLinkedDistributors = selectFrom("select linked_Distributor from admin where userID=".$associatedAgentValue);
			$linkedDistributor = $existingLinkedDistributors["linked_Distributor"];
			if ($linkedDistributor == "") {
				$linkedDistributor .= $_GET['userID'];
			} else {
				$array = explode(", ", $linkedDistributor);
				$flag = true;
				foreach ($array as $userID) {
					if ($userID == $_GET['userID']) {
						$flag = false;
						break;
					}
				}
				if ($flag)
					$linkedDistributor .= ", ".$_GET['userID'];
			}
			$strOverwriteSql = "update admin set linked_Distributor = '".$linkedDistributor."' where userID=".$associatedAgentValue;
			update($strOverwriteSql);
		}
	} else if (isset($_POST["associatedDistributor"])) {
		$userID = "";
		$arrayUserID="";
		for ($i = 0; $i < count($_POST["associatedDistributor"]); $i++) {
			$associatedDistributorValue = $_POST["associatedDistributor"][$i];
			$array = explode("(", $associatedDistributorValue);
			$name = $array[0];
			$userIDs = selectFrom("select userID from admin where name like '%$name%'");
			if ($i === count($_POST["associatedDistributor"]) - 1)
				$userID .= $userIDs["userID"];
			else
				$userID .= $userIDs["userID"] . ", ";
			$arrayUserID[$i] = $userIDs["userID"];
		}
		$strOverwriteSql = "update admin set linked_Distributor = '$userID' where userID=" . $_GET["userID"];
		update($strOverwriteSql);

		for ($i=0;$i<count($arrayUserID);$i++) {
			$associatedAgentValue = $arrayUserID[$i];
			$existingLinkedAgents = selectFrom("select linked_Agent from admin where userID=" . $associatedAgentValue);
			$linkedAgent = $existingLinkedAgents["linked_Agent"];
			if ($linkedAgent == "") {
				$linkedAgent .= $_GET['userID'];
			} else {
				$array = explode(", ", $linkedAgent);
				$flag = true;
				foreach ($array as $userID) {
					if ($userID == $_GET['userID']) {
						$flag = false;
						break;
					}
				}
				if ($flag)
					$linkedAgent .= ", " . $_GET['userID'];
			}
			$strOverwriteSql = "update admin set linked_Agent = '" . $linkedAgent . "' where userID=" . $associatedAgentValue;
			update($strOverwriteSql);
		}
	}
}
///////////////////////////////////
$services = "";
for ($i=0;$i<count($_POST["services"]);$i++)
{
	$servicesValue=$_POST["services"][$i];
	if ($i===count($_POST["services"])-1)
		$services .= $servicesValue;
	else
		$services .= $servicesValue.", ";
}

$strOverwriteSql = "update admin set services = '$services' where userID=".$_GET["userID"];
update($strOverwriteSql);

redirect($backURL);
?>