<?php 
	/**
	 * @package sender Registration
	 * @Subpackage Sender Registration from Premier Exchange Website
	 * This page is used for Registering Sender From the Premier Exchange Web Site
	 * @author Mirza Arslan Baig
	 */

	session_start();
	include ("../include/config.php");
	//ini_set("display_errors","on");
	$date_time = date('d-m-Y  h:i:s A');
	$db = dbConnect();
	/** File Upload **/
	
	if($_FILES['passportDoc']){
		//print_r($_FILES);
		//echo "Cus size:".($_FILES['passportDoc']['size']/1024);
		if(($_FILES['passportDoc']['size']/1024) < 2048 && $_FILES['passportDoc']['error'] == 0){
			//$strNewFileName = mysql_insert_id();
			$strFileName = $_FILES['passportDoc']['name'];
			$arrOldName = explode(".", $strFileName);
			//$strNewFileName = $_SESSION['customerID'].'.'.$arrOldName[count($arrOldName)-1];
			$ext = $arrOldName[count($arrOldName)-1]; //under process 
			//$strNewFileName = str_pad((int)$_SESSION['customerID'],5,"0",STR_PAD_LEFT).'_15_passport';
			$passportTypeQuery = "SELECT doc_cat.documentId FROM ".TBL_CATEGORY." AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'Passport'";
			$resultPassport = mysql_query($passportTypeQuery, $db) or die(mysql_error());
			$passportTypeID = mysql_fetch_array($resultPassport);
			
			$strNewFileName = '00000'.$_SESSION['customerID'].'_15_passport'.'.'.$ext;
			$strPath = '../files/customer/'.$strNewFileName;
			$strTempAddress = $_FILES['passportDoc']['tmp_name'];
			if(move_uploaded_file($strTempAddress, $strPath)){
				$strQueryDocument = "INSERT INTO ".TBL_DOCUMENT_UPLOAD." (documentid, userid, path, status) VALUES('{$passportTypeID['documentId']}', '{$_SESSION['customerID']}', '$strPath', 'Enable')";
				mysql_query($strQueryDocument, $db) or die(mysql_error());
				die("Document successfully uploaded!!");
			}
		}else{
			die('Document cannot be uploaded!!');
		}
	}
	
	/**** Email address availability  ****/
	
	
	if(!empty($_POST['email']) && isset($_POST['chkEmailID'])){
		$chkEmail = $_POST['email'];
		$query = "SELECT email FROM ".TBL_CUSTOMER." WHERE email = '$chkEmail'";
		$records = selectMultiRecords($query);
		$intFlagEmail = count($records);
		if($_POST['chkEmailID'] == '1'){
			if($intFlagEmail > 0 )
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">'.$chkEmail.' is already registered.</em>';
		//else
			//echo '<em style="color:#4B9D05">Available!</em>';
		
			exit;
		}
	}
	
	
	/**** End email address availability ***/
	
	/**** Passport availability  ****/
	if(!empty($_POST['passportNum']) && isset($_POST['chkPassport'])){
		$chkPassport = strtoupper($_POST['passportNum']);
		$queryPassportAvail = "SELECT id_number FROM ".TBL_MULTIPLE_USER_ID_TYPES." WHERE id_number = '$chkPassport' AND user_type = 'C'";
		$records = selectMultiRecords($queryPassportAvail);
		$intFlagPassport = count($records);
		if($_POST['chkPassport'] == '1'){
			if($intFlagPassport > 0)
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">This passport number is already registered.</em>';
				
			exit;
		}
		
	}
	
	/**** End passpoer availibility ****/

	/*** ID card availabilty check ***/
	 
	if(!empty($_POST['idCardNumber']) && isset($_POST['chkIDCard'])){
		$chkIDCard = $_POST['idCardNumber'];
		$queryIDAvailability = "SELECT id_number FROM ".TBL_MULTIPLE_USER_ID_TYPES." WHERE id_number = '$chkIDCard' AND user_type = 'C'";
		$records = selectMultiRecords($queryIDAvailability);
		$intFlagIDCard = count($records);
		if($_POST['chkIDCard'] == '1'){
			if($intFlagIDCard > 0)
				echo '<em style="color:#FF4B4B;font-family:arial;font-size:11px;font-weight:bold">This ID card number is already registered.</em>';
			
			exit;
		}
	}
	
	/*** End id card availabilty check ***/
	
	if($_POST['register'] != 'Register'){
		echo '<em style="color:#FF4B4B;font-size:13px;">Sender can not be registered. Please try again.</em>';
		exit;
	}
	
	if(!empty($_POST['email']) || (!empty($_POST['passportNumber']) || (!empty($_POST['line1']) && !empty($_POST['line2']) && $_POST['line3']))){
		
		$chkEmail = $_POST['email'];
		$passport = $_POST['passportNumber'];
		$NIC = $_POST['line1'].$_POST['line2'].trim($_POST['line3']);
		
		$chkEmail = $_POST['email'];
		$query = "SELECT email FROM ".TBL_CUSTOMER." WHERE email = '$chkEmail'";
		$records = selectMultiRecords($query);
		$intFlagEmail = count($records);
		if($intFlagEmail > 0){
			echo '<em style="color:#FF4B4B;font-size:13px;">This email address is already registered.</em>';
			exit;
		}
		
		
		if(!empty($passport)){
			$passportValidateQuery = "SELECT count(*) AS numberOfRecords FROM ".TBL_MULTIPLE_USER_ID_TYPES." AS userIDType WHERE userIDType.user_type = 'C' AND userIDType.id_number = '$passport'";
			$passportValidation = mysql_query($passportValidateQuery, $db) or die(mysql_error());
			$records = mysql_fetch_array($passportValidation);
			if($records['numberOfRecords'] > 0)
				die('<em style="color:#FF4B4B;font-size:13px;">This passport number is already registered!!</em>');
		}elseif(!empty($NIC)){			
			$idCardValidationQuery = "SELECT count(*) AS numberOfRecords FROM ".TBL_MULTIPLE_USER_ID_TYPES." AS userIDType WHERE userIDType.user_type = 'C' AND userIDType.id_number = '$NIC'";
			$IDCardValidation = mysql_query($idCardValidationQuery, $db) or die(mysql_error());
			$records = mysql_fetch_array($IDCardValidation);
			if($records['numberOfRecords'] > 0)
				die('<em style="color:#FF4B4B;font-size:13px;">This ID card number is already registered!!</em>');
		}
		
	}
	//debug($_POST, true);
	
	/******** Data From Form ********/
	
	
	$mailto = $_POST['email'];
	if(!empty($mailto))
		$custEmail = $mailto;
	
	$ipAddress = $_POST['ipAddress'];
	$title = $_POST['title'];	
	$forename = $_POST['forename'];
	$surname = $_POST['surname'];
	$fullName = $forename.' '.$surname;
	
	$dobDay = $_POST['dobDay'];
	$dobMonth = $_POST['dobMonth'];
	$dobYear = $_POST['dobYear'];
	
	$gender = $_POST['gender'];
	$birCountry = $_POST['birthCountry'];
	$resCountry = $_POST['residenceCountry'];
	$postCode = $_POST['postcode'];
	$buildingNumber = $_POST['buildingNumber'];
	$buildingName = $_POST['buildingName'];
	$street = $_POST['street'];
	$town = $_POST['town'];
	$province = $_POST['province'];
	$landline = $_POST['landline'];
	if(!empty($_POST['mobile']))
		$mobile = $_POST['mobile'];
	// Passport
	if(!empty($_POST['passportNumber'])){
		$passportNumber = $_POST['passportNumber'];
		$passportNumber = strtoupper($passportNumber);
		//debug($passportNumber);
		
		$arrPassportNum = explode("-", $passportNumber);
		$passportNumber1 = $arrPassportNum[0];
		$passportNumber2 = $arrPassportNum[1];
		$passportNumber3 = strtoupper($arrPassportNum[2]);
		$passportNumber4 = $arrPassportNum[3];
		$passportNumber5 = $arrPassportNum[4];
		$passportNumber6 = $arrPassportNum[5];
		$passportNumber7 = $arrPassportNum[6];
		//$passportNumber7 = "<<<<<<<<<<<<<<";
		$passportNumber8 = $arrPassportNum[7];
		$passportNumber9 = $arrPassportNum[8];
		
		//debug($arrPassportNum, true);
		$passportExpiry = $_POST['passportExpiry'];
		$arrpassportExpiry = explode("/", $passportExpiry);
		$passportExpiryDay = $arrpassportExpiry[0];
		$passportExpiryMonth = $arrpassportExpiry[1];
		$passportExpiryYear = $arrpassportExpiry[2];
		
		$passportIssueDate = $_POST['passportIssue'];
		$arrPassportIssueDate = explode("/", $passportIssueDate);
		$passportIssueDay = $arrPassportIssueDate[0];
		$passportIssueMonth = $arrPassportIssueDate[1];
		$passportIssueYear = $arrPassportIssueDate[2];
		
		$passportCountry = $_POST['passportCountry'];
	}
	
	// Identity Card
	$idLine1 = $_POST['line1'];
	$arrLine1 = explode("-", $idLine1);
	$strLine1Part1 = $arrLine1[0];
	$strLine1Part2 = $arrLine1[1];
	$strLine1Part3a = $arrLine1[2];
	$strLine1Part3b = $arrLine1[3];
	$strLine1Part4 = $arrLine1[4];
	
	$idLine2 = $_POST['line2'];
	$arrIdLine2 = explode("-", $idLine2);
	$strLine2Part1a = $arrIdLine2[0];
	$strLine2Part1b = $arrIdLine2[1];
	$strLine2Part2  = $arrIdLine2[2];
	$strLine2Part3a = $arrIdLine2[3];
	$strLine2Part3b = $arrIdLine2[4];
	$strLine2Part4 = $arrIdLine2[5];
	$strLine2Part5 = $arrIdLine2[6];
	$strLine2Part6 = $arrIdLine2[7];
		

	$idLine3 = trim($_POST['line3']);
	if(!empty($idLine1) && !empty($idLine2) && !empty($idLine3))
		$IDCardNumber = $idLine1 . $idLine2 . $idLine3;
	else
		$IDCardNumber = '';
		
	$IDCardNumber = strtoupper($IDCardNumber);
	
	$idExpiryDate = $_POST['idExpiry'];
	$arrIdExpiryDate = explode("/", $idExpiryDate);
	$idExpiryDay = $arrIdExpiryDate[0];
	$idExpiryMonth = $arrIdExpiryDate[1];
	$idExpiryYear = $arrIdExpiryDate[2];
	
	$nationalityCountry = $_POST['nationalityCountry'];
	$idIssueCountry = $_POST['issueCountry'];
	
	
	
	$email = $_POST['email'];
	$userRemarks = $_POST['heardAboutUs'];
	
	if(!empty($buildingNumber))
		$address = $buildingNumber;
	if(!empty($buildingName))
		$address .= ', '.$buildingName;
	if(!empty($street))
		$address .= ', '. $street;
	if(!empty($town))
		$address .= ', '. $town;
	if(!empty($province))
		$address .= ', '.$province;
	if(!empty($resCountry))
		$address .= ', '.$resCountry;
	
	
	/** API Proccess **/
	
	$objParam = new stdClass();
	
	// Test Credentials
	/*
	$objParam ->AccountName		= "integration@premfx.com";
	$objParam ->Password		= "Gw3748563Kb#";
	$objParam ->ProfileId			= "2d7b8dc3-bd08-4b5b-8364-5fd5a8c0d16a";
	*/
	
	// Live Credentials 
	$objParam ->AccountName		= "integration@premfx.com";
	$objParam ->Password		= "238768Fs6hwq8#";

	
	//require_once "../api/gbgroup/autheticatebyprofile.php";
	//require_once "../api/gbgroup/get_itemchecksbyprofile.php";
	$objParam ->ProfileId			= "e3fddd57-780a-4185-a406-250c5f4ceea3";
	//$objParam ->CustomerRef 		= "your own reference";
	
	
	
	
	$objParam ->IPAddress 			= $ipAddress;

	//Personal Details
	$objParam ->UserData = new stdClass();
	$objParam ->UserData->Basic->Title 				= $title;			// required 
	$objParam ->UserData->Basic->Forename 			= $forename;		// required
	$objParam ->UserData->Basic->Surname 			= $surname;			// required
	$objParam ->UserData->Basic->MiddleName			= "";				// 
	$objParam ->UserData->Basic->Gender 			= ucfirst($gender);			// optional / required for 

	$objParam ->UserData->Basic->DOBDay 			= $dobDay;		// optional / required for driving licence check
	$objParam ->UserData->Basic->DOBMonth 			= $dobMonth;		// optional / required for driving licence check
	$objParam ->UserData->Basic->DOBYear 			= $dobYear;		// optional / required for driving licence check


	//Address #1
	$objParam ->UserData->UKData = new stdClass();
	$objParam ->UserData->UKData->Address1 = new stdClass();
	$objParam ->UserData->UKData->Address1->FixedFormat = new stdClass();
	$objParam ->UserData->UKData->Address1->FixedFormat->Postcode 		= $postCode;// required
	$objParam ->UserData->UKData->Address1->FixedFormat->BuildingName 	= "";		// required / optional if BuildingNo supplied
	$objParam ->UserData->UKData->Address1->FixedFormat->BuildingNo 	= $buildingNumber;				// required / optional if BuildingName supplied
	$objParam ->UserData->UKData->Address1->FixedFormat->SubBuilding 	= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Organisation 	= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Street 		= "";	// required
	$objParam ->UserData->UKData->Address1->FixedFormat->SubStreet 		= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->Town 			= "";		// optional
	$objParam ->UserData->UKData->Address1->FixedFormat->District 		= "";		// optional

	/*
	// UK Passport
	$objParam ->UserData->UKData->Passport = new stdClass();
	$objParam ->UserData->UKData->Passport->Number1 = "123456789";
	$objParam ->UserData->UKData->Passport->Number2 = "GBR";
	$objParam ->UserData->UKData->Passport->Number3 = "19890701";
	$objParam ->UserData->UKData->Passport->Number4 = "M";
	$objParam ->UserData->UKData->Passport->Number5 = "0810050";
	$objParam ->UserData->UKData->Passport->Number6 = "12";
	//$arrExpiryDate = explode($_POST['passportExpiry'],"-");
	$expiry = "2013-07-01";
	$arrExpiryDate = explode("-",$expiry);
	$objParam ->UserData->UKData->Passport->ExpiryDay = $arrExpiryDate[2];
	$objParam ->UserData->UKData->Passport->ExpiryMonth = $arrExpiryDate[1];
	$objParam ->UserData->UKData->Passport->ExpiryYear = $arrExpiryDate[0];

	*/

	//Drivers Licence
	// $objParam ->UserData->UKData->Driver = new stdClass();
	// $objParam ->UserData->UKData->Driver->Number1						= "MARR9";			//	5 digits
	// $objParam ->UserData->UKData->Driver->Number2						= "608232";			//	6 digits
	// $objParam ->UserData->UKData->Driver->Number3						= "HB9";			//	3 digits
	// $objParam ->UserData->UKData->Driver->Number4						= "00";				//	2 digits
	// $objParam ->UserData->UKData->Driver->Postcode						= "S61 2NL";		//	postcode capture from Address#1
	//$objParam ->UserData->UKData->Driver->Microfiche					= "";				//	14 digits


	//Landline Telephone
	// $objParam ->UserData->UKData->Telephone = new stdClass();
	// $objParam ->UserData->UKData->Telephone->ExDirectory 				= "TRUE";			// Set to TRUE if no number is supplied
	// $objParam ->UserData->UKData->Telephone->Number 					= "";				// optional can be blank if ExDirectory Flag set to True


	//InternationalPassport
	$objParam ->UserData->InternationalPassport = new stdClass();
	$objParam ->UserData->InternationalPassport->Number1			= $passportNumber1;	// 9	digits
	$objParam ->UserData->InternationalPassport->Number2			= $passportNumber2;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number3			= $passportNumber3;	// 3	digits
	$objParam ->UserData->InternationalPassport->Number4			= $passportNumber4;	// 7	digits
	$objParam ->UserData->InternationalPassport->Number5			= $passportNumber5;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number6			= $passportNumber6;	// 7	digits
	$objParam ->UserData->InternationalPassport->Number7			= $passportNumber7;	// 14	digits
	$objParam ->UserData->InternationalPassport->Number8			= $passportNumber8;	// 1	digits
	$objParam ->UserData->InternationalPassport->Number9			= $passportNumber9;	// 1	digits
	$objParam ->UserData->InternationalPassport->ExpiryDay			= $passportExpiryDay; // 2	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->ExpiryMonth		= $passportExpiryMonth;	// 2	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->ExpiryYear			= $passportExpiryYear;	// 4	digits - optional but recommended for enhanced matching
	$objParam ->UserData->InternationalPassport->CountryOfOrigin	= $passportCountry;	// GetPassportCountries  method - optional but recommended for enhanced matching 


	// Identity Card 
	$objParam ->UserData->IdentityCard = new stdClass();
	$objParam ->UserData->IdentityCard->Line1->Part1DocType			= $strLine1Part1; // 2	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part2IssuingState	= $strLine1Part2; // 3	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part3aDocumentNumber	= $strLine1Part3a; // 9	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part3bCheckDigit		= $strLine1Part3b; // 1	digits / chars
	$objParam ->UserData->IdentityCard->Line1->Part4Optional		= $strLine1Part4; // 15	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part1aDateOfBirth	= $strLine2Part1a;	// 6	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part1bCheckDigit		= $strLine2Part1b; 	// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part2Gender			= $strLine2Part2; 	// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part3aDateOfExpiry	= $strLine2Part3a;	// 6	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part3bCheckDigit		= $strLine2Part3b;		// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part4Nationality		= $strLine2Part4;										// 3	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part5Optional			= $strLine2Part5;								// 11	digits / chars
	$objParam ->UserData->IdentityCard->Line2->Part6CheckDigit			= $strLine2Part6;											// 1	digits / chars
	$objParam ->UserData->IdentityCard->Line3							= $idLine3;	// 30	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryDay						= $idExpiryDay;											// 2	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryMonth						= $idExpiryMonth;											// 2	digits / chars
	$objParam ->UserData->IdentityCard->ExpiryYear						= $idExpiryYear;										// 4	digits / chars
	$objParam ->UserData->IdentityCard->CountryOfNationality			= $nationalityCountry;				// use GetIdentityCardNationalityCountries method
	$objParam ->UserData->IdentityCard->CountryOfIssue					= $issueCountry;								// use GetIdentityCardIssuingCountries method
	
	
	//debug($objParam);
	//LIVE PLATFORM URL	
	//$soap = new SoapClient("https://www.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
	$soap = new SoapClient("https://www.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array( 'trace' => true));
	/*
	echo "<pre>";
	print_r($objParam);
	echo "</pre>";
	*/
	//PILOT PLATFORM URL
	//$soap = new SoapClient("https://pilot.prove-uru.co.uk:8443/URUWS/URU11a.asmx?wsdl", array('compression' => SOAP_COMPRESSION_ACCEPT, 'trace' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));

	if (is_soap_fault($soap)) 
	{
	throw new Exception(" {$soap->faultcode}: {$soap->faultstring} ");
	}
	try {
	$objRet = $soap->AuthenticateByProfile($objParam);
	/*
	echo '<pre>';
	print"Authentication ID : ".($objRet->AuthenticateByProfileResult->AuthenticationId)."<br>";
	print"Score             : ".($objRet->AuthenticateByProfileResult->Score)."<br>";
	print"Band Text         : ".($objRet->AuthenticateByProfileResult->BandText)."<br>";
	echo '</pre>';
	*/
	}
	catch (Exception $e) {
		//	exit('PHPcode check failed');
		die("Visit this url for registration: http://clients.premfx.com/private_registration.php");
		die("<h5 style='color:#FF4B4B;font-style:italic;'>".$e->getMessage()."</h5>");
		
	}
	$authenticationID = $objRet->AuthenticateByProfileResult->AuthenticationId;
	$score = $objRet->AuthenticateByProfileResult->Score;
	$bandText = $objRet->AuthenticateByProfileResult->BandText;
	$timeStamp = $objRet->AuthenticateByProfileResult->Timestamp;
	
	// $objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[2] // National Identity Card Number
	// $objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[3] // Passport International
	
	//debug($objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[2]);
	//debug($objRet->AuthenticateByProfileResult->ResultCodes->ItemCheck[3]);
	
	
	/***** Account Number Generator ****/

	$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER." where 1 ");
	$customerNumber_value = $customerNumber[0]+1;
	//debug($customerNumber_value);
	if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
		$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
	else
		$strCustomerPrefix = "CW-";
	
	$accountName = $strCustomerPrefix.$customerNumber_value;
	//debug($accountName);
	
	/***************  START PDF ATTACHMENT DOCUMENT GENERATION ***************/
	/*
	include ('../pdfClass/class.ezpdf.php');
	$pdf =& new Cezpdf();
	$pdf->selectFont('../pdfClass/fonts/Helvetica.afm');
	
	
	// image can be created only if GD library is installed in server
	$image = imagecreatefromjpeg("images/premier/logo.jpg");
	$pdf->addImage($image,50,700,200);
	
	$pdf->ezSetMargins(0,0,80,80);
	for($i=0;$i<=8;$i++)
		$pdf->ezText('',12);

	$pdf->ezText('',12);
	$pdf->ezText('<b><u>Registration</u></b>',12,array("justification" => "center"));
	$pdf->ezText('',12);
	$pdf->ezText('Registration Details are given Below:',12,array("justification" => 'full'));	
	$pdf->ezText('',12);
	
	$pdf->ezText('',12);
	$pdf->ezText('',12);
	
	
	$data = array(
		array(
			'Title',
			"$title"
		),
		array(
			'Forename',
			"$forename"
		),
		array(
			'Surname',
			"$surname"
		),
		array(
			'Reference Number',
			"$accountName"
		),
		array(
			'Date of Birth',
			"$dob"
		),
		array(
			'Gender',
			"$gender"
		),
		array(
			'Country of Birth',
			"$birCountry"
		),
		array(
			'Country of Residence',
			"$resCountry"
		),
		array(
			'Postcode / Zipcode',
			"$postCode"
		),
		array(
			'Your Address',
			"$address"
		),
		array(
			'Telephone - Landline',
			"$landline"
		),
		array(
			'Telephone - Mobile',
			"$mobile"
		),
		array(
			'Passport',
			"$passportNumber"
		),
		array(
			'Passport Expiry Date',
			"$passportExpiry"
		),
		array(
			'Originating Country',
			"$passportCountry"
		),
		array(
			'National ID Card Line1',
			"$idLine1"
		),
		array(
			'National ID Card Line2',
			"$idLine2"
		),
		array(
			'National ID Card Line3',
			"$idLine3"
		),
		array(
			'Identity Card Expiry Date',
			"$idExpiryDate"
		),
		array(
			'Country of Nationality',
			"$nationalityCountry"
		),
		array(
			'Country of Issue Identity Card',
			"$idIssueCountry"
		),
		array(
			'Email',
			"$email"
		)
	);
	
	$pdf->ezTable($data,'' , '',array('showHeadings'=>0,'shaded'=>0,'showLines'=>2));
	
	for($i=0;$i<=1;$i++)
		$pdf->ezText('',12);
	
	
	for($i=0;$i<=4;$i++)
		$pdf->ezText('',12);
	
	

	$pdfcode = $pdf->output();
	//$pdf->ezStream();
	
	// write pdf 
	$fPDFname = tempnam('/tmp','PDF_Registration Details').'.pdf';
	$fp = fopen($fPDFname,'w');
	fwrite($fp,$pdfcode);
	
	$fpPDF = fopen($fPDFname, "rb");
	$filePDF = fread($fpPDF, 102460);
	fclose($fp);
	$filePDF = chunk_split(base64_encode($filePDF));

		
	$file_name = 'Registeration Data.pdf';
	$file_path = $fPDFname;
*/
/***************  END PDF ATTACHMENT DOCUMENT GENERATION ***************/


/***************  Database operations ***************/

$timeStamp = date('Y-m-d H:i:s');
$query = "INSERT INTO ".TBL_CUSTOMER." (created, firstName, lastName, customerName, accountName,gender, email, Address, city, Zip, Phone, Mobile, dob, customerNumber, customerStatus, Country, placeOfBirth, customerType, userRemarks, ipAddress) VALUES('$timeStamp', '$forename', '$surname', '$fullName', '$accountName','$gender', '$email', '$address', '$town', '$postCode', '$landline', '$mobile', '$dobYear.\"-\".$dobMonth.\"-\".$dobDay', '$customerNumber_value', 'Disable', '$resCountry', '$birCountry', 'C', '$userRemarks', '$ipAddress')";
mysql_query($query, $db) or die(mysql_error());
$_SESSION['customerID'] = @mysql_insert_id();

$userType = 'C';

if(!empty($_POST['passportNumber'])){
	
	//$passportTypeQuery = "SELECT doc_cat.documentId FROM category AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'Passport'";
	
	$passportTypeQuery = "SELECT id FROM ".TBL_ID_TYPES." AS idTypes WHERE idTypes.title = 'Passport' AND idTypes.active = 'Y'";
	$resultPassport = mysql_query($passportTypeQuery, $db) or die(mysql_error());
	$passportTypeID = mysql_fetch_array($resultPassport);
	
	
	// passport database query
	$strPassportIDQuery = "INSERT INTO ".TBL_MULTIPLE_USER_ID_TYPES." (id_type_id, user_id, user_type, id_number, issued_by, issue_date, expiry_date) VALUES('{$passportTypeID['id']}', '{$_SESSION['customerID']}', '$userType', '$passportNumber', '$passportCountry', '$passportIssueYear.\"-\".$passportIssueMonth.\"-\".$passportIssueDay', '$passportExpiryYear.\"-\".$passportExpiryMonth.\"-\".$passportExpiryDay')";

	mysql_query($strPassportIDQuery, $db) or die(mysql_error());

	$queryComplianceGB = "INSERT INTO ".TBL_GB_COMPLIANCE." (customerID, authenticationID, documentType, score, result, verificationDate) VALUES('{$_SESSION['customerID']}', '$authenticationID', 'Passport', '$score', '$bandText', '$timeStamp')";

	mysql_query($queryComplianceGB, $db) or die(mysql_error());

}

// ID Card database query
if(!empty($IDCardNumber)){


//$IDCardQuery = "SELECT doc_cat.documentId FROM category AS cat INNER JOIN document_category AS doc_cat ON cat.id = doc_cat.categoryId WHERE cat.description = 'ID Card'";


$IDCardQuery = "SELECT id FROM ".TBL_ID_TYPES." AS idTypes WHERE idTypes.title = 'Id Card' AND idTypes.active = 'Y'";

$resultIDCard = mysql_query($IDCardQuery, $db) or die(mysql_error());
$IDCardTypeID = mysql_fetch_array($resultIDCard);


$strIDCardQuery = "INSERT INTO ".TBL_MULTIPLE_USER_ID_TYPES." (id_type_id, user_id, user_type, id_number, issued_by, issue_date, expiry_date) VALUES('{$IDCardTypeID['id']}', '{$_SESSION['customerID']}', '$userType', '$IDCardNumber', '$idIssueCountry','','$idExpiryYear.\"-\".$idExpiryMonth.\"-\".$idExpiryDay')";
mysql_query($strIDCardQuery, $db) or die(mysql_error());

$queryComplianceGB = "INSERT INTO ".TBL_GB_COMPLIANCE." (customerID, authenticationID, documentType, score, result, verificationDate) VALUES('{$_SESSION['customerID']}', '$authenticationID', 'ID Card', '$score', '$bandText', '$timeStamp')";
mysql_query($queryComplianceGB, $db) or die(mysql_error());
}
//debug($queryComplianceGB);

/***************  START EMAIL BODY FOR CUSTOMER ***************/
$timeStamp = time();
$num = md5( time() );
$messageT = "\n<br />Dear ".$fullName.",\n<br /><br />Thank you for registering with Premier FX. We will contact you with your account details.<br /><br />Regards,<br /> ";



$messageT.="<table>
				<tr>
					<td align='left' width='30%' >
					<img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/mail_signature.jpg' />
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align='left'>
						<font size='2'>
							<b>UK:</b> 55 Old Broad Street, London, EC2M 1RF  t +44 (0)845 021 2370 \n<br>
							<b>Portugal:</b> Rua Sacadura Cabral, Edif�cio Golfe lA, 8135-144 Almancil, Algarve  t +351 289 358 511 \n<br>
							<b>Spain:</b> C/Rambla dels Duvs, 13-1, 07003, Mallorca.   t +34 971 576 724
						</font>
					</td>
				</tr>
			</table>";
			

			
/***************  END EMAIL BODY FOR CUSTOMER ***************/

/***************  START EMAIL BODY FOR SYSTEM ***************/
$messageSystem = "
				\n<br />New Customer is registered from the website.\n<br /> His Email Address is : $email.
";

/***************  END EMAIL BODY FOR SYSTEM ***************/

// Prepare Email for Sender and Account Manger

$subject = "Registration Confirmation";
$headers  = 'MIME-Version: 1.0' . "\r\n";
// This two steps to help avoid spam   
$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

$fromName  = SYSTEM;
$fromEmail = "info@premfx.com";
//$fromEmail = "arslan.baig@hbstech.co.uk";
$fromEmailSystem = "Reply-To: noreply@premierfx.com";

require_once("lib/phpmailer/class.phpmailer.php");
$sendermailer = new PHPMailer();
$systemMailer = new PHPMailer();

$systemMailer->FromName = SYSTEM;
$systemMailer->From = $fromEmailSystem; 
$systemMailer->AddAddress($fromEmail,'');
$systemMailer->IsHTML(true);

$sendermailer->FromName = $fromName;
$sendermailer->From = $fromEmail; 
$sendermailer->AddAddress($custEmail,'');
$sendermailer->IsHTML(true);




// attachements
/*
if($fpPDF){
	$sendermailer->AddAttachment($file_path, $file_name,"base64");
}
else{
	$attachementInfo = '<br/>Registration information could not be attached with email';
}
*/
// Subject
$sendermailer->Subject = $subject;
$systemMailer->Subject = "New customer registered from website";

// Body
$sendermailer->Body = $messageT;
$systemMailer->Body = $messageSystem;


	if($custEmail){
		$custEmailFlag = $sendermailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
		$systemEmailFlag = $systemMailer->Send();//@mail($custEmail, $subject, $messageT, $headers); 
		
	}
	else{
		$msg = "Sender can not be registered.";

		echo "<h5 style='color:#FF4B4B;font-style:italic;'>".$msg."</h5>";
	}	
	dbClose();
if ($custEmailFlag ) {
	if($custEmailFlag){
		$msg = "Sender is registered successfully and you will receive an activation email after authentication from administrator.";
	}
	//$msg.=$attachementInfo."<br/>";
	echo "<h5 style='color:#4B9D05;font-style:italic;'>".$msg."</h5>";
}
/**** Informative Message ***/

?>