<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");

if ($_POST['Submit'] == 'Save') {
	if ($_POST['sendingCntry'] == 'All') {
		$origCountries = 'All';	
	} else {
		$origCountries = (is_array($_POST['origCountries']) ? implode(",", $_POST['origCountries']) : 'All');
	}
	$customPrefix = "";
	if ($_POST['prefix'] == 'custom') {
		$customPrefix = $_POST['customPrefix'];	
	}
	
	$customPostfix = "";
	if ($_POST['postfix'] == 'custom') {
		$customPostfix = $_POST['customPostfix'];	
	}
	
	$customFormula = "";
	if ($_POST['formula'] == 'custom') {
		$customFormula = $_POST['customFormula'];	
	}
	$Qry_Inst = "INSERT INTO `ref_generator` (`dated`, `origCountries`, `prefix`, `customPrefix`, `transType`, `moneyPaid`, `formula`, `customFormula`, customLength, postFix, customPostFix, incementBased) 
						VALUES ('" . $date_time . "', '" . $origCountries . "', '" . $_POST['prefix'] . "', '" . $customPrefix . "', '" . $_POST['transType'] . "', '" . $_POST['moneyPaid'] . "', '" . $_POST['formula'] . "', '" . $customFormula . "', '".$_POST["customLength"]."', '" . $_POST['postfix'] . "','".$customPostfix."', '".$_POST["incrementBased"]."') ";
	insertInto($Qry_Inst);
	
	$intLastInsertId = @mysql_insert_id();
	
	/**
	 * If prefix is selected by the distributor than store the specific distributor value and prefix for that distributor
	 * @Ticket #3956
	 */
	if(CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1)
	{
		if($_REQUEST['prefix'] == "byDistributor")
		{
			$strUpdateSql = "update ref_generator 
							set
								prefixValue = '".$_REQUEST["distributor"]."',
								userPrefix = '".$_REQUEST["distributorPrefix"]."'
							where 
								id=$intLastInsertId
							";
			update($strUpdateSql);
		}
	}
	
	insertError("Reference Number template is generated successfully.");
	redirect("ref-generator.php?msg=Y&success=Y");
}

?>
<html>
<head>
	<title>Generate Reference Number</title>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script src="jquery.js"></script>
	<script language="javascript">
	<!-- 
function checkForm() {
	
	<? if(CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1) { ?>
	if(document.getElementById("prefix").value == "byDistributor")
	{
		if(document.getElementById("distributor").value == "")
		{
			alert("Please select distributor.");
			document.getElementById("distributor").focus();
			return false;
		}
	}
	<? } ?>
	
	if (document.getElementById('formula').options.selectedIndex == 0) {
		alert("Please select the formula for reference number");
		document.getElementById('formula').focus();
		return false;
	}
	
	if (document.getElementById('formula').value == 'custom') {
		if (document.getElementById('customFormula').value == '') {
			alert("Please provide the first reference number to start with.");
			document.getElementById('customFormula').focus();
			return false;
		}
		if (!isNumeric(document.getElementById('customFormula').value)) {
			alert("Please provide the positive reference number.");
			document.getElementById('customFormula').focus();
			return false;
		}
		
		if (!isNumeric(document.getElementById('customLength').value) && document.getElementById('customLength').value != '') {
			alert("Please Provide a Positive Minimum Length or Leave it empty.");
			document.getElementById('customFormula').focus();
			return false;
		}
	}
	
	return true;
}

function showRow(val, rowID) {
	
	if (val == 'show') {
		document.getElementById(rowID).style.display = "inline";

	} else {
		document.getElementById(rowID).style.display = "none";

	}
}
function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <form action="ref-generator.php" method="post" onSubmit="return checkForm();" name="genRefNum">
  <tr>
      <td align="center"> 
        <table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Generate Reference Number</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		  <tr bgcolor="EEEEEE"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
              	<tr>
              		<td width="40" align="center">
              			<font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
              		</td>
              	</tr>
             	</table>
            </td>
      </tr>
    <? } ?>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
				  <tr bgcolor="#ededed">
            <td width="200"><font color="#005b90"><strong>Sending Countries*</strong></font></td>
            <td width="400"><input type="checkbox" name="sendingCntry" id="sendingCntry" value="All" onClick="if (document.getElementById('sendingCntry').checked == false) { showRow('show', 'specifySend'); } else { showRow('hide', 'specifySend'); }" checked> All
              </td>
        </tr>	                       
				  <tr bgcolor="#ededed" id="specifySend" style="display:none;">
            <td width="200"><font color="#005b90"><strong>Specify Sending Countries*</strong></font></td>
            <td width="400">Hold Ctrl key for multiple selection<br>
            	<SELECT name="origCountries[]" id="origCountries[]" style="font-family:verdana; font-size: 11px" multiple>
				<?
				 if(CONFIG_COUNTRY_SERVICES_ENABLED){
								$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = fromCountryId  and  countryType like '%origin%' order by countryName");
								}
							else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where  countryType like '%origin%' order by countryName");
								}
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT>
              </td>
        </tr>	                       
        <tr bgcolor="#ededed">
            <td><font color="#005b90"><strong>Prefix*</strong></font></td>
            <td>
			<?
				/*
				showRow('show', 'customPrefixRow'); } else { showRow('hide', 'customPrefixRow');
				*/
			?>
			<script>
				function displayRows()
				{
					if (document.getElementById('prefix').value == 'custom') 
					{ 
						$("#customPrefixRow").show(); 
						$("#distributorRow").hide();
					} 
					else 
					{ 
						$("#customPrefixRow").hide(); 
						if(document.getElementById('prefix').value == 'byDistributor')
							$("#distributorRow").show();
						else
							$("#distributorRow").hide();
					}
				}
			</script>
				<?
					$senderLabel = "Sender";
					$senderLabelN = $_arrLabels[$senderLabel];	
					$AgentLabel = "Agent";
					$AgentLabelN = $_arrLabels[$AgentLabel];	
				?> 
            	<select name="prefix" id="prefix" onChange="displayRows();">
            		<option value="byCompany">By Company Prefix</option>
            		<option value="byAgentName">By <?=$AgentLabelN?> Name</option>
            		<option value="byAgentNumber">By <?=$AgentLabelN?> Number</option>
					<? if(CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1) { ?>
					<option value="byDistributor">By Distributor</option>
					<? } ?>
            		<option value="custom">Custom Prefix</option>	
				</select>
            </td>
        </tr>
        
        <tr bgcolor="#ededed" id="customPrefixRow" style="display:none;">
            <td><font color="#005b90"><strong>Custom Prefix</strong></font></td>
            <td><input type="text" name="customPrefix" id="customPrefix" size="6" maxlength="10"></td>
        </tr>
		
		<? if(CONFIG_REF_FOR_SPECIFIC_DISTRIBUTOR == 1) { ?>
		<tr bgcolor="#ededed" id="distributorRow" style="display:none">
            <td><font color="#005b90"><strong>Distributors</strong></font></td>
            <td>
				<?
					$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' order by agentCompany");
				?>
				<script>
					function showDistrbutorPrefixRow()
					{
						if($("#distributor").value != '')
							$("#distributorPrefix").show();
						else
							$("#distributorPrefix").hide();
					}
				</script>
				<select name="distributor" id="distributor" style="font-family:verdana; font-size: 11px" onChange="showDistrbutorPrefixRow();">
					<option value=""> - Select Distributor - </option>
					<?
						for ($i=0; $i < count($agents); $i++){
					?>
					<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
					<?
						}
					?>
			  </select>
			</td>
        </tr>
		<tr bgcolor="#ededed" style="display:none" id="distributorPrefix">
			<td><font color="#005b90"><strong>Distributor Prefix for <script>document.write($("#distributor").value);</script></strong></font></td>
			<td>
				<select name="distributorPrefix">
            		<option value="byDistributorName">By Distributor Name</option>
            		<option value="byDistributorNumber">By Distributor Number</option>
				</select>
			</td>
		</tr>
		<? } ?>
		
        <tr bgcolor="#ededed">
            <td><font color="#005b90"><strong>Postfix</strong></font></td>
            <td>
            	<select name="postfix" id="postfix" onChange="if (document.getElementById('postfix').value == 'custom') { showRow('show', 'customPostfixRow'); } else { showRow('hide', 'customPostfixRow'); }">
            		<option value=""> None </option>
            		<option value="byCompany">By Company Postfix</option>
            		<option value="byAgentName">By <?=$AgentLabelN?> Name</option>
            		<option value="byAgentNumber">By <?=$AgentLabelN?> Number</option>
					<option value="custom">Custom Postfix</option>	
				</select>
            </td>
        </tr>
        <tr bgcolor="#ededed" id="customPostfixRow" style="display:none;">
            <td><font color="#005b90"><strong>Custom Postfix</strong></font></td>
            <td><input type="text" name="customPostfix" id="customPostfix" size="6" maxlength="10"></td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Transaction Type</strong></font></td>
          <td>
          	<select name="transType" id="transType">
          		<option value="All">All</option>
          		<option value="Pick up">Pick up</option>
          		<option value="Bank Transfer">Bank Transfer</option>
          		<option value="Home Delivery">Home Delivery</option>
          	</select>
          </td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
          <td>
          	<select name="moneyPaid" id="moneyPaid">
          		<option value="All">All</option>
          		<option value="By Cash">By Cash</option>
          		<option value="By Cheque">By Cheque</option>
          		<option value="By Bank Transfer">By Bank Transfer</option>
          	</select>
          </td>
        </tr>
        
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Select Formula*</strong></font></td>
          <td>
          	<select name="formula" id="formula" onChange="if (document.getElementById('formula').value == 'custom') { showRow('show', 'customNumRow'); showRow('show', 'customLength');showRow('show', 'incrementBased');} else { showRow('hide', 'customNumRow'); showRow('hide', 'customLength');showRow('hide', 'incrementBased');}">
          		<option value="">- select -</option>
          		<option value="DDMM">DDMM</option>
          		<option value="custInitials"><?=$senderLabelN?> Name Initials</option>
          		<option value="autoNum">Auto Number</option>
				<option value="agentLetterWithAutoNum"><?=$AgentLabelN?> name first letter with Auto Number</option>
				<option value="agentCityWithAutoNum"><?=$AgentLabelN?> city first 4 letters with Auto Number</option>
          		<option value="custom">Custom</option>
          	</select>
          </td>
        </tr>
        <tr bgcolor="#ededed" id="customNumRow" style="display:none;">
            <td><font color="#005b90"><strong>First Reference Number*</strong></font></td>
            <td><input type="text" name="customFormula" id="customFormula" size="10" maxlength="10"> <i>to start from</i></td>
        </tr>
        <tr bgcolor="#ededed" id="customLength" style="display:none;">
            <td><font color="#005b90"><strong>Length Of Number</strong></font></td>
            <td><input type="text" name="customLength" id="customLength" size="10" maxlength="10"> </td>
        </tr>
        <tr bgcolor="#ededed" id="incrementBased" style="display:none;">
            <td><font color="#005b90"><strong>Increment Based On</strong></font></td>
            <td> 
            	<select name="incrementBased" id="incrementBased">
            		<option value="">Default</option>
	          		<option value="all">All Transactions</option>
	          		<option value="agent"><?=$AgentLabelN?> Transactions</option>
          		</select>
            </td>
        </tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="submit" name="Submit" value="Save">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>