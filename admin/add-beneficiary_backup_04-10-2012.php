<?
session_start();
include ("../include/config.php");
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();
$date_time = date('d-m-Y  h:i:s A');
if($agentType == "TELLER")
{
	$parentID = $_SESSION["loggedUserData"]["cp_ida_id"];
	$userID = $_SESSION["loggedUserData"]["cp_ida_id"];
	}else{
	$parentID = $_SESSION["loggedUserData"]["userID"];
	$userID = $_SESSION["loggedUserData"]["userID"];
}

$userDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where username='$username'");
session_register("agentID2");
session_register("Title");
session_register("firstName");
session_register("S/O");
session_register("sonOfType");
session_register("lastName");
session_register("middleName");
session_register("dobDay");
session_register("dobMonth");
session_register("dobYear");
session_register("IDType");
session_register("IDNumber");
session_register("Otherid_name");
session_register("Otherid");
session_register("Address");
session_register("Address1");
session_register("Country");
session_register("City");
session_register("Zip");
session_register("State");
session_register("Phone");
session_register("Mobile");
session_register("bentEmail");
session_register("benID");
session_register("from");
session_register("amount_transactions");
session_register("Country");
session_register("secMiddleName");
if(!empty($_REQUEST["custID"]))
$_SESSION["customerID"]=$_REQUEST["custID"];
// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

if ( is_array( $_REQUEST["IDTypesValuesData"] ) )
{
	$_SESSION["IDTypesValuesData"] = $_REQUEST["IDTypesValuesData"];
}

// End of code against #3299: MasterPayex - Multiple ID Types

if ($_GET["val"] != "")
	$_SESSION["amount_transactions"] = $_GET["val"];

$cpf = 0;
if($_POST["sel"] == "country")
{
	
	if ($_POST["Country"] == CONFIG_CPF_COUNTRY)
	{
		$cpf = 1;	
	}else{
		$_SESSION["CPF"] = "";
	}
}
if ($_GET["notEndSessions"] != "" && $_GET["success"] == "" && CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
{
	$cpf = 1;	
}

//if(!empty($_GET["benID"]))
	$_SESSION["benID"] = $_GET["benID"];

/** 
 * As below we check the benId from GET array 
 * so we are assigning from session to GET array
 * @Ticket# 3521
 */
//if(!empty($_SESSION["benID"]) && empty($_GET["benID"]))
//	$_GET["benID"] = $_SESSION["benID"];


$_SESSION["from"] = $_GET["from"];

$benID = $_SESSION["benID"];

if($_GET["r"]!=""){
	$_SESSION["error"]="";
}
if(($_GET["benID"] == "" && $_GET["notEndSessions"] == "") || ($_GET["benID"] != "" && $_GET["msg"] != "" && $_GET["notEndSessions"] == ""))
{
	if ($_GET["notEndSessions"] == "")
	{
	//$_SESSION["benID"] = "";
	$_SESSION["Title"] = "";
	$_SESSION["other_title"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["secMiddleName"] = "";
	$_SESSION["dobDay"] = "";
	$_SESSION["dobMonth"] = "";
	$_SESSION["dobYear"] = "";
	$_SESSION["IDType"] = "";
	$_SESSION["IDNumber"] = "";
	$_SESSION["Otherid_name"] = "";
	$_SESSION["Otherid"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["Address2"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["CPF"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["benEmail"] = "";
	$_SESSION["bankName"] 		= "";
	$_SESSION["bankCode"] 		= "";
	$_SESSION["branchName"] 		= "";
	$_SESSION["branchCity"] 		= "";
	$_SESSION["branchCode"] 	= "";
	$_SESSION["branchAddress"] 	= "";
	$_SESSION["swiftCode"] 		= "";
	$_SESSION["accNo"] 			= "";
	$_SESSION["ABACPF"] 		= "";
	$_SESSION["IBAN"] 			= "";
	$_SESSION["transType"] = "";
	$_SESSION["Citizenship"] = "";
	$_SESSION["IDissuedate"] = "" ;
	$_SESSION["IDexpirydate"] = "" ;
	$_SESSION["issuedBy"] = "";
	$_SESSION["S/O"] = "";
	$_SESSION["sonOfType"] = "";
	$_SESSION["ben_bank_id"] = "";
	$_SESSION["bankName1"] = "";
	$_SESSION["account1"] = "";
	$_SESSION["branchCode1"] = "";
	$_SESSION["branchAddress1"] = "";
	$_SESSION["swiftCode1"] = "";
	$_SESSION["country1"] = "";
	$_SESSION["bankId1"] = "";
	
}
if ($_POST["from"] != "")
	$_SESSION["from"] = $_POST["from"];
	
if ($_POST["Citizenship"] != "")
	$_SESSION["Citizenship"] = $_POST["Citizenship"];
	
if ($_POST["IDissuedate"] != "")
	$_SESSION["IDissuedate"] = $_POST["IDissuedate"];
	
	if ($_POST["issuedBy"] != "")
	$_SESSION["issuedBy"] = $_POST["issuedBy"];

if ($_POST["IDexpirydate"] != "")
	$_SESSION["IDexpirydate"] = $_POST["IDexpirydate"];

if ($_POST["benID"] != "")
	$_SESSION["benID"] = $_POST["benID"];


if ($_POST["agentID"] != "")
	$_SESSION["agentID2"] = $_POST["agentID"];
elseif ($_POST["agentID"] == "")
	$_SESSION["agentID2"] = $_GET["agentID"];


if ($_POST["firstName"] != "")
	$_SESSION["firstName"] = $_POST["firstName"];


//elseif ($_POST["firstName"] == "")
	//$_SESSION["firstName"] = "";
if ($_POST["lastName"] != "")
	$_SESSION["lastName"] = $_POST["lastName"];
//elseif ($_POST["lastName"] == "")
	//$_SESSION["lastName"] = "";
if ($_POST["middleName"] != "")
	$_SESSION["middleName"] = $_POST["middleName"];
	
if($_POST["secMiddleName"] != "")
	$_SESSION["secMiddleName"] = $_POST["secMiddleName"];	
//elseif ($_POST["middleName"] == "")
	//$_SESSION["middleName"] = "";

if ($_POST["dobDay"] != "")
	$_SESSION["dobDay"] = $_POST["dobDay"];
if ($_POST["dobMonth"] != "")
	$_SESSION["dobMonth"] = $_POST["dobMonth"];
if ($_POST["dobYear"] != "")
	$_SESSION["dobYear"] = $_POST["dobYear"];
		
	if ($_POST["S/O"] != "")
	$_SESSION["S/O"] = $_POST["S/O"];
	
	if ($_POST["sonOfType"] != "")
	$_SESSION["sonOfType"] = $_POST["sonOfType"];
	
if(CONFIG_TITLE_OFF != "1")
{	
	if ($_POST["Title"] != "")
		$_SESSION["Title"] = $_POST["Title"];
		
	if ($_POST["other_title"] != "")
		$_SESSION["other_title"] = $_POST["other_title"];
	
}

//elseif ($_POST["Title"] == "")
	//$_SESSION["Title"] = "";
if ($_POST["IDType"] != "")
	$_SESSION["IDType"] = $_POST["IDType"];
//elseif ($_POST["IDType"] == "")
	//$_SESSION["IDType"] = "";
if ($_POST["IDNumber"] != "")
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
//elseif ($_POST["IDNumber"] == "")
	//$_SESSION["IDNumber"] = "";
	if ($_POST["Otherid_name"] != "")
	$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
//elseif ($_POST["Otherid_name"] == "")
	//$_SESSION["Otherid_name"] = "";
if ($_POST["Otherid"] != "")
	$_SESSION["Otherid"] = $_POST["Otherid"];
//elseif ($_POST["Otherid"] == "")
	//$_SESSION["Otherid"] = "";
if ($_POST["Address"] != "")
	$_SESSION["Address"] = $_POST["Address"];
//elseif ($_POST["Address"] == "")
	//$_SESSION["Address"] = "";
if ($_POST["Address1"] != "")
	$_SESSION["Address1"] = $_POST["Address1"];
//elseif ($_POST["Address1"] == "")
	//$_SESSION["Address1"] = "";
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];
//elseif ($_POST["Country"] == "")
	//$_SESSION["Country"] = "";
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
//elseif ($_POST["City"] == "")
	//$_SESSION["City"] = "";
if ($_POST["Zip"] != "")
	$_SESSION["Zip"] = $_POST["Zip"];
//elseif ($_POST["Zip"] == "")
	//$_SESSION["Zip"] = "";
if ($_POST["State"] != "")
	$_SESSION["State"] = $_POST["State"];
//elseif ($_POST["State"] == "")
	//$_SESSION["State"] = "";
if ($_POST["Phone"] != "")
	$_SESSION["Phone"] = $_POST["Phone"];
//elseif ($_POST["Phone"] == "")
	//$_SESSION["Phone"] = "";
if ($_POST["Mobile"] != "")
	$_SESSION["Mobile"] = $_POST["Mobile"];
//elseif ($_POST["Mobile"] == "")
	//$_SESSION["Mobile"] = "";
if ($_POST["benEmail"] != "")
	$_SESSION["benEmail"] = $_POST["benEmail"];
//elseif ($_POST["benEmail"] == "")
	//$_SESSION["benEmail"] = "";
if ($_POST["userType"] != "")
	$_SESSION["userType"] = $_POST["userType"];

if ($_POST["CPF"] != "")
	$_SESSION["CPF"] = $_POST["CPF"];


if (!empty($_GET["ben_bank_id"]))
	$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];

if(!empty($_POST["bankName"]))
	$_SESSION["bankName1"] = $_POST["bankName"];	
if(!empty($_POST["account"]))
	$_SESSION["account1"] = $_POST["account"];	
if(!empty($_POST["branchName"]))
	$_SESSION["branchCode1"] = $_POST["branchName"];	
if(!empty($_POST["iban"]))
	$_SESSION["iban1"] = $_POST["iban"];	
if(!empty($_POST["swiftCode"]))
	$_SESSION["swiftCode1"] = $_POST["swiftCode"];	
if(!empty($_POST["accountType"]))
	$_SESSION["accountType1"] = $_POST["accountType"];	
if(!empty($_POST["branchAddress"]))
	$_SESSION["branchAddress1"] = $_POST["branchAddress"];	
if(!empty($_POST["bankId"]))
	$_SESSION["bankId1"] = $_POST["bankId"];	
if(!empty($_POST["country1"]))
	$_SESSION["country1"] = $_POST["country1"];	
	
}
elseif($_GET["benID"] != "" && $_GET["msg"] == "")
{
	
$strQuery = "SELECT * FROM beneficiary where benID = $benID";
	$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
	$rstRow = mysql_fetch_array($nResult);				
	
		$_SESSION["agentID2"] = $rstRow["agentID"];
		$_SESSION["firstName"] = $rstRow["firstName"];
		$_SESSION["lastName"] = $rstRow["lastName"];
		$_SESSION["S/O"] = $rstRow["SOF"];
		$_SESSION["sonOfType"] = $rstRow["SOF_Type"];
		$_SESSION["middleName"] = $rstRow["middleName"];
		$_SESSION["secMiddleName"] = $rstRow["secMiddleName"];
		$_SESSION["dobDay"]= substr($rstRow["dob"],8,2);
		$_SESSION["dobMonth"]= substr($rstRow["dob"],5,2);
		$_SESSION["dobYear"]= substr($rstRow["dob"],0,4);
		$_SESSION["Title"] = $rstRow["Title"];
		$_SESSION["other_title"] = $rstRow["other_title"];
		$_SESSION["IDType"] = $rstRow["IDType"];
		$_SESSION["IDNumber"] = $rstRow["IDNumber"];
		$_SESSION["Otherid_name"] = $rstRow["otherId_name"];
		$_SESSION["Otherid"] = $rstRow["otherId"];
		$_SESSION["Address"] = $rstRow["Address"];
		$_SESSION["Address1"] = $rstRow["Address1"];
		$_SESSION["Address2"] = $rstRow["address2"];
		$_SESSION["Country"] = $rstRow["Country"];
		$_SESSION["City"] = $rstRow["City"];
		$_SESSION["CPF"] = $rstRow["CPF"];
		$_SESSION["Zip"] = $rstRow["Zip"];
		$_SESSION["State"] = $rstRow["State"];
		$_SESSION["Phone"] = $rstRow["Phone"];
		$_SESSION["Mobile"] = $rstRow["Mobile"];
		$_SESSION["benEmail"] = $rstRow["email"];
		$_SESSION["Citizenship"] = $rstRow["Citizenship"];
		$_SESSION["IDissuedate"] = $rstRow["IDissuedate"];
		$_SESSION["IDexpirydate"] = $rstRow["IDexpirydate"];
		$_SESSION["customerID"] = $rstRow["customerID"];
		$_SESSION["issuedBy"] = $rstRow["issuedBy"];
		$_SESSION["userType"] = $rstRow["userType"];
		
		/**
	   * If the beneficiary is with bank details
	   * #Ticket# 3321
	   */
		if(CONFIG_BEN_BANK_DETAILS == "1")
		{
			/**
			 * Fetching IBAN number from the beneficiary table
			 */
			$_SESSION["iban"] = $rstRow["IBAN"];
			
			$benBanksDataQry = "select * from ".TBL_BEN_BANK_DETAILS." where benID=$benID ORDER BY id DESC";
			$banBanksData = selectFrom($benBanksDataQry);
			/*
			session_register("bankName");
			session_register("account");
			session_register("branchName");
			session_register("branchAddress");
			session_register("iban");
			session_register("swiftCode");
			session_register("accountType");
			*/
			
			if(count($banBanksData) > 0)
			{
				
				$_SESSION["account1"] = $banBanksData["accountNo"];
				$_SESSION["accountType1"] = $banBanksData["accountType"];
				
				$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				$_SESSION["iban1"]  = $banBanksData["IBAN"];
				$benBanksDataQry = "select * from ".TBL_BANKS." where id='".$banBanksData["bankId"]."'";
				$banBanksData = selectFrom($benBanksDataQry);
				
				/**
				 * Storing the bank id in case of edit beneficuary
				 */
				$_SESSION["ben_bank_id"] = $banBanksData["id"];
				
				$_SESSION["bankName1"] = $banBanksData["name"];
				/*
				$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				*/
				$_SESSION["bankId1"] = $banBanksData["bankId"];
				$_SESSION["country1"] = $banBanksData["country"];
				
				
				
			}
			elseif(!empty($_SESSION["ben_bank_id"]))
			{
				/**
				 * Getting all the bank fields based on the selected bank from bank search
				 */
				$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_SESSION["ben_bank_id"];
				$banBanksData = selectFrom($benBanksDataQry);
				
				
				$_SESSION["bankName1"] = $banBanksData["name"];
				//$_SESSION["branchCode1"] = $banBanksData["branchCode"];
				//$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
				//$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
				$_SESSION["bankId1"] = $banBanksData["bankId"];
				$_SESSION["country1"] = $banBanksData["country"];
				
			}
			
			//print_r($_SESSION);
			
		}
		
		
		
	//debug(strtoupper($_SESSION["Country"]) ."==". strtoupper(CONFIG_CPF_COUNTRY));	
   if (CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
   {
  //	debug("cpf ".$cpf);
   	$cpf = 1;	
   }
   elseif($_POST["sel"] == "country")
	{
		if ($_POST["Country"] != CONFIG_CPF_COUNTRY)
		{
			$cpf = 0;
			//debug(">> cpf ".$cpf);
		}
	}

	

	if($_POST["Country"] != "") {		 
		  $_SESSION["Country"] = $_POST["Country"];
	}	else {
	  $_SESSION["Country"] = $rstRow["Country"];
	  if($_SESSION["Country"] == "") {
	  	$_SESSION["Country"] = $_POST["Country"];
	  }
	}
		
	if($_POST["transType"] == "Select Transaction Type")		 
		  $_SESSION["transType"] = $_REQUEST["transType"];
	else
		{
		  $_SESSION["transType"] =$rstRow["transType"];
		  if($_SESSION["transType"] == "" || $_REQUEST["transType"]!="")
		  	$_SESSION["transType"] = $_REQUEST["transType"];
		}
		
		//$_SESSION["Country"] = ucfirst(strtolower($_SESSION["Country"]));

		$strQuery = "SELECT * FROM cm_bankdetails where benID = $benID";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 		
		$rstRow = mysql_fetch_array($nResult);				
	
		$_SESSION["bankName"] = $rstRow["bankName"];
		$_SESSION["branchCode"] = $rstRow["branchCode"];
		$_SESSION["bankCode"] = $rstRow["BankCode"];
		$_SESSION["branchCity"] = $rstRow["BranchCity"];
		$_SESSION["branchAddress"] = $rstRow["branchAddress"];
		$_SESSION["branchName"] = $rstRow["BranchName"];
		$_SESSION["swiftCode"] = $rstRow["swiftCode"];
		$_SESSION["accNo"] = $rstRow["accNo"];
		$_SESSION["ABACPF"] = $rstRow["ABACPF"];
		$_SESSION["IBAN"] = $rstRow["IBAN"];

}

//debug(strtoupper($_SESSION["Country"]) ."==". strtoupper(CONFIG_CPF_COUNTRY));	
   if (CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
   {
  	//debug("cpf ".$cpf);
   	$cpf = 1;	
   }
   elseif($_POST["sel"] == "country")
	{
		if ($_POST["Country"] != CONFIG_CPF_COUNTRY)
		{
			$cpf = 0;
			//debug(">> cpf ".$cpf);
		}
	}


if($_GET["benID"] != "" && $_GET["msg"] != "")
{
	if($_SESSION["IDexpirydate"] != '' && $_SESSION["IDexpirydate"] != '0000-00-00')
	{
		$_SESSION["IDexpirydate"] = substr($_SESSION["IDexpirydate"],6,4) . "-" . substr($_SESSION["IDexpirydate"],3,2)	. "-" . substr($_SESSION["IDexpirydate"],0,2);
	}
	if($_SESSION["IDissuedate"] != '' && $_SESSION["IDissuedate"] != '0000-00-00')
	{
	 	$_SESSION["IDissuedate"] = substr($_SESSION["IDissuedate"],6,4) . "-" .  substr($_SESSION["IDissuedate"],3,2)	. "-" . substr($_SESSION["IDissuedate"],0,2);
	}
}
if($_POST["Country"] != "")	
	{
		
		if($_SESSION["Country"] != $_POST["Country"])
			{
			
				$_SESSION["bankName"]="";
				$_SESSION["branchName"]="";
				$_SESSION["branchAddress"]="";
				$_SESSION["branchCode"]="";
			}
		 // if($_SESSION["Country"]=='India')	
		  //	$_SESSION["branchCode"]="";
		  $_SESSION["Country"] = $_POST["Country"];
		  
		  
	
	}
	
	
	if ($_SESSION["dobDay"] == "")
	$_SESSION["dobDay"] = $_GET["dobDay"];
	
if ($_SESSION["dobMonth"] == "")
	$_SESSION["dobMonth"] = $_GET["dobMonth"];
	
if ($_SESSION["dobYear"] == "")
	$_SESSION["dobYear"] = $_GET["dobYear"];
	/*else
		{
		  $_SESSION["Country"] = $rstRow["Country"];
		  if($_SESSION["Country"] == "")
		  	{
		    	$_SESSION["Country"] = $_POST["Country"];
				$_SESSION["branchCode"]="";
			}
		}*/
////////Bank Name

if ($_POST["bankName"] != "")
		{
		$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchName"]="";
				$_SESSION["branchAddress"]="";
				$_SESSION["branchCode"]="";
		}
/*else
		{
		  $_SESSION["bankName"] = $rstRow["bankName"];
		  if($_SESSION["bankName"] == "")
		  {
		    	$_SESSION["bankName"] = $_POST["bankName"];
				$_SESSION["branchCode"]="";
			}
		}
	*/	
/////Bank Code
/*if ($_POST["bankCode"] != "")
	{
	$_SESSION["bankCode"] = $_POST["bankCode"];
	$_SESSION["branchCode"]="";
	}*/
/*else
		{
		  $_SESSION["bankCode"] = $rstRow["BankCode"];
		  if($_SESSION["bankCode"] == "")
		  	{
		    	$_SESSION["bankCode"] = $_POST["bankCode"];
				$_SESSION["branchCode"]="";
			}	
		}*/
//////Branch Address			
	if ($_POST["branchAddress"] != "")
		{
		$_SESSION["branchAddress"] = $_POST["branchAddress"];
		$_SESSION["branchCode"]="";
		
		}
		
	/*else
		{
		  $_SESSION["branchAddress"] = $rstRow["branchAddress"];
		  if($_SESSION["branchAddress"] == "")
		  	{
		    	$_SESSION["branchAddress"] = $_POST["branchAddress"];
				$_SESSION["branchCode"]="";
			}	
		}*/
/////Branch Name
	if ($_POST["branchName"] != "")
		{
		$_SESSION["branchName"] = $_POST["branchName"];
		$_SESSION["branchCode"]="";
		$_SESSION["branchAddress"]="";
		}
		
	/*else
		{
		  $_SESSION["branchName"] = $rstRow["BranchName"];
		  if($_SESSION["branchName"] == "")
		  {
		    	$_SESSION["branchName"] = $_POST["branchName"];
				$_SESSION["branchCode"]="";
			}	
		}*/
///////		
if (CONFIG_CPF_ENABLED == "1" && strtoupper($_SESSION["Country"]) == strtoupper(CONFIG_CPF_COUNTRY))
{
	if($_POST["Submit"] == "Validate")
	{
		
		//echo '+'.$_POST["cpfcpnj"].'+';
		
		/**
		 * This method expires and now updated with client specified criteria through code
		 *		 $checkValidity = validCPF($_POST["CPF"]);
		 */
		require_once("lib/classValidateCpfCpnj.php");
		$validate = new VALIDATE; 
	
		if($_POST["cpfcpnj"] == "CPF")
		{
			if(!$validate->cpf($_POST["CPF"]) && $_POST["CPF"] != "00000000000")
			{
				insertError(BE18);	
				
			}
			else
			{
				insertError(BE19);	
				$_GET["success"] ="Y";
			}
		}
		elseif($_POST["cpfcpnj"] == "CPNJ")
		{
			if(!$validate->cnpj($_POST["CPF"]) && $_POST["CPF"] != "00000000000000")
			{
				insertError("This is not a valid CNPJ number");	
				
			}
			else
			{
				insertError("CNPJ number is valid");	
				$_GET["success"] ="Y";
			}
		}
		
			$_SESSION["CPF"] = $_POST["CPF"];
			$_GET["msg"] = "Y";
			
	}
}


if(!empty($_GET["ben_bank_id"]))
{
	$_SESSION["ben_bank_id"] = $_GET["ben_bank_id"];
	/**
	 * Getting all the bank fields based on the selected bank from bank search
	 */
	$benBanksDataQry = "select * from ".TBL_BANKS." where id=".$_SESSION["ben_bank_id"];
	$banBanksData = selectFrom($benBanksDataQry);
	
	$_SESSION["bankName1"] = $banBanksData["name"];
	//$_SESSION["branchCode1"] = $banBanksData["branchCode"];
	//$_SESSION["branchAddress1"] = $banBanksData["branchAddress"];
	//$_SESSION["swiftCode1"] = $banBanksData["swiftCode"];
	$_SESSION["country1"] = $banBanksData["country"];
	$_SESSION["bankId1"] = $banBanksData["bankId"];
	
}

	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types

	// Fetch available ID Types
	$query = "select * from id_types
				where active='Y'
				and show_on_ben_page='Y'";

	$IDTypes = selectMultiRecords( $query );
	// If its an update sender request, find the sender's ID Type values.
	if ( !empty( $_REQUEST["benID"] ) )
	{
		// Now fetch values against available id types.
		$numberOfIDTypes = count( $IDTypes );
		for( $i = 0; $i < $numberOfIDTypes; $i++ )
		{
			$IDTypeValuesQ = "select * from user_id_types
									where
										id_type_id = '" . $IDTypes[$i]["id"] . "'
									and	user_id = '" . $_REQUEST["benID"] . "'
									and user_type = 'B'";
			$IDTypeValues = selectFrom( $IDTypeValuesQ );
			if ( !empty( $IDTypeValues ) )
			{
				$IDTypes[$i]["values"] = $IDTypeValues;
			}
		}
	}
/*
 * @Ticket #4794
 */
	$userIdTypesNotesFlag = false;
	if(defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES") && (CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES=="1" || strstr(strtoupper(CONFIG_SHOW_MULTIPLE_ID_TYPES_NOTES),$agentType))){
		$userIdTypesNotesFlag = true;
	}

	// End of code against #3299: MasterPayex - Multiple ID Types

?>
<html>
<head>
	<title>Beneficiary Information</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {

	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }
 
   <?
	if (CONFIG_CUST_NAME_SEQUENCE == '1') {  
	?>
		if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
	    	alert("Please provide the beneficiary's <? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?>.");
	        theForm.lastName.focus();
	        return false;
	    }	
	  if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
	    	alert("Please provide the beneficiary's first name.");
	        theForm.firstName.focus();
	        return false;
	    }
	<?
	}else{
	?>
		if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
	    	alert("Please provide the beneficiary's first name.");
	        theForm.firstName.focus();
	        return false;
	    }
		if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
	    	alert("Please provide the beneficiary's <? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?>.");
	        theForm.lastName.focus();
	        return false;
	    }	
	<?
	}
	?>
     /* #4726  This config is created to make Address Line 1 a mandatory field for All users.*/
	<? if (CONFIG_BENEFICIARY_ADDRESS_MENDATORY == '1'){ ?>  
		 if(theForm.Address.value == "" || IsAllSpaces(theForm.Address.value)){
    	alert("Please provide the customer's address line 1.");
        theForm.Address.focus();
        return false;
    }
	 
   <? } ?> 
    


     <? if(CONFIG_ID_MANDATORY == '1'){ ?>
    
    if(theForm.IDNumber.value == "" || IsAllSpaces(theForm.IDNumber.value)){
    	alert("Please provide the Beneficiary's ID Number.");
        theForm.IDNumber.focus();
        return false;
    }
  
      <? } ?>
      
      
<? 
if(CONFIG_RADIOIDTYPE_IDNUMBER_CHECK == '1')
{ ?>
	var val = true;
	if(theForm.IDNumber.value != ""){
		val = false;
  }
	for (var i= theForm.IDType.length-1; i > -1; i--) {
		if(theForm.IDType[i].checked){
			val = true;
		}
	 	if(theForm.IDType[i].checked && theForm.IDNumber.value == "") {
   			alert("Please provide the Beneficiary's ID Number.");
   			 return false;
   	}
	} 
	if(!val){
		alert("Please Select the Beneficiary's ID Type.");
   		return false;
	}
<?
}
?>	
  
<?
	// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
	
	if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY")
		&& CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == '1' 
		&& !empty( $IDTypes ))
	{
		?>
			if ( checkIdTypes( theForm ) != true ){
				return false;
			}	
		<?
	}
	// End of code against #3299: MasterPayex - Multiple ID Types
?>
  

<?	if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) {  ?>
	if(theForm.Address.value == "" || IsAllSpaces(theForm.Address.value)){
    	alert("Please provide the customer's address line 1.");
        theForm.Address.focus();
        return false;
    }
	
	<?
	if(CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS != 1){ /*against #3639 by khola*/
	?>
		if(theForm.Zip.value == "" || IsAllSpaces(theForm.Zip.value)){
    		alert("Please provide the customer's Zip/Postal Code.");
        	theForm.Zip.focus();
        	return false;
    	}
	<?
	}
	?>
<?	
}  
?>
    <? if ($cpf == 1){ ?>
	if(theForm.CPF.value == "" || IsAllSpaces(theForm.CPF.value))
	{
			if(theForm.userType.value == "Business User")
				alert("Please provide the CNPJ number.");
			else
    		alert("Please provide the CPF number.");
        
        theForm.CPF.focus();
        return false;
  }	
    if (theForm.CPF.value != "")
    {
			var sText = theForm.CPF.value;
		  
		
    }
  <? } ?>
<? if(CONFIG_CITY_NON_COMPULSORY != '1'){?>
	if(theForm.City.value == "" || IsAllSpaces(theForm.City.value)){
    	alert("Please provide the beneficiary's city.");
        theForm.City.focus();
        return false;
    }
<? }?>   

<?
if(CONFIG_PHONE_NON_COMPULSORY != '1'){?>
	if(theForm.Phone.value == "" || IsAllSpaces(theForm.Phone.value)){
    	alert("Please provide the beneficiary's phone number.");
        theForm.Phone.focus();
        return false;
    }
<? 
}
?>
   
<? if(CONFIG_MOBILE_MANDATORY == '1'){ ?>
	
		if(
			theForm.Mobile != null <? // Condition modified by Usman Ghani against ticket# 3472 ?>
			&& (theForm.Mobile.value == "" || IsAllSpaces(theForm.Mobile.value) )
		){
    	alert("Please provide the beneficiary's mobile number.");
        theForm.Mobile.focus();
        return false;
  	}
<? }?>


	<? if(CONFIG_BEN_BANK_DETAILS == "1"){ ?>
		/*	
		if ( theForm.bankId.value == "" )
		{
			alert('Please provide bank ID.');
			theForm.bankId.focus();
			return false;
		}
		*/
		if(theForm.iban.value != "")
		{
			//validateIban();
			if( document.getElementById('ibnStatus').value == 0 )
			{
					alert('The IBAN number '+ document.getElementById("iban").value +' is invalid.');
					return false;
			}
		}
				
		if(theForm.bankName.value == "")
		{
			alert("Please enter the bank name.");
			theForm.bankName.focus();
			return false;
		}
		/*
		if(theForm.branchName.value == "")
		{
			alert("Please enter the branch name / number.");
			theForm.branchName.focus();
			return false;
		}
		*/
		if(theForm.account.value == "")
		{
			alert("Please enter the account number.");
			theForm.account.focus();
			return false;
		}
		
		if(theForm.country1.value == "")
		{
			alert("Please enter the bank country.");
			theForm.country1.focus();
			return false;
		}
		
	<? } ?>





<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{
		?>
			if ( validatePhoneMobile() == false )
				return false;

			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
		<?
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>

<?
	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		?>
			if ( theForm.Phone != null && theForm.Phone.value == "" )
			{
				alert("Please provide phone number.");
				theForm.Phone.focus();
				return false;
			}
			if ( theForm.phoneType != null && theForm.phoneType.value == "" )
			{
				alert("Please select phone type.");
				theForm.phoneType.focus();
				return false;
			}
		<?
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile
?>


	return true;
}
<?
	// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
	
	if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
		&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
	{
		?>
			function validatePhoneMobile()
			{
				Phone = document.getElementById("Phone");
				Mobile = document.getElementById("Mobile");
				if ( Phone != null && Phone.value != ""
					&& !( /^\d+$/.test(Phone.value) ) ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in phone number.");
					Phone.focus();
					return false;
				}

				if ( Mobile != null && Mobile.value != ""
					&& !( /^\d+$/.test(Mobile.value) ) ) // Checking, through regular expression, if its not a numeric value.
				{
					alert("Please provide only numerals in mobile number.");
					theForm.Mobile.focus();
					return false;
				}
				return true;
			}

			function phoneEventHandler( e )
			{
				keynum = -1;
				if(window.event) // IE
				{
					keynum = e.keyCode;
				}
				else if(e.which) // Netscape/Firefox/Opera
				{
					keynum = e.which;
				}

				if ( 
						keynum > -1
					&& 	keynum != 8 	// Backspace
					&& 	keynum != 13	// Enter
					&& 	keynum != 27	// Escape
					 )
				{
					validatePhoneMobile();
				}
			}
		<?
	}
	// End of code against ticket #3473 Now Transfer - Phone/Mobile validations
?>
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   
function clearExpDate() {	// [by Jamshed]
	document.frmBenificiary.IDexpirydate.value = "";	
}

function clearIssueDate() {	// [by Jamshed]
	document.frmBenificiary.IDissuedate.value = "";	
}
function checkFocus(){
// document.getElementById("firstName").focus();

	if(document.getElementById("userType"))
		newCaption();
<? 
	if(!empty($_SESSION["userType"]))
	{ 
		if($_SESSION["userType"] == "Business User")
		{
?>
			if(document.getElementById("caption"))
			{
			document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CNPJ Number<font color='red'>*</font>&nbsp;</b></font>";	
			document.getElementById("cpfcpnj").value = "CPNJ";	
		}
<? 
		}
		else
		{
?>
		if(document.getElementById("caption"))
		{
			document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CPF Number<font color='red'>*</font>&nbsp;</b></font>";	
			document.getElementById("cpfcpnj").value = "CPF";
		}
<?
		}
	}
?>
	if(document.getElementById("iban"))
	{
		if(document.getElementById("iban").value != "")
			validateIban();
	}

}   

function validateIban()
{
	if(document.getElementById("iban").value != "")
	{
		//$("#benIbanValidStatus").load("validateIban.php?iban_num="+document.getElementById("iban").value);	
		$.ajax({
		   type: "GET",
		   url: "validateIban.php",
		   data: "iban_num="+document.getElementById("iban").value,
		   beforeSend: function(XMLHttpRequest){
		   	document.getElementById('formSubmitButton').disabled = true;
		   },
		   success: function(msg){
		    if(msg == 1)
		    {
		    	document.getElementById('ibnStatus').value=1;
		    	document.getElementById("benIbanValidStatus").innerHTML = "<font color='green'>The IBAN number is valid.</font>";
		    	document.getElementById('formSubmitButton').disabled = false;
		    }
		    else
		    {
		    	document.getElementById('ibnStatus').value=0;
		    	document.getElementById("benIbanValidStatus").innerHTML = "<font color='red'>You have entered an invalid IBAN number.</font>";
		    	document.getElementById('formSubmitButton').disabled = true;
		    }
		   }
		 });		
		
		
	}
	else
	{
		document.getElementById("benIbanValidStatus").innerHTML = "";
   	document.getElementById('formSubmitButton').disabled = false;
	}
}

function newCaption()
{
	var userType = document.getElementById("userType").value;	
		
	if(userType == "Business User" )
	{
		document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CNPJ Number<font color='red'>*</font>&nbsp;</b></font>";	
		document.getElementById("cpfcpnj").value = "CPNJ";
	}
	else
	{
		document.getElementById("caption").innerHTML = "<font color='#005b90'><b>CPF Number<font color='red'>*</font>&nbsp;</b></font>";	
		document.getElementById("cpfcpnj").value = "CPF";
	}
}
	// end of javascript -->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2"><? if($_SESSION["benID"]!= ""){ echo("Update Beneficiary");} else { echo("Add Beneficiary"); } ?></font></strong></td>
  </tr>
  <form name="frmBenificiary" action="add-ben-conf.php?from=<? echo $_GET["from"];?>&agentId2=<? echo $_GET["agentID"];?>&transID=<? echo $_GET["transID"]?>&r=<? echo $_GET["r"]?>&callFrom=<?=$_REQUEST["callFrom"]?>&docWaiver=<?=$_REQUEST["docWaiver"]?>" method="post">
 <input type="hidden" name="benID" value="<? echo $_SESSION["benID"];?>">
 <input type="hidden" name="from" value="<? echo $_SESSION["from"];?>">
  <tr>
      <td align="center"> <table width="686" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong><? if($_SESSION["benID"]!= ""){ echo("Update Beneficiary");} else { echo("Add Beneficiary"); } ?></strong></font></td>
                </tr>
              </table></td>
          </tr>
          <?
           if($_GET["msg"] != "" && $_SESSION['error'] != ""){
            ?>
          <tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE">
<table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo ("<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"); $_SESSION['error'] = ""; ?></td>
                </tr>
              </table></td>
          </tr>
          <? 
          } 
          ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font><font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;+ Can be Compulsory Fields with respect to Country Selection </font></td>
          </tr>
          <tr> 
            <td valign="top" colspan="2" bgcolor="#000000">
              <table width="100%" cellpadding="2" cellspacing="1" border="0">
				<?
				/* if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || strstr($userDetails["rights"], "Create Transaction"))
				{
				?>
                <tr bgcolor="#ededed">
                  <td height="20" align="right"><font color="#005b90"><strong>Select Agent<font color="#ff0000">*</font></strong></font></td>
                  <td colspan="3"><select name="agentID" style="font-family:verdana; font-size: 11px">
                    <option value="">- Select Agent-</option>
                    <?
					$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany");
					for ($i=0; $i < count($agents); $i++){
				?>
				 <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option> 
                   <!--<option value="<?=$agents[$i]["userID"]; ?>"><? // echo $agents[$i]["username"]." [".$agents[$i]["agentContactPerson"]."]"; ?>-->
				   <!-- <option value="<? //=$agents[$i]["userID"]; ?>"><? //echo $agents[$i]["agentCompany"]." [".$agents[$i]["agentContactPerson"]."]"; ?></option>-->
                    <?
					}
				?>
                  </select><script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                                </script></td>
                </tr>
				<?
				}
				else
				{
					echo "<input type='hidden' name='agentID' value='".$_SESSION["loggedUserData"]["userID"]."'>";
				}
				*/
				?>
                <tr bgcolor="#ededed"> 
                  <td width="25%" height="20" align="right"><font color="#005b90"><b>Country<font color="#ff0000">*</font>&nbsp;
                    </b></font></td>
                  <td width="37%"> <input type="hidden" name="sel" value="">
				<?
				if (CONFIG_CPF_ENABLED == "1"){ ?>
                  	<select name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary.php?msg=Y&from=<? echo $_GET["from"];?>&agentID=<? echo $_SESSION["agentID2"];?>&benID=<? echo $_GET["benID"]?>&transID=<? echo $_GET["transID"]?>'; document.forms[0].submit();">
                  <? }elseif(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1"){ ?>
                    <select name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary.php?msg=Y&from=<? echo $_GET["from"];?>&agentID=<? echo $_SESSION["agentID2"];?>&benID=<? echo $_GET["benID"]?>&transID=<? echo $_GET["transID"]?>'; document.forms[0].submit();">
                  	<? }else{ ?>
                  	<select name="Country" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size: 11px">
                  <? } ?>
                      <option value="">- Select Country-</option>
                     <!--               ////onChange="document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]?>'; document.forms[0].submit();"-->
                      
				<?	
					$benCountries = "";
					if (defined("CONFIG_BEN_COUNTRIES_LIST") && CONFIG_BEN_COUNTRIES_LIST!="0"){
						$benCountries = explode(",", CONFIG_BEN_COUNTRIES_LIST);
						$benCountries = array_filter($benCountries);
						for ($k = 0; $k < count($benCountries); $k++) {
				?>
							<option value="<? echo trim($benCountries[$k]) ?>" >
								<? echo trim($benCountries[$k]) ?>
							</option>
				<?	
						}
					}elseif ($agentType == "admin"){
                      if(CONFIG_ENABLE_ORIGIN == "1"){
                	
			                	
			                		$countryTypes = " and countryType like '%destination%' ";
			                	
			                                	
			                }else{
			                		$countryTypes = " ";
			                	
			                } 
                      
             if(CONFIG_COUNTRY_SERVICES_ENABLED){
								$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
								}
							else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
					for ($i=0; $i < count($countires); $i++)
					{
									if(strtoupper($countires[$i]["countryName"]) == strtoupper($_SESSION["Country"]))
									{
									//break;
									?>
									  <option  value="<? echo $_SESSION["Country"];?>"  selected> 
									  <? echo $countires[$i]["countryName"]; ?>									  </option>
									  <?
									}
									else
									{
										// Code added by Usman Ghani aginst ticket #3469 Now Transfer - Default Beneficiary Country
										$selected = "";
										if ( defined("CONFIG_DEFAULT_COUNTRY") && CONFIG_DEFAULT_COUNTRY == 1 && defined("CONFIG_DEFAULT_COUNTRY_VALUE"))
										{
											if ( $countires[$i]["countryName"] == CONFIG_DEFAULT_COUNTRY_VALUE )
											{
												$selected = "selected='selected'";
											}
										}
										// End of code aginst ticket #3469 Now Transfer - Default Beneficiary Country
									?>
									  <option value="<?=$countires[$i]["countryName"]; ?>"> 
									  <?=$countires[$i]["countryName"]; ?>
									  </option>					
									<?
									}
					}
				}
				else
				{
					$IDAcountry	= selectFrom("select IDAcountry from ".TBL_ADMIN_USERS." where userID = '".$userID."'");
					$myCountries = explode(",", $IDAcountry["IDAcountry"]);
					for ($i = 0; $i < count($myCountries); $i++)
					{
						if ($myCountries[$i] == $_SESSION["Country"])
						{
						?>
									  <option  value="<? echo $_SESSION["Country"];?>"  selected> 
									  <? echo $myCountries[$i]; ?>									</option>
									<?
								}
								else
								{
									?>
									  <option value="<?=$myCountries[$i]; ?>"> 
									  <?=$myCountries[$i]; ?>
									  </option>					
									<?
								}
					} 
				}
					if($_SESSION["Country"] != "")
					{
				?>
				<!-- option selected><? echo $_SESSION["Country"];?></option -->
				<?
				}
				?>
                    </select> 				  
					<?
					
				//  echo $_SESSION["Country"];
				  ?>
                  <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>               	  </td>
				  <?php
				  if(CONFIG_CITIZENSHIP == 1){
				  ?>
                  <td width="20%" align="right"><font color="#005b90"><b>Citizenship</b></font></td>
                  <td width="37%"><input type="text" name="Citizenship" value="<?=$_SESSION["Citizenship"]; ?>" maxlength="25"></td>
				  <?
				  }else{ ?>
	
                  <td width="37%">&nbsp;</td>
                  <td width="37%">&nbsp;</td>
				  <?
				}
				  ?>
                </tr>
				
               
								
				<tr bgcolor="#ededed"> 
                <?
                if(CONFIG_TITLE_OFF != '1')
                {
                ?>	
                  <td width="25%" height="20" align="right"><font color="#005b90"><b>Title&nbsp;
                    </b></font></td>
                  <td width="37%"> <select name="Title" onChange="var titleSessValue='<?=$_SESSION[Title]?>'; if (document.forms[0].Title.value == 'Other') { document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]?>'; document.forms[0].submit(); } else if (titleSessValue == 'Other') { document.forms[0].action = 'add-beneficiary.php?from=<? echo $_GET["from"]?>&agentID=<?=$_SESSION["senderAgentID"]."&customerID=".$_SESSION["customerID"]?>'; document.forms[0].submit(); } ">
                      <option value="Mr.">Mr.</option>
                      <option value="Miss.">Miss</option>
                      <option value="Dr.">Dr.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Other">Other</option>
                    </select><script language="JavaScript">
         	SelectOption(document.forms[0].Title, "<?=$_SESSION["Title"]; ?>");
                                </script></td>
                <?
                } else {
                ?>
                 	<td>&nbsp;</td>
                 	<td>&nbsp;</td>
                 <?
                }
                if (CONFIG_CUST_NAME_SEQUENCE == '1') {
                ?>
                 	<td align="right"><font color="#005b90"><b><? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?><font color="#ff0000">*</font>&nbsp;</b></font></td>
                 	<td><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td>
                 	<?

                }else{
            	  ?>
                  <td width="20%" align="right"><font color="#005b90"><b>First 
                    Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="firstName" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td>
                <?
              	}
              	?>
                </tr>
				<? 
					 
					if ($_SESSION["Title"] == "Other" && CONFIG_TITLE_OFF != '1')
					{ ?>
				<tr  bgcolor="#ededed">
				<td align="right"><font color="#005b90"><b>Other Title </b></font></td>
				<td><input type="text" name="other_title" value="<?=$_SESSION["other_title"]; ?>" maxlength="25"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<? } ?>
				
                <tr bgcolor="#ededed"> 
                  <td width="25%" height="20" align="right"><font color="#005b90"><b><? echo (CONFIG_CUSTBEN_MIDNAME == "1" ? CONFIG_CUSTBEN_MIDNAME_TITLE : "Middle Name") ?>&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="middleName" value="<?=$_SESSION["middleName"]; ?>" maxlength="25"></td>
             	<?	if (CONFIG_CUST_NAME_SEQUENCE == '1') {  ?>
                  	<td width="20%" align="right"><font color="#005b90"><b>First 
                    Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  	<td width="37%"><input type="text" id="firstName" name="firstName" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td>
             	<?	} else {  ?>
                  <td width="37%" align="right"><font color="#005b90"><b><? echo (CONFIG_SURNAME == "1" ? CONFIG_SURNAME_VAL : "Last Name")?><font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td>
             	<?	}  ?>
                </tr>
                
               <?  if(CONFIG_BENEFICIARY_SECOND_MIDDLE == "1"){  ?>
				  				
				  				<tr bgcolor="#ededed">
				  					<td width="25%" align="right"><font color="#005b90"><b>Second Middle Name</b></font></td>
                  	<td width="37%"><input type="text" name="secMiddleName" value="<?=$_SESSION["secMiddleName"]; ?>" maxlength="25"></td>
				  					<td width="20%">&nbsp; </td>
                  	<td width="37%">&nbsp;</td>
                  </tr>
				  				
				  		<? } ?>
                
              <?  if(CONFIG_SO_BEN == "1"){  ?>
				  				
				  				<tr bgcolor="#ededed">
				  				<td width="25%">&nbsp; </td>
                  <td width="37%">&nbsp;</td>

                  	<!--<td width="20%" align="right"><font color="#005b90"><b> S/O</b></font></td>-->

                  	<td width="20%" align="right">
						<select id="sonOfType"	name="sonOfType">
							<option value="S/O" <? echo ($_SESSION["sonOfType"] == "S/O" ? "selected" : "") ?>>S/O</option>
							<option value="D/O" <? echo ($_SESSION["sonOfType"] == "D/O" ? "selected" : "") ?>>D/O</option>
							<option value="W/O" <? echo ($_SESSION["sonOfType"] == "W/O" ? "selected" : "") ?>>W/O</option>
						</select>					</td>
                  <td width="37%"><input type="text" name="S/O" value="<?=$_SESSION["S/O"]; ?>" maxlength="25"></td>
                </tr>
				  				
				  <? } ?>
				  
                 <tr bgcolor="#ededed"> 
                  <td width="25%" height="20" align="right"><font color="#005b90">
				  			<b>Date of Birth&nbsp;</b></font></td>
                  <td width="57%"><select name="dobDay" size="1" style="font-family:verdana; font-size: 11px; width:45">
                      <option value="">Day</option>
                      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                    </select> <script language="JavaScript">
         			SelectOption(document.forms[0].dobDay, "<? echo $_SESSION["dobDay"];?>");
          		</script> 
				<select name="dobMonth" size="1" style="font-family:verdana; font-size: 11px; width:50">
                      <option value="">Month</option>
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select> <script language="JavaScript">
         	SelectOption(document.forms[0].dobMonth, "<? echo $_SESSION["dobMonth"];?>");
          </script> 
				<select name="dobYear" size="1" style="font-family:verdana; font-size: 11px">
                      <option value="" selected>Year</option>
                      <?
								  	$cYear=date("Y");
								  	for ($Year=($cYear-15); $Year > ($cYear-100); $Year--)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                    </select> <script language="JavaScript">
         	SelectOption(document.forms[0].dobYear, "<?=$_SESSION["dobYear"]?>");
        </script></td> 
        <td width="20%" align="right">&nbsp;</td>
                  <td width="37%" colspan="2">&nbsp;</td>
		 </tr>

            		<tr bgcolor="#ededed">
            			<td colspan="4">&nbsp;</td>
            		</tr>
            		
					<? 
            		if ($cpf == 1){ 
            		?>
                 <tr bgcolor="#ededed">
                 		<td width="25%" align="right" valign="top"><font color="#005b90"><b>User Type<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  	<td  valign="top">
                  		<select name="userType" id="userType" onChange="newCaption();">
                  			<option value="Private User" <?=($_SESSION["userType"]=="Private User"?"selected":"")?>>Private User</option>
                  			<option value="Business User" <?=($_SESSION["userType"]=="Business User"?"selected":"")?>>Business User</option>
                  		</select>                 		</td>
                  	<td width="20%" align="right" valign="top"><div id="caption"><font color="#005b90"><b>CPF Number<font color="red">*</font>&nbsp;</b></font></div></td>
                  	<td  valign="top"><input type="text" name="CPF" value="<?=$_SESSION["CPF"]; ?>" maxlength="20">
                  		<input type="submit" value="Validate" name="Submit" onClick="document.forms[0].action='add-beneficiary.php?from=<? echo $_GET["from"];?>&agentId2=<? echo $_GET["agentID"];?>&transID=<? echo $_GET["transID"]?>';document.forms[0].sel.value = 'country';">
                 			<input type="hidden" id="cpfcpnj" name="cpfcpnj" value="CPF">                 		</td>
                 	</tr>
                <? 
                } 
                ?>

				<?
					// Added by Usman Ghani against #3299: MasterPayex - Multiple ID Types
					if (defined("CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY")
						&& CONFIG_SHOW_MULTIPLE_ID_TYPES_FUNCTIONALITY == 1 )
					{
						if ( !empty( $IDTypes ) )
						{
						?>
							<tr bgcolor="#ededed">
								<td colspan="4">
									<table width="100%">
										<tr>
										<?
											$tdNumber = 1;
											$numberOfIDTypes = count( $IDTypes );

											$chkIDTypesJS = "";

											$serverDateArray["year"] = date("Y");
											$serverDateArray["month"] = date("m");
											$serverDateArray["day"] = date("d");

											for( $i = 0; $i < $numberOfIDTypes; $i++ )
											{
												$idNumber = "";
												$idTypeValuesSESS = "";
												$issuedBy = "";
												$notes  = "";
												if( !empty( $_SESSION["IDTypesValuesData"] )
													&& !empty( $_SESSION["IDTypesValuesData"][$IDTypes[$i]["id"]] ) )
												{
												
													$idTypeValuesSESS = $_SESSION["IDTypesValuesData"];

													if(is_array($idTypeValuesSESS))
													{
														$idNumber = $idTypeValuesSESS[$IDTypes[$i]["id"]]["id_number"];
														$issuedBy = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issued_by"];
														if($userIdTypesNotesFlag){
															$notes = $idTypeValuesSESS[htmlentities($IDTypes[$i]["values"]["notes"])];
														}
													}
												}
												else if (!empty($IDTypes[$i]["values"]))
												{
													$idNumber = $IDTypes[$i]["values"]["id_number"];
													$issuedBy = $IDTypes[$i]["values"]["issued_by"];
													if($userIdTypesNotesFlag){
														$notes = htmlentities($IDTypes[$i]["values"]["notes"]);
													}
												}

												?>
													<td style="padding:2px; text-align:center; <?=($tdNumber==1 ? "border-right:1px solid black;" : "");?> ">
														<table border="1" cellpadding="2" cellspacing="0" align="center">
														  <tr>
															<td colspan="2" style="border:2px solid black; font-weight:bold;"><?=$IDTypes[$i]["title"];?> <?=($IDTypes[$i]["mandatory"] == 'Y' ? "<span style='color:red;'>*</span>" : ""); ?></td>
														  </tr>
														  <tr>
															<td width="80">ID Number:</td>
															<td>
															
	<input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id]" value="<?=( !empty($IDTypes[$i]["values"]) ? $IDTypes[$i]["values"]["id"] : "" );?>" />
																<input type="hidden" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_type_id]" value="<?=$IDTypes[$i]["id"];?>" />
																<input type="text" id="id_number_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][id_number]" value="<?=$idNumber;?>" size="40" />															</td>
														  </tr>
														  <tr>
															<td>Issued by:</td>
															<td><input type="text" id="id_issued_by_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issued_by]" value="<?=$issuedBy?>" size="40" /></td>
														  </tr>
													<? if($userIdTypesNotesFlag){?>	  
														  <tr>
															<td>Notes:</td>
															<td><input type="text" id="id_notes_<?=$IDTypes[$i]["id"];?>" name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][notes]" value="<?=$notes?>" size="40" /></td>
														  </tr>
													<? }?>
														  <tr>
															<td>Issue Date:</td>
															<td>
																<?
																	$startYear = 1985;
																	$currentYear = $serverDateArray["year"];

																	$issueDay = 0;
																	$issueMonth = 0;
																	$issueYear = 0;

																	if ( !empty($idTypeValuesSESS)
																		&& !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																	if(is_array($idTypeValuesSESS)){
																		$issueDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["day"] - 1;
																		$issueMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["month"] - 1;
																		$issueYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["issue_date"]["year"] - $startYear;                                 
																	}
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["issue_date"] );
																		$issueDay = $dateArray[2] - 1;
																		$issueMonth = $dateArray[1] - 1;
																		$issueYear = $dateArray[0] - $startYear;
																	}
																?>
																<select id="issue_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][day]" >
																</select>
																
																<select id="issue_date_month_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][month]" >
																</select>
																
																<select id="issue_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][issue_date][year]" >
																</select>

																<script type="text/javascript">
																	initDatePicker('issue_date_day_<?=$IDTypes[$i]["id"];?>', 'issue_date_month_<?=$IDTypes[$i]["id"];?>', 'issue_date_year_<?=$IDTypes[$i]["id"];?>', 
																					'', '1:12', '<?=$startYear;?>:<?=$currentYear;?>', <?=$issueDay;?>, <?=$issueMonth;?>, <?=$issueYear;?>);
																</script>															</td>
														  </tr>
														  <tr>
															<td>Expiry Date:</td>
															<td>
																<?
																	$expiryStartYear = $currentYear;
																	$expiryDay = $serverDateArray["day"] - 1; // Didn't do "less 1" here because atleast one day further from current has to be selected.
																	$expiryMonth = $serverDateArray["month"] - 1;
																	$expiryYear = $serverDateArray["year"] - $expiryStartYear;

																	if ( !empty($idTypeValuesSESS)
																		 && !empty($idTypeValuesSESS[$IDTypes[$i]["id"]]) )
																	{
																	if(is_array($idTypeValuesSESS)){
																		$expiryDay = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["day"] - 1;
																		$expiryMonth = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["month"] - 1;
																		$expiryYear = $idTypeValuesSESS[$IDTypes[$i]["id"]]["expiry_date"]["year"] - $expiryStartYear;                          
																	}
																	}
																	else if ( !empty($IDTypes[$i]["values"]) )
																	{
																		$dateArray = explode( "-", $IDTypes[$i]["values"]["expiry_date"] );

																		$expiryStartYear = ( $dateArray[0] <= $currentYear ? $dateArray[0] : $currentYear );

																		$expiryDay = $dateArray[2] - 1;
																		$expiryMonth = $dateArray[1] - 1;
																		$expiryYear = $dateArray[0] - $expiryStartYear;
																	}
																?>
																<select id="expiry_date_day_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][day]" >
																</select>

																<select id="expiry_date_month_<?=$IDTypes[$i]["id"];?>"
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][month]" >
																</select>

																<select id="expiry_date_year_<?=$IDTypes[$i]["id"];?>" 
																		name="IDTypesValuesData[<?=$IDTypes[$i]["id"];?>][expiry_date][year]" >
																</select>

																<script type="text/javascript">
																	initDatePicker('expiry_date_day_<?=$IDTypes[$i]["id"];?>', 'expiry_date_month_<?=$IDTypes[$i]["id"];?>', 'expiry_date_year_<?=$IDTypes[$i]["id"];?>', 
																					'', '1:12', '<?=$expiryStartYear;?>:2035', <?=$expiryDay;?>, <?=$expiryMonth;?>, <?=$expiryYear;?>);
																</script>															</td>
														  </tr>
														</table>													</td>
												<?
													if ( $tdNumber == 2 )
													{
														// Starting a new TR when two TDs are output.
														?>
															</tr><tr>
														<?
														$tdNumber = 1;
													}
													else
														$tdNumber++;

													$chkIDTypesJS .= "id_number_" . $IDTypes[$i]["id"] . " = document.getElementById('id_number_" . $IDTypes[$i]["id"] . "');
																			id_issued_by_" . $IDTypes[$i]["id"] . " = document.getElementById('id_issued_by_" . $IDTypes[$i]["id"] . "');";

													if ( $IDTypes[$i]["mandatory"] == 'Y' )
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
														if($userIdTypesNotesFlag){
															$chkIDTypesJS .= "
																				if ( id_notes_" . $IDTypes[$i]["id"] . ".value=='' )
																				{
																					alert('Please provide " . $IDTypes[$i]["title"] . "\\'s notes detail.');
																					id_notes_" . $IDTypes[$i]["id"] . ".focus();
																					return false;
																				}
																			";
														}
													}
													else
													{
														$chkIDTypesJS .= "
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value=='' && id_issued_by_" . $IDTypes[$i]["id"] . ".value != '' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s id number detail.');
																				id_number_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																			if ( id_number_" . $IDTypes[$i]["id"] . ".value != '' && id_issued_by_" . $IDTypes[$i]["id"] . ".value=='' )
																			{
																				alert('Please provide " . $IDTypes[$i]["title"] . "\\'s issued by detail.');
																				id_issued_by_" . $IDTypes[$i]["id"] . ".focus();
																				return false;
																			}
																		";
													}

													$chkIDTypesJS .= "
																		issueDate = new Date();
																		todayDate = new Date( issueDate );
																		expiryDate = new Date();

																		todayDate.setFullYear( " . $serverDateArray["year"] . ", " . ($serverDateArray["month"] - 1) . ", " . $serverDateArray["day"] . " );
																		issueDate.setFullYear( document.getElementById('issue_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('issue_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').value );
																		expiryDate.setFullYear( document.getElementById('expiry_date_year_" . $IDTypes[$i]["id"] . "').value, document.getElementById('expiry_date_month_" . $IDTypes[$i]["id"] . "').value - 1, document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').value );

																		if ( issueDate >= todayDate )
																		{
																			alert( 'The issue date for " . $IDTypes[$i]["title"] . " should be less than today\\'s date.' );
																			document.getElementById('issue_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																		
																		if ( expiryDate < todayDate )
																		{
																			alert( 'The expiry date for " . $IDTypes[$i]["title"] . " should be greater than today\\'s date.' );
																			document.getElementById('expiry_date_day_" . $IDTypes[$i]["id"] . "').focus();
																			return false;
																		}
																	";
											}
										?>
										</tr>
									</table>
									<script type="text/javascript">
										function checkIdTypes( theForm )
										{
											<?
												if ( !empty($chkIDTypesJS) )
													echo $chkIDTypesJS;
											?>
											return true;
										}
									</script>								</td>
							</tr>
						<?
						}
					} // End of code against #3299: MasterPayex - Multiple ID Types
					else
					{
						?>
							<tr bgcolor="#ededed"> 
							  <td width="25%" height="20" rowspan="2" align="right" <? echo (CONFIG_IDTYPE_DROPDOWN != "1" ? "valign='top'" : "") ?>><font color="#005b90"><b>ID Type<? if(CONFIG_ID_MANDATORY == '1'){echo("*");}else{echo("+");}?>&nbsp;</b></font></td>
							  <td width="37%" rowspan="2">
							  <?	if (CONFIG_IDTYPE_DROPDOWN == "1") {  ?>
								<select name="IDType">
									<option value="">- Select ID Type -</option>
								<?
									if (CONFIG_IDTYPES_FROM_LIST == "1") {
										$idTypeList = explode(",", CONFIG_IDTYPES_LIST);
										for ($i = 0; $i < count($idTypeList); $i++) {
								?>
										<option value="<? echo $idTypeList[$i] ?>" <? echo ($_SESSION["IDType"] == $idTypeList[$i] ? "selected" : "") ?>><? echo $idTypeList[$i] ?></option>
								<?	
										}
									} else {
								?>
									<option value="Passport" <? echo ($_SESSION["IDType"] == "Passport" ? "selected" : "") ?>>Passport</option>
									<option value="ID Card" <? echo ($_SESSION["IDType"] == "ID Card" ? "selected" : "") ?>>ID Card</option>
									<option value="Driving License" <? echo ($_SESSION["IDType"] == "Driving License" ? "selected" : "") ?>>Driving License</option>
								<?	}  ?>
								</select>
							  <?	} else {  
										if (CONFIG_IDTYPES_FROM_LIST == "1") {
											$idTypeList = explode(",", CONFIG_IDTYPES_LIST);
											for ($i = 0; $i < count($idTypeList); $i++) {
							  ?>
								<input type="radio" name="IDType" value="<? echo $idTypeList[$i] ?>" <? echo ($_SESSION["IDType"] == $idTypeList[$i] ? "checked" : "") ?>><? echo $idTypeList[$i] ?><br>
							  <?				
											}
										} else {
							  ?>
								<input type="radio" name="IDType" value="Passport" <? if(CONFIG_ID_MANDATORY == '1'){?>"checked"<?}?> <? if ($_SESSION["IDType"] == "Passport" ) echo "checked"; ?>>
								Passport<br>
								<input type="radio" name="IDType" value="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>>
								<? if(CONFIG_ID_TYPE_NATIONAL_ID == "1"){ ?>National ID<? }else{ ?>ID Card<? } ?><br>
								<input type="radio" name="IDType" value="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>>
								Driving Licence
							  <?	
								}  
								}
							  ?>							  </td>
							  <td width="20%" align="right" valign="top"><font color="#005b90"><b>ID Number+&nbsp;</b></font></td>
							  <td width="37%" valign="top"><input type="text" name="IDNumber" value="<?=$_SESSION["IDNumber"]; ?>" maxlength="50"></td>
							</tr>
							<tr bgcolor="#ededed">
								<td width="20%" align="right" valign="top"><font color="#005b90"><b>Other ID Name </b> </font></td>
							  <td valign="top"><input name="Otherid_name" type="text" value="<?=$_SESSION["Otherid_name"]; ?>" maxlength="15"></td>
							</tr>
							<tr bgcolor="#ededed">
								<td width="25%" height="20" align="right"><font color="#005b90"><b>Issued 
								By&nbsp;</b></font></td>
							  <td width="37%"><input type="text" name="issuedBy" value="<?=$_SESSION["issuedBy"]; ?>" maxlength="255"></td>
							  <td width="20%" align="right" valign="top"><font color="#005b90"><b>Other ID </b></font></td>
							  <td valign="top"><input name="Otherid" type="text" value="<?=$_SESSION["Otherid"]; ?>" maxlength="15"></td>
						   </tr>
							
							<tr  bgcolor="#ededed">
								
							<td align="right"><font color="#005b90"><b>ID Issue Date</b></font></td>
							<td align="left" width="37%"><?	if (CONFIG_FREETEXT_IDFIELDS == '1') {
									if ($_GET["benID"] == "") {
										$IDissuedate = (!is_array($_SESSION["IDissuedate"]) ? $_SESSION["IDissuedate"] : "");	
									} else {
										if ($_SESSION["IDissuedate"] != "0000-00-00") {
										$idIssueDay  = substr($_SESSION["IDissuedate"], 8, 2);
										$idIssueMon  = substr($_SESSION["IDissuedate"], 5, 2);
										$idIssueYear = substr($_SESSION["IDissuedate"], 0, 4);
										
										$IDissuedate = $idIssueDay . "-" . $idIssueMon . "-" . $idIssueYear;
										} else {
											$IDissuedate = "";	
										}
									}
							?>
							<input type="text" name="IDissuedate" value="<? echo $IDissuedate ?>" maxlength="25">&nbsp;&nbsp;&nbsp;<i>DD-MM-YYYY</i>
								<?
									} else {
									if ($_GET["benID"] == "") {
										$IDissuedate = (is_array($_SESSION["IDissuedate"]) ? implode("/", $_SESSION["IDissuedate"]) : $_SESSION["IDissuedate"]);	
															} else {
										if ($_SESSION["IDissuedate"] != "0000-00-00" && $_SESSION["IDissuedate"] != "") {
										$idIssueDay  = substr($_SESSION["IDissuedate"], 8, 2);
										$idIssueMon  = substr($_SESSION["IDissuedate"], 5, 2);
										$idIssueYear = substr($_SESSION["IDissuedate"], 0, 4);
										
										$IDissuedate = $idIssueDay . "/" . $idIssueMon . "/" . $idIssueYear;
										} else {
											$IDissuedate = "";	
										}
									}
								?>
								<input type="text" name="IDissuedate" value="<? echo $IDissuedate ?>" maxlength="25" readonly>
						&nbsp;<a href="javascript:show_calendar('frmBenificiary.IDissuedate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
						&nbsp;&nbsp;<input type="button" name="btnClearDate" value="ClearDate" onClick="clearIssueDate();">
					  <?	}  ?></td>
							 
							<td align="right"><font color="#005b90"><b>ID Expiry Date</b></font></td>
							<td>
							<?	if (CONFIG_FREETEXT_IDFIELDS == '1') {
									if ($_GET["benID"] == "") {
										$idExpiryDate = (!is_array($_SESSION["IDexpirydate"]) ? $_SESSION["IDexpirydate"] : "");	
									} else {
										if ($_SESSION["IDexpirydate"] != "0000-00-00") {
										$idExpiryDay  = substr($_SESSION["IDexpirydate"], 8, 2);
										$idExpiryMon  = substr($_SESSION["IDexpirydate"], 5, 2);
										$idExpiryYear = substr($_SESSION["IDexpirydate"], 0, 4);
										
										$idExpiryDate = $idExpiryDay . "-" . $idExpiryMon . "-" . $idExpiryYear;
										} else {
											$idExpiryDate = "";	
										}
									}
							?>
								<input type="text" name="IDexpirydate" value="<? echo $idExpiryDate ?>" maxlength="25"> &nbsp;&nbsp;&nbsp;&nbsp;<i>DD-MM-YYYY</i>
								<?
									} else {
									if ($_GET["benID"] == "") {
										$idExpiryDate = (is_array($_SESSION["IDexpirydate"]) ? implode("/", $_SESSION["IDexpirydate"]) : $_SESSION["IDexpirydate"]);	
									} else {
										if ($_SESSION["IDexpirydate"] != "0000-00-00" && $_SESSION["IDexpirydate"] != "") {
										$idExpiryDay  = substr($_SESSION["IDexpirydate"], 8, 2);
										$idExpiryMon  = substr($_SESSION["IDexpirydate"], 5, 2);
										$idExpiryYear = substr($_SESSION["IDexpirydate"], 0, 4);
										
										$idExpiryDate = $idExpiryDay . "/" . $idExpiryMon . "/" . $idExpiryYear;
										} else {
											$idExpiryDate = "";	
										}
									}
								?>
								<input type="text" name="IDexpirydate" value="<? echo $idExpiryDate ?>" maxlength="25" readonly>
						&nbsp;<a href="javascript:show_calendar('frmBenificiary.IDexpirydate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
						&nbsp;&nbsp;<input type="button" name="btnClearDate" value="Clear" onClick="clearExpDate();">
					  <?	}  ?>							</td>
							</tr>
					<?
					}
				?>
			
				
            		<tr bgcolor="#ededed">
            			<td colspan="4">&nbsp;</td>
            		</tr>
                <tr bgcolor="#ededed"> 
                 <td width="25%" height="20" align="right"><font color="#005b90"><b>
                  		<? /*To display House No for richalmond*/ 
                  	
                    	if(defined('ADDRESS_LINE1_LABEL'))
                    		{
                    			echo(ADDRESS_LINE1_LABEL);
                    		}else{
                    			echo("Address Line");
                    			}
                    ?>
                    		
                    <? if(CONFIG_DISABLE_ADDRESS2 != '1')
                    {
                    	if(!defined('ADDRESS_LINE1_LABEL'))
                    	{ 
                    		                  			
                    			echo("1"); 
                    			}
                    	}?>
                    			<? if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) { echo "<font color='#ff0000'>*</font>"; } 								
								?>
								<!--#4726  This config is created to make Address Line 1 a mandatory field for All users.-->
								<? if (CONFIG_BENEFICIARY_ADDRESS_MENDATORY == '1') { echo "<font color='#ff0000'>*</font>"; } 
								
								?>
								
                    &nbsp;</b></font></td>
                  <td <? if(CONFIG_DISABLE_ADDRESS2 != '1'){echo('width="26%"');}else{echo('width="76%" colspan="3"');} ?>><input type="text" name="Address" value="<?=$_SESSION["Address"]; ?>" maxlength="254" <? if(CONFIG_DISABLE_ADDRESS2 == '1'){echo('size="90"'); }?>></td>
               
               
               <? if(CONFIG_DISABLE_ADDRESS2 != '1')
               {
               	?>   
                  <td width="20%" align="right"><font color="#005b90"><b><? if(CONFIG_ADDRESS_LINE2_LABEL == "1"){ echo ADDRESS_LINE2_LABEL; }else{ ?>Address Line 2&nbsp;<? } ?></b></font></td>
                  <td width="37%"><input type="text" name="Address1" value="<?=$_SESSION["Address1"]; ?>" maxlength="254"></td>
                     <?
                }
                ?>
                </tr>
				
                <tr bgcolor="#ededed"> 
                  <td width="25%" height="20" align="right"><font color="#005b90"><b>Email&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="benEmail" value="<?=$_SESSION["benEmail"]; ?>" maxlength="100"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>
                    Zip/Postal Code<? if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI') && CONFIG_POSTCODE_NOTCOMPUL_FOR_AGENTLOGINS != 1 /*against #3639 by khola*/) { echo "<font color='#ff0000'>*</font>"; } ?></b></font></td>
                  <td width="37%">
				  <input type="text" name="Zip" value="<? echo stripslashes($_SESSION["Zip"]); ?>" maxlength="16"></td>
                </tr>
				
				      <tr bgcolor="#ededed"> 
                  <td width="25%" height="20" align="right"><font color="#005b90"><b>
                    <? echo (CONFIG_STATE_NAME!="CONFIG_STATE_NAME" ? CONFIG_STATE_NAME : "County"); ?>&nbsp;</b></font></td>
                  <td width="37%"><input type="text" name="State" value="<?=$_SESSION["State"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>City<font color="#ff0000"><? if(CONFIG_CITY_NON_COMPULSORY != '1'){ ?>*<? }?></font>&nbsp;</b></font></td>
                  <? if(CONFIG_BENEFICIARY_CITY_DROPDOWN == "1"){ 
                     if($_SESSION["Country"]!=''){
                     	
                     $cityQuery="select distinct(city) from  ".TBL_CITIES." where country = '".$_SESSION[Country]."' order by city";
                     $cityRS=selectMultiRecords($cityQuery);
                     //echo "city count-->".count($cityRS);	
                    }
                    
                  ?>
                 <td> 
                 <select name="City" style="font-family:verdana; font-size: 11px">
                 <option value="">- Select City-</option>
                 <?
                   for ($p=0; $p < count($cityRS); $p++){
									?>
									  <option  value="<? echo $cityRS[$p]["city"];?>"><? echo $cityRS[$p]["city"]; ?></option>
									 <? } ?>
                  </select>
                   <script>
                  	 SelectOption(document.forms[0].City, "<?=$_SESSION["City"]; ?>");
                    </script>                  </td>
                   <? }else{?>
                  <td width="37%"><input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25"></td>
                  <? } ?>
                </tr>
                
                <?
					// Added by Usman Ghani aginst ticket #3473 Now Transfer - Phone/Mobile validation
					$jsCode = "";
					if ( defined("CONFIG_PHONE_NUMBER_NUMERIC")
						&& CONFIG_PHONE_NUMBER_NUMERIC == 1 )
					{
						$jsCode = " onkeyup='phoneEventHandler(event);' ";
					}
					// End of code against ticket #3473 Now Transfer - Phone/Mobile validation
				?>

                <?
					// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
					if ( defined("CONFIG_PHONES_AS_DROPDOWN")
						&& CONFIG_PHONES_AS_DROPDOWN == 1 )
					{
					?>
						<tr bgcolor="#ededed">
						  <td width="25%" height="20" align="right">
								<font color="#005b90"><b>Phone<font color="#ff0000">*</font></b></font>						  </td>
						  <td align="left" colspan="3">
						  	<?
								$officePhoneSelected = "";
								$mobilePhoneSelected = "";
								$phoneNumber = ( !empty($_SESSION["Phone"])  ? $_SESSION["Phone"] : ( !empty($_SESSION["Mobile"])  ? $_SESSION["Mobile"] : "" ) );

								if ( !empty($_POST["phoneType"]) )
								{
									if ( $_POST["phoneType"] == "phone" )
										$officePhoneSelected = "selected='selected'";
									else if ( $_POST["phoneType"] == "mobile" )
										$mobilePhoneSelected = "selected='selected'";
								}
								else
								{
									if ( !empty($_SESSION["Phone"]) )
										$officePhoneSelected = "selected='selected'";
									else if ( !empty($_SESSION["Mobile"]) )
										$mobilePhoneSelected = "selected='selected'";
								}
							?>
							<input type="text" id="Phone" name="Phone"  <?=$jsCode;?>  value="<?=$phoneNumber; ?>" maxlength="30" />
							&nbsp;Type	<select id="phoneType" name="phoneType">
											<option value="">Select One</option>
											<option value="phone" <?=$officePhoneSelected;?> >Office Phone</option>
											<option value="mobile" <?=$mobilePhoneSelected;?> >Mobile Phone</option>
										</select>						  </td>
						</tr>
					<?
					} // End of code aginst ticket #3472: Now Transfer - Phone/Mobile
					else // The code, which was previously implemented, has been shifted into the following else clause after implementation of ticket #3472: Now Transfer - Phone/Mobile
					{
					?>
						<tr bgcolor="#ededed"> 
						  <td width="25%" height="20" align="right"><font color="#005b90"><b>Phone<font color="#ff0000"><? if(CONFIG_PHONE_NON_COMPULSORY != '1'){?>*<? }?></font>&nbsp;</b></font></td>
						  <td width="37%"><input type="text" id="Phone" name="Phone"  <?=$jsCode;?> value="<?=$_SESSION["Phone"]; ?>" maxlength="30"></td>
						<?	if (CONFIG_MOBILE_FIELD_OFF == '1') {  ?>
						  <td width="20%">&nbsp;</td>
						  <td width="37%">&nbsp;</td>
						<?	} else {  ?>
						  <td width="37%" align="right"><font color="#005b90"><b>Mobile<font color="#ff0000"><? if(CONFIG_MOBILE_MANDATORY == '1'){ ?>*<? }?></font></b></font></td>
						  <td width="37%"><input type="text" id="Mobile" name="Mobile" <?=$jsCode;?> value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td>
					  <?	}  ?>
						</tr>
					<?
					}
					?>
					<!--
                <tr bgcolor="#ededed"> 

                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Phone<font color="#ff0000"><? if(CONFIG_PHONE_NON_COMPULSORY != '1'){?>*<? }?></font>&nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="Phone" value="<?=$_SESSION["Phone"]; ?>" maxlength="30"></td>
            	<?	if (CONFIG_MOBILE_FIELD_OFF == '1') {  ?>
                  <td width="20%">&nbsp;</td>
                  <td width="37%">&nbsp;</td>
            	<?	} else {  ?>
                  <td width="20%" align="right"><font color="#005b90"><b>Mobile<font color="#ff0000"><? if(CONFIG_MOBILE_MANDATORY == '1'){ ?>*<? }?></font></b></font></td>
                  <td width="37%"><input type="text" name="Mobile" value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td>
              <?	}  ?>
                </tr>
         -->
				
				<?
					if(CONFIG_BEN_BANK_DETAILS == "1" && !empty($_SESSION["Country"]))
					{
				?>
				
					<tr bgcolor="#ededed">
            			<td colspan="4">&nbsp;</td>
            		</tr>
					<tr bgcolor="#ededed">
            			<td colspan="4"><font color="#005b90"><b>Beneficiary Bank Details</b></font></td>
            		</tr>
					
					
					<!--
					<tr bgcolor="#ededed"> 
 				<td width="25%" align="center" colspan="4" valign="middle">
 					<font color="#005b90"><b>Bank Name</b></font>
 					<input type="text" size="10" id="searchBankName" name="searchBankName"/>&nbsp;&nbsp;
          <font color="#005b90"><b>Swift Code</b></font>
          <input type="text" size="10" id="searchSwiftCode" name="searchSwiftCode"/>
          <input type="button" onClick=" if(document.frmBenificiary.searchBankName.value == '' && document.frmBenificiary.searchSwiftCode.value == '') { alert ('Please provide bank name or swift code to search.') } else { window.open('search-bank.php?from=add-beneficiary.php&search=Y&transID=<?=$_GET["transID"]?>'+'&bankName='+ document.frmBenificiary.searchBankName.value +'&swiftCode='+ document.frmBenificiary.searchSwiftCode.value, 'searchBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740'); document.forms[0].action='add-beneficiary.php?msg=Y&from=<? echo $_GET["from"];?>&agentID=<? echo $_GET["agentID"];?>&benID=<? echo $_GET["benID"]?>&benID=<? echo $_GET["benID"]?>'; document.forms[0].submit(); }" value="Search" name="Submit"/>
				<!--&nbsp;&nbsp;&nbsp;<span style="background-color:#000000;height:90px" class="style1">
				 	<a onclick=" window.open('add-bank.php?transID=<?=$_GET["transID"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" class="style3" href="javascript:;">
          	<b>Add New Bank</b></a>
          </span>-->
        <!--</td>
      </tr>
			-->
			
			<tr bgcolor="#ededed"> 
 				<td width="25%" align="center" colspan="4" valign="middle">
 					<font color="#005b90"><b>Bank Name</b></font>
 					<input type="text" size="10" name="searchBankName" id="searchBankName" />
 					<!--
 					&nbsp;&nbsp;
          <font color="#005b90"><b>Swift Code</b></font>
          <input type="text" size="10" id="searchSwiftCode" name="searchSwiftCode"/>
          -->
          
          																																																																																																																																																																																																																																																																																						 <!-- document.forms[0].action='add-beneficiary.php?msg=Y&from=<? echo $_GET["from"];?>&agentID=<? echo $_SESSION["agentID2"];?>&benID=<? echo $_GET["benID"]?>&transID=<? echo $_GET["transID"]?>'; document.forms[0].submit(); -->
          <?
		  	/*
			 document.forms[0].submit();
			*/
		  ?>
          <input type="button" onClick=" if(document.frmBenificiary.searchBankName.value == '' || IsAllSpaces(document.frmBenificiary.searchBankName.value)) { alert ('Please provide bank name to search.') } else { window.open('search-bank.php?from=add-beneficiary.php&search=Y&transID=<?=$_GET["transID"]?>'+'&benID=<?=$_GET["benID"]?>&bankName='+ document.frmBenificiary.searchBankName.value +'&swiftCode=&benCountry=<?=$_SESSION["Country"]?>', 'searchBank', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740'); document.forms[0].sel.value = 'country'; document.forms[0].action='add-beneficiary.php?msg=Y&from=<? echo $_GET["from"];?>&agentID=<? echo $_SESSION["agentID2"];?>&benID=<? echo $_GET["benID"]?>&transID=<? echo $_GET["transID"]?>'; }" value="Search" name="Submit"/>
		  
		  &nbsp;&nbsp;&nbsp;<a onClick=" window.open('add-bank.php?from=popUp&opener=left&transID=<?=$_GET["transID"]?>','', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="javascript:;"><b style="color:#000000">Add New Bank</b></a>        </td>
      </tr>
					
			
					
					
					<?
					     if(!empty($_SESSION["ben_bank_id"]))
									$readFlag = "readonly";
					?>
					
					
					<tr bgcolor="#ededed"> 
					  <td width="25%" height="20" align="right">
						<font color="#005b90"><b>Bank Id&nbsp;</b></font>					  </td>
					  <td width="37%">
					  	<input type="text" name="bankId" id="bankId" value="<?=$_SESSION["bankId1"]?>" maxlength="30" <?=$readFlag?>>					  </td>
					  <td width="20%">
						  <font color="#005b90"><b>Country</b></font><font color="red">*</font>&nbsp;					  </td>
					  <td width="37%">
					  <? if(!empty($_SESSION["ben_bank_id"])) {?>
					  	<input type="text" name="country1" id="country1" value="<?=$_SESSION["country1"]?>" maxlength="30" readonly="readonly" />
					  <? }else{ ?>			
						<select name="country1" id="country1"> 
							<option value="">- Select Country -</option>
						<?		
							$benBankCountrySql = "select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName";
							$myCountries = selectMultiRecords($benBankCountrySql);
						
							for ($i = 0; $i < count($myCountries); $i++)
							{
								if ($myCountries[$i]["countryName"] == $_SESSION["country1"])
								{
						?>
								  <option  value="<?=$_SESSION["country1"];?>" selected><?=$myCountries[$i]["countryName"]?></option>
						<?
								}
								else
								{
						?>
								  <option value="<?=$myCountries[$i]["countryName"]; ?>"><?=$myCountries[$i]["countryName"]; ?></option>					
						<?
								}
							} 
						
						?>		
						</select>		
					<? } ?>					  </td>
					</tr>
					
					<tr bgcolor="#ededed"> 
					  <td width="25%" height="20" align="right">
						<font color="#005b90"><b>Bank Name<font color="red">*</font>&nbsp;</b></font>					  </td>
					  <td width="37%">
					  	<input type="text" name="bankName" id="bankName" value="<?=$_SESSION["bankName1"]?>" maxlength="30" <?=$readFlag?>>					  </td>
					  <td width="20%">
						  <font color="#005b90"><b>Swift Code</b></font>&nbsp;					  </td>
					  <td width="37%">
					  	<input type="text" name="swiftCode" id="swiftCode" value="<?=$_SESSION["swiftCode1"]?>" maxlength="30">					  </td>
					</tr>
					
					<tr bgcolor="#ededed"> 
					  <td width="25%" height="20" align="right">
						<font color="#005b90"><b>Branch Code/Name&nbsp;</b></font>					  </td>
					  <td width="37%">
					  	<input type="text" name="branchName" id="branchName" value="<?=$_SESSION["branchCode1"]?>" maxlength="30">					  </td>
					  <td width="20%">
						  <font color="#005b90"><b>Branch Address</b>&nbsp;</font>					  </td>
					  <td width="37%">
					  	<input type="text" name="branchAddress" id="branchAddress" value="<?=$_SESSION["branchAddress1"]?>" maxlength="30" >					  </td>
					</tr>
					<tr bgcolor="#ededed"> 
					  <td width="25%" height="20" align="right">
						<font color="#005b90"><b>Account Number<font color="red">*</font>&nbsp;</b></font>					  </td>
					  <td width="37%">
					  	<input type="text" name="account" id="account" value="<?=$_SESSION["account1"]?>" maxlength="30">					  </td>
					  <td width="20%">
						  <font color="#005b90"><b>IBAN</b></font>&nbsp;					  </td>
					  <td width="37%">
					  	<input type="text" name="iban" id="iban" value="<?=$_SESSION["iban1"]?>" maxlength="30" onBlur="validateIban();">
					  	<div id="benIbanValidStatus"></div>
					  	<input type="hidden" id="ibnStatus" value="0">					  </td>
					</tr>
					<tr bgcolor="#ededed"> 
					  <td width="25%" height="20" align="right" valign="top">
						<font color="#005b90"><b>Account Type<font color="red">*</font>&nbsp;</b></font>					  </td>
					  <td width="37%">
					  	<?
					  		if($_SESSION["accountType1"] == "Savings")
					  		{
					  			$svingAcnt = "Checked";
					  			$crntAcnt = "";
					  		}
					  		else
					  		{
					  			$svingAcnt = "";
					  			$crntAcnt = "Checked";
					  		}
					  	?>
					  	<input type="radio" name="accountType" id="accountType" value="Current" maxlength="30" <?=$crntAcnt?>>&nbsp;Current
					  	<br />
						<input type="radio" name="accountType" id="accountType" value="Savings" maxlength="30" <?=$svingAcnt?>>&nbsp;Saving					  </td>
					  <td width="20%">&nbsp;					  </td>
					  <td width="37%">&nbsp;					  </td>
					</tr>
					<input type="hidden" name="usedBankId" id="usedBankId" value="<?=$_SESSION["ben_bank_id"]?>" />
					
				
				<?
					}
				?>
				
                <tr bgcolor="#ededed"> 


                  <td width="25%" height="20" align="right">&nbsp;</td>
                  <td width="37%">&nbsp;</td>
                  <td width="20%" align="right"><font color="#005b90"><b>&nbsp;</b></font></td>
                  <td width="37%">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center">
			<input type="hidden" name="customerID" value="<? echo $_SESSION["customerID"]?>">
			<input name="Save" id="formSubmitButton" type="submit" value=" Save "  onClick="return checkForm(document.frmBenificiary);">
              &nbsp;&nbsp; <input name="reset" type="reset" value=" Clear"> </td>
          </tr>
        </table></td>
  </tr>
</form>
</table>
</body>
</html>
