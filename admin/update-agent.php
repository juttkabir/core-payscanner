<?php
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
include("connectOtherDataBase.php");
$agentType2 = getAgentType();
//debug($agentType2);
$parentID = $_SESSION["loggedUserData"]["userID"];
//debug("hhhhh");
 /**  
 * #6747:Premier Exchange
 * Agent User type is displayed on agent form.
 * Enable  : "1"
 * Disable  : "0"
 * by Aslam Shahid
 */
$agentUserTypeFlag = false;
if(CONFIG_AGENT_USER_TYPE == "1"){
	$agentUserTypeFlag = true;	
}
$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}

$agentIDNumberFlag = false;
if(defined("CONFIG_SUP_AGENT_DIST_ID_NUMBER") && CONFIG_SUP_AGENT_DIST_ID_NUMBER=="1"){
	$agentIDNumberFlag = true;
}
if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$type = $_GET["type"];
}
if ($_GET["ida"] != ""){
	$ida = $_GET["ida"];
}
if($_GET["pageID"] != ""){
	$pageID = $_GET["pageID"];
	
	}
if($_GET["pageID"] == "alertPage"){
	
	$goBackPage = "viewAlertController.php";
	
}else{
	
	$goBackPage = "agent-list.php";
	
	}
	
	$alertID = $_GET["alertID"];


if($_GET["pageID"] != ""){                             
 		        $pageID = $_GET["pageID"];               
 		                                                 
 		        }                                        
 		if($_GET["pageID"] == "alertPage"){              
 		                                                 
 		        $goBackPage = "viewAlertController.php"; 
 		                                                 
 		}else{                                           
 		                                                 
 		        $goBackPage = "agent-list.php";          
 		                                                 
 		        }                                        
 		                                                 
 $alertID = $_GET["alertID"]; 



if($_GET["userID"] != "")
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
	
else
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_POST["userID"]."'");

/**
 * If at first time than asign the default value to NO
 * @Ticket# 3506
 */
if(empty($_SESSION["replaceDefaultDistributor"]))
	$_SESSION["replaceDefaultDistributor"] = "N";

$managerLabel = "Select Branch Manager";
if(CONFIG_POPULATE_USERS_DROPDOWN=="1")
	$managerLabel = "Select Account Manager";

$arguments = array("flag"=>"populateUsers","selectedID"=>$agentDetails["parentID"]);
$populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN",$arguments,CONFIG_POPULATE_USERS_DROPDOWN);

$validationFlag = true;
if(CONFIG_UPDATE_AGENT_INTRODUCER_FIELDS == '1'){
	if($_REQUEST[ida]!='Y' && $agentType2 == 'SUPA')
		$validationFlag = false;
}
//debug($_REQUEST[ida].'&&'. $agentType2.'*'.CONFIG_UPDATE_AGENT_INTRODUCER_FIELDS);
?>
<html>
<head>
	<title>Update Super Agent</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
<?	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {  ?>
	if(theForm.manualAgentNum.value == "" || IsAllSpaces(theForm.manualAgentNum.value)){
  	alert("Please provide the agent's Login Name.");
      theForm.manualAgentNum.focus();
      return false;
  }
<?	}  ?>
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	/*if(theForm.postcode.value == "" || IsAllSpaces(theForm.postcode.value)){
    	alert("Please provide the agent's Post Code.");
        theForm.postcode.focus();
        return false;
    }*/
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	
	<? if(CONFIG_CITY_MANDATORY == "1") {?>
	
	 if(theForm.City.value == "" || IsAllSpaces(theForm.City.value)){
    	alert("Please provide the Agnet City Name.");
        theForm.City.focus();
        return false;
    }
	<? } ?>
	
	<?
		/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
		if ( defined("CONFIG_POST_CODE_MENDATORY")
			&& CONFIG_POST_CODE_MENDATORY == 1 )
		{
			?>
				if ( theForm.agentZip.value == "" || IsAllSpaces(theForm.agentZip.value ) )
				{
					alert("Please provide post code.");
					theForm.agentZip.focus();
					return false;
				}
			<?
		}
		/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
	?>
	/*
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
<?php 
if($validationFlag){?>
   /*
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the agent's email address.");
        theForm.email.focus();
        return false;
    } else {
	}
	if(theForm.agentCompDirector.value == "" || IsAllSpaces(theForm.agentCompDirector.value)){
    	alert("Please provide the agent company director's name.");
        theForm.agentCompDirector.focus();
        return false;
    }
	if(theForm.agentBank.value == "" || IsAllSpaces(theForm.agentBank.value)){
    	alert("Please provide the agent's Bank name.");
        theForm.agentBank.focus();
        return false;
    }
	if(theForm.agentAccountName.value == "" || IsAllSpaces(theForm.agentAccountName.value)){
    	alert("Please provide the agent's bank account name.");
        theForm.agentAccountName.focus();
        return false;
    }
	if(theForm.agentAccounNumber.value == "" || IsAllSpaces(theForm.agentAccounNumber.value)){
    	alert("Please provide the agent's bank account number.");
        theForm.agentAccounNumber.focus();
        return false;
    }
	if(theForm.agentCurrency.options.selectedIndex == 0){
    	alert("Please select agent's currency.");
        theForm.agentCurrency.focus();
        return false;
    }
	*/
<?
/**
 * To make field mandatory (CONFIG based) if it not distributor
 * @Ticket #4158
 */
	if (CONFIG_AGENT_ACCOUNT_LIMIT == '1' && CONFIG_AGENT_LIMIT =='1' && $_GET["ida"]!="Y"){ ?>
		if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
			alert("Please provide the agent's account limit.");
			theForm.agentAccountLimit.focus();
			return false;
		}
<? } ?>
	/*
	if(theForm.agentCommission.value == "" || IsAllSpaces(theForm.agentCommission.value)){
    	alert("Please provide the agent's commision percentage.");
        theForm.agentCommission.focus();
        return false;
    }*/
	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value)){
    	alert("Please select atleast one country.");
        theForm.IDAcountry.focus();
        return false;
    }
<?	if (CONFIG_BACK_DATED == '1' && $_GET["ida"] != "Y" && $agentType == 'admin') {  ?>
	if (document.getElementById('rights').value == 'Backdated') {
		if (document.getElementById('backDays').value != '') {
			if (document.getElementById('backDays').value == '0') {
				alert("Please provide back days other than zero");
				document.getElementById('backDays').focus();
				return false;
			}
			if (!isNumeric(document.getElementById('backDays').value)) {
				alert("Please provide the positive numeric back days");
				document.getElementById('backDays').focus();
				return false;
			}
		}
	}
<?	}  ?>

// Added By Niaz Ahmad #2810 Connect Plus at 30-01-2008 
      <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?>
          	
       	if(theForm.currencyOrigin.value == "" || IsAllSpaces(theForm.currencyOrigin.value)){
    	  alert("Please provide the agent's settlement currency.");
        theForm.currencyOrigin.focus();
        return false;
    }
          <? } ?> 	

	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/***** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents *****/
		?>	
			assocDistRadioButton = document.getElementById("letMeSelectAssocDist");
			if ( assocDistRadioButton != null && assocDistRadioButton != undefined )
			{
				if( assocDistRadioButton.checked == true )
				{
					// In this case, Atleast one distributor should be selected.
					// User is not allowed to select all the distributors.
					// If user selects all the distributors, prompt him/her to check the "All" distributors radio button.
					distListControl = document.getElementById("distList");
					totalDistributors = distListControl.options.length
					numberOfSelectedDist = countSelectedItems( distListControl );
					if ( numberOfSelectedDist < 1 )
					{
						alert( "Please select atleast one distributor." );
						assocDistRadioButton.focus();
						return false;
					}
					else if ( numberOfSelectedDist == totalDistributors )
					{
						alert( "If you want all the distributors to be associated with this agent,\nplease click on the 'All' distributors option." );
						assocDistRadioButton.focus();
						return false;
					}
				}
			}
		<?
		}
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	?>

	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	
function clearExpDate() {	// [by Jamshed]
	document.frmAgents.dDate.value = "";	
}

function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}

/**
 * Show hide the replace default distributor option
 * @Ticket# 3506
 */
function showHideReplaceBlock()
{
	if(document.getElementById("defaultDistrib").value == 'Y') 
		$("#showReplaceBlock").show();
	else
		$("#showReplaceBlock").hide();
}


function showBackDays() {
	if (document.getElementById('rights').value == 'Backdated') {
		document.getElementById('howmanybackdays').style.visibility = 'visible';
		document.getElementById('howmanybackdays').style.position = 'relative';
	} else {
		document.getElementById('howmanybackdays').style.visibility = 'hidden';
		document.getElementById('howmanybackdays').style.position = 'absolute';
	}
}
	
	</script>

	<?
		if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
			&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 )
		{
			/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ***/
			$onloadJS = "onLoadJS(); ";
		?>
			<script>
				
				function onLoadJS()
				{
					updateAssocDistControls();
				}
		
				function updateAssocDistControls()
				{
					if( document.getElementById("letMeSelectAssocDist").checked == true )
						document.getElementById("distList").disabled = false;
					else
						document.getElementById("distList").disabled = true;
				}
		
				function countSelectedItems( listControl )
				{
					totalItems = listControl.options.length;
					numberOfSelectedItems = 0;
					for (i = 0; i < totalItems; i++)
					{
						if ( listControl.options[i].selected == true )
							numberOfSelectedItems++;
					}
					return numberOfSelectedItems;
				}
				document.onload = onLoadJS;
			</script>
		<?
		}
		/***** End of code against #3428: Connect Plus - Select Distributors for Agents *****/
	?>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body onLoad="<?=(!empty($onloadJS) ? $onloadJS : ""); ?>">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update <?= __("Agent"); ?>  Correspondent</font></strong></td>
  </tr>
  <form name="frmAgents" action="add-agent-conf.php?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>&type=<?=$type;?>&ida=<?=$ida;?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
  <input type="hidden" name="userID" value="<?=$agentDetails["userID"]; ?>">
  <input type="hidden" name="alertID" value="<?=$alertID; ?>">
  <input type="hidden" name="pageID" value="<?=$pageID; ?>">
  <tr>
    <td align="center">
		<table width="448" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update <?=($ida == 'Y')? "Distributor":__("Agent"); ?></strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		  <tr bgcolor="#EEEEEE"> 
        <td colspan="2"> 
          <table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td></tr>
          </table>
         </td>
      </tr>
    <?php } 
		if($validationFlag){?>
   	<tr bgcolor="#ededed">

			<td height="19" colspan="2"><a class="style2" href="<?=$goBackPage ?>?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>&type=<?=$type;?>&ida=<?=$ida;?>&alertID=<?=$alertID;?>&pageID=<?=$pageID; ?>">Go Back</a></td>
		</tr>
	<?php }?>
		<tr bgcolor="#ededed">
			<td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
		
		
	
	<?php	
		
		if($agentDetails["agentType"] == "Supper" && $agentDetails["isCorrespondent"] == "N" || $agentDetails["isCorrespondent"] == "Y" )
		{
			if($agentType2 != 'Branch Manager' && $validationFlag){
		//$nAgentType = $_GET["type"];
		if(IS_BRANCH_MANAGER){
		?>
		
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>
             
               <?php 
				 echo $managerLabel;
              ?>
              </strong></font></td>
            <td>
			<?php
			if(!empty($populateUser))
				echo $populateUser;
			else{
			?>
              <select name="supAgentID" id="supAgentID" style="font-family:verdana; font-size: 11px; width:226" >
                <option value="">- Select One -</option>
                <?
					$agents = selectMultiRecords("select userID,name,username, agentCompany, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Branch Manager'");
					for ($i=0; $i < count($agents); $i++){
				if($agents[$i]["userID"] == $agentDetails["parentID"])
				{
				?>
                <option value="<? echo $agentDetails["parentID"]; ?>" selected >
                <? echo $agents[$i]["name"]." [".$agents[$i]["username"]."]"; ?>
                </option>
                <?
				}
				else
				{					
				?>
                <option value="<? echo $agents[$i]["userID"]; ?>">
                <? echo $agents[$i]["name"]." [".$agents[$i]["username"]."]"; ?>
                </option>
                <?
			}
					}
				?>
              </select>
			  <?php }?>
            </td>
          </tr>
          <?
        }
		}
	if($agentUserTypeFlag && $validationFlag){
	?>
	<tr bgcolor="#ededed"> 
		<td><font color="#005b90"><strong>Account Type</strong></font></td>
		<td>
	   <select id="agentUserType" name="agentUserType">
			<option value="Introducer">Introducer</option>
			<option value="Management Company">Management Company</option>
		</select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentUserType, "<?=$agentDetails["agentUserType"]; ?>");
                                </script>
		</td>
	</tr>
<?php 
	}
}?>			
		   <!-- Added by Niaz Ahmad #3096 at 17-04-2008 -->
		  <?
		  if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y"){
		  	
		  	$loginName = __("Agent") ." Login Name";
		  }elseif(CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y"){
		  	$loginName = "Distributor Login Name";
		  	}
		  ?>
		 
		  <?	if (CONFIG_MANUAL_AGENT_NUMBER == "1" && $_GET["ida"] != "Y" || CONFIG_MANUAL_DISTRIBUTOR_NUMBER == "1" && $_GET["ida"] == "Y") {  ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><?=$loginName?><font color="#ff0000">*</font></strong></font></td>
            <td width="268">
            	<input type="text" name="manualAgentNum" value="<?=stripslashes($agentDetails["username"]); ?>" size="35" maxlength="255" readonly="readonly">
            	<input type="hidden" name="manualAgentNum2" value="<?=stripslashes($agentDetails["username"]); ?>">
            </td>
        </tr>
      <?	}  ?>
      
       <? if(CONFIG_DISTRIBUTOR_ALIAS == "1" && $_GET["ida"] == "Y") {?>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> Distributor Alias</strong></font></td>
            <td>
              <input type="text" name="distAlias" value="<?=stripslashes($agentDetails["distAlias"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
           <? } ?> 
           
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><?= __("Agent") ;?> Company Name<font color="#ff0000">*</font></strong></font></td>
            <td width="268"><input type="text" name="agentCompany" value="<?=stripslashes($agentDetails["agentCompany"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong><?= __("Agent"); ?> Contact Person<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="agentContactPerson" value="<?=stripslashes($agentDetails["agentContactPerson"]); ?>" size="35" maxlength="100"></td>
        </tr>
		<?		if(defined("CONFIG_SHOW_INTRODUCER_FIELD") && (strstr(CONFIG_SHOW_INTRODUCER_FIELD,$agentType.",") || CONFIG_SHOW_INTRODUCER_FIELD=="1") &&  $validationFlag){ ?>

		<tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Source</strong></font></td>
            <td><input type="text" name="source" value="<?=stripslashes($agentDetails["source"]); ?>" size="35" maxlength="100"></td>
        </tr>
		<? } ?>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 1<font color="#ff0000">*</font></b></font></td>
            <td><input type="text" name="agentAddress" value="<?=stripslashes($agentDetails["agentAddress"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><input type="text" name="agentAddress2" value="<?=stripslashes($agentDetails["agentAddress2"]); ?>" size="35" maxlength="255"></td>
        </tr>
		
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><? echo $agentDetails["correspondent"];?><b>Country<font color="#ff0000">*</font></b></font></td>
            <td><input type="hidden" value="<? echo $agentDetails["agentCountry"];?>" name="oldCountry"><SELECT name="Country" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select Country-</OPTION>
				<!--option value="United States">United States</option>
				<option value="Canada">Canada  correspondent </option-->
				<?
				if(CONFIG_ENABLE_ORIGIN == "1"){
                	
        	if($agentDetails["isCorrespondent"] == "ONLY"){
        		$countryTypes = " and countryType like '%destination%' ";
        	}else{
        		$countryTypes = " and countryType like '%origin%' ";
        		$agentCountry = " ";
        	}	
                        	
        }else{
        		$countryTypes = " ";
        		$agentCountry = " and countryName ='United Kingdom' ";
        	}
				
				 if(CONFIG_COUNTRY_SERVICES_ENABLED){
            if($agentDetails["isCorrespondent"] == "ONLY"){
						$serviceTable = TBL_SERVICE;
						if($countryBasedFlag){
							$serviceTable = TBL_SERVICE_NEW;
						}            	
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
						}
						else
						{
							$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName";
							if($countryBasedFlag){
								$countiresQuery = "select distinct(countryName), countryId from ".TBL_COUNTRY.", ".TBL_SERVICE_NEW." where 1 and  countryId = fromCountryId $countryTypes $agentCountry order by countryName";
							}
							$countires = selectMultiRecords($countiresQuery);
						}
					}else
					{
							if($agentDetails["isCorrespondent"] == "ONLY"){
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
						}
						else
						{
							$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
						}
					}					
						//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<? if($_POST["Country"] == "") echo $agentDetails["agentCountry"]; else echo $_POST["Country"]; ?>");
                                </script></td>
        </tr>
		<!--<? if ($agentDetails["agentCountry"] != "United States" && $agentDetails["agentCountry"] != "Canada" && $agentDetails["agentCountry"] != "United Kingdom"){ 
				if($_POST[Country] == "")
					$cities = selectMultiRecords("select distinct city from ".TBL_CITIES." where country='$agentDetails[agentCountry]' order by city");
				else
					$cities = selectMultiRecords("select distinct city from ".TBL_CITIES." where country='$_POST[Country]' order by city");
		?>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>City</b></font></td>
            <td><SELECT name="City" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select City-</OPTION>
				<?
					for ($i=0; $i < count($cities); $i++){
				?>
				<OPTION value="<?=$cities[$i]["city"]; ?>"><?=$cities[$i]["city"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].City, "<?=trim($agentDetails[agentCity]); ?>");
                                </script></td>
        </tr><? } else {
		?><?
		} ?> -->
				<tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>City</b></font>
			<? if(CONFIG_CITY_MANDATORY == "1"){ ?><font color="#ff0000">*</font><? } ?>
			</td>
            <td> 
              <input type="text" name="City" value="<?=stripslashes($agentDetails["agentCity"]);?>" size="35" maxlength="255">
            </td>
        </tr>
				<tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>
              <? if ($agentDetails["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code</b></font>
				<?
					/***** Code added by Usman Ghani aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
					if ( defined("CONFIG_POST_CODE_MENDATORY")
						&& CONFIG_POST_CODE_MENDATORY == 1 )
					{
					?>
						<strong><font color="#ff0000">*</font></strong>
					<?
					}
					/***** End of code aginst ticket #3467 Now Transfer - Postcode Mandatory *****/
				?></td>
            <td><input type="text" name="agentZip" value="<?=stripslashes($agentDetails["agentZip"]); ?>" size="35" maxlength="7"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Phone<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="agentPhone" value="<?=stripslashes($agentDetails["agentPhone"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Mobile</strong></font></td>
            <td><input type="text" name="mobile" value="<?=stripslashes($agentDetails["mobile"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><input type="text" name="agentFax" value="<?=stripslashes($agentDetails["agentFax"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Email</strong></font></td>
            <td><input type="text" name="email" value="<?=stripslashes($agentDetails["email"]); ?>" size="35" maxlength="255"></td>
        </tr>
<?php if($validationFlag){?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td><input type="text" name="agentURL" value="<?=stripslashes($agentDetails["agentURL"]); ?>" size="35" maxlength="255"></td>
        </tr>
		<?php if(CONFIG_HIDE_MSB_AGENT!="1"){?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>MSB Number</strong></font>
			 <? if(CONFIG_REMOVE_MSB_TEXT_FROM_ADD_AGENT_DISTRIBUTOR !="1"){?>
			<br><font size="1">OR (State Bank license or permission from Ministry 
              of Finance)</font>
			  <? } ?>
		 </td>
            <td><input type="text" name="agentMSBNumber" value="<?=stripslashes($agentDetails["agentMSBNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>MSB Expiry</strong></font></td>
            <td>
					<SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].msbDay, "<? echo substr($agentDetails["agentMCBExpiry"], 8,2);?>");
          		</script>
          		<SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbMonth, "<? echo substr($agentDetails["agentMCBExpiry"], 5,2); ?>");
          </script>
          		
          		 <SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<=($cYear+20);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbYear, "<?echo substr($agentDetails["agentMCBExpiry"], 0,4); ?>");
          </script></td>
        </tr>
		<?php }?>
        <? if(CONFIG_AGENT_HOUSE_NUMBER == "1"){?>
         <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>House Number</strong></font></td>
            <td><input type="text" name="agentHouseNumber" value="<?=stripslashes($agentDetails["agentHouseNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <? } ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Registration Number</strong></font></td>
            <td><input type="text" name="agentCompRegNumber" value="<?=stripslashes($agentDetails["agentCompRegNumber"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Director</strong></font></td>
            <td><input type="text" name="agentCompDirector" value="<?=stripslashes($agentDetails["agentCompDirector"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td><input type="text" name="agentDirectorAdd" value="<?=stripslashes($agentDetails["agentDirectorAdd"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong>Director Proof ID</strong></font></td>
            <td valign="top"><input type="radio" name="agentProofID" value="Passport" <? if ($agentDetails["agentProofID"] == "Passport" || $agentDetails["agentProofID"] == "") echo "checked"; ?>>Passport <br><input type="radio" name="agentProofID" value="Driving License" <? if ($agentDetails["agentProofID"] == "Driving License") echo "checked"; ?>>Driving License <br><input type="radio" name="agentProofID" value="Other" <? if ($agentDetails["agentProofID"] == "Other") echo "checked"; ?>>Other please specify <input type="text" name="otherProofID" value="<?=$agentDetails[otherProofID]; ?>" size="15"></td>
        </tr>
		  <? if($agentIDNumberFlag){?>
			  <tr bgcolor="#ededed"> 
				<td width="169"><font color="#005b90"><strong>ID Number</strong></font></td>
				<td>
				  <input type="text" name="agentIDNumber" value="<?=stripslashes($agentDetails["agentIDNumber"]); ?>" size="35" maxlength="255">
				</td>
			  </tr>
		  <? }?>		
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td>
         	<?	if (CONFIG_FREETEXT_IDFIELDS == '1') {
        				if ($agentDetails["agentIDExpiry"] != "0000-00-00 00:00:00") {
        					$idExpiryDay  = substr($agentDetails["agentIDExpiry"], 8, 2);
        					$idExpiryMon  = substr($agentDetails["agentIDExpiry"], 5, 2);
        					$idExpiryYear = substr($agentDetails["agentIDExpiry"], 0, 4);
        					
        					$agentIDExpiryDate = $idExpiryDay . "-" . $idExpiryMon . "-" . $idExpiryYear;
        				} else {
        					$agentIDExpiryDate = "";	
        				}         		
         	?>
        	    <input name="dDate" type="text" id="dDate" value="<? echo $agentIDExpiryDate ?>"> &nbsp;&nbsp;&nbsp;&nbsp;<i>DD-MM-YYYY</i>
        	<?	} else if (CONFIG_IDEXPIRY_WITH_CALENDAR == "1") {
        				if ($agentDetails["agentIDExpiry"] != "0000-00-00 00:00:00") {
        					$idExpiryDay  = substr($agentDetails["agentIDExpiry"], 8, 2);
        					$idExpiryMon  = substr($agentDetails["agentIDExpiry"], 5, 2);
        					$idExpiryYear = substr($agentDetails["agentIDExpiry"], 0, 4);
        					
        					$agentIDExpiryDate = $idExpiryDay . "/" . $idExpiryMon . "/" . $idExpiryYear;
        				} else {
        					$agentIDExpiryDate = "";	
        				}
        	?>
        	    <input name="dDate" type="text" id="dDate" readonly value="<? echo $agentIDExpiryDate ?>">
        	    &nbsp;<a href="javascript:show_calendar('frmAgents.dDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
        	    &nbsp;&nbsp;<input type="button" name="btnClearDate" value="Clear" onClick="clearExpDate();">
        	<?	} else { ?>
            	<SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idMonth, "<? echo substr($agentDetails["agentIDExpiry"], 5,2); ?>");
          </script>
					<SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].idDay, "<? echo substr($agentDetails["agentIDExpiry"], 8,2);?>");
          		</script> <SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<=($cYear+20);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idYear, "<? echo substr($agentDetails["agentIDExpiry"], 0,4);?>");
          </script>
        <?	}  ?>
          </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong> Document Provided</strong></font></td>
            <td><input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($agentDetails["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
MSB License <br>
<input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($agentDetails["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
Company Registration Certificate <br>
<input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($agentDetails["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
Utility Bill <br>
<input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($agentDetails["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
Director Proof of ID</td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Bank</strong></font></td>
            <td><input type="text" name="agentBank" value="<?=stripslashes($agentDetails["agentBank"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Name</strong></font></td>
            <td><input type="text" name="agentAccountName" value="<?=stripslashes($agentDetails["agentAccountName"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Number</strong></font></td>
            <td><input type="text" name="agentAccounNumber" value="<?=stripslashes($agentDetails["agentAccounNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Branch Code</strong></font></td>
            <td><input type="text" name="agentBranchCode" value="<?=stripslashes($agentDetails["agentBranchCode"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Type</strong></font></td>
            <td><select name="agentAccountType">
			<option value="Current">Current</option>
			<option value="Saving">Saving</option>
			<option value="Other">Other</option>
			</select><script language="JavaScript">
						         			SelectOption(document.forms[0].agentAccountType, "<? echo $agentDetails["agentAccountType"]; ?>");
						          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Currency</strong></font></td>
            <td><SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
                                        <OPTION VALUE="" SELECTED>-Select One-</option>
										<OPTION VALUE="USD">US Dollar</option>
                                        <OPTION VALUE="AFA">Afghanistan Afghani</option>
                                        <OPTION VALUE="ALL">Albanian Lek</option>
                                        <OPTION VALUE="DZD">Algerian Dinar</option>
                                        <OPTION VALUE="ADF">Andorran Franc</option>
                                        <OPTION VALUE="ADP">Andorran Peseta</option>
                                        <OPTION VALUE="AON">Angolan New Kwanza</option>
                                        <OPTION VALUE="ARS">Argentine Peso</option>
                                        <OPTION VALUE="AWG">Aruban Florin</option>
                                        <OPTION VALUE="AUD">Australian Dollar</option>
                                        <OPTION VALUE="ATS">Austrian Schilling</option>
                                        <OPTION VALUE="BSD">Bahamian Dollar </option>
                                        <OPTION VALUE="BHD">Bahraini Dinar</option>
                                        <OPTION VALUE="BDT">Bangladeshi Taka</option>
                                        <OPTION VALUE="BBD">Barbados Dollar</option>
                                        <OPTION VALUE="BEF">Belgian Franc</option>
                                        <OPTION VALUE="BZD">Belize Dollar</option>
                                        <OPTION VALUE="BMD">Bermudian Dollar</option>
                                        <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                                        <OPTION VALUE="BOB">Bolivian Boliviano</option>
                                        <OPTION VALUE="BWP">Botswana Pula</option>
                                        <OPTION VALUE="BRL">Brazilian Real</option>
                                        <OPTION VALUE="GBP">British Pound</option>
                                        <OPTION VALUE="BND">Brunei Dollar</option>
                                        <OPTION VALUE="BGL">Bulgarian Lev</option>
                                        <OPTION VALUE="BIF">Burundi Franc</option>
                                        <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                                        <OPTION VALUE="XAF">CFA Franc BEAC</option>
                                        <OPTION VALUE="XPF">CFP Franc</option>
                                        <OPTION VALUE="KHR">Cambodian Riel</option>
                                        <OPTION VALUE="CAD">Canadian Dollar</option>
                                        <OPTION VALUE="CVE">Cape Verde Escudo</option>
                                        <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                                        <OPTION VALUE="CLP">Chilean Peso</option>
                                        <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                                        <OPTION VALUE="COP">Colombian Peso</option>
                                        <OPTION VALUE="KMF">Comoros Franc</option>
                                        <OPTION VALUE="CRC">Costa Rican Colon</option>
                                        <OPTION VALUE="HRK">Croatian Kuna</option>
                                        <OPTION VALUE="CUP">Cuban Peso</option>
                                        <OPTION VALUE="CYP">Cyprus Pound</option>
                                        <OPTION VALUE="CZK">Czech Koruna</option>
                                        <OPTION VALUE="DKK">Danish Krone</option>
                                        <OPTION VALUE="DJF">Djibouti Franc</option>
                                        <OPTION VALUE="DOP">Dominican R. Peso</option>
                                        <OPTION VALUE="NLG">Dutch Guilder</option>
                                        <OPTION VALUE="XEU">ECU</option>
                                        <OPTION VALUE="XCD">East Caribbean Dollar</option>
                                        <OPTION VALUE="ECS">Ecuador Sucre</option>
                                        <OPTION VALUE="EGP">Egyptian Pound</option>
                                        <OPTION VALUE="SVC">El Salvador Colon</option>
                                        <OPTION VALUE="EEK">Estonian Kroon</option>
                                        <OPTION VALUE="ETB">Ethiopian Birr</option>
                                        <OPTION VALUE="EUR">Euro</option>
                                        <OPTION VALUE="FKP">Falkland Islands Pound</option>
                                        <OPTION VALUE="FJD">Fiji Dollar</option>
                                        <OPTION VALUE="FIM">Finnish Markka</option>
                                        <OPTION VALUE="FRF">French Franc</option>
                                        <OPTION VALUE="GMD">Gambian Dalasi</option>
                                        <OPTION VALUE="DEM">German Mark</option>
                                        <OPTION VALUE="GHC">Ghanaian Cedi</option>
                                        <OPTION VALUE="GIP">Gibraltar Pound</option>
                                        <OPTION VALUE="XAU">Gold (oz.)</option>
                                        <OPTION VALUE="GRD">Greek Drachma</option>
                                        <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                                        <OPTION VALUE="GNF">Guinea Franc</option>
                                        <OPTION VALUE="GYD">Guyanese Dollar</option>
                                        <OPTION VALUE="HTG">Haitian Gourde</option>
                                        <OPTION VALUE="HNL">Honduran Lempira</option>
                                        <OPTION VALUE="HKD">Hong Kong Dollar</option>
                                        <OPTION VALUE="HUF">Hungarian Forint</option>
                                        <OPTION VALUE="ISK">Iceland Krona</option>
                                        <OPTION VALUE="INR">Indian Rupee</option>
                                        <OPTION VALUE="IDR">Indonesian Rupiah</option>
                                        <OPTION VALUE="IRR">Iranian Rial</option>
                                        <OPTION VALUE="IQD">Iraqi Dinar</option>
                                        <OPTION VALUE="IEP">Irish Punt</option>
                                        <OPTION VALUE="ILS">Israeli New Shekel</option>
                                        <OPTION VALUE="ITL">Italian Lira</option>
                                        <OPTION VALUE="JMD">Jamaican Dollar</option>
                                        <OPTION VALUE="JPY">Japanese Yen</option>
                                        <OPTION VALUE="JOD">Jordanian Dinar</option>
                                        <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                                        <OPTION VALUE="KES">Kenyan Shilling</option>
                                        <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                                        <OPTION VALUE="LAK">Lao Kip</option>
                                        <OPTION VALUE="LVL">Latvian Lats</option>
                                        <OPTION VALUE="LBP">Lebanese Pound</option>
                                        <OPTION VALUE="LSL">Lesotho Loti</option>
                                        <OPTION VALUE="LRD">Liberian Dollar</option>
                                        <OPTION VALUE="LYD">Libyan Dinar</option>
                                        <OPTION VALUE="LTL">Lithuanian Litas</option>
                                        <OPTION VALUE="LUF">Luxembourg Franc</option>
                                        <OPTION VALUE="MOP">Macau Pataca</option>
                                        <OPTION VALUE="MGF">Malagasy Franc</option>
                                        <OPTION VALUE="MWK">Malawi Kwacha</option>
                                        <OPTION VALUE="MYR">Malaysian Ringgit</option>
                                        <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                                        <OPTION VALUE="MTL">Maltese Lira</option>
                                        <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                                        <OPTION VALUE="MUR">Mauritius Rupee</option>
                                        <OPTION VALUE="MXN">Mexican Peso</option>
                                        <OPTION VALUE="MNT">Mongolian Tugrik</option>
                                        <OPTION VALUE="MAD">Moroccan Dirham</option>
                                        <OPTION VALUE="MZM">Mozambique Metical</option>
                                        <OPTION VALUE="MMK">Myanmar Kyat</option>
                                        <OPTION VALUE="ANG">NL Antillian Guilder</option>
                                        <OPTION VALUE="NAD">Namibia Dollar</option>
                                        <OPTION VALUE="NPR">Nepalese Rupee</option>
                                        <OPTION VALUE="NZD">New Zealand Dollar</option>
                                        <option value="NIO">Nicaraguan Cordoba Oro</option>
                                        <OPTION VALUE="NGN">Nigerian Naira</option>
                                        <OPTION VALUE="KPW">North Korean Won</option>
                                        <OPTION VALUE="NOK">Norwegian Kroner</option>
                                        <OPTION VALUE="OMR">Omani Rial</option>
                                        <OPTION VALUE="PKR">Pakistan Rupee</option>
                                        <OPTION VALUE="XPD">Palladium (oz.)</option>
                                        <OPTION VALUE="PAB">Panamanian Balboa</option>
                                        <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                                        <OPTION VALUE="PYG">Paraguay Guarani</option>
                                        <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                                        <OPTION VALUE="PHP">Philippine Peso</option>
                                        <OPTION VALUE="XPT">Platinum (oz.)</option>
                                        <OPTION VALUE="PLN">Polish Zloty</option>
                                        <OPTION VALUE="PTE">Portuguese Escudo</option>
                                        <OPTION VALUE="QAR">Qatari Rial</option>
                                        <OPTION VALUE="ROL">Romanian Lei</option>
                                        <OPTION VALUE="RUB">Russian Rouble</option>
                                        <OPTION VALUE="WST">Samoan Tala</option>
                                        <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                                        <OPTION VALUE="SAR">Saudi Riyal</option>
                                        <OPTION VALUE="SCR">Seychelles Rupee</option>
                                        <OPTION VALUE="SLL">Sierra Leone Leone</option>
                                        <OPTION VALUE="XAG">Silver (oz.)</option>
                                        <OPTION VALUE="SGD">Singapore Dollar</option>
                                        <OPTION VALUE="SKK">Slovak Koruna</option>
                                        <OPTION VALUE="SIT">Slovenian Tolar</option>
                                        <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                                        <OPTION VALUE="SOS">Somali Shilling</option>
                                        <OPTION VALUE="ZAR">South African Rand</option>
                                        <OPTION VALUE="KRW">South-Korean Won</option>
                                        <OPTION VALUE="ESP">Spanish Peseta</option>
                                        <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                                        <OPTION VALUE="SHP">St. Helena Pound</option>
                                        <OPTION VALUE="SDD">Sudanese Dinar</option>
                                        <OPTION VALUE="SDP">Sudanese Pound</option>
                                        <OPTION VALUE="SRG">Suriname Guilder</option>
                                        <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                                        <OPTION VALUE="SEK">Swedish Krona</option>
                                        <OPTION VALUE="CHF">Swiss Franc</option>
                                        <OPTION VALUE="SYP">Syrian Pound</option>
                                        <OPTION VALUE="TWD">Taiwan Dollar</option>
                                        <OPTION VALUE="TZS">Tanzanian Shilling</option>
                                        <OPTION VALUE="THB">Thai Baht</option>
                                        <OPTION VALUE="TOP">Tonga Pa'anga</option>
                                        <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                                        <OPTION VALUE="TND">Tunisian Dinar</option>
                                        <OPTION VALUE="TRL">Turkish Lira</option>
                                        <OPTION VALUE="AED">UAE Dirham</option>
                                        <OPTION VALUE="UGS">Uganda Shilling</option>
                                        <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                                        <option value="GBP">United Kingdom Pounds</option>
                                        <OPTION VALUE="UYP">Uruguayan Peso</option>
                                        <OPTION VALUE="VUV">Vanuatu Vatu</option>
                                        <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                                        <OPTION VALUE="VND">Vietnamese Dong</option>
                                        <OPTION VALUE="YUN">Yugoslav Dinar</option>
                                        <OPTION VALUE="ZMK">Zambian Kwacha</option>
                                        <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
                                      </SELECT><script language="JavaScript">
						         			SelectOption(document.forms[0].agentCurrency, "<? echo $agentDetails["agentCurrency"]; ?>");
						          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Commission Currency</strong></font></td>
            <td><SELECT NAME="agentCommCurrency" id="agentCommCurrency" style="font-family:verdana; font-size: 11px">
                                        <OPTION VALUE="" SELECTED>-Select One-</option>
										<OPTION VALUE="USD">US Dollar</option>
                                        <OPTION VALUE="AFA">Afghanistan Afghani</option>
                                        <OPTION VALUE="ALL">Albanian Lek</option>
                                        <OPTION VALUE="DZD">Algerian Dinar</option>
                                        <OPTION VALUE="ADF">Andorran Franc</option>
                                        <OPTION VALUE="ADP">Andorran Peseta</option>
                                        <OPTION VALUE="AON">Angolan New Kwanza</option>
                                        <OPTION VALUE="ARS">Argentine Peso</option>
                                        <OPTION VALUE="AWG">Aruban Florin</option>
                                        <OPTION VALUE="AUD">Australian Dollar</option>
                                        <OPTION VALUE="ATS">Austrian Schilling</option>
                                        <OPTION VALUE="BSD">Bahamian Dollar </option>
                                        <OPTION VALUE="BHD">Bahraini Dinar</option>
                                        <OPTION VALUE="BDT">Bangladeshi Taka</option>
                                        <OPTION VALUE="BBD">Barbados Dollar</option>
                                        <OPTION VALUE="BEF">Belgian Franc</option>
                                        <OPTION VALUE="BZD">Belize Dollar</option>
                                        <OPTION VALUE="BMD">Bermudian Dollar</option>
                                        <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                                        <OPTION VALUE="BOB">Bolivian Boliviano</option>
                                        <OPTION VALUE="BWP">Botswana Pula</option>
                                        <OPTION VALUE="BRL">Brazilian Real</option>
                                        <OPTION VALUE="GBP">British Pound</option>
                                        <OPTION VALUE="BND">Brunei Dollar</option>
                                        <OPTION VALUE="BGL">Bulgarian Lev</option>
                                        <OPTION VALUE="BIF">Burundi Franc</option>
                                        <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                                        <OPTION VALUE="XAF">CFA Franc BEAC</option>
                                        <OPTION VALUE="XPF">CFP Franc</option>
                                        <OPTION VALUE="KHR">Cambodian Riel</option>
                                        <OPTION VALUE="CAD">Canadian Dollar</option>
                                        <OPTION VALUE="CVE">Cape Verde Escudo</option>
                                        <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                                        <OPTION VALUE="CLP">Chilean Peso</option>
                                        <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                                        <OPTION VALUE="COP">Colombian Peso</option>
                                        <OPTION VALUE="KMF">Comoros Franc</option>
                                        <OPTION VALUE="CRC">Costa Rican Colon</option>
                                        <OPTION VALUE="HRK">Croatian Kuna</option>
                                        <OPTION VALUE="CUP">Cuban Peso</option>
                                        <OPTION VALUE="CYP">Cyprus Pound</option>
                                        <OPTION VALUE="CZK">Czech Koruna</option>
                                        <OPTION VALUE="DKK">Danish Krone</option>
                                        <OPTION VALUE="DJF">Djibouti Franc</option>
                                        <OPTION VALUE="DOP">Dominican R. Peso</option>
                                        <OPTION VALUE="NLG">Dutch Guilder</option>
                                        <OPTION VALUE="XEU">ECU</option>
                                        <OPTION VALUE="XCD">East Caribbean Dollar</option>
                                        <OPTION VALUE="ECS">Ecuador Sucre</option>
                                        <OPTION VALUE="EGP">Egyptian Pound</option>
                                        <OPTION VALUE="SVC">El Salvador Colon</option>
                                        <OPTION VALUE="EEK">Estonian Kroon</option>
                                        <OPTION VALUE="ETB">Ethiopian Birr</option>
                                        <OPTION VALUE="EUR">Euro</option>
                                        <OPTION VALUE="FKP">Falkland Islands Pound</option>
                                        <OPTION VALUE="FJD">Fiji Dollar</option>
                                        <OPTION VALUE="FIM">Finnish Markka</option>
                                        <OPTION VALUE="FRF">French Franc</option>
                                        <OPTION VALUE="GMD">Gambian Dalasi</option>
                                        <OPTION VALUE="DEM">German Mark</option>
                                        <OPTION VALUE="GHC">Ghanaian Cedi</option>
                                        <OPTION VALUE="GIP">Gibraltar Pound</option>
                                        <OPTION VALUE="XAU">Gold (oz.)</option>
                                        <OPTION VALUE="GRD">Greek Drachma</option>
                                        <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                                        <OPTION VALUE="GNF">Guinea Franc</option>
                                        <OPTION VALUE="GYD">Guyanese Dollar</option>
                                        <OPTION VALUE="HTG">Haitian Gourde</option>
                                        <OPTION VALUE="HNL">Honduran Lempira</option>
                                        <OPTION VALUE="HKD">Hong Kong Dollar</option>
                                        <OPTION VALUE="HUF">Hungarian Forint</option>
                                        <OPTION VALUE="ISK">Iceland Krona</option>
                                        <OPTION VALUE="INR">Indian Rupee</option>
                                        <OPTION VALUE="IDR">Indonesian Rupiah</option>
                                        <OPTION VALUE="IRR">Iranian Rial</option>
                                        <OPTION VALUE="IQD">Iraqi Dinar</option>
                                        <OPTION VALUE="IEP">Irish Punt</option>
                                        <OPTION VALUE="ILS">Israeli New Shekel</option>
                                        <OPTION VALUE="ITL">Italian Lira</option>
                                        <OPTION VALUE="JMD">Jamaican Dollar</option>
                                        <OPTION VALUE="JPY">Japanese Yen</option>
                                        <OPTION VALUE="JOD">Jordanian Dinar</option>
                                        <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                                        <OPTION VALUE="KES">Kenyan Shilling</option>
                                        <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                                        <OPTION VALUE="LAK">Lao Kip</option>
                                        <OPTION VALUE="LVL">Latvian Lats</option>
                                        <OPTION VALUE="LBP">Lebanese Pound</option>
                                        <OPTION VALUE="LSL">Lesotho Loti</option>
                                        <OPTION VALUE="LRD">Liberian Dollar</option>
                                        <OPTION VALUE="LYD">Libyan Dinar</option>
                                        <OPTION VALUE="LTL">Lithuanian Litas</option>
                                        <OPTION VALUE="LUF">Luxembourg Franc</option>
                                        <OPTION VALUE="MOP">Macau Pataca</option>
                                        <OPTION VALUE="MGF">Malagasy Franc</option>
                                        <OPTION VALUE="MWK">Malawi Kwacha</option>
                                        <OPTION VALUE="MYR">Malaysian Ringgit</option>
                                        <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                                        <OPTION VALUE="MTL">Maltese Lira</option>
                                        <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                                        <OPTION VALUE="MUR">Mauritius Rupee</option>
                                        <OPTION VALUE="MXN">Mexican Peso</option>
                                        <OPTION VALUE="MNT">Mongolian Tugrik</option>
                                        <OPTION VALUE="MAD">Moroccan Dirham</option>
                                        <OPTION VALUE="MZM">Mozambique Metical</option>
                                        <OPTION VALUE="MMK">Myanmar Kyat</option>
                                        <OPTION VALUE="ANG">NL Antillian Guilder</option>
                                        <OPTION VALUE="NAD">Namibia Dollar</option>
                                        <OPTION VALUE="NPR">Nepalese Rupee</option>
                                        <OPTION VALUE="NZD">New Zealand Dollar</option>
                                        <option value="NIO">Nicaraguan Cordoba Oro</option>
                                        <OPTION VALUE="NGN">Nigerian Naira</option>
                                        <OPTION VALUE="KPW">North Korean Won</option>
                                        <OPTION VALUE="NOK">Norwegian Kroner</option>
                                        <OPTION VALUE="OMR">Omani Rial</option>
                                        <OPTION VALUE="PKR">Pakistan Rupee</option>
                                        <OPTION VALUE="XPD">Palladium (oz.)</option>
                                        <OPTION VALUE="PAB">Panamanian Balboa</option>
                                        <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                                        <OPTION VALUE="PYG">Paraguay Guarani</option>
                                        <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                                        <OPTION VALUE="PHP">Philippine Peso</option>
                                        <OPTION VALUE="XPT">Platinum (oz.)</option>
                                        <OPTION VALUE="PLN">Polish Zloty</option>
                                        <OPTION VALUE="PTE">Portuguese Escudo</option>
                                        <OPTION VALUE="QAR">Qatari Rial</option>
                                        <OPTION VALUE="ROL">Romanian Lei</option>
                                        <OPTION VALUE="RUB">Russian Rouble</option>
                                        <OPTION VALUE="WST">Samoan Tala</option>
                                        <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                                        <OPTION VALUE="SAR">Saudi Riyal</option>
                                        <OPTION VALUE="SCR">Seychelles Rupee</option>
                                        <OPTION VALUE="SLL">Sierra Leone Leone</option>
                                        <OPTION VALUE="XAG">Silver (oz.)</option>
                                        <OPTION VALUE="SGD">Singapore Dollar</option>
                                        <OPTION VALUE="SKK">Slovak Koruna</option>
                                        <OPTION VALUE="SIT">Slovenian Tolar</option>
                                        <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                                        <OPTION VALUE="SOS">Somali Shilling</option>
                                        <OPTION VALUE="ZAR">South African Rand</option>
                                        <OPTION VALUE="KRW">South-Korean Won</option>
                                        <OPTION VALUE="ESP">Spanish Peseta</option>
                                        <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                                        <OPTION VALUE="SHP">St. Helena Pound</option>
                                        <OPTION VALUE="SDD">Sudanese Dinar</option>
                                        <OPTION VALUE="SDP">Sudanese Pound</option>
                                        <OPTION VALUE="SRG">Suriname Guilder</option>
                                        <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                                        <OPTION VALUE="SEK">Swedish Krona</option>
                                        <OPTION VALUE="CHF">Swiss Franc</option>
                                        <OPTION VALUE="SYP">Syrian Pound</option>
                                        <OPTION VALUE="TWD">Taiwan Dollar</option>
                                        <OPTION VALUE="TZS">Tanzanian Shilling</option>
                                        <OPTION VALUE="THB">Thai Baht</option>
                                        <OPTION VALUE="TOP">Tonga Pa'anga</option>
                                        <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                                        <OPTION VALUE="TND">Tunisian Dinar</option>
                                        <OPTION VALUE="TRL">Turkish Lira</option>
                                        <OPTION VALUE="AED">UAE Dirham</option>
                                        <OPTION VALUE="UGS">Uganda Shilling</option>
                                        <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                                        <option value="GBP">United Kingdom Pounds</option>
                                        <OPTION VALUE="UYP">Uruguayan Peso</option>
                                        <OPTION VALUE="VUV">Vanuatu Vatu</option>
                                        <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                                        <OPTION VALUE="VND">Vietnamese Dong</option>
                                        <OPTION VALUE="YUN">Yugoslav Dinar</option>
                                        <OPTION VALUE="ZMK">Zambian Kwacha</option>
                                        <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
                                      </SELECT><script language="JavaScript">
						         			SelectOption(document.forms[0].agentCurrency, "<? echo $agentDetails["agentCurrency"]; ?>");
						          	  </script></td>
        </tr>
        <? if (CONFIG_AGENT_LIMIT == 1){ ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Limit</strong></font><font color="red">*</font></td>
            <td><input type="text" name="agentAccountLimit" value="<?=stripslashes($agentDetails["agentAccountLimit"]); ?>" size="35" maxlength="15"></td>
        </tr>
      <? } 
       if(CONFIG_USER_COMMISSION_MANAGEMENT == '1' && $_GET["ida"] != "Y"){
        }else{
      ?>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Commission package</strong></font></td>
          <td><select name="commPackage" id="commPackage">
            <option value="001">Fixed Amount</option>
            <option value="002">Percentage of Transaction Amount</option>
            <option value="003">Percentage of Fee</option>
			<option value="004">None</option>
          </select><script language="JavaScript">
						         			SelectOption(document.forms[0].commPackage, "<? echo $agentDetails["commPackage"]; ?>");
						          	  </script>
            </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Commission Percentage/Fee</strong></font></td>
            <td><input type="text" name="agentCommission" value="<?=stripslashes($agentDetails["agentCommission"]); ?>" size="35" maxlength="6"></td>
        </tr>
      <? }?>
	  
	    <!-- Added by Niaz Ahmad at 07-05-2009 @4926 -->
	   <? if(CONFIG_DISTRIBUTOR_BANK_CHARGES == "1" && $_REQUEST["ida"] == "Y") { ?>
	   <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Admin Charges</strong></font></td>
            <td>
	    <input type="text" name="distributorBankcharges" value="<?=stripslashes($agentDetails["bankCharges"]); ?>" size="35" maxlength="6">
		</td>
		</tr>
	    <tr bgcolor="#ededed"> 
	   <? } ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Status</strong></font></td>
            <td><input type="radio" name="agentStatus" value="New" <? if ($agentDetails["agentStatus"] == "" || $agentDetails["agentStatus"] == "New") echo "checked"; ?>>New <input type="radio" name="agentStatus" value="Active" <? if ($agentDetails["agentStatus"] == "Active") echo "checked"; ?>>Active <input type="radio" name="agentStatus" value="Disabled" <? if ($agentDetails["agentStatus"] == "Disabled") echo "checked"; ?>>Disabled <input type="radio" name="agentStatus" value="Suspended" <? if ($agentDetails["agentStatus"] == "Suspended") echo "checked"; ?>>Suspended</td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Correspondent</strong></font></td>
          <td><select name="correspondent" id="correspondent">
            <? if($_GET["ida"] != "Y") {?> <option value="N">Agent</option><!--Can't distribute transactions--><? } ?>
           <? if($_GET["ida"] == "Y"){ ?> <option value="ONLY">Distributor</option><!--Only distribute transactions--><? } ?>
          </select><script language="JavaScript">SelectOption(document.forms[0].correspondent, "<? echo $agentDetails["isCorrespondent"]; ?>");</script></td>       </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Select country to <? if ($_GET["ida"] == "Y") { echo "Assign distribution"; }else{ echo "Send money"; } ?><font color="#ff0000">*</font></strong></font></td>
          <td>Hold Ctrl key for multiple selection <br>
            <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
              <?
              if(CONFIG_ENABLE_ORIGIN == "1"){
              	$countryTypes = " and countryType like '%destination%' ";
              	}else{
              		$countryTypes = " ";
              	}
              if(CONFIG_COUNTRY_SERVICES_ENABLED){
					$serviceTable = TBL_SERVICE;
					if($countryBasedFlag){
						$serviceTable = TBL_SERVICE_NEW;
					}
              		$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
							}else
							{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
							}
              
              
					//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
              <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo $agentDetails["IDAcountry"]==$countires[$i]["countryName"] ? "selected"  : ""?>>
              <?=$countires[$i]["countryName"]; ?>
              </OPTION>
              <?
					}
				?>
              </SELECT>
          </td></tr>
<!--		//////////////////////////////////////////////-->
			<tr bgcolor="#ededed">
				<?php
				if($_GET["ida"]==="" && $_GET["type"]==="") {
					?>
					<td>
						<font color="#005b90"><strong>Select associated distributor <font color="#ff0000">*</font></strong></font>
					</td>
					<td>
						<SELECT name="associatedDistributor[]" size="15" id="associatedDistributor" style="font-family:verdana; font-size: 11px" onchange="selectedAssociatedDistributor();">
							<?
							$query="";
							if ($agentType2 == "SUPA" || $agentType2 == "SUBA" ) {
								$query="select userID, name, agentCompany from admin where userID='$parentID' or  parentID = '$parentID'";
							}  else {
								$query="SELECT userID, name , agentCompany FROM admin WHERE adminType = 'Agent' AND (agentType = 'Supper' or agentType = 'Sub') AND agentStatus='Active'";
							}
							$result=selectMultiRecords($query);
							$query="select linked_Distributor from admin where userID='".$_GET["userID"]."' or  parentID = '".$_GET["userID"]."'";
							$selected=selectMultiRecords($query);
							$row=$selected[0];
							$array=explode(", ", $row["linked_Distributor"]);
							$userID="";
							for($i=0;$i<count($array);$i++){
								$row=$array[$i];
								if ($i==count($array)-1)
									$userID.="'$row'";
								else
									$userID.="'$row', ";
							}
							if ($agentType2 == "SUPA" || $agentType2 == "SUBA" ) {
								$query="select userID, name, agentCompany from admin where userID IN ($userID) or  parentID IN ($userID)";
							}  else {
								$query="SELECT userID, name , agentCompany FROM admin WHERE adminType = 'Agent' AND (agentType = 'Supper' or agentType = 'Sub') AND isCorrespondent = 'ONLY' and agentStatus='Active' and userID IN ($userID)";
							}
							$selected=selectMultiRecords($query);
							foreach($result as $row){
								$flag=false;
								foreach ($selected as $row1) {
									if ($row['userID'] === $row1["userID"]) {
										echo "<option>".$row['userID']." ".$row1["userID"]."</option>";
										$flag = true;
									}
								}
								if ($flag)
									echo "<option value='" . $row['name'] . "(" . $row['agentCompany'] . ")' selected>" . $row['name'] . "(" . $row['agentCompany'] . ")</option>";
								else
									echo "<option value='" . $row['name'] . "(" . $row['agentCompany'] . ")'>" . $row['name'] . "(" . $row['agentCompany'] . ")</option>";
							}

//							$result=selectMultiRecords($query);
//							foreach($result as $row){
//								echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
//							}
							?>
						</SELECT>

					</td>
					<?php
				} else if($_GET["ida"] === "Y") {
					?>
					<td>
						<font color="#005b90"><strong>Select associated agent <font color="#ff0000">*</font></strong></font>
					</td>
					<td>
						<SELECT name="associatedAgent[]" size="4" multiple id="associatedAgent" style="font-family:verdana; font-size: 11px">
							<?
							$query="";
							if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
//								$query="select linked_Agent from admin where userID='$parentID' or  parentID = '$parentID'";
//								$result=selectMultiRecords($query);
								$query=selectFrom("select linked_Agent from admin where userID=$parentID");
								$arrayOfUserID = explode(", ", $query['linked_Agent']);
								$userIDs = "";
								for ($i=0;$i<count($arrayOfUserID);$i++) {
									$userID = $arrayOfUserID[$i];
									if ($i === count($arrayOfUserID)-1) {
										$userIDs .= "'".$userID."'";
									} else {
										$userIDs .= "'".$userID."', ";
									}
								}
								$query="select name, agentCompany from admin where userID in ($userIDs)";
							}  else {
								$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'N' and agentStatus='Active'";
							}
							$result=selectMultiRecords($query);
							$query="select linked_Agent from admin where userID='".$_GET["userID"]."'";
							$selected=selectMultiRecords($query);
							$row=$selected[0];
							$array=explode(", ", $row["linked_Agent"]);
							$userID="";
							for($i=0;$i<count($array);$i++){
								$row=$array[$i];
								if ($i==count($array)-1)
									$userID.="'$row'";
								else
									$userID.="'$row', ";
							}
							if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ) {
								$query="select name, agentCompany from admin where userID IN ($userID)";
							}  else {
								$query="SELECT name , agentCompany FROM `admin` WHERE `adminType` = 'Agent' AND (`agentType` = 'Supper' or `agentType` = 'Sub') AND `isCorrespondent` = 'N' and agentStatus='Active' and userID IN ($userID)";
							}
							$selected=selectMultiRecords($query);
							foreach($result as $row){
								$flag=false;
								foreach ($selected as $row1) {
									$abc=$row1["name"]."(".$row1["agentCompany"].")";
									$xyz=$row['name']."(".$row['agentCompany'].")";
									echo $abc." ".$xyz;
									if ($abc == $xyz)
										$flag=true;
								}
								if ($flag)
									echo "<option value='".$row['name']."(".$row['agentCompany'].")' selected>".$row['name']."(".$row['agentCompany'].")</option>";
								else
									echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
							}
							
//							$result=selectMultiRecords($query);
//							foreach($result as $row){
//								echo "<option value='".$row['name']."(".$row['agentCompany'].")'>".$row['name']."(".$row['agentCompany'].")</option>";
//							}
							?>
						</SELECT>

					</td>
					<?php
				} else {
					?>
					<td>

					</td>
					<?php
				}
				?>
			</tr>
	<script>
		function selectedAssociatedDistributor(){
			var associatedDistributor = document.getElementById("associatedDistributor").value;
			$.ajax({
				url: "add-agent-conf-ajax.php",
				data: {associatedDistributor: associatedDistributor},
				type: "GET",
				success: function (data){
					document.getElementById("selectedDistributor").innerHTML += data;
				}
			});
		}
		function selectDistributor(){
			var selectedDistributor = document.getElementById("selectedDistributor").value;
			$.ajax({
				url: "add-agent-conf-ajax.php",
				data: {selectedDistributor: selectedDistributor},
				type: "GET",
				success: function (data){
					document.getElementById("selectedDistributor").innerHTML = data;
				}
			});
		}
	</script>

<!--	////////////////-->
	<tr bgcolor="#ededed">
		<td><font color="#005b90"><strong>selected associated distributor</strong></font></td>
		<td>
			<select name="selectedDistributor" id="selectedDistributor"  size="15" style="font-family:verdana; font-size: 11px " onchange="selectDistributor(); ">
				<?php
					foreach($result as $row){
						$flag = false;
						foreach ($selected as $row1) {
							if ($row['userID'] == $row1["userID"])
								$flag = true;
						}
						if ($flag)
							echo "<option value='" . $row['name'] . "(" . $row['agentCompany'] . ")' selected>" . $row['name'] . "(" . $row['agentCompany'] . ")</option>";
					}
				?>
			</select>
		</td>
	</tr>
<!--//////////////////////////////////////////////////////	-->
	<tr bgcolor="#ededed">
		<td>
			<font color="#005b90"><strong>Select services  </strong></font>
		</td>
		<td>
			<SELECT name="services[]" size="4" multiple id="services" style="font-family:verdana; font-size: 11px">
				<?php
				$query="SELECT services FROM `admin` WHERE userID= ".$_GET["userID"];
				$row = selectFrom($query);
				$array=explode(", ", $row["services"]);

				if (count($array)>0) {
					$arrservicesabbrevation = array('W','O','BO','G');
					$arrservices = array('Website','Online','Back Office','Gateway');
					for($j=0;$j<4;$j++) {
						$flag = false;
						$service=$arrservicesabbrevation[$j];
						for ($i = 0; $i < count($array); $i++)
						{
							if ($array[$i] == $service)
							{
								$flag = true;
								break;
							}
						}
						if ($flag){
							echo "<option value='$arrservicesabbrevation[$j]' selected='selected'>$arrservices[$j]</option>";
						}
						else{
							echo "<option value='$arrservicesabbrevation[$j]'>$arrservices[$j]</option>";
						}
					}

				}
				else {


					?>
					<option value="W">Website</option>
					<option value="O">Online</option>
					<option value="BO">Back Office</option>
					<option value="G">Gateway</option>
					<?php
				}
				?>


			</SELECT>

		</td>

	</tr>
<!--	//////////-->
		<?php if(CONFIG_HIDE_AGENT_COMPANY_LOGO!="1"){?>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Company Logo </strong></font></td>
          <td><input type="file" name="logo"></td>
        </tr>
		<?php }?>
		<?php if(CONFIG_HIDE_AGENT_AUTHO_SERVICE!="1"){?>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Authorized for the services</strong></font></td>
          <td>
		    Hold Ctrl key for multiple selection<br>
		    <SELECT name="authorizedFor[]" size="4" multiple style="font-family:verdana; font-size: 11px" >
            <?
			if(trim($agentDetails["authorizedFor"]) == "")
			{?>
				<option value="Pick up" selected>Pick up</option>
			<?
			}
			else
			{
			?>
			<option value="Pick up" <? echo (strstr($agentDetails["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick up</option>
            <?
			}
			?>
			<option value="Bank Transfer" <? echo (strstr($agentDetails["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank Transfer</option>
			<option value="Home Delivery" <? echo (strstr($agentDetails["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home Delivery</option>
			  <? if(CONFIG_ATM_CARD_OPTION == "1"){ ?>
       					
       					<option value="ATM Card" <? echo (strstr($agentDetails["authorizedFor"],"ATM Card") ? "selected"  : "")?>>ATM Card</option>
       <? } ?>     
          </SELECT>
		  </td>
        </tr>
		<?php }?>
        <!-- Added By Niaz Ahmad #2810 Connect Plus at 16-01-2008 -->
        <? if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1") { ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Settlement Currency<font color="#ff0000">*</font></strong></font></td>
            <td> 
                 <select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyName), description,country  FROM  currencies ORDER BY currencyName ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								$country = $rstRow["country"];
								$currency = $rstRow["currencyName"];	
								$description = $rstRow["description"];
								$erID  = $rstRow["cID"];							
								
								echo "<option value ='$currency'> ".$currency." --> ".$country." --> ".$description."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.forms[0].currencyOrigin, "<?=$agentDetails["settlementCurrency"]; ?>");
     	  </script>	
         </td>  
         </tr> 
           <? } ?>

		<?
			/*** Code added by Usman Ghani against #3428: Connect Plus - Select Distributors for Agents ****/
			if ( defined("CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX")
				&& CONFIG_SHOW_DISTRIBUTOR_SELECTION_LISTBOX == 1 
				&& $_REQUEST["ida"] != "Y")
			{
				?>
				 <tr bgcolor="#ededed">
					<td><font color="#005b90"><strong>Select Distributors</strong></font></td>
					<td>
					<?
						//debug( selectMultiRecords("select * from admin where isCorrespondent='ONLY'") );
						$csvAssocDistributors = $agentDetails["associated_distributors"];
						$assocDistIDs = array();
						if ( !empty( $csvAssocDistributors ) )
						{
							$assocDistIDs = explode( ",", $csvAssocDistributors );
							//debug( $assocDistArray );
						}
						$allDistributors = selectMultiRecords("select userID, name, username from admin where isCorrespondent='ONLY' and parentID > '0' and adminType='Agent' and agentStatus='Active'");
					?>
						<input type="radio" onClick="updateAssocDistControls();" name="associateAllDistributors" value="1" <?=( empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> All &nbsp;&nbsp
						<input type="radio" onClick="updateAssocDistControls();" id="letMeSelectAssocDist" name="associateAllDistributors" value="0" <?=( !empty($assocDistIDs) ? "checked='checked'" : "" ); ?> /> Let Me Select<br />
						Hold Control key for multiple selection<br />
						<select id="distList" name="assocDistributors[]" multiple="multiple" size="6">
						<?
							foreach( $allDistributors as $distData )
								echo "<option " . ( in_array($distData["userID"], $assocDistIDs) ? "selected='selected'" : "" ) . " value='" . $distData ["userID"] . "'>" . $distData ["name"] . " [" . $distData ["username"] . "]</option>";
						?>
						</select>
					</td>
				 </tr>
				<?
			}
			/*** End of code #3428: Connect Plus - Select Distributors for Agents ****/
		?>
		
         <? if(CONFIG_AGENT_PAYMENT_MODE == "1"){ ?>
          
            <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            <td><select name="agentPaymentMode" id="agentPaymentMode">
            	<? if(CONFIG_PAYMENT_MODE_OPTION == "1"){ ?>
            					
            					<option value="exclude" <? echo ($agentDetails["paymentMode"] == "exclude" ? "selected"  : "")?>>At Source</option>
            	
            	<? }else{ ?>
                <option value="include" <? echo ($agentDetails["paymentMode"] == "include" ? "selected"  : "")?>>At End Of Month</option>
                <option value="exclude" <? echo ($agentDetails["paymentMode"] == "exclude" ? "selected"  : "")?>>At Source</option>
               <? } ?>
              </select>
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
                                </script>
            </td>
          </tr>
          
           <? }else{ ?>
          
            <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Payment Mode</strong></font></td>
            <td><select name="agentPaymentMode" id="agentPaymentMode">
           		
            <option value="include" <? echo ($agentDetails["paymentMode"] == "include" ? "selected"  : "")?>>At End Of Month</option>
            	
            <? } ?>
              </select>
            
              <script language="JavaScript">
         	SelectOption(document.forms[0].agentPaymentMode, "<?=$_SESSION["agentPaymentMode"]; ?>");
             </script>
            </td>
          </tr>
          
        <?php if(CONFIG_HIDE_AGENT_ACCESS_IP!="1"){?>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Access From IP</strong></font></td>
          <td>For multiple please separate by comma.<br>
              <input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255" value="<?=stripslashes($agentDetails["accessFromIP"]); ?>"></td>
        </tr>
		<?php }?>
		 <!-- Added by Niaz Ahmad at 30-04-2009 @4816   -->
		  <? if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT	== "1") { ?> 
		  <tr bgcolor="#ededed"> 
           <td width="37%"align="right"><font color="#005b90"><b>Upload Documents&nbsp;</b></font></td>
           	<td width="37%"><input type="checkbox" name="uploadImage" value="Y" <?=($_SESSION["uploadImage"]=="Y")?'checked="checked"':''?>>
			<input type="hidden" name="mode" value="update">
			</td>
           				</tr>
         <? 
		    }
		 ?>
		 
          <? if (CONFIG_BACK_DATED == "1" && $_GET["ida"] != "Y"){ ?>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Right for Backdated</strong></font></td>
            <td> 
              <select name="rights" id="rights" <? if ($agentType2 == 'admin') { ?>onChange="showBackDays();"<? } ?>>
              	<option value="">No</option>
              	<option value="Backdated" <? if (substr_count($agentDetails["rights"], "Backdated")) echo "selected"; ?>>Yes</option>
              </select>
            </td>
          </tr>
          
           <?	if(CONFIG_PAYIN_CUSTOMER == '1') {?>
          <tr bgcolor="#ededed"> 
	                  <td width="17%"><font color="#005b90"><b>Payin Book&nbsp;</b></font></td>
	                  <td width="26%"><input type="checkbox" name="ch_payinBook" <? if($agentDetails["payinBook"]!="") echo "checked"; ?> ></td>
	                </tr>
	               <tr bgcolor="#ededed">
	                  <td width="20%"><font color="#005b90"><b>Payin Book No.&nbsp;</b></font></td>
	                  <td width="37%"><input type="textbox" name="payinBook" value="<?=$agentDetails["payinBook"];?>" maxlength="<?=CONFIG_PAYIN_NUMBER ?>"></td>
	                </tr>
          <? }?>
          
				<tr bgcolor="#ededed" id="howmanybackdays" <? if (substr_count($agentDetails["rights"], "Backdated")) { } else { ?>style="visibility:hidden;position:absolute;"<? } ?>>
					<td><font color="#005b90"><strong>How Many Back Days</strong></font></td>
					<td> <input type="text" name="backDays" id="backDays" value="<? echo ($agentDetails["backDays"] == '0' ? "" : $agentDetails["backDays"]) ?>" size="3"> <i>By default,</i> 1</td>
				</tr>
        <? } ?>
        <? if (CONFIG_ONLINE_AGENT == "1" && $_GET["ida"] != "Y")
         { 
         	?>
         	 	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Assigned to Online Senders</strong></font>
					</td>
					<td> 
						<select name="isOnline" id="isOnline">
              			<option value="N" <? echo($agentDetails["isOnline"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($agentDetails["isOnline"]=="Y" ? "selected"  : "")?>>Yes</option>	
					</td>
				</tr>
         
         	<?
         }
         ?>
         
          <?
         if(CONFIG_DEFAULT_DISTRIBUTOR == '1' && $_GET["ida"] == "Y")
         {
         	?>
	        <tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Default Distributor</strong></font>
						</td>
						<td> 
							<select name="defaultDistrib" id="defaultDistrib" onChange="showHideReplaceBlock();">
              			<option value="N" <? echo($agentDetails["defaultDistrib"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($agentDetails["defaultDistrib"]=="Y" ? "selected"  : "")?>>Yes</option>
              </select>	
						</td>
					</tr>
					<tr bgcolor="#ededed" id="showReplaceBlock" style="display:<?=($agentDetails["defaultDistrib"] == "Y"?"table-row":"none")?>;">
						<td>
							<font color="#005b90"><strong>Replace Existing Default Distributor</strong></font>
						</td>
						<td> 
							<input type="radio" name="replaceDefaultDistributor" id="replaceDefaultDistributor" value="Y" <?=($_SESSION["replaceDefaultDistributor"]=="Y"?"checked":"")?>>&nbsp;Yes
							<input type="radio" name="replaceDefaultDistributor" id="replaceDefaultDistributor" value="N" <?=($_SESSION["replaceDefaultDistributor"]=="N"?"checked":"")?>>&nbsp;No
						</td>
					</tr>
        <?
         	}
         	?> 
         	<?
         if(CONFIG_MANAGE_CLAVE == '1' && $_GET["ida"] == "Y")
         {
         	?>

         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Use Clave Range</strong></font>
					</td>
					<td> 
						<select name="hasClave" id="hasClave">
              			<option value="N" <? echo($agentDetails["hasClave"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($agentDetails["hasClave"]=="Y" ? "selected"  : "")?>>Yes</option>	
					</td>
				</tr>
        <?
         	}
         	?> 
         	<?
       
         if(CONFIG_UNLIMITED_EX_RATE_OPTION == '1'&& $_GET["ida"] != "Y")
         {
         	?>
         	<tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Manual Exchange Rate</strong></font>
					</td>
					<td> 
						<select name="unlimitedRate" id="unlimitedRate">
              			<option value="N" <? echo($agentDetails["unlimitedRate"]=="Y" ? "" : "selected")?>>No</option>
              			<option value="Y" <? echo($agentDetails["unlimitedRate"]=="Y" ? "selected"  : "")?>>Yes</option>	
					</td>
				</tr>
				
        <tr bgcolor="#ededed">
					<td>
						<font color="#005b90"><strong>Countries for Manual Exchange Rate</strong></font>
					</td>
					<td> 
						<select name="rateCountries[]" id="rateCountries[]" size="4" multiple>
							  <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
								$serviceTable = TBL_SERVICE;
								if($countryBasedFlag){
									$serviceTable = TBL_SERVICE_NEW;
								}
	                	
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".$serviceTable." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
				                //$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
									for ($i=0; $i < count($countires); $i++){
								?>
				                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($agentDetails["rateCountries"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
				                <?=$countires[$i]["countryName"]; ?>
				                </OPTION>
				                <?
									}
								?>
              </select>
     			</td>
				</tr>
        <?
         	}
         	?>
         	
         	
         	 <? 
         	 if(CONFIG_SHARE_OTHER_NETWORK == '1')
		       { 
		       	$jointClients = selectMultiRecords("select * from ".TBL_JOINTCLIENT." where 1 and isEnabled = 'Y' ");
		         	//echo("select availableAt, remoteServerId from ".TBL_SHAREDUSERS." where localServerId = '".$_GET["userID"]."'");
							//	echo($userRemoteId["availableAt"] ." ". $jointClients[$cl]["clientId"]);
		       	?>
		       	 	<tr bgcolor="#ededed">
						<td>
							<font color="#005b90"><strong>Available to</strong></font>
						</td>
						<td> 
							<select name="remoteAvailable" id="remoteAvailable">
								<option value=""> Select One </option>
								<?
									
								$userRemoteId = Selectfrom("select availableAt, remoteServerId from ".TBL_SHAREDUSERS." where localServerId = '".$_GET["userID"]."'");
								
		
								for($cl = 0; $cl < count($jointClients); $cl++)
								{	
									$foundOnRemote = false;
									if($jointClients[$cl]["clientId"] == $userRemoteId["availableAt"])
									{
											$otherClient = new connectOtherDataBase();
											$otherClient-> makeConnection($jointClients[$cl]["serverAddress"],$jointClients[$cl]["userName"],$jointClients[$cl]["password"],$jointClients[$cl]["dataBaseName"]);
											$selectQuery = "select userID, agentStatus from ".$jointClients["dataBaseName"].".admin where userID = '".$userRemoteId["remoteServerId"]."'";
											$selectedUser = $otherClient-> selectOneRecord($selectQuery);
											if($selectedUser["agentStatus"] == "Active")
											{
												$foundOnRemote = true;
											}
											$otherClient-> closeConnection();
											dbConnect();
									}
									
								?>
								
									 <OPTION value="<?=$jointClients[$cl]["clientId"]; ?>" <? echo ($foundOnRemote ? "selected"  : "")?>> <?=$jointClients[$cl]["clientName"]?></option>
								<?
								}
								?>
		           </select> 			
		          
						</td>
					</tr>
		       
		       	<?
		       } 
		       ?>
		       
		       	<? if(CONFIG_NOTES_AGENT_DIST == "1"){ ?>
         	
						<tr bgcolor="#ededed">
	                  <td width="20%"><font color="#005b90"><b>Notes&nbsp;</b></font></td>
	                  <td ><input type="textarea" name="notes" value="<?=$agentDetails["notes"];?>" ></td>
	                
					</tr>
         	<? } ?>
      	<?php } ?>	
		
		<?php 
		if(defined("CONFIG_NEW_INTRODUCER_ASSOCIATION") && CONFIG_NEW_INTRODUCER_ASSOCIATION==1)
		{ ?>
		
			 <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>By Default [Introducer Selection]</strong></font></td>
			<?php 
			if($agentDetails["IsDefault"]=='Y')
				$checked="checked";
			?>
			<td><input id="IsByDefault" name="IsByDefault" type="checkbox" value="true" class="ByDefaultSelected" <?=$checked?>/></td>
		
		
<?php   }
		?>		       
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="hidden" name="usern" value="<?=stripslashes($agentDetails["username"]); ?>">
				<input type="hidden" name="agentParentID" value="<?=stripslashes($agentDetails["parentID"]); ?>">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>