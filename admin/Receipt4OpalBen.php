<?
session_start();
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = getCountryTime(CONFIG_COUNTRY_CODE);

$result = selectFrom("SELECT * FROM ". TBL_TRANSACTIONS ." where `transID` = '".$_GET["transID"]."'");


$queryBen = "select * from ".TBL_BENEFICIARY." where benID = '" . $result["benID"] . "'";
$benificiaryContent = selectFrom($queryBen);
			
$queryCust = "select * from ".TBL_CUSTOMER." where customerID = '" . $result["customerID"] . "'";
$customerContent = selectFrom($queryCust);			

$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID = '".$result["benAgentID"]."'");
$Admresult = $strAddressQuery;

?>
<head>
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">	
<link href="styles/opalreceipt.css" rel="stylesheet" type="text/css" />
</head>
<table width="700" border="0" cellspacing="0" cellpadding="0"  align="center">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="287" rowspan="4">
    <img height="<?=RECEIPT_LOGO_HEIGHT?>" alt="" src="<? echo CONFIG_LOGO?>" width="<?=RECEIPT_LOGO_WIDTH?>" align="top">  </tr>
  <tr>
    <td width="359" class="headingformat"><span style="border-bottom: 3px double;"><? echo COMPANY_INT_NAME ?></span></td>
  </tr>
  <tr>
    <td class="toptext">Reg. No 4741528 MSB Reg. No 12154036</td>
  </tr>
  <tr>
    <td class="headingformat01"><? echo COMPANY_ADDR ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0" class="text">
      <tr>
        <td width="49%" class="textformat2" align="right">Number of Money Transfer</td>
        <td width="20%"><span class="boxformat2"><? echo $result["refNumberIM"] ?></span></td>
		<?
				// For $nowTime showing format... [by JAMSHED]
      	$showdDate1 = explode("-", $result["transDate"]) ;
      	$showdDate2	= explode(" ", $showdDate1[2]) ;
      	$showNowTime = $showdDate2[1] ;
      	$showDay = $showdDate2[0] ;
      	$showMon = $showdDate1[1] ;
      	$showYear = $showdDate1[0] ;
      	$showNowDate = $showDay . "/" . $showMon . "/" . $showYear ;
    ?>
        <td align="right" class="textformat2">Date &nbsp;&nbsp;<span class="boxformat"><? echo $showNowDate ?>
        </span></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%"  border="2" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" class="textformat">Receiver </td>
        <td width="70%" class="textformat2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
      </tr>
      <tr>
        <td class="textformat2">Document type number</td>
        <td class="textformat2"><? echo $benificiaryContent["IDType"] . " " . $benificiaryContent["IDNumber"] ?></td>
      </tr>
      <tr>
        <td class="textformat2">Document issued - valid</td>
        <td class="textformat2">
        	<?
        		if ($benificiaryContent["IDexpirydate"] != "0000-00-00 00:00:00") {
        			$dDate   = explode("-", $benificiaryContent["IDexpirydate"]);
        			$dDate2  = explode(" ", $dDate[2]);
        			$showDay = $dDate2[0];
        			$showExpiryDate = $showDay . "/" . $dDate[1] . "/" . $dDate[0];
        		} else {
							$showExpiryDate = "";
        		}
        		echo $benificiaryContent["IDissuedate"] . " - " . $showExpiryDate;
        	?>
        </td>
      </tr>
      <tr>
        <td class="textformat2">Address phone number</td>
         <td class="textformat2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"] . " " . $benificiaryContent["Phone"] ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textformat2">Received in "<? echo COMPANY_NAME ?>" money transfer:</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
        <table width="100%"  border="2" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" align="center" class="textformat2">Amount</td>
        <td width="10%" align="center" class="textformat2">Currency</td>
        <td width="60%" align="center" class="textformat2">Amount (written)</td>
      </tr>
      <?/*
      	if (CONFIG_TRANS_ROUND_NUMBER == "1" && strstr(CONFIG_ROUND_NUM_FOR, $result["moneyPaid"].",")) {
      		$transAmount = round($result["transAmount"]);	
      	} else if (CONFIG_TRANS_ROUND_NUMBER == "1") {
      		$transAmount = round($result["transAmount"], 2);	
      	}*/
      ?>
      <tr>
        <td align="center" class="textformat2"><? echo number_format($result["transAmount"],2,'.',','); ?></td>
        <td align="center" class="textformat2"><? echo $result["currencyFrom"] ?></td>
     	<?
     		$currDescQry = "SELECT `description` FROM " . TBL_CURRENCY . " WHERE `currencyName` = '".$result["currencyFrom"]."'";
     		$currDescRes = selectFrom($currDescQry);
     	?>
        <td align="center" class="textformat2"><? echo number_to_word($result["transAmount"]) . " " . $currDescRes["description"]; ?></td>
      </tr>
	  <tr>
	  	
	  	<? $coll_point_query = "select * from ".TBL_COLLECTION." where cp_ida_id = '".$Admresult["userID"]."' and cp_id = '".$result["collectionPointID"]."' ";
	  	$coll_point=	selectFrom($coll_point_query); 
	  	$distributor = selectFrom("select agentCompany from ".TBL_ADMIN_USERS." where userID = '".$coll_point["cp_ida_id"]."'");
	  	
	  	
	  	
	  	?>
        <td rowspan="2" class="textformat">Bank Sender: </td>
        <td colspan="2" height="40" class="textformat2"><? echo $coll_point["cp_country"] . " " .$coll_point["cp_city"] . " " .$distributor["agentCompany"] . " " . $coll_point["cp_corresspondent_name"] . " " . $coll_point["cp_branch_address"]. " " .$coll_point["cp_phone"] ?></td>
      </tr>
	  <tr>
	    <td colspan="2" align="center" class="textformat3">Country City Bank's name Address Tel Number</td>
	    </tr>
      <tr>
        <td rowspan="2" class="textformat">Sender Name: </td>
        <td colspan="2" height="40" class="textformat2"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
      </tr>
	  <tr>
	    <td colspan="2" align="center" class="textformat3">Surname Given name Patronymic (if any) Additional information</td>
	    </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textformat2">Amount paid out in local currency: </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%"  border="2" cellspacing="0" cellpadding="0">
  <tr>
    <td height="31" align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center" class="textformat2">Currency</td>
    <td align="center" class="textformat2">Amount (written)</td>
  </tr>
    <tr>
   	<?/*
   		if (CONFIG_TRANS_ROUND_NUMBER == "1" && strstr(CONFIG_ROUND_NUM_FOR, $result["moneyPaid"].",")) {
   			$localAmount = round($result["localAmount"]);
   		} else if (CONFIG_TRANS_ROUND_NUMBER == "1") {
   			$localAmount = round($result["localAmount"], 2);
   		}*/
   	?>
    <td align="center" class="textformat2">Amount: </td>
    <td align="center" class="textformat2"><? echo number_format($result["localAmount"],2,'.',','); ?></td>
    <td align="center" class="textformat2"><? echo $result["currencyTo"] ?></td>
     	<?
     		$currDescQry = "SELECT `description` FROM " . TBL_CURRENCY . " WHERE `currencyName` = '".$result["currencyTo"]."'";
     		$currDescRes = selectFrom($currDescQry);
     	?>
    <td align="center" class="textformat2"><? echo number_to_word($result["localAmount"]) . " " . $currDescRes["description"]; ?></td>
  </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="2" class="textformat2">This Money Transfer is private financial support and it is not related to any commercial purposes.</td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
    <tr>
    <td colspan="2">
      <table width="100%"  border="2" cellspacing="0" cellpadding="0">
  <tr>
    <td class="textformat2">Signature of receiver</td>
    <td class="textformat2">Signature of Opal Transfer sales executive</td>
  </tr>
    <tr>
    <td height="50" align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
    </table>
    </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center">
		<div class='noPrint'>
			<input type="button" name="Submit2" value="Print this Receipt" onClick="print()">
		  </div></td>
    </tr>
</table></td>
  </tr>
</table>