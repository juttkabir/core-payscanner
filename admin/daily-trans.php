<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");


 

/**
 * @package Reports 
 * @subpackage Daily Transaction Reports
 * Short Description
 * #9900: Premier Exchange: Pagination Created
 * @code For Pagination
*/

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

if ($offset == "")
 	    $offset = 0;
 	$limit=50;
 	
 	if ($_REQUEST["newOffset"] != "") {
 	    
 	    $offset = $_REQUEST["newOffset"];
 	}
 	$nxt = $offset + $limit;
 	$prv = $offset - $limit;


$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}

if(CONFIG_REPORTS_SURCHARG == '1')
{
	$extra = ", sum(bankCharges) as extra1, sum(cashCharges) as extra2, sum(admincharges) as extra3, sum(outCurrCharges) as extra4";	
}else{
	$extra = "";
	}
$query = "select date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate, date_format(transDate, '%D %b %Y') as tDate, count(transID) as cnt, sum(IMFee) as totFee, sum(transAmount) as totAmount,sum(localAmount) as localAmount,currencyFrom,currencyTo,trans_source $extra from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin;
if(CONFIG_CANCEL_DAILY_TRANS)
{
	$cancelDexcription = " and t.transStatus = 'Cancelled' AND (ac.description like '%Transaction Cancelled%' OR ac.description like '%Transaction Failed%' OR ac.description like '%Transaction Rejected%')";
	
	/**
	 * #9900 : Premier Exchange - Cancellation Transaction Counting Bugs
	 * Short Description
	 * This Query is Changed to Remove the Counting Bug for the Cancelled Transactions
	 */
	$cancelDexcription = " and t.transStatus = 'Cancelled' AND (ac.description like '%Transaction Cancelled%' OR ac.description like '%Transaction Failed%' OR ac.description like '%Transaction Rejected%')";
	$queryCancel = "select date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(cancelDate, '%Y-%m-%d') as cancelDate, dated as accountDate, sum(amount) as cancelAmount, count(t.transID) as cnt,currencyFrom,currencyTo,trans_source  from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." ,agent_account as ac";
}

if($_POST["country"]!="")
	$country = $_POST["country"];
	elseif($_GET["country"]!="") 
		$country = $_GET["country"];
	
if($_POST["currency"]!="")
	$currency = $_POST["currency"];
	elseif($_GET["currency"]!="") 
		$currency = $_GET["currency"];	
		
		
	if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
	{
		if($_REQUEST["transSource"]!="")
		{
			$transSource = $_REQUEST["transSource"];

		}
	}

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");

$totalTransactions =0;

/**
 * #9900: Premier Exchange: Pagination Created
 * Short Description
 * This Condition is Changed for Pagination
*/

if($_REQUEST["Submit"] =="Search" || $_GET['search'] == 'search')
{
	/**
	 * #9900: Premier Exchange: Pagination Created
	 * Short Description
	 * This Code is Changed For Pagination Creation
	*/
	
	if($_GET['search'] != 'search'){
		$_SESSION["fMonth"]="";
		$_SESSION["fDay"]="";
		$_SESSION["fYear"]="";
		
		$_SESSION["tMonth"]="";
		$_SESSION["tDay"]="";
		$_SESSION["tYear"]="";
		
		$_SESSION["fMonth"]=$_POST["fMonth"];
		$_SESSION["fDay"]=$_POST["fDay"];
		$_SESSION["fYear"]=$_POST["fYear"];
		
		$_SESSION["tMonth"]=$_POST["tMonth"];
		$_SESSION["tDay"]=$_POST["tDay"];
		$_SESSION["tYear"]=$_POST["tYear"];
	}
	
	
	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	if(CONFIG_CANCEL_DAILY_TRANS)
	{
		$cancelDate = " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
	}
	$queryDate = " and (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	/**
	 * #9900: Premier Exchange: Pagination Created
	 * Short Description
	 * This Code is Changed For Pagination Creation
	*/
	if($_REQUEST["fieldName"] != "")
	{
	//	echo("Value in the field is ".$_POST["fieldName"]);
		switch ($_REQUEST["fieldName"])
		{         
			case "superAgent":
			{
				$query .= " , " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Supper' and ( a.name like '%" . $_REQUEST["txtSearch"] . "%' OR a.username like '%" . $_REQUEST["txtSearch"] . "%') ";
				if(CONFIG_CANCEL_DAILY_TRANS)
				{
					$queryCancel .= " , " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Supper' and ( a.name like '%" . $_REQUEST["txtSearch"] . "%' OR a.username like '%" . $_REQUEST["txtSearch"] . "%') ";
				}
				break;
			}
			case "subAgent":
			{
				$query .= " , " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Sub' and a.name like '%" . $_REQUEST["txtSearch"] . "%' ";
				if(CONFIG_CANCEL_DAILY_TRANS)
				{
					$queryCancel .= " ," . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and a.agentType='Sub' and a.name like '%" . $_REQUEST["txtSearch"] . "%' ";
				}
				break;
			}
			case "customer":
			{
				$query .= " where (c.firstName like '%" . $_REQUEST["txtSearch"] . "%' OR c.lastName like '%" . $_REQUEST["txtSearch"] . "%') ";								
				if(CONFIG_CANCEL_DAILY_TRANS)
				{
					$queryCancel .= " where (c.firstName like '%" . $_REQUEST["txtSearch"] . "%' OR c.lastName like '%" . $_REQUEST["txtSearch"] . "%') ";
				}
				break;
			}
			case "Beneficiary":
			{
				$query .= ", " . TBL_BENEFICIARY . " as b where t.benID = b.benID and (b.firstName like '%" . $_REQUEST["txtSearch"] . "%' OR b.lastName like '%" . $_REQUEST["txtSearch"] . "%') ";								
				if(CONFIG_CANCEL_DAILY_TRANS)
				{
					$queryCancel .= ", " . TBL_BENEFICIARY . " as b where t.benID = b.benID and (b.firstName like '%" . $_REQUEST["txtSearch"] . "%' OR b.lastName like '%" . $_REQUEST["txtSearch"] . "%') ";
				}
				break;
			}
			case "Country":
			{
				
				break;
			}
			default:
				$query .= " WHERE 1 ";
				$queryCancel .= " WHERE 1 ";
		
		}
		if(CONFIG_CANCEL_REVERSE_COMM == '1')
		{
			 $queryReverse = $query;
			//echo("<br>");
			$queryReverse .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
		}
		if(CONFIG_CANCEL_DAILY_TRANS)
		{
			$queryCancel .= " and ac.agentID = t.custAgentID and ac.type = 'DEPOSIT' $cancelDexcription  and t.transID = ac.TransID";
			$queryCancel .= " $cancelDate";
			$query .= " $queryDate";
		}else{
			$query .= " $queryDate";
			}
		
	}
	else
	{
		if(CONFIG_CANCEL_REVERSE_COMM == '1')
		{
			 $queryReverse = $query;
			//echo("<br>");
			$queryReverse .= " where (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
		}	
		
		
		if(CONFIG_CANCEL_DAILY_TRANS)
		{
			
			$queryCancel .= " where ac.agentID = t.custAgentID and ac.type = 'DEPOSIT' $cancelDexcription and t.transID = ac.TransID";
			$queryCancel .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
			
		}
		
		
		$query .= " where (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	}
	if($country != "") {
		$query .= " and (t.fromCountry='".$country."') " ;
		$queryCancel .= " and (t.fromCountry='".$country."') " ;
	}
	
	if($currency != "") {
		$query .= " and (t.currencyFrom='".$currency."') " ;
		$queryCancel .= " and (t.currencyFrom='".$currency."') " ;
	}

	if($_REQUEST["transStatus"] != "")
	{
		if(CONFIG_CANCEL_REVERSE_COMM == '1')
		{
			$queryReverse .= " and t.transStatus='".$_REQUEST["transStatus"]."' ";
		}
		$query .= " and t.transStatus='".$_REQUEST["transStatus"]."' ";
	}
	else
	{
		//$query .= " and t.transStatus='Confirmed' ";	
	}
	
}
else
{
	/**
	 * #9900: Premier Exchange: Pagination Created
	 * Short Description
	 * For Retain the Field Value on Pagination
	*/
	
	
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
	
	$todate = date("Y-m-d");
	if(CONFIG_CANCEL_DAILY_TRANS)
		{
			$queryCancel = $query;
			$queryCancel .= " where t.cancelDate >= '$todate'";
		}
	if(CONFIG_CANCEL_REVERSE_COMM == '1')
		{
			$queryReverse = $query;
			$queryReverse .= " where t.cancelDate >= '$todate' ";
		}
	
	$query .= " where t.transDate >= '$todate'";
	
//	$query .= " where t.transDate >= '$todate' and t.transStatus='Confirmed'";
}

if($agentType == "Branch Manager")
{
		if(CONFIG_CANCEL_DAILY_TRANS)
		{
			 $queryCancel .= " and t.custAgentParentID ='$parentID' ";
		}
	$query .= " and t.custAgentParentID ='$parentID' ";				
} 	

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  Appending the condition with queries 
	*/
	$query        .= " and ( am.userID = '".$_SESSION["loggedUserData"]["userID"]."' ) ";
	$queryCancel  .= " and ( am.userID = '".$_SESSION["loggedUserData"]["userID"]."' ) ";
}

if($transSource == "O")
{
	$query.= " and trans_source = '".$transSource."' ";
	$queryCancel.= " and trans_source = '".$transSource."' ";

}
elseif($transSource != "O" && $transSource != "")
{
	$query.= " and trans_source != 'O' ";
	$queryCancel.= " and trans_source != 'O' ";

}


if($agentType=='SUPA' || $agentType=='SUBA')
{
	$query.= " and custAgentID = '".$parentID."' ";
	$queryCancel.= " and custAgentID = '".$parentID."' ";
}


if(CONFIG_CANCEL_REVERSE_COMM == '1')
{
	$queryCancel2 = $queryReverse; 
	 $queryCancel2 .= " and t.transStatus ='Cancelled' and t.refundFee ='No'";
	 $queryCancel2 .= " group by currencyFrom,trans_source order by t.cancelDate DESC";
	 $cancelData2 = selectMultiRecords($queryCancel2);
	
	
	$queryCancel3 = $queryReverse; 
	 $queryCancel3 .= " and (t.transStatus ='Cancelled' and (t.refundFee ='Yes' or t.refundFee = ''))";
	 $queryCancel3 .= " group by currencyFrom,trans_source order by t.cancelDate DESC";
	 $cancelData3 = selectMultiRecords($queryCancel3);
}


 if(CONFIG_CANCEL_DAILY_TRANS)
 {
 	 if(CONFIG_ADD_AMOUNT_COLUMN == "1"){
 	   $queryCancel .= " group by currencyFrom,currencyTo order by t.cancelDate DESC";
	   $contentsCancel = selectMultiRecords($queryCancel);
   
 	  }else{
	  $queryCancel .= " group by currencyFrom,trans_source order by t.transDate DESC";
	  $contentsCancel = selectMultiRecords($queryCancel);
   }
 }	
 if(CONFIG_ADD_AMOUNT_COLUMN == "1"){
 	  $query .= " group by currencyFrom,currencyTo order by transDate DESC";
 	}else{
    $query .= " group by currencyFrom,trans_source order by transDate DESC";
}



/**
 * #9900: Premier Exchange: Pagination Created
 * Short Description
 * variable is created for pagination
*/
$queryCurrenyNames = "SELECT distinct(currencyFrom) AS name FROM ".TBL_TRANSACTIONS." as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." WHERE 1 ".$queryDate." group by date_format(transDate, '%D %b %Y') order by transDate DESC LIMIT $offset, $limit";

$currencyNames = selectMultiRecords($queryCurrenyNames);

//print_r($query);
$queryPagination = $query." LIMIT $offset, $limit";
$contentsTransTotal = selectMultiRecords($query);
$contentsTrans = selectMultiRecords($queryPagination);

/**
 * #9900: Premier Exchange: Pagination Created
 * Short Description
 * This Variable count the total records for Pagination
*/

$allCount = count($contentsTransTotal);


/**
 * #9900 : Currency Wise Total
 * Short Description
 * This @array Store the Curreny Wise Total
 * 
*/
for($x= 0; $x < count($currencyNames); $x++){
	$intTotalCurrencyWise[$currencyNames[$x]['name']] = 0.00;
	$intTotalComission[$currencyNames[$x]['name']] = 0.00;
	$intTotalCancellation[$currencyNames[$x]['name']] = 0.00;
}
$intCancelledTransCounter = 0;

?>
<html>
<head>
	<title>Daily Transaction Summary Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Daily Transaction Summary Report</td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="666666">
        <form action="daily-trans.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                <br>
              <br>	
			  <?	$senderLabel = "Sender";
					$senderLabelN = $_arrLabels[$senderLabel];						
					$AgentLabel = "Agent";
					$AgentLabelN = $_arrLabels[$AgentLabel];				
				?>
        <input name="txtSearch" type="text" id="txtSearch" value="<?php echo $_REQUEST['txtSearch']; ?>">
		<?php 
			/**
			 * #9900: Premier Exchange: For Retain the Value of Fields through Pagination
			 * Short Description
			 * This Code is Written for Retain the values of Fields
			 */
			$selected = "selected";
			$strAgentType = $_REQUEST['fieldName'];
		?>
        <select name="fieldName" id="fieldName">
          <option value="">- Doesn't Matter</option>
          <option value="superAgent" <?php if($strAgentType == 'superAgent') echo $selected;?>>Super <?=$AgentLabelN;?></option>
          <option value="subAgent" <?php if($strAgentType == 'subAgent') echo $selected;?>>Sub <?=$AgentLabelN;?> </option>
          <option value="customer" <?php if($strAgentType == 'customer') echo $selected;?>><?=$senderLabelN;?></option>
          <option value="Beneficiary" <?php if($strAgentType == 'Beneficiary') echo $selected;?>>Beneficiary</option>
          
        </select>
		<script language="JavaScript">SelectOption(document.Search.txtSearch, "<?=$_REQUEST["txtSearch"]; ?>");
			</script>
			<br><br>
        <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Status - </option>
          <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		  <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
        </select>            
				<script language="JavaScript">SelectOption(document.Search.transStatus, "<?=$_REQUEST["transStatus"]; ?>");
					</script>
			<?	if (CONFIG_CNTRY_CURR_FILTER == "1") {  ?>
        <br><br>
         <select name="country" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Country - </option> 
				<?
					$queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType like '%origin%' ";
					$countryData = selectMultiRecords($queryCountry);
					for($k = 0; $k < count($countryData); $k++)
					{?>
						 <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].country, "<?=$country?>");
			</script>
		&nbsp;&nbsp;
		<select name="currency" style="font-family:verdana; font-size: 11px; width:130">
				<option value="">-Select Currency-</option> 
				<?
					$queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyName']?>"><?=$currencyData[$k]['currencyName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].currency, "<?=$currency?>");
			</script>
		
		&nbsp;&nbsp;
			<?	}  ?>
			<? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
	  
	  	<?php 
			/**
			 * #9900: Premier Exchange: Pagination Created
			 * Short Description
			 * Html Structure For Pagination
			*/
			if($allCount > 0){
		?>
      <table width="700" border="1" cellpadding="0" bordercolor="666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
		<tr>
			<td bgcolor="#000000">
				<table cellspacing="0" cellpadding="2" border="0" bgcolor="#FFFFFF" width="100%">
					<tbody>
						<tr>
							<td>            
								Showing <b><?php echo ($offset+1)." - ".($offset + count($contentsTrans)); ?></b>
								of
								<?php 
									/**
									 * #9900: Premier Exchange: Pagination Created
									 * Short Description 
									 * @variable $strPaginationLink is for Paginations Links
									*/
									echo $allCount;
									$strPaginationLink = "&txtSearch=".$_REQUEST['txtSearch']."&fieldName=".$strAgentType."&transStatus=".$_REQUEST['transStatus']."&transSource=".$transSource."&search=search";
									if($prv >= 0){
								?>
							</td>
							<td width="50" >
								<a href="<?php print $PHP_SELF."?newOffset=0".$strPaginationLink; ?>"><font color="#005b90">First</font></a>&nbsp;
							</td>
							<td width="50" align="right">
								<a href="<?php print $PHP_SELF."?newOffset=".$prv.$strPaginationLink; ?>"><font color="#005b90">Previous</font></a>&nbsp;
							</td>
							<?php 
								}
								if($nxt > 0 && $nxt < $allCount){
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
							?>
							<td width="50" align="right">
								<a href="<?php print $PHP_SELF."?newOffset=".$nxt.$strPaginationLink; ?>"><font color="#005b90">Next</font></a>&nbsp;
							</td>
							<td width="50" align="right">
								<a href="<?php print $PHP_SELF."?newOffset=".$alloffset.$strPaginationLink; ?>"><font color="#005b90">Last</font></a>&nbsp;
							</td>
							<?php 
								}
							?>
						</tr>
					</tbody>
				</table>
		    </td>
		</tr>
	    <tr>
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0 || count($contentsCancel) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1">Transaction Source</span></td>
			  <td width="120"><span class="style1">Total Transactions </span></td>
			  <td><span class="style1">Transaction Amount </span></td>
			  <? if(CONFIG_ADD_AMOUNT_COLUMN =="1" ) { ?>
			  <td><span class="style1">Receiving Amount </span></td>
			  <? } ?>
			  <td width="100"><span class="style1"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></span></td>
			  <td width="50"><span class="style1"><? if(CONFIG_REPORTS_SURCHARG == '1'){echo"Surcharges";} ?>&nbsp;</span></td>
			  <?
			  if(CONFIG_CANCEL_DAILY_TRANS == '1')
			  {
			  	?>
			  	<td><span class="style1">Cancelled Amount </span></td>
			  	<td><span class="style1">Cancelled Transactions </span></td>
			  	<?
				}
			  ?>

			  <!--<td align="center">&nbsp;</td>-->
		  </tr>
		    <?
		    
		  		
			$transactionCount = count($contentsTrans)+ count($contentsCancel);
			 $recordNormal = 0;
			 $recordCancel = 0;
		     $myCount=count($contentsCancel);
			 for($i=0;$i < $transactionCount;$i++)
		     {
		     //debug($contentsTrans[$i]["cnt"]);
		     $totalCalculated = 0;
		     $totalCalculatedLocal  = 0;
			 $calculatFee = 0;
			 $counter = 0;
			$counter += $contentsTrans[$i]["cnt"]; 
		      $grandCount = ($grandCount+$contentsTrans[$i]["cnt"]);	
				
						
		     	        
		     	$dateSelected = $contentsTrans[$recordNormal]["simpleDate"];
		     	$currencyFromSelected = $contentsTrans[$recordNormal]["currencyFrom"];
		     	$recordNormal++;
		     	
		     	if($dateSelected == '')
		     	{
		     		$dateSelected = $contentsCancel[$recordCancel]["cancelDate"];
		     		$recordCancel++;
		     		$recordNormal--;
		    	}
		    	if($dateSelected == $contentsCancel[$recordCancel]["cancelDate"])
					{
					$recordCancel++;
					$i++;
					}
			
			 
			for($data = 0; $data < count($contentsTrans); $data++)
			{ 
			
				//if($dateSelected ==  $contentsTrans[$data]["simpleDate"] )
				if(CONFIG_ADD_AMOUNT_COLUMN == "1"){
				 if($dateSelected ==  $contentsTrans[$data]["simpleDate"] && $currencyFromSelected ==  $contentsTrans[$data]["currencyFrom"])
				{
					 //if(CONFIG_ADD_AMOUNT_COLUMN == "1"){
					  $totalCalculatedLocal = $contentsTrans[$data]["localAmount"];  
					  $grandTotalAmountLocal = ($grandTotalAmountLocal + $contentsTrans[$data]["localAmount"]);
					  $currencyFrom=$contentsTrans[$data]["currencyFrom"];
					  $currencyTo=$contentsTrans[$data]["currencyTo"];
						//}
						
					$totalCalculated = $contentsTrans[$data]["totAmount"];
					
					
					
					$grandTotalAmount = ($grandTotalAmount + $contentsTrans[$data]["totAmount"]);
					$calculatFee = $contentsTrans[$data]["totFee"];
					$grandFee = $grandFee + $contentsTrans[$data]["totFee"];
					//$counter += $contentsTrans[$data]["cnt"]; 
					/**
					 * #9900: Premier Exchange: Currency Wise Total
					 * Short Description 
					 * These @array store the currency Wise Total
					*/
					$intTotalCurrencyWise[$currencyFrom] += $totalCalculated;
					$intTotalComission[$currencyFrom] += $calculatFee;
					
					// $grandCount = ($grandCount+$contentsTrans[$data]["cnt"]);
					 if(CONFIG_REPORTS_SURCHARG == '1')
					 {
					 	 $surcharges = $contentsTrans[$data]["extra1"] + $contentsTrans[$data]["extra2"] + $contentsTrans[$data]["extra3"] + $contentsTrans[$data]["extra4"];
					 	 $totalsurcharges += $surcharges;
					 	  
				 	  }
				} // end if condition
			 }else{
			            if($dateSelected ==  $contentsTrans[$data]["simpleDate"])
				      {
					 //if(CONFIG_ADD_AMOUNT_COLUMN == "1"){
						$currencyFrom=$contentsTrans[$data]["currencyFrom"];
						$currencyTo=$contentsTrans[$data]["currencyTo"];
						$totalCalculatedLocal = $contentsTrans[$data]["localAmount"];  
						$grandTotalAmountLocal = ($grandTotalAmountLocal + $contentsTrans[$data]["localAmount"]);
						//}
						
						
					$totalCalculated = $contentsTrans[$data]["totAmount"]; 
					
					
					$grandTotalAmount = ($grandTotalAmount + $contentsTrans[$data]["totAmount"]);
					$calculatFee = $contentsTrans[$data]["totFee"];
					$grandFee = $grandFee + $contentsTrans[$data]["totFee"];
					//$counter += $contentsTrans[$data]["cnt"]; 
					 
					/**
					 * #9900: Premier Exchange: Currency Wise Total
					 * Short Description 
					 * This @array store the currency Wise Total
					*/
					$intTotalCurrencyWise[$currencyFrom] += $totalCalculated;
					$intTotalComission[$currencyFrom] += $calculatFee;
					
					// $grandCount = ($grandCount+$contentsTrans[$data]["cnt"]);
					 if(CONFIG_REPORTS_SURCHARG == '1')
					 {
					 	 $surcharges = $contentsTrans[$data]["extra1"] + $contentsTrans[$data]["extra2"] + $contentsTrans[$data]["extra3"] + $contentsTrans[$data]["extra4"];
					 	 $totalsurcharges += $surcharges;
					 	  
					  }
				}//end if condition
			 }
			}
			if(CONFIG_CANCEL_REVERSE_COMM == '1')
			{
				
				for($data2=0;$data2 < count($cancelData2);$data2++)
				{
					if($dateSelected == $cancelData2[$data2]["cancelDate"])
					{
						$totalCalculated -=  $cancelData2[$data2]["totAmount"];
					    $grandTotalAmount -= $cancelData2[$data2]["totAmount"];
					    
						
						/**
						 * #9900: Premier Exchange: Currency Wise Total
						 * Short Description 
						 * This @array store the currency Wise Total
						*/
						$intTotalCurrencyWise[$currencyFrom] -= $cancelData2[$data2]["cancelAmount"];
						$intCancelledTrans[$currencyNames[$x]['name']] = 0;
					  //  $counter -= $contentsTrans[$data2]["cnt"]; 
					 //$grandCount -= $contentsTrans[$data2]["cnt"];
					    
					}
				}
				
				
				for($data3=0;$data3 < count($cancelData3);$data3++)
				{
					if($dateSelected == $cancelData3[$data3]["cancelDate"] && $currencyFromSelected ==  $contentsTrans[$data]["currencyFrom"])
					{
						$totalCalculated -=  $cancelData3[$data3]["totAmount"];
					//echo("total<br>".$cancelData3[$data3]["totAmount"]."<br>".$cancelData3[$data3]["cnt"]."<br>");
					    $grandTotalAmount -= $cancelData3[$data3]["totAmount"];
					// echo("Grand<br>"."<br>");
					    $calculatFee -= $cancelData3[$data3]["totFee"];
					 // echo("Fee<br>".$cancelData3[$data3]["totFee"]."<br>");
					    $grandFee -= $cancelData3[$data3]["totFee"];
					//  echo("GrandFee<br>");
					   //$counter -= $contentsTrans[$data3]["cnt"]; 
					// $grandCount -= $contentsTrans[$data3]["cnt"];
					/**
					 * #9900: Premier Exchange: Currency Wise Total
					 * Short Description 
					 * This @array store the currency Wise Total
					*/
					$intTotalCurrencyWise[$currencyFrom] -= $cancelData3[$data3]["cancelAmount"];
					}
				}
			}
			//debug($dateSelected);
			?>
				
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo $dateSelected ;?></font></strong></td>
				  <?php if($contentsTrans[$i]["trans_source"] == "O")
					{
						$showTransSource = "Online";
					}
					else
					{
						$showTransSource = "Payex";
					}
					?>
				  
				  <td width="100" bgcolor="#FFFFFF"><? echo $showTransSource ?></td>
				  
				  <td width="120" bgcolor="#FFFFFF"  align="right"><?=$counter?></td>
				  <?php
				  $totalTransactions++;
				  ?>
				  <td width="100" bgcolor="#FFFFFF" align="right"><? echo number_format($contentsTrans[$i]["totAmount"], 2, '.', ''); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; } ?></td>
				  <? if(CONFIG_ADD_AMOUNT_COLUMN =="1" ) { ?>
				   <td width="100" bgcolor="#FFFFFF" align="right"><? echo number_format($contentsTrans[$i]["localAmount"], 2, '.', '')." ".$contentsTrans[$i]["currencyTo"]; ?></td>
				  <? } ?>
				  <? //$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100"  align="right" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo number_format($calculatFee, 2, '.', '');if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; } ?></td>
				  <td width="50" align="center" bgcolor="#FFFFFF"><? echo($surcharges);if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; }  ?>&nbsp;</td>
				  <?
				  if(CONFIG_CANCEL_DAILY_TRANS == '1')
				  { //$contentsTrans[$i]["tDate"]

				  $flag = '0';
	
				  	for($j=0;$j <count($contentsCancel);$j++)
					{	
					$currencyFrom=$contentsCancel[$j]["currencyFrom"];
						if($dateSelected == $contentsCancel [$j]["cancelDate"])
						{
							
							
						if($j > 0 && $flag !='0'){
						
					  	?>
						<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo $dateSelected ;?></font></strong></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $showTransSource ?></td>
				  
				  <td width="120" bgcolor="#FFFFFF"  align="right"><?=$counter?></td>
				  <td width="100" bgcolor="#FFFFFF" align="right"><? echo number_format($contentsTrans[$i]["totAmount"], 2, '.', ''); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; } ?></td>
				  <? if(CONFIG_ADD_AMOUNT_COLUMN =="1" ) { ?>
				   <td width="100" bgcolor="#FFFFFF" align="right"><? echo number_format($contentsTrans[$i]["localAmount"], 2, '.', '')." ".$contentsTrans[$i]["currencyTo"]; ?></td>
				  <? } ?>
				  <? //$agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100"  align="right" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo number_format($calculatFee, 2, '.', '');if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; } ?></td>
				  <td width="50" align="center" bgcolor="#FFFFFF"><? echo($surcharges);if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo" ".$contentsTrans[$i]["currencyFrom"]; }  ?>&nbsp;</td>
				  <?
				  }
				  
				  ?>
					  	<td width="100" bgcolor="#FFFFFF"><? 
					  		// refund, sum(totalAmount
					  		
					  		echo $contentsCancel [$j]["cancelAmount"];
					  		$grandCancel += $contentsCancel [$j]["cancelAmount"];
							/**
							 * #9900: Premier Exchange: Currency Wise Total
							 * Short Description 
							 * This @array store the currency Wise Total
							 */
							$intTotalCancellation[$currencyFrom] += $contentsCancel [$j]["cancelAmount"];

							if(CONFIG_VIEW_CURRENCY_LABLES == "1"){
								echo" ".$currencyFrom;
							 }
					  		?> </td>
					  	<td width="100" bgcolor="#FFFFFF">
							<?
								 echo $contentsCancel [$j]["cnt"];
						  		$grandCancelCount += $contentsCancel [$j]["cnt"];
								/**
								 * 
								 */
								$intCancelledTransCounter += $contentsCancel [$j]["cnt"];
								if(CONFIG_VIEW_CURRENCY_LABLES == "1"){
									//echo" ".$currencyFrom;
								 }
					  		
					  		?> </td>
					  	<?
							if(count($contentsCancel) > 1 && $flag=='0'){
								echo "</tr>";
							}
							$flag = '1';
							if($j > 1){
								echo "</tr>";
							}
							
						}
					} 
					$myCount=0;
					
				  }
				  ?>
					 <? if($flag!= '1'){ ?>
					 <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
					 <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
					 <? } ?>
				</tr>
				
				<?
				}
				
		
			
			
			if(CONFIG_ADD_AMOUNT_COLUMN !="1" ) { ?>
			<tr bgcolor="#FFFFFF" >
			      <td align="center">Total Days: <? echo count($contentsTrans);?></td>
			  <td width="120"  align="center"></td>
			<td width="120"  align="center"><? echo $grandCount;?></td>
			<td width="120"  align="center"><? //echo $totalTransactions;?></td>
			  <td  align="right" width="130px">
			  		<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="1" bordercolor="#666666">
						<?php 
								foreach($intTotalCurrencyWise as $i => $v){
								$y = $intTotalCurrencyWise[$i] - $intTotalCancellation[$i];
								
							?>
						<tr>
							<td align="left" style="padding:0 0 2px 2px;border-top:1px solid #666;border-bottom:1px solid #666;">
							<? echo $i." : "; ?></td>
							<td align="right" style="padding:0 2px 2px 0;border-top:1px solid #666;border-bottom:1px solid #666;">
							<?php echo number_format($y, 2, '.', '');?></td>
						</tr>
						<?php 
							}
						?>
					</table>
	  		  </td>
			  <? if(CONFIG_ADD_AMOUNT_COLUMN =="1" ) { ?>
			  <td  align="right"><? echo number_format($grandTotalAmountLocal, 2, '.', '');?></td>
			  <? } ?>
			  <td width="100" align="right">
			  	<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="1" bordercolor="#666666">
						<?php 
								foreach($intTotalComission as $i => $v){
							?>
						<tr>
							<td align="left" style="padding:0 0 2px 2px;border-top:1px solid #666;border-bottom:1px solid #666;">
								<? echo $i." : "; ?>
							</td>
							<td align="right" style="padding:0 2px 2px 0;border-top:1px solid #666;border-bottom:1px solid #666;">
								<?php echo number_format($v, 2, '.', '');?>
							</td>
						</tr>
						<?php 
							}
						?>
					</table>
			  </td>
			  <td width="50" align="center"><? echo $totalsurcharges; ?>&nbsp;</td>
			 
			  <?
			  if(CONFIG_CANCEL_DAILY_TRANS == '1')
			  {
			  	?>
			  	<td align="right" bgcolor="#FFFFFF">
					&nbsp;
						<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="1" bordercolor="#666666">
						<?php 
								foreach($intTotalCancellation as $i => $v){
							?>
						<tr>
							<td align="left" style="padding:0 0 2px 2px;border-top:1px solid #666;border-bottom:1px solid #666;">
								<? echo $i." : "; ?>
							</td>
							<td align="right" style="padding:0 2px 2px 0;border-top:1px solid #666;border-bottom:1px solid #666;">
								<?php echo number_format($v, 2, '.', '');?>
							</td>
						</tr>
						<?php 
							}
						?>
					</table>
				 </td>
			  	<td align="center" bgcolor="#FFFFFF">&nbsp;
					<?php echo $grandCancelCount;?>
				</td>
			  	<?
				}
			  ?>
			 <!--  <td align="center">&nbsp;</td> -->
			  </tr>
			<?
		}
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
      </table>
	   <?php 
	  	}
		else{
			echo '<table width="700" cellpadding="0" bordercolor="666666" border="1">
					<tbody>
						<tr bgcolor="#FFFFFF">
							<b style="color:#ef5858">No Records Found!!</b>
						</tr>
					</tbody>
				</table>';
		}
	  ?>
	  </td>
  </tr>

</table>
</body>
</html>