<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];

if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
}

if($_POST["custID"]!="")
	$_SESSION["custID"] =$_POST["custID"];
	elseif($_GET["custID"]!="")
		$_SESSION["custID"]=$_GET["custID"];
	else 
		$_SESSION["custID"] = "";
		
	
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Online Customer Account</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
function checkForm(theForm)
{
	if(theForm.custID.value == "")
	{
		alert("Please provide Online Sender Reference Number.");
		theForm.custID.focus();
		return false;	
	}	
	return true;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
  <?
  $senderLabel = "Sender";
	$senderLabelN = $_arrLabels[$senderLabel];	
  ?>
    <td class="topbar">Online <?=$senderLabelN;?> Account </td>
  </tr>
  <? if ($_GET["msg"] == "Y"){ ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
		  <?
		  }
		  ?>
  <tr>
    <td align="center">
           	
	<form action="online-customer-account.php" method="post" name="Search" onSubmit="return checkForm(this);">
		 <table border="1" cellpadding="5" bordercolor="#666666">
		<tr>
            <td width="463" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
  		From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
				<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         		SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        	</script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                <br>
                Online <?=$senderLabelN;?> Reference
                <input name="custID" type="text" id="custID" size="15" style="font-family:verdana; font-size: 11px; width:100" value="<?=$_SESSION["custID"];?>">
                <br>
                <input type="submit" name="Submit" value="Search">
              </td>
      </tr>
    </table>
      <br>
	  <br>
      <br>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["search"]=="search")
			$slctID=$_GET["custID"];
		else
			{
				$slctID=$_POST["custID"];	
			}
			
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(a.Date >= '$fromDate 00:00:00' and a.Date <= '$toDate 23:59:59')";
			 
		if ($slctID !=""){
			$accountQuery = "Select a.customerID, a.Date, a.tranRefNo, a.payment_mode, a.Type, a.amount, a.modifiedBy from customer_account as a, cm_customer as c where a.customerID = c.c_id and  c.c_name like '$slctID'";
			
				 $accountQuery .= " and $queryDate";				
				
			
			$accountQueryCnt = "Select count(a.customerID) from customer_account as a, cm_customer as c where a.customerID = c.c_id and  c.c_name like '$slctID'";
			$accountQueryCnt .= " and $queryDate";				
			
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&custID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			<td>
				<table>
					<tr>
						<td width="300">
							Cumulative Total 
							<? 
								$customerBalance = selectFrom("select c_id from ".TBL_CM_CUSTOMER." where c_name = '".$slctID."'");
								//$allAmount = $customerBalance["Balance"];
								echo number_format(onlineCustomerAccountBalance($customerBalance["c_id"]),2,'.',',');
							?>
			</td>
			<td>
				<? if ($allAmount < 0 ){ ?>
				<font color="red"><strong>The agent's Sender has to pay the money </strong></font>
				<? 
			}		
			if ($allAmount > 0 )
			{		
			?>
			<strong> Agent's Sender Balance  </strong>
			<? } ?>
			</td>
		</tr>
			</table>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
						  <td align="left" ><span class="style1">Date</span></td>
						  <td align="left"><span class="style1">Modified By</span></td>
						  
						<td align="left"><span class="style1">Money In</span></td>
						  
						<td width="213" align="left"><span class="style1">Money Out</span></td>
						<td width="120"  align="left"><span class="style1">Transaction</span></td>
						<td width="96" align="left"><span class="style1">Description</span></td>
				</tr>
				<? for($i=0;$i < count($contentsAcc);$i++)
				{
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td width="200" align="left"><strong><font color="#006699"><? 
					$authoriseDate = $contentsAcc[$i]["Date"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td width="200" align="left">
					<?  
					
					$agents = selectFrom("select userID, username, name from ".TBL_ADMIN_USERS." where userID = '".$contentsAcc[$i]["modifiedBy"]."'");
					echo $agents["username"];
					
					?></td>
					<td width="150" align="left"><?  
					if($contentsAcc[$i]["Type"] == "DEPOSIT")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["Type"] == "WITHDRAW")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?></td>
					<? if($contentsAcc[$i]["tranRefNo"] != ""){
							$trans = selectFrom("SELECT refNumberIM FROM ".TBL_TRANSACTIONS." WHERE refNumberIM ='".$contentsAcc[$i]["tranRefNo"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["tranRefNo"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				<td width="56">
					&nbsp;<? echo $contentsAcc[$i]["payment_mode"]; ?>
				</td>
					</tr>
				<?
				}
				?>
			  <tr bgcolor="#FFFFFF">
				   <td>&nbsp;</td>
				  <td>&nbsp;</td>
				   <td>&nbsp;</td>
				  <td align="center">&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left"><? echo  number_format($Balance,2,'.',','); ?></td>
				  	<td>&nbsp;</td>
						<td>&nbsp;</td>
				</tr>			
				<tr> 
					  <td class="style3" width="200"> Running Total:</td>
					  <td width="200"> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					echo number_format($_SESSION["grandTotal"],2,'.',',');
					?>
					  </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
						<td>&nbsp;</td>	<td>&nbsp;</td>
                </tr>
            <tr bgcolor="#FFFFFF">
              <td colspan="5">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table width="781" border="0" bordercolor="#EFEFEF">
            <tr bgcolor="#FFFFFF">
				<td width="93" align="left"><span class="style1">Date</span></td>
                 
                <td width="215" align="left"><span class="style1">Modified By</span></td>
                      
                 <td width="174" align="left"><span class="style1">Money In</span></td>
                    
                 <td width="281" align="left">Money Out</td>
				 <td width="120"  align="left"><span class="style1">Transaction</span></td>
						<td width="96" align="left"><span class="style1">Description</span></td>
            </tr>	
			<tr bgcolor="#FFFFFF">
				<td>&nbsp;</td>
              	<td>&nbsp;</td>
              	<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
              	<td align="left">&nbsp;</td>
            </tr>
			</table>
		   </td>
		  </tr>
			<?
			}
			//if($slctID!='')
				//{
					if($agentType == "admin" || $agentType == "Admin Manager")//101
					{
						?>
					  <tr> 
						<td bgcolor="#c0c0c0"><span class="child"><a HREF="add_OnlineCustomerAccount.php?agent_Account=<?=$_SESSION["custID"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit 
						  Money</font></a></span></td>
					  </tr>
					  <? 
				  	}
				 //}
	?>
       </table>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>