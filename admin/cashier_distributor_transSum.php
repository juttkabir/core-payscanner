<?php
include ("../include/config.php");
include ("security.php");
include ("javaScript.php");
//include("websonic.php");
$agentType = getAgentType();
$gtotalSum = 0;
if(CONFIG_TRANS_ROUND_LEVEL != "")
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
}

if(CONFIG_DAILY_CASH_FOR_AGENT == "1"){
	$name = "Agent";
}else{
	$name = "Distributor";
}

$condition = False;	
//$_POST['userID']  = $_SESSION["loggedUserData"]["userID"];
$fDate = date('Y-m-d');
$tDate = date('Y-m-d');

if($_POST["fromDate"]!=""){
	$fromDate = $_POST["fromDate"];
}elseif($_GET["fromDate"]!="") {
  $fromDate = $_GET["fromDate"];
}
//$qryString .= "&fromDate=".$fromDate;   	    

if($_POST["toDate"]!=""){
	$toDate = $_POST["toDate"];
}elseif($_GET["toDate"]!="") {
  $toDate = $_GET["toDate"];
}
//$qryString .= "&toDate=".$toDate;   	
if($_POST["bankID"] != "")
{
	$bankID = $_POST["bankID"];	
}else if($_GET["bankID"] != ""){
	$bankID = $_GET["bankID"];	
	}
	if($agentType == "SUPA" || $agentType == "SUPAI" || $agentType == "SUBA" || $agentType == "SUBAI" || $agentType == "TELLER")
	{
		$condition = True;	
	}
	
	
	if(CONFIG_DAILY_CASH_FOR_AGENT == "1"){
		$agentOrDistID = "t.custAgentID"; //this is the agent ID
	}else{
		$agentOrDistID = "t.benAgentID"; // this is distributor ID
	}
$userInfoQuery = "select sum(t.totalAmount) as sum, a.name, a.userID from ".TBL_TRANSACTIONS." t, ".TBL_ADMIN_USERS." a where ".$agentOrDistID." = a.userID and (t.transStatus = 'Pending' or t.transStatus = 'Authorize' or t.transStatus = 'Amended')"; 
	
	if($fromDate != '' && $toDate != ''){
		$fDate = explode("/",$fromDate);
		if(count($fDate) == 3)
			$fDate = $fDate[2]."-".$fDate[1]."-".$fDate[0];
		else
			$fDate = $fromDate;
	
		$tDate = explode("/",$toDate);
		if(count($tDate) == 3)
			$tDate = $tDate[2]."-".$tDate[1]."-".$tDate[0];
		else
			$tDate = $toDate;
	 
	  $queryDate = "(t.transDate >= '$fDate 00:00:00' and t.transDate <= '$tDate 23:59:59')";
		$userInfoQuery .= " and $queryDate";
	}else{
		$queryDate = "(t.transDate >= '$fDate 00:00:00' and t.transDate <= '$tDate 23:59:59')";
		$userInfoQuery .= " and $queryDate";
	}
	
	if($bankID != "" && $bankID != "all")
	{
		$userInfoQuery .= " and ".$agentOrDistID." = '".$_POST["bankID"]."'";
			
	}
 	//if($condition)
 	//{
 		$userInfoQuery .= " and t.addedBy = '".$_SESSION["loggedUserData"]["username"]."'";
	//}
	
$userInfoQuery .= " group by ".$agentOrDistID."";
$userInfo = selectMultirecords($userInfoQuery);


?>
<head>
	<title><? echo(CONFIG_REPORT_CAPTION); ?></title>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
	
	<script language="javascript">
	<!--
	function SelectOption(OptionListName, ListVal)
	{
		for (i=0; i < OptionListName.length; i++)
		{
			if (OptionListName.options[i].value == ListVal)
			{
				OptionListName.selectedIndex = i;
			}
		}
	}
		
// end of javascript -->
 	</script>	
<style type="text/css">
.inputclass{
width:70px;
}
.style2 {
	color: #6699CC;
	font-weight: bold;
}
</style>
</head>
<table width="100%" border="0" cellspacing="1">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2"><? echo(CONFIG_REPORT_CAPTION); ?> </font></strong></td>
  </tr>
</table>
<br> 
<form action="cashier_distributor_transSum.php" method="post" name="Search">
<table border="1" cellpadding="5" bordercolor="#666666" width="60%" align="center">
	<tr>
		<td  valign="top">
			<table border="0" cellpadding="5" bordercolor="#666666" width="100%">
				<tr>
      		<td width="100%" colspan= "4" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters </strong></span>
          </td>
  			</tr>
  			<tr>
      		<td align="center" nowrap colspan="4">
      			
      			From Date <input name="fromDate" type="text" id="fromDate"  value="<? echo $fromDate;?>" readonly>&nbsp;<a href="javascript:show_calendar('Search.fromDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
 						&nbsp;To Date&nbsp;<input name="toDate" type="text" id="toDate"  value="<? echo $toDate;?>" readonly>&nbsp;<a href="javascript:show_calendar('Search.toDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
      			            			
      		</td>
      	</tr>	
      	<?
      	if(!$condition){
      	?>				
					<tr>
						<td><?=$name?> </td>
						<td>
			      	<select name="bankID" style="font-family:verdana; font-size: 11px">
			        	<option value="">- Select <?=$name?>-</option>
			          <option value="all">All <?=$name?></option>
			          <?
			          if(CONFIG_DAILY_CASH_FOR_AGENT == "1"){
			          	$banks = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany");
			          }else{
			          	$banks = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent = 'ONLY' order by agentCompany");
			          }
			          
			          for ($i=0; $i < count($banks); $i++){
			          ?>
			          	<option value="<?=$banks[$i]["userID"]; ?>"><? echo($banks[$i]["agentCompany"]." [".$banks[$i]["username"]."]"); ?></option>
			          <?
			          }  
			          ?>
			       </select>         
			       <script language="JavaScript">
			   	 			SelectOption(document.Search.bankID, "<?=$bankID; ?>");
			       </script>
		       	</td> 
						
		     	</tr>
		     	<?
		     	}
		     	?>
	     	<tr>
    			<td colspan= "4" align="center"><input type="submit" name="Submit" value="Search"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
  	<td colspan="2">Logged in as: <strong><u><? echo $_SESSION["loggedUserData"]["name"];?></u><strong><br></td>
 	</tr>
 	<tr>
		<td>&nbsp;</td>
	</tr>
  <tr>
  	<td>
  		<table border="1" cellpadding="0" cellspacing="0" width="100%" align="center" style="padding:5px;">
				<tr bgcolor="#EBEBEB">
					<td><strong><?=$name?> Name</strong></td>
					<td><strong>Sum</strong></td>
				</tr>
						
			<?
			for($i = 0; $i < count($userInfo); $i++){
			?>
				<tr>
					<td><? echo $userInfo[$i]["name"];?></td>
					<? $sum = round($userInfo[$i]["sum"], $roundLevel);?>
					<td><? echo $sum;?></td>
					<? $gtotalSum += $sum;?>
				</tr>
			<?
			}
			?>
				<tr bgcolor="#EBEBEB">
					<td><strong>TOTAL</strong></td>
					<td><b><?= $gtotalSum ?></b></td>
				</tr>
			
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
  <tr>
   	<td><div class='noPrint'>
   		<a href="cashier_denomination_currency.php?bankID=<?=$bankID?>&fromDate=<?=$fromDate?>&toDate=<?=$toDate?>" class="style2">Add New Denomination</a>
   		</div></td>
   	<td><div class='noPrint'>
			<a href="cashierDCurrency-List.php" class="style2">List View</a>
		</div></td>
  </tr>
</table>