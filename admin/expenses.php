<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	/**
	 * Variables from Query String
	 */
	$cmd 			= $_REQUEST["cmd"];
	$narration 		= $_REQUEST["narration"];
	$amount 		= $_REQUEST["amount"];
	$id 			= $_REQUEST["id"];
	$dates			= $_REQUEST["dates"];
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_SERVER["PHP_SELF"]);
	
	/**
	 * Some pre data preparation.
	 */
	$narration 			= trim($narration);
	$amount 			= (!empty($amount))?trim($amount):"0";
	$userId				= $_SESSION["loggedUserData"]["userID"];
	//debug($_SESSION["loggedUserData"]);
    $limtFlag = false;
	if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1"){
		$limitCheckFlag = true;
	
	$extraField = ",limit_status,status";
	$extraValues = ",'',''";
	$limitDailyExpense 	= $_SESSION["loggedUserData"]["limitDailyExpense"];
	$totalUse = "";
	 $totalExpense ="";

		if(!empty($_REQUEST["adminStaffId"]))
			$userId	 = $_REQUEST["adminStaffId"];
		if(!empty($_REQUEST["userId"]))
			$userId	 = $_REQUEST["userId"];	
	}	
    /**
     * Some data validation.
     */
    $amount         = (is_numeric($amount))?$amount:"0";
	
	if($cmd == "ADD")
	{
		if(!empty($narration) && !empty($amount) && !empty($userId))
		{
			/*
				check user limit
			*/
			
			$sql = "INSERT INTO
						expenses(userId, narration, amount, created, updated ".$extraField.")
					VALUES
					(
						$userId, \"$narration\", $amount, now(), now() ".$extraValues."
					)
					";
			insertInto($sql);
			$recId_limit = @mysql_insert_id();
			
			if($limitCheckFlag){
			 $limitAddSql = "SELECT 
								   SUM(amount) as amount
								FROM
									expenses
								WHERE
									 userId = $userId AND
									 status != 'Rejected'
								";
				$limitAddRS  = selectFrom($limitAddSql);
				$totalExpense	= $limitAddRS["amount"];
				//debug($limitDailyExpense."-->".$totalExpense);
				
				if($totalExpense > $limitDailyExpense)
					$limtFlag = true;
	
				if($limtFlag){
					$limit_status = "N";
					$status = "Hold";			
				}else{
					$limit_status = "Y";
					$status = "";
				}
				
				  $limitUpdateSql = "UPDATE 
										expenses
									SET
										limit_status = '".$limit_status."',
										status = '".$status."',
										updated = now()
									WHERE
										 recId  = $recId_limit AND	
										 userId = $userId
									";
				update($limitUpdateSql);
				//debug($limitUpdateSql);
			if($limtFlag)
					$errMsgLimit = "<span class='errorMessage'>You exceed you Daily Expense Limit.[$limitDailyExpense]</span>";
					
			  unset($limit_status, $status,$totalExpense,$recId_limit);		
			}			
			unset($cmd, $narration, $amount);
			
				
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "UPDATE")
	{
		if(!empty($id) && !empty($narration) && !empty($amount) && !empty($userId))
		{
			 $sql = "UPDATE 
						expenses
					SET
						narration = \"$narration\",
						amount = $amount,
						updated = now()
						".$updateFields."
					WHERE
						recId = $id
						AND userId = $userId
					";
			update($sql);
		
		if($limitCheckFlag){
		 $limitUpdateSql = "SELECT 
						       SUM(amount) as amount
					        FROM
								expenses
							WHERE
								 userId = $userId AND
								 status != 'Rejected'
							";
			$limitUpdateRS  = selectFrom($limitUpdateSql);
			$totalExpense	= $limitUpdateRS["amount"];
			//debug($limitDailyExpense."-->".$totalExpense);
			
			if($totalExpense > $limitDailyExpense)
				$limtFlag = true;

			if($limtFlag){
				$limit_status = "N";
				$status = "Hold";			
			}else{
				$limit_status = "Y";
				$status = "";
			}
			
			  $limitUpdateSql = "UPDATE 
									expenses
								SET
									limit_status = '".$limit_status."',
									status = '".$status."',
									updated = now()
								WHERE
									recId = $id
									AND userId = $userId
								";
			update($limitUpdateSql);
			//debug($limitSUpdateql);
		}	
			unset($cmd, $id, $narration, $amount);
			if($limtFlag)
				$errMsgLimit = "<span class='errorMessage'>You exceed you Daily Expense Limit.[$limitDailyExpense]</span>";
				
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "EDIT")
	{
		if(!empty($id) && !empty($userId))
		{
			$sql = "SELECT 
						recId, narration, amount
					FROM
						expenses
					WHERE
						recId = $id
						AND userId = $userId
					";
			$result = selectFrom($sql);
			
			$id 			= $result["recId"];
			$narration 		= $result["narration"];
			$amount 		= $result["amount"];
			$cmd 			= "UPDATE";
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for editing.</span>";
		}
	}
	
	if($cmd == "DELETE")
	{
		if(!empty($id) && !empty($userId))
		{
			$sql = "DELETE FROM
						expenses
					WHERE
						recId = $id
						AND userId = $userId
					";
			deleteFrom($sql);
			unset($cmd, $id);
			
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for deletion.</span>";
		}
	}
	if($cmd == "REJECT")
	{
		if(!empty($id) && !empty($userId))
		{
			$sql = "UPDATE 
						expenses
					SET
						status = 'Rejected',
						amount = '-".$_REQUEST["rejectAmount"]."',
						updated = now()
					WHERE
						recId = $id
						AND userId = $userId
					";
			update($sql);
			unset($cmd, $id);
			
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for rejecting.</span>";
		}
	}
	if($cmd == "AUTHORIZE")
	{
		if(!empty($id) && !empty($userId))
		{
			$sql = "UPDATE 
						expenses
					SET
						status = 'Approved',
						limit_status = 'Y',
						updated = now()
					WHERE
						recId = $id
						AND userId = $userId
					";
			update($sql);
			unset($cmd, $id);
			
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for approval.</span>";
		}
	}
	
	if(empty($cmd))
	{
		$cmd = "ADD";
		$amount = 0;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Expenses</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td class="reportHeader">Expenses</td>
			</tr>
		</table>
			<br />
			<?
				if(!empty($errMsg))
				{
					echo $errMsg."<br /><br />";
				}
				if(!empty($errMsgLimit) && $limitCheckFlag)
				{
				echo $errMsgLimit;
				}
			?>
		<form action="expenses.php" method="post">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td colspan="3" class="reportSubHeader">Add / Update Expenses</td>
				</tr>
			<tr>
				<td width="50%" valign="top"><br />
					Expense Detail <span class="errorMessage">*</span><br />
					<input name="narration" type="text" class="inputBoxesText" id="narration" value="<?=$narration?>" maxlength="255" />
					<br />
					<br /></td>
				<td width="50%" valign="top"><br />
					Amount <span class="errorMessage">*</span><br />
					<input name="amount" type="text" class="inputBoxesNumerics" id="amount" value="<?=$amount?>" maxlength="8" />
					<br /></td>
			<? if($limitCheckFlag && ($agentType == "admin" || $agentType == "Admin Manager") ){ ?>
				<td align="center">Users <br>
			<?
					
			$strStaffQuery  = "
								select 
									userID,
									username, 
									agentCompany, 
									name, 
									agentContactPerson,
									adminType 
								from 
									".TBL_ADMIN_USERS." 
								where 
										adminType IN ('Admin', 'Call', 'COLLECTOR', 'Branch Manager', 'Admin Manager', 'Support', 'SUPI Manager', 'MLRO', 'AM')
									AND agentStatus='Active' 
								order by 
									adminType, 
									name	";
			$arrStaff = selectMultiRecords($strStaffQuery);

			
			
				
			?>
				<select name="adminStaffId" id="adminStaffId" onchange="javascript: location.href='expenses.php?adminStaffId='+this.value">
					<option value="">- Select User -</option>
					
					<?
						for($i=0; $i < count($arrStaff); $i++)
						{
							if($arrStaff[$i]["adminType"] != $arrStaff[$i-1]["adminType"])
								echo "<optgroup label='".$arrStaff[$i]["adminType"]."'>";
							if($userId == $arrStaff[$i]["userID"])
								{
									$paramSelected2 = "selected";
								} else {
									$paramSelected2 = "";
								}
							if($intUserId == $arrStaff[$i]["userID"]) { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" <?=$paramSelected2?>><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } else { ?>
								<option value="<?=$arrStaff[$i]["userID"]?>" <?=$paramSelected2?>><?=$arrStaff[$i]["name"] . " [" . $arrStaff[$i]["username"] . "]"?></option>
							<? } 

							if($arrStaff[$i]["adminType"] != $arrStaff[$i+1]["adminType"])
								echo "</optgroup>";

						}
					?>
				</select>
				
			</td>
				<? } ?>	
			</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="right" class="reportToolbar"><input name="id" type="hidden" id="id" value="<?=$id?>" />
					<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
					<input name="userId" type="hidden" id="userId" value="<?=$userId?>" />
					<input type="submit" name="Submit" value="Submit" />
							<input type="reset" name="Submit2" value="Reset" /></td>
				</tr>
			</table>
		</form>
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td width="15%" class="columnTitle">Date</td>
				<td width="34%" class="columnTitle">Expense</td>
				<? if($limitCheckFlag){?>
				<td width="12%" align="left" class="columnTitle">Status</td>
				<? } ?>
				<td width="12%" align="left" class="columnTitle">Amount</td>
				
				<td width="27%" align="right" class="columnTitle">
					<select name="dates" id="dates" onchange="javascript: location.href='expenses.php?dates='+this.value">
						<option value="">-- Select Date --</option>
						<?
							$sql = "SELECT 
										DISTINCT DATE_FORMAT(created, '%Y-%m-%d') as created
									FROM
										expenses
									WHERE
										userId = $userId
									";
							$result = SelectMultiRecords($sql);
							for($i=0; $i<sizeof($result); $i++)
							{
								$xDates = $result[$i]["created"];
								
								if(empty($dates))
								{
									$dates = $xDates;
								}
								
								if($xDates == $dates)
								{
									$paramSelected = "selected";
								} else {
									$paramSelected = "";
								}
						?>
								<option value="<?=$xDates?>" <?=$paramSelected?>><?=$xDates?></option>
						<?
							}
						?>						
					</select>
			  </td>
			</tr>
			
			<?
				if($limitCheckFlag){
				$extraField2 = ",limit_status,status";
				}
				$sql = "SELECT
							recId,userId, narration, amount, DATE_FORMAT(created, '%Y-%m-%d') as created ".$extraField2."
						FROM
							expenses
						WHERE
							userId = $userId
							AND created like \"$dates%\"
						";
				$result = SelectMultiRecords($sql);
				$xTotal = 0;
				for($i=0; $i<sizeof($result); $i++)
				{
					$xId 			= $result[$i]["recId"];
					$xUserId		= $result[$i]["userId"];
					$xDate 			= $result[$i]["created"];
					$xNarration 	= $result[$i]["narration"];
					$xAmount 		= $result[$i]["amount"];
					if($limitCheckFlag){
						if($result[$i]["status"] != "Rejected")
							$xTotal 		+= $xAmount;
					}else{
						$xTotal 		+= $xAmount;
						}
			?>
					<tr>
						<td><?=$xDate?></td>
						<td><?=$xNarration?></td>
						<? if($limitCheckFlag){?>
						<td align="left"><?=$result[$i]["status"]?>&nbsp;</td>
						<? } ?>
						<td align="left"><?=number_format($xAmount, 2, ".", ",")?></td>
						<td align="right">&nbsp;
						<?
						 if($limitCheckFlag){
						   if($result[$i]["status"] != 'Rejected'){
						 ?>
							<a href="expenses.php?id=<?=$xId?>&cmd=EDIT">Edit</a> | 
							<a href="expenses.php?id=<?=$xId?>&cmd=DELETE">Delete</a>
							<? if($result[$i]["limit_status"] == 'N' && ($agentType == "admin" || $agentType == "Admin Manager")){ ?>
								| <a href="expenses.php?id=<?=$xId?>&cmd=REJECT&rejectAmount=<?=$xAmount?>&userId=<?=$userId?>">Rejected</a> | 
								<a href="expenses.php?id=<?=$xId?>&cmd=AUTHORIZE&allowAmount=<?=$xAmount?>&userId=<?=$userId?>">Allow</a>
							<?
							     }
								}
							 }else{					
							 ?>
							 <a href="expenses.php?id=<?=$xId?>&cmd=EDIT">Edit</a> | 
							 <a href="expenses.php?id=<?=$xId?>&cmd=DELETE">Delete</a>
							 <? } ?>
						</td>
					</tr>
			<?
				}
			?>
			
			<tr>
				<td>&nbsp;</td>
				<? if($limitCheckFlag){?>
					<td align="right">&nbsp;</td>
				<? } ?>
				<td class="netBalance">Total</td>
				<td align="left" class="netBalance"><?=number_format($xTotal, 2, ".", ",")?></td>
				<td align="right">&nbsp;</td>
			</tr>
		</table></td>
	</tr>
</table>
</body>
</html>
