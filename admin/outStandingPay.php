<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");

if($agentType == "Branch Manager")
{
	$parentID = $_SESSION["loggedUserData"]["userID"]; 
}
//include ("javaScript.php");
$currentDate= date('Y-m-d',strtotime($date_time));
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";
$foriegnTotal="";

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
session_register("agentName");
session_register("grandTotal");
session_register("CurrentTotal");
session_register("bankID");


if ($offset == "")
	$offset = 0;
$limit=15;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;


$agentType = getAgentType();



////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////



//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search"||$_GET["search"]=="search")
{

	if($condition)
	{
		$query = "select distinct t.agentID from agent_account as t,
		" . TBL_ADMIN_USERS . " as a where t.agentID = a.userID" ;
		$queryCnt = "select count(distinct t.agentID) from agent_account as t,
		" . TBL_ADMIN_USERS . " as a where t.agentID = a.userID" ;
		
	}
	else
	{
		$query = "select distinct t.agentID from agent_account as t,
		" . TBL_ADMIN_USERS . " as a where t.agentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."' " ;
		
		$queryCnt = "select count(distinct t.agentID) from agent_account as t,
		" . TBL_ADMIN_USERS . " as a where t.agentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."' " ;
	}
	
	if($agentType == "Branch Manager")
	{
		$query .= " and a.parentID = '$parentID'";								
		$queryCnt .= " and a.parentID = '$parentID'";
	}



	if($_GET["search"]!="search")
	{		
		$_SESSION["fMonth"]="";
		$_SESSION["fDay"]="";
		$_SESSION["fYear"]="";
		
		$_SESSION["tMonth"]="";
		$_SESSION["tDay"]="";
		$_SESSION["tYear"]="";
		
		$_SESSION["agentName"]="";
		$_SESSION["bankID"]="";
		
		$_SESSION["grandTotal"]="";
		$_SESSION["CurrentTotal"]="";	

		/*$_SESSION["fMonth"]=$_POST["fMonth"];
		$_SESSION["fDay"]=$_POST["fDay"];
		$_SESSION["fYear"]=$_POST["fYear"];
		
		$_SESSION["tMonth"]=$_POST["tMonth"];
		$_SESSION["tDay"]=$_POST["tDay"];
		$_SESSION["tYear"]=$_POST["tYear"];
		
		$dfMonth = $_POST["fMonth"];
		$dfDay = $_POST["fDay"];
		$dfYear = $_POST["fYear"];
		
		$dtMonth = $_POST["tMonth"];
		$dtDay = $_POST["tDay"];
		$dtYear = $_POST["tYear"];*/
		$fromDatevalue=$_POST["fromDate"];
		$toDatevalue=$_POST["toDate"];
		if(!empty($fromDatevalue)){
			$fromDatevalue = str_replace('/', '-', $fromDatevalue);
			$fromDate= date("Y-m-d",strtotime($fromDatevalue));
		}
		else
			$fromDate=$currentDate;
		if(!empty($toDatevalue)){
			$toDatevalue = str_replace('/', '-', $toDatevalue);
			$toDate= date("Y-m-d",strtotime($toDatevalue));
		}
		else
			$toDate=$currentDate;
		
		$_SESSION["agentName"]=$_POST["agentName"];
		$_SESSION["bankID"]=$_POST["bankID"];
	}
	
	//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
//	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	
		$queryDate = "(t.dated >= '$fromDate 00:00:00' and t.dated <= '$toDate 23:59:59')";
	
	$query .= " and $queryDate";
	$queryCnt .= " and $queryDate";
	if($_POST["agentName"] != "")
	{
		if($_POST["agentName"]!="all")
		{
			$query .= " and a.userID='".$_POST["agentName"]."' ";
			$queryCnt .= " and a.userID='".$_POST["agentName"]."' ";
		}
	}
	if($_SESSION["agentName"]!="")
	{
		if($_SESSION["agentName"]!="all")
		{
			$query .= " and a.userID='".$_SESSION["agentName"]."' ";
			$queryCnt .= " and a.userID='".$_SESSION["agentName"]."' ";
		}
	}
	
//$contentsTrans2 = selectMultiRecords($query);
//$allCount =count($contentsTrans2);	
//$query .= " LIMIT $offset , $limit";
//echo $query;
$allCount = countRecords($queryCnt);
$contentsTrans = selectMultiRecords($query);
	
$currencyOrigin = $_POST["currencyOrigin"];
}
else
{
	$todate = date("Y-m-d");

}

//echo $query;

?>
<html>
<head>
	<title>Outstanding Agent Payments</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
		  <script type="text/javascript" src="javaScript.js"></script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><Font size="2">Outstanding Balances</font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table width="482" height="106" border="1" cellpadding="5" bordercolor="#666666" id="searchTable">
        <form action="outStandingPay.php" method="post" name="Search">
      <tr>
            <td width="463" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From <input name="fromDate" type="text" id="fromDate"  value="" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
		
        To	<input name="toDate" type="text" id="toDate"  value="" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
		<br>
              <? 
              //if($agentType == "admin" || $agentType == "Call" ||$agentType == "Admin Manager" || $agentType == "Branch Manager")
              if($condition)
              {?>Agent Name 
              <select name="agentName" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Agent-</option>
		  <option value="all">All</option>
			<?
		//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){	
							$agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' AND agentStatus = 'Active' ";
			}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
				$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
		}
//work end by Mudassar Ticket #11425
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$parentID'";				
									
								}
								$agentQuery .= "order by agentCompany";
				$agents = selectMultiRecords($agentQuery);
				for ($i=0; $i < count($agents); $i++){
			?>
			<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
			<?
				}
			?>
		  </select>
		  <script language="JavaScript">
		SelectOption(document.Search.agentName, "<?=$_SESSION["agentName"]; ?>");
			</script>
			<? }?>
		<?
		if(CONFIG_MULTI_CURRENCY_BALANCE == "1")
		{
		?>	
			<select name="currencyOrigin">
				<option value="">-- Select Currency --</option>
						  <?
						 						  
							$strQuery = "SELECT DISTINCT(currencyOrigin) FROM   exchangerate ORDER BY currencyOrigin ";
							$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
					
							while($rstRow = mysql_fetch_array($nResult))
							{
								
								$currencyO = $rstRow["currencyOrigin"];	
								
													
								if($currencyOrigin == $currencyO)
									echo "<option selected value ='$currencyO'> ".$currencyO."</option>";			
								else
								  echo "<option value ='$currencyO'> ".$currencyO."</option>";	
							}
						  ?>				
				</select>
				<script language="JavaScript">
     			SelectOption(document.Search.currencyOrigin, "<?=$currencyOrigin?>");
     	  </script>
     	  <?
     		}
     	  ?>
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="495" border="1" cellpadding="0" bordercolor="#666666">
        <form action="outStandingPay.php" method="post" name="trans">


          <?
			if ($allCount > 0)
			{
				
		?>
          <tr>
            <td width="495"   bgcolor="#000000">
			<table width="498" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF" id="pagination">
                <!--tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&agentID=".$_SESSION["agentName"]."&search=search&total=first";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&agentID=".$_SESSION["agentName"]."&search=search&total=pre"?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&agentID=".$_SESSION["agentName"]."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right">&nbsp; <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"-><font color="#005b90">&nbsp;</font><!--/a->&nbsp;
                  </td>
                  <?php }
				  } ?>
                </tr-->
              </table>
	    <tr>
            <td>&nbsp;</td>
        </tr>
		 
		 
		<?
		if(count($contentsTrans) > 0)
		{
		?>
      <tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Date - <? if($fromDate < $toDate){echo "From ".$fromDate." To ".$toDate;}?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> Outstanding Balances </strong></span></td>
        </tr>                      
       
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table border="0" bordercolor="#EFEFEF" width="497">
                <tr bgcolor="#FFFFFF"> 
                  <td width="160"><span class="style1">Agent Name</span></td>
                  <td width="327"><span class="style1"> Payments</span></td>
                </tr>
                <? for($i=0;$i < count($contentsTrans);$i++)
				{
					if(CONFIG_MULTI_CURRENCY_BALANCE == '1')
					{
						$balance=agentBalanceDateCurrency($contentsTrans[$i]["agentID"], $fromDate, $toDate, $currencyOrigin);
					}else{
							$balance=agentBalanceDate($contentsTrans[$i]["agentID"], $fromDate, $toDate);
						}
					
					if($balance < 0)
					{		
						$q1 = "Select name from ". TBL_ADMIN_USERS ." where userID='".$contentsTrans[$i]["agentID"]."' ";		
						$name = selectFrom($q1); 
						?>
						<tr bgcolor="#FFFFFF">
						  <td width="160" bgcolor="#FFFFFF"><? echo $name["name"]?></td>
						  <td width="327" bgcolor="#FFFFFF"> 
							<? 
						  echo(number_format($balance,2,'.',','));
						  
						  $totalAmount += $balance;
					}						 
				  ?>
                  </td>
                </tr>
                <?
			}
			?>
              </table>
		      <table width="498" border="2" bordercolor="#666666" bgcolor="#CCCCCC">
                <tr>
			      <td width="154">&nbsp;</td>
				  <td width="116"><span class="style3">Total:</span></td>
				  <td width="204"><strong><? echo(number_format($totalAmount,2,'.',','));?></strong></td>
			    </tr>
			<tr>
				  <td class="style3"> <!--span class="style3">Total&nbsp;<? echo $allCount;?> Records.</span-->				</td>
				  <td><span class="style3">Running Total:</span></td>    
                  <td><span class="style3"><?
				if($_GET["total"]=="first")
				{
				$_SESSION["grandTotal"] = $totalAmount;
				$_SESSION["CurrentTotal"]=$totalAmount;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
					$_SESSION["CurrentTotal"]=$totalAmount;
				}elseif($_GET["total"]=="next"){
					$_SESSION["grandTotal"] += $totalAmount;
					$_SESSION["CurrentTotal"]= $totalAmount;
				}else
					{
					$_SESSION["grandTotal"] = $totalAmount;
					$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
					}
				echo(number_format($_SESSION["grandTotal"],2,'.',','));
				?>
                  </span><span class="style3">&nbsp;</span></td>    
                </tr>
		  </table></td>
        </tr>
		  <tr id="actionBtn"> 
            <td>
<div align="center">
                <input type="button" name="Submit2" value="Print This Report" id="printReport">
              </div>
			</td>
		</tr>
		<?
			} else{ // greater than zero
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
				</td>
			</tr>
			<? }?>
		</form>
      </table></td>
  </tr>
</table>
</body>
</html>
