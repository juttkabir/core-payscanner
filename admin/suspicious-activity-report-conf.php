<?php
session_start();

include ("../include/config.php");
include ("security.php");
$userID = $_SESSION["loggedUserData"]["userID"];

$name= $_POST['name'];
$reportingInstitution =$userID;
$created_on =date('Y-m-d');
$suspectID =$_GET['customerID'];
$tableForSuspect ='Customer';
$remarks =$_POST['remarks'];
$activityFrom =$_POST['activityFrom'];
$activityTo =$_POST['activityTo'];
$amountInvolved =$_POST['amountInvolved'];
$arrActivityCategory =$_POST['activityCategory'];
$arrFinServices =$_POST['finServices'];
$arrActivityType =$_POST['activityType'];
$arrAccount =$_POST['arrAccount'];
$finOther=$_POST['finServicesOther'];
$activityOther=$_POST['activityCategoryOther'];
$govtOther=$_POST['govtIssueOther'];

$activityCategory = '';
$finServices = '';
$activityType = '';
$straccNumber='';

for ($i=0;$i<count($arrAccount);$i++ ){
    $accNumber=$arrAccount[$i];
    if ($i==count($arrAccount)-1)
    $straccNumber.=$accNumber;
    else {
        $straccNumber .= $accNumber . ",";
    }
}

for ( $i = 0; $i< sizeof($arrActivityCategory); $i++ )
{
    $value= $arrActivityCategory[$i];
    if($i==sizeof($arrActivityCategory)-1)
        $activityCategory.=$value;
    else{
        $activityCategory.=$value.',';

    }
}

for ( $i = 0; $i< sizeof($arrFinServices); $i++ )
{
    $value= $arrFinServices[$i];
    if($i==sizeof($arrFinServices)-1)
        $finServices.=$value;
    else{
        $finServices.=$value.',';

    }
}

for ( $i = 0; $i< sizeof($arrActivityType); $i++ )
{
    $value= $arrActivityType[$i];
    if($i==sizeof($arrActivityType)-1)
        $activityType.=$value;
    else{
        $activityType.=$value.',';
    }
}

insertInto('INSERT INTO savedSARs(id,agentID,name,reportingInstitution,created_on,suspectID,tableForSuspect,remarks,activityFrom,activityTo,amountInvolved,activityCategory,finServices,activityType) VALUES(0,"'.$userID.'","'.$name.'","'.$reportingInstitution.'","'.$created_on.'",'.$suspectID.',"'.$tableForSuspect.'","'.$remarks.'","'.$activityFrom.'","'.$activityTo.'",'.$amountInvolved.',"'.$activityCategory.'","'.$finServices.'","'.$activityType.'")');
redirect('suspicious-activity-report.php?msg=success');

?>