<?
session_start();
include ("../include/config.php");
//include ("config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$alertID = 	$_GET["alertID"];
$page = $_GET["page"];
$create = $_GET["create"];





if ($alertID != ""){
$Mode = "Update";

 $alertData = selectFrom("select * from ".TBL_EMAIL_ALERT." where 1 and id = $alertID");

}
else{
$Mode = "Add";
}



if($_GET["report"]!= ""){
	
	$report = $_GET["report"];
	}else{
	$report = $alertData["report"];
}

?>
<html>
<head>
	<title>Weekly Report Controller</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	
function checkForm(theForm) {
	if(theForm.day.options.selectedIndex == 0){
    	alert("Please select Day.");
        theForm.day.focus();
        return false;
    }
	
  if(theForm.report.options.selectedIndex == 0){
    	alert("Please select a Report.");
        theForm.report.focus();
        return false;
    }
	
 

	return true;
}


	// end of javascript -->
	</script>
</head>
<body onload="fnBody_OnLoad();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	
  <tr>
    <td class="topbar"><strong><font class="topbar_tex"><?=$Mode?> Weekly Report Controller</font></strong></td>
  </tr>
  <form action="weeklyReportController-conf.php?page=<?=$page?>&create=<?=$create ?>&alertID=<?=$alertID ?>" method="post" onSubmit="return checkForm(this);" name="addAdmin">
 <input type="hidden" name="agentCity" value="<?=$_GET["agentCity"];?>">
	<input type="hidden" name="benCountry" value="<?=$_GET["benCountry"];?>">
	
  <tr>
    <td align="center">
		<table width="448" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong><?=$Mode?> Weekly Report Controller</strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg1"] != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>Your Weekly Report Alert Data Added successfully. </b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <tr bgcolor="#ededed"> 
          	
            <td width="144"><font color="#005b90"><strong>Select Report<font color="#ff0000">*</font></strong></font></td>
            <td><select name="report" style="font-family:verdana; font-size: 11px">
            	
				<option value="">- Select Report -</option>				
				<option value="commissionSummaryReport" <? if($report == "commissionSummaryReport"){?> "selected" <? }?>>Commission Summary report</option>
			  </select>
			  
			  
					  </td>
					</tr>
          
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Select Day<font color="#ff0000">*</font></strong></font></td>
           <td align="left">	
           	<?
           	$currentDay = date("I"); ?>  
					<select name="day" style="WIDTH: 140px;HEIGHT: 18px; ">
					<option value="">Select Day</option>						
						<option value="Monday" <? echo($alertData["day"]== "Monday" ? "selected" : "");?>>Monday</option>
						<option value="Tuesday" <? echo($alertData["day"]== "Tuesday" ? "selected" : "");?>>Tuesday</option>
						<option value="Wednesday" <? echo($alertData["day"]== "Wednesday" ? "selected" : "");?>>Wednesday</option>
						<option value="Thursday" <? echo($alertData["day"]== "Thursday" ? "selected" : "");?>>Thursday</option>
						<option value="Friday" <? echo($alertData["day"]== "Friday" ? "selected" : "");?>>Friday</option>
						<option value="Saturday" <? echo($alertData["day"]== "Saturday" ? "selected" : "");?>>Saturday</option>
						<option value="Sunday" <? echo($alertData["day"]== "Sunday" ? "selected" : "");?>>Sunday</option>
					</select>
					</td>
		  		</tr>
		  		
		  		  <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Enable<font color="#ff0000">*</font></strong></font></td>
           <td align="left">	
           <select name="isEnable" style="WIDTH: 140px;HEIGHT: 18px; ">
						<option value="Y" <? echo($alertData["isEnable"]=="N" ? "" : "selected");?>>Yes</option>						
						<option value="N" <? echo($alertData["isEnable"]=="Y" ? "" : "selected");?>>No</option>
					</select>
					</td>
		  		</tr>
		  	
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Message</strong></font></td>
            <td><input type="text" name="emailText" value="<? echo $alertData["message"]; ?>"></td>
          </tr>
         
          	
          	
            </table>
            </td>
          </tr>
          
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> <input type="submit" value=" <?=$Mode?>">
        </td>
        <? if($Mode!="Add"){ ?>
        <td>
            <a href="manageWeeklyReportController.php?report=<?=$alertData["report"];?>"><strong><font color="#000000">Go Back </font></strong></a>
        </td>
      <? } ?>
             </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
