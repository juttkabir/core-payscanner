<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();
$changedBy = $_SESSION["loggedUserData"]["userID"];	
$modifiedDate = getCountryTime(CONFIG_COUNTRY_CODE);

///////////////////////History is maintained via method named 'activities'

$subIDA = "";
$subIDAComm = "";
$subIDACommType = "";
$actAs = "";
if($_POST["View"] != "")
{
	
	
	if($_POST["subIDA"] != "")
	{
		$subIDAConfigQry = "select confID, commType, commRate, limitValue from ".TBL_DISTRIBUTOR_COMM." where user_id = '".$_POST["subIDA"]."'";
		if(CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')
		{
			$subIDAConfigQry .= " and actAs = '".$_POST["actAs"]."'";
		}
		$subIDAConfig = selectFrom($subIDAConfigQry);
		$subIDA = $_POST["subIDA"];
		$subIDAComm = $subIDAConfig["commRate"];
		$subIDACommType = $subIDAConfig["commType"];
		$subIDALimit = $subIDAConfig["limitValue"];
		$intervalFee = $subIDAConfig["interval"];
		
				
	}
}elseif($_POST["Save"] != "")
{
	
	if($_POST["subIDA"] != "")
	{
		if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
			
			$subIDAConfigQry = "select confID, commType, commRate, limitValue, currency, country from ".TBL_DISTRIBUTOR_COMM." where user_id = '".$_POST["subIDA"]."' AND country ='".$_POST["subIDACountry"]."' " ;
				if(CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')
				{
					$subIDAConfigQry .= " and actAs = '".$_POST["actAs"]."'";
				}
			
		 $subIDAConfig = selectFrom($subIDAConfigQry);	
		}else{ 
		$subIDAConfig = selectFrom("select confID, commType, commRate, limitValue, currency, country from ".TBL_DISTRIBUTOR_COMM." where user_id = '".$_POST["subIDA"]."' " );
	}
		$subIDA = $_POST["subIDA"];
		$subIDAComm = $_POST["subIDAComm"];
		$subIDACommType = $_POST["subIDACommType"];
		$subIDALimit = $_POST["subIDALimit"];
		$subIDACurrency = $_POST["subIDACurrency"];
    	$subIDACountry = $_POST["subIDACountry"];	
		$actAs = $_POST["actAs"];
		if($subIDAConfig["confID"] > 0 && CONFIG_USER_COMMISSION_MANAGEMENT != '1')
		{			
			update(" update ".TBL_DISTRIBUTOR_COMM." set 
			commType = '".$subIDACommType."',
			commRate= '".$subIDAComm."',
			lastModified = '".$modifiedDate."',
			modified_by = '".$changedBy."',
			limitValue = '".$subIDALimit."',
			currency = '".$subIDACurrency."',
			country = '".$subIDACountry."',
			`interval` = '".$_REQUEST["intervalFee"]."'
			where confID = '".$subIDAConfig["confID"]."'");
			 
			 
		}elseif($subIDAConfig["confID"] > 0 && CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
			$querySubIDA = selectFrom("select agentCompany, username from ".TBL_ADMIN_USERS." where userID = '".$subIDA."'");
		   insertError($querySubIDA["agentCompany"]." [".$querySubIDA["username"]."]"."'s Commission rate for ".$subIDACountry ." already exists"); 
		   $msg = "N";
		}
		else{
			if(CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')
				{
					$insertField = ", actAs ";
					$insertValue = " ,'".$_POST["actAs"]."'";
				}else{
					$insertField = " ";
					$insertValue = " ";
					}
					insertInto("insert into ".TBL_DISTRIBUTOR_COMM." 
				(commType, commRate, lastModified, modified_by, user_id, limitValue, currency, country, `interval` ".$insertField.") values
				('".$subIDACommType."', '".$subIDAComm."', '".$modifiedDate."','".$changedBy."', '".$subIDA."', '".$subIDALimit."', '".$subIDACurrency."', '".$subIDACountry."', '".$_REQUEST["intervalFee"]."' $insertValue )");
				insertError(" Added User's commission.");
		    $msg = "Y";
			}
			
	}
	
}

?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
<body>
<script src="jquery.js"></script>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	if(theForm.subIDA.options.selectedIndex == 0){
    	alert("Please select a user from the list.");
        theForm.subIDA.focus();
        return false;
    }
  if(theForm.subIDACurrency.options.selectedIndex == 0){
    	alert("Please select a currency.");
        theForm.subIDACurrency.focus();
        return false;
    }
   if(theForm.subIDACountry.options.selectedIndex == 0){
    	alert("Please select a country.");
        theForm.subIDACountry.focus();
        return false;
    }
	if(theForm.subIDAComm.value == ""){ // || IsAllSpaces(theForm.subIDAComm.value)
    	alert("Please provide commission rate.");
        theForm.subIDAComm.focus();
        return false;
    }
    if(theForm.subIDACommType.options.selectedIndex == 0){
    	alert("Please select a commission type.");
        theForm.subIDACommType.focus();
        return false;
    }
	
	if(document.getElementById("subIDACommType").value=="interval")
	{
		if(document.getElementById("intervalFee").value == "")
		{
			alert("Please enter the interval of the amount.");
	        document.getElementById("intervalFee").focus();
    	    return false;			
		}
	}
	
  }
  
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Add <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){echo(CONFIG_COMMISSION_MANAGEMENT_NAME);}else{ echo ("Sub Distributor");}?> Commission</td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Add <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){echo(CONFIG_COMMISSION_MANAGEMENT_NAME);}else{ echo ("Sub Distributor");}?> Commission</legend>
    <br>
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="commConfig" action="configure-distributor-commission.php" method="post" <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){?>onSubmit="return checkForm(this);"<? }?>>
			  <? if($msg == "Y" && CONFIG_USER_COMMISSION_MANAGEMENT == '1')
			  {
			  ?>
			  <tr align="center">
			  	<td align="right"><font size="4" color="<? echo SUCCESS_COLOR; ?>"><b><i><? echo SUCCESS_MARK?></i></b></font><br><br></td>
			    <td align="left" width="635" colspan="3" class="tab-r"><? echo "<font color='".SUCCESS_COLOR."'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; $msg = ""; ?><br></td>
			    </tr>
			  <?
			  }elseif($msg == "N" && CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
			  	?>
			  <tr align="center">
			  	<td align="right"><font size="5" color="<? echo CAUTION_COLOR; ?>"><b><i><? echo CAUTION_MARK?></i></b></font><br><br></td>
			    <td align="left" colspan="3" class="tab-r"><? echo "<font color='".CAUTION_COLOR."'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; $msg = ""; ?><br><br></td>
			    </tr>
			  <?
			  }
			  
			  else
			  {		  
			  ?>
		     <tr>
                	<td>&nbsp;
                		
                	</td>
                </tr>
         <? }?>       
                <tr bgcolor="#DFE6EA">
			 	<td width="35%" align="right"><? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){ echo ("User");}else{ echo ("Sub Distributor");}?> </td>
			    <td width="65%">
			    	<SELECT name="subIDA" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){ echo ("User");}else{ echo ("Sub Distributor");}?> -</OPTION>
                 
				 
				 
				 
				 
				 
				 
				 <?
					$distributors = "select userID, username, name, agentType from admin where parentID > 0 and adminType='Agent' and isCorrespondent != 'N'  and isCorrespondent != '' and PPType = '' and agentStatus = 'Active' order by username";				
					$ida = selectMultiRecords($distributors);
					
					echo "<optgroup label='Super Distributors'>";
					for ($j=0; $j < count($ida); $j++)
					{
						if($ida[$j]["agentType"] == "Supper")
						{
							if($ida[$j]["userID"] == $_REQUEST["distributors"])
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>" selected="selected">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
							else
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
						}
						elseif($ida[$j]["agentType"] == "Sub")
						{
						
						}
					}
					echo "</optgroup>";
					
					echo "<optgroup label='Sub Distributors'>";
					for ($j=0; $j < count($ida); $j++)
					{
						if($ida[$j]["agentType"] == "Sub")
						{
							if($ida[$j]["userID"] == $_REQUEST["distributors"])
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>" selected="selected">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
							else
							{
						?>
							<option value="<?=$ida[$j]["userID"]?>">
								<?=$ida[$j]["username"]." [".$ida[$j]["name"]."]"?>
							</option>
						<?
							}
						}
					}
					echo "</optgroup>";
				 ?>
				 
				 
				 <?
				 /*
          if(CONFIG_USER_COMMISSION_MANAGEMENT == '1'){
          	if(CONFIG_COMM_MANAGEMENT_FOR_ALL != '1')
          	 {
          	 		$correspond = "  and isCorrespondent != 'ONLY' ";
          	 }
          	 $querySubIDAQry = "select userID, username, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and adminType = 'Agent' ".$correspond." order by agentCompany";          
          	 $querySubIDA = selectMultiRecords($querySubIDAQry);
				}else{				
					$querySubIDA = selectMultiRecords("select userID, username, name, agentContactPerson, agentCompany from ".TBL_ADMIN_USERS." where parentID > 0 and agentType = 'Sub' and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
				}
					for($i=0; $i < count($querySubIDA); $i++){
				?>
                  <option value="<?=$querySubIDA[$i]["userID"]; ?>"><? echo($querySubIDA[$i]["agentCompany"]." [".$querySubIDA[$i]["username"]."]"); ?></option>
                  <?
					}
					*/
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].subIDA, "<?=$subIDA?>");
                                </script></td>
        <?
        if(CONFIG_COMM_MANAGEMENT_FOR_ALL == '1')
        {
        ?>
        <td align="right">
        		User Role
      	</td>
        <td>
        		<SELECT name="actAs" style="font-family:verdana; font-size: 11px">
                  <OPTION value="Agent">- Agent -</OPTION>
          				<OPTION value="Distributor">- Distributor -</OPTION>
            </SELECT>
			      	<script language="JavaScript">
         					SelectOption(document.forms[0].actAs, "<?=$actAs?>");
              </script>
         </td>    
        	
        <?
        }else{
        	?>
        	    <td colspan="2">&nbsp;</td>                 
        	<?
        	}
        ?>                        
                                
                                
            
			  </tr>
			  <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1') 
           {
        ?>
			  <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Currency&nbsp;</b></font></td>
                  <td width="26%">
                  	<select name="subIDACurrency" style="font-family:verdana; font-size: 11px">
		             <option value="">- Select Currency -</option>
		             <? $queryCurrency = "SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName "; 
		                $nResult = mysql_query($queryCurrency)or die("Invalid query: " . mysql_error()); 
							      while($rstRow = mysql_fetch_array($nResult))
							      {
								       $currency = $rstRow["currencyName"];	
								       $description = $rstRow["description"];
								       $erID  = $rstRow["cID"];	
								    						
								       if($subIDACurrency == $currency)
								       {
									       echo "<option value ='$currency' selected> ".$currency." --> ".$description."</option>";			
								       }
								       else
								       {
								         echo "<option value ='$currency'> ".$currency." --> ".$description."</option>";	
								       }
							       }
		             ?>
		            </select>
		        </td>
                  <td width="20%" align="right"><font color="#005b90"><b>Country</b></font></td>
                  <td width="37%">
                  	<select name="subIDACountry" style="font-family:verdana; font-size: 11px">
		             <option value="">- Select Country -</option>
		             <? $queryCountry = selectMultiRecords("select distinct(countryName) from ".TBL_COUNTRY." where 1 order by countryName");
		                for ($j=0; $j < count($queryCountry); $j++){
				          ?>
				            <OPTION value="<?=$queryCountry[$j]["countryName"]; ?>"><?=$queryCountry[$j]["countryName"]; ?></OPTION>
				          <?
					          }
				         ?>
		            
		            </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subIDACountry,"<?=$subIDACountry?>");</script>
                  </td>
              </tr>
         <? }?>
			 <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Commission Rate &nbsp;</b></font></td>
                  <td width="26%"><input type="text" name="subIDAComm" value="<?=$subIDAComm?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Commission Type</b></font></td>
                  <td width="37%">
                  <select name="subIDACommType" id="subIDACommType" style="font-family:verdana; font-size: 11px" onChange='if(this.value=="interval") { $("#intervalFeeRow").show(); } else { $("#intervalFeeRow").hide(); }'>
                  <? if(CONFIG_USER_COMMISSION_MANAGEMENT == '1') { ?>
				   <option value="">- Select Commission Type -</option>
					<option value="fixed">Fixed Amount</option>
					<option value="percentTrans">Percentage of Transaction Amount</option>
					<option value="percentFee">Percentage of Fee</option>
					<option value="interval">Denomination Based Fee</option>
				  <? }else {?>
				   <option value="">- Select Commission Type -</option>		            
		            <option value="percent">- Percentage of Amount -</option>
 				  <? }?>	
		          </select>
		         <script language="JavaScript">SelectOption(document.forms[0].subIDACommType,"<?=$subIDACommType?>");</script>
                  </td>
                </tr>
				<tr bgcolor="#ededed" id="intervalFeeRow" style="display:none"> 
                  <td width="20%" align="right">&nbsp;</td>
                  <td width="26%">&nbsp;</td>
                  <td><font color="#005b90"><b>Commission Interval</b></font><font color="red">*</font></td>
				  <td>
				  	<input type="text" id="intervalFee" name="intervalFee" value="<?=$intervalFee?>" maxlength="25" />
				  </td>
                </tr>
               <? if(CONFIG_USER_COMMISSION_MANAGEMENT != '1') 
                  {
               ?>
               <tr bgcolor="#ededed"> 
                  <td width="20%" align="right"><font color="#005b90"><b>Lower Limit Amount</b></font></td>
                  <td width="26%"><input type="text" name="subIDALimit" value="<?=$subIDALimit?>" maxlength="25"></td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <? }?>
         <tr>         
			   <? if(CONFIG_USER_COMMISSION_MANAGEMENT != '1') 
            {
         ?>
			   
			    <td align="center" valign="top" bgcolor="#DFE6EA" colspan="2">
			    	<input name="View" type="submit" class="flat" value="View">
			    </td>
			    <td align="center" bgcolor="#DFE6EA" colspan="2">
			    	<input name="Save" type="submit" class="flat" value="Save">
			    </td>
			  <? }else
			     {?>
			     	<td align="center" bgcolor="#DFE6EA" colspan="4">
			    	<input name="Save" type="submit" class="flat" value="Save">
			    </td>
			  			
			  <? }?>
			    
			    </tr>
			   </form>
			</table>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>