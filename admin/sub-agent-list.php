<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=20;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($Country != ""){
	$where .= " and agentCountry='$Country'";
	$qryString .= "&Country=$Country";
}
if ($_GET[agentStatus] != ""){
	$where .= " and agentStatus='$agentStatus'";
	$qryString .= "&agentStatus=$agentStatus";
}
if ($agentCode != ""){
	$where .= " and username='$agentCode'";
	$qryString .= "&agentCode=$agentCode";
}
$SQL_Qry = "select * from ".TBL_ADMIN_USERS." where	parentID > 0 and adminType='Agent' and agentType='Sub'".$where;
$agents = SelectMultiRecords($SQL_Qry);
$allCount = count($agents);
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by $sortBy ASC ";
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
// echo $SQL_Qry;
$agents = SelectMultiRecords($SQL_Qry);
?>
<html>
<head>
	<title>Categories List</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		// < (theForm.elements.length - 11)
		for(i =0; i < <?=count($agents); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to disable selected sub agents from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No Sub Agent is selected to disable.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($agents);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Sub Agents List</font></strong></td>
  </tr>
  <tr>
    <td align="center">
		<table width="671" border="0" cellspacing="2" cellpadding="1" align="center">
		<form action="<?=$PHP_SELF; ?>" method="get">
		<tr bgcolor="#FFFFCC">
			<td colspan="5">
				<table width="100%" cellpadding="2" cellspacing="0" border="1" bordercolor="#006699">
				<tr>
					<td width="9%" align="right"><b>Country:</b></td>
					<td width="20%"><SELECT name="Country" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- All -</OPTION>
				<option value="United States">United States</option>
				<option value="Canada">Canada</option>
				<?
					$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["country"]; ?>"><?=$countires[$i]["country"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$Country; ?>");
                                </script></td>
					<td width="9%" align="right"><b>Status:</b></td>
					<td width="18%"><select name="agentStatus">
						<option value="">- All -</option>
						<option value="Active">Active</option>
						<option value="Disabled">Disabled</option>
						<option value="New">New</option>
						<option value="Suspended">Suspended</option>
						</select><script language="JavaScript">
         	SelectOption(document.forms[0].agentStatus, "<?=$_GET[agentStatus]; ?>");
                                </script></td>
					
                  <td width="15%" align="right"><b>Agent Code:</b></td>
                  <td width="23%"><input type="text" name="agentCode" value="<?=$agentCode; ?>"></td>
					<td width="6%"><input type="submit" value="Go"></td>
				</tr>
				</table></td>
		</tr></form>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
  <form action="disable-agents.php" method="post" onSubmit="return checkForm(this);">
  <input type="hidden" name="del" value="0">
  <input type="hidden" name="app" value="0">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
            <td width="47" height="20"><a href="#" onClick="checkAllFn(document.forms[1], 1);"><font color="#005b90"><strong>Disable</strong></font></a></td>
            <td width="143"><a href="<?php print $PHP_SELF . "?sortBy=agentContactPerson".$qryString;?>"><font color="#005b90"><strong>Sub 
              Agent</strong></font></a></td>
            <td width="100"><a href="<?php print $PHP_SELF . "?sortBy=username".$qryString;?>"><font color="#005b90"><strong>Agent Number</strong></font></a></td>
            <td width="201"><a href="<?php print $PHP_SELF . "?sortBy=agentCompany".$qryString;?>"><font color="#005b90"><strong>Company</strong></font></a></td>
			<td width="164"><a href="<?php print $PHP_SELF . "?sortBy=agentCompDirector".$qryString;?>"><font color="#005b90"><strong>Director</strong></font></a></td>
          </tr>
          <?
			for ($i=0; $i<count($agents); $i++){
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
            <td align="center" height="20"> 
              <input type="checkbox" name="userID[<?=$i;?>]" value="<?=$agents[$i]["userID"]; ?>"></td>
            <td><?=stripslashes($agents[$i]["agentContactPerson"]); ?></td>
            <td> 
              <?=$agents[$i]["username"]; ?>
            </td>
            <td> 
              <?=stripslashes($agents[$i]["agentCompany"]); ?>
            </td>
            <td> 
              <?=stripslashes($agents[$i]["agentCompDirector"]); ?>
            </td>
          </tr>
		  <tr bgcolor="#eeeeee">
		  	<td colspan="5" align="right" height="20"><b><? if ($agents[$i]["agentStatus"] != "Disabled"){ ?><a href="javascript:;" onClick=" window.open('disable-agent.php?userID=<?=$agents[$i]["userID"]; ?>&caller=sub','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Disable</b></font></a> | <? } if ($agents[$i]["agentStatus"] != "Active"){ ?><a href="javascript:;" onClick=" window.open('activate-agent.php?userID=<?=$agents[$i]["userID"]; ?>&caller=sub','activateAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Activate</b></font></a> | <? } if ($agents[$i]["agentStatus"] != "Suspended"){ ?><a href="javascript:;" onClick=" window.open('suspend-agent.php?userID=<?=$agents[$i]["userID"]; ?>&caller=sub','suspendAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=200,width=460')"><font color="#005b90"><b>Suspend</b></font></a> | <? } ?><a href="javascript:;" onClick=" window.open('view-agent.php?userID=<?=$agents[$i]["userID"]; ?>&caller=sub','agentDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=400,width=460')"><font color="#005b90"><b>View Details</b></font></a> | <a href="update-sub-agent.php?userID=<?=$agents[$i]["userID"];?>"><font color="#005b90"><b>Update</b></font></a></b>&nbsp;</td>
		  </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="5" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($agents) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($agents));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td colspan="5" align="center"> <input type="submit" value="Disable Sub Agents">
            </td>
          </tr>
          <?
			} else {
		?>
          <tr> 
            <td colspan="5" align="center"> No Sub Agents found in the database. 
            </td>
          </tr>
          <?
			}
		?>
</form>
        </table>
	</td>
  </tr>
</table>
</body>
</html>
