<?php

session_start();
include ("../include/config.php");
include ("security.php");
include ("../ntm/include/functions_ntm.php");

// Get Config Round Level
if(CONFIG_TRANS_ROUND_LEVEL != "")
    $roundLevel = CONFIG_TRANS_ROUND_LEVEL;
else
    $roundLevel = 2;


if(!empty($_GET["fdate"]))
    $fromDate = $_GET["fdate"]." 00:00:00";
else
    $fromDate = date("Y-m-d")." 00:00:00";

if(!empty($_GET["tdate"]))
    $toDate = $_GET["tdate"]." 23:59:59";
else
    $toDate = date("Y-m-d")." 23:59:59";

$strType = $_GET["type"];


$SenCurrency  = $_REQUEST["SenCurrency"];
$Monitioring  = $_REQUEST["Monitioring"];
$SearchType   = $_REQUEST["SearchType"];
$Risk		  = $_REQUEST["Risk"];
$Points	  	  = $_REQUEST["Points"];
$Custtype	  = $_REQUEST["Custtype"];

$showHeader = false;
$outPutData = '';

switch($strType)
{
    case "gridrpt":
        //set content type and xml tag
        header("Content-type:text/xml");
        $outPutData .="<?xml version=\"1.0\"?>";
        $strMainStart = "<rows>";
        $strMainEnd = "</rows>";
        $strSubStart = "<row>";// id='".$resClient[$i]['id']."'>";
        $strSubEnd = "</row>";

        $strStart = "<cell>";
        $strEnd = "</cell>";

        break;

    case "textrpt":
        header("Content-Type: plain/text");
        header("Content-Disposition: Attachment; filename=Suspicious-Transactions.txt");
        header("Pragma: no-cache");

        $strMainStart = "";
        $strMainEnd = "";

        $strSubStart = "";
        $strSubEnd = "\r\n";

        $strStart = "";
        $strEnd = "\t";

        $showHeader = true;

        break;

    case "excelrpt":


        header("Content-type: application/x-msexcel");

        if($_REQUEST["printFile"]=="1")
        {
            header("Content-Disposition: attachment; filename=(PrintThisExcelFile)Suspicious-Transactions.XLS");

        }
        else
        {
            header("Content-Disposition: attachment; filename=Suspicious-Transactions.XLS");
        }
        header("Content-Description: PHP/MYSQL Generated Data");
        header('Content-Type: application/vnd.ms-excel');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        $strMainStart = "<table border='1'>";
        $strMainEnd = "</table>";

        $strSubStart = "<tr>";
        $strSubEnd = "</tr>";

        $strStart = "<td>";
        $strEnd = "</td>";

        $showHeader = true;

        break;


}
$outPutData.= $strMainStart;
/* show header*/
if($showHeader)
{
    $outPutData.= $strSubStart;

    $outPutData.=$strStart."Sr.".$strEnd.$strStart."Customer Type".$strEnd.$strStart."Monitoring Criteria".$strEnd.$strStart."Customer Name".$strEnd.$strStart."Customer Number".$strEnd.$strStart."Search Criteria".$strEnd.$strStart."Aggregated Amount".$strEnd.$strStart."Total Points".$strEnd.$strStart."Risk Type".$strEnd.$strStart."Created On".$strEnd;

    $outPutData.= $strSubEnd;
}
/* start  <rows> */

//up.amount AS Amount,
$strQuery  =" SELECT  
					
					up.creationDate,
					cu.firstName AS fname,
					cu.lastName AS lname,
					cu.customerNumber AS customerNumber,
					up.monitoringCriteria,
					up.orignatingCurrency,
					up.amountCriteria,
					up.points,
					up.riskType,
					up.customerType,
					up.id
			
			from 
					compliance_user_profile AS up,
					transactions as tr
					LEFT JOIN customer AS cu ON tr.customerID = cu.customerID WHERE 1";


if(!empty($fromDate) && !empty($toDate))
{
    $strQuery .= " AND up.creationDate BETWEEN '".$fromDate."' AND '".$toDate."'";
}
if(!empty($SenCurrency) && $SenCurrency !="All")
{
    $strQuery .= " AND up.orignatingCurrency='".$SenCurrency."'";
}
if(!empty($Custtype) && $Custtype !="All")
{
    $strQuery .= " AND up.customerType='".$Custtype."'";
}

if(!empty($Monitioring) && $Monitioring!="All")
{
    $strQuery .= " AND up.monitoringCriteria='".$Monitioring."'";
}

if(!empty($SearchType)  && $SearchType!="All")
{
    $strQuery .= " AND up.	amountCriteria='".$SearchType."'";
}
if(!empty($Points))
{
    $strQuery .= " AND up.points='".$Points."'";
}
if(!empty($Risk) && $Risk!="All")
{
    $strQuery .= " AND up.riskType='".$Risk."'";
}



$strQuery .= "
				    AND (tr.transStatus = 'Suspicious') 
				  
					";

$resClient =  selectMultiRecords($strQuery);

for ($i=0; $i < count($resClient); $i++)
{

    ///pick dist comm from C&E
    $arrParam						= array();
//    $arrParam["Amount"]				= $resClient[$i]["Amount"];
    $arrParam["monitoringCriteria"]	= $resClient[$i]["monitoringCriteria"];
    $arrParam["orignatingCurrency"] = $resClient[$i]["orignatingCurrency"];
    $arrParam["amountCriteria"]		= $resClient[$i]["amountCriteria"];
    $arrParam["points"]				= $resClient[$i]["points"];
    $arrParam["fname"]				= $resClient[$i]["fname"];
    $arrParam["lname"]				= $resClient[$i]["lname"];
    $arrParam["customerNumber"]		= $resClient[$i]["customerNumber"];
    $arrParam["riskType"]			= $resClient[$i]["riskType"];
    $arrParam["customerType"]		= $resClient[$i]["customerType"];
    $arrParam["creationDate"]			= $resClient[$i]["creationDate"];


    $outPutData.= $strSubStart;
    $outPutData.= $strStart.($i+1).$strEnd;
    // Trans date
    $outPutData.= $strStart.$arrParam["customerType"].$strEnd;
    $outPutData.= $strStart.$arrParam["monitoringCriteria"].$strEnd;
    $outPutData.= $strStart.$arrParam["fname"]." ".$arrParam["lname"].$strEnd;
    $outPutData.= $strStart.$arrParam["customerNumber"].$strEnd;
    $outPutData.= $strStart.$arrParam["amountCriteria"].$strEnd;
    $outPutData.= $strStart.number_format($arrParam["Amount"],$roundLevel,'.',',').$strEnd;
    $outPutData.= $strStart.$arrParam["points"].$strEnd;
    $outPutData.= $strStart.$arrParam["riskType"].$strEnd;
    $outPutData.= $strStart.$arrParam["creationDate"].$strEnd;



    /* start  </row>  */
    $outPutData .= $strSubEnd;



}
// end for loop

if($showHeader)
{

    //$outPutData.= $strSubStart;
//		$outPutData.= $strStart."Total".$strEnd;	// Trans date
//		$outPutData.= $strStart."".$strEnd;	// API date
//		$outPutData.= $strStart."".$strEnd;
//		$outPutData.= $strStart."".$strEnd;
//		$outPutData.= $strStart."".$strEnd;
//
//
//		$outPutData.= $strStart.number_format($TapiComm,$roundLevel,'.',',').$strEnd;
//		$outPutData.= $strStart.number_format($TdistComm,$roundLevel,'.',',').$strEnd;
//		$outPutData.= $strStart.number_format($TComComm,$roundLevel,'.',',').$strEnd;
//		$outPutData.= $strStart.number_format($TdistComm,$roundLevel,'.',',').$strEnd;






    $outPutData.= $strSubEnd;
}

$outPutData.= $strMainEnd;

echo trim($outPutData);

?>