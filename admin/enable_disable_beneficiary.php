<?php
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	
	if(defined("CONFIG_DISPLAY_DATE_FORMAT") && CONFIG_DISPLAY_DATE_FORMAT !="0" && is_numeric(CONFIG_DISPLAY_DATE_FORMAT))
	{
		$dfFlag = CONFIG_DISPLAY_DATE_FORMAT;
	} else {
		
		$dfFlag="2";
	}
	
	$userID  = $_SESSION["loggedUserData"]["userID"];
	if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
		 $entity_country =getLoggedUserEntity($userID);
		 
	$agentType 	= getAgentType();
	$parentID 	= $_SESSION["loggedUserData"]["userID"];
	$user		= $agentType;
	
	if ($offset == "")
		$offset = 0;
	$limit=50;
	if ($_REQUEST["newOffset"] != "") {
		$offset = $_REQUEST["newOffset"];
	}
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	$page="comp";
	
	if(CONFIG_SEARCH_RESULT_ORDER == 1){
		$Order = "ASC";
	}elseif(CONFIG_SEARCH_RESULT_ORDER == 0){
		$Order = "DESC";
		}
		
	$strQueryBenSelect	= "	benID,
							created,
							Title,
							firstName,
							middleName,
							lastName,
							Address,
							Country,
							Phone,
							Mobile,
							email,
							status
						 ";
	$query=selectMultiRecords("SELECT customerID FROM customer WHERE agentID=$userID");
	$customerIDs="-1";
	foreach ($query as $row){
		$customerIDs.=",'".$row["customerID"]."'";
	}
	$strQueryBenWhere	= "AND customerID IN ($customerIDs)";
	$queryBenOrderBy	= " ORDER BY firstname $Order, middleName $Order, lastName $Order";
	$strQueryBen		= "";
	$strQueryBenCnt		= "";
	$arrBenData			= array();
	$intAllCount		= 0;
	
	$strBenName			= "";
	$intContactNumber	= "";
	$SearchBeneficiary	= "";
	$charecterFalge		= true;
	
	$benID				= $_REQUEST["benID"];
	$strBenName			= trim($_REQUEST["benName"]);
	$intContactNumber	= trim($_REQUEST["contactNumber"]);
	$searchBeneficiary	= $_REQUEST["benSearch"];
	
	$needle		= "%";
	$intLen1	= strlen(strstr($strBenName,$needle));
	$intLen2	= strlen(strstr($intContactNumber,$needle));

	if($intLen1 > 0 || $intLen2 > 0)
	{
		$charecterFalge = false; 
	}
		
	if($charecterFalge)
	{
		$strQueryBenWhere	.= " AND (firstName LIKE '%".mysql_real_escape_string($strBenName)."%' OR middleName LIKE '%".mysql_real_escape_string($strBenName)."%' OR lastName like '%".mysql_real_escape_string($strBenName)."%' OR beneficiaryName like '%".mysql_real_escape_string($strBenName)."%') 
								 AND (Phone LIKE '%".mysql_real_escape_string($intContactNumber)."%' OR Mobile LIKE'%".mysql_real_escape_string($intContactNumber)."%')";	
		// Fitch Beneficiaries Data Query
		$strQueryBen	= " SELECT 
								" .$strQueryBenSelect. "
							FROM
								" .TBL_BENEFICIARY. "
							WHERE 1
								" .$strQueryBenWhere. "
						 ";
						 
		$strQueryBen	.= $queryBenOrderBy;
		
		// Fitch Total Number of Beneficiaries Query
		$strQueryBenCnt	= " SELECT
								COUNT(benID)
							FROM 
								" .TBL_BENEFICIARY. "
							WHERE 1
								" .$strQueryBenWhere. "
							";
		
		$intAllCount	= countRecords($strQueryBenCnt);
		$strQueryBen 	.= " LIMIT $offset , $limit";
		$arrBenData 	= selectMultiRecords($strQueryBen);
	}
	
?>
<html>
<head>
<title>Enable Disable Beneficiary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<script>
function checkForm(theForm) {
	if(theForm.benName.value == "" || IsAllSpaces(theForm.benName.value))
	{
		if(theForm.contactNumber.value == "" || IsAllSpaces(theForm.contactNumber.value))
		{
    		alert("Please provide beneficiary name OR Contact Number OR both to Search.");
        	theForm.benName.focus();
        	return false;
		}
		return true;
    }
	return true;
}
function IsAllSpaces(myStr){
	while (myStr.substring(0,1) == " "){
			myStr = myStr.substring(1, myStr.length);
	}
	if (myStr == ""){
			return true;
	}
	return false;
}
</script>

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
    <tr>
        <td class="topbar">Enable Disable Beneficiary List </td>
    </tr>
    <tr>
        <td align="center"><br>
            <table border="1" cellpadding="5" bordercolor="#666666">
                <form method="post" id="searchForm" name="Search" action="enable_disable_beneficiary.php" onSubmit="return checkForm(this);">
                    <? if ($_GET["msg"] != ""){ ?>
                    <tr bgcolor="#ededed">
                        <td colspan="2" bgcolor="#EEEEEE">
							<table width="100%" cellpadding="5" cellspacing="0" border="0">
                                <tr>
                                    <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                                    <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?></td>
                                </tr>
                            </table>
						</td>
                    </tr>
                    <? } ?>
                    <tr>
                        <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Beneficiary </strong></span></td>
                    </tr>
                    <tr>
                        <td nowrap>
                            Beneficiary Name
                            <input type="text" name="benName" id="benName" value="<? echo $strBenName?>">
                            Contact Number
                            <input type="text" name="contactNumber" id="contactNumber" value="<? echo $intContactNumber?>">
                            <input type="submit" name="benSearch" value="Search"></td>
                    </tr>
                </form>
            </table>
            <br>
            <br>
            <br>
			<?php
			if(count($arrBenData) > 0)
			{
				$queryString	= " &benName=".$strBenName."&intContactNumber=".$intContactNumber."&benSearch=Search";
			?>
            <table width="1000" border="1" cellpadding="0" bordercolor="#666666">
                <form action="enable_disable_beneficiary.php" method="post" name="trans">
                    <tr>
                        <td height="25" nowrap bgcolor="333333">
							<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                <tr>
                                    <td><?php if (count($arrBenData) > 0) {;?>
                                        Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($arrBenData));?></b> of
                                        <?=$intAllCount; ?>
                                        <?php } ;?>
                                    </td>
                                    <?php 
									if ($prv >= 0) { ?>
                                    <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0".$queryString;?>"><font color="#005b90">First</font></a> </td>
                                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv".$queryString;?>"><font color="#005b90">Previous</font></a> </td>
                                    <?php 
									} ?>
                                    <?php 
									if ( ($nxt > 0) && ($nxt < $intAllCount) ) {
										$alloffset = (ceil($intAllCount / $limit) - 1) * $limit;
									?>
                                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt".$queryString;?>"><font color="#005b90">Next</font></a>&nbsp; </td>
                                    <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset".$queryString;?>"><font color="#005b90">Last</font></a>&nbsp; </td>
                                    <?php } ?>
                                </tr>
                            </table>
						</td>
                    </tr>
                    <tr>
                        <td nowrap bgcolor="#EFEFEF">
							<table width="1000" border="0" bordercolor="#EFEFEF">
                                <tr bgcolor="#FFFFFF">
                                    <th width="120"><span class="style1">Beneficiary Name </span></th>
                                    <th width="100"><span class="style1">Creation Date</span></th>
                                    <th width="120"><span class="style1">Address</span></th>
                                    <th width="100"><span class="style1">Country</span></th>
									<th width="100"><span class="style1">Phone</span></th>
									<th width="100"><span class="style1">Mobile</span></th>
                                    <th width="100"><span class="style1">Email</span></th>
                                    <th width="80"><span class="style1">Edit</span></th>
                                    <th width="80"><span class="style1">Enable/Disable</span></th>
                                    <th width="100"><span class="style1">Total Destination Amount</span></th>
                                </tr>
								<?php
								foreach($arrBenData as $key=>$val)
								{
									$strBenStatus	= "";
									$fltDestAmount	= 0;
									$strFullName	= $val["Title"]." ".$val["firstName"]." ".$val["middleName"]." ".$val["lastName"];
									
									if($val["status"] == "Enabled")
									{
										$strBenStatus 	= "Disable";
										//$action			= "D";
									}
									elseif($val["status"] == "Disabled")
									{
										$strBenStatus 	= "Enable";
										//$action			= "D";
									}
									if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
										$qr="SELECT SUM(receiving_amount) AS localAmount FROM tt_transactions WHERE ben_Id='".$val["benID"]."'  GROUP BY ben_Id ";
									else
										$qr="SELECT SUM(localAmount) AS localAmount FROM ".TBL_TRANSACTIONS." WHERE benID='".$val["benID"]."'  GROUP BY benID ";
									
									$arrDestAmount = selectFrom($qr);
									$fltDestAmount = number_format($arrDestAmount["localAmount"],2,'.',',');
									if($fltDestAmount == 0)
										$fltDestAmount = "--";
								?>
                                <tr bgcolor="#FFFFFF">
                                    <td><?php echo $strFullName;?></td>
                                    <td><?php echo dateFormat($val["created"], $dfFlag);?></td>
									<td><?php echo $val["Address"]?></td>
									<td><?php echo $val["Country"]?></td>
									<td><?php echo $val["Phone"]?></td>
									<td><?php echo $val["Mobile"]?></td>
                                    <td><?php echo $val["email"]?></td>
                                    <td align="center"><a href="javascript:;" class="style3" onClick=" window.open('add-beneficiary.php?from=enableDisableBen&benID=<?=$val["benID"]?>&transID=<?=$_GET["transID"]?>&benName=<?=$strBenName;?>&contactNumber=<?=$intContactNumber;?>&newOffset=<?=$offset;?>&benSearch=<?=$searchBeneficiary?>',null,'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">Edit</a></td>
									<td align="center"><a href="javascript:;" class="style3" onClick=" window.open('disable_beneficiary.php?benID=<?=$val["benID"]?>&fullName=<?=$strFullName?>&benStatus=<?=$val["status"]?>&contactNumber=<?=$intContactNumber?>&benSearch=<?=$searchBeneficiary?>&benName=<?=$strBenName;?>&offset=<?=$offset?>',null,'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><?php echo $strBenStatus; ?></a></td>
									<td align="center"><? echo $fltDestAmount ?></td>
                                </tr>
								<?php
								}
								?>
                                <tr bgcolor="#FFFFFF">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
									<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
									<td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
							 </table>
						</td>
                    </tr>
                </form>
            </table>
			<?php
			} // greater than zero
			else
				echo "No data found."
			?>
		</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</body>
</html>
