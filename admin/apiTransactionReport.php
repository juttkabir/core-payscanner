<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>API Transactions Summary from <?=CONFIG_API_TRANSACTIONS?></title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	
	jQuery(document).ready(function(){
		jQuery("#summaryList").jqGrid({
			url:'apiTransactionReportAjax.php?vi=gr&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Date',
				'Stage', 
				'Files Count',
				'Transactions Count', 
				'Parsed Transactions',
				'Rejected Transactions'
			],
			colModel:[
				{name:'datetime',index:'datetime', width:110, sorttype:'date', datefmt:'d-m-Y'},
				{name:'interaction_step',index:'interaction_step', width:120, align:"right"},
				{name:'files',index:'files', width:115, align:"right"},
				{name:'total_transactions',index:'total_transactions', width:110, align:"right"},
				{name:'parsed',index:'parsed', width:110, align:"right"},
				{name:'rejected',index:'rejected', width:110, align:'left'}
			],
			rowNum: <?=CONFIG_MAX_TRANSACTIONS?>,
			rowList: [10,20,50,100,150,200,250,300],
			imgpath: gridimgpath,
			pager: $('#pager'),
			sortname: 'datetime',
			viewrecords: true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			caption: "API Transactions Summary from <?=CONFIG_API_TRANSACTIONS?>",
			onSelectRow:function(id){
				if(id)
					displayDetails(id);
			}
		});
		
		jQuery('a').cluetip({splitTitle: '|'});
	});
	
	var extraParams = "";
	
	function gridReload()
	{

		var l_fDay = $("#fDay").val();
		var l_fMonth = $("#fMonth").val();
		var l_fYear = $("#fYear").val();
		var l_tDay = $("#tDay").val();
		var l_tMonth = $("#tMonth").val();
		var l_tYear = $("#tYear").val();
		
		var theUrl = "apiTransactionReportAjax.php?vi=gr&fDay="+l_fDay+"&fMonth="+l_fMonth+"&fYear="+l_fYear+"&tDay="+l_tDay+"&tMonth="+l_tMonth+"&tYear="+l_tYear+""
					+"&Submit=Search";
		
		if(extraParams != "")
		{
			theUrl = theUrl + extraParams;
			extraParams = "";
		}
		
		jQuery("#summaryList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
		
	}
	
	function displayDetails(id)
	{
		//alert(id);
		$("#detailRow").load("apiTransactionReportAjax.php?vi=dt&id="+id);
	}
	
</script>
</head>
<body>
<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0" id="boundingBox">
    <tr>
        <td class="pageTitle">API Transaction Summary from <?=CONFIG_API_TRANSACTIONS?></td>
    </tr>
    <tr>
        <td>
			<table width="100%" border="0" cellpadding="5" bordercolor="#666666">
                <tr>
                    <td align="center" width="80%" valign="top">
						From &nbsp;
                        <? 
							$month = date("m");
							$day = date("d");
							$year = date("Y");
						?>
                        <select name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                        <?
						for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
								$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
						?>
                        </select>
                        <select name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                            <option value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</option>
                            <option value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</option>
                            <option value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</option>
                            <option value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</option>
                            <option value="05" <? echo ($month =="05" ? "selected" : "")?>>May</option>
                            <option value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</option>
                            <option value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</option>
                            <option value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</option>
                            <option value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</option>
                            <option value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</option>
                            <option value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</option>
                            <option value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</option>
                        </select>
                        <select name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                        <?
							$cYear=date("Y");
							for ($Year=2004;$Year<=$cYear;$Year++)
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
					  	?>
                        </select>
                        <br /><br />
						To
						&nbsp;
                        <select name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                         <?
							for ($Day=1;$Day<32;$Day++)
							{
									if ($Day<10)
										$Day="0".$Day;
									echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
							}
						  ?>
                        </select>
                        <select name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                            <option value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</option>
                            <option value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</option>
                            <option value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</option>
                            <option value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</option>
                            <option value="05" <? echo ($month =="05" ? "selected" : "")?>>May</option>
                            <option value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</option>
                            <option value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</option>
                            <option value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</option>
                            <option value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</option>
                            <option value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</option>
                            <option value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</option>
                            <option value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</option>
                        </select>
                        <select name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
						<?
							$cYear=date("Y");
							for ($Year=2004;$Year<=$cYear;$Year++)
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
						?>
                        </select>
						<br /><br />
						<input type="button" id="reloadGridBtn" value="Search" onclick="gridReload();" />
					</td>
                </tr>
           </table>
		</td>
    </tr>
    <tr>
        <td align="center">
			<table id="summaryList" class="scroll" cellpadding="0" cellspacing="0"></table>
			<div id="pager" class="scroll" style="text-align:center;"></div>
		</td>
    </tr>
	<tr>
		<td align="center" id="detailRow">&nbsp;</td>
	</tr>
</table>
</body>
</html>