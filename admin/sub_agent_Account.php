<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
include ("javaScript.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

$currentDate= date('Y-m-d',strtotime($date_time));
/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];

	$fromDatevalue=$_POST["fromDate"];
	$toDatevalue=$_POST["toDate"];
	if(!empty($fromDatevalue)){
		$fromDatevalue = str_replace('/', '-', $fromDatevalue);
		$fromDate= date("Y-m-d",strtotime($fromDatevalue));
	}
	else
		$fromDate=$currentDate;
	if(!empty($toDatevalue)){
		$toDatevalue = str_replace('/', '-', $toDatevalue);
		$toDate= date("Y-m-d",strtotime($toDatevalue));
	}
	else
		$toDate=$currentDate;
		
		   
  $fromDateReport=date("m-d-Y",strtotime($fromDate));
  $toDateReport=date("m-d-Y",strtotime($toDate));
if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
	

	
	/*
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];*/
	
}

if($_POST["agentID"]!="") {
	$_SESSION["agentID2"] =$_POST["agentID"];
}elseif($_GET["agentID"]!="") {
	$_SESSION["agentID2"]=$_GET["agentID"];
}else{ 
	$_SESSION["agentID2"] = "";
}
		
	
if ($offset == "") {
	$offset = 0;
}
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

?>
<html>
<head>
<title>Sub Agent Account</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();	
		$("#printReport").hide();
		$(".actionField").hide();
		print();
		$("#searchTable").show();	
		$("#printReport").show();
		$(".actionField").show();
	});
});

var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	  <tr>
    <td class="topbar">Sub Agent Account Statement</td>
  </tr>
	<tr>
      <td width="563">
	<form action="sub_agent_Account.php" method="post" name="Search" id="searchTable">
  		<div align="center"> From Date 
                  	<input name="fromDate" type="text" id="from" value="<?=$fromDateReport?>" readonly >
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date From|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0> </a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="toDate" type="text" id="to" value="<?=$toDateReport?>" readonly >
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;" title="Select Date To|Please select date from calendar"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->		    		    
              
               
                <br>
                &nbsp;&nbsp; 
                <? if ($agentType!= 'SUBA'){ ?>
                <select name="agentID" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select Sub Agent-</option>
                  <?
	//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){				  
                  $agentQuery  = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='sub' and isCorrespondent = 'N' AND agentStatus = 'Active' ";
				}
	else if (CONFIG_AGENT_WITH_ALL_STATUS==1){	
				  $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='sub' and isCorrespondent = 'N' ";
				}
								if ($agentType == 'SUPA') {
									$agentQuery .= " AND parentID = '".$changedBy."' ";
								} 
								/*else if ($agentType == 'SUBAI') {
									$agentQuery .= " AND userID = '".$changedBy."' ";
								}*/
								$agentQuery .= " order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                  <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </select>
                <script language="JavaScript">
                  	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                </script>
                 <? } ?>
                <input type="submit" name="Submit" value="Search">
                
              </div>
        
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search" || $agentType == 'SUBA')
	 {
	 	$Balance="";
		if($_GET["search"]=="search") {
			$slctID=$_GET["agentID"];
		} else {
			$slctID=$_POST["agentID"];	
		}
		
		if ($agentType == 'SUBA'){ 	
			  $slctID = $changedBy; 
		   }
			//$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			//$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			 
		if ($slctID !=""){
			$accountQuery = "Select * from sub_agent_account where agentID = $slctID";
			  $accountQuery .= " and $queryDate";				
				$accountQuery .= " and status != 'Provisional' ";
				
			$accountQueryCnt = "Select count(*) from sub_agent_account where agentID = $slctID";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
			
			
			$allCount = countRecords($accountQueryCnt);
			$accountQuery .= "order by aaID";
			$accountQuery .= " LIMIT $offset , $limit"; 
			$contentsAcc = selectMultiRecords($accountQuery);
		}
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
				<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
			<tr>
			<td>&nbsp;
		
				<table>
					<tr>
						<td width="300">
							Cumulative Total <? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate);  
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>
			</td>
			<td>&nbsp;
			</td>
			</tr>
			</table>
		
			</td>
		</tr>
			<?
		 }
		?>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <td align="left" ><span class="style1">Modified By</span></td>
					  <? if(CONFIG_AGENT_STATEMENT_BALANCE == '1'){?>
					  <td align="left" ><span class="style1">Opening Balance</span></td>
					  <? }?>
					  <td align="left" ><span class="style1">Money In</span></td>
					  <td align="left" ><span class="style1">Money Out</span></td>
						<td align="left" ><span class="style1"><? echo $systemCode; ?></span></td>
						<td align="left" ><span class="style1">Description</span></td>
						<? if(CONFIG_AGENT_STATEMENT_BALANCE == '1'){?>
					  <td align="left" ><span class="style1">Closing Balance</span></td>
					  <? }?>
				</tr>
				<? 
				
				$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					
					///////////////////Opening and Closing////////////////
					
						if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
						{
							
							 $currDate = $contentsAcc[$i]["dated"];
							//echo "--predate--".$preDate;
							if($currDate != $preDate)
							{
							
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_SUB_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
								//echo "select  Max(dated) as datedFrom from " . TBL_SUB_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'";
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_SUB_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
									$account1 .= " and  user_id = '".$slctID."' ";								
									$contents1 = selectFrom($account1);
									if($currDate == $FirstDated)
										$openingBalance =  $contents1["opening_balance"];
									else
										$openingBalance =  $contents1["closing_balance"];
								}
								
						     $closingBalance = $openingBalance;
								 $preDate = $currDate;
								 
							$_SESSION["openingBalance"] = $openingBalance;

							}
								
						}
						$_SESSION["currDate"] = $currDate;
					/////////////////////////Opening and Closing///////////
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td align="left"><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					
					<? if(CONFIG_AGENT_STATEMENT_BALANCE == '1'){?>
					    <td><? echo number_format($openingBalance,2,'.',','); ?></td> 
					  
					  <? } ?>
					
					<td align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo $contentsAcc[$i]["amount"];
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?></td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM, refNumber FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				<td>
					<? echo $contentsAcc[$i]["description"]; ?>
				</td>
				<? if(CONFIG_AGENT_STATEMENT_BALANCE == '1'){
				    
				      if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
				   		 
			    	 $closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
					
					}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
						
					}
					?>
					    <td><? echo number_format($closingBalance,2,'.',','); ?></td> 
					  
					  <? } ?>
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
		<?		
		if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
		{
		?>
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); ?></td>
				  	<td>&nbsp;</td>
						<!-- <td>&nbsp;</td>-->
						
				</tr>			
				<tr> 
					  <td class="style3"> Running Total:</td>
					  <td> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					$rgrandTotal = number_format($_SESSION["grandTotal"],2,'.',',');
					echo $rgrandTotal;
					?>
					  </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>
                </tr>
                <?
            }
            ?>
            <tr bgcolor="#FFFFFF">
              <td colspan="10">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
            <tr>
			  <td height="25" nowrap bgcolor="#6699CC" colspan="10">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
            
            
            
            
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Sub A&D </font>
				</td>
			</tr>
<?
}

$cond = false;

if ($slctID != '') {
	if ($agentType == 'admin') {
		$cond = true;	
	} else if ($agentType == 'SUPA') {
		$qPID = "SELECT `parentID` from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$slctID."' AND `parentID` = '".$changedBy."'";
		if (isExist($qPID)) {
			$cond = true;	
		}
	}
}

	if ($cond) {
?>
			
				
			<tr>
		          <td height="15" bgcolor="#c0c0c0"><span class="child"><a HREF="add_sub_agent_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit/Withdraw 
                    Money</font></a></span> </td>
			 
  	</tr>
<?	}  ?>
       </table>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if ($slctID != '' && count($contentsAcc) > 0){?>
		<tr name="actionField" id="actionField">
		  	<td align="center"> 
				<div class="noPrint">
					<input type="button" id="printReport" name="Submit2" value="Print This Report">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>