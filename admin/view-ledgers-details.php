<?php
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

	if(!empty($_REQUEST["id"]))
	{
	$strSql = "select 
				   id,
				   userID,
				   transID,
				   crAmount,
				   drAmount,
				   crAccount,
				   drAccount,
				   crCurrency,
				   drCurrency, 	
				   description,
				   status,
				   note,
				   created 
		
				from
					 account_ledgers
				where
					id = '".$_REQUEST["id"]."'
					";
	//debug($strSql);
	$result = SelectMultiRecords($strSql);

	//activities($_SESSION["loginHistoryID"],"VIEW",0,"cheque_order","Cheque Account Ledger Details Viewed");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ledger Detail View</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>

<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader"><?=SYSTEM?> (Cash Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y")?><br />
					<br />
					Detailed Report for <?=$arrCreatorDetail["username"]?>
					</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<a href="view-ledgers.php?userId=<?=$_REQUEST["userId"]?>&date_from=<?=$_REQUEST["df"]?>&date_to=<?=$_REQUEST["dt"]?>"><b>Go Back</b></a>
					</td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="5%" class="columnTitle">Sr.#</td>
					<td width="15%" class="columnTitle">&nbsp;User</td>
					<td width="15%" class="columnTitle">&nbsp;Transaction</td>
					<td width="50%" class="columnTitle">&nbsp;Description</td>
					<td width="30%" class="columnTitle">&nbsp;Amount Dr.</td>
					<td width="30%" class="columnTitle">&nbsp;Amount Cr.</td>
					<td width="30%" class="columnTitle">&nbsp;Account Dr.</td>
					<td width="30%" class="columnTitle">&nbsp;Account Cr.</td>
					<td width="10%" class="columnTitle">&nbsp;Currency Dr.</td>
					<td width="10%" class="columnTitle">&nbsp;Currency Cr.</td>
				</tr>
				<?
					//$i=1;
					$totalPaidAmount = 0;
					foreach($result as $k => $v)
					{
						//$totalPaidAmount += $v["paid_amount"];
						 $sqlUser = selectFrom("SELECT username From ".TBL_ADMIN_USERS." WHERE userID = '".$v["userID"]."'");
		                 $username = $sqlUser["username"];
						
						 $sqlTrans = selectFrom("SELECT refNumberIM From ".TBL_TRANSACTIONS." WHERE transID = '".$v["transID"]."'");
		                 $refNumberIM = $sqlTrans["refNumberIM"];
				?>
				<tr>
					<td><?=$k+1?></td>
					<td><?=$username?></td>
					<td><?=$refNumberIM?></td>
					<td><?=$v["description"]?></td>
					<td><?=$v["drAmount"]?></td>
					<td><?=$v["crAccount"]?></td>
					<td><?=$v["drAccount"]?></td>
					<td><?=$v["crAccount"]?></td>
					<td><?=$v["drCurrency"]?></td>
					<td><?=$v["crCurrency"]?></td>
				</tr>
				<?
					//$i++;
					}
				?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
