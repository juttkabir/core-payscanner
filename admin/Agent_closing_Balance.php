<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];



if ($offset == "")
	$offset = 0;
$limit=10;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

$contentsAcc = SelectMultiRecords("select userID, username, name, agentStatus from ".TBL_ADMIN_USERS." where adminType = 'Agent' and agentType = 'Sub' and isCorrespondent != 'Y' and parentID = '".$_SESSION["loggedUserData"]["userID"]."'");
		 		$cumulativeBalance = 0;
		 		

?>
<html>
<head>
	<title>Closing Balances</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">	
	 <tr>
    <td class="topbar">Agent Closing Balance</td>
  </tr>
 </table>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="Agent_closing_Balance.php" method="post" name="Search">
  
  	<tr>
		<td>
		
     		
        <table width="715" height="284" border="1" align="center" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC"> 
			<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
                <tr> 
                  
                  <?php if ($prv >= 0) { ?>
                  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&agentID=".$_SESSION["abank"]."&search=search&total=first";?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="77" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&agentID=".$_SESSION["abank"]."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> 
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="61" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&agentID=".$_SESSION["abank"]."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
      	<tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="left"><a class="style2" href="main.php">Go Back</a></td>
        </tr>  
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong>Agent Closing Balance</strong></span></td>
        </tr>                      
        
          <?
		if(count($contentsAcc) > 0)
		{
		?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> 
				<table border="0" bordercolor="#EFEFEF">
				        <tr bgcolor="#FFFFFF"> 
                  <td width="166"><span class="style1">Agent Name</span></td>
                  <td width="175"><span class="style1">Balance Amount</span></td>
                  <td width="446"><span class="style1">Agent Status</span></td>
                </tr>
                <? 
                for($i=0;$i < count($contentsAcc);$i++)
								{
									$closingSubBalance = selectFrom("select closing_balance, currency from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id = '".$contentsAcc[$i]["userID"]."' and dated =(select  Max(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id = '".$contentsAcc[$i]["userID"]."')");
						 			$cumulativeBalance += $closingSubBalance["closing_balance"];
								?>
                <tr bgcolor="#FFFFFF"> 
                	<td><?	echo($contentsAcc[$i]["name"]."[".$contentsAcc[$i]["username"]."]"); ?> </td>
                  <td><?  echo($closingSubBalance["closing_balance"]." ".$closingSubBalance["currency"]);	?> </td>
                  <td><? echo($contentsAcc[$i]["agentStatus"]); ?> </td>
                </tr>
                <?
								}
								?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td><b>Total Balance</b></td>
                  <td><? echo number_format($cumulativeBalance,2,'.',','); ?></td>
                  <td>&nbsp;</td>
                </tr>
                
              </table>
              <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No Data to Selected </font>
				</td>
			</tr>
          
          <?
			}
			?>
          
		  <tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
        </table>
	   
	
	 </td>
	</tr>
	</form>
</table>
</body>
</html>