<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
//debug($_REQUEST);
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$userName  = $_SESSION["loggedUserData"]["name"];

	$strMainCaption = "Currency Stock list"; 
	$currentDate    = (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$cmd = $_REQUEST["cmd"];
	$activitiesMsg ="";			
	$strExtraUpdateClause	= "";
	$strExtraAddField 		= "";
	$strExtraAddValue 		= "";
	if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1"){
		$strExtraUpdateClause .= ", actualAmount='".$UpActualAmnt."' ";
		$strExtraAddField .= ", actualAmount ";
		$strExtraAddValue .= ", '".$_REQUEST["amount"]."'";
	}

	$bolDataSaved = false;
	if(!empty($cmd))
	{
		/* Request to store or update the data of company */
		if($cmd == "ADD" || $cmd == "UPDATE")
		{//debug($cmd);
			/** 
			 * If the Id is empty than the record is new one
			 */
			$strAddedBy = $userName."|".$userID;
			$arrCurrenciesSymbols = array("GBP"=>"&pound","USD"=>"$","AUD"=>"$","PKR"=>"P Rs","YEN"=>"�","EUR"=>"&euro","CNY"=>"�","PLN"=>"zl");
			
			if($_REQUEST["currency"]!=""){
				$currencySymbolUsed = $arrCurrenciesSymbols[strtoupper($_REQUEST["currency"])];
				$arrCurrency = selectFrom("SELECT id,currency,amount ".$strExtraAddField." from ".TBL_CURRENCY_STOCK_LIST." WHERE upper(currency) = '".strtoupper($_REQUEST["currency"])."'");
				$currencyUsed = $arrCurrency["currency"];
				$amountUsed = $arrCurrency["amount"];
				$actAmount	= $arrCurrency["actualAmount"];
				//debug($currencyUsed);
				//debug($_REQUEST["idVal"]);
				if(($currencyUsed=="") && ($_REQUEST["idVal"]=="")){
					$activitiesMsg = "New Currency is Added Successfully!"; //Command Executed Successfully!
					$strInsertSql = "insert into ".TBL_CURRENCY_STOCK_LIST." 
										(currency, currencySymbol, amount, addedBy, updatedBy, created_on, updated_on ".$strExtraAddField.")
									 values
										('".strtoupper($_REQUEST["currency"])."','".$currencySymbolUsed."','".$_REQUEST["amount"]."','".$strAddedBy."','".$strAddedBy."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."' ".$strExtraAddValue.")";
					insertInto($strInsertSql);
					$lastIntertID = mysql_insert_id();
					$bolDataSaved = true;
					activities($_SESSION["loginHistoryID"],"INSERT",0,TBL_CURRENCY_STOCK_LIST,"New Currency Stock Added");
					
					if((CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1")&& ($currencyUsed==""))
						insertDataInHistory();
						
			}
			if(($currencyUsed!="") && ($_REQUEST["idVal"]!="")){
					$activitiesMsg = "Currency Stock Updated Successfully!";
					$totalAmount = $_REQUEST["amount"]+$amountUsed;
					$UpActualAmnt= $_REQUEST["amount"]+$actAmount;
					$strUpdateSql = "UPDATE ".TBL_CURRENCY_STOCK_LIST." 
										SET amount = '".$totalAmount."',
										updatedBy = '".$strAddedBy."', 
										updated_on = '".date("Y-m-d H:i:s")."' 
										".$strExtraUpdateClause." 										
										WHERE id = '".$_REQUEST["idVal"]."'
										";
					//debug($strUpdateSql,true);
					update($strUpdateSql);
					$bolDataSaved = true;
					activities($_SESSION["loginHistoryID"],"UPDATE",0,TBL_CURRENCY_STOCK_LIST,"Currency Stock Updated");
					if((CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1")&& ($_REQUEST["idVal"]!=""))
						insertDataInHistory();
						
				}
				if(($currencyUsed!="") && ($_REQUEST["idVal"]=="")){
				$activitiesMsg = "Currency Stock is already in List Please updated it from List!";
				$bolDataSaved = true;
				}
				
			}
			$cmd = "";
			$idVal = "";
			$amount = "";
			$currency = "";
			
		}
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["idVal"]))
			{
				$strGetStockDataSql = "select * from ".TBL_CURRENCY_STOCK_LIST." where id='".$_REQUEST["idVal"]."'";
				$arrStockData = selectFrom($strGetStockDataSql);
				
				$strIdVal = $arrStockData["id"];
				$strEditCurrency = $arrStockData["currency"];
				$strEditCurrencySymbol = $arrStockData["currencySymbol"];
				$strEditAmount = number_format($arrStockData["amount"],2,".","");
				
				if($amount_upto == "0.00")
					$amount_upto = "";
				
				$cmd = "UPDATE";
				
				activities($_SESSION["loginHistoryID"],"VIEW",$_REQUEST["idVal"],"cheque_order_fee","Cheque Order Fee Viewed");
			}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["idVal"]))
			{
				$strDelSql = "delete from ".TBL_CURRENCY_STOCK_LIST." where id='".$_REQUEST["idVal"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				activities($_SESSION["loginHistoryID"],"DELETE",$_REQUEST["idVal"],TBL_CURRENCY_STOCK_LIST,"Currency Stock Deleted");
			}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}
	
	function insertDataInHistory(){
	
					//new stock added, also will insert into currency_stock_history to maintain currency stock history
					// added by Kalim @9363: AMB
					$strCurrencyName	=	strtoupper($_REQUEST["currency"]);
					$strCurrencySql		=	"select cID from currencies where currencyName ='".$strCurrencyName."'";
					//debug($strCurrencySql);
					$buying_rate_arr_id = 	selectFrom($strCurrencySql); 
					//debug($buying_rate_arr_id);
					$strCurrencyRateSql	=	"select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
					//debug($strCurrencyRateSql);
					$intBuyingRate 		= 	selectFrom($strCurrencyRateSql); 
					$fltBuyingRate		=	number_format($intBuyingRate[0],4,'.','');
					//debug($fltBuyingRate);
					
					$intLastBatchId	= selectFrom("SELECT id,batch_id FROM ".TBL_CURRENCY_STOCK_HISTORY." WHERE 1 ORDER BY id DESC");
					$batchIdArr		= explode("-",$intLastBatchId['batch_id']);
					$intNextBatchId = $batchIdArr[1] + 1;
					$strBatchId		= "B-".$intNextBatchId; 
					 
					$strInsertSql_2 	= 	"INSERT INTO 
												".TBL_CURRENCY_STOCK_HISTORY."  
												(	batch_id,
													currency,
													amount,
													available_amount, 
													buying_rate, 
													purshased_from, 
													status, 
													created_date
												)
											VALUES
												(	'".$strBatchId."',
													'".$buying_rate_arr_id["cID"]."',
													'".$_REQUEST["amount"]."',
													'".$_REQUEST["amount"]."',
													'".$fltBuyingRate."',
													'',
													'Open',
													'".date("Y-m-d")."'
												)";
					
					//debug($strInsertSql_2);
					insertInto($strInsertSql_2);
					$lastInsertionID = @mysql_insert_id();
				
	}
	//$arrCurrenciesRows = selectMultiRecords("select DISTINCT(currencyName) from ".TBL_CURRENCY." order by currencyName");
	
	$arrCurrenciesRows = SelectMultiRecords("select currencyName 
											from ".TBL_CURRENCY." 
											where cID IN 
										(select buysellCurrency 
											from curr_exchange order by id DESC)
											 OR 
											cID IN (select operationCurrency 
											from curr_exchange order by id DESC)
											group by currencyName");
	
	if(empty($cmd))
		$cmd = "ADD";
	
	/* The default amount in "amount" field should be '1' */
	if(empty($amount))
		$amount = 1;
	
	/* Fetching the list of fees to display at the bottom */	
	$arrAllCurrenyStockData = selectMultiRecords("Select * from ".TBL_CURRENCY_STOCK_LIST." order by currency asc");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Available Currency Stock</title>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" src="jquery.cluetip.js"></script>
<script type="text/javascript">
function print_doc(ob){
	/*ob.parentNode.parentNode.style.display = 'none';
	document.getElementById('submitStock').style.display = 'none';
	document.getElementById('mainTble').style.display = 'none';
	for(i=0;i<allrowscnt;i++){
		if(i == 0){
			document.getElementById('sp_cols').style.display = 'none';
			document.getElementById('sp_cols'+i).style.display = 'none';
			}
		else
			document.getElementById('sp_cols'+i).style.display = 'none';
	}*/
	$('#mainTble').hide();
	$('#printRow').hide();
	window.print();
	/*var yes =confirm(' Do you want to print this page again. ?');		
	if(yes==true){
		print_doc(ob);
	}
	else{
		ob.parentNode.parentNode.style.display = 'table-row';
		document.getElementById('submitStock').style.display = 'block';
		document.getElementById('mainTble').style.display = 'table';
		for(i=0;i<allrowscnt;i++){
			if(i == 0){
				document.getElementById('sp_cols').style.display = 'table-cell';
				document.getElementById('sp_cols'+i).style.display = 'table-cell';
			}
			else
				document.getElementById('sp_cols'+i).style.display = 'table-cell';
		}
	}*/
}
</script>
<script>
	$(document).ready(function() {
		$("#submitStock").validate({
			rules: {
				amount: {
					required: true,
					number: true,
					min: 0
				},
			},
			messages: {
				amount: { 
					required: "<br />The amount is required.",
					number: "<br />This is not valid amount.",
					min: "<br />Value should be greater than or equal to 0"
				}
			}
		});
		$('img').cluetip({splitTitle:'|'});
		
	});
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	line-height:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	line-height:12px;
	color: #000000;
}
</style>
</head>
<body>
<form name="submitStock" id="submitStock" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="50%" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="2" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="2" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;<?=$activitiesMsg?>				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="2" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		<tr bgcolor="#ededed" id="companyRow">
			<td width="40%" class="tdDefination"><b>Currency:&nbsp;</b><font color="red">*</font></td>
			<td align="left" width="60%">
			<?
			if($strEditCurrency==""){
			 ?>
				<select name="currency" id="currency" <?=$disabledCurr;?>>
			<?	if(count($arrCurrenciesRows)>0){ ?>
				<?
					foreach($arrCurrenciesRows as $currKey => $currVal)
					{
						$strCompanySelectedOption = '';
						if($currVal["currencyName"] == $strEditCurrency )
							$strCompanySelectedOption = 'selected="selected"';
				?>
					<option value="<?=$currVal["currencyName"]?>" <?=$strCompanySelectedOption?>><?=$currVal["currencyName"]?></option>
				<? } 
				}
				else{?>
					<option value="">No Currency Rate(s) Defined</option>
				<? }?>
				</select>
			<? }
				else{?>
				<input type="text" name="currency" id="currency" value="<?=$strEditCurrency?>" size="10" readonly="readonly" />
				<? }?>
&nbsp;
				<img src="images/info.gif" border="0" title="Currencies | Currencies are displayed for which Buying/Selling Rates are defined. Otherwise you can't add Currency Stock." />
				</td>
		</tr>
		<tr bgcolor="#ededed">
			<td width="40%" class="tdDefination">Amount :&nbsp;<font color="red">*</font></td>
			<td align="left" width="60%"><input type="text" name="amount" id="amount" size="20" value="<?=$strEditAmount?>" /></td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
			<? if(count($arrCurrenciesRows)>0){?>
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
			<? }?>
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="idVal" value="<?=$strIdVal?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($strIdVal)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>			</td>
		</tr>
	</table>
</form>

<?php /*?><fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;AVAILABLE CURRENCY STOCK&nbsp;
	</legend>
	<?php /*?><table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr><TD width="45%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td width="55%" align="left" class="reportHeader"><?=SYSTEM?> <br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
				  </span></td></tr>
			</table><?php */?>
	<table width="95%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2" style="padding:10px; border:1px #000000 solid;">
		<tr>
			<td colspan="10" style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold;text-align: left;">
			&nbsp;AVAILABLE CURRENCY STOCK&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="10">
			<table width="40%" border="0" cellspacing="0" cellpadding="3" align="center">
				<tr><TD width="15%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td align="left" class="reportHeader" style="font-size: 16px; text-align: left;"><?=SYSTEM?> <br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
				  </span></td></tr>
			</table>
			</td>
		</tr>
		<tr>
			<th width="20%">Currency</th>
			<th width="10%">Symbol</th>
			<th width="10%">Amount </th>
			<th width="10%">System Buying Rate</th>
			<th width="10%">GBP Value</th>
			<th width="10%">Created By</th>
			<th width="10%">Created On</th>
			<th width="10%">Updated By</th>
			<th width="15%">Updated On</th>
			<th id="sp_cols">Actions</th>
		</tr>
		<?php
		$i=0;
		$total =0;
			foreach($arrAllCurrenyStockData as $currVal)
			{
				$strCurrency = $currVal["currency"];
				$strCurrencySymbol = $currVal["currencySymbol"];
				$strAmount = number_format($currVal["amount"],2,".","");
				if(defined("CONFIG_DECIMAL_POINT_CURRENCY_EX_STOCK") && CONFIG_DECIMAL_POINT_CURRENCY_EX_STOCK=='1')
					$strAmount = customNumberFormat($currVal["amount"],2,true);
				$stractualAmount = $currVal["actualAmount"];
				$arrCreatedUserData = explode("|",$currVal["addedBy"]);
				$strCreatedUserName = strtoupper($arrCreatedUserData[0]);
				$strCreatedOn = date("d/m/Y",strtotime($currVal["created_on"]));
				$arrUpdatedUserData = explode("|",$currVal["updatedBy"]);
				$strUpdatedUserName = strtoupper($arrUpdatedUserData[0]);
				$strUpdatedOn = date("d/m/Y H:i:s",strtotime($currVal["updated_on"]));
				$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
				$buying_rate_arr_id = selectFrom($cur_sql); 
				$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
				//debug($cur_sql);
				$buying_rate_arr = selectFrom($cur_sql); 
				//debug($buying_rate_arr);
				$buying_rate=number_format($buying_rate_arr[0],4,'.','');
				$GBP_value =number_format($strAmount/$buying_rate,4,'.',NULL);
				$total +=$strAmount/$buying_rate;
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td>
			<a href="#" onClick="window.open('currencyStock-account.php?currencyStockN=<?=$strCurrency?>','viewCurrencyStockAccount', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=620,width=740')"><font color="#006699"><?=$strCurrency?></a></td>
			<td><?=$strCurrencySymbol?></td>
			<td><?=$strAmount?></td>
			<td><?=$buying_rate?></td>
			<td><?=$GBP_value?></td>
			<td><?=$strCreatedUserName?></td>
			<td><?=$strCreatedOn?></td>
			<td><?=$strUpdatedUserName?></td>
			<td><?=$strUpdatedOn?> GMT</td>
			<td id="sp_cols<?=$i?>">
				<a href="<?=$_SERVER['PHP_SELF']?>?idVal=<?=$currVal["id"]?>&cmd=EDIT" onclick="return confirm('Are you sure to Add amount?')">
					<img src="images/edit_th.gif" border="0" title="Edit Currency| You can edit the record by clicking on this thumbnail. And add amount" />				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?idVal=<?=$currVal["id"]?>&cmd=DEL"  onclick="return confirm('Are you sure to Delete Record?')">
					<img src="images/remove_tn.gif" border="0" title="Remove Stock|By clicking on this the currency stock will be no longer available." />				</a>			</td>
		</tr>
				
		<?
		$i++;
			}
		?>
		</tr>
	  <tr class="<?=$strClass?>" align="center">
      <td colspan="3"></td><td style="font-size:12px"><b>Total GBP Value</b></td><td style="font-size:12px"><b><?=number_format($total,4,'.',',')?></b></td><td></td><td></td><td></td><td></td><td></td>
      </tr>
	  <tr align="center" id="printRow">
      <td colspan="10"><input type="button" name="Submit2" value="Export Stock List to Excel" onClick="document.submitStock.action='Export_Currency_Exchange_List.php';document.submitStock.submit();"> &nbsp; <input type="button" value="Print" onclick="print_doc(this)" />
      </td>
	  </tr>
	</table>
	<?php /*?><script>
	var allrowscnt = <?=count($arrAllCurrenyStockData);?>;
	</script>
</fieldset>
<?php */?>
</body>
</html>