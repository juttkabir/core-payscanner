<?php
/** 
 * short description here
 * This page is used for Printing Selected Transaction.
 * @package Transaction Module
 * @subpackage Print Transaction
 * @author Mirza Arslan Baig
 * @copyright HBS Tech. (Pvt) Ltd.
 */
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$loggedUName = $_SESSION["loggedUserData"]["username"];
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$agentType = getAgentType();
	$transID = $_POST['checkbox'];
	if($transID=="")
	{
		$transID = $_REQUEST["transID"];
		$countTrans = 1;
	}
	else
	{
		$transID = $_POST['checkbox'];
		$countTrans = count($transID);
	}

	$_POST["benID"] = $_SESSION["benID"];
//debug($_POST["benID"]);
	
	$today = date("d-m-Y");
	$todayD = date("Y-m-d"); 
	$emailFlag = false;

/*
* 7897 - Premier FX
* Rename 'Create Transaction' text in the system.
* Enable  : string
* Disable : "0"
*/
$strTransLabel = 'Transaction';

if(defined('CONFIG_SYSTEM_TRANSACTION_LABEL') && CONFIG_SYSTEM_TRANSACTION_LABEL!='0')
	$strTransLabel = CONFIG_SYSTEM_TRANSACTION_LABEL;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Print <?=$strTransLabel;?> Receipt</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
	td.myFormat
	{
		font-size:10px;	
	}
	
	td.Arial
	{
		font-size:10px;
		font-family:"Arial",Times,serif;
		border-bottom: solid 1px #000000;
		border-right: solid 1px #000000;
	}
	table.double
	{
		border:double;
	}
	.noborder
	{
		font-size:11px;
	}
	table.single
	{
		border-style:solid;
		border-width:1px;
	}
	td.bottom
	{
		border-bottom: solid 1px #000000;
		font-size:10px;
	}
	td.sign
	{
		font-size:10px;
	}
	td.style2
	{
		font-size:10px;
	}

</style>
<script language="javascript">

function printDoc() 
{		
	var row=document.getElementById("printRow");
	
	if (row.style.display== '')
	{
		row.style.display="none";
	}
	else 
	{
		row.style.display='';	
	}
	window.print();
}
</script>
</head>
<body>
<?php

if(is_array($transID) && $transID!="")
{
	$countTrans = count($transID);
}
else
{
	$countTrans = 1;
	$transID = array($transID);
}
$nTransID = NULL;

for($t=0;$t<$countTrans;$t++)
{
	$receiptContents = '';
	$receiptContents .= '<table border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA" width="80%">
						<tr>';
	
	$custLogo = selectFrom("select logo  from ".TBL_ADMIN_USERS." where username ='".$_SESSION["loggedUserData"]["username"]."'");
	if($custLogo["logo"] != "" && file_exists("../logos/" . $custLogo["logo"]))
	{
		$arr = getimagesize("../logos/" . $custLogo["logo"]);		
		$width = ($arr[0] > 200 ? 200 : $arr[0]);
		$height = ($arr[1] > 100 ? 100 : $arr[1]);	
		
		$receiptContents .= '<td align="center">'.printLogoOnReciept($_SESSION["loggedUserData"]["userID"], $_SESSION["loggedUserData"]["username"]).'</td>';
		
	}
	else
	{
		$receiptContents .= '<td align="center" ><img id=Picture2 height="'.CONFIG_LOGO_HEIGHT.'" alt="" src="'.CONFIG_LOGO.'" 	        width="'.CONFIG_LOGO_WIDTH.'" border="0"></td>';

	} 

	$sqltrans = "select * from ".TBL_TRANSACTIONS."  where transID ='".$transID[$t]."'";
	
	$transContent = selectFrom($sqltrans);

	//debug($_REQUEST['transID'],true);		
	
	$agentsql="SELECT * from ".TBL_ADMIN_USERS."  WHERE userID  = '".$transContent['custAgentID']."'";
	$custAgentContents=selectFrom($agentsql);		
	//	debug ($custAgentContents);
	$emailFlag = true;
	
	$receiptContents .= '<td class="sign" width="35%">';
	
	if ($agentType=='SUPA' && $custLogo["logo"] != "")
	{
		$receiptContents .= $custAgentContents['agentCompany']."<br>".$custAgentContents['agentAddress'].",".$custAgentContents['agentAddress2'].",<br>".$custAgentContents['agentCity'].",".$custAgentContents['agentZip'].",<br>".$custAgentContents['agentCountry']."<br>";
		$receiptContents .= "Tel : ".$custAgentContents['agentPhone']."<br>Fax : ".$custAgentContents['agentFax']."<br>Email : ".$custAgentContents['email']."<br>Web : ".$custAgentContents['agentURL'];
		//	echo "<br>company";
	} 
	else
	{
		$adminsql= "SELECT * from ".TBL_ADMIN_USERS."  WHERE username = '".$loggedUName."'";
		$adminContents=selectFrom($adminsql);
		$receiptContents .= $adminContents['agentCompany']."<br>".$adminContents['agentAddress'].", ".$adminContents['agentAddress2'].",<br>".$adminContents['agentCity'].", ".$adminContents['agentZip'].", ".$adminContents['agentCountry']."<br>";
		$receiptContents .= "Tel : ".$adminContents['agentPhone']."<br>Fax : ".$adminContents['agentFax']."<br>Email : ".$adminContents['email']."<br>Web : ".$adminContents['agentURL'];
		//debug ($adminContents);
	}
	$receiptContents .= '</td>
					</tr>
				</table>
				<form name="addTrans" method="post" action="">';
				
	//$queryTrans="select * from ".TBL_TRANSACTIONS."  where transID= ".$transID[$t];
	//$transContent = selectFrom($queryTrans); 
	
	$customerID = $transContent['customerID'];
	
	$queryCust = "select customerID, Title, accountName, firstName, lastName, middleName, accountName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks, parentID  from ".TBL_CUSTOMER."  where customerID ='" . $customerID . "'";

	$customerContent = selectFrom($queryCust); 
	
	$sqlAccManager="select name from ".TBL_ADMIN_USERS." where userID= '" . $customerContent["parentID"] . "'";
	$accManagerContent = selectFrom($sqlAccManager);
	
	$sql="select * from ".TBL_ADMIN_USERS."  where userID= '" . $custAgentContents["userID"] . "'";
	$userContent = selectFrom($sql);

	$queryusername= " select name from ".TBL_ADMIN_USERS."  where username = '".$transContent['addedBy']."'";
	$usernameCont= selectFrom($queryusername);

	$sql= "select refNumber from ".TBL_TRANSACTIONS."  where transID='" . $transID[$t] . "'"; 
	$refContent = selectFrom($sql);

	$query = "select currencyFrom from ".TBL_TRANSACTIONS."  where transID='".$transID[$t]."'";
	$currencyContent = selectFrom($query);

	$queryBen = "select benID, Title, firstName, lastName, middleName" . $SOFField . ", Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from beneficiary where benID ='" . $_POST["benID"] . "'";
	//debug($queryBen);
	if($_GET['msg'] == "Y")
{

$queryBenup = "update beneficiary SET reference='".$_SESSION["tip"]."' where benID ='".$_POST["benID"]."'";
$ff=update($queryBenup);
//debug($queryBenup);
}
	
	$benificiaryContent = selectFrom($queryBen);
	
	$accountSQl = "SELECT * FROM accounts WHERE currency = '".$currencyContent['currencyFrom']."' AND showOnDeal = 'Y' ";
	$accountRS = selectFrom($accountSQl);
	$fieldsChecked = $accountRS["fieldsChecked"];
	
	if(!empty($fieldsChecked))
		$fieldsCheckedArr = unserialize($fieldsChecked);

	$trAccountrTrading = "";
	
	for($i=0;$i<count($fieldsCheckedArr);$i++)
	{
		$fieldsArr = explode("|",$fieldsCheckedArr[$i]);
		if(is_array($fieldsArr))
		{
			$fieldsValue = $fieldsArr[0];
			$fieldsLabel = $fieldsArr[1];
		}
		else
		{
			$fieldsValue = $fieldsCheckedArr[$i];
		}
		if(empty($fieldsLabel))
			$fieldsLabel = $fieldsValue;

		$trAccountrTrading .= ' <tr><td class="Arial" width="20%">'.strtoupper($fieldsLabel).':</td>
									<td width="35%" class="bottom">'.$accountRS[$fieldsValue].'</td>
								</tr>';	
	}
	
	
	$bankdetailsQuery = selectFrom("select * from bankDetails where transID='".$transID[$t]."'");		 
	 
	$receiptContents .= '<table border="0" width="80%" class="single" align="center">
						<tr>
							<td>
								<h6 align="Center">DEAL CONTRACT</h6>
									<table border="0" width="80%" align="center" class="double">
										<tr border="1">
											<td width="20%" class="Arial">CLIENT NAME:</td>
											<td width="80%" class="bottom">'.$customerContent["firstName"] . " " . $customerContent["lastName"] . '</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">CLIENT NO.</td>
											<td width="80%" class="bottom">'.$customerContent["accountName"].'</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">'.TITLE.' DEALER</td>
											<td width="80%" class="bottom">'.$accManagerContent['name'].'</td>
										</tr>
									</table>
									<table border="0" width="80%" align="center">
										<tr>
											<td align="center" colspan="2" class="noborder" style="text-align:justify">This Trade has now been executed in accordance with your verbal instruction, which is legally binding in accordance with our Terms & Conditions.
											<table border="0" width="80%" align="center" class="double">
										<tr>
											<td class="Arial" width="20%">TRADE DATE:</td>
											<td width="80%" class="bottom">'.dateFormat($transContent['transDate'],2).'&nbsp;</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">MATURITY DATE:</td>
											<td width="80%" class="bottom">'.dateFormat($transContent['valueDate'],2).'&nbsp;</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">YOU BUY:</td>
											<td width="80%" class="bottom">'.$transContent["localAmount"]. ' '. $transContent["currencyTo"].'</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">AT A RATE OF:</td>
											<td width="80%" class="bottom">'.$transContent['exchangeRate'].'&nbsp;</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">YOU SELL:</td>
											<td width="80%" class="bottom">'.number_format($transContent["transAmount"],2).' '.$transContent["currencyFrom"].'</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">TT Fee:</td>
											<td width="80%" class="bottom">ZERO</td>
										</tr>
										<tr>
											<td class="Arial" width="20%">REFERENCE:</td>
											<td width="80%" class="bottom">'.$transContent['refNumber'].'&nbsp; &nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table border="0" width="80%" align="center">
							<tr>
								<td align="center" colspan="2" class="noborder">Where to instruct your Bank to send payment to us:</td>
							</tr>
						</table>
						<table border="0" width="50%" align="center" class="double">';
						
						if(!empty($transContent['currencyFrom']))
						{
							$receiptContents .= '
												<tr>
													<td class="Arial" width="20%">ACCOUNT NAME:</td>
													<td width="25%" class="Arial">'.$accountRS["accountName"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="20%">ACCOUNT NUMBER:</td>
													<td width="35%" class="bottom">'.$accountRS["accounNumber"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="20%">BANK:</td>
													<td width="25%" class="bottom">'.$accountRS["bankName"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="20%">IBAN:</td>
													<td width="35%" class="bottom">'.$accountRS["IBAN"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="20%">SORT CODE:</td>
													<td width="25%" class="bottom">'.$accountRS["sortCode"].'&nbsp;</td>
												<tr>
													<td class="Arial" width="20%">Swift Code:</td>
													<td width="35%" class="bottom">'.$accountRS["swiftCode"].'&nbsp;</td>
												</tr>';

							$receiptContents .= $trAccountrTrading;
						}	
						$receiptContents .= '<table widh="100%" border="0" bordercolor="#FF0000" align="center">
												<tr>
													<td>
														<table border="0" width="80%" align="center">
															<tr>
																<td colspan="2" style="text-align:justify">I confirm that the details set out above are correct and that I will transmit the amount due to the account specified.  I accept that the cost of failing to provide cleared settlement funds (1) one day before the maturity date of this trade may be subject to a &pound;25.00 per day late payment fee.</td>
															</tr>';
						if(CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT != '1'){
							$receiptContents .= '<tr>		
																<td width="21%" height="35"><br/>SIGNED</td>
																<td width="79%" height="35">&nbsp;</td>
															</tr>
															<tr>
																<td width="21%" height="35"><br/>NAME (Print):</td>
																<td width="79%" height="35">&nbsp;</td>
															</tr>
															<tr>
																<td width="21%" height="35"><br/>DATED:</td>
																<td width="79%" height="35"><br/>'.date("d/m/y",strtotime($transContent["transDate"])).'&nbsp;</td>
															</tr>';
						}
									$receiptContents .= '</table>
													</td>
												</tr>
												</table>
											</form>
					<table border="0" width="80%" align="center" class="double">
												<tr>
													<td class="Arial" colspan="2" align="center" style="font-weight:bold;font-size:14px; text-decoration:underline;">REQUEST FOR INTERNATIONAL TELEGRAPHIC TRANSFER</td>
												</tr>
												<tr>
													<td class="Arial" colspan="2" align="center">Please note that if you are making more than ONE transfer you will need to copy this page for each instruction.</td>
												</tr>
												<tr>
													<td class="Arial" width="45%">CLIENT NUMBER:</td>
													<td width="55%" class="Arial">'.$customerContent["accountName"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="45%">AMOUNT OF CURRENCY TO BE TRANSFERRED:</td>
													<td width="55%" class="bottom">'.$transContent["localAmount"].' '.$transContent["currencyTo"].'</td>
												</tr>';
				if(CONFIG_CHANGE_PRINT_CONFIRM_DEAL_LAYOUT == '1'){
						$receiptContents .= '
							<tr>
								<td class="Arial">BENEFICIARY ACCOUNT NAME :</td>
								<td class="bottom">'.$bankdetailsQuery["bankName"].'</td>
							</tr>
							<tr>
								<td class="Arial">Account NO :</td>
								<td class="bottom">'.$bankdetailsQuery["accNo"].'</td>
							</tr>
							<tr>
								<td class="Arial">Branch Name / Number :</td>
								<td class="bottom">'.$bankdetailsQuery["branchCode"].'</td>
							</tr>
							<tr>
								<td class="Arial">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO :</td>
								<td class="bottom">'.$bankdetailsQuery["branchAddress"].'</td>
							</tr>
							<tr>
								<td class="Arial">SWIFT/BIC Code of your bank :</td>
								<td class="bottom">'.$bankdetailsQuery["swiftCode"].'</td>
							</tr>
							<tr>
								<td class="Arial">SORT CODE :</td>
								<td class="bottom">'.$bankdetailsQuery["sortCode"].'</td>
							</tr>
							<tr>
								<td class="Arial">BENEFICIARY\'S IBAN :<br />
								(if the payment is in euros, a full iban is required to avoid a bank administration charge)</td>
								<td class="bottom">'.$bankdetailsQuery["IBAN"].'</td>
							</tr>
							<tr>
								<td class="Arial">ABA/ROUTING NUMBER (USA ONLY) :</td>
								<td class="bottom">'.$bankdetailsQuery["routingNumber"].'</td>
							</tr>
						';
				}else{
					$receiptContents .= '<tr>
													<td class="Arial" width="45%">BENEFICIARY&rsquo;S ACCOUNT NAME:</td>
													<td width="55%" class="bottom">'.$bankdetailsQuery["bankName"].'&nbsp;</td>
												</tr>
												<tr>
									
													<td class="Arial" width="45%">BENEFICIARY&rsquo;S IBAN / ACCOUNT NO:
												IF THE PAYMENT IS IN EUROS, A FULL IBAN IS REQUIRED TO AVOID A BANK ADMINISTRATION CHARGE</td>
													<td width="55%" class="bottom">'; 
													if($bankdetailsQuery["IBAN"]!='' && $bankdetailsQuery["accNo"]!='')
													{			
														$receiptContents .= 'IBAN: '.$bankdetailsQuery["IBAN"].' / Account No. '.$bankdetailsQuery["accNo"];
													}
													elseif($bankdetailsQuery["IBAN"]!='')
													{
														$receiptContents .= 'IBAN: '.$bankdetailsQuery["IBAN"];
													}
													elseif($bankdetailsQuery["accNo"]!='')
													{
														$receiptContents .= 'Account No. '.$bankdetailsQuery["accNo"];
													}		
													$receiptContents .= '&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="45%">NAME AND ADDRESS OF THE BANK YOUR CURRENCY IS BEING TRANSFERRED TO:</td>
													<td width="55%" class="bottom">'.$bankdetailsQuery["branchAddress"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="45%">SWIFT/BIC CODE OF THE YOUR BANK:</td>
													<td width="55%" class="bottom">'.$bankdetailsQuery["swiftCode"].'&nbsp;</td>
												</tr>
												<tr>
													<td class="Arial" width="45%">CLEARING CODE OF THE BANK:(I.E. SORT CODE/ABA ROUTING NUMBER)</td>
													<td width="55%" class="bottom">';
													if($bankdetailsQuery["sortCode"]!="" &&  $bankdetailsQuery["routingNumber"]!="") 
													{
														$receiptContents .= $bankdetailsQuery["sortCode"].' / '.$bankdetailsQuery["routingNumber"];			
													}
													elseif($bankdetailsQuery["sortCode"] == "")
													{
														$receiptContents .= $bankdetailsQuery["routingNumber"];			
													}
													elseif($bankdetailsQuery["routingNumber"] == "")
													{
														$receiptContents .= $bankdetailsQuery["sortCode"];
													}
													$receiptContents .= '&nbsp;</td>
												</tr>';
					}
												$receiptContents .= '<tr>
													<td class="Arial" width="45%">WHAT REFERENCE, IF ANY,DO YOU WANT TO QUOTE ON YOUR TRANSFER:</td>
													<td width="55%" class="bottom">';
													if(CONFIG_HIDE_TIP_FIELD != "1")
													{
														$receiptContents .= $transContent["tip"];
													}	
													else
													{
														$receiptContents .= 'BARC GB 22';
													}
													$receiptContents .= '&nbsp;													</td>
												</tr>
											</table>
											<table widh="100%" border="0" bordercolor="#FF0000" align="center">
												<tr>
												<td align="center">';		
												foreach ($_POST as $k =>$v)
												{
													$hiddenFields .= "<input name=\"" . $k. "\" type=\"hidden\" value=\"". $v ."\">\n";
													$str.="$k\t\t:$v<br>";
												}

												$receiptContents .= $hiddenFields;
												$receiptContents .= ' <tr>';
												$TRANSACTION_DATE = dateFormat($todayD,1);
												if(CONFIG_TRANS_CONDITION_ENABLED == '1')
												{
												
													if(defined("CONFIG_TRANS_COND"))
													{
														$receiptContents .= '<td class = "myFormat" align="center">'. (CONFIG_TRANS_COND).'</td>';						
													}
													else
													{  
														$termsConditionSql = selectFrom("SELECT	company_terms_conditions
																							FROM 
																								company_detail
																							WHERE 
																							company_terms_conditions!='' AND 
																							dated = (SELECT MAX(dated)
																							FROM 
																							company_detail 
																							WHERE 
																							company_terms_conditions!=''
																							)");
																		
														$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
														if(!empty($termsConditionSql["company_terms_conditions"]))
															$tremsConditions = addslashes($termsConditionSql["company_terms_conditions"]);
														//eval("$tremsConditions");
														eval("\$tremsConditions = \"$tremsConditions\";");
														
														//$receiptContents .= stripslashes($tremsConditions);
														//echo $tremsConditions;
													}
												}
												else
												{
													$receiptContents .= 'I accept that I haven\'t used any illegal ways to earn money and I am paying tax regulary. I also accept that I am not going to send this money for any illegal act.';
												}
												$receiptContents .= '</tr></form>';
											echo $receiptContents;
									$lastRec = ",";
									if($t == ($countTrans-1))
										$lastRec = "";
									$nTransID.= $transID[$t].$lastRec;
  								} 
								
								//$nTransID = substr($nTransID, 0, -1);
								
								
								?>
			<form name="form1" action="sendmail_premier_pdf.php" method="post">
			<table align="center" width="650" border="0" cellpadding="0"  cellspacing="0">	
				<tr>
				<?php
					if($emailFlag)
					{
						?>
						<div class='noPrint' align="right">
							<tr id="printRow"><td>
							<input type="submit" name="email" value="Email">
							<input type="hidden" name="transid" value="<?=$nTransID?>">
							To <input type="text" name="emailto" value="<?=$customerContent['Email']?>">	
							</div>
							<td>
						<?
					}
					?>
					<input type="button" name="Submit2" value="Print this Receipt" onClick="printDoc()">
					&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create <?=$strTransLabel?></a>
				</td>
			</tr>
		</div>
	</table>
	</form>

</body>
</html>

