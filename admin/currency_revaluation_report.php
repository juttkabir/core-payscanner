<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Currency Revaluation Report </title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/inlineedit.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var selectedId = "";
	var extraParams = "";
	$(document).ready(function(){
	
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		});
	
		$("#transList").jqGrid({
			url:'currency-revaluation-conf.php?get=getceGrid&type=revaluation&q=2&nd='+new Date().getTime(),
			datatype: "json",
			height: 400, 
			width: 950,
			colNames:[
				'Cur cd', 
				'Currency Description',
				'F.C Balance Amount',
				'L.C Balance Amount',
				'Revaluation Rate',
				'Revalued Amount',
				'Profit/(Loss)',
				'Avg. Rate',
				'Avg. Rate Amount'
			],
			colModel:[
				{name:'srNo',index:'id', width:20, align:"center", sortable:false},
				{name:'currency',index:'currency', width:100},
				{name:'FAmount',index:'amount', width:30, align:"right"},
				{name:'LAmount',index:'LAmount', width:30, align:"right", sortable:false},
				{name:'rate',index:'revaluation_rate', width:20, align:"left",editable:true,editrules:{number:true}},
				{name:'RAmount',index:'RAmount', width:30,align:"right", sortable:false},
				{name:'PL',index:'PL', width:30, align:"right", sortable:false},
				{name:'avgRate',index:'avgRate', width:20, align:"right", sortable:false},
				{name:'avgRateAm',index:'avgRateAm', width:30, align:"left", sortable:false}
			],
			rowNum: 20,
			rowList: [5,10,20,50],
			imgpath: gridimgpath,
			pager: jQuery('#pager'),
			sortname: 'id',
			viewrecords: true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			cellEdit: true,
			cellurl: 'currency-revaluation-conf.php?get=editRate',
			caption: "Currency Revaluation Report",
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
			gridReload();
			}		
			/*onSelectRow:function(id){
					loadReciept(id);
			}*/
		});
		jQuery('a').cluetip({splitTitle: '|'});
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});

		$("#search_buysell").click(function(){
			gridReload();
		});
		
		
		$("#exportBtn").click(function(){
			$("#exporCurrRow").show();
		});
		
		$("#exporCurrRow").change(function(){
			if($(this).val() != "")
			{
				var strUrl = "buySellCurrExport.php?type=Cancelled&exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()
							+"&createdBy="+$("#createdBy").val()+
							"&clientName="+$("#clientName").val()+"&type_of_exchange="+$("#type_of_exchange").val()+"&Submit=Search";		
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
		$("#exportBtn").click(function(){
			$("#exporCurrRow").show();
		});
		$("#exporCurrRow").change(function(){
			if($(this).val() != "")
			{
				var strUrl = "currency-evaluation-export.php?exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()+"&Submit=Search";
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
	});
	
	function gridReload()
	{
		var date_from = $("#from_date").val();
		var to_date = $("#to_date").val();
		var theUrl = "currency-revaluation-conf.php?get=getceGrid&type=revaluation&from_date="+date_from+"&to_date="+to_date+"&Submit=Search";
		if(extraParams != "")
		{
			theUrl = theUrl + extraParams;
			extraParams = "";
		}
		$("#transList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
		
	}
	function resetTotals()
	{
/*		var ret = jQuery("#transList").getRowData(rowId);
		var fCurrency = ret.currency;*/
		lAmt = 0;
		tAmt = 0;
		jQuery("#lfa").text(lAmt.toFixed(2));
	}
	function updateTotals(rowId)
	{
		var ret = jQuery("#transList").getRowData(rowId);
		var fCurrency = ret.currency;
		if(jQuery("#jqg_"+rowId).attr("checked"))
		{
			lAmt += parseFloat(ret.localAmount);
		}
		else{
			lAmt = lAmt - parseFloat(ret.localAmount);
		}
		
		jQuery("#lfa").text(lAmt.toFixed(2) + " GBP");
	}

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee
/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
/*a.dp-choose-date {
	float: left;
	width: 16px;
	height: 16px;
	padding: 0;
	margin: 5px 3px 0;
	display: block;
	text-indent: -2000px;
	overflow: hidden;
	background: url(images/calendar.jpg) no-repeat; 
}
a.dp-choose-date.dp-disabled {
	background-position: 0 -20px;
	cursor: default;
}*/
.error {
	color: red;
	font: 6pt verdana;
	font-weight:bold;
}
.summaryRow{
	font-size:14px;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" bgcolor="#DFE6EA">
			<strong>Currency Revaluation Report </strong>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
			<form name="search_buysell" id="search_buysell" action="" method="post">
				Date In From :&nbsp;
				<input type="text" name="from_date" id="from_date" readonly="" />
				&nbsp;&nbsp;
				Date In To :&nbsp;
				<input type="text" name="to_date" id="to_date" readonly="" />
				<br/><br/>				
				<input type="button" id="search_buysell" name="search_buysell" value="Search" style="font-weight:bold" />
				&nbsp;&nbsp;<input type="reset" value="Clear All Filters" />
				&nbsp;&nbsp;<input type="button" id="exportBtn" value="Export Evaluation Report" />
				&nbsp;
				<select name="exporCurrRow" id="exporCurrRow" style="display:none">
					<option value="">Select Format</option>
					<option value="XLS">Excel</option>
					<option value="CSV">CSV</option>
					<option value="HTML">HTML</option>
				</select>
			</form>
		</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="100%">
			</table>
			<div id="pager" class="scroll" style="text-align:center;"></div>		
		</td>
    </tr>
</table>
</body>
</html>
