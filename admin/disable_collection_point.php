<?
session_start();
include ("../include/config.php");
include("connectOtherDataBase.php");
$date_time = date('Y-m-d  h:i:s A');
dbConnect();
$username=loggedUser();
if ($_GET["agentCity"]!="")
{
	$_SESSION["cityValue"] = $_GET["agentCity"];
}
///////////////////////History is maintained via method named 'activities'
$agentDetails = selectFrom("select * from ".TBL_COLLECTION." where cp_id='".$_GET["cp_id"]."'");


if( isset($_REQUEST["disableEnable"]) ){

	$newActiveStatus = $agentDetails["cp_active"] == "Active" ? "Disabled" : "Active";

	update("update ".TBL_COLLECTION." set cp_active='" . $newActiveStatus . "',disabledDate='$date_time', disabledBy='$username', disableReason='".checkValues($_GET[suspensionReason])."' where cp_id='".$_GET[cp_id]."'");

	
	
	if(CONFIG_SHARE_OTHER_NETWORK == '1' )
	{
				
		$sharedTableQuery = selectFrom("select * from ".TBL_SHAREDUSERS." where localServerId = '".$_GET["cp_id"]."' AND availableAs = 'Collection Point'");

			//if collection  point is shared
				if($sharedTableQuery["availableAs"]== 'Collection Point'){
					
					$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTableQuery["availableAt"]."'");
		$otherClient = new connectOtherDataBase();
		$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
					
					update("update ".$jointClient["dataBaseName"].".cm_collection_point set cp_active='Disabled',disabledDate='$date_time', disabledBy='$username', disableReason='".checkValues($_GET[suspensionReason])."' where cp_id='".$sharedTableQuery["remoteServerId"]."'");
					$otherClient->closeConnection();
				dbConnect();
					
					
		}
	}			
					
	
	$descript = "Collection point of city ".$agentDetails["cp_city"]."  is Disabled ";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["userID"],TBL_COLLECTION,$descript);
	$collectionPointUpdated = true;
	
	//echo "update ".TBL_COLLECTION." set cp_active='Disabled',disabledDate=$date_time, disabledBy='$username', disableReason='".checkValues($_GET[suspensionReason])."' where cp_id='".$_GET[cp_id]."'";
		
	/*if ($_GET["caller"] == "sub")
		insertError(eregi_replace("Supper","Sub",AG36));
	else
		insertError(AG36);
	redirect("manage_collection_point.php?msg=Y&userID=".$_GET[cp_id]);*/

	$cityValue = ( !empty($_SESSION["cityValue"]) ? $_SESSION["cityValue"] : "" );
}
?>
<html>
<head>
	<title>Disable Collection Point</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<?
	if ( $collectionPointUpdated == true )
	{
		?>
			<script language="javascript">
				var val = "<?=$cityValue;?>";
				<?
					if ( !isset( $_REQUEST["reloadOpener"] ) )
					{
						?>
							opener.document.location.reload();
						<?
					}
					else
					{
						?>
							opener.document.location = "manage_collection_point.php?msg=Y&agentCity="+val;
						<?
					}
				?>
				
				window.close();
			</script>
		<?
	}
?>
	<script language="javascript">
	<!-- 
function checkForm(theForm) {
	if(theForm.suspensionReason != null  && ( theForm.suspensionReason.value == "" || IsAllSpaces(theForm.suspensionReason.value) )){
    	alert("Please enter disabling reason.");
        theForm.suspensionReason.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
</head>
<body>
<form action="disable_collection_point.php" method="get" onSubmit="return checkForm(this);">
<table border="0" cellspacing="1" cellpadding="5">
  <tr class="topbar">
    <td height="30"><strong><font size="2"><?=$agentDetails["cp_active"] == "Active"? "Disable" : "Enable";?> collection point: <?=$agentDetails["cp_branch_name"];?></font></strong></td>
	<td align=right><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td>
  </tr>
  <input type="hidden" name="cp_id" value="<? echo $_GET["cp_id"];?>">
  <tr>
    <td align="center" colspan=2>
		<!--table width="419" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Delete 
                    <?=stripslashes($agentDetails[agentContactPerson])." [".stripslashes($agentDetails[agentCompany])."]"; ?>
                    </strong></font></td>
                </tr>
              </table></td>
          </tr-->
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed"> 
            <td colspan="2" bgcolor="#FFFFCC"><table width="100%" height="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } else { ?>
          <tr bgcolor="#ededed"> 
            <td width="136" valign=top><font color="#005b90"><strong><?=$agentDetails["cp_active"] == "Active"? "Disable Reason*" : "Enable";?></strong></font></td>
			<?
				if ( $agentDetails["cp_active"] == "Active" )
				{
					?>
					<td width="272"><textarea name="suspensionReason" cols="40" rows="4" style="font-family: verdana; font-size: 11px"><?=stripslashes($_SESSION["suspensionReason"]); ?></textarea></td>
				  </tr>
				  <tr bgcolor="#ededed"> 
					<td></td>
					<?
				}
			?>
            <td><input type="submit" name="disableEnable" value="<?=$agentDetails["cp_active"] == "Active"? "Disable" : "Enable";?>"></td>
          </tr>
		  <? } ?>
        </table>
	</td>
  </tr>
</table>
</form>
</body>
</html>