<? 

session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if($_POST["fYear"]!= '' && $_POST["fMonth"]!= '' && $_POST["fDay"]!= ''){
		$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
		
		$fMonth = $_POST["fMonth"];
		$fDay = $_POST["fDay"];
		$fYear = $_POST["fYear"];
	
}else if($_GET["fromDate"] != '') {
	$fromDate = $_GET["fromDate"];
	$fYear = substr($fromDate, 0, 4);
	$fMonth = substr($fromDate, 5, 2);
	$fDay = substr($fromDate, 8, 2);
	}
	
if($_POST["tYear"]!= '' && $_POST["tMonth"]!= '' && $_POST["tDay"]!= ''){	
	$toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
	
	$tMonth = $_POST["tMonth"];
	$tDay = $_POST["tDay"];
	$tYear = $_POST["tYear"];
	
}else if($_GET["toDate"] != ''){
	
	$toDate = $_GET["toDate"];
	$tYear = substr($toDate, 0, 4);
	$tMonth = substr($toDate, 5, 2);
	$tDay = substr($toDate, 8, 2);
	
	
	}
	
if($_POST["companyName"]!= '')
{
		$companyName = $_POST["companyName"];
}elseif($_GET["companyName"]!= '')
{
			$companyName = $_GET["companyName"];
}


//echo ("Company Name is".$companyName);

if($_POST["paymentType"]!= '')
{
					$paymentType = $_POST["paymentType"];	
}else if($_GET["paymentType"] != '')
{
					$paymentType = $_GET["paymentType"];	
}
	
	
if($_POST["commisionType"] != ''){
		
		
	$commisionType = $_POST["commisionType"];
	
}else{
	
				$commisionType = $_GET["commisionType"];
	
	}
	
if($_POST["custAgent"] != ''){
		
		$custAgent = $_POST["custAgent"];
		
}else{
	$custAgent = $_GET["custAgent"];
	}

if($_POST["transStatus"] != '')
{
	$transStatus = $_POST["transStatus"];
}else if($_GET["transStatus"] != '')
{
	$transStatus = $_GET["transStatus"];	
}




//if ($_POST["Submit"] == "Search") {

 $outgoingTransQry= "select a.userID, count(t.transID) as noOfTransaction, sum(t.totalAmount) as totalAmount, sum(t.transAmount) as principalAmount, sum(t.IMFee) as totalCommission, sum(st.totalComm) as totalSharedComm, sum(AgentComm) as AgentComm, sum(st.remoteComm) as remoteComm  
from admin as a , transactions as t, sharedTransactions as st 
where  st.remoteServerId = '".$companyName."'
AND a.userID = t.custAgentId AND st.generatedLocally = 'Y' AND t.transId = st.localTrans AND st.remoteServerId = '".$companyName."'  ";

//echo("select localServerId from sharedUsers where availableAt = '".$companyName."' and availableAs like 'Base Agent' ");
$baseAgent = selectFrom("select localServerId from sharedUsers where availableAt = '".$companyName."' and availableAs like 'Base Agent' ");
//$selectSubAgent = selectFrom( "select userID from admin where parentID =  '".$baseAgent["localServerId"]."' ");

$baseAgentTransQry = "select a.userID,a.userName,a.name,count(t.transID) as noOfTransaction,sum(t.totalAmount) as totalAmount, sum(t.transAmount) as principalAmount, sum(t.IMFee) as totalCommission, sum(s.totalComm) as totalSharedComm, sum(AgentComm) as AgentComm, sum(s.remoteComm) as remoteComm from admin as a , transactions as t, sharedTransactions as s
 where  a.userID = t.custAgentParentID  AND t.transID = s.localTrans and s.generatedLocally = 'N' and userID = '".$baseAgent["localServerId"]."'";



$outgoingCancelQry = "select a.userID, count(t.transID) as noOfTransaction, sum(t.totalAmount) as totalAmount, sum(t.transAmount) as principalAmount, sum(t.IMFee) as totalCommission, t.refundFee as refundFee, sum(st.totalComm) as totalSharedComm, sum(AgentComm) as AgentComm, sum(st.remoteComm) as remoteComm  
from admin as a , transactions as t, sharedTransactions as st 
where  st.remoteServerId = '".$companyName."'
AND a.userID = t.custAgentId AND st.generatedLocally = 'Y' AND t.transId = st.localTrans AND st.remoteServerId = '".$companyName."'  ";
$baseAgentCancelQry = "select a.userID,a.userName,a.name,count(t.transID) as noOfTransaction,sum(t.totalAmount) as totalAmount, sum(t.transAmount) as principalAmount, sum(t.IMFee) as totalCommission, t.refundFee as refundFee, sum(s.totalComm) as totalSharedComm, sum(AgentComm) as AgentComm, sum(s.remoteComm) as remoteComm from admin as a , transactions as t, sharedTransactions as s
 where  a.userID = t.custAgentParentID  AND t.transID = s.localTrans and s.generatedLocally = 'N' and userID = '".$baseAgent["localServerId"]."'";

$outgoingCancelQry .= " and t.transStatus = 'Cancelled' ";
$baseAgentCancelQry .= " and t.transStatus = 'Cancelled' ";

if(CONFIG_REMOTE_LEDGER_AT_CREATE == "1"){
	
	$dateFilter = "transDate";
	
	}else{
		
		$dateFilter = "authoriseDate";
		
		}

	if ($fromDate != "" && $toDate!= "") {
			 $baseAgentTransQry .= " and (t.".$dateFilter." >= '$fromDate 00:00:00' and t.".$dateFilter." <= '$toDate 23:59:59') ";
			 $outgoingTransQry.= " and (t.".$dateFilter." >= '$fromDate 00:00:00' and t.".$dateFilter." <= '$toDate 23:59:59') ";
				
				$outgoingCancelQry .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59') ";
				$baseAgentCancelQry .= " and (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59') ";
}

if($custAgent != '')
{
 $baseAgentTransQry .= " and (a.userId = '".$custAgent."') ";
 $outgoingTransQry.= " and (a.userId = '".$custAgent."') ";
 
 $outgoingCancelQry .= " and (a.userId = '".$custAgent."') ";
 $baseAgentCancelQry.= " and (a.userId = '".$custAgent."') ";
}
if($commisionType != "")
{

	 $baseAgentTransQry .= " and (a.paymentMode = '".$commisionType."') ";
	 $outgoingTransQry.= " and (a.paymentMode = '".$commisionType."') ";
	 
	 $outgoingCancelQry .= " and (a.paymentMode = '".$commisionType."') ";
	 $baseAgentCancelQry.= " and (a.paymentMode = '".$commisionType."') ";

}
	
if($transStatus != "")
{
	$baseAgentTransQry  .= " and (t.transStatus='".$transStatus."') ";
	$outgoingTransQry .= " and (t.transStatus='".$transStatus."') ";
	
	$outgoingCancelQry  .= " and (t.transStatus='".$transStatus."') ";
	$baseAgentCancelQry .= " and (t.transStatus='".$transStatus."') ";
}


$outgoingCancelQry  .= " and (t.transStatus = 'Cancelled') ";
$baseAgentCancelQry .= " and (t.transStatus = 'Cancelled') ";


//$outgoingTransQry .= "	and a.paymentMode !=''";
 $outgoingTransQry.= "group by a.userID";

  $baseAgentTransQry .= "group by a.userID";
//echo "OUTGOING".$outgoingTransQry;


$outgoingCancelQry.= "group by a.userID, t.refundFee";

  $baseAgentCancelQry .= "group by a.userID, t.refundFee";
//echo "OUTGOING".$outgoingCancelQry;


if($paymentType != 'debit')
{
	$outgoingTrans = selectMultiRecords($outgoingTransQry);
	$outgoingCancel = selectMultiRecords($outgoingCancelQry);
}

if($paymentType != 'credit')
{
	$baseAgentTrans = selectMultiRecords($baseAgentTransQry);
	$baseAgentCancel = selectMultiRecords($baseAgentCancelQry);
}
//}



$qryString = "action=".$_GET["action"]."&companyName=".$companyName."&fromDate=".$fromDate."&toDate=".$toDate."&paymentType=".$paymentType."&commisionType=".$commisionType."&transStatus=".$transStatus;


 ?>




<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>API Report</title>
	<script language="javascript">
	
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>


<style type="text/css">
<!--
.style1 {
	font-family: Georgia;
	font-size: 24px;
}
-->
</style>

</head>

<body>
<form id="form2" name="form2" method="post" action="APITransactionReportLevel2.php">
	<table width="100%" border="0" cellspacing="0">
  
  <tr>
    <th colspan="10" scope="col"><span class="style1">API Link Summary Report</span></th>
  </tr>
</table>
	<fieldset align="center">
		<legend><b>Search</b></legend>
<table width="70%" border="0" cellspacing="0" align="center">
  
 
  <tr>
    <th colspan="10" scope="col">&nbsp;</th>
  </tr>
  <tr>
    <td colspan="2">Date from </td>
    <td colspan="3">
      <label>
						<?   
		$month = date("m"); 
		  
		$day = date("d");  
		$year = date("Y");  
		?>
       <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			  
			  
				<OPTION value="" >Day</OPTION>
          <?
					  	
					  	for ($Day=1;$Day<32;$Day++)
						{ 
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.form2.fDay, "<?=$fDay?>");
        </script>
        </label>
 	<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="" >Month</OPTION>
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.form2.fMonth, "<?=$fMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
						<OPTION value="" >Year</OPTION>
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.form2.fYear, "<?=$fYear?>");
        </script></td>
    <td colspan="2">Date To </td>
    <td colspan="3">
   <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				<OPTION value="" >Day</OPTION>
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.form2.tDay, "<?=$tDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value=""   >Month</OPTION>
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.form2.tMonth, "<?=$tMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
				<OPTION value="" >Year</OPTION>
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.form2.tYear, "<?=$tYear?>");
        </script></td>
    </tr>
  <tr>
    <td colspan="2">Payment In/Payment Out </td>
    <td width="21%"><select name="paymentType">
      <option>-Select-</option>
      <option value="allPaymentType" <? echo ($paymentType == "allPaymentType"? "selected" : "")?>>All</option>
      <option value="debit" <? echo ($paymentType == "debit"? "selected" : "")?>>Debit</option>
      <option value="credit" <? echo ($paymentType == "credit"? "selected" : "")?>>Credit</option>
    </select></td>
    <td width="10%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td colspan="2">Agent</td>
    
    
    <td width="9%">
    	<?  $agentQuery= "select s.localServerId,a.name from sharedUsers as s, admin as a  where a.userID= s.localServerId and availableAs = 'Agent' and availableAt = '".$companyName."' "; 
    				$agentContent = selectMultiRecords($agentQuery);
    	
    	?>
    	<select name="custAgent" style="font-family:verdana; font-size: 11px; width:100">
    	
		  <option value=""> - Select Agent - </option> 

		 <? for($k=0;$k < count($agentContent);$k++){ ?>
		  			
		  				<option value="<? echo $agentContent[$k]["localServerId"]; ?>"><? echo $agentContent[$k]["name"]; ?> </option> 
		  
		  <? } ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.form2.custAgent, "<?=$custAgent?>");
        </script>
        </td>
    <td width="11%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">Commision at </td>
    <td><select name="commisionType">
      <option value="">-Select-</option>
      <option value="exclude" <? echo ($commisionType == "exclude"? "selected" : "")?> >At Source</option>
      <option value="include" <? echo ($commisionType == "include"? "selected" : "")?>>End Of Month</option>
        </select></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="10">&nbsp;</td>
  </tr>
  
<tr><td colspan="10" align="center">
		<input type="hidden" name="companyName" value="<?=$companyName?>">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
  </table>
</fieldset>

  <table width="100%" border="0" cellpadding="3" cellspacing="3" border="0" bgcolor="#EFEFEF">
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#005b90><b>Agent Name</b></font></div></td>
    
    <td bordercolor="#000000" bgcolor="#FFFFFF" ><div align="center"><font color=#005b90><b>Agent Number </b></div></td>
    <td bordercolor="#000000" bgcolor="#FFFFFF" ><div align="center"><font color=#005b90><b>Net Balance</b></div></td>
    
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#005b90><b>Number Of Transactions</b></div></td>
    
 
    
    
    
    
    
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#005b90><b>Commision Earned </div></td>
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#005b90><b>Commision to Pay </div></td>
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#005b90><b>total commision </div></td>
    <td bordercolor="#000000" bgcolor="#FFFFFF"><div align="center"><font color=#5b9000><b>Agent commision </div></td>
  </tr>
 <? 
 $commissionShare = selectFrom("select commissionShare from jointClients where clientId = '".$companyName."'"); 
 
 
 $totalOutgoing = count($outgoingTrans) + count($outgoingCancel);
 
 $cancelCount = array();
  for($j=0;$j < count($outgoingTrans);$j++){ 
  	
  	$imFee = 0;
  	$commisionToPay =  0; 
	$commisionEarned = 0;
	$totalCommision =  0;
  $netOutGoing = 0;	
 //$outGoingTotalAmount += $outgoingTrans[$j]["totalAmount"]; 
 $imFee = $outgoingTrans[$j]["totalSharedComm"];////////as it describes the total fee - commission
 $commisionToPay =  $outgoingTrans[$j]["remoteComm"];
 $outgoingCounter = $outgoingTrans[$j]["noOfTransaction"];
 $outgoingAgentCommission = $outgoingTrans[$j]["AgentComm"];
 //$commShare = $outgoingTrans[$i]["commissionShare"];
 //$clientName= $outgoingTrans[$i]["clientName"];
 /*
 totalSharedComm, sum(st.remoteComm) as remoteComm 
 */
$agentInfo = selectFrom("select name, username from admin where userID = '".$outgoingTrans[$j]["userID"]."'");
         
 	$netOutGoing = $outgoingTrans[$j]["principalAmount"] + $outgoingTrans[$j]["remoteComm"];
	
	
	$counter = 0;
	foreach($outgoingCancel as $arrayToCheck)
	{
			//print_r($arrayToCheck);
			if($arrayToCheck["userID"] == $outgoingTrans[$j]["userID"])
			{
		
				$cancelCount[] = $counter;
				$outgoingCounter --;
					$netOutGoing = $netOutGoing - $arrayToCheck["principalAmount"];
					if($arrayToCheck["refundFee"] != "No")
					{
							$netOutGoing = $netOutGoing - $arrayToCheck["remoteComm"];
							$imFee = $imFee - $arrayToCheck["totalSharedComm"];////////as it describes the total fee - commission
							$commisionToPay = $commisionToPay - $arrayToCheck["remoteComm"];
							$outgoingAgentCommission = $outgoingAgentCommission - $arrayToCheck["AgentComm"];
					}
			 }
		 	$counter ++;
	 }


	$commisionEarned = $imFee - $commisionToPay;
  $totalCommision = $commisionToPay + $commisionEarned;
  
  
 ?>
 

 
   <tr> 
   
       				<td bgcolor="#FFFFFF"><? echo $agentInfo["name"]; ?></td>
					    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					    	<? echo $agentInfo["username"]; ?></a></td>
					    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					    	<? echo number_format($netOutGoing,2,'.',','); ?></a></td>
					    <td bgcolor="#FFFFFF"><? echo $outgoingCounter ; ?></td>
					    
 
    
 
    <td bgcolor="#FFFFFF"><? echo number_format($commisionEarned,2,'.',',');?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($commisionToPay,2,'.',',');?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($totalCommision,2,'.',','); ?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($outgoingAgentCommission,2,'.',','); ?></td>
    
    </tr>
<? } ?> 
  
  
  <?
  /*************This part will be only if cancelled are there but not normal entries****************/
  if(count($cancelCount) < count($outgoingCancel))
  {
  	
  	 for($j=0;$j < count($outgoingCancel);$j++){ 
  	 	$flag = false;
  	 		
  	 			if(array_search($j,$cancelCount) === false)
  	 			{
  	 				$flag = true;	
  	 			}
  	 		
		  		if($flag)
		  		{
		  			$outgoingCounter = 0;
				  	$imFee = 0;
				  	$commisionToPay =  0; 
					$commisionEarned = 0;
					$totalCommision =  0;
				  $netOutGoing = 0;	
				  $outgoingAgentCommission = 0;
				 //$outGoingTotalAmount += $outgoingTrans[$j]["totalAmount"]; 
				 //$commShare = $outgoingTrans[$i]["commissionShare"];
				 //$clientName= $outgoingTrans[$i]["clientName"];
				 /*
				 totalSharedComm, sum(st.remoteComm) as remoteComm 
				 */
				$agentInfo = selectFrom("select name, username from admin where userID = '".$outgoingCancel[$j]["userID"]."'");
			 
					
							
							$outgoingCounter --;
								$netOutGoing = $netOutGoing - $outgoingCancel[$j]["principalAmount"];
								if($outgoingCancel[$j]["refundFee"] != "No")
								{
										$netOutGoing = $netOutGoing - $outgoingCancel[$j]["remoteComm"];
										$imFee = $imFee - $outgoingCancel[$j]["totalSharedComm"];////////as it describes the total fee - commission
										$commisionToPay = $commisionToPay - $outgoingCancel[$j]["remoteComm"];
										$outgoingAgentCommission = $outgoingAgentCommission - $arrayToCheck["AgentComm"];
								}
						 	
					
				
				
					$commisionEarned = $imFee - $commisionToPay;
				  $totalCommision = $commisionToPay + $commisionEarned;
				  
				  
				 ?>
				 
				
				 
				   <tr> 
				   
				       				<td bgcolor="#FFFFFF"><? echo $agentInfo["name"]; ?></td>
									    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
									    	<? echo $agentInfo["username"]; ?></a></td>
									    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
									    	<? echo number_format($netOutGoing,2,'.',','); ?></a></td>
									    <td bgcolor="#FFFFFF"><? echo $outgoingCounter ; ?></td>
									    
				 
				    
				 
				    <td bgcolor="#FFFFFF"><? echo number_format($commisionEarned,2,'.',',');?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($commisionToPay,2,'.',',');?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($totalCommision,2,'.',','); ?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($outgoingAgentCommission,2,'.',','); ?></td>
				    
				    </tr>
				<? }  
			}
		  	
  	}
  	/*************This part will be only if cancelled are there but not normal entries****************/
  ?>
  
  
 <? 
 $cancelCount2 = array();
 
 for($j=0;$j < count($baseAgentTrans);$j++){ 
 	$baseAgentCounter = 0;
 	$imFee = 0;
 	$totalIncomingAmount = 0;
	$commisionToPay =  0; 
	$commisionEarned = 0;
	$totalCommision =  0;
	$baseAgentCommission = 0;
		 $imFee = $baseAgentTrans[$j]["totalCommission"];////////as it describes the total fee - commission 	
		 
 		$totalIncomingAmount = $baseAgentTrans[$j]["principalAmount"] + $baseAgentTrans[$j]["totalSharedComm"] - $baseAgentTrans[$j]["remoteComm"];
 	//	echo("Incoming Amount".$totalIncomingAmount);
 		$commisionToPay =  $baseAgentTrans[$j]["remoteComm"];
 		$baseAgentCounter = $baseAgentTrans[$j]["noOfTransaction"];
 		$baseAgentCommission = $baseAgentTrans[$j]["AgentComm"];
 		$counter2 = 0;
 		foreach($baseAgentCancel as $arrayToCheck)
		{
		//	print_r($arrayToCheck);
			if($arrayToCheck["userID"] == $baseAgentTrans[$j]["userID"])
			{
			
				$cancelCount2[] = $counter2;
				$baseAgentCounter --;
					$totalIncomingAmount = $totalIncomingAmount - $arrayToCheck["principalAmount"];
			//			echo("principle amount is ".$arrayToCheck["principalAmount"]."and amount is ".$totalIncomingAmount);
					if($arrayToCheck["refundFee"] != "No")
					{
				//		echo("commission valvues are".$arrayToCheck["totalSharedComm"]." and ". $arrayToCheck["remoteComm"]);
							$totalIncomingAmount = $totalIncomingAmount - $arrayToCheck["totalSharedComm"] + $arrayToCheck["remoteComm"];
							
							$imFee = $imFee - $arrayToCheck["totalSharedComm"];////////as it describes the total fee - commission
							$commisionToPay = $commisionToPay - $arrayToCheck["remoteComm"];
							$baseAgentCommission = $baseAgentCommission - $arrayToCheck["AgentComm"];
					}
					//echo(" updated incoming amount =".$totalIncomingAmount);
			 }	
			 $counter2++;
		 }
 		
 		
 		
 		
 		
    $commisionEarned = $imFee - $commisionToPay;
    $totalCommision = $commisionToPay + $commisionEarned;
 	
 	?>
 	
 	 <tr> 
   
       				<td bgcolor="#FFFFFF"><? echo $baseAgentTrans[$j]["name"]; ?></td>
					    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=N&otherServer=<?=$companyName?>&agentID=<?=$baseAgentTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					    	<? echo $baseAgentTrans[$j]["userName"]; ?></a></td>
					    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=N&otherServer=<?=$companyName?>&agentID=<?=$baseAgentTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
					    	<? echo ("-".number_format($totalIncomingAmount,2,'.',',')); ?></a></td>
					    <td bgcolor="#FFFFFF"><? echo $baseAgentCounter ; ?></td>
					    
 
    
 
    <td bgcolor="#FFFFFF"><? echo number_format($commisionEarned,2,'.',',');?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($commisionToPay,2,'.',',');?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($totalCommision,2,'.',','); ?></td>
    <td bgcolor="#FFFFFF"><? echo number_format($baseAgentCommission,2,'.',','); ?></td>
    </tr>
 	
<? } ?> 

  <?
  /*************This part will be only if cancelled are there but not normal entries****************/
  if($cancelCount < count($baseAgentCancel))
  {
  	
  	 for($j=0;$j < count($baseAgentCancel);$j++){ 
  	 	$flag = true;
  	 		for($l = 0; $l < count($baseAgentTrans); $l++)
  	 		{
  	 			if($baseAgentCancel[$j]["userID"] == $baseAgentTrans[$l]["userID"])
  	 			{
  	 				$flag = false;	
  	 			}
  	 		}
		  		if($flag)
		  		{
				  	$imFee = 0;
				  	$commisionToPay =  0; 
					$commisionEarned = 0;
					$totalCommision =  0;
				  $totalIncomingAmount = 0;
				  $baseAgentCounter= 0;	
				  $baseAgentCommission = 0;
				 //$outGoingTotalAmount += $outgoingTrans[$j]["totalAmount"]; 
				 //$commShare = $outgoingTrans[$i]["commissionShare"];
				 //$clientName= $outgoingTrans[$i]["clientName"];
				 /*
				 totalSharedComm, sum(st.remoteComm) as remoteComm 
				 */
				$agentInfo = selectFrom("select name, username from admin where userID = '".$baseAgentCancel[$j]["userID"]."'");
				 
					for($k = 0;  $k < count($baseAgentCancel); $k++)
					{
						if($baseAgentCancel[$k]["userID"] == $baseAgentCancel[$j]["userID"])
						{
							
							$baseAgentCounter --;
								$totalIncomingAmount = $totalIncomingAmount - $outgoingCancel[$k]["principalAmount"];
								if($outgoingCancel[$k]["refundFee"] != "No")
								{
										$totalIncomingAmount = $totalIncomingAmount - $baseAgentCancel[$k]["remoteComm"];
										$imFee = $imFee - $baseAgentCancel[$k]["totalSharedComm"];////////as it describes the total fee - commission
										$commisionToPay = $baseAgentCancel - $baseAgentCancel[$k]["remoteComm"];
										$baseAgentCommission = $baseAgentCommission - $baseAgentCancel[$k]["AgentComm"];
								}
						 }	
					 }
				
				
					$commisionEarned = $imFee - $commisionToPay;
				  $totalCommision = $commisionToPay + $commisionEarned;
				  
				  
				 ?>
				 
				
				 
				   <tr> 
				   
				       				<td bgcolor="#FFFFFF"><? echo $agentInfo["name"]; ?></td>
									    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
									    	<? echo $agentInfo["username"]; ?></a></td>
									    <td bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('sharedTransactionDetail.php?<?=$qryString?>&generatedLocaly=Y&otherServer=<?=$companyName?>&agentID=<?=$outgoingTrans[$j]["userID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')">
									    	<? echo number_format($totalIncomingAmount,2,'.',','); ?></a></td>
									    <td bgcolor="#FFFFFF"><? echo $baseAgentCounter ; ?></td>
									    
				 
				    
				 
				    <td bgcolor="#FFFFFF"><? echo number_format($commisionEarned,2,'.',',');?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($commisionToPay,2,'.',',');?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($totalCommision,2,'.',','); ?></td>
				    <td bgcolor="#FFFFFF"><? echo number_format($baseAgentCommission,2,'.',','); ?></td>
				    </tr>
				<? }  
			}
		  	
  	}
  	/*************This part will be only if cancelled are there but not normal entries****************/
  ?>





</table>

</form>
</body>
</html>