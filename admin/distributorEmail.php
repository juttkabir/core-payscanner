<?
	session_start();
	include ("../include/config.php");
	include ("security.php");

	/**
	 * Variables from Query String
	 */
	$cmd 			= $_REQUEST["cmd"];
	$id				= $_REQUEST["id"];
	$distributor 	= $_REQUEST["distributor"];
	$emailTo 		= $_REQUEST["emailTo"];
	$emailCC		= $_REQUEST["emailCC"];
	$emailBCC		= $_REQUEST["emailBCC"];

	/**
	 * Some pre data preparation.
	 */
	$emailTo		= trim($emailTo);
	$emailCC		= trim($emailCC);
	$emailBCC		= trim($emailBCC);
	
	if($cmd == "ADD")
	{
		if(!empty($distributor) && !empty($emailTo))
		{
			$sql = "SELECT
						id
					FROM
						distributor_email
					WHERE
						distributorId = ".$distributor."
					";
			$result = mysql_query($sql) or die(mysql_error());
			$numrows = mysql_num_rows($result);
			
			if($numrows <= 0)
			{
				$sql = "INSERT INTO
							distributor_email
							(
								distributorId,
								emailTo,
								emailCC,
								emailBCC,
								created
							)
						VALUES
						(
							".$distributor.",
							\"".$emailTo."\",
							\"".$emailCC."\",
							\"".$emailBCC."\",
							NOW()
						)
						";
				$result = mysql_query($sql) or die(mysql_error());
				$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
				unset($cmd, $distributor, $emailTo, $emailCC, $emailBCC);
			} else {
				$errMsg = "<span class='errorMessage'>Record already exists, please use a different distributor. Or you may edit the existing record to update the values.</span>";
			}
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "UPDATE")
	{
		if(!empty($id) && !empty($distributor) && !empty($emailTo))
		{
			$sql = "UPDATE
						distributor_email
					SET
						emailTo = \"".$emailTo."\",
						emailCC = \"".$emailCC."\",
						emailBCC = \"".$emailBCC."\",
						updated = NOW()
					WHERE
						id = ".$id."
					";
			$result = mysql_query($sql) or die(mysql_error());
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
			unset($cmd, $distributor, $emailTo, $emailCC, $emailBCC, $id);
		} else {
			$errMsg = "<span class='errorMessage'>One or more required fields are empty.</span>";
		}
	}
	
	if($cmd == "EDIT")
	{
		if(!empty($id))
		{
			$sql = "SELECT
						distributorId,
						emailTo,
						emailCC,
						emailBCC
					FROM
						distributor_email
					WHERE
						id=".$id."
					";
			$result = mysql_query($sql) or die(mysql_error());
			$numrows = mysql_num_rows($result);
			
			if($numrows > 0)
			{
				while($rs = mysql_fetch_array($result))
				{
					$distributor	= $rs["distributorId"];
					$emailTo		= $rs["emailTo"];
					$emailCC		= $rs["emailCC"];
					$emailBCC		= $rs["emailBCC"];
				}
				
				$cmd = "UPDATE";
			} else {
				$errMsg = "<span class='errorMessage'>No record found, please select a valid record.</span>";
			}
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for editing.</span>";
		}
	}
	
	if($cmd == "DELETE")
	{
		if(!empty($id))
		{
			$sql = "DELETE FROM
						distributor_email
					WHERE
						id=".$id."
					";
			$result = mysql_query($sql) or die(mysql_error());
			$errMsg = "<span class='successMessage'>Operation executed successfully.</span>";
			unset($cmd, $id);
		} else {
			$errMsg = "<span class='errorMessage'>Please select a valid record for deletion.</span>";
		}
	}
	
	switch($cmd)
	{
		case "UPDATE":
			$disableDistributor = "disabled=\"disabled\"";
			$hiddenDistributor = "<input type=\"hidden\" name=\"distributor\" value=\"".$distributor."\" />";
			break;
			
		default:
			$cmd = "ADD";
			break;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Distributor Email</title>
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.cluetip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.2.5.min.js"></script>
<script type="text/javascript" src="jquery.cluetip.js"></script>
<script type="text/javascript">
function exportAndSendEmail()
{
	$.ajax
	({
		type: "GET",
		url: "../scheduled/exportTransactionSummary.php",
		data: "",
		beforeSend: function(XMLHttpRequest)
		{
			$("#status").text("Processing...");
		},
		success: function(responseText)
		{
			$("#status").html(responseText);
		}
	});
}

$(document).ready(function() {
	$('a').cluetip({splitTitle: '|'});
});
</script>
</head>
<body>
<table width="600" border="0" align="center" cellpadding="10" cellspacing="0" id="boundingBox">
	<tr>
		<td><p class="pageTitle">Distributor Email </p>
			<p class="description">This screen allows to define email address(es) for distributors. The information defined here is used in the automated procedure which sends &quot;Authorized&quot; and &quot;Amended&quot; transactions via email to selected distributor - to defined email address(es). The transactions are exported in a Microsoft Excel (.xls) format file and the file is attached with the email. </p>
			
			<?
				if(!empty($errMsg))
				{
					echo "<p>".$errMsg."</p>";
				}
			?>
			
		<form id="form1" name="form1" method="post" action="distributorEmail.php">
			<table width="100%" border="0" cellspacing="0" cellpadding="10">
				<tr>
					<td colspan="2" valign="top"><p><strong>Distributor</strong> <span class="mandatory">*</span> <br />
						<select name="distributor" id="distributor" title="Select a distributor." <?=$disableDistributor?>>
							<option value="">-- Select -- </option>
							<?
								$sql = "SELECT
											userID, username, name
										FROM
											admin
										WHERE
											isCorrespondent = 'ONLY'
											AND parentID > 0
											AND adminType = 'Agent'
										ORDER BY
											name
										";
								$result = SelectMultiRecords($sql);
								$numrows = sizeof($result);
								
								for($i=0; $i<$numrows; $i++)
								{
									if($distributor == $result[$i]["userID"])
									{
										$selected = " selected=\"selected\"";
									} else {
										$selected = "";
									}
									
									echo "<option value=\"".$result[$i]["userID"]."\"".$selected.">".$result[$i]["name"]." [".$result[$i]["username"]."]</option>";
								}
							?>
						</select>
						<?
							if(!empty($hiddenDistributor))
							{
								echo $hiddenDistributor;
							}
						?>
						<a href="#" title="Distributor|Shows a list of available distributors. You can select one to enter the email address(es) for.">[?]</a></p>
						<p><strong>Email Address</strong> - (single email address only)<span class="mandatory"> *</span><br />
							<input name="emailTo" type="text" class="inputBoxesText" id="emailTo" title="Recipient's Email Address" value="<?=$emailTo?>" size="70" maxlength="255" />
								<a href="#" title="Email Address|A valid email address of recipient. Enter only one email address, this will be used as TO field of a standard email.">[?]</a></p>
						<p><strong>CC Email Address(es)</strong>  - (if more than one, separate with comma)<br />
							<input name="emailCC" type="text" class="inputBoxesText" id="emailCC" title="CC Email Address(es) of Recipient(s)" value="<?=$emailCC?>" size="70" maxlength="255" />
								<a href="#" title="CC Email Address(es)|You can enter more than one email addresses - separated by comma, to send a copy of email to.">[?]</a></p>
						<p><strong>BCC Email Address(es)</strong> -  (if more than one, separate with comma)<br />
							<input name="emailBCC" type="text" class="inputBoxesText" id="emailBCC" title="BCC Email Address(es) of Recipient(s)" value="<?=$emailBCC?>" size="70" maxlength="255" />
								<a href="#" title="BCC Email Address(es)|You can enter more than one email addresses - separated by comma, to send a blind copy of email to. In this technique the list of recipients isn't exposed and the email is pretended to be sent separately for every recipient defined here.">[?]</a></p>
						</td>
					</tr>
				<tr>
					<td width="50%" valign="top"><span id="status" class="statusMessage"></span></td>
					<td width="50%" align="right" valign="top">
						<?
							if($cmd != "ADD")
							{
						?>
								<a href="distributorEmail.php" title="System|Click to cancel the current edit mode and to refresh the screen for insertion of new record.">Insert New Record</a>
						<?
							}
						?>
					</td>
				</tr>
				<tr>
					<td class="buttonsToolbar"><input type="submit" value="Submit" title="Click to save the changes." />
						<input type="reset" value="Reset" title="Click to revert back current changes." />
						<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
						<input name="id" type="hidden" id="id" value="<?=$id?>" /></td>
					<td align="right" class="buttonsToolbar">
						<input name="sendEmail" type="button" id="sendEmail" value="Export &amp; Send Email" onclick="exportAndSendEmail()" title="Click to send email." />
						<a href="#" title="Export &amp; Send Email|By clicking this button you can manually initiate the same procedure which exports transactions and sends via email automatically to distributors listed below.">[?]</a>
					</td>
				</tr>
			</table>
			</form>
			<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="25%" class="reportHeader">Distributor</td>
					<td width="55%" class="reportHeader">Email Addresses </td>
					<td width="20%" class="reportHeader">&nbsp;</td>
				</tr>
				
				<?
					$sql = "SELECT
								d.id,
								a.username,
								a.name,
								d.emailTo,
								d.emailCC,
								d.emailBCC
							FROM
								distributor_email d,
								admin a
							WHERE
								d.distributorID = a.userID
							";
					
					$result = mysql_query($sql);
					$numrows = mysql_num_rows($result);
					
					while($rs = mysql_fetch_array($result))
					{
						$fId				= $rs["id"];
						$fUserName			= $rs["username"];
						$fName				= $rs["name"];
						$fEmailTo			= $rs["emailTo"];
						$fEmailCC			= $rs["emailCC"];
						$fEmailBCC			= $rs["emailBCC"];
				?>
						<tr>
							<td class="list"><?=$fName." [".$fUserName."]"?></td>
							<td class="list">
								<b>To: </b><?=$fEmailTo?><br />
								<b>CC: </b><?=$fEmailCC?><br />
								<b>BCC: </b><?=$fEmailBCC?>
							</td>
							<td align="right" class="list">
								<a href="distributorEmail.php?id=<?=$fId?>&amp;cmd=EDIT" title="System|Click to edit/modify the record">Edit</a> | 
								<a href="distributorEmail.php?id=<?=$fId?>&amp;cmd=DELETE" title="System|Click to delete the record">Delete</a>
							</td>
						</tr>
				<?
					}
				?>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
