<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$systemCode = SYSTEM_CODE;
///////////////////////History is maintained via method named 'activities'


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPI" && $agentType != "SUPAI")
		{ 
			$condition = True;	
		}
	}else{
		
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager" || $agentType == "SUPI")
			{
				 $condition = True;
			}
		
		}
	
		
//////////////////////////Making of Condition///////////////




session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");


if($_POST["Submit"]=="Search")
{
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";		
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
}

if ($_POST['destcurrency'] != "") {
	$destcurrency = $_POST['destcurrency'] ;	
} else if ($_GET['destcurrency'] != "") {
	$destcurrency = $_GET['destcurrency'] ;	
}

//session_register["agentID2"];
$_SESSION["agentID2"] = "";

if($_POST["agentID"]!=""){
	
	$_SESSION["agentID2"] = $_POST["agentID"];
	
}elseif($_GET["agentID"]!=""){
	
	$_SESSION["agentID2"]=$_GET["agentID"];
	
}elseif($agentType == 'SUPI'){
	
	$_SESSION["agentID2"] = $changedBy;
	
	
	}
if ($offset == "")
	$offset = 0;
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
?>
<html>
<head>
	<title>Distributor Account Statement</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function validForm(theForm) {
	if (theForm.agentID.selectedIndex == 0) {
		alert("Please select Distributor")	;
		theForm.agentID.focus() ;
		return false ;
	}
	return true ;
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Distributor Account Statement</td>
  </tr>
  <tr>
  <td width="563">
			<form action="bank_Account.php" method="post" name="Search" onSubmit="return validForm(this);">
        <div align="center">
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

		 <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script><br><br>
       <?    
       
       //if($agentType == "admin" || $agentType == "Admin Manager")//101
       if($condition)
			  {
			  ?>
			  Distributor &nbsp;
			  <select name="agentID" style="font-family:verdana; font-size: 11px" <? if (DEST_CURR_IN_ACC_STMNTS == "1") { ?>onchange="document.Search.submit();"<? } ?>>
				<option value="">- Select Distributor -</option>
					<? if($agentType != "SUPI Manager" && DEST_CURR_IN_ACC_STMNTS != "1") {?>
				<!--option value="All">- All Banks -</option-->
				<?
			    }
					$linkedDistributors = "";
					$linkedDistributor = selectFrom("select linked_Distributor from ".TBL_ADMIN_USERS." where userID = $changedBy");
					$arrLinkedDistributor = explode(",", trim($linkedDistributor["linked_Distributor"]));
					for ($i = 0; $i < count($arrLinkedDistributor); $i++){
						$row = $arrLinkedDistributor[$i];
						if ($i === count($arrLinkedDistributor) - 1) {
							$linkedDistributors .= "'".$row."'";
						} else {
							$linkedDistributors .= "'".$row."', ";
						}
					}
						$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'ONLY' and userID IN ($linkedDistributors) order by agentCompany");

						for ($i=0; $i < count($agents); $i++){
					?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
						}
					?>
			  </select>
			  <script language="JavaScript">
				SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
									</script>
		<? }?>
		<?	if (DEST_CURR_IN_ACC_STMNTS == "1" && $_SESSION["agentID2"] != "") {  ?>
					<br><br>
					Currency &nbsp;
					<? 
					if ($_SESSION["agentID2"] != "") {
										
						
						if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1'){
							 $settlementCurr   = selectFrom("select userID,settlementCurrency from ".TBL_ADMIN_USERS." where userID = '".$_SESSION["agentID2"]."' ");	
              
						}else{
							
							 //$query = "select distinct(currency) from exchangerate where rateFor = 'settlement' and rateValue = '".$_SESSION["agentID2"]."'";
						   	$queryCurrency1 = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																  WHERE c.countryId = s.toCountryId " ;
						   $queryCurrency = selectMultiRecords($queryCurrency1);
						}
						$distIDACountries = selectFrom("SELECT `IDAcountry` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_SESSION["agentID2"]."'") ;						
						$arrDistCountries = explode(",", $distIDACountries['IDAcountry']);
						$strCountries = implode ("','", $arrDistCountries);
					  $query = "select DISTINCT(s.currencyRec) from countries as c, services as s where c.countryId = s.toCountryId and c.countryName IN ('".$strCountries."') ";
					  $queryCurrency = selectMultiRecords($query);
					  
						$strCurrency = "";
						for($j = 0; $j < count($queryCurrency); $j++)
						{
							if($j > 0)
								
								$strCurrency .= ", ";
								$strCurrency .= trim($queryCurrency[$j]["currencyRec"]);
						}
														
					$arrCurrency = explode (", ",$strCurrency);
					$uniqueCurrencyList1 = array_unique($arrCurrency);	
					
				 foreach($uniqueCurrencyList1 as $value)
					{
					     $uniqueCurrencyList[]=$value;
				  }
				
				  
					} else {

						$queryCurrency = "SELECT DISTINCT(c.currency) FROM ".TBL_COUNTRY." AS c , ".TBL_SERVICE." AS s 
																WHERE c.countryId = s.toCountryId " ;
					} ?>
					
					
					
					<select name="destcurrency" style="font-family:verdana; font-size: 11px; width:130">
			    
			     <?  if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1'){ ?>
			      
							 <option value="<?=trim($settlementCurr["settlementCurrency"])?>"><?=trim($settlementCurr["settlementCurrency"])?></option>	
							
			     	<?  
			      }else{
						
						for($k = 0; $k < count($uniqueCurrencyList); $k++)
						{?>
							 <option value="<?=trim($uniqueCurrencyList[$k])?>"><?=trim($uniqueCurrencyList[$k])?></option>	
						<? 
						}
					}	
						
				?>
		</select>
		<? 
			//echo $arrDistCountries[4];
	
			//echo $currencyData[1];
		
		?>
		<script language="JavaScript">SelectOption(document.forms[0].destcurrency, "<?=$destcurrency?>");
			</script>
		
		&nbsp;&nbsp;
		<?	}  ?>
          <input type="submit" name="Submit" value="Search">
        </div>
      </form> 
		</td>
		<tr>
		
    <td> 
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["agentID"]!="")
			$slctID=$_GET["agentID"];
		elseif($_GET["agentID"]=="") 
			{
				$slctID=$_POST["agentID"];
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
			}
					
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			
		//	if($slctID!='')
				{
									
					//if($agentType == "admin" || $agentType == "Admin Manager")//101
					if($condition)
					{
						
						if($slctID!='')
						{
							
							$accountQuery = "Select * from bank_account where 1";
							$accountQueryCnt = "Select count(*) from bank_account where 1";
							if($slctID!='All')
							{
								$accountQuery .= " and bankID = $slctID";
								$accountQueryCnt .= " and bankID = $slctID";
							}
							if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
								$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
								$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
							}
							$accountQuery .= " and $queryDate";	
							$accountQueryCnt .= " and $queryDate";
							
							//$contentsAcc = selectMultiRecords($accountQuery);
			
					$allCount = countRecords($accountQueryCnt);
					
					//echo $queryBen;
				  $accountQuery .= " LIMIT $offset , $limit";
					$contentsAcc = selectMultiRecords($accountQuery);
						}
					
							 
					}else
					{
						
						$accountQuery="Select * from bank_account where 1";
						$accountQuery.= " and bankID = $changedBy";
						if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
								$accountQuery .= " and (currency = '$destcurrency' OR currency = '')";
						}
						$accountQuery .= " and $queryDate";	
						
						$accountQueryCnt = "Select count(*) from bank_account where 1";
						$accountQueryCnt .= " and bankID = $changedBy";
						if ($destcurrency != "" && DEST_CURR_IN_ACC_STMNTS == "1") {
								$accountQueryCnt .= " and (currency = '$destcurrency' OR currency = '')";
						}
						$accountQueryCnt .= " and $queryDate";
						
						
						//$contentsAcc = selectMultiRecords($accountQuery);
			
					$allCount = countRecords($accountQueryCnt);
					//echo $queryBen;
					$accountQuery .= " LIMIT $offset , $limit";
					$contentsAcc = selectMultiRecords($accountQuery);
					}
					//if( $_SESSION["tYear"]!="" && $fromDate < $toDate)
						//{
									
						//}
					
					
				}		
	?>
      <div align="center">
        <table width="758" height="278" border="1" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC"> <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
                <tr> 
                  <td align="left">
                    <?php if (count($contentsAcc) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                 <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=first";?>"><font color="#005b90">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> </td>
               <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                   <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&destcurrency=".$destcurrency."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
              <?php } ?>
                </tr>
              </table></td>
          </tr>
          <?
          $preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
		if(count($contentsAcc) > 0)
		{
		?>
		
		<tr>
			<td>
				<table>
						<? if(CONFIG_DISTRIBUTOR_STATEMENT_CUMULATIVE == "1"){?>
					<tr>
						<td width="300">
							Cumulative Total <? //$allAmount=bankBalanceDate($slctID, $fromDate, $toDate);  
							
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							//echo "select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'";
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>
			</td>
			
		</tr>
		<? } ?>
			</table>
			</td>
		</tr>
		
		
		
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> <table width="809" height="181" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td align="left" ><span class="style1">Date</span></td>
                   <td align="left" ><span class="style1"><? echo $systemCode; ?></span></td>
                  <td align="left"><span class="style1">Modified By</span></td>
                  <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){ ?>
                  <td align="left"><span class="style1">Opening Balance</span></td>
                  <? }?>
                  <td align="left"><span class="style1">Money In</span></td>
                  <td width="120" align="left"><span class="style1">Money Out</span></td>
				  <td width="120" align="left"><span class="style1">Description</span></td>
                  <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){ ?>
                  <td align="left"><span class="style1">Closing Balance</span></td>
                  <? }?>
                  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
                  <td width="120" align="left"><span class="style1"><? echo $manualCode;?></span></td>
                  <?	}  ?>
                </tr>
                <? 
				
				$strPreviousDate = "";
				$strTodayDate = "";
				
				for($i=0;$i<count($contentsAcc);$i++)
				{
					$strTodayDate = $contentsAcc[$i]["dated"];
				
				$cumulativeCurr=$contentsAcc[$i]["currency"];
				
			//$BeneficiryID = $contentsBen[$i]["benID"];
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="230" align="left"><strong><font color="#006699">
                    <? 
			  	$authoriseDate = $contentsAcc[$i]["dated"];
			  	echo date("F j, Y", strtotime("$authoriseDate"));
			  	?>
           </font></strong></td>
            <?  
             if($contentsAcc[$i]["TransID"] > 0) {
					$trans = selectFrom("SELECT refNumber,refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'");
					//echo "SELECT refNumber,refNumberIM FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'";
					 ?>
         <td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>      
               
               <? }else{?>
			      	<td>&nbsp;</td>
			
				   <? }?>
               <td width="231" align="left">   
                  <!-- <td width="231" align="left"> 
                 <? echo  $trans["refNumberIM"]?>
                  </td> -->
                    <? 
                    if(strstr($contentsAcc[$i]["description"], "by Teller"))
										{
											$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
											
										}
										else
                    $q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); echo $q["name"]; //echo $contentsAcc[$i]["modified_by"];  ?>
                  </td>
                  <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){
                  	
                  			$currDate = $contentsAcc[$i]["dated"];
                  			
							/*if($currDate != $preDate)
							{*/
									if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT== "1" && $settleCurr!='') {
	 								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ");
	 								//echo "select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate' and currency='".$settleCurr."' ";
							  }else{
							  	$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
							  	//echo "select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'";
							  	}
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								
								 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
								  $account1 .= " and  user_id = '".$slctID."' ";	
								  if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') {
								  $account1 .= " and  currency='".$settleCurr."' ";		
								  }
								  			
									$contents1 = selectFrom($account1);
									
									if($currDate == $FirstDated){
										 $openingBalance =  $contents1["opening_balance"];
								  
									}else{
										$openingBalance =  $contents1["closing_balance"];
								
									}
								}
						//	}
									
								
								 //$closingBalance = $openingBalance;
								
								 $preDate = $currDate;
								
                  	
                  	
                  	 ?>
                  <td width="231" align="left">
				  	<? 
						if($strTodayDate == $strPreviousDate)
						{
							
							echo "&nbsp;";
						}
						else
						{
							echo $openingBalance;
							$strPreviousDate = $strTodayDate;
						}
					?>
				  </td>
                  <? }?>
                  <td width="210" align="left">
                    <?  
			  	if($contentsAcc[$i]["type"] == "DEPOSIT")
			  	{
			  		
			  		if (DEST_CURR_IN_ACC_STMNTS == "1") {
				  		echo $contentsAcc[$i]["currency"] . " " ;
			  		}
			  		echo customNumberFormat($contentsAcc[$i]["amount"]);
					$Balance = $Balance+$contentsAcc[$i]["amount"];
			  }
			  	?>
                  </td>
                  <td align="left">
                    <? 
			  	if($contentsAcc[$i]["type"] == "WITHDRAW")
			  	{

					if (DEST_CURR_IN_ACC_STMNTS == "1") {
						echo $contentsAcc[$i]["currency"] . " " ;
					}
					
					
					echo customNumberFormat($contentsAcc[$i]["amount"]);
					$Balance = $Balance-$contentsAcc[$i]["amount"];

			  	}
			  	
			  	?>
                  </td>
    			  <td align="left">
				  	<?=$contentsAcc[$i]["description"]?>
				  </td>	
          
          <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){
          	
          		if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
				   		 
			    	 	$closingBalance =  $closingBalance - $contentsAcc[$i]["amount"];
					
					}elseif($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						$closingBalance =  $closingBalance + $contentsAcc[$i]["amount"];
						
					}
          	
          	
          	
          	
          	
          	
          	
          	 ?>
                  <td width="231" align="left"><? echo customNumberFormat($closingBalance); ?></td>
                  <? }?>
                  
            <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
           <td align="left"><? echo $trans["refNumber"] ?></td>
          <?	}  ?>
          
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
                  <td>&nbsp;</td>
                  <?	}  ?>
                  <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){?>
                  
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <? } ?>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="5" align="left"><b>Page Total Amount:</b>
                  <? echo  number_format($Balance,2,'.',','); ?></td>
                  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
                 <?	}  ?>
                 
                  <? if(CONFIG_DISTRIBUTOR_STATEMENT_BALANCE == "1"){?>
                  
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <? } ?>
                  <td>&nbsp;</td>
				  <td>&nbsp;</td>
                </tr>
                <tr>            		
                			<td colspan="9"> 
                <table>
                	<tr>
	                  <td class="style3" width="130"> Cumulative Total : </td>
	                  <td width="100"> 
                    <?
												if($_GET["total"]=="first")
												{
												$_SESSION["grandTotal"] = $Balance;
												$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="pre"){
													$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
													$_SESSION["CurrentTotal"]=$Balance;
												}elseif($_GET["total"]=="next"){
													$_SESSION["grandTotal"] += $Balance;
													$_SESSION["CurrentTotal"]= $Balance;
												}else
													{
													$_SESSION["grandTotal"] = $Balance;
													$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
													}
												echo number_format($_SESSION["grandTotal"],2,'.',',');
												if (DEST_CURR_IN_ACC_STMNTS == "1") {
												    echo" ".$cumulativeCurr;
											   }
												?>
												</td>
												<td >
														<? if ($_SESSION["grandTotal"] < 0 ){ ?>
																<font color="red"><strong><?=$company ?> has to pay to bank/distributor</strong></font>
																<? 
															}		
															if ($_SESSION["grandTotal"] > 0 )
															{		
															?>
															<strong><?=$company ?> Credit Balance  </strong>
															<? } ?>
													
													
												</td>
                </tr>
              </table>
                  </td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td colspan="9"> 
                    <!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->
                  </td>
                </tr>
              </table>
              <?
			} // greater than zero
			else
			{
			?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"> <table width="810" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td width="98" align="left"><span class="style1">Date</span></td>
                  <td width="231" align="left"><span class="style1">Modified By</span></td>
                  <td width="187" align="left"><span class="style1">Money In</span></td>
                  <td width="276" align="left" class="style1">Money Out</td>
                </tr>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <?
			}if($slctID!='')
				{
					if($agentType == "admin" || $agentType == "Admin Manager")//101
					{
						?>
					  <tr> 
						<td bgcolor="#c0c0c0"><span class="child"><a HREF="add_Bank_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&modifiedBy=<?=$_SESSION["loggedUserData"]["userID"];?>&destcurrency=<?=$destcurrency?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit 
						  Money</font></a></span></td>
					  </tr>
					  <? 
				  	}
				 }
				  ?>
        </table>
        <?	
	 }///End of If Search
	 ?>
      </div></td>
	</tr>
	
</table>
</body>
</html>