<?
session_start();
include ("../include/config.php");
include ("calculateBalance.php");
include("double-entry-functions.php");
include ("security.php");

$agentID = "";
if($_REQUEST["agentID"] != ""){
	$agentID = $_REQUEST["agentID"];
}

/* #8597 - AMB Exchange
 * When sender amount is typed then it is copied in receiving amount fields as well.
*/
$senderReceiverAmFlag = false;
if(CONFIG_SENDING_COPY_TO_RECEIVING_INTER_ACCOUNT == 1){
	$senderReceiverAmFlag = true;
}

$rateDigitsRight = false;
$roundLevel = "2";
$roundRateTo = "8";
if(defined("CONFIG_ROUND_LEVEL_RATE") && is_numeric(CONFIG_ROUND_LEVEL_RATE)){
	$rateDigitsRight = true;
	$roundRateTo = CONFIG_ROUND_LEVEL_RATE;
}
if(CONFIG_TRANS_ROUND_NUMBER == "1" && defined("CONFIG_TRANS_ROUND_LEVEL") && is_numeric(CONFIG_TRANS_ROUND_LEVEL)){
	$rateDigitsRight = true;
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}

$backUrl ="detail_account_transfer.php";
$created = date('Y-m-d  H:i:s');
$createdBy = $_SESSION["loggedUserData"]["userID"];
//debug($_REQUEST);
$sendingAc = $_REQUEST["sendingAc"];
$receivingAc = $_REQUEST["receivingAc"];
//debug ($receivingAc);

$_REQUEST["sendingCurrencyName"] = $_REQUEST["sendingCurrency"];
$_REQUEST["receivingCurrencyName"] = $_REQUEST["receivingCurrency"];

/**
 * Validation and Inserting the form entries into database
 */
 
if(!empty($_REQUEST["formSubmitted"]))
{
	/**
	 * Making the exchange rate entry in note field
	 */
	$noteToInsert = "";
	if($_REQUEST["sendingCurrency"] != $_REQUEST["recievingCurrency"])
		$noteToInsert = "( Exchange Rate: 1 ".$_REQUEST["sendingCurrency"]." = ".$_REQUEST["rate"]." ".$_REQUEST["recievingCurrency"]." ) ";
	$noteToInsert .= $_REQUEST["note"];

	if(!empty($_REQUEST["rate"]))
		$recevingAmount = $_REQUEST["rate"] * $_REQUEST["amount"];
	else
		$recevingAmount = $_REQUEST["amount"];
		
	$rateValue = $_REQUEST["rate"];
	if(empty($rateValue))
		$rateValue = number_format(($_REQUEST["ramount"] / $_REQUEST["amount"]),$roundRateTo,'.','');

	// get total fund transfers count and make transID for fund transfer
	$type = 'FT';
	$args = array('flag'=>'getTransID','type'=>$type);
	$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
	if(!empty($returnData['transID'])){
		$transID = $returnData['transID'];
		$transType = $type;
	}

	$arguments = array('flagTradingLedger'=>true,'transID'=>$transID,'amount'=>$_REQUEST["amount"],'ramount'=>$_REQUEST["ramount"],'sendingAc'=>$_REQUEST["sendingAc"],'receivingAc'=>$_REQUEST["receivingAc"],'sendingCurrency'=>$_REQUEST["sendingCurrency"],'receivingCurrency'=>$_REQUEST["receivingCurrency"],'note'=>addslashes($noteToInsert),'createdBy'=>$createdBy,'created'=>$created,'extraFields'=>array('transType'=>$transType));
	$acountLF = accountLedger($arguments);
	if($acountLF){
		$strInsertSql = "INSERT INTO ".TBL_ACCOUNT_DETAILS."  
											(
											  transID,
											  currencySell,
											  currencyBuy,
											  amountSell,
											  amountBuy,
											  accountSell,
											  accountBuy,
											  exchangeRate,
											  description,
											  created
											)
									 values
										   (
											 '".$transID."',
											 '".$_REQUEST["sendingCurrency"]."',
											 '".$_REQUEST["receivingCurrency"]."',
											 '".$_REQUEST["amount"]."',
											 '".$_REQUEST["ramount"]."',
											 '".$_REQUEST["sendingAc"]."',
											 '".$_REQUEST["receivingAc"]."',
											 '".$rateValue."',
											 '".addslashes($noteToInsert)."',
											 '".$created."'
											)";
		
		insertInto($strInsertSql);
		//$lastEntryId = @mysql_insert_id();
		
		redirect($backUrl.'?amount='.$_REQUEST["amount"].'&receivingAc='.$_REQUEST['receivingAc'].'&sendingAc='.$_REQUEST["sendingAc"].'&sendingCurrency='.$_REQUEST["sendingCurrency"].'&receivingCurrency='.$_REQUEST["receivingCurrency"].'&note='.$noteToInsert.'&receivingAmountCB='.$_REQUEST['receivingAmountCB'].'&sendingAmountCB='.$_REQUEST['sendingAmountCB'].'&acctitle='.$_REQUEST['acctitle']);
		$msg = "Funds Transfer Successfully";
		$stat = SUCCESS_COLOR;
		//$stat = SUCCESS_MARK;
	}else{
		$msg = "Funds Not Transfer";
		$stat = CAUTION_COLOR;
		//$stat = CAUTION_MARK;
	}	
}

	
	/**
	 * Seleting the currencies of agent from which amount will be with draw
	 */
	$sendingCBalance = 0;
	$receivingCBalance = 0;
	if(!empty($_REQUEST["sendingAc"])){
		$sqlSC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["sendingAc"]."' ";
		$sendingCurrData = selectMultiRecords($sqlSC);
		$sendCurrData = array();
		foreach($sendingCurrData as $k=>$v){
			$sendCurrData[] = $v['currency'];

			$strClosingBalance = "
					SELECT 
						id,
						closing_balance,
						dated,
						accountNumber 
					FROM 
						".TBL_ACCOUNT_SUMMARY." 
					WHERE
						accountNumber = '".$v["accounNumber"]."' AND
						dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
						WHERE accountNumber = '".$v["accounNumber"]."')
					 ";
				$closingBalance = selectFrom($strClosingBalance);
			
			/*
			If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
			If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
			*/
			//debug($accountRs[$k]['balance']);
			if(in_array($_REQUEST["sendingCurrency"],$sendCurrData)){
				if($v['balanceType'] == 'Dr')
					$sendingCBalance = ($closingBalance["closing_balance"] - $v['balance']);
				elseif($v['balanceType'] == 'Cr')
					$sendingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
			}
		}
		//debug($sendingCBalance."=".$crBalance);
		/*if(count($sendCurrData)>0){
			if(in_array($_REQUEST["sendingCurrency"],$sendCurrData)){
				 $_arrInput = array(
									  "accountNumber" =>$sendingAc,
									  "currency" => $_REQUEST["sendingCurrency"]
									);
				$sendingCBalance = getClosingBalance($_arrInput);	
			}
		}*/
	}
	if(!empty($_REQUEST["receivingAc"])){
		$sqlRC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["receivingAc"]."' ";
		$receivingCurrData = selectMultiRecords($sqlRC);
		$receiveCurrData = array();
		foreach($receivingCurrData as $k=>$v){
			$receiveCurrData[] = $v['currency'];
			$strClosingBalance = "
					SELECT 
						id,
						closing_balance,
						dated,
						accountNumber 
					FROM 
						".TBL_ACCOUNT_SUMMARY." 
					WHERE
						accountNumber = '".$v["accounNumber"]."' AND
						dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
						WHERE accountNumber = '".$v["accounNumber"]."')
					 ";
				$closingBalance = selectFrom($strClosingBalance);
			
			/*
			If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
			If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
			*/
			if(in_array($_REQUEST["receivingCurrency"],$receiveCurrData)){
				if($v['balanceType'] == 'Dr')
					$receivingCBalance = ($closingBalance["closing_balance"] - $v['balance']);
				elseif($v['balanceType'] == 'Cr')
					$receivingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
			}
		}
		/*if(count($receiveCurrData)>0){
			if(in_array($_REQUEST["receivingCurrency"],$receiveCurrData)){
				$_arrInput = array(
									  "accountNumber" =>$receivingAc,
									  "currency" => $_REQUEST["receivingCurrency"]
									);
				$receivingCBalance = getClosingBalance($_arrInput);
			}
		}*/
	}
	$sendingCBalance   = number_format($sendingCBalance,$roundLevel,'.','');
	$receivingCBalance = number_format($receivingCBalance,$roundLevel,'.','');
?>
<html>
<head>
<title>Inter Account Transfer</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script language="javascript">
<!-- 
$(document).ready(function(){
	$("#accountForm").validate({
		rules: {
				sendingAc: "required",
				sendingCurrency: "required",
				receivingAc:  "required",
				recievingCurrency: "required",
				amount: {
					required: true,
					number: true,
					min: 0.0001//,
				},
				ramount: {
					required: true,
					number: true,
					min: 0.0001//,
				}
		},
		messages: {
				sendingAc: "<br />Please select sending account",
				sendingCurrency: "<br />Please select sending currency",
				receivingAc: "<br />Please select receiving account.",
				recievingCurrency: "<br />Please select receiving currency.",
				amount: { 
					required:"<br />Please enter sending amount.",
					number: "<br />This is not valid amount.",
					min: "<br />Amount can not be 0."//,
					//min: "<br />Value should be greater than "+ $("#amount_from").val()
				},
				amount: { 
					required:"<br />Please enter receiving amount.",
					number: "<br />This is not valid amount.",
					min: "<br />Amount can not be 0."//,
					//min: "<br />Value should be greater than "+ $("#amount_from").val()
				}
		}
	});

	var roundLevel = "<?=$roundLevel?>";
	$(".decimalsLimit").keyup(function(){
		if(roundLevel!=""){
			var decNumber = $(this).val();
			var decArray  = decNumber.split(".");
			var decValue  = decArray[1];
			if(decValue)
			{
				if(decValue.length>roundLevel)
					$(this).val(decNumber.substring(0,decNumber.length-1));
			}
		}
	});
	$(".decimalsLimit").blur(function(){
		if(roundLevel!=""){
			var fixedDec = parseFloat($(this).val());
			if(!isNaN(fixedDec))
				$(this).val(fixedDec.toFixed(roundLevel));
		}
	});	
	var roundRateTo = "<?=$roundRateTo?>";
	$(".decimalsLimitRate").keyup(function(){
		if(roundRateTo!=""){
			var decNumber = $(this).val();
			var decArray  = decNumber.split(".");
			var decValue  = decArray[1];
			if(decValue)
			{
				if(decValue.length>roundRateTo)
					$(this).val(decNumber.substring(0,decNumber.length-1));
			}
		}
	});
	$(".decimalsLimitRate").blur(function(){
		if(roundRateTo!=""){
			var fixedDec = parseFloat($(this).val());
			if(!isNaN(fixedDec))
				$(this).val(fixedDec.toFixed(roundRateTo));
		}
	});
	$("#revcal").click(function(){
		calculateAmount();
	});
		
	<?php if($senderReceiverAmFlag){?>
		/*$("#amount").keyup(function(){
			$("#ramount").val($(this).val());
		});	
		$("#amount").blur(function(){
			$("#ramount").val($(this).val());	
		});
		$("#ramount").keyup(function(){
			$("#amount").val($(this).val());
		});	
		$("#ramount").blur(function(){
			$("#amount").val($(this).val());	
		});*/
	<?php }?>
});
function checkForm(theForm) {
	
	if(theForm.sendingAc.options.selectedIndex == 0){
    	alert("Please select sending account.");
        theForm.sendingAc.focus();
        return false;
	}
	
	if(theForm.sendingCurrency.options.selectedIndex == 0){
    	alert("Please select sending currency.");
        theForm.sendingCurrency.focus();
        return false;
    }
	
	if(theForm.receivingAc.options.selectedIndex == 0){
    	alert("Please select receiving account.");
        theForm.receivingAc.focus();
        return false;
    }
	
	if(theForm.receivingCurrency.options.selectedIndex == 0){
    	alert("Please select recieving currency.");
        theForm.receivingCurrency.focus();
        return false;
    }
	
	
	if(document.getElementById("amount").value < 0 || isNaN(document.getElementById("amount").value))
	{
		alert("The amount to send should be positive.");
		document.getElementById("amount").focus();
		return false;
	}
	
	if(document.getElementById("amount").value == "")
	{
		alert("Please enter the amount to send.");
		document.getElementById("amount").focus();
		return false;	
	}
if(document.getElementById("ramount").value < 0 || isNaN(document.getElementById("ramount").value))
	{
		alert("The amount to receive should be positive.");
		document.getElementById("ramount").focus();
		return false;
	}
	
	if(document.getElementById("ramount").value == "")
	{
		alert("Please enter the amount to receive.");
		document.getElementById("ramount").focus();
		return false;	
	}
	/*
	<?  if(!empty($_REQUEST["receivingAc"]) && !empty($_REQUEST["sendingCurrencyName"])) { ?>
		var agentBalanceDisplay = <?=$agentBalance?>;
		if(agentBalanceDisplay <= 0)
		{
			alert("The agent balance must be in positive to transfer funds.");
			document.getElementById("receivingAc").focus();
			return false;
		}
		else if(agentBalanceDisplay - document.getElementById("amount").value < 0)
		{
			alert("You can enter upto "+ agentBalanceDisplay +" " + document.getElementById("sendingCurrency").value + " as Transfer funds.");
			document.getElementById("amount").focus();
			return false;
		}
	<? } ?>
	*/
 	return confirm("Do you confirm "+ document.getElementById("amount").value+" "+ theForm.sendingCurrency.value +" transfer of funds"); 
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function updateInterface()
{
	var roundLevel = "<?=$roundLevel?>";
	var roundRateTo = "<?=$roundRateTo?>";
	var defaultRate = 1;
	defaultRate = defaultRate.toFixed(roundRateTo);
	var amountObj = document.getElementById("amount");
	if(amountObj){
		var amountValue = "";
		if(amountObj.value!=""){
			amountValue = parseFloat(amountObj.value);
			if(amountValue)
				amountValue = amountValue.toFixed(roundLevel);
		}
	}

	if(document.getElementById("sendingCurrency").value != "" && document.getElementById("receivingCurrency").value != "")
	{
		if(document.getElementById("sendingCurrency").value == document.getElementById("receivingCurrency").value)
		{
			$("#exchangeRate").hide();
			document.getElementById("ramount").value = amountValue;
		}
		else
		{
			$("#exchangeRate").show();
			document.getElementById("ramount").value = amountValue;
			document.getElementById("rate").value = defaultRate;
			//document.getElementById("rate").value = defaultRate.toFixed(roundRateTo);
		}
	}
	/*document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("receivingCurrency").value;*/

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("receivingCurrency").value;	
}

function calculateAmount()
{
	var totCnt = $("input:checked").length;
	var opr = '*';
	if(totCnt>0)
		opr = '/';

	var roundLevel = "<?=$roundLevel?>";
	var roundRateTo = "<?=$roundRateTo?>";
	
	var finalAmount;
	var amount = document.getElementById("amount").value;
	var rate = document.getElementById("rate").value;

	if(rate<=0){
		var rate = 1;
		rate = rate.toFixed(roundRateTo);
 	    document.getElementById("rate").value = rate;
	}
	if(amount != "" && rate != "")
	{
		finalAmount = amount * rate;
		if(opr == '/')
			finalAmount = amount / rate;
		else
			finalAmount = amount * rate;
		finalAmount = finalAmount.toFixed(roundLevel);
		document.getElementById("ramount").value = finalAmount;
	}
	
	if(document.getElementById("sendingCurrency").value == document.getElementById("receivingCurrency").value)
	{
		var amountValue = document.getElementById("amount").value;
		if(amountValue){
			amountValue = parseFloat(amountValue);
			document.getElementById("ramount").value = amountValue.toFixed(roundLevel);
		}
	}
	
}


function placeValues(focusObj)
{
	/*document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("receivingCurrency").value;*/

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("receivingCurrency").value;		

	updateInterface();
	setFocus(focusObj);
}

function setFocus(focusObj){
	if(focusObj!=''){
		if(focusObj == 'sendingCurrency')
			document.getElementById('amount').focus();
		else if(focusObj == 'receivingCurrency')
			document.getElementById('ramount').focus();
		else
			document.getElementById(focusObj).focus();
	}
}
-->
</script>
</head>
<body onLoad="placeValues('<?=$_REQUEST['focus']?>');">
<div class="topbar"><strong><font class="topbar_tex">Inter Account Transfer</font></strong></div>
<form action="" method="post" onSubmit="return checkForm(this);" name="accountForm">
<table width="100%" border="1" cellspacing="1" cellpadding="5">
	<tr>
    <td align="center">
			<table width="75%" border="0" cellspacing="1" cellpadding="2" align="center">
        <? if ($msg != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE">
            	<table width="100%" cellpadding="5" cellspacing="0" border="1">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($stat ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ( $stat ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ( $stat ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$msg." </b><br><br></font>"; ?></td>
                </tr>
              </table>            </td>
          </tr>
        <? } ?>
        <tr bgcolor="#ededed"> 
	          <td width="20%"><font color="#005b90"><strong>Sending Account <font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?php
				$excludeAcc = "";
				if(!empty($_REQUEST["receivingAc"]))
					$excludeAcc = " and accounNumber !='".$_REQUEST["receivingAc"]."' ";

				$strSql = "SELECT 
								id,
								accounNumber,
								accounType,
								accountName,
								currency 
							FROM 
								accounts 
							WHERE 
								status = 'AC' 
								".$excludeAcc." ";
				$src = selectMultiRecords($strSql);
					//debug($src);
            	?>
           		<select name="sendingAc" id="sendingAc" style="font-family:verdana; font-size: 11px" onChange="document.accountForm.action = '<?=$_SERVER['PHP_SELF']?>?focus=sendingAc';document.accountForm.submit();">
						<option value="">- Select Account -</option>
						<?php
							for ($i=0; $i < count($src); $i++)
							{
						?>
								<option value="<?=$src[$i]["accounNumber"]; ?>"><? echo $src[$i]["accountName"] . " [" . $src[$i]["accounNumber"] . "]" ; ?></option>	
						<?php
							}
						?>
				</select>
				<script language="JavaScript">
			   			SelectOption(document.forms.accountForm.sendingAc,"<?=$_REQUEST["sendingAc"]; ?>");
			   </script>
				&nbsp;
				&nbsp;
       			<select name="sendingCurrency" id="sendingCurrency" style="font-family:verdana; font-size: 11px" onChange="document.accountForm.action = '<?=$_SERVER['PHP_SELF']?>?focus=sendingCurrency';document.accountForm.submit();updateInterface();">
					<option value="">- Currency  -</option>
					<?php 
					foreach ($sendingCurrData as $uniqueCurrency )
					{
						$uniqueCurrency = $uniqueCurrency["currency"];
						?>
							<option <?=$selected;?> value="<?=trim($uniqueCurrency)?>" ><?=trim($uniqueCurrency); ?></option>
						<?php
					}
					?>
			  	</select>
				<script language="JavaScript">
			   			SelectOption(document.forms.accountForm.sendingCurrency,"<?=$_REQUEST["sendingCurrency"]; ?>");
			   </script>
			  &nbsp;
		      Closing Balance: <?=$sendingCBalance?></td>
	  </tr>
	  <tr bgcolor="#ededed"> 
				<td><font color="#005b90"><strong>Sending Funds<font color="#ff0000">*</font></strong></font></td>
				<td>
					<?php /*?><input type="text" id="amount" name="amount" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["amount"]?>" onBlur="calculateAmount();" class="decimalsLimit"/><?php */?>
					<input type="text" id="amount" name="amount" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["amount"]?>" class="decimalsLimit" onBlur="calculateAmount();" />
					&nbsp;<b id="ta"></b>			  </td>
			</tr>
      <tr bgcolor="#ededed"> 
	    	<td><font color="#005b90"><strong>Receiving Account <font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?php
					$agents = array();
					//if ( !empty( $_REQUEST["sendingCurrency"] ) )
					//{
						if(!empty($_REQUEST["sendingAc"]))
							$excludeAcc = " and accounNumber !='".$_REQUEST["sendingAc"]."' ";
		
						$strSql = "SELECT 
										id,
										accounNumber,
										accounType,
										accountName,
										currency 
									FROM 
										accounts 
									WHERE 
										status = 'AC' 
										".$excludeAcc." ";
										//debug($strSql);
						$src = selectMultiRecords($strSql);
					//}
				?>
	         	<select name="receivingAc" id="receivingAc" style="font-family:verdana; font-size: 11px" onChange="document.accountForm.action = '<?=$_SERVER['PHP_SELF']?>?focus=receivingAc';document.accountForm.submit();">
					<option value="">- Select Account -</option>
						<?php
							for ($i=0; $i < count($src); $i++)
							{
						?>
								<option value="<?=$src[$i]["accounNumber"]; ?>"><? echo $src[$i]["accountName"] . " [" . $src[$i]["accounNumber"] . "]" ; ?></option>	
						<?php
							}
						?>
				</select>
				<script language="JavaScript">
			   			SelectOption(document.forms.accountForm.receivingAc,"<?=$_REQUEST["receivingAc"]; ?>");
			   </script>
				&nbsp;
				&nbsp;
				<select name="receivingCurrency" id="receivingCurrency" style="font-family:verdana; font-size: 11px" onChange="document.accountForm.action = '<?=$_SERVER['PHP_SELF']?>?focus=receivingCurrency';document.accountForm.submit();updateInterface();">
					<option value="">- Currency  -</option>
					<?php 
						foreach ($receivingCurrData as $uniqueCurrency )
						{
							$uniqueCurrency = $uniqueCurrency["currency"];
							?>
								<option value="<?=trim($uniqueCurrency)?>" ><?=trim($uniqueCurrency); ?></option>
					<?php
						}
					?>
				</select>
				<script language="JavaScript">
			   			SelectOption(document.forms.accountForm.receivingCurrency,"<?=$_REQUEST["receivingCurrency"]; ?>");
			   </script>
				&nbsp;
			   Closing Balance: <?=$receivingCBalance?></td>
			</tr>
			
			<tr bgcolor="#ededed" id="exchangeRate" style="display:none"> 
				<td><font color="#005b90"><strong>Exchange Rate<font color="#ff0000">*</font></strong></font></td>
				<td>
					<b> 1 </b><span id="sc"><?=$_REQUEST["sendingCurrencyName"]?></span> = 
                    <input type="text" size="15" id="rate" name="rate" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["rate"]?>" onBlur="calculateAmount();" class="decimalsLimitRate"/>					
                  <span id="rc"><?=$_REQUEST["receivingCurrencyName"]?></span>	
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#005b90"><strong>Reverse Calculate</strong></font>
				  <input type="checkbox" id="revcal" name="revcal" />
				  </td>
			  </tr>
			<tr bgcolor="#ededed"> 
				<td><font color="#005b90"><strong>Receiving Funds</strong><font color="#ff0000">*</font></font></td>
				<td>
					<input type="text" id="ramount" name="ramount" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST['ramount']?>" class="decimalsLimit" readonly="readonly" />
					&nbsp;<b id="ra"></b>			    
					</td>
			</tr>		
			<tr bgcolor="#ededed"> 
				<td valign="top"><font color="#005b90"><strong>Transfer Fund Note</strong></font></td>
				<td>
					<textarea name="note" wrap="soft" cols="47" rows="7"><?=$_REQUEST["note"]?></textarea>			  </td>
			</tr>
    	</table>
  	</td>
  </tr>
          
  <tr bgcolor="#ededed"> 
  	<td colspan="3" align="center"> <input type="submit" value="Transfer Fund" name="formSubmitted"> </td>
  </tr>
</table>
</form>
</body>
</html>