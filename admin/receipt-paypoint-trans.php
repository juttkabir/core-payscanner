<?php

	session_start();
	include ("../include/config.php");
	include ("security.php");

	$titleReciept = "Paypoint Transaction Reciept";
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	
	$contentsPayTrans = selectFrom("select pt.*,a.name,a.userID,c.firstname, c.middlename, c.lastname from paypointTransactions pt LEFT JOIN customer c ON c.customerID = pt.customerID LEFT JOIN admin a ON pt.addedBy=a.username where pt.transID = ".$_REQUEST["transID"]." ");
/*	if($userID != $contentsPayTrans["userID"]){
		echo $userID ." - ". $contentsPayTrans['name']."<br/>";
		echo "<h2>You are not Authorized for this Operation!</h2>";
		exit;
	}*/
	$currName   = $contentsPayTrans['currency'];
	$countName  = $contentsPayTrans['country'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$titleReciept?></title>
<script language="javascript" src="jquery.js"></script>
<script>
	$(document).ready(function(){
		$("#printbtn").click(function(){
			$(this).hide();
			print();
		});
	});
</script>
<style>
td{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	color:#000000;
}
</style>
</head>
<body>
<table border="1" bordercolor="#4C006F" cellspacing="0" cellpadding="0" width="320">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="3" id="table">
				<tr>
				   <td colspan="4" align="center" bgcolor="#FFFFFF">
						<img height='50' alt='' src='<?=CONFIG_LOGO?>' width='100' />
				   </td>
				</tr>
				<tr>
					<td colspan="4" align="center" bgcolor="#FFFFFF">
						<h2><?=$titleReciept?></h2>
						<br /><br />
					</td>
				</tr>
				<tr>
					<td width="37%" scope="col"><b>Date:</b></td>
					<td width="63%" scope="col"><?=date("d/m/Y",strtotime($contentsPayTrans["transDate"]))?></td>
				</tr>
				<tr>
					<td scope="col"><b>Agent Name:</b></td>
					<td scope="col"><?=$contentsPayTrans["name"]?></td>
				</tr>
				<tr>
					<td colspan="2"><hr width="100%" style="border-style:dotted" /></td>
				</tr>
				
				<tr>
					<td scope="col" colspan="2">
						<i><b>Customer</b></i><br />
						&nbsp;&nbsp;<?=$contentsPayTrans["firstname"]." ".$contentsPayTrans["middlename"]." ".$contentsPayTrans["lastname"]?>
					</td>
				</tr>
				<tr>
					<td scope="col" colspan="2">
						<i><b>Country </b></i><br />
						&nbsp;&nbsp;<?=$countName?>
					</td>
				</tr>
				<tr><td><br /></td></tr>
				<tr>
					<td align="left"><b>Service Type :</b></td>
					<? $serviceRS = selectFrom("SELECT serviceName FROM servicesPaypoint WHERE serviceID='".$contentsPayTrans["serviceID"]."'");?>
					<td align="left"><?=$serviceRS["serviceName"]?></td>
				</tr>
				<tr>
					<? $categoriesRS = selectFrom("SELECT categoryName FROM categoriesPaypoint WHERE serviceID='".$contentsPayTrans["serviceID"]."'");?>
					<td align="left"><b>Sub-Service (Category):</b></td>
					<td align="left"><?=$categoriesRS["categoryName"]?></td>
				</tr>
				<tr>
					<td align="left"><b>Amount :</b></td>
					<td align="left"><?=$contentsPayTrans["transAmount"]. " ". $currName?></td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="bottom"><hr width="80%" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Customer Signature</b>
						<br /><br />
						<hr noshade="noshade" size="1" width="100%" />
						<b>Authorized</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="button" id="printbtn" value="Print Order Receipt" />

</body>
</html>