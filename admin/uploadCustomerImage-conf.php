<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include (("../include/config.php"));
include ("security.php");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$payinAmount = CONFIG_PAYIN_FEE;
$agentType = getAgentType();
$tran_date = date("Y-m-d");
$tran_dateTime = getCountryTime(CONFIG_COUNTRY_CODE);
if($_REQUEST["custName"] != "")
	$custName = $_REQUEST["custName"];
	
if($_REQUEST["DocID"] != "")
	$DocID = $_REQUEST["DocID"];
			
if($_REQUEST["mode1"] != "")
 $mode = $_REQUEST["mode1"];
 
if($_REQUEST["customerID"] != "")
	$customerID = $_REQUEST["customerID"];
	   
if($_REQUEST["pageID"] != "")
  $pageID = $_REQUEST["pageID"];
  
if($_REQUEST["ida"] != "")
 $ida = $_REQUEST["ida"];

if($_REQUEST["type"] != "")
 $typeV = $_REQUEST["type"];
	//checks if the customer already exists
	$customerName = $_POST["firstName"] . " " . $_POST["middleName"] . " " . $_POST["lastName"];
	$field = "customerName,Mobile,IDNumber";
	$values = $customerName.",".$_POST["Mobile"].",".$_POST["IDNumber"];
	//$values = $customerName;
	$tble = "customer";
	$retValue = "accountName";

$customerAcc = selectFrom("select agentID,accountName from customer where customerID ='".$customerID."' ");
$AccountName = $customerAcc["accountName"];

	if($_POST["moreCategory"]!= ""){
		$redirectPage = "uploadCustomerImage.php?type=".$typeV."&customerID=".$customerID."&msg2=Y&pageID=".$pageID."&mode=".$mode."&ida=".$ida;
 }else{
	if($_POST["pageID"] == 'popUp'){
		$redirectPage = "add-transaction.php?msg=Y&customerID=".$customerID."&pageID=".$pageID."&agentID=".$customerAcc["agentID"]."";
		}elseif($_POST["pageID"] == 'search'){
			$redirectPage = "search-cust.php?msg=Y&custName=".$custName."";
		}elseif($_POST["pageID"] == 'add_agent' && $mode == "Add"){
			$redirectPage = "add-agent.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";	
		}elseif($_POST["pageID"] == 'add_agent' && $mode == "update"){
			$redirectPage = "update-agent.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";	
		}elseif($_POST["pageID"] == 'add_sub_agent' && $mode == "Add"){
			$redirectPage = "add-sub-agent.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";	
		}elseif($_POST["pageID"] == 'add_sub_agent' && $mode == "update"){
			$redirectPage = "update-sub-agent.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";	
		}elseif($_POST["pageID"] == 'add-agent_dist' && $mode == "Add"){
			$redirectPage = "add-agent_Distributor.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";	
		}elseif($_POST["pageID"] == 'add-agent_dist' && $mode == "update"){
			$redirectPage = "update-agent_Distributor.php?type=".$typeV."&msg=Y&page=uploadDocument&userID=".$customerID."&mode=".$mode."&ida=".$ida."";
		}elseif($pageID != '')
		{
			$returnPage = $pageID;
			$redirectPage = $returnPage."?&customerID=".$customerID."&pageID=".$pageID."";
			//$from = 'popUp';
		}else{
	
			$redirectPage = "search-edit-cust.php?msg=Y&custName=".$custName."";
}
	}


	/* File Uploading   */
	
	$complete = false;
if($mode == "Add"){	
for ($i=0; $i < count($_FILES); $i++){ 
	//$logo_name = $_FILES["file".$i]["name"];
	//print_r($_FILES["file".$i]["name"]);
//	echo count($_FILES)." ".$_FILES["file".$i]['type'];die();
	if($_FILES["file".$i]['type'] != "")
	{
		
		
		if ( $_FILES["file".$i]['type'] == "application/pdf" || $_FILES["file".$i]['type'] == "image/gif"
			|| $_FILES["file".$i]['type'] == "image/jpg"
			|| $_FILES["file".$i]['type'] == "image/jpeg"
			|| $_FILES["file".$i]['type'] == "image/pjpeg" 
			|| $_FILES["file".$i]['type'] == 'application/pdf' || $_FILES["file".$i]['type'] == "pdf" 
			|| $_FILES["file".$i]['type'] == "application/msword"
			|| $_FILES["file".$i]['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" 
			|| ( defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") && CONFIG_CUSTOM_UPLOAD_FILE_LIST == 1 && $_FILES["file".$i]['type'] == "image/tiff" ) // Added by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
			|| ( (!defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") || CONFIG_CUSTOM_UPLOAD_FILE_LIST == 0) && $_FILES["file".$i]['type'] == "image/png" ) // Condition modified by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
			|| ( (!defined("CONFIG_CUSTOM_UPLOAD_FILE_LIST") || CONFIG_CUSTOM_UPLOAD_FILE_LIST == 0) && $_FILES["file".$i]['type'] == "image/x-png" )  // Condition modified by Usman Ghani against ticket #3468: Now Transfer - Upload Document Types
			 )
		{
			
			
		$logo_name = $_FILES["file".$i]['tmp_name'];
		 $Doc_size = $_FILES["file".$i]["size"];
		
		  $limit_size=(2*1048576);
		 if(defined("CONFIG_LIMIT_FILE_SIZE"))
		 $limit_size=(CONFIG_LIMIT_FILE_SIZE*1048576);
		// debug($Doc_size,true);	
			$newName = "../files/customer/";
		

	

		//echo "NAME".	$newName =  $newName .  $_FILES['file0']['name'] ; 
			 $newName =  $newName .  "00000" .$customerID."_".$DocID[$i]."_".basename($_FILES["file".$i]['name']) ;
			// debug(2*1048576); 
			//debug($Doc_size);
			if($Doc_size<$limit_size)
			{
				
			//debug($Doc_size);
			
				if (is_uploaded_file($logo_name))
				{
					
					 if(move_uploaded_file($_FILES["file".$i]['tmp_name'], $newName ))
					{
						
						$docQuery = "INSERT INTO ".TBL_DOCUMENT_UPLOAD." (documentId, userId, path,status ) VALUES 	('$DocID[$i]', '$customerID', '$newName', 'Enable')";
						//echo "+$docQuery+";
					//	debug($docQuery,true);//remove
						insertInto($docQuery);
						
						 $msg = "Document uploaded successsfully.";
						$complete = true;
			
insertError($msg);

//redirect($redirectPage);
					 }else
					{
						 

					  $msg = "A Problem occured in Document Uploading";
					  
						} 
					//header("location: /matrimonial/doLogin.php?sid=$sid&msg=MSG11");
				}
				else
				{
					
					insertError("Your document can not be uploaded due to some error1.");
					redirect($redirectPage);
				}
			}else
				{
				
					//debug($redirectPage,true);
					insertError("Your document can not be uploaded because file size is large");
					redirect($redirectPage);
				}
			//die();
				
		}else
		{
		
			//echo $_FILES["file".$i]['type'];
			//die();
			insertError("Invalid file format.");		
			redirect("uploadCustomerImage.php?customerID=".$customerID."&msg2=Y&success=N");
			
		}
	}
if($_FILES["file".$i]['type'] == "")
	{
	insertError("Your document can not be uploaded due to some error2.");
	redirect("uploadCustomerImage.php?customerID=".$customerID."&msg2=Y&success=N");
	}
}



	/* File uploading*/


}elseif($mode=="update"){
	
		//echo("Name Is");
		//debug($mode);	
for ($i=0; $i < count($_FILES); $i++){ 
//$logo_name = $_FILES["file".$i]["name"];
//debug($i);
//debug($_FILES["file".$i]['type']);
	if($_FILES["file".$i]['type'] != ""){
		if ($_FILES["file".$i]['type'] == "image/gif" || $_FILES["file".$i]['type'] == "image/jpg" || $_FILES["file".$i]['type'] == "image/jpeg" || $_FILES["file".$i]['type'] == "image/pjpeg" || $_FILES["file".$i]['type'] == "image/png" || $_FILES["file".$i]['type'] == "image/x-png"|| $_FILES["file".$i]['type'] == "application/pdf" || $_FILES["file".$i]['type'] == "application/msword" || $_FILES["file".$i]['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
		{
			
			
			$logo_name = $_FILES["file".$i]['tmp_name'];
			
			$newName = "../files/customer/";
		
		//echo "NAME".$_FILES['file0']['name'] ; 
			$newName =  $newName .  "00000" .$_POST["customerID"]."_".$DocID[$i]."_".basename($_FILES["file".$i]['name']); 
			debug ($newName , true);
			//if(extension($logo_name)==1)
			
			
			//echo " ext ".$Ext = strrchr($logo_name,".");
			//$fileName = "fName";
			 $Doc_size = $_FILES["file".$i]["size"];
		 $limit_size=(2*1048576);
		 if(defined("CONFIG_LIMIT_FILE_SIZE"))
		 $limit_size=(CONFIG_LIMIT_FILE_SIZE*1048576);
		 //debug($Doc_size,true);
			if($Doc_size<$limit_size)
			{
				if (is_uploaded_file($logo_name))
				{
				
					//echo " pict ".$Pict=strtolower($fileName.$Ext);
					//debug($_FILES["file".$i]['tmp_name']);
					//debug($newName);
					if(move_uploaded_file($_FILES["file".$i]['tmp_name'], $newName ))
					{
						//$existingQuery = "select path from ".TBL_DOCUMENT_UPLOAD." where userId = '".$_POST["customerID"]."' and documentId = '".$DocID[$i]."'";
						//$existingPath = selectFrom($existingQuery);
						//if($existingPath["path"] == "" ){
							$docQuery = "INSERT INTO ".TBL_DOCUMENT_UPLOAD." (documentId, userId, path,status ) VALUES 	('$DocID[$i]', '$customerID', '$newName', 'Enable')";
							insertInto($docQuery);
						//}else{
						//$docQuery = "update ".TBL_DOCUMENT_UPLOAD." set path = '".$newName."' where userId = '".$_POST["customerID"]."' and documentId = '".$DocID[$i]."'";
							//update($docQuery);
						//}
						
						 $msg = "Document uploaded successsfully.";
						$complete = true;
					}else{
					
				
					 $msg = "A Problem occured in Document Uploading";
						}						
					header("location: /matrimonial/doLogin.php?sid=$sid&msg=MSG11");
				}
				else
				{
					insertError("Your document can not be uploaded due to some error3.");
					redirect($redirectPage);
				}
			}else
				{
					
				insertError("Your document can not be uploaded because file size is large");
					redirect($redirectPage);
				}
			
				
		}else
		{
			//echo $_FILES["file".$i]['type'];
			//die();	
			insertError("Invalid file format.");		
			redirect("uploadCustomerImage.php?customerID=".$customerID."&msg2=Y&success=N");
			
		}
	}
}


}
	
	
insertError("Document(s) Uploaded Successfully for Customer ".$AccountName.".");
insertError("Document(s) Uploaded Successfully");
include("lib/html_to_doc.inc.php");
$htmltodoc= new HTML_TO_DOC();
include ("mailCustomerInfo.php");
	/////////////////////
$To = $_REQUEST["custEmail"]; 
$fromName = SUPPORT_NAME;
$fromEmail = SUPPORT_EMAIL;
$htmltodoc->createDoc($data,"test");
//$htmltodoc->createDocFromURL("http://yahoo.com","test");
$subject = "Premier FX ";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$num = md5( time() );
/*$message = "\n<div align='left'><img width='25%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg' /></div>\n<br />Dear ".$_POST["firstName"]."\n<br /><br />Please find attached details of your trading account with Premier FX. To\n<br />
complete your application we require a scanned copy of your passport and\n<br />
a proof of address document.  This is common practice among all\n<br />
financial institutions under money laundering regulations. \n<br />
\n<br />
Please do not hesitate to contact me if you have any further  questions.\n<br />
 I look forward to being of service to you in the near future.\n<br /><br />
Regards<br />Premier Support ";

*/
$message="\n Dear ".$_POST["fname"].",\n<br>";
$passQuery=selectFrom(" select documentId from document_upload where userId='".$_POST["customerID"]."'");

if($passQuery["documentId"]!="15"){
	//$message.=" a SIGNED copy of your passport and"."\n\r";
	$message.= "\n Please find attached details of your trading account with Premier FX."."\n" ;
	$message.= "\n To complete your application we will require a scanned copy of your passport and a proof of address document. 		This is common practice for all financial institutions under money laundering regulations."."\n";
	$message.="\n Please do not hesitate to contact me if you have any further questions.  I look forward to being of service to you in the near future."."\n";
}
else {
	$message.= "\n<br> Please find attached details of your trading account with Premier FX."."\n<br>" ;
$message.="\n Please do not hesitate to contact me if you have any further questions. I look forward to being of service to you in the near future."."\n<br>";
}
$message.=" \n<br>Regards,"."\n<br />";


/*
$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom(" select name from admin where userID = '".$agentIDQuery["parentID"]."'");
$message .= "\n".$agentUserName["name"]."\n";
*/

if(!defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT!=1)
	$agentIDQuery=selectFrom(" select parentID from customer where customerID='".$customerID."'");
$agentUserName=selectFrom(" select name from admin where userID = '".$agentIDQuery["parentID"]."'");
$message .= "\n ".$agentUserName["name"]."\n";
//Premier Support\n
$message.="<table><tr><td align='left' width='30%'><img width='100%' border='0' src='http://".$_SERVER['HTTP_HOST']."/admin/images/premier/logo.jpg' /></td><td>&nbsp;</td></tr><tr><td width='20%'>&nbsp;</td><td align='left'><font size='2px'><font color='#F7A30B'>LONDON OFFICE</font>\n<br>11 th Floor City Tower 40 Basinghall St London EC2V 5DE \n<br><font color='#F7A30B'> PORTUGAL HEAD OFFICE</font> \n<br> Rua Sacadura Cabral Edificio Golfe 1A, 8135-144 Almancil \n<br> <font color='#F7A30B'>Tel:</font> UK +44 (0) 845 021 2370 | <font color='#F7A30B'>Int:</font> +351 289 358 511 \n<br> <font color='#F7A30B'>FAX:</font> +351 289 358 513 \n <br> <font color='#F7A30B'>Email:</font> info@premfx.com | www.premfx.com</font></td></tr></table>";
//debug($message);

$strresume = "test.doc";
// MAIL HEADERS with attachment
//echo $message;
$fp = fopen($strresume, "rb");
$file = fread($fp, 102460);

$file = chunk_split(base64_encode($file));
$num = md5(time());

	//Normal headers

$headers .= 'From: Premier FX <'.$fromEmail.'>';
$headers  .= "MIME-Version: 1.0\r\n";
$headers  .= "Content-Type: multipart/mixed; ";
$headers  .= "boundary=".$num."\r\n";
$headers  .= "--$num\r\n";

	// This two steps to help avoid spam   

$headers .= "Message-ID: <".date("Y-m-d")." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
$headers .= "X-Mailer: PHP v".phpversion()."\r\n";         

	// With message
   
$headers .= "Content-Type: text/html; charset=iso-8859-1\r\n";
$headers .= "Content-Transfer-Encoding: 8bit\r\n";
$headers .= "".$message."\n";
$headers .= "--".$num."\n"; 

	// Attachment headers

$headers  .= "Content-Type: application/msword";
$headers  .= "name=\"attachment.doc\"r\n";
$headers  .= "Content-Transfer-Encoding: base64\r\n";
$headers  .= "Content-Disposition: attachment; ";
$headers  .= "filename=\"Welcome Letter.doc\"\r\n\n";
$headers  .= "".$file."\r\n";
$headers  .= "--".$num."--";

//		To access your account please <a href=\"".USER_ACCOUNT_URL."/\">click here</a><br><br>
if($_POST["payinBook"] != ""){
$strQuery = "insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
											 values('".$customerID."','".$tran_date."','','Customer Registered','WITHDRAW','".$payinAmount."','".$modifyby."'
											 )";
								$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	}
		

	//sendMail($_POST["custEmail"], $subject, $message, $fromName, $fromEmail);    it will not send meail because of customer login problem.

	//debug($message);
	if(CONFIG_MAIL_GENERATOR == '1' && $_REQUEST["sendEmail"]=='Y' && $_REQUEST["uploadImage"]=="Y"){		
		@mail($To,$subject,$message,$headers);		
		//debug("mail has sent");
		/*echo $data;
		exit;*/
	}

	////////////////////////
	$redirectPage .= "&success=Y";	

	//debug($redirectPage); //add-transaction.php?msg=Y&customerID=10103&pageID=popUp&success=Y// remove
		//echo "before loop".$redirectPage;
		
	if(($_POST["pageID"] == 'popUp'  && $_POST["moreCategory"]== "") || $_POST["pageID"] == "buySellCurr.php" || $_REQUEST["callFrom"]=="TT")
	{ 
	
		if((defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1) && $_REQUEST["callFrom"]=="TT")
		{?>
		<script language="javascript">
			var prevId=opener.document.TT_form.cusdi.value;
			if(opener.document.TT_form.cusdi.value!="<?=$customerID?>")
			{
				var upCust=0;
				var newCust=1;
					//alert(prevId);
				
			}
			else
			{	
				var upCust=<?=$customerID?>;
				var newCust='';
			}
			window.opener.document.TT_form.cusdi.value ="<?=$customerID?>";
			window.opener.LoadCustInfo(upCust,newCust);
			window.close();
		</script>';	
		
	<?php	}
		else{
	//debug($redirectPage, true);
	?>
		
		<script language="javascript">
	
		opener.document.location = "<?= $redirectPage?>"; //remove
			window.close(); //remove

	
	</script>
		
	<?	
		}} else{


		redirect($redirectPage); // remove
	}

//echo "   after".$redirectPage;

?>