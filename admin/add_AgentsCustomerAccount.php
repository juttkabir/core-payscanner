<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
///////////////////////History is maintained via method named 'activities'

$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
if($_GET["agent_Account"]!="")
	$_SESSION["agentID2"] = $_GET["agent_Account"];
$custID = $_SESSION["agentID2"];


$backUrl = "AgentsCustomerAccount.php?custID=$_SESSION[agentID2]&search=search&msg=Y";
$checkQuery = "Select count(customerID) from customer where accountName like '".$_SESSION["agentID2"]."' ";
$customerIDArr = selectFrom("select customerID from customer where accountName like '".$_SESSION["agentID2"]."'");
$customerID = $customerIDArr["customerID"];
$checkQuery2 = countRecords($checkQuery);
if ($checkQuery2 == 0)
{
	insertError(CU17);
	redirect($backUrl);
}


// Added by Niaz Ahmad at 25-04-2009 @4950 (AMB)
if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){
if($_POST["Deposit"] =="Deposit")
{
	if(isset($_REQUEST["paymentType"]))
	{
		$modeOfPayment = $_REQUEST["paymentType"];
		$chequeNumber = trim($_REQUEST["chequeNumber"]);
		$chequeStatus = "UNCLEARED";
		
		if($modeOfPayment == "Cheque" && empty($chequeNumber))
		{
			$errMessage = "<font color='#ff0000'><b>Please enter cheque number.</b></font>";
		} elseif($modeOfPayment == "Cheque" && !empty($chequeNumber)) {
			// Check for duplicate cheque number
			$sql = "SELECT
						chequeNumber
					FROM
						agents_customer_account
					WHERE
						chequeNumber = '".$chequeNumber."'
					";
			$result = mysql_query($sql) or die(mysql_error());
			$numrows = mysql_num_rows($result);
			
			if($numrows > 0)
			{
				$errMessage = "<font color='#ff0000'><b>Duplicate cheque number, please enter a different (unique) cheque number.</b></font>";
			}
		} elseif ($modeOfPayment == "Bank Transfer"){
			$chequeStatus = "UNCLEARED";
		} elseif ($modeOfPayment != "Cheque"){
			$chequeStatus = "";
		}
	} else {
		$modeOfPayment = "Cash";
		$chequeNumber = "";
		$chequeStatus = "";
	}
 }
if(!empty($errMessage)){
$backUrl = "AgentsCustomerAccount.php?custID=$_SESSION[agentID2]&search=search&msg=Y";
insertError($errMessage);
redirect($backUrl);
}

}
?>
<html>
<head>
<title>Add into Account</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function handleChequeNumField()
{
	var paymentMode = document.getElementById("paymentType").value;
	var chequeNum = document.getElementById("chequeNumber");
	
	if(paymentMode == "Cheque")
	{
		chequeNum.disabled = false;
	} else {
		chequeNum.disabled = true;
	}
}
</script>
</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="add_AgentsCustomerAccount.php">
  	<input type="hidden" name="agentID2" value="<?=$_SESSION["agentID2"]?>">
    <tr> 
      <td width="39%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Add Amount &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="money"></td>
    </tr>
     <tr> 
      <td><div align="right"><font color="#005b90">Add Note &nbsp;&nbsp;</font></div></td>
      <td><input type="text" name="note"></td>
    </tr>
 
    <? if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1"){?>
    
    <tr> 
      <td><div align="right"><font color="#005b90">Select Payment Mode &nbsp;&nbsp;</font></div></td>
      <td><SELECT name="paymentType" size="1" id="paymentType" onChange="javascript: handleChequeNumField();">
      				<option value="Cash">By Cash</option>
      				<option value="Bank Transfer">By Bank Transfer</option>
      				<option value="Cheque">By Cheque</option>
      		
      		</select>
      </td>
    </tr>
	<tr> 
			  <td><div align="right"><font color="#005b90">Cheque Number &nbsp;&nbsp;</font></div></td>
			  <td><input type="text" size="30" name="chequeNumber" id="chequeNumber" value="" disabled="disabled" />
			  		<?
					
						if(!empty($errMessage))
						{
							echo $errMessage;
						}
					?>
				</td>
			</tr>
    
    <? } ?>
    
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Deposit" value="Deposit"></td>
    </tr>
	 <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if($_POST["Deposit"] =="Deposit")
{
	if(CONFIG_SENDER_NUMBER_GENERATOR == 1)
	{
		$sql = "SELECT
					(count(caID) + 1) AS cnt
				FROM
					agents_customer_account
				WHERE
					Type = 'DEPOSIT'
				";
		//debug($sql);
		$result = mysql_query($sql) or die(mysql_error());
		
		while($rs = mysql_fetch_array($result))
		{
			$refNum = $rs["cnt"];
		}
		
		$refNum = SYSTEM_PRE."-".$refNum;
		$flag = true;
		
		//debug($refNum);
	} else {
		$refNumRange = selectFrom("SELECT id,rangeFrom,rangeTo,prefix,used from ".TBL_RECEIPT_RANGE." where agentID ='".$customerID."' and rangeTo >= (rangeFrom + used) and  created =(SELECT min(created) from ".TBL_RECEIPT_RANGE." where agentID ='".$customerID."' and rangeTo >= (rangeFrom + used))");

		if($refNumRange["prefix"] == "agentNumber")
		{
			$prefixQuery = "select	a.username from customer as c, admin as a where c.agentID = a.userID and c.customerID ='".$customerID."'  ";
			$prefixValue = selectFrom($prefixQuery);
			$prefix = $prefixValue["username"];
		} elseif($refNumRange["prefix"] == "senderNumber") {
			$prefixQuery = "select accountname from customer where customerID = '".$customerID."'";
			$prefixValue = selectFrom($prefixQuery);
			$prefix = "GE".$prefixValue["accountname"];
		}
		
		$limitVal = $refNumRange["rangeTo"] - ($refNumRange["rangeFrom"] + $refNumRange["used"]);

		if($refNumRange["id"] != '')
		{
			$flag = True;
			$value = $refNumRange["rangeFrom"] + $refNumRange["used"];
			$refNum = $prefix."-".$value;
			$used = $refNumRange["used"];
			$used = ++$used;
			
			update("update ".TBL_RECEIPT_RANGE." set used = '".$used."' where agentID ='".$customerID."' and id ='".$refNumRange["id"]."' ");
		} else {
			$flag = False;
			$errMsg = "Receipt cannot be generated, add sender's reciept number range. ";
		}
	}
 $today = getCountryTime(CONFIG_COUNTRY_CODE); 
 
 if($flag){
	$insertQuery="insert into agents_customer_account(customerID , Date, tranRefNo, payment_mode, Type, amount, modifiedBy, note,paymentType,refNo) values('".$customerID."', '$today', '', 'Manually Deposited', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', '".$_POST["note"]."','".$_POST["paymentType"]."','".$refNum."')";
$q=mysql_query($insertQuery);
$insertedID = @mysql_insert_id();

if(CONFIG_PAYMENT_MODE_ON_CUSTOMER_ACCOUNT == "1")
{
 			update("
					update 
						agents_customer_account 
					set 
						chequeNumber = '".$chequeNumber."',
						chequeStatus='".$chequeStatus."' 
					where
						caID = '".$insertedID."'
					");
		}

$contantagentID = selectFrom("select balance,agentID from customer where customerID ='".$customerID."'");
$agentBalance	= $contantagentID["balance"];
$agentID  = $contantagentID["agentID"];
$agentBalance += $_POST["money"];
update("update customer set balance  = $agentBalance where customerID ='".$customerID."'");



$descript = "Amount ".$_POST["money"]." is added into Agents Customer Account";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agents_customer_account",$descript);
}
	///////////////////////
	
		if(CONFIG_PAYIN_CUST_AGENT == '1')
			{
				$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
				if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agentID!=""){
					$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agentID."'";
				}
				$agentPayinContents = selectFrom($agentPayinQuery);
				if($agentPayinContents["agentType"] == 'Sub')
				{
					updateSubAgentAccount($agentPayinContents["userID"], $_POST["money"], '', 'DEPOSIT', "Manually Deposited", "Agent", $today);
					$qq = updateAgentAccount($agentPayinContents["parentID"], $_POST["money"], '', 'DEPOSIT', "Manually Deposited", "Agent",$_POST["note"],'', $today);
				}else{
					$qq = updateAgentAccount($agentPayinContents["userID"], $_POST["money"], '', 'DEPOSIT', "Manually Deposited", "Agent",$_POST["note"],'', $today);
				}	
			}
			
	////////////////////Auto Authorization//////////
//if(CONFIG_AUTO_AHTHORIZE == "1")
//{
	$contantagentID = selectFrom("select balance from customer where customerID ='".$customerID."'");
	$custBalance	= $contantagentID["balance"];
	$contantTrans = selectMultiRecords("select * from ".TBL_TRANSACTIONS." where customerID = '$customerID' and transStatus = 'Pending' and createdBy != 'CUSTOMER'");
	for($i = 0; $i < count($contantTrans); $i++)
	{
		if(CONFIG_EXACT_BALANCE == "1")
		{
			if($custBalance == $contantTrans[$i]["totalAmount"])
			{
				$conditionFlag = 1;	
			}
			else
			{
				$conditionFlag = 0;	
			}	
		}
		else
		{
			if($custBalance >= $contantTrans[$i]["totalAmount"])
			{
				$conditionFlag = 1;	
			}
			else
			{
				$conditionFlag = 0;	
			}	
		}
		
		if($conditionFlag == 1)
		{
			
				update("update ". TBL_TRANSACTIONS." set transStatus = 'Authorize', authorisedBy = '".$username."', authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='".$contantTrans[$i]["transID"]."'");				
				
		}
	}
	
//}



if($q)
	{
		
		if(CONFIG_CUSTOMER_LEDGER_PAGE == "1"){
		 $returnPage = "customerAccountReciept.php?custID=$custID&insertedID=$insertedID";
			
					redirect($returnPage);
			
			
			}else{
		
		
		?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>">You 
        have successfully added 
        <? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
        </font></div></td>
  	  <td bgcolor="#CCCCCC" align="left">&nbsp;</td>
  </tr>
<? }
}else{ ?>
	 <tr> 
      <td height="32" bgcolor="#CCCCCC" ><div align="right"><font color="<? echo SUCCESS_COLOR ?>"><? echo $errMsg ?> for the Amount:
        <? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
        </font></div></td>
  	  <td bgcolor="#CCCCCC" align="left">&nbsp;</td>
  </tr>
<?
	
	}
}	 
?>
	<tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr> 
      
    <td height="19"><a href="AgentsCustomerAccount.php?custID=<?=$_SESSION["agentID2"]?>&search=search" class="style2" ><font color="#000000">Go Back <? echo (CONFIG_PAYIN_CUSTOMER_LABEL_SHOW == "1" ?  CONFIG_PAYIN_CUSTOMER_LABEL : "Payin Book Sender");?> Account</font></a></td>
      
    <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>