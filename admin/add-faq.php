<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) 
{
	if (document.frmFAQ.question.value == "")
	{
		alert("Please write question.");
		frmFAQ.question.focus();
		return false;	
	}
	if (document.frmFAQ.answer.value == "")
	{
		alert("Please write answer.");
		frmFAQ.answer.focus();
		return false;	
	}
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
   function checkFocus(){
 frmFAQ.question.focus();
}
	// end of javascript -->
	</script>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Add FAQ</td>
  </tr>
          <? if ($_GET["msg"] != ""){ ?>
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td width="40" align="center">&nbsp;</td>
                <td width="635" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>&nbsp;&nbsp;<? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></td>
              </tr>
            </table></td>
          </tr>
		  <?
		  }
		  ?>
  <form action="add-faq-conf.php" method="post" onSubmit="return checkForm();" name="frmFAQ">
  <tr>
    <td align="center">
		<table width="500" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add 
                    FAQ</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<tr bgcolor="#ededed">
			<td colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="105" valign="top"><font color="#005b90"><strong>Question<font color="#ff0000">*</font></strong></font></td>
            <td><textarea name="question" cols="30" rows="4" style="font-family: verdana; font-size: 11px"><?=stripslashes($_SESSION["question"]); ?></textarea></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="105" valign="top"><font color="#005b90"><strong>Answer<font color="#ff0000">*</font></strong></font></td>
            <td><!--? include ("editor.php"); ?-->
          <!--iframe src="temp.php?details=<? echo urlencode(stripslashes($_SESSION["answer"])); ?>" width="542" height="150" id="idContent" class="tblCoolbar"></iframe-->
          <textarea name="answer" cols="80" rows="8" style="font-family: verdana; font-size: 11px"><?=stripslashes($_SESSION["answer"]); ?></textarea>
          <!--input  name="answer" type=hidden id="answer"-->
          </td>
        </tr>
		<tr bgcolor="#ededed">
			<td height="25" colspan="2" align="center">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
