<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";


if(CONFIG_COMMISSION_ON_RETURN == '1')
{
	$cancelEffectOn = "Cancelled - Returned";	
}else{
	$cancelEffectOn = "Cancelled";	
	}


if ($offset == "")
	$offset = 0;
$limit=400;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";


$agentType = getAgentType();


if(CONFIG_CURRENCY_BASED_REPORTS == '1')
{
	$currency = "currencyFrom";
}else{
	$currency = "currencyTo";
}


////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType !="SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

//$_SESSION["middleName"] = $_POST["middleName"];
if($_POST["Submit"] =="Search"||$_GET["search"]=="search")
{
	if($condition)
	{

		$query2 = "select distinct $currency from ". TBL_TRANSACTIONS . " as t,

		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
		$queryCnt2 = "select count(distinct $currency) from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
		
		if($agentType == "Branch Manager")
		{
			$query2 .= " and a.parentID = '$parentID'";				
			$queryCnt2 .= " and a.parentID = '$parentID'";			
		} 	
	}
	else
	{

		$query2 = "select distinct $currency from ". TBL_TRANSACTIONS . " as t,

		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;
		$queryCnt2 = "select count(distinct $currency)  from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;
	}
	
	
	if (CONFIG_CNTRY_CURR_FILTER == "1")
{		
	if($_POST["country"]!="")
		 $country = $_POST["country"];
		elseif($_GET["country"]!="") 
			 $country = $_GET["country"];
		
	if($_POST["currency"]!="")
		$currency2 = $_POST["currency2"];
		elseif($_GET["currency"]!="") 
			$currency2 = $_GET["currency2"];	
}
	
	if($_GET["search"]!="search")
	{
			
		$_SESSION["rgrandTotal"] = "";
		$_SESSION["rCurrentTotal"]="";	

		$rfMonth = $_POST["fMonth"];
		$rfDay = $_POST["fDay"];
		$rfYear = $_POST["fYear"];
		
		$rtMonth = $_POST["tMonth"];
		$rtDay = $_POST["tDay"];
		$rtYear = $_POST["tYear"];
		
		$ragentName = $_POST["agentName"];
		$stat = $_POST["stat"];
	}

	$fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
	$toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	$queryDate = " (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	
	$cancelDate = " (t.cancelDate >= '$fromDate 00:00:00' and t.cancelDate <= '$toDate 23:59:59')";
	if(CONFIG_CANCEL_REVERSE_COMM == '1')
	{
		$query2 .=  " and ($queryDate or $cancelDate)";				
		$queryCnt2 .=  " and ($queryDate or $cancelDate)";				
	}else{
		$query2 .=  " and ($queryDate or $cancelDate)";				
		$queryCnt2 .=  " and ($queryDate or $cancelDate)";				
		}

	if($ragentName != "")
	{
		if($ragentName != "all")
		{
			$query2 .= " and a.userID='".$ragentName."' ";
			$queryCnt2 .= " and a.userID='".$ragentName."' ";
		}
	}
	
	if($stat != "")
	{
	
			$query2 .= " and t.transStatus ='".$stat."' ";
			$queryCnt2 .= " and t.transStatus ='".$stat."' ";
	
	}
	if(CONFIG_CANCEL_REVERSE_COMM != '1')
	{
		$query2 .= " AND NOT (t.transStatus ='".$cancelEffectOn."' and (t.refundFee = 'Yes' or t.refundFee = '')) ";
		$queryCnt2 .= " AND NOT (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' or t.refundFee = '')) ";
	}
	
	if (CONFIG_CNTRY_CURR_FILTER == "1")
	{	
		if($country != "") {
			$query2 .= " and (t.fromCountry='".$country."')" ;
			$queryCnt2 .= " and (t.fromCountry='".$country."')" ;
	}
	
		if($currency2 != "") {
			$query2 .= " and (t.currencyFrom='".$currency2."')" ;
			$queryCnt2 .= " and (t.currencyFrom='".$currency2."')" ;
		}
	}	
	
	 $query2 .= "  order by transDate DESC";
	$exportQuery = $query2;
  $query2 .= " LIMIT $offset , $limit";
	$contentsTrans2 = selectMultiRecords($query2);

	$allCount =countRecords($queryCnt2);
	
}

//debug($query2);

?>
<html>
<head>
	<title>Agent Commission Report</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#000000" size="2"><?=__("Agent")?> Commission 
      Report </font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666" id="searchTable">
        <form action="commission_summary_report.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
        <br>
		<?  
		if($condition)
		{
		?>
				  <?=__("Agent")?> Name 
				  <select name="agentName" style="font-family:verdana; font-size: 11px;">
			  <option value="">- Select Agent-</option>
			  <option value="all">All</option>
				<?
				$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
				if($agentType == "Branch Manager")
				{
					$agentQuery .= " and parentID = '$parentID' ";
				}
				$agentQuery .=" order by agentCompany";
				
						$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
				<?
					}
				?>
				</select>      
			 <script language="JavaScript">
			SelectOption(document.Search.agentName, "<?=$ragentName?>");
				</script>
			      
		<? }//End Admin If?>
		
		<br>
		Select Status 
	   	<select name="stat" style="font-family:verdana; font-size: 11px;">
			  <option value="">- Select Status-</option>
			  <option value="Pending">Pending</option>
		  <option value="Processing">Processing</option>
		  <option value="Recalled">Recalled</option>
		  <option value="Rejected">Rejected</option>
		  <option value="Suspended">Suspended</option>
		  <option value="amended">Amended</option>
		  <option value="Cancelled">Cancelled</option>
		  <option value="AwaitingCancellation">Awaiting Cancellation</option>
		  <option value="Authorize">Authorized</option>
		  <option value="Failed">Undelivered</option>
		  <option value="Delivered">Delivered</option>
		  <option value="Out for Delivery">Out for Delivery</option>
		  <option value="Picked up">Picked up</option>
		  <option value="Credited">Credited</option>
		  <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
		  <option value="Cancelled - Returned">Cancelled - Returned</option>
		  <? } ?>
		  </select>   
				<script language="JavaScript">
			SelectOption(document.Search.stat, "<?=$stat?>");
				</script>
			
			<?	if (CONFIG_CNTRY_CURR_FILTER == "1") {  ?>
        <br>
         <select name="country" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Country - </option> 
				<?
					$queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType like '%origin%' ";
					$countryData = selectMultiRecords($queryCountry);
					for($k = 0; $k < count($countryData); $k++)
					{?>
						 <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].country, "<?=$country?>");
			</script>
		&nbsp;&nbsp;
		<select name="currency2" style="font-family:verdana; font-size: 11px; width:130">
				<option value="">-Select Currency-</option> 
				<?
					$queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyName']?>"><?=$currencyData[$k]['currencyName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].currency2, "<?=$currency2?>");
			</script>
		
		&nbsp;&nbsp;
			<?	}  ?>
		
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
       <form action="commission_summary_report.php" method="post" name="trans">


          <?
			if ($allCount > 0){
			
			
			
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF" id="pagination">
                <tr>
                  <td>
                    <?php /* if (count($contentsTrans2) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans2));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ; */ ?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&search=search";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&search=search"?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&search=search";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right">&nbsp; <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"><font color="#005b90">&nbsp;</font><--/a-->&nbsp;
                  </td>
                  <?php }
				  } ?>
                </tr>
              </table>
	    <tr>
            <td>&nbsp;</td>
        </tr>
		 
		 
		<?
		if(count($contentsTrans2) > 0)
		{
			$companyLabel="";			
			if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1")) {
				if(CONFIG_FEE_DEFINED == '1'){
					$value_fee = CONFIG_FEE_NAME.", ";
				}else{ 
					$value_fee = "$systemPre Fee, ";
				}
				$companyLabel="$company Commission,";
			}	
			if(CONFIG_REPORTS_SURCHARG == '1'){
				$value_surcharge = "Surcharges, ";
			}else{
				$value_surcharge = "";
			}
			if(CONFIG_TOTALS_FIELD_OFF != "1"){
				$value_totals = "Totals, ";
			}else{
				$value_totals = "";
			}
			if (CONFIG_SHOW_MANUAL_CODE == '1')
			{
				$value_manualCode = $manualCode.", ";
			}else{
				$value_manualCode = "";
				}
			
			$values=("Agent ID, Agent Name, ".$value_manualCode."Transaction Amount, ".$value_fee."Agent Commission,".$companyLabel." ".$value_surcharge.$value_totals."Transactions Count");
			
			
			
		?>
      <tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Dated <? echo date("m.d.y");?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> Commission Report </strong></span></td>
        </tr>                      
        <tr>                       
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><? if($ragentName != "all" || $ragentName == ""){$agentNa = selectFrom("select username,name from " . TBL_ADMIN_USERS . " where userID='".$ragentName."'");echo " ".$agentNa["name"];}else {echo(" for All ". __("Agent"));}?> </span></td>
        </tr>                      
        <tr>                       
            <td height="25" nowrap bgcolor="#C0C0C0" align="center">
              <span class="tab-u">
              <? if($rfMonth != "" && $fromDate < $toDate){echo "Between Dates ".$rfMonth."-".$rfDay."-".$rfYear." and ".$rtMonth."-".$rtDay."-".$rtYear ;}?>
              </span></td>
        </tr>
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table border="0" bordercolor="#EFEFEF" width="100%">
			<!--tr bgcolor="#C0C0C0">
		  <td>&nbsp;</td>
			<td>&nbsp;</td>
            <td>Cumulative </td>
			<td>Total</td>
			<td> <? echo number_format($allAmount,2,'.',',');?></td>
			<td>Agent Commision </td>
			<td><? echo number_format($allAgent,2,'.',',');?></td>
			<td><? echo $company;?> Commission</td>
			<td><? echo number_format($allSamba,2,'.',',');?></td>
        </tr-->
			<tr bgcolor="#FFFFFF">
				<? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){ ?>
				 <td width="100"><span class="style1">Origen</span></td>
				  <td width="100"><span class="style1">Facts</span></td>
				
				<? }else{ ?>
			  <td width="100"><span class="style1"><?=__("Agent")?> ID</span></td>
			  <td width="100"><span class="style1"><?=__("Agent")?> Name</span></td>
			  <? } ?>
			  <?
			  if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
			  	<td width="75"><span class="style1"><? echo $manualCode;?></span></td>
				<?	
				}  
				?>
			  <td width="100"><span class="style1">Transaction Amount</span></td>	
			  
			  
			  	<? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){ ?>
			  	<td width="100"><span class="style1">Com</span></td>	
			  	<? }else{ 
				if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{
				?>	  
			  <td width="100"><span class="style1"><span class="style1"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></span></td>
			  	<? } ?>
			  <td width="100"><span class="style1">
			    <?=__("Agent")?> 
			    Commission</span></td>
			  		<? if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{?>
			  <td width="100"><span class="style1"><? echo $company;?> Commission</span></td>
			  <? } 
			  }
			  ?>
			  
			   <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
			   
			   <td width="100"><span class="style1">Agent:Profit Made</span></td>
			   <? } ?>
			   
			    <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
			   
			   <td width="100"><span class="style1">Agent:Loss Incurred</span></td>
			   <? } ?>
			   
			  <?
			  //if(CONFIG_REPORTS_SURCHARG == '1')
			   if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{ ?>
			  
			  	<td width="75"><span class="style1">Surcharges</span></td>
			  <?
			  }	
			  if(CONFIG_TOTALS_FIELD_OFF != "1"){
			  ?>
			  	<td width="75"><span class="style1">Totals</span></td>
			  <?
				}
			  ?>
			  <? if(CONFIG_AGENT_COMMISSION_REPORT != "1"){ ?>
			  <td width="75"><span class="style1">Transactions Count</span></td>
			  <? } ?>
			  </tr>
			  
			   
     <? for($i=0;$i < count($contentsTrans2);$i++)
			{
				///////
				$totalAmount = 0;
			$totalFee = 0;
			$totalAgent = 0;
			$totalCompany = 0;
			$totalSurcharges = 0;
			$totalCounter = 0;
				
				if($condition)
				{
					$query = "select distinct custAgentID from ". TBL_TRANSACTIONS . " as t,
					" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
					
					if($agentType == "Branch Manager")
					{
						$query .= " and a.parentID = '$parentID'";				
					} 	
				}
				else
				{
					$query = "select distinct custAgentID from ". TBL_TRANSACTIONS . " as t,
					" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;
				}
				
				if(CONFIG_CANCEL_REVERSE_COMM == '1')
				{
					$query .=  " and ($queryDate or $cancelDate)";				
				}else{
					$query .=  " and ($queryDate or $cancelDate)";				
				}
			
				if($ragentName != "")
				{
					if($ragentName != "all")
					{
						$query .= " and a.userID='".$ragentName."' ";
					}
				}
				
				if($stat != "")
				{
						$query .= " and t.transStatus ='".$stat."' ";
				}
				
				if(CONFIG_CANCEL_REVERSE_COMM != '1')
				{
					$query .= " AND NOT (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' or t.refundFee = '')) ";
				}
				if(CONFIG_CURRENCY_BASED_REPORTS == '1')
				{
					$query .= " and currencyFrom = '".$contentsTrans2[$i]["currencyFrom"]."'";
					}else{
				$query .= " and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";
				}
				
				///////////////////////////////////////If Client Wants To Use Multiple Country and Currency  For Sending//////Enterprise Liscence
			 if (CONFIG_CNTRY_CURR_FILTER == "1")
			 {		
					if($country != "")
					{
						$query .= " and (t.fromCountry='".$country."')";
					}
		
					if($currency2 != "")
					{
						$query .= " and (t.currencyFrom='".$currency2."')";
					}
			  }
			  ///////////////////////////Enterprise Liscence///////////////////////////
				

			 // echo "  2nd Query".$query .= "  order by transDate DESC";
			// 	echo("--Main Query--");
				$contentsTrans = selectMultiRecords($query);
				
				/////
			  
		     for($j=0;$j < count($contentsTrans);$j++)
			{
				if(CONFIG_REPORTS_SURCHARG == '1')
				{
					$extra = ", bankCharges, cashCharges, admincharges, outCurrCharges";	
				}else{
					$extra = "";
					}
				$agentData = selectFrom("select userID,username, agentCompany, name, agentContactPerson, paymentMode from ".TBL_ADMIN_USERS." where userID = '".$contentsTrans[$j]["custAgentID"]."'");
				
				$queryTrans = "select transID,transAmount, refNumber, IMFee, AgentComm $extra from ". TBL_TRANSACTIONS . " as t where t.custAgentID = '".$contentsTrans[$j]["custAgentID"]."' and $queryDate ";
				
					if(CONFIG_CANCEL_REVERSE_COMM != '1')
				{
					$queryTrans .= "  AND NOT (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' or t.refundFee = '')) ";
				}
			
				if($stat != "")
				{
					$queryTrans .= " and t.transStatus ='".$stat."' ";
				}
				
				if(CONFIG_CURRENCY_BASED_REPORTS == '1')
				{
					$queryTrans .= " and currencyFrom = '".$contentsTrans2[$i]["currencyFrom"]."'";		
				}else{
					$queryTrans .= " and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";	
					}
				
				if (CONFIG_CNTRY_CURR_FILTER == "1")
			  {		
					if($country != "")
					{
						$queryTrans .= " and (t.fromCountry='".$country."')";
					}
		
					if($currency2 != "")
					{
						$queryTrans .= " and (t.currencyFrom='".$currency2."')";
					}
				}
				//echo(" FirstQuery--".$queryTrans."--");
				$transactionsData = selectMultiRecords($queryTrans);
				
					$queryTrans2 = "select transAmount, IMFee, AgentComm $extra from ". TBL_TRANSACTIONS . " as t where t.custAgentID = '".$contentsTrans[$j]["custAgentID"]."' and $cancelDate ";
					if($stat != "")
					{
						$queryTrans2 .= " and t.transStatus ='".$stat."' ";
					}
					
					if(CONFIG_CURRENCY_BASED_REPORTS == '1')
					{
						$queryTrans2 .= " and currencyFrom = '".$contentsTrans2[$i]["currencyFrom"]."'";		
					}else{
						$queryTrans2 .= " and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";	
						}
					
					if (CONFIG_CNTRY_CURR_FILTER == "1")
				 	{	
						if($country != "")
						{
							$queryTrans2 .= " and (t.fromCountry='".$country."')";
						}
			
						if($currency2 != "")
						{
							$queryTrans2 .= " and (t.currencyFrom='".$currency2."')";
						}
					}
					
					$queryTrans2 .= " and t.transStatus ='".$cancelEffectOn."' and t.refundFee ='No'";
					//echo(" SecondQuery--".$queryTrans2."--");
					$transactionsData2 = selectMultiRecords($queryTrans2);
					
				if(CONFIG_CANCEL_REVERSE_COMM == '1')
				{
					$queryCancel = "select transID,transAmount, IMFee, AgentComm $extra from ". TBL_TRANSACTIONS . " as t where t.custAgentID = '".$contentsTrans[$j]["custAgentID"]."' and $cancelDate ";
								
					if($stat != "")
					{
						$queryCancel .= " and t.transStatus ='".$stat."' ";
					}
					$queryCancel .= " and (t.transStatus ='".$cancelEffectOn."' and (t.refundFee ='Yes' or t.refundFee = ''))";
					
					if(CONFIG_CURRENCY_BASED_REPORTS == '1')
					{
						 $queryCancel .= " and currencyFrom = '".$contentsTrans2[$i]["currencyFrom"]."'";		
					}else{
						 $queryCancel .= " and currencyTo = '".$contentsTrans2[$i]["currencyTo"]."'";	
					}
					
					///////////////////////////////////////If Client Wants To Use Multiple Country and Currency  For Sending//////Enterprise Liscence	
					if (CONFIG_CNTRY_CURR_FILTER == "1")
					{			
							if($country != "")
							{
								$queryCancel .= " and (t.fromCountry='".$country."')";
							}
				
							if($currency2 != "")
							{
								$queryCancel .= " and (t.currencyFrom='".$currency2."')";
							}
					}
				///////////////////////////////////////If Client Wants To Use Multiple Country and Currency  For Sending//////Enterprise Liscence	
				
							//echo("  CancelQuery--".$queryCancel."--");	
					$cancelData = selectMultiRecords($queryCancel);
		
				}
								
				$queryString = "commission";
				$allFee=0;
				$allAgent=0;
				$allAmount=0;
				$allCompany=0;
				$surcharges = 0;
			
				for($k=0;$k < count($transactionsData);$k++)
				{
						$transID = $transactionsData[$k]["transID"];
						if(CONFIG_SHOW_AGENT_PROFIT == '1')
						{	
								$agentProfit = selectFrom("select margin from transactionExtended where transID = '".$transID."' and margin > '0'");
									  $totalAgentProfit = $totalAgentProfit + $agentProfit["margin"];
						}
						if(CONFIG_SHOW_AGENT_LOSS == '1')	
						{
								$agentLoss = selectFrom("select margin from transactionExtended where transID = '".$transID."' and margin < '0'");
								
							  $totalAgentLoss = $totalAgentLoss + $agentLoss["margin"];
						} 
						$refNumber = $transactionsData[$k]["refNumber"];
						$allAmount += $transactionsData[$k]["transAmount"];
						$allFee += $transactionsData[$k]["IMFee"];
						$allAgent += $transactionsData[$k]["AgentComm"];
						$allCompany += $transactionsData[$k]["IMFee"] - $transactionsData[$k]["AgentComm"];
						$surcharges += $transactionsData[$k]["bankCharges"] + $transactionsData[$k]["cashCharges"] + $transactionsData[$k]["admincharges"] + $transactionsData[$k]["outCurrCharges"];
				}
				for($l=0;$l < count($transactionsData2);$l++)
				{
						$allAmount -= $transactionsData2[$l]["transAmount"];
						/*$allFee += $transactionsData[$l]["IMFee"];
						$allAgent += $transactionsData[$l]["AgentComm"];
						$allCompany += $transactionsData[$l]["IMFee"] - $transactionsData[$l]["AgentComm"];
						$surcharges += $transactionsData[$l]["bankCharges"] + $transactionsData[$l]["cashCharges"] + $transactionsData[$l]["admincharges"] + $transactionsData[$k]["outCurrCharges"];*/
				}
				
				for($k=0;$k < count($cancelData);$k++)
				{
						$allAmount  -=  $cancelData[$k]["transAmount"];
						$allFee     -= $cancelData[$k]["IMFee"];
						$allAgent   -=   $cancelData[$k]["AgentComm"];
						$allCompany -= ($cancelData[$k]["IMFee"] - $cancelData[$k]["AgentComm"]);
						$surcharges -= ($cancelData[$k]["bankCharges"] + $cancelData[$k]["cashCharges"] + $cancelData[$k]["admincharges"] + $cancelData[$k]["outCurrCharges"]);
				
					if(CONFIG_SHOW_AGENT_PROFIT == '1')
						{	
						
								$agentProfit = selectFrom("select margin from transactionExtended where transID = '".$cancelData[$k]["transID"]."' and margin > '0'");
								  $totalAgentProfit = $totalAgentProfit - $agentProfit["margin"];
								
						}
						if(CONFIG_SHOW_AGENT_LOSS == '1')	
						{
								$agentLoss = selectFrom("select margin from transactionExtended where transID = '".$cancelData[$k]["transID"]."' and margin < '0'");
								
							  $totalAgentLoss = $totalAgentLoss - $agentLoss["margin"];
						} 
				
				}
				$counter = (count($transactionsData) - count($cancelData) - count($transactionsData2));
				
				$totalAmount += $allAmount;
				$totalFee += $allFee;
				$totalAgent += $allAgent;
				$totalCompany += $allCompany;
				$totalSurcharges += $surcharges;
				if(CONFIG_TOTALS_FIELD_OFF != "1"){
					$totals = $allAmount + $allCompany;
					if (CONFIG_REPORTS_SURCHARG == '1') 
					{
						$totals = $totals + $surcharges ;
					}
					
					if(CONFIG_MONTH_END_COMMISSION == '1' && ($agentData["paymentMode"] == "At End Of Month" || $agentData["paymentMode"] == "include"))
					{
						$totals = $totals + $allAgent;
					}
					
				}
				$queryString = "commission";
				?>
				<tr bgcolor="#FFFFFF">
					<? 
					//Added by Niaz Ahmad against ticket # 2555 at 17-10-2007
					
					
					$url="commission_detail_report.php";
				  
					?>
					<? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){ ?>
					  <td width="75" bgcolor="#FFFFFF"><? echo $agentData["agentCompany"]." [".$agentData["username"]."]"?></td>
					 <td width="75" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('<? echo $url ?>?custAgentID=<? echo $agentData["userID"]?>&currencyTo=<?=$contentsTrans2[$i]["$currency"]?>&transStatus=<?=$stat?>&toDate=<? echo $toDate;?>&fromDate=<? echo $fromDate; ?>&queryString=<? echo $queryString; ?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo($counter); $totalCounter += $counter;?></font></strong></a></td>
					<? }else{ ?>
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('<? echo $url ?>?custAgentID=<? echo $agentData["userID"]?>&currencyTo=<?=$contentsTrans2[$i]["$currency"]?>&transStatus=<?=$stat?>&toDate=<? echo $toDate;?>&fromDate=<? echo $fromDate; ?>&queryString=<? echo $queryString; ?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $agentData["username"]?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $agentData["name"]?></td>
				  <? } ?>
				  <?
				  if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
			  		<td width="75" bgcolor="#FFFFFF"><? echo $refNumber ?></td>
					<?	
					}  
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ($allAmount." ".$contentsTrans2[$i]["currencyFrom"]);?></td>	
				  <? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){ ?>  				  
				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($allFee,2,'.',','); ?></td>
				  <? }else{ 
				   if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{
				  ?>
				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($allFee,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ". $contentsTrans2[$i]["currencyFrom"];} ?></td>
				 <? } ?>
				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($allAgent,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];} ?></td>
				<? if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{ ?>
				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($allCompany,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];} ?></td>
				  <? 
				  }
				  } ?>
				  <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
				  
				 				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($totalAgentProfit,2,'.',','); ?></td>
				  
				  <? } ?>
				    <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
				  
				 				  <td width="100" bgcolor="#FFFFFF"><? echo number_format($totalAgentLoss,2,'.',','); ?></td>
				  
				  <? } ?>
				  
				  
				   <?
				 // if(CONFIG_REPORTS_SURCHARG == '1')
				 if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{
				  ?>
				  	<td width="100" bgcolor="#FFFFFF"><?=number_format($surcharges,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></td>
				  <?
				  }	
				  if(CONFIG_TOTALS_FIELD_OFF != "1"){
				  	
				 		$gTotals += $totals;
				 	?>
				  <td width="75" bgcolor="#FFFFFF"><? echo number_format($totals,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];} ?></td>
				  <?
				  }
				  ?>
				    <? if(CONFIG_AGENT_COMMISSION_REPORT != "1"){ ?> 
				   <td width="75" bgcolor="#FFFFFF"><? echo($counter); 
				   $totalCounter += $counter;

?></td>
<? } ?>
				</tr>
				<?
			}
		?>
		<tr bgcolor="#CCCCCC" >
			 
			  <td width="50" align="right"><span class="style3">Total:</span></td>
			  <td width="90">&nbsp;</td>
			  <?	
			  if (CONFIG_SHOW_MANUAL_CODE == '1') 
			  {
			  ?>
        	<td width="27" height="28">&nbsp;</td>
        <?
        }
        ?>
			  
			  <td width="100"><strong><? echo $currencyFrom." ".number_format($totalAmount,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];} ?></strong></td>
			   <? if(CONFIG_AGENT_COMMISSION_REPORT == "1"){
			   	?>
			   		<td width="90"><strong><? echo number_format($totalFee,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></strong></td>
			   	<? }else{?> 
			   		<td width="90"><strong><? echo number_format($totalFee,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></strong></td>
				  <? 	if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{ ?>
					<td width="90"><strong><? echo number_format($totalAgent,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></strong></td>
					<? } 
					if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				{	?>
					  
					  <td width="90"><strong><? echo number_format($totalCompany,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></strong></td>
				   <? 
				   }
				   } ?>
				   
				   <? if(CONFIG_SHOW_AGENT_PROFIT == "1"){ ?>
				   
				   <td width="90">&nbsp;<strong><? echo number_format($totalAgentProfit,2,'.',',');?><strong></td>
				   
				   <? } ?>


					 <? if(CONFIG_SHOW_AGENT_LOSS == "1"){ ?>
				   
				   <td width="90">&nbsp;<strong><? echo number_format($totalAgentLoss,2,'.',',');?><strong></td>
				   
				   <? } ?>
				   
			   <?
				//  if(CONFIG_REPORTS_SURCHARG == '1')
			if((!strstr(CONFIG_HIDE_COMMISSION_REPORT_COLUMNS, $agentType.",") && CONFIG_HIDE_COMMISSION_REPORT_COLUMNS!="1"))
				
				  {?>
				  	<td width="90"><strong><? echo number_format($totalSurcharges,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];};?></strong></td>
				  <?
				  }	
				  if(CONFIG_TOTALS_FIELD_OFF != "1"){
				  ?>
				   	<td width="74"><strong><?=number_format($gTotals,2,'.',','); if(CONFIG_SUB_DISTRIBUTOR_COMMISSION_REPORT == "1"){echo " ".$contentsTrans2[$i]["currencyFrom"];}?></strong></td>
				  <?
				  }
				  ?>
				  
				 			  
			 <? if(CONFIG_AGENT_COMMISSION_REPORT != "1"){ ?> 
			 <td width="74"><?=$totalCounter?></td>
			 <? } ?>
			</tr>
		<?
		}
			?>
			          </table>
			
		  </td>
        </tr>
		<tr id="actionBtn">
		  	<td> 
				<div align="center" class='noPrint'>
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
					<input type="button" name="export" value="Generate Excel" onClick="action='export_excel.php'; submit();">
					<input type="hidden" name="totalFields" value="<?=$values?>">
					<input type="hidden" name="query" value="<?=$exportQuery?>">
					<input type="hidden" name="transDate" value="<?=$queryDate?>">
					<input type="hidden" name="accountDate" value="<?=$cancelDate?>">
					<input type="hidden" name="reportName" value="Agent-Commission-Report">
					<input type="hidden" name="reportFrom" value="export_commission_summary.php">
					<input type="hidden" name="agentName" value="<?=$ragentName?>">
					<input type="hidden" name="stat" value="<?=$stat?>">
					<input type="hidden" name="currency" value="<?=$currency?>">
                    <input type="hidden" name="pageUrl" value="<?=$_SERVER['PHP_SELF'];?>">
					<?
					if (CONFIG_CNTRY_CURR_FILTER == "1")
					{
						?>
						<input type="hidden" name="country" value="<?=$country?>">
						<input type="hidden" name="currency2" value="<?=$currency2?>">
						<?
					}
						?>	
						
				</div>
			</td>
		  </tr>
		  <?
			} else{ // greater than zero
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
				</td>
			</tr>
			<? }?>
		</form>
      </table></td>
  </tr>
</table>
</body>
</html>
