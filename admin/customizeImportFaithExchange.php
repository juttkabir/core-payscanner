<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
require_once './cm_xls/Excel/reader.php';

if($_POST["Submit"] != "")
{
	if ($_FILES["csvFile"]["size"] <= 0)
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	} 
	else 
	{
	
		$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));

		if ($ext == ".xls" )
		{
			if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
			{
				$filename = $_FILES["csvFile"]["tmp_name"];
				$data = new Spreadsheet_Excel_Reader();
				// Set output Encoding.
				$data->setOutputEncoding('CP1251');
				$data->read($filename);
				
				$intTotalCustomerCount = 0;
				$intTotalBeneficiaryCount = 0;
				$intDuplicateCustomer = 0;
				$intDuplicateBeneficiary = 0;
				$id = 0;
				
				//debug($data->sheets[0]['cells'],true);
				
				for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
				{
					

						/*						
						[1] => TransID
						[2] => TransDate
						[3] => SterlingAmt
						[4] => FaithRate
						[5] => CommintRate
						[6] => NairaAmt
						[7] => TransferCharges
						[8] => FaithNaira_C
						[9] => Ben_names
						[10] => Ben_phone
						[11] => Ben_idcard
						[12] => Ben_password
						[13] => Ben_phone2
						[14] => Ben_collectime
						[15] => Ben_collectpoint
						[16] => Sender_surname
						[17] => Sender_othername
						[18] => Sender_Add
						[19] => Sender_city
						[20] => Sender_postcode
						[21] => Sender_phone
						[22] => Sender_email
						[23] => Remaks
						[24] => OtherRemaks
						[25] => Ben_collectime2
						[26] => Agent_name
						[27] => Ukbank_branch
						[28] => Paytmode
						*/
						
						$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //TransID
						$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //TransDate
						$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //SterlingAmt*
						$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);  //FaithRate
						$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	// CommintRate*
						$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);	// NairaAmt
						$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);	// TransferCharges
						$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	//FaithNaira_C
						$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);  //Ben_names
						$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);	// Ben_phone*
						$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);	// Ben_idcard*
						$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Ben_password
						$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //Ben_phone2
						$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //Ben_collectime*
						$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //Ben_collectpoint
						$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //Sender_surname
						$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);// Sender_othername
						$data1[18]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][18]);  //Sender_Add
						$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //Sender_city
						$data1[20]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][20]);  //Sender_postcode
						$data1[21]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][21]);  //Sender_phone
						$data1[22]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][22]);  //Sender_email
						$data1[23]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][23]);  //Remaks
						$data1[24]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][24]);  //OtherRemaks
						$data1[25]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][25]);  //Ben_collectime2
						$data1[26]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][26]);  //Agent_name
						$data1[27]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][27]);  //Ukbank_branch
						$data1[28]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][28]);  //Paytmode
						
						
						
					// Check for record existance based on customer ID
	
					$intCustomerId = "";
					
					$sql = "select customerID from customer where firstName='".$data1[16]."' and lastName='".$data1[17]."' and phone='".$data1[21]."'";
					$result = mysql_query($sql) or die(mysql_error().debug($sql));
					$numrows = mysql_num_rows($result);
					
					
					
					if($numrows < 1)
					{
						//$password = createCode();
						//$customerName = $data1[2] . " " . $data1[3] . " " . $data1[4];
						//$accountName = "C-".$data1[1];
						// Insert
						/*
						[16] => Sender_surname
						[17] => Sender_othername
						[18] => Sender_Add
						[19] => Sender_city
						[20] => Sender_postcode
						[21] => Sender_phone
						[22] => Sender_email
						*/
						$strInsertCustomer = "INSERT INTO ".TBL_CUSTOMER." 
											(
												firstName, lastName, created, 		
												Address, Country, City, Zip,  
												Phone, email, acceptedTerms
											)
										VALUES 
											(
												'".$data1[16]."', '".$data1[17]."', NOW(), 
												'".$data1[18]."', 'United Kingdom','".$data1[19]."', '".$data1[20]."',
												'".$data1[21]."', '".$data1[22]."', 'Y'
											)";
						$resultCustomer = mysql_query($strInsertCustomer) or die(mysql_error().debug($strInsertCustomer));
						$intTotalCustomerCount++;
						
						$intCustomerId = mysql_insert_id();
					}
					else
					{
						$intDuplicateCustomer++;
						$crid = mysql_fetch_array($result);
						$intCustomerId = $crid["customerID"];
					}
					
					
					
					/* Only consider the beneficiary part */
					
					/* Check for duplication */
					
					$sqlBenficiaryCheck = "select benID from ".TBL_BENEFICIARY." where beneficiaryName='".$data1[9]."' and Phone='".$data1[10]."'";
					$resultBeneficiary = mysql_query($sqlBenficiaryCheck) or die(mysql_error().debug($sqlBenficiaryCheck));
					$intRows = mysql_num_rows($resultBeneficiary);
					
					if($intRows < 1)
					{
						/*
						 * Insert into beneficiary table
						[9] => Ben_names
						[10] => Ben_phone
						[11] => Ben_idcard
						[12] => Ben_password
						[13] => Ben_phone2
						[14] => Ben_collectime
						[15] => Ben_collectpoint
						*/				
						/*
						$arrNameParts = explode(" ",$data1[9]);
						$strFields = "";
						$strValues = "";
						if(count($arrNameParts) > 0)
						{
							if(strpos(strtoupper($arrNameParts[0]),"MR"))
							{
								$strFields .= " title, ";
								$strValues .= " '".$arrNameParts[0]."' ";
							}
							
							if(count($arrNameParts) == 2)
							{
								$strFields .= "firstName, lastName";
								$strValues .= " '".$arrNameParts[0]."' ";
							}
							elseif(count($arrNameParts) == 3)
							{
								$strFields .= "firstName, middleName, lastName";
							}
							
						}
						*/

						$strInsertBeneficiary = "INSERT INTO ".TBL_BENEFICIARY." 
												(firstName, Phone, Mobile, 
												IDType, customerID, 
												created, beneficiaryName, Password, Country) VALUES 
											('".$data1[9]."', '".$data1[10]."', '".$data1[13]."', 
											 '".$data1[11]."', '".$intCustomerId."',
											 NOW(), '".$data1[9]."', '".$data1[12]."', 'Nigeria')";
						$resultBen = mysql_query($strInsertBeneficiary) or die(mysql_error().debug($strInsertBeneficiary));
						$intTotalBeneficiaryCount++;
						//debug($strInsertBeneficiary);
						//debug(mysql_insert_id());
					}
					else
					{
						$intDuplicateBeneficiary++;
					}
					
					

				}
				$msg = "$counter New Customers Imported.<br />$counter2 Found Duplicate and Updated.<br />$unlinked Found Missing ID and Ignored.";
				
				//exit;
					// END
			}
			else
			{
				$msg = CAUTION_MARK . " Your file in not uploaded due to some error.";
			}
		}
		else
		{
			$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
		}	
	}
}
/*debug("New Customer Count: ".$intTotalCustomerCount);
debug("New Beneficiary Count: ".$intTotalBeneficiaryCount);
debug("Duplicate Customer Count: ".$intDuplicateCustomer);
debug("Duplicate Beneficiary Count: ".$intDuplicateBeneficiary);*/
?>
<html>
<title>Cutomize Import for Faith Exchange</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> 
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
  <tr>
    <td bgcolor="#6699cc">
		<b><strong><font color="#FFFFFF">Import Customize Data (Sender - Beneficiary, with Relationship)</font></strong></b>
		<h2>ONLY FOR FAITH EXCHANGE</h2>
	</td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0" align="center">
  <tr>
    <td  valign="top"><fieldset>
    <legend class="style2">Import Customize</legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="imp" action="<?=$PHP_SELF?>" method="post" enctype="multipart/form-data">
			  <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Excel File </td>
			    <td width="75%" bgcolor="#DFE6EA">			      <input name="csvFile" type="file" id="csvFile" size="15"></td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
		      <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			    <td colspan="2">
					<h4>New Customer Count: <?=$intTotalCustomerCount?></h4>
					<h4>New Beneficiary Count: <?=$intTotalBeneficiaryCount?></h4>
					<h4>Duplicate Customer Count: <?=$intDuplicateCustomer?></h4>
					<h4>Duplicate Beneficiary Count: <?=$intDuplicateBeneficiary?></h4>
				</td>
			   </tr>
			  <?
			  }
			  ?>
		      
			</table>
		    <br>
        </fieldset></td>
    <td width="431" valign="top">
	</td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>