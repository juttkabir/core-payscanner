<?
session_start();
include ("../include/config.php");
include ("security.php");
$date_time = date('d-m-Y  h:i:s A');
$agentType2 = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

session_register("loginName");
session_register("password");
session_register("personName");
session_register("address");
session_register("Country");
session_register("City");
session_register("postcode");
session_register("phone");
session_register("mobile");
session_register("fax");
session_register("email");
session_register("accessFromIP");
session_register("companyName");
session_register("designation");

if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}

if($_GET["searchBy"])
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}


if ($_GET["flag"]!="Y")	
	{
		$_SESSION["loginName"] = "";
		$_SESSION["password"] =  "";
		$_SESSION["password1"]="";
		$_SESSION["personName"] =  "";
		$_SESSION["address"] =  "";		
		$_SESSION["Country"] =  "";
		$_SESSION["City"] =  "";
		$_SESSION["postcode"] =  "";
		$_SESSION["phone"] =  "";
		$_SESSION["mobile"] =  "";
		$_SESSION["fax"] =  "";
		$_SESSION["email"] =  "";		
		$_SESSION["accessFromIP"] =  "";
		$_SESSION["companyName"] =  "";
		$_SESSION["designation"] =  "";

}

$_SESSION["userID"] = $_GET["userID"];

if($_GET["userID"] != ""){
	$supAgentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");

		
		
		$_SESSION["loginName"] = $supAgentDetails["username"];
		$_SESSION["password"] = $supAgentDetails["password"];
		$_SESSION["password1"] = $supAgentDetails["password"];
		$_SESSION["personName"] = $supAgentDetails["name"];
		$_SESSION["address"] = $supAgentDetails["agentAddress"];			
		$_SESSION["Country"] = $supAgentDetails["agentCountry"];
		$_SESSION["City"] = $supAgentDetails["agentCity"];
		$_SESSION["postcode"] = $supAgentDetails["postCode"];
		$_SESSION["phone"] = $supAgentDetails["agentPhone"];
		$_SESSION["mobile"] = $supAgentDetails["agentFax"];
		$_SESSION["fax"] = $supAgentDetails["agentFax"];		
		$_SESSION["email"] = $supAgentDetails["email"];
		
		$_SESSION["accessFromIP"] = $supAgentDetails["accessFromIP"];
		$_SESSION["companyName"] = $supAgentDetails["agentCompany"];		
		$_SESSION["designation"] = $supAgentDetails["designation"];
		
}
else
{
	if ($_POST["userID"] != "")
	$_SESSION["userID"] = $_POST["userID"];
if ($_POST["loginName"] != "")
	$_SESSION["loginName"] = $_POST["loginName"];
if ($_POST["password"] != "")
	$_SESSION["password"] = $_POST["password"];
if ($_POST["password1"] != "")
	$_SESSION["password1"] = $_POST["password1"];
if ($_POST["personName"] != "")
	$_SESSION["personName"] = $_POST["personName"];
if ($_POST["address"] != "")
	$_SESSION["address"] = $_POST["address"];
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
if ($_POST["postcode"] != "")
	$_SESSION["postcode"] = $_POST["postcode"];
if ($_POST["phone"] != "")
	$_SESSION["phone"] = $_POST["phone"];
if ($_POST["mobile"] != "")
	$_SESSION["mobile"] = $_POST["mobile"];
if ($_POST["fax"] != "")
	$_SESSION["fax"] = $_POST["fax"];
if ($_POST["email"] != "")
	$_SESSION["email"] = $_POST["email"];	
if ($_POST["accessFromIP"] != "")
	$_SESSION["accessFromIP"] = $_POST["accessFromIP"];
if ($_POST["companyName"] != "")
	$_SESSION["companyName"] = $_POST["companyName"];
if ($_POST["designation"] != "")
	$_SESSION["designation"] = $_POST["designation"];
}
?>
<html>
<head>
	<title>Add Super Admin</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	
	
	if(theForm.loginName.value == "" || IsAllSpaces(theForm.loginName.value)){
    	alert("Please provide the Login Name.");
        theForm.loginName.focus();
        return false;
    }
	
	if(theForm.password.value == "" || IsAllSpaces(theForm.password.value)){
    	alert("Please provide the admin's password.");
        theForm.password.focus();
        return false;
    }
	if(theForm.password1.value == "" || IsAllSpaces(theForm.password1.value)){
    	alert("Please provide the admin's confirm password.");
        theForm.password1.focus();
        return false;
    }
	if(theForm.password.value != theForm.password1.value){
    	alert("Both password fields must poses the same value.");
    		theForm.password1.value = "";
        theForm.password1.focus();
        return false;
    }
	
	if(theForm.personName.value == "" || IsAllSpaces(theForm.personName.value)){
    	alert("Please provide the Super admin name.");
        theForm.personName.focus();
        return false;
    }
	if(theForm.address.value == "" || IsAllSpaces(theForm.address.value)){
    	alert("Please provide the agent's address line .");
        theForm.address.focus();
        return false;
    }
	
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the country from the list.");
        theForm.Country.focus();
        return false;
    }

    
   /*  if(theForm.City.value == "" || IsAllSpaces(theForm.City.value)){
    	alert("Please provide the agent's city name.");
        theForm.City.focus();
        return false;
    }
    if(theForm.postcode.value == "" || IsAllSpaces(theForm.postcode.value)){
    	alert("Please provide the agent's Zip/Post Code.");
        theForm.postcode.focus();
        return false;
    }
      */    
	if(theForm.phone.value == "" || IsAllSpaces(theForm.phone.value)){
    	alert("Please provide the admin's phone number.");
        theForm.phone.focus();
        return false;
    }
/*    if(theForm.mobile.value == "" || IsAllSpaces(theForm.mobile.value)){
    	alert("Please provide the admin's mobile number.");
        theForm.mobile.focus();
        return false;
    }
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the admin's email address.");
        theForm.email.focus();
        return false;
    }*/ 	
    if(theForm.companyName.value == "" || IsAllSpaces(theForm.companyName.value)){
    	alert("Please provide the company Name.");
        theForm.companyName.focus();
        return false;
    } 	
   /*  if(theForm.designation.value == "" || IsAllSpaces(theForm.designation.value)){
    	alert("Please provide the designation.");
        theForm.designation.focus();
        return false;
    }*/ 	
	return true;
}
function checkFocus(){
 theForm.loginName.focus();
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
	<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
-->
</style>
</head>
<body onLoad="checkFocus();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex"><? if($_GET["userID"] != "") {?>Update Super Admin <? } else {?> Add Super Admin <? } ?></font></strong></td>
  </tr>
  <form name="theForm" action="confirm_super_admin.php?type=<?=$_GET["type"]?>&userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
<input type="hidden" name="userID" value="<?=stripslashes($_GET["userID"]); ?>">
  <tr>
    <td align="center">
		<table width="528" border="0" cellspacing="1" cellpadding="2" align="center">
          
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="FFFFCCC"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><font color="#FF0000"> 
                    <? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
                    </font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? 
          } 
          if ($_GET["userID"]!="")
          {
          ?>
          <tr bgcolor="#EEEEEE"> 
            <td height="19" colspan="2"><a class="style2" href="manage_super_admin.php?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>">Go Back</a></td>
          </tr>
        <? } ?>
          <tr bgcolor="#EEEEEE"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
           <tr bgcolor="#ededed" colspan="2"> 
            <td><font color="#005b90"><strong>
              Login Information
              </strong></font></td> 
              <td width="361">
               &nbsp;
            </td>           
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>
              Login Name<font color="#ff0000">*</font>
              </strong></font></td>
            <td width="361">
              <input type="text" name="loginName" value="<?=stripslashes($_SESSION["loginName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong> Password<font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="password" name="password" value="<?=stripslashes($_SESSION["password"]); ?>" size="35" maxlength="100">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Confirm Password<font color="#ff0000">*</font></b></font></td>
            <td>
              <input type="password" name="password1" value="<?=stripslashes($_SESSION["password1"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed" colspan="2"> 
            <td><font color="#005b90"><strong>
              Personal Information
              </strong></font></td> 
              <td width="361">
               &nbsp;
            </td>           
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Name<font color="#ff0000">*</font></b></font></td>
            <td>
              <input type="text" name="personName" value="<?=stripslashes($_SESSION["personName"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
           <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Address<font color="#ff0000">*</font></b></font></td>
            <td>
              <input type="text" name="address" value="<?=stripslashes($_SESSION["address"]); ?>" size="35" maxlength="254">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>Country<font color="#ff0000">*</font></b></font></td>
            <td>
              <SELECT name="Country" style="font-family:verdana; font-size: 11px" > 
                <OPTION value="">- Select Country-</OPTION>
                
                <?
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
                if($_GET["ida"] == "Y"){
									$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." where serviceAvailable !='' order by country");
								}
								else
								{
									$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." where country ='United Kingdom' order by country");
								}}
								else
								{
									$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
								}
								
								
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["country"]; ?>">
                <?=$countires[$i]["country"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
              <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b>City</b></font></td>
            <td> 
              <input type="text" name="City" value="<?=$_SESSION[City];?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156" valign="top"><font color="#005b90"><b> Post Code</b></font></td>
            <td>
              <input type="text" name="postcode" value="<?=stripslashes($_SESSION["postcode"]); ?>" size="35" maxlength="15">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Phone<font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="text" name="phone" value="<?=stripslashes($_SESSION["phone"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Mobile</strong></font></td>
            <td>
              <input type="text" name="mobile" value="<?=stripslashes($_SESSION["mobile"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Fax</strong></font></td>
            <td>
              <input type="text" name="fax" value="<?=stripslashes($_SESSION["fax"]); ?>" size="35" maxlength="32">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Email</strong></font></td>
            <td>
              <input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
            <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Access From IP</strong></font></td>
            <td>For multiple please separate by comma. 
              <input name="accessFromIP" type="text" id="accessFromIP" value="<?=stripslashes($_SESSION["accessFromIP"]); ?>" size="40" maxlength="255">
            </td>
            
            <tr bgcolor="#ededed" colspan="2"> 
            <td><font color="#005b90"><strong>
              Organization Information
              </strong></font></td> 
              <td width="361">
               &nbsp;
            </td>  
            <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Company Name<font color="#ff0000">*</font></strong></font></td>
            <td>
              <input type="text" name="companyName" value="<?=stripslashes($_SESSION["companyName"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="156"><font color="#005b90"><strong>Designation</strong></font></td>
            <td>
              <input type="text" name="designation" value="<?=stripslashes($_SESSION["designation"]); ?>" size="35" maxlength="255">
            </td>
          </tr>         
          </tr>
         
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> 
              <input type="submit" value=" Save ">
              &nbsp;&nbsp; 
              <input type="reset" value=" Clear ">
            </td>
          </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
