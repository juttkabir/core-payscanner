<?
session_start();
include ("../include/config.php");
include ("security.php");
$agentType = getAgentType();

$intMaxDays = 300;

$arrUserGroupTitle = array(
							"admin" => "Super Admin",
							"Admin" => "Admin Staff",
							"SUPA"  => "Super Agent",
							"SUBA"  => "Sub Agent",
							"SUPAI"  => "Super AnD",
							"SUBAI"  => "Sub AnD",
							"SUPI"  => "Super Distributor",
							"SUBI"  => "Sub Distributor",
							"Admin Manager"  => "Admin Manager"
							);

/* Update the group rights*/
if(!empty($_REQUEST["update"]))
{
	//debug($_REQUEST);
	foreach($_REQUEST["ad"] as $key => $val)
	{
		if(isset($_REQUEST["haveRight".$key]))
			$strUpdateSql = " update oldTransactionGroupRight 
							  set 
							  	haveRight = 'Y',
								backDays = '".$_REQUEST["backDays".$key]."',
								updated = CURRENT_TIMESTAMP
							  where
							  	id = '".$val."' ";
			
		else
			$strUpdateSql = " update oldTransactionGroupRight 
							  set 
							  	haveRight = 'N',
								backDays = '0',
								updated = CURRENT_TIMESTAMP
							  where
							  	id = '".$val."' ";
		//debug($strUpdateSql);
		
		update($strUpdateSql);
	}
	
}

$strSqlGroupRights = "select * from oldTransactionGroupRight";
$arrGroupRights = selectMultiRecords($strSqlGroupRights);

?>
<html>
<head>
<title> Define View Old Transaction's Rights per User Group </title>	
</head>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
</style>	
<script src="jquery.js"></script>
<script>
function updateForm(ref)
{
	if($("#haveRight"+ref).attr("checked"))
		$("#backDays"+ref).attr("disabled", false);
	else
		$("#backDays"+ref).attr("disabled", true);
		
}
function setForm()
{
	for(c = 0; c < document.getElementById("total").value; c++)
		updateForm(c);
}
</script>
<body onLoad="setForm();">
<br>
<br>
<table width="90%" border="0" cellspacing="3" cellpadding="5" align="center">
	<tr>
		<h2>Define View Old Transaction's Rights per User Group</h2>	
	</tr>   
    <tr> 
		<td bgcolor="#EFEFEF">
			<form name="rightUpdate" id="rightUpdate" action="oldTransactionGroupRights.php" method="post">
			<table align="center" width="88%" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#DFE6EA"> 
					<td class="style1" align="center"><strong>User Group</strong></td>
					<td class="style1" align="center"><strong>Old Transaction Right</strong></td>
					<td class="style1" align="center"><strong>How many days back?</strong></td>
					<td class="style1" align="center"><strong>Last Updated</strong></td>
				</tr>
				<?
				for($i = 0; $i < count($arrGroupRights); $i++)
				{      
				?>
					<tr bgcolor="#ffffff"> 
						<td align="center"><b>
							<?
								if(isset($arrUserGroupTitle[$arrGroupRights[$i]["group"]]))
									echo $arrUserGroupTitle[$arrGroupRights[$i]["group"]];
								else
									echo $arrGroupRights[$i]["group"];
							?>
							</b>
							</td>
						<td align="center">
							<?
								if($arrGroupRights[$i]["haveRight"] == "Y")
									$isHaveRight = 'checked="checked"';
								else
									$isHaveRight = '';
							?>
							<input type="checkbox" id="haveRight<?=$i?>" name="haveRight<?=$i?>" value="<?=$arrGroupRights[$i]["haveRight"]?>" <?=$isHaveRight?> onClick="updateForm(<?=$i?>);" />
						</td>
						<td align="center">
							<select name="backDays<?=$i?>" style="width:100px" id="backDays<?=$i?>">
								<?php
									if(empty($arrGroupRights[$i]["backDays"]))
										$arrGroupRights[$i]["backDays"] = 1;
										
									for($day = 1; $day <= $intMaxDays ; $day++)
									{
										if($day == $arrGroupRights[$i]["backDays"])
											echo "<option value='".$day."' selected='selected'>".$day."</option>";
										else
											echo "<option value='".$day."'>".$day."</option>";
									}
								?>
							</select>
						</td>
						<td align="center">
							<?=$arrGroupRights[$i]["updated"]?>
							<input type="hidden" name="ad[]" value="<?=$arrGroupRights[$i]["id"]?>" />
						</td>
					</tr>
				<?
				}
				?>
				<tr>
					<td colspan="4" align="center">
					    <br /><br />
						<input type="hidden" name="total" id="total" value="<?=$i?>" />
						<input type="submit" name="update" value="Update Rights" />&nbsp;&nbsp;<input type="button" value="Reset Rights" onClick='document.getElementById("rightUpdate").reset();setForm();' />
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
</body>
</html>