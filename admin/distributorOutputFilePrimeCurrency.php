<?
session_start(); 
include ("../include/config.php");
include ("security.php");

$filters = "";

/* If to export the old transaction */
if($_REQUEST["export"] == "old")
	$filters .= " and tr.dispatchDate != '' ";
else /* else it will search for new transactions */
	$filters .= " and tr.dispatchDate IS NULL ";

// Fetch Distributor Name, if available
if(!empty($_REQUEST["distributors"]))
{
	$sqlDist = "
			SELECT 
				name,
				userID
			FROM 
				admin 
			WHERE 
				userID = ".$_REQUEST["distributors"]."
			ORDER BY
				name
			";
	$resultDist = mysql_query($sqlDist) or die(__LINE__.": ".mysql_error());
	
	while($rsDist = mysql_fetch_array($resultDist))
	{
		$distName = $rsDist["name"];
		$distID =   $rsDist["userID"];
	}
}


/**
 * At the new transactions dont display the date search filter
 * @Ticket #4222
 */
if(defined("CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF") && CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF == "1" && empty($_REQUEST["Submit"])) 
{
    $_REQUEST["Submit"] = "search";

}

if($_REQUEST["Submit"]) //!empty($_REQUEST["Submit"]) && $_REQUEST["Submit"] == "search"
{
	/**
	 * Date filter
	 */
	$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
	$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
	
	 $bolShowSearchFilter = false;
	 if($_REQUEST["export"] == "old")
		$bolShowSearchFilter = true;
	 else
	 {
		if(!defined("CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF"))
			$bolShowSearchFilter = true;
		elseif(CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF == "1")
			$bolShowSearchFilter = false;
	 }
	
	if($bolShowSearchFilter) 
		$filters .= " and (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";

	if(!empty($_REQUEST["distributors"]))
		$filters .= " and tr.benAgentID = '".$_REQUEST["distributors"]."' ";
	
	if(!empty($_REQUEST["transactionStatus"]))
		$filters .= " and tr.transStatus = '".$_REQUEST["transactionStatus"]."' ";
	else
		$filters .= " and (tr.transStatus = 'Authorize' or tr.transStatus = 'Amended') ";
	
	$sql = "select 
					tr.transID,
					tr.refNumberIM, 
					tr.transDate,
					tr.localAmount,
					tr.refNumber,
					tr.tip,
					tr.transAmount,
					tr.exchangeRate,
					tr.toCountry,
					tr.transType,
					tr.collectionPointID,
					tr.benAgentID,
					ben.firstName as bf,
					ben.middleName as bm, 
					ben.lastName as bl,
					ben.Phone as bp,
					ben.Address as ba,
					ben.City as bc,
					ben.IDType,
					ben.IDNumber,
					ben.IDexpirydate,
					ben.IDissuedate,
					".(SYSTEM_PRE == "MIL"?"ben.SOF,ben.SOF_Type,":"")."
					cust.firstName,
					cust.middleName, 
					cust.lastName,
					cust.Phone,
					cust.Address,
					cust.City,
					cust.profession,
					cust.dob,
					cust.placeOfBirth, 
					cust.Country,
					cust.Zip
				from 
					transactions as tr, 
					beneficiary as ben, 
					customer as cust
				where
					tr.benID = ben.benID and
					tr.customerID = cust.customerID 
					
					". $filters ." 
				order by tr.transDate 
					";
	if(!empty($sql))
		$fullRS = SelectMultiRecords($sql);
	//debug($sql);
	//debug($fullRS);
}


if($_REQUEST["Submit"] == "Export")
{
	/* Export version is excel */
	
	$idList = explode(",",CONFIG_DISTRIBUTORS_LIST_FOR_OUTPUT);
	$disDollareast = $idList[0];
	$distFairdeal = $idList[1];
	
	header ("Content-type: application/x-msexcel"); 
	header ("Content-Description: Payex Generated Data" );
	
	$fileName = str_replace(array(" ", "."), array("_", "_"), $distName);

	if($_REQUEST["export"] == "old")
		header("Content-disposition: attachment; filename=DistributorOutput-".$fileName."-OldTransactions.xls");
	else
		header("Content-disposition: attachment; filename=DistributorOutput-".$fileName.".xls");
	
	$outputData = "<table border='1'>";
	
	if(SYSTEM_PRE == "MIL" && $distID == $disDollareast ){
		$outputData .= "<tr>
				<td><b>Date</b></td>
				<td><b>S.No.</b></td>
				<td><b>REF NO</b></td>
				<td><b>RT NO</b></td>
				<td><b>PARTICULARE</b></td>
				<td><b>Receiver Telephone</b></td>
				<td><b>SENDER NAME</b></td>
				<td><b>STATION</b></td>
				<td><b>F. AMT Rs.</b></td>
				<td><b>@</b></td>
				<td><b>AMT &pound;</b></td>
				<td><b>CHGS &pound;</b></td>
				<td><b>TOTAL AMT. RCVD &pound;</b></td>
			</tr>";
	}elseif(SYSTEM_PRE == "MIL" && $distID == $distFairdeal){
	 		
			$outputData .= "<tr>
				<td><b>Country</b></td>
				<td><b>Station</b></td>
				<td><b>Rate</b></td>
				<td><b>Amount</b></td>
				<td><b>Due</b></td>
				<td><b>P/M</b></td>
				<td><b>Remitter Name</b></td>
				<td><b>Remitter Address</b></td>
				<td><b>Remitter City</b></td>
				<td><b>Remitter Post Code</b></td>
				<td><b>Remitter Contact No</b></td>
				<td><b>Receiver Name</b></td>
				<td><b>Relation, A/C</b></td>
				<td><b>S/O,D/O,W/O, Bank Name</b></td>
				<td><b>ADDRESS, Bank Branch</b></td>
				<td><b>Receiver City</b></td>
				<td><b>Phone</b></td>
			</tr>";
				
	}else{
		$outputData .= "<tr>
				<td>Reference Code</td>
				<td>Date</td>
				<td>Manual Receipt Code</td>
				<td>Sender Name</td>
				<td>Occupation</td>
				<td>Date of Birth</td>
				<td>Place of birth</td>
				<td>Receiver Name</td>
				<td>Receiver Phone Number</td>
				<td>ID Type</td>
				<td>ID Number</td>
				<td>ID Expiry Date</td>
				<td>ID Issue Date</td>
				<td>Receiver Verification ID</td>
				<td>Foreign Amount</td>
				<td>Exchange Rate</td>
				<td>Local Amount</td>
				<td>Receiver Country</td>
				<td>Transaction Type</td>
				<td>Collection Point Address</td>
				<td>Collection Point City</td>
				<td>Bank Name</td>
				<td>Account No.</td>
				<td>Branch Code</td>
				<td>Branch Address</td>
				<td>Receiver City</td>
				<td>Receiver Address</td>
			</tr>";
	    }
	for($i=0; $i < count($fullRS); $i++)
	{
		
		/* Marking the transactions as exported, by assigning the dispatchDate column of transaction table to current data time entry*/
		if($_REQUEST["export"] != "old") /* if export transactions are new */
		{
			$strUpdateTransactionSql = "update transactions set dispatchDate = '".date("Y-m-d h:i:s")."' where transID= '".$fullRS[$i]["transID"]."'";
			update($strUpdateTransactionSql);
			//debug($strUpdateTransactionSql);
		}

		$strDateTime = strtotime($fullRS[$i]["transDate"]);
		$strDateTime = date("m/d/Y",$strDateTime);
		
		// Load Collection Point Information based on Transaction Type
		$collectionAddress = '';
		$collectionCity = '';
		$bankName = '';
		$account = '';
		$brCode = '';
		$brAddress = '';
		
		if($fullRS[$i]["transType"]=='Pick up')
		{
			$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state, cp_corresspondent_name, cp_branch_no, cp_ida_id from " . TBL_COLLECTION . " where cp_id='".$fullRS[$i]["collectionPointID"]."'");
			$collectionAddress = $collectionPoint["cp_branch_address"];
			$collectionCity = $collectionPoint["cp_city"];
			$correspondent = $collectionPoint["cp_corresspondent_name"];
			$locidnum = $collectionPoint["cp_branch_no"];
			$selectDistributorInfo = selectFrom("select userID, username, name  from " . TBL_ADMIN_USERS . " where userID = '".$collectionPoint["cp_ida_id"]."'");	
			$bankName = $selectDistributorInfo["name"]."[".$selectDistributorInfo["username"]."]";
		} elseif($fullRS[$i]["transType"]=='Bank Transfer'){
			$bankDetails = selectFrom("select bankName, accNo, branchAddress, branchCode from ".TBL_BANK_DETAILS." where transID = '".$fullRS[$i]["transID"]."'");
			$bankName = $bankDetails["bankName"];						
			$account = $bankDetails["accNo"];						
			$brCode = $bankDetails["branchCode"];						
			$brAddress = $bankDetails["branchAddress"];
		} elseif($fullRS[$i]["transType"]=='Home Delivery'){
			$selectDistributorInfo = selectFrom("select userID, username, name  from " . TBL_ADMIN_USERS . " where userID = '".$fullRS[$i]["benAgentID"]."'");	
			$bankName = $selectDistributorInfo["name"]."[".$selectDistributorInfo["username"]."]";					
		}	
/*
		if(SYSTEM_PRE == "MIL")
			$outputData .= "<tr>
					<td>".$strDateTime."</td>
					<td>".($i+1)."</td>
					<td>".$fullRS[$i]["refNumberIM"]."</td>
					<td>".$fullRS[$i]["refNumber"]."</td>
					<td>".$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]."<br /> S/O ".$fullRS[$i]["SOF"]."</td>
					<td>".($fullRS[$i]["transType"]=='Bank Transfer'?"BANK":$collectionCity)."</td>
					<td>".number_format($fullRS[$i]["localAmount"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["exchangeRate"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
					<td> - </td>
					<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
				</tr>";	
		else
			$outputData .= "<tr>
				<td>".$fullRS[$i]["refNumberIM"]."</td>
				<td>".$strDateTime."</td>
				<td>".$fullRS[$i]["refNumber"]."</td>
				<td>".$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]."</td>
				<td>".$fullRS[$i]["profession"]."</td>
				<td>".$fullRS[$i]["dob"]."</td>
				<td>".$fullRS[$i]["placeOfBirth"]."</td>
				<td>".$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]."</td>
				<td>".$fullRS[$i]["bp"]."</td>
				<td>".$fullRS[$i]["IDType"]."</td>
				<td>".$fullRS[$i]["IDNumber"]."</td>
				<td>".$fullRS[$i]["IDexpirydate"]."</td>
				<td>".$fullRS[$i]["IDissuedate"]."</td>
				<td>".$fullRS[$i]["tip"]."</td>
				<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
				<td>".number_format($fullRS[$i]["exchangeRate"],2,".","")."</td>
				<td>".number_format($fullRS[$i]["localAmount"],2,".","")."</td>
				<td>".$fullRS[$i]["toCountry"]."</td>
				<td>".$fullRS[$i]["transType"]."</td>
				<td>".$collectionAddress."</td>
				<td>".$collectionCity."</td>
				<td>".$bankName."</td>
				<td>".$account."</td>
				<td>".$brCode."</td>
				<td>".$brAddress."</td>
				<td>".$fullRS[$i]["bc"]."</td>
				<td>".$fullRS[$i]["ba"]."</td>
			</tr>";
*/
		if(SYSTEM_PRE == "MIL" && $distID == $disDollareast){
			$outputData .= "<tr>
					<td>".$strDateTime."</td>
					<td>".($i+1)."</td>
					<td>".$fullRS[$i]["refNumberIM"]."</td>
					<td>".$fullRS[$i]["refNumber"]."</td>
					<td>".$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"].($fullRS[$i]["SOF"]!="" ? " ".$fullRS[$i]["SOF_Type"]." ".$fullRS[$i]["SOF"]:"")." ".($fullRS[$i]["transType"]=='Bank Transfer'? $detailsDisplay :"")."</td>
					<td align='left'>".$fullRS[$i]["bp"]."</td>
					<td>".$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]."</td>
					<td>".($fullRS[$i]["transType"]=='Bank Transfer'?"BANK":$collectionCity)."</td>
					<td>".number_format($fullRS[$i]["localAmount"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["exchangeRate"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
					<td> - </td>
					<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
				</tr>";	
		}elseif(SYSTEM_PRE == "MIL" && $distID == $distFairdeal){
		      $outputData .= "<tr>
					<td>".$fullRS[$i]["Country"]."</td>
					<td>".$fullRS[$i]["City"]."</td>
					<td>".number_format($fullRS[$i]["exchangeRate"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["localAmount"],2,".","")."</td>
					<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
					<td>".$fullRS[$i]["transType"]."</td>
					<td>".$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]."</td>
					<td>".$fullRS[$i]["Address"]."</td>
					<td>".$fullRS[$i]["City"]."</td>
					<td>".$fullRS[$i]["Zip"]."</td>
					<td>".$fullRS[$i]["Phone"]."</td>
					<td>".$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]."</td>
					<td>".($fullRS[$i]["SOF"]!="" ? $fullRS[$i]["SOF_Type"].", " : "").($fullRS[$i]["transType"]=='Bank Transfer'?$account:"")."</td>
					<td>".($fullRS[$i]["SOF"]!="" ? $fullRS[$i]["SOF"].", " : "").($fullRS[$i]["transType"]=='Bank Transfer'?$bankName:"")."</td>
					<td>".$fullRS[$i]["ba"]." ".($fullRS[$i]["transType"]=='Bank Transfer'?$brAddress:"")."</td>
					<td>".$fullRS[$i]["bc"]."</td>
					<td>".$fullRS[$i]["bp"]."</td>
				</tr>";	
		}else{
			$outputData .= "<tr>
				<td>".$fullRS[$i]["refNumberIM"]."</td>
				<td>".$strDateTime."</td>
				<td>".$fullRS[$i]["refNumber"]."</td>
				<td>".$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]."</td>
				<td>".$fullRS[$i]["profession"]."</td>
				<td>".$fullRS[$i]["dob"]."</td>
				<td>".$fullRS[$i]["placeOfBirth"]."</td>
				<td>".$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]."</td>
				<td>".$fullRS[$i]["bp"]."</td>
				<td>".$fullRS[$i]["IDType"]."</td>
				<td>".$fullRS[$i]["IDNumber"]."</td>
				<td>".$fullRS[$i]["IDexpirydate"]."</td>
				<td>".$fullRS[$i]["IDissuedate"]."</td>
				<td>".$fullRS[$i]["tip"]."</td>
				<td>".number_format($fullRS[$i]["transAmount"],2,".","")."</td>
				<td>".number_format($fullRS[$i]["exchangeRate"],2,".","")."</td>
				<td>".number_format($fullRS[$i]["localAmount"],2,".","")."</td>
				<td>".$fullRS[$i]["toCountry"]."</td>
				<td>".$fullRS[$i]["transType"]."</td>
				<td>".$collectionAddress."</td>
				<td>".$collectionCity."</td>
				<td>".$bankName."</td>
				<td>".$account."</td>
				<td>".$brCode."</td>
				<td>".$brAddress."</td>
				<td>".$fullRS[$i]["bc"]."</td>
				<td>".$fullRS[$i]["ba"]."</td>
			</tr>";

		}	
	}	
	
	$outputData .= "</table>";
	
	echo $outputData;
	die();
}
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Distributor Output File (<?=$distName?> <? if($_REQUEST["export"] == "old") echo " - Old Transactions";?>)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 24px;
	font-weight: bold;
	height: 50px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 16px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->

</style>
<script>
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
</head>
<body>
  <form name="search" id="searchFrm" method="post" action="<?=$PHP_SELF?>">
	<table border="0" align="center" cellpadding="5" cellspacing="1" >
      <? 
			/**
			 * At the new transactions dont display the date search filter
			 * @Ticket #4222
			 */
			 $bolShowSearchFilter = false;
			 if($_REQUEST["export"] == "old")
			 	$bolShowSearchFilter = true;
			 else
			 {
			 	if(!defined("CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF"))
					$bolShowSearchFilter = true;
				elseif(CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF == "1")
					$bolShowSearchFilter = false;
			 }
			 
			//if(!defined("CONFIG_DONT_SHOW_DATE_FILTER_AT_DOF") && $_REQUEST["export"] == "old") 
			
		?>
	  
	  <tr>
      	<td align="center">
  			<b>SEARCH FILTER</b>    				
      	</td>
    	</tr>
      <tr>
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
		  <?
			if($bolShowSearchFilter)
			{ 
		  ?>
			<b>From </b>
			<? 
				$month = date("m");
				$day = date("d");
				$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
				for ($Day=1;$Day<32;$Day++)
				{
					if ($Day<10)
						$Day="0".$Day;
					echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
				}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.fDay, "<?=(!empty($_POST["fDay"])?$_POST["fDay"]:"")?>");
			</script>
			<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.fMonth, "<?=(!empty($_POST["fMonth"])?$_POST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
		    <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT> 
			<script language="JavaScript">
				SelectOption(document.search.fYear, "<?=(!empty($_POST["fYear"])?$_POST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			<?
				for ($Day=1;$Day<32;$Day++)
				{
					if ($Day<10)
						$Day="0".$Day;
					echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
				}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.search.tDay, "<?=(!empty($_POST["tDay"])?$_POST["tDay"]:"")?>");
			</script>
			<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.tMonth, "<?=(!empty($_POST["tMonth"])?$_POST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.search.tYear, "<?=(!empty($_POST["tYear"])?$_POST["tYear"]:"")?>");
			</script>
			<? } ?>
			
			<br /><br />
			<b>Transaction Status</b>&nbsp;
			<select name="transactionStatus">
				<option value="">All</option>
				<option value="Authorize" <?=($_REQUEST["transactionStatus"]=="Authorize"?'selected="selected"':"")?>>Authorize</option>
				<option value="Amended" <?=($_REQUEST["transactionStatus"]=="Amended"?'selected="selected"':"")?>>Amended</option>
			</select>
						
			<input type="hidden" name="export" value="<?=$_REQUEST["export"]?>" />
			<input type="hidden" name="distributors" value="<?=$_REQUEST["distributors"]?>"> <!-- Sky Bank user Id 1002640297 -->
			<!-- Testing userid GL9 / 10026402443 -->
			<br /><br />
 		    <input type="submit" name="Submit" value="search" onClick="document.getElementById('searchFrm').target='_self'" />
			</td>
		</tr>
	</table>

	
	<table width="90%" cellpadding="2" cellspacing="0" align="center">
		<!-- Report Header Starts -->
		<tr>
			<td colspan="27" class="reportHeader">
				Distributor Output File <?=$distName?> 
				<? if($_REQUEST["export"] == "old") echo " (Old Transactions)";?>
			</td>
		</tr>
		<!-- Report Header Ends -->
		<!-- Column Header Starts -->
		<tr>
			<td class="columnHeader">Reference Code</td>
			<td class="columnHeader">Date</td>
			<td class="columnHeader">Manual Receipt Code</td>
			<td class="columnHeader">Sender Name</td>
			<td class="columnHeader">Occupation</td>
			<td class="columnHeader">Date of Birth</td>
			<td class="columnHeader">Place of birth</td>
			<td class="columnHeader">Receiver Name</td>
			<td class="columnHeader">Receiver Phone Number</td>
			<td class="columnHeader">ID Type</td>
			<td class="columnHeader">ID Number</td>
			<td class="columnHeader">ID Expiry Date</td>
			<td class="columnHeader">ID Issue Date</td>
			<td class="columnHeader">Receiver Verification ID</td>
			<td class="columnHeader">Foreign Amount</td>
			<td class="columnHeader">Exchange Rate</td>
			<td class="columnHeader">Local Amount</td>
			<td class="columnHeader">Receiver Country</td>
			<td class="columnHeader">Transaction Type</td>
			<td class="columnHeader">Collection Point Address</td>
			<td class="columnHeader">Collection Point City</td>
			<td class="columnHeader">Bank Name</td>
			<td class="columnHeader">Account No.</td>
			<td class="columnHeader">Branch Code</td>
			<td class="columnHeader">Branch Address</td>
			<td class="columnHeader">Receiver City</td>
			<td class="columnHeader">Receiver Address</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?
			for($i=0; $i < count($fullRS); $i++)
			{
					$strDateTime = strtotime($fullRS[$i]["transDate"]);
					$strDateTime = date("m/d/Y",$strDateTime);
					
					// Load Collection Point Information based on Transaction Type
					$collectionAddress = '';
					$collectionCity = '';
					$bankName = '';
					$account = '';
					$brCode = '';
					$brAddress = '';
					
					if($fullRS[$i]["transType"]=='Pick up')
					{
						$collectionPoint = selectFrom("select cp_branch_address, cp_city, cp_state, cp_corresspondent_name, cp_branch_no, cp_ida_id from " . TBL_COLLECTION . " where cp_id='".$fullRS[$i]["collectionPointID"]."'");
						$collectionAddress = $collectionPoint["cp_branch_address"];
						$collectionCity = $collectionPoint["cp_city"];
						$correspondent = $collectionPoint["cp_corresspondent_name"];
						$locidnum = $collectionPoint["cp_branch_no"];
						$selectDistributorInfo = selectFrom("select userID, username, name  from " . TBL_ADMIN_USERS . " where userID = '".$collectionPoint["cp_ida_id"]."'");	
						$bankName = $selectDistributorInfo["name"]."[".$selectDistributorInfo["username"]."]";
					} elseif($fullRS[$i]["transType"]=='Bank Transfer'){
						$bankDetails = selectFrom("select bankName, accNo, branchAddress, branchCode from ".TBL_BANK_DETAILS." where transID = '".$fullRS[$i]["transID"]."'");
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
					} elseif($fullRS[$i]["transType"]=='Home Delivery'){
						$selectDistributorInfo = selectFrom("select userID, username, name  from " . TBL_ADMIN_USERS . " where userID = '".$fullRS[$i]["benAgentID"]."'");	
						$bankName = $selectDistributorInfo["name"]."[".$selectDistributorInfo["username"]."]";					
					}	
			?>
			<tr>
				<td class="transactions" align="center"><?=$fullRS[$i]["refNumberIM"]?></td>
				<td class="transactions" align="center"><?=$strDateTime?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["refNumber"]?></td>	<!-- transactino=>refNumber -->
				<td class="transactions" align="center"><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["profession"]?></td> <!-- customer=>profession -->
				<td class="transactions" align="center"><?=$fullRS[$i]["dob"]?></td> <!-- customer=>dob -->
				<td class="transactions" align="center"><?=$fullRS[$i]["placeOfBirth"]?></td> <!-- customer=>placeOfBirth -->
				<td class="transactions" align="center"><?=$fullRS[$i]["bf"]." ".$fullRS[$i]["bm"]." ".$fullRS[$i]["bl"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["bp"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["IDType"]?></td> <!-- beneficiary=>IDType -->
				<td class="transactions" align="center"><?=$fullRS[$i]["IDNumber"]?></td> <!-- beneficiary=>IDNumber -->
				<td class="transactions" align="center"><?=$fullRS[$i]["IDexpirydate"]?></td> <!-- beneficiary=>IDexpirydate -->
				<td class="transactions" align="center"><?=$fullRS[$i]["IDissuedate"]?></td> <!-- beneficiary=>IDissuedate -->
				<td class="transactions" align="center"><?=$fullRS[$i]["tip"]?></td> <!-- transaction=>tip -->
				<td class="transactions" align="center"><?=number_format($fullRS[$i]["transAmount"],2,".","")?></td> <!-- transaction=>transAmount -->
				<td class="transactions" align="center"><?=number_format($fullRS[$i]["exchangeRate"],2,".","")?></td> <!-- transaction=>exchangeRate -->
				<td class="transactions" align="center"><?=number_format($fullRS[$i]["localAmount"],2,".","")?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["toCountry"]?></td> <!-- transaction=>toCountry -->
				<td class="transactions" align="center"><?=$fullRS[$i]["transType"]?></td> <!-- transaction=>transType -->
				<td class="transactions" align="center"><?=$collectionAddress?></td> <!-- cm_collection_point=>cp_branch_address -->
				<td class="transactions" align="center"><?=$collectionCity?></td> <!-- cm_collection_point=>cp_city -->
				<td class="transactions" align="center"><?=$bankName?></td> <!-- bankDetails=>bankName -->
				<td class="transactions" align="center"><?=$account?></td> <!-- bankDetails=>accNo -->
				<td class="transactions" align="center"><?=$brCode?></td> <!-- bankDetails=>branchCode -->
				<td class="transactions" align="center"><?=$brAddress?></td> <!-- bankDetails=>branchAddress -->
				<td class="transactions" align="center"><?=$fullRS[$i]["bc"]?></td>
				<td class="transactions" align="center"><?=$fullRS[$i]["ba"]?></td>
			</tr>
			<!-- SubTotal Ends -->
			<!-- Transactions Data Ends -->
			
	<?
		}
/**
 * If no records to show
 */
 if(count($fullRS) < 1)
 {
?>
	<tr>
		<td class="transactions" align="center" colspan="27">No Records found!</td>
	</tr>
<?
	}
?>
</table>
	<table width="500" border="0" align="center" cellpadding="10" cellspacing="0">
		<tr>
			<td width="50%" align="center">
				<? if(count($fullRS) > 0) {  ?>
					<input type="submit" name="Submit" value="Export" onClick="document.getElementById('searchFrm').target='_blank'" /> 
				<? } ?>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>