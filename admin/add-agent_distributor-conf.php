<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
$agentType2 = getAgentType();
$parentID2 = $_SESSION["loggedUserData"]["userID"];

define("PIC_SIZE","51200");

/*	#5441 - AMB Exchange
	Operator ID is added in AnD form.
	to be displayed in Western Union Output export file.
	by Aslam Shahid
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1"){
	$operatorFlag = true;
}
/*	#4966 
	Company Type dropdown added on agents form and Trading field also added.
	by Aslam Shahid
*/
$companyTypeFlag = false;
$tradingNameFlag = false;
if(CONFIG_COMPANY_TYPE_DROPDOWN=="1"){
	$companyTypeFlag = true;
}
if(CONFIG_TRADING_COMPANY_NAME=="1"){
	$tradingNameFlag = true;
}
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;


if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}

if($_GET["searchBy"] != "")
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$CountryGet = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatusGet = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}
if ($_GET["type"] != ""){
	$typeV = $_GET["type"];
}
if ($_REQUEST["mode"] != ""){ 
    $mode = $_REQUEST["mode"]; 
}
switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_COUNTRY." where countryName='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}
$_SESSION["IDAcountry"] = (is_array($_POST["IDAcountry"]) ? implode(",", $_POST["IDAcountry"]) :"");
$_SESSION["authorizedFor"] = (is_array($_POST["authorizedFor"]) ? implode(",", $_POST["authorizedFor"]) :"");
$_SESSION["currencyOrigin"] = (is_array($_POST["currencyOrigin"]) ? implode(",", $_POST["currencyOrigin"]) :"");



/*
if($_POST["supAgentID"] != '' )
	$supAgentID = $_SESSION["supAgentID"];
else*/
	$supAgentID = $_POST["supAgentID"];

/*echo $supAgentID;
echo "<br>";

exit();*/
//echo  implode(",", $_POST["IDAcountry"]);
//echo implode(" ", $arr );
//exit;

	session_register("manualAgentNum");
	session_register("agentCompany");
	session_register("agentContactPerson");
	session_register("agentAddress");
	session_register("agentAddress2");
	session_register("postcode");
	session_register("agentCountry");
	session_register("agentCity");
	session_register("agentZip");
	session_register("agentPhone");
	session_register("agentFax");
	session_register("email");
	session_register("agentURL");
	session_register("agentMSBNumber");
	session_register("msbMonth");
	session_register("msbDay");
	session_register("msbYear");
	session_register("agentCompRegNumber");
	session_register("agentCompDirector");
	session_register("agentDirectorAdd");
	session_register("agentProofID");
	session_register("otherProofID");
	session_register("idMonth");
	session_register("idDay");
	session_register("idYear");
	session_register("agentDocumentProvided");
	session_register("agentBank");
	session_register("agentAccountName");
	session_register("agentAccounNumber");
	session_register("agentBranchCode");
	session_register("agentAccountType");
	session_register("agentCurrency");
	session_register("agentAccountLimit");
	session_register("agentCommission");
	session_register("agentStatus");
	session_register("parentID");
	session_register("companyType");
	session_register("tradingName");
if($operatorFlag)
	session_register("OperatorID");

	$_SESSION["manualAgentNum"] = $_POST["manualAgentNum"];
	$_SESSION["agentCompany"] = $_POST["agentCompany"];
	$_SESSION["agentContactPerson"] = $_POST["agentContactPerson"];
	$_SESSION["agentAddress"] = $_POST["agentAddress"];
	$_SESSION["agentAddress2"] = $_POST["agentAddress2"];
	$_SESSION["postcode"] = $_POST["postcode"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["agentZip"] = $_POST["postcode"];
	$_SESSION["agentPhone"] = $_POST["agentPhone"];
	$_SESSION["agentFax"] = $_POST["agentFax"];
	$_SESSION["email"] = $_POST["email"];
	$_SESSION["agentURL"] = $_POST["agentURL"];
	$_SESSION["agentMSBNumber"] = $_POST["agentMSBNumber"];
	$_SESSION["msbMonth"] = $_POST["msbMonth"];
	$_SESSION["msbDay"] = $_POST["msbDay"];
	$_SESSION["msbYear"] = $_POST["msbYear"];
	$_SESSION["agentCompRegNumber"] = $_POST["agentCompRegNumber"];
	$_SESSION["agentCompDirector"] = $_POST["agentCompDirector"];
	$_SESSION["agentDirectorAdd"] = $_POST["agentDirectorAdd"];
	$_SESSION["agentProofID"] = $_POST["agentProofID"];
	$_SESSION["otherProofID"] = $_POST["otherProofID"];
	$_SESSION["idMonth"] = $_POST["idMonth"];
	$_SESSION["idDay"] = $_POST["idDay"];
	$_SESSION["idYear"] = $_POST["idYear"];
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");
	$_SESSION["agentBank"] = $_POST["agentBank"];
	$_SESSION["agentAccountName"] = $_POST["agentAccountName"];
	$_SESSION["agentAccounNumber"] = $_POST["agentAccounNumber"];
	$_SESSION["agentBranchCode"] = $_POST["agentBranchCode"];
	$_SESSION["agentAccountType"] = $_POST["agentAccountType"];
	$_SESSION["agentCurrency"] = $_POST["agentCurrency"];
	$_SESSION["agentAccountLimit"] = $_POST["agentAccountLimit"];
	$_SESSION["agentCommission"] = $_POST["agentCommission"];
	$_SESSION["agentStatus"] = $_POST["agentStatus"];
	$_SESSION["isCorrespondent"] = $_POST["correspondent"];
	$_SESSION["parentID"] = $_POST["supAgentID"];  
	
	$_SESSION["CommissionAnD"] = $_POST["CommissionAnD"];    
	$_SESSION["commPackageAnD"] = $_POST["commPackageAnD"];              
	$_SESSION["agentPaymentMode"] = $_POST["agentPaymentMode"];    
	
	$_SESSION["settlementCurrencyAsAgent"] = $_REQUEST["settlementCurrencyAsAgent"];
	$_SESSION["settlementCurrencyAsDistributor"] = $_REQUEST["settlementCurrencyAsDistributor"];
	$_SESSION["companyType"] = $_REQUEST["companyType"];
	$_SESSION["tradingName"] = $_REQUEST["tradingName"];
	if($operatorFlag)
		$_SESSION["OperatorID"] = $_REQUEST["OperatorID"];
		
	if ($_SESSION["parentID"]==""){		
	if($agentType2 == 'Branch Manager' || $agentType2 == 'admin' || $agentType2 == 'Admin Manager' )
	{		
		$_SESSION["parentID"] = $parentID2;
	}}
	

	
	////$_GET["ida"] == "Y"
	
	//$_SESSION["IDAcountry"] = implode(",", $_POST["IDAcountry"]);
$uploadImageVar = $_POST["uploadImage"];
if ($_POST["userID"] == ""){
	
		$backURL = "add-agent_Distributor.php?msg=Y&type=$typeV&ida=$ida&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country$Country&agentStatus=$agentStatus";
		
} else {
	
		$backURL = "update-agent_Distributor.php?userID=$_POST[userID]&msg=Y&ida=$ida&type=$typeV&userID=$userID&agentCode=$agentCode&searchBy=$searchBy&Country=$CountryGet&agentStatus=$agentStatusGet";
	
}
if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") { 
	if (trim($_POST["manualAgentNum"]) == "") { 
		insertError(AG42) ; 
		redirect($backURL) ;     
	} else { 
		if ($_POST["userID"] == "") { 
			$qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE lower(username) = '".strtolower(trim($_POST["manualAgentNum"]))."'" ; 
			$qUserRes = selectFrom($qUserName) ; 
			if ($qUserRes["username"] != "") { 
				insertError(AG43) ; 
				redirect($backURL) ;     
			} 
		} else { 
			$qUserName = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE `userID` = '".$_POST["userID"]."'" ;      
			$qUserRes = selectFrom($qUserName) ; 
			if ($qUserRes["username"] != trim($_POST["manualAgentNum"])) { 
				$qUserName2 = "SELECT `username` FROM ".TBL_ADMIN_USERS." WHERE lower(username) = '".strtolower(trim($_POST["manualAgentNum"]))."'" ; 
				$qUserRes2 = selectFrom($qUserName2) ; 
				if ($qUserRes2["username"] != "") { 
					insertError(AG43) ; 
					redirect($backURL) ;     
				} 
			} 
		} 
	} 
}

if (trim($_POST["agentCompany"]) == ""){
	insertError(AG1);
	redirect($backURL);
}
if (trim($_POST["agentContactPerson"]) == ""){
	insertError(AG2);
	redirect($backURL);
}
if (trim($_POST["agentAddress"]) == ""){
	insertError(AG3);
	redirect($backURL);
}
if (trim($_POST["Country"]) == ""){
	insertError(AG4);
	redirect($backURL);
}
/*
if (trim($_POST["City"]) == ""){
	insertError(AG5);
	redirect($backURL);
}
if (trim($_POST["agentZip"]) == ""){
	insertError(AG6);
	redirect($backURL);
}*/
if (trim($_POST["agentPhone"]) == ""){
	insertError(AG7);
	redirect($backURL);
}/*
if(!email_check(trim($_POST["email"]))){
	insertError(AG8);
	redirect($backURL);
}*/
/*if (trim($_POST["agentCompDirector"]) == ""){
	insertError(AG9);
	redirect($backURL);
}
if (trim($_POST["agentBank"]) == ""){
	insertError(AG10);
	redirect($backURL);
}
if (trim($_POST["agentAccountName"]) == ""){
	insertError(AG11);
	redirect($backURL);
}
if (trim($_POST["agentAccounNumber"]) == ""){
	insertError(AG12);
	redirect($backURL);
}
if (trim($_POST["agentCurrency"]) == ""){
	insertError(AG13);
	redirect($backURL);
}
if (trim($_POST["agentAccountLimit"]) == ""){
	insertError(AG14);
	redirect($backURL);
}
if (trim($_POST["agentCommission"]) == ""){
	insertError(AG15);
	redirect($backURL);
}*/
if ($logo_name != "")
{
	if ($logo_size>PIC_SIZE)
	{
		insertError("Image size must be less than 50K");
		redirect($backURL);
	}
	if (extension($logo_name)!=1)
	{
		insertError("Invalid file format. Only gif, jpg and png are allowed");
		redirect($backURL);
	}
}

if ($_POST["agent_type"]== "")
$agent_type = 'Supper';
else
$agent_type = $_POST["agent_type"];

/* Settlement curriencies of A&D */
$strAgentDistributorSettlementCurrencies = "";
if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1"){
	$strAgentDistributorSettlementCurrencies = $_REQUEST["settlementCurrencyAsAgent"]."|".$_REQUEST["settlementCurrencyAsDistributor"];	
}
if ($_POST["userID"] == ""){


	$password = createCode();


		$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {
			$agentCode = trim($_POST["manualAgentNum"]) ; 
			if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
			if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
				insertError(AG43);
				redirect($backURL);
			}
		} else {
			$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
		}
		$strQuery = "select max(userID) as maxid from admin";
		$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
		$rstRow = mysql_fetch_array($nResult);
 		$supAgentID = ($rstRow['maxid']+1);
		$Querry_Sqls = "INSERT INTO ".TBL_ADMIN_USERS." (username, password, changedPwd, name, email, created, parentID, 
		agentNumber, agentCompany, agentContactPerson, agentAddress, agentAddress2, agentCity, agentZip, agentCountry, 
		agentCountryCode, agentPhone, agentFax, agentURL, agentMSBNumber, agentMCBExpiry, agentCompRegNumber, agentCompDirector,
		 agentDirectorAdd, agentProofID, agentIDExpiry, agentDocumentProvided, agentBank, agentAccountName, agentAccounNumber,
		  agentBranchCode, agentAccountType, agentCurrency, agentAccountLimit, commPackage ,agentCommission, agentStatus, 
		  isCorrespondent, IDAcountry, agentType, accessFromIP, authorizedFor, postCode, commPackageAnDDist, commAnDDist, defaultDistrib,paymentMode, settlementCurrencies) VALUES 
		('$agentCode', '$password', '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["email"])."',
		 '".getCountryTime(CONFIG_COUNTRY_CODE)."', '".$_SESSION["parentID"]."', '".($agentNumber[0]+1)."', '".checkValues($_POST["agentCompany"])."',
		 '".checkValues($_POST["agentContactPerson"])."', '".checkValues($_POST["agentAddress"])."', 
		 '".checkValues($_POST["agentAddress2"])."', '".$_POST["City"]."', '".$_POST["agentZip"]."', '".$_POST["Country"]."', 
		 '$ccode', '".checkValues($_POST["agentPhone"])."', '".checkValues($_POST["agentFax"])."', 
		 '".checkValues($_POST["agentURL"])."', '".checkValues($_POST["agentMSBNumber"])."',
	   '".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', '".checkValues($_POST["agentCompRegNumber"])."', 
	   '".checkValues($_POST["agentCompDirector"])."', '".checkValues($_POST["agentDirectorAdd"])."', 
	   '".checkValues($_POST["agentProofID"])."', '".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', 
	   '".$_SESSION["agentDocumentProvided"]."', '".checkValues($_POST["agentBank"])."', 
		 '".checkValues($_POST["agentAccountName"])."', '".checkValues($_POST["agentAccounNumber"])."', 
		 '".checkValues($_POST["agentBranchCode"])."', '".$_POST["agentAccountType"]."', '".$_POST["agentCurrency"]."', 
		 '".checkValues($_POST["agentAccountLimit"])."', '".checkValues($_POST["commPackage"])."', 
		 '".checkValues($_POST["agentCommission"])."', '".$_POST["agentStatus"]."', '".$_POST["correspondent"]."', 
		 '".$_SESSION["IDAcountry"]."',  '$agent_type', '".checkValues($_POST["accessFromIP"])."', '".checkValues($_SESSION["authorizedFor"])."',
		 '".$_POST["postcode"]."','".$_POST["commPackageAnD"]."','".$_POST["CommissionAnD"]."', '".$_POST["defaultDistrib"]."',
		 '".$_POST["agentPaymentMode"]."', '".$strAgentDistributorSettlementCurrencies."')";

	
	insertInto($Querry_Sqls);
	$insertedID = @mysql_insert_id();
	
	$arrLastInsertId = selectFrom("select LAST_INSERT_ID() as li");
	$insertedID = $arrLastInsertId["li"];
	$userID = $insertedID;
	$_SESSION["supAgentID"] = '';
	// PICTURE UPLOAD CODE
	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict = strtolower($agentCode.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($agentCode . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='$agentCode'");
		}
	}
	if(!empty($agentCode) && isset($_POST["companyType"]) && $companyTypeFlag){
			update("update " . TBL_ADMIN_USERS. " set companyType='".$_POST["companyType"]."' where username='".$agentCode."'");
	}
	if(!empty($agentCode) && isset($_POST["tradingName"]) && $tradingNameFlag){
			update("update " . TBL_ADMIN_USERS. " set tradingName='".$_POST["tradingName"]."' where username='".$agentCode."'");
	}
	if(isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where username='".$insertedID."'");
	}	
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "A&D Account creation at $company";
	$message = "Congratulations! ".$_POST["name"].",\n\n
		Your A&D account has been created at $company agent module. Your login information is as follows:\n\n
		Login: ".$agentCode."\n\n
		Password: ".$password."\n\n
		Name: ".$_POST["agentContactPerson"]."\n\n
		To access your account please <a href=\"".ADMIN_ACCOUNT_URL."\">click here</a>\n\n

		$company Support";
	sendMail($_POST["email"], $subject, $message, $fromName, $fromEmail);
	////To record in History
	$descript = "A&D is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_ADMIN_USERS,$descript);

	$_SESSION["manualAgentNum"] = "";
	$_SESSION["agentCompany"] = "";
	$_SESSION["agentContactPerson"] = "";
	$_SESSION["agentAddress"] = "";
	$_SESSION["agentAddress2"] = "";
	$_SESSION["postcode"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["agentZip"] = "";
	$_SESSION["agentPhone"] = "";
	$_SESSION["agentFax"] = "";
	$_SESSION["email"] = "";
	$_SESSION["agentURL"] = "";
	$_SESSION["agentMSBNumber"] = "";
	$_SESSION["msbMonth"] = "";
	$_SESSION["msbDay"] = "";
	$_SESSION["msbYear"] = "";
	$_SESSION["agentCompRegNumber"] = "";
	$_SESSION["agentCompDirector"] = "";
	$_SESSION["agentDirectorAdd"] = "";
	$_SESSION["agentProofID"] = "";
	$_SESSION["otherProofID"] = "";
	$_SESSION["idMonth"] = "";
	$_SESSION["idDay"] = "";
	$_SESSION["idYear"] = "";
	$_SESSION["agentDocumentProvided"] = "";
	$_SESSION["agentBank"] = "";
	$_SESSION["agentAccountName"] = "";
	$_SESSION["agentAccounNumber"] = "";
	$_SESSION["agentBranchCode"] = "";
	$_SESSION["agentAccountType"] = "";
	$_SESSION["agentCurrency"] = "";
	$_SESSION["agentAccountLimit"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["authorizedFor"] = "";
	$_SESSION["agentCommission"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["CommissionAnD"] = "";
	$_SESSION["commPackageAnD"] = "";
	$_SESSION["agentPaymentMode"] = "";
	$_SESSION["settlementCurrencyAsAgent"] = "";
	$_SESSION["settlementCurrencyAsDistributor"] = "";
	$_SESSION["companyType"] = "";
	$_SESSION["tradingName"] = "";
	if($operatorFlag)
		$_SESSION["OperatorID"] = "";
	
	if($_POST["supAagentID"] != "")
	{
		
			insertError(eregi_replace("Supper",'Sub', AG16). " The login name for the A&D is <b>$agentCode</b> and password is <b>$password</b>");
		
	}
	else
	{
		
			insertError(AG16 . " The login name for the A&D is <b>$agentCode</b> and password is <b>$password</b>");
		
	}

		//insertError( ($_POST["correspondent"] == "ONLY" ? eregi_replace("Agent",'IDA', AG16) : AG16). " The login name for the agent is <b>$agentCode</b> and password is <b>$password</b>");
} else {
	if (trim($_POST["oldCountry"]) != trim($_POST["Country"])){
		$agentNumber = selectFrom("select MAX(agentNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		
		if (CONFIG_MANUAL_SUPER_AnD_NUMBER == "1") {
			if (strtolower(trim($_POST["manualAgentNum"])) != strtolower(trim($_POST["manualAgentNum2"]))) {
				if (isExist("SELECT `username` FROM " . TBL_ADMIN_USERS . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
					insertError(AG43);
					redirect($backURL);
				}
				if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE lower(username) = '".strtolower($agentCode)."'")) {
					insertError(AG43);
					redirect($backURL);
				}
			}
		}
		$agentCode = "".$systemPre."".$ccode.($agentNumber[0]+1);///Changed Kashi 11_Nov
		$agentAcountUpdate = "username='$agentCode', agentNumber='".($agentNumber[0]+1)."', ";
	}
	$_SESSION["agentDocumentProvided"] = (is_array($_POST["agentDocumentProvided"]) ? implode(",",$_POST["agentDocumentProvided"]) : "");

	$Querry_Sqls = "update ".TBL_ADMIN_USERS." set $agentAcountUpdate 
	 name='".$_POST["agentContactPerson"]."', 
	 email='".checkValues($_POST["email"])."', 
	 parentID =  '".$_SESSION["parentID"]."',
	 agentCompany='".checkValues($_POST["agentCompany"])."', 
	 agentContactPerson='".checkValues($_POST["agentContactPerson"])."', 
	 agentAddress='".checkValues($_POST["agentAddress"])."', 
	 agentAddress2='".checkValues($_POST["agentAddress2"])."',	 
	 agentCity='".$_POST["City"]."', 
	 agentZip='".$_POST["agentZip"]."', 
	 agentCountry='".$_POST["Country"]."', 
	 agentCountryCode='$ccode', 
	 agentPhone='".checkValues($_POST["agentPhone"])."', 
	 agentFax='".checkValues($_POST["agentFax"])."', 
	 agentURL='".checkValues($_POST["agentURL"])."', 
	 agentMSBNumber='".checkValues($_POST["agentMSBNumber"])."', 
	 agentMCBExpiry='".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
	 agentCompRegNumber='".checkValues($_POST["agentCompRegNumber"])."', 
	 agentCompDirector='".checkValues($_POST["agentCompDirector"])."', 
	 agentDirectorAdd='".checkValues($_POST["agentDirectorAdd"])."', 
	 agentProofID='".checkValues($_POST["agentProofID"])."', 
	 agentIDExpiry='".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', 
	 agentDocumentProvided='".$_SESSION["agentDocumentProvided"]."', 
	 agentBank='".checkValues($_POST["agentBank"])."', 
	 agentAccountName='".checkValues($_POST["agentAccountName"])."', 
	 agentAccounNumber='".checkValues($_POST["agentAccounNumber"])."', 
	 agentBranchCode='".checkValues($_POST["agentBranchCode"])."', 
	 agentAccountType='".$_POST["agentAccountType"]."', 
	 agentCurrency='".$_POST["agentCurrency"]."', 
	 agentAccountLimit='".checkValues($_POST["agentAccountLimit"])."', 
	 commPackage = '".$_POST["commPackage"]."',
	 agentCommission='".checkValues($_POST["agentCommission"])."', 
	 agentStatus='".$_POST["agentStatus"]."', 
	 isCorrespondent = '".$_POST["correspondent"]."',
	 IDAcountry = '".$_SESSION["IDAcountry"]."',
	 accessFromIP = '".checkValues($_POST["accessFromIP"])."',
	 authorizedFor = '".checkValues($_SESSION["authorizedFor"])."',
	 postCode='".checkValues($_POST["postcode"])."',
	 commPackageAnDDist = '".$_POST["commPackageAnD"]."', 
	 commAnDDist = '".$_POST["CommissionAnD"]."',
	 defaultDistrib = '".$_POST["defaultDistrib"]."',
	 paymentMode = '".$_POST["agentPaymentMode"]."',
	 settlementCurrencies = '".$strAgentDistributorSettlementCurrencies."'
	 where userID='".$_POST[userID]."'";

	if ($logo_name != "")
	{
		$Ext = strrchr($logo_name,".");
		if (is_uploaded_file($logo))
		{
			$Pict=strtolower($username.$Ext);
			move_uploaded_file($logo, "../logos/" . strtolower($_POST["usern"] . $Ext) );
			update("update " . TBL_ADMIN_USERS. " set logo='$Pict' where username='".$_POST["usern"]."'");
		}
	}

	update($Querry_Sqls);
	if(!empty($_POST["usern"]) && isset($_POST["companyType"]) && $companyTypeFlag){
			update("update " . TBL_ADMIN_USERS. " set companyType='".$_POST["companyType"]."' where username='".$_POST["usern"]."'");
	}
	if(!empty($_POST["usern"]) && isset($_POST["tradingName"]) && $tradingNameFlag){
			update("update " . TBL_ADMIN_USERS. " set tradingName='".$_POST["tradingName"]."' where username='".$_POST["usern"]."'");
	}
	if(!empty($_POST["userID"]) && isset($_POST["OperatorID"]) && $operatorFlag){
			update("update " . TBL_ADMIN_USERS. " set OperatorID='".$_POST["OperatorID"]."' where userID='".$_POST["userID"]."'");
	}	
		////To record in History
	$descript ="A&D is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["userID"],TBL_ADMIN_USERS,$descript);
	
	//echo("\n\n Query is updated");
	insertError(AG17);
}
$_SESSION["Country"] = "";
 	// #4996 point 2 upload document functionality for AnD forms
if(CONFIG_UPLOAD_DOCUMNET_ADD_SUPPER_AGENT  == "1"){ 
    if($uploadImageVar == "Y"){ 
	   $backURL = "uploadCustomerImage.php?type=".$typeV."&from=add_supper_agent&mode=".$mode."&pageID=add-agent_dist&ida=".$ida.""; 
		if(!empty($insertedID)){ 
			$backURL .="&customerID=".$insertedID;
		}else{ 
			$backURL .="&customerID=".$_REQUEST["userID"]; 
		} 
	} 
}
redirect($backURL);
?>