<?php

session_start();
include ("../include/config.php");
include ("security.php");
include ("double-entry-functions.php");
include ("lib/audit-functions.php");

$agentType = getAgentType();

$userID  = $_SESSION["loggedUserData"]["userID"];
$userName  = $_SESSION["loggedUserData"]["userName"];
$agentID = $_SESSION["loggedUserData"]["userID"];
$payinAmount = CONFIG_PAYIN_FEE;
$user_id_agent_account='';
if($agentType=='SUPA' || $agentType=='SUBA'   )
{
	$user_id_agent_account=$agentID;
}else
{

	$user_id_agent_account=$_REQUEST['userId'];
}


if(defined("CONFIG_TRANS_TYPE_TT") && CONFIG_TRANS_TYPE_TT==1)
{
	$ttTransFlag=true;
}

$id=$_REQUEST["id"];

if($_POST['action'] == 'checkaccnumber'){
	$accNumber = $_POST['accNumber'];
	$currency = $_POST['currency'];
	$returnFlag = "true";
	if(!empty($accNumber)){
		$strDuplicate = "select id,accounNumber from ".TBL_ACCOUNTS." WHERE accounNumber='".$accNumber."'";
		$rsDuplicate= selectFrom($strDuplicate);
		if (!empty($rsDuplicate["id"])){
			$returnFlag = "false";
		}
	}
	echo $returnFlag;
	exit;
}

$arguments = array("flag"=>"ibanAccountAddUpdate","IBAN"=>$_REQUEST["IBAN"],"fieldsChecked"=>$_REQUEST["fieldsChecked"]);
$ibanField = gateway("CONFIG_IBAN_TRADING_ACCOUNT",$arguments,CONFIG_IBAN_TRADING_ACCOUNT);
if(is_array($ibanField)){
	$insertKeys = $ibanField["keys"];
	$insertValues = $ibanField["values"];
	$updateKeyValues = $ibanField["keysValues"];
}
$arguments = array("flag"=>"dealFieldsAddUpdate","showOnDeal"=>$_REQUEST["showOnDeal"]);
$dealField = gateway("CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT",$arguments,CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT);
if(is_array($dealField)){
	$insertKeys .= $dealField["keys"];
	$insertValues .= $dealField["values"];
	$updateKeyValues .= $dealField["keysValues"];
	$showOnDealMsg = $dealField["msg"];
}

$company = COMPANY_NAME;
$created_date = date("Y-m-d h:i:s A");
$modifiedAt = date("Y-m-d h:i:s A");

if(!empty($_REQUEST["saveInputs"]))
{
	if(empty($_REQUEST["id"]))
	{

		$sql= "select id,accounNumber from ".TBL_ACCOUNTS." where accounNumber='".$_REQUEST["accountNUmber"]."' AND currency='".$_REQUEST["currency"]."'";
		$result= selectFrom($sql);
		if (!empty($result["id"]))
		{
			$msg = "Account Number already exist.<br/>";
		}
		else
		{

			$strInsertSql = "insert into ".TBL_ACCOUNTS."  
											(
											  accounType,
											  name,
											  bankName,
											  bankAddress,
											  branchName,
											  branchAddress,
											  country,
											  city,
											  accountName,
											  balance,				
											  phone,
											  sortCode,	
											  swiftCode,	
											  mobile,
											  fax,
											  userId,									  
											  currency,
											  accounNumber,
											  transFlag,
											  depositFlag,
											  created,
											  createdBy,
											 
											  balanceType
											  ".$insertKeys." 
											)
									 values
										   (
											 '".$_REQUEST["accountChart"]."',
											 '".$_REQUEST["contactPerson"]."',
											 '".$_REQUEST["bankName"]."',
											 '".$_REQUEST["bankAddress"]."',
											 '".$_REQUEST["branchName"]."',
											 '".$_REQUEST["branchAddress"]."',
											 '".$_REQUEST["country"]."',
											 '".$_REQUEST["city"]."',
											 '".$_REQUEST["accountName"]."',
											 '".$_REQUEST["balance"]."',
											 '".$_REQUEST["phone"]."',
											 '".$_REQUEST["sortCode"]."',
											 '".$_REQUEST["swiftCode"]."',
											 '".$_REQUEST["mobile"]."',
											 '".$_REQUEST["fax"]."',
											 '".$user_id_agent_account."',
								 			 '".$_REQUEST["currency"]."',											
											 '".$_REQUEST["accountNUmber"]."',	
											 '".$_REQUEST["transFlag"]."',	
											 '".$_REQUEST["depositFlag"]."',			 
											 '".$created_date."',
											 '".$userName."',
											 
											 '".$_REQUEST["balanceType"]."'
											 ".$insertValues."
											)";
			mysql_query($strInsertSql);
			$id = mysql_insert_id();

			if($ttTransFlag && !empty($id))
			{
				$CustId=$_POST["cusdi"];

				$updateQuery = " update ".TBL_ACCOUNTS." set CustID = '".$CustId."' where id = '".$id."'";
				update($updateQuery);

			}
			$msg = "Record is inserted successfully.<br/>";
			$_arrInput = array(
				"accountNumber" => $_REQUEST["accountNUmber"],
				"currency" => $_REQUEST["currency"],
				"amount" => $_REQUEST["balance"],
				"type" => $_REQUEST["balanceType"],
				"userID" => $user_id_agent_account,
				"currency" => $_REQUEST["currency"],
				"description" => '',
				"createdBy" => $userName,
				"created" => $created_date,
				"tableName" => ''
			);
		}
	}
	else
	{

		/**
		 * Else the record is not new and required the updation
		 */

		$querPreValues = "select * from ".TBL_ACCOUNTS."  where id = '".$id."'";
		$getPreValues  = mysql_query($querPreValues);
		$fetchValues   = mysql_fetch_array($getPreValues);
		$querLogin = "SELECT history_id FROM `login_history` WHERE `user_id` = '".$userID."' order by `history_id` DESC limit 1";
		$logIn = selectFrom($querLogin);
		// print_r($logIn["history_id"]);

		activities($logIn["history_id"],"UPDATION",$fetchValues["id"],"accounts","accounts table logs are maintained in audit_modify_history ");

		$strUpdateSql = "update ".TBL_ACCOUNTS."  
							 set
							 	name = '".$_REQUEST["contactPerson"]."',
								bankName = '".$_REQUEST["bankName"]."',
								bankAddress = '".$_REQUEST["bankAddress"]."',
								branchName = '".$_REQUEST["branchName"]."',
								branchAddress = '".$_REQUEST["branchAddress"]."',
								country = '".$_REQUEST["country"]."',
								city = '".$_REQUEST["city"]."',
								accountName = '".$_REQUEST["accountName"]."',
								balance = '".$_REQUEST["balance"]."',
								phone = '".$_REQUEST["phone"]."',
								sortCode = '".$_REQUEST["sortCode"]."',
								swiftCode = '".$_REQUEST["swiftCode"]."',
								mobile = '".$_REQUEST["mobile"]."',
								fax = '".$_REQUEST["fax"]."',
								accounType = '".$_REQUEST["accountChart"]."',
								currency = '".$_REQUEST["currency"]."',
								accounNumber = '".$_REQUEST["accountNUmber"]."',
								transFlag = '".$_REQUEST["transFlag"]."',
								depositFlag = '".$_REQUEST["depositFlag"]."',
								userId = '".$user_id_agent_account."',
								balanceType = '".$_REQUEST["balanceType"]."',
								
								created='".$created_date."'	
								".$updateKeyValues."";

		if($ttTransFlag && !empty($id))
			$strUpdateSql .= " ,CustID = '".$_REQUEST["cusdi"]."'";

		$strUpdateSql .=" where	id = '".$id."'";

		$updated_array = mysql_query($strUpdateSql);

		/// print_r($fetchValues);

		logChangeSet($fetchValues,$userID,'accounts','id',$id,$logIn["history_id"]);
		//print_r('yes everything ok');
		$msg = "Record is updated successfully.<br/>";

	}
}
$msg .= $showOnDealMsg;

$arguments = array("flag"=>"dealFieldsUpdateRest","showOnDeal"=>$_REQUEST["showOnDeal"],"id"=>$id,"currency"=>$_REQUEST["currency"]);
$dealUpdateRestField = gateway("CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT",$arguments,CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT);
if(is_array($dealUpdateRestField)){
	$insertKeys .= $ibanField["keys"];
	$insertValues .= $ibanField["values"];
	$updateKeyValues .= $ibanField["keysValues"];
}
/**
 * If the record is for editing
 * For this check if the company_id field is empty or not, if not than request will be for editing the record
 */
$account_id	=	$_REQUEST["id"];
$strMainCaption = "Add New Account";
if(!empty($_REQUEST["id"]))
{

	$strGetDataSql = "SELECT 
							  name,
							  bankName,
							  bankAddress,
							  branchName,
							  branchAddress,
							  country,
							  userId,
							  balance,
							  city,							 
							  phone,
							  mobile,
							  fax,
							  accounType,
							  currency,							  
							  accounNumber,
							  created,
							  accountName,
							  sortCode,
							  createdBy,   
							  updated, 
							  status,		
						      transNote ,
							  fieldsChecked,
							  showOnDeal,
							  swiftCode,
							  transFlag,
							  balanceType,
							  depositFlag
							  ".$insertKeys."";




	if($ttTransFlag)
		$strGetDataSql .= " ,CustID";

	$strGetDataSql .=" FROM 
									 ".TBL_ACCOUNTS."   
								  WHERE 
									id = '".$id."'";

	$arrCompanyData = selectFrom($strGetDataSql);
	$id = $id;
	$contactPerson = $arrCompanyData["name"];
	$bankName = $arrCompanyData["bankName"];
	$IBAN = $arrCompanyData["IBAN"];
	$fieldsCheckedStr = $arrCompanyData["fieldsChecked"];
	$showOnDeal = $arrCompanyData["showOnDeal"];
	$bankAddress = $arrCompanyData["bankAddress"];
	$branchName = $arrCompanyData["branchName"];
	$branchAddress = $arrCompanyData["branchAddress"];
	$country = $arrCompanyData["country"];
	$city = $arrCompanyData["city"];
	$fax = $arrCompanyData["fax"];
	$userId = $arrCompanyData["userId"];
	$balance = $arrCompanyData["balance"];
	$accountChart = $arrCompanyData["accounType"];
	$phone = $arrCompanyData["phone"];
	$mobile = $arrCompanyData["mobile"];
	$currency = $arrCompanyData["currency"];
	$accountNUmber = $arrCompanyData["accounNumber"];
	$accountName = $arrCompanyData["accountName"];
	$sortCode = $arrCompanyData["sortCode"];
	$swiftCode = $arrCompanyData["swiftCode"];
	$transFlag = $arrCompanyData["transFlag"];
	$depositFlag = $arrCompanyData["depositFlag"];
	$accounType = $arrCompanyData["accounType"];
	$balanceType = $arrCompanyData["balanceType"];
	$transNote = $arrCompanyData["transNote"];
	$status = $arrCompanyData["status"];
	$updated = $arrCompanyData["updated"];

	if($ttTransFlag)
		$CustID = $arrCompanyData["CustID"];
	if(!empty($CustID))
	{
		$strGetDataSql = "SELECT 
							  firstName,
							  lastName
							  							  							  
						  FROM 
						  	 customer 
						  WHERE 
						    customerID = '".$CustID."'";
		$arrCustData = selectFrom($strGetDataSql);
	}
}

$arguments = array("flag"=>"ibanAccountField","IBAN"=>$IBAN,"fieldsChecked"=>$fieldsChecked,"fieldLabel"=>"IBAN");
$ibanField = gateway("CONFIG_IBAN_TRADING_ACCOUNT",$arguments,CONFIG_IBAN_TRADING_ACCOUNT);
if(is_array($ibanField)){
	$fieldIBANLabel = $ibanField["fieldLabel"];
	$fieldIBANStr = $ibanField["fieldStr"];
	$fieldsCheckedArr = unserialize($fieldsCheckedStr);
	if(is_array($fieldsCheckedArr)){
		$arrayParams = array($fieldsCheckedArr);
		array_walk($fieldsCheckedArr,'removeFromEnd',$arrayParams);
		$fieldsCheckedArr = array_flip($fieldsCheckedArr);
	}
}
$arguments = array("flag"=>"dealFieldsAccountField","showOnDeal"=>$showOnDeal,"fieldDealLabel"=>"Show on Receipt(s)");
$dealField = gateway("CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT",$arguments,CONFIG_TRADING_ACCOUNT_TRANSACTION_RECEIPT);
if(is_array($dealField)){
	$dealFieldsFlag = true;
	$fieldDealLabel = $dealField["fieldDealLabel"];
	$fieldDealStr = $dealField["fieldDealStr"];
}

/**
 * Picking up the country and city data to display in the drop down list
 */
$arrCountries = selectMultiRecords("select countryname from countries order by countryname");

/* #8597 - AMB Exchange
 * Allow Zero balance in account
*/
$minBalanceFlag = true;
if(CONFIG_ALLOW_ZERO_BALANCE_ACCOUNT == 1){
	$minBalanceFlag = false;
}

$accNumberReadonly = '';
if(!empty($id)){
	$accNumberReadonly = 'readonly="readonly"';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Add New Account</title>
	<script language="javascript" src="jquery.js"></script>
	<script language="javascript" src="javascript/jquery.validate.js"></script>
	<script language="javascript" src="javascript/jquery.form.js"></script>
	<script language="javascript" src="./javascript/functions.js"></script>
	<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
	<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
	<script>
		$(document).ready(function() {

			$("#customer_input").autocomplete('searchCustomer-ajax.php', {
					mustMatch: true,
					maxItemsToShow : -1,
				}
			);
			$("#customer_input").result(function(event, data, formatted) {
				if (data)
				{

					$("#cusdi").val(data[1]);

				}
			});
			$("#newCustomerBtn").click(function(){
				window.open ("/admin/add-customer.php","AddCustomer","location=1,scrollbars=1,width=900,height=450");
			});
			$.validator.addMethod("checkAvailability",function(value,element){
				var accNumber = $("#accountNUmber").val();
				var currency = $("#currency").val();
				var parameter="action=checkaccnumber&accNumber="+accNumber+"&currency="+currency;
				$.ajax({
					url: "add-account.php",
					type: "POST",
					async: false,
					data: parameter,
					success:function(output)
					{
						alert(output);
						return output === "true" ? true : false;
					}
				});
			},"<br/>Sorry, this account number already exist.");

			$("#submitCompany").validate({
				rules: {
					accountNUmber: {
						required: true
						<?php if(empty($id)){?>
						//,checkAvailability: true // remote check for duplicate account number
						,remote:{
							url: "add-account.php",
							type: "POST",
							data: {
								action: function () {
									return "checkaccnumber";
								},
								accNumber: function() {
									var accNumber = $("#accountNUmber").val();
									return accNumber;
								},
								currency: function() {
									var currency = $("#currency").val();
									return currency;
								}
							}
						}
						<?php }?>
					},
					currency: "required",
					balance: {
						required:true,
						number:true
						<?php if($minBalanceFlag){?>
						,min:0.0001
						<?php }?>
					},
					balanceType: "required"
				},
				messages: {
					accountNUmber: {
						required: "<br/>Please enter account number"
						<?php if(empty($id)){?>
						,remote: "<br/>This account number already exist."
						<?php }?>
					},
					currency: "<br/>Please select the currency from dropdown",
					balance:  {
						required:"<br/>Please enter opening balance",
						number:"<br/>Please enter valid amount"
						<?php if($minBalanceFlag){?>
						,min:"<br/>Please provide more than 0."
						<?php }?>
					},
					balanceType: "<br/>Please select balance type"
				}

			});


			$("#closeWindow").click(function(){
				close();
			});

			$("#accountChart").change(function(){
				if($("#accountChart").val() == 'IN-BAR-001')
				{
					$("#bankNameRows").show();
					$("#sortCodeRows").show();
				}
				else if($("#accountChart").val() == '1005' || $("#accountChart").val() == '1001')
				{
					$("#bankNameRows").show();
					$("#bankAddRows").show();
					$("#branchNameRows").show();
					$("#branchAddRows").show();
					$("#sortCodeRows").show();
				}
				else
				{
					$("#bankNameRows").hide();
					$("#bankAddRows").hide();
					$("#branchNameRows").hide();
					$("#branchAddRows").hide();
					$("#sortCodeRows").hide();
				}
			});

			$("#accountChart").change(function(){

				if($("#accountChart").val() == '100' || $("#accountChart").val() == '1001')
					$("#transRow2").show();
				else
					$("#transRow2").hide();
			});

			$("#accountChart").change(function(){

				if($("#accountChart").val() == '1001' || $("#accountChart").val() == '4001' || $("#accountChart").val() == '420')
					$("#transRow").show();
				else
					$("#transRow").hide();
			});

			// trigger on change event to display account type details on update case.
			$("#accountChart").trigger("change");
		});

		function SelectOption(OptionListName, ListVal)
		{
			for (i=0; i < OptionListName.length; i++)
			{
				if (OptionListName.options[i].value == ListVal)
				{
					OptionListName.selectedIndex = i;
					break;
				}
			}
		}


		// function will clear input elements on each form
		function clearFormCustomize(){
			// declare element type
			var type = null;
			// loop through forms on HTML page
			for (var x=0; x<document.forms.length; x++){
				// loop through each element on form
				for (var y=0; y<document.forms[x].elements.length; y++){
					// define element type
					type = document.forms[x].elements[y].type
					// switch on element type
					switch(type){
						case "text":
						case "textarea":
						case "password":
							document.forms[x].elements[y].value = "";
							break;
						case "radio":
						case "checkbox":
							document.forms[x].elements[y].checked = "";
							break;
						case "select-one":
							document.forms[x].elements[y].options[0].selected = true;
							break;
						case "select-multiple":
							for (z=0; z<document.forms[x].elements[y].options.length; z++){
								document.forms[x].elements[y].options[z].selected = false;
							}
							break;
					}
				}
			}
		}
	</script>
	<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
	<style>
		td{ font-family:Verdana, Arial, Helvetica, sans-serif;
			size:8px;
		}
		.tdDefination
		{
			font-size:12px;
			text-align:right;
		}
		.remarks
		{
			font-size:12px;
			text-align:center;
			font-weight:bold;
		}
		.error {
			color: red;
			font: 8pt verdana;
			font-weight:bold;
		}
		.goback
		{
			font-size:10px;
			text-align:left;
			font-weight:bold;
		}
	</style>
</head>
<body>
<form name="submitCompany" id="submitCompany" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="80%" border="0" cellspacing="1" cellpadding="2" align="center">
		<tr><td bgcolor="#DFE6EA" class="goback">
				<?
				if (!empty($_REQUEST['id']))
				{
					echo "<a href='update-account.php'>Go Back</a>";
				}
				?>
			</td>
			<td colspan="4" align="center" bgcolor="#DFE6EA">

				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if(!empty($msg)) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:10px;">
					<img src="images/info.gif" />&nbsp;<?=$msg?>				</td>
			</tr>
		<? } ?>

		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
				All fields mark with <font color="red">*</font> are compulsory Fields.			</td>
		</tr>
		<?php if($ttTransFlag) { ?>
			<tr bgcolor="#ededed">
				<td align="right" colspan="4">
					<a href="javascript:void(0)" id="newCustomerBtn" class="links">[ Add New Customer ]</a>
				</td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="25%" class="tdDefination"><b>Search Customer:&nbsp;</b></td>
				<td align="left" colspan="3">
					<input type="text" name="customer_input" id="customer_input" size="80" maxlength="150"  value="<?

					if(!empty($arrCustData))
						echo $arrCustData["firstName"]." ".$arrCustData["lastName"]?>"/>

				</td>
			</tr>

		<?php } ?>


		<tr bgcolor="#ededed">
			<td width="15%" class="tdDefination"><b>Account Type: &nbsp;</b></td>
			<td align="left" colspan="3">
				<?
				$strSql = "SELECT id,accountName,accountNumber FROM ".TBL_ACCOUNTS_CHART;
				$rsAccount = selectMultiRecords($strSql);

				?>
				<select name="accountChart" id="accountChart">
					<option value="">--Select Account Type--</option>
					<?

					for($i=0; $i<sizeof($rsAccount); $i++) {
						$strSelected = '';
						if($rsAccount[$i]["accountNumber"] == $accounType)
							$strSelected = ' selected="selected"';
						?>
						<option value="<?=$rsAccount[$i]["accountNumber"]?>" <?=$strSelected ?>><?=$rsAccount[$i]["accountName"];?></option>
					<? }?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#ededed"><!--id="bankNameRows" style="display:none"-->
			<td width="15%" class="tdDefination"><b>Bank Name:&nbsp;</b></td>
			<td align="left" width="25%">
				<input type="text" name="bankName" id="bankName" size="50" maxlength="150" value="<?=$bankName?>" /></td>
			<?php if(!empty($fieldIBANLabel) && !empty($fieldIBANStr)){?>
				<td width="25%" class="tdDefination"><b><?=$fieldIBANLabel?>:&nbsp;</b></td>
				<td align="left" width="25%"><?=$fieldIBANStr?></td>
			<?php }else{?>
				<td width="25%" class="tdDefination" colspan="2">&nbsp;</td>
			<?php }?>
		</tr>
		<tr bgcolor="#ededed"><!-- id="bankAddRows" style="display:none" -->
			<td width="15%" class="tdDefination"><b>Bank Address:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="bankAddress" id="bankAddress" size="50" maxlength="150" value="<?=$bankAddress?>" /></td>
		<tr bgcolor="#ededed" id="branchNameRows" style="display:none">
			<td width="15%" class="tdDefination"><b>Branch Name:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="branchName" id="branchName" size="50" maxlength="150" value="<?=$branchName?>" /></td>
		<tr bgcolor="#ededed" id="branchAddRows" style="display:none">
			<td width="15%" class="tdDefination"><b>Branch Address:&nbsp;</b></td>
			<td align="left" colspan="3"><input type="text" name="branchAddress" id="branchAddress" size="50" maxlength="150" value="<?=$branchAddress?>" /></td>
		</tr>
		<tr bgcolor="#ededed"><!-- id="sortCodeRows" style="display:none" -->
			<td width="15%" class="tdDefination"><b>Swift Code:&nbsp;</b></td>
			<td align="left" width="25%">
				<input type="text" name="swiftCode" id="swiftCode" size="20" maxlength="150" value="<?=$swiftCode?>" /></td>
			<td width="25%" class="tdDefination"><b>Sort Code:&nbsp;</b></td>
			<td align="left" width="25%">
				<input type="text" name="sortCode" id="sortCode" size="20" maxlength="150" value="<?=$sortCode?>" /></td>
		</tr>
		<div id="accountRows">
			<tr bgcolor="#ededed">
				<td width="15%" class="tdDefination"><b>Contact Person:&nbsp;</b></td>
				<td align="left" colspan="3"><input type="text" name="contactPerson" id="contactPerson" size="50" maxlength="150" value="<?=$contactPerson?>" />
					<?php if($dealFieldsFlag){?>
						&nbsp;<input type="checkbox" name="fieldsChecked[]" id="cntPers" value="name|Contact Person" <?=(isset($fieldsCheckedArr["name"])?'checked':'')?> />
					<?php }?>
				</td>

			</tr>
			<tr bgcolor="#ededed">
				<td width="15%" class="tdDefination"><b>Account Title&nbsp;</b></td>
				<td align="left" width="25%">
					<input type="text" name="accountName" id="accountName" size="20" maxlength="150" value="<?=$accountName?>" /></td>
				<td width="25%" class="tdDefination"><b>Account Number&nbsp;</b><font color="red">*</font></td>
				<td align="left" width="25%">
					<input type="text" name="accountNUmber" id="accountNUmber" size="20" maxlength="150" value="<?=$accountNUmber?>" <?//=$accNumberReadonly?> /></td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="25%" class="tdDefination"><b>Opening Balance&nbsp;</b><font color="red">*</font></td>
				<td align="left" width="25%"><input type="text" name="balance" id="balance" size="20" maxlength="10" value="<?=$balance?>" /></td>
				<td width="25%" class="tdDefination"><b>Currency&nbsp;</b><font color="red">*</font></b></td>
				<?php
				$strQuery = selectMultiRecords("SELECT DISTINCT(currencyName), description  FROM  currencies ORDER BY currencyName ");
				?>
				<td align="left" width="50%" colspan="3">
					<select name="currency" id="currency">
						<option value="">Select Currency</option>
						<?
						for($i = 0; $i < count($strQuery); $i++)
						{
							$strSelected = '';
							if($strQuery[$i]["currencyName"] == $currency)
								$strSelected = ' selected="selected"';
							?>
							<option value="<?=$strQuery[$i]["currencyName"] ?>" <?=$strSelected ?>><?=$strQuery[$i]["currencyName"]?></option>
						<?  } ?>
					</select>			</td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="25%" class="tdDefination"><b>Select Balance Type&nbsp;</b><font color="red">*</font></td>
				<td align="left" width="25%">
					<select name="balanceType" id="balanceType" style="width: 100px;">
						<option value="">Select type</option>
						<option value="Dr" <? echo ($balanceType == 'Dr'? 'selected="selected"':'') ?>>Debit</option>
						<option value="Cr" <? echo ($balanceType == 'Cr'? 'selected="selected"':'') ?>>Credit</option>
					</select>
				</td>

				<td width="25%" class="tdDefination">&nbsp;</td>
				<td align="left" width="50%" colspan="3">&nbsp;</td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="25%" class="tdDefination"><b>Country:&nbsp;</b></td>
				<td align="left" width="25%" >
					<select name="country" id="country">
						<option value="">Select Country</option>
						<?
						for($i = 0; $i < count($arrCountries); $i++)
						{
							$strSelected = '';
							if($arrCountries[$i]["countryname"] == $country)
								$strSelected = ' selected="selected"';
							?>
							<option value="<?=$arrCountries[$i]["countryname"] ?>" <?=$strSelected ?>><?=$arrCountries[$i]["countryname"]?></option>
						<?  } ?>
					</select>			</td>
				<td class="tdDefination"><b>City:&nbsp;</b></td>
				<td align="left"><input type="text" name="city" id="city" size="20" maxlength="150" value="<?=$city?>" /></td>
			</tr>
		</div>
		<tr bgcolor="#ededed">
			<td width="25%" class="tdDefination"><b>Phone:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="phone" id="phone" size="20" maxlength="150" value="<?=$phone?>" />
				<?php if($dealFieldsFlag){?>
					&nbsp;<input type="checkbox" name="fieldsChecked[]" id="ph" value="phone|Phone" <?=(isset($fieldsCheckedArr["phone"])?'checked':'')?> />
				<?php }?>
			</td>
			<td width="25%" class="tdDefination"><b>Mobile:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="mobile" id="mobile" size="20" maxlength="150" value="<?=$mobile?>" /></td>
		<tr bgcolor="#ededed">

			<?
			if($agentType == "SUPA" || $agentType == "SUBA"){
				echo '<td width="25%" class="tdDefination"><b>User Type:&nbsp;</b></td>';
				$allUsersSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						WHERE userID = $userID
						";
			}
			else { ?>
				<td width="25%" class="tdDefination"><b>User Type:&nbsp;</b></td>
				<?

				$allUsersSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS;
				//where adminType='Supper' AND isMain='Y' AND agentStatus='Active' order by username
			}
			?>
			<td align="left" width="25%"><select name="userId" id="userId">
					<option value=""> -Select User- </option>

					<optgroup label="Super Admin">
						<?
						$superAdminRS = selectMultiRecords($allUsersSql);

						for($i=0; $i<sizeof($superAdminRS); $i++) {
							$strSelected= '';
							if($superAdminRS[$i]["userID"] == $userId)
								$strSelected = ' selected="selected"';
							?>
							<option value="<?=$superAdminRS[$i]["userID"]?>" <?=$strSelected?>><?=$superAdminRS[$i]["username"]."[".$superAdminRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>
					<!--<optgroup label="Agents">-->
					<optgroup label="Introducer">
						<?
						$agentClause = " where adminType = 'Agent' AND parentID > 0 AND isCorrespondent != 'ONLY' AND agentStatus='Active' order by username";
						$agentsRS = selectMultiRecords($allUsersSql.$agentClause);
						$strSelected = ' selected="selected"';
						?>
						<?
						for($i=0; $i<sizeof($agentsRS); $i++) {
							$strSelected = '';
							if($agentsRS[$i]["userID"] == $userId)
								$strSelected = 'selected="selected"';
							?>
							<option value="<?=$agentsRS[$i]["userID"]?>" <?=$strSelected?>><?=$agentsRS[$i]["username"]."[".$agentsRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>
					<optgroup label="Distributors">
						<?
						$distClause = " where adminType='Agent' AND isCorrespondent ='ONLY' AND agentStatus='Active' order by username";
						$distRS = selectMultiRecords($allUsersSql.$distClause);
						?>
						<? for($i=0; $i<sizeof($distRS); $i++) { ?>
							<option value="<?=$distRS[$i]["userID"]?>"><?=$distRS[$i]["username"]."[".$distRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>

					<optgroup label="Admin Staff">
						<?
						$adminStaffClause = " where adminType = 'Admin' AND agentStatus='Active' order by username";
						$adminStaffRS = selectMultiRecords($allUsersSql.$adminStaffClause);
						?>
						<? for($i=0; $i<sizeof($adminStaffRS); $i++) { ?>
							<option value="<?=$adminStaffRS[$i]["userID"]?>"><?=$adminStaffRS[$i]["username"]."[".$adminStaffRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>

					<optgroup label="Branch Manager">
						<?
						$branchManagerClause = " where adminType='Branch Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$branchManagerRS = selectMultiRecords($allUsersSql.$branchManagerClause);
						?>
						<? for($i=0; $i<sizeof($branchManagerRS); $i++) { ?>
							<option value="<?=$branchManagerRS[$i]["userID"]?>"><?=$branchManagerRS[$i]["username"]."[".$branchManagerRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>

					<optgroup label="Admin Manager">
						<?
						$adminManagerClause = " where adminType='Admin Manager' AND isMain='N' AND agentStatus='Active' order by username";
						$adminManagerRS = selectMultiRecords($allUsersSql.$adminManagerClause);
						?>
						<? for($i=0; $i<sizeof($adminManagerRS); $i++) { ?>
							<option value="<?=$adminManagerRS[$i]["userID"]?>"><?=$adminManagerRS[$i]["username"]."[".$adminManagerRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>
					<optgroup label="Call Center Staff">
						<?
						$callCenterStaffClause = " where adminType='Call' AND isMain='N' AND agentStatus='Active' order by username";
						$callCenterStaffRS = selectMultiRecords($allUsersSql.$callCenterStaffClause);
						?>
						<? for($i=0; $i<sizeof($callCenterStaffRS); $i++) { ?>
							<option value="<?=$callCenterStaffRS[$i]["userID"]?>"><?=$callCenterStaffRS[$i]["username"]."[".$callCenterStaffRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>
					<optgroup label="Support Staff">
						<?
						$supportStaffClause = " where adminType ='Support' AND agentStatus='Active' order by username";
						$supportStaffRS = selectMultiRecords($allUsersSql.$supportStaffClause);
						?>
						<? for($i=0; $i<sizeof($supportStaffRS); $i++) { ?>
							<option value="<?=$supportStaffRS[$i]["userID"]?>"><?=$supportStaffRS[$i]["username"]."[".$supportStaffRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>

					<optgroup label="COLLECTOR">
						<?
						$collectorClause = " where adminType ='COLLECTOR' AND  isMain='N' AND  agentStatus='Active' order by username";
						$collectorRS = selectMultiRecords($allUsersSql.$collectorClause);
						?>
						<? for($i=0; $i<sizeof($collectorRS); $i++) { ?>
							<option value="<?=$collectorRS[$i]["userID"]?>"><?=$collectorRS[$i]["username"]."[".$collectorRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>

					<optgroup label="Distributor Manager">
						<?
						$distManagerClause = " where adminType ='SUPI Manager' AND  isMain='N' AND agentStatus='Active' order by username";
						$distManagerRS = selectMultiRecords($allUsersSql.$distManagerClause);
						?>
						<? for($i=0; $i<sizeof($distManagerRS); $i++) { ?>
							<option value="<?=$distManagerRS[$i]["userID"]?>"><?=$distManagerRS[$i]["username"]."[".$distManagerRS[$i]["name"]."]" ?></option>
						<? }?>
					</optgroup>


				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$user_id_agent_account; ?>");
				</script>			</td>


			<td width="25%" class="tdDefination"><b>Fax:&nbsp;</b></td>
			<td align="left" width="25%"><input type="text" name="fax" id="fax" size="20" maxlength="10" value="<?=$fax?>" />
				<?php if($dealFieldsFlag){?>
					&nbsp;<input type="checkbox" name="fieldsChecked[]" id="fx" value="fax|Fax" <?=(isset($fieldsCheckedArr["fax"])?'checked':'')?> />
				<?php }?>
			</td>
		</tr>
		<?
		$checked = '';

		?>
		<tr bgcolor="#ededed" id="transRow" style="display:none">
			<td class="tdDefination"><b>Associate with Transaction </b></td>

			<td align="left">Yes: &nbsp;
				<input type="radio" name="transFlag" id="transFlag" value="Y" <? echo ($transFlag== 'Y'?'checked="checked"':'') ?> />
				&nbsp;&nbsp;No:
				<input type="radio" name="transFlag" id="transFlag" value="N"  <? echo ($transFlag == 'Y'?'':'checked="checked"') ?> /></td>
			<td class="tdDefination">&nbsp;</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr bgcolor="#ededed" id="transRow2" style="display:none">
			<td class="tdDefination"><b>Associate with Agent Manual Deposit/Withdraw </b></td>

			<td align="left">Yes: &nbsp;
				<input type="radio" name="depositFlag" id="depositFlag" value="Y" <? echo ($depositFlag== 'Y'?'checked="checked"':'') ?> />
				&nbsp;&nbsp;No:
				<input type="radio" name="depositFlag" id="depositFlag" value="N"  <? echo ($depositFlag == 'Y'?'':'checked="checked"') ?> /></td>
			<td class="tdDefination">&nbsp;</td>
			<td align="left">&nbsp;</td>
		</tr>
		<?php if($dealFieldsFlag){?>
			<tr bgcolor="#ededed">
				<td class="tdDefination"><b><?=$fieldDealLabel?></b></td>
				<td align="left">&nbsp;&nbsp;<?=$fieldDealStr?></td>
				<td class="tdDefination">&nbsp;</td>
				<td align="left">&nbsp;</td>
			</tr>
		<?php }?>

		<tr bgcolor="#ededed">
			<td colspan="4" class="remarks"><br />
				<table>
				</table>		  </td>
		</tr>
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />	&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="customerID" value="<?=$_REQUEST["customerID"]?>" />
				<input type="hidden" name="from" value="<?=$_REQUEST["from"]?>" />
				<input type="hidden" name="agentID" value="<?=$_REQUEST["agentID"]?>" />
				<input type="hidden" name="transID" value="<?=$_REQUEST["transID"]?>" />
				<input type="hidden" name="id" value="<?=$_REQUEST["id"]?>" />
				<input type="hidden" name="updated" value="<?=$arrCompanyData["updated"]?>" />
				<input type="hidden" name="status" value="<?=$arrCompanyData["status"]?>" />
				<input type="hidden" name="transNote" value="<?=$arrCompanyData["transNote"]?>" />
				<input type="hidden" name="accounType" value="<?=$arrCompanyData["accounType"]?>" />

				<input type="hidden" name="saveInputs" value="y" />
				<?php 	if($ttTransFlag) ?>
				<input type="hidden" name="cusdi" id="cusdi" value="<?php echo $CustID?>" />

				&nbsp;&nbsp;
				<!--<input type="button" id="closeWindow" value="Close Window" />-->
			</td>
		</tr>
	</table>
</form>
</body>
</html>
