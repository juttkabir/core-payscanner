<?
include ("../include/config.php");
include ("security.php");
session_start();

if ($_POST["Search"] == "Search")
{
	$_SESSION["countryName"] = $_POST["country"];
	$_SESSION["state"] = $_POST["state"];
	$_SESSION["city"] = $_POST["city"];
	$cityCount = countRecords("select count(*) from ".TBL_SRV_NOT_AVAILABLE." where city = '".$_SESSION["city"]."' and state = '".$_SESSION["state"]."' and country = '".$_SESSION["countryName"]."' and Active = 'Yes'");
	$queryDistID = selectFrom("select dist_ID from ".TBL_SRV_NOT_AVAILABLE." where city = '".$_SESSION["city"]."' and state = '".$_SESSION["state"]."' and country = '".$_SESSION["countryName"]."'");
}

elseif($_GET["msg"] != "")
{
	$cityCount = countRecords("select count(*) from ".TBL_SRV_NOT_AVAILABLE." where city = '".$_SESSION["city"]."' and state = '".$_SESSION["state"]."' and country = '".$_SESSION["countryName"]."' and Active = 'Yes'");
	$queryDistID = selectFrom("select dist_ID from ".TBL_SRV_NOT_AVAILABLE." where city = '".$_SESSION["city"]."' and state = '".$_SESSION["state"]."' and country = '".$_SESSION["countryName"]."'");
}

else
{
	$_SESSION["countryName"] = "";	
	$_SESSION["state"] = "";
	$_SESSION["city"] = "";
	$_SESSION["ass"] = "";
}
		
?>
<html>
<head>
	<title>Home Delivery Services</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
			function checkForm()
			{
				if(document.cities.country.value == "")
				{
					alert("Please select the Country.");
					cities.country.focus();
					return false;	
				}	
				if(document.cities.state.value == "")
				{
					alert("Please provide the State Name.");
					cities.state.focus();
					return false;	
				}	
				if(document.cities.city.value == "")
				{
					alert("Please provide the City Name.");
					cities.city.focus();
					return false;	
				}	
			}	
		</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0" align="center"><strong><font color="#FFFFFF" size="2">Home Delivery Services</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form method="post" name="cities" action="city-services.php" onSubmit="return checkForm();">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap><strong>Country*</strong> 
              <select name="country" style="font-family:verdana; font-size: 11px">
						<option value="">-Select Country-</option>
						<? 
						$countryContents = selectMultiRecords("select * from ".TBL_COUNTRY." where countryType like '%destination%'");
						for ($i=0;$i<count($countryContents);$i++){
							?>
						<option value="<? echo $countryContents[$i]["countryName"]?>" <? echo ($countryContents[$i]["countryName"]==$_SESSION["countryName"] ? "selected" : "")?>><? echo $countryContents[$i]["countryName"]?></option>
					<? } ?>
					</select>
					<strong>State/Province*</strong>
				<input type="text" name="state" value="<? echo $_SESSION["state"]?>">
				<strong>City*</strong> 
				<input type="text" name="city" value="<? echo $_SESSION["city"]?>">
				<input type="submit" value="Search" name="Search">
              </td>
      </tr>
	  </form>
    </table>
      <br>
      <strong>
      	<? echo ($_GET["msg"]!="" ? ($_GET["msg1"]!="" ? SUCCESS_MARK . " Updated Successfully" : "") : "")?>
      </strong>
      <br>
      <br>
      <?
			if ($_POST["Search"] == "Search" || $_GET["msg"] != "")
			{
			?>
      <table width="40%" border="1" align="center" cellpadding="3" cellspacing="0">
       <tr>
        <td width="39%"><span class="style1"><font color="#005b90" > Country Name </font></span></td>
        <td width="61%"><span class="style1"><? echo $_SESSION["countryName"]?></span></td>
        </tr>
        <tr>
        <td width="39%"><span class="style1"><font color="#005b90" >State Name </font></span></td>
        <td width="61%"><span class="style1"><? echo $_SESSION["state"]?></span></td>
        </tr>
        <tr>
        <td width="39%"><span class="style1"><font color="#005b90" >City Name </font></span></td>
        <td width="61%"><span class="style1"><? echo $_SESSION["city"]?></span></td>
        </tr>
        <tr>
        <td width="39%"><span class="style1"><font color="#005b90" >Distributor Name </font></span></td>
        <td width="61%"><span class="style1">
        	<? 
			$queryDist = selectFrom("select userID, agentCompany, name from ".TBL_ADMIN_USERS." where userID = '".$queryDistID[0]."'");
			$queryDistCnt = countRecords("select count(*) from ".TBL_ADMIN_USERS." where userID = '".$queryDistID[0]."'");
			if ($queryDistCnt > 0)
			{
				echo $queryDist[1] . " [" . $queryDist[2] . "]"; 
		  	$_SESSION["distID"] = $queryDist[0];
		  	$_SESSION["ass"] = "";
			} else { 
				$_SESSION["distID"] = "";
				$_SESSION["ass"] = "Not Assigned Yet";
			?>
				No Distributor Assigned
		<? } ?>
        	</span></td>
        </tr>
        <tr>
        <td width="39%"><span class="style1"><font color="#005b90" >Home Delivery Service </font></span></td>
        <td width="61%"><span class="style1">
        	<? 
				$_SESSION["enDis"]=($cityCount > 0 ? "Enabled" : "Disabled");
				echo $_SESSION["enDis"];
				?>
        	</span></td>
        </tr>
        <tr>
        <td width="39%">&nbsp;</td>
        <td width="61%">&nbsp;</td>
        </tr>
        <tr>
        <td width="39%"><span class="style1"><a href="update-city-services.php" class="style3"><font color="#005b90"><b>Update</b></font></a></span></td>
        <td width="61%">&nbsp;</td>
        </tr>
       </table>
<? } ?>
</body>
</html>