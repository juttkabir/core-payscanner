<?
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Available Cheque Orders</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var selectedId = "";

	$(document).ready(function(){
	
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
		});

		
		$("#transList").jqGrid({
			url:'chequeOrderCalculation.php?get=gd&q=2&nd='+new Date().getTime(),
			datatype: "json",
			height: 400, 
			width: 900,
			colNames:[
				'Cheque Reference', 
				'Cheque Number', 
				'Client',
				'Date In',
				'Estimated Date',
				'Paid On',
				'Value',
				'Amount Paid',
				'Status',
				'Created By'
			],
			colModel:[
				{name:'cheque_ref',index:'cheque_ref', width:55},
				{name:'cheque_no',index:'cheque_no', width:50},
				{name:'name',index:'name', width:200},
				{name:'created_on',index:'created_on', width:80, align:"right"},
				{name:'estimated_date',index:'estimated_date', width:80, align:"right"},		
				{name:'paid_on',index:'paid_on', width:80,align:"right"},
				{name:'cheque_amount',index:'cheque_amount', width:50, sortable:false},
				{name:'paid_amount',index:'paid_amount', width:60, sortable:false},
				{name:'status',index:'status', width:80, sortable:false},
				{name:'created_by',index:'created_by', width:150},
			],
			rowNum: 20,
			rowList: [10,20,50],
			imgpath: gridimgpath,
			pager: jQuery('#pager'),
			sortname: 'created_on',
			viewrecords: true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Available Cheque Orders",
			onSelectRow:function(id){
							if(id)
							{
								if(selectedId == id)
								{
									selectedId = "";
									$("#orderDetail").html("");
								}
								else
								{
									selectedId = id;
									//alert(id);
									loadOrder(id);
								}
							}
			}/*,
			onSelectAll:function(ids){
							alert(ids);
			}*/
		});
		
		jQuery('a').cluetip({splitTitle: '|'});
		
		
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});


		$("#search_orders").click(function(){
			gridReload();
		});
		
		
		$("#exportBtn").click(function(){
			$("#exportOrdersRow").show();
		});
		
		$("#exportOrdersRow").change(function(){
			if($(this).val() != "")
			{
				//alert($(this).val());
				var strUrl = "chequeOrderExport.php?exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()
							+"&cheque_ref="+$("#cheque_ref").val()+
							"&name="+$("#name").val()+"&order_status_filter="+$("#order_status_filter").val()+"&Submit=Search";		
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
		
	});
	
	function gridReload()
	{
		var date_from = $("#from_date").val();
		var to_date = $("#to_date").val();
		var cheque_ref = $("#cheque_ref").val();
		var name = $("#name").val();
		var order_status_filter = $("#order_status_filter").val();
		
		var theUrl = "chequeOrderCalculation.php?get=gd&from_date="+date_from+"&to_date="+to_date+"&cheque_ref="+cheque_ref+
  					"&name="+name+"&order_status_filter="+order_status_filter+"&Submit=Search";
		
		$("#transList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
		
	}


	function loadOrder(di)
	{
	
		//$("#orderDetail").load("chequeOrderCalculation.php?data=order&odi="+di);
		
		$.ajax({
			url: "chequeOrderCalculation.php?data=order&odi="+di,
			cache: false,
			type: "POST",
			success: function(html){
				$("#orderDetail").html(html);
				$("#estimated_date").datePicker();
				validateEditOrder();
			}
		});
		
	}
	
	
	function validateEditOrder()
	{
	
		$("#edit_order").validate({
			rules:{
				branch_name: {
					required: true
				},
				account_no: {
					required: true
				},
				order_status: {
					required: true
				}
			},
			messages: {
				branch_name: {
					required: "<br />Please enter branch value."
				},
				account_no: {
					required: "<br />Please enter account of the cheque."
				},
				order_status: {
					required: "<br />Please select the appropriate cheque status."
				}
			},
			submitHandler: function(form){

				$("#edit_order").ajaxSubmit(function(data){
					if(data == "S")
					{
						var order_id = $("#eodi").val();
						var status = $("#order_status").val();
						alert("Order Updated.");
						$("#orderDetail").html("");
						gridReload();
						if(status == "ID")
							window.open ("/admin/chequeReceipt.php?oi="+order_id,"CustomerReceipt","location=0,scrollbars=1,width=300,height=500"); 
					}
					else
					{
						alert("Could not update the order, try again.");
						$("#orderDetail").html("");
					}
				});
			}
		});
	
	}
	

</script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
/*a.dp-choose-date {
	float: left;
	width: 16px;
	height: 16px;
	padding: 0;
	margin: 5px 3px 0;
	display: block;
	text-indent: -2000px;
	overflow: hidden;
	background: url(images/calendar.jpg) no-repeat; 
}
a.dp-choose-date.dp-disabled {
	background-position: 0 -20px;
	cursor: default;
}*/
.error {
	color: red;
	font: 6pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" bgcolor="#DFE6EA">
			<strong>Available Cheque Orders</strong>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
			<form name="search_cheque" id="search_cheque" action="" method="post">
				Date In From 
				<input type="text" name="from_date" id="from_date" readonly="" />
				&nbsp;&nbsp;
				Date In To:
				<input type="text" name="to_date" id="to_date" readonly="" />
				<br /><br />
				Cheque Order Status:&nbsp;
				<select name="order_status_filter" id="order_status_filter">
					<option value="">All</option>
					<? foreach($arrChequeStatues as $kS => $vS) { ?>
						<option value="<?=$kS?>"><?=$vS?></option>
					<? } ?>
				</select>
				
				&nbsp;&nbsp;&nbsp;
				Cheque Refrence:&nbsp;<input type="text" name="cheque_ref" id="cheque_ref" />
				&nbsp;&nbsp;&nbsp;
				Client:&nbsp;<input type="text" name="name" id="name" />
				<br /><br />
				<input type="button" id="search_orders" name="search_orders" value="Search" style="font-weight:bold" />
				&nbsp;&nbsp;<input type="reset" value="Clear All Filters" />
				&nbsp;&nbsp;<input type="button" id="exportBtn" value="Export Searched Orders" />
				&nbsp;
				<select name="exportOrdersRow" id="exportOrdersRow" style="display:none">
					<option value="">Select Format</option>
					<option value="XLS">Excel</option>
					<option value="CSV">CSV</option>
					<option value="HTML">HTML</option>
				</select>
			</form>
		</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="80%">
			</table>
			<div id="pager" class="scroll" style="text-align:center;"></div>		
		</td>
    </tr>
	<tr>
		<td colspan="2" align="center">
			<div id="orderDetail" align="center" style="width:80%">
			</div>
		</td>
	</tr>
</table>
</body>
</html>
