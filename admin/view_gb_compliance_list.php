<?
/**
 * @package Compliance
 * @subpackage GB Compliance Sender List
 * This page display the records and search filters etc
 * @author Mirza Arslan Baig
 */

session_start();
include("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include("security.php");
//include ("javaScript.php");
//$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
?>
<html>
<head>
    <title>GB Compliance List</title>
    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type="text/javascript" src="./javascript/functions.js"></script>
    <link href="images/interface.css" rel="stylesheet" type="text/css"/>
    <link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css"/>
    <link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee"
          media="screen"/>
    <!--<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />-->
    <link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen"/>
    <link href="css/datePicker.css" rel="stylesheet" type="text/css"/>

    <script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
    <script src="javascript/date.js" type="text/javascript"></script>
    <script type="text/javascript" src="javascript/jquery.datePicker.js"></script>
    <script type="text/javascript" src="javascript/jqGrid/js/grid.locale-en.js"></script>
    <script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
    <script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
    <!--<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>-->
    <!--<script src="javascript/jqGrid/js/grid.inlineedit.js" type="text/javascript"></script>-->
    <!--<script src="jquery.cluetip.js" type="text/javascript"></script>-->
    <!--<script language="javascript" src="javascript/jquery.autocomplete.js"></script>-->
    <script type="text/javascript">
        var gridimgpath = 'javascript/jqGrid/themes/basic/images';
        var extraParams;
        jQuery(document).ready(function () {
            // Date Picker
            Date.format = 'yyyy-mm-dd';
            $('#fdate').datePicker({clickInput: true});
            $('#fdate').dpSetStartDate('2000-01-01');

            $('#tdate').datePicker({clickInput: true});
            $('#tdate').dpSetStartDate('2000-01-01');
            var lastSel;
            jQuery("#getList").jqGrid({
                url: 'view_gb_compliance_list_conf.php?getGrid=getList&Submit=Search',
                datatype: "json",
                height: 210,
                width: 920,
                colNames: [
                    'Sr.',
                    'Customer Name',
                    'Customer Number',
                    'Address',
                    'Country',
                    'Email',
                    'Creation Date',
                    'Compliance Details',
                    'Document',
                    'Verified By',
                    'Status',
                    'Browser Name'
                    //,'Edit'
                ],
                colModel: [
                    {name: 'sr', index: 'sr', width: 20, align: "center", sortable: false},
                    {
                        name: 'customerName',
                        index: 'customerName',
                        width: 60,
                        align: "left",
                        editable: false,
                        sortable: true
                    },
                    {
                        name: 'accountName',
                        index: 'accountName',
                        width: 90,
                        align: "left",
                        editable: false,
                        sortable: false
                    },
                    {name: 'Address', index: 'Address', width: 80, align: "left", editable: false, sortabel: false},
                    {name: 'Country', index: 'Country', width: 80, align: "left", editable: false, sortable: false},
                    {name: 'email', indx: 'email', width: 80, align: "left", sortable: false},
                    {name: 'created', index: 'created', width: 80, align: "left"},
                    {
                        name: 'Compliance Details',
                        index: 'Compliance Details',
                        width: 100,
                        align: "left",
                        sortable: false
                    },
                    {name: 'Document', index: 'Document', width: 60, align: "left", sortable: false},
                    {name: 'Verified By', index: 'Verified By', width: 60, align: "left", sortable: false},
                    {name: 'status', index: 'status', width: 50, align: "left"},
                    {name: 'browserName', index: 'browserName', width: 100, align: "center"},
                    //,{name:'Edit', index:'Edit', width:50, align:"left", sortable:false}
                ],
                imgpath: gridimgpath,
                rowNum: 10,
                rowList: [5, 10, 20, 50, 100, 200],
                pager: jQuery('#pagernavT'),
                multiselect: true,
                sortname: "created",
                sortorder: "DESC",
                viewrecords: true,
                loadonce: false,
                multiselect: false,
                loadui: "block",
                loadtext: "Loading ...",
                caption: "GB Compliance Customers List"
            }).navGrid('#pagernavT',
                {edit: false, add: false, del: false, search: false},
                {afterSubmit: editMessage, closeAfterEdit: false}, 	// edit options
                {}, 	// add options
                {} 		// del options
            );


            $('#Submit').click(
                function () {
                    if ($('#fdate').val() == '' && $('#tdate').val() == '' && $('#custName').val() == '' && $('#custNum').val() == '') {
                        alert("Please give filter values to search");
                        $('#custName').focus();
                        return;
                    }
                    if ($('#tdate').val() == '') {
                        /* alert("Please give to Date");
                        $('#tdate').click();
                        return; */
                    }
                    gridReload('getList');
                }
            );


            $('#export').click(
                function () {
                    gridReload('export');
                }
            );

            $('#print').click(
                function () {
                    gridReload('print');
                }
            );
        });


        // define handler function for 'afterSubmit' event.
        var editMessage = function (response, postdata) {
            var json = response.responseText;		// response text is returned from server.
            var result = JSON.parse(json);			// convert json object into javascript object.
            return [result.status, result.message, null];
        }


        function gridReload(grid) {
            var theUrl = "view_gb_compliance_list_conf.php";
            var extraParam = '';
            var fdate = jQuery("#fdate").val();
            var tdate = jQuery("#tdate").val();
            var customerName = jQuery("#custName").val();
            var customerNumber = jQuery("#custNum").val();
            extraParam = "?getGrid=" + grid + "&fdate=" + fdate + "&tdate=" + tdate + "&custName=" + customerName + "&custNum=" + customerNumber + "&Submit=Search";
            if (grid == 'export' || grid == 'print') {
                exportList(extraParam);
            } else if (grid == 'getList') {
                jQuery("#getList").setGridParam({
                    url: theUrl + extraParam,
                    page: 1
                }).trigger("reloadGrid");
            }
        }

        /*
        function Export(action)
        {
            var id_type_id	= jQuery("#id_type_id").val();
            var alertType	= jQuery("#alertType").val();
            var userType 	= jQuery("#userType").val();
            var status 		= jQuery("#status").val();
            window.open ("export_alert_controller.php?act="+action+"&id_type_id="+id_type_id+"&alertType="+alertType+"&userType="+userType+"&status="+status,"Export Alerts");
        }
        */
        function exportList(extraParam) {
            window.open("view_gb_compliance_list_conf.php" + extraParam, 'ExportDailyTransaction', 'height=600,width=1000,location=1,status=1,scrollbars=1');
        }


        function complianceDetails(senderID) {
            window.open("compliance_GB_details.php?custID=" + senderID, '_blank', 'scrollbars=yes,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=300,width=800');
        }

        function documentDetails(senderID) {
            window.open("viewDocument.php?customerID=" + senderID, '_blank', 'scrollbars=yes,toolbar=no,location=no,status=no,menubar=no,resizeable=yes,height=300,width=800');
        }

        /*
        function customerEdit(senderID, type){
            if(type != 'company')
                window.open("add-customer.php?customerID="+senderID, '_blank', 'scrollbars=yes,toolbar=no, location =no,status=no,menubar=no,resizeable=yes,height=300,width=800');

        }*/

    </script>
    <style type="text/css">
        <!--
        .style2 {
            color: #6699CC;
            font-weight: bold;
        }

        .style3 {
            color: #990000
        }

        .style1 {
            color: #005b90
        }

        .style4 {
            color: #005b90;
            font-weight: bold;
        }

        #searchTable {
            background-color: #ededed;
            border: 1px solid #000000;
        }

        #searchTable tr td {
            border-top: 1px solid #FFFFFF;
        }

        .dp-choose-date {
            margin-left: 5px;
        }

        div.scroll span {
            line-height: 17px;
            margin-left: 5px;
        }

        -->
    </style>
</head>
<body>
<div style=" margin:5px; padding-left:5px; height:30px; line-height:30px;background-color:#6699cc; color:#FFFFFF;">
    <strong>GB Compliance List<strong></div>
<form name="frmSearch" id="frmSearch">
    <table width="100" border="0" align="center">
        <tr>
            <td>
                <table id="searchTable" align="center" border="0" cellpadding="5" bordercolor="#666666">
                    <tr>
                        <th align="left" colspan="4" bgcolor="#6699cc"><span
                                    class="tab-u">Search Senders for Compliance</span></th>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            From <input type="text" readonly="readonly" name="fdate" id="fdate" value=""/>
                            To <input type="text" readonly="readonly" name="tdate" id="tdate"/>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap align="right">Customer Name</td>
                        <td>
                            <input id="custName" name="custName" style="font-family:verdana; font-size: 11px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap align="right">Customer Number</td>
                        <td>
                            <input id="custNum" name="custNum" style="WIDTH: 140px;HEIGHT: 18px; "/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" nowrap align="center">
                            <input type="button" name="Submit" value="Search" id="Submit"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table id="getList" border="0" cellpadding="5" cellspacing="1" class="scroll"></table>
                <div id="pagernavT" class="scroll" style="text-align:center;"></div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <form action="">
                    <input type="button" id="print" name="print" value="Print All"/>
                    <input type="button" id="export" name="export" value="Export All"/>
                </form>
            </td>
        </tr>
    </table>
</form>
</body>
</html>