<?php
session_start();
/**
 * @package Compliance
 * @subpackage Compliance Configuration at Create Transaction
 * Short Description
 * This file is used to create and edit compliance rules to implement at transaction creation level
 */
include ("../include/config.php");
include ("security.php");
include ("javaScript.php");
include_once("lib/audit-functions.php");
$date_time = date('Y-m-d h:i:s');
$edit_log_timeStamp=time();
extract(getHttpVars());
$documentFlag = true;
$errorFlag ='';
$agentType = getAgentType();
if ($agentType == "SUPA" || $agentType == "SUBA") {
    $userID  = $_SESSION["loggedUserData"]["userID"];
} else {
    $userID = $_POST["userID"];
}

$autoAuthorize1="";

if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
    $entity_country =getLoggedUserEntity($userID);

if($fDate == '' || $tDate == '')
{

    /* if user not set date for cumulative amount then min date from transaction
       table will be from date and current date will be to date by default   */

    $toDate = date('Y-m-d'); // current date
    $dateQuery=selectFrom("select min(transDate) as transDate  from ".TBL_TRANSACTIONS." ");

    if($dateQuery["transDate"] != '0000-00-00 00:00:00' && $dateQuery["transDate"] != '')
        $fromDate = date("Y-m-d", strtotime($dateQuery["transDate"])); //Min date
}

//$_SESSION["docCategory"] =($_REQUEST["docCategory"] != "" ? $_REQUEST["docCategory"] : $_SESSION["docCategory"]);
// Hold Transaction Code
$isHold =0;
if($_REQUEST['holdTrans'] == "on")
{
    $isHold =1;
}

if(CONFIG_AUTO_AUTHORIZATION=='1') {
    $autoAuthorize ="N";
    if($_REQUEST['autoAuthorizeTrans'] == "on")
    {
        $autoAuthorize ="Y";
    }
}

////end Hold
//show Message Code
$gridShow = "N";
if($_REQUEST['show_msg'] == "on")
{
    $gridShow = "Y";
}
if(!empty($_REQUEST["chk_doc_expiry"]))
    $chk_doc_expiry = $_REQUEST["chk_doc_expiry"];
else
    $chk_doc_expiry = 'N';

if(!empty($_REQUEST["chk_doc_upload"]))
    $chk_doc_upload = $_REQUEST["chk_doc_upload"];
else
    $chk_doc_upload = 'N';

if(!empty($_REQUEST["chk_doc_anyone"]))
    $chk_doc_anyone = $_REQUEST["chk_doc_anyone"];
else
    $chk_doc_anyone = 'N';

if(!empty($_REQUEST["is_proceed"]))
    $is_proceed = $_REQUEST["is_proceed"];
else
    $is_proceed = 'N';

if(isset($_REQUEST['isForAPItrans']) && $_REQUEST['isForAPItrans'] == '1'){
    $isForAPItrans = 1;
}else{
    $isForAPItrans = 0;
}
//end Show Message
if($fDate != "" && $tDate != "")
{
    $fDateArray= explode("/",$fDate);
    $fDay = $fDateArray[0];
    $fMonth = $fDateArray[1];
    $fYear = $fDateArray[2];

    $tDateArray= explode("/",$tDate);
    $tDay = $tDateArray[0];
    $tMonth = $tDateArray[1];
    $tYear = $tDateArray[2];

    $fromDate = $fYear . "-" . $fMonth . "-" . $fDay;
    $toDate = $tYear . "-" . $tMonth . "-" . $tDay;
}

if($ruleID !='' && $flag == 'update' && $submit != "Save" )
{
    $query = selectFrom("
							 SELECT 
							 		*
							 FROM 
								".TBL_COMPLIANCE_LOGIC." 
							 WHERE ruleID = ".$ruleID."
							");

    $applyAt=$query["applyAt"];
    $userType = $query["userType"];
    $applyAtTrans=$query["applyTrans"];
    $fDate=$query["cumulativeFromDate"];
    $tDate=$query["cumulativeToDate"];
    $amountCriteria=$query["matchCriteria"];
    $amount=$query["amount"];
    $amountType=$query["amountType"];
    $message=$query["message"];
    $country=$query["country"];
    $dCountry=$query["dest_country"];
    $tType = $query["ttype"];
    $originCountry = $query["country"];
    $dest_country = $query["dest_country"];
    $isHold = $query["isHold"];
    $showMessage = $gridShow = $query["showGrid"];
    $originCCY = $query["originCurrency"];
    $destCCY = $query["destCurrency"];
    $ctrChk=$query["CTR"];
    if(CONFIG_AUTO_AUTHORIZATION=='1') {
        $autoAuthorize = $query["autoAuthorize"];
    }
    $chk_doc_expiry    = $query["id_expiry"];
    $chk_doc_upload    = $query["id_upload"];
    $chk_doc_anyone    = $query["id_check"];
    $chk_trans_proceed = $query["is_proceed"];
    //print_r($query);
    $docType = $query["docCategory"];
    $isForAPItrans = $query["isForAPItrans"];
    if(!empty($query["period"]))
    {
        $arrPeriod = explode("|",$query["period"]);
        $period = $arrPeriod[0];
        $setDays = $arrPeriod[1];
    }
    if($documentFlag)
    {
        if(!empty($query["doc_provided"]))
            $_SESSION["docCategory"] = unserialize($query["doc_provided"]);
    }
}

// This query select ruleID  form compliance Logic table. if ruleID already  exist  then update previous rule .
$autoAuthorize1="";
if(CONFIG_AUTO_AUTHORIZATION=='1') {
    $autoAuthorize1 = "  autoAuthorize= '".$autoAuthorize."' AND ";
}
$addedConf = selectFrom("
								  SELECT 
										ruleID 
								  FROM 
										".TBL_COMPLIANCE_LOGIC."
								  WHERE

										applyTrans     	= '".$applyAtTrans."' AND
										applyAt        	='".$applyAt."' AND
										userType		= '".$userType."' AND
										matchCriteria  	= '".$amountCriteria."' AND
										amount 			= '".$amount."' AND
										isHold 			= ".$isHold." AND
										".$autoAuthorize1."
										id_expiry 		= '".$chk_doc_expiry."' AND
										id_upload 		= '".$chk_doc_upload."' AND
										id_check 		= '".$chk_doc_anyone."' AND
										is_proceed 		= '".$chk_trans_proceed."'");
////										country 		= '".$country."' AND
//dest_country 	= '".$dCountry."' AND
//ttype 			= '".$tType."' AND
//period 			LIKE ('".$period."%') AND
//amountType 		= '".$amountType."' AND
//originCurrency 	= '".$originCCY."' AND
//destCurrency 	= '".$destCCY."' AND




if($submit == "Save" && $ruleID != '' )
{
    if($addedConf["ruleID"]!= '')
    {
        $arrBeforeEdit = getRowData("complianceLogic", 'ruleID', $ruleID);

        $autoAuthorize1="";
        if(CONFIG_AUTO_AUTHORIZATION=='1') {
            $autoAuthorize1 = "  autoAuthorize 			= '".$autoAuthorize."', ";
        }
        $chk_trans_proceed = isset($_POST['chk_trans_proceed'])? $_POST['chk_trans_proceed'] : '';
        $_SESSION["docCategory"] =($_REQUEST["docCategory"] != "" ? $_REQUEST["docCategory"] : '');

        $updateQuery = "UPDATE 
                            ".TBL_COMPLIANCE_LOGIC." 
                        SET 
                            isHold         = ".$isHold.", 
                            ".$autoAuthorize1."
                            showGrid       = '".$gridShow."',
                            message        = '".$message."',
                            updated        = '".$date_time."',
                            CTR            = '".$ctrChk."',
                            doc_provided   = '".serialize($_SESSION["docCategory"])."',
                            id_expiry 	   = '".$chk_doc_expiry."',
                            id_upload 	   = '".$chk_doc_upload."' ,
                            id_check 	   = '".$chk_doc_anyone."' ,
                            docCategory    = '".$selectDocs."',
                            is_proceed 	   = '".$chk_trans_proceed."',
                            isForAPItrans  = '".$isForAPItrans."',
                            userID         = ".$userID."
                            
                        WHERE
                            ruleID= '".$ruleID."' ";

        if(update($updateQuery)){
            //maintainin audit log
            logChangeSet($arrBeforeEdit, $_SESSION["loggedUserData"]["userID"], "complianceLogic", 'ruleID', $ruleID, $edit_log_timeStamp);

            $msg="This Rule Already Exist Just Message,Hold & Document List is Updated";
            $descript="Compliance Rule ( $message ) is updated ";
            activities($_SESSION["loginHistoryID"],"UPDATION",$ruleID,TBL_COMPLIANCE_LOGIC,$descript);
        }
    }
    else
    {
        if($period == 'days' && $setDays !='')
            $period = $period."|".$setDays;
        else
            $period = $period."|";
        $arrBeforeEdit = getRowData("complianceLogic", 'ruleID', $ruleID);

        $autoAuthorize1="";
        if(CONFIG_AUTO_AUTHORIZATION=='1') {
            $autoAuthorize1 = "  autoAuthorize 			= '".$autoAuthorize."',";
        }
        $chk_trans_proceed = isset($_POST['chk_trans_proceed'])? $_POST['chk_trans_proceed'] : '';
        $_SESSION["docCategory"] =($_REQUEST["docCategory"] != "" ? $_REQUEST["docCategory"] : '');
        $updateQuery = " UPDATE 
								 ".TBL_COMPLIANCE_LOGIC."
							 SET
								 applyTrans     = '".$applyAtTrans."', 
								 applyAt        = '".$applyAt."', 
								 userType       = '".$userType."',
								 matchCriteria  = '".$amountCriteria."', 
								 amount         = '".$amount."',
								 message        = '".$message."',
								 doc_provided   = '".serialize($_SESSION["docCategory"])."', 
								 CTR            = '".$ctrChk."', 
								 showGrid       = '".$gridShow."', 
								 isHold         = ".$isHold.","
            .$autoAuthorize1."
								 updated        = '".$date_time."',
								 isForAPItrans  = '".$isForAPItrans."',
								 is_proceed     = '".$chk_trans_proceed."',
								 userID         = ".$userID."
							  WHERE 
							 	 ruleID = '".$ruleID."' ";
        //debug($updateQuery,true);
        if(update($updateQuery))
        {
            logChangeSet($arrBeforeEdit, $_SESSION["loggedUserData"]["userID"], "complianceLogic", 'ruleID', $ruleID, $edit_log_timeStamp);
            $msg="Record Updated Successfully";
            $descript="Compliance Rule ( $message ) is updated ";
            activities($_SESSION["loginHistoryID"],"UPDATION",$ruleID,TBL_COMPLIANCE_LOGIC,$descript);
        }
    }
}


if($submit == "Save" && $flag != 'update')
{
    if($period == 'days' && $setDays !='')
        $period = $period."|".$setDays;
    else
        $period = $period."|";

    if($addedConf["ruleID"]!= '')
    {
        $arrBeforeEdit = getRowData("complianceLogic", 'ruleID', $addedConf["ruleID"]);
        //debug("208");
        $autoAuthorize1="";
        if(CONFIG_AUTO_AUTHORIZATION=='1') {
            $autoAuthorize1 = "  autoAuthorize = '".$autoAuthorize."',";
        }
        $chk_trans_proceed = isset($_POST['chk_trans_proceed'])? $_POST['chk_trans_proceed'] : '';
        $updateQuery = " UPDATE 
								   ".TBL_COMPLIANCE_LOGIC." 
							 SET
									applyTrans     = '".$applyAtTrans."',
									applyAt        = '".$applyAt."',
									userType       = '".$userType."',
									matchCriteria  = '".$amountCriteria."',
									amount         = '".$amount."',
									message        = '".$message."',
									showGrid       = '".$gridShow."',
									isHold         = ".$isHold.",
									".$autoAuthorize1."
									CTR            = '".$ctrChk."',
									updated        = '".$date_time."',
									isForAPItrans  = '".$isForAPItrans."',
									is_proceed	   = '".$chk_trans_proceed."',
									userID         = ".$userID."
							  WHERE  
							  	 	ruleID = '".$addedConf["ruleID"]."' ";
        //echo $updateQuery;
        if(update($updateQuery))
        {
            logChangeSet($arrBeforeEdit, $_SESSION["loggedUserData"]["userID"], "complianceLogic", 'ruleID', $addedConf["ruleID"], $edit_log_timeStamp);
            $msg="This Rule Already Exist and Updated Now.";
            $errorFlag = 'N';
            $descript="Compliance Rule ( $message ) is updated ";
            activities($_SESSION["loginHistoryID"],"UPDATION",$ruleID,TBL_COMPLIANCE_LOGIC,$descript);
        }else{
            $msg="SQL Error:";
            $errorFlag = 'Y';
            die(mysql_error().": ".$updateQuery );
        }
        $msg="This rule already exist please try to add another rule.";
    }
    else
    {
        $autoAuthorizeTitle = "";
        $autoAuthorizeValu="";
        if(CONFIG_AUTO_AUTHORIZATION=='1') {
            $autoAuthorizeTitle = ", autoAuthorize  ";
            $autoAuthorizeValu=", '".$autoAuthorize."' ";
        }
        $Querry_Sqls = "INSERT INTO 
										".TBL_COMPLIANCE_LOGIC."   	
										(
										 applyTrans,
										 applyAt,
										 userType,
										 matchCriteria,
										 amount,
										 message,
										 cumulativeFromDate,
										 cumulativeToDate,
										 dated,
										 CTR,
										 isHold,
										 showGrid,
										 doc_provided,
										 userID,
										 id_expiry,  
										 id_upload,      
										 id_check,        
										 is_proceed,
										 isForAPItrans
										 ".$autoAuthorizeTitle." 

										 )
										 VALUES
										 (
										 '".$applyAtTrans."',
										 '".$applyAt."',
										 '".$userType."',
										 '".$amountCriteria."',
										 '".$amount."',
										 '".$message."',
										 '".$fromDate."',
										 '".$toDate."',
										 '".$date_time."',
										 '".$ctrChk."',
										 ".$isHold.",
										 '".$gridShow."',
										 '".serialize($docCategory)."',
										 ".$userID.",
										 '".$chk_doc_expiry."',
								         '".$chk_doc_upload."',
								         '".$chk_doc_anyone."',
								         '".$chk_trans_proceed."',
										 '".$isForAPItrans."'
										 ".$autoAuthorizeValu.")";
        if(insertInto($Querry_Sqls)){
            $ruleID = @mysql_insert_id();
            $msg="Record added successfully";
            $errorFlag = 'N';

            $descript="Compliance Rule ( $message ) is added ";
            activities($_SESSION["loginHistoryID"],"INSERTION",$ruleID,TBL_COMPLIANCE_LOGIC,$descript);
        }else{
            $msg="SQL Error:";
            $errorFlag = 'Y';
            die(mysql_error().": ".$Querry_Sqls );
        }
    }
}

/**
 * script to check if api transaction compliance check configuration is enabled
 * or not in the database for this client.
 */
$apiRuleApplyQuery = "	SELECT *
							
						FROM 
							setUserRights 
						WHERE 
							rightName = 'API_Transactions_Compliance_Cron' 
					";
$arrAPIRuleData	= selectFrom($apiRuleApplyQuery);
if(count($arrAPIRuleData) > 0){
    if(!empty($arrAPIRuleData['rightStatus']) && $arrAPIRuleData['rightStatus'] == '1'){
        $apiComplianceConf = true;
    }else{
        $apiComplianceConf = false;
    }
}else{
    $apiComplianceConf = false;
}
//echo $chk_trans_proceed;
?>
<html>
<head>
    <title>Compliance Amount Configurations at Create Transaction</title>
    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
    <script src="jquery.cluetip.js" type="text/javascript"></script>
    <script language="javascript" src="javascript/jquery.form.js"></script>

    <link rel="stylesheet" type="text/css" href="css/datePicker.css" />

    <script language="javascript">
        function onChangeUserType(){
            var userType= document.getElementById("userType").value;
            if(userType=='agent' || userType=='distributor'){
                document.getElementById("Passport").checked=false;
                document.getElementById("Visa").checked =false;
                document.getElementById("DRIVER LICENSE").checked=false;
                document.getElementById("OTHER ID CARD").checked=false;
                document.getElementById("RESIDENCE PERMIT").checked=false;
                document.getElementById("Social Security Number").checked=false;
                document.getElementById("sfdsf").checked=false;
                document.getElementById("docCat").style.display="none";

            }else
                document.getElementById("docCat").style.display="";

        }
        function checkForm(theForm)
        {
            if(theForm.userID.value == "" || IsAllSpaces(theForm.userID.value)){
                alert("Please Select Active Agent.");
                theForm.userID.focus();
                return false;
            }
            if(theForm.tType.value == "" || IsAllSpaces(theForm.tType.value)){
                alert("Please Select Transaction Type.");
                theForm.tType.focus();
                return false;
            }
            return true;
        }
        $(document).ready(function()
        {

            $("#applyAtTrans").change(function()
            {
                if($("#applyAtTrans").val() == 'Accumulative Transaction')
                    $("#periodRow").show();
                else
                    $("#periodRow").hide();
            });

            $("#period").change(function()
            {
                var period = $("#period").val();
                if(period == 'days')
                    $("#daysRow").show();
                else
                    $("#daysRow").hide();
            });
            $("#country").change(function()
            {
                var country = $("#country").val()

                if(country == 'United States')
                    $("#ctrRow").show();
                else
                    $("#ctrRow").hide();

                if(country == 'Italy')
                    $("#adeguataRow").show();
                else
                    $("#adeguataRow").hide();

                get_currency($("#country").val(),'originCCY');
            });

            $("#dCountry").change(function(){
                get_currency($("#dCountry").val(),'destCCY');
            });
        });


        function get_currency(countrVal,id)
        {
            $.get("Currency_Compliance_conf_NTM.php",{Country:countrVal},function(data){
                $("#"+id).val(data);
            });
        }

        function SelectOption(OptionListName, ListVal)
        {

            for (i=0; i < OptionListName.length; i++)
            {
                if (OptionListName.options[i].value == ListVal)
                {
                    OptionListName.selectedIndex = i;
                    break;
                }
            }
        }
        function IsAllSpaces(myStr){
            while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
            }
            if (myStr == ""){
                return true;
            }
            return false;
        }

        function test()
        {
            document.genRefNum.action='compliance-amount-configuration.php';
            document.genRefNum.submit();
        }

        /*function hideDocs()
         {

         var docs=document.getElementById("selectDocs");
         if(docs=="specificDocs")
         {
         document.getElementById("docCat").show();

         }
         if(docs=="allDocs")
         {
         document.getElementById('docCat').style.display = 'block';
         }
         }*/
        function autoAuthorize()
        {
            if ($("#autoAuthorizeTrans").attr('checked')) {
                $('#holdTrans').removeAttr('checked')
            }
            if ($("#holdTrans").attr('checked')) {
                $('#autoAuthorizeTrans').removeAttr('checked')
            }

        }
        function autoAuthorize2()
        {

            if ($("#holdTrans").attr('checked')) {
                $('#autoAuthorizeTrans').removeAttr('checked')
            }

        }

    </script>
    <style type="text/css">

        .style1 {color: #005b90;font-weight: bold;}
        .style3 {
            color: #3366CC;
            font-weight: bold;
        }

        .listWidth {
            font-family:verdana;
            font-size: 11px;
            width:200px;
        }

    </style>
</head>

<body>

<form action="compliance-amount-configuration.php" method="post" name="transConf" onSubmit="return checkForm(this);">
    <table width="100%" border="0" cellspacing="1" cellpadding="5">
        <tr>
            <td align="center">
                <table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
                    <tr>
                        <td colspan="2" bgcolor="#000000">
                            <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                <tr>
                                    <td align="center" bgcolor="#DFE6EA">
                                        <font color="#000066" size="2"><strong>Compliance Configurations At Create Transaction</strong></font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php if ($_GET["msg"] != ""){ ?>
                        <tr bgcolor="EEEEEE">
                            <td colspan="2">
                                <table width="100%" cellpadding="5" cellspacing="0" border="0">
                                    <tr>
                                        <td width="40" align="center">
                                            <font size="5" color="<?php echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><?php echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><?php echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ""; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr bgcolor="#ededed">
                        <?php if($ruleID !=''&& $flag == 'update'){ ?>
                            <td align="left"><a href="compliance-view-popup.php"  ><font color="#005b90"><strong>GO Back </strong></font></a></td><?php } ?>
                        <td colspan="2" align="center"><font color="#FF0000">If two or more settings come in same criteria, latest one will be applicable</font></td>
                    </tr>
                    <?php if($msg!= '') { ?>
                        <tr bgcolor="#ededed">
                            <td colspan="2" align="center">
                                <font color="<?php echo ($errorFlag == "N" ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2">
                                    <b><i><?php echo ($errorFlag == "N" ? SUCCESS_MARK : CAUTION_MARK);?></i></b>
                                    <strong><?php echo $msg; ?></strong></font>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php
                        if (!($agentType == "SUPA" || $agentType == "SUBA")) {
                            ?>
                            <tr bgcolor="#ededed">
                                <td class="style1">Active Agents</td>
                                <td>
                                    <select name="userID" id="userID" class="listWidth">
                                        <option value="0">- Select Agent -</option>
                                        <?php
                                        $activeAgents = selectMultiRecords("select userID, name from admin where adminType = 'Agent' and agentStatus = 'Active'");
                                        foreach ($activeAgents as $row) {
                                            echo "<option value='" . $row["userID"] . "'>" . $row["name"] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].userID, "<?php echo $userID?>");
                            </script>
                            <?php
                        }
                    ?>
                    <tr bgcolor="#ededed">
                        <td class="style1">Originating Country</td>
                        <td>
                            <SELECT name="country" id="country" class="listWidth" >
                                <OPTION value="">- Select Country-</OPTION>

                                <?php
                                if(empty($entity_country)){

                                    $countiresOrigin = selectMultiRecords("SELECT 
																			countryName,
																			countryId 
																		FROM 
																			".TBL_COUNTRY." 
																		WHERE  countryType like '%origin%' ORDER BY countryName");
                                }
                                else{
                                    $countiresOrigin = selectMultiRecords("SELECT 
																			countryName,
																			countryId 
																		FROM 
																			".TBL_COUNTRY." 
																		WHERE  countryType like '%origin%' and countryName = '$entity_country' ORDER BY countryName");

                                }
                                for ($i=0; $i < count($countiresOrigin); $i++)
                                {
                                    ?>
                                    <OPTION value="<?php echo $countiresOrigin[$i]["countryId"]; ?>">
                                        <?php echo $countiresOrigin[$i]["countryName"]; ?>
                                    </OPTION>
                                    <?php
                                }
                                ?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].country, "<?php echo $country?>");
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td class="style1">Destination Country</td>
                        <td>
                            <SELECT id="dCountry" name="dCountry" class="listWidth">
                                <OPTION value="">- Select Country-</OPTION>
                                <?php
                                $countires = selectMultiRecords("SELECT 
																	   countryName,
																	   countryId
																	FROM 
																		".TBL_COUNTRY."
																	WHERE 
																		countryType LIKE '%destination%' ORDER BY countryName");
                                for ($i=0; $i < count($countires); $i++)
                                {
                                    ?>						          	<OPTION value="<?=$countires[$i]["countryId"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
                                    <?php
                                }	?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].dCountry, "<?=$dCountry?>");
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td class="style1">Origin Currency</td>
                        <td>
                            <SELECT id="originCCY" name="originCCY" class="listWidth">
                                <?php
                                $countries = selectMultiRecords("SELECT
                                currency
                                FROM
                                ".TBL_COUNTRY."
                                WHERE
                                1");
                                foreach ($countries as $currency) {
                                    echo "<OPTION value=''>".$currency['currency']."</OPTION>";
                                }

                              ?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].originCCY, "<?php echo $originCCY?>");
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor = "#ededed">
                        <td class="style1">Destination Currency</td>
                        <td>
                            <!--<input type="text" name="destCCY" id="destCCY" readonly="readonly" value="<?=$destCCY?>" />
							<div id="destCCY" name="destCCY"></div>-->

                            <SELECT id="destCCY" name="destCCY" class="listWidth">
                                <?php
                                $countries = selectMultiRecords("SELECT
                                currency
                                FROM
                                ".TBL_COUNTRY."
                                WHERE
                                1");
                                foreach ($countries as $currency) {
                                    echo "<OPTION value=''>".$currency['currency']."</OPTION>";
                                }

                                ?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].destCCY, "<?php echo $destCCY?>");
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td class="style1">Transaction Type<font color="#FF0000">*</font></td>
                        <td>
                            <SELECT id="tType" name="tType" class="listWidth">
                                <OPTION value="">- Select Type-</OPTION>
                                <OPTION value="all">- All-</OPTION>
                                <?php
                                if(CONFIG_FOR_CCR_TYPE == '1'){
                                    ?>
                                    <option value="CCR">CCR</option>
                                    <?php
                                }
                                $transType = selectMultiRecords("SELECT	 transactionType, id	
																FROM transactionTypeList
																	WHERE  status = 'Enable' ORDER BY id ASC");

                                for ($i=0; $i < count($transType); $i++)
                                {
                                    ?>
                                    <OPTION value="<?php echo $transType[$i]["transactionType"]; ?>"><?php echo $transType[$i]["transactionType"]; ?></OPTION>
                                    <?php
                                }	?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].tType, "<?php echo $tType?>");
                            </script>
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Apply At</strong></font></td>
                        <td>
                            <select name="applyAt" id="applyAt" class="listWidth" >
                                <option value="Create Transaction">Create Transaction</option>
                                <?php if(CONFIG_COMPLIANCE_RULE_CONFIRM_TRANSACTION == "1")
                                {?>
                                    <option value="Confirm Transaction">Confirm Transaction</option>
                                    <?php
                                } ?>
                            </select>
                        </td>
                    </tr>
                    <script language="JavaScript">
                        SelectOption(document.forms[0].applyAt, "<?php echo $applyAt ?>");
                    </script>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Apply At Transaction</strong></font></td>
                        <td>
                            <select name="applyAtTrans" id="applyAtTrans" class="listWidth">
                                <option value="Current Transaction">Current Transaction</option>
                                <option value="Accumulative Transaction">Accumulative Transaction</option>
                            </select>
                        </td>
                    </tr>
                    <script language="JavaScript">
                        SelectOption(document.forms[0].applyAtTrans, "<?php echo $applyAtTrans ?>");
                    </script>
                    <?php
                    $periodShow = 'style="display:none"';
                    if($applyAtTrans == 'Accumulative Transaction')
                        $periodShow = 'style=display:""';
                    else
                        $periodShow = 'style="display:none";';
                    ?>

                    <tr bgcolor="#ededed" id="periodRow" <?=$periodShow;?>>
                        <td><font color="#005b90"><strong>Period</strong></font></td>
                        <td>
                            <select name="period" id="period" class="listWidth">
                                <option value="">- Select Period -</option>
                                <option value="annualy">Annualy</option>
                                <option value="monthly">Monthly</option>
                                <option value="quarterly">Quarterly</option>
                                <option value="weekly">Weekly</option>
                                <option value="daily">Daily</option>
                                <option value="days">Days</option>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].period, "<?php echo $period?>");
                            </script>
                        </td>
                    </tr>
                    <?php
                    $daysSelected = '';
                    if($period == 'days')
                    {
                        $daysSelected = 'style=display:""';
                    }
                    else
                    {
                        $daysSelected = 'style="display:none";';
                    }
                    ?>
                    <tr bgcolor="#ededed" id="daysRow" <?php echo $daysSelected;?>>
                        <td><font color="#005b90"><strong>Days</strong></font></td>
                        <td><input name="setDays" id="setDays"  value="<?php echo stripslashes($setDays);?>" size="10" /></td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>User Type</strong></font></td>
                        <td>
                            <select name="userType" id="userType" class="listWidth" onchange="onChangeUserType()" >
                                <!--<option value="">- Select User -</option>-->
                                <option value="sender"> Sender </option>
                                <option value="beneficiary"> Beneficiary </option>
                                <option value="agent">Agent</option>
                                <option value="distributor"> Distributor </option>
                            </select>
                        </td>
                        <script language="JavaScript">
                            SelectOption(document.forms[0].userType, "<?php echo $userType ?>");
                        </script>
                    </tr>
                    <?php
                    $ctrSelected = '';
                    $ctrChecked= '';
                    if($country == 'United States' && $ctrChk == '1')
                    {
                        $ctrSelected = 'style=display:"";';
                        $ctrChecked = 'checked="checked"';
                    }
                    else
                    {
                        $ctrSelected = 'style="display:none";';
                        $ctrChecked = '';
                    }

                    $ctrSelected2 = '';
                    $ctrChecked2= '';
                    if($country == 'Italy' && $ctrChk == 'adeguata')
                    {
                        $ctrSelected2 = 'style="display:";';
                        $ctrChecked2 = 'checked="checked"';
                    }
                    else
                    {
                        $ctrSelected2 = 'style="display:none";';
                        $ctrChecked2 = '';
                    }
                    ?>
                    <tr bgcolor="#ededed" id="ctrRow" <?php echo $ctrSelected;?>>
                        <td><font color="#005b90"><strong>CTR</strong></font></td>
                        <td><input type="checkbox" name="ctrChk" id="ctrChk" value="1"  <?php echo $ctrChecked;?>/></td>
                    </tr>
                    <tr bgcolor="#ededed" id="adeguataRow" <?php echo $ctrSelected2;?>>
                        <td><font color="#005b90"><strong>Adeguata Verifica</strong></font></td>
                        <td><input type="checkbox" name="ctrChk" id="ctrChk" value="adeguata" <?php echo $ctrChecked2;?> /></td>
                    </tr>
                    <?php
                    if(CONFIG_COMPLIANCE_NUM_OF_DAYS == "1")
                    { ?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>From Date</strong></font></td>
                            <td><input name="fDate" type="text" id="fDate" value="<?php echo stripslashes($fDate); ?>" readonly>&nbsp;<a href="javascript:show_calendar('transConf.fDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;&nbsp;
                                <font color="#005b90"><strong>To Date</strong></font>&nbsp;&nbsp;
                                <input name="tDate" type="text" id="tDate" value="<?php echo stripslashes($tDate); ?>" readonly>&nbsp;<a href="javascript:show_calendar('transConf.tDate');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a></td>
                        </tr>
                        <?php
                    } ?>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Amount Criteria</strong></font></td>
                        <td>
                            <select name="amountCriteria" id="amountCriteria" class="listWidth">
                                <option value="==">Equal To</option>
                                <option value="<">Less Than</option>
                                <option value=">">Greater Than</option>
                                <option value="<=">Less Than or Equal To</option>
                                <option value=">=">Greater Than or Equal To</option>
                                <option value="!=">Not Equal To</option>
                                <option value="BETWEEN">BETWEEN</option>
                            </select>&nbsp;<br><font color="#FF0000">Note: if Criteria BETWEEN then amount separte by - i.e. 2000-5000 </font>
                        </td>
                    </tr>
                    <script language="JavaScript">
                        SelectOption(document.forms[0].amountCriteria, "<?php echo $amountCriteria ?>");
                    </script>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Amount</strong></font></td>
                        <td>
                            <input type="text" name="amount" value="<?php echo stripslashes($amount); ?>" >&nbsp;&nbsp;
                            Apply On:
                            <select name="amountType" id="amountType" class="listWidth">
                                <option value="trans" selected="selected">Transaction Amount</option>
                                <option value="destination">Destination Amount</option>
                                <option value="total">Total Amount</option>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].amountType, "<?php echo $amountType ?>");
                            </script>
                        </td>
                    </tr>

                    <tr bgcolor="#ededed" id="docCat" >
                        <td><font color="#005b90"><strong>Document Category</strong></font></td>
                        <td >
                            <?php
                            $categoryQuery = "select id,title from id_types WHERE active = 'Y' ";
                            $categoryData = selectMultiRecords($categoryQuery);

                            for ($i=0; $i < count($categoryData); $i++)
                            {
                                $categoryName = $categoryData[$i]["title"];
                                echo "<span>".$categoryName;
                                ?>
                                <input type="checkbox" name="docCategory[<?php echo $categoryData[$i]["id"]?>]" id="<?php echo $categoryName?>" value="Y"
                                    <?php echo ($_SESSION["docCategory"][$categoryData[$i]["id"]]) == "Y"? 'checked="checked"':''?> /> </span>&nbsp;
                                <?php if($i == 2) { ?> <br> <?php } ?>
                                <?php
                            } ?>
                            <br />
                            <font color="#FF0000">Note: if category box is checked then it will mandatory otherwise non mandatroy</font>
                        </td>
                    </tr>

                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Message</strong></font></td>
                        <td>
                            <TEXTAREA Name="message" rows="5" cols="30"><?php echo stripslashes($message); ?></TEXTAREA><br />
                            Show Message: <input type="checkbox" name="show_msg" id="show_msg" <? echo ($gridShow == "Y"?'checked="checked"':"")?> />
                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Hold Transaction</strong></font></td>
                        <td>

                            <input type='checkbox'  name='holdTrans' id="holdTrans" <? echo ($isHold == "Y"?'checked="checked"':"")?> onClick="autoAuthorize2();" />

                        </td>
                    </tr>

                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Transaction Proceed</strong></font>
                            <img src="images/info.gif" border="0"
                                 title="Transaction Proceed | if transction proceed box is checked then then proceed button appear at NTM  otherwise not" /></td>
                        <td>
                            <?php
                            $chk_trans_proceed_checked = '';
                            if($chk_trans_proceed == "Y")
                                $chk_trans_proceed_checked = 'checked="checked"';
                            ?>
                            <input type="checkbox" name="chk_trans_proceed" id="chk_trans_proceed" value="Y"  <?php echo $chk_trans_proceed_checked;?> />


                        </td>
                    </tr>
                    <tr bgcolor="#ededed">
                        <td><font color="#005b90"><strong>Transaction Auto Authorization</strong></font>
                            <img src="images/info.gif" border="0"
                                 title="Transaction Proceed | if transction proceed box is checked then then proceed button appear at NTM  otherwise not" /></td>
                        <td>
                            <!--                            --><?php
                            //                            $chk_trans_proceed_checked = '';
                            //                            if($chk_trans_proceed == "Y")
                            //                                $chk_trans_proceed_checked = 'checked="checked"';
                            //                            ?>
                            <input type="checkbox" name="chk_trans_proceed" id="chk_trans_proceed" value="Y"  <?php //echo $chk_trans_proceed_checked;?> />


                        </td>
                    </tr>
                    <?php if(CONFIG_AUTO_AUTHORIZATION=='1') {?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>Transaction Auto Authorization</strong></font>
                                <img src="images/info.gif" border="0" title="Transaction Auto Authorization | if transaction hold box is checked then transaction will hold" /> </td>
                            <td>
                                <input type='checkbox'  name='autoAuthorizeTrans' id="autoAuthorizeTrans" <? echo ($autoAuthorize == "Y"?'checked="checked"':"")?> onClick="autoAuthorize();" />

                            </td>
                        </tr>
                    <?php } ?>
                    <?php
                    if($apiComplianceConf == true){
                        ?>
                        <tr bgcolor="#ededed">
                            <td><font color="#005b90"><strong>Apply At API Transactions</strong></font><img src="images/info.gif" border="0" title="If checked, the rule will be implemented at API's transactions as well otherwise this rule will work only at the transactions created through PAYEX system" /></td>
                            <td><input type="checkbox" name="isForAPItrans" value="1" <?php if($isForAPItrans == "1"){ echo "checked";} ?> /></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <input type=hidden name=ruleID value="<?php echo ($_REQUEST["ruleID"] !='' ? $ruleID:"")?>">
                    <input type=hidden name=flag value="<?php echo ($_REQUEST["flag"] !='' ? $flag : "")?>">
                    <tr bgcolor="#ededed">
                        <td colspan="2" align="center">
                            <input type="submit" name="submit" value="Save">
                            <input type="reset" name="clear" value="Clear">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>