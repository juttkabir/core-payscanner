<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];

if($agentType == "TELLER"){
	$agentID = $_SESSION["loggedUserData"]["cp_ida_id"];
	$agentparentID = $_SESSION["loggedUserData"]["cp_ida_id"];
}
else{
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$agentparentID = $_SESSION["loggedUserData"]["userID"];
}


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;



$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	



$_SESSION["transType"] = $_POST["transType"];
$_SESSION["transID"] = $_POST["transID"];
$_SESSION["searchBy"] = $_POST["searchBy"];

if($_GET["submit"]!="Search")
{
	session_register("fMonth");
	session_register("fDay");
	session_register("fYear");
	session_register("tMonth");
	session_register("tDay");
	session_register("tYear");
	
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
}



/*$sambaCheck = selectMultiRecords("select * from sambaida_list");
for($i=0;$i<count($sambaCheck);$i++)
{
	if($i == 0)
		$ida_id = $sambaCheck[$i]["ida_id"];
	if($i == 1)	
		$ida_id2 = $sambaCheck[$i]["ida_id"];
}

$sambaCheck = selectFrom("select * from admin where username = 'SAMBAAPEX'");
$apex_userid = $sambaCheck["userID"];

$sambaCheck = selectFrom("select * from admin where username = 'SAMBARURAL'");
$rural_userid = $sambaCheck["userID"];
					
$sambaCheck = selectFrom("select * from sambaida_check");
if($sambaCheck["IDA_id"] == $agentID)
{
	$OR = "OR t.benAgentID = '$ida_id' OR t.benAgentID = '$ida_id2'";
}
elseif($agentID == $apex_userid)
{
	$OR = "OR t.benAgentID = '$rural_userid'";
}
else*/
	$OR = "";


if($agentType == "admin" || $agentType == "Call")
{
	$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where 1 ";
}
elseif($agentType == "SUPI" || $agentType == "SUPAI")
{
	$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where (t.transStatus = 'Picked up' OR  t.transStatus='Dispatched' OR t.transStatus='Revoked') and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID' $OR) ";
}
else
{
	$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.benAgentID = '$agentID' ";
}
if($_GET["act"]=="edit")
{
	if($agentType == "SUPI" || $agentType == "SUPAI")
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='Edited' and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID' $OR) ";
	}
	else
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='Edited' and t.benAgentID = '$agentID' ";
	}

}
if($_GET["act"]=="amend")
{
	if($agentType == "SUPI" || $agentType == "SUPAI")
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='Amended' and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID' $OR) ";
	}
	else
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='Amended' and t.benAgentID = '$agentID' ";
	}

}
if($_GET["act"]=="hold")
{
	if($agentType == "SUPI" || $agentType == "SUPAI")
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='Hold' and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID' $OR) ";
	}
	else
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.customerID =  c.customerID and t.transStatus='Hold' and t.benAgentID = '$agentID' ";
	}

}
if($_GET["act"]=="cancel")
{
	if($agentType == "SUPI" || $agentType == "SUPAI")
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='AwaitingCancellation' and (t.benAgentParentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.benAgentID = '$agentID' $OR) ";
	}
	elseif($agentType == "admin" || $agentType == "Call")
	{
		$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t where t.transStatus='AwaitingCancellation' ";
	}
	else
	{
		$query .= " and t.benAgentID = '$agentID'"; 
	}
}


if($_POST["Submit"] =="Search" || $_GET["submit"]=="Search")
{
	
	if($_GET["submit"]!="Search")
{	
	if($_POST["fMonth"]!="")
		$_SESSION["fMonth"]=$_POST["fMonth"];
	if($_POST["fDay"]!="")
		$_SESSION["fDay"]=$_POST["fDay"];
	if($_POST["fYear"]!="")
		$_SESSION["fYear"]=$_POST["fYear"];
	if($_POST["tMonth"]!="")
		$_SESSION["tMonth"]=$_POST["tMonth"];
	if($_POST["tDay"]!="")
		$_SESSION["tDay"]=$_POST["tDay"];
	if($_POST["tYear"]!="")
		$_SESSION["tYear"]=$_POST["tYear"];
}

	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
//echo $fromDate;
if($fromDate <= $toDate)
	$dated .= " and (t.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";

	
	
	
	$search = "Search";
	if($_POST["transType"] != "")
	{
		$type = $_POST["transType"];
		$query .= " and t.transType='".$_POST["transType"]."'";
	}elseif($_GET["transType"] != "")
	{	
		$type = $_GET["transType"];
		$query .= " and t.transType='".$_GET["transType"]."'";
	}
	
	if($_POST["transID"] != "" || $_GET["transID"] != "")
	{
		if($_POST["transID"] != "")
			{
			
			$id = $_POST["transID"];
			}else{
			$id = $_GET["transID"];
			}
		if($_POST["searchBy"] != "")
			{
			$by = $_POST["searchBy"];
			}else{
			$by = $_GET["searchBy"];
			}
		}
	
	if($id != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID =  b.benID and (t.transStatus='Picked up' OR t.transStatus='Credited' OR  t.transStatus='Dispatched' OR t.transStatus='Revoked') and t.benAgentID = '$agentID' and  (b.IDNumber = '$id' OR b.otherId = '$id') and t.createdBy != 'CUSTOMER' ";
				
				$queryonline = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where t.benID =  cb.benID and (t.transStatus='Picked up' OR t.transStatus='Credited') and t.benAgentID = '$agentID' and cb.NICNumber = '$id' and t.createdBy = 'CUSTOMER' ";
				$queryonline .= $dated;
				if ($type !=""){
				$queryonline .= " and t.transType='".$type."'";
			}
				$queryonline .= "  order by t.transDate DESC";
				
				$countOnlineRec = count($onlinecustomer2 = selectMultiRecords($queryonline));
			//echo("Count got is  ->".$countOnlineRec."--");
			if($offset < $countOnlineRec)
			{
				$queryonline .= " LIMIT $offset , $limit";
				
				 $onlinecustomer = selectMultiRecords($queryonline);
			}
				
				
				break;
			}
			case 1:
			{
				$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID =  b.benID and (t.transStatus='Picked up' OR t.transStatus='Credited' OR  t.transStatus='Dispatched' OR t.transStatus='Revoked') and t.benAgentID = '$agentID' and  (b.Phone = '$id' OR b.Mobile = '$id') and t.createdBy != 'CUSTOMER' ";
				
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where t.benID =  cb.benID and (t.transStatus='Picked up' OR t.transStatus='Credited') and t.benAgentID = '$agentID' and (cb.Phone = '$id' OR cb.Mobile = '$id') and t.createdBy = 'CUSTOMER' ";
				$queryonline .= $dated;
				if ($type !=""){
				$queryonline .= " and t.transType='".$type."'";
			}
				$queryonline .= "  order by t.transDate DESC";
				
				$countOnlineRec = count($onlinecustomer2 = selectMultiRecords($queryonline));
			//echo("Count got is  ->".$countOnlineRec."--");
			if($offset < $countOnlineRec)
			{
				$queryonline .= " LIMIT $offset , $limit";
				
				 $onlinecustomer = selectMultiRecords($queryonline);
			}
				
				
				break;
			}
			case 2:
			{
				$query = "select transID,transDate,refNumberIM,refNumber,transStatus,totalAmount,t.customerID,t.benID  from ". TBL_TRANSACTIONS . " as t, ". TBL_BENEFICIARY ." as b where t.benID =  b.benID and (t.transStatus='Picked up' OR t.transStatus='Credited' OR  t.transStatus='Dispatched' OR t.transStatus='Revoked') and t.benAgentID = '$agentID' and  b.lastName like '$id%' and t.createdBy != 'CUSTOMER' ";
				
				$queryonline = "select *  from ". TBL_TRANSACTIONS . " as t, ".TBL_CM_BENEFICIARY." as cb where t.benID =  cb.benID and (t.transStatus='Picked up' OR t.transStatus='Credited') and t.benAgentID = '$agentID' and cb.lastName like '$id%' and t.createdBy = 'CUSTOMER' ";
				$queryonline .= $dated;
				if ($type !=""){
				$queryonline .= " and t.transType='".$type."'";
			}
				$queryonline .= "  order by t.transDate DESC";
				
				$countOnlineRec = count($onlinecustomer2 = selectMultiRecords($queryonline));
			//echo("Count got is  ->".$countOnlineRec."--");
			if($offset < $countOnlineRec)
			{
				$queryonline .= " LIMIT $offset , $limit";
				
				 $onlinecustomer = selectMultiRecords($queryonline);
			}
				
				
				break;
			}
					
		}
		
	}
	
	

}else{
		if($_GET["act"]=="")
			$query .= " and (t.transStatus='Picked up' ) ";
	}
	$query .= $dated;
	if ($type !=""){
	$query .= " and t.transType='".$type."'";
}
	$query .= "  order by t.transDate DESC";

$totalContent = selectMultiRecords($query);
//echo(" Agents are -->".count($totalContent)."--");
 $allCount = count($totalContent)+ $countOnlineRec;
 $other = $offset + $limit;
 if($other > $countOnlineRec)
 {
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
 $query .= " LIMIT $offset2 , $limit2";
  if($_POST["Submit"]!= ""  ){
  	
  	if($_POST["searchBy"] != "" && $_POST["transID"] != ""){
  	
							$contentsTrans = selectMultiRecords($query);
		}elseif($_POST["searchBy"] == "" && $_POST["transID"] == ""){
			
							$contentsTrans = selectMultiRecords($query);
			
			
			}
		
}
 }

//$contentsTrans = selectMultiRecords($query);

//echo $query;
?>
<html>
<head>
	<title>Release Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Following Transaction 
      are available<? if($_GET["act"]=="cancel"){echo(" for Cancellation.");} else{echo(" for Release.");}?> </font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <br>
      <table width="671" border="1" cellpadding="5" bordercolor="#666666">
        <form action="old-trans-search.php" method="post" name="Search">
          <tr>
            <td width="387" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
			
          </tr>
          <tr>
            <td nowrap> Search Transaction
                <input name="transID" type="text" id="transID" value="<? echo $_SESSION["transID"]?>">
                <!--select name="transType" style="font-family:verdana; font-size: 11px; width:100">
                  <option value=""> - Type - </option>
                  <option value="Pick up">Pick up</option>
                  <option value="Bank Transfer">Bank Transfer</option>
                  <option value="Home Delivery">Home Delivery</option>
                </select>
                <script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$_SESSION["transType"]; ?>");</script-->
			             
				<select name="searchBy" >
				<option value=""> - Search By - </option>
                <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Beneficiary ID</option>
                <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Phone Number</option>
                <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Last Name</option>
                
				  </select>
                <script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$_SESSION["searchBy"]; ?>");</script>
				<input type="submit" name="Submit" value="Search">
				</td>
			</tr>	
			
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                <br>       
        

        </td>
      </tr>
      
      
      
      
      
        </form>
      </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
          <?  if($_GET["message"]!=""){?>
          <tr> 
            <td height="25" nowrap  bgcolor="#FFFFCC"><font color="#FF0000"><strong>&nbsp;<? echo $_GET["message"];?></strong></font></td>
          </tr>
          <? }?>
          <tr>
    		<td>
    			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="250">
                    <?php if (count($contentsTrans) > 0 || count($onlinecustomer) > 0) {?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans)+count($onlinecustomer));?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&submit=$search&transID=$id&searchBy=$by&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&submit=$search&transID=$id&searchBy=$by&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  				
                <?php } ?>
                <?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&submit=$search&transID=$id&searchBy=$by&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transID=$id&searchBy=$by&submit=$search&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 					
                  <?php } 
                	}
                  ?>
                </tr>
              </table>
    		</td>
    	</tr>
          <tr> 
            <td height="25" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo(count($contentsTrans)+ count($onlinecustomer));?> records to <? echo ($_GET["action"] != "" ? ucfirst($_GET["action"]) : "Manage")?>.</strong></span></td>
          </tr>
          <?
		if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
		{
		?>
          <tr> 
            <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
                <tr bgcolor="#FFFFFF"> 
                  <td><span class="style1">Date</span></td>
                  <td><span class="style1"><? echo $systemCode; ?> </span></td>
                  <td><span class="style1"><? echo $manualCode; ?></span></td>
                  <td><span class="style1">Status</span></td>
                  <td width="100" bgcolor="#FFFFFF"><span class="style1">Tot Amount 
                    </span></td>
                  <td width="100"><span class="style1">Sender Name</span></td>
                  <td width="100"><span class="style1">Beneficiary Name</span></td>
                  <td align="center">&nbsp; </td>
                </tr>
                
               <? for($i=0;$i<count($onlinecustomer);$i++)
			{
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="100" bgcolor="#FFFFFF"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumber"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["totalAmount"]?></td>
                  
                  
                  	<? 
                    $transBene = selectFrom("select * from cm_beneficiary  where benID='".$onlinecustomer[$i]["benID"]."'");
				 						$benefName = $transBene["firstName"];
				 					  $benefName .= " ";
									  $benefName .= $transBene["lastName"];
									  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
                   $customerContent = selectFrom("select firstName, lastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="release-trans-details.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&act=<? echo $_GET["act"]?>&id=<?=$id?>&back=release-trans&type=<?=$type?>&by=<?=$by?>"><strong><font color="#006699">View 
                    Details</font></strong></a> </td>
                  
                  
                
                </tr>
                <?
			}
			?> 
                
                
                <? for($i=0;$i<count($contentsTrans);$i++)
			{
				
				
					
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
																			
									}
				
				?>
                <tr bgcolor="#FFFFFF"> 
                  <td width="100" bgcolor="#FFFFFF"><strong><font color="<? echo $fontColor ?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
                  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"]?></td>
                  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER") {
                    $transBene = selectFrom("select * from cm_beneficiary  where benID='".$contentsTrans[$i]["benID"]."'");
				 						$benefName = $transBene["firstName"];
				 					  $benefName .= " ";
									  $benefName .= $transBene["lastName"];
									  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
                   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="release-trans-details.php?transID=<? echo $contentsTrans[$i]["transID"]?>&act=<? echo $_GET["act"]?>&id=<?=$id?>&back=release-trans&type=<?=$type?>&by=<?=$by?>"><strong><font color="#006699">View 
                    Details</font></strong></a> </td>
                  
                  
                <? }else {  
								  $transBene = selectFrom("select * from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
								  $benefName = $transBene["firstName"];
								  $benefName .= " ";
								  $benefName .= $transBene["lastName"];
								  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
                  <? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");?>
                  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                  <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><a href="release-trans-details.php?transID=<? echo $contentsTrans[$i]["transID"]?>&act=<? echo $_GET["act"]?>&id=<?=$id?>&back=release-trans&type=<?=$type?>&by=<?=$by?>"><strong><font color="#006699">View 
                    Details</font></strong></a> </td>
                    <? } ?>
                </tr>
                <?
			}
			?>
                <tr bgcolor="#FFFFFF"> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="100" align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
                </tr>
                <?
			} // greater than zero
			?>
              </table></td>
          </tr>
        </form>
      </table></td>
	</tr>
</table>
</body>
</html>