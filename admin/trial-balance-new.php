<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	$agentType = getAgentType();
	include ("javaScript.php");
	//$currentdate = date('d-m-Y  h:i:s A');
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
    $print = $_REQUEST["print"];
	
//	debug($_REQUEST);
	
		//debug($_REQUEST,true);
		$strAccountChart = "SELECT 
							   id,
							   accountName,
							   accountNumber,
							   description,
							   currency 
						  FROM 
							 accounts_chart
						  WHERE 
						  	status = 'AC'
						  ";
			         //debug($strAccountChart);
					$accountChartRs = selectMultiRecords($strAccountChart);
					

	
		// get total GBP of stock amount
		$total_stock_amount = number_format(getTotalStockAmount(),2,'.',',');

   	if(!empty($_REQUEST["date_report"]) && !empty($_REQUEST["Submit"]))
	{
		//debug($_REQUEST["date_report"]);21/02/2012
		$qrDate=explode("/",$_REQUEST["date_report"]);
			$date_report	= $qrDate[2] . "-" . $qrDate[1] . "-" . $qrDate[0];
				
		//debug($date_report);
		$queryDated = " and dated <= '$date_report' ";
		$qrCreationDate=" and created <= '$date_report 23:59:59'";	
	}
	   
	
	$curr_exch_acc = '';
	if(defined("CONFIG_CURRENCY_EXCHANGE_ACCOUNT"))
		$curr_exch_acc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Trial Balance</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
.ce{
	color:#0066FF;
	cursor:pointer;
}
</style>
<script src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
		$("#date_report").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
	});
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function popup(url) 
{
 var width  = 800;
 var height = 800;
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=yes';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}

function exportTrialBal(){
	var exportFormat 	= document.getElementById("exportFormat").value;
	
	if(exportFormat!="")
	{
		window.open('exportTrialBalance.php?exportType='+exportFormat+'&date_report='+'<?=$date_report?>','Export Trail Balance', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420');
	}
	else
	{
		alert("Please Select export type");
		return false;	
	}
}
function hideDivs(){
document.getElementById("printRow").style.display="none";
//document.location.reload();
}

// -->

</script>
</head>
<body>

<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<TD width="40%"  height="60" align="right" valign="middle"><img id=Picture2 height="60" alt="" src="<?=CONFIG_LOGO?>" width="60" border=0></TD>
					<td align="left" class="reportHeader"><?=SYSTEM?> (TRAIAL BALANCE AS ON)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span>
					</td>
				 </tr>
			</table>
		</td>
	</tr>
<form action="" method="post" name="searchForm">
<tr>
	<td valign='top'>
		<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">			 
			<tr>
				<td align="left"><b>Report Date:</b> </td>
				
				<td> <input type="text" name="date_report" id="date_report" value="<?=$_REQUEST["date_report"]?>" /></td>
				
			</tr>
			<br />
			<tr>
				<td align="center" colspan="4">
				<input type="submit" name="Submit" value="Search">							
				</td>
			</tr>
		</table>
	</td>
</tr>
</form>
			
<form action="" method="post" name="searchForm">
<?
			if(!empty($accountChartRs)){ ?>
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
<tr>
	<td>
		<br />
		
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td width="10%" class="columnTitle">&nbsp;</td>
				<td width="25%" class="columnTitle">&nbsp;</td>
				
				<td colspan="2" width="30%" align="left" class="columnTitle">Amount in Operational Currency </td>
			</tr>
			<tr>
				<td width="10%" class="columnTitle">&nbsp;A/C</td>
				<td width="25%" class="columnTitle">&nbsp;CONTROL NAME </td>
				
				<td width="15%" align="left" class="columnTitle">&nbsp;Dr. </td>
				<td width="15%" align="left" class="columnTitle">&nbsp;Cr. </td>
				
			</tr>
			<tr>
				<td align="left" colspan="4">&nbsp;</td>
			</tr>
			<?php 
			   // debug(count($accountChartRs));
			   $drSum = 0;
			   $crSum = 0;
			   $drSumF = 0;
			   $crSumF = 0;
			  
				for($i=0; $i < count($accountChartRs); $i++){
				 $accDrBal =0;
				 $accCrBal=0;
				 $strAccount = "SELECT 
								   id,
								   accounNumber,
								   accountName,
								   currency,
								   balanceType,
								   balance 
							  FROM 
								 accounts
							  WHERE 
								status = 'AC' AND
								accounType = '".$accountChartRs[$i]["accountNumber"]."' ".$qrCreationDate."";
				// AND	currency = 'GBP'			  
			  //debug($accCrBal);
				 $accountRs = selectMultiRecords($strAccount);
				
			?>
		
			
			<? if(count($accountRs) > 0 ) { ?>
			<!--<tr><td colspan="4">&nbsp;</td></tr>-->
			<? 
			for($k=0; $k < count($accountRs); $k++){ 
				$drBalance = 0;
				$crBalance = 0;
				$drBalance1 = 0;
				$crBalance1 = 0;
			   $date_report ='';
			   $date_to ='';
			   /*$accDrBal=0; //add all acc dr amount 
			   $accCrBal=0; //add all acc cr amount*/
			   $strClosingBalance = "
									SELECT 
										id,
										closing_balance,
										dated,
										accountNumber 
									FROM 
										".TBL_ACCOUNT_SUMMARY." 
									WHERE
										accountNumber = '".$accountRs[$k]["accounNumber"]."'
										
										AND
										dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
										WHERE accountNumber = '".$accountRs[$k]["accounNumber"]."' ".$queryDated." )";
										
				

					

					
					$closingBalance = selectFrom($strClosingBalance);
					
					//debug($strClosingBalance);
				
			
				if($accountRs[$k]['balanceType'] == 'Dr')
					$drBalance =$closingBalance["closing_balance"] - $accountRs[$k]['balance'];
				elseif($accountRs[$k]['balanceType'] == 'Cr')
					$crBalance =$closingBalance["closing_balance"] + $accountRs[$k]['balance'];
//			debug($closingBalance["closing_balance"]."--->".$accountRs[$k]['balance']);	
			if($accountRs[$k]["currency"] == "GBP"){	  
			 if($drBalance < 0)
				$drSum += $drBalance;
			 if($drBalance > 0)	
				$crSum += $drBalance;
			 if($crBalance < 0)
				$drSum += $crBalance;
			 if($crBalance > 0)	
				$crSum += $crBalance;	
			}else{ 
				if($drBalance < 0)
					$drSumF += $drBalance;
				if($drBalance > 0)	
					$crSumF += $drBalance;
				if($crBalance < 0)
					$drSumF += $crBalance;
				if($crBalance > 0)	
					$crSumF += $crBalance;
				$strCurrency=$accountRs[$k]["currency"];	
				$cur_sql="select cID from currencies where currencyName ='$strCurrency'";
				$buying_rate_arr_id = selectFrom($cur_sql); 
				$cur_sql="select buying_rate from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."' and created_at = (select max(created_at) from curr_exchange where buysellCurrency = '".$buying_rate_arr_id[0]."')";
				//debug($cur_sql);
				$buying_rate_arr = selectFrom($cur_sql); 
				//debug($buying_rate_arr);
				$buying_rate=$buying_rate_arr[0];
			}
			$show_total_amount = '';
			if($accountRs[$k]["accounNumber"] == $curr_exch_acc)
				$show_total_amount = '<strong>Stock='.$total_stock_amount.'<strong>';

			 if(!empty($closingBalance["dated"]))
				$date_report = date("Y-m-d", strtotime($closingBalance["dated"]));
			?>
							
				
				<?php 
				 $BALANCE = "0.00";
				if($accountRs[$k]["currency"] == "GBP"){
					if($drBalance<0 ){
						$BALANCE = $drBalance; //debug($BALANCE);
					}
					else if($crBalance<0){
						$BALANCE =$crBalance; //debug($BALANCE);
					}
				}else{
					if($drBalance<0 ){
						$drBalance1=$drBalance/$buying_rate;
						$BALANCE = $drBalance1; //debug($BALANCE);
					}
					else if($crBalance<0){
						$crBalance1=$crBalance/$buying_rate;
						$BALANCE =$crBalance1; //debug($BALANCE);
					}
					 
				} 
				//debug($BALANCE);
				$accDrBal+=number_format($BALANCE, 2, ".", "");
				?>
				<?php 
				$res = selectFrom("select * from account_summary");
				
				 $BALANCE = "0.00";
				if($accountRs[$k]["currency"] == "GBP"){
					if($drBalance>0 ){ 
						 $BALANCE =$drBalance; 
						 //debug($BALANCE);
						}
					else if($drBalance == 0 && $crBalance>0 ){
						 $BALANCE =$crBalance; //debug($BALANCE);
						}
				}else{
					if($drBalance>0 ){
						$drBalance1=$drBalance/$buying_rate;
						$BALANCE =$drBalance1; //debug($BALANCE);
						}
					else if($drBalance == 0 && $crBalance>0 ){
						$crBalance1=$crBalance/$buying_rate;
						$BALANCE =$crBalance1; //debug($BALANCE);
						}
						 if($drBalance < 0)
							$drSum += $drBalance1;
						 if($drBalance > 0)	
							$crSum += $drBalance1;
						 if($crBalance < 0)
							$drSum += $crBalance1;
						 if($crBalance > 0)	
							$crSum += $crBalance1;
				}
				//debug("a".$BALANCE);
				$accCrBal+=number_format($BALANCE, 2, ".", "");
				
				?>
			<? } ?>
			<tr><td colspan="4">&nbsp;</td></tr>
			<?
			   }
			   //Returns a positive value if a > b, zero if a = b and a negative value if a < b.
			  // $Rzt=gmp_cmp(abs($accCrBal),abs($accDrBal));
			  
			  $drSide=number_format(abs($accDrBal), 2, ".", "");
			  $crSide=number_format($accCrBal, 2, ".", "");
			 /* $drSide=abs($accDrBal);
			  $crSide=$accCrBal;*/
			  $Rzt=$drSide-$crSide;
				//debug($accDrBal."-".$accCrBal);
				//debug($drSide."-".$crSide."==".$Rzt);
				if($Rzt>0)
				{
					$drSide=$Rzt;
					$crSide=0;
				}
				elseif($Rzt<0)
				{
					$drSide=0;
					$crSide=$Rzt;
				}
				else
				{
					$drSide=0;
					$crSide=0;
				}
			  //$largerSide=$accCrBal-$accDrBal;
			 
				?>
		
				<tr>
				<td  width="10%" align="left" class="heading">&nbsp;<?=$accountChartRs[$i]["accountNumber"]?></td>
				<td width="25%" align="left" class="heading">&nbsp;<?=$accountChartRs[$i]["accountName"]?></td>					
								
				<td align="left" class="heading"><?=number_format(abs($drSide), 2, ".", ",");?></td>
				<td align="left" class="heading"><?=number_format(abs($crSide), 2, ".", ",");?></td>
			</tr> 
		<?php 	 
				$drSumT+=$drSide;
				$crSumT+=$crSide;
		} ?>
			 
			 
			<tr>
				<td align="left" colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="heading">&nbsp; TOTAL </td>
			
				<td align="left" class="heading"><?=number_format(abs($drSumT),2, ".", ",")?></td>
				<td align="left" class="heading"><?=number_format(abs($crSumT),2, ".", ",")?></td>
				
			</tr>
			<tr>
				<td align="left" colspan="4">&nbsp;</td>
			</tr>
			<tr id="printRow">
				<td>&nbsp;</td>
				<td class="heading">&nbsp;  Select Format:&nbsp;&nbsp;
				<select name="exportFormat" id="exportFormat">
				<option value="">Select an Option</option>				
				<option value="xls">EXCEL</option>
				
				<option value="html">HTML</option>
			  </select>
			  <input name="btnExport" id="btnExport" type="button"  value="Export" onClick="exportTrialBal();">
				<script language="JavaScript">
					SelectOption(document.forms[0].exportFormat, "<?=$_REQUEST["exportFormat"]; ?>");
				</script>
				</td>
				
				<td align="left" class="heading">&nbsp;<input type="button" name="Submit2" value=" Print " onClick="hideDivs();print()"></td>
				<td align="left" class="heading" colspan='5'></td>
			
			</tr>
			<?php 
			}
			?>
		</table>
		<br />
		
	</td>
</tr>
</table>
</form>
</table>
</body>
</html>
