<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];
include("connectOtherDataBase.php");
//$username2 = $_SESSION["loggedUserData"]["username"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

unset($dateError);
unset($allCount);

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
{
	$limit=50;
}
	
	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	
	
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";

if($offset == 0)
{
$totalAmount = 0;	
}

$moneyPaid = "";

if ($_POST["moneyPaid"] != "") {
	$moneyPaid = $_POST["moneyPaid"];
} else if ($_GET["moneyPaid"] != "") {
	$moneyPaid = $_GET["moneyPaid"];
}

if($_POST["transType"]!="")
		$transType = $_POST["transType"];
	elseif($_GET["transType"]!="") 
		$transType=$_GET["transType"];
		
	if($_POST["transStatus"]!="")
		$transStatus = $_POST["transStatus"];
	elseif($_GET["transStatus"]!="")
		$transStatus = $_GET["transStatus"];

if($_POST["Submit"]!="")
		$Submit = $_POST["Submit"];
	elseif($_GET["Submit"]!="") 
		$Submit=$_GET["Submit"];
		
	if($_POST["transID"]!="")
		$transID = $_POST["transID"];
	elseif($_GET["transID"]!="")
		$transID = $_GET["transID"];
		

		
if($_POST["Submit"] =="Search" || $_GET["submit"]=="Search")
{
	
			if($_POST["fMonth"]!="")
				$_SESSION["fMonth"]=$_POST["fMonth"];
			if($_POST["fDay"]!="")
				$_SESSION["fDay"]=$_POST["fDay"];
			if($_POST["fYear"]!="")
				$_SESSION["fYear"]=$_POST["fYear"];
			if($_POST["tMonth"]!="")
				$_SESSION["tMonth"]=$_POST["tMonth"];
			if($_POST["tDay"]!="")
				$_SESSION["tDay"]=$_POST["tDay"];
			if($_POST["tYear"]!="")
				$_SESSION["tYear"]=$_POST["tYear"];
		}

	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	$dated = " and (t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	
if($Submit == "Search")
{
	if ($fromDate > $toDate)
	{
		$dateError = "FromDate must be less than or equal to ToDate";	
	}
	else
	{
	$query = "select * from ". TBL_TRANSACTIONS . " as t where 1";
	$queryCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t where 1";
	/*******
		#5273 - Minas Center
				We can give transaction stuses in config which we
				want to view in view inputter transaction for all users.
	*******/
	if(CONFIG_SHOW_INPUTTER_TRANS_STATUS_ONLY=="1" || CONFIG_SHOW_INPUTTER_TRANS_STATUS_ONLY!="0"){
		$transStatusesArr = explode(",",CONFIG_SHOW_INPUTTER_TRANS_STATUS_ONLY);
		if(CONFIG_SHOW_INPUTTER_TRANS_STATUS_ONLY!="1"){
			for($s=0;$s<count($transStatusesArr);$s++){
				if($transStatusesArr[$s]!="")
					if($s!=0)
						$fistComma = ",";
					$transStr .= $fistComma."'".strtoupper(trim($transStatusesArr[$s]))."'";
			}
			if($transStr!=""){
				$query .= " and upper(t.transStatus) IN(".$transStr.")";
				$queryCnt .= " and upper(t.transStatus) IN(".$transStr.")";
			}
		}
	}
	else{
		$query .= " and t.transStatus !='Cancelled'";
		$queryCnt .= " and t.transStatus !='Cancelled'";
	}
	$query .= $dated;
	$queryCnt .= $dated;

	if($_POST["agentName"] !="")
	{
		if($_POST["agentName"] != "All")
		{
			$agentName = $_POST["agentName"];
			$query  .=" and t.addedBy = '".$agentName."'";
			$queryCnt  .=" and t.addedBy = '".$agentName."'";
		}
	}
	
	if ($moneyPaid != "") {
		$query    .= " and t.moneyPaid = '".$moneyPaid."'";
		$queryCnt .= " and t.moneyPaid = '".$moneyPaid."'";
	}
	
/*
switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
	case "admin":
	case "Call":
		$query .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
		$queryCnt .= " and (t.addedBy = '".$_SESSION["loggedUserData"]["username"]."')";
	
}

if($agentType == "Branch Manager"){
	$query .= " and t.custAgentParentID ='$agentID' ";
	$queryCnt .= " and t.custAgentParentID ='$agentID' ";
	}*/
 $query .= " order by t.transDate DESC";
//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
 $query .= " LIMIT $offset , $limit";
 $contentsTrans = selectMultiRecords($query);

 $allCount = countRecords($queryCnt);

}
}		
?>
<html>
<head>
	<title>View Inputter Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">View Inputter Transactions</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="view_inputer_transactions.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
         </td>
      </tr>
      <tr>
      	<td align="center">
       	<select name="agentName">
       		<option value="">--- Select Name ---</option>
			<? if($agentType == "admin") {?>
       		<option value="All" <? echo ($agentName ? "selected" : "")?>>All</option>
			<? } ?>

       	<?
		if($agentType == "SUPA")
		{
			$query2 = "
				select 
					username, name 
				from " . TBL_ADMIN_USERS . " 
				where 
					 userID=".$_SESSION["loggedUserData"]["userID"].
				   " or  parentID=".$_SESSION["loggedUserData"]["userID"];
		}
		else
		{
			$query2 = "
			select 
				username, name 
			from " . TBL_ADMIN_USERS . " 
			where 
				(adminType = 'Agent' and agentType = 'Supper' and (isCorrespondent = 'Y' or isCorrespondent = 'N')) 
				or (adminType = 'Agent' and agentType = 'Sub' and (isCorrespondent = 'Y' or isCorrespondent = 'N')) 
				or (adminType = 'Branch Manager') 
				or (adminType = 'Supper' and isMain = 'Y') 
				or ((adminType = 'Call' or adminType = 'Admin Manager') and isMain = 'N') 
				or (adminType = 'Admin' and rights like '%Create Transaction%')";

		}

       	$agentContents = selectMultiRecords($query2);
       	for ($i = 0; $i < count($agentContents); $i++)
       	{
       ?>
       		<option value="<? echo $agentContents[$i]["username"]?>" <? echo ($agentName == $agentContents[$i]["username"] ? "selected" : "")?>><? echo $agentContents[$i]["name"] . " [" . $agentContents[$i]["username"] . "]"?></option>
       <? }	?>
       	</select>
		<?	
		if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Mode -</option>
          <option value="By Cash">By Cash</option>
          <option value="By Cheque">By Cheque</option>
          <option value="By Bank Transfer">By Bank Transfer</option>
		  </select>
		  <script language="JavaScript">
				SelectOption(document.Search.moneyPaid, "<?=$moneyPaid; ?>");
			</script>
		<?	}  ?>
        <input type="submit" name="Submit" value="Search"> <br>   

        </td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="view-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
				
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) ));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
				<?
					if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
					{
						?>
							<td><span class="style1">Cash Amount</span></td>
							<td><span class="style1">Cheque Amount</span></td>
						<?
					}
				?>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By </span></td>
		    <td width="74" align="center">&nbsp;</td>
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  	<?
						if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
						{
							$cashAmount = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["chequeAmount"];
							$chequeAmt = ($contentsTrans[$i]["chequeAmount"] > 0 ? $contentsTrans[$i]["chequeAmount"] : 0);
							?>
								<td><?=number_format($cashAmount, 4) .  " " . $contentsTrans[$i]["currencyFrom"]; ?></td>
								<td><?=number_format($chequeAmt, 4) .  " " . $contentsTrans[$i]["currencyFrom"];?></td>
							<?
						}
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]) .  " " . getCurrencySign($contentsTrans[$i]["currencyFrom"])?></td>
				  <? $totalAmount += $contentsTrans[$i]["totalAmount"]; 
				   if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	
				  	if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");   
            }
            if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
							$beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
							$otherClient->closeConnection();
							dbConnect();
						}else{
						
				  	
						  	$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
						  	$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
						  }
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>

				
				  <td align="center" bgcolor="#FFFFFF"><a href="add-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2"> <? if($contentsTrans[$i]["transStatus"] == "Pending"){ ?>Edit <? } else {?> &nbsp; <? }?></a></td>
				</tr>
				<?
			}
			?>
			
			
			<tr>
			  	<td colspan="2">
					<strong><font color="#006699">Page Total:</font></strong>
			  	</td>
			  	<td>&nbsp;</td>
			  	<td>&nbsp;</td>
			  	<td><?=customNumberFormat($totalAmount);?></td>
			  	<td>&nbsp;</td>
			  	<td>&nbsp;</td>
			  	<td>&nbsp;</td>
			  	<!--td colspan="2">
					<strong><font color="#006699">
					Cumulative Total </font></strong><? $allAmount=TransactionBalanceDate($username2, $fromDate, $toDate);  ?>							
				</td>
			  	<td>
			  		<? echo number_format($allAmount,2,'.',','); ?>
			  	</td-->
			</tr>
			<?
			} // greater than zero
			?>
          </table>
		  </td>
        </tr>
		

          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans) );?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
		  <tr>
		  		<td align="center">
					<input type="button" value="Print Report" onBlur="window.print();" />
				</td>
		  </tr>

          <?
        }
          else if(!isset($allCount))
          {
          	if (!isset($dateError))
          	{
          		echo "";
          	}
          	else
          	{
          		echo "<strong>$dateError</strong>";	
          	}
					}
			 		else 
			 		{
					?>
          <tr>
            <td  align="center"> No Transaction found in the database.
            </td>
          </tr>
          <?
					}
					?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>