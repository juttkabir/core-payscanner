<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

$minAmount = "";
$custName = "";
$custIDs = "";
$currencyFrom = "";
$Submit = "";

if ($_POST["minAmount"] != "") {
	$minAmount = $_POST["minAmount"];
}	elseif ($_GET["minAmount"]!="") {
	$minAmount = $_GET["minAmount"];
}

if($_POST["custName"] != "") {
	$custName = $_POST["custName"];
}	elseif ($_GET["custName"]!="") {
	$custName = $_GET["custName"];
}

if($_POST["currencyFrom"] != "") {
	$currencyFrom = $_POST["currencyFrom"];
}	elseif ($_GET["currencyFrom"]!="") {
	$currencyFrom = $_GET["currencyFrom"];
}

if ($_POST["Submit"] != "") {
	$Submit = $_POST["Submit"];
}	elseif ($_GET["Submit"]!="") {
	$Submit = $_GET["Submit"];
}


	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
if($_POST["fYear"]!= "" && $_POST["fMonth"]!= "" && $_POST["fDay"]!= ""){
		$_SESSION["fromDate"]=	$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
}else{
	$_SESSION["fromDate"] = "";
	
	
	}
	
if($_POST["tYear"]!= "" && $_POST["tMonth"]!= "" && $_POST["tDay"]!= ""){	
	$_SESSION["toDate"] = $toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
}else{
	
	$_SESSION["toDate"]= "";
	
	}

if ($custName != "") {
	
	 // searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="c";
			 $q=searchNameMultiTables($custName,$fn,$mn,$ln,$alis);
			 
	 $custIDQry = "SELECT `customerID` FROM " . TBL_CUSTOMER . " as c WHERE 1 ".$q." ORDER BY `firstname`, `middleName`, `lastName`";
	//$custIDQry = "SELECT `customerID` FROM " . TBL_CUSTOMER . " WHERE 1 AND (`firstName` LIKE '".$custName."%' OR `middleName` LIKE '".$custName."%' OR `lastName` LIKE '".$custName."%') ORDER BY `firstname`, `middleName`, `lastName`";
	$custIDRes = selectMultiRecords($custIDQry);
	for ($i = 0; $i < count($custIDRes); $i++) {
		$custIDs .= "'" . $custIDRes[$i]['customerID'] . "',";
	}
	$custIDs = substr($custIDs, 0, (strlen($custIDs) - 1));
	$custIDs;
}
	
if ($Submit == "Search") {
	
	if ($minAmount == "") {
		$minAmount = '0';	
	}
	
	$query = "SELECT * FROM ". TBL_TRANSACTIONS . " WHERE 1 AND custAgentID = $userID AND `totalAmount` >= '".$minAmount."' AND transStatus != 'Cancelled'";
	$queryCnt = "SELECT COUNT(*) FROM ". TBL_TRANSACTIONS . " WHERE 1 AND custAgentID = $userID AND `totalAmount` >= '".$minAmount."' AND transStatus != 'Cancelled'";
	if ($custIDs != "") {
		$query .= " AND `customerID` IN (".$custIDs.") ";
		$queryCnt .= " AND `customerID` IN (".$custIDs.") ";
	}
	if ($currencyFrom != "") {
		$query .= " AND `currencyFrom` = '".$currencyFrom."' ";
		$queryCnt .= " AND `currencyFrom` = '".$currencyFrom."' ";
	}
	
	if ($fromDate != "" && $toDate!= "") {
	$query .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
}
	$query .= " ORDER BY $sortBy DESC ";
	$query .= " LIMIT $offset , $limit";
	$allCount = countRecords($queryCnt);
	$contentsTrans = selectMultiRecords($query);
}
		
?>
<html>
<head>
	<title><?=__("Sender");?> AML Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2"><?=__("Sender")?> AML Report</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="cust-aml-report.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap align="center">  Amount equal or above than  
              <input name="minAmount" type="text" id="minAmount" size="10" value="<? echo ($Submit != "" ? $minAmount : CONFIG_AMOUNT_CONTROLLER) ?>">
       <br>
       <?=__("Sender")?> Name <input type="text" name="custName" id="custName" value="<? echo $custName ?>">
       <br>
       Sending Currency 
		<select name="currencyFrom" style="font-family:verdana; font-size: 11px; width:130">
				<option value="">-Select-</option> 
				<?
					$queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyName']?>" <? echo ($currencyFrom == $currencyData[$k]['currencyName'] ? "selected" : "") ?>><?=$currencyData[$k]['currencyName']?></option>	
					<? 
					}
				?>
		</select>
	</tr>
		       <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
				<OPTION value="" >Day</OPTION>
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($_SESSION["fDay"] == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value="" >Month</OPTION>
          <OPTION value="01" <? echo ($_SESSION["fMonth"] =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($_SESSION["fMonth"] =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($_SESSION["fMonth"] =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($_SESSION["fMonth"] =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($_SESSION["fMonth"] =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($_SESSION["fMonth"] =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($_SESSION["fMonth"] =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($_SESSION["fMonth"] =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($_SESSION["fMonth"] =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($_SESSION["fMonth"] =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($_SESSION["fMonth"] =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($_SESSION["fMonth"] =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
						<OPTION value="" >Year</OPTION>
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $_SESSION["fYear"] ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
				<OPTION value="" >Day</OPTION>
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($_SESSION["tDay"] == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					<OPTION value=""   >Month</OPTION>
          <OPTION value="01" <? echo ($_SESSION["tMonth"] =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($_SESSION["tMonth"] =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($_SESSION["tMonth"] =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($_SESSION["tMonth"] =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($_SESSION["tMonth"] =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($_SESSION["tMonth"] =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($_SESSION["tMonth"] =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($_SESSION["tMonth"] =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($_SESSION["tMonth"] =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($_SESSION["tMonth"] =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($_SESSION["tMonth"] =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($_SESSION["tMonth"] =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
				<OPTION value="" >Year</OPTION>
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $_SESSION["tYear"] ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
      </tr>
     <tr> <td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
      <form action="cust-aml-report.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td  bgcolor="#000000">
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td width="100"><span class="style1">Date</span></td>
			  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
			  <td width="100"><span class="style1"><? echo $manualCode;?></span></td>
				<?	}  ?>
			  <td width="100"><span class="style1"><?=__("Sender");?> Name </span></td>
			  <? if(CONFIG_ID_DETAILS_AML_REPORT ==1){?>
			  <td width="100"><span class="style1">ID Number </span></td>
			  <td width="100"><span class="style1">ID Expiry Date </span></td>
			  <? } ?>
			  <td width="100"><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Total Amount </span></td>
			  <td width="100"><span class="style1">Sending Currency </span></td>
			</tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>
				<?	
					if (CONFIG_AML_COLOR_SCHEME == '1') {
						if ($contentsTrans[$i]["totalAmount"] > CONFIG_AMOUNT_CONTROLLER) {
							$color = "#FF0000";
						} else {
							$color = "#000000";
						}
					} else {
						$color = "#000000";	
					}
				?>
				<tr bgcolor="#FFFFFF">
					<? if($agentType == "MLRO"){ ?>
							<td bgcolor="#FFFFFF"><strong><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><font color="#006699"><?  echo dateFormat($contentsTrans[$i]["transDate"], "2") ?></font></a></strong></td>
					<? }else{ ?>
				  <td bgcolor="#FFFFFF"><strong><font color="#006699"><?  echo dateFormat($contentsTrans[$i]["transDate"], "2") ?></font></strong></td>
				  <? } ?>
				  <?	if (CONFIG_SHOW_MANUAL_CODE == '1') {  ?>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["refNumber"]?></font></td>
				  <?	}  ?>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, middleName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, middleName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["middleName"]) . " " . ucfirst($customerContent["LastName"]) ?></font></td>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["middleName"]) . " " . ucfirst($beneContent["lastName"]) ?></font></td>
				  	<?
				  }else
				  {
				  	$customerContent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
				  	$beneContent = selectFrom("select firstName, middleName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["middleName"]) . " " . ucfirst($customerContent["lastName"]) ?></font></td>
				  	<? if(CONFIG_ID_DETAILS_AML_REPORT ==1){?>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $customerContent["IDNumber"]?></font></td>
				  	<td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $customerContent["IDExpiry"]?></font></td>
				  	
				  	<? } ?>
				  	
				  	
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["middleName"]) . " " . ucfirst($beneContent["lastName"]) ?></font></td>
				  	<?
				  	}
				  ?>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"])?></font></td>
				  <td bgcolor="#FFFFFF"><font color="<? echo $color ?>"><? echo $contentsTrans[$i]["currencyFrom"]?></font></td>
				</tr>
				<?
			}
			
			} // greater than zero
			
			?>
          </table></td>
        </tr>


          <tr>
            <td  bgcolor="#000000"> 
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&minAmount=$minAmount&custName=$custName&currencyFrom=$currencyFrom&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>

          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				} 
			}  
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>