<?php
session_start();
include ("../include/config.php");
include ("security.php");
$userID  = $_SESSION["loggedUserData"]["userID"];
$currentDate= date('dmy');
	
if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "txt")	
{
 

   $fileName = CONFIG_UK3DAY_EXPORT_FILE_NAME.$currentDate;
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
	header('Content-type: plain/text');
	header("Content-disposition: Attachment; filename=$fileName.txt");
	
	$strFieldsToFetchFromDb = "
					tr.refNumberIM,
					tr.transID,					
					tr.transaction_notes as transaction_notes,
					tr.transDate as transDate,
					tr.transType as transType,
					tr.currencyTo as currencyTo,
					tr.currencyFrom as currencyFrom,
					tr.transAmount as transAmount,
					tr.localAmount as localAmount,
					tr.fundSources as fundSources,
					tr.transactionPurpose as transactionPurpose,
					tr.collectionPointID,
					tr.trading_sell_account,
					tr.tip,
					tbd.accNo,
					tbd.IBAN,
					tbd.swiftCode,
					tbd.sortCode,
					tbd.routingNumber,
					tbd.bankName,
					tbd.branchAddress,
					tbd.branchCode
			";
			
	if(!empty($_REQUEST["recId"]))
	{
			$strSql = "
					select 
						$strFieldsToFetchFromDb					
					from 
						transactions as tr
						LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
						
					where 
						tr.dispatchNumber = '".$_REQUEST["recId"]."'
					order by 
						tr.transDate ";
	  
	}
	
	if(strtolower($_REQUEST["searchTrans"]) == "new" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					$strFieldsToFetchFromDb
				from 
					transactions as tr
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where 
					
					tr.transID IN (".$arrAllTransactions.")
				order by 
					tr.transDate ";
	
	}	else if(strtolower($_REQUEST["searchTrans"]) == "old" && !empty($_REQUEST["at"]))
	{
		$arrAllTransactions = substr($_REQUEST["at"],0,strlen($_REQUEST["at"])-1);
		
		$strSql = "
				select 
					$strFieldsToFetchFromDb
				from 
					transactions as tr
					LEFT JOIN bankDetails as tbd ON tr.transID = tbd.transID
				where 
					
					tr.transID IN (".$arrAllTransactions.")
				order by 
					tr.transDate ";
	}
		
	$fullRS = SelectMultiRecords($strSql);
 
	//$cpfseperator = array("-","/",".");
	//$transSeperator = "|";
	
	//$sortCode = 'SC200000';
	//$accountCode = 'BARCGB22';
	//$address = CONFIG_MT103_ADDRESS;
	
	/*  function just_clean($string)
	{
	// Replace other special chars
	$specialCharacters = array(
	'#' => '',
	'$' => '',
	'%' => '',
	'&' => '',
	'@' => '',
	'.' => '',
	'+' => '',
	'=' => '',
	'\\' => '',
	'/' => '',
	'-' => '',
	);
	
	while (list($character, $replacement) = each($specialCharacters)) {
	$string = str_replace($character, '' . $replacement . '', $string);
	}
	
	// Remove all remaining other unknown characters
	$string = preg_replace('/[^a-zA-Z0-9\-]/', '', $string);
	$string = preg_replace('/^[\-]+/', '', $string);
	$string = preg_replace('/[\-]+$/', '', $string);
	$string = preg_replace('/[\-]{2,}/', '', $string);
	
	return $string;
	}  */
	
	$accno ="";
	$strCsvOutput = "";
	$lineseperator = "\r\n";
	$strtransID = '';
	for($i=0; $i < count($fullRS); $i++)
	{
		
	$flag = 0;	
	
	/*  $_arrNote = explode("|",$fullRS[$i]["transaction_notes"]);
	//$transNote = str_replace("-","",$_arrNote[2]);
	$transNote = just_clean($_arrNote[2]);
	$transNote = wordwrap($transNote,35,$transseperator);
	$refNumberIM = just_clean($fullRS[$i]["refNumberIM"]);
	if( defined("CONFIG_REF_NUMBER_FORMAT_MT103") && CONFIG_REF_NUMBER_FORMAT_MT103 != "0" ) 
	$refNumberIM = $fullRS[$i]["refNumberIM"]; */
	 
	//$localAmount = number_format($fullRS[$i]["localAmount"]*100,2,",","");
	$localAmount = $fullRS[$i]["localAmount"]*100;
	$sortcode =$fullRS[$i]['sortCode'];
	if(!is_numeric($sortcode))
	$sortcode="";
	$accno =$fullRS[$i]['accNo']; 
	if(!is_numeric($accno))
	     $accno="";
	$accountType =$fullRS[$i]['accountType'];
	$tip= $fullRS[$i]['tip'];
	$bankname= $fullRS[$i]['bankName'];
	
	if(strtolower($_REQUEST["searchTrans"]) == "old")
		$accountNumber = $fullRS[$i]["trading_sell_account"];
		
		$accNo = $fullRS[$i]['currencyTo'];
	  
		$strAccNumber = SelectFrom("select accounNumber,sortCode,accountName from accounts where currency = '".$accNo."'");
		
		$intAccountNumber = $strAccNumber['accounNumber'];

		$intSortcode = $strAccNumber['sortCode'];

		$intAccountName = $strAccNumber['accountName'];
	
	
 
			if( empty($sortcode) || (strlen($sortcode)>6) ||
				empty($accno) || (strlen($accno)>8)||
				empty($localAmount) || (strlen($localAmount)>11)||
				 (strlen($tip)>18)||
				empty($bankname) || (strlen($bankname)>18)
				)
				
				{
					$flag = 1;
					continue;
				}  
				
		
		$strtransID .= $fullRS[$i]['transID'].',';
		$strCsvOutput .=str_pad(substr($sortcode,0,6),6); 
		$strCsvOutput .=str_pad(substr($accno,0,8),8); 
		$strCsvOutput .=str_pad(substr($accountType,0,1),1);
		$strCsvOutput .=str_pad(substr("99",0,2),2);
		$strCsvOutput .=str_pad(substr($intSortcode,0,6),6);
		$strCsvOutput .=str_pad(substr($intAccountNumber,0,8),8);
		$strCsvOutput .=str_pad(substr($fullRS[$k][''],0,4),4);
		$strCsvOutput .=str_pad(substr($localAmount,0,11),11);
		$strCsvOutput .=str_pad(substr($intAccountName,0,18),18);

		if($tip == ""){
		$tip = 'Payment';
		$strCsvOutput .=str_pad(substr($tip,0,18),18);
		}
		else{
		$strCsvOutput .=str_pad(substr($tip,0,18),18);
		}
		$strCsvOutput .=str_pad(substr($bankname,0,18),18);
		$strCsvOutput .=str_pad(substr(date('dmy'),0,6),6);
		
		if($flag == 0){
		$strCsvOutput .=$lineseperator;
		}
			
	}
 
 
	if(( $_REQUEST["searchTrans"] == "new"))
	{
	
	$strtransID = substr($strtransID, 0, -1);

	$updateQuery = "UPDATE transactions SET `transStatus` = 'Credited' WHERE transID IN (".$strtransID.")";
	$updateTransStatus=update($updateQuery);
	
	$querTransID1 = "SELECT transID FROM ".TBL_TRANSACTIONS." WHERE dispatchNumber ='".$_REQUEST["recId"]."' ";
	 $querTransID = mysql_query($querTransID1);
	 $i=0;
	 while($transID = mysql_fetch_array($querTransID)){
	 $transIDs .= $transID["transID"].",";
	 $i++;
	 }
	 
	 $transIDs  = rtrim($transIDs,",");
	 activities($_SESSION["loginHistoryID"],"UPDATION",$_REQUEST["recId"],"transactions","For transactions ".$transIDs." status is updated to Credited for ".CONFIG_UK3DAY_EXPORT_FILE_NAME."");
	 
	 $queryDispatchBookId = selectFrom("SELECT recId FROM ".TBL_DISPATCH_BOOK." WHERE dispatchNumber = '".$_REQUEST["recId"]."'");
	
	  activities($_SESSION["loginHistoryID"],"EXPORT",$_REQUEST["recId"],"dispatch_book","".CONFIG_UK3DAY_EXPORT_FILE_NAME ." file exported with ".$i." transactions");
	
	
	
	
	} 
	if( $_REQUEST["searchTrans"] == "old"){
	 
 	activities($_SESSION["loginHistoryID"],"Re-exported",$_REQUEST["recId"],"transactions","transactions with dispatchNumber ".$_REQUEST["recId"]." are re-exported");
	}
	
	echo $strCsvOutput;
	/* End of txt version */
}
?>
