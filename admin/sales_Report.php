<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";

$company = COMPANY_NAME;
if($_POST["Submit"] =="Search")
{

	$fMonth =$_POST["fMonth"];
	$fDay   =$_POST["fDay"];
	$fYear  =$_POST["fYear"];
	
	$tMonth =$_POST["tMonth"];
	$tDay   =$_POST["tDay"];
	$tYear  =$_POST["tYear"];
	
	$agent  = "";
}


if($_POST["agentID"]!="")
	$agent =$_POST["agentID"];
		
if($_POST["managerID"]!="")
	$manager =$_POST["managerID"];

if ($offset == "")
	$offset = 0;
$limit=240;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

if($_POST["currencySending"]!="")
	$_SESSION["currencySending"] =$_POST["currencySending"];
	elseif($_GET["currencySending"]!="")
		$_SESSION["currencySending"]=$_GET["currencySending"];
	else 
		$_SESSION["currencySending"] = "";

////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA" && $agentType != "SUPAI")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

?>
<html>
<head>
	<title>
		<?
		 if(CONFIG_SALES_AS_BALANCE == '1'){
		 	echo("Agent Account Balance");
		}else{echo("Sales Report");}
		?>
		</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
	<tr>
   <td class="topbar"><? echo(CONFIG_SALES_AS_BALANCE == '1'? "Agent Account Balance" : "Daily Sales Report" );?></td>
  </tr>	
</table>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  
  <tr>
  <td width="800">
  	<form action="sales_Report.php" method="post" name="Search">
  			<table width="557" align="center" id="searchTable">
          <tr>
					<td><div class='noPrint'>
			
			From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$fMonth?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$fDay?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$fYear?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$tMonth?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

		 <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$tDay?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$tYear?>");
        </script>
       
              <? 
              if(IS_BRANCH_MANAGER == '1')
              {
		              if($condition)
		              {
		              	?>
		               <br> Select a Branch Manager
		              <select name="managerID" style="font-family:verdana; font-size: 11px">
		               	<option value="">Select Branch Managers</option>
		               	<option value="all">All</option>
		                <?
		                $managerQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where adminType = 'Branch Manager'";
							
										$managerQuery .= " order by agentCompany";
										$managers = selectMultiRecords($managerQuery);
										for ($i=0; $i < count($managers); $i++){
									?>
					                <option value="<?=$managers[$i]["userID"]; ?>"><? echo($managers[$i]["agentCompany"]." [".$managers[$i]["username"]."]"); ?></option>
					                <?
										}
									?>
					             </select>
								 <script language="JavaScript">
					         	SelectOption(document.forms[0].managerID, "<?=$manager?>");
					                                </script>
        			<? 
        			}
        		}
        			 ?>
        <br>
              <? 
              if($condition)
              {
              	?>
             Select <?=__("Agent")?>  <select name="agentID" style="font-family:verdana; font-size: 11px">
                <option value="">- Select Agent-</option>
								<option value="all">All Agents</option>
                <?
                $agentQuery = "select userID,username, agentCompany, name, agentStatus, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$changedBy'";				
									
								}
								
								
								$agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                <?
					}
				?>
             </select>
			 <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$agent?>");
        </script>
               
         <? if(CONFIG_ADD_CURRENCY_DROPDOWN == "1"){?>       
          <br><br>
					Sending Currency &nbsp;
          	<select name="currencySending" style="font-family:verdana; font-size: 11px; width:130"> 
          	<?
          	  $currQuery = "select distinct(currencyOrigin) from exchangerate where currencyOrigin !=''";
          	  $sendingCurr = selectMultiRecords($currQuery);
          	  
          	   for($k = 0; $k < count($sendingCurr); $k++){
						 ?>
							 <option value="<?=trim($sendingCurr[$k]["currencyOrigin"])?>"><?=trim($sendingCurr[$k]["currencyOrigin"])?></option>	
							
          
           <? } ?>
           
           
             <script language="JavaScript">
         	    SelectOption(document.forms[0].currencySending, "<?=$_SESSION["currencySending"]; ?>");
             </script>
             <? }} ?>    
      <input type="submit" name="Submit" value="Search">
	  
      				</div></td>
				</tr>
			</table>	
			</form>				
		</td>
		</tr>
		<form method="post" name="trans">
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"] =="Search"||$_GET["search"] == "search")
	 {
	 	if($_POST["agentID"]!="")
		{
	 		$Balance="";
			$agent = $_POST["agentID"];
			
		}
		else 
				$agent = $_GET["agentID"];
		
			if(!$condition)
			{
				$agent = $_SESSION["loggedUserData"]["userID"];
			}	
			if($agent != "" || $manager != ""){
				
				if($agent == "all"){
					
					$accountQuery = "select userID, username, balance, agentCompany, name, agentStatus, agentContactPerson,isCorrespondent from ".TBL_ADMIN_USERS." where parentID >= 0 and adminType='Agent' and isCorrespondent != 'ONLY'";
					}
				elseif($agent != ""){
					$accountQuery = "select userID, username, balance, agentCompany, name, agentStatus, agentContactPerson,isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$agent."' ";
					}
					else{
						$accountQuery = "select userID, username, balance, agentCompany, name, agentStatus, agentContactPerson,isCorrespondent from ".TBL_ADMIN_USERS." where parentID >= 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
					}
					
						if($agentType == "Branch Manager")
						{
							$accountQuery .= " and parentID = '$changedBy'";				
							
						}
						
						if($manager != "")
						{
							if($manager != "all"){
								$accountQuery .= " and parentID = '$manager'";				
							}
						
						}
						
				 $accountQuery .= "order by agentCompany";
				 
				// echo $accountQuery;
				 
						$contentsAcc = selectMultiRecords($accountQuery);
				$exportQuery = $accountQuery;
				$allCount = count($contentsAcc);
				$allAmount=0;
					
					//echo $accountQuery;
				//$accountQuery .= " LIMIT $offset , $limit";
				
		}
	     
	?>	
        <table width="715" height="284" border="1" align="center" cellpadding="0" bordercolor="#666666">
          <tr> 
            <td height="25" nowrap bgcolor="#6699CC" colspan="11"> <div class='noPrint'>
			<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF" id="pagination">
                <tr> 
                  <td width="256" align="left">
                    <?php if (count($contentsAcc) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&agentID=$agent&search=search&total=first";?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="77" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&agentID=$agent&search=search&total=pre";?>"><font color="#005b90">Previous</font></a> 
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="61" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&agentID=$agent&search=search&total=next";?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <!--td width="50" align="right"><a href="<?php //print $PHP_SELF . "?agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td-->
                  <?php } ?>
                </tr>
              </table> </div></td>
          </tr>
		  <tr>
            <td height="25" nowrap bgcolor="#ffffff" colspan="11"><span class="tab-u"><font color="000000"><strong><i>&nbsp;Report 
              Date - <? echo date("m.d.y");?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <? echo(CONFIG_SALES_AS_BALANCE == '1'? "Agent Account Balance" : "Report about Daily Sales" );?></i></strong></font></span></td>
        </tr>
          <?
		if(count($contentsAcc) > 0)
		{
			$totalSale = 0;
			$totalCmpComm = 0;
			$totalAgentComm = 0;
			$totalSurcharges = 0;
			$totalGross = 0;
			$totalCash = 0;
			$totalBank = 0;
			$totalCancel = 0;
			$totalBalance = 0;
			$totalCummulative = 0;
			$totalManualWithdraw = 0;
			$totalOpeningBalance = 0;
			$totalClosingBalance = 0;			
			
			$fromDate = $fYear . "-" . $fMonth . "-" . $fDay;
			$toDate   = $tYear . "-" . $tMonth . "-" . $tDay;
			$transDate = "(t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
			$cancelDate = "(cancelDate >= '$fromDate 00:00:00' and cancelDate <= '$toDate 23:59:59')";
			$cancelDexcription = " and (description like '% Cancelled%' OR description like '% Failed%' OR description like '% Rejected%')";
			$accountDate = "(dated >= '$fromDate' and dated <= '$toDate')";
				
		?>
        
          <tr bgcolor="#FFFFFF"> 
            	<td width="166"><span class="style1"><b><?=__("Agent")?> Name</b></span></td>
            	<?
            		/**
            		 * Introiducing the new column
            		 * @Ticket# 3256
            		 * @addedBy	jahangir
            		 */
            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1'):
            	?>
            		<td width="175"><span class="style1"><b>Opening Balance</b></span></td>
            <?
            		endif;
            if(CONFIG_SALES_AS_BALANCE == '1'){
            ?>
            	<td width="175"><span class="style1"><b>Gross Transactions Amount</b></span></td>
            	<td width="166"><span class="style1"><b>Cash Withdarw</b></span></td>
	            <td width="166"><span class="style1"><b>Cash Payment Received</b></span></td>
	            <td width="166"><span class="style1"><b>Bank Payment Received</b></span></td>
	            <td width="166"><span class="style1"><b>Cancelled Transactions</b></span></td>
	            
	            <?
            		/**
            		 * Introiducing the new column
            		 * @Ticket# 3256
            		 * @addedBy	jahangir
            		 */
            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1'){
            	?>
            		<td width="175"><span class="style1"><b>Closing Balance</b></span></td>
	            <?
	               } else {
	            ?>
	            <td width="166"><span class="style1"><b>Balance</b></span></td>
	            
            <?
            }
        	}else{
            ?>
	            <td width="175"><span class="style1"><b>Net Sale</b></span></td>
	            <td width="446"><span class="style1"><b><?=$company?> Commission</b></span></td>
	            <td width="446"><span class="style1"><b><?=__("Agent")?> Commission</b></span></td>
	            <td width="166"><span class="style1"><b>Surcharges</b></span></td>
	            <td width="175"><span class="style1"><b>Gross Sale</b></span></td>
	            <td width="166"><span class="style1"><b>Cash Payment Received</b></span></td>
	            <td width="166"><span class="style1"><b>Bank Payment Received</b></span></td>
	            <td width="166"><span class="style1"><b>Cancelled Transactions</b></span></td>
	            <td width="166"><span class="style1"><b>Balance</b></span></td>
	            <td width="166"><span class="style1"><b>Cumulative Balance</b></span></td>
            <?
        	}
            ?>
          </tr>
     <? 
     $values = ("Agent Name,Net Sale,$company Commission,Agent Commission,Surcharges,Gross Sale,Cash Payment Recieved,Bank Payment Recieved,Balance,Cumulative Balance");
    
     for($i=0;$i < count($contentsAcc);$i++)
			{
			 if($contentsAcc[$i]["isCorrespondent"] == 'N'){
			  $tblName = 'agent_account'					 	;
			}elseif($contentsAcc[$i]["isCorrespondent"] == 'Y'){
				
				     $tblName = 'agent_Dist_account';
				}
			
			
			  $transQuery = "select date_format(transDate, '%Y-%m-%d') as simpleDate, date_format(transDate, '%D %b %Y') as tDate, count(transID) as cnt, sum(transAmount) as netSales, 
				sum(IMFee) as totalFee, sum(AgentComm) as agentCommission, sum(bankCharges) as extra1, sum(cashCharges) as extra2, sum(admincharges) as extra3, sum(outCurrCharges) as extra4,currencyFrom,currencyTo
				from ". TBL_TRANSACTIONS . " as t where t.custAgentID = '".$contentsAcc[$i]["userID"]."' and $transDate ";
			  			  
		    $accountQuery = "select sum(amount) as cashPayment,currency from ".$tblName." where agentID = '".$contentsAcc[$i]["userID"]."' and type = 'DEPOSIT' and description like 'Manually Deposited' and status != 'Provisional' and $accountDate ";
			  
				$accountQuery2 = "select sum(amount) as bankPayment,currency from ".$tblName." where agentID = '".$contentsAcc[$i]["userID"]."' and type = 'DEPOSIT' and (description like 'Natwest Payment' or description like 'Barclays Payment') and status != 'Provisional' and $accountDate";
			 
				
				// $accountDate $cancelQuery = "select sum(a.amount) as cancelled from agent_account as a, ".TBL_TRANSACTIONS." as t  where a.TransID = t.transID  and a.agentID = '".$contentsAcc[$i]["userID"]."' and a.type = 'DEPOSIT' and t.transStatus  = 'Cancelled' and $cancelDate group by a.agentID";
				$cancelledAmount = 0;
				
				 if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
			 		
			 		$transQuery .= " and t.currencyFrom = '".$_SESSION["currencySending"]."' ";
			 		$accountQuery .= " and currency = '".$_SESSION["currencySending"]."' ";
			 		$accountQuery2 .= " and currency = '".$_SESSION["currencySending"]."' ";
			 			
			  }
			  
			  $transQuery .= " group by t.custAgentID ";
			  $accountQuery .= " group by agentID ";
			  $accountQuery2 .= " group by agentID ";	
			   
			  $transData    =  selectFrom($transQuery);
			  $accountData  =  selectFrom($accountQuery);
			  $accountData2 =  selectFrom($accountQuery2);
			  
			
			  
				if(CONFIG_BALANCE_FROM_TRANSACTION == '1')
				{
					
						$transCancellQuery = "SELECT sum(totalAmount), sum(AgentComm), sum(transAmount), refundFee,currencyFrom,currencyTo
						FROM ".TBL_TRANSACTIONS." WHERE custAgentID ='".$contentsAcc[$i]["userID"]."' and $cancelDate"; 
						
						
						if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
								
								$transCancellQuery .= " and currency = '".$_SESSION["currencySending"]."' ";
						}
						$transCancellQuery .= " group by refundFee ";
						$transCancelled = selectMultiRecords($transCancellQuery);
						
						/*$transCancelled = selectMultiRecords("SELECT sum(totalAmount), sum(AgentComm), sum(transAmount), refundFee,currencyFrom,currencyTo
							FROM ".TBL_TRANSACTIONS." WHERE custAgentID ='".$contentsAcc[$i]["userID"]."' and $cancelDate group by refundFee"); */
						
						
						for($c = 0; $c < count($transCancelled); $c++)
						{
							if($transCancelled["refundFee"] == 'No')
								{
									$cancelledAmount += $transCancelled["transAmount"];		
								}else{
									$cancelledAmount += $transCancelled["totalAmount"] - $transCancelled["AgentComm"];		
									}
								$currencyCancellTrans = $transCancelled["currencyFrom"]; 	
							
							}
							
					}else{
					$cancelQuery = "select sum(amount) as cancelled,currency from ".$tblName." where agentID = '".$contentsAcc[$i]["userID"]."' and type = 'DEPOSIT' and $accountDate $cancelDexcription";
					
						if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
								
								$cancelQuery .= " and currency = '".$_SESSION["currencySending"]."' ";
						 }
						 $cancelQuery .= " group by agentID ";	
					
					$cancelData = selectFrom($cancelQuery);
					$cancelledAmount = $cancelData["cancelled"];
					$currencyCancellTrans = $cancelData["currency"]; 									
				}
				$totalCancel += $cancelledAmount;
				
				$withDrawQuery = "select sum(amount) as manualWithdraw,currency from ".$tblName." where agentID = '".$contentsAcc[$i]["userID"]."' and type = 'WITHDRAW' and $accountDate and description like 'Manually Withdrawn'";
				
				$withDrawQuery .= " group by agentID ";	
				
				if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
								
								$withDrawQuery .= " and currency = '".$_SESSION["currencySending"]."' ";
						 }
				
				$withDrawData = selectFrom($withDrawQuery);
				
			if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
				$currencyBalance = '';
			  if($transData["currencyFrom"] !='' ){
			  	$currencyBalance = $transData["currencyFrom"];
			  }elseif($accountData["currency"] !=''){
			  	$currencyBalance = $accountData["currency"];
			  }elseif($accountData2["currency"] !=''){
			  	$currencyBalance = $accountData2["currency"];
			  }elseif($transCancelled["currencyFrom"] !=''){
			  	$currencyBalance = $transCancelled["currencyFrom"];
			  }elseif($cancelData["currency"] !=''){
			  	$currencyBalance = $cancelData["currency"];
			  }elseif($withDrawData["currency"] !=''){
			  	$currencyBalance = $withDrawData["currency"];
			  }else{
			  	
			  	$currencyBalance = " ";
			  	}
			 	
			}
			 
				//if($transData["cnt"] > 0)
				{
					?>
						 <tr bgcolor="#FFFFFF"> 
		            	<td width="166"><span class="style1"> &nbsp;<? echo($contentsAcc[$i]["name"]."[".$contentsAcc[$i]["username"]."]"); ?></span></td>
		            	<?
	            		/**
	            		 * Fetching its coulms value
	            		 */
	            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1'):
						
										//echo "select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '".$contentsAcc[$i]["userID"]."' and dated <= '$fromDate'<br>";
										$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_ACCOUNT_SUMMARY . " where user_id = '".$contentsAcc[$i]["userID"]."' and dated <= '$fromDate'");
										 //print_r($datesFrom); 
										 $FirstDated = $datesFrom["datedFrom"];
										
										////$FirstDated = $fromDate;
										
										
										$datesTo = selectFrom("select  Max(dated) as datedTo from " . TBL_ACCOUNT_SUMMARY . " where user_id = '".$contentsAcc[$i]["userID"]."' and dated <= '$toDate'");
										$LastDated = $datesTo["datedTo"];
										
										
										$openingBalance = 0;
										$closingBalance = 0;
										
										
										if($FirstDated != "")
										{
											$account1 = "Select opening_balance, closing_balance from
											".TBL_ACCOUNT_SUMMARY." where 1";
											$account1 .= " and dated = '$FirstDated'";
											$account1 .= " and user_id = '".$contentsAcc[$i]["userID"]."' ";
											if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == "1" && $settleCurr!='') 
											$account1 .= " and currency='".$settleCurr."' ";
										
											////echo $account1."<br>";
											$contents1 = selectFrom($account1);
											///print_r($contents1)."<br>";
											if($fromDate == $FirstDated)
												$openingBalance = $contents1["opening_balance"];
											else
												$openingBalance = $contents1["closing_balance"];
											
											/////-------$closingBalance = $contents1["closing_balance"];
											
											$totalOpeningBalance += $openingBalance;
										}
										
										
										if($LastDated != "")
										{
											$account2 = "Select opening_balance, closing_balance from ".TBL_ACCOUNT_SUMMARY." where 1";
											$account2 .= " and dated = '$LastDated'";				
											$account2 .= " and  user_id = '".$contentsAcc[$i]["userID"]."' ";								
											$contents2 = selectFrom($account2);
											
											$closingBalance =  $contents2["closing_balance"];
										}
										
										
										
	            		?>
	            			<td width="175" align="center"><span class="style1"><?=number_format($openingBalance ,2,'.',',')?></span></td>
		            <?
		            	endif;
		            if(CONFIG_SALES_AS_BALANCE == '1'){
		            ?>
			           
			            <td width="175"><span class="style1"> &nbsp;<?  
			            	
			            	$surcharges = $transData["extra1"] + $transData["extra2"] + $transData["extra3"] + $transData["extra4"]; 
			            	$gorssSale = $transData["netSales"] + $transData["totalFee"] + $surcharges;
			            																							 
			            																							echo  number_format($gorssSale      ,2,'.',',');if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$transData["currencyFrom"];}
			            																							 $totalGross += $gorssSale; 
			            																						?></span></td>
						<td width="166"><span class="style1"> &nbsp;<?	$totalManualWithdraw += $withDrawData["manualWithdraw"]; 
			            	
			            																							echo number_format($withDrawData["manualWithdraw"] ,2,'.',',');
			            																							if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$withDrawData["currency"];}
			            																						?></span></td>			            																																											
			            <td width="166"><span class="style1"> &nbsp;<?	$totalCash += $accountData["cashPayment"]; 
			            	
			            																							echo number_format($accountData["cashPayment"] ,2,'.',',');
			            																							if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$accountData["currency"];}
			            																						?></span></td>
			            <td width="166"><span class="style1"> &nbsp;<? $totalBank += $accountData2["bankPayment"];
			            																							echo number_format($accountData2["bankPayment"] ,2,'.',',');
			            																								if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$accountData2["currency"];}
			            																							
			            																						?></span></td>
						<td width="166"><span class="style1"> &nbsp;<? 
			            																							echo number_format($cancelledAmount ,2,'.',',');if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$currencyCancellTrans;}
			            																						?></span></td>
			            																									            																						
			            
			            
			            <td width="166">
			            	<span class="style1"> &nbsp;
			            		<?
					            		/**
					            		 * Introiducing the new column
					            		 * @Ticket# 3256
					            		 * @addedBy	jahangir
					            		 */
					            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1')
					            		{
					            			echo number_format($closingBalance ,2,'.',',');
					            			$totalClosingBalance += $closingBalance;
					            		}
					            		else
					            		{
					          				$balance = $gorssSale - ($accountData["cashPayment"] + $accountData2["bankPayment"] + $cancelledAmount) + $withDrawData["manualWithdraw"];
					          				$totalBalance += $balance;
				            				echo number_format($balance ,2,'.',',');if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$currencyBalance;} 
				            			}
		            			?>
		            		</span>
		            	</td>
		            
		            <?
		        	}else{
		            ?>
		            
		            <td width="175"><span class="style1"> &nbsp;<? $totalSale += $transData["netSales"]; echo($transData["netSales"]); ?></span></td>
		            <td width="446"><span class="style1"> &nbsp;<? $cmpComm = $transData["totalFee"] - $transData["agentCommission"];
		            																								$totalCmpComm += $cmpComm; 
		            																								echo number_format($cmpComm ,2,'.',',');  ?></span></td>
		            <td width="446"><span class="style1"> &nbsp;<? $totalAgentComm += $transData["agentCommission"];
		            																							 echo number_format($transData["agentCommission"] ,2,'.',',');
		            																						?></span></td>
		            <td width="166"><span class="style1"> &nbsp;<? $surcharges = $transData["extra1"] + $transData["extra2"] + $transData["extra3"] + $transData["extra4"]; 
		            																								$totalSurcharges += $surcharges;
		            																								echo number_format($surcharges ,2,'.',',');
		            																					?></span></td>
		            <td width="175"><span class="style1"> &nbsp;<?  $gorssSale = $transData["netSales"] + $cmpComm + $surcharges;
		            																							 
		            																							 echo number_format($gorssSale ,2,'.',',');
		            																							 $totalGross += $gorssSale; 
		            																						?></span></td>																					
		            <td width="166"><span class="style1"> &nbsp;<?	$totalCash += $accountData["cashPayment"]; 
		            																							
		            																							echo number_format($accountData["cashPayment"] ,2,'.',',');
		            																						?></span></td>
		            <td width="166"><span class="style1"> &nbsp;<? $totalBank += $accountData2["bankPayment"];
		            																							echo number_format($accountData2["bankPayment"] ,2,'.',',');
		            		
		            																						?></span></td>
		            																						
					<td width="166"><span class="style1"> &nbsp;<? 
			            																							echo number_format($cancelledAmount ,2,'.',',');
			            																						?></span></td>
			           
			           		            																						
		            <td width="166"><span class="style1"> &nbsp;<? 
		          $balance = $gorssSale - $accountData["cashPayment"] - $accountData2["bankPayment"]- $cancelledAmount;
		            																					$totalBalance += $balance;
		            																					echo number_format($balance ,2,'.',',');
		            																						?></span></td>
		            <td width="166"><span class="style1">
		            	<?
		            	$preBalance = 0;
		            
		            	
		            	$accountQuery = "Select type, amount, agentID from agent_account where agentID = '".$contentsAcc[$i]["userID"]."'";
									$accountQuery .= " and dated < '$fromDate'";
									$accountQuery .= " and status != 'Provisional' ";			
									
								  $result = mysql_query($accountQuery);
									while($row = mysql_fetch_row($result)) 
									{
										
										if($row[0] == "DEPOSIT")
										{
										  	$preBalance = $preBalance + $row[1];
										}
										  	
										if($row[0] == "WITHDRAW")
										{
										  	$preBalance = $preBalance - $row[1];
										}
									}
										
										echo($preBalance);
		            	?> &nbsp; </span></td>
		            <?
		            }
		            ?>
		          </tr>
					
					<?	
				}
			
			}
			?>
				        <tr bgcolor="#FFFFFF"> 
                  <td height="37" colspan="11">&nbsp; </td>
                </tr>

               
                <tr bgcolor="#FFFFFF"> 
                  <td><b>&nbsp;Total</b></td>
                  <?
	            		/**
	            		 * Fetching its coulms value
	            		 */
	            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1'):
	            		?>
	            			<td><b>&nbsp;<? echo number_format($totalOpeningBalance  ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$transData["currencyFrom"];}?></b></td>
	            		<?
	            		endif;
	            		
                  if(CONFIG_SALES_AS_BALANCE == '1')
                  {
                  ?>
                  	<td><b>&nbsp;<? echo number_format($totalGross     ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$transData["currencyFrom"];}?></b></td>
                  	<td><b>&nbsp;<? echo number_format($totalManualWithdraw     ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$withDrawData["currency"];}?></b></td>
	                  <td><b>&nbsp;<? echo number_format($totalCash      ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$accountData["currency"];}?></b></td>
	                  <td><b>&nbsp;<? echo number_format($totalBank      ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$accountData2["currency"];}?></b></td>
	                  <td><b>&nbsp;<? echo number_format($totalCancel      ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$currencyCancellTrans;}?></b></td>
	                  <?
		            		/**
		            		 * Fetching its coulms value
		            		 */
		            		if(CONFIG_SHOW_AGENT_OPENING_BAL_ON_REPORT == '1'){
		            		?>
	            			<td><b>&nbsp;<? echo number_format(round($totalClosingBalance,2)  ,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$transData["currencyFrom"];}?></b></td>
	            			<?
	            		  } else { 
	            			?>
	                  <td><b>&nbsp;<? echo number_format($totalBalance,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES =="1"){ echo" ".$currencyBalance;}?>   </b></td>
                  <?
                  	}
                	}else{
                  ?>
                  <td><b>&nbsp;<? echo number_format($totalSale      ,2,'.',',');?>      </b></td>
                  <td><b>&nbsp;<? echo number_format($totalCmpComm   ,2,'.',',');?>   </b></td>
                  <td><b>&nbsp;<? echo number_format($totalAgentComm ,2,'.',',');?> </b></td>
                  <td><b>&nbsp;<? echo number_format($totalSurcharges,2,'.',',');?></b></td>
                  <td><b>&nbsp;<? echo number_format($totalGross     ,2,'.',',');?>     </b></td>
                  <td><b>&nbsp;<? echo number_format($totalCash      ,2,'.',',');?>      </b></td>
                   <td><b>&nbsp;<? echo number_format($totalBank      ,2,'.',',');?>      </b></td>
                  <td><b>&nbsp;<? echo number_format($totalCancel      ,2,'.',',');?>      </b></td>
                  <td><b>&nbsp;<? echo number_format($totalBalance,2,'.',',');?>   </b></td>
                  <td><b>&nbsp;	<? //=$total ?>       </b></td>
                  <?
                	}
                  ?>
                </tr>
                
                <tr bgcolor="#FFFFFF"> 
                  <td height="37" colspan="11"> </td>
                </tr>
              </table>
              <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view. Select Proper Filters to Show Data </font>
				</td>
			</tr>
          
          <?
			}
			?>
          
		  <tr id="actionBtn">
		  	<td align="center">  
		  		<div class='noPrint'>
						<input type="button" name="Submit2" value="Print This Report" id="printReport">
						<?
						if(CONFIG_SALES_AS_BALANCE != '1'){
						?>
						<input type="button" name="export" value="Generate Excel" onClick="document.trans.action='<?=ADMIN_ACCOUNT_URL?>export_excel.php'; document.trans.submit();">
						<input type="hidden" name="totalFields" value="<?=$values?>">
						<input type="hidden" name="query" value="<?=$exportQuery?>">
						<input type="hidden" name="transDate" value="<?=$transDate?>">
						<input type="hidden" name="accountDate" value="<?=$accountDate?>">
                        <input type="hidden" name="pageUrl" value="<?=$_SERVER['PHP_SELF']?>">
						<input type="hidden" name="reportName" value="Sales_Report">
						<input type="hidden" name="reportFrom" value="export_daily_sales.php">
						<?
						}
						?>
						
					</div>
			
			</td>
		  </tr>
        </table>
	<?	
	 }?>	 
	 </td>
	</tr>
</form>
</table>
</body>
</html>