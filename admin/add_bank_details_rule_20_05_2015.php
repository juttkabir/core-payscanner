<?php 
	session_start();
	//ini_set("display_errors","1");
	//error_reporting("1");
	// Including files
	include_once ("../include/config.php");
	include_once ("security.php");
	include_once ("lib/audit-functions.php");
	//echo $_SESSION["loggedUserData"]["userID"];
	/*
	echo "<pre>";
		print_r($_SESSION["loggedUserData"]);
	echo "</pre>";
	
	$_SESSION["loggedUserData"]["name"];
	*/
	if(isset($_POST['addRule'])){
		/* $queryChkExist = "SELECT count(*) AS rules FROM ".TBL_BEN_BANK_DETAILS_RULE." WHERE `benCountry` = '{$_POST['benCountry']}' and `currency` = '{$_POST['currency']}'"; */
		$queryChkExist = "SELECT count(*) AS rules FROM ".TBL_BEN_BANK_DETAILS_RULE." WHERE `currency` = '{$_POST['currency']}'";
		$existRule = selectFrom($queryChkExist);
		if($existRule['rules'] <= 0){
			$queryInsertRule = "INSERT INTO ".TBL_BEN_BANK_DETAILS_RULE." ";
			$fields = "( ";
			$values = "VALUES( ";
			foreach($_POST as $i => $v){
				if($i == 'addRule')
					continue;
				$fields .= "`$i`, ";
				$values .= "'$v', ";
				
			}
			//$exp = '/\, $/';
			//$fields = preg_replace($exp, "", $fields);
			//$values = preg_replace($exp, "", $values);
			$todayDate = date("Y-m-d h:i:s");
			$fields .= "`creationDate`, `createdBy`, `updateDate`, `updatedBy`)";
			$values .= " '$todayDate', '{$_SESSION["loggedUserData"]["userID"]}', '$todayDate', '{$_SESSION["loggedUserData"]["userID"]}')";
			$queryInsertRule .= $fields ." ". $values;
			mysql_query($queryInsertRule) or die(mysql_error());
			activities($_SESSION["loginHistoryID"],"INSERTION",$_SESSION["loginHistoryID"],ben_country_rule_bank_details,"Rules Added Successfully");
			$msg = "<span style='color:#009039'>Rule Created Successfully!!</span>";
		}
		
		else
			$msg = "<span style='color:#FF0000'>Rule for this currency is already existed.</span>";
	}
	
	/* $previousValues = selectFrom("SELECT * FROM ben_country_rule_bank_details  WHERE  id='".$ruleID."' "); */
	
	if(isset($_POST['updateRule']) && isset($_POST['ruleID'])){
		$ruleID = $_POST['ruleID'];
		$updateValues = " SET ";
		/*
		foreach($_POST as $i => $v){
			if($i == 'updateRule' || $i == 'ruleID')
				continue;
			$updateValues .= "`$i` = '$v', ";
			//debug($updateValues);
		}
		*/
		//$exp = '/\, $/';
		//$fields = preg_replace($exp, "", $fields);
		//$values = preg_replace($exp, "", $values);
		
		 $previousValues = selectFrom("SELECT * FROM ben_country_rule_bank_details  WHERE  id='".$ruleID."' ");
		 //debug ($previousValues);
		if(isset($_POST['benCountry']) && !empty($_POST['benCountry']))
			$updateValues .= "`benCountry` = '{$_POST['benCountry']}', ";
			
		if(isset($_POST['currency']) && !empty($_POST['currency']))
		$currency = trim($_POST['currency']);
			$updateValues .= "`currency` = '{$currency}', ";
		if(isset($_POST['accountName']) && $_POST['accountName'] == 'Y')
			$updateValues .= "`accountName` = 'Y', ";
		else
			$updateValues .= "`accountName` = 'N', ";
			
		if(isset($_POST['accNo']) && $_POST['accNo'] == 'Y')
			$updateValues .= "`accNo` = 'Y', ";
		else
			$updateValues .= "`accNo` = 'N', ";
			
		if(isset($_POST['branchNameNumber']) && $_POST['branchNameNumber'] == 'Y')
			$updateValues .= "`branchNameNumber` = 'Y', ";
		else
			$updateValues .= "`branchNameNumber` = 'N', ";
		
		if(isset($_POST['branchAddress']) && $_POST['branchAddress'] == 'Y')
			$updateValues .= "`branchAddress` = 'Y', ";
		else
			$updateValues .= "`branchAddress` = 'N', ";
		
		if(isset($_POST['iban']) && $_POST['iban'] == 'Y')
			$updateValues .= "`iban` = 'Y', ";
		else
			$updateValues .= "`iban` = 'N', ";
			
		if(isset($_POST['swiftCode']) && $_POST['swiftCode'] == 'Y')
			$updateValues .= "`swiftCode` = 'Y', ";
		else
			$updateValues .= "`swiftCode` = 'N', ";
			
		if(isset($_POST['sortCode']) && $_POST['sortCode'] == 'Y')
			$updateValues .= "`sortCode` = 'Y', ";
		else
			$updateValues .= "`sortCode` = 'N', ";
			
		if(isset($_POST['routingNumber']) && $_POST['routingNumber'] == 'Y')
			$updateValues .= "`routingNumber` = 'Y', ";
		else
			$updateValues .= "`routingNumber` = 'N', ";
		
		if(isset($_POST['status']) && $_POST['status'] == 'Enable')
			$updateValues .= "`status` = 'Enable', ";
		else
			$updateValues .= "`status` = 'Disable', ";
		
		$todayDate = date("Y-m-d h:i:s");
		$updateValues .= " `updateDate` = '$todayDate', ";
		$updateValues .= " `updatedBy` = '{$_SESSION["loggedUserData"]["userID"]}'";
		$queryUpdateRule = "UPDATE ".TBL_BEN_BANK_DETAILS_RULE." ".$updateValues." WHERE id = '$ruleID' LIMIT 1";
		//debug($queryUpdateRule);
		mysql_query($queryUpdateRule) or die(mysql_error());
		// logs detail at modify history
		logChangeSet($previousValues,$_SESSION["loggedUserData"]["userID"],'ben_country_rule_bank_details','id',$ruleID ,$_SESSION["loginHistoryID"]);
		// log activity detail 
		activities($_SESSION["loginHistoryID"],"UPDATION",$ruleID,ben_country_rule_bank_details,"Rules Modified Successfully");
		$msg = "<span style='color:#009039'>Rule Updated Successfully!!</span>";
	}
	
	if(isset($_GET['rid']) && isset($_GET['act']) && $_GET['act'] == 'e'){
		$ruleID = $_GET['rid'];
		$queryRule = "SELECT id, benCountry,currency, accountName, accNo, branchNameNumber, branchAddress, swiftCode, sortCode, iban, routingNumber, creationDate, updateDate, status FROM ".TBL_BEN_BANK_DETAILS_RULE." AS benBankDetailsRule WHERE id = '$ruleID' LIMIT 1";
		//debug($queryRule);
		$rule = selectFrom($queryRule); 
		$curr = $rule["currency"];
	}
?>
<html>
	<head>
		<title>Select Mandatory Fields for Beneficiary Bank Details</title>
		<link rel="stylesheet" type="text/css" href=""/>
		<style>
			body{
				margin:0;
				font-family:arial;
				color:#555;
			}
			h2{
				margin-bottom:0;
			}
			.wrapper{
				width:600px;
				margin:0 auto;
				overflow:hidden;
			}
			.headtxt{
				color:#fff;
				background:#6699CC;
				padding:2px;
				font-size:14px;
			}
			label,p,.link{
				font-size:12px;
			}
			p{
				font-weight:bold;
			}
			.container{
				border:1px solid #aaa;
				padding:10px 5px;
				overflow:hidden;
			}
			.link{
				float:right;
				display:inline-block;
				text-decoration:none;
			}
			label{
				display:inline-block;
				vertical-align:top;
				padding-top:2px;
			}
			input{
				margin:0;
			}
			.left-col{
				float:left;
				width:50%;
			}
			.right-col{
				float:right;
				width:50%;
			}
			.row{
				clear:both;
				margin:5px 0;
				padding:5px;
			}
			.msg{
				font-size:12px;
				margin-bottom:10px;
			}
		</style>
	<Script type="text/javascript">
	function checkForm(theForm) {
	
	}
    </script>	
		
	</head>
	<body>
		<div class="wrapper">
			<h2 class="headtxt">Bank Fields Configuration</h2>
			<div class="container">
				<div class="msg">
					<?php 
						if(isset($msg))
							echo $msg;
					?>
				</div>
				<form action="<?php echo $_SERVER["PHP_SELF"];?>" onSubmit="return checkForm(this);" method="post" name ="form">
				<!-- hide against @12703->
					<!--<label>Beneficiary Country: </label>
					<?php 
					/*	$queryCountries = "SELECT DISTINCT countryName, countryId FROM `countries` WHERE countryName != ''";
						$countries = selectMultiRecords($queryCountries);
						
					?>
				
					<select name="benCountry">
						<?php 
							for($x = 0; $x < count($countries); $x++){
						?>
						<option value="<?php echo $countries[$x]['countryId']; ?>" <?php 
							if(isset($rule['benCountry']) && $rule['benCountry'] == $countries[$x]['countryId'])
								echo "selected='selected'";
							?>
							><?php echo $countries[$x]['countryName'] ?></option>
						<?php 
							} */
						?>
					</select>-->
					<label>Beneficiary Currency:</label><font color="#ff0000">*</font>
				
					<?php 
						/* $queryCurrencies = "SELECT DISTINCT currencyName, cID FROM `currencies` WHERE currencyName != ''"; */
						$queryCurrencies ="select distinct(currencyRec) from services where currencyRec != ''";
						
						$currencies = selectMultiRecords($queryCurrencies);
						
					?>
					
					<!--<tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Login Name<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="loginName" value="<?//=stripslashes($_SESSION["loginName"]); ?>"  size="35" maxlength="32"></td>
          </tr> -->
				<!--	<select name="currency">
						<?php 
						/*	for($y = 0; $y < count($currencies); $y++){
						?>
						<option value="<?php echo $currencies[$y]['currencyName']; ?>" <?php 
							if(isset($rule['currency']) && $rule['currency'] == $currencies[$y]['currencyName'])
								echo "selected='selected'";
							?>
							><?php echo $currencies[$y]['currencyName'] ?></option>
						<?php 
							} */
						?>
					</select> -->
					
					<select name="currency">
						<?php 
						
	if(isset($rule['currency']) && $rule['currency'] != ''){
		?>
	<option value="<?php echo  $rule['currency'];?>"><?php echo  $rule['currency']; ?></option>
	<?
	}		
	    for($d= 0; $d < count($currencies); $d++){
		if(strpos($currencies[$d]['currencyRec'],',')){
		$arr = explode(',',$currencies[$d]['currencyRec']);
		foreach($arr as $val){
		?>
		<option value="<?php echo  $val;?>"><?php echo  $val; ?></option>	
		<?}
		}
		else{
		 ?>
		 
		 <!--<option value="<?php //echo  $currencies[$d]['currencyRec'];?>"><?php //echo  $currencies[$d]['currencyRec']; ?></option>-->
		 
		 <?
		
		}
		
	}
	
	?>
					</select>
					
					
					
					
					<a class="link" href="view_bank_details_rule.php">View Rules</a>
					<p>Select Mandatory Fields for Beneficiary Bank</p>
					<div class="left-col">
						<div class="row">
							<input name="accountName" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['accountName']) && $rule['accountName'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Beneficiary Account Name</label>
						</div>
						<div class="row">
							<input name="accNo" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['accNo']) && $rule['accNo'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Account Number</label>
						</div>
						<div class="row">
							<input name="branchNameNumber" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['branchNameNumber']) && $rule['branchNameNumber'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Branch Name / Number</label>
						</div>
						<div class="row">
							<input name="branchAddress" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['branchAddress']) && $rule['branchAddress'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Branch Address</label>
						</div>
					</div>
					<div class="right-col">
						<div class="row">
							<input name="iban" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['iban']) && $rule['iban'] == 'Y')
										echo "checked";
								?>
							/>
							<label>IBAN</label>
						</div>
						<div class="row">
							<input name="swiftCode" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['swiftCode']) && $rule['swiftCode'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Swift Code</label>
						</div>
						<div class="row">
							<input name="sortCode" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['sortCode']) && $rule['sortCode'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Sort Code</label>
						</div>
						<div class="row">
							<input name="routingNumber" type="checkbox" value="Y" 
								<?php 
									if(isset($rule['routingNumber']) && $rule['routingNumber'] == 'Y')
										echo "checked";
								?>
							/>
							<label>Routing Number</label>
						</div>
					</div>
					<div class="row">
						<label>Rule Status:</label>
						<select name="status">
							<option value="Enable" 
								<?php 
									if(isset($rule['status']) && $rule['status'] == 'Enable')
										echo "selected='selected'";
								?>
							>Enable</option>
							<option value="Disable" 
								<?php
									if(isset($rule['status']) && $rule['status'] == 'Disable')
										echo "selected='selected'";
								?>
							>Disable</option>
						</select>
					</div>
					<div class="row">
						<?php 
							if(isset($ruleID) && isset($_GET['act']) && $_GET['act'] == 'e'){
						?>
							<input name="updateRule" value="Update Rule" type="submit" />
							<input name="ruleID" value="<?php echo $ruleID; ?>" type="hidden"/>
						<?php 
							}else{
						?>
						<input name="addRule" type="submit" value="Add Rule"/>
						<?php 
							}
						?>
					</div>
				<form>
			</div>
		</div>
	</body>
</html>