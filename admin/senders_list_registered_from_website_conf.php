<?php

use Payex\Helper\AgentHelper;
use Payex\Repository\AgentRepository;

require '../src/bootstrap.php';

/**
 * @Package Online Register Users
 * @subpackage New Registered Senders List Processing
 * This page Process the Filter from the New Senders List and deliver data for Display
 * @author Mirza Arslan Baig
 */

session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$user=$agentType;

$agentRepository = new AgentRepository($app->getDb());
$agentHelper = new AgentHelper();
$agent = $agentRepository->getById($parentID);

/**
 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
 * The JSON.php is a standard class which encodes/decodes JSON data requests.
 */
include ("JSON.php");

$response	= new Services_JSON();
$page		= $_REQUEST['page']; // get the requested page
$limit		= $_REQUEST['rows']; // get how many rows we want to have into the grid
$sidx		= $_REQUEST['sidx']; // get index row - i.e. user click to sort
$sord		= $_REQUEST['sord']; // get the direction

if(!$sidx)
{
	$sidx = 1;
}
if($limit == 0)
{
	$limit=50;
}
if(empty($_REQUEST['status']))
	$_REQUEST['status'] = 'All';
$_SESSION['status'] = $_REQUEST['status'];
$_SESSION['customerNumber'] = $_REQUEST['custNum'];
$_SESSION['fdate'] = $_REQUEST['fdate'];
$_SESSION['tdate'] = $_REQUEST['tdate'];
$condition = '';
$action = $_REQUEST['getGrid'];
$bolExportFlag = false;
if($_REQUEST['Submit'] == 'Search'){
	/**
	 * This Piece of Code is for Exporting and Printing Records
	 */
	$bolExportFlag = true;
	if ($action == 'export') {
		header("Content-type: application/x-msexcel");
		header("Content-Disposition: attachment; filename=senders_list_registered_from_website.xls");
		header("Content-Description: PHP/MYSQL Generated Data");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	} elseif($action == 'export' || $action == 'print') {
		$strMainStart = '<table border="1" align="center" cellspacing="0" width="900">';
		$strMainEnd = '</table>';
		$strRowStart = '<tr>';
		$strRowEnd = '</tr>';
		$strColStart = '<td>&nbsp;';
		$strColStartSr = '<td align="left">';
		$strColEndSr = '</td>';
		$strColEnd = '</td>';

		$strHead = "<th colspan='6' style='font-size:20pt'>GB Compliance List</th>";
		$strHeadCols = "<th>Serial No.</th>";
		$strHeadCols .= "<th>Customer Name</th>";
		$strHeadCols .= "<th>Reference Number</th>";
		$strHeadCols .= "<th>Creation Date</th>";
		$strHeadCols .= "<th>Country</th>";
		$strHeadCols .= "<th>Status</th>";
		$strHeadCols .= "<th>User Remarks</th>";
		$strHeadCols .= "<th>Browser Name</th>";
		$strHeadCols .= "<th>Customer Services</th>";
		$strHeadCols .= "<th>Agent Name</th>";
	}
	if(!empty($_SESSION['status']) && $_SESSION['status'] != 'All')
		;//$condition = " AND customerStatus = '{$_SESSION['status']}'";
	elseif($_SESSION['status'] != 'All')
		;//$condition = " AND customerStatus = 'Disable'";
	if(!empty($_SESSION['customerNumber']))
		$condition .= " AND accountName = '{$_SESSION['customerNumber']}'";
	else
		$condition .= " AND accountName LIKE 'CW-%'";
	if ($agentHelper->isAgent($agent)){
		$condition .= " AND agentID = '{$agent->getId()}'";
	}
	if(empty($_SESSION['tdate']))
		$_SESSION['tdate'] = date("Y-m-d");

	if(!empty($_SESSION['fdate']))
		$condition .= " AND (customer.created >= '{$_SESSION['fdate']} 00:00:00' AND customer.created <= '{$_SESSION['tdate']} 23:59:59') ";
	else
		$condition .= " AND (customer.created <= '{$_SESSION['tdate']} 23:59:59') ";

	$condition .= " AND customerType NOT LIKE 'company'";
	$condition .= " AND custCategory = 'O'";
	$query = "SELECT name, agentCompany, date_format(customer.created, '%Y-%m-%d') as creationDate, customerID, customerName, customerStatus, Country, accountName, customerType, userRemarks, online_customer_services, browserName, card_issued FROM customer, admin WHERE EnabledFromNew = '0' AND customer.agentID = admin.userID  AND customerStatus='New'";

	if(!empty($query) && !empty($condition))
		$query .= $condition;
	if(!empty($query)){
		$allCustomersData = mysql_query($query) or die(__LINE__ .":". mysql_error());
		$allCustomersCount = mysql_num_rows($allCustomersData);
		if($allCustomersCount > 0){
			$totalPages = ceil($allCustomersCount / $limit);
		}else{
			$totalPages = 0;
		}
		if($page > $totalPages)
			$page = $totalPages;
		$start = $limit * $page - $limit;

		if($start < 0)
			$start = 0;
		if($bolExportFlag)
			$query .= " ORDER BY customer.created";
		else
			$query .= " ORDER BY $sidx $sord LIMIT $start, $limit";
		$customersData = mysql_query($query) or die(__LINE__.":".mysql_error());
		if($action == 'export' || $action == 'print'){
			$strExportData = $strMainStart.$strRowStart.$strHead.$strRowEnd;
			$strExportData .= $strRowStart.$strHeadCols;
		}else{
			$response->page = $page;
			$response->total = $totalPages;
			$response->records = $allCustomersCount;
		}

		$i = 0;
		while($customer = mysql_fetch_array($customersData)){
			$serialNo = $i +1;
			$customerService = '';
			$cardStatus = '';
			if($customer['online_customer_services'] == 'PC'){
				$customerService = 'Prepaid Customer';
			}else if($customer['online_customer_services'] == 'TC'){
				$customerService = 'Trading Customer';
			}else if($customer['online_customer_services'] == 'PC,TC'){
				$customerService = 'Prepaid & Trading Customer';
			}else {
				$customerService = 'Trading Customer';
			}

			if($customer['card_issued'] == 'Y'){
				$cardStatus = 'Card Already Issued';
			}else{
				$cardStatus = 'Card Issue Request';
			}



			if($action == 'export' || $action == 'print'){
				$strExportData .= $strRowEnd.$strRowStart.$strColStartSr.$serialNo.$strColEndSr;
				$strExportData .= $strColStart.$customer['customerName'].$strColEnd;
				$strExportData .= $strColStart.$customer['accountName'].$strColEnd;
				$strExportData .= $strColStart.$customer['creationDate'].$strColEnd;
				$strExportData .= $strColStart.$customer['Country'].$strColEnd;
				$strExportData .= $strColStart.$customer['customerStatus'].$strColEnd;
				$strExportData .= $strColStart.$customer['userRemarks'].$strColEnd;
				$strExportData .= $strColStart.$customer['browserName'].$strColEnd;
				$strExportData .= $strColStart.$customerService.$strColEnd;
				$strExportData .= $strColStart.$customer['name'].' ( '.$customer['agentCompany'].' )'.$strColEnd;
			}

			if($customer['customerStatus'] != 'Enable')
			{
				$response->rows[$i]['cell'] = array(
				$serialNo,
				$customer['customerName'],
				$customer['accountName'],
				$customer['customerStatus'],
				$customer['creationDate'],
				$customer['Country'],
				'<span style="color:#888">Not editable</span>'
				,'<a href="javascript:void(0)" onClick="IssueCard('.$customer['customerID'].');">'.$cardStatus.'</a>'
				,'<a href="javascript:void(0);" onClick="customerAction('.$customer['customerID'].');">Enable/Disable</a>'
				,$customer['userRemarks']
				,$customer['browserName']
				,$customerService
				,$customer['name'].' ( '.$customer['agentCompany'].' )'
				);
			}else{
				$response->rows[$i]['cell'] = array(
				$serialNo,
				$customer['customerName'],
				$customer['accountName'],
				$customer['customerStatus'],
				$customer['creationDate'],
				$customer['Country']
				,'<a href="add-customer.php?customerID='.$customer['customerID'].'&page=1&country='.$customer['Country'].'"">Edit</a>'
				,'<a href="javascript:void(0)" onClick="IssueCard('.$customer['customerID'].');">'.$cardStatus.'</a>'
				,'<a href="javascript:" onClick="customerAction('.$customer['customerID'].');">Enable/Disable</a>'
				,$customer['userRemarks']
				,$customer['browserName']
				,$customerService
				,$customer['name'].' ( '.$customer['agentCompany'].' )'
				);
			}
			$response->rows[$i]['id'] = 'AA-'.$customer['customerID'];
			$i++;
		}
		if($action == 'export')
			echo $strExportData;
		elseif($action == 'print'){
			echo $strExportData;
			echo '<table width="900" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><input type="button" value="Print" onClick="print();"/></td>
					</tr>
				</table>';
		}else
			echo $response->encode($response);
	}
}

?>