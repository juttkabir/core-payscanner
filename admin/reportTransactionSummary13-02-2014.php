<?
 	session_start();
 	include ("../include/config.php");
 	$date_time = date('d-m-Y  h:i:s A');
 	include ("security.php");
 	include ("javaScript.php");
	$currentDate= date('Y-m-d',strtotime($date_time));
 	$systemCode = SYSTEM_CODE;
 	$company = COMPANY_NAME;
 	$systemPre = SYSTEM_PRE;
 	$manualCode = MANUAL_CODE;
	
 	$agentType = getAgentType();
	//debug($systemPre);
	//debug($manualCode);
 	$parentID = $_SESSION["loggedUserData"]["userID"];
	//debug($parentID);
 	$totalAmount="";
 	$foriegnTotal="";
	
	/**	maintain report logs 
	 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
	 *	If not exist enter it in order to maintain report logs
	 */
	include_once("maintainReportsLogs.php");
	maintainReportLogs($_SERVER["PHP_SELF"]);

 	session_register("fMonth");
 	session_register("fDay");
 	session_register("fYear");
 	session_register("tMonth");
 	session_register("tDay");
 	session_register("tYear");
 	session_register("agentName");
 	session_register("grandTotal");
 	session_register("CurrentTotal");
 	session_register("bankID");
 	
 	if ($offset == "")
 	    $offset = 0;
 	$limit=20;
 	
 	if ($_GET["newOffset"] != "") {
 	    
 	    $offset = $_GET["newOffset"];
 	}
 	$nxt = $offset + $limit;
 	$prv = $offset - $limit;
 	$sortBy = $_GET["sortBy"];
 	if ($sortBy == "")
 	{
 	    $sortBy = "currencyTo, transDate";
 	  }else
 	  {
 	  	$sortBy = " $sortBy";
 	  	}
		
		if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{
			if($_REQUEST["transSource"]!="")
			{
				$transSource = $_REQUEST["transSource"];
			
			}
		}
 	   
 	 //$agentType = getAgentType();

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}
			
/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$parentID."' ) ";
	$extra = " LEFT JOIN ".TBL_CUSTOMER." as c ON vwTrans.customerID = c.customerID".$extraJoin;
}
			
 	////////////////////////////////Making Condition for Report Visibility/////////////
 	$condition = False;
 	    
 	   if(CONFIG_REPORT_ALL == '1')
 	   {
 	        if($agentType != "SUPA" && $agentType != "SUPAI")
 	        {
 	            $condition = True;   
 	        }
 	    }else{
 	            if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager" )
 	            {
 	                $condition = True;
 	            }
 	        
 	       }
 	        
 	//////////////////////////Making of Condition///////////////
 	
 	if($_POST["Submit"] =="Search" || $_GET["search"]=="search")
 	{
 	
 	    $qryString = "search=search";
 	    if($condition)
 	    {
 	    	  
           if($agentType=='SUPI' || $agentType == 'SUPAI'){ 
			  $query = "select * from ". VW_TRANSACTION_SUMMARY . " as vwTrans ,". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID "; 
			 // debug($query);
			  //$query = "select * from ". VW_TRANSACTION_SUMMARY . " as vwTrans INNER JOIN (". VW_BENFICIARY_AGENT . " as vwBenAgent, ".TBL_CUSTOMER.") where 1 and vwTrans.transID = vwBenAgent.transID "; 
 	         $query .= "and vwTrans.transStatus NOT IN('Pending','Processing','Cancelled','AwaitingCancellation') and vwBenAgent.benAgentID=$parentID ";
			// debug($query);
           //$query .= "and vwBenAgent.benAgentID=$parentID";     
           
           $queryCurrency = "select count(*) from ". VW_TRANSACTION_SUMMARY . " as vwTrans ,". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID "; 
           $queryCurrency.= "and vwTrans.transStatus NOT IN('Pending','Processing','Cancelled','AwaitingCancellation') and vwBenAgent.benAgentID=$parentID ";
		 // debug($queryCurrency);
           //$queryCurrency.= "and vwBenAgent.benAgentID=$parentID";          
           
           $queryGroup = "select count(currencyTo) as totalGroup, sum(totalAmount) as grandAmount, sum(transAmount) as totalTransAmount, sum(localAmount) as totalLocalAmount, sum(IMFee) as IMFee, vwTrans.currencyFrom,  vwTrans.currencyTo from ". VW_TRANSACTION_SUMMARY . " as vwTrans ,". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID and vwBenAgent.benAgentID=$parentID "; 
         }
	
          
 	        if($agentType!='SUPI' && $agentType != 'SUPAI'){
			  $query = "select * from ". VW_TRANSACTION_SUMMARY . " as vwTrans ".$extra.",". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID ".$extraCondition; 
			 // debug($query);
			  // The Problem is here for Excel File
		   //$query = "select * from ". VW_TRANSACTION_SUMMARY . " as vwTrans ,". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID "; 
                      	
 	         $queryCurrency = "select count(*) from ". VW_TRANSACTION_SUMMARY . " as vwTrans ".$extra.",". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID ".$extraCondition;
 	      //  debug($queryCurrency);
           $queryGroup = "select count(currencyTo) as totalGroup, sum(totalAmount) as grandAmount, sum(transAmount) as totalTransAmount, sum(localAmount) as totalLocalAmount, sum(IMFee) as IMFee, currencyFrom, currencyTo from ". VW_TRANSACTION_SUMMARY . " as vwTrans ".$extra.",". VW_BENFICIARY_AGENT . " as vwBenAgent where 1 and vwTrans.transID = vwBenAgent.transID ".$extraCondition;
		   // debug($queryGroup);
 	        }
 	       
 	
 	    }
 	    else
 	    {
 	        $queryCurrency = "select count(*) from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where 1 and u.name like '$id%' ".$extraCondition; 	         //debug($queryCurrency);
 	        $query = "select currencyTo,transStatus, transDate, transID, refNumberIM, totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, t.customerID, benID, exchangeRate, localAmount, benAgentID, collectionPointID from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where 1 and u.name like '$id%' ".$extraCondition;
			//debug($query);
 	        //$query = "select currencyTo,transStatus, transDate, transID, refNumberIM, totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, t.customerID, benID, exchangeRate, localAmount, benAgentID, collectionPointID, a.name AS adminName from ". TBL_TRANSACTIONS . " as t, " . TBL_ADMIN_USERS . " as a," . TBL_CUSTOMER . " as c where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'and t.customerID = c.customerID " ;
			
 	        $queryGroup = "select count(currencyTo) as totalGroup, sum(totalAmount) as grandAmount, sum(transAmount) as totalTransAmount, sum(localAmount) as totalLocalAmount, t.currencyFrom t.currencyTo from ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID LEFT JOIN ". TBL_ADMIN_USERS ." as u ON t.custAgentID =  u.userID ".$extraJoin." where 1 and u.name like '$id%' ".$extraCondition;
 	       
 	    }
 //	debug($queryGroup);
 	    if($_GET["search"]!="search")
 	    {       
 	        $_SESSION["fMonth"]="";
 	        $_SESSION["fDay"]="";
 	        $_SESSION["fYear"]="";
 	        
 	        $_SESSION["tMonth"]="";
 	        $_SESSION["tDay"]="";
 	        $_SESSION["tYear"]="";
 	        
 	        $_SESSION["agentName"]="";
 	        $_SESSION["bankID"]="";
 	        
 	        $_SESSION["grandTotal"]="";
 	        $_SESSION["CurrentTotal"]="";   
 	
 	       /* $_SESSION["fMonth"]=$_POST["fMonth"];
 	        $_SESSION["fDay"]=$_POST["fDay"];
 	        $_SESSION["fYear"]=$_POST["fYear"];
 	        
 	        $_SESSION["tMonth"]=$_POST["tMonth"];
 	        $_SESSION["tDay"]=$_POST["tDay"];
 	        $_SESSION["tYear"]=$_POST["tYear"];*/
			
			$fromDatevalue=$_POST["fromDate"];
			$toDatevalue=$_POST["toDate"];
			if(!empty($fromDatevalue)){
				$fromDatevalue = str_replace('/', '-', $fromDatevalue);
				$fromDate= date("Y-m-d",strtotime($fromDatevalue));
			}
			else
				$fromDate=$currentDate;
			if(!empty($toDatevalue)){
				$toDatevalue = str_replace('/', '-', $toDatevalue);
				$toDate= date("Y-m-d",strtotime($toDatevalue));
			}
			else
				$toDate=$currentDate;
				
 	        $_SESSION["agentName"]=$_POST["agentName"];
 	        $_SESSION["bankID"]=$_POST["bankID"];
 	    }
 	    /**
		 * #9898 : PremierFX Currency Filter
		 * Short Description
		 * @variable used for currency filter
		 */
		 $Tocurrency = $_REQUEST['Tocurrency'];
		 
		 /**
		 * #7888: Premeir FX - Agent Filter Applied
		 * Short Description
		 * This Code Adds the Agent Filter in Search
		 */
		 $agentName = $_SESSION["agentName"];
		
 	    if($_POST["cashier"]!="")
 	        $cashier = $_POST["cashier"];
 	        elseif($_GET["cashier"]!="") {
 	            $cashier = $_GET["cashier"];
 	                }
 	        $qryString .= "&cashier=".$cashier;   
 	            
 	           
 	            if($_POST["paymentMode"]!="")
 	        $paymentMode = $_POST["paymentMode"];
 	        elseif($_GET["paymentMode"]!="") {
 	        $paymentMode = $_GET["paymentMode"];
 	        
 	        }
 	        $qryString .= "&paymentMode=".$paymentMode;
 	       
 	        if($_POST["transactionType"]!="")
 	        $transactionType = $_POST["transactionType"];
 	        elseif($_GET["transactionType"]!="") {
 	        $transactionType = $_GET["transactionType"];
 	        
 	        }
 	        $qryString .= "&transactionType=".$transactionType; 
			
			 if($_POST["amountType"]!=""){
 	        	$amountType = $_POST["amountType"];
 	        }elseif($_GET["amountType"]!="") {
 	        	$amountType = $_GET["amountType"];
 	        }
 	        
			  if($_POST["amountCriteria"]!=""){
 	        	$amountCriteria = $_POST["amountCriteria"];
 	        }elseif($_GET["amountCriteria"]!="") {
 	        	$amountCriteria = $_GET["amountCriteria"];
 	        }
			if($_POST["amount"]!=""){
 	        	$amount = $_POST["amount"];
 	        }elseif($_GET["amount"]!="") {
 	        	$amount = $_GET["amount"];
 	        }
 	        
 	        if($_POST["amountType"]!=""){
 	        	$amountType = $_POST["amountType"];
 	        }elseif($_GET["amountType"]!="") {
 	        	$amountType = $_GET["amountType"];
 	        }
			
			if(!empty($_REQUEST["transactionStatus"]))
				$transactionStatus = $_REQUEST["transactionStatus"];
				
			$qryString .= "&transactionStatus=".$transactionStatus;
			
			if($stat != "") {
 	        $query .= " and (transStatus='".$stat."')" ;
 	        $queryCurrency .= " and (transStatus='".$stat."')" ;
 	        $queryGroup .= " and (transStatus='".$stat."')" ;
 	    }  
		
 	   if(!empty($distributor) && $distSpecificNum){
					//				debug($query);
					//			debug($queryCurrency);
				//				debug($queryGroup);
 	        $query .= " and (vwTrans.benAgentID='".$distributor."')" ;
 	        $queryCurrency .= " and (vwTrans.benAgentID='".$distributor."')" ;
 	        $queryGroup .= " and (vwTrans.benAgentID='".$distributor."')" ;
 	    }  
		
 	 		if($amount != ""){
 	 			if($amountType == "sent") {
 	 				$type = "transAmount";
 	 			}elseif($amountType == "foriegn"){
 	 				$type = "localAmount";
 	 			}elseif($amountType == "afterCommission"){
 	 				$type = "totalAmount";
 	 			}
				
 				
			}
 	    if($_POST["currency"]!="")
 	        $currency = $_POST["currency"];
 	        elseif($_GET["currency"]!="") {
 	            $currency = $_GET["currency"];
 	        
 	        }
 	        $qryString .= "&currency=".$currency;
 	        
 	        if($_POST["agentName"] != "")
 	   		 {
 	        $agentName = $_POST["agentName"];
 	        }
 	        elseif($_GET["agentName"]!="") {
 	            $agentName = $_GET["agentName"];
 	        }   
 	        $qryString .= "&agentName=".$agentName;
 	        
			
 	        if($_POST["Tocountry"] != "")
 	   		 {
 	        $Tocountry = $_POST["Tocountry"];
 	        }
 	        elseif($_GET["Tocountry"]!="") {
 	            $Tocountry = $_GET["Tocountry"];
 	        }   
 	        $qryString .= "&Tocountry=".$Tocountry;
 	        
 	        
 	        if($_POST["bankID"]!="")
 	        {
 	            $bankID = $_POST["bankID"];
 	        }elseif($_GET["bankID"]!="") {
 	            $bankID = $_GET["bankID"];
 	        }   
 	        $qryString .= "&bankID=".$bankID;
			
			/**
			 * #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
			 * Short Description
			 * This @var is String for Retain the value in Field through Pagination
			*/
			$strForPagination = "&agentName=".$_REQUEST['agentName']."&cashier=".$_REQUEST['cashier']."&transactionType=".$_REQUEST['transactionType']."&bankID=".$_REQUEST['bankID']."&Tocountry=".$_REQUEST['Tocountry']."&paymentMode=".$_REQUEST['paymentMode']."&transactionStatus=".$_REQUEST['transactionStatus']."&Tocurrency=".$_REQUEST['Tocurrency']."&supAgentID=".$_REQUEST['supAgentID']."&search=search";
 	    
 	  //  $fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
 	  // $toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
 	    
 	    $queryDate = "(transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
 	    
 	
 	    $query .=  " and $queryDate";               
 	    $queryCurrency .= " and $queryDate";
 	     $queryGroup .=  " and $queryDate";        
 	    if($agentName!= "")
 	    {
 	        if($agentName != "all")
 	        {
				
			 /**
			  * # 7888 : Premier Exchange - Sort By Admin Staff 
			  * Short Description
			  * This @var for Query is Changed for sort by Admin Staff Name
			  */
			  /*$query .= " and vuserID='".$agentName."' ";
 	            $queryCurrency .= " and userID='".$agentName."' ";
 	            $queryGroup .= " and userID='".$agentName."' ";*/
			  
				$query .= " and vwTrans.userID='".$agentName."' ";
 	            $queryCurrency .= " and userID='".$agentName."' ";
 	            $queryGroup .= " and userID='".$agentName."' ";
 	        }
 	    }
 	    
 	   
			
 	    
 	    if($bankID != "")
 	    {
 	    	   if( $agentType!="SUPI" && $agentType!="SUPAI" ){ 
 	        if($bankID != "all")
 	        {
 	            $query .= " and vwTrans.benAgentID='".$bankID."' ";
 	            $queryCurrency .= " and vwTrans.benAgentID='".$bankID."' ";
 	            $queryGroup .= " and vwTrans.benAgentID='".$bankID."' ";
 	        }
 	      }
 	    }
 	    
 	    if( $agentType!="SUPI" && $agentType!="SUPAI" ){ 
 	    if($Tocountry != "") {
 	        $query .= " and (toCountry='".$Tocountry."')" ;
 	        $queryCurrency .= " and (toCountry='".$Tocountry."')" ;
 	        $queryGroup .= " and (toCountry='".$Tocountry."')" ;
 	    }
 	  }
	  
 	    if($Tocurrency != "") {
 	        $query .= " and (currencyTo='".$Tocurrency."' or currencyFrom='".$Tocurrency."')" ;
 	        $queryCurrency .= " and (currencyTo='".$Tocurrency."' or currencyFrom='".$Tocurrency."')" ;
 	        $queryGroup .= " and (currencyTo='".$Tocurrency."' or currencyFrom='".$Tocurrency."')" ;
 	    }
 	    
 	    
 	if($cashier != "") {
 	
 	    $query .= " and (addedBy = '".$cashier."')" ;
 	    $queryCurrency .= " and (addedBy = '".$cashier."')" ;
 	    $queryGroup .= " and (addedBy = '".$cashier."')" ;
 	    }
 	
 	    
 	    if($paymentMode != "") {
 	        $query .= " and (moneyPaid='".$paymentMode."')" ;
 	        $queryCurrency .= " and (moneyPaid='".$paymentMode."')" ;
 	        $queryGroup .= " and (moneyPaid='".$paymentMode."')" ;
 	    }
 	    
 	     if($transactionType != "") {
 	        $query .= " and (transType='".$transactionType."')" ;
 	        $queryCurrency .= " and (transType='".$transactionType."')" ;
 	        $queryGroup .= " and (transType='".$transactionType."')" ;
 	    }
		
		if($transactionStatus != "") {
 	        $query .= " and (transStatus='".$transactionStatus."')" ;
 	        $queryCurrency .= " and (transStatus='".$transactionStatus."')" ;
 	        $queryGroup .= " and (transStatus='".$transactionStatus."')" ;
 	    }
		if($amountCriteria == "BETWEEN" || $amountCriteria == "NOT BETWEEN"){
					
 					$amountArray = split('-',$amount);
 					$query .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ".$amountArray[1].")" ;
 	        $queryCurrency .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ".$amountArray[1].")" ;
 	        $queryGroup .= " and (".$type." ".$amountCriteria." ".$amountArray[0]." AND ".$amountArray[1].")" ;
 	      }else if($amountCriteria!=''){
			
 	      	$query .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	        $queryCurrency .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	        $queryGroup .= " and (".$type." ".$amountCriteria." ".$amount.")" ;
 	      }
		  
		if($transSource == "O")
		{
			$query.= " and trans_source = '".$transSource."' ";
			$queryCurrency.= " and trans_source = '".$transSource."' ";
			$queryGroup.= " and trans_source = '".$transSource."' ";

		}
		elseif($transSource != "O" && $transSource != "")
		{
			$query.= " and trans_source != 'O' ";
			$queryCurrency.= " and trans_source != 'O' ";
			$queryGroup.= " and trans_source != 'O' ";

		}
		  
 	    
		
		/**
		 * # 7888 : Filter For the Logged in and associated agents transactions ab
		 */
		if($agentType == 'Admin' || $agentType == "Admin Manager" || $agentType == "Branch Manager" ){
		//debug($arrInput);
		 	$arrInput =  array("userID"=>$parentID);
			//debug($arrInput);
			$associatedUserIds = linkAgentList($arrInput);
			//debug($associatedUserIds);
			if(empty($associatedUserIds))
				$associatedUserIds = "''";
			$query .= " and vwTrans.userID IN (".$associatedUserIds.")";
		}
		
 	    if ($sortBy !="")
 	  		$query .= " order by $sortBy ASC ";

	   	$queryExport =	$query;
		$query .= " LIMIT $offset , $limit";
		//debug($query);
	   	$contentsTrans = selectMultiRecords($query);
	
		$allCount = countRecords($queryCurrency);  
 	// $allCount = count($contentsTrans);  
	// debug($allCount);
 	    ////////Group Query////
		
		$queryGroupEnd = " group by currencyFrom,currencyTo order by currencyTo ASC";
		
		$queryGroupSendingCurrencyCancelled .= $queryGroup."  AND (transStatus = 'Cancelled' OR transStatus = 'Cancelled - Returned') ".$queryGroupEnd;
 	    $queryGroup .= " AND transStatus != 'Cancelled' and transStatus != 'Cancelled - Returned' ".$queryGroupEnd;
		//debug($queryGroup);
		$contentsTransCancelled = selectMultiRecords($queryGroupSendingCurrencyCancelled); 	 
 	    $contentsTransGroup = selectMultiRecords($queryGroup);
 	  //  debug($contentsTransGroup);
 	    	
 	}
 	else
 	{
 		$_SESSION["fMonth"]="";
		$_SESSION["fDay"]="";
		$_SESSION["fYear"]="";
		
		$_SESSION["tMonth"]="";
		$_SESSION["tDay"]="";
		$_SESSION["tYear"]="";
		
		$_SESSION["agentName"]="";
		$_SESSION["bankID"]="";
		
		$_SESSION["grandTotal"]="";
		$_SESSION["CurrentTotal"]="";   
 	    $todate = date("Y-m-d");
 	    
 	}
	//debug($queryGroup);
	//debug($Tocurrency);
	//debug($query);
	//debug($queryCurrency);
	//debug($queryExport);
 	?>
 	<html>
<head>
<title>Report Transaction Summary</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<?php 
/**
* # 7888: Premeir FX - Admin Staff and Correspoding Agents
* Short Description
* This Config Enable / Disable the Agent Staff Field and Corresponding Field and if any admin staff is loign his sub agents are shown
*/
if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1" && (!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))){
?>
	<script language="javascript" src="./javascript/jquery.selectboxutils.js"></script>
<?php 
}
?>
<script language="javascript">
	$(document).ready(function(){
		$("#printReport").click(function(){
			// Maintain report print logs 
			$.ajax({
				url: "maintainReportsLogs.php",
				type: "post",
				data:{
					maintainLogsAjax: "Yes",
					pageUrl: "<?=$_SERVER['PHP_SELF']?>",
					action: "P"
				}
			});
			
			$("#searchFilter").hide();
			$("#paginationTable").hide();
			$("#actionBtnRow").hide();
			print();
			$("#searchFilter").show();
			$("#paginationTable").show();
			$("#actionBtnRow").show();
		});
	});
 	function SelectOption(OptionListName, ListVal)
 	{
 	    for (i=0; i < OptionListName.length; i++)
 	    {
 	        if (OptionListName.options[i].value == ListVal)
 	        {
 	            OptionListName.selectedIndex = i;
 	            break;
 	        }
 	    }
 	}
	function export_report() {
		var condition=document.getElementById("condition").value;
		var query=document.getElementById("query").value;
		var queryCurrency=document.getElementById("queryCurrency").value;
		var queryGroup=document.getElementById("queryGroup").value;
		var queryGroupSendingCurrencyCancelled=document.getElementById("queryGroupSendingCurrencyCancelled").value;
		var dfFlag=document.getElementById("dfFlag").value;
		window.location = "ExportRTS.php?condition="+condition+"&query="+query+"&queryCurrency="+queryCurrency+"&queryGroup="+queryGroup+"&queryGroupSendingCurrencyCancelled="+queryGroupSendingCurrencyCancelled+"&dfFlag="+dfFlag+"&pageUrl=<?=$_SERVER['PHP_SELF']?>";
	}   
function allowNumber(e)
	{
		var key;
		var keychar;
		
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		
		// control keys
		if ((key==null) || (key==0) || (key==8) || 
		    (key==9) || (key==13) || (key==27) )
		   return true;
		   // numbers
		else if ((("0123456789.-").indexOf(keychar) > -1))
   		return true;
		else
		   return false;
	}

<?php 
/**
 * # 7888: Premeir FX - Admin Staff and Correspoding Agents
 * Short Description
 * This Config Enable / Disable the Agent Staff Field and Corresponding Field and if any admin staff is loign his sub agents are shown
 */
if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1" && (!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))){
?>
function Associated_Introducers(){
	var bolToReturn;	
	var dataToPass = {
		AccountMangr: $("#supAgentID").val(),
		agentID: '<?=$_SESSION['agentID']?>',
		customerID: '',
		agentType: '<?php echo $agentType; ?>',
		selectedAgentID: '<?php echo $agentName; ?>',
		Submit: 'Validate', 
	
	};
	
	$.ajax({
		url: "AsocAgents.php",
		async: false,
		data: dataToPass,
		success: function(data){ 
			$("#agentName").html(data);
		}
	});
}
$(document).ready(function(){
	Associated_Introducers();
	$('#supAgentID').change(function(){
			Associated_Introducers();
	});
});
<?php 
	}
?>
</script>
<script language="javascript">
  function SelectOption(OptionListName, ListVal)
 	{
 	    for (i=0; i < OptionListName.length; i++)
 	    {
 	        if (OptionListName.options[i].value == ListVal)
 	        {
 	            OptionListName.selectedIndex = i;
 	            break;
 	        }
 	    }
 	}
	function checkForm(Search) {
//alert("you are in ");
if(document.Search.amount.value !='')
{
	var x=document.Search.amount.value;
var amountnum=x.indexOf("-");
if (amountnum<1&&(document.Search.amountCriteria.value=='BETWEEN')||amountnum<1&&(document.Search.amountCriteria.value=='NOT BETWEEN'))
  {
  alert("Not a valid Amount");
  return false;
  }
  
}
		/*if((document.Search.fromDate.value != "" && document.Search.toDate.value == "") || (document.Search.fromDate.value == "" && document.Search.toDate.value != "")){
    	alert("Please select from date and to date.");
        Search.fromDate.focus();
        return false;
  	}*/
		if(!((document.Search.amount.value == "" && document.Search.amountType.value == "" && document.Search.amountCriteria.value == "") || (document.Search.amount.value != "" && document.Search.amountType.value != "" && document.Search.amountCriteria.value != ""))){
    	alert("Please select all filters for amount search.");
        Search.amountType.focus();
        return false;
  	}
  	  	 	
  	return true;
  }
  
  function validateForm()
{
	
var x=document.Search.amount.value;
var amountnum=x.indexOf("-");
if (amountnum<1 || amountnum>1)
  {
  alert("Not a valid Amount");
  return false;
  }
}
 	function CheckAll()
 	{
 	
 	    var m = document.trans;
 	    var len = m.elements.length;
 	    if (document.trans.All.checked==true)
 	    {
 	        for (var i = 0; i < len; i++)
 	         {
 	              m.elements[i].checked = true;
 	         }
 	    }
 	    else{
 	          for (var i = 0; i < len; i++)
 	          {
 	               m.elements[i].checked=false;
 	          }
 	        }
 	}   
	function strstr(haystack, needle, bool) {
  
  var pos = 0;

    haystack += "";
    pos = haystack.indexOf(needle); if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}
  
  
</script>

<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
</style>
</head>
<body>
 	<table width="100%" border="0" cellspacing="1" cellpadding="5">
 	  <tr>
 	    <td class="topbar"> Report Transaction Summary</td>
 	  </tr>
 	 
 	  <tr>
 	    <td align="center"><br>
 	          <form action="reportTransactionSummary.php" method="post" name="Search"  onSubmit="return checkForm(this); validateForm(this);">
 	        <table border="1" bordercolor="#666666">
 	            <tr id="searchFilter" ><td  valign="top"><table border="0" cellpadding="5" bordercolor="#666666" width="100%" >
 	     
 	      <tr>
 	            <td width="100%" colspan= "4" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters
 	              </strong></span></td>
 	        </tr>
 	        <tr>
 	        <td align="center" nowrap colspan="4">
 	        From <input name="fromDate" type="text" id="fromDate"  value="" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.fromDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
		
        To	<input name="toDate" type="text" id="toDate"  value="" readonly>
		    		    &nbsp;<a href="javascript:show_calendar('Search.toDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
 	        
 	       
 	      </td></tr>
		   <tr>
 	    		<td colspan="4" align="center">
           		<font color="#FF0000" size=1>Note: If amount Criteria selected is "BETWEEN or NOT BETWEEN" then amount should be entered as 2000-5000 otherwise enter a single value as 2000 </font>
          </td>
 	    	</tr>
 	    	<tr>
 	    		<td colspan="4" align="center">Sending/Foreign Amount
 	    			<input name="amount" type="text" value="<?=$amount?>" onKeyPress="return allowNumber(event)">
		  			<select name="amountCriteria" id="amountCriteria">
            		<option value="">-Amount Criteria- </option>
            		<!--option value="=">Equal To</option-->
            		<option value="<">Less Than</option>
            		<option value=">">Greater Than</option>
            		<option value="<=">Less Than or Equal To</option>
            		<option value=">=">Greater Than or Equal To</option>
            		<option value="!=">Not Equal To</option>
            		<option value="BETWEEN">BETWEEN</option>
            		<option value="NOT BETWEEN">NOT BETWEEN</option>
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].amountCriteria, "<?=$amountCriteria; ?>");</script>	
							
						<select name="amountType" style="font-family:verdana; font-size: 11px;">
		    				<option value="">-Select Amount- </option>
	    					<option value="sent">Amount Sent</option>
	    					<option value="foriegn">Foriegn Amount</option>
	    					<option value="afterCommission">Amount After Commission</option>
        			</select>
      				<script language="JavaScript">SelectOption(document.forms[0].amountType, "<?=$amountType; ?>");</script>
           </td>
            
 	    	</tr>
 	        <?
 	        //if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
 	        /*if($condition)
 	        {*/
 	        ?>
			 <? $adminStaffList		=	"'Admin','Call','COLLECTOR','Collector','Branch Manager','Admin Manager','Support','SUPI Manager','MLRO'";
			 /**
			  * #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
			  * Short Description
			  * This array is changed for retain the index after search
			  */
				$argumentsArr		=	array("userType"=>"populateAdminStaff","adminStaffList"=>$adminStaffList,"allOption"=>"Y", "selectedID"=>$_REQUEST['supAgentID']);
				//debug($argumentsArr);
				if(!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))
				{
			 ?>
					 <tr>
						<td>Admin Staff</td>
						<td><?php echo gateway("CONFIG_POPULATE_ADMINSTAFF_DROPDOWN", $argumentsArr, "1");?></td>
					 </tr>
				<?php 
				}?>
				 
				 <tr>
				 	<td><?=__("Agent"); ?> Name </td>
					<?php 
						/**
						 * # 7888: Premeir FX - Admin Staff and Correspoding Agents
						 * Short Description
						 * This Config Enable / Disable the Agent Staff Field and Corresponding Field and if any admin staff is loign his sub agents are shown
						 */
						if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1" && (!strstr(CONFIG_ADMIN_STAFF_LIST, $agentType.","))){
					?>
					<td>
						<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          				<option value="all">All</option>
 	          			</select>
					<?php 
						}
						elseif($agentType == 'Admin' || $agentType == "Admin Manager" || $agentType == "Branch Manager" ){
					?>
						<td>
							<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          					<option value="">- Select Agent-</option>
								<!--
									#9898: Premier Exchange:RTS is not working / Filter Values are not Retained
									option all has no value
									Give this option 'all' value for retain this value in pagination and inner pages
								-->
	         					<option value="all">All</option>
 	            			<?
								/**
								 * #7888: Premier Exchange
								 * Short Description
								 * @var $assocAgentsIDs is for the associated users ids
								 * @array $arrAgentsID of Associated IDs
								 * @var $intNumberAgents is the counter for associated agents
								 */
								/*$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
								if($agentType == "Branch Manager")
 	                        	{
									$agentQuery .= " and parentID = '$parentID'";                   
 	                            }
 	                            $agentQuery .= "order by agentCompany";*/
								$arrInput = array("userID" => $parentID);
								$assocAgentsIDs = linkAgentList($arrInput);
								//debug($assocAgentsIDs);
								$arrAgentsID = explode(",", $assocAgentsIDs);
								$intNumberAgents = count($arrAgentsID);
								for($x = 0; $x < $intNumberAgents; $x ++){
									$agentQuery = "SELECT userID, name, username, agentCompany FROM ".TBL_ADMIN_USERS." WHERE userID = ".$arrAgentsID[$x];
									//debug($agentQuery);
									$assocAgent = selectFrom($agentQuery);
									echo "<option value='".$assocAgent['userID']."'>".$assocAgent['agentCompany']." [".$assocAgent['username']."] </option>";
								}
								
 	                	/*$agents = selectMultiRecords($agentQuery);
 	                	for ($i=0; $i < count($agents); $i++){
 	            		?>
 	            			<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            		<?
 	                	}*/
 	            		?>
 	          			</select>
					<?php 
						}
						else
						{
						
							/**
							 * # 7888: Premier Exchange
							 * Short Description
							 * This @condition is for super Admin without Config 'CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS'
							 */
					?>
							<td>
							<select name="agentName" id="agentName" style="font-family:verdana; font-size: 11px;">
 	          					<option value="">- Select Agent-</option>
								<option value="all">All</option>
								
					<?php 
								$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
								//debug($agentQuery);
								if($agentType == "Branch Manager")
 	                        	{
									$agentQuery .= " and parentID = '$parentID'";                   
 	                            }
 	                            $agentQuery .= "order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
		 	                	for ($i=0; $i < count($agents); $i++){
 	    		        		?>
 	            				<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
 	            				<?
 	                			}
						}
					?>
						</select>
 	          			<script language="JavaScript">
 	        				SelectOption(document.Search.agentName, "<?=$_SESSION["agentName"]; ?>");
 	            		</script>
 	            	</td>
 	            <? //debug($agentQuery); ?>
 	            <!-- currency -->
 	             <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
 	          <td>
 	            <?
 	                $queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES." where 1 and currency!=''";
 	                $DestCurrencyData = selectMultiRecords($queryDestCurrency);
 	                    
 	                 $queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES." where 1 and currencyOrigin=''";
 	                 $OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
 	                    
 	                    for($a = 0; $a < count($DestCurrencyData); $a++)
 	                {
 	                 $allCurrency[$a] = $DestCurrencyData[$a]['currency'];
 	                            
 	                    $y++;
 	                
 	                }
 	            
 	                for($b = 0; $b < count($OriginCurrencyData); $b++)
 	                {
 	                        $allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
 	                    
 	                $y++;
 	                }
 	                $uniqueCurrency = array_unique($allCurrency);
 	                
 	        
 	            $uniqueCurrency = array_values ($uniqueCurrency);
 	        	//debug($_REQUEST['Tocurrency']);
 	                ?>  
 	                    Currency </td><td>
 	                    <select name="Tocurrency" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value="">-Select Currency-</option>
 	               
 	                   
 	                    <?
 	                    
 	                    for($c = 0; $c < count($uniqueCurrency); $c++)
 	                    {?>
 	                         <option value="<?=$uniqueCurrency[$c]?>" <?php if($_REQUEST['Tocurrency'] == $uniqueCurrency[$c]) echo "selected";?> ><?=$uniqueCurrency[$c]?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	       
 	       
 	        <script language="JavaScript">SelectOption(document.forms[0].Tocurrency, "<?=$Tocurrency?>");
 	          </script>  <? } ?>
 	           <!-- currency  --> 
 	           <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
 	            <td>
 	            	Bank Name </td><td>
 	              <select name="bankID" style="font-family:verdana; font-size: 11px">
 	                <option value="">- Select Bank-</option>
 	                	<? if($agentType != "SUPI Manager") {?>
 	                  <option value="all">All Banks</option>
 	                
 	                 <?
 	                   }
 	                    $banks = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'N' order by agentCompany");
					
 	                    for ($i=0; $i < count($banks); $i++){
 	                ?>
 	                <option value="<?=$banks[$i]["userID"]; ?>"><? echo($banks[$i]["agentCompany"]." [".$banks[$i]["username"]."]"); ?></option>
 	                <?
 	                  }  }
 	                ?>
 	             </select>         
 	             <script language="JavaScript">
 	        SelectOption(document.Search.bankID, "<?=$_SESSION["bankID"]; ?>");
 	            </script>
 	        </td>  </tr>
 	        <tr><td>
 	            <?
 	                $queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES." where 1 ";
 	                    $DestCurrencyData = selectMultiRecords($queryDestCurrency);
 	                    
 	                 $queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES." where 1 ";
 	                    $OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
 	                    
 	                    for($a = 0; $a < count($DestCurrencyData); $a++)
 	                {
 	                 $allCurrency[$a] = $DestCurrencyData[$a]['currency'];
 	                            
 	                    $y++;
 	                
 	                }
 	            
 	                for($b = 0; $b < count($OriginCurrencyData); $b++)
 	                {
 	                        $allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
 	                    
 	                $y++;
 	                }
 	                $uniqueCurrency = array_unique($allCurrency);
 	                
 	        
 	            $uniqueCurrency = array_values ($uniqueCurrency);
				 	            
 	                ?>  <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
 	                    Currency </td><td>
 	                    <select name="Tocurrency" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value="">-Select Currency-</option>
 	               
 	                   
 	                    <?
 	                    $selected = "selected='selected'";
 	                    for($c = 0; $c < count($uniqueCurrency); $c++)
 	                    {
						?>
 	                         <option value="<?=$uniqueCurrency[$c]?>" <?php if($_REQUEST['Tocurrency'] == $uniqueCurrency[$c]){ echo $selected; } ?> ><?=$uniqueCurrency[$c]?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	       <script language="JavaScript">
		   /**
		    * #9898: Premier Exchange:RTS is not working / Filter Values are not Retained
			* Short Description
			* This code is changed for the retain of the value of field after search
		   */
		   //SelectOption(document.forms[0].Tocurrency, "<?=$Tocurrency?>");
 	            </script><? } ?>
 	           
 	        </td>
 	        <? $agentType; if( $agentType!="SUPI" && $agentType!="SUPAI" ){ ?>
 	            <td>
 	            Destination Country </td><td align="left">
 	            <select name="Tocountry" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value=""> - Select Country - </option>
 	                <?
 	                    $queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType Like '%destination%' ";
 	                    $countryData = selectMultiRecords($queryCountry);
 	                    for($k = 0; $k < count($countryData); $k++)
 	                    {?>
 	                         <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>   
 	                    <?
 	                  }}
 	                ?>
 	        </select>
 	        <script language="JavaScript">SelectOption(document.forms[0].Tocountry, "<?=$Tocountry?>");
 	            </script>
 	           
 	                   <br><br><?
					   
					   /**
					    * #7888: Premier FX- account manager performance RTS
						* Short Description
						* This Query is Changed to Get the Agents Except Who is logged in
						*/
			//$cashierType =  "select name,username,adminType from ".TBL_ADMIN_USERS." where adminType IN ('call','Agent','admin') ";			
 	        $cashierType =  "select name,username,adminType from ".TBL_ADMIN_USERS." where adminType IN ('call','Agent','admin') AND userID != '$parentID'";
 	                    $cashierTypeQuery= selectMultiRecords($cashierType);
 	                    
 	                $cashierTypeTeller =  "select loginName,name from ".TBL_TELLER." where 1 ";
 	                    $cashierTypeQueryTeller= selectMultiRecords($cashierTypeTeller);
 	                
 	                for($k = 0; $k < count($cashierTypeQuery); $k++)
 	                {
 	                     $cashierAdminType[$k] = $cashierTypeQuery[$k]['adminType'];
 	                      
 	                     $cashierUsername[$k]= $cashierTypeQuery[$k]['name'];
 	                      $cashierName[$k]= $cashierTypeQuery[$k]['username'];
 	                    // $cashierName[$k]= $cashierTypeQuery[$k]['username'];
 	                    
 	                    
 	                    $x++;
 	                
 	                }
 	            
 	                for($j = 0; $j < count($cashierTypeQueryTeller); $j++)
 	                {
 	                    $cashierUsername[$x]= $cashierTypeQueryTeller[$j]['name'];
 	                    $cashierAdminType[$x] = 'Teller';
 	                $x++;
 	                }
 	                        
 	            
 	                        
 	                ?>
 	            </td></tr>
 	            <tr><td>
 	        Creator </td><td>
 	        <select name="cashier" style="font-family:verdana; font-size: 11px; width:130">
 	                <option value=""> - Select Creator - </option>
 	                <?
 	                    //$cashierID = "select addedBy from ".TBL_TRANSACTIONS." where 1  ";
 	                    //$cashierIDQuery= selectMultiRecords($cashierID);
 	                    
 	                
 	                
 	                    for($j = 0; $j < $x ; $j++)
 	                    {?>
 	                         <option value="<?=$cashierName[$j]?>"><?=$cashierAdminType[$j]." [".$cashierUsername[$j]."]"?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	   
 	        <script language="JavaScript">SelectOption(document.forms[0].cashier, "<?=$cashier?>");
 	            </script>
 	        </td><td>
 	        Payment Mode </td><td>
 	        <select name="paymentMode" style="font-family:verdana; font-size: 11px;">
 	                <option value="">-Select Transaction Type-</option>
 	                <?
 	                    $queryMoneyPaid = "select distinct(moneyPaid) from ".TBL_TRANSACTIONS." where 1 and moneyPaid!='' ";
 	                    $queryData = selectMultiRecords($queryMoneyPaid);
 	                    for($k = 0; $k < count($queryData); $k++)
 	                    {?>
 	                         <option value="<?=$queryData[$k]['moneyPaid']?>"><?=$queryData[$k]['moneyPaid']?></option>   
 	                    <?
 	                    }
 	                ?>
 	        </select>
 	        <script language="JavaScript">SelectOption(document.forms[0].paymentMode, "<?=$paymentMode?>");
 	            </script>
 	       
 	    </td></tr>
 	    <tr>
 	    	<td>Transaction Type </td>
			<td><select name="transactionType" style="font-family:verdana; font-size: 11px;">
 	                <option value="">-Select Transaction Type-</option>
					<option value="Pick up">Pick up</option>   
					<option value="Bank Transfer">Bank Transfer</option>  
					<option value="Home Delivery">Home Delivery</option>       
 	        	</select>
 	       		<script language="JavaScript">SelectOption(document.forms[0].transactionType, "<?=$transactionType?>");</script>
 	    	</td>
			<td>Transaction Status</td>
			<td>
				<?php
					$transStatusQuery = "SELECT DISTINCT(transStatus) From transactions WHERE 1"; 
					$transStatuArr = SelectMultiRecords($transStatusQuery);
				?> 
				<select name="transactionStatus">
					<option value="">- Select -</option>
					<?php 
					foreach($transStatuArr as $key=>$val)
						echo "<option value='".$val["transStatus"]."'>".$val["transStatus"]."</option>";
					?>
				</select>
				<script language="JavaScript">SelectOption(document.forms[0].transactionStatus, "<?=$transactionStatus?>");</script>
			</td>
 	    	
 	    </tr>
		<tr>
 	    	<td>Transaction Source</td>
			<td><? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
		{ 
		
		?>
            <select name="transSource">
                <option value=""> - Transaction Source - </option>
                <option value="O">Online</option>
                <option value="P">Payex</option>
                
            
            </select>
            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
            <? } ?>
 	    	</td>
		</tr>
 	    <tr>
		<td colspan= "4" align="center"><input type="submit" name="Submit" value="Search"></td>
		</tr>
 	     
 	   </tr>
	</td>
 </table>
</table>
 	     </form>
 	      <br>
 	      <br>
 	      <br>
 	
 	        <form action="reportTransactionSummary.php" method="get" name="trans">
 	
 	
 	         
 	        <?
 	//$contentsTrans2 = selectMultiRecords($queryCnt);   
 	//$allCount =count($contentsTrans2);
	//debug($allCount);
 	        if($allCount > 0)
 	        {
				//debug($query);
 	        
 	        ?>
 	        <tr>
            <td>
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF" id="paginationTable" style="border:2px solid #000000">
                <tr>
                  <td>
                  
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans) );?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                  <?php if ($prv >= 0) 
                  { ?>
				 <!--
				  	#9898: Premier Exchange:RTS is not working / Filter Values are not Retained
					This Link is Changed For Retain the values of field in Pagination
				  -->
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&amountCriteria=$amountCriteria&amountType=$amountType&amount=$amount&transSource=$transSource&sortBy=".$_GET["sortBy"].$strForPagination;?>"><font color="#005b90">First</font></a>
                  </td>
				  <!--
				  	#9898: Premier Exchange:RTS is not working
					This Link is Changed For Retain the values of field in Pagination
				  -->
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&amountCriteria=$amountCriteria&amountType=$amountType&amount=$amount&transSource=$transSource&sortBy=".$_GET["sortBy"].$strForPagination;?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) 
					{
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
						
				?>
                  <td width="50" align="right">
				  	 <!--<a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&$qryString&currencyUsed=$currencyTobeUsed";?>"><font color="#005b90">Next</font></a>&nbsp;-->
			<!--
				#9898: Premier Exchange:RTS is not working / Filter Values are not Retained
				This Link is changed for retain the values of field to make the search Validate on next Pages
			-->
			 <a href="<?php print $PHP_SELF . "?newOffset=$nxt&amountCriteria=$amountCriteria&amountType=$amountType&amount=$amount&transSource=$transSource&sortBy=".$_GET["sortBy"].$strForPagination; ?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
				  <!--
				  	#9898: Premier Exchange:RTS is not working / Filter Values are not Retained
					This Link is Changed For Retain the values of field in Pagination
				  -->
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&amountCriteria=$amountCriteria&amountType=$amountType&amount=$amount&transSource=$transSource&sortBy=".$_GET["sortBy"].$strForPagination;?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>
 	      <tr>
 	            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000">
 	            	<span class="tab-u"><strong>&nbsp;Report

 	              Date - <? if($fromDate <= $toDate){echo "From ".$fromDate." To ".$toDate;}?></strong></span></td>

 	        </tr>
 	        <tr>
 	            <td height="25"  bgcolor="#C0C0C0" align="center"> <span class="tab-u"><strong> Report Transaction Summary</strong></span></td>
 	        </tr>                     
 	       
 	        <tr>
 	          <td bgcolor="#EFEFEF" width="100%">
 	          
 	          <table width="100%" border="0" bordercolor="#EFEFEF">
 	             
 	            <tr bgcolor="#DFE6EA">
 	            	<? 
 	            	$sortByUpper = strtoupper($sortBy);
 	            	$trimed = trim($sortByUpper);
 	       	 
 	            	 ?>

 	             <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	                 <td width="50"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transDate";?>"><strong><font color="#006699">Date Created<? if($trimed == "TRANSDATE"){ echo  "*"; }?></font></strong></a></span></td>   
 	             <? } ?>
 	              <td width="81"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=refNumberIM";?>"><strong><font color="#006699"><? echo $systemCode;?><? if($trimed == "REFNUMBERIM"){ echo  "*"; }?></font></strong></a></span></td>
				  <td width="50"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=refNumber";?>"><strong><font color="#006699"><? echo $manualCode;?><? if($trimed == "REFNUMBER"){ echo  "*"; }?></font></strong></a></span></td>             
 	              <td width="50"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transStatus";?>"><strong><font color="#006699">Transaction Status<? if($trimed == "TRANSACTIONSTATUS"){ echo  "*"; }?></font></strong></a></span></td>
				  <td width="50"><span class="style1"><strong><font color="#006699">Transaction Source</font></strong></a></span></td>
				   <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
 	              <td width="50"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=Time";?>"><strong><font color="#006699">Time<? if($trimed == "TIME"){ echo  "*"; }?></font></strong></a></span></td>   
 	              <? }?>
 	              	              
 	              <td width="83"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=firstName";?>"><strong><font color="#006699"><?=__("Sender")?> Name<? if($trimed == "FIRSTNAME"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="75"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=BeneficiaryFirstName";?>"><strong><font color="#006699">Beneficiary Name <? if($trimed == 'BENEFICIARYFIRSTNAME'){ echo  "*"; }?></font></strong></a></span></td>
 	              
 	              <!-- pick up for distributor-->
 	              <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	              <td width="132"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=correspondentName";?>"><strong><font color="#006699">Collection Point<? if($trimed == "CORRESPONDENTNAME"){ echo  "*"; }?></font></strong></a></span></td>
 	             <? } ?>
 	             <!-- pick up for distributor-->
 	             
 	                <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="67"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transAmount";?>"><strong><font color="#006699">Amount Sent<? if($trimed == "TRANSAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	              <? } ?>
 	               <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=currencyFrom";?>"><strong><font color="#006699">Originating Currency Type<? if($trimed == "CURRENCYFROM"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="68"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=exchangeRate";?>"><strong><font color="#006699">Exchange Rate<? if($trimed == "EXCHANGERATE"){ echo  "*"; }else {echo "";}?></font></strong></a></span></td>
 	              <? } ?>
 	              
 	                 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=localAmount";?>"><strong><font color="#006699">Foreign Amount<? if($trimed == "LOCALAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	              <? } ?>
 	               <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=currencyTo";?>"><strong><font color="#006699">Destination Currency Type<? if($trimed == "CURRENCYTO"){ echo  "*"; }?></font></strong></a></span></td>
 	              <? } ?>
 	               
 	               <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	               <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transAmount";?>"><strong><font color="#006699">
 	               	 Total Amount<? if($trimed == "TRANSAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=IMFee";?>"><strong><font color="#006699">Fees<? if($trimed == "IMFEE"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=totalAmount";?>"><strong><font color="#006699">Net Amount<? if($trimed == "TOTALAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	               <? } ?>
 	               <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transAmount";?>"><strong><font color="#006699">Amount before Commission<? if($trimed == "TRANSAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=IMFee";?>"><strong><font color="#006699">Commission<? if($trimed == "IMFEE"){ echo  "*"; }?></font></strong></a></span></td>
 	              <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=totalAmount";?>"><strong><font color="#006699">Amount after Commission<? if($trimed == "TOTALAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	              <? } ?>
 	              <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?> 
 	             <td width="71"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=localAmount";?>"><strong><font color="#006699">Amount Paid in Local Currency<? if($trimed == "LOCALAMOUNT"){ echo  "*"; }?></font></strong></a></span></td>
 	            <? } ?>
 	              
 	               <?//$systemPre!="PE" this is used becaused we don't wnat this column in Premier FX.... 
				   if($agentType!="SUPI" && $agentType!="SUPAI" && $systemPre!="PE"){ ?> 
 	              <td width="78"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=distributorName";?>"><strong><font color="#006699">Bank Name<? if($trimed == "DISTRIBUTORNAME"){ echo  "*"; }?></font></strong></a></span></td>
 	              <? } ?>
 	              <? //$systemPre!="PE" this is used becaused we don't wnat this column in Premier FX.... 
				  if($agentType!="SUPI" && $agentType!="SUPAI" && $systemPre!="PE"){ ?> 
 	             <td width="132"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=correspondentName";?>"><strong><font color="#006699">Pickup Location<? if($trimed == "CORRESPONDENTNAME"){ echo  "*"; }?></font></strong></a></span></td>
 	             <? }
				 	/**
					 * #7888 : Premeir FX : Adding Column Admin Staff
					 * Short Description
					 * This is the addition of the column Admin Staff Name
					 */
					if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
				  ?>
 	             <td width="132" align="center"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=";?>"><strong><font color="#006699">Admin Staff Name<? if($trimed == "ADMINSTAFFNAME"){ echo  "*"; }?></font></strong></a></span></td>
				<?php 
					}
				?>
 	               <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	              <td width="86"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=name";?>"><strong><font color="#006699"><?=__("Agent") ;?> Name<? if($trimed == "NAME"){ echo  "*"; }?> </font></strong></a></span></td>
 	              <? } ?>
 	               <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?> 
 	              <td width="86"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=transStatus";?>"><strong><font color="#006699">Status<? if($trimed == "DISTRIBUTORNAME"){ echo  "*"; }?> </font></strong></a></span></td>
 	              <td width="86"><span class="style1"><a href="<?php print $PHP_SELF . "?$qryString&sortBy=deliveryDate";?>"><strong><font color="#006699">Date Picked up<? if($trimed == "DELIVERYDATE"){ echo  "*"; }?> </font></strong></a></span></td> 
 	               <? } ?>
 	              </tr>
 	            
 	            
<? 
 	         // debug($query);
			 // debug($queryGroup);
			 // debug($contentsTrans);
			  //debug($queryCurrency);
 	            for($i=0;$i < count($contentsTrans);$i++)
 	            {
 	               // $queryTrans = $query;
 	                
 	               // $queryTrans .= " and t.currencyTo = '".$contentCurrency[$k]["currencyTo"]."'";
 	                
 	                /*if ($sortBy !="")
 	                    $queryTrans = $queryTrans. " order by $sortBy ASC ";
 	                 $queryTrans .= " LIMIT $offset , $limit";
 	                $contentsTrans = selectMultiRecords($queryTrans);
 	                */
 	                    $totalAmount = 0;
 	                    $foriegnTotal = 0;
 	                    $grandTotal = 0;
 	                    
 	                    
 	                   // for($i = 0; $i< count($contentsTrans); $i++)
 	                   //Added by Niaz Ahmad #2962 at 26-02-2008 for connectPlus
 	                   if(CONFIG_SHOW_FLAG == "1"){
 	                   
 	                   $transColor = showColor($contentsTrans[$i]["transStatus"]);
 	                   }
 	                    {           
 	                    ?>
 	                    
 	                    <tr bgcolor="#FFFFFF" style="color:<?=$transColor?>">
 	                       
 	                       <!-- date created only for distributor -->
 	                        <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	                          <td width="100" bgcolor="#FFFFFF"><? echo dateFormat($contentsTrans[$i]["transDate"],1); ?></td>
 	                        <? } ?>
 	                      <!-- date created -->
 	                       <td width="81" ><span class="style1"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"];?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="<? if(CONFIG_SHOW_FLAG == "1"){ echo $transColor;}else{?> #006699 <? } ?>"><? echo $contentsTrans[$i]["refNumberIM"];?></font></strong></a></span></td>
 	                       <td width="50" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
						   <td width="50" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td> 
						   <?php if($contentsTrans[$i]["trans_source"] == "O")
						   {
							$showTransSource = "Online";
						   }
						   else
						   {
							$showTransSource = "Payex";
						   
						   }
						   ?>
						   <td width="50" bgcolor="#FFFFFF"><? echo $showTransSource?></td> 
                         <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	                      <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["Time"]; ?></td>
 	                        <? } ?>               
 	                      <? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");?>
 	                      <td width="83" bgcolor="#FFFFFF"><? echo ucfirst($contentsTrans[$i]["firstName"]). " " . ucfirst($contentsTrans[$i]["lastName"]).""?></td>
 	                      <? $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");?>
 	                      <td width="75" bgcolor="#FFFFFF"><? echo ucfirst($contentsTrans[$i]["BeneficiaryFirstName"]). " " . ucfirst($contentsTrans[$i]["BeneficiaryLastName"]).""?></td>
 	                      
 	                      <!-- pickup for distributor-->
 	                       <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	                       <? $locationContent = selectFrom("select cp_branch_name, cp_corresspondent_name from cm_collection_point where cp_id='".$contentsTrans[$i]["collectionPointID"]."'");?>
 	                       <td width="132"><? echo $contentsTrans[$i]["correspondentName"]." ".$contentsTrans[$i]["BranchName"];?></td>
 	                      <? } ?>
 	                      <!--pickup for distributor -->
 	                      
 	                        <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	                      <td width="67" bgcolor="#FFFFFF">
 	                      <?
 	                      echo number_format($contentsTrans[$i]["transAmount"],2,'.',',');
 	                      
 	                      $totalAmount += $contentsTrans[$i]["transAmount"];
 	                    
 	                      ?></td> <? } ?>
 	                      
 	                       <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	                       <td width="71"><? echo getCurrencySign($contentsTrans[$i]["currencyFrom"]); ?></td>
							
							<?  if($systemPre=="PE"){ ?> 
 	                  <td width="68"><? echo ($contentsTrans[$i]["exchangeRate"]);?></td>
 	                  <? } else {?>
 	                  <td width="68"><? echo number_format($contentsTrans[$i]["exchangeRate"],2,'.',',');?></td>
 	                     <?} }  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	                  <td width="71"><? echo number_format($contentsTrans[$i]["localAmount"],2,'.',',');
 	                  $foriegnTotal += $contentsTrans[$i]["localAmount"];
 	                  ?></td> <? } ?>
 	               
 	                  <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?> 
 	                  <td width="71"><? echo getCurrencySign($contentsTrans[$i]["currencyTo"]); ?></td>
 	                  <? } ?>
 	                  
 	                  <td width="71"><? echo number_format($contentsTrans[$i]["transAmount"],2,'.',','); ?></td>
 	                  
 	                 	                  
 	                  <td width="71"><? echo number_format($contentsTrans[$i]["IMFee"],2,'.',','); ?></td>
 	                  	                   
 	                  
 	                  <td width="71"><? echo number_format($contentsTrans[$i]["totalAmount"],2,'.',',');
 	                      $grandTotal += $contentsTrans[$i]["totalAmount"];
 	                      ?></td>
 	                      
 	                  <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?> 
 	                  <td width="71"><? echo number_format($contentsTrans[$i]["localAmount"],2,'.',',');
 	                  $foriegnTotal += $contentsTrans[$i]["localAmount"];
 	                  ?></td> <? } ?> 
 	                      
 	                  <? $bankContent = selectFrom("select name from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["benAgentID"]."'");?>
 	                  
 	                  <?  if($agentType!="SUPI" && $agentType!="SUPAI" && $systemPre!="PE"){ ?> 
 	                   <td width="78"><? echo $contentsTrans[$i]["distributorName"];?></td>
 	                     
 	                   <? } ?>
 	                 
 	                   <?  if($agentType!="SUPI" && $agentType!="SUPAI" && $systemPre!="PE" ){ ?> 
 	                  <? $locationContent = selectFrom("select cp_branch_name, cp_corresspondent_name from cm_collection_point where cp_id='".$contentsTrans[$i]["collectionPointID"]."'");?>
 	                  <td width="132"><? echo $contentsTrans[$i]["correspondentName"]." ".$contentsTrans[$i]["BranchName"];?></td>
 	                 <? } ?>
 	                 <?php 
					 	/**
						 * #7888 : Premeir FX : Adding Column Admin Staff
						 * Short Description
						 * This CONFIG is for the addition of the column Admin Staff Name
						 */
						if(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
							$queryAdminStaff = "SELECT cust.parentID, admins.username, admins.name FROM ".TBL_CUSTOMER." AS cust, ".TBL_ADMIN_USERS." AS admins WHERE cust.customerID = '".$contentsTrans[$i]["customerID"]."' AND admins.userID = cust.parentID LIMIT 1";
							$adminStaffName = selectFrom($queryAdminStaff);
					 ?>
						<td width="132" align="center">
							<?php 
								echo $adminStaffName['name']." [".$adminStaffName['username']."]";
							?>
						</td>
					 <?php 
					 	}
				 	?>
 	                 <?  if($agentType!="SUPI" && $agentType!="SUPAI" ){ ?> 
 	                  <td width="86" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["name"]?></td>
 	                   <? } ?> 
 	                  
 	                  <? if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>               
 	                     <td width="86" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
 	                     <td width="86" bgcolor="#FFFFFF"><? if($contentsTrans[$i]["deliveryDate"]!= '0000-00-00 00:00:00'){ echo  dateFormat($contentsTrans[$i]["deliveryDate"]);} ?></td>
 	                  <? } ?>

                   </tr>
 	                    <?
 	                }
 	                
 	                ///To Show at Bottom///
 	                	$currencyBottom[$k] = $contentCurrency[$k]["currencyTo"];
 	                	$totalAmountBottom[$k] = $totalAmount;
 	                	$foriegnTotalBottom[$k] = $foriegnTotal;
 	                	$grandTotalBottom[$k] = $grandTotal;
 	                	/////////////
 	                
 	                ?>
 	                   
 	                
 	            <?
 	            }
 	            ?>
 	             <tr bgcolor="#CCCCCC">
 	                <?  if($agentType =="SUPI" || $agentType =="SUPAI"){ ?> 
 	             	<td colspan="13">&nbsp;</td>
 	             	<?
					/**
					 * # 7888: Premeir Exchange - Addition of Admin Staff Name Column
					 * Short Description
					 * This @condition is for HTML layout after addition of column Admin Staff Name
					 */
 	            }elseif(CONFIG_ADMINSTAFF_ASSOCIATED_AGENTS == "1"){
 	           	?>
					<td colspan="18">&nbsp;</td>
				<?php 
				}
				else{	
					?>
 	            	<td colspan="17">&nbsp;</td> 	            	
 	            	<?
 	            	}
 	             	?>
 	                </tr>
 	            <tr>
                  <td  align="left">&nbsp;</td>
				 </tr>
				 
				 
				 
				 <?
					$totalCancelLess 			= 0;
					$totalTransAmountGrouped	= 0;
					$totalIMFee 				= 0;
					$totalGrandAmountLess 		= 0;
					$totalCurrencyArr			= array();
					$totalCurrencyArr1          = array();
					$totalIMFeeArr              =array();
					$cumulativeArrTotal=array();
					
					
					
					//debug($queryGroupSendingCurrencyCancelled);
					for($s=0;$s < count($contentsTransGroup);$s++)
					{
						//debug($contentsTransGroup[$s]["totalGroup"]);
						//$totalCurrencyArr[] = $contentsTransGroup[$s]["currencyFrom"];
						//$totalCurrencyArr1[]=$contentsTransGroup[$s]["currencyTo"];
						$totalIMFeeArr[]=$contentsTransGroup[$s]["IMFee"];
					//	$commulativeArr[]= $contentsTransGroup[$s]["totalTransAmount"];
						//debug($totalCurrencyArr);
						$totalCancelLess += $contentsTransGroup[$s]["totalGroup"];
						$totalTransAmountGrouped += $contentsTransGroup[$s]["totalTransAmount"];
						$totalIMFee += $contentsTransGroup[$s]["IMFee"];
						$totalGrandAmountLess += $contentsTransGroup[$s]["grandAmount"];
					}
					//debug($contentsTransGroup);
					if(count($contentsTransGroup)>0){?>
						<?php
						for($bottom=0;$bottom < count($contentsTransGroup);$bottom++)
						{
						?>
						<tr bgcolor="#CCCCCC"  style="text-align: right;">
						 <td width="75">&nbsp;</td>
						 <td width="75">&nbsp;</td>
						 <td width="71">&nbsp;</td> 
						 <td width="81">&nbsp;</td>
						 <td width="50">&nbsp;</td>
						 <td width="83">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="67"><?
				 echo number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',');
										  
					/*if ((isset $contentsTransGroup[$bottom]["currencyFrom"]))
					{
					
					$commulativeArr[$bottom]+=	$contentsTransGroup[$bottom]["totalTransAmount"];
					}	
					*/
					
					if(isset( $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]))
					{
					 $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]= $totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]] + $contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
					else {
						
						$totalCurrencyArr[$contentsTransGroup[$bottom]["currencyFrom"]]=$contentsTransGroup[$bottom][  	 
							 "totalTransAmount"];
					}
						 		
								// debug($commulativeArr);
										  ?></td>
						 <? } ?>
						 <td align="left"><?=getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"])?></td>
						
						 <td width="71">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');
									


        if(isset( $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]))
					{
					 $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]= $totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]] + $contentsTransGroup[$bottom]["totalLocalAmount"];
					}
					else {
						
						$totalCurrencyArr1[$contentsTransGroup[$bottom]["currencyTo"]]=$contentsTransGroup[$bottom][  	 
							"totalLocalAmount"];
					}
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td align="left">&nbsp;<? echo ($contentsTransGroup[$bottom]["currencyTo"]);
				
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="100">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalTransAmount"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]); ?></td>
						 
						 <? } ?>
						 <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="71"></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71"><? 
										echo number_format($contentsTransGroup[$bottom]["IMFee"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]);

					
										?></td>
						 <td width="100"><? 
										echo number_format($contentsTransGroup[$bottom]["grandAmount"],2,'.',',').' '.getCurrencySign($contentsTransGroup[$bottom]["currencyFrom"]); 
										?></td>
						 <?
									  }
										?>
						 <td width="71">&nbsp;</td>
						 <!-- Net Amount for distributor only -->
						 <? if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="80">&nbsp;<? echo number_format($contentsTransGroup[$bottom]["totalLocalAmount"],2,'.',',');   
									  ?></td>
						 <? } ?>
						 <!-- Net Amount for distributor only   -->
						 <td width="78">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						</tr>
						<?
								}
						if(is_array($totalCurrencyArr))
							//$totalCurrencyArr = array_unique($totalCurrencyArr);
						
						if(count($totalCurrencyArr)> 0){
							//debug($totalCurrencyArr);
						//	debug($queryGroup);
							$totalCurrency = $totalCurrencyArr[0];
							?>
							<tr  style="text-align: right;">
								 <td  align="left" colspan="5">&nbsp;<b><font color="#006699">Total for Transactions (excluding Cancelled): &nbsp;<i><?=$totalCancelLess?></i></font></b></td>
								 <!--<td  align="right" colspan="2"><b><font color="#006699">Total Amount(s) :&nbsp;</font></b></td>
								 <td><?=number_format($totalTransAmountGrouped,2,'.',',');?></td>
								 <td align="left"><?=$totalCurrency?></td>
								 <td colspan='3'>&nbsp;</td>
								 <td><?=number_format($totalTransAmountGrouped,2,'.',',');?> <?=$totalCurrency?></td>
								 <td><?=number_format($totalIMFee,2,'.',',');?> <?=$totalCurrency?></td>
								 <td><?=number_format($totalGrandAmountLess,2,'.',',');?> <?=$totalCurrency?></td>-->
								</tr>
							<?
						}	
					}
				// debug($commulativeArr);
				
					$totalTransAmountCanc 		= 0;
					$totalTransIMFeeCanc 		= 0;
					$totalTransGrandAmountCanc 	= 0;
					$totalCurrencyArrCanc		= array();
					//debug($contentsTransCancelled);					
					for($c=0;$c < count($contentsTransCancelled);$c++){
					
						$totalCurrencyArrCanc[]	= $contentsTransCancelled[$c]["currencyFrom"];
						$totalTransAmountCanc += $contentsTransCancelled[$c]["totalTransAmount"];
						$totalTransIMFeeCanc += $contentsTransCancelled[$c]["IMFee"];
						$totalTransGrandAmountCanc += $contentsTransCancelled[$c]["grandAmount"];
					}		
					
					if(count($contentsTransCancelled)>0){
				
						
						for($cancel=0;$cancel < count($contentsTransCancelled);$cancel++)
						{
						?>
						<tr bgcolor="#CCCCCC"  style="color:#FF0000;text-align: right;" >
						 <td width="75">&nbsp;</td>
						 <td width="75">&nbsp;</td>
						 <td width="71">&nbsp;</td>
						 <td width="81">&nbsp;</td>
						 <td width="50">&nbsp;</td>
						 <td width="83">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="67"><?
										  echo number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',');
										  
					
										  
										//  debug($totalCurrencyArrCanc);
										  ?></td>
						 <? } ?>
						 <td  align="left"><?=getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"])?></td>
						 <td width="71">&nbsp;</td>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');
									  
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td align="left">&nbsp;<? echo ($contentsTransCancelled[$cancel]["currencyTo"]);
				
									  ?></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="100">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalTransAmount"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); ?></td>
						 <? } ?>
						 <?  if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="71"></td>
						 <? } ?>
						 <?  if($agentType!="SUPI" && $agentType!="SUPAI"){ ?>
						 <td width="71"><? 
										echo number_format($contentsTransCancelled[$cancel]["IMFee"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); 
										?></td>
						 <td width="100"><? 
										echo number_format($contentsTransCancelled[$cancel]["grandAmount"],2,'.',',').' '.getCurrencySign($contentsTransCancelled[$cancel]["currencyFrom"]); 
										?></td>
						 <?
									  }
										?>
						 <td width="71">&nbsp;</td>
						 <!-- Net Amount for distributor only -->
						 <? if($agentType=="SUPI" || $agentType=="SUPAI"){ ?>
						 <td width="80">&nbsp;<? echo number_format($contentsTransCancelled[$cancel]["totalLocalAmount"],2,'.',',');   
									  ?></td>
						 <? } ?>
						 <!-- Net Amount for distributor only   -->
						 <td width="78">&nbsp;</td>
						 <td width="86">&nbsp;</td>
						</tr>
						<?
								}
						if(is_array($totalCurrencyArrCanc))
							$totalCurrencyArrCanc = array_unique($totalCurrencyArrCanc);
							$totalCurrencyCanc = $totalCurrencyArrCanc[0];
							?>
							<tr style="color:#FF0000;text-align: right;">
								 <td  align="left" colspan="5">&nbsp;<b><font color="#006699">Total for Transactions [Cancelled]:&nbsp;<i style='padding-left:20px;'><?=$allCount-$totalCancelLess?></i></font></b></td>
								<!-- <td  align="right" colspan="2"><b><font color="#006699">Total Amount(s) :&nbsp;</font></b></td>
								 <td><?=number_format($totalTransAmountCanc,2,'.',',');?></td>
								 <td align="left"><?=$totalCurrencyCanc?></td>
								 <td colspan='3'>&nbsp;</td>
								 <td><?=number_format($totalTransAmountCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>
								 <td><?=number_format($totalTransIMFeeCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>
								 <td><?=number_format($totalTransGrandAmountCanc,2,'.',',');?> <?=$totalCurrencyCanc?></td>-->
								</tr>
								
							<?
							//}
					} ?>
				 
 	          </table>
			 
			 <table width="100%" border="0" bordercolor="#EFEFEF">


<tr bgcolor="#CCCCCC" align="right">
<td width="75"></td>
<td width="75"></td>
<td width="71"></td>
<td width="81"></td>
<td width="50"></td>
<td width="71">
<?foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
	
}?></td>


<td width="81"><?//print_r($totalCurrencyArr);

?></td>
<!--<td width="67"></td>-->

<td width="140"><?//print_r($totalCurrencyArr1);
foreach ($totalCurrencyArr1 as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}
?></td>

<td width="86"><?//print_r($totalCurrencyArr);
foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}

?></td>

<td width="86"><?//print_r($totalCurrencyArr);
foreach ($totalCurrencyArr as $key => $value){
	echo $key.'='.number_format($value,2,'.',',').'</br>';
}

?></td>
<td width="132" align="center"></td>
<td width="86"></td>


</tr>
</br>
</br>
<tr><b><font color="#006699">Cumulative Sum Of Transactions [Not Cancelled]</font></b></tr>
		</table>
 	              </td>
 	        </tr>
			
 	        <tr id="actionBtnRow">
 	              <td>
 	                <div align="center">
 	                    <input type="button" name="Submit2" value="Print This Report" id="printReport">
						
						<input type="hidden" name="condition" id="condition" value="<?=$condition?>" />
						<input type="hidden" name="query" id="query" value="<?=$queryExport?>" />
						<input type="hidden" name="queryCurrency" id="queryCurrency" value="<?=$queryCurrency?>" />
						<input type="hidden" name="queryGroup" id="queryGroup" value="<?=$queryGroup?>" />
						<input type="hidden" name="queryGroupSendingCurrencyCancelled" id="queryGroupSendingCurrencyCancelled" value="<?=$queryGroupSendingCurrencyCancelled?>" />
						<input type="hidden" name="dfFlag" id="dfFlag" value="<?=$dfFlag?>" />
						
						<!--<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
						<input type="hidden" name="fromDate" id="fromDate" value="<?=$fromDate?>" />-->
						<input type="button" name="Submit2"  value="Export All" onClick="export_report();">
 	                </div>
 	            </td>
 	           
 	        </tr>
 	        <?
 	            } else{ // greater than zero
 	            ?>
 	            <tr>
 	                <td bgcolor="#FFFFCC"  align="center">
 	                <font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
 	                </td>
 	            </tr>
 	            <? }?>
 	        </form>
 	      </table></td>
 	  </tr>
 	</table>

 	</body>
 	</html>