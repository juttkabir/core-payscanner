<?php
session_start();
include("../include/config.php");
include ("security.php");
$date_time	= date('d-m-Y  h:i:s A');
$benAgentID =  $_GET["benAgentID"];
$systemCode = SYSTEM_CODE;
$company 	= COMPANY_NAME;
$systemPre 	= SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$strGetSql = " SELECT 
					actionCount  
				FROM 
					viewReportHistory 
				WHERE 
					id = '".$_REQUEST["id"]."'";
					
$arrGetData = selectFrom($strGetSql);
	
?>
<html>
<head>
<title>View Action Details</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<script language="javascript">
</script>
<style type="text/css">
#displayTable, #displayTable td, #displayTable th{
	border:1px solid #88C9EE;
	border-collapse:collapse;
}
.headingBar{
	margin:5px;
	height:20px;
	line-height:20px;
	padding-left:5px;
	background-color:#6699CC;
	color:#FFFFFF;
	font-weight:bold;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<div class="headingBar">Action Detail</div>
<?php
if(!empty($arrGetData))
{?>
    <table width="50%" align="center" id="displayTable" cellpadding="5">
        <tr bgcolor="#A6C9E2"> 
            <th align="left">Action</th>
            <th align="left">Count</th>
        </tr>
        <tr>
            <td>View</td>
            <td><?=$_GET['count']?></td>
        </tr>
        <?php
        $total= $_GET['count'];
        $arrActionLabel=array('E'=>'Export', 'P'=>'Print', 'S'=>'Save');
        $actionArr=unserialize($arrGetData["actionCount"]);
        foreach($actionArr as $key=>$val)
        { 
        $total +=$val;
        ?>      
        <tr> 
            <td><?=$arrActionLabel[$key]?></td>
            <td><?=$val?></td>
        </tr>
        <?php
        }?>
         <tr bgcolor="#DFEFFC">
            <td colspan="2" style="border-bottom:2px solid #88C9EE;"><strong>Total:</strong></td>
        </tr>
        <tr bgcolor="#C5DBEC"> 
            <td>&nbsp;</td>
            <td><strong><?=$total?></strong></td>
        </tr>
    </table>
<?php
}
else{
	echo '<div style="background-color:#FFFFCC; color:#FF0000" align="center">No Data Found</div>';
}?>
</body>
</html>