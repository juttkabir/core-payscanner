<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

// define page navigation parameters
if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = "dated";

$colSpans = 12;

if(CONFIG_AGENT_RATE_MARGIN)
{
	$colSpans = $colSpans + 1;
}
if(CONFIG_24HR_BANKING == "1" && !empty( $_REQUEST["rateType"]) && $_REQUEST["rateType"]=="bank24hr" ){
	$colSpans = $colSpans + 3;
}
if($_GET["search"]!="search")
	{
			
		$rfMonth = $_POST["fMonth"];
		$rfDay = $_POST["fDay"];
		$rfYear = $_POST["fYear"];
		
		$rtMonth = $_POST["tMonth"];
		$rtDay = $_POST["tDay"];
		$rtYear = $_POST["tYear"];
		
		$fromCountry = $_POST["origCountry"];
		$toCountry = $_POST["destCountry"];
		
		$fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay ." 00:00:00";
		$toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay ." 23:59:59";
		
	}else{
		$rfMonth = substr($_GET["fDate"],5,2);
		$rfDay = substr($_GET["fDate"],8,2);
		$rfYear = substr($_GET["fDate"],0,4);
		
		$rtMonth = substr($_GET["tDate"],5,2);
		$rtDay =  substr($_GET["tDate"],8,2);
		$rtYear =  substr($_GET["tDate"],0,4);
		
		$fromDate = $_GET["fDate"];	
		$toDate = $_GET["tDate"];
		
		$fromCountry = $_GET["fCountry"];
		$toCountry = $_GET["tCountry"];
		
		}	
	
	
	$queryString = " &fDate=".$fromDate."&tDate=".$toDate."&fCountry=".$fromCountry."&tCountry=".$toCountry."&search=search";
	
	
 $SQL_Qry = "select * from ".TBL_HISTORICAL_EXCHANGE_RATES." 
					 where  updationDate >= '".$fromDate."' and updationDate <= '".$toDate."'";
 if($fromCountry != "")
 { 
 		$SQL_Qry .= " and countryOrigin = '".$fromCountry."' " ;
 }
 if($toCountry != "")
 { 
 		$SQL_Qry .= " and country = '".$toCountry."' " ;
 }
	/*
	 *
	 * Code added to show additional filter applied on rateFor field in historicalExchangeRate table in the database.
	 * @Ticket: #3543: Connect Plus - Manage Old Exchange Rate Filters
	 * @By: Usman Ghani
	 *
	 */
	
	if ( !empty( $_REQUEST["rateType"] ) )
	{
		$SQL_Qry .= " and rateFor = '" . $_REQUEST["rateType"] . "' ";
		$queryString .= "&rateType=" . $_REQUEST['rateType'];
		
	}
	// End of Usman's code aginst #3543: Connect Plus - Manage Old Exchange Rate Filters

$rates = SelectMultiRecords($SQL_Qry);
$allCount = count($rates);
if ($sortBy !="")
	$SQL_Qry = $SQL_Qry. " order by $sortBy DESC ";
 $SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
//debug($SQL_Qry);
$rates = SelectMultiRecords($SQL_Qry);
?>
<html>
<head>
	<title>Categories List</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	function checkSearchForm()
	{
		rateType = document.getElementById("rateType");
		if ( rateType.value == "" )
		{
			alert( "Please select a rate type." );
			rateType.focus();
			return false;
		}
		return true;
	}
	<!-- 
	function checkForm(theForm) {
		var a = false;
		var none = true;
		var j = 2; 
		for(i =0; i < <?=count($rates); ?>; i++){
			if(theForm.elements[j].checked == true){
				a = confirm("Are you sure you want to delete selected exchange rate(s) from the database?");
				none = false;
				break;
			}
			j++;
		}
		if (none) {
			a = false;
			alert("No exchange rate(s) is selected for deletion.")
		}
		return a;
	}
	function checkAllFn(theForm, field) {
		if (field == 1) {
			if (theForm.del.value == 0)
				theForm.del.value = 1;
			else
				theForm.del.value = 0;
			if (theForm.del.value == 1){
				j=2;
				for(i=0; i < <?=count($rates);?>; i++) {
					theForm.elements[j].checked = true;
					j++;
				}
			} else {
				j = 2;
				for(i=0; i< <?=count($rates);?>; i++) {
					theForm.elements[j].checked = false;
					j++;
				}
			}
		} else {
			if (theForm.app.value == 0)
				theForm.app.value = 1;
			else
				theForm.app.value = 0;
			if (theForm.app.value == 1){
				j = 3;
				for(i=0; i<<?=count($rates);?>; i++) {
					theForm.elements[j].checked = true;
					j = j+2;
				}
			} else {
				j = 3;
				for(i=0; i<<?=count($rates);?>; i++) {
					theForm.elements[j].checked = false;
					j = j+2;
				}
			}
		}
	}
	
	
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Manage Exchange Rates</td>
  </tr>
  <tr>
  	<td>
  		<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
         <form name="search" action="old-rate-list.php" method="post" onSubmit="return checkSearchForm();">
         			<tr>
					        <td align="center" nowrap>
							Update Date From 
							<? 
							$month = date("m");
							
							$day = date("d");
							$year = date("Y");
							?>
		
			        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			
			          <?
								  	for ($Day=1;$Day<32;$Day++)
									{
										if ($Day<10)
										$Day="0".$Day;
										echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
									}
								  ?>
			        </select>
			        <script language="JavaScript">
			         	SelectOption(document.search.fDay, "<?=$rfDay?>");
			        </script>
							<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
			          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			       </SELECT>
		        <script language="JavaScript">
		         	SelectOption(document.search.fMonth, "<?=$rfMonth?>");
		        </script>
		        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
		
		          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
					  ?>
			        </SELECT> 
							<script language="JavaScript">
			         	SelectOption(document.search.fYear, "<?=$rfYear?>");
			        </script>
		        To	
		        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          	<?
					  	for ($Day=1;$Day<32;$Day++)
							{
								if ($Day<10)
								$Day="0".$Day;
								echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
							}
					  ?>
       		 </select>
					<script language="JavaScript">
	         	SelectOption(document.search.tDay, "<?=$rtDay?>");
	        </script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

	          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
	          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
	          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
	          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
	          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
	          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
	          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
	          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
	          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
	          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
	          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
	          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
	        </SELECT>
					<script language="JavaScript">
         		SelectOption(document.search.tMonth, "<?=$rtMonth?>");
        	</script>
        	<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          				<?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
								  ?>
	        </SELECT>
					<script language="JavaScript">
	         	SelectOption(document.search.tYear, "<?=$rtYear?>");
	        </script>	      </td>
      </tr>       	
      <tr>
				<td align="center" nowrap><font color="#005b90"><strong>Originating Country</strong></font>
            <SELECT name="origCountry" id="origCountry" class=flat>
              <OPTION value="">- Select Country - </OPTION>
             <?
	                		$countryTypes = " and  countryType like '%origin%' ";
	            
	            if(CONFIG_COUNTRY_SERVICES_ENABLED){
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = fromCountryId  $countryTypes order by countryName");
									}
								else
									{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									}
								for ($i=0; $i < count($countires); $i++){
							?>
			            <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo ($countires[$i]["countryName"] == $fromCountry ? "selected" : "")?>>
			            <?=$countires[$i]["countryName"]; ?>
			            </OPTION>
			            <?
								}
							?>
            	</SELECT>
				                    
						</font>					</td>
      	</tr>
       	<tr>
					<td align="center" nowrap><font color="#005b90"><strong>Destination Country</strong></font>
									
            <SELECT name="destCountry" id="destCountry" class=flat>
              <OPTION value="">- Select Country - </OPTION>
             <?
	            //if(CONFIG_ENABLE_ORIGIN == "1"){
	                	
	                		$countryTypes = " and  countryType like '%destination%' ";
		                //}else{
	                		//$countryTypes = " ";
	                	//}
	            if(CONFIG_COUNTRY_SERVICES_ENABLED){
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
								}
							else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
							for ($i=0; $i < count($countires); $i++){
						?>
		            <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo ($countires[$i]["countryName"] == $toCountry ? "selected" : "")?>>
		            <?=$countires[$i]["countryName"]; ?>
		            </OPTION>
		            <?
							}
						?>
            </SELECT>
                    
					</font>				</td>
      </tr>
	<?
	  	/*
		 *
		 * Code added to show additional filter applied on rateFor field in historicalExchangeRate table in the database.
		 * @Ticket: #3543: Connect Plus - Manage Old Exchange Rate Filters
		 * @By: Usman Ghani
		 *
		 */

		$rateForList = selectMultiRecords( "select distinct(rateFor) from " . TBL_HISTORICAL_EXCHANGE_RATES . " order by rateFor" );
		?>
		<tr>
       	  <td align="center" nowrap><font color="#005b90"><strong>Rate Type <span style="color:#FF0000;">*</span></strong></font>
			<SELECT name="rateType" id="rateType" class="flat">
            	<OPTION value="">- Select Rate type - </OPTION>
				<?
					foreach( $rateForList as $rateFor )
					{
						$selected = "";
						if ( !empty( $_REQUEST["rateType"] ) && $_REQUEST["rateType"] == $rateFor["rateFor"] )
						{
							$selected = "selected='selected'";
						}
						?>
    		        		<OPTION <?=$selected;?> value="<?=$rateFor["rateFor"];?>"><?=$rateFor["rateFor"];?></OPTION>
						<?
					}
				?>
			</SELECT>
            </font> </td>
     	</tr>
    	<?
	 	// End of Usman's code against ticket #3543: Connect Plus - Manage Old Exchange Rate Filters
	 ?>
  			<tr>
  				<td align="center" nowrap>
  					<input type="submit" name="Submit" value="Submit" class="flat">  				</td>
  			</tr>
  		</form>
    </table>
  	</td>
  </tr>
  <form action="delete-rates.php" method="post" onSubmit="return checkForm(this);">
   <tr>
    <td align="center">
		<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
          <?
			if ($allCount > 0){
		?>
          <tr> 
            <td colspan="<?=$colSpans?>" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($rates) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <td>&nbsp;</td>
            			<td>&nbsp;</td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
          	<td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Originating Country</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=currencyOrigin";?>"><font color="#005b90"><strong>Originating Currency</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Destination Country</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=currency";?>"><font color="#005b90"><strong>Destination Currency</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Date</strong></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=country";?>"><font color="#005b90"><strong>Updation Date</strong></font></a></td>
            <td width="125"><a href="<?php print $PHP_SELF . "?sortBy=primaryExchange";?>"><font color="#005b90"><b><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_PIMARY_RATE);}else{echo("Primary Exchange");} ?></b></font></a></td>
            <td width="109"><a href="<?php print $PHP_SELF . "?sortBy=marginPercentage";?>"><font color="#005b90"><b>Margin</b></font></a></td>
            <td width="93"><a href="<?php print $PHP_SELF . "?sortBy=primaryExchange";?>"><font color="#005b90"><b><? if(CONFIG_ENABLE_RATE == "1"){echo(CONFIG_NET_RATE);}else{echo("Net Exchange");} ?></b></font></a></td>
            <td><font color="#005b90"><b>Based On</b></font></td>
            <td><font color="#005b90"><b>Value</b></font></td>
            <?
		        if(CONFIG_AGENT_RATE_MARGIN == '1')
		        {
		        ?>
            	<td><font color="#005b90"><b>Agent Margin</b></font></td>
            	<td><font color="#005b90"><b>Agent Value</b></font></td>

		        <?
		      	}
		        ?>
		        <?
		        if(CONFIG_24HR_BANKING == "1" && !empty( $_REQUEST["rateType"]) && $_REQUEST["rateType"]=="bank24hr" )
		        {
		        ?>
            	<td><font color="#005b90"><b>24 hr Banking Margin Type</b></font></td>
            	<td><font color="#005b90"><b>24 hr Banking Margin Value</b></font></td>
            	<td><font color="#005b90"><b>24 hr Banking Exchange Rate</b></font></td>

		        <?
		      	}
		        ?>
          </tr>
		  
          <?
			for ($i=0; $i < count($rates); $i++){
				
		?>
          <tr valign="top" bgcolor="#eeeeee" title="Rate By: <? echo $rates[$i]["sProvider"]?>">
          
	        	  <td><font color="#005b90"><b><?=stripslashes($rates[$i]["countryOrigin"]); ?></b></font></td>
	            <td><?=stripslashes($rates[$i]["currencyOrigin"]); ?></td>
	            <td><font color="#005b90"><b><?=stripslashes($rates[$i]["country"]); ?></b></font></td>
		          <td><?=stripslashes($rates[$i]["currency"]); ?></td>
	            <!-- td><font color="#005b90"><b><?=stripslashes($rates[$i]["country"]); ?></b></font></td -->
	            <td><?=dateFormat(stripslashes($rates[$i]["dated"]),"2"); ?></td>
	            <? $dateTime = split(" ",$rates[$i]["updationDate"]);?>
	            <td><?=dateFormat(stripslashes($dateTime['0']),"2")." ".$dateTime['1']; ?></td>
	            <td><?=stripslashes($rates[$i]["primaryExchange"]); ?></td>
	            <td><? echo(stripslashes($rates[$i]["marginPercentage"])); if($rates[$i]["marginType"] != 'fixed'){echo(" %");} ?> </td>
	            <td><? if($rates[$i]["marginType"] == 'fixed')
	            	{
	            		 $calculatedRate = $rates[$i]["primaryExchange"] - $rates[$i]["marginPercentage"];
	            	}else{
	            		 $calculatedRate = round($rates[$i]["primaryExchange"] - ($rates[$i]["primaryExchange"] * $rates[$i]["marginPercentage"])/100, 4);
	            		}
	            	
	            	echo($calculatedRate); ?></td>
	            <td><?=strtoupper(stripslashes($rates[$i]["rateFor"])); ?></td>
	            
            <td><? 
            		if($rates[$i]["rateFor"] == "distributor" || $rates[$i]["rateFor"] == "agent" || $rates[$i]["rateFor"] == "intermediate")
            		{
            			$agentName = selectFrom("select userID,username, name from ".TBL_ADMIN_USERS." where userID = '".$rates[$i]["rateValue"]."'");
            			echo($agentName["name"]." [".$agentName["username"]."]");
            			}else{
            				 echo(stripslashes($rates[$i]["rateValue"]));
            				 } 
            			?>
            </td>
            <?
		        if(CONFIG_AGENT_RATE_MARGIN == '1')
		        {
		        ?>
	            <td><? echo(stripslashes($rates[$i]["agentMarginValue"])); if($rates[$i]["agentMarginType"] != 'fixed'){echo(" %");} ?></td>
	            
	            
	            <td><? if($rates[$i]["agentMarginType"] == 'fixed')
            	{
            		 $agentCalculatedRate = ($rates[$i]["primaryExchange"] - $rates[$i]["marginPercentage"])- $rates[$i]["agentMarginValue"];
            	}else{
            		 $agentCalculatedRate2 = round($rates[$i]["primaryExchange"] - ($rates[$i]["primaryExchange"] * $rates[$i]["marginPercentage"])/100, 4);
            		 $agentCalculatedRate = ($agentCalculatedRate2 - (($agentCalculatedRate2 * $rates[$i]["agentMarginValue"])/100));
            		 //echo("-".(($agentCalculatedRate2 * $rates[$i]["agentMarginValue"])/100)."-");
            		}
            	
            	echo($agentCalculatedRate); ?></td>
       
            </td>
	          <?
          	}
          	?>
			
          	<?
			// #3672 added by ashahid
		        if(CONFIG_24HR_BANKING == "1" && !empty( $_REQUEST["rateType"]) && $_REQUEST["rateType"]=="bank24hr" ){
/*			$bank24hrQuery = "Select primaryExchange,_24hrMarginType,_24hrMarginValue,rateFor,marginType,marginPercentage from ".TBL_EXCHANGE_RATES." where erID='".$rates[$i]['erID']."'";
			$bank24hrData  = SelectMultiRecords($bank24hrQuery);*/
          	?>  
          		<td><?=stripslashes($rates[$i]["_24hrMarginType"]);?></td>
            	<td><?=stripslashes($rates[$i]["_24hrMarginValue"]); ?></td>
            	<?
            	$exRate24hr = 0.0;
            	if($rates[$i]["rateFor"] == "bank24hr"){
	            	if($rates[$i]["_24hrMarginType"] == 'lower'){
						$exRate24hr = $rates[$i]["primaryExchange"] - $rates[$i]["_24hrMarginValue"];
					}elseif($rates[$i]["_24hrMarginType"] == 'upper'){
						$exRate24hr = $rates[$i]["primaryExchange"] + $rates[$i]["_24hrMarginValue"];
					}
					if($rates[$i]["marginType"] == 'fixed')
					{
						$exRate24hr = ($exRate24hr - $rates[$i]["marginPercentage"]);
					}else{
						$exRate24hr = ($exRate24hr - ($exRate24hr * $rates[$i]["marginPercentage"]) / 100);
					}
				}
				?>
							<td><?=$exRate24hr?></td>
          	<?
          	}
          	?>
			
          </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="<?=$colSpans?>" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($rates) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($rates));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset$queryString&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                   <td>&nbsp;</td>
            			<td>&nbsp;</td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          
          <?
			} else {
		?>
          <tr> 
            <td colspan="5" align="center"> No Exchnage Rates found in the database. Please select proper Filters
            </td>
          </tr>
          <?
			}
		?>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
