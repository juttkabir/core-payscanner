<?
$showAmountLocalBtnFlag = false;
if(defined("CONFIG_SWAP_AMOUNT_LOCALAMOUNT_BUTTONS_REVERSE") && CONFIG_SWAP_AMOUNT_LOCALAMOUNT_BUTTONS_REVERSE=="1"){
    $showAmountLocalBtnFlag = true;
}

/*
* 7166 - Premier FX
* Now you can re-label Amount and Local Amount labels alongwith buttons using config
* define("CONFIG_BUTTON_LABELS_AT_TRANSACTION","Client Selling,Client Buying");
* Enable  : i.e "lable1,lable2".
* Disable : "0"
* Module : Transaction
* Client: Premier FX
* by Aslam Shahid
*/
$labelFlag = false;
if(defined("CONFIG_BUTTON_LABELS_AT_TRANSACTION") && CONFIG_BUTTON_LABELS_AT_TRANSACTION !="0")
{
    $arrLabel = explode(",",CONFIG_BUTTON_LABELS_AT_TRANSACTION);
    $labelFlag = true;
    // debug($arrLabel);
    $labelAmount = __($arrLabel[0]);
    $labelLocalAmount = __($arrLabel[1]);
    $classAmount ="amount";
    $classLocalAmount ="localAmount";
    $classAmountBtn = "amountBtn";
    $classLocalAmountBtn = "localAmountBtn";
    $amountBtnLen = "style='width:".(strlen($labelAmount)*8)."px';";
    $localAmountBtnLen = "style='width:".(strlen($labelLocalAmount)*8)."px';";;
}else{
    $labelAmount = __("Amount");
    $labelLocalAmount = __("Local Amount");
    $classAmount ="";
    $classLocalAmount ="";
    $classAmountBtn = $classLocalAmountBtn = "hideBtns";
}

/**
 * Logic for Payscanner
 * Show currency for customer and beneficiary which related from their country
 */

$customerCurrencyQuery = selectFrom("SELECT currencyName FROM currencies WHERE country = '" . strtoupper($customerContent['Country']) . "'");
$beneficiaryCurrencyQuery = selectFrom("SELECT currencyName FROM currencies WHERE country = '" . strtoupper($benificiaryContent['Country']) . "'");
$currencyBuying = SelectMultiRecords("SELECT * FROM exchangerate WHERE exchangerate.country = '" . $benificiaryContent['Country'] . "' AND exchangerate.rateValue = " . $_SESSION['loggedUserData']['userID']);
$customerCurrency = $customerCurrencyQuery['currencyName'];
$beneficiaryCurrency = $beneficiaryCurrencyQuery['currencyName'];

if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT == "1") {
    ?>

    <tr>
        <td width="128" height="20" align="right"><font color="#005b90"><? echo $manualCode;?></font><font color="#ff0000"><? if(CONFIG_TRANS_MANUAL_CODE_COMP == "1"){?>* <? }?></font></td>
        <td height="20" width="195"><input type="text" name="refNumber" value="<?=$_SESSION["refNumber"];?>" maxlength="10">

        </td>
        <td width="96" height="20" align="right" valign="top"><font color="#005b90"><?=$labelLocalAmount?><? if($fromTotalConfig != '1'){echo("<font color='#ff0000'>*</font>");} ?></font> </td>

        <td width="281" height="20">
            <?
            if ( empty($localAmount) && !empty($transAmount) )
            {
                $localAmount = $transAmount * $exRate;
            }

            if ( empty( $transAmount ) && !empty( $localAmount ) )
            {
                $transAmount = $localAmount / $exRate;
            }
            //debug( $exRate );

            ?>
            <input type="text" <? echo ((CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != "") ? "readonly" : "");?> name="localAmount" id="localAmountID"
                   value="<?
                   if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM')
                   {
                       echo ($localAmount > 0 ? RoundVal($localAmount) : "");
                   }
                   else
                   {
                       echo ($localAmount > 0 ? round($localAmount, $roundLevel) : "");
                   }

                   ?>" maxlength="25"  <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('local');" <? } ?> <? if(CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>size="7"<? } ?> <? if($fromTotalConfig == '1'){echo("readonly");} ?>
                   onclick="disableClicks();" >

        <span class="style1">
<?

$strQueryCountry1 = selectFrom("SELECT *  FROM  ".TBL_COUNTRY." where countryName = '$benCountryName'");
$strQueryCountry2 = selectFrom("SELECT *  FROM  ".TBL_COUNTRY." where countryName = '".$customerContent["Country"]."'");

$benCountryId = $strQueryCountry1["countryId"];
$custCountryId = $strQueryCountry2["countryId"];

$strQuery = "SELECT *  FROM  ".TBL_SERVICE." where fromCountryId = '$custCountryId' and toCountryId = '$benCountryId' ORDER BY currencyRec ";
if($countryBasedFlag){
    // $countryBasedFlag and $_SESSION["serviceType"] comes form add-transaction.php
    $strQuery= "select * from " . TBL_SERVICE_NEW . " where fromCountryId = '".$custCountryId."' and toCountryId = '" . $benCountryId . "' AND serviceAvailable='".$_SESSION["serviceType"]."' ORDER BY currencyRec ";
}

if(CONFIG_ADD_TRADING_MODULE=="1" && CONFIG_TRANSACTION_CURRENCIES_TRADING=="1"){
    $strQuery = "SELECT distinct(currency) as currencyRec FROM accounts ORDER BY currency";
    $currencyRecArr = SelectMultiRecords($strQuery);
    for($i=0;$i<count($currencyRecArr);$i++)
        $currencyRec[$i] = $currencyRecArr[$i]["currencyRec"];
}
else{
    $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

    $rstRow = mysql_fetch_array($nResult);
    $currencyRec = explode(", " , $rstRow["currencyRec"]);
    if($countryBasedFlag){
        $currencyRec = explode("," , $rstRow["currencyRec"]);
    }
}

$i = 0;
?>
            <?
            $benCurrency = selectFrom ("select");
            ?>
            <select name="currencyTo" id="currencyTo" onChange="document.addTrans.action='<?=$transactionPage?>//?transID=<? echo $_GET["transID"]?>//&enterOwnRate=<?=$_GET["enterOwnRate"]?>//&enterOwnRate=<?=$_GET["enterOwnRate"]?>//&customerID=<?=$_GET["customerID"]?>//&focusID=<?=$focusButtonID?>//'; document.addTrans.submit();">
<?
while($i < count($currencyRec)){
    if (CONFIG_DEFAULT_CURRENCY_BEN == '1' && CONFIG_CURRENCY_NAME_BEN == $currencyRec[$i]){
        if ($_SESSION["currencyTo"] == "" || $_SESSION["currencyTo"] == $currencyRec[$i]){
            echo "<option  value ='$currencyRec[$i]' selected>".$currencyRec[$i]."</option>";
        }else{
            echo "<option  value ='$currencyRec[$i]'>".$currencyRec[$i]."</option>";
        }
    }else{
        echo "<option  value ='$currencyRec[$i]'>".$currencyRec[$i]."</option>";
    }
    $i = ($i+1);
}
?>

</select>
<script language="JavaScript">
SelectOption(document.addTrans.currencyTo, "<?=$_SESSION["currencyTo"];?>");
</script>
        </span>
            <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>
                <? if($showAmountLocalBtnFlag){ ?>
                    <input type="submit" name="amount" value="Amount" onclick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classAmount?>" id="amountBtnID" <?=$amountBtnLen?> />
                    <label for="amountBtnID" class="<?=$classAmountBtn?>"><?=$labelAmount?></label>
                <? }else{?>
                    <input type="submit" name="local" id="localBtnID" value="Local Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classLocalAmount?>" <?=$localAmountBtnLen?>>
                    <label for="localBtnID" class="<?=$classLocalAmountBtn?>"><?=$labelLocalAmount?></label>
                <? }?>
            <? } ?>
        </td>
    </tr>
    <?
    if((CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.",")) || $_GET["enterOwnRate"]=="Y")
    {

        ?>
        <tr>
            <td colspan="2"><font color="#005b90">
                    Manual Exchange Rate
                    &nbsp; &nbsp; Yes<input type="radio" name="checkManualRate" id="checkManualRate" value="Y" <? if($_SESSION["checkManualRate"] == "Y" || $_GET["enterOwnRate"]=="Y"){echo("checked");} ?>
                                            onchange="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                    No<input type="radio" name="checkManualRate" id="checkManualRate" value="N" <? if($_SESSION["checkManualRate"] != "Y"){echo("checked");} ?>
                             onchange="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                </font></td>
            <td align="right"><font color="#005b90">Reverse Calculate</font></td>
            <td><input type="checkbox" name="reverseCalculation" id="reverseCalculation" value="Y" <?=$checkedReverse?>/></td>
        </tr>
        <?
    }
    ?>
    <tr>
        <?

        if(($_SESSION["checkManualRate"] == 'Y' && CONFIG_AGENT_OWN_MANUAL_RATE == '1') || $_GET["enterOwnRate"] == "Y")
        {
            ?>
            <td align="right"><font color="#005b90">Exchange Rate<font color='#ff0000'>*</font></font></td>
            <td><input type="text" name="exchangeRate" id="exchangeRate" value="<?=$exRate?>" maxlength="25" onchange="disableClicks();"<? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('trans');" <? } ?> >
                <input type="hidden" name="exchangeID" id="exchangeID" value="<? echo $exID?>">
            </td>
            <?
        }
        else
        {

            $strDisableCode = "";
            if(CONFIG_DENOMINATION_BASED_EXCHANGE_RATE == "1")
            {
                if($_SESSION["checkManualRate"] == "N" || empty($_SESSION["checkManualRate"]))
                    $strDisableCode = "readonly='readonly'";
            }
            ?>
            <td width="128" height="20" align="right"><font color="#005b90">Exchange Rate<? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "<font color='#ff0000'>*</font>" : "");?></font></td>
            <td width="195" height="20"><input type=text name="exchangeRate" id="exchangeRate" value="<?=$exRate?>" maxlength="25" <? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "" : "readonly");?> <?=$strDisableCode?>>
                <input type="hidden" name="exchangeID" id="exchangeID" value="<? echo $exID?>">
            </td>
            <?
        }
        ?>
        <td width="96" height="20" align="right" valign="top"><font color="#005b90"><?=$labelAmount?><? if($fromTotalConfig != '1'){echo("<font color='#ff0000'>*</font>");} ?></font></td>
        <td width="241" height="20" class="style1"><input type="text" name="transAmount" id="transAmountID" maxlength="32" <? echo ((CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != "") ? "readonly" : "");?>
                                                          value="<?
                                                          if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                                                              /**
                                                               * Override the rounding through the function by
                                                               * Updating as mentioned by Ticket# 3268
                                                               */
                                                              //echo ($transAmount > 0 ? RoundVal($transAmount) : "");
                                                              /**
                                                               * New Statment
                                                               */
                                                              echo ($transAmount > 0 ? round($transAmount,0) : "");
                                                          }else{
                                                              echo ($transAmount > 0 ? round($transAmount, $roundLevel) : "");
                                                          }


                                                          ?>"
                                                          <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>size="7"<? } ?> <? if($fromTotalConfig == '1'){echo("readonly");}

            ?>
                                                          onclick="disableClicks();"
                <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?> onblur="updateAmounts('trans');<? } ?> return batch_trans_msg();">
    <span class="style1">
<?

$strQuery = "SELECT distinct(currencyOrigin) FROM ".TBL_EXCHANGE_RATES." where countryOrigin = '".$customerContent["Country"]."' and country = '$benCountryName' ORDER BY currencyOrigin ";

$result = selectMultiRecords($strQuery);

?>
        <select name="currencyFrom" id="currencyFrom" onChange="document.addTrans.action='<?=$transactionPage?>//?transID=<? echo $_GET["transID"]?>//&enterOwnRate=<?=$_GET["enterOwnRate"]?>//&focusID=<?=$focusButtonID?>//'; document.addTrans.submit();">
<?
for($i=0; $i < count($result); $i++)
{
    ?>
    <?
    if(1)
    {
        ?>
        <option  value=<?=$result[$i]["currencyOrigin"]?> selected > <?=$result[$i]["currencyOrigin"]?> </option>
        <?
    }
    elseif(CONFIG_DEFAULT_CURRENCY == '1' && CONFIG_CURRENCY_NAME == $result[$i]["currencyOrigin"])
    {
        if($_SESSION["currencyFrom"] == "" || $_SESSION["currencyFrom"] == $result[$i]["currencyOrigin"])
        {
            ?>
            <option  value=<?=$result[$i]["currencyOrigin"]?> selected > <?=$result[$i]["currencyOrigin"]?> </option>
            <?
        }else{
            ?>
            <option  value=<?=$result[$i]["currencyOrigin"]?>> <?=$result[$i]["currencyOrigin"]?> </option>
            <?
        }
    }else
    {
        ?>
        <option  value=<?=$result[$i]["currencyOrigin"]?>> <?=$result[$i]["currencyOrigin"]?> </option>
        <?
    }
}
?>
</select>
<script language="JavaScript">
SelectOption(document.addTrans.currencyFrom, "<?=$_SESSION["currencyFrom"];?>");
</script>

    </span>
            <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>
                <? if($showAmountLocalBtnFlag){?>
                    <input type="submit" name="local" id="localBtnID" value="Local Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classLocalAmount?>" <?=$localAmountBtnLen?>>
                    <label for="localBtnID" class="<?=$classLocalAmountBtn?>"><?=$labelLocalAmount?></label>
                <? }else{ ?>
                    <input type="submit" name="amount" value="Amount" onclick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classAmount?>" id="amountBtnID" />
                    <label for="amountBtnID" class="<?=$classAmountBtn?>"><?=$labelAmount?></label>
                <? }?>

            <? } ?>
        </td>
    </tr>

    <?
} else {
    ?>

    <tr>
        <td width="128" height="20" align="right">
            <font color="#005b90"><? echo $manualCode;?></font>
            <font color="#ff0000"><? if(CONFIG_TRANS_MANUAL_CODE_COMP == "1"){?>* <? }?></font>
        </td>
        <td height="20" width="195">
            <input type="text" name="refNumber" value="<?=$_SESSION["refNumber"];?>" <? if(SYSTEM == "cpexpress") { ?> style="height:25; font-size:14px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; width:150px;" <? } ?> />
        </td>
        <td width="140" height="20" align="right" valign="top"><font color="#005b90"><?=$labelAmount?><?


                if($fromTotalConfig != '1'){echo("<font color='#ff0000'>*</font>");} ?></font></td>
        <td width="313" height="20" class="style1">
            <input type="text" name="transAmount" id="transAmountID" maxlength="32" <? echo ((CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != "") ? "readonly" : "");?>
                   value="<?
                   if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_TO'){
                       /**
                        * Override the rounding through the function
                        * Updating as mentioned by Ticket# 3268
                        */
                       //echo ($transAmount > 0 ? RoundVal($transAmount) : "");
                       /**
                        * New Statment
                        */
                       echo ($transAmount > 0 ? round($transAmount,0) : "");
                   }else{
                       echo ($transAmount > 0 ? round($transAmount, 2) : "");

                   }

                   ?>"
                   <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>size="7"<? } ?> <? if($fromTotalConfig == '1'){echo("readonly");} ?>
                   onclick="disableClicks();"
                   <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('trans'); <? } ?> return batch_trans_msg();">

    <span class="style1">
<?

$strQuery = "SELECT distinct(currencyOrigin) FROM ".TBL_EXCHANGE_RATES." where countryOrigin = '".$customerContent["Country"]."' and country = '$benCountryName' ORDER BY currencyOrigin ";
if(CONFIG_ADD_TRADING_MODULE=="1" && CONFIG_TRANSACTION_CURRENCIES_TRADING=="1"){
    $strQuery = "SELECT distinct(currency) as currencyOrigin FROM accounts ORDER BY currency";
}
$result = selectMultiRecords($strQuery);
$custCurrency = selectFrom ("select currencyName from currencies where country = '".$_POST['custCountry']."'");
?>
        <select name="currencyFrom" id="currencyFrom" onChange="document.addTrans.action='<?=$transactionPage?>////?transID=<? echo $_GET["transID"]?>////&enterOwnRate=<?=$_GET["enterOwnRate"]?>////&focusID=<?=$focusButtonID?>////'; document.addTrans.submit();">
<?
// for($i=0; $i < count($result); $i++){
// 	$selected = "";
?>
            <?
            // 	if(1){
            // 		if($result[$i]["currencyOrigin"] == $custCurrency["currencyName"]){
            // 			$selected = 'selected="selected"';
            // 		}
            ?>
            <!-- 		 <option value=--><?//=$result[$i]["currencyOrigin"]?><!-- --><?// echo $selected;?><!--  > --><?//=$result[$i]["currencyOrigin"]?><!-- </option> -->
            <?
            // 	}
            // 	elseif(CONFIG_DEFAULT_CURRENCY == '1' && CONFIG_CURRENCY_NAME == $result[$i]["currencyOrigin"]){
            // 		if($_SESSION["currencyFrom"] == "" || $_SESSION["currencyFrom"] == $result[$i]["currencyOrigin"]){
            ?>
            <!--			 <option  value=--><?//=$result[$i]["currencyOrigin"]?><!-- selected > --><?//=$result[$i]["currencyOrigin"]?><!-- </option> -->
            <?
            // 		}else{
            ?>
            <!-- 			 <option  value=--><?//=$result[$i]["currencyOrigin"]?><!-- > --><?//=$result[$i]["currencyOrigin"]?><!-- </option> -->
            <?
            // 		}
            // 	}else{
            ?>
            <!-- 		 <option  value=--><?//=$result[$i]["currencyOrigin"]?><!-- > --><?//=$result[$i]["currencyOrigin"]?><!-- </option> -->
            <?
            // 	}
            // }
            ?>
            <option  value=<?=$customerCurrency;?> selected="selected"><?=$customerCurrency;?></option>
</select>
<script language="JavaScript">
SelectOption(document.addTrans.currencyFrom, "<?=$_SESSION["currencyFrom"];?>");
</script>




        </span>
            <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>
                <? if($showAmountLocalBtnFlag){?>
                    <span style="position:relative;"><input type="submit" name="local" id="localBtnID" value="Local Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="localAmount" <?=$localAmountBtnLen?>>
		   <div style="position:absolute; top:17px; left:-117px; width:182px;"><label for="localBtnID" class="<?=$classLocalAmountBtn?>"><?=$labelLocalAmount?></label></div></span>
                <? }else{ ?>
                    <span style="position:relative;"><input type="submit" name="amount" value="Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classAmount?>" id="amountBtnID" <?=$amountBtnLen?> /><div style="position:absolute; top:17px; left:-117px; width:182px;">
		   <label for="amountBtnID" class="<?=$classAmountBtn?>"><?=$labelAmount?></label></div></span>
                <? }?>
            <? } ?>
        </td>
    </tr>
    <?
    if((CONFIG_AGENT_OWN_MANUAL_RATE == '1' && strstr(CONFIG_MANUAL_RATE_USERS, $agentType.",")) || $_GET["enterOwnRate"] == "Y")
    {
        $readonlyFlag = '';
        ?>
        <tr>

            <td colspan="2"><font color="#005b90">
                    Manual Exchange Rate
                    &nbsp; &nbsp; Yes<input type="radio" name="checkManualRate" id="checkManualRate" value="Y" <? if($_SESSION["checkManualRate"] == "Y"  || $_GET["enterOwnRate"] == "Y"){echo("checked");} ?>
                                            onchange="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                    No<input type="radio" name="checkManualRate" id="checkManualRate" value="N" <? if($_SESSION["checkManualRate"] != "Y"){echo("checked"); $readonlyFlag = 'readonly';} ?>
                             onchange="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&enterOwnRate=<?=$_GET["enterOwnRate"]?>&focusID=<?=$focusButtonID?>'; document.addTrans.submit();">
                </font></td>
            <td align="right"><font color="#005b90">Reverse Calculate</font></td>
            <td><input type="checkbox" name="reverseCalculation" id="reverseCalculation" value="Y" <?=$checkedReverse?>/></td>
        </tr>
        <?
    }
    ?>
    <tr>
        <?



        if(($_SESSION["checkManualRate"] == 'Y' && CONFIG_AGENT_OWN_MANUAL_RATE == '1') || $_GET["enterOwnRate"] == "Y")
        {
            ?>
            <td align="right"><font color="#005b90">Exchange Rate<font color='#ff0000'>*</font></font></td>
            <td><input type="text" name="exchangeRate" id="exchangeRate" value="<?=$exRate?>" maxlength="25" onchange="disableClicks();" <?=$readonlyFlag?> <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('total');" <? } ?>/>
                <input type="hidden" name="exchangeID" id="exchangeID" value="<? echo $exID?>">
            </td>
            <?
        }else{
            if($_SESSION["checkManualRate"] == "N" || empty($_SESSION["checkManualRate"]))
                $readonlyFlag = "readonly='readonly'";

            ?>
            <td width="128" height="20" align="right"><font color="#005b90">Exchange Rate<? echo ((CONFIG_MANUAL_FEE_RATE == "1" && $flagBenCountry == "1") ? "<font color='#ff0000'>*</font>" : "");?></font></td>
            <td width="195" height="20" align="left"><input type=text name="exchangeRate" id="exchangeRate" value="<?=$exRate?>" maxlength="25" <?=$readonlyFlag?> />
                <input type=hidden name="exchangeID" id="exchangeID" value="<? echo $exID?>">
            </td>
            <?
        }
        ?>

        <td width="140" height="20" align="right" valign="top"><font color="#005b90"><?=$labelLocalAmount?><? if($fromTotalConfig != '1'){echo("<font color='#ff0000'>*</font>");} ?></font> </td>
        <td width="241" height="20"><input type="text" name="localAmount" id="localAmountID"
                <? echo ((CONFIG_EDIT_VALUES == "1" && $_GET["transID"] != "") ? "readonly" : "");?>
                                           value="<?
                                           if(CONFIG_TRANS_ROUND_NUMBER == 1 && CONFIG_TRANS_ROUND_CURRENCY_TYPE != 'CURRENCY_FROM'){
                                               echo ($localAmount > 0 ? round($localAmount,0) : "");
                                           }
                                           else{
                                               echo ($localAmount > 0 ? round($localAmount, 2) : "");
                                           }


                                           ?>" maxlength="25" <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>size="7"<? } ?> <? if($fromTotalConfig == '1'){echo("readonly");} ?>
                                           onclick="disableClicks();"
                                           <? if (CONFIG_SWAP_AMOUNT_LOCALAMOUNT_AJAX != "1") { ?>onblur="updateAmounts('local'); <? }?>">

        <span class="style1">
<?

$strQueryCountry1 = selectFrom("SELECT *  FROM  ".TBL_COUNTRY." where countryName = '$benCountryName'");
$strQueryCountry2 = selectFrom("SELECT *  FROM  ".TBL_COUNTRY." where countryName = '".$customerContent["Country"]."'");

$benCountryId = $strQueryCountry1["countryId"];
$custCountryId = $strQueryCountry2["countryId"];

$strQuery = "SELECT *  FROM  ".TBL_SERVICE." where fromCountryId = '$custCountryId' and toCountryId = '$benCountryId' ORDER BY currencyRec ";
if($countryBasedFlag){
    // $countryBasedFlag and $_SESSION["serviceType"] comes form add-transaction.php
    $strQuery= "select * from " . TBL_SERVICE_NEW . " where fromCountryId = '".$custCountryId."' and toCountryId = '" . $benCountryId . "' AND serviceAvailable='".$_SESSION["serviceType"]."' ORDER BY currencyRec ";
}
if(CONFIG_ADD_TRADING_MODULE=="1" && CONFIG_TRANSACTION_CURRENCIES_TRADING=="1"){
    $strQuery = "SELECT distinct(currency) as currencyRec FROM accounts ORDER BY currency";
    $currencyRecArr = SelectMultiRecords($strQuery);
    for($i=0;$i<count($currencyRecArr);$i++)
        $currencyRec[$i] = $currencyRecArr[$i]["currencyRec"];
}
else{
    $nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());

    $rstRow = mysql_fetch_array($nResult);
    $currencyRec = explode(", " , $rstRow["currencyRec"]);
    if($countryBasedFlag){
        $currencyRec = explode("," , $rstRow["currencyRec"]);
    }
}
$i = 0;

?>
            <?
            $benCurrency = selectFrom("select currencyName from currencies where country = '$benCountryName'");
            ?>
            <select name="currencyTo" id="currencyTo" onChange="document.addTrans.action='<?=$transactionPage?>////?transID=<? echo $_GET["transID"]?>////&customerID=<?=$_GET["customerID"]?>////&focusID=<?=$focusButtonID?>////'; document.addTrans.submit();">
<?
// while($i < count($currencyRec)){
// 	$selected = "";
// 	if (CONFIG_DEFAULT_CURRENCY_BEN == '1' && CONFIG_CURRENCY_NAME_BEN == $currencyRec[$i]){
// 		if ($_SESSION["currencyTo"] == "" || $_SESSION["currencyTo"] == $currencyRec[$i]){
// 			echo "<option  value ='$currencyRec[$i]' selected>".$currencyRec[$i]."</option>";
// 		}else{
// 			echo "<option  value ='$currencyRec[$i]'>".$currencyRec["dfa"]."</option>";
// 		}
// 	}else{
// 		if($currencyRec[$i] == $benCurrency["currencyName"]){
// 			$selected = 'selected="selected"';
// 		}
// 		echo "<option  value ='$currencyRec[$i]' ".$selected."> ".$currencyRec[$i]."</option>";
// 	}
// 	$i = ($i+1);
// }
//echo "<option  value ='$beneficiaryCurrency' selected='selected'>".$beneficiaryCurrency."</option>";
//foreach ($currencyBuying as $currencyItem) {
//    echo '<option  value =' . $currencyItem['currency'] . ' selected="selected">'.$currencyItem['currency'].'</option>';
//}
//echo '<option  value =' . $currencyBuying['currency'] . ' selected="selected">'.$currencyBuying['currency'].'</option>';
?>
                <option  value=<?=$beneficiaryCurrency;?> selected="selected"><?=$beneficiaryCurrency;?></option><!--Asad-->
</select>
<script language="JavaScript">
SelectOption(document.addTrans.currencyTo, "<?=$_SESSION["currencyTo"];?>");
</script>


        </span>
            <? if (CONFIG_FRONT_BUTTONS == '1' && $fromTotalConfig != '1') { ?>
                <? if($showAmountLocalBtnFlag){ ?>
                    <input type="submit" name="amount" value="Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="<?=$classAmount?>" id="amountBtnID" <?=$amountBtnLen?> >
                    <label for="amountBtnID" class="<?=$classAmountBtn?>"><?=$labelAmount?></label>
                <? }else{?>
                    <span style="position:relative;"><input type="submit" name="local" id="localBtnID" value="Local Amount" onClick="document.addTrans.action='<?=$transactionPage?>?transID=<? echo $_GET["transID"]?>&customerID=<? echo $_GET["customerID"]?>&focusID=<?=$focusButtonID?>';" class="localAmount" <?=$localAmountBtnLen?>>
		   <div style="position:absolute; top:17px; left:-117px; width:182px;"><label for="localBtnID" class="<?=$classLocalAmountBtn?>"><?=$labelLocalAmount?></label></div></span>

                <? }?>
            <? } ?>
        </td>
    </tr>

<?	}  ?>
