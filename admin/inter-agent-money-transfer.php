<?
session_start();
include ("../include/config.php");
include ("calculateBalance.php");
//include ("config.php");
include ("security.php");

$date_time = date('d-m-Y  h:i:s A');
$agentID = "";
if($_REQUEST["agentID"] != ""){
	$agentID = $_REQUEST["agentID"];
}
/*
if($_POST["agentID_W"] != "")
	$_REQUEST["agentID_W"] = $_POST["agentID_W"];

if($_POST["amount"] != "")
	$_REQUEST["amount"] = $_POST["amount"];

if($_POST["agentID_D"] != "")
	$_SESSION["agentID_D"] = $_POST["agentID_D"];

$_SESSION["note"] = $_POST["note"];
*/

$_REQUEST["sendingCurrencyName"] = $_REQUEST["sendingCurrency"];
$_REQUEST["recievingCurrencyName"] = $_REQUEST["recievingCurrency"];

/**
 * Validation and Inserting the form entries into database
 */
 	if(!empty($_REQUEST["agentID_W"]) && !empty($_REQUEST["sendingCurrencyName"]))
 		$agentBalance = agentBalanceWithCurrency($_REQUEST["agentID_W"],$_REQUEST["sendingCurrencyName"]);
 
	if( isset($_REQUEST["formSubmitted"]) && !empty($_REQUEST["agentID_W"]) && !empty($_REQUEST["agentID_D"]) && !empty($_REQUEST["amount"]) && $_REQUEST["amount"]> 0 && !empty($_REQUEST["sendingCurrencyName"]) && !empty($_REQUEST["recievingCurrencyName"]))
	{
		if( $agentBalance < 0 || $agentBalance - $_REQUEST["amount"] < 0 )
 		{
 			$msg = "Insuffcient amount to withdraw from agent";
 			$stat = 0; // failure color
 		}
 		else
		{
			/**
			 * Making the exchange rate entry in note field
			 */
			$noteToInsert = "";
			if($_REQUEST["sendingCurrency"] != $_REQUEST["recievingCurrency"])
				$noteToInsert = "( Exchange Rate: 1 ".$_REQUEST["sendingCurrency"]." = ".$_REQUEST["rate"]." ".$_REQUEST["recievingCurrency"]." ) ";
			$noteToInsert .= $_REQUEST["note"];
			
			if(!empty($_REQUEST["rate"]))
				$recevingAmount = $_REQUEST["rate"] * $_REQUEST["amount"];
			else
				$recevingAmount = $_REQUEST["amount"];
			
			/**
			 * Make Withdraw entry on the ledger of the agent which is selected in point1
			 */
			 $uniqueID = time();
			if(isAnD($_REQUEST["agentID_W"]))
			{
				$sqlWd = "insert into agent_Dist_account (agentID, dated, type, amount, currency, modified_by, description, note,TransID)
				 values (".$_REQUEST["agentID_W"].",'".date("Y-m-d")."','WITHDRAW',".$_REQUEST["amount"].",'".$_REQUEST["sendingCurrencyName"]."',".$_SESSION["loggedUserData"]["userID"].",'".$noteToInsert."', 'INTER AnD WITHDRAW','".$uniqueID."')"; 
			}
			else
			{
				$sqlWd = "insert into ".TBL_AGENT_ACCOUNT." (agentID, dated, type, amount, currency, modified_by, Description, note,TransID)
				values (".$_REQUEST["agentID_W"].",'".date("Y-m-d")."','WITHDRAW',".$_REQUEST["amount"].",'".$_REQUEST["sendingCurrencyName"]."',".$_SESSION["loggedUserData"]["userID"].",'".$noteToInsert."', 'INTER AGENT WITHDRAW','".$uniqueID."')";
			
			}
			
			insertInto($sqlWd);
 				
			/**
			 * Deposit entry in agent which is selected in point 4.
			 */
			if(isAnD($_REQUEST["agentID_W"]))
			{
				$sqlDp = "insert into agent_Dist_account (agentID, dated, type, amount, currency, modified_by, description, note,TransID)
					values (".$_REQUEST["agentID_D"].",'".date("Y-m-d")."','DEPOSIT',".$recevingAmount.",'".$_REQUEST["recievingCurrencyName"]."','".$_SESSION["loggedUserData"]["userID"]."', '".$noteToInsert."', 'INTER AnD DEPOSIT','".$uniqueID."')";
			}
			else
			{
				$sqlDp = "insert into ".TBL_AGENT_ACCOUNT." (agentID, dated, type, amount, currency, modified_by, Description, note,TransID)
				values (".$_REQUEST["agentID_D"].",'".date("Y-m-d")."','DEPOSIT',".$recevingAmount.",'".$_REQUEST["recievingCurrencyName"]."','".$_SESSION["loggedUserData"]["userID"]."', '".$noteToInsert."', 'INTER AGENT DEPOSIT','".$uniqueID."')";
			}
			insertInto($sqlDp);

 			$msg = "Funds Transfered sucessfully.";
 			$stat = 1; // pass color
 			/**
 			 * Destroying the current values as trasfer successful
 			 */
			unset($_REQUEST["agentID_W"]);
			unset($_REQUEST["agentID_D"]);
			unset($_REQUEST["amount"]);
			unset($_REQUEST["note"]);
			unset($_REQUEST["sendingCurrencyName"]);
			unset($_REQUEST["recievingCurrencyName"]);
 		}
	}
	
	/**
	 * Seleting the currencies of agent from which amount will be with draw
	 */
	$uniqueCurrencyList = array();
	if ( !empty( $_REQUEST["agentID_W"] ) )
	{

		if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1")
		{
			$sendingAgentQuery = "select agentCountry, settlementCurrency from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$_REQUEST["agentID_W"]."'";
			$sendingAgentData = selectMultiRecords( $sendingAgentQuery );
		}
		else
		{
			$sendingAgentQuery = "select agentCountry from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$_REQUEST["agentID_W"]."'";
			$sendingAgentData = selectFrom( $sendingAgentQuery );
			//debug($sendingAgentData);
			$currQuery = "SELECT distinct(currencyOrigin) FROM " . TBL_EXCHANGE_RATES . " where countryOrigin = '".$sendingAgentData["agentCountry"]."' ORDER BY currencyOrigin ";
			$currencyData = selectMultiRecords( $currQuery );
			//debug($currencyData);
			foreach( $currencyData as $currency)
			{
				if( !empty($currency["currencyOrigin"]) )
					$uniqueCurrencyList[]["settlementCurrency"] = $currency["currencyOrigin"];
			}

			$sendingAgentData = $uniqueCurrencyList;

			//debug($uniqueCurrencyList);
			
		}
		
	}

	/**
	 * Seleting the currencies of agent from which amount will be deposite
	 */
	$uniqueCurrencyListRA = array();
	if ( !empty( $_REQUEST["agentID_D"] ) )
	{

		if(CONFIG_SETTLEMENT_CURRENCY_DROPDOWN == "1")
		{
			$recievingAgentQuery = "select settlementCurrency from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$_REQUEST["agentID_D"]."'";
			$recievingAgentData = selectMultiRecords( $recievingAgentQuery );
		}
		else
		{
			$recievingAgentQuery = "select agentCountry from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$_REQUEST["agentID_D"]."'";
			$recievingAgentData = selectFrom( $recievingAgentQuery );
			
			$currQuery = "SELECT distinct(currencyOrigin) FROM " . TBL_EXCHANGE_RATES . " where countryOrigin = '".$recievingAgentData["agentCountry"]."' ORDER BY currencyOrigin ";
			$currencyData = selectMultiRecords( $currQuery );
			
			foreach( $currencyData as $currency)
			{
				if( !empty($currency["currencyOrigin"]) )
					$uniqueCurrencyListRA[]["settlementCurrency"] = $currency["currencyOrigin"];
			}	
			$recievingAgentData = $uniqueCurrencyListRA;	
		}		
	}
	
?>
<html>
<head>
	<title>Inter Agent Money Transfer </title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 

function checkForm(theForm) {
	
	if(theForm.agentID_W.options.selectedIndex == 0){
    	alert("Please select an agent for withdraw.");
        theForm.agentID_W.focus();
        return false;
  }
	
	if(theForm.sendingCurrency.options.selectedIndex == 0){
    	alert("Please select a sending currency to transfer");
        theForm.sendingCurrency.focus();
        return false;
  }
	
	if(theForm.agentID_D.options.selectedIndex == 0){
    	alert("Please select an agent for deposite.");
        theForm.agentID_D.focus();
        return false;
  }
	
	if(theForm.recievingCurrency.options.selectedIndex == 0){
    	alert("Please select a recieving currency to transfer");
        theForm.recievingCurrency.focus();
        return false;
  }
	
	if(document.getElementById("amount").value < 0 || isNaN(document.getElementById("amount").value))
	{
		alert("The amount to be transfer should be positive.");
		document.getElementById("amount").focus();
		return false;
	}
	
	if(document.getElementById("amount").value == "")
	{
		alert("Please enter the amount to transfer.");
		document.getElementById("amount").focus();
		return false;	
	}
	
	<? 
		//if(!empty($agentBalance) || $agentBalance <= 0)  
		if(!empty($_REQUEST["agentID_W"]) && !empty($_REQUEST["sendingCurrencyName"]))
		{
	?>
		var agentBalanceDisplay = <?=$agentBalance?>;
		if(agentBalanceDisplay <= 0)
		{
			alert("The agent balance must be in positive to transfer funds.");
			document.getElementById("agentID_W").focus();
			return false;
		}
		else if(agentBalanceDisplay - document.getElementById("amount").value < 0)
		{
			alert("You can enter upto "+ agentBalanceDisplay +" " + document.getElementById("sendingCurrency").value + " as Transfer funds.");
			document.getElementById("amount").focus();
			return false;
		}
	<? } ?>
 	return confirm("Do you confirm "+ document.getElementById("amount").value+" "+ theForm.sendingCurrency.value +" transfer of funds"); 
}

function updateInterface()
{
	if(document.getElementById("sendingCurrency").value != "" && document.getElementById("recievingCurrency").value != "")
	{
		if(document.getElementById("sendingCurrency").value == document.getElementById("recievingCurrency").value)
		{
			$("#exchangeRate").hide();
			document.getElementById("ramount").value = document.getElementById("amount").value;
		}
		else
		{
			$("#exchangeRate").show();
			document.getElementById("ramount").value = document.getElementById("amount").value;
			document.getElementById("rate").value = 1;
		}
	}
	document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("recievingCurrency").value;

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("recievingCurrency").value;	
}

function calculateAmount()
{
	var finalAmount;
	var amount = document.getElementById("amount").value;
	var rate = document.getElementById("rate").value;
	
	if(amount != "" && rate != "")
	{
		finalAmount = amount * rate;
		document.getElementById("ramount").value = finalAmount;
	}
	
	if(document.getElementById("sendingCurrency").value == document.getElementById("recievingCurrency").value)
	{
		document.getElementById("ramount").value = document.getElementById("amount").value;
	}
}


function placeValues()
{
	document.getElementById("sc").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("rc").innerHTML = document.getElementById("recievingCurrency").value;

	document.getElementById("ta").innerHTML = document.getElementById("sendingCurrency").value;
	document.getElementById("ra").innerHTML = document.getElementById("recievingCurrency").value;		

	updateInterface();
}

-->
</script>
</head>
<body onLoad="placeValues();">
<div class="topbar">
  <strong><font class="topbar_tex">Inter Agent Money Transfer</font></strong>
</div>
<form action="inter-agent-money-transfer.php" method="post" onSubmit="return checkForm(this);" name="agentForm">
<table width="100%" border="1" cellspacing="1" cellpadding="5">
	<tr>
    <td align="center">
			<table width="650" border="0" cellspacing="1" cellpadding="2" align="center">
        <? if ($msg != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE">
            	<table width="100%" cellpadding="5" cellspacing="0" border="1">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($stat ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ( $stat ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ( $stat ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$msg." </b><br><br></font>"; ?></td>
                </tr>
              </table>
            </td>
          </tr>
        <? } ?>
        <tr bgcolor="#ededed"> 
	          <td width="144"><font color="#005b90"><strong>Sending Agent <font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
					$excludeW = "";
					if(!empty($_REQUEST["agentID_D"]))
							$excludeW = " and userID !=".$_REQUEST["agentID_D"];

					$qry1 = "select userID, username, agentCompany, name, agentCountry, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Agent' and isCorrespondent != 'ONLY' and agentStatus='Active' and fromServer='' ".$excludeW." order by name";
					$src = selectMultiRecords($qry1);
            	?>
           <select name="agentID_W" id="agentID_W" style="font-family:verdana; font-size: 11px" onChange="document.agentForm.submit();">
							<option value="">- Select Agent -</option>
							<?
								for ($i=0; $i < count($src); $i++)
								{
									if($_REQUEST["agentID_W"] == $src[$i]["userID"]):
									?>
										<option value="<?=$src[$i]["userID"]; ?>" selected><? echo $src[$i]["agentCompany"] . " [" . $src[$i]["username"] . "]" ; ?></option>
									<?
									else:
									?>
										<option value="<?=$src[$i]["userID"]; ?>"><? echo $src[$i]["agentCompany"] . " [" . $src[$i]["username"] . "]" ; ?></option>
									<?
									endif;
								}
							?>
				</select>
				&nbsp;
				&nbsp;
       			<select name="sendingCurrency" id="sendingCurrency" style="font-family:verdana; font-size: 11px" onChange="document.agentForm.submit();updateInterface();">
					<option value="">- Currency  -</option>
					<? 
					foreach ($sendingAgentData as $uniqueCurrency)
					{
						//if(!empty($sendingAgentData["settlementCurrency"]))
						//{
							$uniqueCurrency = $uniqueCurrency["settlementCurrency"];
							$selected = "";
							if( $_REQUEST["sendingCurrencyName"] == $uniqueCurrency )
								$selected = "selected='selected'";
							?>
								<option <?=$selected;?> value="<?=trim($uniqueCurrency)?>" ><?=trim($uniqueCurrency); ?></option>
							<?
						//}
					}
					?>
			  	</select>
			</td>
	  </tr>
	  <?
	  	if(!empty($_REQUEST["sendingCurrencyName"]) && !empty($_REQUEST["agentID_W"]))
	  	{
	  ?>
	  		<tr bgcolor="#ededed"> 
	  				<td>&nbsp;</td>
            <td width="144">
            	<font color="#005b90"><strong>Agent Balance</strong></font>
            	:<b><?=$agentBalance." ".$_REQUEST["sendingCurrencyName"]?></b>
            </td>
				</tr>
	  <?
	  	}
	  ?>
		<tr bgcolor="#ededed"> 
	    	<td width="144"><font color="#005b90"><strong>Receiving Agent <font color="#ff0000">*</font></strong></font></td>
            <td>
            	<?
					$agents = array();
					if ( !empty( $_REQUEST["sendingCurrencyName"] ) )
					{
						$excludeD = "";
						if($_REQUEST["agentID_W"] != "")
								$excludeD = " and userID !=".$_REQUEST["agentID_W"];
	
						$qry2 = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where adminType='Agent' and isCorrespondent != 'ONLY' and agentStatus='Active'  ".$excludeD." order by name";
						$agents = selectMultiRecords($qry2);
						
					}
				?>
	         	<select name="agentID_D" style="font-family:verdana; font-size: 11px" onChange="document.agentForm.submit();">
					<option value="">- Select Agent -</option>
					<?
						for ($i=0; $i < count($agents); $i++)
						{
								if($_REQUEST["agentID_D"] == $agents[$i]["userID"]):
					?>
							<option value="<?=$agents[$i]["userID"]; ?>" selected><? echo $agents[$i]["agentCompany"] . " [" . $agents[$i]["username"] . "]" ; ?></option>
					<?
						else:
					?>
							<option value="<?=$agents[$i]["userID"]; ?>"><? echo $agents[$i]["agentCompany"] . " [" . $agents[$i]["username"] . "]" ; ?></option>
					<?
						endif;
						}
					?>
				</select>
				&nbsp;
				&nbsp;
				<select name="recievingCurrency" id="recievingCurrency" style="font-family:verdana; font-size: 11px" onChange="updateInterface();">
					<option value="">- Currency  -</option>
					<?
					foreach ($uniqueCurrencyListRA as $uniqueCurrency )
					{
						//if(!empty($recievingAgentData["settlementCurrency"]))
						//{
							$uniqueCurrency = $uniqueCurrency["settlementCurrency"];
							$selected = "";
							if( $_REQUEST["sendingCurrencyName"] == $uniqueCurrency )
								$selected = "selected='selected'";
							
					?>
						<option <?=$selected;?> value="<?=trim($uniqueCurrency)?>" ><?=trim($uniqueCurrency); ?></option>
					<? } ?>
				</select>
					</td>
			</tr>
			<tr bgcolor="#ededed"> 
				<td width="144"><font color="#005b90"><strong>Transfer Funds<font color="#ff0000">*</font></strong></font></td>
				<td>
					<input type="text" id="amount" name="amount" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["amount"]?>" onBlur="calculateAmount();" />
					&nbsp;<b id="ta"></b>
					  </td>
					</tr>
					<tr bgcolor="#ededed" id="exchangeRate" style="display:none"> 
				<td width="144"><font color="#005b90"><strong>Exchange Rate<font color="#ff0000">*</font></strong></font></td>
				<td>
					<b> 1 </b><span id="sc"><?=$_REQUEST["sendingCurrencyName"]?></span> = 
					<input type="text" size="5" maxlength="5" id="rate" name="rate" style="font-family:verdana; font-size: 11px" value="<?=$_REQUEST["rate"]?>" onBlur="calculateAmount();" />
					<span id="rc"><?=$_REQUEST["recievingCurrencyName"]?></span>
					  </td>
					</tr>
					<tr bgcolor="#ededed"> 
				<td width="144"><font color="#005b90"><strong>Receiving Funds</strong></font></td>
				<td>
					<input type="text" id="ramount" name="ramount" style="font-family:verdana; font-size: 11px" value="" readonly/>
					&nbsp;<b id="ra"></b>
					  </td>
			</tr>		
			<tr bgcolor="#ededed"> 
				<td width="144" valign="top"><font color="#005b90"><strong>Transfer Fund Note</strong></font></td>
				<td>
					<textarea name="note" wrap="soft" cols="47" rows="7"><?=$_REQUEST["note"]?></textarea>
			  </td>
			</tr>
    	</table>
  	</td>
  </tr>
          
  <tr bgcolor="#ededed"> 
  	<td colspan="3" align="center"> <input type="submit" value="Transfer Fund" name="formSubmitted"> </td>
  </tr>
</table>
</form>
</body>
</html>
