<?

function transStatusChange($transId, $newStatus, $username, $remarks, $refundFee='Yes')
{
	$updateTrans = "update " . TBL_TRANSACTIONS . " set transStatus= '".$newStatus."'";
	
	
	$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount,localAmount,benAgentID, AgentComm, IMFee, createdBy,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '".$transId."'");
	
	if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1"){
		$transBalance = $amount["localAmount"];
		if(!defined("CONFIG_CANCELLATION_LEDGER_CURRENCY") && CONFIG_CANCELLATION_LEDGER_CURRENCY!="1")
			$currencyFrom = $amount["currencyTo"];
		else
			$currencyFrom = $amount["currencyFrom"];
  }else{		
  	if(CONFIG_SAVE_TOTAL_AMOUNT_IN_DISTRIBUTOR_LEDGER)
	{
		$transBalance = $amount["totalAmount"];
	} else {
  		$transBalance = $amount["transAmount"];
	}
	
  	$currencyFrom = $amount["currencyFrom"];
  }
  
	$commission = $amount["AgentComm"];
	$balance = $amount["totalAmount"]; 
	$agent = $amount["custAgentID"];
	$bank = $amount["benAgentID"];
	
	$agentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent."'");
	
	
	if($newStatus == 'Authorize')
	{
			$updateTrans .= " , authorisedBy = '".$username."',
													authoriseDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
		  $descript = "Transaction is Authorized";									
		  
		  
		  
		  
		  
		  
		if(CONFIG_LEDGER_AT_CREATION != "1")
		{
			if($amount["createdBy"] != 'CUSTOMER')
				{
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
						
						if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
											$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
											$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
											
												if($agentPaymentMode == 'exclude')
												{
														$balance = $amount["totalAmount"] - $commission;
												
												}elseif($agentPaymentMode == 'include')
												{
														$balance = $amount["totalAmount"];
													
												}
	
							}else{
							$balance = $amount["totalAmount"];
						}
						
						$agentEntryType = "WITHDRAW";
					  $agentEntryDesc = "Transaction Authorized";
					 
						$updateAgentAccount = true;
						
					}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						//For these customer, ledger is created in the start
							/*insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						 values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						 
				
						$newBalance = $payin["balance"] - $balance ; 
						$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
						update($update_Balance);*/
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Authorized','WITHDRAW','".$amount["totalAmount"]."','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					if(CONFIG_ONLINE_AGENT == '1')
					{	
						$descript = "Transaction Authorized";
						updateAgentAccount($agentContents["userID"], $amount["totalAmount"], $imReferenceNumber, "WITHDRAW", $descript, 'Agent',$note,$currencyFrom);	
					}	
				}
			}
			
			if(CONFIG_POST_PAID != '1')
		{
			$dist = $amount["benAgentID"];
			$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
			$currentBalance = $benAgentContents["balance"];
						
			
			$currentBalance =  $currentBalance - $transBalance;
			update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
				
			$distributorEntryType = "WITHDRAW";
			$distributorEntryDesc = "Transaction Authorized";
					 
			$updateBankAccount = true;
							

		}
	
	$printingMessage = "Transaction is Authorized";
			
			
	}
	////////////////////////////Transaction has been Authorized////////////////
	///////////////////////////////////////Now Start Transaction Cancelled Or Failed////////////
	if($newStatus == 'Cancelled')
	{
	
		$updateTrans .= " , cancelledBy = '".$username."',
													cancelDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													refundFee = '".$refundFee."',
													remarks		= '".$remarks."',
													cancelReason='".$remarks."'
													";
		  $descript = "Transaction is Cancelled";									
		  
		  
	
	
	//if(CONFIG_LEDGER_AT_CREATION == "1")
	//{
				if($amount["createdBy"] != 'CUSTOMER')
				{
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
							if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
											$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
											$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
											
												if($agentPaymentMode == 'exclude')
												{
														$balance = $amount["totalAmount"] - $commission;
												
												}elseif($agentPaymentMode == 'include')
												{
														$balance = $amount["totalAmount"];
													
												}
	
							}else{
							$balance = $amount["totalAmount"];
						}
						
						

						if($refundFee == "No"){
							$balance = $amount["totalAmount"] - $amount["IMFee"];
						}else{
							$balance = $balance;
							}
							
						$agentEntryType = "DEPOSIT";
					  $agentEntryDesc = "Transaction Cancelled";
					 
						$updateAgentAccount = true;
						
						
						$currentBalance = $balance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
						
						

					}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						
						if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
						{
							$strAmountSql = "select amount from agents_customer_account where tranRefNo = ".$transId;
							$arrAmountData = selectFrom($strAmountSql);
							$balance = $arrAmountData["amount"];
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
					 		values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
						else
						{
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						  values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','".$balance."','".$transId."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
						 
				
						$newBalance = $payin["balance"] + $balance ; 
						$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
						update($update_Balance);
						
						if(CONFIG_PAYIN_CUST_AGENT == '1')
						{
							$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
							if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agent !=""){
								$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent ."'";
							}
							$agentPayinContents = selectFrom($agentPayinQuery);
							if($agentPayinContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$currencyFrom,getCountryTime(CONFIG_COUNTRY_CODE));
								$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$note,$currencyFrom, getCountryTime(CONFIG_COUNTRY_CODE));
							}else{
								$q = updateAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', "Transaction Cancelled", "Agent",$note,$currencyFrom, getCountryTime(CONFIG_COUNTRY_CODE));
							}	
						}
					
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					 if(CONFIG_ONLINE_AGENT == '1')
					{	
						
						$descript = "Transaction is Cancelled";
						updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent',$note,$currencyFromD);	
					}	
				}
				
				if(CONFIG_POST_PAID != '1')
				{
					
									
					$dist = $amount["benAgentID"];
					$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
					$currentBalance = $benAgentContents["balance"];
							
					$currentBalance =  $currentBalance + $transBalance;
					update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
						
					$distributorEntryType = "DEPOSIT";
					$distributorEntryDesc = "Transaction Cancelled";
							 
					$updateBankAccount = true;
					
					}
	
	if(CONFIG_DOUBLE_ENTRY == '1')
	{
		 $param = array(
			 "transID"=> $transId,
			 "amount"=> $balance,
			 "refund"=> $refundFee,
			 "description"=>"Transaction Cancelled",
			 "action"=>'CANCELLED_TRANSACTION'
		 );
		  updateLedger($param);
 }		
	//}
	
	$printingMessage = "Transaction is Cancelled ";
	if(CONFIG_DIST_REF_NUMBER == "1" && CONFIG_REUSE_DIST_REF_NUMBER == "1")
	{
		removeDistRef($transId, $printingMessage);
	}
}
//////////////////////////////////////////////Cancelled transactions Ended////////////////////////////


///////////////////////////////////////////Cancel Return Starts here///////////////////////////
if($newStatus == 'Cancelled - Returned')
	{
	
		$updateTrans .= " , returnedBy = '".$username."',
													returnDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'
													
													";
		  $descript = "Transaction is Cancelled - Returned";									
		  
		  
	
	
	//if(CONFIG_LEDGER_AT_CREATION == "1")
	//{
				if($amount["createdBy"] != 'CUSTOMER')
				{
					$payin = selectFrom("select balance, payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
					if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
					{
						
							if(CONFIG_EXCLUDE_COMMISSION == '1')
						{
							$balance = $amount["totalAmount"] - $commission;
						}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
	
											$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
											$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
											
												if($agentPaymentMode == 'exclude')
												{
														$balance = $amount["totalAmount"] - $commission;
												
												}elseif($agentPaymentMode == 'include')
												{
														$balance = $amount["totalAmount"];
													
												}
	
							}else{
							$balance = $amount["totalAmount"];
						}
						
						if($refundFees == "No"){
							$balance = $amount["totalAmount"] - $amount["IMFee"];
						}else{
							$balance = $balance;
							}
						
						$agentEntryType = "DEPOSIT";
					  $agentEntryDesc = "Transaction Cancelled - Returned";
					 
						$updateAgentAccount = true;
						
						
						$currentBalance = $balance + $currentBalance;
						update("update " . TBL_ADMIN_USERS . " set balance = $currentBalance where userID = '$agent'");
						
						

					}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
					{
						
						if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
						{
							$strAmountSql = "select amount from agents_customer_account where tranRefNo = ".$transId;
							$arrAmountData = selectFrom($strAmountSql);
							$balance = $arrAmountData["amount"];
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						  values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled - Returned','DEPOSIT','".$balance."','".$_GET["transID"]."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
						else
						{
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						  values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled - Returned','DEPOSIT','".$balance."','".$_GET["transID"]."','".$_SESSION["loggedUserData"]["userID"]."')");
						}
				
						$newBalance = $payin["balance"] + $balance ; 
						$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
						update($update_Balance);
						
						if(CONFIG_PAYIN_CUST_AGENT == '1')
						{
							$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
							if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agent !=""){
								$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent ."'";
							}
							$agentPayinContents = selectFrom($agentPayinQuery);
							if($agentPayinContents["agentType"] == 'Sub')
							{
								updateSubAgentAccount($agentPayinContents["userID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled - Returned", "Agent",$currencyFrom,getCountryTime(CONFIG_COUNTRY_CODE));
								$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled - Returned", "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
							}else{
								$q = updateAgentAccount($agentPayinContents["userID"], $balance, $_GET["transID"], 'DEPOSIT', "Transaction Cancelled - Returned", "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
							}	
						}
					
					}
				}elseif($amount["createdBy"] == 'CUSTOMER')
				{
					$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
					 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Cancelled - Returned','DEPOSIT','$balance','".$amount["refNumberIM"]."'
					 )";
					 insertInto($strQuery);
					 
					 if(CONFIG_ONLINE_AGENT == '1')
					{	
						
						$descript = "Transaction is Cancelled - Returned";
						updateAgentAccount($agent, $balance, $amount["refNumberIM"], "DEPOSIT", $descript, 'Agent',$note,$currencyFrom);	
					}	
				}
				
				if(CONFIG_POST_PAID != '1')
				{
					
									
					$dist = $amount["benAgentID"];
					$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '$dist'");
					$currentBalance = $benAgentContents["balance"];
							
					$currentBalance =  $currentBalance + $transBalance;
					update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$bank'");
						
					$distributorEntryType = "DEPOSIT";
					$distributorEntryDesc = "Transaction Cancelled - Returned";
							 
					$updateBankAccount = true;
					
					}
	
		
	//}
	
	$printingMessage = "Transaction is Cancelled - Returned ";
	
}
//////////////////////////////////////////////Cancelled Return transactions Ended////////////////////////////



//////////////////////////////////////////////Transaction Failed Or Rejected////////////////////

			if($newStatus == "Failed")////Deposit Agent		
				{
					$updateTrans .= " , deliveredBy = '".$username."',
													failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
		  		$descript = "Transaction is Failed";							
					
				
					if($amount["createdBy"] != 'CUSTOMER')
					{
						$payin = selectFrom("select payinBook, customerID, balance from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");
						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1")
						{
							
							if(CONFIG_EXCLUDE_COMMISSION == '1')
							{
								$balance = $balance - $commission;
								
							}elseif( CONFIG_AGENT_PAYMENT_MODE == "1"){
									
									$agentPaymentModeQuery = selectFrom("select paymentMode  from ". TBL_ADMIN_USERS." where userID = '".$_POST["custAgentID"]."' ");
									$agentPaymentMode = $agentPaymentModeQuery["paymentMode"];
									
										if($agentPaymentMode == 'exclude'){
											
											$balance = $balance - $commission;
											
										}elseif($agentPaymentMode == 'include'){
													
													$balance = $balance;
										}
									
							}else{
								$balance = $balance;
								}
								
								
								
								
									
								
								
								
							
								$agentEntryType = "DEPOSIT";
					  		$agentEntryDesc = "Transaction Failed";
					 
								$updateAgentAccount = true;
						
							
							$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '".$amount["custAgentID"]."'");
							$currentBalance = $agentContents["balance"];
							
							$currentBalance = $balance + $currentBalance;
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
																		
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						{
							
							
								if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
								{
									$strAmountSql = "select amount from agents_customer_account where tranRefNo = ".$transId;
									$arrAmountData = selectFrom($strAmountSql);
									$balance = $arrAmountData["amount"];
								
									insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
							 		values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Failed','DEPOSIT','".$balance."','".$transId."','".$agent."'
							 		)");		
								}
								else
								{
									insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
								 	values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction Failed','DEPOSIT','".$balance."','".$transId."','".$agent."'
								 	)");			
		
								}

								$newBalance = $payin["balance"] + $balance ; 
								$update_Balance = "update ".TBL_CUSTOMER." set balance = '".$newBalance."' where customerID= '".$payin["customerID"]."'";
								update($update_Balance);
								
	
						 
						  if(CONFIG_PAYIN_CUST_AGENT == '1')
							{
								$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".CONFIG_PAYIN_AGENT_NUMBER."'";
								if(CONFIG_PAYIN_CUST_AGENT_ALL=="1" && $agent !=""){
									$agentPayinQuery = "select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$agent ."'";
								}
 							    $agentPayinContents = selectFrom($agentPayinQuery);
								if($agentPayinContents["agentType"] == 'Sub')
								{
									updateSubAgentAccount($agentPayinContents["userID"], $balance,$transId, 'DEPOSIT', "Transaction Failed", "Agent",$currencyFrom,getCountryTime(CONFIG_COUNTRY_CODE));
									$q = updateAgentAccount($agentPayinContents["parentID"], $balance, $transId, 'DEPOSIT', "Transaction Failed", "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
								}else{
									$q = updateAgentAccount($agentPayinContents["userID"], $balance, $transId, 'DEPOSIT', "Transaction Failed", "Agent", getCountryTime(CONFIG_COUNTRY_CODE),$note,$currencyFrom);
								}	
								if(CONFIG_CREATE_ADMIN_STAFF_LEDGERS == "1")
									updateAdminStaffLedger($transId, "DEPOSIT", "Transaction Failed", $note, $currencyFrom);
							}
						 
						}
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Failed','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
				if(CONFIG_POST_PAID != '1')
				{
					
					$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$bank."'");
					//$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
					$currentBalance = $benAgentContents["balance"];
							
					$currentBalanceBank =  $currentBalance + $transBalance;
					update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '$bank'");
						
					
					
					
					
					
						$distributorEntryType = "DEPOSIT";
						$distributorEntryDesc = "Transaction Failed";
					 
						$updateBankAccount = true;
					
					
				  }
				  if(CONFIG_DIST_REF_NUMBER == "1" && CONFIG_REUSE_DIST_REF_NUMBER == "1")
					{
						removeDistRef($transId, "Transaction is Failed");
					}
				}
	
	//////////////////////////////////////////////Failed transactions Ended////////////////////////////
//////////////////////////////////////////////Transaction Picked up or Credited////////////////////
							
						if($newStatus == "Picked up" || $newStatus == "Credited")////Deposit Agent		
						{		
							 $updateTrans .= " , deliveredBy = '".$username."',
													deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'";
														
							if(CONFIG_SHARE_OTHER_NETWORK == '1')
	            {    
	             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$transId."' and generatedLocally = 'N'");   
	             
	            }   							
														
							if(CONFIG_POST_PAID == '1' || (CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1' && $sharedTrans["localTrans"] == $transId))
							{
								
								
							//	echo("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$bank."'");
								$benAgentContents = selectFrom("select userID, balance, isCorrespondent, parentID, agentType from " . TBL_ADMIN_USERS . " where userID = '".$bank."'");
								
					$currentBalance = $benAgentContents["balance"];
							
					$currentBalanceBank =  $currentBalance + $transBalance;
					update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '$bank'");
					
								
								
								
								
								
								
									$distributorEntryType = "WITHDRAW";
							if($newStatus == "Picked up")
							{
								$distributorEntryDesc = "Transaction Picked up";
								$descript = "Transaction is Picked up";
								}elseif($newStatus == "Credited")
								{
									$distributorEntryDesc = "Transaction Credited";
									$descript = "Transaction is Credited";
								}			
						
					 
							$updateBankAccount = true;
						
						}
						
						
							
							
						}
	
//////////////////////////Updating Queries and Accounts//////////////
	if($updateAgentAccount)
	{
			if(CONFIG_SETTLEMENT_AMOUNT_AGENT_ACCOUNT_STATEMENT == '1')
			{
				/* Changing localAmount to totalAmount as bug found in Ticket #3829*/
				$amountToLedger = $amount["totalAmount"];	
				$currecyToLedger = $amount["currencyTo"];
			}else{
				$amountToLedger = $balance;	
				$currecyToLedger = $amount["currencyFrom"];
				}	
			
		
		
			if($agentContents["agentType"] == 'Sub')
			{
				updateSubAgentAccount($agentContents["userID"], $balance, $transId, $agentEntryType, $agentEntryDesc, "Agent",$currecyToLedger);
				updateAgentAccount($agentContents["parentID"], $amountToLedger, $transId, $agentEntryType, $agentEntryDesc, "Agent",$note,$currecyToLedger);
				
			}else{
				updateAgentAccount($agentContents["userID"], $amountToLedger, $transId, $agentEntryType, $agentEntryDesc, "Agent",$note,$currecyToLedger);
			}
			if(CONFIG_CREATE_ADMIN_STAFF_LEDGERS == "1")
				updateAdminStaffLedger($transId, $agentEntryType, $agentEntryDesc, $note, $currecyToLedger);
	 }
		
		if($updateBankAccount)
		{
			
			/*if(CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == '1')
			{
				$amountToLedger = $amount["localAmount"];	
				$currecyToLedger = $amount["currencyTo"];
			}else{
				$amountToLedger = $transBalance;
				$currecyToLedger = $currencyFrom;
				}	*/
		  
			
			if($benAgentContents["agentType"] == 'Sub')
			{
				
				updateSubAgentAccount($benAgentContents["userID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor",$currencyFrom);
				updateAgentAccount($benAgentContents["parentID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor",$note,$currencyFrom);
			}else{
				
				updateAgentAccount($benAgentContents["userID"], $transBalance, $transId, $distributorEntryType, $distributorEntryDesc, "Distributor",$note,$currencyFrom);
			}
			
		}
	
	 $updateTrans .= "  where transID='".$transId."'";
	//if($newStatus != "Picked up" && $newStatus != "Credited")////Deposit Agent		
	{
		update($updateTrans);
	}
	
	activities($_SESSION["loginHistoryID"],"UPDATION",$transId,TBL_TRANSACTIONS,$descript);
insertError($printingMessage);
	///////////////////////////////////////End///////////////////////////////
	
// Mail is sent on Status update of Transaction. b Aslam Shahid	
	if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1" || CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
	
		$clientCompany = COMPANY_NAME;
		$systemPre = SYSTEM_PRE;
	
		$custID 	= $amount["customerID"];
		$benID 		= $amount["benID"];
		$custAgentID = $amount["custAgentID"];
		$benAgentID = $amount["benAgentID"];
		$imReferenceNumber = $amount["refNumberIM"];
		$manualRefrenceNumber = $amount["refNumber"];
		$collectionPointID = $amount["collectionPointID"];
		$type  = $amount["transType"];
		$transDate   = $amount["transDate"];
		if($transDate!=""){
			$transDate = date("F j, Y", strtotime($transDate));
		}
		$currencyFrom 	= $amount["currencyFrom"];
		$currencyTo 	= $amount["currencyTo"];
		
		///Exchange rate used 
		$exRate = $amount["exchangeRate"];
	
		$transAmount = $amount["transAmount"];
		$fee = $amount["IMFee"];
		$localamount =($transAmount * $exRate);
		$purpose = $amount["transactionPurpose"];
		$totalamount =  $transAmount + $fee;
		$moneypaid = $amount["moneyPaid"];
		if(CONFIG_CASH_PAID_CHARGES_ENABLED && $moneypaid=='By Cash')
		{
			$cashCharges = $amount["cashCharges"];
			$totalamount =  $totalamount +$cashCharges;
		}
		if($amount["authoriseDate"]!=""){
			$strAuthoriseDate = date("F j, Y", strtotime($amount["authoriseDate"]));
		}
		 
		$fromName = SUPPORT_NAME;
		$fromEmail = SUPPORT_EMAIL;
		// getting Admin Email address
		$adminContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where username= 'admin'");
		$adminEmail = $adminContents["email"];
		// Getting customer's Agent Email
		if(CONFIG_AGENT_EMAIL_ADD_TRANS_ENABLED)
		{
		$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$custAgentID'");
		$custAgentEmail	= $agentContents["email"];
		}
		// getting BEneficiray agent Email
		if(CONFIG_DISTRIBUTOR_EMAIL_ADD_TRANS_ENABLED)
		{
		$agentContents = selectFrom("select email from " . TBL_ADMIN_USERS . " where userID = '$benAgentID'");
		$benAgentEmail	= $agentContents["email"];
		}
		// getting beneficiary email
		$benContents = selectFrom("select email from ".TBL_BENEFICIARY." where benID= '$benID'");
		$benEmail = $benContents["email"];
		// Getting Customer Email
		$custContents = selectFrom("select email from " . TBL_CUSTOMER . " where customerID= '$custID'");
		$custEmail = $custContents["email"];
		
		if($amount["createdBy"] == 'CUSTOMER')
		{
		
					$subject = "Status Updated";
		
					$custContents = selectFrom("select c_name,Title,FirstName,MiddleName,LastName,c_country,c_city,c_zip,username,c_email,c_phone from cm_customer where c_id= '". $custID ."'");
					$custEmail = $custContents["c_name"];
					$custTitle = $custContents["Title"];
					$custFirstName = $custContents["FirstName"];
					$custMiddleName = $custContents["MiddleName"];
					$custLastName = $custContents["LastName"];
					$custCountry = $custContents["c_country"];
					$custCity = $custContents["c_city"];
					$custZip = $custContents["c_zip"];
					$custLoginName = $custContents["username"];
					$custEmail = $custContents["c_email"];
					$custPhone = $custContents["c_phone"];
		
		
					$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from cm_beneficiary where benID= '". $benID ."'");
					$benTitle = $benContents["Title"];
					$benFirstName = $benContents["firstName"];
					$benMiddleName = $benContents["middleName"];
					$benLastName = $benContents["lastName"];
					$benAddress  = $benContents["Address"];
					$benCountry = $benContents["Country"];
					$benCity = $benContents["City"];
					$benZip = $benContents["Zip"];
					$benEmail = $benContents["email"];
					$benPhone = $benContents["Phone"];
		
		
		/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
					$AdmindLoginName = $AdmindContents["username"];
					$AdmindName = $AdmindContents["name"];
					$AdmindEmail = $AdmindContents["email"];*/
		
		/***********************************************************/
		$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
			<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
		  </tr>";
					
		$messageCust =     "				
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  ".$custTitle." ".$custFirstName." ".$custLastName."  </p></td>
							  </tr>				
					
										  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
									  <tr>
								<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
							  </tr>			
					
					
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
								
					
							  <tr>
								<td> Transaction Type:  ".$type." </td>
								<td> Status: ".$newStatus." </td>
							  </tr>
							  <tr>
								<td> Transaction No:   ".$imReferenceNumber." </td>
								<td> Transaction Date:  ".$transDate."  </td>
							  </tr>
							  <tr>
								<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
								<td> &nbsp;</td>
							  </tr>
							  <tr>
								<td><p>Dated: ".date("F j, Y")."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							
							
							  <tr>
								<td><p><strong>Beneficiary Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Address:  ".$benAddress." </td>
								<td> Postal / Zip Code:  ".$benZip." </td>
							  </tr>
							  <tr>
								<td> Country:   ".$benCountry."   </td>
								<td> Phone:   ".$benPhone."   </td>
							  </tr>
							  <tr>
								<td><p>Email:  ".$benEmail."   </p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";
		$messageBen =     "				
							  <tr>
								<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
							  </tr>				
					
										  <tr>
								<td width='205'>&nbsp;</td>
								<td width='295'>&nbsp;</td>
							  </tr>
									  <tr>
								<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
							  </tr>			
					
					
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Transaction Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
								
					
							  <tr>
								<td> Transaction Type:  ".$type." </td>
								<td> Status: ".$newStatus." </td>
							  </tr>
							  <tr>
								<td> Transaction No:   ".$imReferenceNumber." </td>
								<td> Transaction Date:  ".$transDate."  </td>
							  </tr>
							  <tr>
								<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
								<td> &nbsp;</td>
							  </tr>
							  <tr>
								<td><p>Dated: ".date("F j, Y")."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							
							
							  <tr>
								<td><p><strong>Sender Detail: </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td colspan='2'>-----------------------------------------------------------------------------------</td>
							  </tr>
							  <tr>
								<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Country:   ".$custCountry."   </td>
								<td> Phone:   ".$custPhone."   </td>
							  </tr>
							  <tr>
								<td><p>Email:  ".$custEmail."   </p></td>
								<td> Postal / Zip Code:  ".$custZip." </td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>";			
					if(trim($type) == 'Bank Transfer')
					{
						$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benContents["Country"]."'");
		
						$strQuery = "SELECT * FROM cm_bankdetails where benID= '". $benID ."'";
						$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
						$rstRow = mysql_fetch_array($nResult);
		
		
						if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
						{
							$message .= "	<tr>
												<td>IBAN</td>
												<td>" . $rstRow["IBAN"] . "</td>
											</tr>";
						}
						else
						{
							$bankNameV = $rstRow["bankName"];
							$branchCodeV = $rstRow["branchCode"];
							$branchAddressV = $rstRow["branchAddress"];
							$swiftCodeV = $rstRow["swiftCode"];
							$accNoV = $rstRow["accNo"];
							$branchCityV = $rstRow["BranchCity"];
							
							$messageRemain="	<tr>
											<td><p><strong>Beneficiary Bank Details </strong></p></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td> Bank Name:  ".$bankNameV."  </td>
											<td> Acc Number:  ".$accNoV."  </td>
										</tr>
										<tr>
											<td> Branch Code:  ".$branchCodeV."  </td>
											<td> Branch Address:  ".$branchAddressV.", ".$branchCityV."  </td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>";
						}
					}
					elseif(trim($type) == "Pick up")
					{
					
					$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone, from cm_collection_point where cp_id  = '". $collectionPointID ."'");
					$agentname = $cContents["cp_corresspondent_name"];
					$contactperson = $cContents["cp_corresspondent_name"];
					$company = $cContents["cp_branch_name"];
					$address = $cContents["cp_branch_address"];
					$country = $cContents["cp_country"];
					$city = $cContents["cp_city"];
					$phone = $cContents["cp_phone"];
					$tran_date = date("Y-m-d");
					$tran_date = date("F j, Y", strtotime("$tran_date"));
					
							$messageRemain = "
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td><p><strong>Collection Point Details </strong></p></td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td> Agent Name : ".$agentname." </td>
								<td> Contact Person:  ".$contactperson." </td>
							  </tr>
							  <tr>
								<td> Company:  ".$company." </td>
								<td> Address:  ".$address." </td>
							  </tr>
							  <tr>
								<td>Country:   ".$country."</td>
								<td>City:  ".$city."</td>
							  </tr>
							  <tr>
								<td>Phone:  ".$phone."</td>
								<td>&nbsp;</td>
							  </tr> ";
		}
		
		/*if($amount > 500)
		{
			$tempAmount = ($amount - 500);
			$nExtraCharges =  (($tempAmount * 0.50)/100);
			$charges = ($fee + $amount + $nExtraCharges);
		}*/
			
		/*if($trnsid[$i] != "")
		{
		 update("update transactions set IMFee = '$fee',
														 totalAmount  = '$totalamount',
														 localAmount = '$localamount',
														 exchangeRate  = '$exRate'
															 where
															 transID  = '". $trnsid[$i] ."'");
		}*/
		
		$messageEnd ="
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Amount Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Exchange Rate:  ".$exRate."</td>
			<td> Amount:  ".$transAmount." </td>
		  </tr>
		  <tr>
			<td> ".$systemPre." Fee:  ".$fee." </td>
			<td> Local Amount:  ".$localamount." </td>
		  </tr>  <tr>
			<td> Transaction Purpose:  ".$purpose." </td>
			<td> Total Amount:  ".$totalamount." </td>
		  </tr>  <tr>
			<td> Money Paid:  ".$moneypaid." </td>
			<td> Bank Charges:  ".$nExtraCharges." </td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		</table>
		";
		
		}else{
		
		
					$subject = "Status Updated";
		
					$custContents = selectFrom("select Title,firstName,middleName,lastName,Country,City,Zip,email,Zip,Phone,customerStatus from " . TBL_CUSTOMER . " where customerID = '". $custID ."'");
					$custTitle = $custContents["Title"];
					$custFirstName = $custContents["firstName"];
					$custMiddleName = $custContents["middleName"];
					$custLastName = $custContents["lastName"];
					$custCountry = $custContents["Country"];
					$custCity = $custContents["City"];
					$custZip = $custContents["Zip"];
					$custEmail = $custContents["email"];
					$custPhone = $custContents["Phone"];
					$custStatus = $custContents["customerStatus"];
		
		
					$benContents = selectFrom("select Title,firstName,middleName,lastName,Address,Country,City,Zip,email,Phone from ".TBL_BENEFICIARY." where benID= '". $benID ."'");
					$benTitle = $benContents["Title"];
					$benFirstName = $benContents["firstName"];
					$benMiddleName = $benContents["middleName"];
					$benLastName = $benContents["lastName"];
					$benAddress  = $benContents["Address"];
					$benCountry = $benContents["Country"];
					$benCity = $benContents["City"];
					$benZip = $benContents["Zip"];
					$benEmail = $benContents["email"];
					$benPhone = $benContents["Phone"];
		
		
		/*			$AdmindContents = selectFrom("select username,name,email from admin where userID = '". $benAgentID ."'");
					$AdmindLoginName = $AdmindContents["username"];
					$AdmindName = $AdmindContents["name"];
					$AdmindEmail = $AdmindContents["email"];*/
		
		/***********************************************************/
		$message = "<table width='500' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
			<td colspan='2'><p><strong>Subject</strong>: Transaction update </p></td>
		  </tr>";
		 $messageCust =" 
		  <tr>
			<td colspan='2'><p><strong>Dear</strong>  ".$custTitle."&nbsp;".$custFirstName."&nbsp;".$custLastName ." </p></td>
		  </tr>
		  <tr>
			<td width='205'>&nbsp;</td>
			<td width='295'>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of your transaction via emails. Please see the attached transaction details for your reference. </p></td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Transaction Detail: </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		
		  <tr>
			<td> Transaction Type:  ".$type." </td>
			<td> Status: ".$newStatus." </td>
		  </tr>
		  <tr>
			<td> Transaction No:   .".$imReferenceNumber." </td>
			<td> Transaction Date:  ".$transDate."  </td>
		  </tr>
			<tr>
			<td> Manual Reference No:   ".$strManualReferenceNumber ."</td> 
			<td> &nbsp; </td>
		  </tr>
		  <tr>
			<td><p>Dated: ".date("F j, Y")."</p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		
		
		  <tr>
			<td><p><strong>Beneficiary Detail: </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td><p>Beneficiary Name:  ".$benTitle." ".$benFirstName." ".$benLastName."</p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Address:  ".$benAddress." </td>
			<td> Postal / Zip Code:  ".$benZip." </td>
		  </tr>
		  <tr>
			<td> Country:   ".$benCountry."   </td>
			<td> Phone:   ".$benPhone."   </td>
		  </tr>
		  <tr>
			<td><p>Email:  ".$benEmail."   </p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>";

	$messageBen ="				
			  <tr>
				<td colspan='2'><p><strong>Dear</strong>  ".$benTitle." ".$benFirstName." ".$benLastName."  </p></td>
			  </tr>				
	
						  <tr>
				<td width='205'>&nbsp;</td>
				<td width='295'>&nbsp;</td>
			  </tr>
					  <tr>
				<td colspan='2'><p>Thank you for choosing ".$clientCompany." as your money transfer company. We will keep you informed about the status of transaction via emails. Please see the attached transaction details for this transaction reference. </p></td>
			  </tr>			
	
	
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><p><strong>Transaction Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
				
	
			  <tr>
				<td> Transaction Type:  ".$type." </td>
				<td> Status: ".$newStatus." </td>
			  </tr>
			  <tr>
				<td> Transaction No:   ".$imReferenceNumber." </td>
				<td> Transaction Date:  ".$transDate."  </td>
			  </tr>
			  <tr>
				<td> Manual Refrence Number:   ".$strManualReferenceNumber." </td>
				<td> &nbsp;</td>
			  </tr>
			  <tr>
				<td><p>Dated: ".date("F j, Y")."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			
			
			  <tr>
				<td><p><strong>Sender Detail: </strong></p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td colspan='2'>-----------------------------------------------------------------------------------</td>
			  </tr>
			  <tr>
				<td><p>Sender Name:  ".$custTitle." ".$custFirstName." ".$custLastName."</p></td>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td> Country:   ".$custCountry."   </td>
				<td> Phone:   ".$custPhone."   </td>
			  </tr>
			  <tr>
				<td><p>Email:  ".$custEmail."   </p></td>
				<td> Postal / Zip Code:  ".$custZip." </td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>";	
		if(trim($type) == 'Bank Transfer')
		{
			$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benCountry."'");
		
			$strQuery = "SELECT * FROM ".TBL_BANK_DETAILS." where benID= '". $benID ."'";
			$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
			$rstRow = mysql_fetch_array($nResult);
		
			if(CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
			{
				$messageRemain .= "	<tr>
									<td>IBAN</td>
									<td>" . $rstRow["IBAN"] . "</td>
								</tr>";
			}
			else
			{
				$bankNameV = $rstRow["bankName"];
				$branchCodeV = $rstRow["branchCode"];
				$branchAddressV = $rstRow["branchAddress"];
				$swiftCodeV = $rstRow["swiftCode"];
				$accNoV = $rstRow["accNo"];
		
				$messageRemain="  <tr>
							<td><p><strong>Beneficiary Bank Details </strong></p></td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td> Bank Name:  ".$bankNameV."  </td>
							<td> Acc Number:  ".$branchCodeV."  </td>
						  </tr>
						  <tr>
							<td> Branch Code:  ".$branchAddressV."  </td>
							<td> Branch Address:  ".$swiftCodeV."  </td>
						  </tr>
						  <tr>
							<td><p>Swift Code:  ".$accNoV."  </p></td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						";
			}
		}
		elseif(trim($type) == "Pick up")
		{
		
		$cContents = selectFrom("select cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_branch_name,cp_branch_address,cp_country,cp_city,cp_phone from cm_collection_point where cp_id  = '". $collectionPointID ."'");
		$agentname = $cContents["cp_corresspondent_name"];
		$contactperson = $cContents["cp_corresspondent_name"];
		$company = $cContents["cp_branch_name"];
		$address = $cContents["cp_branch_address"];
		$country = $cContents["cp_country"];
		$city = $cContents["cp_city"];
		$phone = $cContents["cp_phone"];
		$tran_date = date("Y-m-d");
		$tran_date = date("F j, Y", strtotime("$tran_date"));
		
		$messageRemain = "
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Collection Point Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Agent Name : ".$agentname." </td>
			<td> Contact Person:  ".$contactperson." </td>
		  </tr>
		  <tr>
			<td> Company:  ".$company." </td>
			<td> Address:  ".$address." </td>
		  </tr>
		  <tr>
			<td>Country:   ".$country."</td>
			<td>City:  ".$city."</td>
		  </tr>
		  <tr>
			<td>Phone:  ".$phone."</td>
			<td>&nbsp;</td>
		  </tr> ";
		
		}
		/*if($amount > 500)
		{
			$tempAmount = ($amount - 500);
			$nExtraCharges =  (($tempAmount * 0.50)/100);
			$charges = ($fee + $amount + $nExtraCharges);
		}*/
		
		
		//Distributor Commition logic
		
		$benAgentID = $amount["benAgentID"];
		
		$packageQuery="select agentCommission,commPackage from ". TBL_ADMIN_USERS." where userID = '".$benAgentID."'";
					$agentPackage = selectFrom($packageQuery);
		
					$package = $agentPackage["commPackage"];
					
					//
					switch ($package)
						  {
							case "001": // Fixed amount per transaction
							{
								$agentCommi = $agentPackage["agentCommission"];
								$commType = "Fixed Amount";
								break;
							}
							case "002": // percentage of total transaction amount
							{
								$agentCommi = ( $transAmount* $agentPackage["agentCommission"]) / 100;
								$commType = "Percentage of total Amount";
								break;
							}
							case "003": // percentage of IMFEE
							{
								$agentCommi = ( $fee * $agentPackage["agentCommission"]) / 100;
								$commType = "Percentage of Fee";
								break;
							}
							case "004":
							{
								$agentCommi = 0;
								$commType = "None";
								break;
							}
						}				  
						   
					//
				 $agentCommi;
		
		
		// end
		
		
		
		
		/*if($trnsid[$i] != "")
		{
		 update("update transactions set IMFee = '$fee',
														 totalAmount 	 = '$totalamount',
														 localAmount 	 = '$localamount',
														 exchangeRate 	 = '$exRate',
														 distributorComm = '$agentCommi'
															 where
															 transID  = '". $trnsid[$i] ."'");
		}*/
  $messageEnd="
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><p><strong>Amount Details </strong></p></td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan='2'>-----------------------------------------------------------------------------------</td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td> Exchange Rate:  ".$exRate."</td>
			<td> Amount:  ".$transAmount." </td>
		  </tr>
		  <tr>
			<td> ".$systemPre." Fee:  ".$fee." </td>
			<td> Local Amount:  ".$localamount." </td>
		  </tr>  <tr>
			<td> Transaction Purpose:  ".$purpose." </td>
			<td> Total Amount:  ".$totalamount." </td>
		  </tr>  <tr>
			<td> Money Paid:  ".$moneypaid." </td>
			<td> Bank Charges:  ".$nExtraCharges." </td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		<tr>
			<td colspan='2'><p>For further details please contact ".CONFIG_INVOICE_FOOTER.". </p></td>
			
		  </tr>
		</table>
		";
		}
		if(CONFIG_SENDER_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			sendMail($custEmail, $subject, $message.$messageCust.$messageRemain.$messageEnd, $fromName, $fromEmail);
		}
		if(CONFIG_BEN_MAIL_ON_STATUS_CHANGE_LEDGER=="1"){
			sendMail($benEmail, $subject, $message.$messageBen.$messageRemain.$messageEnd, $fromName, $fromEmail);
		}
	}
}
?>