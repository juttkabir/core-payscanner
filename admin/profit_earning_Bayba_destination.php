<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

$totalAmount="";



////////////////////////////////Making Condition for Report Visibility/////////////
$condition = False;
	
	if(CONFIG_REPORT_ALL == '1')
	{
		if($agentType != "SUPA")
		{
			$condition = True;	
		}
	}else{
			if($agentType == "admin" || $agentType == "Call"||$agentType == "Admin Manager" || $agentType == "Branch Manager")
			{
				$condition = True;
			}
		
		}
		
//////////////////////////Making of Condition///////////////

if($_GET["profitID"] != "" || $_POST["save"] != "")
{
	
	
	
	if($_POST["save"] != "")
	{
		$date_time = getCountryTime(CONFIG_COUNTRY_CODE);
		
		
		$updationQuery = "update ".TBL_PROFIT_EARNING." set 
		reciev_rate_inter_to_dest = '".$destRate."',
		update_time = '".$date_time."'
		where profit_id = '".$_GET["profitID"]."'
		";
		
		update($updationQuery);
		
		insertError("Profit Report Updated successfully");
		$msg = True;
		////////For History
			$descript="Profit Earning ".$_GET["profitID"]." is updated ";
			activities($_SESSION["loginHistoryID"],"INSERTION",$profitID,TBL_PROFIT_EARNING,$descript);
		/////////////////////////
	}
	if($_GET["profitID"] != "")
	{
		$selectProfitQuery = "select * From ".TBL_PROFIT_EARNING." where profit_id = '".$_GET["profitID"]."'";
		$profitData = selectFrom($selectProfitQuery);
		
	}	
	
	
}

?>
<html>
<head>
	<title>Calculate Profit</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="83%" border="0" cellspacing="1" cellpadding="5" align="center">
  <form action="profit_earning_Bayba_destination.php?profitID=<?=$_GET["profitID"]?>" method="post" name="calculator">
  
  <tr>
  	<td>
  		<table width="100%" border="0" align="center">	
  			<tr>
  				<td>
  					Destination Rate <input type="text" name="destRate" value="<?=$destRate?>">   					
  				</td>
  			<tr>
  			<tr>
  				<td>
  					<input type="submit" name="save" value="Save Calculation">
  				</td>
  			</tr>
		</table>	     
         <table>
         	<? 
         	if ($msg){ ?>
		  	<tr bgcolor="EEEEEE"> 
              <td colspan="2"> 
              	<table align="center">
              	  <tr>
              		<td width="40" align="center"><font size="5" color="#990000"><b><i><? echo ($msg ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td width="635"><? echo "<font color='#990000'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
              		</td>
              	  </tr>
                </table>
              </td>
            </tr>
     		<? } ?>	
         	<tr bgcolor="#DFE6EA"> 
         		<td width="146"><font color="#005b90"><strong>Date</strong></font></td>
         		<td width="146"><font color="#005b90"><strong>Countries</strong></font></td>
	            <td width="146"><font color="#005b90"><strong>Net Amount</strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Bank Charges</strong></font></td>	
	            <td width="159"><font color="#005b90"><strong>Conversion Rate</strong></font></td>	
	            <td width="146"><font color="#005b90"><strong>Destination Rate Originating</strong></font></td>
	            <td width="146"><font color="#005b90"><strong>Destination Rate Destination</strong></font></td>
	            <td width="159"><font color="#005b90"><strong>Margin Profit </strong></font></td>	
	            <td width="159"><font color="#005b90"><strong>Total Profit </strong></font></td>	            
         	</tr>
         	<?
         	
		        ///Destination Rate with Intermediate Currency at Sending End.  e.g. USD --> GMD at UK
				if($profitData["reciev_rate_inter_to_dest"] != "")
				{	
					$dest_Rate = $profitData["send_rate_inter_to_dest"]/$profitData["intermediate_rate"];
					
					$marginProfit = $profitData["reciev_rate_inter_to_dest"] - $dest_Rate;
					$NetAmount = ($profitData["origin_amount"] * $profitData["intermediate_rate"])-$profitData["bank_charges"]; 
					$totalProfit = $NetAmount * $marginProfit;
				}
		         ?>
		         	<tr bgcolor="#DFE6EA"> 
			            <td width="146"><? echo($profitData["entry_time"]); ?></td>
			            <td width="159"><? echo("From ".$profitData["origin_country"]." To ".$profitData["dest_country"]); ?></td>	
			            <td width="146"><? echo($NetAmount." ".$profitData["intermediate_curr"]); ?></td>
			            <td width="159"><? echo($profitData["bank_charges"]); ?></td>	            
			            <td width="159"><? echo($profitData["intermediate_rate"]." ".$profitData["intermediate_curr"]); ?></td>	
			            <td width="159"><? echo($dest_Rate." At ".$profitData["origin_country"]); ?></td>	            
			            <td width="159"><? echo($profitData["reciev_rate_inter_to_dest"]." At ".$profitData["dest_country"]); ?></td>	            
			            <td width="159"><? echo($marginProfit); ?></td>	            
			            <td width="159"><? echo($totalProfit ." ".$profitData["dest_currency"]); ?></td>	            
		         	</tr>
        </table>      
			
	 
	 </td>
	</tr>
	</form>
</table>
</body>
</html>