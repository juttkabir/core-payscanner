<?php
session_start();

include ("../include/config.php");
include ("security.php");
$userID = $_SESSION["loggedUserData"]["userID"];
$partnerID=$_GET["partnerID"];
$clientStatus=$_GET["status"];

if (isset($_GET['submit'])) {
    $connectionType = $_POST['connectionType'];
    $host = $_POST['host'];
    $wsdlURL = $_POST['wsdlURL'];
    $userName = $_POST['userName'];
    $password = $_POST['password'];
    $pin = $_POST['pin'];
    $accessCode = $_POST['accessCode'];
    $locationCode = $_POST['locationCode'];
    $services = $_POST['services'];
    $uploadCertificate = $_POST['uploadCertificate'];
    $version = $_POST['version'];
    $APISpecs = $_POST['APISpecs'];

//echo "INSERT INTO api_configuration(userID,partnerID,connectionType, host, wsdlURL, userName, password, pin, accessCode, locationCode, services, certificatePath, version, APISpecs) VALUES ($userID,$partnerID,'$connectionType','$host','$wsdlURL','$userName','$password',$pin,'$accessCode',$locationCode,'$services','$uploadCertificate',$version,'$APISpecs')";
//die();
    insertInto("INSERT INTO api_configuration VALUES (0,$userID,'$userName',$partnerID,$clientStatus,'$connectionType','$wsdlURL','$host','$password',$pin,'$accessCode','$uploadCertificate',$version,'$APISpecs','$services',$locationCode)");
    $apiConf = mysql_insert_id();

//$cronURL=$_POST['cronURL'];
//$cronTime=$_POST['cronTime'];
//$status=$_POST['status'];


    for ($i = 0; $i < sizeof($_POST['cronURL']); $i++) {
        $cronURL = $_POST['cronURL'][$i];
        $cronTime = $_POST['cronTime'][$i];
        $cronStatus = $_POST['cronStatus'][$i];
        insertInto("INSERT INTO crons( apiID, userID, partnerID, cronURL, cronTime, status) VALUES($apiConf,$userID,$partnerID,'$cronURL','$cronTime','$cronStatus')");
    }
}
?>
<html>
<head>
    <title>configur-partner.php</title>
    <script language="javascript" src="./javascript/functions.js"></script>
    <META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Include the above in your HEAD tag -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="GBP, EUR, USD, GBPEUR, GBPUSD,EURUSD foreign exchange specialists, foreign currency, foreign exchange brokers, foreign currencies, money transfer, forex, fx, transfer money abroad, dollars, euros, sterling, expats, online foreign exchange, currency brokers, overseas property, Corporate, business, Exchange rate, best rates of exchange, Currency converter, emigrating, currency market, money market, trading, import, export, margin, one off transfer, save money, forward contracts, spot, market order, rate watch" />
    <link rel="alternate" type="application/rss+xml" title="Blog" href="/rss" />
    <link href="content/stylesheets/application.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="content/stylesheets/jquery.fancybox-1.2.6.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/new-agent.css" />
    <link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
    <link rel="stylesheet" href="./assets_spi/css/styles00.min.css">
    <!-- Favicon and Apple Icons -->
    <link rel="icon" type="image/png" href="./assets_spi/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="./assets_spi/images/icons/faviconx57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./assets_spi/images/icons/faviconx72.png">
    <link rel="canonical" href="https://css-tricks.com/examples/DragAndDropFileUploading/">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,400" />
    <link rel="stylesheet" href="./assets_spi/css/bootstrap-datetimepicker.min.css">
    <script src="./assets_spi/js/scriptjs.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<form action="configure-partner.php?partnerID=<?php echo $partnerID; ?>&status=<?php echo $clientStatus; ?>&submit=" method="post" name="configurePartnerForm" id="configurePartnerForm" onsubmit="return checkForm()" style="background-color: #fff; border-color: #fff">
<!--    <script>-->
<!--        function checkForm() {-->
<!--            window.close();-->
<!--            return true;-->
<!--        }-->
<!--    </script>-->
    <fieldset class="individual group " style="width:900px;background-color:#d4edda; margin-right: -200px; padding: 30px">
        <div class="row">
            <div class="col col-3">
                <p><b>Connection Type</b></p>
                <div class="">
                    <input class="required form-control" id="source" name="connectionType" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>host</b></p>
                <div class="">
                    <input class="required form-control" id="source" name="host" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>wsdlURL</b></p>
                <div class="">
                    <input class="required form-control" id="source" name="wsdlURL" type="text" value="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col col-3">
                <p><b>username</b></p>
                <div>
                    <input class="required form-control" id="source" name="userName" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>password</b></p>
                <div>
                    <input class="required form-control" id="source" name="password" type="password" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>pin</b></p>
                <div>
                    <input class="required form-control" id="source" name="pin" type="password" value="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col col-3">
                <p><b>accessCode</b></p>
                <div>
                    <input class="required form-control" id="source" name="accessCode" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>location code</b></p>
                <div>
                    <input class="required form-control" id="source" name="locationCode" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>services</b></p>
                <div>
                    <input class="required form-control" id="source" name="services" type="text" value="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col col-3">
                <p><b>upload certificate</b></p>
                <div>
                    <input class="required form-control" id="source" name="uploadCertificate" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>Version</b></p>
                <div>
                    <input class="required form-control" id="source" name="version" type="text" value="">
                </div>
            </div>
            <div class="col col-3">
                <p><b>API Specs</b></p>
                <div>
                    <input class="required form-control" id="source" name="APISpecs" type="text" value="">
                </div>
            </div>
        </div>
        <br>

        <div>
            <p><b>Cron URLs</b></p>
            <div class="row">
                <div class="col col-3">
                    <div class="form-group mb10">
                        <input class="form-control" id="cronURL" name="cronURL[0]" type="text" value="" placeholder="URL" group="SD001">
                    </div>
                </div>
                <div class="col col-3">
                    <div class="form-group mb10">
                        <input class="form-control" id="cronTime" name="cronTime[0]" type="text" value="" placeholder="time" group="SD001">
                    </div>
                </div>
                <div class="col col-3">
                    <div class="form-group mb10">
                        <input class="form-control" id="cronStatus" name="cronStatus[0]" type="text" value="" placeholder="status" group="SD001">
                    </div>
                </div>
                <div class="col col-1">
                    <div class="form-group">
                        <input type="button" onClick="copy_cronURLS();" id="add-cronURL-row" value="+" class="fa fa-plus btn btn-success" aria-hidden="true" style="width: 45px" />
                    </div>
                </div>
                <div class="col col-1">
                    <div class="form-group">
                        <input type="button" onClick="remove_cronURLS();" id="add-Director-row" value="-" class="fa fa-plus btn btn-success" aria-hidden="true"  style="width: 45px; margin-left: -13px"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_Next_Crons">
        </div>
        <br>
        <div class="row">
            <div class="col col-4">
                <input class="btn btn-primary btn-default" type="submit" value="SAVE" style="margin-top: 25px;margin-bottom: -30px">
            </div>
            <div class="col col-4">
                <input class="btn btn-primary btn-default" type="submit" value="CANCEL" style="margin-top: 25px;margin-bottom: -30px">
            </div>
        </div>
    </fieldset>
</form>
<script>
    var number;
    function copy_cronURLS()
    {
        var nbSD = ($("#configurePartnerForm").find("[group^='SD001']").length / 3);
        number=nbSD+1;
        var html_clone='   <div id="auto_copy'+number+'" class="row auto_copy_class">  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"> <div class="form-group mb10"><input type="text" class="form-control" id="cronURL" name="cronURL['+nbSD+']" placeholder="URL '+number+' " group="SD001"></div></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><div class="form-group mb10"><input type="text" class="form-control" id="cronTime" name="cronTime['+nbSD+']" placeholder="Time" group="SD001"></div></div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><div class="form-group mb10"><input type="text" class="form-control" id="cronStatus" name="cronStatus['+nbSD+']" placeholder="Status" group="SD001"></div></div>  <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"><div class="form-group mb10">'+''+' </div></div>   <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1"><div class="form-group mb10">'+''+'</div></div></div> ';
        var ccn_div = $('.row_Next_Crons').last();
        console.log(ccn_div);
        // alert(ccn_div);
        //  e.preventDefault();

        if (nbSD < 10) {
            // $("#cp_row").clone().insertBefore(".row_Next_Directors");
            $(html_clone).insertBefore(".row_Next_Crons");
        }else{
            alert("Cron URLS more than 10 not allowed");
        }

    }

         function remove_cronURLS()
         {

         /* remove-Director-row */

//           e.preventDefault();
//         $(this).closest('.auto_copy_class').fadeOut(300, function(){$(this).closest('.auto_copy_class').remove();});
//         return false;
            var abc = '#auto_copy'+number;
          $(abc).css('display', 'none');
             number--;

         }

</script>
</body>
</html>