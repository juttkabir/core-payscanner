<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
//session_register["agentID2"];

if($_POST["Submit"]=="Search")
{
	
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";


	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
}

if($_POST["agentID"]!="") {
	$_SESSION["agentID2"] =$_POST["agentID"];
}elseif($_GET["agentID"]!="") {
	$_SESSION["agentID2"]=$_GET["agentID"];
}else{ 
	$_SESSION["agentID2"] = "";
}

if($_POST["currencySending"]!="")
	$_SESSION["currencySending"] =$_POST["currencySending"];
	elseif($_GET["currencySending"]!="")
		$_SESSION["currencySending"]=$_GET["currencySending"];
	else 
		$_SESSION["currencySending"] = "";
				
	
if ($offset == "") {
	$offset = 0;
}
$limit=20;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

?>
<html>
<head>
	<title>Sub Agent Account</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	  <tr>
    <td class="topbar">Sub A&D Account Statement</td>
  </tr>
	<tr>
      <td width="563">
	<form action="sub_Account.php" method="post" name="Search">
  		<div align="center" id="searchTable">From 
        <? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
                <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
                To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  <?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
				$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
		  ?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
		 <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
                <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                  <?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
		  ?>
                </SELECT>
                <script language="JavaScript">
         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
                <br>
                &nbsp;&nbsp; 
                <select name="agentID" style="font-family:verdana; font-size: 11px">
                  <option value="">- Select Sub A&D-</option>
                  <?
                  $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and agentType='sub' and isCorrespondent = 'Y' ";
								if ($agentType == 'SUPAI') {
									$agentQuery .= " AND parentID = '".$changedBy."' ";
								} else if ($agentType == 'SUBAI') {
									$agentQuery .= " AND userID = '".$changedBy."' ";
								}
								$agentQuery .= " order by agentCompany";
								$agents = selectMultiRecords($agentQuery);
					for ($i=0; $i < count($agents); $i++){
				?>
                  <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                  <?
					}
				?>
                </select>
                <script language="JavaScript">
         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                                </script>
                 
                <? if(CONFIG_ADD_CURRENCY_DROPDOWN == "1"){?>       
          <br><br>
					Sending Currency &nbsp;
          	<select name="currencySending" style="font-family:verdana; font-size: 11px; width:130"> 
          	<?
          	  $currQuery = "select distinct(currencyOrigin) from exchangerate where currencyOrigin !=''";
          	  $sendingCurr = selectMultiRecords($currQuery);
          	  
          	   for($k = 0; $k < count($sendingCurr); $k++){
						 ?>
							 <option value="<?=trim($sendingCurr[$k]["currencyOrigin"])?>"><?=trim($sendingCurr[$k]["currencyOrigin"])?></option>	
							
          
           <? } ?>
           
           
             <script language="JavaScript">
         	    SelectOption(document.forms[0].currencySending, "<?=$_SESSION["currencySending"]; ?>");
             </script>
             <? } ?>                 
                <input type="submit" name="Submit" value="Search">
              </div>
        
		<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
	 	$Balance="";
		if($_GET["search"]=="search") {
			$slctID=$_GET["agentID"];
		} else {
			$slctID=$_POST["agentID"];	
		}
			
			$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
			$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
			$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			 
		if ($slctID !=""){
			$accountQuery = "Select * from sub_agent_Dist_account where agentID = $slctID";
			  $accountQuery .= " and $queryDate";				
				$accountQuery .= " and status != 'Provisional' ";
				
			$accountQueryCnt = "Select count(*) from sub_agent_Dist_account where agentID = $slctID";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
				if($_SESSION["currencySending"]!="" && CONFIG_ADD_CURRENCY_DROPDOWN == "1"){
			 		
			 		$accountQuery .= " and currency = '".$_SESSION["currencySending"]."' ";
			 		$accountQueryCnt .= " and currency = '".$_SESSION["currencySending"]."' ";		
			  }
			
			
			$allCount = countRecords($accountQueryCnt);
			 $accountQuery .= "order by dated";
			$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
		}/*else{
			$slctID = $changedBy;
			$accountQuery="Select * from sub_agent_Dist_account where agentID = $changedBy";
				 $accountQuery .= " and $queryDate";				
				 $accountQuery .= " and status != 'Provisional' ";
				
				
				
			//$contentsAcc = selectMultiRecords($accountQuery);
		
			$accountQueryCnt = "Select count(*) from sub_agent_Dist_account where agentID = $changedBy";
			$accountQueryCnt .= " and $queryDate";				
			$accountQueryCnt .= " and status != 'Provisional' ";
			
			
			$allCount = countRecords($accountQueryCnt);
			//echo $queryBen;
			$accountQuery .= "order by dated";
			$accountQuery .= " LIMIT $offset , $limit"; 
			
			$contentsAcc = selectMultiRecords($accountQuery);
			}*/
		?>	
			
		  <table width="700" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
			  <td height="25" nowrap bgcolor="#6699CC">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF" id="pagination">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			<td>&nbsp;
			<?		
			if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
			{
			?>
				<table>
					<tr>
						<td width="300">
							Cumulative Total <? //$allAmount=agentBalanceDate($slctID, $fromDate, $toDate);  
							$contantagentID = selectFrom("select balance from " . TBL_ADMIN_USERS . " where userID ='".$slctID."'");
							$allAmount = $contantagentID["balance"];
							echo number_format($allAmount,2,'.',',');
							?>
			</td>
			<td>&nbsp;
			</td>
			</tr>
			</table>
			<?
		}
		?>
			</td>
		</tr>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
			  <table width="781" border="0" bordercolor="#EFEFEF">
				<tr bgcolor="#FFFFFF">
					  <td align="left" ><span class="style1">Date</span></td>
					  <td align="left" ><span class="style1">Modified By</span></td>
					  <td align="left" ><span class="style1">Money In</span></td>
					  <td align="left" ><span class="style1">Money Out</span></td>
						<td align="left" ><span class="style1"><? echo $systemCode; ?></span></td>
						<td align="left" ><span class="style1">Description</span></td>
				</tr>
				<? 
				
				$preDate = "";
				if($_SESSION["currDate"] != "")
					$preDate = $_SESSION["currDate"];
				if($_SESSION["closingBalance"] != "")	
					$closingBalance = $_SESSION["closingBalance"];
				if($_SESSION["openingBalance"] != "")	
					$openingBalance = $_SESSION["openingBalance"];
				for($i=0;$i < count($contentsAcc);$i++)
				{
					$currSending = $contentsAcc[$i]["currency"];
					///////////////////Opening and Closing////////////////
					
						if(CONFIG_AGENT_STATEMENT_BALANCE == '1')
						{
							$currDate = $contentsAcc[$i]["dated"];
							if($currDate != $preDate)
							{
								
								$datesFrom = selectFrom("select  Max(dated) as datedFrom from " . TBL_SUB_ACCOUNT_SUMMARY . " where user_id = '$slctID' and dated <= '$currDate'");
								$FirstDated = $datesFrom["datedFrom"];
								 
								 $openingBalance = 0;
								 
								if($FirstDated != "")
								{
									$account1 = "Select opening_balance, closing_balance from ".TBL_SUB_ACCOUNT_SUMMARY." where 1";
									$account1 .= " and dated = '$FirstDated'";				
									$account1 .= " and  user_id = '".$slctID."' ";								
									$contents1 = selectFrom($account1);
									if($currDate == $FirstDated)
										$openingBalance =  $contents1["opening_balance"];
									else
										$openingBalance =  $contents1["closing_balance"];
								}
								
								 $closingBalance = $openingBalance;
								 $preDate = $currDate;
								 
							$_SESSION["openingBalance"] = $openingBalance;

							}
								
						}
						$_SESSION["currDate"] = $currDate;
					/////////////////////////Opening and Closing///////////
				//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
				<tr bgcolor="#FFFFFF">
					<td align="left"><strong><font color="#006699"><? 	
					$authoriseDate = $contentsAcc[$i]["dated"];
					echo date("F j, Y", strtotime("$authoriseDate"));
					?></font></strong></td>
					<td align="left">
					<? 
					if(strstr($contentsAcc[$i]["description"], "by Teller"))
					{
						$q=selectFrom("SELECT * FROM ".TBL_TELLER." WHERE tellerID ='".$contentsAcc[$i]["modified_by"]."'"); 
						
					}
					else
						$q=selectFrom("SELECT * FROM ".TBL_ADMIN_USERS." WHERE userID ='".$contentsAcc[$i]["modified_by"]."'"); 
					echo $q["name"]; ?></td>
					<td align="left"><?  
					if($contentsAcc[$i]["type"] == "DEPOSIT")
					{
						echo $contentsAcc[$i]["amount"];
						if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$contentsAcc[$i]["currency"];}
						$Balance = $Balance+$contentsAcc[$i]["amount"];
					}
					?></td>
					<td align="left"><? 
					if($contentsAcc[$i]["type"] == "WITHDRAW")
					{
						echo $contentsAcc[$i]["amount"];
						if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$contentsAcc[$i]["currency"];}
						$Balance = $Balance-$contentsAcc[$i]["amount"];
					}
					?></td>
					<? if($contentsAcc[$i]["TransID"] > 0){
							$trans = selectFrom("SELECT refNumberIM, refNumber FROM ".TBL_TRANSACTIONS." WHERE transID ='".$contentsAcc[$i]["TransID"]."'"); 
							
							?>
				<td><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsAcc[$i]["TransID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $trans["refNumberIM"]; ?></font></strong></a></td>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
				<td>
					<? echo $contentsAcc[$i]["description"]; ?>
				</td>
					</tr>
				<?
				}
				?>
				<tr bgcolor="#FFFFFF">
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
		<?		
		/*if(CONFIG_AGENT_STATEMENT_CUMULATIVE == '1')		
		{*/
		?>
				<tr bgcolor="#FFFFFF">
				  <td colspan="3" align="left"><b>Page Total Amount</b></td>
	
				  <td align="left" colspan="2"><? echo  number_format($Balance,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></td>
				  	<td>&nbsp;</td>
						<td>&nbsp;</td>
						
				</tr>			
				<tr> 
					  <td class="style3"> Running Total:</td>
					  <td> 
						<?
					if($_GET["total"]=="first")
					{
					$_SESSION["grandTotal"] = $Balance;
					$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="pre"){
						$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
						$_SESSION["CurrentTotal"]=$Balance;
					}elseif($_GET["total"]=="next"){
						$_SESSION["grandTotal"] += $Balance;
						$_SESSION["CurrentTotal"]= $Balance;
					}else
						{
						$_SESSION["grandTotal"] = $Balance;
						$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
						}
					$rgrandTotal = number_format($_SESSION["grandTotal"],2,'.',','); 
					echo $rgrandTotal;if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;}
					?>
					  </td>
				  	<td>&nbsp;</td>
					<td>&nbsp;</td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>
                </tr>
                <?
           //}
            ?>
            <tr bgcolor="#FFFFFF">
              <td colspan="10">
<!--
/************************************************************************/

Here You will write your Comments or any Information

/************************************************************************/
-->			  
			  </td>

            </tr>
            <tr>
			  <td height="25" nowrap bgcolor="#6699CC" colspan="10">
			  <table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				  <tr>
				  <td align="left"><?php if (count($contentsAcc) > 0) {;?>
					Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
					<?=$allCount; ?>
					<?php } ;?>
				  </td>
				  <?php if ($prv >= 0) { ?>
				  <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=first&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">First</font></a> </td>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=pre&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">Previous</font></a> </td>
				  <?php } ?>
				  <?php 
						if ( ($nxt > 0) && ($nxt < $allCount) ) {
							$alloffset = (ceil($allCount / $limit) - 1) * $limit;
					?>
				  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&currDate=$currDate&closingBalance=$closingBalance&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=next&currencySending=$_SESSION[currencySending]";?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				  <td width="50" align="right"><!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$accountQuery."&agentID=".$slctID."&search=search&total=last";?>"><font color="#005b90">Last</font></a-->&nbsp; </td>
				  <?php } ?>
				</tr>
				
			  </table></td>
			</tr>
            
            
            
            
			</table>		
            <?
			} // greater than zero
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Sub A&D </font>
				</td>
			</tr>
<?
}

$cond = false;

if ($slctID != '') {
	if ($agentType == 'admin') {
		$cond = true;	
	} else if ($agentType == 'SUPAI') {
		$qPID = "SELECT `parentID` from " . TBL_ADMIN_USERS . " WHERE `userID` = '".$slctID."' AND `parentID` = '".$changedBy."'";
		if (isExist($qPID)) {
			$cond = true;	
		}
	}
}

	if ($cond) {
?>
			
				
			<tr>
		          <td height="15" bgcolor="#c0c0c0"><span class="child"><a HREF="add_sub_Account.php?agent_Account=<?=$_SESSION["agentID2"];?>&currencySending=<?=$_SESSION["currencySending"]?>" target="mainFrame" class="tblItem style4"><font color="#990000">Deposit/Withdraw 
                    Money</font></a></span> </td>
			 
  	</tr>
<?	}  ?>
       </table>
	   
	<?	
	 }else
			{
				$_SESSION["grandTotal"]="";
				$_SESSION["CurrentTotal"]="";
		 
	
				$_SESSION["fMonth"]="";
				$_SESSION["fDay"]="";
				$_SESSION["fYear"]="";
				
				$_SESSION["tMonth"]="";
				$_SESSION["tDay"]="";
				$_SESSION["tYear"]="";
			}///End of If Search
	 ?>
	 
	 </td>
	</tr>
	<? if ($slctID != '' && count($contentsAcc) > 0){?>
		<tr id="actionBtn">
		  	<td align="center"> 
				<div class="noPrint">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
			</td>
		  </tr>
		  <? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>