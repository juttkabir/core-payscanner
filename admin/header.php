<?
include ("../include/config.php");
include ("security.php");

/**
 * @var $agent \Payex\Model\Agent
 */
$agent = $app->getAgentRepository()->getById($_SESSION['loggedUserData']['userID']);
$agentLogo = $agent->getLogoPath();

$agentType = getAgentType();
if($agentType == "TELLER")
{
	$username = $_SESSION["loggedUserData"]["loginName"];
 $loggedUserID = $_SESSION["loggedUserData"]["tellerID"];
}else{
 $username = $_SESSION["loggedUserData"]["username"];
 $loggedUserID = $_SESSION["loggedUserData"]["userID"];
}
$content=selectFrom("select name from "  . TBL_ADMIN_USERS . " where username='$username'");
if($content["name"]=="")
{

	$content=selectFrom("select name from "  . TBL_TELLER . " where loginName='$username'");

	}
	if($content["name"]=="")
{
	$content=selectFrom("select name from "  . TBL_USER . " where UserName='$username'");

	}


	switch ($agentType)
	{
		case "Call":
		{
			$str = "You are Call Center Staff";
			break;
		}

		case "admin":
		{
			$str = "You are Super Admin";
			break;
		}
		case "SUPAI":
		{
			$str = "You are Super ". __("Agent")." and Distributor";
			break;
		}
		case "SUPA":
		{
			$str = "You are Super ". __("Agent");
			break;
		}
		case "SUPI":
		{
			$str = "You are Super Distributor";
			break;
		}
		case "SUBAI":
		{
			$str = "You are Sub ". __("Agent"). " and Distributor";
			break;
		}
		case "SUBA":
		{
			$str = "You are Sub ". __("Agent");
			break;
		}
		case "SUBI":
		{
			$str = "You are Sub Distributor";
			break;
		}
		case "TELLER":
		{
			$str = "You are Teller";
			break;
		}
		case "COLLECTOR":
		{
			$str = "You are Collector";
			break;
		}
		case "Branch Manager":
		{
			if(CONFIG_CUSTOMER_CATEGORY == '1')
				$str = CONFIG_ADMIN_STAFF_LABEL;
			else
				$str = "You are Branch Manager";
			break;
		}
		case "Admin Manager":
		{
			$str = "You are Admin Manager";
			break;
		}
		case "Support":
		{
			$str = "Payex Support Person";
			break;
		}
		case "MLRO":
		{
			$str = "You are MLRO user";
			break;
		}
			case "PAYING BOOK CUSTOMER":
		{
			$str = "You are a PAYING BOOK CUSTOMER";
			break;
		}

			case "SUPI Manager":
		{
				$str = "You are Distributor Manager";
			break;
		}

		default:
		{
			$str = "You are Admin Staff";
			break;
		}
	}

//Added by Niaz Ahmad against ticket #2202 at 19-10-2007

if(CONFIG_GLOBAL_BALANCE == "1")
 {
	  // case:1 super admin he can view balance of all super agents and all sub agents
	  if ($agentType == "admin"){
	  	 // this query select super agents with latest date closing balance
	  	  $query2= selectMultiRecords("select distinct(a.userID) from ".TBL_ADMIN_USERS." as a ,".TBL_ACCOUNT_SUMMARY." as s where  a.adminType='Agent'and a.agentType='Supper' and a.isCorrespondent!='ONLY' and a.parentID >0 and a.userID=s.user_id");
	  	   //echo "select distinct(a.userID) from ".TBL_ADMIN_USERS." as a ,".TBL_ACCOUNT_SUMMARY." as s where  a.adminType='Agent'and a.agentType='Supper' and a.isCorrespondent!='ONLY' and a.parentID >0 and a.userID=s.user_id";
	  	    $q2 = array();

	  	    for ($i=0 ; $i< count($query2); $i++){
	  	  	    $agentID2=selectFrom("select id from ".TBL_ACCOUNT_SUMMARY." where user_id='".$query2[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id='".$query2[$i]["userID"]."' Group by user_id)");
	  	         $q2[$i]=$agentID2["id"];
	  	    }
	  	     $superAgentID=implode(",",$q2);
	  	    $superAgentquery=selectMultiRecords("select * from ".TBL_ACCOUNT_SUMMARY." as ac,".TBL_ADMIN_USERS." as u where u.userID=ac.user_id and ac.id IN (".$superAgentID.") ");

	  	      	//echo "count=".count($superAgentquery);
	  	  	for($k=0 ; $k< count($superAgentquery); $k++){
	  	   $superAgentBalance+=$superAgentquery[$k]["closing_balance"];
	  	   $user_id[$k]=$superAgentquery[$k]["user_id"];
	  	   $dated=$superAgentquery[$k]["dated"];
	  	   $b=$superAgentquery[$k]["closing_balance"];
	  	   $name=$superAgentquery[$k]["name"];
	  	   //$user_name=$superAgentquery[$k]["username"];
	  	  // echo"<br>";
	  	  // echo"  ".$userid;
	  	  // echo"          ".$name;
	  	   //echo"         ".$username;
	  	   //echo"           ".$b;
	  	   //echo"    ".$dated;
	  	     }
	  	     $userid=$superAgentID;

	  	     $queryBalance=$superAgentBalance;
	  	  	   //echo "<br>Super Agent balance=".$superAgentBalance;

	  	   // this query select sub agents with latest date closing balance
	  	    $query1= selectMultiRecords("select distinct(a.userID) from ".TBL_ADMIN_USERS." as a ,".TBL_SUB_ACCOUNT_SUMMARY." as s where  a.adminType='Agent' and a.agentType='Sub' and a.isCorrespondent!='ONLY' and a.parentID >0 and a.userID=s.user_id");
	  	   // echo "<br> All sub Agents";
	  	    //echo "select distinct(a.userID) from ".TBL_ADMIN_USERS." as a ,".TBL_SUB_ACCOUNT_SUMMARY." as s where  a.adminType='Agent' and a.agentType='Sub' and a.isCorrespondent!='ONLY' and a.parentID >0 and a.userID=s.user_id";

	  	    $va = array();
	  	    for ($i=0 ; $i< count($query1); $i++){
	  	  	    $agentID=selectFrom("select id from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$query1[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$query1[$i]["userID"]."' Group by user_id)");
	  	         $va[$i]=$agentID["id"];
	  	    }
	  	     $subAgentID=implode(",",$va);
	  	     $subAgentquery=selectMultiRecords("select * from ".TBL_SUB_ACCOUNT_SUMMARY." where id IN (".$subAgentID.") ");
	  	    	for($i=0 ; $i< count($subAgentquery); $i++){
	  	  	  $subAgentBalance+=$subAgentquery[$i]["closing_balance"];
	  	     }
	  	  	 //echo "<br> sub agent balance=".$subAgentBalance;
	  	   $useridSub=$subAgentID;
	  	}


	  // case:2 super agent he can view own balance and associate  sub agents balance
	  if ($agentType == "SUPA"){

	  	 //This query select super agent own balance
	  	 $agent1=selectMultiRecords("select * from ".TBL_ACCOUNT_SUMMARY." where user_id='".$loggedUserID."' and dated= (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id='".$loggedUserID."' Group by user_id)");

	  	for($j=0 ; $j< count($agent1); $j++){
	  	  	  $superAgentBalance+=$agent1[$j]["closing_balance"];
	  	  	    $userid=$agent1[$j]["id"];
	  	     }


	  	     $agent2= selectMultiRecords("select userID from ".TBL_ADMIN_USERS." where parentID='".$loggedUserID."' ");

	  	      $SID = array();
	  	    for ($i=0 ; $i< count($agent2); $i++){
	  	  	    $subagentID=selectFrom("select id from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$agent2[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$agent2[$i]["userID"]."' Group by user_id)");
	  	         $SID[$i]=$subagentID["id"];
	  	    }

	  	   $id=implode(",",$SID);
	  	   $useridSub=$id;
	  	   if($id!= ""){
	  	     $subAgent2=selectMultiRecords("select * from ".TBL_SUB_ACCOUNT_SUMMARY." where id IN (".$id.") ");
	  	    // echo "<br> select associates agents form sub a/c summary";
	  	    // echo "select * from ".TBL_SUB_ACCOUNT_SUMMARY." where id IN (".$id.")";

	  	    	for($p=0 ; $p< count($subAgent2); $p++){
	  	  	  $subAgentBalance+=$subAgent2[$p]["closing_balance"];
	  	     }
	  	  	// echo "<br> Associate sub agent balance=".$balance2;
	  	   }

	  	}


	   // case:3 sub agent he can view own balance
	  if ($agentType == "SUBA"){

	  	  	 $query_subagent=selectMultiRecords("select * from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$loggedUserID."' and dated= (select MAX(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$loggedUserID."' Group by user_id)");

	  	for($n=0 ; $n< count($query_subagent); $n++){
	  	  	  $subAgentBalance+=$query_subagent[$n]["closing_balance"];
	  	  	   $useridSub=$query_subagent[$n]["id"];
	  	     }



	  	}
	   // case:4 Admin staff they can view balance of associate super/sub agents
	  if ($agentType == "COLLECTOR" || $agentType == "Call"  || $agentType == "Branch Manager" || $agentType == "Admin Manager" || $agentType == "Support" || $agentType == "MLRO" || $agentType == "Admin" || $agentType== "SUPI Manager")
	    {

	  	 // This query select balaces of all associates sub agents of  Admin Staff
	  	     $admin_query1= selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$loggedUserID."' ");
	  	     // echo "select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$loggedUserID."'";

	  	     $username=$admin_query1["linked_Agent"];
	  	     $username2=substr_replace($username,"",-1);
	  	     $username3=explode(",",$username2);

	  	     for($u=0; $u< count($username3); $u++){

	  	     	$a= $username3[$u];

	  	     	$c[]="'".$a."'";

	  	     	}

	  	  	 $username4=implode(",",$c);
	  	      if($username4!= ""){

	  	     $adminsupereragent=selectMultiRecords("select userID from ".TBL_ADMIN_USERS." where adminType='Agent' and agentType='Supper' and isCorrespondent!='ONLY' and parentID >0 and username IN(".$username4.")");
	  	  	 //echo "select userID from ".TBL_ADMIN_USERS." where adminType='Agent' and agentType='Supper' and isCorrespondent!='ONLY' and parentID >0 and username IN(".$username4.")";
	  	  	 $adminsuberagent=selectMultiRecords("select userID from ".TBL_ADMIN_USERS."  where adminType='Agent' and agentType='Sub' and isCorrespondent!='ONLY' and parentID >0 and username IN(".$username4.")");
	  	 	  // echo "select userID from ".TBL_ADMIN_USERS."  where adminType='Agent' and agentType='Sub' and isCorrespondent!='ONLY' and parentID >0 and username IN(".$username4.")";
	  	  	}
	  	  	  // associate super agets

	  	  	  for ($i=0 ; $i< count($adminsupereragent); $i++){
	  	  	    $superagentID2=selectFrom("select id from ".TBL_ACCOUNT_SUMMARY." where user_id='".$adminsupereragent[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id='".$adminsupereragent[$i]["userID"]."' Group by user_id)");
	  	      //  echo "select id from ".TBL_ACCOUNT_SUMMARY." where user_id='".$adminsupereragent[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_ACCOUNT_SUMMARY." where user_id='".$adminsupereragent[$i]["userID"]."' Group by user_id)";
	  	        $adminID1[$i]=$superagentID2["id"];
	  	    }
	  	     $superAgentID=implode(",",$adminID1);
	  	     $userid=$superAgentID;
	  	     if($superAgentID!= ""){
	  	    $adminsuperAgentquery=selectMultiRecords("select * from ".TBL_ACCOUNT_SUMMARY." where id IN (".$superAgentID.") ");

	  	    for($n=0 ; $n< count($adminsuperAgentquery); $n++){
	  	  	  $adminsuperAgentBalance+=$adminsuperAgentquery[$n]["closing_balance"];
	  	     }

	  	    }
	  	  // associates sub agents
	  	   for ($i=0 ; $i< count($adminsuberagent); $i++){
	  	  	    $suberagentID=selectFrom("select id from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$adminsuberagent[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$adminsuberagent[$i]["userID"]."' Group by user_id)");
	  	       // echo "select id from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$adminsuberagent[$i]["userID"]."' and dated= (select MAX(dated) from ".TBL_SUB_ACCOUNT_SUMMARY." where user_id='".$adminsuberagent[$i]["userID"]."' Group by user_id)";
	  	        $adminID2[$i]=$suberagentID["id"];
	  	    }
	  	     $adminsubAgentID=implode(",",$adminID2);
	  	     $useridSub=$adminsubAgentID;

	  	     if( $adminsubAgentID!=""){
	  	    $adminsubAgentquery=selectMultiRecords("select * from ".TBL_SUB_ACCOUNT_SUMMARY." where id IN (".$adminsubAgentID.") ");

	  	  for($n=0 ; $n< count($adminsubAgentquery); $n++){
	  	  	  $adminsubAgentBalance+=$adminsubAgentquery[$n]["closing_balance"];
	  	     }

	  	    }

 	}



	} // end if condition


?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="images/interface_admin.css" rel="stylesheet" type="text/css">

<!--<script language="javascript" src="ticker/ticker.js"></script>-->
<!-- =============== AREA FOR DEVELOPER : BEGIN =============== -->
<STYLE TYPE="text/css">
/* Here you can specify the following ticker options:
		- background position
		- background size
		- background color
	NOTE 1: the width of tickpos must be equal w1 (see script)
*/
	.tickbckgr { Z-INDEX: 0; POSITION: ABSOLUTE; RIGHT: 15; TOP: 80; WIDTH: 500; HEIGHT: 20; BACKGROUND-COLOR: RGB(255, 0, 0) }

/* Here you can specify the following news text options:
		- text font family
		- text font size
		- text color
	 NOTE 1: to use different colors / fonts etc use <font> tag
	         when you define the text of the news
*/
	.ticktext { Z-INDEX: 1; POSITION: ABSOLUTE; FONT-FAMILY: verdana; FONT-SIZE: 9px; COLOR: #005b90; VISIBILITY: HIDDEN }
	.ticktext A { FONT-FAMILY: ARIAL; FONT-SIZE: 10pt; COLOR: RGB(255, 255, 0); }

</STYLE>
<!-- ================ AREA FOR DEVELOPER : END ================ -->

<style type="text/css">
<!--
.style1 {color: #CCCCCC}
-->
</style>
</head>

<body topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0" marginheight="0" marginwidth="0">
<!-- =============== AREA FOR DEVELOPER : BEGIN =============== -->
<!-- Please, change width and height here and set to them same walues
     as the ticker background style "tickbckgr" has in stylesheet

     Also, please, change the 1x1 pixel image "newsticker_bckgr.gif".
     Paint its only pixel by the color of ticker background
     (same color with background color of "tickbckgr" is recommended).
-->

<!-- ================ AREA FOR DEVELOPER : END ================ -->

<!-- =============== AREA FOR DEVELOPER : BEGIN =============== -->
<!-- Here you can specify the list of news. Each text
Here you can specify the following ticker options:
		- position
		- size
		- background color
	 NOTE 1: if you change the number of news or their IDs, you must
	         also change the init_news_list() function (see script)
	 NOTE 2: each <div> must have its own id
-->
<?/*
$query = "select * from " . TBL_EXCHANGE_RATES . " order by country ";
$curencies = selectMultiRecords($query);
$query = "select * from " . TBL_EXCHANGE_RATES . " where  country ='United Kingdom'";
$contentRate = selectFrom($query);
$countryRate =  $contentRate["primaryExchange"];

for($i=0;$i<count($curencies);$i++)
{
	$margin = $curencies[$i]["marginPercentage"];

	$curencies[$i]["currency"];
	if(strtolower($curencies[$i]["country"]) != strtolower("United Kingdom"))
	{
		$rates .= "<img src='images/bullet021.gif' align='middle'> " . displayFlag($curencies[$i]["country"], '../') . " " .  round ( ($curencies[$i]["primaryExchange"]) - (($curencies[$i]["primaryExchange"] ) * $margin) / 100   , 4) . " <strong>" . $curencies[$i]["currency"] . "</strong>" . "&nbsp;&nbsp;&nbsp;\n";
	}
}*/
//echo $str;

    if(CONFIG_HEADER_HEIGHT!=""){
        $theight=CONFIG_HEADER_HEIGHT;
    }else{
         $theight=40;
       }

?>
<div id="ticktext1" class="ticktext" onMouseOver="disable_tick()" onMouseOut="enable_tick()" title="All rates are from GBP">
<nobr><? echo $rates;?></nobr>
</div>

<TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 <? if (CONFIG_CUSTOM_COLOR_ENABLE == '1') { ?>bgcolor="<? echo CONFIG_CUSTOM_COLOR1 ?>"<? } ?>>
  <TBODY>
    <TR>
      <TD valign="top">
        <TABLE height="123" cellSpacing=0 cellPadding=0 width=100% background="" border=0>
          <TBODY>

            <TR>
              <!--<TD width=13% height="81" align="center" vAlign=top><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></TD>-->
           <?php
           //$senderAgentContent = selectFrom($queryCust);
           if($agentLogo && CONFIG_ALLOW_SHOW_AGENT_LOGO){ ?>
		        <td width="28%"><?= $app->getUiHelper()->getLogoHtml($agentLogo); ?></td>
		<?php }else{ ?>
            <td align="center" width="15%"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0></td>
        <?php } ?>


			  <TD width=56% height="81" align="center" vAlign=middle background=""><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center"><strong><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#ff0000") ?>" size="+2" face="Times New Roman, Times, serif"><i><? echo (CONFIG_SYSTEM_INFO == '1' ? CONFIG_SYSTEM_INFO_CONTENT : "&nbsp;") ?></i></font></strong></td>
                 </tr>
                 <!-- Added by Niaz Ahmad at 21-10-2007-->
                <? if(CONFIG_GLOBAL_BALANCE == "1"){ ?>
               <? if ($agentType == "admin"){ ?>
                <TR>
                	<TD align="center"><a href="agent_Account_Summary.php?userid=<? echo $userid ?>&queryBalance=<? echo $superAgentBalance ?>&queryFlag=true&Agent=superAgent " target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Super Agents Balance:<?=$superAgentBalance?></font></a></TD>
                	</TR>
                	<TR>
                	<TD align="center"><a href="agent_Account_Summary.php?useridSub=<? echo $useridSub ?>&queryBalance=<? echo $subAgentBalance ?>&queryFlag=true&Agent=subAgent" target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Sub Agents Balance:<?=$subAgentBalance?></font></a></TD>
                	</TR>
                <? } elseif ($agentType == "SUPA"){ ?>
                <TR>
                	<TD align="center"><a href="agent_Account_Summary.php?userid=<? echo $userid ?>&queryFlag=true&Agent=superAgent " target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Super Agents Balance:<?=$superAgentBalance?></font></a></TD>
                	</TR>
                	<TR>
                	<TD align="center"><a href="agent_Account_Summary.php?useridSub=<? echo $useridSub ?>&queryFlag=true&Agent=subAgent" target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Sub Agents Balance:<?=$subAgentBalance?></font></a></TD>
                	</TR>
                <? } elseif ($agentType == "SUBA"){ ?>

                	<TR>
                	<TD align="center"><a href="agent_Account_Summary.php?useridSub=<? echo $useridSub ?>&queryFlag=true&Agent=subAgent" target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Sub Agent Balance:<?=$subAgentBalance?></font></a></TD>
                	</TR>
                <? } elseif ($agentType == "COLLECTOR" || $agentType == "Call"  || $agentType == "Branch Manager" || $agentType == "Admin Manager" || $agentType == "Support" || $agentType == "MLRO" || $agentType == "Admin" || $agentType== "SUPI Manager"){ ?>

                <TR>
                	<TD align="center"><a href="agent_Account_Summary.php?userid=<? echo $userid ?>&queryFlag=true&Agent=superAgent " target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Super Agents Balance:<?=$adminsuperAgentBalance?></font></a></TD>
                	</TR>
                	<TR>
                	<TD align="center"><a href="agent_Account_Summary.php?useridSub=<? echo $useridSub ?>&queryFlag=true&Agent=subAgent" target="mainFrame"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Sub Agents Balance:<?=$adminsubAgentBalance?></font></a></TD>
                	</TR>
                <? } ?>

             <? } ?>

              </table></TD>

              <TD width=31% height="81" align="center" vAlign="middle"><font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>">Your IP address is logged <br>
                <? echo getIP();?></font></TD>

            </TR>

<?
	if (CONFIG_ADD_SHORTCUT == "1" && CONFIG_LEFT_FUNCTION == "1") {
?>
            <TR>
              <TD height="18" align="right">
			  <font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#005b90") ?>"><strong>My Shortcuts: </strong></font>
			  &nbsp;&nbsp;&nbsp;
			  </TD>
			  <TD height="18" colspan="2">
	<?
		$queryShortcut = "SELECT * FROM " . TBL_SHORTCUTS . " WHERE `username` = '$username'";
		$shortcutContents = selectMultiRecords($queryShortcut);
		for ($i = 0; $i < count($shortcutContents); $i++) {
	?>
		<? echo ($i != 0 ? " |" : "") ?>
		<a href="<? echo $shortcutContents[$i]["link"] ?>" target="mainFrame">
			<font color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>"><? echo $shortcutContents[$i]["link_title"] ?></font>
		</a>
	<?php }
		if(defined("CONFIG_SYSTEM_WEBSITE_URL")){
//			$arrUrlData = explode("|", CONFIG_SYSTEM_WEBSITE_URL);
            echo '| ' . $app->getUiHelper()->getShortcutHeaderLink($agent->getName(), $agent->getAgentUrl());
            ?>
<!--              | <a href="--><?//=$arrUrlData[1]?><!--" target="_blank"><b><font color="#264661">--><?//=$arrUrlData[0]?><!--</font></b></a>-->
		<?php } ?>
			 </TD>

            </TR>
<?php } ?>

            <TR>

              <TD vAlign=top colSpan=3 height=24>
                <TABLE cellSpacing=0 cellPadding=0 width=100% background="" <? if (CONFIG_CUSTOM_COLOR_ENABLE == '1') { ?>bgcolor="<? echo CONFIG_CUSTOM_COLOR2 ?>"<? } ?> border=0>
                  <TBODY>
                    <TR>

                      <TD width=460 height="24" <? if (CONFIG_CUSTOM_COLOR_ENABLE != '1') { ?>background="images/hdr12.gif"<? } ?>><TABLE height=24 cellSpacing=0 cellPadding=0 width=214
                        border=0>
                          <TBODY>
                            <TR>
                              <TD width="214" <? if (CONFIG_CUSTOM_COLOR_ENABLE != '1') { ?>background="images/hdr11.gif"<? } ?> class=sml01><NOBR>&nbsp;&nbsp;<B><FONT
                              color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#005b90") ?>">Logged as:</FONT> <B><FONT color=#ffffff><?echo $content['name']?></FONT></B></B></NOBR></TD>
                            </TR>
                          </TBODY>
                      </TABLE></TD>
                      <TD width=68 valign="top" class=sml01><? if (CONFIG_CUSTOM_COLOR_ENABLE != '1') { ?><img src="images/hdr13.gif" width="54" height="24"><? } ?></TD>
                      <TD width=216 valign="top" class=sml01><a href="partner-list.php" target="_top" style="color: black"><button>Request for API connection</button></a></TD>
                      <TD class=sml01 align=right width=249><FONT color="<? echo (CONFIG_CUSTOM_COLOR_ENABLE == '1' ? "#ffffff" : "#000000") ?>"><strong><? echo $str ?>&nbsp;</strong></FONT></TD>
                    </TR>
                  </TBODY>
              </TABLE></TD>
            </TR>
          </TBODY>
      </TABLE></TD>
    </TR>
  </TBODY>
</TABLE>
</body>
</html>