<?php
	
	include ("../include/config.php");
	include ("security.php");
	include ("calculateBalance.php");
	include("double-entry-functions.php");
	 
	if(!empty($_REQUEST["sendingAcc"]) && $_REQUEST["getCurr"] == "sendingAcCurr")
	{
		
		$sqlSC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["sendingAcc"]."' ";
		$sendingCurrData = selectMultiRecords($sqlSC);
		$sendCurrData = array();
		foreach($sendingCurrData as $k=>$v){
			$sendCurrData[] = $v['currency'];
			
			$strClosingBalance = "
					SELECT 
						id,
						closing_balance,
						dated,
						accountNumber 
					FROM 
						".TBL_ACCOUNT_SUMMARY." 
					WHERE
						accountNumber = '".$v["accounNumber"]."' AND
						dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
						WHERE accountNumber = '".$v["accounNumber"]."')
					 ";
				$closingBalance = selectFrom($strClosingBalance);
			
			/*
			If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
			If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
			*/
			//debug($accountRs[$k]['balance']);
			
			if(in_array($v['currency'],$sendCurrData)){
				if($v['balanceType'] == 'Dr'){
					$sendingCBalance = $closingBalance["closing_balance"] - $v['balance'];
				
				}
				elseif($v['balanceType'] == 'Cr')
					$sendingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
			}
			//debug($sendingCBalance);
			$sendCurrDataReturn=$v['currency']."~".$sendingCBalance;
			echo $sendCurrDataReturn;
		}
		
	
	} 
	elseif(!empty($_REQUEST["receivingAcc"]) && $_REQUEST["getCurr"] == "receivingAcCurr")
	{
		$sqlRC = "SELECT id,accounNumber,accountName,currency,balanceType,balance FROM accounts WHERE status = 'AC' and accounNumber ='".$_REQUEST["receivingAcc"]."' ";
		$receivingCurrData = selectMultiRecords($sqlRC);
		
		//debug($sqlRC);
		//debug($receivingCurrData);
		$receiveCurrData = array();
		foreach($receivingCurrData as $k=>$v){
			$receiveCurrData[] = $v['currency'];
			$strClosingBalance = "
					SELECT 
						id,
						closing_balance,
						dated,
						accountNumber 
					FROM 
						".TBL_ACCOUNT_SUMMARY." 
					WHERE
						accountNumber = '".$v["accounNumber"]."' AND
						dated = (select MAX(dated) as dated FROM ".TBL_ACCOUNT_SUMMARY." 
						WHERE accountNumber = '".$v["accounNumber"]."')
					 ";
				$closingBalance = selectFrom($strClosingBalance);
				//debug($closingBalance);
			//debug($strClosingBalance);
			/*
			If closing balances of any account is in - (negative) then it appear as Debit Entry in this report
			If closing balances of any account is in + (positive) then it appear as Credit Entry in this report
			*/
			if(in_array($v['currency'],$receiveCurrData)){
				if($v['balanceType'] == 'Dr')
					$receivingCBalance = ($closingBalance["closing_balance"] - $v['balance']);
				elseif($v['balanceType'] == 'Cr')
					$receivingCBalance = ($closingBalance["closing_balance"] + $v['balance']);
			}
			//debug($receivingCBalance);
				$recvCurrDataReturn=$v['currency']."~".$receivingCBalance;
				echo $recvCurrDataReturn;
		}
		
	} 
?>