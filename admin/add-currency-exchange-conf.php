<?
// Session Handling
session_start();
// Including files
include ("../include/config.php");
include ("security.php");
include("connectOtherDataBase.php");
include("ledgersUpdation.php");

$agentType = getAgentType();
	if(!defined("CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE") || CONFIG_ENABLE_CURRENCY_EXCHANGE_MODULE!="1"){
		echo "<h2>Invalid Request.You have no rights to Access this Page !</h2>";
		exit;
	}
	/* #5781 - AMB Exchange
	 * Currency Exchange Fee module added
	 * Also works for manual fee.
	 * by Aslam Shahid.
	*/
	$currencyFeeFlag = false;
	if(defined("CONFIG_CURRENCY_EXCHANGE_FEE_USERS") && (strstr(CONFIG_CURRENCY_EXCHANGE_FEE_USERS,$agentType) || CONFIG_CURRENCY_EXCHANGE_FEE_USERS=="1")){
		$currencyFeeFlag = true;
	}
	$loggedUserID = $_SESSION["loggedUserData"]["userID"];
	$_SESSION["amountConverted"]  = ($_POST["amountConverted"]!="" ? $_POST["amountConverted"] : "");
	//redirect("reciept_curr_exch.php?recCurrExch=Y");
	if ($_POST["dDate"] == "")
	{
		$exchangeDate = date("Y-m-d");
		$exchangeDate1 = date("d/m/Y");
	}
	else
	{
		$dDate = explode("/",$_POST["dDate"]);
		if(count($dDate) == 3)
		{
			$dDate = $dDate[2]."-".$dDate[1]."-".$dDate[0];
		}
		else
		{
			$dDate = $_POST["dDate"];
		}			
		$exchangeDate = $dDate;
		$exchangeDate1 = $_POST["dDate"];
	}
	$_SESSION["exchangeDate"] = $exchangeDate1;	
	
	if($_REQUEST["saveCurrExchReport"] == "y")
	{
		$noteAsDispatchNum = time();
		$reportName = $_REQUEST['reportName'];
		$saveSql="insert into ".TBL_COMPLIANCE_QUERY." 
		(reportName,logedUser,logedUserID,queryDate,reportLabel,note) 
		values ('buySellCurrReports.php','".$agentType."','".$loggedUserID."','".date("Y-m-d H:i:s")."','".$reportName."','".$noteAsDispatchNum."')";
		insertInto($saveSql);
		$reportID = @mysql_insert_id();

		$excludeParams = array('saveCurrExchReport','get','page','rows','sidx','sord','nd','_search','q','PHPSESSID');
		//debug($_REQUEST);
		foreach($_REQUEST as $gk => $gv)
		{
			$gVal = $gv;
			if(!in_array($gk,$excludeParams)){
				if($gk == 'to_date' && empty($gVal))
					$gVal = '08/12/2010';
				$saveFiltersSql = "insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$gk."','".$gVal."')";
				insertInto($saveFiltersSql); 
			}
		}
		$returnMsg="Report Has Been Saved";  
		echo $returnMsg;
		exit;
	}
	if($_REQUEST["action"] == "Add")
	{
		$strCreatedBy = $_SESSION["loggedUserData"]["userID"];
	
		if(empty($_REQUEST["operationCurrency"]))
			$strOperationCurrencyVal = "";
		else
			$strOperationCurrencyVal = $_REQUEST["operationCurrency"];
		
		if(empty($_REQUEST["buysellCurrency"]))
			$strBuySellCurrencyVal = "";
		else
			$strBuySellCurrencyVal = $_REQUEST["buysellCurrency"];
		
		if(empty($_REQUEST["buying_rate"]))
			$strBuying_RateVal = "NULL";
		else
			$strBuying_RateVal = "'".$_REQUEST["buying_rate"]."'";
			
		if(!empty($_REQUEST["selling_rate"]))
			$strSelling_RateVal = "NULL";
		else
			$strSelling_RateVal = "'".$_REQUEST["selling_rate"]."'";
			
		$strExchangeDateVal = date("Y-m-d h:i:s");

		$Querry_Sqls = "insert into curr_exchange (operationCurrency, buysellCurrency,buying_rate,selling_rate,createdBy,updatedBy,created_at) 
	 	values (".$_REQUEST["operationCurrency"].", ".$_REQUEST["buysellCurrency"].", '".$_REQUEST["buying_rate"]."', '".$_REQUEST["selling_rate"]."',".$loggedUserID.",".$loggedUserID.",'".$strExchangeDateVal."')";
		if(insertInto($Querry_Sqls)){
			$ratId = @mysql_insert_id();
			$descript = "Currency Exchange added";
			activities($loggedUserID,"INSERTION",$ratId,"curr_exchange",$descript);
			$returnMsg ="Currency Exchange added successfully. "; 
			echo $returnMsg;
		}
		else{
			echo "E";
		}
	}
	elseif($_REQUEST['action']=='Update'){
		 $returnMsg = "Rate updated successfully";
		//debug($returnMsg);
		$updateSQL = " UPDATE curr_exchange 
		set buysellCurrency = '".$_REQUEST["buysellCurrency"]."', 
		buying_rate = '".$_REQUEST["buying_rate"]."', 
		selling_rate = '".$_REQUEST["selling_rate"]. "'  
		where id= '".$_REQUEST['id']."'";
		//debug($updateSQL, true);
		mysql_query($updateSQL);
		echo $returnMsg;
		exit; 	
	}
	if($_REQUEST["action"] == "addBuySell" && $_REQUEST["storeBuySellForm"]!="")
	{
		$strCreatedBy = $_SESSION["loggedUserData"]["userID"];
		$strCreated = date('Y-m-d H:i:s');
		$rate              = "";
		$localAmount       = "";
		$totalAmount       = "";
		$buy_or_sell       = "";
		$descript          = "";
		if($currencyFeeFlag){
			$currency_fee      = $_REQUEST['currency_fee'];// fee amount
			$currency_ID       = $_REQUEST['fi']; // fee id
			$total_localAmount = $_REQUEST['buy_sell_total_amount']; // total local after adding fee
		}
		$sendResponse = array();
		if($_REQUEST['storeBuySellForm'] =="B"){
			$rate           = $_REQUEST['buy_rate'];
			$localAmount    = $_REQUEST['buy_sell_amount'];
			$totalAmount    = $_REQUEST['totalAmountBought'];
			$buy_or_sell    ="B";
			$descript = "Buying currency for Customer added";
		}
		elseif($_REQUEST['storeBuySellForm'] =="S"){
			$rate           = $_REQUEST['sell_rate'];
			$localAmount    = $_REQUEST['buy_sell_amount'];
			$totalAmount    = $_REQUEST['totalAmountBought'];
			$buy_or_sell    ="S";
			$descript = "Selling currency for Customer added";
		}
			
		if(empty($_REQUEST["operationCurrency"]))
			$operationCurrency = 246;
		else
			$operationCurrency = $_REQUEST["operationCurrency"];
		
		if(empty($_REQUEST["buysellCurrency"]))
			$buysellCurrency = "";
		else
			$buysellCurrency = $_REQUEST["buysellCurrency"];

		if(empty($_REQUEST["customerID"]))
			$customerID = "";
		else
			$customerID = $_REQUEST["customerID"];
		
		if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1"){
			// records selected from grid on buy/sell txn page
			$strTransIds = $_REQUEST["transIds"];
		}		
		$strExchangeDateVal = date("Y-m-d H:i:s");
		if ($_POST["erID"] == ""){
			 $value = "";
			$backURL = "add-currency-exchange.php?msg=Y";
		} 
		else {
			$backURL = "update-rate.php?erID=$_POST[erID]&msg=Y&newOffset=$offset";
		}
		if(CONFIG_CURR_EXCH_STOCK_ACCOUNT=="1"){
			$strCurrIDs = "SELECT currencyName FROM ".TBL_CURRENCY." WHERE cID='$buysellCurrency' ";
			//debug($strCurrIDs);
			$strCurrIDRS = selectFrom($strCurrIDs);
			$strCurrName = $strCurrIDRS['currencyName'];
			
			$strStockCurr = "SELECT id FROM ".TBL_CURRENCY_STOCK_LIST." WHERE currency = '$strCurrName' ";
			//debug($strStockCurr);
			$strStockCurrRS = selectFrom($strStockCurr);
			if(empty($strStockCurrRS['id'])){
				echo 'stockError';
				exit;
			}
		}
		$Querry_Sqls = "insert into ".TBL_CURR_EXCHANGE_ACCOUNT." (operationCurrency,buysellCurrency,buy_sell,rate,localAmount,totalAmount,customerID,createdBy,updatedBy,created_at) values (".$operationCurrency.", ".$buysellCurrency.",'".$buy_or_sell."', ".$rate.",".$localAmount.",".$totalAmount.",'".$customerID."',".$loggedUserID.",".$loggedUserID.",'".$strExchangeDateVal."')";
		if(insertInto($Querry_Sqls)){
			$ratId = @mysql_insert_id();
			activities($loggedUserID,"INSERTION",$ratId,TBL_CURR_EXCHANGE_ACCOUNT,$descript);
			/*****
				#5258 - Minas Center
						Ledger is maintained for currency exchange module
						to be shown in Admin Account Statement
						Admin Account Summary is maintained for Opening/Closing Balances.
						by Aslam Shahid
			*****/
			$operationCurrencyQ = selectFrom("SELECT currencyName FROM ".TBL_CURRENCY." where cID='".$operationCurrency."'");
			$currecyToLedger = $operationCurrencyQ["currencyName"];
			if((CONFIG_CURR_EXCH_ADMIN_LEDGER=="1" && $agentType!="admin") || CONFIG_CURR_EXCH_STOCK_ACCOUNT=="1"){
				$amountToLedger = $localAmount;
				if($buy_or_sell=="S"){
					/**
					 * Make Withdraw entry on the ledger of the admin
					 */
					maintainAdminStaffAccount($loggedUserID, $amountToLedger,"", "WITHDRAW", "","CURRENCY EXCHANGE WITHDRAW", $currecyToLedger,date("Y-m-d"));
				}
				elseif($buy_or_sell=="B"){
					/**
					 * Deposit entry in admin account
					 */
					maintainAdminStaffAccount($loggedUserID, $amountToLedger,"", "DEPOSIT", "","CURRENCY EXCHANGE DEPOSIT", $currecyToLedger,date("Y-m-d"));
				}
			}
			if($currencyFeeFlag){
				$update_fee_str = "UPDATE ".TBL_CURR_EXCHANGE_ACCOUNT." 
									SET 	
										currency_fee_id='".$currency_ID."',
										currency_fee='".$currency_fee."',
										total_localAmount='".$total_localAmount."'
									WHERE id='".$ratId."' ";
				update($update_fee_str);
			}
			/*****
				#5287 - Minas Center
						Currency stock account is mainted and currency stock
						amounts are deducted/added
						by Aslam Shahid
			*****/
			if(CONFIG_CURR_EXCH_STOCK_ACCOUNT=="1"){
				$localCurrencyQ = selectFrom("SELECT currencyName FROM ".TBL_CURRENCY." WHERE cID='".$operationCurrency."'");
				$localCurrencyV = strtoupper($localCurrencyQ["currencyName"]);
				$buySellCurrencyQ = selectFrom("SELECT currencyName FROM ".TBL_CURRENCY." WHERE cID='".$buysellCurrency."'");
				$buySellCurrencyV = strtoupper($buySellCurrencyQ["currencyName"]);
				
				$withdrawAmount=0;
				$depositAmount=0;
				if($buy_or_sell == "S"){
					$withdrawCurr = $localCurrencyV;
					$withdrawAmount = number_format($localAmount,"2",".","");
				}
				elseif($buy_or_sell == "B"){
					$withdrawCurr = $buySellCurrencyV;
					$withdrawAmount = number_format($totalAmount,"2",".","");
				}
	
				if($buy_or_sell == "B"){
					$depositCurr = $localCurrencyV;
					$depositAmount =number_format($localAmount,"2",".","");
				}
				elseif($buy_or_sell == "S"){
					$depositCurr = $buySellCurrencyV;
					$depositAmount = number_format($totalAmount,"2",".","");
				}
				$setUpdateWith = "amount=amount-".$withdrawAmount;
				$setUpdateDep = "amount=amount+".$depositAmount;
				if(CONFIG_REVERSE_STOCK_ACCOUNT=="1"){
					$setUpdateWith = "amount=amount+".$withdrawAmount;
					$setUpdateDep = "amount=amount-".$depositAmount;
				}
				update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET ".$setUpdateWith." WHERE currency='".$withdrawCurr."'");
				update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET ".$setUpdateDep." WHERE currency='".$depositCurr."'");
			}	
		 	if(defined('CONFIG_CURRENCY_EXCHANGE_ACCOUNT') && CONFIG_CURRENCY_EXCHANGE_ACCOUNT!='0'){
				if($buy_or_sell == 'B'){
					$bsNotes = 'Currency Exchange Buy';
					$receiveAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
					$sendAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
				}
				else if($buy_or_sell == 'S'){
					$bsNotes = 'Currency Exchange Sell';
					$receiveAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
					$sendAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
				}
				$transID = $ratId;
				// get total fund transfers count and make transID for fund transfer
				$args = array('flag'=>'getTransID','type'=>'CE');
				$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);//'FT-'.$totalCntFT;
				if(!empty($returnData['transID']))
					$transID = $returnData['transID'];

				$arguments = array('flagTradingLedger'=>true,'amount'=>$localAmount,'ramount'=>$localAmount,'transID'=>$transID,'sendingAc'=>$sendAcc,'receivingAc'=>$receiveAcc,'sendingCurrency'=>$currecyToLedger,'receivingCurrency'=>$currecyToLedger,'note'=>$bsNotes,'createdBy'=>$strCreatedBy,'created'=>$strCreated,'buySell'=>$buy_or_sell);
				accountLedger($arguments); // This affects trading ledger and summary.
			}
			//exit;
			echo $ratId."|".$operationCurrency."|".$buysellCurrency;
		}
		else{
			echo "E";
		}
		
		//Buy New Stock will insert into ".TBL_CURRENCY_STOCK_HISTORY." to maintain currency stock history
		// added by Kalim @9363: AMB
		if(CONFIG_FOREIGN_CURRENCY_STOCK_HISTORY == "1")
		{
			if($buy_or_sell =="B")
			{
				$intLastBatchId	= selectFrom("SELECT id,batch_id FROM ".TBL_CURRENCY_STOCK_HISTORY." WHERE 1 ORDER BY id DESC");
				$batchIdArr		= explode("-",$intLastBatchId['batch_id']);
				$intNextBatchId = $batchIdArr[1] + 1;
				$strBatchId		= "B-".$intNextBatchId; 
				$strInsertStockHistory 	= "INSERT INTO 
												".TBL_CURRENCY_STOCK_HISTORY."
												(	batch_id,
													currency,
													amount, 
													available_amount, 
													buying_rate, 
													purshased_from, 
													status, 
													created_date
												)			
											VALUES
												(
													'".$strBatchId."',
													'".$buysellCurrency."',
													'".$totalAmount."',
													'".$totalAmount."',
													'".$rate."',
													'".$customerID."',
													'Open',
													'".date("Y-m-d")."'
												)";
												
				insertInto($strInsertStockHistory);
				$lastInsertionId = @mysql_insert_id();
			}
			// Selling Stock will affect Used Account, Available Amount and Status of stock history
			if($buy_or_sell =="S" && !empty($strTransIds))
			{
				$fltAmount			= 0;
				$fltUsedAmount		= 0;
				$fltAvlAmount		= 0;
				$fltUpdateAvlAmount	= 0;
				$fltUpdateUsedAmount= 0;
				$fltTotalAmount		= $totalAmount;  // Total Amount to Sold at a time from many Batches
				$fltSoldChunkAmount = $totalAmount;	// Chunk of Amount Sold Against specific batch 
				$endLoopFlg			= false;
				//debug($strTransIds);
				$queryAmounts	= "SELECT
										id,
										batch_id, 
										amount, 
										used_amount,
										available_amount 
									FROM 
										".TBL_CURRENCY_STOCK_HISTORY." 
									WHERE 
										id IN (".$strTransIds.")
									ORDER BY amount ASC";
				//debug($queryAmounts, true);					
				$arrAmounts		= selectMultiRecords($queryAmounts);
				//debug($arrAmounts);
				foreach($arrAmounts as $key=>$val)
				{	
				
					$strStatusQuery	= ""; // str will be reset for next entery
					
					$strBatchID		= $val["batch_id"];				// Batch ID
					$fltAmount		= $val["amount"];				// Total amount
					$fltUsedAmount	= $val["used_amount"];			// Already used amount
					$fltAvlAmount	= $val["available_amount"]; 	// Available Amount (Stock)
					
					
					if($fltAvlAmount >= $fltTotalAmount)
					{
						$fltUpdateAvlAmount	= $fltAvlAmount - $fltTotalAmount;	 // Available Amount after Selling
						$fltUpdateUsedAmount= $fltUsedAmount + $fltTotalAmount; // Used Amount after Selling
						$fltSoldChunkAmount	= $fltTotalAmount;					// This Chunk of Amount Sold from this Batch [$strBatchID] 
						
						// Available Amount after Selling become zero close it and enter closinf date
						if($fltUpdateAvlAmount == 0)
							$strStatusQuery = ", status='Closed', closing_date='".date("Y-m-d")."'";
							
						$endLoopFlg = true; 						
					}
					else
					{
						$fltUpdateAvlAmount	= 0;
						$fltUpdateUsedAmount= $fltAmount; 
						$fltTotalAmount		= $fltTotalAmount - $fltAvlAmount;
						$fltSoldChunkAmount	= $fltSoldChunkAmount - $fltTotalAmount;		// This Chunk of Amount Sold from this Batch [$strBatchID] 
						$strStatusQuery 	= ", status='Closed', closing_date='".date("Y-m-d")."'";
					} 

					$strUpdateStockHistory 	= "UPDATE 
													".TBL_CURRENCY_STOCK_HISTORY." 
												SET 
													used_amount			= '".$fltUpdateUsedAmount."',
													available_amount	= '".$fltUpdateAvlAmount."'";

					$strUpdateStockHistory	.=	$strStatusQuery;
					$strUpdateStockHistory	.= " WHERE id='".$val['id']."'";
											   
					update($strUpdateStockHistory);
					
					// To keep Stock Selling History
					$strInsertStockHistoryUsed	= " INSERT INTO 
														currency_stock_history_used
														(	from_batch_id, 
															amount,  
															sell_rate, 
															date_out
														)
													VALUES
														(	'".$strBatchID."',
															'".$fltSoldChunkAmount."',
															'".$rate."',
															'".date("Y-m-d")."'
														)
													";
													
					insertInto($strInsertStockHistoryUsed);
					$lastIntryId = @mysql_insert_id();
					
					if($endLoopFlg)
						break;
				}
			}
		}		
	} 
	if($_REQUEST["rate"] == "get")
	{
		include ("JSON.php");
		$response = new Services_JSON();

		if(!empty($_REQUEST["currID"]))
		{
			$strRatesData = selectFrom("select buying_rate,selling_rate,currencyName from curr_exchange ce,currencies cr where ce.buysellCurrency = cr.cID and buysellCurrency=".$_REQUEST["currID"]." order by id DESC");
			$rates = array();
			if($strRatesData["buying_rate"] != ""){
				$rates['buying_rate'] = round($strRatesData["buying_rate"],4);
				$rates['selling_rate'] = round($strRatesData["selling_rate"],4);
				$rates['buySellCurrName'] = $strRatesData["currencyName"];
			}
			echo $response->encode($rates); 
			exit;
		}
		echo "E";
		exit;
	}

if($currencyFeeFlag){
	if($_REQUEST["fee"] == "get")
	{
		//include ("JSON.php");
		//$response = new Services_JSON();
		$fee = 0 ;
		$feeType = "F";
		if(!empty($_REQUEST["currencyF"]) && !empty($_REQUEST["currencyT"]))
		{
			$feeId = calculateCurrencyExchangeFee('CE', $_REQUEST["currencyF"], $_REQUEST["currencyT"]);
			//debug($feeId);
			if($feeId)
			{
				$arrFullFeeData = selectFrom("select fee_id, type, fee from currency_exchange_fee where fee_id='".$feeId."'");
				$fee = $arrFullFeeData["fee"];
				$feeType = $arrFullFeeData["type"];
				if(CONFIG_ROUND_FEE_ENABLED_CHEQ_CASH=="1"){
					if(defined("CONFIG_ROUND_NUMBER_TO_CHEQ_CASH") && is_numeric(CONFIG_ROUND_FEE_TO_CHEQ_CASH)){
						$fee = round($fee,CONFIG_ROUND_FEE_TO_CHEQ_CASH);
					}
				}
			}
		}
		/* Any problem in the communication, so print E for error indication */
		echo $feeId."|".$fee."|".$feeType;//$response->encode($arrFullFeeData); 
		exit;
	}
}

	if($_REQUEST["get"] == "getceGrid")
	{
		include ("JSON.php");

		$response = new Services_JSON();
		$page = $_REQUEST['page'];
		$limit = $_REQUEST['rows'];
		$sidx = $_REQUEST['sidx'];
		$sord = $_REQUEST['sord'];
		
		if(!$sidx)
			$sidx = 1;
		$currExchData = "select ce.*,a.name,a.userID,c.firstname, c.middlename, c.lastname from ".TBL_CURR_EXCHANGE_ACCOUNT." ce LEFT JOIN customer c ON ce.customerID = c.customerID,admin a where ce.createdBy=a.userID ";
		/*	#4693
			Cancel Transaction functionality added.
			Config handles cancelling a transaction and also
			viewing Cancelled transactions on its Report page.
			by Aslam Shahid
		*/
		if(CONFIG_CANCEL_TRANS_CURR_EXCH=="1"){
			if($_REQUEST["type"] == "Cancelled") // action for buySellCurrReports-cancelled.php
			{
				$currExchData.=" AND ce.transStatus ='Cancelled'";
			}
			else{
				$currExchData.=" AND ce.transStatus !='Cancelled'";
			}
			if($_REQUEST["cancelTrans"] == "Y") // action for cancelling transaction
			{
				if(!empty($_REQUEST["transIds"])){
					update("UPDATE ".TBL_CURR_EXCHANGE_ACCOUNT." SET transStatus ='Cancelled' WHERE id IN(".$_REQUEST["transIds"].")");
					if(defined('CONFIG_CURRENCY_EXCHANGE_ACCOUNT') && CONFIG_CURRENCY_EXCHANGE_ACCOUNT!='0'){
						//debug($_REQUEST["transIds"]);
						$cancelledTransArr = explode(',',$_REQUEST["transIds"]);
						//debug($cancelledTransArr);
						//$cancelledTransIds = explode(','$_REQUEST["transIds"]);
						
						foreach($cancelledTransArr as $k=>$v){
							/*@5976-AMB: when cancell trasaction then stock value also reverse by Niaz Ahmad*/
							$cancellData = selectFrom("SELECT id,operationCurrency,buysellCurrency,buy_sell,localAmount,totalAmount
							FROM ".TBL_CURR_EXCHANGE_ACCOUNT." WHERE id ='$v' ");
							if($cancellData["id"] !='')
							{
								$strBuyCurr = selectFrom("SELECT cID,currencyName  FROM currencies WHERE cID ='".$cancellData["buysellCurrency"]."'");
								$strOpCurr = selectFrom("SELECT cID,currencyName  FROM currencies WHERE cID ='".$cancellData["operationCurrency"]."'");
								if($strBuyCurr["cID"]!='')
									$strBuyStockAmount = selectFrom("SELECT id,amount FROM ".TBL_CURRENCY_STOCK_LIST." WHERE currency ='".$strBuyCurr["currencyName"]."'");
								if($strOpCurr["cID"]!='')
									$strOpStockAmount = selectFrom("SELECT id,amount FROM ".TBL_CURRENCY_STOCK_LIST." WHERE currency ='".$strOpCurr["currencyName"]."'");
		
								if(CONFIG_REVERSE_STOCK_ACCOUNT=="1"){
									$setUpdateWith = "amount=amount+".$withdrawAmount;
									$setUpdateDep = "amount=amount-".$depositAmount;
								}
								if($cancellData["buy_sell"] == 'B'){
									$updateLA = $strOpStockAmount["amount"] - $cancellData["localAmount"];
									$updateBA = $strBuyStockAmount["amount"] + $cancellData["totalAmount"];
									if(CONFIG_REVERSE_STOCK_ACCOUNT=="1"){
										$updateLA = $strOpStockAmount["amount"] + $cancellData["localAmount"];
										$updateBA = $strBuyStockAmount["amount"] - $cancellData["totalAmount"];
									}
									update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET amount ='".$updateLA."' WHERE currency = '".$strOpCurr["currencyName"]."'");
									update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET amount ='".$updateBA."' WHERE currency = '".$strBuyCurr["currencyName"]."'");
								}elseif($cancellData["buy_sell"] == 'S'){
									$updateLA = $strOpStockAmount["amount"] + $cancellData["localAmount"];
									$updateBA = $strBuyStockAmount["amount"] - $cancellData["totalAmount"];
									if(CONFIG_REVERSE_STOCK_ACCOUNT=="1"){
										$updateLA = $strOpStockAmount["amount"] - $cancellData["localAmount"];
										$updateBA = $strBuyStockAmount["amount"] + $cancellData["totalAmount"];
									}
									update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET amount ='".$updateLA."' WHERE currency = '".$strOpCurr["currencyName"]."'");
									update("UPDATE ".TBL_CURRENCY_STOCK_LIST." SET amount ='".$updateBA."' WHERE currency = '".$strBuyCurr["currencyName"]."'");
								}	
								$strTradeAcc = "SELECT 	transID,
														crAmount,
														drAmount,
														crAccount,
														drAccount,
														currency,
														description,
														createdBy,
														created 
													FROM account_ledgers 
													WHERE transID='$v' ";
								//debug($strTradeAcc);
								$tradeAccRS = selectFrom($strTradeAcc);
								//debug($tradeAccRS);
								$strCreatedBy = $_SESSION["loggedUserData"]["userID"];
								$strCreated = date('Y-m-d H:i:s');
								if(!empty($tradeAccRS['transID'])){
									$description = $tradeAccRS['description'];
									$localAmount = $tradeAccRS['crAmount'];
									$ratId = $tradeAccRS['transID'];
									$currecyToLedger = $tradeAccRS['currency'];
								}
								else{
									if($cancellData["buy_sell"] == 'B')
										$description = 'Currency Exchange Buy';
									elseif($cancellData["buy_sell"] == 'S')
										$description = 'Currency Exchange Sell';
									$localAmount = $cancellData["localAmount"];
									$ratId = $v;
									$currecyToLedger = $strOpCurr["currencyName"];
								}
								if($description == 'Currency Exchange Buy'){
									$bsNotes = $description;
									$buy_or_sell = 'S';
									$receiveAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
									$sendAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
								}
								else if($description == 'Currency Exchange Sell'){
									$bsNotes = $description;
									$buy_or_sell = 'B';
									$receiveAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
									$sendAcc = CONFIG_CURRENCY_EXCHANGE_ACCOUNT;
								}
								
								$transID = $ratId;
								
								// get total fund transfers count and make transID for fund transfer
								$args = array('flag'=>'getTransID','type'=>'CE','CancelTrans'=>$cancellData["id"]);
								$returnData = gateway('CONFIG_ACCOUNT_LEDGER_TRANS_TYPE',$args,CONFIG_ACCOUNT_LEDGER_TRANS_TYPE);
								
								if(!empty($returnData['transID']))
									$transID = $returnData['transID'];
								$arguments = array('flagTradingLedger'=>true,'amount'=>$localAmount,'transID'=>$transID,'sendingAc'=>$sendAcc,'receivingAc'=>$receiveAcc,'sendingCurrency'=>$currecyToLedger,'note'=>$bsNotes,'createdBy'=>$strCreatedBy,'created'=>$strCreated,'buySell'=>$buy_or_sell,'status'=>'C');	
								//debug($arguments);
								accountLedger($arguments); // This affects trading ledger and summary.
							}
						}
					}
				}
			}
		}
		//debug($currExchData,true);
		$names = explode(" ",$_REQUEST["clientName"]);
		if($_REQUEST["Submit"] == "Search")
		{
			if(!empty($_REQUEST["from_date"]))
			{
				$date = explode("/",$_REQUEST["from_date"]);
				$currExchData .= " and created_at >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
			}
			
			if(!empty($_REQUEST["to_date"]))
			{
				$date = explode("/",$_REQUEST["to_date"]);
				$currExchData .= " and created_at <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
			}
		
			if(!empty($_REQUEST["clientName"])){
				$names = $_REQUEST["clientName"];
				$currExchData .= " and (firstname like '%".$names."%' || middlename like '%".$names."%' || lastname like '%".$names."%') ";
			}
			if(!empty($_REQUEST["createdBy"])){
				$currExchData .= " and name like '%".$_REQUEST["createdBy"]."%'";
			}
			if(!empty($_REQUEST["type_of_exchange"])){
				$currExchData .= " and buy_sell ='".$_REQUEST["type_of_exchange"]."'";
			}
			if(CONDIF_CURRENY_FILTER_CURRENCY_EXCHANGE=="1"){
				if(!empty($_REQUEST["currency"])){
					$currExchData .= " and buysellCurrency ='".$_REQUEST["currency"]."'";
				}
			}
		}
		$result = mysql_query($currExchData) or die(__LINE__.": ".mysql_query());
		$count = mysql_num_rows($result);

		if($count > 0)
		{
			$total_pages = ceil($count / $limit);
		} else {
			$total_pages = 0;
		}
		
		if($page > $total_pages)
		{
			$page = $total_pages;
		}
		
		$start = $limit * $page - $limit; 
		
		if($start < 0)
		{
			$start = 0;
		}
		
		$currExchData .= " order by $sidx $sord LIMIT $start , $limit";
		$result = mysql_query($currExchData) or die(__LINE__.": ".mysql_error());
	
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $count;

		$i=0;
		$totalBuyLAmount = 0;
		$totalSellLAmount = 0;
		$total_amount =0;
		$totalLocalLAmount =0;
		while($arrOrderData = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$exchangeType = $arrOrderData["buy_sell"] =="B" ? "Buy" : "Sell";
			$opcurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$arrOrderData["operationCurrency"]);
			$bscurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$arrOrderData["buysellCurrency"]);
			$opcurrName   = $opcurrData['currencyName'];
			$opcountName  = $opcurrData['country'];
			$bscurrName   = $bscurrData['currencyName'];
			$bscountName  = $bscurrData['country'];
			$creator 	  = $arrOrderData["createdBy"];
			if($exchangeType == 'Buy')
				$totalBuyLAmount += $arrOrderData["localAmount"];
			else if($exchangeType == 'Sell')
				$totalSellLAmount += $arrOrderData["localAmount"];
				
			$totalLocalLAmount += $arrOrderData["localAmount"];	
			$total_amount += $arrOrderData["totalAmount"];	
			$response->rows[$i]['id'] = $arrOrderData["id"];
			$response->rows[$i]['cell'] = array(
												$arrOrderData['id'],
												$arrOrderData["firstname"],
												$arrOrderData["created_at"],
												$exchangeType,
												$bscurrName,
												$arrOrderData["localAmount"],
												$arrOrderData["rate"],
												$arrOrderData["totalAmount"],
												$arrOrderData["name"]
											);
			$i++;
		}	
//		$response->buyLAmount = $totalBuyLAmount;
//		$response->sellLAmount = $totalSellLAmount;		
/*		echo '<input type="hidden" value="'.$totalBuyLAmount.'" id="total_buy" />';
		echo '<input type="hidden" value="'.$totalSellLAmount.'" id="total_sell" />';
		echo '<input type="hidden" value="'.$totalLocalLAmount.'" id="total_local" />';
		echo '<input type="hidden" value="'.$total_amount.'" id="total_amount" />';*/
		echo $response->encode($response);
		exit;
	}

if($_REQUEST["get"] == "getcommGrid")
{
	include ("JSON.php");

	$response = new Services_JSON();
	$page = $_REQUEST['page'];
	$limit = $_REQUEST['rows'];
	$sidx = $_REQUEST['sidx'];
	$sord = $_REQUEST['sord'];
	
	if(!$sidx)
		$sidx = 1;
	$currExchData = "select ce.*,a.name,a.userID,c.firstname, c.middlename, c.lastname from ".TBL_CURR_EXCHANGE_ACCOUNT." ce LEFT JOIN customer c ON ce.customerID = c.customerID,admin a where ce.createdBy=a.userID ";
	//debug($currExchData,true);
	$names = explode(" ",$_REQUEST["clientName"]);
	if($_REQUEST["Submit"] == "Search")
	{
		if(!empty($_REQUEST["from_date"]))
		{
			$date = explode("/",$_REQUEST["from_date"]);
			$currExchData .= " and created_at >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 00:00:00'";
		}
		
		if(!empty($_REQUEST["to_date"]))
		{
			$date = explode("/",$_REQUEST["to_date"]);
			$currExchData .= " and created_at <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))." 23:59:59'";
		}
	
		if(!empty($_REQUEST["clientName"])){
			$names = $_REQUEST["clientName"];
			$currExchData .= " and (firstname like '%".$names."%' || middlename like '%".$names."%' || lastname like '%".$names."%') ";
		}
		if(!empty($_REQUEST["createdBy"])){
			$currExchData .= " and name like '%".$_REQUEST["createdBy"]."%'";
		}
		if(!empty($_REQUEST["type_of_exchange"])){
			$currExchData .= " and buy_sell ='".$_REQUEST["type_of_exchange"]."'";
		}
		if(!empty($_REQUEST["currency"])){
			$currExchData .= " and buysellCurrency ='".$_REQUEST["currency"]."'";
		}
	}
	$result = mysql_query($currExchData) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);

	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; 
	
	if($start < 0)
	{
		$start = 0;
	}
	
	$currExchData .= " order by $sidx $sord LIMIT $start , $limit";
	$result = mysql_query($currExchData) or die(__LINE__.": ".mysql_error());

	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	while($arrOrderData = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$exchangeType = $arrOrderData["buy_sell"] =="B" ? "Buy" : "Sell";
		$opcurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$arrOrderData["operationCurrency"]);
		$bscurrData   = selectFrom("select currencyName,country from currencies where 1 and cID = ".$arrOrderData["buysellCurrency"]);
		$opcurrName   = $opcurrData['currencyName'];
		$opcountName  = $opcurrData['country'];
		$bscurrName   = $bscurrData['currencyName'];
		$bscountName  = $bscurrData['country'];
		$creator 	  = $arrOrderData["createdBy"];
		$response->rows[$i]['id'] = $arrOrderData["id"];
		$response->rows[$i]['cell'] = array(
											$arrOrderData['id'],
											$arrOrderData["firstname"],
											$arrOrderData["created_at"],
											$exchangeType,
											$bscurrName,
											$arrOrderData["localAmount"],
											$arrOrderData["currency_fee"],
											$arrOrderData["total_localAmount"],
											$arrOrderData["totalAmount"],
											$arrOrderData["name"]
										);
		$i++;
	}	
	echo $response->encode($response);
	exit;
}





// get stock history grid on buy sell currency exchange
if($_REQUEST["get"] == "getStockHistoryGrid")
{
	// generic logic to use JSON
	include ("JSON.php");
	$response = new Services_JSON(); 
	// generic query string variables retrieved automatically
	$page = $_REQUEST['page'];
	$limit = $_REQUEST['rows'];
	$sidx = $_REQUEST['sidx'];
	$sord = $_REQUEST['sord'];
	if(!$sidx)
		$sidx = 1;
		
	$intForeignCurrID	=	$_REQUEST["currencyID"];
	// query to fetch records and display in grid
	$stockHistoryQuery	= "	SELECT 
								id,
								batch_id,
								currency,
								amount,
								used_amount,
								available_amount,
								buying_rate,
								purshased_from,
								status,
								created_date,
								closing_date
							FROM 
								".TBL_CURRENCY_STOCK_HISTORY."	
							WHERE
								currency	= '".$intForeignCurrID."'";
								
	if($_REQUEST["Submit"] == "Search")
	{
		if(!empty($_REQUEST["from_date"]))
		{
			$date = explode("/",$_REQUEST["from_date"]);
			$stockHistoryQuery .= " AND created_date >= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
		}
		
		if(!empty($_REQUEST["to_date"]))
		{
			$date = explode("/",$_REQUEST["to_date"]);
			$stockHistoryQuery .= " AND created_date <= '".date("Y-m-d",mktime(0,0,0,$date[1],$date[0],$date[2]))."'";
		}
	}

	//debug($stockHistoryQuery);
	$result = mysql_query($stockHistoryQuery) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($result);
	//debug($count);
	// generic code for pagination logic
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; 
	
	if($start < 0)
	{
		$start = 0;
	}
	
	 //order by logic
	$stockHistoryQuery .= " order by $sidx $sord LIMIT $start , $limit";
	//debug($stockHistoryQuery);
	$result = mysql_query($stockHistoryQuery) or die(__LINE__.": ".mysql_error());
	
	// generic parameter values
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;
	
	$i=0;
	while($arrStockHistoryData = mysql_fetch_array($result, MYSQL_ASSOC)) 
	{
		$batch_id			=	$arrStockHistoryData["batch_id"];
		$fltAmount			=	$arrStockHistoryData["amount"];
		$fltUsedAmount		=	$arrStockHistoryData["used_amount"];
		$fltAvailableAmount	=	$arrStockHistoryData["available_amount"];
		$fltBuyingRate		=	$arrStockHistoryData["buying_rate"];
		$intCustomerID		=	$arrStockHistoryData["purshased_from"];
		$arrPurshasedFrom	=	selectFrom("SELECT firstName, middleName, lastName FROM customer WHERE customerID='".$intCustomerID."'");
		$strPurshasedFrom	=	$arrPurshasedFrom["firstName"]." ".$arrPurshasedFrom["middleName"]." ".$arrPurshasedFrom["lastName"];	
		$strStatus			=	$arrStockHistoryData["status"];
		$dateInArr			=	explode("-",$arrStockHistoryData["created_date"]);
		$dateIN				=	$dateInArr[2]."/".$dateInArr[1]."/".$dateInArr[0];
		$dateClosingArr		=	explode("-",$arrStockHistoryData["closing_date"]);	
		$dateClosing		=	$dateClosingArr[2]."/".$dateClosingArr[1]."/".$dateClosingArr[0];
		if($strStatus == "Open")
			$dateClosing	= "";
		
		$response->rows[$i]['id'] = $arrStockHistoryData["id"];
		$response->rows[$i]['cell'] = array(
											$batch_id,
											$dateIN,
											$fltAmount,
											$fltUsedAmount,
											$fltAvailableAmount,
											$fltBuyingRate,
											$strPurshasedFrom,
											$strStatus,
											$dateClosing
										);
		$i++;
	}	
	echo $response->encode($response);
	exit;
}

?>