<?
session_start();
require_once ("../include/config.php");
//require_once ("./include/flags.php");
require_once ("security.php");

$date_time = date('d-m-Y  h:i:s A');
$_SESSION["pincode"] = "";


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$preFix = SYSTEM_PRE;

if ($_GET["userID"]!="")
{
 	$userID = $_GET["userID"];
}

if($_GET["searchBy"])
{
	$searchBy = $_GET["searchBy"];	
}

if ($_GET["Country"] != ""){
	$Country = $_GET["Country"];
}
if ($_GET["agentStatus"] != ""){
	$agentStatus = $_GET["agentStatus"];
}
if ($_GET["agentCode"] != ""){
	$agentCode = $_GET["agentCode"];
}

if ($_GET["flag"] != "C"){ 

	$_SESSION["loginName"] = $_POST["loginName"];
	$_SESSION["password"] = $_POST["password"];
	$_SESSION["personName"] = $_POST["personName"];
	$_SESSION["address"] = $_POST["address"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["postcode"] = $_POST["postcode"];
	$_SESSION["phone"] = $_POST["phone"];
	$_SESSION["mobile"] = $_POST["mobile"];
	$_SESSION["fax"] = $_POST["fax"];
	$_SESSION["email"] = $_POST["email"];	
	$_SESSION["accessFromIP"] = $_POST["accessFromIP"];
	$_SESSION["companyName"] = $_POST["companyName"];
	$_SESSION["designation"] = $_POST["designation"];	
}
	
	

if($_POST["userID"] != "")
{
	$backUrl = "add_super_admin.php?msg=Y&userID=" . $_POST["userID"];
}
else
{
	$backUrl = "add_super_admin.php?msg=Y&flag=Y";
}

if ($_GET["flag"]!="C"){
if(trim($_POST["loginName"])== "") {				
	insertError("please enter the Login Name");	
	redirect($backUrl);	
} else {			
	if (isExist("select username from ".TBL_ADMIN_USERS." where username = '".checkValues($_POST["loginName"])."' and userID != '".checkValues($_POST["userID"])."'")) {
		insertError(AG32);			
		redirect($backUrl);
	}
	if (isExist("SELECT `username` FROM " . TBL_CM_CUSTOMER . " WHERE `username` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);			
		redirect($backUrl);
	}
	if (isExist("SELECT `accountName` FROM " . TBL_CUSTOMER . " WHERE `accountName` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);			
		redirect($backUrl);
	}
	if (isExist("SELECT `loginName` FROM " . TBL_TELLER . " WHERE `loginName` = '".checkValues($_POST["loginName"])."'")) {
		insertError(AG32);			
		redirect($backUrl);
	}
}




if(trim($_POST["password"]) == "")
{
	insertError("Please provide password");	
	redirect($backUrl);
}

if(trim($_POST["personName"]) == "")
{
	insertError("Please provide the Admin Name");	
	redirect($backUrl);
}
if(trim($_POST["address"]) == "")
{
	insertError("Please provide the Address");	
	redirect($backUrl);
}
if(trim($_POST["Country"]) == "")
{
	insertError("Please select the country");	
	redirect($backUrl);
}
/*	
if(trim($_POST["postcode"]) == "")
{
	insertError("Please enter post code");	
	redirect($backUrl);
}
if(trim($_POST["City"]) == "")
{
	insertError("Please enter city name");	
	redirect($backUrl);
}*/	
if(trim($_POST["phone"]) == "")
{
	insertError("Please enter phone number");	
	redirect($backUrl);
}
/*
if(trim($_POST["mobile"]) == "")
{
	insertError("Please enter mobile number");	
	redirect($backUrl);
}
if(trim($_POST["email"]) == "")
{
	insertError("Please enter email number");	
	redirect($backUrl);
}*/
if(trim($_POST["companyName"]) == "")
{
	insertError("Please enter company name");	
	redirect($backUrl);
}
/*
if(trim($_POST["designation"]) == "")
{
	insertError("Please enter your designation");	
	redirect($backUrl);
}
*/
}
?>
<html>
<head>
<title>Super Admin Confirmation</title>
<script>
		function checkForm(){
		
			if(document.forms.addTrans.pincode.value == "" || IsAllSpaces(document.forms.addTrans.pincode.value)){
					alert("Please provide Password.");
				 document.forms.addTrans.pincode.focus();
				return false;
			}
			return true;
		}	
		
		function pincodeValue()
		{
				var temp;
				temp = document.forms.addTrans.pincode.value;
				var strurl = 'check-password.php?from=popUp&pincode=' + temp;
				window.open(strurl,'CheckPassword', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=150,width=400');
		}
		function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
</script>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 <form name="addTrans" action="add_super_admin_conf.php?userID=<?=$userID;?>&agentCode=<?=$agentCode;?>&searchBy=<?=$searchBy;?>&Country=<?=$Country;?>&agentStatus=<?=$agentStatus;?>" method="post" onSubmit="return checkForm();">
  
    <tr>
      <td align="center" valign="top"><br>
        <table width="700" border="0" bordercolor="#FF0000">
  <tr height="30">
    <td align="left" bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">&nbsp;Confirm Super Admin.</font></b></td>
  </tr>		
          <tr>
            <td align="right"><a href="add_super_admin.php" class="style2">Change Information</a></td>
          </tr>
           <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="FFFFCCC"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><font color="#FF0000"> 
                    <? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?>
                    </font></td>
                </tr>
              </table>
            </td>
          </tr>
          <? } ?>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Confirm Super Admin</legend>
            <br>
            <?

	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo $width?>" height="<? echo $height?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img id=Picture2 height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>" border=0> </td>
              </tr>
            </table>
	<?
	}
	?>
            <br>
			<font color="#005b90">Date: </font><? echo date("F j, Y")?><br>
            </fieldset></td>
          </tr>

          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Personal Information</legend>
              <table border="0" cellpadding="2" cellspacing="0">
               
                <tr>
                  <td width="150" align="right"><font color="#005b90">Admin Name</font></td>
                  <td colspan="3" align="left"><? echo $_SESSION["personName"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3" align="left"><? echo $_SESSION["address"] . " " . $customerContent["c_address2"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200" align="left"><? echo $_SESSION["postcode"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200" align="left"><? echo $_SESSION["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200" align="left"><? echo $_SESSION["phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Mobile</font></td>
                  <td width="200" align="left"><? echo $_SESSION["mobile"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Fax</font></td>
                  <td width="200" align="left"><? echo $_SESSION["fax"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200" align="left"><? echo $_SESSION["email"]?></td>
                </tr>
              
            </table>
              </fieldset></td>
          </tr>
            
              <tr>
                <td><fieldset>
              <legend class="style2">Company Information </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                     
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Company Name</font></td>
                        <td width="200" align="left"><? echo $_SESSION["companyName"]?></td>
                        <td width="100" align="right"><font color="#005b90">Access from IP</font></td>
                        <td width="200" align="left"><? echo $_SESSION["accessFromIP"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Designation</font></td>
                        <td width="200" align="left"><? echo $_SESSION["designation"]?></td>                       
                       
                      </tr>				  
					  <td colspan="4"><fieldset>
                    <legend class="style2">Password </legend>
					  
					<table width="650" border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                     
                      <tr>
                        <td  height="20" align="right"><font color="#005b90">Password</font> </td>
                        <td  height="20" align="left" colspan="3">
						<input type="password" name="pincode" value="<?=$_SESSION["pincode"]; ?>" maxlength="20">
                  &nbsp;&nbsp;&nbsp;<a href="javascript:;" class="style1" onClick="pincodeValue()"><font color="#FF0000"><b>Verify 
                 Password</b></font></a> </td>
						 <tr>
							 <td colspan="4">&nbsp;</td>
						 </tr>
                      </tr>
                    </table>					  
					  </fieldset>
					  </td>					  
					  </tr>
					 	
				
					
          <tr>
            <td align="center" colspan="4">
			<input type="submit" name="Submit" value="submit">
            		
			</td>
          </tr>
          <tr>
            <td align="right"><a href="add_super_admin.php?flag=Y" class="style2">Change Information</a></td>
          </tr>
      </table></td>
		
    </tr>
</form>	
</table>
</body>
</html>
