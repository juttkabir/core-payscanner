<?
	session_start();
	include ("../include/config.php");
	$date_time = date('d-m-Y  h:i:s A');
	include ("security.php");
	$agentType = getAgentType();

	$numberOfDecimal = 2;
	$totals["openingBal"] = 0;
	$totals["bankTransferCount"] = 0;
	$totals["totalBankTraferAmount"] = 0;
	$totals["totalAuthorizeTransAmount"] = 0;
	$totals["closingBal"] = 0;
	$totals["cashBalance"] = 0;
	$totals["overBanking"] = 0;
	$totals["totalClearedChequeAmount"] = 0;
	$totals["totalUnclearedChequeAmount"] = 0;
	
	// New Values
	$totals["totalBankTransAgentGainOrLoss"] = 0;
	$totals["totalAuthorizeTransAgentGainOrLoss"] = 0;
	$totals["totalClearedChequeTransAgentGainOrLoss"] = 0;
	$totals["totalUnclearedChequeTransAgentGainOrLoss"] = 0;

	/* To have Total Of Mixed Banking And Cheque based on todays transactions only */
	$totals["todayTotalOfMixedBankingAndCheque"] = 0;

	$reportAgentsData = array();

	$startDate = "";
	$endDate = "";

	/** A pathetic patch **/
	if(empty($_REQUEST["searchRequest"]))
	{
		$_REQUEST["searchRequest"] = "1";
		$_REQUEST["startYear"] = date("Y", time());
		$_REQUEST["startMonth"] = date("m", time());
		$_REQUEST["startDay"] = date("d", time());
		$_REQUEST["endYear"] = date("Y", time());
		$_REQUEST["endMonth"] = date("m", time());
		$_REQUEST["endDay"] = date("d", time());
	}

	if ( !empty( $_REQUEST["searchRequest"] ) )
	{
		$startDate = $_REQUEST["startYear"] . "-" . $_REQUEST["startMonth"] . "-" . $_REQUEST["startDay"];
		$endDate = $_REQUEST["endYear"] . "-" . $_REQUEST["endMonth"] . "-" . $_REQUEST["endDay"];

		$agentFilterCondition = "";

		if ( !empty( $_REQUEST["agentIDsForReport"] ) || !empty($_REQUEST["agentIDsForReport"][0]) )
		{
			foreach( $_REQUEST["agentIDsForReport"] as $agentID )
			{
				if ( !empty( $agentID ) )
					$agentFilterCondition .= "'" . $agentID . "',";
			}
			if ( !empty($agentFilterCondition) )
				$agentFilterCondition = " userID in(" . substr( $agentFilterCondition, 0, strlen($agentFilterCondition) - 1 ) . ") and ";
		}

		$reportAgentsQuery = "select userID,username, agentCompany, name, agentContactPerson,paymentMode from ".TBL_ADMIN_USERS." where " . $agentFilterCondition . " parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'N' order by username asc";

		$reportAgentsData = selectMultiRecords( $reportAgentsQuery );

		if ( $agentType == "admin" )
		{ 
			foreach( $reportAgentsData as $key => $agentData )
			{
				$reportAgentsData[$key]["accountStatement"]["openingBal"] = 0;
				$reportAgentsData[$key]["accountStatement"]["bankTransferCount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["closingBal"] = 0;
				$reportAgentsData[$key]["accountStatement"]["cashBalance"] = 0;
				$reportAgentsData[$key]["accountStatement"]["overBanking"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"] = 0;
	
				// New values
				$reportAgentsData[$key]["accountStatement"]["totalBankTransAgentGainOrLoss"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAgentGainOrLoss"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalClearedChequeTransAgentGainOrLoss"] = 0;
	
				// Opening balance field.
				$openingBalData = selectFrom("select opening_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id='" . $agentData["userID"] . "' and dated='" . $startDate . "' ");
				if ( empty( $openingBalData ) )
				{
					// If opening balance is not available on the specified date, we will fetch the last available closing balance.
					$lastAvailableClosingBalData = selectFrom("select closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id='" . $agentData["userID"] . "' and dated<='" . $startDate . "' order by dated desc limit 0, 1");
					$reportAgentsData[$key]["accountStatement"]["openingBal"] = $lastAvailableClosingBalData["closing_balance"];
				}
				else
				{
					// It means that the opening balance is available on the specified date.
					$reportAgentsData[$key]["accountStatement"]["openingBal"] = $openingBalData["opening_balance"];
				}
	
				
				/********************************************************************************
				*  Banking Total. (MIXED, CASH, CHEQUE)
				**********************************************************************************/
				$sql = "select 
							sum(amount) as totalDepositAmount,
							count(*) as bkCount
						from 
							agent_account 
						where 
							agentID='" . $agentData["userID"] . "' 
							and type = 'DEPOSIT' 
							and dated between '" . $startDate . "' and '" . $endDate . "' 
							and TransID = ''
						";
						
				if(!CONFIG_EXCLUDE_MIXED_AND_CHEQUE_ON_AGENT_ACCOUNT)
				{
					if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT)
					{
						$sql .= " and ((modeOfPayment='MIXED' and chequeStatus='CLEARED') or modeOfPayment='CASH' or (modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED'))";
					} else {
						$sql .= " and ((modeOfPayment='MIXED' and chequeStatus='CLEARED') or modeOfPayment='CASH' or (modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED'))";
					}
				} else {
					$sql .= " and modeOfPayment = 'CASH'";
				}

				$bankTransferTransData = selectFrom($sql);
				$totalBankTransAmount = $bankTransferTransData["totalDepositAmount"];

				// Following section is fetching all records to affect the closing balance while
				// the above section is just to populate the records for display only.
				$sql = "select 
							sum(amount) as totalDepositAmount,
							count(*) as bkCount
						from 
							agent_account 
						where 
							agentID='" . $agentData["userID"] . "' 
							and type = 'DEPOSIT' 
							and dated between '" . $startDate . "' and '" . $endDate . "'
						";

				$sql .= " and ((modeOfPayment='MIXED' and chequeStatus='CLEARED') or modeOfPayment='CASH' or (modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED'))";
				$dummy_bankTransferTransData = selectFrom($sql);
				$dummy_totalBankTransAmount = $dummy_bankTransferTransData["totalDepositAmount"];

				// Section ends /////////////////////////////////////////////////////////////////
				
				if( !empty($bankTransferTransData) )
				{
					$reportAgentsData[$key]["accountStatement"]["bankTransferCount"] = $bankTransferTransData["bkCount"];
					$reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"] = is_numeric($totalBankTransAmount) ? $totalBankTransAmount : 0;
				}
				
				/****************************************************************
				* Message Total field - Total. (All authorized transactions)
				*****************************************************************/
				$amountColumn = "transAmount";
				
				if(CONFIG_AGENT_PAYMENT_MODE == "1")
				{
					if($agentData["paymentMode"] == "exclude")
					{
						$amountColumn = "transAmount";
					} else {
						$amountColumn = "totalAmount";
					}
				}
				
				$authorizedTransData = selectMultiRecords("select 
																transID, 
																".$amountColumn." 
															from " . TBL_TRANSACTIONS . " 
															where custAgentID='" . $agentData["userID"] . "' 
																and transStatus in('Authorize', 'Amended', 'Delivered', 'Picked up', 'Credited', 'AwaitingCancellation') 
																and transDate between '" . $startDate . " 00:00:00' 
																and '" . $endDate . " 23:59:59'");
	
				if ( !empty( $authorizedTransData ) )
				{
					$totalAuthorizeTransAmount = 0;
					$netGainOrLoss = 0;
					foreach($authorizedTransData as $authorizeTrans)
					{
						$totalAuthorizeTransAmount += $authorizeTrans[$amountColumn];
						$accountEntry = selectMultiRecords( "select type, amount from agent_account where transID = '" . $authorizeTrans["transID"] . "'" );
						if ( count($accountEntry) > 1 )
						{
							if ( strtoupper($accountEntry[0]["type"]) == strtoupper("Deposit") )
								$netGainOrLoss += $accountEntry[0]["amount"];
							else
								$netGainOrLoss -= $accountEntry[0]["amount"];
						}
					}
	
					$authorizedTransData["totalAuthorizeTransAmount"] = $totalAuthorizeTransAmount;
					$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAgentGainOrLoss"] = $netGainOrLoss;
					$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] = is_numeric($authorizedTransData["totalAuthorizeTransAmount"]) ? $authorizedTransData["totalAuthorizeTransAmount"] : 0;
				}
	
				/********************************************************
				** Closing balance field formula.
				********************************************************/
				/**
				$reportAgentsData[$key]["accountStatement"]["closingBal"] = ( $reportAgentsData[$key]["accountStatement"]["openingBal"]
																		   + $reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"]
																		   - $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] );
				**/
				$reportAgentsData[$key]["accountStatement"]["closingBal"] = ( $reportAgentsData[$key]["accountStatement"]["openingBal"]
																		   + $dummy_totalBankTransAmount
																		   - $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] );
																		   
				//$dummy_bankTransferTransData = selectFrom($sql);
				//$dummy_totalBankTransAmount = $dummy_bankTransferTransData["totalDepositAmount"];
				/*******************************************************
				** Cash balance and Over banking fields.
				********************************************************/
				if ( $reportAgentsData[$key]["accountStatement"]["closingBal"] > 0 )
					$reportAgentsData[$key]["accountStatement"]["cashBalance"] = $reportAgentsData[$key]["accountStatement"]["closingBal"];
				else
					$reportAgentsData[$key]["accountStatement"]["overBanking"] = $reportAgentsData[$key]["accountStatement"]["closingBal"];
	
				/*************************************************************************
				** Mixed banking field - Total. (MIXED) - cheque is conditional
				*************************************************************************/
				$clearedSql = "select 
									sum(amount) as totalAmount
								from 
									agent_account 
								where 
									agentID='" . $agentData["userID"] . "' 
									and type = 'DEPOSIT' 
									and dated <= '".$endDate."'
								"; //and dated between '" . $startDate . "' and '" . $endDate . "' 
				
				if(CONFIG_INCLUDE_CHEQUES_IN_MIXED_ON_AGENT_ACCOUNT)
				{
					$clearedSql .= " and (";
					
					if(!CONFIG_INCLUDE_CLEARED_MIXED_ON_AGENT_ACCOUNT)
					{
						$clearedSql .= "(modeOfPayment='MIXED' and chequeStatus='UNCLEARED')";
					} else {
						$clearedSql .= "modeOfPayment='MIXED'";
					}
					
					$clearedSql .= " or ";
					
					if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT)
					{
						$clearedSql .= "(modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED')";
					} else {
						$clearedSql .= "(modeOfPayment='CHEQUE' and chequeStatus = 'UNCLEARED')";
					}
					
					$clearedSql .= ")";
				} else {
					$clearedSql .= " and modeOfPayment in('MIXED')";
					
					if(!CONFIG_INCLUDE_CLEARED_MIXED_ON_AGENT_ACCOUNT)
					{
						$clearedSql .= " and chequeStatus = 'UNCLEARED'";
					}
				}
				
				$clearedChequeTransData = selectMultiRecords($clearedSql);
				
				if ( !empty($clearedChequeTransData) )
				{
					$totalClearedChequeTransAmount = $clearedChequeTransData[0]["totalAmount"];
					$clearedChequeTransData["totalClearedChequeTransAmount"] = $totalClearedChequeTransAmount;
					$reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"] = is_numeric($clearedChequeTransData["totalClearedChequeTransAmount"]) ? $clearedChequeTransData["totalClearedChequeTransAmount"] : 0;
				}
	
				/***********************************************************************************
				** Cheques to be posted field - Total. (Cheque amount of unauthorized transactions.
				***********************************************************************************/
				$unclearedMixedSql = "select 
											sum(amount) as totalUnclearedChequeAmount 
									  from 
									  		agent_account 
									  where 
									  		agentID='" . $agentData["userID"] . "' ";
				if(CONFIG_INCLUDE_UNCLEARED_MIXED_IN_CHEQUES_POSTED_ON_AGENT_ACCOUNT)
				{
					$unclearedMixedSql .= "and (modeOfPayment = 'CHEQUE' or modeOfPayment = 'MIXED')";
				} else {
					$unclearedMixedSql .= "and modeOfPayment = 'CHEQUE'";
				}
				
				$unclearedMixedSql .= " and chequeStatus = 'UNCLEARED' and dated <= '".$endDate."'"; //and dated between '" . $startDate . "' and '" . $endDate . "'"
				$unclearedChequeTransData = selectFrom($unclearedMixedSql);
				$reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"] = is_numeric($unclearedChequeTransData["totalUnclearedChequeAmount"]) ? $unclearedChequeTransData["totalUnclearedChequeAmount"] : 0;
				
				/*************************************************************************
				** Today's Total of Mixed Banking and Cheque
				*************************************************************************/
				$today = date("Y-m-d", time());
				$todaySql = "select 
									sum(amount) as totalAmount
								from 
									agent_account 
								where 
									agentID='" . $agentData["userID"] . "' 
									and type = 'DEPOSIT' 
									and dated = '" . $today . "' 
									and ( 
								";
				
				
				if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT_TODAY_TOTAL)
				{
					$todaySql .= "(modeOfPayment='MIXED' and chequeStatus='CLEARED') or (modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED')";
				} else {
					$todaySql .= "(modeOfPayment='MIXED' and chequeStatus='UNCLEARED') or (modeOfPayment='CHEQUE' and chequeStatus = 'UNCLEARED')";
				}
				
				$todaySql .= ")";
				$todayTotalMixedChequeData = selectMultiRecords($todaySql);
				
				if ( !empty($todayTotalMixedChequeData) )
				{
					$todayTotalMixedCheque = $todayTotalMixedChequeData[0]["totalAmount"];
					//$clearedChequeTransData["totalClearedChequeTransAmount"] = $totalClearedChequeTransAmount;
					$reportAgentsData[$key]["accountStatement"]["todayTotalOfMixedBankingAndCheque"] = is_numeric($todayTotalMixedCheque) ? $todayTotalMixedCheque : 0;
				}
	
				/***************************************************************
				* Calculation of totals.
				***************************************************************/
				$totals["openingBal"] += $reportAgentsData[$key]["accountStatement"]["openingBal"];
				$totals["bankTransferCount"] += $reportAgentsData[$key]["accountStatement"]["bankTransferCount"];
				$totals["totalBankTraferAmount"] += $reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"];
				$totals["totalAuthorizeTransAmount"] += $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"];
				$totals["closingBal"] += $reportAgentsData[$key]["accountStatement"]["closingBal"];
				$totals["cashBalance"] += $reportAgentsData[$key]["accountStatement"]["cashBalance"];
				$totals["overBanking"] += $reportAgentsData[$key]["accountStatement"]["overBanking"];
				$totals["totalClearedChequeAmount"] += $reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"];
				$totals["totalUnclearedChequeAmount"] += $reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"];
				$totals["todayTotalOfMixedBankingAndCheque"] += $reportAgentsData[$key]["accountStatement"]["todayTotalOfMixedBankingAndCheque"];
				
				// New values
				$totals["totalBankTransAgentGainOrLoss"] += $reportAgentsData[$key]["accountStatement"]["totalBankTransAgentGainOrLoss"];
				$totals["totalAuthorizeTransAgentGainOrLoss"] += $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAgentGainOrLoss"];
				$totals["totalClearedChequeTransAgentGainOrLoss"] += $reportAgentsData[$key]["accountStatement"]["totalClearedChequeTransAgentGainOrLoss"];
			}
		}
		else
		{
			// why this else part is here??????
			
			foreach( $reportAgentsData as $key=>$agentData )
			{
				$reportAgentsData[$key]["accountStatement"]["openingBal"] = 0;
				$reportAgentsData[$key]["accountStatement"]["bankTransferCount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["closingBal"] = 0;
				$reportAgentsData[$key]["accountStatement"]["cashBalance"] = 0;
				$reportAgentsData[$key]["accountStatement"]["overBanking"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"] = 0;
				$reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"] = 0;
	
				// Opening balance field.
				$openingBalData = selectFrom("select opening_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id='" . $agentData["userID"] . "' and dated='" . $startDate . "' ");
				if ( empty( $openingBalData ) )
				{
					// If opening balance is not available on the specified date, we will fetch the last available closing balance.
					$lastAvailableClosingBalData = selectFrom("select closing_balance from " . TBL_ACCOUNT_SUMMARY . " where user_id='" . $agentData["userID"] . "' and dated<='" . $startDate . "' order by dated desc limit 0, 1");
					$reportAgentsData[$key]["accountStatement"]["openingBal"] = $lastAvailableClosingBalData["closing_balance"];
				}
				else
				{
					// It means that the opening balance is available on the specified date.
					$reportAgentsData[$key]["accountStatement"]["openingBal"] = $openingBalData["opening_balance"];
				}
		
				/********************************************************************************
				*  Banking Total. (MIXED, CASH, CHEQUE)
				**********************************************************************************/
				$sql = "select 
							sum(amount) as totalDepositAmount,
							count(*) as bkCount
						from 
							agent_account 
						where 
							agentID='" . $agentData["userID"] . "' 
							and type = 'DEPOSIT' 
							and dated between '" . $startDate . "' and '" . $endDate . "'
						";
						
				if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT)
				{
					$sql .= " and (modeOfPayment='MIXED' or modeOfPayment='CASH' or (modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED'))";
				}

				$bankTransferTransData = selectFrom($sql);
				$totalBankTransAmount = $bankTransferTransData["totalDepositAmount"];
				
				if( !empty($bankTransferTransData) )
				{
					$reportAgentsData[$key]["accountStatement"]["bankTransferCount"] = $bankTransferTransData["bkCount"];
					$reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"] = is_numeric($totalBankTransAmount) ? $totalBankTransAmount : 0;
				}
				
				// Message Total field - Total. (All authorized transactions)
				$authorizedTransData = selectFrom("select sum(totalAmount) as totalAuthorizeTransAmount from " . TBL_TRANSACTIONS . " where custAgentID='" . $agentData["userID"] . "' and transStatus in('Authorize', 'Amended', 'Delivered', 'Picked up', 'Credited', 'AwaitingCancellation') and transDate between '" . $startDate . " 00:00:00' and '" . $endDate . " 23:59:59'");
	
				$reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] = is_numeric($authorizedTransData["totalAuthorizeTransAmount"]) ? $authorizedTransData["totalAuthorizeTransAmount"] : 0;
	
				// Closing balance field formula.
				$reportAgentsData[$key]["accountStatement"]["closingBal"] = ( $reportAgentsData[$key]["accountStatement"]["openingBal"]
																		   - $reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"]
																		   + $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"] );
	
				// Cash balance and Over banking fields.
				if ( $reportAgentsData[$key]["accountStatement"]["closingBal"] > 0 )
					$reportAgentsData[$key]["accountStatement"]["cashBalance"] = $reportAgentsData[$key]["accountStatement"]["closingBal"];
				else
					$reportAgentsData[$key]["accountStatement"]["overBanking"] = $reportAgentsData[$key]["accountStatement"]["closingBal"];
	
				/*************************************************************************
				** Mixed banking field - Total. (MIXED) - cheque is conditional
				*************************************************************************/
				$clearedSql = "select 
									sum(amount) as totalAmount
								from 
									agent_account 
								where 
									agentID='" . $agentData["userID"] . "' 
									and type = 'DEPOSIT' 
									and dated between '" . $startDate . "' and '" . $endDate . "' 
								";
				
				if(CONFIG_INCLUDE_CHEQUES_IN_MIXED_ON_AGENT_ACCOUNT)
				{
					$clearedSql .= " and (modeOfPayment='MIXED' or ";
					
					if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT)
					{
						$clearedSql .= "(modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED')";
					} else {
						$clearedSql .= "modeOfPayment='CHEQUE'";
					}
					
					$clearedSql .= ")";
				} else {
					$clearedSql .= " and modeOfPayment in('MIXED')";
				}
				
				$clearedChequeTransData = selectMultiRecords($clearedSql);
				
				if ( !empty($clearedChequeTransData) )
				{
					$totalClearedChequeTransAmount = $clearedChequeTransData[0]["totalAmount"];
					$clearedChequeTransData["totalClearedChequeTransAmount"] = $totalClearedChequeTransAmount;
					$reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"] = is_numeric($clearedChequeTransData["totalClearedChequeTransAmount"]) ? $clearedChequeTransData["totalClearedChequeTransAmount"] : 0;
				}
	
				/***********************************************************************************
				** Cheques to be posted field - Total. (Cheque amount of unauthorized transactions.
				***********************************************************************************/
				$unclearedChequeTransData = selectFrom("select sum(amount) as totalUnclearedChequeAmount from agent_account where agentID='" . $agentData["userID"] . "' and modeOfPayment='CHEQUE' and chequeStatus = 'UNCLEARED' and dated between '" . $startDate . "' and '" . $endDate . "'");
				$reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"] = is_numeric($unclearedChequeTransData["totalUnclearedChequeAmount"]) ? $unclearedChequeTransData["totalUnclearedChequeAmount"] : 0;
				
				/*************************************************************************
				** Today's Total of Mixed Banking and Cheque
				*************************************************************************/
				$today = date("Y-m-d", time());
				$todaySql = "select 
									sum(amount) as totalAmount
								from 
									agent_account 
								where 
									agentID='" . $agentData["userID"] . "' 
									and type = 'DEPOSIT' 
									and dated = '" . $today . "' 
									and (
								";
				
				if(!CONFIG_INCLUDE_UNCLEARED_CHEQUES_ON_AGENT_ACCOUNT_TODAY_TOTAL)
				{
					$todaySql .= "(modeOfPayment='CHEQUE' and chequeStatus = 'CLEARED')";
				} else {
					$todaySql .= "modeOfPayment='CHEQUE'";
				}
				
				$todaySql .= ")";
				
				$todayTotalMixedChequeData = selectMultiRecords($todaySql);
				
				if ( !empty($todayTotalMixedChequeData) )
				{
					$todayTotalMixedCheque = $todayTotalMixedChequeData[0]["totalAmount"];
					//$clearedChequeTransData["totalClearedChequeTransAmount"] = $totalClearedChequeTransAmount;
					$reportAgentsData[$key]["accountStatement"]["todayTotalOfMixedBankingAndCheque"] = is_numeric($todayTotalMixedCheque) ? $todayTotalMixedCheque : 0;
				}
				
				$totals["openingBal"] += $reportAgentsData[$key]["accountStatement"]["openingBal"];
				$totals["bankTransferCount"] += $reportAgentsData[$key]["accountStatement"]["bankTransferCount"];
				$totals["totalBankTraferAmount"] += $reportAgentsData[$key]["accountStatement"]["totalBankTraferAmount"];
				$totals["totalAuthorizeTransAmount"] += $reportAgentsData[$key]["accountStatement"]["totalAuthorizeTransAmount"];
				$totals["closingBal"] += $reportAgentsData[$key]["accountStatement"]["closingBal"];
				$totals["cashBalance"] += $reportAgentsData[$key]["accountStatement"]["cashBalance"];
				$totals["overBanking"] += $reportAgentsData[$key]["accountStatement"]["overBanking"];
				$totals["totalClearedChequeAmount"] += $reportAgentsData[$key]["accountStatement"]["totalClearedChequeAmount"];
				$totals["totalUnclearedChequeAmount"] += $reportAgentsData[$key]["accountStatement"]["totalUnclearedChequeAmount"];
				$totals["todayTotalOfMixedBankingAndCheque"] += $reportAgentsData[$key]["accountStatement"]["todayTotalOfMixedBankingAndCheque"];
			}
		}
	}

	$currentYear=date("Y");

	$selectedStartDay = !empty( $_REQUEST["startDay"] ) ? $_REQUEST["startDay"] : date("d");
	$selectedStartMonth = !empty( $_REQUEST["startMonth"] ) ? $_REQUEST["startMonth"] : date("m");
	$selectedStartYear = !empty( $_REQUEST["startYear"] ) ? $_REQUEST["startYear"] : date("Y");

	$selectedEndDay = !empty( $_REQUEST["endDay"] ) ? $_REQUEST["endDay"] : date("d");
	$selectedEndMonth = !empty( $_REQUEST["endMonth"] ) ? $_REQUEST["endMonth"] : date("m");
	$selectedEndYear = !empty( $_REQUEST["endYear"] ) ? $_REQUEST["endYear"] : date("Y");

	$allAgents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from " . TBL_ADMIN_USERS . " where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'N' order by agentCompany asc");
?>
<HTML>
<HEAD>
<TITLE>Agent Account Statement</TITLE>
<LINK REL="stylesheet" HREF="css/reports.css" TYPE="text/css">
<SCRIPT TYPE="text/javascript" SRC="jquery.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" SRC="javascript/jquery.selectboxutils.js"></SCRIPT>
<SCRIPT>

$(document).ready( function()
{
	<? // Initialize date picker for start date in search. ?>
	initDatePicker('startDay', 'startMonth', 'startYear', 
						'', '1:12', '2004:<?=$currentYear;?>', <?=($selectedStartDay - 1);?>, <?=($selectedStartMonth - 1);?>, <?=$selectedStartYear - 2004;?>);

	<? // Initialize date picker for end date in search. ?>
	initDatePicker('endDay', 'endMonth', 'endYear', 
									'', '1:12', '2004:<?=$currentYear;?>', <?=($selectedEndDay - 1);?>, <?=($selectedEndMonth - 1);?>, <?=$selectedEndYear - 2004;?>);

});

function printThisReport()
{
	window.open( "print.php?dataContainerId=reportContainer", "Print", "scrollbars=yes,toolbar=no,status=no,location=no,directories=no,width=850,height=550,top=50,left=200" );
}

</SCRIPT>
<SCRIPT LANGUAGE="javascript" SRC="./javascript/functions.js"></SCRIPT>
<LINK HREF="images/interface.css" REL="stylesheet" TYPE="text/css" />
<STYLE TYPE="text/css">
	<!--
	.style1 {color: #005b90}
	.style3 {
		color: #3366CC;
		font-weight: bold;
	}
	-->
</STYLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1"></HEAD>
<BODY>
<FORM ACTION="agent_account_statement_prime_currency.php" METHOD="get" NAME="Search">
	<TABLE ALIGN="center" BORDER="1">
		<TR>
			<TD ALIGN="center" STYLE="border:0px;">
				<H3>Select Filters </H3>
			</TD>
		</TR>
		<TR>
			<TD ALIGN="center" STYLE="border:0px;">
				

				<INPUT TYPE="hidden" NAME="searchRequest" VALUE="1" />
				
				From 
				<SELECT ID="startDay" NAME="startDay"></SELECT>
				<SELECT ID="startMonth" NAME="startMonth"></SELECT>
				<SELECT ID="startYear" NAME="startYear" ></SELECT>

                To 
				<SELECT ID="endDay" NAME="endDay"></SELECT>
				<SELECT ID="endMonth" NAME="endMonth"></SELECT>
				<SELECT ID="endYear" NAME="endYear" ></SELECT>

				<?
					if ( $agentType == "admin" )
					{
						?>
							&nbsp;&nbsp;Agents<SELECT NAME="agentIDsForReport[]" ID="agentIDsForReport">
								<OPTION VALUE="">All</OPTION>
								<?
									foreach( $allAgents as $agent )
									{
										$selected = "";
										if ( !empty( $_REQUEST["agentIDsForReport"] ) && in_array( $agent["userID"], $_REQUEST["agentIDsForReport"] )  )
										{
											$selected = "selected='selected'";
										}
										?>
											<OPTION VALUE="<?=$agent["userID"]?>" <?=$selected;?> ><?=$agent["agentCompany"] . " [" . $agent["username"] . "]";?></OPTION>
										<?
									}
								?>
							</SELECT>
						<?
					}
					else
					{
						?>
							<INPUT TYPE="hidden" NAME="agentIDsForReport[]" ID="agentIDsForReport" VALUE="<?=$_SESSION["loggedUserData"]["userID"];?>" />
						<?
					}
				?>
				&nbsp; <INPUT TYPE="submit" NAME="Submit" VALUE="Search">
			</TD>
		</TR>
	</TABLE>
</FORM>

<?
	if ( !empty( $reportAgentsData ) )
	{
		?>
			<TABLE WIDTH="95%" ALIGN="center" ID="reportContainer" CELLPADDING="5" CLASS="boundingTable">
				<TR>
					<TD>
						<TABLE CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
							<TR>
								<TD></TD>
								<TD COLSPAN="11">
								<H1 CLASS="reportHeader">Agents Working With <?=COMPANY_NAME?> - Account Balance</H1>								</TD></TR>
							<TR>
								<TD CLASS="columnTitle" WIDTH="5%">Sr. No</TD>
								<TD CLASS="columnTitle" WIDTH="10%">Agent Code</TD>
								<TD CLASS="columnTitle" WIDTH="15%">Agent Name</TD>
								<TD CLASS="columnTitle" WIDTH="12%" ALIGN="right">Opening Balance<BR />(<?=$startDate;?>)</TD>
								<TD CLASS="columnTitle" WIDTH="8%" ALIGN="right">BK CNT</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Banking<BR />Total</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Messages<BR />Total</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Closing Balance<BR />(<?=$endDate;?>)</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Over Banking</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Cash Balance</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Mixed Banking</TD>
								<TD CLASS="columnTitle" WIDTH="10%" ALIGN="right">Cheque To Be Posted</TD>
							</TR>
							<?
								$i = 0;
								foreach( $reportAgentsData as $agentData )
								{
									$i++;
									?>
										<TR>
											<TD><?=$i;?></TD>
											<TD><?=$agentData["username"];?></TD>
											<TD><?=$agentData["agentContactPerson"];?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["openingBal"], $numberOfDecimal);?></TD>
											<TD ALIGN="right"><?=$agentData["accountStatement"]["bankTransferCount"];?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["totalBankTraferAmount"], $numberOfDecimal);?></TD>
											<TD ALIGN="right">
											<?
												if ( $agentType == "admin" )
												{ 
													//echo number_format($agentData["accountStatement"]["totalAuthorizeTransAmount"] - $agentData["accountStatement"]["totalAuthorizeTransAgentGainOrLoss"], $numberOfDecimal);
													echo number_format($agentData["accountStatement"]["totalAuthorizeTransAmount"], $numberOfDecimal);
												}
												else
												{
													echo number_format($agentData["accountStatement"]["totalAuthorizeTransAmount"], $numberOfDecimal);
												}
											?>											</TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["closingBal"], $numberOfDecimal);?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["cashBalance"], $numberOfDecimal);?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["overBanking"], $numberOfDecimal);?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["totalClearedChequeAmount"], $numberOfDecimal);?></TD>
											<TD ALIGN="right"><?=number_format($agentData["accountStatement"]["totalUnclearedChequeAmount"], $numberOfDecimal);?></TD>
										</TR>
									<?
								}
								
								//$totalMixedBanking = number_format($totals["totalClearedChequeAmount"] + $totals["totalUnclearedChequeAmount"], $numberOfDecimal);
								$totalMixedBanking = number_format($totals["totalClearedChequeAmount"] , $numberOfDecimal);
							?>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD CLASS="reportSubHeader">Total Of Mixed Banking And Cheque</TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right">
									<?=number_format($totals["todayTotalOfMixedBankingAndCheque"], $numberOfDecimal)?>
									<? //=$totalMixedBanking;?>								</TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD CLASS="reportSubHeader">Total</TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["openingBal"], $numberOfDecimal);?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=$totals["bankTransferCount"];?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["totalBankTraferAmount"] + $totals["todayTotalOfMixedBankingAndCheque"], $numberOfDecimal);//+ $totalMixedBanking?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right">
									<?
										if ($agentType == "admin")
										{
											 //echo number_format($totals["totalAuthorizeTransAmount"] - $totals["totalAuthorizeTransAgentGainOrLoss"], $numberOfDecimal);
											 echo number_format($totals["totalAuthorizeTransAmount"], $numberOfDecimal);
										}
										else
										{
											echo number_format($totals["totalAuthorizeTransAmount"], $numberOfDecimal);
										}
									?>								</TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["closingBal"], $numberOfDecimal);?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["cashBalance"], $numberOfDecimal);?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["overBanking"], $numberOfDecimal);?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["totalClearedChequeAmount"], $numberOfDecimal);?></TD>
								<TD CLASS="reportSubHeader" ALIGN="right"><?=number_format($totals["totalUnclearedChequeAmount"], $numberOfDecimal);?></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<TABLE ALIGN="center" WIDTH="90%">
				<TR>
					<TD ALIGN="center">
						<INPUT TYPE="button" VALUE="Print Report" onClick="printThisReport();" />
					</TD>
				</TR>
			</TABLE>
		<?
	}
?>

</BODY>
</HTML>