<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

$totalAmount="";
$foriegnTotal="";

session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
session_register("agentName");
session_register("grandTotal");
session_register("CurrentTotal");
session_register("bankID");
session_register("sendingCurrency");
session_register("receivingCurrency");


if(CONFIG_TRANS_ROUND_LEVEL > 0)
{
	$roundLevel = CONFIG_TRANS_ROUND_LEVEL;
}else{
	$roundLevel = 4;
	
}



if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " t.transDate";

$agentType = getAgentType();
	if($_POST["country"]!="")
		$country = $_POST["country"];
		elseif($_GET["country"]!="") 
			$country = $_GET["country"];
		
	if($_POST["currency"]!="")
		$currency = $_POST["currency"];
		elseif($_GET["currency"]!="") 
			$currency = $_GET["currency"];	
			
	if($_POST["sendingCurrency"]!="")
		$sendingCurrency = $_POST["sendingCurrency"];
		elseif($_GET["sendingCurrency"]!="") 
			$sendingCurrency = $_GET["sendingCurrency"];		
	
	if($_POST["receivingCurrency"]!="")
		$receivingCurrency = $_POST["receivingCurrency"];
		elseif($_GET["receivingCurrency"]!="") 
			$receivingCurrency = $_GET["receivingCurrency"];


if($_POST["Submit"] =="Search"||$_GET["search"]=="search")
{

	/*if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager")
	{
		$query = "select transStatus, transDate, transID, refNumberIM, totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, customerID, benID, exchangeRate, localAmount, exchangeID, benAgentID, collectionPointID, addedBy from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
		$queryCnt = "select transStatus, transDate, transID, refNumberIM , totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, customerID, benID, exchangeRate, localAmount, benAgentID, exchangeID, collectionPointID, addedBy from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID" ;
		
		if($agentType == "Branch Manager")
		{
			$query .= " and a.parentID = '$parentID'";								
			$queryCnt .= " and a.parentID = '$parentID'";								
		} 	
	}
	else
	{
		$query = "select transStatus, transDate, transID, refNumberIM, totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, customerID, benID, exchangeRate, exchangeID, localAmount, benAgentID, collectionPointID, addedBy from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;
		$queryCnt = "select transStatus, transDate, transID, refNumberIM, totalAmount, transAmount, name, IMFee,refNumber, commPackage, agentCommission, AgentComm, CommType, customerID, benID, exchangeRate, exchangeID , localAmount, benAgentID, collectionPointID, addedBy  from ". TBL_TRANSACTIONS . " as t,
		" . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.userID='".$_SESSION["loggedUserData"]["userID"]."'" ;
	}*/
	if(CONFIG_EXCH_RATE_USED == '1'){
					$query = "select *  from ". TBL_TRANSACTIONS . " as t,".TBL_EXCH_RATE_USED." as e, ".TBL_ADMIN_USERS." as a where 1 and t.transID = e.transID" ;
					$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t,".TBL_EXCH_RATE_USED." as e , ".TBL_ADMIN_USERS." as a where 1 and t.transID = e.transID" ;
		}else{
			if($sendingCurrency =="" && $receivingCurrency =="" ){
			$query = "select  *  from ". TBL_TRANSACTIONS . " as t where 1" ;
		  $queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t where 1" ;
			}else{
					
					$query = "select *  from ". TBL_TRANSACTIONS . " as t,".TBL_EXCHANGE_RATES." as e, ".TBL_ADMIN_USERS." as a where 1 and t.exchangeID=e.erID" ;
					$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " as t,".TBL_EXCHANGE_RATES." as e , ".TBL_ADMIN_USERS." as a where 1 and t.exchangeID=e.erID" ;
				//	$queryCnt = "select count(*)  from ". TBL_TRANSACTIONS . " where 1" ;
			}
	}

//if(CONFIG_AGENT_FILTER == "1"){
			$query .= "  and t.custAgentId = a.userId ";
			$queryCnt .= "  and t.custAgentId = a.userId ";
	
	//}
	if($_GET["search"]!="search")
	{		
		$_SESSION["fMonth"]="";
		$_SESSION["fDay"]="";
		$_SESSION["fYear"]="";
		
		$_SESSION["tMonth"]="";
		$_SESSION["tDay"]="";
		$_SESSION["tYear"]="";
		
		$_SESSION["agentName"]="";
		$_SESSION["bankID"]="";
		
		$_SESSION["grandTotal"]="";
		$_SESSION["CurrentTotal"]="";	

		$_SESSION["fMonth"]=$_POST["fMonth"];
		$_SESSION["fDay"]=$_POST["fDay"];
		$_SESSION["fYear"]=$_POST["fYear"];
		
		$_SESSION["tMonth"]=$_POST["tMonth"];
		$_SESSION["tDay"]=$_POST["tDay"];
		$_SESSION["tYear"]=$_POST["tYear"];
		
		$_SESSION["agentName"]=$_POST["agentName"];
		$_SESSION["bankID"]=$_POST["bankID"];
		$_SESSION["receivingCurrency"]=$_POST["receivingCurrency"];
		$_SESSION["sendingCurrency"]=$_POST["sendingCurrency"];
	}
	
			
	
	$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
	$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
	
 $queryDate = "(t.transDate >= '$fromDate 00:00:00' and t.transDate <= '$toDate 23:59:59')";
	
	/*if( $_SESSION["tYear"]!="" && $fromDate < $toDate)
	{*/
	$query .=  " and $queryDate";				
	$queryCnt .=  " and $queryDate";
	//}				
if(CONFIG_AGENT_FILTER == "1"){	
	if($_POST["agentName"] != "")
	{
		if($_POST["agentName"]!="all")
		{
			$query .= " and a.userID='".$_POST["agentName"]."' ";
			$queryCnt .= " and a.userID='".$_POST["agentName"]."' ";
		}
	}
}
	
	/*
	if($_POST["bankID"] != "")
	{
		if($_POST["bankID"]!="all")
		{
			$query .= " and t.benAgentID='".$_POST["bankID"]."' ";
			$queryCnt .= " and t.benAgentID='".$_POST["bankID"]."' ";
		}
	}
	if($_SESSION["bankID"]!="")
	{
		if($_SESSION["bankID"]!="all")
		{
			$query .= " and t.benAgentID='".$_SESSION["bankID"]."' ";
			$queryCnt .= " and t.benAgentID='".$_SESSION["bankID"]."' ";
		}
	}*/
	if(CONFIG_EXCH_RATE_USED == '1')
	{
			if($sendingCurrency!= ""){
				$query .= " and (e.currencyFrom='".$sendingCurrency."')" ;
				$queryCnt .= " and (e.currencyFrom='".$sendingCurrency."')" ;
				}
			if($receivingCurrency!= ""){
				$query .= " and (e.currencyTo ='".$receivingCurrency."')" ;
				$queryCnt .= " and (e.currencyTo ='".$receivingCurrency."')";
				}
	}else{
				
			if($sendingCurrency!= ""){
				$query .= " and (e.currencyOrigin='".$sendingCurrency."')" ;
				$queryCnt .= " and (e.currencyOrigin='".$sendingCurrency."')" ;
				}
			if($receivingCurrency!= ""){
				$query .= " and (e.currency='".$receivingCurrency."')" ;
				$queryCnt .= " and (e.currency='".$receivingCurrency."')";
				}
			}
if (CONFIG_CNTRY_CURR_FILTER == "1") {  
	if($country != "") {
		$query .= " and (t.fromCountry='".$country."')" ;
		$queryCnt .= " and (t.fromCountry='".$country."')" ;
	}
	
	if($currency != "") {
		$query .= " and (t.currencyFrom='".$currency."')" ;
		$queryCnt .= " and (t.currencyFrom='".$currency."')" ;
	}
}
$query .= "  order by t.transDate DESC";
$query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);
$allCount = countRecords($queryCnt);	
}
else
{
	$todate = date("Y-m-d");
	//$query .= " and t.transStatus='Confirmed'";
}


//echo $query;
?>
<html>
<head>
	<title>Exchange Rate Earning Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="javascript/jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
$(document).ready(function(){	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#searchTable").hide();
		$("#pagination").hide();
		$("#actionBtn").hide();
		print();
		$("#searchTable").show();
		$("#pagination").show();
		$("#actionBtn").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function IsAllSpaces(myStr){
	while (myStr.substring(0,1) == " "){
			myStr = myStr.substring(1, myStr.length);
	}
	if (myStr == ""){
			return true;
	}
	return false;
}

function checkForm(search) {

     if (Search.sendingCurrency.value == "" || IsAllSpaces(Search.sendingCurrency.value)){
        alert("Please Select Sending Currency");	
        //search.sendingCurrency.focus();
        return false;
        }	

    if (Search.receivingCurrency.value == "" || IsAllSpaces(Search.receivingCurrency.value)){
      alert("Please Select Receiving Currency");	
     // Search.receivingCurrency.focus();
      return false;
      }
     	return true;
}	
</script>
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">Exchange Rate Earning Report</td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666" id="searchTable">
        <form action="company_profit.php" method="post" name="Search" onSubmit="return checkForm(this);" enctype="multipart/form-data" >
      <tr>
            <td width="463" nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From 
		<? 
		$month = date("m");
		
		$day = date("d");
		$year = date("Y");
		?>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
      <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
      <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
      <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
      <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
      <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
      <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
      <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
      <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
      <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
      <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
      <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
      <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
    </SELECT>
        <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$_SESSION["fMonth"]; ?>");
        </script>
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$_SESSION["fDay"]; ?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT> 
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$_SESSION["fYear"]; ?>");
        </script>
        To	<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$_SESSION["tMonth"]; ?>");
        </script>
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$_SESSION["tDay"]; ?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$_SESSION["tYear"]; ?>");
        </script>
        
        <!-- Added by Niaz against ticket# 2091 at 03-10-2007-->
        
        <br><br> Sending Currency	<? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1") { echo "<font color=#ff0000>*</font>"; }?>
         <select name="sendingCurrency" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Currency - </option> 
				<?
					$queryCurrency = "select distinct(currencyOrigin) from ".TBL_EXCHANGE_RATES." where 1 and currencyOrigin != '' ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyOrigin']?>"><?=$currencyData[$k]['currencyOrigin']?></option>	
					<? 
					}
				?>
		</SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.sendingCurrency, "<?=$_SESSION["sendingCurrency"]; ?>");
        </script>
			
			 Receiving Currency<? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1") { echo "<font color=#ff0000>*</font>"; }?>
         <select name="receivingCurrency" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Currency - </option> 
				<?
					$queryCurrency = "select distinct(currency) from ".TBL_EXCHANGE_RATES." where 1 and currency!= '' ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currency']?>"><?=$currencyData[$k]['currency']?></option>	
					<? 
					}
				?>
				<option value="PKR">PKR</option>
				</SELECT>
	<script language="JavaScript">
         	SelectOption(document.Search.receivingCurrency, "<?=$_SESSION["receivingCurrency"]; ?>");
        </script>
        
        
        
        <? 
      if(CONFIG_AGENT_FILTER == "1"){
        if($agentType == "admin" || $agentType == "Call" || $agentType == "Admin Manager" || $agentType == "Branch Manager"){?>
              <br> 
              Agent <select name="agentName" style="font-family:verdana; font-size: 11px;">
          <option value="">- Select Agent-</option>
		  <option value="all">All</option>
			<?
			
							$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
					
								if($agentType == "Branch Manager")
								{
									$agentQuery .= " and parentID = '$parentID'";				
									
								}
								$agentQuery .= "order by agentCompany";
				$agents = selectMultiRecords($agentQuery);
				for ($i=0; $i < count($agents); $i++){
			?>
			<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
			<?
				}
			?>
		  </select>
		  <script language="JavaScript">
		SelectOption(document.Search.agentName, "<?=$_SESSION["agentName"]; ?>");
			</script>
			<? }} ?>
			
			<?	if (CONFIG_CNTRY_CURR_FILTER == "1") {  ?>
        <br><br> Originating Country	
         <select name="country" style="font-family:verdana; font-size: 11px; width:130">
				<option value=""> - Select Country - </option> 
				<?
					$queryCountry = "select distinct(countryName) from ".TBL_COUNTRY." where 1 and countryType like '%origin%' ";
					$countryData = selectMultiRecords($queryCountry);
					for($k = 0; $k < count($countryData); $k++)
					{?>
						 <option value="<?=$countryData[$k]['countryName']?>"><?=$countryData[$k]['countryName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].country, "<?=$country?>");
			</script>
		&nbsp;&nbsp; Originating Currency
		<select name="currency" style="font-family:verdana; font-size: 11px; width:130">
				<option value="">-Select Currency-</option> 
				<?
					$queryCurrency = "select distinct(currencyName) from ".TBL_CURRENCY." where 1 ";
					$currencyData = selectMultiRecords($queryCurrency);
					for($k = 0; $k < count($currencyData); $k++)
					{?>
						 <option value="<?=$currencyData[$k]['currencyName']?>"><?=$currencyData[$k]['currencyName']?></option>	
					<? 
					}
				?>
		</select>
		<script language="JavaScript">SelectOption(document.forms[0].currency, "<?=$currency?>");
			</script>
		
		&nbsp;&nbsp;
			<?	}  ?>
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="100%" border="1" cellpadding="0" bordercolor="#666666">
        <form action="company_profit.php" method="post" name="trans">


          <?
			if ($allCount > 0){
		?>
          <tr>
            <td width="833"  bgcolor="#000000">
			<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF" id="pagination">
                <tr>
                  <td>
                    <?php if (count($contentsTrans) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&sendingCurrency=$sendingCurrency&receivingCurrency=$receivingCurrency&country=$country&currency=$currency&search=search";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&sendingCurrency=$sendingCurrency&receivingCurrency=$receivingCurrency&country=$country&currency=$currency&search=search"?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&sendingCurrency=$sendingCurrency&receivingCurrency=$receivingCurrency&country=$country&currency=$currency&search=search";?>"><font color="#005b90">Next</font></a>
                  </td>
                  <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last&sendingCurrency=$sendingCurrency&receivingCurrency=$receivingCurrency&country=$country&currency=$currency&search=search";?>"<font color="#005b90">Last</font></a>
                  </td>
                  <?php }
				  } ?>
                </tr>
              </table>
	    <tr>
            <td>&nbsp;</td>
        </tr>
		 
		 
		<?
		if(count($contentsTrans) > 0)
		{
		
		$allAmount=0;
				$allForiegn=0;
				
				for($k=0;$k < $allCount;$k++)
				{
					$allAmount += $contentsTrans2[$k]["transAmount"];
					$allForiegn += $contentsTrans2[$k]["localAmount"];
		
				}
		?>
      <tr>
            <td height="25" bgcolor="#C0C0C0" bordercolor="#000000"><span class="tab-u"><strong>&nbsp;Report 
              Date - <? if($_SESSION["fMonth"]!="" && $fromDate <= $toDate){echo "From ".$_SESSION["fMonth"]."-".$_SESSION["fDay"]."-".$_SESSION["fYear"]." To ".$_SESSION["tMonth"]."-".$_SESSION["tDay"]."-".$_SESSION["tYear"];}?></strong></span></td>
        </tr>
        <tr>
            <td height="25" nowrap bgcolor="#C0C0C0" align="center"><span class="tab-u"><strong> Exchange Rate Earning Report </strong></span></td>
        </tr>                      
        
        <tr>
          <td nowrap bgcolor="#EFEFEF">
		  <table border="0" bordercolor="#EFEFEF" width="100%">
			 
			<tr bgcolor="#FFFFFF">
				<td><span class="style1">Date </span></td>	
			  <td><span class="style1"><? echo $systemCode;?></span></td>
			  <td><span class="style1"><? echo $manualCode;?></span></td>			  
			  <td><span class="style1">Amount Sent</span></td>
			  <td><span class="style1">Exchange Rate</span></td>
			  <td><span class="style1">Foriegn Amount</span></td>
			 <? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1"){ ?>
			  <td><span class="style1">Agent Commision</span></td>
			  <td><span class="style1">Distributor Commission</span></td>
			  <td><span class="style1">Surcharges</span></td>
			  <td><span class="style1">Company Commission</span></td>
			 <? } ?>
			  <td><span class="style1">Net Gain (For Sending Currencies)</span></td>
			  <td><span class="style1">Net Foreign Gain (For Receiving Currencies)</span></td>
			  <td><span class="style1">Created By </span></td>
			  </tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				?>
				
				<tr bgcolor="#FFFFFF">
					<td bgcolor="#FFFFFF"><span class="style1"><? echo  dateFormat($contentsTrans[$i]["transDate"], "2")?></span></td>
				   <td><span class="style1"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"];?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo $contentsTrans[$i]["refNumberIM"];?></font></strong></a></span></td>
				   <td bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  				  
				  <td bgcolor="#FFFFFF">
				  <? 
				  echo number_format($contentsTrans[$i]["transAmount"],2,'.',',');
				  
				  $totalAmount += $contentsTrans[$i]["transAmount"];
				 
				  ?></td>
				  <? 
				  if(CONFIG_EXCH_RATE_USED == '1')
				  {
				  	$exchangeRate = $contentsTrans[$i]["rateWithoutMargin"];
				  }else{
					  $contentExchange = selectFrom("select primaryExchange from " . TBL_EXCHANGE_RATES . " where erID ='".$contentsTrans[$i]["exchangeID"]."'");
					  
					  $exchangeRate = $contentExchange ["primaryExchange"];
					}
				 $actualAmount = $exchangeRate * $contentsTrans[$i]["transAmount"];
				  $netGainLocal = $actualAmount - $contentsTrans[$i]["localAmount"];
				  $netGainLocalTotal +=	 $netGainLocal;
				  $netGain =  $netGainLocal / $exchangeRate;
				   $netGain = round($netGain,$roundLevel);
				  $netGainTotal += $netGain;				  
				 ?>
				 
			  <td><? echo number_format($contentsTrans[$i]["exchangeRate"],2,'.',',');?></td>
			  <td><? echo number_format($contentsTrans[$i]["localAmount"],2,'.',',');
			  $foriegnTotal += $contentsTrans[$i]["localAmount"];
			  ?></td>	
			   <? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1"){ 
			   	$totalAgentComm+=$contentsTrans[$i]["AgentComm"];
			   	$totalDistributorComm+=$contentsTrans[$i]["distributorComm"];
			   	 $companyComm = $contentsTrans[$i]["IMFee"] - ($contentsTrans[$i]["AgentComm"] + $contentsTrans[$i]["distributorComm"]);
			   	 $totalCompanyComm+= $contentsTrans[$i]["IMFee"] - ($contentsTrans[$i]["AgentComm"] + $contentsTrans[$i]["distributorComm"]);
			   	 $surcharge=$contentsTrans[$i]["admincharges"]+$contentsTrans[$i]["bankCharges"]+$contentsTrans[$i]["outCurrCharges"]+$contentsTrans[$i]["cashCharges"];
			   	 $totalSurcharge+=$contentsTrans[$i]["admincharges"]+$contentsTrans[$i]["bankCharges"]+$contentsTrans[$i]["outCurrCharges"]+$contentsTrans[$i]["cashCharges"];
			   	?>
			     	 
			   <td><? echo number_format($contentsTrans[$i]["AgentComm"],2,'.',',');?></td>
			   <td><? echo number_format($contentsTrans[$i]["distributorComm"],2,'.',',');?></td>
			   <td><? echo number_format($surcharge,2,'.',',');?></td>
			   <td><? echo number_format($companyComm,2,'.',',');?></td>
			   
			   <? } ?>
			  		 
			  <td><? echo $netGain." ".getCurrencySign($contentsTrans[$i]["currencyFrom"]);
			  			$currencyFrom = $contentsTrans[$i]["currencyFrom"];
			  	
			  	?></td>			  
			  <td><? echo number_format($netGainLocal,2,'.',',')." ".getCurrencySign($contentsTrans[$i]["currencyTo"]);
			  				$currencyTo = $contentsTrans[$i]["currencyTo"];
			  	?></td>			  
			  <td bgcolor="#FFFFFF"><? if($contentsTrans[$i]["addedBy"]=="") {?> Customer <? } else { echo $contentsTrans[$i]["addedBy"]; }?></td>				  
		    </tr>
				<?
			}
			?>
				

          <tr bordercolor="#000000" bgcolor="#999999">
			    <td><span class="style3">Total:</span></td>
			    <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  
				  
				  <td><strong><? echo number_format($totalAmount,2,'.',',');?></strong></td>
				 <td>&nbsp;</td> 
				  <td><? echo number_format($foriegnTotal,2,'.',',');?></td>
				   <? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1"){ ?>
				     <td><? echo number_format($totalAgentComm,2,'.',',');?></td>
				     <td><? echo number_format($totalDistributorComm,2,'.',',');?></td>
				     <td><? echo number_format($totalSurcharge,2,'.',',');?></td>
				       <td><? echo number_format($totalCompanyComm,2,'.',',');?></td>
				   <? } ?>
				 
				 
				  <td><? echo round($netGainTotal,$roundLevel)."  ".getCurrencySign($currencyFrom);?></td>
				  
				  <td><? echo $netGainLocalTotal ." ".getCurrencySign($currencyTo);?></td>
				 
				  <td>&nbsp;</td>			 
			</tr>
			<tr bordercolor="#000000" bgcolor="#999999">
				  <td class="style3" align="left"> Running Profit:</td>
				  	<td>&nbsp;</td>
			  <td><span class="style3">&nbsp;</span></td>
			  <td>&nbsp; </td>
			  <td>&nbsp;</td>
			  <? if(CONFIG_COMPULSARY_FIELDS_COLUMNS == "1"){ ?>
				     <td>&nbsp;</td>
				     <td>&nbsp;</td>
				     <td>&nbsp;</td>
				     <td>&nbsp;</td>
				   <? } ?>
				<td>&nbsp; </td>
				<td>
				<?
				if($_GET["total"]=="first")
				{
				$_SESSION["grandTotal"] = $netGainTotal;
				$_SESSION["CurrentTotal"]=$netGainTotal;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["grandTotal"] -= $_SESSION["CurrentTotal"];			
					$_SESSION["CurrentTotal"]=$netGainTotal;
				}elseif($_GET["total"]=="next"){
					$_SESSION["grandTotal"] += $netGainTotal;
					$_SESSION["CurrentTotal"]= $netGainTotal;
				}else
					{
					$_SESSION["grandTotal"] = $netGainTotal;
					$_SESSION["CurrentTotal"]=$_SESSION["grandTotal"];
					}
				echo number_format($_SESSION["grandTotal"],2,'.',',');
				?>
				</td>
			
			      <td><span class="style3">&nbsp;</span></td>
			  
			  
			  <td>&nbsp;</td>
			</tr>
		  </table></td>
        </tr>
		<tr id="actionBtn">
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" id="printReport">
				</div>
			</td>
			
		</tr>
		<?
			} else{ // greater than zero
			?>
			<tr>
				<td bgcolor="#FFFFCC"  align="center">
				<font color="#FF0000">No data to view for Selected Filter. Select Proper Filters shown Above </font>
				</td>
			</tr>
			<? }?>
		</form>
      </table></td>
  </tr>
</table>
</body>
</html>