<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$countryBasedFlag = false;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
$_SESSION["adminType"]=$_POST["adminType"];

/*	#5441 - AMB Exchange
	Operator ID is added in Admin Staff form.
	to be displayed in Western Union Output export file.
	by Niaz Ahmad
*/
$operatorFlag = false;
if(CONFIG_OPERATOR_ID_AnD=="1")
	$operatorFlag = true;

/*	#5957 - Premier Exchange
	Branch Manager use as Account Manager.
	by Niaz Ahmad
*/
$ACMFlag = false;
if(CONFIG_CUSTOMER_CATEGORY == '1')
	$ACMFlag = true;

if ($_GET["act"] == "addAdmin") {
	$_SESSION["loginName"] = "";
	$_SESSION["password1"] = "";
	$_SESSION["password2"] = "";
	$_SESSION["name"] = "";
	$_SESSION["rights"] = "";
	$_SESSION["email"] = "";
	$_SESSION["adminType"] = "";
	$_SESSION["agentStatus"] = "";
	$_SESSION["custCountries"] = "";
	$_SESSION["IDAcountry"] = "";
	$_SESSION["accessFromIP"] = "";	
	$_SESSION["linkedAgent"] ="";
	$_SESSION["linkedDistributor"] = "";
	$_SESSION["limitTransAmount"] = "";
	$_SESSION["limitChequeAmount"] = "";
	$_SESSION["limitCurrExchange"] = "";
	$_SESSION["limitDailyExpense"] = "";
if($operatorFlag)
	$_SESSION["OperatorID"] = "";
	
}
?>
<html>
<head>
	<title>Add Admin Staff</title>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript" src="jquery.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
	
	
function disableRights(theForm) 
{ 
	var rad_val = '';	
	for (var i=0; i < theForm.adminType.length; i++)
   {
   if (theForm.adminType[i].checked)
      {
      	rad_val = theForm.adminType[i].value;
      }
   }
   
  <?
  if(CONFIG_VERIFY_TRANSACTION_ENABLED) 
  {?> 
	 	for(i=0;i<7;i++)
	<?
	}else{ ?> 
	 for(i=0;i<6;i++)
	<?
	}
	?>   
	if(rad_val == "Admin" || rad_val == "MLRO")
	{
   	{
      theForm.elements[i+14].disabled = false;
		} 
	}else{
	 	{
      theForm.elements[i+14].disabled = true;
		} 
	}
} 

	function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	var flag = 0;
	if(theForm.loginName.value == "" || IsAllSpaces(theForm.loginName.value)){
    	alert("Please provide login name for the admin.");
        theForm.loginName.focus();
        return false;
    }
	if(theForm.password1.value == "" || IsAllSpaces(theForm.password1.value)){
    	alert("Please provide the admin's password.");
        theForm.password1.focus();
        return false;
    }
	if(theForm.password2.value == "" || IsAllSpaces(theForm.password2.value)){
    	alert("Please provide the admin's confirm password.");
        theForm.password2.focus();
        return false;
    }
	if(theForm.password2.value != theForm.password1.value){
    	alert("Both password fields must poses the same value.");
        theForm.password1.focus();
        return false;
    }
  if(theForm.name.value == "" || IsAllSpaces(theForm.name.value)){
    	alert("Please provide Full Name.");
        theForm.name.focus();
        return false;
    }
	/*if(!theForm.elements[4].checked && !theForm.elements[5].checked && !theForm.elements[6].checked && !theForm.elements[7].checked && !theForm.elements[8].checked && !theForm.elements[9].checked){
    	if(theForm.adminType[3].checked != true)
		{
			alert("Please assign atleast one right to the admin.");
    	    theForm.elements[4].focus();
        	return false;
		}
    }*/
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the admin's email address.");
        theForm.email.focus();
        return false;
    }
    <? if(CONFIG_VERIFY_TRANSACTION_ENABLED) { ?>	
    for (j=0;j<6;j++)
    <? }else{ ?>
    for (j=0;j<5;j++)
    <? } ?>
    {
    	if(theForm.elements[j+12].checked == true)
    	{
    		flag = 1;
    	}
    }
    for (k=0;k<theForm.adminType.length;k++)
    {
    	if (theForm.adminType[k].checked)
    	{
    		adminTypeValue = 	theForm.adminType[k].value;
    	}	
    }
    if (flag == 0 && adminTypeValue == "Admin")
    {
    	<? if(CONFIG_BACK_DATED == '1'){ ?>
			alert("Please assign atleast one right to the User (whether you selected 'Backdated Transaction' or not).");
		<? }else{?>
    	alert("Please assign atleast one right to the User.");
    <? } ?>
        theForm.elements[12].focus();
        return false;
    }
     else {
	}
<?	if (CONFIG_CUST_COUNTRIES == "1") {  ?>
	if(theForm.custCountries.value == "" || IsAllSpaces(theForm.custCountries.value)){
  	alert("Please select atleast one country for customers.");
      theForm.custCountries.focus();
      return false;
  }
<?	}  ?>
	if(theForm.IDAcountry.value == "" || IsAllSpaces(theForm.IDAcountry.value )){
    	alert("Please provide the country to send money."+getUserType(document.addAdmin.adminType));
        theForm.IDAcountry.focus();
        return false;
    }
<?	if (CONFIG_BACK_DATED == '1' && $agentType == 'admin') {  ?>
	if (document.getElementById('backdated').checked == true) {
		if (document.getElementById('backDays').value != '') {
			if (document.getElementById('backDays').value == '0') {
				alert("Please provide back days other than zero");
				document.getElementById('backDays').focus();
				return false;
			}
			if (!isNumeric(document.getElementById('backDays').value)) {
				alert("Please provide the positive numeric back days");
				document.getElementById('backDays').focus();
				return false;
			}
		}
	}
<?	}  ?>
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }


function isNumeric(strString) {
   //  check for valid numeric strings	
   
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	
	if (strString.length == 0) {
		return false;
	}
	
	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
	  strChar = strString.charAt(i);
	  if (strValidChars.indexOf(strChar) == -1)	{
	     blnResult = false;
	  }
	}
	return blnResult;
}

function showBackDays() {
	if (document.getElementById('backdated').checked == true) {
		document.getElementById('howmanybackdays').style.visibility = 'visible';
		document.getElementById('howmanybackdays').style.position = 'relative';
	} else {
		document.getElementById('backDays').value = '';
		document.getElementById('howmanybackdays').style.visibility = 'hidden';
		document.getElementById('howmanybackdays').style.position = 'absolute';
	}
}
function displayAgentsRow()
{
	if($("#isAssignAgents").attr("checked"))
		$("#agentsListRow").show();
	else
		$("#agentsListRow").hide();
}

	// end of javascript 
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font class="topbar_tex">Add Admin Staff</font></strong></td>
  </tr>
  <form action="add-admin-conf-opal.php" method="post" onSubmit="return checkForm(this);" name="addAdmin">
  <tr>
    <td align="center">
		<table width="480" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add 
                    Admin Staff</strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#EEEEEE">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* 
              Compulsory Fields</font></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td><font color="#005b90"><strong>Login Name<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="loginName" value="<?=stripslashes($_SESSION["loginName"]); ?>"  size="35" maxlength="32"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Password<font color="#ff0000">*</font></strong></font></td>
            <td><input type="password" name="password1" value="<?=stripslashes($_SESSION["password1"]); ?>" size="35" maxlength="16"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Confirm Password<font color="#ff0000">*</font></strong></font></td>
            <td><input type="password" name="password2" value="<?=stripslashes($_SESSION["password2"]); ?>" size="35" maxlength="16"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144" valign="top"><font color="#005b90"><b>Full Name<font color="#ff0000">*</font></b></font></td>
            <td><input type="text" name="name" value="<?=stripslashes($_SESSION["name"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Email<font color="#ff0000">*</font></strong></font></td>
            <td><input type="text" name="email" value="<?=stripslashes($_SESSION["email"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Admin Type</strong></font></td>
            <td><table width="100%"  border="0" cellspacing="0" cellpadding="3">
              <tr>
<? if (IS_BRANCH_MANAGER == '1'){ ?>
                <td><input type="radio" name="adminType" id="adminType" value="Branch Manager" <? if ($_SESSION["adminType"] == "Branch Manager") echo "checked"; ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td><? if($ACMFlag){ echo CONFIG_ADMIN_STAFF_LABEL;}else{?>Branch Manager <? } ?></td>                
<? } else { ?>
                <td><input type="hidden" name="hiddenval">&nbsp;</td>
                <td>&nbsp;</td>
<? } ?>
              	<td><input type="radio" name="adminType" id="adminType" value="Collector" <? if ($_SESSION["adminType"] == "Collector") echo "checked"; ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Collector </td>                
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="Admin" <? if ($_SESSION["adminType"] == "Admin") echo "checked"; ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>
                	<?
						if(CONFIG_LABEL_BASIC_USER == '1')
						{
						echo(CONFIG_BASIC_USER_NAME);	
					}else{
						echo("Basic User");
						}                	
                	?> </td>
                <td><input type="radio" name="adminType" id="adminType" value="Call" <? if ($_SESSION["adminType"] == "Call") echo "checked"; ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Call center Staff </td>
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="Admin Manager" <? if ($_SESSION["adminType"] == "Admin Manager") {echo "checked"; } ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Admin Manager </td>                
                <td><input type="radio" name="adminType" id="adminType" value="Support" <? if ($_SESSION["adminType"] == "Support") {echo "checked"; } ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Support Staff </td>                
              </tr>
              <tr>
                <td><input type="radio" name="adminType" id="adminType" value="SUPI Manager" <? if ($_SESSION["adminType"] == "SUPI Manager") {echo "checked"; } ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Distributor Managment </td>
				  <td><input type="radio" name="adminType" id="adminType" value="MLRO" <? if ($_SESSION["adminType"] == "MLRO") {echo "checked"; } ?> onClick="javascript:disableRights(document.addAdmin);"></td>
				  <td>MLRO </td>

				 <!-- <td><input type="radio" name="adminType" id="adminType" value="Account Manager" <?// if ($_SESSION["adminType"] == "Account Manager") {echo "checked"; } ?> onClick="javascript:disableRights(document.addAdmin);"></td>
                <td>Account Manager </td>   -->
              </tr>
            </table>
            </td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td width="144" valign="top"><font color="#005b90"><b>Rights<font color="#ff0000">*</font></b></font></td>
            <td><table width="100%" border="0">
				<tr>
					<td id="createAgent"><input name="adminRights[0]" id="adminRights0" type="checkbox" value="Create Agent" <? if (substr_count($_SESSION["rights"], "Create Agent")) echo "checked"; ?>> Create <?= __("Agent") ;?></td>
					<td><input name="adminRights[1]" id="adminRights1" type="checkbox" value="Delete Agent" <? if (substr_count($_SESSION["rights"], "Delete Agent")) echo "checked"; ?>> Delete <?=__("Agent")?></td>
				</tr>
				<tr>
					<td><input name="adminRights[2]" id="adminRights2" type="checkbox" value="Suspend Agent" <? if (substr_count($_SESSION["rights"], "Suspend Agent")) echo "checked"; ?>> Suspend <?=__("Agent") ?> </td>
					<td id="createTransaction"><input name="adminRights[5]" id="adminRights5" type="checkbox" value="Create Transaction" <? if (substr_count($_SESSION["rights"], "Create Transaction")) echo "checked"; ?>> Create Transaction</td>
				</tr>
				<tr>
					<td><input name="adminRights[3]" id="adminRights3" type="checkbox" value="Authorise Transaction" <? if (substr_count($_SESSION["rights"], "Authorise Transaction")) echo "checked"; ?>> Authorise Transaction</td>
					<? if(CONFIG_VERIFY_TRANSACTION_ENABLED)
          {?>
					<td><input name="adminRights[4]" id="adminRights4" type="checkbox" value="Verify Transaction" <? if (substr_count($_SESSION["rights"], "Verify Transaction")) echo "checked"; ?>> Verify Transaction</td>
					<? 
					}else { ?>
					<td>&nbsp;</td>
					<? } ?>
					
				</tr>
				<? if(CONFIG_BACK_DATED == '1'){ ?>
				<tr>
					<td colspan="2" id="backTransaction"><input name="adminRights[6]" id="adminRights6" id="backdated" type="checkbox" value="Backdated" <? if (substr_count($_SESSION["rights"], "Backdated")) echo "checked"; ?> <? if ($agentType == 'admin') { ?>onClick="showBackDays();"<? } ?>> Backdated Transaction</td>
				</tr>
				<tr id="howmanybackdays" style="visibility:hidden;position:absolute;">
					<td colspan="2">How Many Back Days <input type="text" name="backDays" id="backDays" size="3"> <i>By default,</i> 1</td>
				</tr>
				<? } ?>
						
			</table></td>
          </tr>
         
          <tr bgcolor="#ededed">
            <td><font color="#005b90"><strong>Status</strong></font></td>
            <td><input type="radio" name="agentStatus" value="New" <? if ($_SESSION["agentStatus"] == "" || $_SESSION["agentStatus"] == "New") echo "checked"; ?>>
              New
                <input type="radio" name="agentStatus" value="Active" <? if ($_SESSION["agentStatus"] == "Active") echo "checked"; ?>>
                Active
                <input type="radio" name="agentStatus" value="Disabled" <? if ($_SESSION["agentStatus"] == "Disabled") echo "checked"; ?>>
                Disabled
                <input type="radio" name="agentStatus" value="Suspended" <? if ($_SESSION["agentStatus"] == "Suspended") echo "checked"; ?>>
                Suspended</td>
          </tr>
      	<!-- Niaz -->
      	 <?	if (CONFIG_CUST_COUNTRIES == "1") {  ?>
         <tr bgcolor="#ededed"> 
           <td width="144"><font color="#005b90"><strong>Select Country for Customers</strong></font></td>
            <td>
								Hold Ctrl key for multiple selection<br>
              <SELECT name="custCountries[]" size="4" multiple id="custCountries" style="font-family:verdana; font-size: 11px" >
                <?
               if (CONFIG_CANADIAN_COUNTRY_OPTION == "1" && ($agentType == "SUPAI" || $agentType == "SUPA" || $agentType == "SUBAI" || $agentType == "SUBA"))
               {
               	$Agent_Country = selectFrom("select agentCountry from ".TBL_ADMIN_USERS." where userID = '".$_SESSION["loggedUserData"]["userID"]."'");
               	if ($Agent_Country["agentCountry"] == "Canada")
               	{
               	?>
               	<option value="Canada">Canada</option>
               	<?		
               	}
               	else
               	{
               		if(CONFIG_ENABLE_ORIGIN == "1"){
        	
            			$countryTypes = " and countryType like '%origin%' ";
            			$agentCountry = " ";
            	           	
            		}else{
            			$countryTypes = " ";
            			$agentCountry = " and countryName ='United Kingdom' ";
            		}
					$countriesQuery = "select countryName, countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName";
					if($countryBasedFlag){
						$countriesQuery = "select countryName, countryId from ".TBL_COUNTRY.", ".TBL_SERVICE_NEW." where 1 AND countryId = fromCountryId $countryTypes $agentCountry order by countryName";
					}							
					$countires = selectMultiRecords($countriesQuery);
					for ($i=0; $i < count($countires); $i++){
						?>
              <option value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["custCountries"], $countires[$i]["countryName"]) ? "selected"  : "")?>>
              <?=$countires[$i]["countryName"]; ?>
              </option>
              <?
						}
               	}
              }
              else
              {
               if(CONFIG_ENABLE_ORIGIN == "1"){
        	
            
            		$countryTypes = " and countryType like '%origin%' ";
            		$agentCountry = " ";
            	           	
            }else{
            		$countryTypes = " ";
            		$agentCountry = " and countryName ='United Kingdom' ";
            	}
					$countires = selectMultiRecords("select countryName, countryId from ".TBL_COUNTRY." where 1 $countryTypes $agentCountry order by countryName");
					for ($i=0; $i < count($countires); $i++){
				?>
                      <option value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["custCountries"], $countires[$i]["countryName"]) ? "selected"  : "")?>>
                      <? echo $countires[$i]["countryName"]; ?>
                      </option>
                      <?
					}
				}
				?>
              </SELECT>
                	</td>
          </tr>
        <?	}  ?>
       
          <tr bgcolor="#ededed" id="countryToSendMoney"> 
            <td width="144"><font color="#005b90"><strong>Select Country to Send money</strong></font></td>
            <td>
								Hold Ctrl key for multiple selection<br>
              <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
                
                <?
                
                if(CONFIG_ENABLE_ORIGIN == "1"){
                	
                	
                		$countryTypes = " and countryType like '%destination%' ";
                	                                	
                	}else{
                		$countryTypes = " ";
                	
                	}
                if(CONFIG_COUNTRY_SERVICES_ENABLED){
	                
	                	
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where  countryId = toCountryId  $countryTypes order by countryName");
									
								}else
								{
										$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
									
								}
                
					for ($i=0; $i < count($countires); $i++){
				?>
                <OPTION value="<?=$countires[$i]["countryName"]; ?>" <? echo (strstr($_SESSION["IDAcountry"],$countires[$i]["countryName"]) ? "selected"  : "")?>> 
                <?=$countires[$i]["countryName"]; ?>
                </OPTION>
                <?
					}
				?>
              </SELECT>
                	</td>
          </tr>
		  <? if($operatorFlag){?>
          <tr bgcolor="#ededed"> 
            <td align="left"><font color="#005b90"><strong>Operator ID</strong></font></td>
            <td align="left">
              <input type="text" name="OperatorID" value="<?=stripslashes($_SESSION["OperatorID"]); ?>" size="35" maxlength="255">
            </td>
          </tr>
	   <? } if(CONFIG_ADMIN_STAFF_AMOUNT_LIMIT_ALERT == "1"){ ?>
	      <tr bgcolor="#ededed"> 
			<td align="left"><font color="#005b90"><strong>Amount Limit at Create Transaction</strong></font></td>
			<td align="left">
				<input type="text" name="limitTransAmount" id="limitTransAmount" value="<?=$_SESSION["limitTransAmount"]; ?>"  />
			</td>
		</tr>		
		<? if(CONFIG_HIDE_CHEQUE_CASHING_LIMIT=="1"){?>
		<tr bgcolor="#ededed"> 
			<td align="left"><font color="#005b90"><strong>Cheque Cashing Limit</strong></font></td>
			<td  align="left">
				<input type="text" name="limitChequeAmount" id="limitChequeAmount" value="<?=$_SESSION["limitChequeAmount"]; ?>"  />
			</td>
		</tr>
		<? } 
		if(CONFIG_HIDE_CURRENCY_EXCHANGE_LIMIT=="1") { ?>
			<tr bgcolor="#ededed"> 
			<td align="left"><font color="#005b90"><strong>Currency Exchange Limit</strong></font></td>
			<td align="left">
				<input type="text" name="limitCurrExchange" id="limitCurrExchange" value="<?=$_SESSION["limitCurrExchange"]; ?>" />
			</td>
		</tr>
		<? } 
		if(CONFIG_HIDE_DAILY_EXPENSE_LIMIT=="1") {
		?>		
			<tr bgcolor="#ededed"> 
			<td align="left"><font color="#005b90"><strong>Daily Expense Limit</strong></font></td>
			<td align="left">
				<input type="text" name="limitDailyExpense" id="limitDailyExpense" value="<?=$_SESSION["limitDailyExpense"]; ?>" />
			</td>
		</tr>  		
		  <?	
		  		}
		   } ?>
		  
        <?
        if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1')
        {
			if($agentType == "admin")
			{
        ?>
		<tr bgcolor="#ededed"> 
			<td colspan="2" align="left">
				<input type="checkbox" name="isAssignAgents" id="isAssignAgents" value="Y" onChange='displayAgentsRow();' />
				&nbsp;<b>Want to assign this admin user to <?=__("agent"); ?>s?</b>
			</td>
		</tr>
          <tr bgcolor="#ededed" id="agentsListRow" style="display:none"> 
            <td width="144"><font color="#005b90"><strong>Select Associated Agent to Create Transaction</strong></font></td>
            <td>
				Hold Ctrl key for multiple selection<br>
              <SELECT name="linkedAgent[]" size="4" multiple id="linkedAgent" style="font-family:verdana; font-size: 11px" >
                
                <?
                
                	$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent = 'N' or isCorrespondent ='Y'";
					
					if($agentType == "Branch Manager")
					{
						$agentQuery .= " and parentID = '$changedBy'";				
						
					}
					$agentQuery .= "order by agentCompany";
					$agents = selectMultiRecords($agentQuery);
                
					for ($i=0; $i < count($agents); $i++){
				?>
                <OPTION value="<?=$agents[$i]["username"]; ?>" <? echo (strstr($_SESSION["linkedAgent"],$agents[$i]["username"]) ? "selected"  : "")?>>
                	<? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?>
               
                </OPTION>
                <?
					}
				?>
              </SELECT>
                	</td>
          </tr>
          <?
		  	}
			elseif($agentType == "SUPA")
			{
			?>
				<input type="hidden" name="linkedAgent[]" value="<?=$_SESSION["loggedUserData"]["username"]?>" />
			<?
			}
        }




        if(CONFIG_ADMIN_ASSOCIATE_DISTRIBUTOR == '1')
        {
        ?>  
        	<tr bgcolor="#ededed" id="DistributorList"> 
            <td width="144"><font color="#005b90"><strong>Select Associated Distributor </strong></font></td>
            <td>Hold Ctrl key for multiple selection<br>
              <SELECT name="linkedDistributor[]" size="4" multiple id="linkedDistributor" style="font-family:verdana; font-size: 11px" >  
	        			<?
	        			$distQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where 1 and parentID > 0 and adminType='Agent' and isCorrespondent = 'ONLY'";
	        			if($agentType == "Branch Manager")
								{
									$distQuery .= " and parentID = '$changedBy'";				
								}
								$distQuery .= "order by agentCompany";
								$dist = selectMultiRecords($distQuery);
	        			for ($i=0; $i < count($dist); $i++){
								?>
	                <OPTION value="<?=$dist[$i]["username"]; ?>" <? echo (strstr($_SESSION["linkedDistributor"],$dist[$i]["username"]) ? "selected"  : "")?>>
	                	<? echo($dist[$i]["agentCompany"]." [".$dist[$i]["username"]."]"); ?>
	               	</OPTION>
	              <?
								}
								?>
              </SELECT>
            </td>
          </tr>
        <?
        }
        ?>
		
		<? 
			/**
			 * To associate the admin staff with the super agents
			 * @Ticket #4384
			 
			if(CONFIG_ADMIN_ASSOCIATE_AGENT == "1" ) { 
		?>
		<tr bgcolor="#ededed"> 
			<td colspan="2" align="left">
				<input type="checkbox" name="isAssignAgents" id="isAssignAgents" value="Y" onChange='displayAgentsRow();' />
				&nbsp;<b>Want to assign this admin user to agents?</b>
			</td>
		</tr>
		
		<tr bgcolor="#ededed" id="agentsListRow" style="display:none"> 
            <td width="144"><font color="#005b90"><strong>Select Associated Agents</strong></font></td>
            <td>Hold Ctrl key for multiple selection<br>
              <select name="linkedAgent[]" size="4" id="agentsList" style="font-family:verdana; font-size: 11px" multiple>
        			<?
					$strAgentSql = "select userid, username, agentCompany from admin where parentID > 0 and adminType='Agent' and agentType='Supper' and isCorrespondent = 'N' and PPType = '' order by agentCompany ";

					$arrAgents = selectMultiRecords($strAgentSql);

					for ($i=0; $i < count($arrAgents); $i++){
					?>
	                <option value="<?=$arrAgents[$i]["username"]; ?>" <? echo (strstr($_SESSION["linked_Agent"],$arrAgents[$i]["userid"]) ? "selected"  : "")?>>
	                	<? echo($arrAgents[$i]["agentCompany"]." [".$arrAgents[$i]["username"]."]"); ?>
	               	</option>
	                <? } ?>
              </select>
            </td>
          </tr>
		<? } */ ?>
		
		
		
		<!--Niaz-->
          
          <tr bgcolor="#ededed"> 
            <td width="144"><font color="#005b90"><strong>Access From IP</strong></font></td>
            <td><input type="text" name="accessFromIP" id="accessFromIP" value="<?=stripslashes($_SESSION["accessFromIP"]); ?>" size="35" maxlength="255"></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center"> <input type="submit" value=" Save ">
              &nbsp;&nbsp; <input type="reset" value=" Clear "> </td>
          </tr>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
