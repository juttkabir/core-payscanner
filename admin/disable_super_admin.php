<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
dbConnect();
$username=loggedUser();

///////////////////////History is maintained via method named 'activities'
$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
if(isset($_GET["Disable"])){
	$Disable = $_GET["Disable"];
}
if(isset($Disable) && $Disable == "Disable"){
	update("update ".TBL_ADMIN_USERS." set agentStatus='Disabled', disabledBy='$username', disableReason='".checkValues($_GET[suspensionReason])."' where userID='".$_GET[userID]."'");
	
	$descript = "Super Admin ".$agentDetails["username"]."  is Disabled ";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["userID"],TBL_ADMIN_USERS,$descript);		 
	
	if ($_GET["caller"] == "sub")
		insertError(eregi_replace("Supper","Sub",AG20));
	else
	insertError(AG37);
	redirect("disable_super_admin.php?msg=Y&success=Y&userID=".$_GET[userID]);
}
?>
<html>
<head>
	<title>Disable Super Admin</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function checkForm(theForm) {
	if(theForm.suspensionReason.value == "" || IsAllSpaces(theForm.suspensionReason.value)){
    	alert("Please enter disabling reason.");
        theForm.suspensionReason.focus();
        return false;
    }
	return true;
}

function closeMe() {
	window.opener.window.location = 'manage_super_admin.php';
	window.close();	
}

function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" height="100%" border="0" cellspacing="1" cellpadding="5">
  <tr class="topbar">
    <td height="30"><strong><font color="#FFFFFF" size="2">Disable <? echo stripslashes($agentDetails[agentContactPerson])." [".stripslashes($agentDetails[agentCompany])."]"; ?></font></strong></td>
	<td align=right><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td>
  </tr>
  <form action="disable_super_admin.php" method="get" onSubmit="return checkForm(this);">
  <input type="hidden" name="userID" value="<?=$_GET[userID]; ?>">
  <tr>
    <td align="center" colspan=2>
		<table width="419" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Disable 
                    <?=stripslashes($agentDetails[agentContactPerson])." [".stripslashes($agentDetails[agentCompany])."]"; ?>
                    </strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed"> 
            <td colspan="2" bgcolor="#FFFFCC"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr> 
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; $_SESSION['error'] = ''; ?></td>
                </tr>
                <tr> 
                  <td align="center" colspan="2"><input type="button" value="Close" onClick="closeMe();"></td>
                </tr>
              </table></td>
          </tr>
          <? } else { ?>
          <tr bgcolor="#ededed"> 
            <td width="136" valign=top><font color="#005b90"><strong>Disable Reason*</strong></font></td>
            <td width="272"><textarea name="suspensionReason" cols="40" rows="4" style="font-family: verdana; font-size: 11px"><?=stripslashes($_SESSION["suspensionReason"]); ?></textarea></td>
          </tr>
          <tr bgcolor="#ededed"> 
		  	<td></td>
            <td><input type="submit" name="Disable" value="Disable"></td>
          </tr>
		  <? } ?>
        </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
