<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

if($_GET["userID"] != "")
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_GET["userID"]."'");
else
	$agentDetails = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$_POST["userID"]."'");
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	if(theForm.agentCompany.value == "" || IsAllSpaces(theForm.agentCompany.value)){
    	alert("Please provide the agent's company name.");
        theForm.agentCompany.focus();
        return false;
    }
	if(theForm.agentContactPerson.value == "" || IsAllSpaces(theForm.agentContactPerson.value)){
    	alert("Please provide the agent's contact person name.");
        theForm.agentContactPerson.focus();
        return false;
    }
	if(theForm.agentAddress.value == "" || IsAllSpaces(theForm.agentAddress.value)){
    	alert("Please provide the agent's address line 1.");
        theForm.agentAddress.focus();
        return false;
    }
	if(theForm.postcode.value == "" || IsAllSpaces(theForm.postcode.value)){
    	alert("Please provide the agent's Post Code.");
        theForm.postcode.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the agent's country from the list.");
        theForm.Country.focus();
        return false;
    }
	/*
	if(theForm.City.options.selectedIndex == 0){
    	alert("Please select the agent's city from the list.");
        theForm.City.focus();
        return false;
    }*/
	if(theForm.agentPhone.value == "" || IsAllSpaces(theForm.agentPhone.value)){
    	alert("Please provide the agent's phone number.");
        theForm.agentPhone.focus();
        return false;
    }
	if(theForm.email.value == "" || IsAllSpaces(theForm.email.value)){
    	alert("Please provide the agent's email address.");
        theForm.email.focus();
        return false;
    } else {
	}
	if(theForm.agentCompDirector.value == "" || IsAllSpaces(theForm.agentCompDirector.value)){
    	alert("Please provide the agent company director's name.");
        theForm.agentCompDirector.focus();
        return false;
    }
	if(theForm.agentBank.value == "" || IsAllSpaces(theForm.agentBank.value)){
    	alert("Please provide the agent's Bank name.");
        theForm.agentBank.focus();
        return false;
    }
	if(theForm.agentAccountName.value == "" || IsAllSpaces(theForm.agentAccountName.value)){
    	alert("Please provide the agent's bank account name.");
        theForm.agentAccountName.focus();
        return false;
    }
	if(theForm.agentAccounNumber.value == "" || IsAllSpaces(theForm.agentAccounNumber.value)){
    	alert("Please provide the agent's bank account number.");
        theForm.agentAccounNumber.focus();
        return false;
    }
	if(theForm.agentCurrency.options.selectedIndex == 0){
    	alert("Please select agent's currency.");
        theForm.agentCurrency.focus();
        return false;
    }
	if(theForm.agentAccountLimit.value == "" || IsAllSpaces(theForm.agentAccountLimit.value)){
    	alert("Please provide the agent's account limit.");
        theForm.agentAccountLimit.focus();
        return false;
    }
	if(theForm.agentCommission.value == "" || IsAllSpaces(theForm.agentCommission.value)){
    	alert("Please provide the agent's commision percentage.");
        theForm.agentCommission.focus();
        return false;
    }
	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Update Agent/Correspondent</font></strong></td>
  </tr>
  <form action="add-admin-manager-conf.php" method="post" onSubmit="return checkForm(this);" enctype="multipart/form-data">
  <input type="hidden" name="userID" value="<?=$agentDetails["userID"]; ?>">
  <tr>
    <td align="center">
		<table width="448" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr>
          <td colspan="2" bgcolor="#000000">
		  <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
		  	<tr>
				  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Update 
                    Agent/Correspondent</strong></font></td>
			</tr>
		  </table>
		</td>
        </tr>
		<? if ($_GET["msg"] != ""){ ?>
		  <tr bgcolor="#EEEEEE"> 
            <td colspan="2"> 
              <table width="100%" cellpadding="5" cellspacing="0" border="0"><tr><td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td><td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td></tr></table></td></tr><? } ?>
		<tr bgcolor="#ededed">
			<td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory 
              Fields</font></td>
		</tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Agent Company Name*</strong></font></td>
            <td width="268"><input type="text" name="agentCompany" value="<?=stripslashes($agentDetails["agentCompany"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Agent Contact Person*</strong></font></td>
            <td><input type="text" name="agentContactPerson" value="<?=stripslashes($agentDetails["agentContactPerson"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 1*</b></font></td>
            <td><input type="text" name="agentAddress" value="<?=stripslashes($agentDetails["agentAddress"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Address Line 2</b></font></td>
            <td><input type="text" name="agentAddress2" value="<?=stripslashes($agentDetails["agentAddress2"]); ?>" size="35" maxlength="255"></td>
        </tr>
		<tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Post Code </b></font></td>
            <td><input type="text" name="postcode" value="<?=stripslashes($agentDetails["postCode"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>Country*</b></font></td>
            <td><input type="hidden" value="<? echo $agentDetails["agentCountry"];?>" name="oldCountry"><SELECT name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action = 'update-agent.php'; document.forms[0].submit();">
                <OPTION value="">- Select Country-</OPTION>
				<!--option value="United States">United States</option>
				<option value="Canada">Canada</option-->
				<?
					$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1  order by countryName");
					for ($i=0; $i < count($countires); $i++){
				?>
				<OPTION value="<?=$countires[$i]["countryName"]; ?>"><?=$countires[$i]["countryName"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<? if ($_POST["Country"] == "") echo $agentDetails["agentCountry"]; else echo $_POST["Country"]?>");
                                </script></td>
        </tr>
		<? if ($agentDetails["agentCountry"] != "United States" && $agentDetails["agentCountry"] != "Canada" && $agentDetails["agentCountry"] != "United Kingdom"){ 
				if($_POST[Country] == "")
					$cities = selectMultiRecords("select distinct city from ".TBL_CITIES." where country='$agentDetails[agentCountry]' order by city");
				else
					$cities = selectMultiRecords("select distinct city from ".TBL_CITIES." where country='$_POST[Country]' order by city");
		?>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>City*</b></font></td>
            <td><SELECT name="City" style="font-family:verdana; font-size: 11px">
                <OPTION value="">- Select City-</OPTION>
				<?
					for ($i=0; $i < count($cities); $i++){
				?>
				<OPTION value="<?=$cities[$i]["city"]; ?>"><?=$cities[$i]["city"]; ?></OPTION>
				<?
					}
				?>
				</SELECT><script language="JavaScript">
         	SelectOption(document.forms[0].City, "<?=trim($agentDetails[agentCity]); ?>");
                                </script></td>
        </tr><? } else {
		?><!--tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><b>
              <? if ($agentDetails["Country"] == "United States") echo "Zip"; else echo "Postal"; ?>
              Code*</b></font></td>
            <td><input type="text" name="agentZip" value="<?=stripslashes($agentDetails["postCode"]); ?>" size="35" maxlength="7"></td>
        </tr--><?
		} ?>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td><input type="text" name="agentPhone" value="<?=stripslashes($agentDetails["agentPhone"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Fax</strong></font></td>
            <td><input type="text" name="agentFax" value="<?=stripslashes($agentDetails["agentFax"]); ?>" size="35" maxlength="32"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Email*</strong></font></td>
            <td><input type="text" name="email" value="<?=stripslashes($agentDetails["email"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company URL</strong></font></td>
            <td><input type="text" name="agentURL" value="<?=stripslashes($agentDetails["agentURL"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>MSB Number</strong></font><br>
              <font size="1">OR (State Bank license or permission from Ministry 
              of Finance)</font></td>
            <td><input type="text" name="agentMSBNumber" value="<?=stripslashes($agentDetails["agentMSBNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>MSB Expiry</strong></font></td>
            <td>
					<SELECT name="msbDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].msbDay, "<? echo substr($agentDetails["agentMCBExpiry"], 8,2);?>");
          		</script>
          		<SELECT name="msbMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbMonth, "<? echo substr($agentDetails["agentMCBExpiry"], 5,2); ?>");
          </script>
          		
          		 <SELECT name="msbYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].msbYear, "<?echo substr($agentDetails["agentMCBExpiry"], 0,4); ?>");
          </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Registration Number</strong></font></td>
            <td><input type="text" name="agentCompRegNumber" value="<?=stripslashes($agentDetails["agentCompRegNumber"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Company Director*</strong></font></td>
            <td><input type="text" name="agentCompDirector" value="<?=stripslashes($agentDetails["agentCompDirector"]); ?>" size="35" maxlength="100"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Director Address</strong></font></td>
            <td><input type="text" name="agentDirectorAdd" value="<?=stripslashes($agentDetails["agentDirectorAdd"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong>Director Proof ID</strong></font></td>
            <td valign="top"><input type="radio" name="agentProofID" value="Passport" <? if ($agentDetails["agentProofID"] == "Passport" || $agentDetails["agentProofID"] == "") echo "checked"; ?>>Passport <br><input type="radio" name="agentProofID" value="Driving License" <? if ($agentDetails["agentProofID"] == "Driving License") echo "checked"; ?>>Driving License <br><input type="radio" name="agentProofID" value="Other" <? if ($agentDetails["agentProofID"] == "Other") echo "checked"; ?>>Other please specify <input type="text" name="otherProofID" value="<?=$agentDetails[otherProofID]; ?>" size="15"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>ID Expiry</strong></font></td>
            <td><SELECT name="idMonth" size="1" style="font-family:verdana; font-size: 11px">
                                  <OPTION value="">Month</OPTION>
                                  <OPTION value="01">Jan</OPTION>
                                  <OPTION value="02">Feb</OPTION>
                                  <OPTION value="03">Mar</OPTION>
                                  <OPTION value="04">Apr</OPTION>
                                  <OPTION value="05">May</OPTION>
                                  <OPTION value="06">Jun</OPTION>
                                  <OPTION value="07">Jul</OPTION>
                                  <OPTION value="08">Aug</OPTION>
                                  <OPTION value="09">Sep</OPTION>
                                  <OPTION value="10">Oct</OPTION>
                                  <OPTION value="11">Nov</OPTION>
                                  <OPTION value="12">Dec</OPTION>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idMonth, "<? echo substr($agentDetails["agentIDExpiry"], 5,2); ?>");
          </script>
					<SELECT name="idDay" size="1" style="font-family:verdana; font-size: 11px">
                               <OPTION value="">Day</OPTION>
							      <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value=\"$Day\">$Day</option>\n";
						}
					  ?>
                                </select> <script language="JavaScript">
         			SelectOption(document.forms[0].idDay, "<?echo substr($agentDetails["agentIDExpiry"], 8,2);?>");
          		</script> <SELECT name="idYear" size="1" style="font-family:verdana; font-size: 11px">
                                 <OPTION value="" selected>Year</OPTION> <?
								  	$cYear=date("Y");
								  	for ($Year=$cYear; $Year<($cYear+5);$Year++)
									{
										echo "<option value=\"$Year\">$Year</option>\n";
									}
		  ?>
                                </SELECT> <script language="JavaScript">
         	SelectOption(document.forms[0].idYear, "<?echo substr($agentDetails["agentIDExpiry"], 0,4);?>");
          </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169" valign="top"><font color="#005b90"><strong> Document Provided</strong></font></td>
            <td><input name="agentDocumentProvided[0]" type="checkbox" id="agentDocumentProvided[0]" value="MSB License" <? echo (strstr($agentDetails["agentDocumentProvided"],"MSB License") ? "checked"  : "")?>>
MSB License <br>
<input name="agentDocumentProvided[1]" type="checkbox" id="agentDocumentProvided[1]" value="Company Registration Certificate" <? echo (strstr($agentDetails["agentDocumentProvided"],"Company Registration Certificate") ? "checked"  : "")?>>
Company Registration Certificate <br>
<input name="agentDocumentProvided[2]" type="checkbox" id="agentDocumentProvided[2]" value="Utility Bill" <? echo (strstr($agentDetails["agentDocumentProvided"],"Utility Bill") ? "checked"  : "")?>>
Utility Bill <br>
<input name="agentDocumentProvided[3]" type="checkbox" id="agentDocumentProvided[3]" value="Director Proof of ID" <? echo (strstr($agentDetails["agentDocumentProvided"],"Director Proof of ID") ? "checked"  : "")?>>
Director Proof of ID</td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Bank*</strong></font></td>
            <td><input type="text" name="agentBank" value="<?=stripslashes($agentDetails["agentBank"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Name*</strong></font></td>
            <td><input type="text" name="agentAccountName" value="<?=stripslashes($agentDetails["agentAccountName"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Number*</strong></font></td>
            <td><input type="text" name="agentAccounNumber" value="<?=stripslashes($agentDetails["agentAccounNumber"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Branch Code</strong></font></td>
            <td><input type="text" name="agentBranchCode" value="<?=stripslashes($agentDetails["agentBranchCode"]); ?>" size="35" maxlength="255"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Type</strong></font></td>
            <td><select name="agentAccountType">
			<option value="Current">Current</option>
			<option value="Saving">Saving</option>
			<option value="Other">Other</option>
			</select><script language="JavaScript">
						         			SelectOption(document.forms[0].agentAccountType, "<? echo $agentDetails["agentAccountType"]; ?>");
						          	  </script></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Currency*</strong></font></td>
            <td><SELECT NAME="agentCurrency" style="font-family:verdana; font-size: 11px">
                                        <OPTION VALUE="" SELECTED>-Select One-</option>
										<OPTION VALUE="USD">US Dollar</option>
                                        <OPTION VALUE="AFA">Afghanistan Afghani</option>
                                        <OPTION VALUE="ALL">Albanian Lek</option>
                                        <OPTION VALUE="DZD">Algerian Dinar</option>
                                        <OPTION VALUE="ADF">Andorran Franc</option>
                                        <OPTION VALUE="ADP">Andorran Peseta</option>
                                        <OPTION VALUE="AON">Angolan New Kwanza</option>
                                        <OPTION VALUE="ARS">Argentine Peso</option>
                                        <OPTION VALUE="AWG">Aruban Florin</option>
                                        <OPTION VALUE="AUD">Australian Dollar</option>
                                        <OPTION VALUE="ATS">Austrian Schilling</option>
                                        <OPTION VALUE="BSD">Bahamian Dollar </option>
                                        <OPTION VALUE="BHD">Bahraini Dinar</option>
                                        <OPTION VALUE="BDT">Bangladeshi Taka</option>
                                        <OPTION VALUE="BBD">Barbados Dollar</option>
                                        <OPTION VALUE="BEF">Belgian Franc</option>
                                        <OPTION VALUE="BZD">Belize Dollar</option>
                                        <OPTION VALUE="BMD">Bermudian Dollar</option>
                                        <OPTION VALUE="BTN">Bhutan Ngultrum</option>
                                        <OPTION VALUE="BOB">Bolivian Boliviano</option>
                                        <OPTION VALUE="BWP">Botswana Pula</option>
                                        <OPTION VALUE="BRL">Brazilian Real</option>
                                        <OPTION VALUE="GBP">British Pound</option>
                                        <OPTION VALUE="BND">Brunei Dollar</option>
                                        <OPTION VALUE="BGL">Bulgarian Lev</option>
                                        <OPTION VALUE="BIF">Burundi Franc</option>
                                        <OPTION VALUE="XOF">CFA Franc BCEAO</option>
                                        <OPTION VALUE="XAF">CFA Franc BEAC</option>
                                        <OPTION VALUE="XPF">CFP Franc</option>
                                        <OPTION VALUE="KHR">Cambodian Riel</option>
                                        <OPTION VALUE="CAD">Canadian Dollar</option>
                                        <OPTION VALUE="CVE">Cape Verde Escudo</option>
                                        <OPTION VALUE="KYD">Cayman Islands Dollar</option>
                                        <OPTION VALUE="CLP">Chilean Peso</option>
                                        <OPTION VALUE="CNY">Chinese Yuan Renminbi</option>
                                        <OPTION VALUE="COP">Colombian Peso</option>
                                        <OPTION VALUE="KMF">Comoros Franc</option>
                                        <OPTION VALUE="CRC">Costa Rican Colon</option>
                                        <OPTION VALUE="HRK">Croatian Kuna</option>
                                        <OPTION VALUE="CUP">Cuban Peso</option>
                                        <OPTION VALUE="CYP">Cyprus Pound</option>
                                        <OPTION VALUE="CZK">Czech Koruna</option>
                                        <OPTION VALUE="DKK">Danish Krone</option>
                                        <OPTION VALUE="DJF">Djibouti Franc</option>
                                        <OPTION VALUE="DOP">Dominican R. Peso</option>
                                        <OPTION VALUE="NLG">Dutch Guilder</option>
                                        <OPTION VALUE="XEU">ECU</option>
                                        <OPTION VALUE="XCD">East Caribbean Dollar</option>
                                        <OPTION VALUE="ECS">Ecuador Sucre</option>
                                        <OPTION VALUE="EGP">Egyptian Pound</option>
                                        <OPTION VALUE="SVC">El Salvador Colon</option>
                                        <OPTION VALUE="EEK">Estonian Kroon</option>
                                        <OPTION VALUE="ETB">Ethiopian Birr</option>
                                        <OPTION VALUE="EUR">Euro</option>
                                        <OPTION VALUE="FKP">Falkland Islands Pound</option>
                                        <OPTION VALUE="FJD">Fiji Dollar</option>
                                        <OPTION VALUE="FIM">Finnish Markka</option>
                                        <OPTION VALUE="FRF">French Franc</option>
                                        <OPTION VALUE="GMD">Gambian Dalasi</option>
                                        <OPTION VALUE="DEM">German Mark</option>
                                        <OPTION VALUE="GHC">Ghanaian Cedi</option>
                                        <OPTION VALUE="GIP">Gibraltar Pound</option>
                                        <OPTION VALUE="XAU">Gold (oz.)</option>
                                        <OPTION VALUE="GRD">Greek Drachma</option>
                                        <OPTION VALUE="GTQ">Guatemalan Quetzal</option>
                                        <OPTION VALUE="GNF">Guinea Franc</option>
                                        <OPTION VALUE="GYD">Guyanese Dollar</option>
                                        <OPTION VALUE="HTG">Haitian Gourde</option>
                                        <OPTION VALUE="HNL">Honduran Lempira</option>
                                        <OPTION VALUE="HKD">Hong Kong Dollar</option>
                                        <OPTION VALUE="HUF">Hungarian Forint</option>
                                        <OPTION VALUE="ISK">Iceland Krona</option>
                                        <OPTION VALUE="INR">Indian Rupee</option>
                                        <OPTION VALUE="IDR">Indonesian Rupiah</option>
                                        <OPTION VALUE="IRR">Iranian Rial</option>
                                        <OPTION VALUE="IQD">Iraqi Dinar</option>
                                        <OPTION VALUE="IEP">Irish Punt</option>
                                        <OPTION VALUE="ILS">Israeli New Shekel</option>
                                        <OPTION VALUE="ITL">Italian Lira</option>
                                        <OPTION VALUE="JMD">Jamaican Dollar</option>
                                        <OPTION VALUE="JPY">Japanese Yen</option>
                                        <OPTION VALUE="JOD">Jordanian Dinar</option>
                                        <OPTION VALUE="KZT">Kazakhstan Tenge</option>
                                        <OPTION VALUE="KES">Kenyan Shilling</option>
                                        <OPTION VALUE="KWD">Kuwaiti Dinar</option>
                                        <OPTION VALUE="LAK">Lao Kip</option>
                                        <OPTION VALUE="LVL">Latvian Lats</option>
                                        <OPTION VALUE="LBP">Lebanese Pound</option>
                                        <OPTION VALUE="LSL">Lesotho Loti</option>
                                        <OPTION VALUE="LRD">Liberian Dollar</option>
                                        <OPTION VALUE="LYD">Libyan Dinar</option>
                                        <OPTION VALUE="LTL">Lithuanian Litas</option>
                                        <OPTION VALUE="LUF">Luxembourg Franc</option>
                                        <OPTION VALUE="MOP">Macau Pataca</option>
                                        <OPTION VALUE="MGF">Malagasy Franc</option>
                                        <OPTION VALUE="MWK">Malawi Kwacha</option>
                                        <OPTION VALUE="MYR">Malaysian Ringgit</option>
                                        <OPTION VALUE="MVR">Maldive Rufiyaa</option>
                                        <OPTION VALUE="MTL">Maltese Lira</option>
                                        <OPTION VALUE="MRO">Mauritanian Ouguiya</option>
                                        <OPTION VALUE="MUR">Mauritius Rupee</option>
                                        <OPTION VALUE="MXN">Mexican Peso</option>
                                        <OPTION VALUE="MNT">Mongolian Tugrik</option>
                                        <OPTION VALUE="MAD">Moroccan Dirham</option>
                                        <OPTION VALUE="MZM">Mozambique Metical</option>
                                        <OPTION VALUE="MMK">Myanmar Kyat</option>
                                        <OPTION VALUE="ANG">NL Antillian Guilder</option>
                                        <OPTION VALUE="NAD">Namibia Dollar</option>
                                        <OPTION VALUE="NPR">Nepalese Rupee</option>
                                        <OPTION VALUE="NZD">New Zealand Dollar</option>
                                        <option value="NIO">Nicaraguan Cordoba Oro</option>
                                        <OPTION VALUE="NGN">Nigerian Naira</option>
                                        <OPTION VALUE="KPW">North Korean Won</option>
                                        <OPTION VALUE="NOK">Norwegian Kroner</option>
                                        <OPTION VALUE="OMR">Omani Rial</option>
                                        <OPTION VALUE="PKR">Pakistan Rupee</option>
                                        <OPTION VALUE="XPD">Palladium (oz.)</option>
                                        <OPTION VALUE="PAB">Panamanian Balboa</option>
                                        <OPTION VALUE="PGK">Papua New Guinea Kina</option>
                                        <OPTION VALUE="PYG">Paraguay Guarani</option>
                                        <OPTION VALUE="PEN">Peruvian Nuevo Sol</option>
                                        <OPTION VALUE="PHP">Philippine Peso</option>
                                        <OPTION VALUE="XPT">Platinum (oz.)</option>
                                        <OPTION VALUE="PLN">Polish Zloty</option>
                                        <OPTION VALUE="PTE">Portuguese Escudo</option>
                                        <OPTION VALUE="QAR">Qatari Rial</option>
                                        <OPTION VALUE="ROL">Romanian Lei</option>
                                        <OPTION VALUE="RUB">Russian Rouble</option>
                                        <OPTION VALUE="WST">Samoan Tala</option>
                                        <OPTION VALUE="STD">Sao Tome/Principe Dobra</option>
                                        <OPTION VALUE="SAR">Saudi Riyal</option>
                                        <OPTION VALUE="SCR">Seychelles Rupee</option>
                                        <OPTION VALUE="SLL">Sierra Leone Leone</option>
                                        <OPTION VALUE="XAG">Silver (oz.)</option>
                                        <OPTION VALUE="SGD">Singapore Dollar</option>
                                        <OPTION VALUE="SKK">Slovak Koruna</option>
                                        <OPTION VALUE="SIT">Slovenian Tolar</option>
                                        <OPTION VALUE="SBD">Solomon Islands Dollar</option>
                                        <OPTION VALUE="SOS">Somali Shilling</option>
                                        <OPTION VALUE="ZAR">South African Rand</option>
                                        <OPTION VALUE="KRW">South-Korean Won</option>
                                        <OPTION VALUE="ESP">Spanish Peseta</option>
                                        <OPTION VALUE="LKR">Sri Lanka Rupee</option>
                                        <OPTION VALUE="SHP">St. Helena Pound</option>
                                        <OPTION VALUE="SDD">Sudanese Dinar</option>
                                        <OPTION VALUE="SDP">Sudanese Pound</option>
                                        <OPTION VALUE="SRG">Suriname Guilder</option>
                                        <OPTION VALUE="SZL">Swaziland Lilangeni</option>
                                        <OPTION VALUE="SEK">Swedish Krona</option>
                                        <OPTION VALUE="CHF">Swiss Franc</option>
                                        <OPTION VALUE="SYP">Syrian Pound</option>
                                        <OPTION VALUE="TWD">Taiwan Dollar</option>
                                        <OPTION VALUE="TZS">Tanzanian Shilling</option>
                                        <OPTION VALUE="THB">Thai Baht</option>
                                        <OPTION VALUE="TOP">Tonga Pa'anga</option>
                                        <OPTION VALUE="TTD">Trinidad/Tobago Dollar</option>
                                        <OPTION VALUE="TND">Tunisian Dinar</option>
                                        <OPTION VALUE="TRL">Turkish Lira</option>
                                        <OPTION VALUE="AED">UAE Dirham</option>
                                        <OPTION VALUE="UGS">Uganda Shilling</option>
                                        <OPTION VALUE="UAH">Ukraine Hryvnia</option>
                                        <option value="GBP">United Kingdom Pounds</option>
                                        <OPTION VALUE="UYP">Uruguayan Peso</option>
                                        <OPTION VALUE="VUV">Vanuatu Vatu</option>
                                        <OPTION VALUE="VEB">Venezuelan Bolivar</option>
                                        <OPTION VALUE="VND">Vietnamese Dong</option>
                                        <OPTION VALUE="YUN">Yugoslav Dinar</option>
                                        <OPTION VALUE="ZMK">Zambian Kwacha</option>
                                        <OPTION VALUE="ZWD">Zimbabwe Dollar</option>
                                      </SELECT><script language="JavaScript">
						         			SelectOption(document.forms[0].agentCurrency, "<? echo $agentDetails["agentCurrency"]; ?>");
						          	  </script></td>
        </tr>
        <!--tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Account Limit*</strong></font></td>
            <td><input type="text" name="agentAccountLimit" value="<?=stripslashes($agentDetails["agentAccountLimit"]); ?>" size="35" maxlength="15"></td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Commission package</strong></font></td>
          <td><select name="commPackage" id="commPackage">
            <option value="001">Fixed Amount</option>
            <option value="002">Percentage of Transaction Amount</option>
            <option value="003">Percentage of Fee</option>
			<option value="004">None</option>
          </select><script language="JavaScript">
						         			SelectOption(document.forms[0].commPackage, "<? echo $agentDetails["commPackage"]; ?>");
						          	  </script>
            </td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Commission Percentage/Fee*</strong></font></td>
            <td><input type="text" name="agentCommission" value="<?=stripslashes($agentDetails["agentCommission"]); ?>" size="35" maxlength="6"></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="169"><font color="#005b90"><strong>Status</strong></font></td>
            <td><input type="radio" name="agentStatus" value="New" <? if ($agentDetails["agentStatus"] == "" || $agentDetails["agentStatus"] == "New") echo "checked"; ?>>New <input type="radio" name="agentStatus" value="Active" <? if ($agentDetails["agentStatus"] == "Active") echo "checked"; ?>>Active <input type="radio" name="agentStatus" value="Disabled" <? if ($agentDetails["agentStatus"] == "Disabled") echo "checked"; ?>>Disabled <input type="radio" name="agentStatus" value="Suspended" <? if ($agentDetails["agentStatus"] == "Suspended") echo "checked"; ?>>Suspended</td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Correspondent</strong></font></td>
          <td><select name="correspondent" id="correspondent">
            <option value="N">Agent</option>
            <option value="ONLY">Distributor</option>
            <option value="Y">Both Agent and Distributor</option>
          </select><script language="JavaScript">SelectOption(document.forms[0].correspondent, "<? echo $agentDetails["isCorrespondent"]; ?>");</script></td>        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>IDA for the country</strong></font></td>
          <td>Hold Ctrl key for multiple selection
            <SELECT name="IDAcountry[]" size="4" multiple id="IDAcountry" style="font-family:verdana; font-size: 11px" >
              <OPTION value="">- Select Country-</OPTION>
              <option value="United States" <? echo (strstr($agentDetails["IDAcountry"],"United States") ? "selected"  : "")?>>United States</option>
              <option value="Canada" <? echo (strstr($agentDetails["IDAcountry"],"Canada") ? "selected"  : "")?>>Canada</option>
              <?
					$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
              <OPTION value="<?=$countires[$i]["country"]; ?>" <? echo (strstr($agentDetails["IDAcountry"],$countires[$i]["country"]) ? "selected"  : "")?>>
              <?=$countires[$i]["country"]; ?>
              </OPTION>
              <?
					}
				?>
              </SELECT>
          </td></tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Company Logo </strong></font></td>
          <td><input type="file" name="logo"></td>
        </tr>
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Authorized for the services</strong></font></td>
          <td>
		    Hold Ctrl key for multiple selection<br>
		    <SELECT name="authorizedFor[]" size="4" multiple style="font-family:verdana; font-size: 11px" >
            <?
			if(trim($agentDetails["authorizedFor"]) == "")
			{?>
				<option value="Pick up" selected>Pick up</option>
			<?
			}
			else
			{
			?>
			<option value="Pick up" <? echo (strstr($agentDetails["authorizedFor"],"Pick up") ? "selected"  : "")?>>Pick up</option>
            <?
			}
			?>
			<option value="Bank Transfer" <? echo (strstr($agentDetails["authorizedFor"],"Bank Transfer") ? "selected"  : "")?>>Bank Transfer</option>
			<option value="Home Delivery" <? echo (strstr($agentDetails["authorizedFor"],"Home Delivery") ? "selected"  : "")?>>Home Delivery</option>
          </SELECT>
		  </td>
        </tr>
		
        <tr bgcolor="#ededed">
          <td><font color="#005b90"><strong>Access From IP</strong></font></td>
          <td>For multiple please separate by comma.
              <input name="accessFromIP" type="text" id="accessFromIP" size="40" maxlength="255" value="<?=stripslashes($agentDetails["accessFromIP"]); ?>"></td>
        </tr-->
		<tr bgcolor="#ededed">
			<td colspan="2" align="center">
				<input type="hidden" name="usern" value="<?=stripslashes($agentDetails["username"]); ?>">
				<input type="submit" value=" Save ">&nbsp;&nbsp; <input type="reset" value=" Clear ">
			</td>
		</tr>
		
      </table>
	</td>
  </tr>
</form>
</table>
</body>
</html>
