<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();

$filters = "";

$agentName = $_REQUEST["agentName"];
/**
 * Date filter
 */
$fromDate = $_REQUEST["fYear"] . "-" . $_REQUEST["fMonth"] . "-" . $_REQUEST["fDay"];
$toDate = $_REQUEST["tYear"] . "-" . $_REQUEST["tMonth"] . "-" . $_REQUEST["tDay"];
$exportFormate = $_REQUEST["exportFormate"];
if($exportFormate=="")
	$exportFormate="xls";
// Show only Bank Transactions.
$transType = "Bank Transfer";
if($_POST["Submit"])
{
	$filters .= " (tr.transDate BETWEEN '$fromDate 00:00:00' AND '$toDate 23:59:59')";
	$filters .= " AND transType='$transType'";
	if($agentName!="" && $agentName!="all")	
		$filters .= " AND custAgentID='$agentName' ";
	$sql = "select 
					tbd.bankName, 
					tbd.branchAddress,
					tr.transID, 
					tr.refNumberIM, 
					tr.benID, 
					tr.transDate,
					tr.localAmount,
					tr.custAgentID,
					ben.firstName,
					ben.middleName, 
					ben.lastName,
					ben.Country,
					ben.IDType,
					ben.IDNumber,
					ben.userType,
					ben.CPF,
					tbd.accNo,
					tbd.accountType,
					tbd.branchCode
				from 
					transactions as tr, 
					bankDetails as tbd, 
					beneficiary as ben,
					admin ad
				where
					tbd.transID = tr.transID and
					tr.benID = ben.benID and
					tr.custAgentID = ad.userID and
					". $filters ." 
				order by 
					tbd.bankName asc
					";
}

?>
<script type="text/javascript">
function loadExportedFiles(transCount,exportFormat){
if(transCount>0)
	window.open('exportBankTransactionsMT-file.php?agentName=<?=$agentName?>&fromDate=<?=$fromDate?>&toDate=<?=$toDate?>&exportType='+exportFormat+'&transType=<?=$transType?>','disableAgent', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420');
else
	alert("No transactions to be Exported");
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Bank Based Trasactions Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.reportHeader {
	font-size: 20px;
	font-weight: bold;
	height: 40px;
}

.columnHeader {
	border: solid #cccccc 1px;
	font-weight: bold;
	background-color: #EEEEEE;
}

.bankName {
	font-size: 16px;
	font-weight: bold;
}

.transactions {
	border-bottom: solid #cccccc 1px;
	border-top: solid #cccccc 1px;
}

.subTotal {
	font-size: 14px;
	font-weight: bold;
	border-bottom: solid #bbbbbb 2px;
}

.reportSummary {
	font-weight: bold;
	border: solid #bbbbbb 1px;
}
-->
</style>
<script>
<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
-->
</script>
</head>
<body>
	<?
		/**
		 * If report is not opened from saved reports section 
		 * Than display the filters
		 */
		if(empty($_GET["RID"]) && empty($_REQUEST["dt"]))
		{
	?>
	<table border="0" align="center" cellpadding="5" cellspacing="1" >
      <tr>
      	<td align="center">
  					<b>Search Filter</b>    				
      	</td>
    	</tr>
			  <form name="Search" method="post" action="exportBankTransactionsMT.php">
      <tr>
          <td width="100%" colspan="4" bgcolor="#EEEEEE" align="center">
			<b>From </b>
			<? 
			$month = date("m");
			
			$day = date("d");
			$year = date("Y");
			?>
			
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
					for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
			?>
			</select>
			<script language="JavaScript">
				SelectOption(document.Search.fDay, "<?=(!empty($_POST["fDay"])?$_POST["fDay"]:"")?>");
			</script>
					<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">

			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">
				SelectOption(document.Search.fMonth, "<?=(!empty($_POST["fMonth"])?$_POST["fMonth"]:"")?>");
			</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			  <?
						$cYear=date("Y");
						for ($Year=2004;$Year<=$cYear;$Year++)
							{
								echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
							}
					  ?>
			</SELECT> 
					<script language="JavaScript">
				SelectOption(document.Search.fYear, "<?=(!empty($_POST["fYear"])?$_POST["fYear"]:"")?>");
			</script>
			<b>To	</b>
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			  <?
				for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
			  ?>
			</select>
					<script language="JavaScript">
				SelectOption(document.Search.tDay, "<?=(!empty($_POST["tDay"])?$_POST["tDay"]:"")?>");
			</script>
					<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
	
			  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
			  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
			  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
			  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
			  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
			  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
			  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
			  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
			  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
			  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
			  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
			  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
					<script language="JavaScript">
				SelectOption(document.Search.tMonth, "<?=(!empty($_POST["tMonth"])?$_POST["tMonth"]:"")?>");
			</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
			?>
			</SELECT>
					<script language="JavaScript">
			SelectOption(document.Search.tYear, "<?=(!empty($_POST["tYear"])?$_POST["tYear"]:"")?>");
			</script>
				  <? if(!empty($msg)) { ?>
					<br /><br />
					<b><?=$msg?></b>
				  <? } ?>
			</td>
	</tr>
		<tr>
					<td width="100%" colspan="4" bgcolor="#EEEEEE" align="center"> 
											<SELECT NAME="agentName" id="agentName" STYLE="font-family:verdana; font-size: 11px;">
						<OPTION VALUE="">- Select Agent-</OPTION>
						<OPTION VALUE="all">All</OPTION>
						<?
						$agentQuery = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
						$agentQuery .=" order by agentCompany";
						$agents = selectMultiRecords($agentQuery);
							
							for ($i=0; $i < count($agents); $i++)
							{
						?>
								<OPTION VALUE="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></OPTION>
						<?
							}
						?>
					</SELECT>
					<SCRIPT LANGUAGE="JavaScript">
					SelectOption(document.Search.agentName, "<?=$agentName?>");
					</SCRIPT>
			</td>
		</tr>
			<tr>
						<td width="100%" colspan="4" bgcolor="#EEEEEE" align="center"> 
						<input type="submit" name="Submit" value="Process">
						</td>
	</tr>
			</form>
</table>
		<?
			}
		?>
	
	<table width="90%" cellpadding="2" cellspacing="0" align="center" border="1">
	<?
			$amountUnderOneBank = 0;
			$totalAmount = 0;
			$totalTrasactions = 0;
			$tids = "";
			
			if(!empty($sql))
				$fullRS = SelectMultiRecords($sql);
	?>
		<!-- Report Header Starts -->
		<tr>
			<td colspan="6" class="reportHeader">
				Bank Transactions <br />			</td>
			<td colspan="4" align="right" class="subTotal">
			Select Format : 
				<select name="exportFormat" id="exportFormat">
					<option value="xls">EXCEL</option>
					<option value="csv">CSV</option>
					<option value="html">HTML</option>
				</select>
			<input type="button" name="exportBtn" id="exportBtn" value="Export" onClick="loadExportedFiles(<?=count($fullRS)?>,document.getElementById('exportFormat').value);">			</td>
		</tr>
		<!-- Report Header Ends -->
		<!-- Column Header Starts -->
		<tr>
			<td class="columnHeader">DATE</td>
			<td class="columnHeader">Trans Ref #</td>
			<td class="columnHeader">AGENT CODE</td>
			<td class="columnHeader">BENEFICIARY</td>
			<td class="columnHeader">BANK NAME</td>
			<td class="columnHeader">BANK ADDRESS</td>
			<td class="columnHeader">SORT CODE</td>
			<td class="columnHeader">ACCOUNT #</td>
			<td class="columnHeader">VALUE</td>
		</tr>
		<!-- Column Header Ends -->
		<!-- Transactions Data Starts -->
		<!-- Bank Name Starts -->
		<?

			//debug($sql);
			for($i=0; $i < count($fullRS); $i++)
			{
				$tids .= $fullRS[$i]["transID"].",";
				
				$lastBankName = $fullRS[$i-1]["bankName"];
				$nextBankName = $fullRS[$i+1]["bankName"];
				$totalAmount += $fullRS[$i]["localAmount"];
				$totalTrasactions += 0;
				if((!empty($lastBankName) && $lastBankName != $fullRS[$i]["bankName"]) || $i == 0)
				{
					$noOfOrders = 0;
					$amountUnderOneBank = 0;
		?>
					<tr>
						<td colspan="9" class="bankName"><?=$fullRS[$i]["bankName"]?></td>
					</tr>
		<?
				}
				$noOfOrders += 1;
				$amountUnderOneBank += $fullRS[$i]["localAmount"];
		?>
		<!-- Bank Name Ends -->
		<!-- Transactions List Starts -->
		<tr>
		<? 
				$agentCodeQuery = selectFrom("Select username from admin where userID = '".$fullRS[$i]["custAgentID"]."'");
				$agentCode		= strtoupper($agentCodeQuery["username"]);
		?>
			<td class="transactions"><?=$fullRS[$i]["transDate"]?></td>
			<td class="transactions"><?=$fullRS[$i]["refNumberIM"]?></td>
			<td class="transactions"><?=$agentCode?></td>
			<td class="transactions"><?=$fullRS[$i]["firstName"]." ".$fullRS[$i]["middleName"]." ".$fullRS[$i]["lastName"]?></td>
			<td class="transactions"><?=(!empty($fullRS[$i]["bankName"])? $fullRS[$i]["bankName"] : "&nbsp;" )?></td>
			<td class="transactions"><?=(!empty($fullRS[$i]["branchAddress"])? $fullRS[$i]["branchAddress"] : "&nbsp;" )?></td>			
			<td class="transactions"><?=(!empty($fullRS[$i]["branchCode"])? $fullRS[$i]["branchCode"] : "&nbsp;" )?></td>
			<td class="transactions"><?=$fullRS[$i]["accNo"]?></td>
			<td class="transactions"><?=number_format($fullRS[$i]["localAmount"],2,".",",")?></td>
		</tr>
		<!-- Transactions List Ends -->
		<!-- SubTotal Starts  -->
<?
		if((!empty($nextBankName) && $nextBankName != $fullRS[$i]["bankName"]) || $i == count($fullRS)-1)
		{
?>
		<tr>
			<td class="subTotal">&nbsp;</td>
			<td class="subTotal">&nbsp;</td>
			<td colspan="3" class="subTotal">Number of Transactions: <?=$noOfOrders?> </td>
			<td class="subTotal">&nbsp;</td>
			<td colspan="2" class="subTotal">Total Bank: </td>
			<td class="subTotal"><?=number_format($amountUnderOneBank,2,".",",")?></td>
		</tr>
<?
		}
?>
		<!-- SubTotal Ends -->
		<!-- Transactions Data Ends -->
<?
		}
?>
<?
/**
 * If no records to show
 */
 if(count($fullRS) < 1)
 {
?>
	<tr>
			<td class="transactions" align="center" colspan="9">No Records found!</td>
	</tr>
<?
	}
?>
</table>

<br />

	<!-- Report Summary Starts -->
	<table width="500" border="0" align="center" cellpadding="10" cellspacing="0" class="reportSummary">
		<tr>
			<td width="35%" align="right">No. Transactions </td>
			<td width="15%"><?=$i?></td>
			<td width="35%" align="right">Final Total </td>
			<td width="15%"><?=number_format($totalAmount,2,".",",")?></td>
		</tr>
		<tr>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right">Total Foreign </td>
			<td><?=number_format($totalAmount,2,".",",")?></td>
		</tr>
	</table>
	<!-- Report Summary Ends -->
</body>
</html>