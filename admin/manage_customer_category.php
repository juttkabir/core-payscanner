<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
    $date_time = date('Y-m-d  h:i:s');
	$strMainCaption = "Manage Customer Category"; 

	$cmd = $_REQUEST["cmd"];

	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into ".TBL_CUSTOMER_CATEGORY." 
							(
							name,
							description,
							enabled,
							createdBy,
							dated
							)
							 values
								(
								'".$_REQUEST["catName"]."',
								'".$_REQUEST["description"]."',
								'".$_REQUEST["status"]."',
								'".$userID."',
								'".$date_time."'
								)";
							
				if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$cID = "";
			
		}
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["cID"]))
			{
				$strUpdateSql = "update ".TBL_CUSTOMER_CATEGORY."
								 set
									name = '".$_REQUEST["catName"]."',
									description = '".$_REQUEST["description"]."',
									enabled = '".$_REQUEST["status"]."',
									updated = '".$date_time."'
								 where
									id = '".$_REQUEST["cID"]."'	"; 
									
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$cID = "";
				$cmd = "";
						
			}	
		}
		
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["cID"]))
			{
				$strGetDataSql = "select * from ".TBL_CUSTOMER_CATEGORY." where id='".$_REQUEST["cID"]."'";
				$arrCompanyData = selectFrom($strGetDataSql);
													
				$catName = $arrCompanyData["name"];
				$description = $arrCompanyData["description"];
				$enabled = $arrCompanyData["enabled"];
				$createdBy = $arrCompanyData["createdBy"];
				$dated = $arrCompanyData["dated"];
				$updated = $arrCompanyData["updated"];
										
				$cmd = "UPDATE";
				$cID = $_REQUEST["cID"];
				}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["cID"]))
			{
				$strDelSql = "delete from ".TBL_CUSTOMER_CATEGORY." where id='".$_REQUEST["cID"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}

	if(empty($cmd))
		$cmd = "ADD";
	
	/* The default amount in "amount from" field should be '1' */
	if(empty($amount_from))
		$amount_from = 1;
	
	/* Fetching the list of secret questions to display at the bottom */	
	$arrAllCustomerData = selectMultiRecords("Select * from ".TBL_CUSTOMER_CATEGORY." order by id");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Customer Category</title>
<script language="javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.cluetip.js"></script>
<script>
$(document).ready(function() {
	$("#companyForm").validate({
		rules: {
			catName: {
				required: true
			}
		},
		messages: {
			catName: {
				required: "<br />Please provide customer category."
			}
		}
	});
	
  
			
  $('img').cluetip({splitTitle:'|'});
});
	

function disableFields(strFieldName, chrVal) 
{ 
	var rad_val = '';	
	
	if(chrVal == "Y")
		document.getElementById(strFieldName).disabled = false;
	else
		document.getElementById(strFieldName).disabled = true;
	
   }
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000066;
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
.style1 {color: #FF0000}
</style>
</head>
<body>
<form name="companyForm" id="companyForm" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="700" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
			 All fields mark with <font color="red">*</font> are compulsory Fields.</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td colspan="4" valign="top" class="tdDefination">
		   <table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" id="addressTble">
		   <tr bgcolor="#ededed">
		   	<td valign="top" class="tdDefination">Category Name<span class="style1"> *</span> </td>
		    <td align="left">
			<? 
		    $text = 'write here customer category name.';
			//$text2 =htmlspecialchars($text); 
			?>
			<input type="text" name="catName" id="catName" size="50" value="<?=$catName?>" />
			<img src="images/info.gif" border="0" title="Customer Category |<?=$text?>" />
			</td>
		   </tr>
		  <tr bgcolor="#ededed">
		   <td class="tdDefination">Description</td>
		  <td align="left" valign="top">
		   <textarea name="description" id="description" rows="5" cols="38"><?=$description?></textarea> </td>
	     </tr>
		  <tr bgcolor="#ededed">
		   <td class="tdDefination">Enable</td>
		   <td align="left">
		   <? 
		   $checked = '';
		   if($enabled == 'N')
		   		$checked = 'checked="checked"';
		   ?>
		    Yes <input type="radio" name="status" value="Y" checked="checked" /> No  <input type="radio" name="status" value="N" <?=$checked?>/> 
		 </td>
	     </tr>

		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="cID" value="<?=$cID?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($cID)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>			</td>
		</tr>
  </table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;Company Detail &nbsp;
	</legend>
	<table width="100%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="50%">Name</th>
			<th width="150%">Description</th>
			<th width="20%">Status</th>
			<th width="80%">Created</th>
			<th width="80%">Updated</th>
			<th width="20%">Actions</th>
		</tr>
		<?php
			foreach($arrAllCustomerData as $customerVal)
			{
				
					
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=stripcslashes($customerVal["name"])?></td>
			<td><?=stripcslashes($customerVal["description"])?></td>
			<td><?=stripcslashes($customerVal["enabled"])?></td>
			<td><?=stripcslashes($customerVal["dated"])?></td>
			<td><?=stripcslashes($customerVal["updated"])?></td>
		<td valign="top">
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$customerVal["id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit Customer Category Name| You can edit the record by clicking on this thumbnail." />				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$customerVal["id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove Customer Category|By clicking on this the record will be no longer available." />				</a>		  </td>
		</tr>
				
		<?
			}
		?>
</table>
</fieldset>

</body>
</html>