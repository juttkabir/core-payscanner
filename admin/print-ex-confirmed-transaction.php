<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$today = date('d-m-Y  h:i:s A');//date("d-m-Y");
$agentType = getAgentType();

if($_SESSION["custAgentID"] != "")
{
	$strAddressQuery = selectFrom("select * from ". TBL_ADMIN_USERS." where userID ='".$_SESSION["custAgentID"]."'");
	$CustomerData = selectFrom("select accountName from ". TBL_CUSTOMER." where customerID ='".$_SESSION["customerID"]."'");
	
	
	$printAddress = $strAddressQuery["agentAddress"];
	$printPhone = $strAddressQuery["agentPhone"];
	$printSenderNo = $CustomerData["accountName"];
	
}
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 
  <? if ($_GET["msg"] == "Y"){ ?>
  
		  <tr>
            <td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
              	
                <td width="40" align="center"><div class='noPrint'><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></div></td>
                <td width="635"><div class='noPrint'><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; $_SESSION['error'] = ""; ?></div></td>
                 
              </tr>
            </table></td>
          </tr>
       
		  <?
		  }
		  ?>

   <tr>
      <td align="center"><br>
        <table width="100%" border="0" bordercolor="#FF0000">
         
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#000000">
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#D9D9FF" >
                	<?
                	if($_SESSION["loggedUserData"]["logo"] != "" && file_exists("../logos/" . $_SESSION["loggedUserData"]["logo"]))
									{
										$arr = getimagesize("../logos/" . $_SESSION["loggedUserData"]["logo"]);
										
										$width = ($arr[0] > 200 ? 200 : $arr[0]);
										$height = ($arr[1] > 100 ? 100 : $arr[1]);
									?>
										<img src="../logos/<? echo $_SESSION["loggedUserData"]["logo"];?>" width="<?=$width?>" height="<?=$height?>">
									<?
                	}
                	else
                	{
                	?>
	                	<img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>">
                	<?
                	}
                	?>
                
                
                </td>
              	 <td width="100%" align="center" colspan="3">
            	<strong><? echo strtoupper($company." ".COMPANY_NAME_EXTENSION); ?></strong><br>Address:
            	<? if(CONFIG_POS_ADDRESS == '1'){ echo(COMPANY_ADDRESS); ?><br>
            <? echo(COMPANY_PHONE);}else{echo($printAddress); ?> <br>
            	<? echo("Phone:".$printPhone);} ?><br><strong><u>Customer Receipt</u></strong>
            </td>
              </tr>
              <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90">Date</font></td>
              	<td width="250">
              		<!--Against the back date condition according to the 
              				latest requirement have to show the current date and time -->
              		<? if($_SESSION["dDate"]!= "" && CONFIG_BACK_LEDGER_DATES == '1'){echo($_SESSION["dDate"]);}else{echo($_SESSION["mydDate"]);} ?>
              	</td>
              	<!-- Added by Niaz Ahmad againist ticket # 2533 at 10-10-2007 -->
                <? if(CONFIG_SHOW_REFERENCE_CODE_BOX == "1"){ ?>
              	<td width="100" align="left"><font color="#005b90">Transaction No</font></td>
              	<td width="250"><table border="1"><tr><td align="center"><strong><font size="3"><? echo $_SESSION["imReferenceNumber"]?></font></strong></td></tr></table></td>
              	
              	<? }else{ ?>
              	
              	 <td width="100" align="left"><font color="#005b90">Transaction No</font></td>
              	<td width="250">
              		<? echo $_SESSION["imReferenceNumber"]?>
              	</td>
              	<? } ?>
            </tr>
            <tr bgcolor="#ffffff">
              	  <td width="100" align="left"><font color="#005b90">Sender No</font></td>
              	<td width="250"><font size="2"><b> <? echo($printSenderNo);?></b></font></td>
              	 <td width="150" align="left"><font color="#005b90"><? echo $manualCode;?></font></td>
              	<td width="200">
              		<? echo($_SESSION["refNumber"]); ?>
              	</td>           	
            </tr>
            <?
            if(CONFIG_MANAGE_CLAVE == '1')
            {
            	
            	$getClave = selectFrom("select clave from ".TBL_TRANSACTION_EXTENDED." as e, ".TBL_TRANSACTIONS." as t where t.transID = e.transID and t.refNumberIM= '".$_SESSION["imReferenceNumber"]."'");
            	?>
            	<tr bgcolor="#ffffff">
              	  <td width="100" align="left">&nbsp;</td>
              	<td width="250"><font size="2">&nbsp;</td>
              	 <td width="150" align="left"><font color="#005b90">Clave</font></td>
              	<td width="200">
              		<? echo($getClave["clave"]); ?>
              	</td>           	
            </tr>
            	<?
            	}
            ?>
             <tr bgcolor="#ffffff">
              	 <td width="700" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
              	
            </tr>
            
     <? 
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, mobile, Email  from ".TBL_CUSTOMER." where customerID ='" . $_SESSION["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
              	<td width="550" colspan="3">
              		<? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff" valign="top">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="550" colspan="3">
              		<? echo $customerContent["Address"] . " " . $customerContent["Address1"]?>
             	</td>              	
            </tr>
            <tr bgcolor="#ffffff" valign="top">
           	    <td width="150" align="left"><font color="#005b90">Mobile</font></td>
              	<td width="550" colspan="3">
					<?=$customerContent["mobile"]?>
              	</td>              	
            </tr>

             
            <?
        }
            ?>
            
              <tr bgcolor="#ffffff">
              	 <td width="700" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
             </tr>
              <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,Mobile from ".TBL_BENEFICIARY." where benID ='" . $_SESSION["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Beneficiary Name</font></td>
              	<td width="500" colspan="3">
              		<? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?>
              	</td>
            </tr>
                     
             <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Address</font></td>
              	<td width="500" colspan="3">
              		<? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?>
              	</td>
				
              	
            </tr> 
			<?	
			if (CONFIG_SHOW_BEN_MOBILE=="1")
			{
			?>
			 <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Mobile</font></td>
              	<td width="500" colspan="3">
              		<?=$benificiaryContent["Mobile"]?>
              	</td>
			<?
			}
			?>	
              	
            </tr> 
        <?
        }            
        ?>
           
            <tr bgcolor="#ffffff">
              	 <td width="750" align="left" colspan="4" height="25"><font color="#005b90"><strong>Transaction Detail</strong></font></td>
             		</tr>
           			<tr bgcolor="#ffffff">
              		 <td width="200" align="left"><font color="#005b90">Destination</font></td>
              		 <td width="500" colspan="3">
              			<? 
	              			$queryColl = "select cp_id, cp_corresspondent_name,cp_phone, cp_fax,cp_branch_name,cp_branch_address,cp_ida_id  from ".TBL_COLLECTION." where cp_id ='".$_SESSION["collectionPointID"]."'";
											$destinCollection = selectFrom($queryColl);
											if($destinCollection["cp_id"] != '')
											{
												$distData = selectFrom("select name,distalias from admin where userID = '".$destinCollection["cp_ida_id"]."'");
												$destination = $destinCollection["cp_corresspondent_name"]." ".$destinCollection["cp_branch_name"];
												$cp_address = $destinCollection["cp_branch_name"]." ".$destinCollection["cp_branch_address"];
											}

											if(CONFIG_CP_DATA_ON_RECIEPT == "1"){
												
											if($distData["distalias"] != ""){
													
													echo $distData["distalias"].", ";
											}else{
												IF($agentType == "SUPA" || $agentType == "SUBA"){
													
												}else{
													echo $distData["name"].", ";
												}
											
											}
										}

											// dded by Niaz Ahmad #3096 at 18-04-2008
											if(CONFIG_CP_BRANCH_ADDRESS == "1"){
												echo $cp_address;
												}else{
											 echo $destination;
										}
										if(CONFIG_CP_DATA_ON_RECIEPT == "1"){
										echo " ,".$destinCollection["cp_phone"] ."," .$destinCollection["cp_fax"];
									}
										?>
              		</td>
           		 </tr> 
               <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90">Amount Paid</font></td>
              	<td width="250">
              		<? echo($_SESSION["currencyFrom"]." ".$_SESSION["transAmount"]);?>
              	</td>
              	 <td width="100" align="left"><font color="#005b90">Exchange Rate</font></td>
              	<td width="250">
              		<? echo($_SESSION['currencyTo']." ".$_SESSION["exchangeRate"]); ?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="100" align="left"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
              	<td width="250"> 
              		<? echo($_SESSION["currencyFrom"]." ".$_SESSION["IMFee"]);?>
              	</td>
              	 <td width="100" align="left"><font color="#005b90">Curr. Charges</font></td>
              	<td width="250">
              		<? echo($_SESSION["currencyFrom"]." ".$_SESSION["currencyCharge"]);?> 
              	</td>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="200" align="left"><font color="#005b90">Total Amount</font></td>
              	<td width="250">
              		<? echo($_SESSION["currencyFrom"]); ?> <? echo number_format($_SESSION["totalAmount"],2,'.',','); ?> 
              		
              	</td>
              	 <td width="200" align="left"><font color="#005b90">Foriegn Amount</font></td>
              	<td width="250">
              		
              		<? echo($_SESSION["currencyTo"]); ?> <? echo number_format($_SESSION["localAmount"],2,'.',','); ?> 
              	</td>
            </tr> 
              <tr bgcolor="#ffffff">
              	 <td width="100" align="left" colspan="4"  height="25"><font color="#005b90"><strong></strong></font></td>
              	
            </tr>        
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">CUSTOMER SIGNATURE</font></td>
              	<td width="200">&nbsp;
              		
              	</td>
							<td width="150" align="left"><font color="#005b90">AGENT SIGNATURE</font></td>
              	<td width="200">&nbsp;
              		
              	</td>		              	 
            </tr> 
          
            </table>
            
          
            <br>
            </fieldset></td>
           
          </tr>         
        
                       
               <table border="0" width="100%" cellspacing="1" cellpadding="0">
            	<tr>
            		<td width="100%" align="center"><strong><u>SAMEDAY - PICKUP LOCATIONS</u></strong></td>
            		
            	</tr>
            		</table>
            		
            		<table border="0" width="100%" cellspacing="1" cellpadding="0">
            	<? $collectionPoints = selectMultiRecords("select* from ".cm_collection_point." where sameday='Y'");
            			         	
            	?>
            	
         <tr bgcolor="#ffffff">
            	<? for($i=0;$i< count ($collectionPoints); $i++){ ?>
            	
            		<td width="100"> 
            			<u><? echo strtoupper($collectionPoints[$i]["cp_corresspondent_name"]); ?></u>
            			<br>
            			<? echo $collectionPoints[$i]["cp_branch_address"]; ?>
            			<?
            			if($collectionPoints[$i]["cp_phone"] != "")
            			{?>
            				<br>(T)<? echo $collectionPoints[$i]["cp_phone"]; 
            			}
            			if($collectionPoints[$i]["cp_fax"] != "")
            			{?>
            				<br>(F)<? echo $collectionPoints[$i]["cp_fax"];
            			}
            			if($collectionPoints[$i]["workingDays"] != "")
            			{?>
            				<br><? echo $collectionPoints[$i]["workingDays"];
            				if($collectionPoints[$i]["openingTime"] != '00:00:00' ||$collectionPoints[$i]["closingTime"] != '00:00:00')
            				{?>
            					<br><? echo $collectionPoints[$i]["openingTime"];?> to <? echo $collectionPoints[$i]["closingTime"]; 
            				}
            			}
            			if($collectionPoints[$i]["operatesWeekend"]== 'Y')
            			{ 
            				if($collectionPoints[$i]["weekendWorkingDays"] != "")
            				{?>
            					<br><? echo $collectionPoints[$i]["weekendWorkingDays"];?>&nbsp;<? echo $collectionPoints[$i]["weekendOpeningTime"];?> to <? echo $collectionPoints[$i]["weekendClosingTime"];
            				}
            			}?>
            			</td>
            			
            		<? if($j=='3'){
            			// echo "loop for jzero, i = ".$i." j ".$j;
            				$j =0; ?>
            				
            	</tr><tr bgcolor="#ffffff">
            		
            		<? } else {
            				//echo "loop j++, i =".$i." j ".$j;
            			 	$j++;
            			 } ?>
            			
            		
            
            		
            	
        <? } ?>	
            	
            </tr>	
            	
            </table>
            
            <br>
            <table border="0" width="100%" cellspacing="1" cellpadding="0">
            	<tr>
            	<td width="100%" align="center"><strong><u>TERMS AND CONDITIONS</u></strong><br><br></td></tr>
            		<tr>
            		<td width="100%" align="center">" <? echo (CONFIG_TRANS_COND);?> "</td>
            	</tr>
            		</table>
            <br>
            <table border="0" width="100%" cellspacing="1" cellpadding="0">
            	<? 
            	
            	$advert = selectMultiRecords("select * from ".Campaign_Type." where isEnable = 'Y'");
            			         	
            	?>
            	
            	<tr>
            	<td width="100%" align="center" colspan="5"><strong><u>Adverts / Promotions:-Depending on Season/Calendar.</u></strong><br><br></td></tr>
            		
            		<? for($i=0;$i< count ($advert); $i++){ ?>
            		<tr>
            		 <td align="center"> <u><? echo strtoupper($advert[$i]["heading"]); ?></u> &nbsp; <? echo $advert[$i]["message"]; ?></td>
            	 </tr>
            	<? } ?>
            	
            		</table>
            		
            		<tr>
            <td align="center">
         <form name="addTrans" action="print-ex-confirmed-transaction.php" method="post">
		<div class='noPrint'>
			<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
			&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="style2">Go to Create Transaction</a>
		</div>
         </form>
			</td>
          </tr>
            </table>

           
            
            </td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>