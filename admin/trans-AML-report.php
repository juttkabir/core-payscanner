<?
session_start();
include ("../include/config.php");
include("connectOtherDataBase.php");
include ("security.php");
$date_time = date('Y-m-d h:i:s');
$agentType = getAgentType();
$loggedUserID = $_SESSION["loggedUserData"]["userID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
extract(getHttpVars());
$reportName='trans-AML-report.php';
$reportLabel='Transaction AML Report';
$reportFlag=true;


$filterValue=array();
$filterName=array();

if ($offset == "") {
	$offset = 0;
}
		
if ($limit == 0) {
	$limit=50;
}
	
if ($newOffset != "") {
	$offset = $newOffset;
}
	
$nxt = $offset + $limit;
$prv = $offset - $limit;

$sortBy = $_GET["sortBy"];

if ($sortBy == "") {
	$sortBy = "transDate";
}

    $rfDay = $fDay;
	  $rfMonth =$fMonth;
	  $rfYear =$fYear;
	
	  $rtDay=$tDay;
	  $rtMonth =$tMonth;
	  $rtYear =	$tYear;

/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements 
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of 
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
   /**
	*  @var $extraCondition type string
	*  This varibale is used to define a condition 
	*/
	$extraCondition  = " and ( am.userID = '".$loggedUserID."' ) ";
}

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
	$custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
	$acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
	$extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
	$acManagerFlag = true;
}
/**
 *  @var $user type string 
 *  Variable that stores the alias of the usertype selected
 *  For customer it stores 'c' and for beneficiary it stores 'b'  
 */
$user = "";

if($RID!=''){
	
	$reportFlag=false;
	$filterArray=array(); 
	 $value=array(); 
	 $finalArray=array();
		$compQuery = "select queryID,RID,filterName,filterValue from " . TBL_COMPLIANCE_QUERY_LIST ." where RID=".$RID."";
		
		$compRecord = selectMultiRecords($compQuery); 
		$compQueryMain = selectFrom("select note from " . TBL_COMPLIANCE_QUERY ." where RID=".$RID."");
				
		for($c=0; $c< count($compRecord) ; $c++){
		
		 $filterArray[$c]= $compRecord[$c]["filterName"];
		 $value[$c]= $compRecord[$c]["filterValue"];
		  $finalArray[$filterArray[$c]]=$value[$c];
		 
} 
	$Save=$finalArray["Save"];
	$userType=$finalArray["userType"];
	$postCode=$finalArray["postCode"];
	$address=$finalArray["address"];
	$fromDate=$finalArray["fromDate"];
	$toDate=$finalArray["toDate"];
	$idType=$finalArray["idType"];
	$idNumber=$finalArray["idNumber"];

}	  


	  
if($Submit!= "" || $Save == 'Save Report'){

if($Submit!= ""){
	$filterValue[]=$Submit;
	$filterName[]='Submit';
}elseif($Save == 'Save Report'){
	$filterValue[]=$Save;
	$filterName[]='Save';
	}
	
	if ($_POST["note"] != "")
	{
		$note = $_POST["note"];
	}elseif ($_GET["note"] != "")
	{
		$note = $_GET["note"];	
	}
		
		if($fromDate!='' && $toDate!=''){
		$fD = explode("-",$fromDate);
		if (count($fD) == 3)
		{
				$rfDay = $fD[2];
				$rfMonth = $fD[1];
				$rfYear = $fD[0];
		}
				$tD = explode("-",$toDate);
		if (count($tD) == 3)
		{
				$rtDay = $tD[2];
				$rtMonth = $tD[1];
				$rtYear = $tD[0];
		}
	}	
		 $fromDate = $rfYear . "-" . $rfMonth . "-" . $rfDay;
  	 $toDate = $rtYear . "-" . $rtMonth . "-" . $rtDay;
	

		
$executeQuery=false;
$saveQuery=false;


  if( $userType == 'sender'){

	  if ($agentType == "SUPA" || $agentType == "SUBA"){
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 AND c.agentID = $loggedUserID ".$extraCondition;
		  $queryCnt = "SELECT count(transID) FROM ". TBL_TRANSACTIONS . " as t LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".$extraJoin." where 1 AND c.agentID = $loggedUserID ".$extraCondition;
	  } else {
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 " . $extraCondition;
		  $queryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 " . $extraCondition;
	  }
  	$executeQuery=true;
  	$filterValue[]=$userType;
	  $filterName[]='userType';
	  $user = "c.";
	  
  } elseif($userType == 'beneficiary'){

	  if ($agentType == "SUPA" || $agentType == "SUBA"){
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID LEFT JOIN " . TBL_BENEFICIARY . " as b ON t.benID = b.benID " . $extraJoin . " where 1 AND c.agentID = $loggedUserID " . $extraCondition;
		  $queryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID LEFT JOIN " . TBL_BENEFICIARY . " as b ON t.benID = b.benID " . $extraJoin . " where 1 AND c.agentID = $loggedUserID " . $extraCondition;
	  } else {
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID LEFT JOIN " . TBL_BENEFICIARY . " as b ON t.benID = b.benID " . $extraJoin . " where 1 " . $extraCondition;
		  $queryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID LEFT JOIN " . TBL_BENEFICIARY . " as b ON t.benID = b.benID " . $extraJoin . " where 1 " . $extraCondition;
	  }
  
  	$executeQuery=true;
  	$filterValue[]=$userType;
	  $filterName[]='userType';
	  $user = "b.";
  }elseif(($fromDate != '' && $toDate!= '')&&($idType == "" && $idNumber == "" && $userType == '' )){

	  if ($agentType == "SUPA" || $agentType == "SUBA"){
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 AND c.agentID = $loggedUserID " . $extraCondition;
		  $queryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 AND c.agentID = $loggedUserID " . $extraCondition;
	  } else {
		  $query = "SELECT t.transID,t.transDate,t.totalAmount,t.custAgentID,t.transType,t.transAmount,t.localAmount,t.refNumber,t.refNumberIM,t.transStatus,t.customerID,t.benID,t.collectionPointID,t.createdBy FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 " . $extraCondition;
		  $queryCnt = "SELECT count(transID) FROM " . TBL_TRANSACTIONS . " as t LEFT JOIN " . TBL_CUSTOMER . " as c ON t.customerID = c.customerID " . $extraJoin . " where 1 " . $extraCondition;
	  }
    $executeQuery=true;
    
 }



if($postCode!= ''){
	
	$query  .= " and ".$user."Zip='".$postCode."' ";
	$queryCnt  .= " and  ".$user."Zip='".$postCode."' ";
	$filterValue[]=$postCode;
	$filterName[]='postCode';
	}	
if ($address != '') {
	
	    $query  .= " and ".$user."Address Like '".$address."%' ";
	    $queryCnt  .= " and ".$user."Address Like '".$address."%' ";
	    $filterValue[]=$address;
	    $filterName[]='address';
	}

if ($idType != "" && $idNumber != "") {
		
		 $query .= " AND ".$user."IDType= '".$idType."' ";
		 $query .= " AND ".$user."IDNumber= '".$idNumber."' ";
		 $queryCnt .= " AND ".$user."IDType= '".$idType."' ";
		 $queryCnt .= " AND ".$user."IDNumber= '".$idNumber."' ";
		 
		 $filterValue[]=$idType;
	   $filterName[]='idType';
	   $filterValue[]=$idNumber;
	   $filterName[]='idNumber';
	}

if ($fromDate != "" && $toDate!= "") {
	   $query .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
	   $queryCnt .= " and (transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
	   
	  $filterValue[]=$fromDate;
	  $filterName[]='fromDate';
	  $filterValue[]=$toDate;
	  $filterName[]='toDate';
     	}

if($executeQuery==true)	{

	
    $query .= " ORDER BY $sortBy DESC ";
    $query .= " LIMIT $offset , $limit";
    
    $allCount = countRecords($queryCnt);
	  $contentsTrans = selectMultiRecords($query);
 }
if($Save == 'Save Report' && $reportFlag == true){

 
   $Querry_Sqls="insert into ".TBL_COMPLIANCE_QUERY." (reportName,logedUser,logedUserID,queryDate,reportLabel,note) values ('".$reportName."','".$agentType."','".$loggedUserID."','".$date_time."','".$reportLabel."','".$note."')";
	insertInto($Querry_Sqls);
	$reportID = @mysql_insert_id();
	

	for($i=0;$i< count($filterName);$i++){
	$Querry_Sqls2="insert into ".TBL_COMPLIANCE_QUERY_LIST." (RID,filterName,filterValue ) values (".$reportID.",'".$filterName[$i]."','".$filterValue[$i]."')";
	insertInto($Querry_Sqls2); 
  }
		$msg="Report Has Been Saved"; 
    
}
	  

}	
//debug($query);
?>
<html>
<head>
	<title>Transaction AML Report</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<!--<script language="javascript" src="trans-AML-report-ajax.js"></script>-->

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
   
 <script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {
    
    <? if($idType!=''){?>
    if(theForm.userType.value == "" || IsAllSpaces(theForm.userType.value)){
    	alert("Please Select User Type.");
        theForm.userType.focus();
        return false;
    }
  <? } ?>
		if(theForm.idNumber.value == "" || IsAllSpaces(theForm.idNumber.value)){
    	alert("Please provide <?=$idType ?> Number.");
        theForm.idNumber.focus();
        return false;
    }
    	if(theForm.bankName.value == "" || IsAllSpaces(theForm.bankName.value)){
    	alert("Please provide <?=$bankDetail ?>");
       theForm.bankName.focus();
        return false;
    }
    return true;
}

	function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
 
 	-->
</script>  
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">Transaction AML Report</font></strong></td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
	  <form action="trans-AML-report.php" method="post" name="Search" onSubmit="return checkForm(this);" >
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
        </tr>
        <tr>
            <td nowrap align="center">
            	  
            	  <SELECT name="userType" id="userType" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='trans-AML-report.php'; document.forms[0].submit()">
            	         <?	$senderLabel = "Sender";
					$senderLabelN = $_arrLabels[$senderLabel];						
					$AgentLabel = "Agent";
					$AgentLabelN = $_arrLabels[$AgentLabel];				
				?>
                <OPTION value="">--Select User Type---</OPTION>
                <OPTION value="sender"><?=$senderLabelN;?></OPTION>
                <OPTION value="beneficiary">Beneficiary</OPTION>
              </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].userType, "<?=$userType ?>");
		       	  </script> 	
            <SELECT name="idType" id="idType" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action='trans-AML-report.php'; document.forms[0].submit()">
                <OPTION value="">--Select ID Type---</OPTION>
                <OPTION value="Passport">Passport</OPTION>
                <OPTION value="ID Card">ID Card</OPTION>
                <OPTION value="Driving License">Driving License</OPTION>
             </SELECT>
             <script language="JavaScript">
		           	SelectOption(document.forms[0].idType, "<?=$idType ?>");
		       	  </script>
       <? if($idType!= '') { ?>
			<?=$idType ?> Number<font color="#ff0000">*</font> <input type="text" name="idNumber" id="idNumber" value="<? echo $idNumber ?>">
			<? } ?>
      </td></tr>
     <? if($userType!= '') { ?>
     <tr>
            <td nowrap align="center"> 
           Post Code <input type="text" name="postCode" id="postCode" value="<? echo $postCode ?>">
           Address <input type="text" name="address" id="address" value="<? echo $address ?>">
            
      
      </td></tr>
      <? } ?>
		       <tr>
        <td align="center" nowrap>
		From
		<?
		 $month = date("m");
		 $day = date("d");
		 $year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
				
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
        <script language="JavaScript">
         	SelectOption(document.Search.fDay, "<?=$rfDay?>");
        </script>
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
				
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?> >Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		 <script language="JavaScript">
         	SelectOption(document.Search.fMonth, "<?=$rfMonth?>");
        </script>
        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
						
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.fYear, "<?=$rfYear?>");
        </script>
        To	
        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			
          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day' " .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<script language="JavaScript">
         	SelectOption(document.Search.tDay, "<?=$rtDay?>");
        </script>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
					
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>
		<script language="JavaScript">
         	SelectOption(document.Search.tMonth, "<?=$rtMonth?>");
        </script>
        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
				
          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year'" .  ($Year == $year ? "selected" : "") . " >$Year</option>\n";
									}
		  ?>
        </SELECT>
				<script language="JavaScript">
         	SelectOption(document.Search.tYear, "<?=$rtYear?>");
        </script>
      </tr>
     <tr> <td align="center">
		<input type="submit" name="Submit" value="Search"></td>
      </tr>
	  <!-- </form> -->
    </table>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
     <!--  <form action="trans-AML-report.php" method="post" name="trans" > -->


          <?
			if (count($contentsTrans) > 0){
		?>
	 		<tr>
	    	<td  bgcolor="#000000">
				
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">	
						<? if($msg!= '') { ?>
						<tr>
							  <td colspan="2" align="center"> <font color="<? echo ($msg!= '' ? SUCCESS_COLOR : CAUTION_COLOR); ?>" size="2"><b><i><? echo ($msg != '' ? SUCCESS_MARK : CAUTION_MARK);?></b></i><strong><? echo $msg; ?></strong></font></td>
						</tr>
						<? } ?>	
	 					<tr>
							<td valign="center" align="right">Add Note/Comment</td>
							<td><textarea name="note" cols="40"><?=$compQueryMain["note"];?></textarea></td>
	        		
						</tr>  
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">	
	      	<input type="submit" name="Save" value="Save Report">
	      </td>
	    </tr>
			
      <tr>
            <td  bgcolor="#000000">
			
			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
			         
                <tr>
                  <td>
                    <?php if ($allCount > 0) {
                   
                    	?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+ count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ?>
                    </td>
                    
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$alloffset&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
		    </td>
          </tr>



	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There
              are <? echo count($contentsTrans) ?> records to View.</span></td>
        </tr>

   
   
        
		<?
		if(count($contentsTrans) > 0)
		{
			
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?></span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			  <td><span class="style1"><?=__("Sender");?> Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Created By</span></td>
		    <? if(CONFIG_TRANS_AML_AMOUNT_REPORT_DOCUMENTS == "1") { ?>
				  <td width="100"><span class="style1">&nbsp;</span></td>
			  <? }?>
			</tr>
		    				
			 <? for($i=0;$i < count($contentsTrans);$i++){ ?>

				<tr bgcolor="#FFFFFF">
					<td width="100" bgcolor="#FFFFFF"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></td>
				  
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong>
				  <font color="#006699"><? echo  $contentsTrans[$i]["refNumberIM"] ?></font></strong></a></td>
				  
				  <!--<td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td> -->
          <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  
				<?
					$contentsTrans[$i]["totalAmount"] = number_format($contentsTrans[$i]["totalAmount"], 2, '.', '');
				?>				  
				  
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"] .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  	{
				  
				   $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");  
				   $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  }else
				  {
				  	if(CONFIG_SHARE_OTHER_NETWORK == '1')
            {    
             $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");   
            }   
						if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
						{
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
							$otherClient = new connectOtherDataBase();
							$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
							
							$customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");		
							$beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");		
							$otherClient->closeConnection();
							dbConnect();
						}else{
							$customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
							$beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
						}
				  	?>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  	<td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  	<?
				  	}
				  ?>
					
				  <?
				  if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
				  {
				  $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
				  $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
				  }
				  else

				  {
				  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
				  $createdBy = ucfirst($agentContent["name"]);
				  }
				  ?>

				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo $createdBy; ?>
				  </td>
            <? if(CONFIG_TRANS_AML_AMOUNT_REPORT_DOCUMENTS == "1") { 
			?>
			  		<td bgcolor="#FFFFFF" align="center">
					<? if($contentsTrans[$i]["createdBy"]!="CUSTOMER"){?>
						<a class="style2" onClick="javascript:window.open('viewDocument.php?customerID=<?=$contentsTrans[$i]["customerID"]?>', 'ViewDocuments', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="#">
 	                        View Documents
 	                    </a>
					<? }else{?>
					&nbsp;
					<? }?>
					</td>
			  		<? } ?>			
				</tr>
				
				<?  }?>
			  
			    
			<?	
						
			} // greater than zero
			
			?>
          </table></td>
        </tr>

     <tr>
            <td  bgcolor="#000000">
            	<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td>
                    <?php if (count($contentsTrans)  > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset + count($contentsTrans));?></b>
                    of
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Previous</font></a>
                  </td>
                  <?php } ?>
                  <?php
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$alloffset&userType=$userType&idType=$idType&idNumber=$idNumber&postCode=$postCode&address=$address&fromDate=$fromDate&toDate=$toDate&Submit=$Submit";?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
                  <?php } ?>
                </tr>
              </table>
             </td>
          </tr>
         
          <?
			} else {
				if ($Submit != "") {
		?>
			<tr>
				<td align="center"><i>No transaction found according to above filter.</i></td>
			</tr>
		<?	
				}
			} 
		?>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
