<?
session_start();
include ("../include/config.php");
include ("security.php");

$date_time = date('d-m-Y  h:i:s A');
session_register("Title");
session_register("firstName");
session_register("lastName");
session_register("middleName");
session_register("IDType");
session_register("IDNumber");
session_register("Address");
session_register("Address1");
session_register("Country");
session_register("City");
session_register("Zip");
session_register("State");
session_register("Phone");
session_register("Mobile");
session_register("bentEmail");
session_register("transType");

session_register("bankName");
session_register("branchCode");
session_register("branchAddress");
session_register("swiftCode");
session_register("accNo");
session_register("ABACPF");
session_register("IBAN");

if ($_POST["bankName"] != "")
	$_SESSION["bankName"] = $_POST["bankName"];
if ($_POST["branchCode"] != "")
	$_SESSION["branchCode"] = $_POST["branchCode"];
if ($_POST["branchAddress"] != "")
	$_SESSION["branchAddress"] = $_POST["branchAddress"];
if ($_POST["swiftCode"] != "")
	$_SESSION["swiftCode"] = $_POST["swiftCode"];
if ($_POST["accNo"] != "")
	$_SESSION["accNo"] = $_POST["accNo"];
if ($_POST["ABACPF"] != "")
	$_SESSION["ABACPF"] = $_POST["ABACPF"];
if ($_POST["IBAN"] != "")
	$_SESSION["IBAN"] = $_POST["IBAN"];



if ($_POST["agentID"] != "")
	$_SESSION["agentID2"] = $_POST["agentID"];
if ($_POST["firstName"] != "")
	$_SESSION["firstName"] = $_POST["firstName"];
if ($_POST["lastName"] != "")
	$_SESSION["lastName"] = $_POST["lastName"];
if ($_POST["middleName"] != "")
	$_SESSION["middleName"] = $_POST["middleName"];
if ($_POST["Title"] != "")
	$_SESSION["Title"] = $_POST["Title"];
if ($_POST["IDType"] != "")
	$_SESSION["IDType"] = $_POST["IDType"];
if ($_POST["IDNumber"] != "")
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
if ($_POST["Address"] != "")
	$_SESSION["Address"] = $_POST["Address"];
if ($_POST["Address1"] != "")
	$_SESSION["Address1"] = $_POST["Address1"];
if ($_POST["Country"] != "")
	$_SESSION["Country"] = $_POST["Country"];
if ($_POST["transType"] != "")
	$_SESSION["transType"] = $_POST["transType"];
if ($_POST["City"] != "")
	$_SESSION["City"] = $_POST["City"];
if ($_POST["Zip"] != "")
	$_SESSION["Zip"] = $_POST["Zip"];
if ($_POST["State"] != "")
	$_SESSION["State"] = $_POST["State"];
if ($_POST["Phone"] != "")
	$_SESSION["Phone"] = $_POST["Phone"];
if ($_POST["Mobile"] != "")
	$_SESSION["Mobile"] = $_POST["Mobile"];
if ($_POST["benEmail"] != "")
	$_SESSION["benEmail"] = $_POST["benEmail"];


?>
<html>
<head>
	<title>Add beneficiary</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript">
<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function checkForm(theForm) {
	
	var strErr = "";
	strEmpty = /^\s*$/;
	strDigit = /^\d+$/;
	strEml = /^[A-Za-z0-9-_.]+\@[A-Za-z0-9-_]+.[A-Za-z0-9-_.]+$/;
	strLogin = /^[A-Za-z0-9-_]+$/
		
	if(theForm.firstName.value == "" || IsAllSpaces(theForm.firstName.value)){
    	alert("Please provide the beneficiary's first name.");
        theForm.firstName.focus();
        return false;
    }
	if(theForm.lastName.value == "" || IsAllSpaces(theForm.lastName.value)){
    	alert("Please provide the beneficiary's last name.");
        theForm.lastName.focus();
        return false;
    }
	/*if(theForm.IDNumber.value == "" || IsAllSpaces(theForm.IDNumber.value)){
    	alert("Please provide the beneficiary's ID Number.");
        theForm.IDNumber.focus();
        return false;
    }*/
	if(theForm.Address.value == "" || IsAllSpaces(theForm.Address.value)){
    	alert("Please provide the beneficiary's address line 1.");
        theForm.Address.focus();
        return false;
    }
	if(theForm.Country.options.selectedIndex == 0){
    	alert("Please select the beneficiary's country from the list.");
        theForm.Country.focus();
        return false;
    }
	if(theForm.City.value == ""){
    	alert("Please provide the beneficiary's city.");
        theForm.City.focus();
        return false;
    }
	if(theForm.Phone.value == "" || IsAllSpaces(theForm.Phone.value)){
    	alert("Please provide the beneficiary's phone number.");
        theForm.Phone.focus();
        return false;
    }
	/*if(theForm.benEmail.value != "")
	{
			if(!theForm.benEmail.value.match(strEml))
			{
				alert("Invalid email format provided.\n");					
				theForm.benEmail.focus();
				return (false);
			}	
    } */

	return true;
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }
	// end of javascript -->
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>

<table width="100%" border="0" cellspacing="1" cellpadding="5">

  <form name="frmBenificiary" action="add-ben-conf2.php?from=<? echo $_GET["from"]?>" method="post" onSubmit="return checkForm(this);">
  <tr>
      <td colspan="2" align="center" valign="top"> 
	  <table width="686" border="0" cellspacing="1" cellpadding="2" align="center">
          <tr height="30">
            <td align="left" bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">&nbsp;Add 
              beneficiary</font></b></td>
  </tr>
          <tr> 
            <td colspan="2" bgcolor="#000000"> <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add 
                    beneficiary</strong></font></td>
                </tr>
              </table></td>
          </tr>
          <? if ($_GET["msg"] != ""){ ?>
          <tr bgcolor="#ededed">
            <td colspan="2" bgcolor="#EEEEEE"><table width="100%" cellpadding="5" cellspacing="0" border="0">
                <tr>
                  <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
                  <td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b><br><br></font>"; ?></td>
                </tr>
              </table></td>
          </tr>
          <? } ?>
          <tr bgcolor="#ededed"> 
            <td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font><font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;+ Can be Compulsory Fields with respect to Country Selection </font></td>
          </tr>
          <tr> 
            <td valign="top" colspan="2" bgcolor="#ffffff">
              <table width="100%" cellpadding="2" cellspacing="1" border="0">

                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Title&nbsp;
                    </b></font></td>
                  <td width="26%" align="left"> <select name="Title">
                      <option value="Mr.">Mr.</option>
                      <option value="Miss.">Miss</option>
                      <option value="Dr.">Dr.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="Other">Other</option>
                  </select><script language="JavaScript">
         	SelectOption(document.forms[0].Title, "<?=$_SESSION["Title"]; ?>");
                                </script></td>
                  <td width="20%" align="right"><font color="#005b90"><b>First 
                    Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%" align="left"><input type="text" name="firstName" value="<?=$_SESSION["firstName"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Middle Name&nbsp;</b></font></td>
                  <td width="26%" align="left"><input type="text" name="middleName" value="<?=$_SESSION["middleName"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Last 
                    Name<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%" align="left"><input type="text" name="lastName" value="<?=$_SESSION["lastName"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right" valign="top"><font color="#005b90"><b>ID Type+&nbsp;</b></font></td>
                  <td width="26%" align="left"><input type="radio" name="IDType" value="Passport" <? if ($_SESSION["IDType"] == "Passport" || $_SESSION["IDType"] == "") echo "checked"; ?>> Passport <br>
                    <input type="radio" name="IDType" value="ID Card" <? if ($_SESSION["IDType"] == "ID Card") echo "checked"; ?>>
                    ID Card<br>
                    <input type="radio" name="IDType" value="Driving License" <? if ($_SESSION["IDType"] == "Driving License") echo "checked"; ?>>
                  Driving Licence</td>
                  <td width="20%" align="right" valign="top"><font color="#005b90"><b>ID Number+&nbsp;</b></font></td>
                  <td width="37%" align="left" valign="top"><input type="text" name="IDNumber" value="<?=$_SESSION["IDNumber"]; ?>" maxlength="32"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Address Line 1<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="26%" align="left"><input type="text" name="Address" value="<?=$_SESSION["Address"]; ?>" maxlength="254"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Address Line 2&nbsp;</b></font></td>
                  <td width="37%" align="left"><input type="text" name="Address1" value="<?=$_SESSION["Address1"]; ?>" maxlength="254"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Country<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="26%" align="left"> <select name="Country" style="font-family:verdana; font-size: 11px" >
                      <option value="">- Select Country-</option>
                      <?
                    
                    if(CONFIG_ENABLE_ORIGIN == "1"){
                	
			                	
			                		$countryTypes = " and countryType like '%destination%' ";
			                
			                                	
			                }else{
			                		$countryTypes = " ";
			                	
			                } 
                    
                    
                      
             			if(CONFIG_COUNTRY_SERVICES_ENABLED){
             				
								$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
								}
							else
								{
									
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
					//$countires = selectMultiRecords("select distinct country from ".TBL_CITIES." order by country");
					for ($i=0; $i < count($countires); $i++){
				?>
                      <option value="<?=$countires[$i]["countryName"]; ?>"> 
                      <?=$countires[$i]["countryName"]; ?>
                      </option>
                      <?
					}
				?>
                    </select> 
                  <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script></td>
                  <td width="20%" align="right"><font color="#005b90"><b>
                    <? if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom") echo "City"; else echo "Zip/postal code"; ?>
                    <font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%" align="left"><? if ($_SESSION["Country"] != "United States" && $_SESSION["Country"] != "Canada" && $_SESSION["Country"] != "United Kingdom"){ ?>
				  
								<input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25">
								
								
				  <? } else { ?>
				  <input type="text" name="Zip" value="<? echo stripslashes($_SESSION["Zip"]); ?>" maxlength="7"><? } ?></td>
                </tr>
				<? if ($_SESSION["Country"] == "United States" || $_SESSION["Country"] == "Canada" || $_SESSION["Country"] == "United Kingdom"){ ?>
				<tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>
                    <? if ($_SESSION["Country"] == "United States") echo "State"; else echo "County"; ?><font color="#ff0000">*</font>
                    &nbsp;</b></font></td>
                  <td width="26%" align="left"><input type="text" name="State" value="<?=$_SESSION["State"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>City<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="37%" align="left"><input type="text" name="City" value="<?=$_SESSION["City"]; ?>" maxlength="25"></td>
                </tr><? } ?>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Phone<font color="#ff0000">*</font>&nbsp;</b></font></td>
                  <td width="26%" align="left"><input type="text" name="Phone" value="<?=$_SESSION["Phone"]; ?>" maxlength="25"></td>
                  <td width="20%" align="right"><font color="#005b90"><b>Mobile&nbsp;</b></font></td>
                  <td width="37%" align="left"><input type="text" name="Mobile" value="<?=$_SESSION["Mobile"]; ?>" maxlength="25"></td>
                </tr>
                <tr bgcolor="#ededed"> 
                  <td width="17%" height="20" align="right"><font color="#005b90"><b>Email&nbsp;</b></font></td>
			    <td width="26%" align="left"><input type="text" name="benEmail" value="<?=$_SESSION["benEmail"]; ?>" maxlength="25"></td>
                  <td height="20" align="right"><font color="#005b90"><b>Transaction Type &nbsp;</b></font></td>
                  <td align="left">
                    <select name="transType" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].action = 'add-benificiary2.php?from=<? echo $_GET["from"]?>'; document.forms[0].submit();">
                      <option value="Select Transaction Type">- Select Transaction Type-</option>
					  <?
					  if($_SESSION["transType"] == "Bank Transfer")
					  {
					  ?>
					  <option value="Bank Transfer" selected>Bank Transfer</option>
					  <?
					  }
					  else
					  {
					  ?>
                      <option value="Bank Transfer">Bank Transfer</option>                      
					  <?
					  }
					  ?>
                    </select>
                    <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$_SESSION["transType"]; ?>");
                                </script></td>
                </tr>
				
				
				
				
				
				<tr>
				<td colspan="4" bgcolor="#ffffff">
				<?
					
			  if($_SESSION["transType"] == "Bank Transfer")
			  { ?>				
					<table width="100%" border="0" cellpadding="2" cellspacing="1" bordercolor="#006600">
										  <tr bgcolor="#ededed">
											<td height="20" colspan="4"><font color="#005b90"><b>Beneficiary Bank Details</font></td>
										  </tr>
										  <tr bgcolor="#ededed">
											<td width="150" height="20" align="right"><font color="#005b90">Bank Name<font color="#ff0000">*</font> </font></td>
											<td width="200" height="20" align="left">
											<? 
											$benCountryName = $_SESSION['Country'];
											$q = "select * from imbanks where country  = '$benCountryName'";
											$nResult = mysql_query($q)or die("Invalid query: " . mysql_error());
											$rstRow = mysql_fetch_array($nResult);						
											
											if (count($rstRow) > 1)
											{
												$q = "select distinct bankName from imbanks where country  = '$benCountryName'";
												$imBakns = selectMultiRecords($q);
												?>
												<select name='bankName' style='font-family:verdana; font-size: 11px; width:150'  >
												<?
												for($i=0;$i < count($imBakns); $i++)
												{
													if($_SESSION["bankName"] == $imBakns[$i]["bankName"])
														echo "<option value='".$imBakns[$i]["bankName"]."' selected>".$imBakns[$i]["bankName"]."</option>\n";
													else
														echo "<option value='".$imBakns[$i]["bankName"]."'>".$imBakns[$i]["bankName"]."</option>\n";
												}
												?>
												</select>
												<script language="JavaScript">SelectOption(document.addTrans.bankName, "<?=$_SESSION["bankName"]; ?>");</script>
												<?
											}
											else
											{?>
												<input name="bankName" type="text" id="bankName" value="<?=$_SESSION["bankName"]; ?>" maxlength="25">
											<?
											}
											?>
											</td>
											<td width="100" align="right"><font color="#005b90">Acc Number<font color="#ff0000">*</font> </font></td>
											<td width="200" align="left"><input name="accNo" type="text" id="accNo" value="<?=$_SESSION["accNo"]; ?>" maxlength="50"></td>
										  </tr>
										  <tr bgcolor="#ededed">
											<td width="150" height="20" align="right"><font color="#005b90">Branch Code<font color="#ff0000">*</font> </font></td>
											<td width="200" height="20" align="left"><input name="branchCode" type="text" id="branchCode" value="<? echo $_SESSION["branchCode"]; ?>" maxlength="25"></td>
											<? if(CONFIG_CPF_ENABLED == '1')
											{?>
												<td width="100" align="right"><font color="#005b90">
													<?
														if(strstr(CONFIG_CPF_COUNTRY ,$_SESSION["Country"]))
														{
															echo  "CPF Number<font color='#ff0000'>*</font>";
														}
														?>
										        &nbsp;
										    </font>
										   </td>
									        <td width="200"><?
													if(strstr(CONFIG_CPF_COUNTRY , $_SESSION["Country"]))
													{
													?>
							              <input name="ABACPF" type="text" id="ABACPF" value="<?=$_SESSION["ABACPF"]; ?>">
							              <?
													}
													?> &nbsp;</td>
											<?
											}else{
											?>
												<td width="150" height="20" align="right"><font color="#005b90"> &nbsp;</font></td>
							        	<td width="200" height="20"> &nbsp;</td>
											<?
											}
											?>
										  </tr>
										  <tr bgcolor="#ededed">
											<td width="150" height="20" align="right"><font color="#005b90">Branch Address<font color="#ff0000">*</font></font> </td>
											<td width="200" height="20" align="left"><input name="branchAddress" type="text" id="branchAddress" value="<?=$_SESSION["branchAddress"]; ?>" size="30" maxlength="254"></td>
											<td width="100" height="20">&nbsp;</td>
											<td width="200" height="20" align="left">&nbsp;</td>
										  </tr>
										  <!--tr bgcolor="#ededed">
											<td width="150" height="20" align="right"><font color="#005b90">Swift Code<font color="#ff0000">*</font></font></td>
											<td width="200" height="20" align="left"><input name="swiftCode" type="text" id="swiftCode" value="<?=$_SESSION["swiftCode"]; ?>" maxlength="25"></td>
											<td width="100" height="20" align="right" title="For european Countries only"><font color="#005b90">IBAN Number </font></td>
											<td width="200" height="20" align="left" title="For european Countries only"><input name="IBAN" type="text" id="IBAN" value="<?=$_SESSION["IBAN"]; ?>" maxlength="50"></td>
										  </tr-->
										  <?
									}
									?>
					</table>				
				
				
				
				</td>				
				</tr>
				
              </table></td>
          </tr>
          <tr bgcolor="#ededed"> 
            <td colspan="2" align="center">
			<input type="hidden" name="customerID" value="<?echo $_SESSION["customerID"]?>">
			<input name="Save" type="submit" value=" Save ">
              &nbsp;&nbsp; <input name="reset" type="reset" value=" Clear "> </td>
          </tr>
        </table>	  </td>
		
  </tr>
  
</form>
</table>


</body>
</html>
