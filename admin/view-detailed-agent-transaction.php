<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$custAgentID =  $_GET["custAgentID"];
$fromDate =  $_GET["fromDate"];
$toDate =  $_GET["toDate"];
$currencyTo = $_GET["currencyTo"];


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

if ($offset == "")
{
	$offset = 0;

}
$limit=200;

if ($_GET["newOffset"] != "") {
	
	$offset = $_GET["newOffset"];
}

if($offset == 0)
{
$_SESSION["dgrandTotal"]="";
	$_SESSION["dCurrentTotal"]="";	
	
	$_SESSION["dgrandTotal2"]="";
	$_SESSION["dCurrentTotal2"]="";		
}

$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
if ($sortBy == "")
	$sortBy = " transDate";



	$queryDate = "(t.transDate >= '".$_GET["fromDate"]." 00:00:00' and t.transDate <= '".$_GET["toDate"]." 23:59:59')";

		$query = "select * from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";
		$query .=  " and $queryDate";
		$query .= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";
   	$query .=  " and t.currencyTo like '".$currencyTo."'";
		
		$query .= " LIMIT $offset , $limit";                      
		$contentsTrans = selectMultiRecords($query);             
		
		
		$queryCnt = "select count(*) from " . TBL_TRANSACTIONS . " as t where t.custAgentID='".$custAgentID."'";   	
   	$queryCnt	.=  " and $queryDate";                                                    
   	$queryCnt	.= " and (t.transStatus !='Pending' and t.transStatus !='Processing') ";  
   	$queryCnt	.=  " and t.currencyTo like '".$currencyTo."'";       
   	
   	$allCount = countRecords($queryCnt);	   					
   						
   						
   						
?>            
<html>        
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5" >
  <tr> 
    <td class="topbar"><b><font color="#000000" size="2">View transaction details.</font></b></td>
  </tr>
  <tr> 
    <td><b><font color="#000000">Amount in this report is excluding Transaction <? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></b></td>
  </tr>
   <?
			if ($allCount > 0){
	?>
	
	<tr>
		<td colspan="11" align="center">
	<form action="view-detailed-agent-transaction.php" method="post" name="trans">
				<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
          <tr>
            <td width="477">
              <?php if (count($contentsTrans) > 0) {;?>
              Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsTrans));?></b>
              of
              <?=$allCount; ?>
              <?php } ;?>
            </td>
            <?php if ($prv >= 0) { ?>
            <td width="50"> <a href="<?php print $PHP_SELF ."?newOffset=0&sortBy=".$_GET["sortBy"]."&total=first&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo";?>"><font color="#005b90">First</font></a>
            </td>
            <td width="53" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$prv&sortBy=".$_GET["sortBy"]."&total=pre&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo"?>"><font color="#005b90">Previous</font></a>
            </td>
            <?php } ?>
            <?php
		if ( ($nxt > 0) && ($nxt < $allCount) ) {
			$alloffset = (ceil($allCount / $limit) - 1) * $limit;
	?>
            <td width="50" align="right"> <a href="<?php print $PHP_SELF ."?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&total=next&custAgentID=$custAgentID&toDate=$toDate&fromDate=$fromDate&currencyTo=$currencyTo";?>"><font color="#005b90">Next</font></a>&nbsp;
            </td>
            <td width="82" align="right"> <!--a href="<?php //print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&total=last";?>"--><font color="#005b90">&nbsp;</font><!--/a-->&nbsp;
            </td>
            <?php }
	   ?>
          </tr>
        </table>		
			</form>  
		</td>
	 </tr> 
<? 
			}
?>
  <tr> 
    <td align="center">
    	<table width="842" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
        <tr class="style1"> 
					<td>Dated</td>
          <td><? echo $systemPre;?> Number</td>
          <td>System Number</td>
          <td>Agent Name</td>
          <td>Sender Name</td>
          <td>Beneficiary Name</td>
          <td>Beneficiary Telephone</td>
          <td>Amount Sent GBP</td>
          <td>Exchange Rate</td>
          <td>Foreign Amount</td>
		  		<td>Status</td>
        </tr>
<?
if(count($contentsTrans) > 0)
 {
 	$total = 0;
   	$totalLocal = 0;
   for($i = 0; $i < count($contentsTrans); $i++)
   {      
   	
    //	$contentsTrans = selectMultiRecords($query);
		//if(count($contentsTrans) > 0)
		?>
        <tr> 
		<td> &nbsp; 
            <?  echo $contentsTrans[$i]["transDate"]; ?>
          </td>
          <td> &nbsp; 
            <?  echo $contentsTrans[$i]["refNumber"]; ?>
          </td>
          <td>
           &nbsp; <?  echo $contentsTrans[$i]["refNumberIM"]; ?>
          </td>
          <td>
           &nbsp; <? $Collection = selectFrom("select name from admin where userID='".$contentsTrans[$i]["custAgentID"]."'"); echo $Collection["name"];?>
          </td>
          <td>
           &nbsp; <? 
           
           					if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsTrans[$i]["customerID"]."'"); echo $CollCustomer["firstName"];
           						}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
           						{
           							$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'"); echo $CollCustomer["FirstName"];
           							}
           				?>
          </td>
          <td>
           &nbsp; <? 
           					if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName, lastName, Phone from beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); echo $collben["firstName"];
           				}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName, lastName, Phone from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); echo $collben["firstName"];
           				}
           					?>
          </td>
          <td>
          &nbsp;  <? echo $collben["Phone"]; ?>
          </td>
          <td>
           &nbsp; <?  echo number_format($contentsTrans[$i]["transAmount"],2,'.',','); 
           $totalLocal = $totalLocal + $contentsTrans[$i]["transAmount"];
           ?>
          </td>
          <td>
           &nbsp; <?  echo number_format($contentsTrans[$i]["exchangeRate"],2,'.',','); ?>
          </td>
          <td>
           &nbsp; <?  echo(number_format($contentsTrans[$i]["localAmount"],2,'.',',')); 
           $total = $total + $contentsTrans[$i]["localAmount"];
           ?>
          </td>
		  <td>
           &nbsp; <?  echo $contentsTrans[$i]["transStatus"]; ?>
          </td>
        </tr>
		
  <?
  	}?>
  	<tr>
			<td colspan="11" border="0" bordercolor="#FFFFFF">
				&nbsp;
			</td>
		</tr>
  	</tr>
		<tr bgcolor="#CCCCCC" class="style1">
		<td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp; Total
    </td>	
  	<td>
  		
  		&nbsp; <? echo(number_format($totalLocal,2,'.',',')); ?>
    </td>
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;<?  echo(number_format($total,2,'.',',')." ".$contentsCurrency["currencyTo"]); ?>
    </td>	
    <td>
    	&nbsp;
    </td>
  </tr>
  
  
  	<tr bgcolor="#CCCCCC" class="style1">
		<td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;
    </td>
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp; Total
    </td>	
  	<td>
  		<?
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotal"] = $totalLocal;
				$_SESSION["dCurrentTotal"]=$totalLocal;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotal"] -= $_SESSION["dCurrentTotal"];			
					$_SESSION["dCurrentTotal"]=$totalLocal;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotal"] += $totalLocal;
					$_SESSION["dCurrentTotal"]= $totalLocal;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotal"]=$totalLocal;
					$_SESSION["dCurrentTotal"]=$_SESSION["dgrandTotal"];
					}
				////For Destination Currency Total
				
				if($_GET["total"]=="first")
				{
				$_SESSION["dgrandTotal2"] = $total;
				$_SESSION["dCurrentTotal2"]=$total;
				}elseif($_GET["total"]=="pre"){
					$_SESSION["dgrandTotal2"] -= $_SESSION["dCurrentTotal2"];			
					$_SESSION["dCurrentTotal2"]=$total;
				}elseif($_GET["total"]=="next"){
					$_SESSION["dgrandTotal2"] += $total;
					$_SESSION["dCurrentTotal2"]= $total;
				}elseif($_GET["total"]=="")
					{
					$_SESSION["dgrandTotal2"]=$total;
					$_SESSION["dCurrentTotal2"]=$_SESSION["dgrandTotal2"];
					}
				
				
				?>
  		&nbsp; <? echo(number_format($_SESSION["dgrandTotal"],2,'.',',')); ?>
    </td>
    <td>
    	&nbsp;
    </td>	
    <td>
    	&nbsp;<?  echo(number_format($_SESSION["dgrandTotal2"],2,'.',',')." ".$contentsCurrency["currencyTo"]); ?>
    </td>	
    <td>
    	&nbsp;
    </td>
  </tr>
  	
  <?}
			?>
      </table></td>
  </tr>
  
  <tr>
    <td align="center">
	<div align="center">
	<form name="form1" method="post" action="">   
		<div class='noPrint'>               
			<input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
		</div>
	</form>
	</div>
	</td>
  </tr>
</table>
</body>
</html>