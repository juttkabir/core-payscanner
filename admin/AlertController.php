<?
session_start();
include ("../include/config.php");
//include ("config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

$alertID	= $_GET["alertID"];
$page 		= $_GET["page"];
$create 	= $_GET["create"];

if ($alertID != ""){
	$Mode = "Update";
	$alertData = selectFrom("select * from ".TBL_EMAIL_ALERT." where 1 and id = $alertID");
	
	/*$IDType		= $alertData["report"];
	$report		= $alertData["report"];
	$userType	= $alertData["userType"];
	$day		= $alertData["day"];
	$isEnable	= $alertData["isEnable"];
	$emailText	= $alertData["message"];*/

}
else{
	$Mode = "Add";
}


if($_GET["report"]!= ""){
	$report = $_GET["report"];
}else{
	$report = $alertData["report"];
}

if($_GET["IDType"]!= ""){
	$IDType = $_GET["IDType"];
}else{
	$IDType = $alertData["id_type_id"];
}

if($_GET["userType"]!= ""){ 
	$userType = $_GET["userType"]; 
}else{ 
	$userType = $alertData["userType"]; 
} 

if($_GET["alertType"]!= ""){
	$alertType = $_GET["alertType"];
}else{
	$alertType = $alertData["alertType"];
}

if($_GET["day"]!= ""){
	$day = $_GET["day"];
}else{
	$day = $alertData["day"];
}

if($_GET["isEnable"]!= ""){
	$isEnable = $_GET["isEnable"];
}else{
	$isEnable = $alertData["isEnable"];
}

if($_GET["emailText"]!= ""){
	$emailText = $_GET["emailText"];
}else{
	$emailText = $alertData["message"];
}

if($_GET["act"]=="add")
	$strMsg	= "Your Alert Data Added Successfully.";
elseif($_GET["act"]=="update")
	$strMsg	= "Your Alert Data Updated Successfully.";
if($_GET["dup"] == "Y")
	$strMsg = "Alert Already Exist.";
if(empty($strMsg))
	$strMsg	= "Your Alert Data Added Successfully.";

if(CONFIG_NEW_ALERT_CONTROLLER_PAGE == "1")
	$actionPage= "alertControllerAction.php";
else
	$actionPage= "alertController-conf.php";
?>
<html>
<head>
<title>Add an Alert</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./javascript/functions.js"></script>
<script type="text/javascript" src="./javascript/jquery.js"></script>
<script type="text/javascript" src="./javascript/jquery.validate.js"></script>
<script type="text/javascript">
<!-- 

$(document).ready(function(){
	$("#addAlertForm").validate({
		rules: {
			IDType: "required",
			userType: "required",
			alertType: "required",	
			day: {			
				required: true,
				number: true,
				min: "1"
			},
			emailText: "required"
		},
		messages: {
			IDType: "<br>Please select ID Type.",
			userType: "<br>Please select User Type.",
			alertType: "<br>Please select Alert Type.",
			day: {
				required: "<br>Please enter number of Days.",
				number: "<br>Please enter valid number.",
				min: "<br>Please enter a value greater than or equal to 1."
			},
			emailText: "<br>Please provide Alert Message."
		}
	});
});
function fnBody_OnLoad(){
	theForm.IDType.focus();
}

// end of javascript -->
</script>
<style type="text/css">
label.error {
  color: red;
  padding-left: 5px;
  font-weight:bold;
}
</style>
</head>
<body onLoad="fnBody_OnLoad();">
<form id="addAlertForm" action="<?=$actionPage?>?page=<?=$page?>&create=<?=$create ?>&alertID=<?=$alertID ?>" method="post" onSubmit="return checkForm(this);" name="theForm">
<input type="hidden" name="agentCity" value="<?=$_GET["agentCity"];?>">
<input type="hidden" name="benCountry" value="<?=$_GET["benCountry"];?>">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
<tr>
	<td class="topbar"><strong><font class="topbar_tex"><?=$Mode?> Alert Controller</font></strong></td>
</tr>
<tr>
	<td align="center">
		<table width="460" border="0" cellspacing="1" cellpadding="2" align="center">
			<tr> 
				<td colspan="2" bgcolor="#000000">
					<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
						<tr> 
							<td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong><?=$Mode?> Alert Controller</strong></font></td>
						</tr>
					</table>
				</td>
			</tr>
			<? if ($_GET["msg1"] != ""){ ?>
			<tr bgcolor="#EEEEEE">
				<td colspan="2" bgcolor="#EEEEEE">
					<table width="100%" cellpadding="5" cellspacing="0" border="0">
						<tr>
							<td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
							<td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$strMsg."</b><br><br></font>"; ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<? } ?>
			<tr bgcolor="#ededed"> 
				<td height="19" colspan="2" align="center"><font color="#FF0000">* Compulsory Fields</font></td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="144"><font color="#005b90"><strong>Select ID Type<font color="#ff0000">*</font></strong></font></td>				
				<td><select id="IDType" name="IDType" style="font-family:verdana; font-size: 11px">
						<option value="">- Select -</option>
						<option value="-1" <? if($IDType == '-1'){ echo "selected";}?>>All</option>
						<?php
						if(CONFIG_DB_ID_TYPES == "1")
						{
							$arrIDTypeData	= selectMultiRecords("SELECT id, title FROM id_types WHERE 1 ORDER BY title ASC");
							foreach($arrIDTypeData as $key=>$val)
							{
							?>
								<option value="<?=$val["id"];?>" <? if($IDType == $val["id"]){ echo "selected";}?>><?=$val["title"];?></option>
							<?php
							}
						}
						else
						{?>				
						<option value="Passport" <? if($report == "Passport"){ echo "selected";}?>>Passport</option>
						<option value="ID Card" <? if($report == "ID Card"){ echo "selected";}?>>ID Card</option>
						<option value="Driving License" <? if($report == "Driving License"){ echo "selected";}?>>Driving License</option>
						<option value="Other ID" <? if($report == "Other ID"){ echo "selected";}?>>Other ID</option>
						<option value="MSB" <? if($report == "MSB"){ echo "selected";}?>>MSB Expiry</option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr bgcolor="#ededed">
				<td width="144"><font color="#005b90"><strong>Select User Type<font color="#ff0000">*</font></strong></font></td>
				<td><select id="userType" name="userType" style="font-family:verdana; font-size: 11px">
						<option value="">- Select -</option>
						<option value="All" <? if($userType == "All"){ echo "selected";}?>>All</option>	
						<option value="Agent" <? if($userType == "Agent"){ echo "selected";}?>>Agent</option>
						<option value="Beneficiary" <? if($userType == "Beneficiary"){ echo "selected";}?>>Beneficiary</option>			
						<option value="Customer" <? if($userType == "Customer"){ echo "selected";}?>>Customer</option>
					</select>
				</td>
			</tr>
			<?php
			if(CONFIG_ALERT_TYPE_ON_ALERT_CONTROLLER == "1")
			{
			?>
			<tr bgcolor="#ededed"> 
				<td width="144"><font color="#005b90"><strong>Select Alert Type<font color="#ff0000">*</font></strong></font></td>
				<td><select id="alertType" name="alertType" style="font-family:verdana; font-size: 11px">
						<option value="">- Select -</option>
						<option value="Expired Ids" <? if($alertType == "Expired Ids"){ echo "selected";}?>>Expired Ids</option>				
						<option value="Future Expiry" <? if($alertType == "Future Expiry"){ echo "selected";}?>>Future Expiry</option>
					</select>
				</td>
			</tr>
			<?php
			}
			?>			
			<tr bgcolor="#ededed"> 
				<td width="144"><font color="#005b90"><strong>Select Days<font color="#ff0000">*</font></strong></font></td>
				<td align="left"><? $currentDay = date("I"); ?>
					<input type="text" id="day" name="day" value="<?=$day?>" />
				</td>
			</tr>
			<tr bgcolor="#ededed"> 
			<td width="144"><font color="#005b90"><strong>Enable<font color="#ff0000">*</font></strong></font></td>
				<td align="left">
					<select name="isEnable" style="WIDTH: 140px;HEIGHT: 18px; ">
						<option value="Y" <? if($isEnable == "Y"){ echo "selected";}?>>Yes</option>						
						<option value="N" <? if($isEnable == "N"){ echo "selected";}?>>No</option>
					</select>
				</td>
			</tr>
			<tr bgcolor="#ededed"> 
				<td width="144"><font color="#005b90"><strong>Message<font color="#ff0000">*</font></strong></font></td>
				<td><textarea id="emailText" cols="20" rows="3" name="emailText"><?=$emailText?></textarea></td>
			</tr>	
		</table>
	</td>
</tr>
<tr bgcolor="#ededed"> 
	<td colspan="2" align="center"> <input type="submit" value=" <?=$Mode?>" onClick="return checkForm(this);"></td>
	<? if($Mode!="Add"){ ?>
	<td><a href="manageAlertController.php?IDType=<?=$report?>"><strong><font color="#000000">Go Back </font></strong></a></td>
	<? } ?>
</tr>
</table>
</form>
</body>
</html>