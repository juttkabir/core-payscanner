<?
session_start();
include ("../include/flags.php");
include ("../include/config.php");
include ("security.php");
$countryBasedFlag = false;
$serviceTable = TBL_SERVICE;
if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
	$serviceTable = TBL_SERVICE_NEW;
	$contryBasedClause = " fromCountryId!='' AND toCountryId!='' AND currencyRec!='' ";
}
if($_POST["sel"] == "CountryOrigin")
{
		$nCountryOrigin =  $_POST["CountryOrigin"];
		if ($nCountryOrigin == "All")
		{
			$query = "select * from " . $serviceTable . " ";
			if($countryBasedFlag){
				$query .= " where ".$contryBasedClause;
			}
		}
		else
		{
			$query = "select * from " . $serviceTable . " where  fromCountryId = '$nCountryOrigin' ";
			if($countryBasedFlag){
				$query .= " and ".$contryBasedClause;
			}
		}
		$curencies = selectMultiRecords($query);
}
elseif($_POST["sel"] == "country")
{
		$nCountry =  $_POST["Country"];
		if ($nCountry == "All")
		{
			$query = "select * from " . $serviceTable . " ";
			if($countryBasedFlag){
				$query .= " where ".$contryBasedClause;
			}
		}
		else
		{
			$query = "select * from " . $serviceTable . " where  toCountryId = '$nCountry' ";
			if($countryBasedFlag){
				$query .= " AND ".$contryBasedClause;
			}
		}
		$curencies = selectMultiRecords($query);
}
elseif($_POST["sel"] == "Service")
{
		$nService  = $_POST["Service"];
		if ($nService == "All")
		{
			$query = "select * from " . $serviceTable . " ";
			if($countryBasedFlag){
				$query .= " where ".$contryBasedClause;
			}
		}
		elseif ($nService == "")
		{
			$query = "SELECT * FROM ".$serviceTable." where  serviceAvailable = '$nService' ";
			if($countryBasedFlag){
				$query .= " AND ".$contryBasedClause;
			}
		}
		else
		{
			$query = "SELECT * FROM ".$serviceTable." where  serviceAvailable like '%$nService%' ";	
			if($countryBasedFlag){
				$query .= " AND ".$contryBasedClause;
			}

		}
		$curencies = selectMultiRecords($query);
}
else
{
		$query = "select * from " . $serviceTable . " ";
		if($countryBasedFlag){
			$query .= " where ".$contryBasedClause;
		}
		$curencies = selectMultiRecords($query);
}
/*
$contentAgent = selectFrom("select agentCountry from " . TBL_ADMIN_USERS . " where username = '$username'");
$country = ($_GET["currency"] == "GBP" || $contentAgent["agentCountry"] == "" ? "United Kingdom" : $contentAgent["agentCountry"]);

$query = "select * from " . TBL_EXCHANGE_RATES . " where  country ='$country'";
$contentRate = selectFrom($query);
$countryRate =  $contentRate["primaryExchange"];
*/
$agentType = getAgentType();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="images/interface.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>View Service</title>
<script>
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
<script language="javascript" src="./javascript/functions.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
}
.style2 {color: #6699CC;
	font-weight: bold;
}
-->
</style></head>

<body>

<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">View Service</td>
  </tr>
  <tr>
    <td><table width="80%"  border="0">
        <tr>
          <td><fieldset>
            <legend class="style2">View Service</legend>
			  <form name="search" method="post" action="amount-transfer-method.php">
            <table border="0" align="center" cellpadding="5" cellspacing="1">
              <tr>
			  <input type="hidden" name="sel" value="">
			  <td bgcolor="#DFE6EA">
			  Originating Country <br>Name:&nbsp;
			  </td>
                <td colspan="3" bgcolor="#DFE6EA">
				<SELECT name="CountryOrigin" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'CountryOrigin'; document.forms[0].submit();">
                  <OPTION value="">--- Select Country---</OPTION>
                  <OPTION value="All" <? echo ($nCountryOrigin=="All" ? "selected" : "")?>>All</OPTION>
                  <?
					$queryCountryOrigin = "select distinct c.countryId, c.countryName from ".TBL_COUNTRY." as c, ".$serviceTable." as s where c.countryId = s.fromCountryId ";
					if($countryBasedFlag){
						$queryCountryOrigin .= " AND s.fromCountryId!='' AND s.toCountryId!='' AND s.currencyRec!='' ";
					}
					$queryCountryOrigin .= " order by c.countryName";
					$countires = selectMultiRecords($queryCountryOrigin);
					for ($i=0; $i < count($countires); $i++){
				?>
                  <OPTION value="<?=$countires[$i]["countryId"]; ?>" <? echo ($nCountryOrigin==$countires[$i]["countryId"] ? "selected" : "")?>>
                  <?=$countires[$i]["countryName"]; ?>
                  </OPTION>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	//SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>
                </td>
				
              </tr>
              <tr>
			  <td bgcolor="#DFE6EA">
			  Destination Country <br>Name:&nbsp;
			  </td>
                <td colspan="3" bgcolor="#DFE6EA">
				<SELECT name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'country'; document.forms[0].submit();">
                  <OPTION value="">--- Select Country---</OPTION>
                  <OPTION value="All" <? echo ($nCountry=="All" ? "selected" : "")?>>All</OPTION>
                  <?
					$queryCountryDest = "select distinct c.countryId, c.countryName from ".TBL_COUNTRY." as c, ".$serviceTable." as s where c.countryId = s.toCountryId ";
					if($countryBasedFlag){
						$queryCountryDest .= " AND s.fromCountryId!='' AND s.toCountryId!='' AND s.currencyRec!='' ";
					}
					$countires = selectMultiRecords($queryCountryDest);
					for ($i=0; $i < count($countires); $i++){
				?>
                  <OPTION value="<?=$countires[$i]["countryId"]; ?>" <? echo ($nCountry==$countires[$i]["countryId"] ? "selected" : "")?>>
                  <?=$countires[$i]["countryName"]; ?>
                  </OPTION>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	//SelectOption(document.forms[0].Country, "<?=$_SESSION["Country"]; ?>");
                                </script>
                </td>
				
              </tr>
              <tr>
			  
			  <td bgcolor="#DFE6EA">
			  Service Name:&nbsp;
			  </td>
                <td colspan="3" bgcolor="#DFE6EA">
				<SELECT name="Service" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'Service'; document.forms[0].submit();">
                  <OPTION value="">--- Select Service ---</OPTION>
                  <OPTION value="All" <? echo ($nService=="All" ? "selected" : "")?>>All</OPTION>
				  <? if($countryBasedFlag){
				  		$contentsService = selectMultiRecords("select DISTINCT(serviceAvailable) from ".$serviceTable." where ".$contryBasedClause." order by serviceAvailable");
				  		for($s=0;$s<count($contentsService);$s++){
					?>
	                  <option value="<?=$contentsService[$s]["serviceAvailable"]?>" <? echo (strtoupper($nService)==strtoupper($contentsService[$s]["serviceAvailable"]) ? "selected" : "")?>><?=$contentsService[$s]["serviceAvailable"]?></option>
					  <? }?>
				  <? }else{?>
                  <option value="Cash Collection" <? echo ($nService=="Cash Collection" ? "selected" : "")?>>Cash Collection</option>
                  <option value="Home Delivery" <? echo ($nService=="Home Delivery" ? "selected" : "")?>>Home Delivery </option> 
				  				<option value="Bank Deposit" <? echo ($nService=="Bank Deposit" ? "selected" : "")?>>Bank Deposit </option>
				  <? }?>
                </SELECT>
			      <script language="JavaScript">
         	//SelectOption(document.forms[0].Service, "<?=$_SESSION["Service"]; ?>");
                                </script>
                </td>
              </tr>			  
            </table>
			</form>
            <br>
            <table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <tr>
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Originating Country</strong></font></td>
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Destination Country</strong></font></td>
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Services Available</strong></font></td>
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Estimated Delivery Time</strong></font></td>
			  <?
			  if(CONFIG_REMOVE_ADMIN_CHARGES != "1" && CONFIG_CUSTOM_BANK_CHARGE != '1')
			  {
			  ?>	
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Charges on Bank Transfer</strong></font></td>
			  	<?
			  }
			  	
			  	 if(CONFIG_CURRENCY_CHARGES == '1'){ ?>
			  		<td bgcolor="#DFE6EA"><font color='#005b90'><strong>International Currency Charges</strong></font></td>
			  	<? } ?>
			  	<td bgcolor="#DFE6EA"><font color='#005b90'><strong>Destination Currency</strong></font></td>
			  </tr>
			  <? 
			  for($i=0;$i < count($curencies);$i++)
			  { 
			  	$countryFlag1 = "select countryName from ".TBL_COUNTRY." where countryId = '".$curencies[$i]["fromCountryId"]."'";
			  	$res1 = mysql_query($countryFlag1);
			  	$row1 = mysql_fetch_row($res1);
			  	$countryFlag2 = "select countryName from ".TBL_COUNTRY." where countryId = '".$curencies[$i]["toCountryId"]."'";
			  	$res2 = mysql_query($countryFlag2);
			  	$row2 = mysql_fetch_row($res2);
			  	?>
			  <tr>
			  	<td bgcolor="#DFE6EA"><? echo displayFlag($row1[0], './'); echo " <font color='#005b90'><strong>" . $row1[0] . "</strong></font>"?> </td>
			  	<td bgcolor="#DFE6EA"><? echo displayFlag($row2[0], './'); echo " <font color='#005b90'><strong>" . $row2[0] . "</strong></font>"?> </td>
			  	<td bgcolor="#DFE6EA"><? echo $curencies[$i]["serviceAvailable"]?></td>
			  	<td bgcolor="#DFE6EA"><? echo $curencies[$i]["deliveryTime"]?></td>
			  <?
			  if(CONFIG_REMOVE_ADMIN_CHARGES != "1" && CONFIG_CUSTOM_BANK_CHARGE != '1')	
			  {
			  ?>
			  	<td bgcolor="#DFE6EA"><? echo $curencies[$i]["bankCharges"]?></td>
			  	<? 
			  }	
			  	if(CONFIG_CURRENCY_CHARGES == '1'){ ?>
			  		<td bgcolor="#DFE6EA"><? echo $curencies[$i]["outCurrCharges"]?></td>
			  	<? } ?>
			  	<td bgcolor="#DFE6EA"><? echo $curencies[$i]["currencyRec"]?></td>
			  </tr>
			<? } ?>
            </table>
            <br>
          </fieldset></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
