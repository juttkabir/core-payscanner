<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("connectOtherDataBase.php");

$transID = (int) $_GET["transID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
global $isCorrespondent;
global $adminType;

$agentType = getAgentType();

if($agentType == "TELLER")
{
	$userID = $_SESSION["loggedUserData"]["cp_id"];
	$loginName = $_SESSION["loggedUserData"]["loginName"];
}
else{
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$isCorrespondent  = $_SESSION["loggedUserData"]["isCorrespondent"];
	$adminType  = $_SESSION["loggedUserData"]["adminType"];
		$loginName = $_SESSION["loggedUserData"]["username"];
}



$get=$_GET["act"];
$today = date("Y-m-d");

// Added by Niaz Ahmad #3085 at 22-04-2008
if(CONFIG_ADD_FIELDS_RECEIVING_RECEIPTS == "1"){
	$today = date("d-m-Y");
	}


if($_POST["Submit"] != "")
{
	if($_POST["Submit"] == "Submit")
	{
				if( $_POST["changeStatusTo"]  == "Failed")////Deposit Agent		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Failed')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Failed','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						// insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Failed')");
				}
				if( $_POST["changeStatusTo"]  == "Suspicious")
				{
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Suspicious', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', suspeciousDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				}
				if( $_POST["changeStatusTo"]  == "Picked up")
				{
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						if($checkQues["answer"] != $_POST["answer"])
						{
							$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
							
							}
					}else{
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
						}
					
				}
				if( $_POST["changeStatusTo"]  == "Credited")
				{
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						if($checkQues["answer"] != $_POST["answer"])
						{
							$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
							
							}
					}else{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					}
				}
				if( $_POST["changeStatusTo"]  == "Sent By Courier")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Delivered")///Withdraw Bank		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Out for Delivery")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryOutDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
				
				}
				if( $_POST["changeStatusTo"]  == "Rejected")///Deposit Agent//Cancelled		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, transAmount, customerID, createdBy, refNumberIM, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Rejected')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Rejected','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Rejected')");
					
						}
				if( $_POST["changeStatusTo"]  == "Cancelled")		
				{
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					if($amount["createdBy"] != 'CUSTOMER')
					{
						insertInto("insert into agent_account values('', '$agent', '$today', 'DEPOSIT', '$balance', '$userID', '". $_GET["transID"]."', 'Transaction Cancelled')");
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','$today','Transaction is Cancelled','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
					insertInto("insert into bank_account(aaID, bankID, dated, type, amount, modified_by, TransID, description) values('', '$bank', '$today', 'DEPOSIT', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Cancelled')");
				}
				include ("email.php");
		
	}
	
	redirect("release-trans.php?act=$get&message=$mess");
}
if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
	
	$arrCustomerNumber = selectFrom("select agentPhone from admin where userID='".$contentTrans["benAgentID"]."'");
}
else
{
	redirect("release-trans.php?act=$get");
}


if(!empty($contentTrans["benAgentID"]))
{
	$arrDistributorData = selectFrom("select name from admin where userid='".$contentTrans["benAgentID"]."'");
}

?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../styles/admin.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  	<tr>
		<td>
				<? 
					if($_GET["back"]=="release-trans")
					{?>
					<a class="style2" href="release-trans.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
					<? }elseif($_GET["back"]=="cancel_Transactions"){?>
				 <a class="style2" href="cancel_Transactions.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
				 	<? } ?>
		</td>
	</tr>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
         
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            
            <table border="0" width="100%" cellspacing="1" cellpadding="0" bgcolor="#000000">
              <tr  bgcolor="#ffffff">
                <td align="center" bgcolor="#D9D9FF" ><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              	 <td width="100%" align="center" colspan="3">
            	<strong>
					<?=$company?><br>
		            Pick-Up Reciept 
				</strong>
				<br /> 
				<? if(!empty($arrCustomerNumber["agentPhone"])) { ?>
					Customer Service Numbers: <?=$arrCustomerNumber["agentPhone"]?>
				<? } ?>
            </td>
              </tr>
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Date</font></td>
              	<td width="200">
              		<? echo $today; ?>
              	</td>
              	 <!-- Added by Niaz Ahmad againist ticket # 2533 at 10-10-2007 -->
                <? if(CONFIG_SHOW_REFERENCE_CODE_BOX == "1"){ ?>
              	<td width="150" align="left"><font color="#005b90"><?=SYSTEM_CODE?></font></td>
              	<td width="200">
					<? if(SYSTEM != "cpexpress") { ?>
					<table border="1">
						<tr>
							<td align="center">
								<strong>
									<? echo $contentTrans["refNumberIM"]; ?>
								</strong>
							</td>
						</tr>
					</table>
					<? 
						}
						else
							echo $contentTrans["refNumberIM"]; 
					?>
				</td>
              	<? } else {?>
              	 <td width="150" align="left"><font color="#005b90"> <? if(CONFIG_DIST_REF_NUMBER == '1'){ echo CONFIG_DIST_REF_NAME; }else{ ?>Transaction No <? }?></font></td>
              	<td width="200"><? if(CONFIG_DIST_REF_NUMBER == '1'){ echo $contentTrans["distRefNumber"]; }else{ echo $contentTrans["refNumberIM"]; } ?></td>
              	<? } ?>
            </tr>
            <tr bgcolor="#ffffff">

             	<td width="100" align="left" style="color:#005b90">Distributor Name</td>
              	<td width="250"><?=$arrDistributorData["name"]?></td>
              	<td width="150" align="left"><font color="#005b90"><? echo $manualCode;?></font></td>
              	<td width="200" <? if(SYSTEM == "cpexpress") { ?> style="font-size:14px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; " <? } ?>>
              		<? echo($contentTrans["refNumber"]); ?>
              	</td>           	
            </tr>
            
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Sender Detail</strong></font></td>
              	
            </tr>
            <? 
            
          if(CONFIG_SHARE_OTHER_NETWORK == '1')
          {    
           $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentTrans["transID"]."' and generatedLocally = 'N'");   
          }   
					if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
					{
						
						$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
						$otherClient = new connectOtherDataBase();
						$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
						
						$customerContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentTrans["customerID"]."'");		
						$benificiaryContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentTrans["benID"]."'");		
							
							
							$custId = $customerContent["customerID"];
							$fullNameCust = $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"];
							$addressCust =  $customerContent["Address"]." ".$customerContent["Address1"];
							$zipCust = $customerContent["Zip"];
							$phoneCust = $customerContent["Phone"];
							
						
							$benID = $benificiaryContent["benID"];
							$fullNameBen = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
							$phoneBen = $benificiaryContent["Phone"];
							$destCodeBen = $benificiaryContent["City"];
							$destCityBen = $benificiaryContent["City"];
							$addressBen = $benificiaryContent["Address"];
							$idTypeBen = $benificiaryContent["IDType"];
							$idNoBen = $benificiaryContent["IDNumber"];
							$sof = $benificiaryContent["SOF"];
												
					}else{
						
								if($createdBy != 'CUSTOMER')
								{
									
									/////Customer Details//////
									$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
									$customerContent = selectFrom($queryCust);
										///////Beneficiary Details/////////
									$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, IDNumber,SOF  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
									$benificiaryContent = selectFrom($queryBen);

									
        				$custId = $customerContent["customerID"];
          			$fullNameCust	=	$customerContent["firstName"]	.	"	"	.	$customerContent["middleName"] . " " . $customerContent["lastName"];
          			$addressCust =	$customerContent["Address"]."	".$customerContent["Address1"];
          			$zipCust = $customerContent["Zip"];
          			$phoneCust = $customerContent["Phone"];
          			
          		
          			$benID = $benificiaryContent["benID"];
          			$fullNameBen = $benificiaryContent["firstName"]	.	"	"	.	$benificiaryContent["middleName"]	.	"	"	.	$benificiaryContent["lastName"];
          			$phoneBen	=	$benificiaryContent["Phone"];
          			$destCodeBen = $benificiaryContent["City"];
          			$destCityBen = $benificiaryContent["City"];
          			$addressBen	=	$benificiaryContent["Address"];
          			$idTypeBen = $benificiaryContent["IDType"];
          			$idNoBen = $benificiaryContent["IDNumber"];
          			$sof = $benificiaryContent["SOF"];
			
					
								}elseif($createdBy == 'CUSTOMER')
								{
								/////Customer Details//////
								$queryCust = "select c_id, c_name, Title, FirstName, LastName, MiddleName, c_address, c_address2, c_city, c_state, c_zip, c_country, c_phone, c_email,dob  from ".TBL_CM_CUSTOMER." where c_id ='" . $contentTrans[customerID] . "'";
								$customerContent = selectFrom($queryCust);
									///////Beneficiary Details/////////
								$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, NICNumber from  ".TBL_CM_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
								$benificiaryContent = selectFrom($queryBen);
								
								$custId = $customerContent["customerID"];
								$fullNameCust = $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"];
								$addressCust =  $customerContent["Address"]." ".$customerContent["Address1"];
								$zipCust = $customerContent["Zip"];
								$phoneCust = $customerContent["Phone"];
								
							
								$benID = $benificiaryContent["benID"];
								$fullNameBen = $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"];
								$phoneBen = $benificiaryContent["Phone"];
								$destCodeBen = $benificiaryContent["City"];
								$destCityBen = $benificiaryContent["City"];
								$addressBen = $benificiaryContent["Address"];
								$idTypeBen = $benificiaryContent["IDType"];
								$idNoBen = $benificiaryContent["IDNumber"];

							}
						}
										  

			if($custId != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Full Name</font></td>
              	<td width="200" colspan="3">
              		<? echo $fullNameCust; ?>
              	</td>              	
            </tr>
             <!--tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="200" colspan="3">
              		<? echo $addressCust; ?>
              	</td>              	
            </tr>
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Postal Code / Zip</font></td>
              	<td width="200">
              		<? echo $zipCust; ?>
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200">
              		<? echo $phoneCust; ?>
              	</td>
            </tr-->
            <?
        }
            ?>
            
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4" height="25"><font color="#005b90"><strong>Beneficiary Detail</strong></font></td>
             </tr>
              <? 
		
			
			if($benID != "")
			{
		?>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Beneficiary Name</font></td>
              	<td width="200" colspan="3">
              		<? echo $fullNameBen; ?>
              	</td>
             </tr>
            <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Address</font></td>
              	<td width="200" colspan="3">
              		<? echo $addressBen; ?>
              	</td>              	
            </tr>
						<tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Telephone No</font></td>
              	<td width="200" colspan="3">
              		<? echo $phoneBen; ?>
              	</td>
            </tr>
             
          
             
               <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong>Remittance Details</strong></font></td>
              	
            </tr>
            
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Destination Code</font></td>
              	<td width="200">
              		<? echo $destCodeBen; ?> Office
              	</td>
              	 <td width="150" align="left"><font color="#005b90">Destination</font></td>
              	<td width="200">
              		<? echo $destCityBen; ?> Office
              	</td>
            </tr> 
            
               <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Transfer Currency</font></td>
              	<td width="200">
              		<? echo $contentTrans["currencyFrom"]?>
              	</td>
              		<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType == 'SUPI' || $agentType == 'SUBI')){?>
              		 <td width="150" align="left"></td>
              		 <td width="150" align="left"></td>
              		
              		<? }else{ ?>
              	 <td width="150" align="left"><font color="#005b90">Amt Transfer</font></td>
              	<td width="200">
              		<? echo $contentTrans["transAmount"]?> 
              	</td>
              	<? } ?>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Paid In</font></td>
              	<td width="200">
              		<? echo $contentTrans["currencyTo"]?>
              	</td>
              		<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT == "1" && ($agentType == 'SUPI' || $agentType == 'SUBI')){?>
              		<td width="150" align="left"></td>
              		 <td width="150" align="left"></td>
              		<? }else{ ?>
              	 <td width="150" align="left"><font color="#005b90">Exchange Rate</font></td>
              	<td width="200">
              		<? echo $contentTrans["exchangeRate"]?> 
              	</td>
              	<? } ?>
            </tr> 
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">&nbsp;</font></td>
              	<td width="200">
              		<? //echo $contentTrans["IMFee"]Commission ?> 
              	</td>
              	 <td width="150" align="left"><font color="#005b90">In <?=$contentTrans["currencyTo"]?></font></td>
              	<td width="200">
              		<? echo $contentTrans["localAmount"]?> 
              	</td>
            </tr> 
              <tr bgcolor="#ffffff">
              	 <td width="150" align="left" colspan="4"  height="25"><font color="#005b90"><strong></strong></font></td>
              	
            </tr>        
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">ID Description</font></td>
              	<td width="200">
              		<? echo $idTypeBen; ?>&nbsp;
              	</td>
              	 <td width="150" align="left"><font color="#005b90">ID Serial no</font></td>
              	<td width="200">
              		<? echo $idNoBen; ?>&nbsp;
              	</td>
            </tr> 
            <?
        }            
            ?>
           
             <tr bgcolor="#ffffff">
              	 <td width="150" align="left"><font color="#005b90">Cashier Number</font></td>
              	<td width="200">&nbsp;<? echo($loginName); ?>
              		
              	</td>
              	 <td width="150" align="left"><font color="#005b90"><i>Signature</i></font></td>
              	<td width="200">&nbsp;
              		
              	</td>
            </tr>             
             
            </table>
          
            <br>
            </fieldset></td>
           
          </tr>         
       <? if(CONFIG_BENEFICIARY_TERMS == '1'){ ?>   
        <tr>
               
				  <td align="left">
				      Beacon Crest will pay the beneficiary set out above on production, by the beneficiary, of valid personal identification. Beacon Crest reserves the right to withhold any payment from any beneficiary who is unable to satisfy reasonable identification procedures.<br />The rate of exchange established on the day on which the money transfer was received, will be the rate upon which the currency amount will be paid to the beneficiary.   
              <font color="#000000"></font>
                        
				  </td>
              </tr>
              <?
            }
              ?>
           <tr>
               
				  <td align="center">
				         
                            <input type="submit" name="Submit2" value="Print This Transaction" onClick="print()">
                        
				  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>