<?	if ($_GET["page"] == "custben") {  ?>
Follow the instructions to correctly upload either senders or beneficiaries:
<br><br>
NOTE: The upload file needs to be created in excel. 
<br><br>
1. Please click on the "Sample Template" link.<br>
2. The fields in Bold are compulsory fields. Therefore, make sure, that you fill in the corresponding fields.<br>
3. The fields not in Bold are optional fields. Although it is highly recommended that these fields be filled in.<br>
4. Date should be entered in  (yyyy-mm-dd) format.   
<?	} elseif ($_GET["page"] == "exchrate") {  ?>
Follow the instructions to correctly upload the exchange rate:
<br><br>
1. Please click on the "Sample Template" link to download sample file.<br>
2. The upload file needs to be created in excel.<br>
3. The excel file should be same as Sample Template
<?	}  ?>