<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include("calculateBalance.php");
include ("javaScript.php");
$agentType = getAgentType();

///////////////////////History is maintained via method named 'activities'
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];
$_SESSION["agentID2"];
if($_POST["agent_Account"]!="") {
	$_SESSION["agentID2"] = $_POST["agent_Account"];
} else {
	$_SESSION["agentID2"] = "";
}

$condition = true;
	
if ($_POST["Deposit"] == "Deposit") { // [by Jamshed]
	if (CONFIG_BACKDATING_PAYMENTS == "1") {
		if ($_POST["dDate"] != "") {
			$dDate = explode("/", $_POST["dDate"]);
			$today = $dDate[2] . "-" . $dDate[1] . "-" . $dDate[0];
			if ($today > date("Y-m-d")) {
				$condition = false;	
			}
		} else {
			$today = date("Y-m-d");
		}
	} else {
		$today = date("Y-m-d");
	}
}

?>
<html>
<head>
<title>Add into Agent's Account</title>
<script>
var checkflag = "false";
function check(field) {
if (checkflag == "false") {
  for (i = 0; i < field.length; i++) {
  field[i].checked = true;}
  checkflag = "true";
  return "Uncheck all"; }
else {
  for (i = 0; i < field.length; i++) {
  field[i].checked = false; }
  checkflag = "false";
  return "Check all"; }
}

function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function delete_confirm(id)
{
  if(confirm("Are you sure you want to delete this item?"))
    return true;
  else
    return false;
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="EEEEEE">
  <form name="form1" method="post" action="direct_add_agent_account.php">
    <tr>
    	<td><div align="right"><font color="#005b90">Select Agent</font></div></td> 
      <td> 
      	<select name="agent_Account" style="font-family:verdana; font-size: 11px">
          <option value="">- Select Agent-</option>
          <?
					$agents = selectMultiRecords("select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' order by agentCompany");
					for ($i=0; $i < count($agents); $i++){
					?>
             <option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
          <?
						}
					?>
                </select>
                <script language="JavaScript">
         		SelectOption(document.forms[0].agent_Account, "<?=$_SESSION["agentID2"]; ?>");
                                </script></td>
    </tr>
    <tr> 
      <td><div align="right"><font color="#005b90">Add Amount</font></div></td>
      <td><input type="text" name="money"></td>
    </tr>
    <?	if (CONFIG_BACKDATING_PAYMENTS == "1") {  ?>
    <tr> 
      <td><div align="right"><font color="#005b90">Date &nbsp;&nbsp;</font></div></td>
      <td>
      	<input type="text" name="dDate" readonly>&nbsp;
      	<a href="javascript:show_calendar('form1.dDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>
      </td>
    </tr>
    <?	}  ?>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Deposit" value="Deposit"></td>
    </tr>
	 <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </form>
  <?
if($_POST["Deposit"] =="Deposit")
{
	if ($condition) {
		
		 $isAgentAnD = 'N';
		 
		 //
		 $agentContents = selectFrom("select userID, balance, isCorrespondent from " . TBL_ADMIN_USERS . " where userID = '".$_SESSION["agentID2"]."'");
		 $currentBalance = $agentContents["balance"];
		 
		 if(CONFIG_AnD_ENABLE == '1' && $agentContents["isCorrespondent"] == 'Y')
		 {
		 	$isAgentAnD = 'Y';
		 }
 
 
 //$currentBalance = $_POST["money"] + $currentBalance;
 //update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '".$_SESSION["agentID2"]."'");
 
 ////////////////Either to insert into Agent or A&D Account////////
 if($isAgentAnD == 'Y')
 {
 	$insertQuery="insert into ".TBL_AnD_ACCOUNT." (agentID, dated, type, amount, modified_by, description, status) values('".$_SESSION["agentID2"]."', '$today', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', 'Manually Deposited','Provisional')";
 	$q=mysql_query($insertQuery);	
	$insertedID = @mysql_insert_id();
	
	$descript = "Amount ".$_POST["money"]." is added in Agents Account ";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,TBL_AnD_ACCOUNT,$descript);

 }else{
  
	$insertQuery="insert into agent_account (agentID, dated, type, amount, modified_by, description, status) values('".$_SESSION["agentID2"]."', '$today', 'DEPOSIT', '".$_POST["money"]."', '$changedBy', 'Manually Deposited','Provisional')";
	
	$q=mysql_query($insertQuery);	
	$insertedID = @mysql_insert_id();
	
	$descript = "Amount ".$_POST["money"]." is added in Agents Account ";
	activities($_SESSION["loginHistoryID"],"INSERTION",$insertedID,"agent_account",$descript);
	}
	


if($q)
	{?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" colspan="2"><div align="center"><font color="#990000">You 
        have successfully added 
        <? echo number_format($_POST["money"],2,'.',',');?>&nbsp;&nbsp;
        </font></div></td>
  </tr>
  <? 
  }
} else {
?>
  <tr> 
      <td height="32" bgcolor="#CCCCCC" colspan="2"><div align="center"><font color="<? echo CAUTION_COLOR ?>">
      	Enter Date should not be greater than today Date.
      	</font></div></td>
  </tr>
<?
}	
} 
?>
	<tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>