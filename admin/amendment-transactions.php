<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType = getAgentType();
$agentID = $_SESSION["loggedUserData"]["userID"];

$_SESSION["back"] = "";
$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
		$offset = 0;
		
if($limit == 0)
	$limit=50;

	if ($_GET["newOffset"] != "") {
		$offset = $_GET["newOffset"];
	}
	
if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if ( CONFIG_CUSTOM_CUSTOMER_TRANSACTION == 1 )
{
	$customerTransactionPage = CONFIG_CUSTOMER_TRANSACTION_PAGE;
}
else
{
	$customerTransactionPage = "add-customer-transaction.php";
}
	
$moneyPaid = "";

	$nxt = $offset + $limit;
	$prv = $offset - $limit;
	/*
	To empty transaction Variables
	
	*/
	 		$_SESSION["transType"] 		= "";
			$_SESSION["benAgentID"] 	= "";
			$_SESSION["customerID"] 	= "";
			$_SESSION["benID"] 			= "";
			$_SESSION["moneyPaid"] 		= "";
			$_SESSION["transactionPurpose"] = "";
			$_SESSION["other_pur"] = "";
			$_SESSION["fundSources"] 	= "";
			$_SESSION["refNumber"] 		= "";
	
			$_SESSION["transAmount"] 	= "";
			$_SESSION["exchangeRate"] 	= "";
			$_SESSION["exchangeID"] 	= "";
			$_SESSION["localAmount"] 	= "";
			$_SESSION["totalAmount"] 	= "";
			$_SESSION["IMFee"] 			= "";
	
			// resetting Session vars for trans_type Bank Transfer
			$_SESSION["bankName"] 		= "";
			$_SESSION["branchCode"] 	= "";
			$_SESSION["branchAddress"] 	= "";
			$_SESSION["swiftCode"] 		= "";
			$_SESSION["accNo"] 			= "";
			$_SESSION["ABACPF"] 		= "";
			$_SESSION["IBAN"] 			= "";
			
			$_SESSION["question"] 			= "";
			$_SESSION["answer"]     = "";	
			$_SESSION["tip"]     = "";	
			$_SESSION["currencyCharge"] = "";
			$_SESSION["transDate"]="";
	
if($_POST["Submit"] == "Search" || $_GET["search"] == "Search")
{
	$query = "select * from ". TBL_TRANSACTIONS." as t where 1 ";
	$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS." as t where 1 ";

	if ($_POST["moneyPaid"] != "") {
		$moneyPaid = $_POST["moneyPaid"];
	} else if ($_GET["moneyPaid"] != "") {
		$moneyPaid = $_GET["moneyPaid"];
	}
	
	if($_REQUEST["transID"] != "")
	{
		$id = $_REQUEST["transID"];
	}
	if($_REQUEST["searchBy"] != "")
	{
		$by = $_REQUEST["searchBy"];
	}
	if($_REQUEST["transID"] != "")
	{
		$transID = $_REQUEST["transID"];
	}
			
	if($_POST["nameType"] != ""){
		$nameType = $_POST["nameType"];
	}elseif($_GET["nameType"] != ""){
		$nameType = $_GET["nameType"];
	}
	
	if($id != "")
	{
		switch($by)
		{
			case 0:
			{
				$query = "select *  from ". TBL_TRANSACTIONS . " as t where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ";
				$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " as t where (t.refNumber = '$id' OR  t.refNumberIM = '$id') ";
				break;
			}
			case 1:
			{
			
				if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$id);
										$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
									$nameClause = " ".$nameType." LIKE '".$id."%'";
									
									}
					
					
								$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and ".$nameClause." ";
								$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and ".$nameClause." ";
					}else{
							// searchNameMultiTables function (Niaz)
				       $fn="firstName";
							 $mn="middleName";
							 $ln="lastName"; 		
							 $alis="c";
							 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
							 
								//$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID and (c.firstName like '$id%' OR c.lastName like '$id%') and t.createdBy != 'CUSTOMER' ";
								$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID ".$q." ";
								$queryCnt = "select count(transID)  from ". TBL_TRANSACTIONS . " t, ". TBL_CUSTOMER ." c where t.customerID =  c.customerID ".$q." ";
				}
				break;
			}
			case 2:
			{
					
						if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){
					
					
								if($nameType == "fullName"){
									
										$name = split(" ",$id);
										$nameClause = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
									
								}else{
									
									$nameClause = " ".$nameType." LIKE '".$id."%'";
									
									}
									
									$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and $nameClause ";
									$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID and $nameClause ";
									
								}else{
					
					// searchNameMultiTables function (Niaz)
       $fn="firstName";
			 $mn="middleName";
			 $ln="lastName"; 		
			 $alis="b";
			 $q=searchNameMultiTables($id,$fn,$mn,$ln,$alis);
			 
			$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID ".$q." ";
			$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " t, ". TBL_BENEFICIARY ." b where t.benID =  b.benID ".$q." ";
			}
			break;
			}
			case 3:
			{
				$query = "select *  from ". TBL_TRANSACTIONS . " t, ". TBL_ADMIN_USERS ." u where t.custAgentID =  u.userID and u.name like '$id%' ";
				$queryCnt = "select count(transID) from ". TBL_TRANSACTIONS . " t, ". TBL_ADMIN_USERS ." u where t.custAgentID =  u.userID and u.name like '$id%' ";
				break;
			}

		}
		 
			 		
	}
	// Forcefully not displaying Transactions of Online Sender
	$query .= " and t.createdBy != 'CUSTOMER' ";
	$queryCnt .= " and t.createdBy != 'CUSTOMER' ";
	
	if($agentType == "Branch Manager"){
		$query .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID ) ";
		$queryCnt .= " and (custAgentParentID ='$agentID' OR t.benAgentParentID =  $agentID) ";
	}
	if($agentType == "Branch Manager"){
		$query .= " and custAgentParentID ='$agentID' ";
		$queryCnt .= " and custAgentParentID ='$agentID' ";
	}
	
	if ($moneyPaid != "") {
		$query .= " and (t.moneyPaid = '".$moneyPaid."')";
		$queryCnt .= " and (t.moneyPaid = '".$moneyPaid."')";
	}
	if(CONFIG_SHARE_OTHER_NETWORK == '1')
	{
		$query .= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N')";	
		$queryCnt.= " and t.transID not in (select localTrans from ".TBL_SHARED_TRANSACTIONS." where generatedLocally = 'N') ";
	}

$query .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') ";	
$queryCnt .= " and (t.transStatus ='Authorize' || t.transStatus ='Amended') ";	
$query .= "  order by t.transDate DESC";

$countRec = countRecords($queryCnt);
$totalContent = $countRec;
$query .= " LIMIT $offset , $limit";
$contentsTrans = selectMultiRecords($query);
if($_GET["allCount"] == ""){
	$allCount = $countRec;
}else{
	$allCount = $_GET["allCount"];
}

$rangeOffset = count($contentsTrans)+$offset;
/*$totalContent = selectMultiRecords($query);
//echo(" Agents are -->".count($totalContent)."--");
 $allCount = count($totalContent)+ $countOnlineRec;
 $other = $offset + $limit;
 if($other > $countOnlineRec)
 {
	if($offset < $countOnlineRec)
	{
		$offset2 = 0;
		$limit2 = $offset + $limit - $countOnlineRec;	
	}elseif($offset >= $countOnlineRec)
	{
		$offset2 = $offset - $countOnlineRec;
		$limit2 = $limit;
	}
  $query .= " LIMIT $offset2 , $limit2";

	$contentsTrans = selectMultiRecords($query);
 }*/
}


$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

?>
<html>
<head>
	<title>Amend Transactions</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>


<script language="JavaScript">
<!--

function nameTypeCheck(){
	
	
			
			
			
			if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
					
				
				document.getElementById('nameTypeRow').style.display = '';
			}	else{
				
				
				document.getElementById('nameTypeRow').style.display = 'none';
				}
			
}

-->
</script>
    <style type="text/css">

<table>
	<tr>
.style2 {
	color: #005B90;
	font-weight: bold;
}
-->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="C0C0C0"><strong><font color="#FFFFFF" size="2">Amend Transactions</font></strong></td>
  </tr>
  	<tr>
		<td>
			<table width="255" border="1" cellpadding="5" bordercolor="#666666" align="center">
        <form action="amendment-transactions.php" method="post" name="Search">
          <tr>
            <td width="286" bgcolor="#C0C0C0"><span class="tab-u"><strong>Search</strong></span></td>
          </tr>
          <tr>
            <td align="center" nowrap>
            	<input name="transID" type="text" id="transID" value="<?=$id?>" size="15" style="font-family:verdana; font-size: 11px; width:100"> 
              <select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
                  <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
                  <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By Sender Name</option>
                  <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
						<?	if($agentType == "admin" || $agentType == "System" || $agentType == "Call" || $agentType == "Admin Manager")
							{
							?>
							 	<option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Agent Name</option>
							<?
							}
						?>
              </select>
		<?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
			<br>Payment Mode
      <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;" >
          <option value="">- Select Mode -</option>
          <option value="By Cash" <? echo ($moneyPaid == "By Cash" ? "selected" : "") ?>>By Cash</option>
          <option value="By Cheque" <? echo ($moneyPaid == "By Cheque" ? "selected" : "") ?>>By Cheque</option>
          <option value="By Bank Transfer" <? echo ($moneyPaid == "By Bank Transfer" ? "selected" : "") ?>>By Bank Transfer</option>
		  </select>
		<?	}  ?>
	</tr>
	
	<? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>
   	
			   	<tr align="center" id="nameTypeRow"><td>
			   		Name Type: <select name="nameType">
			   			<option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
			   			<option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
			   			<option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
   		
  	</td></tr>
  	<?} ?>
	
	
	<tr>
		<td align="center">
                <input type="submit" name="Submit" value="Search"></td>
 				</td>      
          </tr>
        </form>
      </table>
		</td>
	</tr>
  <tr>
    <td align="center"><? //if($agentType!='admin'){echo ("You can only update the transaction within one hour of creation.");}?><br>      <br>
		
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="amendment-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">
			<tr>
    		<td>
    			<table width="700" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr>
                  <td width="250">
                    <?php if (count($contentsTrans) > 0) {?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($rangeOffset);?></b>
                    of
                    <?=$allCount; ?>
                  </td>
                <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a>
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>
                  </td>
				  				
                <?php } ?>
                <?php
								if ( ($nxt > 0) && ($nxt < $allCount) ) {
									$alloffset = (ceil($allCount / $limit) - 1) * $limit;
								?>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&search=Search&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp;
                  </td>
                  <td width="50" align="left"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&transID=$id&moneyPaid=$moneyPaid&searchBy=$by&search=Search&nameType=".$nameType."&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp;
                  </td>
				 					
                  <?php } 
                	}
                  ?>
                </tr>
              </table>
    		</td>
    	</tr>
	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There 
              are <? echo(count($contentsTrans));?> records to Update.</span></td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td><span class="style1"><? echo $systemCode; ?> </span></td>
			  <td><span class="style1"><? echo $manualCode; ?></span></td>
			  <td><span class="style1">Status</span></td>
				<?
					if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
					{
						?>
							<td><span class="style1">Cash Amount</span></td>
							<td><span class="style1">Cheque Amount</span></td>
						<?
					}
				?>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
			   <td><span class="style1">Sender Name </span></td>
			  <td><span class="style1">Beneficiary Name </span></td>
			  <td width="100"><span class="style1">Sender Agent</span></td>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
		    <? for($i=0;$i < count($contentsTrans);$i++)
			{
				
				if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
					$customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
						$onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");
				
								if ($customerDetails["customerStatus"] == "Disable"){
									
														$fontColor = "#CC000";
														$custStatus = "Disable";
									}elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){
										
														$fontColor = "#006699";
														$custStatus = "Enable";
										
										}
										
										
										
									
									}else{
									$fontColor = "#006699";
									}
				
				?>
				
				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>&act=Amended','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="<? echo $fontColor;?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
					<?
						if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
						{
							$cashAmount = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["chequeAmount"];
							$chequeAmt = ($contentsTrans[$i]["chequeAmount"] > 0 ? $contentsTrans[$i]["chequeAmount"] : 0);
							?>
								<td><?=number_format($cashAmount, 4); ?></td>
								<td><?=number_format($chequeAmt, 4);?></td>
							<?
						}
					?>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"])?></td>
				  
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER") {
                $transBene = selectFrom("select * from cm_beneficiary  where benID='".$contentsTrans[$i]["benID"]."'");
		 						$benefName = $transBene["firstName"];
		 					  $benefName .= " ";
							  $benefName .= $transBene["lastName"];
							  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
               $customerContent = selectFrom("select firstName, lastName,status from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");?>
              <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
              <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
              <? }else{?>
				  	<? $customerContent = selectFrom("select firstName, lastName,customerStatus from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <? $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  <? }?>
				  
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
				  <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER") {
				  			if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER && $customerContent["status"] == "Disable"){ ?>
												<td width="50" align="center" bgcolor="#FFFFFF">&nbsp;</td>
									<? }else{ ?>
				  	  					<td width="50" align="center"><a href="<?=$customerTransactionPage?>?transID=<? echo $contentsTrans[$i]["transID"]?>&act=Amended&back=amendment-transactions.php"><strong><font color="#006699">Amend</strong></font></a></td>
				  	  
					<? }
					
					}else{ 
						
							if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER && $customerContent["customerStatus"] == "Disable"){ ?>
												<td width="50" align="center" bgcolor="#FFFFFF">&nbsp;</td>
									<? }else{ ?>
						
				  <td width="50" align="center"><a href="<?=$transactionPage?>?transID=<? echo $contentsTrans[$i]["transID"]?>&act=Amended&back=amendment-transactions.php"><strong><font color="#006699">Amend</strong></font></a></td>
				  <? }} ?>
			    </tr>
				<?
			}
			?>

			<? for($i=0;$i < count($onlinecustomer);$i++)
			{
				?>
               <tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $onlinecustomer[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
				  <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
				  <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["totalAmount"])?></td>
				  
				  <? 
				  if($onlinecustomer[$i]["createdBy"] == "CUSTOMER") {
                $transBene = selectFrom("select * from cm_beneficiary  where benID='".$onlinecustomer[$i]["benID"]."'");
		 						$benefName = $transBene["firstName"];
		 					  $benefName .= " ";
							  $benefName .= $transBene["lastName"];
							  $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
               $customerContent = selectFrom("select firstName, lastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");?>
              <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
              <td width="100" bgcolor="#FFFFFF"><? echo $benefName; ?></td>
              <? }else{?>
		  
				  	<? $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
				  <? $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
				  <? } ?>
				  
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				  <? echo ucfirst($agentContent["name"])?></td>
				  
				  <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER") {?>
				  	  <td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$customerTransactionPage;?>?transID=<? echo $onlinecustomer[$i]["transID"]?>&act=Amended&back=amendment-transactions.php" class="style2"><strong><font color="#006699">Amend</strong></font></a></td>
					<? }else{ ?>
						<td width="50" align="center" bgcolor="#FFFFFF"><a href="<?=$transactionPage?>?transID=<? echo $onlinecustomer[$i]["transID"]?>&act=Amended&back=amendment-transactions.php"><strong><font color="#006699">Amend</strong></font></a></td>
						<? 
						}
						?>
			    </tr>
                <?
			}
			?> 
			
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  <td width="50" align="center">&nbsp;</td>
			  </tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
    </table></td>
  </tr>

</table>
</body>
</html>