<?
	session_start();
	
	$date_time = date('d-m-Y  h:i:s A');
	include ("../include/config.php");
	include ("security.php");
	//debug($_REQUEST);
	/**
	 * Implementing JSON for serialized data transfer over HTTP/HTTPS.
	 * The jQuery's plugin jqGrid allows JSON and XML based data transmission.
	 * The JSON.php is a standard class which encodes/decodes JSON data requests.
	 */
	include ("JSON.php");

	$response = new Services_JSON();

	$page = $_REQUEST['page']; // get the requested page 
	$limit = $_REQUEST['rows']; // get how many rows we want to have into the grid 
	$sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort 
	$_REQUEST['sord'] = 'desc';
	$sord = $_REQUEST['sord']; // get the direction 
	if($_REQUEST['stopRule']=="true"){
		$payIds=explode(',',$_REQUEST['payId']);
		for($i=0;$i<count($payIds);$i++){
			update("update recurring_transactions set Status='Close' where transID='".$payIds[$i]."'");
			$descript="Recurring Rule transID ".$payIds[$i]." Stop";
			activities($_SESSION["loginHistoryID"],"STOP",$payIds[$i],TBL_TRANSACTIONS,$descript);
		}
	}
	
	if($_REQUEST['resumeRule']=="true"){
		$payIds=explode(',',$_REQUEST['payId']);
		for($i=0;$i<count($payIds);$i++){
			update("update recurring_transactions set Status='Active' where transID='".$payIds[$i]."'");
			$descript="Recurring Rule transID ".$payIds[$i]." Resume";
			activities($_SESSION["loginHistoryID"],"RESUME",$payIds[$i],TBL_TRANSACTIONS,$descript);
			
			
		}
	}
	
	
	if(!$sidx)
	{
		$sidx = 1;
	}
	
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	
	
	if ($offset == "")
	{
		$offset = 0;
	}
	
			
	if($limit == 0)
	{
		$limit=50;
	}
	
	$SubmitJS		= "";
	$fromJS     	= "";
	$toJS			= "";
	$refNumberJS	="";
	$amount1JS		= "";
	$descJS	= "";


if($_REQUEST["btnAction"] == "Paid"){
	$transIDArray		= $_REQUEST["transIDs"];
	for ($d=0;$d < count($transIDArray);$d++)
	{
		if(isset($transIDArray[$d]))
		{
			update("update ".TBL_TRANSACTIONS." set agentComDuePaid = AgentComm where transID = '".$transIDArray[$d]."'");
			$err = "Commission Paid successfully.";
			
		}
	}

	if(trim($err) == "")
		$err = "Error occured while performing this action";
	// Number of iteration should be equal to the number of  transactions
		echo "<input type='hidden' name='payListPaid' id='payListPaid' value='".$err."'>";
		exit;
}

if($_REQUEST["Submit"] == "SearchTrans")
{
	if ($_REQUEST["from"]!="")
		$from = $_REQUEST["from"];
	if ($_REQUEST["to"]!="")
		$to = $_REQUEST["to"];
	if ($_REQUEST["searchBy"]!="")
		$searchBy = $_REQUEST["searchBy"];
	//echo "Search by".$searchBy;
	
	if ($_REQUEST["currencyTrans"]!="")
		$currencyT = $_REQUEST["currencyTrans"];
	if($_REQUEST["status"]!="")
	    $status = $_REQUEST["status"];
   // echo "Status".$status;
	
		if($_REQUEST["txtField"]!="")
	    $txtField = $_REQUEST["txtField"];
   //  echo "Txt Field".$txtField;
		
	if ($from != "")
	{
		$dDateF = explode("/",$from);
		if(count($dDateF) == 3)
			$from = $dDateF[2]."-".$dDateF[1]."-".$dDateF[0];
	}
	if ($to != "")
	{
		$dDate = explode("/",$to);
		if(count($dDate) == 3)
			$to = $dDate[2]."-".$dDate[1]."-".$dDate[0];
	}
	if ($from != "" || $to != "")
	{
		if ($from != "" && $to != "")
		{
			$queryDated = "  (tr.transDate >= '$from 00:00:00' and tr.transDate <= '$to 23:59:59') and ";	
		}
		elseif ($from != "")
		{
			$queryDated = "  (tr.transDate >= '$from 00:00:00') and ";	
		}
		elseif ($to != "")
		{
			$queryDated = "  (tr.transDate <= '$to 23:59:59') and ";	
		}	
	}
	if ($queryDated != "")
	{
		$queryClause .= $queryDated;
	}
	if($searchBy =="SN" && $txtField !="" )
	{
	    $senderName = explode(" ", $txtField);
	    $queryClause .= " (cust.firstName LIKE '$senderName[0]%' ) and ";
	}
	if($searchBy =="BN"  && $txtField !="")
	{
	    $beneficiaryName = explode(" ", $txtField);
	    $queryClause .= " (ben.firstName LIKE '$beneficiaryName[0]%' ) and ";
	  
	}
	if($searchBy =="RN" && $txtField !="")
	{
	   $queryClause .= " tr.refNumberIM ='$txtField' and ";
	  
	}
	
	if ($status != "")
	{
		$querystatus .= " recur.Status='$status' and";
	}
	
		
	if ($currencyT != "" )
	{
		$queryCurrency .= "  tr.currencyFrom = '$currencyT' and ";
	}
}

$extraQuery = "";
if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && ($agentType != 'admin' && $agentType != 'Branch Manager' /*&& $agentType != 'Admin Manager'*/))
{
	
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$userID."'");
	
	$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
	//$query .= " and username in '".$linkedAgent."'";
}
$queryCust = "select userID,username from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent != 'ONLY' ";
if($agentType == "Branch Manager"){
	$queryCust .= " and parentID = '$userID'";						
}
$queryCust .=" order by agentCompany";
//debug($queryCust);
$agents = selectMultiRecords($queryCust);
if (empty($custAgentID))
{
	if($agentType == 'SUPA' || $agentType == 'SUBA'){
	$extraQuery .= " AND tr.custAgentID = '$agentID' ";
	}
	else if(count($agents)>0){
		$linkedAgentIDs = array();
		$linkedAgentIDstr = "";
		if(count($linkedAgent)>0){
			for($s=0;$s<count($agents);$s++){
				$flag = false;
				if(in_array($agents[$s]["username"], $linkedAgent) && CONFIG_ADMIN_ASSOCIATE_AGENT == '1')
					$flag = true;	
				if((CONFIG_ADMIN_ASSOCIATE_AGENT != '1') || (strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType.",") === false))
					$flag = true;
				if($flag)
					$linkedAgentIDArr[] = $agents[$s]['userID'];
			}
			if(!empty($linkedAgentIDArr))
				$linkedAgentIDstr = implode("','",$linkedAgentIDArr);
		}
		if(!empty($linkedAgentIDstr) || $agentType != 'admin')
			$extraQuery .= " AND tr.custAgentID IN ('$linkedAgentIDstr') ";
	}
}

if($_REQUEST["getGrid"] == "intComReport"){
	$queryCom = "SELECT 
					cust.customerID, 
					cust.firstName as custFirstName,
                    cust.middleName as custMiddleName,					
					cust.lastName as custLastName,
                    ben.firstName as benFirstName,
                    ben.middleName as benMiddleName,
                    ben.lastName as benLastName,					
					recur.transID as recurTransID,
					recur.Status as Status,
					recur.Time as Time,
					recur.startDate as startDate,
					recur.endDate as endDate,
					recur.transactionIDs as transactionIDs,
                    tr.transID as transID,
                    tr.refNumberIM as refnumber,
					tr.currencyFrom as currencyFrom,
                    tr.currencyTo	as currencyTo,
                    tr.exchangeRate as exchangeRate,
                    tr.localAmount as localAmount,
                    tr.transAmount as transAmount,					
                    tr.transStatus as transStatus					
					from 
					".TBL_RECURRING_PAYMENT." as recur
                    
                    LEFT JOIN ".TBL_TRANSACTIONS." as tr ON  recur.transID = tr.transID					
					LEFT JOIN ".TBL_CUSTOMER." as cust ON  tr.customerID =cust.customerID  
					LEFT JOIN ".TBL_BENEFICIARY." as ben ON  ben.benID = tr.benID
					
					where  
														
					";
	 if(!empty($queryClause))
		$queryCom .= $queryClause;
	
	if(!empty($querystatus))
	    $queryCom .= $querystatus;
	
	if(!empty($queryCurrency))
	    $queryCom .= $queryCurrency; 
    
    $queryCom .=" ". 1; 	
    //debug ($queryCom);
	//echo $queryCom;
	//exit;
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_query());
	$count = mysql_num_rows($resultCom);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryCom .= " order by $sidx $sord LIMIT $start , $limit";
	$resultCom = mysql_query($queryCom) or die(__LINE__.": ".mysql_error());

	//echo $queryCom;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	while($row = mysql_fetch_array($resultCom))
	{	
	
	/* $Ids = explode(",", $row['transactionIDs']);
	$idLength = count($Ids);  */
	$totalVal = explode(",", $row["transactionIDs"]);
	$childIDsCount = count($totalVal);
	foreach ($totalVal as $val){
	if ($val == ''){
	$totalIntCount = '0';
	}
	else {
	$totalIntCount = $childIDsCount;
	}
	}
	$startDate = date("d-m-Y", strtotime($row["startDate"]));
	$endDate = date("d-m-Y", strtotime($row["endDate"]));
	 $IdTypes ="";
	for($x=0; $x<$childIDsCount;$x++)
	{
	  $IdQuery= "Select refNumberIM from transactions where transID ='".$totalVal[$x]."'";
	  $IDqueryResult = selectFrom($IdQuery);
	  
	 $IdTypes.=$IDqueryResult["refNumberIM"].",";  
	
	} 
		
		
    	$response->rows[$i]['id'] = $row["transID"];
		$response->rows[$i]['cell'] = array(
		                            $row["refnumber"],									
		                            $row["transStatus"],
                                    $startDate,
                                    $endDate,									
									$row["custFirstName"].' '.$row["custMiddleName"].' '.$row["custLastName"],
									$row["benFirstName"].' '.$row["benMiddleName"].' '.$row["benLastName"],
									$row["Time"],
									$row["transAmount"],
									$row["currencyFrom"],
									$row["currencyTo"],
									$row["exchangeRate"],
									$row["localAmount"],
									$row["Status"],
									$totalIntCount,
									//$IdTypes
									
									/* "<a ref='http://dev1.dev.hbstech.co.uk/admin/commission_report_ajax.php' target='_blank'> edit </a> "*/
									
									
								);
		$i++;
	}
	
	echo $response->encode($response); 
}	

if($_REQUEST["getGrid"] == "intComReportSubGrid"){
	$ridArr = $_REQUEST["rid"];
	//print $ridArr;
	$recurring_data = explode(",",$ridArr);
	//print $recurring_data[0]; 
    $TotalCount = count($recurring_data);
	//echo $TotalCount;
	
    for($i=0;$i<$TotalCount;$i++)
 {	
  
	$queryComSub = "SELECT 
					transStatus
					
				from
                    transactions				
                where 
                    transID= '".$recurring_data[$i]."'				
					";
	//$queryComSub .= " AND cust.customerID = '$custID' ";
	//echo $queryComSub;
	//exit;
	/* if(!empty($queryClause))
		$queryComSub .= $queryClause;

	if(!empty($extraQuery))
		$queryComSub .= $extraQuery; */

	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_query());
	//print "query Res".$queryComSub;
	
	}
	//print($resultComSub);
	//exit();
	$count = mysql_num_rows($resultComSub);
	
	//debug($_REQUEST);
	
	if($count > 0)
	{
		$total_pages = ceil($count / $limit);
	} else {
		$total_pages = 0;
	}
	
	if($page > $total_pages)
	{
		$page = $total_pages;
	}
	
	$start = $limit * $page - $limit; // do not put $limit*($page - 1)
	
	/**
	 * A patch to handle -ve value in case the $count=0, 
	 * because in this case the $start is set to a -ve value 
	 * which causes query to break.
	 */
	if($start < 0)
	{
		$start = 0;
	}
	
	$queryComSub .= " order by $sidx $sord LIMIT $start , $limit";
	$resultComSub = mysql_query($queryComSub) or die(__LINE__.": ".mysql_error());

	//echo $resultComSub;
	//exit;
	//echo $count;
	$response->page = $page;
	$response->total = $total_pages;
	$response->records = $count;

	$i=0;
	
	  while($row = mysql_fetch_array($resultComSub))
	{
		
		$response->rows[$i]['id'] = $row["transIDT"];
		$response->rows[$i]['cell'] = array(
									$row["transStatus"],
									"3",
									$row["refNumT"],
									$row["transDateT"],
									$transAm,
									$row["destAmT"],
									$transCommDue,
									$RemAmount,
									$transCommDuePaid
								);
		$i++;
	}
	 
//	echo $response->encode($response); 
}	
if($_REQUEST["getGrid"] == "intComReportSubGrid"){

         
		 $sumSettlementAmount =0;
		 $SeltransIds = array();
	//{
	$strRecurringId = $_REQUEST['rid'];
	 $selectTransId = SelectFrom("SELECT transactionIDs FROM recurring_transactions WHERE  `transID` = $strRecurringId");

	$getTransactions = selectMultiRecords("SELECT transID,refNumberIM,transStatus, transAmount, currencyFrom, localAmount,currencyTo,transType,transDate,exchangeRate FROM ".TBL_TRANSACTIONS." WHERE transID IN(".$selectTransId["transactionIDs"].")");
		 $i = 0;
		  foreach($getTransactions as $val => $key)
		{
	 
		$response->rows[$i]['id'] = $key;
		$response->rows[$i]['cell'] = array(
		                          
		                            $key["refNumberIM"],			
		                            $key["transStatus"],
                                    $key["transAmount"],
                                    $key["transType"],
									$key["exchangeRate"],
									//$key["currencyTo"],
									//$key["settlementValue"],	
								);
	$i++;
		
		}  

		echo $response->encode($response); 
	}
	
?>