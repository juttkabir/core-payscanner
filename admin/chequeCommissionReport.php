<?
	session_start();
	include ("../include/config.php");
	include ("security.php");
	$date_time = date('d-m-Y  h:i:s A');
	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
	$agentID = $_SESSION["loggedUserData"]["userID"];
	$systemCode = SYSTEM_CODE;
	$company = COMPANY_NAME;
	$systemPre = SYSTEM_PRE;
	$manualCode = MANUAL_CODE;
	$countOnlineRec = 0;
	$limit = CONFIG_MAX_TRANSACTIONS;
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cheque Commission Report</title>
<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="javascript/jquery.form.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var selectedId = "";

	$(document).ready(function(){
	
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $("#storeData").attr("disabled",true);
/*			$("#transList").appendPostData({totalPaid:2});
			$("#transList").appendPostData({totalCommission:3});
			$("#transList").appendPostData({totalAmount:4});*/
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $("#storeData").attr("disabled",false);
/*			var totalPaid = $("#transList").getGridParam("totalPaid");
			var totalCommission = $("#transList").getGridParam("totalCommission");
			var totalAmount = $("#transList").getGridParam("totalAmount");*/
		});

		
		$("#transList").jqGrid({
			url:'chequeOrderCalculation.php?get=gd&mode=commission&q=2&nd='+new Date().getTime(),
			datatype: "json",
			height: 400, 
			width: 900,
			colNames:[
				'Created By',
				'Amount Paid',
				'Commission',
				'Value'
			],
			colModel:[
				{name:'created_by',index:'created_by', width:150},
				{name:'paid_amount',index:'paid_amount', width:60},
				{name:'commission',index:'commission', width:60},
				{name:'cheque_amount',index:'cheque_amount', width:50},
			],
			rowNum: 20,
			rowList: [10,20],
			imgpath: gridimgpath,
			pager: jQuery('#pager'),
			sortname: 'created_by',
			viewrecords: true,
			sortorder: "asc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Commission Report",
			onSelectRow:function(id){
				if(id)
				{
					if(selectedId == id)
					{
						selectedId = "";
					}
					else
					{
						selectedId = id;
						//alert(id);
						var totalRec= $("#transList").getGridParam("records");
						if(id!=totalRec+1)
							loadOrder(id);
					}
				}
			}
		});
		
		jQuery('a').cluetip({splitTitle: '|'});
		
		
		$("#from_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});
		$("#to_date").datePicker({
			startDate: '<?=date("d/m/Y",strtotime("-2 years"))?>'
		});


		$("#search_orders").click(function(){
			gridReload();
		});
		
		
		$("#exportBtn").click(function(){
			$("#exportOrdersRow").show();
		});
		
		$("#exportOrdersRow").change(function(){
			if($(this).val() != "")
			{
				//alert($(this).val());
				var strUrl = "chequeCommOrderExport.php?exportType="+$(this).val()+"&from_date="+$("#from_date").val()+"&to_date="+$("#to_date").val()
							+"&cheque_ref="+$("#cheque_ref").val()+
							"&name="+$("#name").val()+"&order_status_filter="+$("#order_status_filter").val()+"&Submit=Search";		
				window.open(strUrl,"export","menubar=1,resizable=1,status=1,toolbar=1,scrollbars=1"); 
			}
		});
		
	});
	
	function gridReload()
	{
		var date_from = $("#from_date").val();
		var to_date = $("#to_date").val();
		var creator = $("#creator").val();
		
		var theUrl = "chequeOrderCalculation.php?get=gd&mode=commission&from_date="+date_from+"&to_date="+to_date+"&creator="+creator+"&Submit=Search";
		
		$("#transList").setGridParam({
			url: theUrl,
			page:1
		}).trigger("reloadGrid");
	}	
	function loadOrder(di)
	{
		var date_from = $("#from_date").val();
		var to_date = $("#to_date").val();
		var creator = $("#creator").val();
		
		var transListG	= jQuery("#transList").getRowData(di);
		var strUrl = "chequeCommReport-detail.php?from_date="+date_from+"&to_date="+to_date+"&creator="+transListG.created_by;
					"&name="+$("#name").val()+"&queryString=commission";
		window.open(strUrl,'viewCommissionTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')
	}
</script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<style>
/*a.dp-choose-date {
	float: left;
	width: 16px;
	height: 16px;
	padding: 0;
	margin: 5px 3px 0;
	display: block;
	text-indent: -2000px;
	overflow: hidden;
	background: url(images/calendar.jpg) no-repeat; 
}
a.dp-choose-date.dp-disabled {
	background-position: 0 -20px;
	cursor: default;
}*/
.error {
	color: red;
	font: 6pt verdana;
	font-weight:bold;
}
</style>
</head>
<body>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="80%" border="0" align="center" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" bgcolor="#DFE6EA">
			<strong>Commission Report</strong>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="center">
			<form name="search_cheque" id="search_cheque" action="" method="post">
				Date In From 
				<input type="text" name="from_date" id="from_date" readonly="" />
				&nbsp;&nbsp;
				Date In To:
				<input type="text" name="to_date" id="to_date" readonly="" />
				<br /><br />
				Creator:&nbsp;<input type="text" name="creator" id="creator" />
				&nbsp;<br />
				<br />
				<input type="button" id="search_orders" name="search_orders" value="Search" style="font-weight:bold" />
				&nbsp;&nbsp;<input type="reset" value="Clear All Filters" />
				<!--&nbsp;&nbsp;<input type="button" id="exportBtn" value="Export Searched Orders" />
				&nbsp;
				<select name="exportOrdersRow" id="exportOrdersRow" style="display:none">
					<option value="">Select Format</option>
					<option value="XLS">Excel</option>
					<option value="CSV">CSV</option>
					<option value="HTML">HTML</option>
				</select>-->
			</form>
		</td>
    </tr>
    <tr>
        <td colspan="2" align="center">
			<table id="transList" class="scroll" cellpadding="0" cellspacing="0" width="80%">
			</table>
			<div id="pager" class="scroll" style="text-align:center;"></div>		
		</td>
    </tr>
	<tr>
		<td colspan="2" align="center" valign="top">
			<div id="printDIV" align="center" style="width:80%;"><input type="button" id="printBtn" name="printBtn" value=" Print this Report " onclick="print();"></div>
		</td>
	</tr>
</table>
</body>
</html>
