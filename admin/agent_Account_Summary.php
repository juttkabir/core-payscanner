<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];

/**	maintain report logs 
 *	Search report url and its Title in the array $arrSavePageAccess within maintainReportLogs function
 *	If not exist enter it in order to maintain report logs
 */
include_once("maintainReportsLogs.php");
maintainReportLogs($_SERVER["PHP_SELF"]);

// Added by Niaz Ahmad at 22-10-2007
if(CONFIG_GLOBAL_BALANCE == "1")
{ 
	$queryBalance = $_GET["queryBalance"];
	$queryFlag = $_GET["queryFlag"];
	$userid = $_GET["userid"];
	$Agent = $_GET["Agent"];
	$userID  = $_SESSION["loggedUserData"]["userID"];
	
	if($userid!= "" && $Agent == "superAgent")
	{
		$superAgentquery=selectMultiRecords("select * from ".TBL_ACCOUNT_SUMMARY." as ac,".TBL_ADMIN_USERS." as u where u.userID=ac.user_id and ac.id IN (".$userid.") ");	  	  
	}
	if($useridSub!= "" && $Agent == "subAgent")
	{
		$superAgentquery=selectMultiRecords("select * from ".TBL_SUB_ACCOUNT_SUMMARY." as ac,".TBL_ADMIN_USERS." as u where u.userID=ac.user_id and ac.id IN (".$useridSub.") ");	  	  
	}
}
if ($offset == "")
$offset = 0;
$limit=50;
if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;

if($_REQUEST["agentID"]!="")
	$agentName = $_REQUEST["agentID"];		

if($_POST["Submit"]=="Search")
{
	$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
	$toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
	$queryCurrency = $_POST["currency"];
	
	$fYear = $_POST["fYear"];
	$fMonth = $_POST["fMonth"];
	$fDay = $_POST["fDay"];
	
	$tYear = $_POST["tYear"];
	$tMonth = $_POST["tMonth"];
	$tDay = $_POST["tDay"];
	
	$_SESSION["fYear"] = $fYear;
	$_SESSION["fMonth"] = $fMonth;
	$_SESSION["fDay"] = $fDay;
	
	$_SESSION["tYear"] = $tYear;
	$_SESSION["tMonth"] = $tMonth;
	$_SESSION["tDay"] = $tDay;
}
elseif($_GET["Submit"]=="Search")
{
	$fromDate = $_GET["fromDate"];
	$toDate = $_GET["toDate"];
	$queryCurrency = $_GET["currency"];
	
	$fYear = $_SESSION["fYear"];
	$fMonth = $_SESSION["fMonth"];
	$fDay = $_SESSION["fDay"];
	
	$tYear = $_SESSION["tYear"];
	$tMonth = $_SESSION["tMonth"];
	$tDay = $_SESSION["tDay"];
}
$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
$queryDateSUP = "(s.dated >= '$fromDate 00:00:00' and s.dated <= '$toDate 23:59:59')";	
?>
<html>
<head>
<title>Account Summary</title>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/datePicker.css" />
<script type="text/javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.pi.js"></script>
<script language="javascript" src="javascript/date.js"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script language="javascript" src="./javascript/functions.js"></script>
<script language="javascript">
$(document).ready(function(){
	Date.format = 'yyyy-mm-dd';
	$("#fromDate").datePicker({
		startDate: '2000-01-01'
	});
	$("#toDate").datePicker({
		startDate: '2000-01-01'
	});
	
	$("#printReport").click(function(){
		// Maintain report print logs 
		$.ajax({
			url: "maintainReportsLogs.php",
			type: "post",
			data:{
				maintainLogsAjax: "Yes",
				pageUrl: "<?=$_SERVER['PHP_SELF']?>",
				action: "P"
			}
		});
		
		$("#printDiv").hide();
		$("#searchTable").hide();
		$("#paginationRow").hide();
		print();
		$("#printDiv").show();
		$("#searchTable").show();
		$("#paginationRow").show();
	});
});
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
function exportAgentDistSummary(){
	var exportFormat 	= document.getElementById("exportFormat").value;
	var allCountRows 	= document.getElementById("allCountRows").value;
	var fromDate 		= document.getElementById("fromDate").value;
	var toDate 			= document.getElementById("toDate").value;
	if( document.getElementById("currency"))
		var currency		= document.getElementById("currency").value;
	var slctID 			= document.getElementById("agentID").value;
	var queryFlag		= document.getElementById("queryFlag").value;
	var pageUrl			= "<?=$_SERVER['PHP_SELF']?>";
	if(exportFormat!="" && allCountRows>0){
		window.open('exportAgentDistributorSummary.php?agentID='+slctID+'&fromDate='+fromDate+'&toDate='+toDate+'&exportType='+exportFormat+'&allCountRows='+allCountRows+'&queryFlag='+queryFlag+'&currency='+currency+'&pageUrl='+pageUrl+'','Export A&D Account Summary', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420');
	}
	else if(exportFormat==""){
		alert("Please Select export type");
		return false;	
	}
	else{
		alert("Please First Select filters and Search.");
		return false;
	}
}
function checkForm(){
	var currency = document.getElementById("currency").value;
	if(currency==""){
		alert("Please select a currency");
		return false;
	}
}
</script>
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
color: #3366CC;
font-weight: bold;
}
input, select{ padding:3px;}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<form action="agent_Account_Summary.php" method="post" name="Search" onSubmit="return checkForm();">
<div class="topbar" style="padding:10px">Account Summary Report</div>
<table id="searchTable" style=" border:1px solid #666666" cellpadding="5" align="center" width="500">
<tr>
	<td colspan="2" bgcolor="#C0C0C0" style="border:1px solid #333333"><span class="tab-u"><strong><?=__("Search Filters") ?></strong></span></td>
</tr>
		<td colspan="2" align="center">From &nbsp;
			<? 
			$month = date("m");
			$day = date("d");
			$year = date("Y");
			?>
			<SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
			<?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
					$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
			?>
			</select>
			<script language="JavaScript">SelectOption(document.forms[0].fDay, "<?=$fDay; ?>");</script>
			<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
				<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
				<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
				<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
				<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
				<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
				<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
				<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
				<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
				<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
				<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
				<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
				<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">SelectOption(document.forms[0].fMonth, "<?=$fMonth?>");</script>
			<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
			<?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
			?>
			</SELECT>
			<script language="JavaScript">SelectOption(document.forms[0].fYear, "<?=$fYear;?>");</script>
		&nbsp; To &nbsp;
			<SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
			<?
			for ($Day=1;$Day<32;$Day++)
			{
				if ($Day<10)
					$Day="0".$Day;
				echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
			}
			?>
			</select>
			<script language="JavaScript">SelectOption(document.forms[0].tDay, "<?=$tDay; ?>");</script>
			<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
				<OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
				<OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
				<OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
				<OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
				<OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
				<OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
				<OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
				<OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
				<OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
				<OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
				<OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
				<OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
			</SELECT>
			<script language="JavaScript">SelectOption(document.forms[0].tMonth, "<?=$tMonth; ?>");</script>
			<SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
			<?
			$cYear=date("Y");
			for ($Year=2004;$Year<=$cYear;$Year++)
			{
				echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
			}
			?>
			</SELECT>
			<script language="JavaScript">SelectOption(document.forms[0].tYear, "<?=$tYear; ?>");</script>
		</td>
	</tr>
	<?php
	if($agentType == "admin" || $agentType == "Admin Manager" || $agentType == "COLLECTOR" || $agentType == "Branch Manager" || $agentType == "Admin")//101
	{
		$userID  = $_SESSION["loggedUserData"]["userID"];
		if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
			$entity_country =getLoggedUserEntity($userID);
		$userEntity  = $_SESSION["loggedUserData"]["entityId"];
		if($entity_country == ''){	
			$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson, isCorrespondent,agentStatus,agentType from ".TBL_ADMIN_USERS." where parentID >= 0 and adminType='Agent' and agentStatus='Active'";
		}
		else
		{
			$agentQuery  = "select userID,username, agentCompany, name, agentContactPerson, isCorrespondent,agentStatus,agentType from ".TBL_ADMIN_USERS." where parentID >= 0 and adminType='Agent' and entityId = $userEntity and agentStatus='Active'";
		}	
		?>
	<tr>
		<td width="250" align="right">Select Agent/Distributor</td>
		<td width="250"><select id="agentID" name="agentID" style="font-family:verdana; font-size: 11px; width:200">
				<option value="all">- Select All-</option>
				<option value="allSUPA">- Select All Super Agents-</option>
				<option value="allSUPI">- Select All Super Distributors-</option>
				<option value="allSUBA">- Select All <?=__("Sub Agent");?>s-</option>
				<option value="allSUBI">- Select All Sub Distributors-</option>
				<?
				if($agentType == "Branch Manager")
				{	
					$agentQuery .= " and parentID = '$changedBy'  and isCorrespondent != 'Y'";				
				}
				$agentQuery .= "ORDER BY agentType,isCorrespondent, username, agentCompany";
				$agents = selectMultiRecords($agentQuery);
				for ($i=0; $i < count($agents); $i++)
				{
					$agentUsername = $agents[$i]["username"];
					$agentCompany  = $agents[$i]["agentCompany"];
					$agentContactPerson  = $agents[$i]["agentContactPerson"];
					$agentStatus = $agents[$i]["agentStatus"];
					$currentAgent = $agents[$i]["isCorrespondent"];
					$previousAgent = $agents[$i-1]["isCorrespondent"];
					if($agents[$i]["agentType"] == "Supper")
					{
						if($currentAgent != $previousAgent)
						{
							if($currentAgent == "N")
								$strAgent .= "<optgroup label='".$agents[$i]["agentType"]." Agents - (".$agents[$i]["agentStatus"].")'>";
							else
								if($currentAgent == "ONLY")
									$strAgent .= "<optgroup label='".$agents[$i]["agentType"]." Distributors - (".$agents[$i]["agentStatus"].")'>";
						}	
					}
					
					if($agents[$i]["agentType"] == "Sub")
					{
						if($currentAgent != $previousAgent)
						{
							if($currentAgent == "N")
								$strAgent .= "<optgroup label='".$agents[$i]["agentType"]." Agents - (".$agents[$i]["agentStatus"].")'>";
							else
								if($currentAgent == "ONLY")
									$strAgent .= "<optgroup label='".$agents[$i]["agentType"]." Distributors - (".$agents[$i]["agentStatus"].")'>";
						}	
					}
					
					if(defined("CONFIG_DIST_DROPDOWN_NUMBER_ONLY_USERS") && (CONFIG_DIST_DROPDOWN_NUMBER_ONLY_USERS=="1" || strstr(CONFIG_DIST_DROPDOWN_NUMBER_ONLY_USERS,$agentType.",")) && $isCorrespondent=="ONLY")
					{
						$distOptions = $agentUsername;
					}
					else
					{
						if(defined("CONFIG_AGENT_USERS_DROPDOWN_FORMAT_USERS") && (CONFIG_AGENT_USERS_DROPDOWN_FORMAT_USERS=="1" || strstr(CONFIG_AGENT_USERS_DROPDOWN_FORMAT_USERS,$agentType.",")))
							$distOptions = ($agentUsername." [".$agentContactPerson."]");
						else
							$distOptions = ($agentUsername." [".$agentCompany."]");
					}
					$strAgent .= '<option value="'.$agents[$i]["userID"].'">'.$distOptions.'</option>';
					//$strAgent .= "</optgroup>";
				}
				$dropdown = str_replace("Supper","Super",$strAgent);
				echo $dropdown;
				?>
				</select>
				<script language="JavaScript">SelectOption(document.forms[0].agentID, "<?=$agentName?>");</script>
		</td>
	</tr>
	<?php
	} 
	?>
	
	<?
	if(CONFIG_ADD_CURRENCY_DROPDOWN == "1")
	{
	?>       
	<tr>
		<td align="right">Currency&nbsp;<font color="#FF0000">*</font></td>
		<td><select id="currency" name="currency" style="font-family:verdana; font-size: 11px; width:130">
			<option value="">- Select Currency -</option>
			<?
			$queryDestCurrency = "select distinct currency from ".TBL_EXCHANGE_RATES;
			$DestCurrencyData = selectMultiRecords($queryDestCurrency);
			$queryOriginCurrency = "select distinct currencyOrigin from ".TBL_EXCHANGE_RATES;
			$OriginCurrencyData = selectMultiRecords($queryOriginCurrency);
			for($a = 0; $a < count($DestCurrencyData); $a++)
			{
				$allCurrency[$a] = $DestCurrencyData[$a]['currency'];
				$y++;
			}
			for($b = 0; $b < count($OriginCurrencyData); $b++)
			{
				$allCurrency[$y]= $OriginCurrencyData[$b]['currencyOrigin'];
				$y++;
			}
			$uniqueCurrency = array_unique($allCurrency);
			$uniqueCurrency = array_values ($uniqueCurrency);
			for($k = 0; $k < count($uniqueCurrency); $k++)
			{
			?>
				<option value="<?=trim($uniqueCurrency[$k])?>"><?=trim($uniqueCurrency[$k])?></option>
			<? 
			} 
			?>
			</select>
			<script language="JavaScript">SelectOption(document.forms[0].currency, "<?=$_REQUEST["currency"]; ?>");</script>
			<br> 
			<? 
		} 
		?>
	</td>
	</tr>
	<tr><td colspan="2" align="center"><input type="submit" name="Submit" value="Search"></td></tr>
</table>
<br /> 
<?
if($_POST["Submit"]=="Search" || $_GET["search"]=="search" || $_GET["queryFlag"]=="true")
{
	$userID  = $_SESSION["loggedUserData"]["userID"];
	if(defined("CONFIG_REMOVE_ENTITY") && CONFIG_REMOVE_ENTITY != '1')
		$entity_country =getLoggedUserEntity($userID);
	$Balance="";
	
	if($agentName == "allSUPA")
	{
		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Supper' ";
		//$accountQuery .= " and $queryDateSUP";
	}elseif($agentName == "allSUPI")
	{
		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Supper' ";
		//$accountQuery .= " and $queryDateSUP";	
	}elseif($agentName == "allSUBA")
	{
		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'N' and agentType = 'Sub' ";
		//$accountQuery .= " and $queryDateSUP";	
	}elseif($agentName == "allSUBI")
	{
		$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType='Agent' and isCorrespondent = 'ONLY' and agentType = 'Sub' ";
		//$accountQuery .= " and $queryDateSUP";	
	}else{
			$accountQuery = "Select userID as user_id, name, username, isCorrespondent, agentType, adminType from ".TBL_ADMIN_USERS." where 1 and parentID >= 0 and adminType = 'Agent' and isCorrespondent != '' ";
			//$accountQuery .= " and $queryDate";				
		if($agentName != "" && $agentName != "all")
		{
			$accountQuery .= " and  userID = '$agentName' ";	
		}
	}
	$userEntity  = $_SESSION["loggedUserData"]["entityId"];
	if($entity_country != ''){	
		$accountQuery .= " and entityId = '$userEntity'";								
	}
	//	debug($entity_country);	
	if($agentType == "Branch Manager")
	{
		$accountQuery .= " and parentID = '$changedBy'";								
	}
	//$accountQuery .= " and entityId = '$userEntity'";	

	$accountQueryCnt = $accountQuery;
	$accountQuery .= " LIMIT $offset , $limit ";
	$contentsAcc = selectMultiRecords($accountQuery);
	//$allCount = count($contentsAcc);
	$allCount = count(selectMultiRecords($accountQueryCnt));
	
	$queryString = "&Submit=Search&search=search&agentID=".$agentName."&fromDate=".$fromDate."&toDate=".$toDate."&currency=".$queryCurrency."";
	?>		
	<table width="800" border="1" cellpadding="0" bordercolor="#666666" align="center">
	<tr id="paginationRow">
		<td height="25">
			<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
				<tr>
				<? if(CONFIG_GLOBAL_BALANCE == "1"){ ?>
				<td align="left"><?php if (count($superAgentquery) > 0) {;?>
				Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($superAgentquery));?></b> of
				<?=count($superAgentquery); ?>
				<?php } ;?>
				</td>
				<? }  ?>
				<? if($queryFlag!="true"){ ?>
				<td align="left"><?php if (count($contentsAcc) > 0) {;?>
				Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAcc));?></b> of
				<?=$allCount; ?>
				<?php } ;?>
				</td>
				<? } ?>
				<?php if ($prv >= 0) { ?>
				<td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&total=first";?><?=$queryString?>"><font color="#005b90">First</font></a> </td>
				<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&total=pre";?><?=$queryString?>"><font color="#005b90">Previous</font></a> </td>
				<?php } ?>
				<?php 
				if ( ($nxt > 0) && ($nxt < $allCount) ) {
				$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
				<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&total=next";?><?=$queryString?>"><font color="#005b90">Next</font></a>&nbsp; </td>
				<td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&total=last";?><?=$queryString?>"><font color="#005b90">Last</font></a>&nbsp; </td>
				<?php } ?>
				</tr>
				
			</table>
		</td>
	</tr>
	<?
	if(count($contentsAcc) > 0)
	{
		if(CONFIG_DATE_FORMAT_ON_ACCOUNT_SUMMERY == "1")
			$strReportDate = date("d-m-Y",strtotime($fromDate)) ." - ".date("d-m-Y",strtotime($toDate));
		else
			$strReportDate = date("d.m.Y");
	?>
	<tr>
	<td>
		<table width="100%">
			<tr>
				<td width="33%"><strong><font size="2">Report Date - <? echo $strReportDate; ?></font></strong></td>
				<td width="33%"><strong><font size="2">Account Summary</font></strong></td>
			</tr>
		</table>
	</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="1" bordercolor="#DDDDDD" style=" border-collapse:collapse">
			<tr bgcolor="#EFEFEF">
			<?php
			if(CONFIG_GLOBAL_BALANCE == "1"){
				if($_GET["queryFlag"]=="true") { 
				?>
				<td align="left" ><span class="style1">Agent </span></td>
				<td align="left" ><span class="style1">Closing Balance</span></td>
				<td align="left" ><span class="style1">Date</span></td>
				<td align="left" ><span class="style1">&nbsp;</span></td>	
				<td align="left" ><span class="style1">&nbsp;</span></td>	
				<td align="left" ><span class="style1">&nbsp;</span></td>	
				<td align="left" ><span class="style1">&nbsp;</span></td>	
				<? } 
			}?>
			<?php
			if($_GET["queryFlag"]!="true") { ?>
				<td align="left" ><span class="style1">Agent </span></td>
				<td align="left" ><span class="style1">Amount Used(Dr)</span></td>
				<td align="left" ><span class="style1">Amount Paid(Cr)</span></td>
				<!--<td align="left" ><span class="style1">Opening Balance</span></td>
				<td align="left" ><span class="style1">Running Balance</span></td>-->
				<td align="left" ><span class="style1">Closing Balance</span></td>
				<td align="left" >&nbsp;</td>	
			<? } ?>
			</tr>
			<?php 
			if(CONFIG_GLOBAL_BALANCE == "1"){ 			 
				if($_GET["queryFlag"]=="true") {
					for($k=0; $k< count($superAgentquery);$k++){
						$superAgentBalance+=$superAgentquery[$k]["closing_balance"];
						?>
						<tr bgcolor="#FFFFFF">
						<td width="200" align="left"><strong><font color="#006699">
						<? 
						echo($superAgentquery[$k]["name"]." [".$superAgentquery[$k]["username"]."]");
						?>
						</font></strong></td>
						<td width="200" align="left"><? echo number_format($superAgentquery[$k]["closing_balance"],2,'.',','); ?></td>
						<td width="150" align="left"><? echo $superAgentquery[$k]["dated"] ?></td>
						<td width="150" align="left">&nbsp;</td>
						<td width="150" align="left">&nbsp;</td>
						<td width="150" align="left">&nbsp;</td>
						<td width="150" align="left">&nbsp;</td>
						</tr>
					<? }
				}
			} // end config if
			?>
			<?
			
			if($_GET["queryFlag"]!="true")
			{ 	
				$totalOpeningBalance = 0;
				$totalClosingBalance = 0;
				$totalDailyBalance = 0;
				$totalDebit = 0;
				$totalCredit = 0;
				 
				for($i=0;$i < count($contentsAcc);$i++)
				{
					if($contentsAcc[$i]["agentType"] == "Supper")
					{
						//Super Agent and Super Distributor
						$summaryTable = TBL_ACCOUNT_SUMMARY;	
						$distributorLedger = TBL_DISTRIBUTOR_ACCOUNT;
						$agentLedger = TBL_AGENT_ACCOUNT;	
					}else{
						//Sub Agent and Sub Distributor
						$summaryTable = TBL_SUB_ACCOUNT_SUMMARY;		
						$distributorLedger = TBL_SUB_DISTRIBUTOR_ACCOUNT;
						$agentLedger = TBL_SUB_AGENT_ACCOUNT;
					}
					$dateFromQuery = "select  Max(dated) as datedFrom from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$fromDate'";
					$datesFrom = selectFrom($dateFromQuery);
					$FirstDated = $datesFrom["datedFrom"];
					
					$dateToQuery = "select  Max(dated) as datedTo from " . $summaryTable . " where user_id = '".$contentsAcc[$i]["user_id"]."' and dated <= '$toDate'";
					$datesTo = selectFrom($dateToQuery);
					$LastDated = $datesTo["datedTo"];
					
					$openingBalance = 0;
					$closingBalance = 0;
					$dailyBalance = 0;
					$Debit = 0;
					$Credit = 0;
					if(!defined("CONFIG_SWAP_CLOSING_TO_OPENING") || CONFIG_SWAP_CLOSING_TO_OPENING != "1"){	 
						if($FirstDated != "")
						{
							$account1 = "Select opening_balance, closing_balance from ".$summaryTable." where 1";
							$account1 .= " and dated = '$FirstDated'";				
							$account1 .= " and  user_id = '".$contentsAcc[$i]["user_id"]."' ";
							if($queryCurrency!=""){
								$account1 .= " and currency = '$queryCurrency'";
							}								
							$contents1 = selectFrom($account1);
							if($fromDate == $FirstDated)
								$openingBalance =  $contents1["opening_balance"];
							else
								$openingBalance =  $contents1["closing_balance"];
						}
						
						if($LastDated != "")
						{
							$account2 = "Select opening_balance, closing_balance from ".$summaryTable." where 1";
							$account2 .= " and dated = '$LastDated'";				
							$account2 .= " and  user_id = '".$contentsAcc[$i]["user_id"]."' ";
							if($queryCurrency!=""){
								$account2 .= " and currency = '$queryCurrency'";
							}							
							$contents2 = selectFrom($account2);
							$closingBalance =  $contents2["closing_balance"];
						}
						$dailyBalance = $closingBalance - $openingBalance;
						$totalOpeningBalance += $openingBalance;
						$totalClosingBalance += $closingBalance;
						$totalDailyBalance   += $dailyBalance;	
						if($contentsAcc[$i]["isCorrespondent"] == 'N')
						{
							$transBalanceQuery = "Select sum(amount) as amount,aaID, type , description,currency,dated from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
						}
						elseif($contentsAcc[$i]["adminType"] == 'Agent' and $contentsAcc[$i]["isCorrespondent"] == 'Y') /* A&D*/
						{
							$transBalanceQuery 	= "Select sum(amount) as amount,aaID, type, description,currency,dated from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";
							$transBalanceQuerySwap = "Select * from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";									 
						}	
						else /* Distributor */
						{
							$transBalanceQuery = "Select sum(amount) as amount,aaID, type, description,currency,dated from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
						}
						if($fromDate!="" && $toDate!=""){
							$transBalanceQuery .= " and $queryDate";
							$transBalanceQuerySwap .= " and $queryDate";
						}
						if($queryCurrency!=""){
							$transBalanceQuery .= " and currency = '$queryCurrency'";
							$transBalanceQuerySwap.= " and currency = '$queryCurrency'";
						}
						
						$transBalanceQuery .= " group by type, description ";
						$transBalance = selectMultiRecords($transBalanceQuery);	
					}
					else if(defined("CONFIG_SWAP_CLOSING_TO_OPENING") && CONFIG_SWAP_CLOSING_TO_OPENING == "1")
					{
						if($contentsAcc[$i]["isCorrespondent"] == 'N')
						{
							$transBalanceQuery = "Select sum(amount) as amount,aaID, type , description,currency,dated from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
							$transBalanceQuerySwap = "Select * from ".$agentLedger." where agentID = '".$contentsAcc[$i]["user_id"]."' ";	
						}
						elseif($contentsAcc[$i]["adminType"] == 'Agent' and $contentsAcc[$i]["isCorrespondent"] == 'Y') /* A&D*/
						{
							$transBalanceQuery 	= "Select sum(amount) as amount,aaID, type, description,currency,dated from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";
							$transBalanceQuerySwap = "Select * from ".TBL_AnD_ACCOUNT." where agentID = '".$contentsAcc[$i]["user_id"]."' ";									 
						}	
						else /* Distributor */
						{
							$transBalanceQuery = "Select sum(amount) as amount,aaID, type, description,currency,dated from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
							$transBalanceQuerySwap = "Select * from ".$distributorLedger." where bankID = '".$contentsAcc[$i]["user_id"]."' ";	
						}
						if($fromDate!="" && $toDate!=""){
							$transBalanceQuery .= " and $queryDate";
							$transBalanceQuerySwap .= " and $queryDate";
						}
						if($queryCurrency!=""){
							$transBalanceQuery .= " and currency = '$queryCurrency'";
							$transBalanceQuerySwap.= " and currency = '$queryCurrency'";
						}
						$transBalance = selectMultiRecords($transBalanceQuerySwap);	
						$account3 = "Select opening_balance, closing_balance from ".$summaryTable." where $queryDate  and  user_id = '".$contentsAcc[$i]["user_id"]."' and currency = '".$_REQUEST["currency"]."' order by id asc";
						$account4 = "Select opening_balance, closing_balance from ".$summaryTable." where $queryDate  and  user_id = '".$contentsAcc[$i]["user_id"]."' and currency = '".$_REQUEST["currency"]."' order by id desc";
						$contents3 = selectFrom($account3);
						$contents4 = selectFrom($account4);
						$openingBalance = $contents3["opening_balance"];
						$closingBalance = $contents4["closing_balance"];
						
						$dailyBalance = $closingBalance - $openingBalance;
						$totalOpeningBalance += $openingBalance;
						$totalClosingBalance += $closingBalance;
						$totalDailyBalance   += $dailyBalance;	
						$currSending = $transBalance[0]["currency"];
					}
					for($m = 0; $m < count($transBalance); $m++)
					{
						if($transBalance[$m]["type"] == "DEPOSIT")
						{
							if( $transBalance[$m]["description"]== 'Transaction Cancelled' || $transBalance[$m]["description"] == 'Transaction Amended')
							{
								$Debit = $Debit - $transBalance[$m]["amount"];
							}else{
								$Credit = $Credit + $transBalance[$m]["amount"];
							}
						}
						if($transBalance[$m]["type"] == "WITHDRAW")
						{
							if( $transBalance[$m]["description"]== 'Manually Withdrawn')
							{
								$Credit = $Credit - $transBalance[$m]["amount"];
							}else{
								$Debit = $Debit + $transBalance[$m]["amount"];
							}
						}
					}	
					$totalCredit += $Credit;
					$totalDebit += $Debit;	
					//$BeneficiryID = $contentsBen[$i]["benID"];
					?>
					<tr bgcolor="#FFFFFF">
						<td width="35%" align="left"><strong><font color="#006699"><?=($contentsAcc[$i]["name"]." [".$contentsAcc[$i]["username"]."]");?></font></strong></td>
						<td width="15%" align="left"><? echo number_format($Debit,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></td>
						<td width="15%" align="left"><? echo number_format($Credit,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></td>
						<!--<td align="left"><? //echo number_format($openingBalance,2,'.',','); ?></td>
						<td width="56"><? //echo number_format($dailyBalance,2,'.',','); ?></td>-->
						<td width="20%"><? echo number_format($closingBalance,2,'.',','); ?></td>
						<td width="15%">&nbsp;</td>
					</tr>
					
					<?
				}
			}
			?>			
			<tr> 
				<td align="left" colspan="4">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF"> 
				<td><strong>Total</strong></td>
				<? if($_GET["queryFlag"]=="true") { ?>
					<td>&nbsp; <? echo number_format($superAgentBalance,2,'.',','); ?></td>	
				<? } else{ ?>
					<td><strong><? echo number_format($totalDebit,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></strong></td>	
					<td><strong><? echo number_format($totalCredit,2,'.',','); if(CONFIG_VIEW_CURRENCY_LABLES == "1"){echo " ".$currSending;} ?></strong></td>
					<td><strong>Total Balance</strong></td>			
					<!--<td><?  //echo number_format($totalOpeningBalance,2,'.',','); ?></td>
					<td><?  //echo number_format($totalDailyBalance,2,'.',','); ?></td>
					<td><?  //echo number_format($totalClosingBalance,2,'.',','); ?></td>	-->	
					<td><strong> <?  echo number_format($totalCredit - $totalDebit,2,'.',','); ?></strong></td>	
			<? } ?>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>	
		</td>
	</tr>	
	<?
	} // greater than zero
	else
	{
	?>
	<tr>
		<td bgcolor="#FFFFCC"  align="center"><font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font></td>
	</tr>
	<tr>
		<td nowrap bgcolor="#EFEFEF">
			<table width="781" border="0" bordercolor="#EFEFEF">
				<!-- <tr bgcolor="#FFFFFF">
				<td align="left" ><span class="style1">Agent </span></td>
				<td align="left" ><span class="style1">Amount Used(Dr)</span></td>
				<td align="left" ><span class="style1">Amount Paid(Cr)</span></td>
				<td align="left" ><span class="style1">Opening Balance</span></td>
				<td align="left" ><span class="style1">Running Balance</span></td>
				<td align="left" ><span class="style1">Closing Balance</span></td>
				<td align="left" ><span class="style1">&nbsp;</span></td>	
				<td align="left" ><span class="style1">&nbsp;</span></td>
				</tr>
				<tr bgcolor="#FFFFFF">
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>
				<td align="left" >&nbsp;</td>	
				<td align="left" >&nbsp;</td>
				</tr>	-->
			</table>
		</td>
	</tr>
	<?
	}
	?>
</table>
	
<?	
}
else
{
	$_SESSION["grandTotal"]="";
	$_SESSION["CurrentTotal"]="";
}///End of If Search
?>
<div align="center" id="printDiv">
	<input type="hidden" id="allCountRows" name="allCountRows" value="<?=$allCount?>" >
	<input type="hidden" id="queryFlag" name="queryFlag" value="<?=$_GET["$queryFlag"]?>" >
	<input type="hidden" id="fromDate" name="fromDate" value="<?=$fromDate?>" >
	<input type="hidden" id="toDate" name="toDate" value="<?=$toDate?>" >
	<br/><br/>
	<? if($_GET["queryFlag"]!="true" && $allCount>0) {?>
	Select Format <font color="#FF0000">*</font> : 
	<select name="exportFormat" id="exportFormat">
	<option value="">Select an Option</option>				
	<option value="xls">EXCEL</option>
	<option value="csv">CSV</option>
	<option value="html">HTML</option>
	</select>
	<input name="btnExport" id="btnExport" type="button"  value="Export All" onClick="exportAgentDistSummary();">
	<script language="JavaScript">
	SelectOption(document.forms[0].exportFormat, "<?=$_REQUEST["exportFormat"]; ?>");
	</script>
	<? }?>
	<? if($allCount>0) {?>
	<br/><br/>
	<input type="button" name="Submit2" value="Print This Report" id="printReport">
	<? }?>
</div>
</form>
</body>
</html>