<?
session_start();
include ("../include/config.php");
$date_time = date('d/m/Y  h:i:s A');
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}




if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

$buttonValue = $_GET["transSend"];	
	/**
	   Added by Niaz Ahmad at 27-04-2009 @4847
	   when send print command from quick menu then it will show all selected transaction 
	   detail and separate all transactions by horizontal line.
	   
	  */	
	if(isset($_REQUEST["checkbox"]) && sizeof($_REQUEST["checkbox"]) > 0)
	{
		for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
		{
			$transID = $_REQUEST["checkbox"][$i];
			$queryTransaction[$i] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			
			if($queryTransaction[$i]["custAgentID"] != '')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[$i]["custAgentID"] . "'";
				$custAgent=$queryTransaction[$i]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}	
		
			$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[$i]["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[$i]["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
		}
			$horizontalLine = true;
	} 
	else{	
		
			if($transID!='')
			{
				$queryTransaction[0] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction[0]["custAgentID"]!='')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[0]["custAgentID"] . "'";
				$custAgent=$queryTransaction[0]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}
		$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[0]["customerID"] . "'";
		$customerContent = selectFrom($queryCust);
		$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[0]["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
}				
		
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.border {
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
}

.labelFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
.linkFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color:#000000;
}

.tddataFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	vertical-align: baseline;
	font-variant:normal;
}


.headingFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	font-weight: bold;
	
}
.tddataFontsmall {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	vertical-align: baseline;
	}
	

.italics {	
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			font-style:italic;
			}	

.tdsize {
			 height:50px;
			 vertical-align:top;
		   }

			
.tditalics {
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			font-style:italic;
			vertical-align:top;
			height:30px;
		   }

			
		}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<?
	for($i=0; $i<sizeof($queryTransaction); $i++)
	{
?>
<table width="100%" border="0"  >
		<tr>
		<td>
		<table width="650" border="0"  align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td class="headingFont">
				<? if(SYSTEM != "faithExchange") { ?>
				LEMO INTEGRATED <br> <br>
				<? } ?>
		</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table width="650" border="1"  align="center" cellpadding="3" cellspacing="0">
		<tr>
			<td class="tditalics">Transaction ID:-</td>
			<td valign="top" class="tddataFont"><?=$queryTransaction[$i]["refNumberIM"]?></td>
			<td valign="top" class="tditalics">Transaction Date:- </td>
			<td valign="top" class="tddataFont"><? 
			$transDate = $queryTransaction[$i]["transDate"];
			echo date('d/m/Y H:i:s A',strtotime($transDate));
			?></td>
		</tr>
		<tr>
			<td class="tditalics">Transaction Type:- </td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]["transType"];?></td>
			<td valign="top" class="tditalics">Secret Code :-</td>
			<td valign="top" class="tddataFont"><? echo $queryTransaction[$i]["benIDPassword"];?></td>
		</tr>
		<tr>
			<td class="tditalics">Receiving Country:-</td>
			<td valign="top" class="tddataFont"><? echo $benificiaryContent["Country"]?></td>
			<td colspan="2" valign="top"><br><br></td>
		</tr>
		<tr>
		<td colspan="2" class="tdsize"><span class="italics">Sender Details:-</span><br>
			  <span class="tddataFont">
			  
			  <? //echo $customerContent["accountName"].$customerContent["IDType"];?>
			  
			  <? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?>
			  <br>
			  <? echo $customerContent["Address"] . " " . $customerContent["Address1"]?>
			  <br>
			  <? echo $customerContent["City"]." , ".$customerContent["Country"]?>		
			  <br>
			  <? echo $customerContent["Zip"].", Phone-".$customerContent["Phone"]?></span>			</td>
			<td colspan="2" class="tdsize" >
			<span class="italics">Receiver  Detail :-</span>
			<br>
			<span class="tddataFont">
			<? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?>
			<br>
			<? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?>
			<br>
		  <? echo $benificiaryContent["City"].",".$benificiaryContent["Country"].",Phone-".$benificiaryContent["Phone"]?>			</span>			</td>
		  </tr>
			<tr>
			<td colspan="2" class="tdsize">
			<span class="italics">Sending Agent Name and Address:-</span> <br>
			<span class="tddataFont">
			
			<? echo $senderAgentContent["agentCompany"]?>
			<br>
			<? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?>
			<? echo ", ".$senderAgentContent["agentCity"].", ".$senderAgentContent["agentZIP"]?>
			<br>
			<? echo "Phone-".$senderAgentContent["agentPhone"];?>			</span>			</td>
	 
		<td colspan="2" class="tdsize">
		<span class="italics">Receiving Agent Name and Address:-</span> <br>

		<? 
		if($queryTransaction[$i]["transType"] == "Bank Transfer")
		 {
				$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
				$qEUTrans = selectFrom("SELECT DISTINCT `countryRegion` FROM ".TBL_COUNTRY." WHERE countryName = '".$benificiaryContent["Country"]."'");
				$queryDistributorBank = "select agentCompany,agentPhone  from " .TBL_ADMIN_USERS. " where  userID  ='" .$queryTransaction[$i]["benAgentID"]. "'";
				$queryExecuteDistributorBank = selectFrom($queryDistributorBank);	
				
								
				if (CONFIG_EURO_TRANS_IBAN == "1" && $qEUTrans["countryRegion"] == "European")
				{
					 echo $benBankDetails["IBAN"];
					 echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;");
					 echo ($benificiaryContent["Country"] == "GEORGIA" ? $_SESSION["ibanRemarks"] : "&nbsp;");
				 }
				 else
				 {
				  ?>
		  		  <span class="tddataFont"> 
				  <?
				  		//debug($benBankDetails);
						echo $benBankDetails["bankName"];
						//echo $benBankDetails["accNo"]; 
						//echo $benBankDetails["branchCode"];
						
						if(CONFIG_CPF_ENABLED == '1')
						{
						echo  "CPF Number*";
						echo $benBankDetails["ABACPF"];
						}		  
												
						echo "<br>".$benBankDetails["branchAddress"]; 
						
						if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
						{
						//	echo $benBankDetails["accountType"];
						}
						echo"<br>Phone-".$queryExecuteDistributorBank["agentPhone"];
				 ?>		
				</span>		
				<?			
				}
				
		} 
		if($queryTransaction[$i]["transType"] == "Pick up")
		{
		  
				$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $queryTransaction[$i]["collectionPointID"] . "'";
				$cpContent = selectFrom($queryCust);	
		
				$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $cpContent["cp_ida_id"] . "'";
				$queryExecute = selectFrom($queryDistributor);		
				if($cpContent["cp_id"] != "")
				{
				?>	
					<span class="tddataFont">
					<? 
					if (CONFIG_SHOW_DIST_BANK_NAME=="1")
						print $queryExecute["name"]." (".$cpContent["cp_corresspondent_name"].")";
					else
					   	print $queryExecute["name"];
					?>
				    <!--// commented no included in reciept echo $cpContent["cp_contact_person_name"]-->
					 <br>
					  <? echo $cpContent["cp_branch_address"]?>
					 <br>
					 <? echo $cpContent["cp_city"].", ".$cpContent["cp_country"]?>
					 <br>
					 <? echo "Phone-".$cpContent["cp_phone"]?>					</span>
			  <?
				}
		}
		?>			</td>
		</tr>
	<tr>
		<td colspan="4" class="tditalics">Transaction Details </td>
		</tr>
	<tr>
		<td class="tditalics">Sending Amount:- </td>
		<td valign="top" class="tddataFont">
		
		<? echo $queryTransaction[$i]['transAmount']?>
			<?
			if(CONFIG_ENABLE_DOMESTIC != '1')
			{
				  print $queryTransaction[$i]["currencyFrom"];
			}
			?>		</td>
		<td class="tditalics">Payout Amount:-</td>
			
		<td valign="top" class="tddataFont"><? if(CONFIG_ENABLE_DOMESTIC != '1') echo $queryTransaction[$i]["localAmount"]." ".$queryTransaction[$i]["currencyTo"] ?></td>
	</tr>
	<tr>
		<td class="tditalics">Transfer Charges :-</td>
		<td valign="top"  class="tddataFont"><? if(CONFIG_ENABLE_DOMESTIC != '1') echo $queryTransaction[$i]['IMFee']+$queryTransaction[$i]["bankCharges"]?></td>
		<td class="tditalics">Exchange Rate:- </td>
		<td class="tddataFont"><? if(CONFIG_ENABLE_DOMESTIC != '1') echo $queryTransaction[$i]['exchangeRate']?></td>
	</tr>
	<tr>
		<td class="tditalics"  colspan="1">Total Paying Amount :- </td>
		<td class="tddataFont" colspan="3"><? if ($dis!="on") echo $queryTransaction[$i]["totalAmount"]?>  </td>
	</tr>
	<tr>
		<td class="tditalics"  colspan="4">&nbsp;</td>
		</tr>
	<tr>
		<td colspan="4" class="tddataFontsmall" ><ol>
			<li>The sender hereby agrees that all present and future data provided
may be stored and used as the company considers in its opinion to be of
benefit to the company
.</li>
			<li>This receipt must be produced before any changes can be made to the
transaction or before cancellation</li>
			<li>Please be advised that any Name or Location changes will incur an
Administrative fee which is currently &pound;2 (or N500).  This can be changed
without prior notice.</li>
<li>In the event of a cancellation request, issuing of the refund of
principal amount sent (where applicable) would typically take 3-5
working days.  However, this procedure might take up to one calendar
month in some circumstances because written advice from the receiving
country is required before a refund can be issued.  Please note that
transfer charges are non-refundable</li>
			<li>If calling the Customer Support Centre with regards to pay-out in a
receiving country, please ensure that the following requirements have
been met: <br>
				a. The person collecting the money is the same person specified on the
paperwork.<br>
				b. The person collecting has gone to the correct branch/pay-out location
and gave the correct information as specified.<br>
				c. The person collecting has provided the specified photo identification
at the payout location.</li>
			<li>In case of any problem, please ring: 0161 2737375   0161
2230411  07957883455  07747465588. </li>
		</ol>		</td>
		</tr>
</table>
</td>
</tr>
	<tr>
	<td>
	 	<? if($horizontalLine) { ?>
		 <hr noshade="noshade" size="1" />
	<?
	   }
	 } // end for loop
	 ?>			
   <table align="center" width="650" border="0" cellpadding="0"  cellspacing="0">
   <tr>
	<td height="60" valign="bottom">
	<? if(SYSTEM != "faithExchange") { ?>
	LEMO INTEGRATED
	CO. LTD 
	<? } ?>
	</td>
	</tr>
	<tr>
	<td>
	<div class='noPrint' align="right">
	<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create Transaction</a>
	</div>
	</td>
	</tr>

</table>
</td>
</tr>
</table>
</body>
</html>