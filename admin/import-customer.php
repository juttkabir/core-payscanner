<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
require_once './cm_xls/Excel/reader.php';

//$agentType = getAgentType();


function ExcelSerialDateToDMY($nSerialDate)
{
    // Excel/Lotus 123 have a bug with 29-02-1900. 1900 is not a
    // leap year, but Excel/Lotus 123 think it is...
    if ($nSerialDate == 60)
    {
        $nDay    = 29;
        $nMonth    = 2;
        $nYear    = 1900;

        return;
    }
    else if ($nSerialDate < 60)
    {
        // Because of the 29-02-1900 bug, any serial date 
        // under 60 is one off... Compensate.
        $nSerialDate++;
    }

    // Modified Julian to DMY calculation with an addition of 2415019
    $l = $nSerialDate + 68569 + 2415019;
    $n = (int)(( 4 * $l ) / 146097);
            $l = $l - (int)(( 146097 * $n + 3 ) / 4);
    $i = (int)(( 4000 * ( $l + 1 ) ) / 1461001);
        $l = $l - (int)(( 1461 * $i ) / 4) + 31;
    $j = (int)(( 80 * $l ) / 2447);
     $nDay = $l - (int)(( 2447 * $j ) / 80);
        $l = (int)($j / 11);
        $nMonth = $j + 2 - ( 12 * $l );
    $nYear = 100 * ( $n - 49 ) + $i + $l;
    
     return($nYear."-".$nMonth."-".$nDay);
}

if($_POST["Submit"] != "")
{
	if ($_FILES["csvFile"]["size"] <= 0)
	{
		$msg = CAUTION_MARK . " Please select file to import.";
	} else {
	
		$ext = strtolower(strrchr($_FILES["csvFile"]["name"],"."));

		if ($ext == ".txt" || $ext == ".csv" || $ext == ".xls" )
		{
			if (is_uploaded_file($_FILES["csvFile"]["tmp_name"]))
			{
				$Pict="$username-" . date("Y-m-d") . $ext;
				$filename = $_FILES["csvFile"]["tmp_name"];
				$data = new Spreadsheet_Excel_Reader();
				// Set output Encoding.
				$data->setOutputEncoding('CP1251');
				$data->read($filename);
				error_reporting(E_ALL ^ E_NOTICE);
				$tran_date = date("Y-m-d");
				
				if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
					$strCustomerNumberPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
				else
					$strCustomerNumberPrefix = "C-";
				
				
				$counter = 0;
				$counter2 = 0;
				$unlinked = 0;
				
				for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) 
				{
					// Code added by Usman Ghani against #3211 Minas Center Import data
					if ( CONFIG_IMPORT_CUSTOM_SENDERS_DATA == 1 )
					{
						$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //customerID
						$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //Title
						$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //firstName*
						$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);  //middleName
						$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	// lastName*
						$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);	// Gender
						$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);	// Sender Registration Date
						$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	//Address1*
						$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);  //Address2
						$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);	// Country*
						$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);	// City*
						$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Zip
						$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //State
						$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //Phone*
						$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //Mobile
						$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //email
						$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);// Date of Birth
						$data1[18]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][18]);  //IDType
						$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //IDNumber
						$data1[20]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][20]);  //IDExpiry
						$data1[21]  = $data->sheets[0]['cells'][$i][21];  //otherId
						$data1[22]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][22]);  //Other ID Name
						$data1[23]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][23]);  //Marital Status
						$data1[24]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][24]);  //Secret Word
						
						
						
						// Check for record existance based on customer ID
						if(!empty($data1[1]) || $data1[1] != 0)
						{
							$password = createCode();
							$customerName = $data1[2] . " " . $data1[3] . " " . $data1[4];
							
							$accountName = $strCustomerNumberPrefix.$data1[1];
						
							$sql = "select customerID from customer where customerID=".$data1[1];
							$result = mysql_query($sql) or die(mysql_error());
							$numrows = mysql_num_rows($result);
							
							if($numrows > 0)
							{
								// Update
								$Querry_Sqls = "UPDATE ".TBL_CUSTOMER." SET 
															Title='".$data1[2]."', firstName='".$data1[3]."', middleName='".$data1[4]."',lastName='".$data1[5]."', gender='".$data1[6]."',
															Address='".$data1[8]."', Address1='".$data1[9]."', Country='".$data1[10]."',
															City='".$data1[11]."', Zip='".$data1[12]."', State='".$data1[13]."', Phone='".$data1[14]."',
															Mobile='".$data1[15]."', email='".$data1[16]."', dob='".$data1[17]."', IDType='".$data1[18]."',
															IDNumber='".$data1[19]."', IDExpiry='".$data1[20]."', otherId='".$data1[21]."', otherId_name='".$data1[22]."', marital_status='".$data1[23]."',secret_word='".$data1[24]."'
												WHERE
														customerID = ".$data1[1]."
												";
								$result = mysql_query($Querry_Sqls) or die(mysql_error());
								$counter2 ++;
							} else {
								// Insert
								$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." 
													(
														customerID, Title, firstName, middleName, lastName,gender,created, 		
														Address, Address1, Country, City, Zip, State, 
														Phone, Mobile, email, dob, IDType, IDNumber, IDExpiry, otherId, 
														otherId_name, marital_status, secret_word,customerName, acceptedTerms, password, accountName
													)
												VALUES 
													(
														".$data1[1].", '".$data1[2]."', '".$data1[3]."', '".$data1[4]."',
														'".$data1[5]."', '".$data1[6]."', NOW(), '".$data1[8]."',
														'".$data1[9]."', '".$data1[10]."', '".$data1[11]."', '".$data1[12]."',
														'".$data1[13]."', '".$data1[14]."', '".$data1[15]."', '".$data1[16]."',
														'".$data1[17]."', '".$data1[18]."', '".$data1[19]."', '".$data1[20]."',
														'".$data1[21]."', '".$data1[22]."', '".$data1[23]."', '".$data1[24]."',																												
														'".$customerName."', 'Yes', '".$password."', '".$accountName."'
													)";
								$result = mysql_query($Querry_Sqls) or die(mysql_error());
								$counter++;
							}
						} else {
							$unlinked++;
						}
					} // End of code against 3211 Minas Center Import data
					else
					{
						$data1[1]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][1]);  //Title
						$data1[2]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][2]);  //firstName*
						$data1[3]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][3]);  //middleName
						$data1[4]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][4]);	// lastName*
						$data1[5]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][5]);	//Address*
						
						$data1[6]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][6]);  //Address1
						$data1[7]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][7]);	// Country*
						$data1[8]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][8]);	// City*
						$data1[9]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][9]);  //Zip
						$data1[10]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][10]);  //State
						$data1[11]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][11]);  //Phone*
						$data1[12]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][12]);  //Mobile
						$data1[13]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][13]);  //email
						$data1[14]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][14]);  //dob
						$data1[15]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][15]);  //IDType
						
						
						$data1[16]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][16]);  //IDNumber
						$data1[17]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][17]);  //IDExpiry
						//$data1[17]  = $data->sheets[0]['cells'][$i][17];  //IDExpiry
						$data1[18]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][18]);  //otherId
						$data1[19]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][19]);  //otherId_name
						
						$data1[20]  = ereg_replace("\'|NULL", "", $data->sheets[0]['cells'][$i][20]);  //customer Id
						
						$password = createCode();
						
						//debug($data1);
						
						$customerName = $data1[2] . " " . $data1[3] . " " . $data1[4];
						$field = "customerName,Address,City,Phone";
						$values = $customerName.",".$data1[5].",".$data1[8].",".$data1[11];
						$tble = "customer";
						$retValue = "customerID";
						
						$var = checkExistence($field,$values,$tble,$retValue);
						
						if($var == '')
						{
							$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (customerID, password, Title, firstName, middleName, lastName, Address, 
							Address1, Country, City, Zip, State, Phone, Mobile, email, dob, acceptedTerms, IDType, 
							IDNumber, IDExpiry, otherId, otherId_name, created, customerName) VALUES 
							('".$data1[20]."','$password','".$data1[1]."', '".$data1[2]."', '".$data1[3]."', '".$data1[4]."', '".$data1[5]."',
							 '".$data1[6]."', '".$data1[7]."', '".$data1[8]."', '".$data1[9]."', '".$data1[10]."', '".$data1[11]."', '".$data1[12]."', '".$data1[13]."', 
'".$data1[14]."','".$data1[15]."',
							  '".$data1[16]."', '".$data1[17]."', '".$data1[18]. "','" 
.$data1[19]. "','".$data1[20]."','".date("Y-m-d")."','".$customerName."')";

							
							//debug($Querry_Sqls);
							
							insertInto($Querry_Sqls);
	
							$customerID = mysql_insert_id();
	
								$customerCode = $strCustomerNumberPrefix.$customerID;
								$numberQuery = " update ".TBL_CUSTOMER." set accountName = '".$customerCode."' where customerID = '".$customerID."'";
								update($numberQuery);	
	
							$counter ++;
						}	
						else
						{
							$counter2 ++;
						}
					}
				}
				$msg = "$counter New Customers Imported.<br />$counter2 Found Duplicate and Updated.<br />$unlinked Found Missing ID and Ignored.";
				
				//exit;
					// END
			}
			else
			{
				$msg = CAUTION_MARK . " Your file in not uploaded due to some error.";
			}
		}
		else
		{
			$msg = CAUTION_MARK . " File format must be .txt, .csv or .dat";
		}	
	}
}


// $logoContent = selectFrom("select logo from " . TBL_ADMIN_USERS. " where username='$username'");



?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: normal;
}
-->
</style>
<body>
<script language="javascript">
function checkForm(theForm) {
	if(theForm.transID.value == "" || IsAllSpaces(theForm.transID.value)){
    	alert("Please provide transaction ID.");
        theForm.transID.focus();
        return false;
    }
	if(theForm.subject.value == "" || IsAllSpaces(theForm.subject.value)){
    	alert("Please provide the subject for complaint.");
        theForm.subject.focus();
        return false;
    }
	if(theForm.details.value == "" || IsAllSpaces(theForm.details.value)){
    	alert("Please provide the details of complaint.");
        theForm.details.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

</script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc"><b><strong><font color="#FFFFFF">Import <?=__("Sender");?>s </font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="800"  border="0">
  <tr>
    <td  valign="top"><fieldset>
    <legend class="style2">Import <?=__("Sender");?>s </legend>
    <br>
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
			<form name="addComplaint" action="import-customer.php" method="post" enctype="multipart/form-data">
			  
			  <tr align="center" class="tab-r">
			    <td colspan="2">Please follow the instructions on the Readme.txt file.</td>
			    </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
				<a href="#" onClick="javascript:window.open('import-readme.php?page=custben', 'ReadMe', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=250,width=650,top=200,left=170')" class="style2">
				Readme.txt
			  </a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="center" bgcolor="#DFE6EA" colspan="2">
					<?
						if(CONFIG_IMPORT_CUSTOM_SENDERS_DATA == 1)
						{
							$template = "xls/sampleTempSenderMC1.xls"; 
						} else {
							$template = "xls/sampleTempSender.xls";
						}
					?>
				<a href="<?=$template?>" class="style2">Sample Template</a>	
				</td>
			  </tr>
			  <tr>
				<td width="25%" align="right" bgcolor="#DFE6EA">Excel File </td>
			    <td width="75%" bgcolor="#DFE6EA">			      <input name="csvFile" type="file" id="csvFile" size="15"></td>
			  </tr>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA"><input name="Submit" type="submit" class="flat" value="Submit"></td>
		      </tr></form>
		      <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			  	<? if($msg!= " $counter Customers are Imported Successfully and there were $counter2 duplicate values"){ ?>
			    <td colspan="2"><font color="#CC0000"><strong><? echo $msg?></strong><br></td>
			    	<? } else { ?>
			    	 <td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
			    	 	<? } ?>
			    </tr>
			  <tr>
			  <?
			  }
			  ?>
		      
			</table>
		    <br>
        </fieldset></td>
    <td width="431" valign="top">
	<? if ($str != "")
	{
	?>
	
	<fieldset>
    <legend class="style2">Import Senders Results </legend>
    <br>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
      <form name="addComplaint" action="import-customer.php" method="post" enctype="multipart/form-data">
        <tr>
          <td valign="top" bgcolor="#DFE6EA"><font color="#3366FF"><? echo $str?></font></td>
          </tr>
          
      </form>
    </table>
    <br>
    </fieldset>
	<?
	}
	?>
	</td>
  </tr>
  
  
</table>

	</td>
  </tr>
</table>
</body>
</html>
