<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
//$agentType = getAgentType();
///////////////////////History is maintained via method named 'activities'
$countryBasedFlag = false;
//debug($_REQUEST);

if(defined("CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES") && CONFIG_ADD_USE_COUNTRY_BASED_SERVIECES=="1"){
	$countryBasedFlag = true;
}
if($_POST["sel"] == "country")
{
	//echo $_POST["Country"];
	//$contentCountry = selectFrom("select * from " . TBL_SERVICE . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "'");
	$contentCountryQ = "select * from " . TBL_SERVICE . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "'";
	if($countryBasedFlag){
		$contentCountryQ = "select * from " . TBL_SERVICE_NEW . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "' order by serviceAvailable asc";		
	}
	$countryNameSel = selectFrom("select countryName from ".TBL_COUNTRY." where 1 and countryType like '%destination%' and countryId='".$_POST["Country"]."'");
	$countryNameSelSer = $countryNameSel["countryName"];

	$contentCountry = selectFrom($contentCountryQ);
	$serviceIdV = $contentCountry["serviceId"];
	$CountryV = $_POST["Country"];
	$CountryOriginV = $_POST["CountryOrigin"];
	$service = $contentCountry["serviceAvailable"];
	$delivery = $contentCountry["deliveryTime"];
	$bankChargesV = $contentCountry["bankCharges"];
	$currencyCharge = $contentCountry["outCurrCharges"];
	$currenciesV = $contentCountry["currencyRec"];
	$chkBank = $contentCountry["bankChargesDisabled"];
	$showServiceCurr = $currenciesV;
}
else{
	if($_POST["Submit"] == "Submit")
	{
		$services = @implode(", " , $_POST["service"]);
		$currenciesV = @implode(", " , $_POST["currencies"]);
		$bankChargesV = $_POST["bankCharges"];
		$currencyCharge = $_POST["currencyCharge"];
		$serviceIDs = explode(",",$_POST["serviceIds"]);
		//echo $query = "update " . TBL_CITIES . " set  currencyRec = '$currenciesV', serviceAvailable = '$services', currencyRec = '". $_POST["currency"]."', deliveryTime = '". $_POST["delivery"]."' where country = '".$_POST["Country"]."'";
		$contentServiceQ  = "select serviceId from " . TBL_SERVICE . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "'";
		if($countryBasedFlag){
			$contentServiceQ = "select serviceId from " . TBL_SERVICE_NEW . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "'";
		}
		$contentService = selectFrom($contentServiceQ);
		if($contentService["serviceId"] != ""){
			if($countryBasedFlag){
				for($sU;$sU<count($serviceIDs);$sU++){
					$query = "update " . TBL_SERVICE_NEW . " set  
					currencyRec = '$currenciesV', 
					bankCharges = '".$_POST["bankCharges"]."',
					outCurrCharges =  '".$_POST["currencyCharge"]."'
					where serviceId = '".$serviceIDs[$sU]."'";
					update($query);
				}
				$msg = "Services has been Updated.";
				$descript = "Services are updated";
				activities($_SESSION["loginHistoryID"],"UPDATION",$contentService["serviceId"],TBL_SERVICE_NEW,$descript);
			}
			else{
				$query = "update " . TBL_SERVICE . " set  
				currencyRec = '$currenciesV', 
				serviceAvailable = '$services',  
				deliveryTime = '". $_POST["delivery"]."',
				bankCharges = '".$_POST["bankCharges"]."',
				outCurrCharges =  '".$_POST["currencyCharge"]."'
				where serviceId = '".$contentService["serviceId"]."'";
				update($query);
				$descript = "Services are updated";
				activities($_SESSION["loginHistoryID"],"UPDATION",$contentService["serviceId"],TBL_SERVICE,$descript);
			}
		}elseif($countryBasedFlag && $contentService["serviceId"]==""){
			$msg = "Please Select services from Drop Down to Use them.";
		}else{
			
			insertInto("insert into ".TBL_SERVICE." (fromCountryId, toCountryId, currencyRec, serviceAvailable, deliveryTime, bankCharges, outCurrCharges) VALUES
			('".$_POST["CountryOrigin"]."', '". $_POST["Country"]."','".$currenciesV."','". $services."','".$_POST["delivery"]."','".$_POST["bankCharges"]."','".$_POST["currencyCharge"]."')");
			
			$descript = "Services are Added";
			$serviceIdds = @mysql_insert_id();
			activities($_SESSION["loginHistoryID"],"INSERTION",$serviceIdds,TBL_SERVICE,$descript);
			$msg = SUCCESS_MARK . " Operation performed successfully.";
		}
		//echo $query;	
	}
	if($_POST["addNewServiceBtn"] == "Add New Service"){
		$newServiceV = trim($_POST["newService"]);
		$getNewServiceQ = "select serviceId from " . TBL_SERVICE_NEW . " where upper(serviceAvailable) ='".$newServiceV."'";
		$getNewServiceR = selectFrom($getNewServiceQ);
		if($getNewServiceR["serviceId"] != ""){
			$msg = " The Service '".ucwords($newServiceV)."' already exists.";
		}else{
			
			insertInto("insert into ".TBL_SERVICE_NEW." (serviceAvailable) VALUES
			('".$newServiceV."')");
			
			$descript = "New Service are Added";
			$serviceIdds = @mysql_insert_id();
			activities($_SESSION["loginHistoryID"],"INSERTION",$serviceIdds,TBL_SERVICE_NEW,$descript);
			$msg = SUCCESS_MARK . " Service has been added successfully.";
		}
	}
	if($_POST["useServiceBtn"] == "Use Selected Service"){
		$useDeliveryV = trim($_POST["useDelivery"]);
		$useServiceV = trim($_POST["useService"]);
		if($useServiceV!=""){
			$getUsedServiceQ = "select serviceId,currencyRec from " . TBL_SERVICE_NEW . "  where ((fromCountryId='0' and toCountryId='0') OR (fromCountryId='".$_POST["CountryOrigin"]."' and toCountryId='".$_POST["Country"]."')) and serviceAvailable = '".$useServiceV."' order by toCountryId desc";
			$getUsedServiceR = selectFrom($getUsedServiceQ);
			if($getUsedServiceR["serviceId"]!=""){
				$queryUpdateSer = "update " . TBL_SERVICE_NEW . " set  
					deliveryTime = '". $_POST["useDelivery"]."',
					currencyRec = '". $getUsedServiceR["currencyRec"]."',
					fromCountryId = '". $_POST["CountryOrigin"]."',
					toCountryId = '".$_POST["Country"]."'
					where serviceId = '".$getUsedServiceR["serviceId"]."'";
				update($queryUpdateSer);
			}
			else{
				insertInto("insert into ".TBL_SERVICE_NEW." (fromCountryId, toCountryId, serviceAvailable, deliveryTime) VALUES
				('".$_POST["CountryOrigin"]."','". $_POST["Country"]."','". $useServiceV."','".$useDeliveryV."')");
			}
			$msg = "Service '".strtoupper($useServiceV)."' has been used";
		}
		else{
			$msg = "Please select a service from service from Drop Down";
		}
	}
	if($_POST["removeServiceBtn"] == "Remove Selected Service"){
		$selectedServicesArr = $_POST["selectedServices"];
		for($ss=0;$ss<count($selectedServicesArr);$ss++){
			if($ss==0)
				$amp="";
			else
				$amp=",";				
			$selectedServicesV.=	$amp."'".$selectedServicesArr[$ss]."'";
		}
		$getNewServiceNames = selectMultiRecords("select serviceAvailable from " . TBL_SERVICE_NEW . " where serviceId IN(".$selectedServicesV.")");
		if($selectedServicesV!=""){
			update("delete from ".TBL_SERVICE_NEW." where serviceId IN(".$selectedServicesV.")");
		}
		else{
			$msg = "Service(s) can not be removed from here";
		}
		for($ser=0;$ser<count($selectedServicesArr);$ser++){
			$getNewServiceHistory = selectFrom("select serviceId from " . TBL_SERVICE_NEW . " where serviceId ='".$selectedServicesArr[$ser]."'");
			if($getNewServiceHistory["serviceId"]==""){
				insertInto("insert into ".TBL_SERVICE_NEW." (serviceAvailable) VALUES
				('".$getNewServiceNames[$ser]["serviceAvailable"]."')");
			}
		}
		
	}
	if($_POST["useCurrencyBtn"] == "Use Selected Currencies" && is_array($_POST["selectedServices"])){
		$useCurrencies = $_POST["currencies"];
		$selectedServicesArr = $_POST["selectedServices"];
		if(is_array($useCurrencies)){
			for($s=0;$s<count($selectedServicesArr);$s++){
				$getCurrRecordRows = selectFrom("select currencyRec from ".TBL_SERVICE_NEW." where  serviceId ='".$selectedServicesArr[$s]."'");
				$getCurrArr = explode(",",$getCurrRecordRows["currencyRec"]);
				if($getCurrArr[0]!="" && $useCurrencies[0]!=""){
					$useCurrencies = array_merge($getCurrArr,$useCurrencies);
					$useCurrencies = array_unique($useCurrencies);
					$useCurrencies = array_values($useCurrencies);
				}
				$useSelCurrencies = "";
				for($ss=0;$ss<count($useCurrencies);$ss++){
					if($ss==0)
						$amp="";
					else
						$amp=",";				
					$useSelCurrencies.=	$amp.$useCurrencies[$ss];
				}
				if($useSelCurrencies!=""){
					$queryUpdateSer = "update " . TBL_SERVICE_NEW . " set  
						currencyRec = '". $useSelCurrencies."'
						where serviceId ='".$selectedServicesArr[$s]."'";
					update($queryUpdateSer);
					$msg = "Currencies has been used";
				}
				else{
					$msg = "Please select Currencies to Use";				
				}
			}
		}
		else{
			$msg = "Please select Service(s) to use for Currencies";
		}
	}
	
	/**
	 * To store the Multiple IDs against the Service
	 */
	if(!empty($_REQUEST["updateIds"]) && is_array($_POST["selectedServices"]))
	{
		$selectedServicesArr = $_POST["selectedServices"];
		//debug($_REQUEST);
		if(!empty($_REQUEST["multipleIds"]))
		{
			$strMultipleIds = "";
			foreach($_REQUEST["multipleIds"] as $mulIdKey => $mulIdVal)
				$strMultipleIds .= $mulIdVal.",";
			for($s=0;$s<count($selectedServicesArr);$s++){
				$queryUpdateSer = "update " . TBL_SERVICE_NEW . " set  
					multiple_ids = '". $strMultipleIds."'
					where serviceId='".$selectedServicesArr[$s]."'";
				update($queryUpdateSer);
			}
				$msg = "Multiple Ids updated.";
		
		}
	
	}
	
	if($_POST["removeCurrencyBtn"] == "Remove Selected Currencies" && is_array($_POST["selectedCurrencies"])){
		$selectedCurrenciesArr = $_POST["selectedCurrencies"];
		$getCurrRecordRows = selectFrom("select currencyRec from ".TBL_SERVICE_NEW." where serviceId='".$_POST["serviceIdsCurr"]."'");
		$getCurrValues = $getCurrRecordRows["currencyRec"];
		$getCurrValuesArr = explode(",",$getCurrValues);
		$newCurrenciesArr = array_diff($getCurrValuesArr, $selectedCurrenciesArr);
		if($newCurrenciesArr!=""){
			$newCurrenciesArr = array_values($newCurrenciesArr);
		}
		else{
			$newCurrenciesArr = "";
		}
		$newCurrencies = "";
		for($ss=0;$ss<count($newCurrenciesArr);$ss++){
			if($ss==0)
				$amp="";
			else
				$amp=",";				
			$newCurrencies.=	$amp.$newCurrenciesArr[$ss];
		}
		if($_POST["serviceIdsCurr"]!=""){
			update("update ".TBL_SERVICE_NEW." set currencyRec='".$newCurrencies."' where serviceId='".$_POST["serviceIdsCurr"]."'");
		}
		
	}
	$contentCountryQ = "select * from " . TBL_SERVICE . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "'";
	if($countryBasedFlag){
		$contentCountryQ = "select * from " . TBL_SERVICE_NEW . " where fromCountryId = '".$_POST["CountryOrigin"]."' and toCountryId = '" . $_POST["Country"] . "' order by serviceAvailable asc";
	}	
	
	$countryNameSel = selectFrom("select countryName from ".TBL_COUNTRY." where 1 and countryType like '%destination%' and countryId='".$_POST["Country"]."'");
	$countryNameSelSer = $countryNameSel["countryName"];

	$contentCountry= selectFrom($contentCountryQ);

	$CountryV = $_POST["Country"];
	$CountryOriginV = $_POST["CountryOrigin"];

	$serviceIdV = $contentCountry["serviceId"];
	$service = $contentCountry["serviceAvailable"];
	$delivery = $contentCountry["deliveryTime"];
	$bankChargesV = $contentCountry["bankCharges"];
	$currencyCharge = $contentCountry["outCurrCharges"];
	$currenciesV = $contentCountry["currencyRec"];
	$showServiceCurr = $currenciesV;	
}
/*
if($_POST["Submit"] != "")
{
	$contTrans = selectFrom("select transID from  ". TBL_TRANSACTIONS."  where transID='".$_POST["transID"]."' OR refNumber = '".$_POST["transID"]."' OR  refNumberIM = '".$_POST["transID"]."'");
	if($contTrans["transID"] == "")
	{
		$msg = CAUTION_MARK . " Invalid Ref No. OR Trans No.";
	}
	else
	{
		$_POST["transID"] = $contTrans["transID"];
		insertInto("insert into ".TBL_COMPLAINTS." (dated, transID, agent, subject, details) VALUES ('".getCountryTime(CONFIG_COUNTRY_CODE)."', '". $_POST["transID"]."','$username','". checkValues($_POST["subject"])."','". checkValues($_POST["details"])."')");
		$msg = SUCCESS_MARK . " Enguiry added successfully";
		insertError($msg);
		$cmpID = @mysql_insert_id();
		redirect("complaint-details.php?msg=Y&cmpID=$cmpID");
	}
}
*/
?>
<html>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style4 {
	color: #0000FF;
	font-weight: bold;
}
-->
</style>
<body>
<script language="javascript" src="jquery.js"></script>
<script language="javascript">
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}

function checkForm(theForm) {
	if(theForm.transID.value == "" || IsAllSpaces(theForm.transID.value)){
    	alert("Please provide transaction ID.");
        theForm.transID.focus();
        return false;
    }
	if(theForm.subject.value == "" || IsAllSpaces(theForm.subject.value)){
    	alert("Please provide the subject for complaint.");
        theForm.subject.focus();
        return false;
    }
	if(theForm.details.value == "" || IsAllSpaces(theForm.details.value)){
    	alert("Please provide the details of complaint.");
        theForm.details.focus();
        return false;
    }
}
function IsAllSpaces(myStr){
        while (myStr.substring(0,1) == " "){
                myStr = myStr.substring(1, myStr.length);
        }
        if (myStr == ""){
                return true;
        }
        return false;
   }

function checkService(obj){
	var originCount = document.getElementById("Country").value;
	var destCount =  document.getElementById("CountryOrigin").value;
	if(originCount=="" || destCount==""){
		alert("Please select Originating and Destination Countries");
		return false;
	}

	var ServiceV = obj.value;
	if(obj.id=="useService" && ServiceV==""){
        alert("Please Select a service to Use.");
		obj.focus();
		return false;
    }
	if(obj.id=="selectedServices" && ServiceV==""){
        alert("Please Select service(s) to Remove.");
		obj.focus();
		return false;
    }
	if(obj.id=="currencies" && (ServiceV=="" || document.getElementById("selectedServices")==null)){
        alert("Please Select currencie(s)/service(s) to Use.");
		obj.focus();
		return false;
    }
	if(obj.id=="selectedCurrencies" && ServiceV==""){
        alert("Please Select currencie(s) to Remove.");
		obj.focus();
		return false;
    }
	if(obj.id=="newService" && (ServiceV=="" || IsAllSpaces(ServiceV))){
        alert("Please Provide New Service Name.");
		obj.focus();
		return false;
    }
	if((obj.id=="selectedServices" || obj.id=="selectedCurrencies") && ServiceV!=""){
		return confirm("Are you sure to Remove ?");
	}
}
$(document).ready(function(){
	$("#loading").ajaxStart(function(){
		$(this).show();
		$("#selectedCurrRow").html('<td colspan="2" align="center" bgcolor="#999999" height="80" valign="middle"><strong>Loading Currencies Please Wait....</strong></td>');
		$("#multipleIdRow").html('<td colspan="2" align="center" bgcolor="#999999" height="200" valign="middle"><strong>Loading Ids Please Wait....</strong></td>');
	});
	$("#loading").ajaxComplete(function(request, settings){
		$(this).hide();
	});
});
function selectRespectiveCurr(theForm,toCountry){
	var selectedOpt = getChoices(theForm);
	var selectedOptArr = selectedOpt.split(",");
	var ssId = document.getElementById("selectedServices").value;
	var ssIdQS = '?sID='+ssId+'&sToCountry='+toCountry;

	if(selectedOptArr.length==1){
		$("#selectedCurrRow").load("getServicesCurr.php"+ssIdQS);
		$("#multipleIdRow").load("getMultiIdsCurr.php"+ssIdQS);
	}
}

function getChoices(theForm) 
{ 
  var values, options, option, k=0; 
  values  = new Array(); 
  options = theForm.selectedServices.options; 
  while(option=options[k++]) 
    if(option.selected) 
      values[values.length] = option.value; 
  return values.join(','); 
} 

function checkMultipleIds()
{
	if(document.getElementById("multipleIds").value == "" || document.getElementById("multipleIds").value == null)
	{
		alert("Please select one or more than one multiple Id to continue.");
		document.getElementById("multipleIds").focus();
		return false
	}
	return true;
}
</script>
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><strong><font class="topbar_tex">Add/Update Services</font></strong></b></td>
  </tr>
  <tr>
    <td>
	<table width="70%"  border="0">
  <tr>
    <td><fieldset>
    <legend class="style2">Add/Update Services </legend>
    <br>
			<form name="addComplaint" action="add-services.php" method="post">
			<table width="90%" border="0" align="center" cellpadding="5" cellspacing="1">
			  <? if($msg!= "")
			  {
			  ?>
			  <tr align="center">
			   <? if (CLIENT_NAME == "Spinzar") { ?>
          	 <td colspan="2"><b><font color= <? echo SUCCESS_COLOR ?> ><? echo $msg?></font></b><br></td>
          	 <? } else { ?>
            <td width="4%" colspan="2" class="tab-r"><? echo $msg?><br></td>
            <? } ?>
			    </tr>
			  <?
			  }
			  
			  ?>
		    <tr>
			 	<td width="27%" align="right" bgcolor="#DFE6EA">
				  Originating Country</td>
			    <td width="69%" bgcolor="#DFE6EA">
			    	<SELECT name="CountryOrigin" id="CountryOrigin" style="font-family:verdana; font-size: 11px">
                  <OPTION value="">- Select Country-</OPTION>
                 <?          
					$countiresOrigin = selectMultiRecords("select countryName, countryId from ".TBL_COUNTRY." where 1 and countryType like '%origin%' order by countryName");
					for ($i=0; $i < count($countiresOrigin); $i++){
				?>
                  <OPTION value="<?=$countiresOrigin[$i]["countryId"]; ?>">
                  <?=$countiresOrigin[$i]["countryName"]; ?>
                  </OPTION>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].CountryOrigin, "<?=$CountryOriginV?>");
                                </script></td>
			  </tr>
			    
			  <tr>
				<td width="27%" align="right" bgcolor="#DFE6EA"><input type="hidden" name="sel" value="">
				  Destination Country</td>
			    <td width="69%" bgcolor="#DFE6EA"><SELECT id="Country" name="Country" style="font-family:verdana; font-size: 11px" onChange="document.forms[0].sel.value = 'country'; document.forms[0].submit();">
                  <OPTION value="">- Select Country-</OPTION>
                  <!--option value="United States">United States</option>
                  <option value="Canada">Canada</option-->
                  <?
                  
					$countires = selectMultiRecords("select countryName, countryId from ".TBL_COUNTRY." where 1 and countryType like '%destination%' order by countryName");
					for ($i=0; $i < count($countires); $i++){
				?>
                  <OPTION value="<?=$countires[$i]["countryId"]; ?>">
                  <?=$countires[$i]["countryName"]; ?>
                  </OPTION>
                  <?
					}
				?>
                </SELECT>
			      <script language="JavaScript">
         	SelectOption(document.forms[0].Country, "<?=$CountryV?>");
                                </script></td>
			  </tr>
			  <tr>
				<td align="right" valign="top" bgcolor="#DFE6EA">Services</td>
			    <td bgcolor="#DFE6EA">
<? if($countryBasedFlag){?>
<? 
	$contentsService = selectMultiRecords("select DISTINCT(serviceAvailable) from " . TBL_SERVICE_NEW. " order by serviceAvailable");	
?>
<select name="useService" id="useService">
	<option value="">Please select One</option>
<? for($sA=0;$sA<count($contentsService);$sA++){?>
	<option value="<?=$contentsService[$sA]["serviceAvailable"]?>"><?=$contentsService[$sA]["serviceAvailable"]?></option>
<? }?>
</select>
<script language="JavaScript"> SelectOption(document.forms[0].useService, "<?=$_POST["useService"]?>"); </script>
&nbsp;&nbsp;&nbsp;
Delivery Time:&nbsp;&nbsp;<input name="useDelivery" type="text" id="useDelivery" size="10" value="">
&nbsp;&nbsp;
<input type="submit" name="useServiceBtn" id="useServiceBtn" value="Use Selected Service"  onClick="return checkService(useService);">
<? }else{?>

				<input name="service[0]" type="checkbox" id="service[0]" value="Cash Collection" <? echo (strstr($service,"Cash Collection") ? "checked"  : "")?>>
Cash Collection
<br>
<input name="service[1]" type="checkbox" id="service[1]" value="Home Delivery" <? echo (strstr($service,"Home Delivery") ? "checked"  : "")?>>
Home Delivery
<br>
<input name="service[2]" type="checkbox" id="service[2]" value="Bank Deposit" <? echo (strstr($service,"Bank Deposit") ? "checked"  : "")?>>
Bank Deposit

<? if(CONFIG_ATM_CARD_OPTION == "1"){ ?>
			 
			 <br>
<input name="service[3]" type="checkbox" id="service[3]" value="ATM Card" <? echo (strstr($service,"ATM Card") ? "checked"  : "")?>>
ATM Card
			 
			 <? } ?>
<? }?>
</td>
			 
			 
			  </tr>
<? if($countryBasedFlag){?>
<tr>
<? 
	$strQuerySer = "SELECT serviceId, serviceAvailable, deliveryTime FROM  ".TBL_SERVICE_NEW." where fromCountryId = '".$CountryOriginV."' and toCountryId = '" . $CountryV . "' ORDER BY serviceAvailable  ";
	$nResult = mysql_query($strQuerySer)or die("Invalid query: " . mysql_error()); 
	$totalResultSer = mysql_num_rows($nResult);
	$showMsg = "";
	if($countryNameSelSer=="")
		$showMsg .= "Please Select Destination Country.";
	if($totalResultSer<1 && $showMsg=="")
		$showMsg .= "No Services Selected.";
	if($countryNameSelSer!="" && $totalResultSer>0){ ?>
				  <td align="right" valign="top" bgcolor="#DFE6EA">Selected Services for <strong><?=$countryNameSelSer?></strong></td>
			    <td bgcolor="#DFE6EA" valign="top">
				<select name="selectedServices[]" id="selectedServices" multiple="multiple" size="5" onChange="selectRespectiveCurr(addComplaint,'<?=$countryNameSelSer?>');">
						  <?
							$serviceIds = "";
							$rsCounter = 0;
							while($rstRow = mysql_fetch_array($nResult))
							{	
								$separator ="";
								if($rsCounter<$totalResultSer-1){
									$separator = ",";
								}
								$selectedOpt = "";
								if($rsCounter==0)
									$selectedOpt = "selected='selected'";
								$serviceIds .= $rstRow["serviceId"].$separator;
							    echo "<option  value ='".$rstRow["serviceId"]."' $selectedOpt> ".$rstRow["serviceAvailable"].($rstRow["deliveryTime"]!=""?"  (".$rstRow["deliveryTime"].")":"")."</option>";	
								$rsCounter++;

							}
						  ?>				
				</select>
				<?
					echo "<input name='serviceIds' type='hidden' id='serviceIds' value='".$serviceIds."' >";
				?>
				&nbsp;&nbsp;
				<input type="submit" name="removeServiceBtn" id="removeServiceBtn" value="Remove Selected Service"  onClick="return checkService(selectedServices)">

				</td>
<? }else{?>
			  <tr>
				  <td align="center" valign="top" bgcolor="#999999" colspan="2"><font size="2" color="#990000"><strong><?=$showMsg;?></strong></font></td>
<? }?>
			
			<tr>
				<td valign="top" bgcolor="#DFE6EA" align="right">&nbsp;
					
				</td>
				<td valign="top" bgcolor="#DFE6EA" align="left">
					<input type="text" name="newService" id="newService" value="" size="30">&nbsp;&nbsp;&nbsp;
					<input type="submit" name="addNewServiceBtn" id="addNewServiceBtn" value="Add New Service" onClick="return checkService(newService);">
				</td>
			</tr>
			
			
						
			
<? }?>		  
		  	<tr>
				  <td align="right" valign="top" bgcolor="#DFE6EA"><? if($countryBasedFlag){ echo "Use ";}?>Receiving Currency</td>
			    <td bgcolor="#DFE6EA"><span class="style4">Hold Ctrl key for multiple selection</span><br />
				
				<select name="currencies[]" id="currencies" multiple size="10" >
				<option value="">-- Select Currency --</option>
				  <?
				    /* #5955-premier exchange by Niaz Ahmad*/
					if(CONFIG_CUSTOME_SENDING_RECEIVING_CURRENCIES == '1'){
					$strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies currencyName 
					WHERE currency_for = 'R' OR currency_for = 'B' ORDER BY currencyName";
					}else{
					 $strQuery = "SELECT DISTINCT(currencyName), description FROM  currencies ORDER BY currencyName";
					}
							
					$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error()); 
			
					while($rstRow = mysql_fetch_array($nResult))
					{
						$currency = $rstRow["currencyName"];	
						$description = $rstRow["description"];
						$erID  = $rstRow["cID"];							
						if(strstr($currenciesV,$currency) && $countryBasedFlag==false)
							echo "<option selected value ='$currency'>".$currency." --> ".$description."</option>";			
						else
							echo "<option  value ='$currency'> ".$currency." --> ".$description."</option>";	
					}
				  ?>				
				</select>
<? if($countryBasedFlag){?>
				<input type="submit" name="useCurrencyBtn" id="useCurrencyBtn" value="Use Selected Currencies"  onClick="return checkService(currencies);">				
<? }?>				</td>
			  </tr>
<? if($countryBasedFlag){?>
<tr id="selectedCurrRow">
<? 
	
	$strQueryCur = "SELECT serviceId,currencyRec FROM  ".TBL_SERVICE_NEW." where fromCountryId = '".$CountryOriginV."' and toCountryId = '" . $CountryV . "' ORDER BY serviceAvailable";
	$strQueryCurR = selectFrom($strQueryCur);
	$showMsgCur = "";
	if($countryNameSelSer=="")
		$showMsgCur .= "Please Select Destination Country.";
	if($showServiceCurr=="" && $showMsgCur=="")
		$showMsgCur .= "No Currency Used.";
	if($countryNameSelSer!="" && $showServiceCurr!=""){ 
	
	?>
	
			  <td align="right" valign="top" bgcolor="#DFE6EA">Selected Currencies for <strong><?=$countryNameSelSer?></strong></td>
			    <td bgcolor="#DFE6EA" valign="top">
				<select name="selectedCurrencies[]" id="selectedCurrencies" multiple="multiple" size="5" onChange="selectRespectiveCurr(addComplaint,'<?=$countryNameSelSer?>');">
					  <?
					  $currRecord = explode(",",$showServiceCurr);
					  $currenciesNames = $showServiceCurr;
					  for($cr=0;$cr<count($currRecord);$cr++){
							echo "<option  value ='".$currRecord[$cr]."'> ".$currRecord[$cr]."</option>";
					  }
					  ?>				
				</select>
				<?
					echo "<input name='serviceIdsCurr' type='hidden' id='serviceIdsCurr' value='".$strQueryCurR["serviceId"]."' >";
				?>
				&nbsp;&nbsp;
				<input type="submit" name="removeCurrencyBtn" id="removeCurrencyBtn" value="Remove Selected Currencies" onClick="return checkService(selectedCurrencies);">

				</td>
<? }else{?>
				  <td align="center" valign="top" bgcolor="#999999" colspan="2"><font size="2" color="#990000"><strong><?=$showMsgCur;?></strong></font></td>
<? }?>
</tr>
<? }?>
		
	<? if($countryBasedFlag && !empty($countryNameSelSer) && !empty($showServiceCurr) && !empty($showServiceCurr)) { ?>
		<tr id="multipleIdRow">
			<td align="center" valign="top" bgcolor="#DFE6EA">
				<strong>Select IDs required for beneficiary (<?=$countryNameSelSer?>)</strong>
			</td>
			<td valign="top" bgcolor="#DFE6EA">
				<?
					$strQueryIds = "SELECT serviceId,multiple_ids FROM  ".TBL_SERVICE_NEW." where fromCountryId = '".$CountryOriginV."' and toCountryId = '" . $CountryV . "' AND currencyRec!='' ORDER BY serviceAvailable";
					$strQueryIdsR = selectFrom($strQueryIds);
					$showServiceIds = $strQueryIdsR["multiple_ids"];
					
					$strMultipleIdSql = "select title, id from id_types where show_on_ben_page = 'Y' and active = 'Y'";
					$arrMultipleIds = selectMultiRecords($strMultipleIdSql);
					
					if(!empty($showServiceIds))
						$strAlreadySelectedIds = explode(",",$showServiceIds);

					if(!empty($arrMultipleIds))
					{
				?>
				<select id="multipleIds" name="multipleIds[]" multiple="multiple" style="width:300">
					<?  
						foreach ($arrMultipleIds as $arrKey => $arrVal) 
						{ 
							$strMultiIdsSelected = '';
							if(in_array($arrVal["id"], $strAlreadySelectedIds))
								$strMultiIdsSelected = 'selected="selected"';
					?>
						<option value="<?=$arrVal["id"]?>" <?=$strMultiIdsSelected?>><?=$arrVal["title"]?></option>
					<? 	
						} 
					?>
				</select>
				<br />
				<i>Press Ctrl+Click to select multiple IDs.</i>
				<br /><br />
				<input type="submit" name="updateIds" id="updateIds" value="Update Multiple Ids" onClick="return checkMultipleIds();" />
				<? }else{ ?>
					<b>Could not found any ID defined for beneficiary. For creating the ID please click <a href="id-types.php" style="color:#336666">here</a>.</b>
				<? } ?>
			</td>
		</tr>
	<? } ?>
			  <!--
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Receiving Currency</td>
			    <td bgcolor="#DFE6EA"><input name="currency" type="text" id="currency" size="10" value="<? //echo $_SESSION["currency"]?>"></td>
			  </tr>
			  -->
	<? if($countryBasedFlag==false){?>
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">Delivery Time </td>
			    <td bgcolor="#DFE6EA"><input name="delivery" type="text" id="delivery" size="10" value="<?=$delivery?>"></td>
			    </tr>
	<? }?>		  
			  <? if(CONFIG_REMOVE_ADMIN_CHARGES != "1" && CONFIG_CUSTOM_BANK_CHARGE != '1' && $countryBasedFlag==false)
           {?>  
			    <tr> 		
			    <td align="right" valign="top" bgcolor="#DFE6EA">Bank Charges</td>
			    <td bgcolor="#DFE6EA"><input name="bankCharges" type="text" id="bankCharges" size="10" value="<?=$bankChargesV?>">Apply for Bank Transfers Only </td>
			  </tr>
			  <? }?>
			  <? if(CONFIG_CURRENCY_CHARGES == '1' && $countryBasedFlag==false)
			  { 
			  	?>
				  <tr>
				    <td align="right" valign="top" bgcolor="#DFE6EA">Currency Charges</td>
				    <td bgcolor="#DFE6EA"><input name="currencyCharge" type="text" id="currencyCharge" size="10" value="<?=$currencyCharge?>">Apply where Beneficiary will recieve International Currency.</td>
				  </tr>
				  <?
			  }
			  ?>
			 <? if($countryBasedFlag==false){?> 
			  <tr>
			    <td align="right" valign="top" bgcolor="#DFE6EA">&nbsp;</td>
			    <td align="center" bgcolor="#DFE6EA">			      <input name="Submit" type="submit" class="flat" value="Submit"></td></tr>
			 <? }?>
			 <? if($countryBasedFlag){?> 
			  <tr>
			    <td valign="top" bgcolor="#DFE6EA" colspan="2" align="center">
					<a href="id-types.php" target="_blank"><font color="#336666" size="2"><strong>Click here to Manage Multiple ID(s)</strong></font></a>
				</td></tr>
			 <? }?>
			</table>
			</form>
		    <br>
        </fieldset></td>
  </tr>
</table>

	</td>
  </tr>
</table>
</body>
</html>