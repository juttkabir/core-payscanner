<?php
session_start();
/**
* @package Payex
* @subpackage Transactions
* Short Description
* This page shows the listing of unresolved bank payments 
* we can filter these transaction and also resolved and delete unresolved payments
* also from this page
*/	
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("javaScript.php");
$agentType = getAgentType();

$today = date("d/m/Y");

$modifyby = $_SESSION["loggedUserData"]["userID"];
$currencyies = selectMultiRecords("SELECT DISTINCT(currencyName) FROM currencies");

$barclaysRec = selectMultiRecords("SELECT DISTINCT(accountNo) FROM barclayspayments WHERE accountNo !=''");
?>
<html>
<head>
<title>Unresolved Payments for Item-Wise Reconciliation</title>
<link href="images/interface.css" rel="stylesheet" type="text/css"> <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css" />
<link href="css/inputScreens.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" title="basic" href="javascript/jqGrid/themes/basic/grid.css" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/coffee/grid.css" title="coffee" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/green/grid.css" title="green" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="javascript/jqGrid/themes/sand/grid.css" title="sand" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="javascript/jqGrid/themes/jqModal.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" media="screen" />

<script src="javascript/jqGrid/jquery.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.datePicker.js"></script>
<script src="javascript/jqGrid/jquery.jqGrid.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqModal.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqGrid/js/grid.celledit.js" type="text/javascript"></script>
<script src="jquery.cluetip.js" type="text/javascript"></script>
<script language="javascript" src="javascript/jquery.autocomplete.js"></script>
<script language="javascript" type="text/javascript">
	var gridimgpath = 'javascript/jqGrid/themes/basic/images';
	var tAmt = 0;
	var lAmt = 0;
	var extraParams;
	var tranactionSource = "payex";
	jQuery(document).ready(function(){
/*		("#loading").ajaxStart(function(){
			$(this).show();
		   $("#btnAction").attr("disabled",true);
		   $("#btnActionDel").attr("disabled",true);
		   $("#btnCancel").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
			$(this).hide();
		   $("#btnAction").attr("disabled",false);
		   $("#btnActionDel").attr("disabled",false);
		   $("#btnCancel").attr("disabled",false);
		});*/
		$("#loading").ajaxStart(function(){
		   $(this).show();
		   $(".btnClass").attr("disabled",true);
		});
		$("#loading").ajaxComplete(function(request, settings){
		   $(this).hide();
		   $(".btnClass").attr("disabled",false);
		});
		var lastSel;
		var maxRows = 10;
		jQuery("#payList").jqGrid({
			url:'unres-payments-action.php?type=<?=$_GET["type"];?>&getGrid=payList&nd='+new Date().getTime(),
			datatype: "json",
			height: 270, 
			colNames:[
				'Date', 
				'Bank Account',
				'Original Statement date',
				'CODE', 
				'Amount', 
				'Description'
			],
			colModel:[
				{name:'importedOn',index:'importedOn', width:60, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'accountNo',index:'accountNo', width:50, align:"left"},
				{name:'entryDate',index:'entryDate', width:90, height:30, sorttype:'date', datefmt:'d-m-Y'},
				{name:'tlaCode',index:'tlaCode', width:40, align:"center"},
				{name:'amount',index:'amount', width:60, align:"right"},
				{name:'description',index:'description', width:300, align:"left"}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,150,200,250,300,1000000],
			loadComplete: function() {
				$("option[value=1000000]").text('All');
			},
			pager: jQuery('#pagernav'),
//			sortname: 'importedOn',
			viewrecords: true,
			multiselect:true,
			sortorder: "ASC",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Unresolved payments"
		});
			jQuery("#transList").jqGrid({	
			url:'unres-payments-action.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			height: 270, 
			colNames:[
				'Total Amount',
				'City', 
				'Reference Code', 
				'Sender Name',
				'Beneficiary Name',
				'Date' 
				<?php if(strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_EDIT_TRANSACTION_UNRESOLVED=="1"){?>
					,' '
				<?php }?>
			],
			colModel:[
				{name:'totalAmount',index:'totalAmount', width:80, align:"center"},
				{name:'City',index:'City', width:50, align:"left"},
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center"},
				{name:'name',index:'name', width:80, align:"left"},
				{name:'beneficiaryname',index:'name', width:80, align:"left"},
				{name:'transDate',index:'transDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'}
				<?php if(strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_EDIT_TRANSACTION_UNRESOLVED=="1"){?>
					,{name:'link',index:'link', width:50, align:"left"}
				<?php }?>
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50,100,150,200,250,300,1000000],
			loadComplete: function() {
				$("option[value=1000000]").text('All');
			},
			pager: jQuery('#pagernavT'),
//			sortname: 'transDate',
			viewrecords: true,
			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions",
			hidedlg: true
			}); 
		
		jQuery("#transList").setColumns();
		$("#addTransaction").hide();
		$("#cancelTransRow").hide();
		$("input:radio").click(function(){
			//alert("this is the alert for"+$(this).val());
				if($(this).val() == "unistream" || $(this).val() == "anelik")
				{
					$("#addTransaction").show();
					$("#cancelTransRow").show();
				}else{
					$("#addTransaction").hide();
					
				}
				tranactionSource = $(this).val();
					//	jQuery("#transList").GridUnload('transList');
					//	newGridupdate(tranactionSource);				
			
					  
				jQuery("#transList").setColumns();
					   
					$("#payListDeleteData").load("unres-payments-action.php?type="+<?=$_GET['type']?>, {'tranactionSource': tranactionSource},
						 function(){

						gridReload('transList');
						 
						
					});
			});
			jQuery("#btnAddTrans").click( function(){
				if($(this).val() == "Submit")
				{
					var reference = $("#txtTransRef").val();
					var amount = $("#txtTransAmount").val();
					if(reference.length < 1)
					{
						alert("Reference can not be Empty");
					}else{
						//alert(reference+"--Reference ___ amount+"+amount);
						//alert("tranactionSource1"+tranactionSource);
						//&getGrid=transList
						$("#payListDeleteData").load("unres-payments-action.php?type="+<?=$_GET['type']?>, { 'btnAction': 'AddTransaction','amount': amount, 'reference':reference, 'tranactionSource': tranactionSource},
							 function(){
							
							gridReload('transList');
						});
						    $("#txtTransRef").val("");
								$("#txtTransAmount").val("");

					}
				}else{
					//alert("Not Desired Value"+$(this).val());	
				}
			});
		jQuery("#btnActionDel").click( function(){
			if($(this).val()=="Delete"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				if(payIDs!=''){
					payIDs = payIDs.toString();
					var confirmDel = confirm("Are you sure to delete selected Payment Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allPayIdsArr = payIDs.split(",");
					$("#payListDeleteData").load("unres-payments-action.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'btnAction': 'Delete'},
						 function(){
						alert($("#payListDelete").val());
						gridReload('payList');
					});
				}
				else{
					alert("Please Select Payment(s) to delete");
					$("#allPayIdsV").val('');
					return false;
				}
				$("#allPayIdsV").val('');
				gridReload('deletePayList');
			}
		});		
		
		jQuery("#btnCancel").click( function(){
				var transIDsCanc = jQuery("#transList").getGridParam('selarrrow');
				var refundFee = jQuery("#refundFee").val();
				if(transIDsCanc!=''){
					transIDsCanc = transIDsCanc.toString();
					var confirmDel = confirm("Are you sure to cancel selected Transaction Record(s)");
					if(confirmDel==false){
						return false;
					}
					var allTransIdsCancArr = transIDsCanc.split(",");
					$("#payListDeleteData").load("unres-payments-action.php?type="+<?=$_GET['type']?>, { 'transIDsCanc[]': allTransIdsCancArr,'btnAction': 'Cancel','refundFee': refundFee},
						 function(){
						  alert($("#transListCancel").val());
						  gridReload('transList');
					});
				}
				else{
					alert("Please Select Transaction(s) to cancel");
					//$("#allTransIDsV").val('');
					return false;
				}
				//$("#allTransIDsV").val('');
				//gridReload('transList');
		});		
		
		
		jQuery("#btnAction").click( function(){
			if($(this).val()=="Resolve"){
				var payIDs = jQuery("#payList").getGridParam('selarrrow');
				var transIDs = jQuery("#transList").getGridParam('selarrrow');
				if(payIDs!='' && transIDs!=''){
					if(reconcileCheck(payIDs,transIDs)==false){
						return false;
					}
					var allPayIds = $("#allPayIdsV").val();
					var allPayIdsArr = allPayIds.split(",");
					var allTransIDs = $("#allTransIDsV").val();
					var allTransIdsArr = allTransIDs.split(",");
					var userType = $("#userType").val();
					
							var from = jQuery("#from").val();
							var to = jQuery("#to").val();
							var fromT = jQuery("#fromT").val();
							var toT = jQuery("#toT").val();
			


					$("#payListResolveData").load("unres-payments-action.php?type="+<?=$_GET['type']?>, { 'payIDs[]': allPayIdsArr,'transIDs[]': allTransIdsArr,'btnAction': 'Resolve','userTypes': userType, 'from':from, 'to':to, 'fromT':fromT, 'toT':toT},
						 function(){
						//alert($("#payListResolve").val()); Success Message is removed
						gridReload('bothList');
					});
				}
				else{
					alert("Please Select Rows from both sides");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}

				$("#userType").val("customer");
				//gridReload('bothList');
			}
		});
		/*jQuery('a').cluetip({splitTitle: '|'});*/
		jQuery(".searchPayObj").each(function(i, val){
			jQuery($(this)).bind('select change keyup', function() {
				gridReload('payList');
			});
		});
		/*mouseup*/
		jQuery(".searchTransObj").each(function(i, val){
			jQuery($(this)).bind('select change keyup', function() {
				gridReload('transList');
			});
		});
	});
	
	function newGridupdate(gridName)
	{
		
		var lastSel;
		var maxRows = 10;
		if(gridName == "payex")
		{
			jQuery("#transList").jqGrid({	
			url:'unres-payments-action.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Total Amount',
				'City', 
				'Reference Code', 
				'Sender Name',
				'Beneficiary Name',
				'Date' 
				<?php if(strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_EDIT_TRANSACTION_UNRESOLVED=="1"){?>
					,' '
				<?php }?>
			],
			colModel:[
				{name:'totalAmount',index:'totalAmount', width:80, align:"center"},
				{name:'City',index:'City', width:50, align:"left"},
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center"},
				{name:'name',index:'name', width:80, align:"left"},
				{name:'beneficiaryname',index:'name', width:80, align:"left"},
				{name:'transDate',index:'transDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'}
				<?php if(strstr(CONFIG_EDIT_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_EDIT_TRANSACTION_UNRESOLVED=="1"){?>
					,{name:'link',index:'link', width:50, align:"left"}
				<?php }?>
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50],
			pager: jQuery('#pagernavT'),
//			sortname: 'transDate',
			viewrecords: true,
			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions"
			}); 
		}else{
			
			jQuery("#transList").jqGrid({	
			url:'unres-payments-action.php?getGrid=transList&nd='+new Date().getTime(),
			datatype: "json",
			height: 250, 
			colNames:[
				'Total Amount',
				'Reference Code', 
				'Date' 
			],
			colModel:[
				{name:'totalAmount',index:'totalAmount', width:80, align:"center"},
				{name:'refNumberIM',index:'refNumberIM', width:80, align:"center"},
				{name:'transDate',index:'transDate', width:70, height:30, sorttype:'date', datefmt:'d-m-Y'}
			],
			imgpath:gridimgpath, 
			rowNum: maxRows,
			rowList: [10,20,50],
			pager: jQuery('#pagernavT'),
//			sortname: 'transDate',
			viewrecords: true,
			multiselect:true,
			sortorder: "desc",
			loadonce: false,
			loadtext: "Loading, please wait...",
			loadui: "block",
			forceFit: true,
			shrinkToFit: true,
			caption: "Transactions"
			});
			}
	}
	
	function deletePayments(pids){
		var amountPay   =0;
		var pids = pids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
	}
    function reconcileCheck(pids,tids){
		var amountPay   =0;
		var amountTrans =0;
		var pids = pids.toString();
		var tids = tids.toString();
		$("#allPayIdsV").val(pids);
		$("#allTransIDsV").val(tids);
		var allPayIds	=new Array();
		var allTransIds	=new Array();
		//var allPayCurr = new Array();
		//var allTransCurr = new Array();		
		if(pids!=null)
			allPayIds   = pids.split(",");
		if(tids!=null)
			allTransIds = tids.split(",");
			
		for(var p=0;p<allPayIds.length;p++){
			var payG	= jQuery("#payList").getRowData(allPayIds[p]);
			amountPay	= amountPay + parseFloat(payG.amount);
			//allPayCurr[p]  = payG.currency;
		}
		for(var t=0;t<allTransIds.length;t++){
			var transG	= jQuery("#transList").getRowData(allTransIds[t]);
			amountTrans = amountTrans + parseFloat(transG.totalAmount);
			//allTransCurr[t]  = transG.currencyTrans;
		}
		/*for(var c1=0;c1<allTransCurr.length;c1++){
			for(var c2=0;c2<allPayCurr.length;c2++){
				var transCurr = allTransCurr[c1];
				var payCurr = allPayCurr[c2];
				if(transCurr.toString()!=payCurr.toString()){
					alert("Currencies do not match from both sides.Please select same currency Amounts.");
					$("#allPayIdsV").val('');
					$("#allTransIDsV").val('');
					return false;
				}
			}
		}*/
		if(amountPay!=amountTrans){
			alert("Amounts should match.\nTotal Payment Amounts = "+amountPay+"\nTotal Transaction Amount = "+amountTrans+"\nPlease Select again.");
			$("#allPayIdsV").val('');
			$("#allTransIDsV").val('');
			return false;
		}
	}
	
	function gridReload(grid)
	{
		var theUrl = "unres-payments-action.php";
		
		if(grid=='payList' || grid=='bothList' || grid=='deletePayList'){
			var extraParam="?type="+<?=$_GET["type"]?>;
			var from = jQuery("#from").val();
			var to = jQuery("#to").val();
			var amount1 = jQuery("#amount1").val();
			var accountNo = jQuery("#accountNo").val();
			
			var desc = jQuery("#desc").val();
			//var currencyPay1 = jQuery("#currencyPay").val();
			//if(grid=='payList'){
				extraParam = extraParam + "&from="+from+"&to="+to+"&desc="+desc+"&amount1="+amount1+"&accountNo="+accountNo+"&Submit=SearchPay&getGrid=payList";
			/*}
			else{
				extraParam = extraParam + "&getGrid=payList";
			}*/
			//alert("First one==="+extraParam);
			jQuery("#payList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
		}

		if(grid=='transList' || grid=='bothList'){
			var extraParam='';
			var fromT = jQuery("#fromT").val();
			var toT = jQuery("#toT").val();
			var amountT = jQuery("#amountT").val();
			var searchBy = jQuery("#searchType").val();
			var searchName = jQuery("#searchName").val();
			//var currencyTrans1 = jQuery("#currencyTrans").val();
			var paymentMod = jQuery("#paymentMod").val();
			var destCountry = jQuery("#destCountry").val();
			var suggest = jQuery("#suggest").val();
			//alert('suggest'+suggest);
			//if(grid=='transList'){
				//$("#userType").val(searchBy);
				extraParam = "?fromT="+fromT+"&toT="+toT+"&amountT="+amountT+"&searchBy="+searchBy+"&searchName="+searchName+"&paymentMod="+paymentMod+"&destCountry="+destCountry+"&suggest="+suggest+"&tranactionSource="+tranactionSource+"&Submit=SearchTrans&getGrid=transList";
			/*}
			else{
				extraParam = "?getGrid=transList";
			}*/
			//alert("Second one==="+extraParam);
			
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam,
				page:1
			}).trigger("reloadGrid");
			
//			if(suggest > 0)
//			{
				
				extraParam2 = "?type="+<?=$_GET["type"]?>+"&suggest="+suggest+"&Submit=SearchPay&getGrid=payList";
				jQuery("#payList").setGridParam({
				url: theUrl+extraParam2,
				page:1
			}).trigger("reloadGrid");
				extraParam2 = "?fromT="+fromT+"&toT="+toT+"&type="+<?=$_GET["type"]?>+"&suggest="+suggest+"&Submit=SearchTrans&getGrid=transList";	
				
					//getTransaction(extraParam2);
				
//			}
		}
	}
	function pausecomp(millis) 
	{
	var date = new Date();
	var curDate = null;
	
	do { curDate = new Date(); } 
	while(curDate-date < millis);
	} 
	function getTransaction(extraParam2)
	{
		var theUrl = "unres-payments-action.php";
		//pausecomp(5000);
			//alert(theUrl+"---Before Load here---"+extraParam2);
			jQuery("#transList").setGridParam({
				url: theUrl+extraParam2,
				page:1
			}).trigger("reloadGrid");
			//alert("Load here"+suggest);	
	}
	function reconciledOptions(id){
		var ret 	= jQuery("#payListList").getRowData(id);
		var recOptIds  = jQuery("#recOptIds").val();
		
		var recArr = new Array();
		if(recOptIds!=''){
			recArr = recOptIds.split(',');
			//alert('id='+id);
			//alert('Length of Array = '+recArr.length +' index[0]='+recArr[0]+' index[1]='+recArr[1]);
			//alert('Exists = '+recArr.indexOf(id));
			if(recArr.indexOf(id)==-1){
				recArr.push(id);
				recArr.join(',');
				jQuery("#recOptIds").val(recArr);
			}
		}
		else{
			jQuery("#recOptIds").val(id);
		}
		alert('After recOptIds='+jQuery("#recOptIds").val());
		//+" Reconcile="+ret.mark+" Username="+ret.username+" UserType="+ret.userType+" Notes="+ret.notes
	}
	function uniqueArr(arrayName)
	{
		var newArray=new Array();
		label:for(var i=0; i<arrayName.length;i++ )
		{  
			for(var j=0; j<newArray.length;j++ )
			{
				if(newArray[j]==arrayName[i]) 
					continue label;
			}
			newArray[newArray.length] = arrayName[i];
		}
		return newArray;
	}
	
</script>
<style type="text/css">
<!--
.style2 {	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000}
.style1 {color: #005b90}
.style4 {color: #005b90; font-weight: bold; }
.deleteBtn{float:right;}
.searchObj{}
.linkClass {
	color:#000000;
	font-weight:bold;
	text-decoration:underline;
}
-->
</style>
<script language="javascript">
/*
function clearDates()
{
	document.frmSearch.from.value = "";
	document.frmSearch.to.value = "";
	return true;
}
*/
</script>
</head>
<body>
	<form name="frmSearch" id="frmSearch">
<div id="loading" style="display:none; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:12px; color: #FFFFFF; background-color:#FF0000; position:absolute; z-index:1; bottom:0; right:0; ">&nbsp;Loading....&nbsp;</div>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td bgcolor="#6699cc" colspan="2"><b><strong><font color="#FFFFFF">Unresolved  Barclays payment</font></strong></b></td>
  </tr>
  <tr>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top" >
     <tr>
    	<td><a href="unresolved-payments.php" class="style2">Go Back to Unresolved Payments List </a></td>
    </tr>
    
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Payments Search Filter<? echo $from2 . $to;?></strong></span></td>
        </tr>
        <tr>
				
                  <td nowrap align="center">From Date 
                  	<input name="from" type="text" id="from" value="<?=$today?>" readonly class="searchPayObj">
                  	<!--input name="from" type="text" id="from" value="<?=$from1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.from');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
		    		    <input name="to" type="text" id="to" value="<?=$today?>" readonly  class="searchPayObj">
		    		    <!--input name="to" type="text" id="to" value="<?=$to1;?>" readonly-->
		    		    &nbsp;<a href="javascript:show_calendar('frmSearch.to');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
		    		    <!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
		    		    </td>
                </tr>
        <tr>
            <td nowrap align="center"><!--input name="refNumber" type="text" id="refNumber" value=<?=$refNumber?>-->
		  				Amount 
              <input name="amount1" type="text" id="amount1"  class="searchPayObj">
              <!--input name="amount1" type="text" id="amount1" value=<?=$amount1?>-->
			  &nbsp;&nbsp;&nbsp;
              Description 
              <input name="desc" type="text" id="desc"  class="searchPayObj">
              <!--input name="desc" type="text" id="desc" value=<?=$desc?>-->
		</td>
      </tr>
	  <tr>
         <td nowrap align="center">
              Select Currency
              <select name="currencyPay" id="currencyPay" class="searchPayObj">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($cp=0;$cp<count($currencyies);$cp++){?>
					  <option value="<?=$currencyies[$cp]["currencyName"]?>"><?=$currencyies[$cp]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
              Select Account
              <select name="accountNo" id="accountNo" class="searchPayObj" size="4" multiple="multiple">
				  <option value="">- Select Account -</option>
			  	  <? if(count($barclaysRec)>0){?>
					  <? for($a=0;$a<count($barclaysRec);$a++){?>
					  <option value="<?=trim($barclaysRec[$a]["accountNo"])?>"><?=trim($barclaysRec[$a]["accountNo"])?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
				<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">		 
		 </td>
		</tr>
		 <tr><td align="center"><input type="button" name="Submit" value="Search Payments" onClick="gridReload('payList')">
		</td></tr>
    </table>
  	</td>
	</tr>
    
    <tr><td>&nbsp;</td></tr>
	<tr>
    <td valign="top">
    			<table id="payList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernav" class="scroll" style="text-align:center;"></div>
				<div id="payListResolveData" style="visibility:hidden;"></div>
				<div id="payListDeleteData" style="visibility:hidden;"></div>
				<div id="transListCancelData" style="visibility:hidden;"></div>
	</td>
    </tr>
	  <tr bgcolor="#FFFFFF">
		<td align="center">
		<input class="btnClass" name="btnAction" id="btnAction" type="button"  value="Resolve">
		<input type='hidden' name='totTrans' id='totTrans'>
		<input type='hidden' name='recOptIds' id='recOptIds' value="">
		<input type="hidden" name="allPayIdsV" id="allPayIdsV" value="">
		<input type="hidden" name="allTransIDsV" id="allTransIDsV" value="">
		<input type="hidden" name="userType" id="userType" value="customer">		
		<input class="btnClass" name="btnActionDel" id="btnActionDel" type="button"  value="Delete"  class="deleteBtn" ></td>
		
	  </tr>
</table>
	</td>
	<td valign='top'>
	<table  border="0" style="float:left; vertical-align:top;margin-top:20px;" >
    <tr>
    <td align="center">
      <table border="1" cellpadding="5" bordercolor="#666666">
      <tr>
            <td nowrap bgcolor="#6699cc"><span class="tab-u"><strong>Transactions Search Filter</strong></span></td>
        </tr>
        <tr>
		  <td nowrap align="center">From Date 
				<input name="fromT" type="text" id="fromT" value="<?=$today?>" readonly  class="searchTransObj">
				&nbsp;<a href="javascript:show_calendar('frmSearch.fromT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp; &nbsp;To Date&nbsp;
				<input name="toT" type="text" id="toT" value="<?=$today?>" readonly  class="searchTransObj">
				&nbsp;<a href="javascript:show_calendar('frmSearch.toT');" onMouseOver="window.status='Date Picker';return true;" onMouseOut="window.status='';return true;"><img src="images/show-calendar.gif" width=24 height=15 border=0></a>&nbsp;
				<!--input type="button" name="clrDates" value="Clear Dates" onClick="return clearDates();"-->
			</td>
        </tr>
        <tr>
	      <td nowrap align="center"> 
              Search Type
              <select name="searchType" id="searchType" class="searchTransObj">
				  <option value="customer">Transaction</option>
				  <option value="agent">Agent</option>
				  <option value="distributor">Distributor</option>
			  </select>
			  Suggestion
              <select name="suggest" id="suggest" class="searchTransObj">
				  <option value="0">Normal View</option>
				  <option value="1">Amount and Reference</option>
				  <option value="2">Amount and Last Name</option>
				  <option value="3">Amount and First  Name</option>
				  <option value="4">Amount and City</option>
			  </select>
			  &nbsp;&nbsp;&nbsp;Reference Number/Username 
              <input name="searchName" type="text" id="searchName" class="searchTransObj">
		</td>
      </tr>
      <tr>
	      <td nowrap align="center"> 
	      	  Destination Countries	<SELECT name="destCountry" id="destCountry" multiple size="3" style="font-family:verdana; font-size: 11px" class="searchTransObj">
            <OPTION value="">- Select Country-</OPTION>
          
	      	  	<?
	      	  		$countryTypes = " and  countryType like '%destination%' ";
		          	if(CONFIG_COUNTRY_SERVICES_ENABLED){
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." , ".TBL_SERVICE." where 1 and countryId = toCountryId  $countryTypes order by countryName");
								}
								else
								{
									$countires = selectMultiRecords("select distinct(countryName), countryId from ".TBL_COUNTRY." where 1 $countryTypes order by countryName");
								}
								for ($i=0; $i < count($countires); $i++){
							?>
			            <OPTION value="<? echo("'".$countires[$i]["countryName"]."'"); ?>">
			            <?=$countires[$i]["countryName"]; ?>
			            </OPTION>
			            <?
								}
							?>
			          </SELECT>
              Payment Mode
              <select name="paymentMod" id="paymentMod" class="searchTransObj">
           <option value="">Select One</option>   	
				  <option value="By Cash">By Cash</option>
				  <option value="By Cheque">By Cheque</option>
				  <option value="By Bank Transfer">By Bank Transfer</option>
			  </select>
		</td>
      </tr>
	  <tr>
		  <td nowrap="nowrap" align="center">
	  	  Amount 
            <input name="amountT" type="text" id="amountT" class="searchTransObj">
			&nbsp;&nbsp;&nbsp;
              Select Currency
              <select name="currencyTrans" id="currencyTrans" class="searchTransObj">
				  <option value="">Select Currency</option>
			  	  <? if(count($currencyies)>0){?>
					  <? for($ct=0;$ct<count($currencyies);$ct++){?>
					  <option value="<?=$currencyies[$ct]["currencyName"]?>"><?=$currencyies[$ct]["currencyName"]?></option>
					  <? }?>
				  <? }?>
			  </select>
			  &nbsp;&nbsp;&nbsp;
			<input type="button" name="Submit" value="Search Transactions" onClick="gridReload('transList')">
			<input type="hidden" name="type" id="type" value="<?=$_GET["type"];?>">
		  </td>
	  </tr>
	  
    </table>
  	</td>
	</tr>
    
    <tr><td>
    		<table>
    				<tr>
    					<td>
    						<input type="radio" name="tranactionSource" id="tranactionSource1" value="payex">Payex Transactions  
    					</td>
    					<td>
    						<input type="radio" name="tranactionSource" id="tranactionSource2" value="unistream">Unistream Transactions  
    					</td>
    					<td>	
    						<input type="radio" name="tranactionSource" id="tranactionSource3" value="anelik">Anelik Transactions
    					</td>	
    				</tr>
    				<tr id="addTransaction">
    					<td>
    						Ref. No <input type="text" name="txtTransRef" id="txtTransRef" value="" />
    					</td>
    					<td>
    						Amount <input type="text" name="txtTransAmount" id="txtTransAmount" value="" />  
    					</td>
    					<td>	
    						<input type="button" name="btnAddTrans" id="btnAddTrans" value="Submit" /> 
    					</td>	
    				</tr>
    		</table>	
    </td></tr>
	<tr>
    <td valign="top">
    			<table id="transList" border="0" align="center" cellpadding="5" cellspacing="1" class="scroll"></table>
				<div id="pagernavT" class="scroll" style="text-align:center;"></div>
	</td>
    </tr>
<?php if(strstr(CONFIG_CANCEL_TRANSACTION_UNRESOLVED,$agentType.",") || CONFIG_CANCEL_TRANSACTION_UNRESOLVED=="1"){?>
	  <tr bgcolor="#FFFFFF" id="cancelTransRow">
		<td align="center"><input class="btnClass" name="btnCancel" id="btnCancel" type="button"  value="Cancel">&nbsp;&nbsp;Cancel with fee refund?
		<select name="refundFee" id="refundFee">
			<option value="Yes">Yes</option>
			<!--option value="No">No</option-->
		</select>
		</td>
	  </tr>
<?php }?>
</table>

</td>
  </tr>
</table>
</form>
</body>
</html>