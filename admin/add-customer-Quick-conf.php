<?php
// Session Handling
session_start();
///////////////////////History is maintained via method named 'activities'

// Including files
include ("../include/config.php");
include ("security.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$centralEmail = CENTRAL_EMAIL;
$payinAmount = CONFIG_PAYIN_FEE;

$tran_date = date("Y-m-d");

switch(trim($_POST["Country"])){
case "United States": $ccode = "US"; break;
case "Canada": $ccode = "CA"; break;
default: 
	$countryCode = selectFrom("select countryCode from ".TBL_CITIES." where country='".trim($_POST["Country"])."'");
	$ccode = $countryCode["countryCode"];
break;
}

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}

if ($_POST["customerID"] == ""){
	session_register("Title");
	session_register("other_title");
	session_register("agentID");
	session_register("firstName");
	session_register("lastName");
	session_register("middleName");
	session_register("Address");
	session_register("Address1");
	session_register("proveAddress");
	session_register("Country");
	session_register("City");
	session_register("Zip");
	session_register("State");
	session_register("Phone");
	session_register("Mobile");
	session_register("County");
	$_SESSION["Title"] = $_POST["Title"];
	$_SESSION["other_title"] = $_POST["other_title"];
	$_SESSION["agentID"] = $_POST["agentID"];
	$_SESSION["firstName"] = $_POST["firstName"];
	$_SESSION["lastName"] = $_POST["lastName"];
	$_SESSION["middleName"] = $_POST["middleName"];
	$_SESSION["Address"] = $_POST["Address"];
	$_SESSION["Address1"] = $_POST["Address1"];
	$_SESSION["proveAddress"] = $_POST["proveAddress"];
	$_SESSION["Country"] = $_POST["Country"];
	$_SESSION["City"] = $_POST["City"];
	$_SESSION["Zip"] = $_POST["Zip"];
	$_SESSION["State"] = $_POST["State"];
	$_SESSION["Phone"] = $_POST["Phone"];
	$_SESSION["Mobile"] = $_POST["Mobile"];
	$_SESSION["payinBook"] = $_POST["payinBook"];
	$_SESSION["ch_payinBook"] = $_POST["ch_payinBook"];
	$_SESSION["County"] = $_POST["County"];
	$_SESSION["IDType"] = $_POST["IDType"];
	$_SESSION["IDNumber"] = $_POST["IDNumber"];
	$_SESSION["Otherid_name"] = $_POST["Otherid_name"];
	$_SESSION["Otherid"] = $_POST["Otherid"];
	$_SESSION["dobDay"] = $_POST["dobDay"];
	$_SESSION["dobMonth"] = $_POST["dobMonth"];
	$_SESSION["dobYear"] = $_POST["dobYear"];
	$_SESSION["placeOfBirth"] = $_POST["placeOfBirth"];
	
	$dob =  $_POST["dobYear"]."-".$_POST["dobMonth"]."-".$_POST["dobDay"];
	
	//checks if the customer already exists
	$customerName = $_POST["firstName"] . " " . $_POST["middleName"] . " " . $_POST["lastName"];
	$field = "customerName,Mobile,IDNumber";
	$values = $customerName.",".$_POST["Mobile"].",".$_POST["IDNumber"];
	$tble = "customer";
  $retValue = "accountName";
	
	
	
	$backURL = "add-customer-Quick.php?msg=Y&from=".$_GET["from"]."&r=".$_GET["r"];
} else {
	$backURL = "update-customer.php?customerID=$_POST[customerID]&msg=Y"."&r=".$_GET["r"];
}

if(CONFIG_SEARCH_EXISTING_DATA == '1'){
	
	$var = checkExistence($field,$values,$tble,$retValue);

if($var != ""){  //checks whether the customer already exists or not
			insertError(CU22." ".$var." ".CU23);
			
			redirect($backURL);
		
		}
	}

if (CONFIG_CUST_DOC_FUN == '1') {
	$documentProvided = "Y";	
} else {
	if($_POST["documentProvided"] == 'Y')
	{
		$documentProvided = "Y";
	}else{
		$documentProvided = "N";
		}
}

if (trim($_POST["agentID"]) == ""){
	$agentIds=$_GET["agentId2"];
}elseif(trim($_POST["agentID"]) != "")
{
	$agentIds=$_POST["agentID"];
	}
	
	if($agentIds == "")
	{
		insertError(CU1);
		redirect($backURL);
	}
	
if (trim($_POST["firstName"]) == ""){
	insertError(CU2);
	redirect($backURL);
}
if (trim($_POST["lastName"]) == ""){
	insertError(CU3);
	redirect($backURL);
}
/*if (trim($_POST["IDNumber"]) == ""){
	insertError(CU4);
	redirect($backURL);
}*/
/*if (trim($_POST["Address"]) == ""){
	insertError(CU5);
	redirect($backURL);
}*/
if (trim($_POST["Country"]) == ""){
	insertError(CU6);
	redirect($backURL);
}
if (trim($_POST["City"]) == ""){
	insertError(CU7);
	redirect($backURL);
}

if (trim($_POST["IDType"]) == "" && CONFIG_ID_MANDATORY == '1') {		
		
		insertError(CU19);
		redirect($backURL);
	}

if (CONFIG_COMPUL_FOR_AGENTLOGINS == '1' && ($agentType == 'SUBA' || $agentType == 'SUBAI' || $agentType == 'SUPA' || $agentType == 'SUPAI')) {
	if (trim($_POST["Address"]) == ""){
		
		insertError("Please provide the customer's address line 1.");
		redirect($backURL);
	}
	if (trim($_POST["Zip"]) == ""){
		
		insertError("Please provide the customer's Zip/Postal Code.");
		redirect($backURL);
	}
}
if (CONFIG_COMPULSORY_MOBILE != '1' && (trim($_POST["Phone"]) == ""  && trim($_POST["Mobile"]) == "" )){
	
	insertError(CU8);
	redirect($backURL);
}

if (CONFIG_COMPULSORY_MOBILE== '1' && trim($_POST["Mobile"]) == "" ){
	
	insertError(CU20);
	redirect($backURL);
}

if (CONFIG_COMPULSORY_MOBILE== '1' && trim($_POST["Mobile"]) != "" ){
$mobileNumber=$_POST["Mobile"];
$number = explode(CONFIG_MOBILE_FORMAT,$mobileNumber);
if(count($number) == '3'  && (strlen($number[0])!= '4' || strlen($number[1]) != '3'|| strlen($number[2]) != '4'))
{
	insertError(CU21);
	redirect($backURL);
	
	
	}elseif(strlen($mobileNumber) != '11' || !is_numeric($mobileNumber)){
	insertError(CU21);
	redirect($backURL);
	
	}	
}

if (trim($_POST["ch_payinBook"]) != ""){
	
	if (trim($_POST["payinBook"]) == "" && CONFIG_AUTO_PAYIN_NUMBER != "1"){	
	insertError(CU14);
	redirect($backURL);
}}

if (trim($_POST["payinBook"]) != ""){
	
	if (trim($_POST["ch_payinBook"]) == ""){	
	insertError(CU13);
	redirect($backURL);
	}
	if(CONFIG_PAYIN_CUSTOMER == '1' && CONFIG_AUTO_PAYIN_NUMBER != '1')
	{
		if (!is_numeric($_POST["payinBook"])){
		insertError(CU12);
		redirect($backURL);
		}
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber < CONFIG_PAYIN_NUMBER ){	
		insertError(CU15." ".CONFIG_PAYIN_NUMBER);
		redirect($backURL);
		}
		$payinBookNumber = strlen(trim($_POST["payinBook"]));
		if ($payinBookNumber > CONFIG_PAYIN_NUMBER ){	
		insertError(CU16." ".CONFIG_PAYIN_NUMBER);
		redirect($backURL);
		}
	}
}

if ($_POST["customerID"] == ""){
if ($_POST["payinBook"] != ""){
	//echo $_POST["payinBook"];	
	if (isExist("select accountName from ".TBL_CUSTOMER." where accountName = '".$_POST["payinBook"]."'")){
		insertError(AG35);
		redirect($backURL);
	
	}
}
}
else
{
	if ($_POST["payinBook"] != ""){
	//echo $_POST["payinBook"];	
	if (isExist("select accountName from ".TBL_CUSTOMER." where accountName = '".$_POST["payinBook"]."' and customerID != '".$_POST["customerID"]."'")){
		insertError(AG35);
		$backURL = "add-customer-Quick.php?msg=Y&from=" . $_GET["from"]."&customerID=".$_POST["customerID"]."&r=".$_GET["r"];
		redirect($backURL);
	
}
	
	}
}



/*
if(!email_check(trim($_POST["custEmail"]))){
	insertError(CU9);
	redirect($backURL);
}*/
if ($_POST["customerID"] == ""){
	$password = createCode();
	$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_CUSTOMER." where Country='".$_POST["Country"]."'");
	//$customerCode = $systemPre."CUST".$ccode.($customerNumber[0]+1);////Chnged By Kashi 11-Nov
	$customerNumber_value = $customerNumber[0]+1;
	
	//payinBook logic
	if($_POST["payinBook"]=="" || CONFIG_PAYIN_CUSTOMER != '1'){
	$customerCode = $systemPre."CUST".$ccode.($customerNumber[0]+1);////Chnged By Kashi 11-Nov
	$balance = 0 ;
	}
	else
	{
	$customerCode = $_POST["payinBook"];
	$balance = -$payinAmount ;	
	}	
	if(isset($_POST["County"])){
		$_POST["State"]=$_POST["County"];
		}

	// Added by Usman Ghani aginst ticket #3472: Now Transfer - Phone/Mobile
	$phone = checkValues($_POST["Phone"]);
	$mobile = checkValues($_POST["Mobile"]);
	if ( defined("CONFIG_PHONES_AS_DROPDOWN")
		&& CONFIG_PHONES_AS_DROPDOWN == 1 )
	{
		if ( $_POST["phoneType"] == "phone" )
		{
			$phone = checkValues($_POST["Phone"]);
			$mobile = "";
		}
		else
		{
			$phone = "";
			$mobile = checkValues($_POST["Phone"]);
		}
	}
	// End of code against ticket #3472: Now Transfer - Phone/Mobile

	$Querry_Sqls = "INSERT INTO ".TBL_CUSTOMER." (accountName, password, agentID, Title, other_title, firstName, middleName, lastName, IDType, IDNumber, otherId, otherId_name,Address,  Address1, proveAddress, Country, City, Zip, State, Phone, Mobile, dob,placeOfBirth,payinBook, balance, customerNumber, created, documentProvided,customerName) VALUES 
	('$customerCode', '$password', '$agentIds', '".$_POST["Title"]."', '".$_POST["other_title"]."', '".checkValues($_POST["firstName"])."', '".checkValues($_POST["middleName"])."', '".checkValues($_POST["lastName"])."','".checkValues($_POST["IDType"])."','".checkValues($_POST["IDNumber"])."','".checkValues($_POST["Otherid"])."','".checkValues($_POST["Otherid_name"])."', '".checkValues($_POST["Address"])."', '".checkValues($_POST["Address1"])."', '".($_POST["proveAddress"] != "" ? $_POST["proveAddress"] : "N")."', '".$_POST["Country"]."', '".checkValues($_POST["City"])."', '".checkValues($_POST["Zip"])."', '".checkValues($_POST["State"])."', '".$phone."', '".$mobile."', '".$dob."', '".checkValues($_POST["placeOfBirth"])."','".$_POST["payinBook"]."','".$balance."','$customerNumber_value', '".$tran_date."', '".$documentProvided."', '".$customerName."')";
//	echo $Querry_Sqls; exit;
	insertInto($Querry_Sqls);
	$customerID = @mysql_insert_id();
	
	if(defined("CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX"))
		$strCustomerPrefix = CONFIG_CUSTOMER_ACCOUNTNAME_PREFIX;
	else
		$strCustomerPrefix = "C-";
	
	if($_POST["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != '1')
	{
		$customerCode = $strCustomerPrefix.$customerID;
		$numberQuery = " update customer set accountName = '".$customerCode."' where customerID = '".$customerID."'";
			
		update($numberQuery);		
	}
	if ($_POST["payinBook"] == "" && CONFIG_AUTO_PAYIN_NUMBER == '1') {
		$customerCode = $strCustomerPrefix . $customerID;
		$_POST["payinBook"] = $customerCode;
		$payinQuery = "UPDATE " . TBL_CUSTOMER . " SET `payinBook` = '$customerCode' WHERE `customerID` = '$customerID'";
		
		update($payinQuery);
	}
	$descript = "Customer data is added";
	activities($_SESSION["loginHistoryID"],"INSERTION",$customerID,TBL_CUSTOMER,$descript);
	
	$fromName = SUPPORT_NAME;
	$fromEmail = SUPPORT_EMAIL;
	$subject = "Sender Account creation at $company Transfer";
	$message = "Congratulations! ".$_POST["firstName"].",<br><br>
		Your agent account has been created at $company agent module. Your login information is as follows:<br><br>
		Login: ".$customerCode."<br><br>
		Password: ".$password."<br><br>
		Name: ".$_POST["agentContactPerson"]."<br><br>
		

		$company Support";
//	sendMail($_POST["custEmail"], $subject, $message, $fromName, $fromEmail);
	//sendMail($centralEmail, $subject, $message, $fromName, $fromEmail);
	
	if($_POST["payinBook"] != ""){
$strQuery = "insert into  agents_customer_account (customerID ,Date,tranRefNo ,payment_mode,Type ,amount, modifiedBy) 
											 values('".$customerID."','".$tran_date."','','Customer Registered','WITHDRAW','".$payinAmount."','".$modifyby."'
											 )";
								$nResult = mysql_query($strQuery)or die("Invalid query: " . mysql_error());
	}
	$_SESSION["agentID"] = "";
	$_SESSION["firstName"] = "";
	$_SESSION["lastName"] = "";
	$_SESSION["middleName"] = "";
	$_SESSION["Address"] = "";
	$_SESSION["Address1"] = "";
	$_SESSION["proveAddress"] = "";
	$_SESSION["Country"] = "";
	$_SESSION["City"] = "";
	$_SESSION["Zip"] = "";
	$_SESSION["State"] = "";
	$_SESSION["Phone"] = "";
	$_SESSION["Mobile"] = "";
	$_SESSION["County"] = "";
	$_SESSION["placeOfBirth"] = "";
	$_SESSION["dobYear"] = "";
	$_SESSION["dobMonth"] = "";
	$_SESSION["dobDay"] = "";
	insertError(CU10."The login name for the sender is <b>$customerCode</b> ");

} else {
	if (trim($_POST["oldCountry"]) != trim($_POST["Country"])){
		$customerNumber = selectFrom("select MAX(customerNumber) from ".TBL_ADMIN_USERS." where agentCountry='".$_POST["Country"]."'");
		$customerCode = $systemPre."".$ccode.($customerNumber[0]+1);///Changed Kashi 11_Nov
		$agentAcountUpdate = "username='$customerCode', customerNumber='".($customerNumber[0]+1)."', ";
	}
	$Querry_Sqls = "update ".TBL_ADMIN_USERS." set $agentAcountUpdate name='".checkValues($_POST["agentContactPerson"])."',
	 email='".checkValues($_POST["email"])."', 
	 agentCompany='".checkValues($_POST["agentCompany"])."', 
	 agentContactPerson='".checkValues($_POST["agentContactPerson"])."', 
	 agentAddress='".checkValues($_POST["agentAddress"])."', 
	 agentAddress2='".checkValues($_POST["agentAddress2"])."', 
	 agentCity='".$_POST["City"]."', 
	 agentZip='".$_POST["agentZip"]."', 
	 agentCountry='".$_POST["Country"]."', 
	 agentCountryCode='$ccode', 
	 agentPhone='".checkValues($_POST["agentPhone"])."', 
	 agentFax='".checkValues($_POST["agentFax"])."', 
	 agentURL='".checkValues($_POST["agentURL"])."', 
	 agentMSBNumber='".checkValues($_POST["agentMSBNumber"])."', 
	 agentMCBExpiry='".$_POST["msbYear"]."-".$_POST["msbMonth"]."-".$_POST["msbDay"]."', 
	 agentCompRegNumber='".checkValues($_POST["agentCompRegNumber"])."', 
	 agentCompDirector='".checkValues($_POST["agentCompDirector"])."', 
	 agentDirectorAdd='".checkValues($_POST["agentDirectorAdd"])."', 
	 agentProofID='".checkValues($_POST["agentProofID"])."', 
	 agentIDExpiry='".$_POST["idYear"]."-".$_POST["idMonth"]."-".$_POST["idDay"]."', 
	 agentDocumentProvided='".$_POST["agentDocumentProvided"]."', 
	 agentBank='".checkValues($_POST["agentBank"])."', 
	 agentAccountName='".checkValues($_POST["agentAccountName"])."', 
	 agentAccounNumber='".checkValues($_POST["agentAccounNumber"])."', 
	 agentBranchCode='".checkValues($_POST["agentBranchCode"])."', 
	 agentAccountType='".$_POST["agentAccountType"]."', 
	 agentCurrency='".$_POST["agentCurrency"]."', 
	 agentAccountLimit='".checkValues($_POST["agentAccountLimit"])."', 
	 agentCommission='".checkValues($_POST["agentCommission"])."', 
	 agentStatus='".$_POST["agentStatus"]."' where customerID='".$_POST[customerID]."'";
	update($Querry_Sqls);
	$descript = "Customer data is updated";
	activities($_SESSION["loginHistoryID"],"UPDATION",$_POST["customerID"],TBL_CUSTOMER,$descript);
	
	insertError(CU11);
}

if($_GET["from"] == "popUp")
{
	 $_SESSION["customerID"] = $customerID;
	?>
	<script language="javascript">
		opener.document.location = "<?=$returnPage?>?msg=Y&success=Y";
		window.close();
	</script>
	<?
}
else
{
	$backURL .= "&success=Y";
	redirect($backURL);
}
?>