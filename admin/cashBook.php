<?
	session_start();

	include ("../include/config.php");
	include ("security.php");
	
	$agentType = getAgentType();

	/**
	 * Variables from Query String
	 */
	$cmd 				= $_REQUEST["cmd"];
	$print				= $_REQUEST["print"];
	$openingBalance 	= $_REQUEST["openingBalance"];
	$closingBalance 	= $_REQUEST["closingBalance"];
	$banks				= $_REQUEST["banks"];
	$overShort			= $_REQUEST["overShort"];
	$id 				= $_REQUEST["id"];
	$userId		    	= $_REQUEST["userId"];
	$currentDate		= $_REQUEST["currentDate"];
	$totalTransactions	= $_REQUEST["totalTransactions"];
	$totalExpense		= $_REQUEST["totalExpense"];
	
	/**
	 * Some pre data preparation.
	 */
	$openingBalance 	= (!empty($openingBalance))?trim($openingBalance):"0";
	$closingBalance 	= (!empty($closingBalance))?trim($closingBalance):"0";
	$banks 				= (!empty($banks))?trim($banks):"0";
	$overShort		 	= (!empty($overShort))?trim($overShort):"0";
	$userId				= (!empty($userId))?$userId:$_SESSION["loggedUserData"]["userID"];
	$currentDate		= (!empty($currentDate))?$currentDate:date("Y-m-d", time());
	$allowUpdate		= ($currentDate == date("Y-m-d", time()))?"Y":"N";
	$totalTransactions  = (!empty($totalTransactions))?$totalTransactions:"0";
	$totalExpense  		= (!empty($totalExpense))?$totalExpense:"0";

	/**
	 * Check for data validity
	 */
	$openingBalance 	= (is_numeric($openingBalance))?$openingBalance:"0";
	$closingBalance 	= (is_numeric($closingBalance))?$closingBalance:"0";
	$banks 				= (is_numeric($banks))?$banks:"0";
	$overShort		 	= (is_numeric($overShort))?$overShort:"0";
	
	if($cmd == "UPDATE")
	{
		if(!empty($id))
		{
			$closingBalance = $openingBalance + $totalTransactions - $totalExpense - $banks;
			$sql = "UPDATE
						cash_book
					SET
						openingBalance = $openingBalance,
						closingBalance = $closingBalance,
						banks = $banks,
						overShort = $overShort,
						updated = now()
					WHERE
						recId = $id
						AND userId = $userId
					";
			update($sql);
		} else {
			$errMsg = "";
		}
	}
	
	if($cmd == "ADD")
	{
		$sql = "INSERT INTO
					cash_book
					(
						userId,
						openingBalance,
						closingBalance,
						banks,
						overShort,
						created,
						updated
					)
				VALUES
					(
						$userId,
						$openingBalance,
						$closingBalance,
						$banks,
						$overShort,
						now(),
						now()
					)
				";
		insertInto($sql);
		$cmd = "UPDATE";
	}
	
	$sql = "SELECT 
				COUNT(recId) AS cnt
			FROM 
				cash_book
			WHERE
				userId = $userId
				AND created like '$currentDate%'
			";
	$result = selectFrom($sql);
	$numRows = $result["cnt"];
	
	if($numRows > 0)
	{
		$latestUpdatesOn = $currentDate;
		$cmd = "UPDATE";
	} else {
		$sql = "SELECT
					DISTINCT DATE_FORMAT(created, '%Y-%m-%d') as created
				FROM
					cash_book
				ORDER BY
					created DESC
				LIMIT
					0,1
				";
		$result = selectFrom($sql);
		$latestUpdatesOn = $result["created"];
	}

	$sql = "SELECT 
				recId,
				openingBalance,
				closingBalance,
				banks,
				overShort
			FROM
				cash_book
			WHERE
				userId = $userId
				AND created like '$latestUpdatesOn%'
			";
	$result = selectFrom($sql);
	$id						= $result["recId"];

	if($numRows > 0)
	{
		$openingBalance 	= $result["openingBalance"];
		$closingBalance		= $result["closingBalance"];
	} else {
		$openingBalance		= $result["closingBalance"];
		$closingBalance		= 0;
	}
	
	$banks				= $result["banks"];
	$overShort			= $result["overShort"];
	$xBalance			= $openingBalance;
	
	if(empty($cmd))
	{
		$cmd = "ADD";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cash Book</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function updateBalance(b)
{
	var banks = parseFloat(document.getElementById("banks").value);
	var closingBalance = parseFloat(document.getElementById("closingBalance").value);
	var final = 0;
	
	closingBalance = closingBalance + b;
	final = closingBalance - banks;
	
	document.getElementById("closingBalance").value = final;
	document.getElementById("balanceAfterBank").innerHTML = final;
	document.getElementById("closingFinal").innerHTML = final;
}
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</script>
</head>

<body>

<? 
if($print != "Y" && $agentType =="admin")
{
?>
	<form action="" method="post" name="frmSearch">
	<table width="80%" border="0" align="center" cellpadding="3" cellspacing="0" class="boundingTable">
		<tr>
			<td class="columnTitle" colspan="2">Search</td>
		</tr>
		<tr>
			<td align="right">Agents</td>
			<?
//work by Mudassar Ticket #11425
		if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
			$agentSql = "SELECT 
							userID,
							username, 
							name, 
							agentStatus
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
							AND agentStatus = 'Active' 
						ORDER BY username
						";
			}
		else if (CONFIG_AGENT_WITH_ALL_STATUS==1){
		$agentSql = "SELECT 
							userID,
							username, 
							name
						FROM 
							".TBL_ADMIN_USERS."
						WHERE 
							adminType = 'Agent'
							AND parentID > 0
							AND isCorrespondent != 'ONLY' 
						ORDER BY username
						";
		}
	//work end by Mudassar Ticket #11425
			$agentResult = SelectMultiRecords($agentSql);
				
			?>
			<td align="center">
				<select name="userId" id="userId">
					<option value=""> -Select Agent-</option>
					
					<?
					for($i=0; $i<sizeof($agentResult); $i++)
					{
					?>
						<option value="<?=$agentResult[$i]["userID"]?>"><?=$agentResult[$i]["username"]."[".$agentResult[$i]["name"]."]" ?></option>
					<?
					}
					?>
				</select>
				<script language="JavaScript">
					SelectOption(document.forms[0].userId, "<?=$userId; ?>");
			   </script>
			</td>
		</tr>
		<tr>
			<td width="50%" align="right" class="reportToolbar" colspan="2"><input type="submit" name="Submit" value="Submit" /></td>
		</tr>
	</table>
	</form>
<?
}
?>
<br />
<table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" class="boundingTable">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td align="center" class="reportHeader"><?=SYSTEM?> (Cash Book)<br />
					<span class="reportSubHeader"><?=date("l, d F, Y", strtotime($currentDate))?><br />
					<br />
					</span></td></tr>
			</table>
			<br />
			<form action="" method="post">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
				<tr>
					<td width="5%" class="columnTitle">Sr.#</td>
					<td width="15%" class="columnTitle">Transaction Ref.# </td>
					<td width="50%" class="columnTitle">Particulars</td>
					<td width="10%" align="right" class="columnTitle">Dr. &pound; </td>
					<td width="10%" align="right" class="columnTitle">Cr. &pound;</td>
					<td width="10%" align="right" class="columnTitle">Balance &pound;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Opening Balance b/f </td>
					<td align="right">-  </td>
					<td align="right">-</td>
					<td align="right">
						<?
						if($allowUpdate == "Y")
						{
						?>
							<input name="openingBalance" type="text" class="inputBoxesNumerics" id="openingBalance" value="<?=$openingBalance?>" size="15" />
						<?
						}else{
							echo $openingBalance;
						}
						?>	
					</td>
				</tr>
				<?
					$sql = "SELECT 
								t.refNumberIM, 
								t.totalAmount, 
								concat(c.firstName, ' ', c.middleName, ' ', c.lastName) as fullName
							FROM 
								transactions t, 
								customer c 
							WHERE 
								t.customerID = c.customerID 
								AND t.custAgentID = $userId
								AND t.transDate like '$currentDate%'
							";
							//AND t.transStatus IN('Authorize', 'Credited', 'Pending', 'Amended', 'Picked up', 'Processing', 'AwaitingCancellation')
								
					$result = SelectMultiRecords($sql);
					
					for($i=0; $i<sizeof($result); $i++)
					{
						$xTransactionId			= $result[$i]["refNumberIM"];
						$xSenderFullName		= $result[$i]["fullName"];
						$xTransactionAmount		= $result[$i]["totalAmount"];
						$xBalance				+= $xTransactionAmount;
						$xTotalTransactions		+= $xTransactionAmount;
				?>
						<tr>
							<td><?=($i + 1)?></td>
							<td><?=$xTransactionId?></td>
							<td><?=$xSenderFullName?></td>
							<td align="right">0.00</td>
							<td align="right"><?=number_format($xTransactionAmount, 2, ".", ",")?></td>
							<td align="right"><?=number_format($xBalance, 2, ".", ",")?></td>
						</tr>
				<?
					}
				?>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">CANCELLED</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					$sql = "SELECT 
								t.refNumberIM, 
								t.totalAmount, 
								concat(c.firstName, ' ', c.middleName, ' ', c.lastName) as fullName
							FROM 
								transactions t, 
								customer c 
							WHERE 
								t.customerID = c.customerID 
								AND t.transStatus IN('Cancelled', 'Failed', 'Rejected', 'Suspicious')
								AND t.custAgentID = $userId
								AND t.transDate like '$currentDate%'
							";
					$result = SelectMultiRecords($sql);
					
					for($i=0; $i<sizeof($result); $i++)
					{
						$xTransactionId			= $result[$i]["refNumberIM"];
						$xSenderFullName		= $result[$i]["fullName"];
						$xTransactionAmount		= $result[$i]["totalAmount"];
						$xBalance				-= $xTransactionAmount;
						$xTotalTransactions		-= $xTransactionAmount;
				?>
						<tr>
							<td><?=($i + 1)?></td>
							<td><?=$xTransactionId?></td>
							<td><?=$xSenderFullName?></td>
							<td align="right"><?=number_format($xTransactionAmount, 2, ".", ",")?></td>
							<td align="right">0.00</td>
							<td align="right"><?=number_format($xBalance, 2, ".", ",")?></td>
						</tr>
				<?
					}
				?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="heading">EXPENSES</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<?
					$sql = "SELECT 
								narration, 
								amount 
							FROM 
								expenses
							WHERE 
								userId = $userId
								AND created like '$currentDate%'
							";
					$result = SelectMultiRecords($sql);
					
					for($i=0; $i<sizeof($result); $i++)
					{
						$xNarration			= $result[$i]["narration"];
						$xAmount			= $result[$i]["amount"];
						$xBalance			-= $xAmount;
						$xTotalExpense		+= $xAmount;
				?>
						<tr>
							<td><?=($i + 1)?></td>
							<td>&nbsp;</td>
							<td><?=$xNarration?></td>
							<td align="right"><?=number_format($xAmount, 2, ".", ",")?></td>
							<td align="right">0.00</td>
							<td align="right"><?=number_format($xBalance, 2, ".", ",")?></td>
						</tr>
				<?
					}
				?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Total Expense </td>
					<td align="right" class="heading"><?=number_format($xTotalExpense, 2, ".", ",")?></td>
					<td align="right">0.00</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Total Transactions </td>
					<td align="right">0.00</td>
					<td align="right" class="heading"><?=number_format($xTotalTransactions, 2, ".", ",")?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Total Physical Cash excluding Expense </td>
					<td align="right">0.00</td>
					<td align="right" class="heading"><?=number_format(($xTotalTransactions - $xTotalExpense), 2, ".", ",")?></td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Bank</td>
					<td align="right">
						<?
						if($allowUpdate == "Y")
						{
						?>
							<input name="banks" type="text" class="inputBoxesNumerics" id="banks" value="<?=$banks?>" size="15" />
						<?
						}else{
							echo $banks;
						}
						?>						
					</td>
					<td align="right">0.00</td>
					<td align="right" id="balanceAfterBank">
						<?
							$xBalance -= $banks;
							
							echo number_format($xBalance, 2, ".", ",");
						?>					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Closing Balance of the Day </td>
					<td align="right">-</td>
					<td align="right">-</td>
					<td align="right" class="netBalance" id="closingFinal"><?=number_format($xBalance, 2, ".", ",")?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="heading">Over/Short</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">
						<?
						if($allowUpdate == "Y")
						{
						?>
							<input name="overShort" type="text" class="inputBoxesNumerics" id="overShort" value="<?=$overShort?>" size="15" />
						<?
						}else{
							echo $overShort;
						}
						?>
					</td>
				</tr>
			</table>
			<br />
			<?
				if($print != "Y")
				{
			?>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="50%" class="reportToolbar"><input name="btnPrint" type="button" id="btnPrint" value=" Print " onclick="javascript: location.href='cashBook.php?id=<?=$id?>&print=Y&userId=<?=$userId?>&currentDate=<?=$currentDate?>';" /></td>
							<td width="50%" align="right" class="reportToolbar"><input name="id" type="hidden" id="id" value="<?=$id?>" />
								<input name="cmd" type="hidden" id="cmd" value="<?=$cmd?>" />
								<input name="userId" type="hidden" id="userId" value="<?=$userId?>" />
								<input name="closingBalance" type="hidden" id="closingBalance" value="<?=$xBalance?>" />
								<input name="totalTransactions" type="hidden" id="totalTransactions" value="<?=$xTotalTransactions?>" />
								<input name="totalExpense" type="hidden" id="totalExpense" value="<?=$xTotalExpense?>" />
								<?
								if($allowUpdate == "Y")
								{
								?>
									<input type="submit" name="Submit" value="Submit" />
									<input type="reset" name="Submit2" value="Reset" /></td>
								<?
								}
								?>
						</tr>
					</table>
			<?
				}
			?>
			</form>
		</td>
	</tr>
</table>
<?
	if($print == "Y")
	{
?>
		<script type="text/javascript">
			print();
		</script>
<?
	}
?>
</body>
</html>
