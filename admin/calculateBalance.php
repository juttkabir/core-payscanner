<?
function calculateBalance($cID)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, bankID  from bank_account where bankID = $cID";
			
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}
/**
 * Modified version of calculateBalance
 * which calulate the balance on basis of currency selected.
 */
function calculateBalanceWithCurrency($cID,$curr)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, bankID  from bank_account where bankID = $cID";
		
		if(!empty($curr))
			$accountQuery .= " and currency = '$curr'";
			
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}



function calculateBalanceDate($cID,$fromDate,$toDate)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, bankID from bank_account where bankID = $cID";
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
		//if($fromDate < $toDate)
		//{
			$accountQuery .=  " and $queryDate";				

		//}	
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}


function distributorOpeningBalance($cID,$currency,$fromDate= '')
{
	$Balance = 0;
	$accountQuery = "Select type, amount, bankID from bank_account where bankID = $cID";
	if(!empty($fromDate))
		$queryDate = " dated < '$fromDate'";

	$accountQuery .=  " and $queryDate and  currency='".$currency."'";				

	$contentsAcc = selectMultiRecords($accountQuery);
	
	$allCount = count($contentsAcc);
	for($i=0;$i < count($contentsAcc);$i++)		
	{
		if($contentsAcc[$i]["type"] == "DEPOSIT")
		{
			$Balance = $Balance+$contentsAcc[$i]["amount"];
		}
			
		if($contentsAcc[$i]["type"] == "WITHDRAW")
		{
			$Balance = $Balance-$contentsAcc[$i]["amount"];
		}
	}		
			
	return $Balance;
}


/////////////
function agentBalance($cID)
{
	
		$Balance = 0;
		
		$agentInfo = selectFrom("select isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$cID."'");
		if(CONFIG_AnD_ENABLE == '1' && $agentInfo["isCorrespondent"] == 'Y')
		{
			$accountQuery = "Select sum(amount) as amount, type from agent_Dist_account where agentID = $cID";
			$accountQuery .= " and status != 'Provisional' and actAs != 'Distributor' group by type";	
		}else{
			
			$accountQuery = "Select sum(amount) as amount, type from agent_account where agentID = $cID";
			$accountQuery .= " and status != 'Provisional'  group by type";
		
		}
				
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}	
			
				
		return $Balance;
}

/**
 * Calculating the agent balance based on the currency provided
 */
function agentBalanceWithCurrency($cID, $curr)
{
	
		$Balance = 0;
		
		$agentInfo = selectFrom("select isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$cID."'");
		
		$currencyQry = "";
		if(!empty($curr))
			$currencyQry = " and currency='$curr'";
		
		if(CONFIG_AnD_ENABLE == '1' && $agentInfo["isCorrespondent"] == 'Y')
		{
			$accountQuery = "Select sum(amount) as amount, type from agent_Dist_account where agentID = $cID $currencyQry";
			$accountQuery .= " and status != 'Provisional' and actAs != 'Distributor' $currencyQry group by type";	
		}else{
			
			$accountQuery = "Select sum(amount) as amount, type from agent_account where agentID = $cID $currencyQry";
			$accountQuery .= " and status != 'Provisional' $currencyQry  group by type";
		
		}
		
		$contentsAcc = selectMultiRecords($accountQuery);
//		debug($accountQuery);
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}	
			
				
		return $Balance;
}






function agentBalanceDate($cID,$fromDate,$toDate)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, agentID from agent_account where agentID = $cID ";
		if(CONFIG_BALANCE_FROM_TRANSACTION == '1')
		{
				$accountQuery .= " and type = 'DEPOSIT'";
		}
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
		//if($fromDate < $toDate)
		//{
			$accountQuery .=  " and $queryDate";	
			 $accountQuery .= " and status != 'Provisional' ";			
			
		//}	
		
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
		if(CONFIG_BALANCE_FROM_TRANSACTION == '1')
		{
			
				$transQuery = "SELECT transactions.custAgentID, transactions.transDate, transactions.totalAmount, transactions.transID, transactions.AgentComm  FROM `transactions`  INNER JOIN customer ON transactions.customerID = customer.customerID where customer.payinBook = '' OR customer.`accountName` != customer.`payinBook` and custAgentID = '".$cID."'" ;
				if((@$result = mysql_query ($transQuery))==FALSE)
   	{
	
				if (DEBUG=="True")
				{
					echo mysql_message($transQuery);		
				}	
				}else
			 	{	
				   
					while ($row = mysql_fetch_array($result)) 
					{
						$amountToUse = 0;
						$amountToUse = $row["totalAmount"] - $row["AgentComm"];
						$Balance = $Balance - $amountToUse;
							
					}
				
				}
		}	
				
				
				
		return $Balance;
}

////////

// calcualte the bank cumulative balance.

function bankBalanceDate($cID,$fromDate,$toDate)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, bankID from bank_account where bankID = $cID";
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
		//if($fromDate < $toDate)
		//{
			//$accountQuery .=  " and $queryDate";	
			//$accountQuery .= " and status != 'Provisional' ";			
			
		//}	
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}

/////// end




function calculateBalanceCustomer($cID)
{
		$Balance = 0;
		$accountQuery = "Select sum(amount) as amount, Type from agents_customer_account where customerID like '$cID' group by Type ";
			
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);

		for($i=0;$i < count($contentsAcc);$i++)		
		{
		
			if($contentsAcc[$i]["Type"] == "DEPOSIT")
			{
			  	 $Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["Type"] == "WITHDRAW")
			{
			  	 $Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}
function calculateBalanceDateCustomer($cID,$fromDate,$toDate)
{
		$Balance = 0;
		$accountQuery = "Select sum(amount) as amount, Type from agents_customer_account where customerID = '$cID'";
		$queryDate = "(Date >= '$fromDate 00:00:00' and Date <= '$toDate 23:59:59') ";
		//if($fromDate < $toDate)
		//{
			$accountQuery .=  " and $queryDate";				
			
		//}	
		 $accountQuery .=  " group by Type";				
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["Type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["Type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}		
				
		return $Balance;
}


function TransactionBalanceDate($cID,$fromDate,$toDate)
{
		$Balance = 0;
		$accountQuery = "Select * from transactions where addedBy like '$cID'";
		$queryDate = "(transDate >= '$fromDate 00:00:00' and transDate <= '$toDate 23:59:59')";
		if($fromDate < $toDate)
		{
			$accountQuery .=  " and $queryDate";	
			//$accountQuery .= " and status != 'Provisional' ";			
			
		}	
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			
			  	$Balance = $Balance+$contentsAcc[$i]["totalAmount"];
		
		}		
				
		return $Balance;
}

/////// end


function agentBalanceDateCurrency($cID,$fromDate,$toDate, $currencyPrefferd)
{
		$Balance = 0;
		$accountQuery = "Select type, amount, agentID, TransID, currency from agent_account where agentID = $cID ";
		if(CONFIG_BALANCE_FROM_TRANSACTION == '1')
		{
				$accountQuery .= " and type = 'DEPOSIT'";
		}
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
		//if($fromDate < $toDate)
		//{
			//$accountQuery .=  " and $queryDate";	
			$accountQuery .= " and status != 'Provisional' ";			
			
		//}	
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			$currAmount = 0;
			$currAmount = $contentsAcc[$i]["amount"];
			
			if($contentsAcc[$i]["currency"] != $currencyPrefferd)
			{
				//echo("--".$i." select * from " . TBL_EXCHANGE_RATES . " where currency = '".$currencyPrefferd."' and currencyOrigin = '".$contentsAcc[$i]["currency"]."' and dated = (select MAX(dated) from " . TBL_EXCHANGE_RATES . " where currency = '".$currencyPrefferd."' and currencyOrigin = '".$contentsAcc[$i]["currency"]."')");
				$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where currency = '".$currencyPrefferd."' and currencyOrigin = '".$contentsAcc[$i]["currency"]."' and dated = (select MAX(dated) from " . TBL_EXCHANGE_RATES . " where currency = '".$currencyPrefferd."' and currencyOrigin = '".$contentsAcc[$i]["currency"]."')");
				
				if($exchangeRate["erID"] != "")
				{
						$currAmount = $currAmount * $exchangeRate["primaryExchange"];
				}else{
					
					//echo("-2-".$i."select * from " . TBL_EXCHANGE_RATES . " where currency = '".$contentsAcc[$i]["currency"]."' and currencyOrigin = '".$currencyPrefferd."' and dated = (select MAX(dated) from " . TBL_EXCHANGE_RATES . " where currency = '".$contentsAcc[$i]["currency"]."' and currencyOrigin = '".$currencyPrefferd."')");
					
					$exchangeRate = selectFrom("select * from " . TBL_EXCHANGE_RATES . " where currency = '".$contentsAcc[$i]["currency"]."' and currencyOrigin = '".$currencyPrefferd."' and dated = (select MAX(dated) from " . TBL_EXCHANGE_RATES . " where currency = '".$contentsAcc[$i]["currency"]."' and currencyOrigin = '".$currencyPrefferd."')");
					
					if($exchangeRate["erID"] != "")
					{
							$currAmount = $currAmount/$exchangeRate["primaryExchange"];
					}
				}
				
			}
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$currAmount;
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$currAmount;
			}
		}		
		if(CONFIG_BALANCE_FROM_TRANSACTION == '1')
		{
			
				$transQuery = "SELECT transactions.custAgentID, transactions.transDate, transactions.totalAmount, transactions.transID, transactions.AgentComm  FROM `transactions`  INNER JOIN customer ON transactions.customerID = customer.customerID where customer.payinBook = '' OR customer.`accountName` != customer.`payinBook` and custAgentID = '".$cID."'" ;
				if((@$result = mysql_query ($transQuery))==FALSE)
   	{
	
				if (DEBUG=="True")
				{
					echo mysql_message($transQuery);		
				}	
				}else
			 	{	
				   
					while ($row = mysql_fetch_array($result)) 
					{
						$amountToUse = 0;
						$amountToUse = $row["totalAmount"] - $row["AgentComm"];
						$Balance = $Balance - $amountToUse;
							
					}
				
				}
		}	
				
				
				
		return $Balance;
}


////////


/**
 * Calculating the A&D balance based on the currency provided, to and from dates
 * This function is copy of agentBalanceWithCurrency but with more parameter
 *
 * @var $cID 	int		The A&D id
 * @var $curr	string	The balance for which currency (optional)
 * @var $from 	string	From which data, thbalance should calculated
 * @var $to		string	Upto which date, the balance should 
 *
 * @return 	$Balance	int		The calculated balance based on given parameters
 */
function agentNdistributorBalance($cID, $curr='',$to='',$from='')
{
	
		$Balance = 0;
		
		$agentInfo = selectFrom("select isCorrespondent from ".TBL_ADMIN_USERS." where userID = '".$cID."'");
		
		$currencyQry = "";
		if(!empty($curr))
			$currencyQry = " and currency='$curr'";
			
		if(!empty($to))
			$currencyQry .= " and dated < '$to'";
		
		if(CONFIG_AnD_ENABLE == '1' && $agentInfo["isCorrespondent"] == 'Y')
		{
			$accountQuery = "Select sum(amount) as amount, type from agent_Dist_account where agentID = $cID ";
			$accountQuery .= " and status != 'Provisional' $currencyQry group by type";	
		}else{
			
			$accountQuery = "Select sum(amount) as amount, type from agent_account where agentID = $cID ";
			$accountQuery .= " and status != 'Provisional' $currencyQry  group by type";
		
		}
		//debug($accountQuery);
		$contentsAcc = selectMultiRecords($accountQuery);
		
		$allCount = count($contentsAcc);
		for($i=0;$i < count($contentsAcc);$i++)		
		{
			if($contentsAcc[$i]["type"] == "DEPOSIT")
			{
			  	$Balance = $Balance+$contentsAcc[$i]["amount"];
			}
			  	
			if($contentsAcc[$i]["type"] == "WITHDRAW")
			{
			  	$Balance = $Balance-$contentsAcc[$i]["amount"];
			}
		}	
			
		return $Balance;
}


function onlineCustomerAccountBalance($intCustomerId)
{

	$strRandomQuery = "SELECT 
							sum(amount) as WA
					   FROM 
							customer_account
					   WHERE 
							customerID = '".$intCustomerId."' AND
							Type = 'WITHDRAW'";
	
	$nRandomResult = mysql_query($strRandomQuery) or die("Invalid query: " . mysql_error());
	$nRows = mysql_fetch_array($nRandomResult);
	
	$strRandomQuery = "SELECT 
							sum(amount) as DA
					   FROM 
							customer_account
					   WHERE 
							customerID = '".$intCustomerId."' AND
							Type = 'DEPOSIT'";

	$nRandomResult = mysql_query($strRandomQuery) or die("Invalid query: " . mysql_error());
	$nRowsDeposit = mysql_fetch_array($nRandomResult);

	return round($nRowsDeposit["DA"] - $nRows["WA"],2);
}

?>