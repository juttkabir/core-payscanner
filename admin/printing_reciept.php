<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

//$nAgentID = $_GET['agentID'];
?>

<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style5 {
	color: #005b90;
	font-weight: bold;
}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><b><font color="#FFFFFF" size="2">Confirm Transaction.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            
          <td align="right">&nbsp;</td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Secure and Safe way to send money</legend>
            <br>
            <?
			$transactionID = $_GET["transID"];
			$transQuery="select * from ".TBL_TRANSACTIONS." where transID='$transactionID'";
			$transactionContent = selectFrom($transQuery);
			///custAgentID 
			$customerAgent = $transactionContent["custAgentID"]; 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='$customerAgent'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$custAgentParentID = $senderAgentContent["parentID"];
	if($senderAgentContent["logo"] != "" && file_exists("../logos/" . $senderAgentContent["logo"]))
	{
	$arr = getimagesize("../logos/" . $senderAgentContent["logo"]);
	
	$width = ($arr[0] > 200 ? 200 : $arr[0]);
	$height = ($arr[1] > 100 ? 100 : $arr[1]);
	?>
            <table border="1" align="center" cellpadding="2" cellspacing="2" bordercolor="#DFE6EA">
              <tr>
                <td><img src="../logos/<? echo $senderAgentContent["logo"]?>" width="<? echo $width?>" height="<? echo $height?>"></td>
              </tr>
            </table>
            <?
	}
	else
	{
	?>
            <table width="277" border="0" align="center">
              <tr>
                <td width="323" height="80" colspan="4" align="center" bgcolor="#D9D9FF"><img src="images/logo.jpg" width="326" height="287"></td>
              </tr>
            </table>
	<?
	}
	?>
            <br>
			<font color="#005b90">Transaction Date: </font><? echo date("F j, Y")?><br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Agent Details</legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td align="right"><font color="#005b90">Company Name</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentCompany"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Country</font></td>
                <td width="200"><? echo $senderAgentContent["agentCountry"]?></td>
                <td width="100" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">MSB No.</font></td>
                <td width="200"><? echo $senderAgentContent["agentMSBNumber"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="200"><? echo $senderAgentContent["email"]?></td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td>
			
			<fieldset>
              <legend class="style2">Customer Details </legend>
              <table border="0" cellpadding="2" cellspacing="0">
                <? 
			$transCust = $transactionContent["customerID"]; 
			$queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='$transCust'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Customer Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="200"><? echo $customerContent["Email"];?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
              <? 
			
		/*	$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $_POST["agentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			$benAgentParentID = $senderAgentContent["parentID"];
			if($senderAgentContent["userID"] != "")
			{*/
		?>
              <tr>
                <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table border="0" cellpadding="2" cellspacing="0" bordercolor="#006600">
                      <? 
		    $transBene = $transactionContent["benID"];
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email from ".TBL_BENEFICIARY." where benID ='$transBene'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Beneficiary Name</font></td>
                        <td colspan="2"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="2"><? echo $benificiaryContent["Address"] . " " . $benificiaryContent["Address1"]?></td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                        <td width="200"><? echo $benificiaryContent["Zip"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? echo $benificiaryContent["Country"]?>      </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? echo $benificiaryContent["Phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">Email</font></td>
                        <td width="200"><? echo $benificiaryContent["Email"]; ?></td>
                      </tr>
                      <?
				}
				$transType = $transactionContent["transType"];
			  if($transType == "Bank Transfer")
			  {
		  //			$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transactionID."'");
					?>
                      <tr>
                        <td colspan="2"><span class="style5">Beneficiary Bank Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200"><? //echo $_POST["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? //echo $_POST["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200"><? //echo $_POST["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                          <?
				/*if($_POST["benCountry"] == "United States")
				{
					echo "ABA Number*";
				}
				elseif($_POST["benCountry"] == "Brazil")
				{
					echo  "CPF Number*";
				}*/
				?>
                        &nbsp;  </font></td>
                        <td width="200"><?
				/*if($_POST["benCountry"] == "United States" || $_POST["benCountry"] == "Brazil")
				{*/
				?>
                          <? /*echo $_POST["ABACPF"];*/ ?>
                          <?
				//}
				?> &nbsp;</td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200"><? // echo $_POST["branchAddress"]; ?>                        </td>
		<td width="100" height="20"  align="right">&nbsp; </td>
						<td width="200" height="20">&nbsp;  </td>
                      </tr>
                      <tr>
                        <td width="150" align="right">&nbsp;  </td>
                        <td width="200">&nbsp;  </td>
                        <td width="100" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" title="For european Countries only">&nbsp;</td>
                      </tr>
                      <?  //echo $_POST["IBAN"]; color="#005b90">IBAN Number</font font color="#005b90"Swift Code font echo $_POST["swiftCode"];
  			}
			  if($transType == "Pick up")
			  {
			  
			//$queryCust = "select *  from cm_collection_point where  cp_id  ='" . $_SESSION["collectionPointID"] . "'";
			//$senderAgentContent = selectFrom($queryCust);			
			//if($senderAgentContent["cp_id"] != "")
			{
		?>
                      <tr>
                        <td colspan="2"><span class="style5">Collection Point Details </span></td>
                        <td width="100">&nbsp;</td>
                        <td width="200">&nbsp;</td>
                      </tr>		
                        <tr>
                        <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                        <td width="200"><? // echo $senderAgentContent["cp_corresspondent_name"]?> </td>
                        <td width="100" align="right"><font color="#005b90">Contact Person </font></td>
                        <td width="200"><?   //echo $senderAgentContent["cp_corresspondent_name"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Address</font></td>
                        <td colspan="3"><? //echo $senderAgentContent["cp_branch_address"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Company</font></td>
                        <td width="200"><? //echo $senderAgentContent["cp_corresspondent_name"]?></td>
                        <td width="100" align="right"><font color="#005b90">Country</font></td>
                        <td width="200"><? //echo $senderAgentContent["cp_country"]?></td>
                      </tr>
                      <tr>
                        <td width="150" align="right"><font color="#005b90">Phone</font></td>
                        <td width="200"><? //echo $senderAgentContent["cp_phone"]?></td>
                        <td width="100" align="right"><font color="#005b90">City</font></td>
                        <td width="200"><? //echo $senderAgentContent["cp_city"]?></td>
                      </tr> 
                      <?
			  }
			  
			  
			  }
  ?>
            </table>
              </fieldset></td>
              </tr>
         
<?

//}
?>		  
          <tr>
            <td></td>
          </tr>
		  
		 
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" align="right"><font color="#005b90"><font color="#005b90"><? echo $manualCode; ?></font></font></td>
                <td width="200"><? echo $transactionContent["refNumber"]?> </td>
                <td width="100" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" ><? echo $transactionContent["transAmount"]?> in  <? echo $transactionContent["currencyFrom"]?></td>
              </tr>
			  <tr>
                <td width="150" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200"><? echo $transactionContent["exchangeRate"]?>                  </td>
                <td width="100" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="200"><? echo $transactionContent["localAmount"]?>  in  <? echo $transactionContent["currencyTo"];?>              </td>
			  </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? echo $systemPre; ?> Fee</font></td>
                <td width="200"><? echo $transactionContent["IMFee"]?>                </td>
                <td width="100" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="200"><? echo $transactionContent["totalAmount"]?>                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200"><? echo $transactionContent["transactionPurpose"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="200"><? echo $transactionContent["fundSources"]?>
                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200"><? echo $transactionContent["moneyPaid"]?>
                </td>
				<?
				/*if($_SESSION["moneyPaid"] == 'By Cash')
				{
				?>
                <td width="100" align="right">&nbsp;</td>
                <td width="200" align="left">&nbsp; </td>
				<?
				}
				else
				{*/
				?>
                <td width="100" align="right">&nbsp;</td>
                <td width="200" align="left">&nbsp;</td>			
				<?
				//}
				?>
              </tr>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="left"><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $_POST["transType"]?></td>
                <td width="100" align="right">&nbsp;</td>
                <td width="200">&nbsp;</td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? echo $systemPre; ?> <? echo $systemCode; ?> </font></td>
                <td width="200" height="20">
				<? 
						$query = "select count(transID) as cnt from ". TBL_TRANSACTIONS." where custAgentID = '".$transactionContent["custAgentID"]."'";
						$nextAutoID = countRecords($query);
						$nextAutoID = $nextAutoID + 1;
						$cntChars = strlen($nextAutoID);
						if($cntChars == 1)
							$leadingZeros = "00000";
						elseif($cntChars == 2)
							$leadingZeros = "0000";
						elseif($cntChars == 3)
							$leadingZeros = "000";		
						elseif($cntChars == 4)
							$leadingZeros = "00";		
						elseif($cntChars == 5)
							$leadingZeros = "0";		
						if($agentType == "admin" || $agentType == "Call")
						{
							$senderAgent = selectFrom("select username from ". TBL_ADMIN_USERS." where userID = '".$transactionContent["custAgentID"]."'");
							$imReferenceNumber = strtoupper($senderAgent["username"]) . $leadingZeros . $nextAutoID ;
						}
						else
						{
							$imReferenceNumber = strtoupper($username) . $leadingZeros . $nextAutoID ;
						}

				echo $imReferenceNumber?> </td>
                <td width="100" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
              </tr>
            </table>
            </fieldset>
              </td>
          </tr>
          <tr>
            <td align="center"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td width="50%"> _______________________________<br>
                  <br>
                Agent Official Signature</td>
                <td width="50%" align="right"> _______________________________<br>
                  <br>
                Customer Signature </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center">
				  <form name="addTrans" action="add-transaction-conf.php" method="post">
				  <input name="benAgentParentID" type="hidden" value="<? echo $benAgentParentID?>">
				  <input name="custAgentParentID" type="hidden" value="<? echo $custAgentParentID?>">
              <?
	  		foreach ($_POST as $k=>$v) 
			{
				$hiddenFields .= "<input name=\"" . $k . "\" type=\"hidden\" value=\"". $v ."\">\n";
				$str .= "$k\t\t:$v<br>";
			}
		
//echo $str;
echo $hiddenFields;
	  ?>
              <input type="button" name="Submit2" value="Print this Receipt"  onKeyDown="print()" onKeyUp="print()">
            </form>			
			</td>
          </tr>
          <tr>
            
          <td align="right">&nbsp;</td>
          </tr>
        </table></td>
    </tr>

</table>
</body>
</html>
