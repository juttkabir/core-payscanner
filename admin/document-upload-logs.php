<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Document Upload Tracking Logs</title>

<style>
body{
	margin:0;
	font-family:arial;
	color:#555;
	font-size:12px;
}
h2{
	margin-bottom:0;
}
.wrapper{
	max-width:980px;
	margin:0 auto;
	overflow:hidden;
}
.headtxt{
	color:#fff;
	background:#6699CC;
	padding:2px;
	font-size:14px;
}
label,p,.link{
	font-size:12px;
}
p{
	font-weight:bold;
}
.container{
	border:1px solid #aaa;
	padding:10px 5px;
	overflow:hidden;
}
.link{
	float:right;
	display:inline-block;
	text-decoration:none;
}
label{
	display:inline-block;
	vertical-align:top;
	padding-top:2px;
}
input{
	margin:0;
}
.left-col{
	float:left;
	width:50%;
}
.right-col{
	float:right;
	width:50%;
}
.row{
	clear:both;
	margin:5px 0;
	padding:5px;
	overflow:hidden;
}
.msg{
	font-size:12px;
}
.title{
	background:#A2CAF2;
}
table{
	border:1px solid #000000;
}
table tr td, table, table tr,table tr th{
	font-size:11px;
}
table tr td, table tr th{
	text-align:center;
	padding:2px;
}
table tr th{
	color:#fff;
}
input, select{
	font-size:12px;
	margin-right:10px;
}
</style>
<script>
			function SelectOption(OptionListName, ListVal)
			{
				for (i=0; i < OptionListName.length; i++)
				{
					if (OptionListName.options[i].value == ListVal)
					{
						OptionListName.selectedIndex = i;
						break;
					}
				}
			}
</script>
</head>
<?

session_start();
include ("../include/config.php");
include ("security.php");
$db = dbConnect();
$agentType2 = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];


if(isset($_POST['searchRule'])){
	$search = $_POST['search'];
	$searchby = $_POST['searchby'];
	
	if($search == "Customer Number")
	{
		$condition = "accountName like '%".$searchby."%' ";	
		
	}
	else if($search == "Customer First Name")
	{
		$condition = "firstName like '%".$searchby."%' ";	
		
	}
	else if($search == "Customer Last Name")
	{
		$condition = "lastName like '%".$searchby."%' ";	
		
	}
	if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ){
		$customerID=selectMultiRecords("select customerID from customer WHERE agentID=$parentID");
		$customerIDs = "";
		for ($i=0;$i<count($customerID);$i++) {
			$userID = $customerID[$i];
			if ($i === count($customerID)-1) {
				$customerIDs .= "'".$userID['customerID']."'";
			} else {
				$customerIDs .= "'".$userID['customerID']."', ";
			}
		}

		$query = "select customer.accountName, customer.firstName, customer.lastName, registrationFromRequest.logs from registrationFromRequest left join customer on registrationFromRequest.session_id=customer.customerID where registrationFromRequest.session_id !=0 and registrationFromRequest.session_id !=1 and ".$condition." and session_id in ($customerIDs)";

	}
	else {
		$query = "select customer.accountName, customer.firstName, customer.lastName, registrationFromRequest.logs from registrationFromRequest left join customer on registrationFromRequest.session_id=customer.customerID where registrationFromRequest.session_id !=0 and registrationFromRequest.session_id !=1 and " . $condition . " ";
	}
}
else {
	if ($agentType2 === "SUPA" || $agentType2 === "SUBA" ){
		$customerID=selectMultiRecords("select customerID from customer WHERE agentID=$parentID");
		$customerIDs = "";
		for ($i=0;$i<count($customerID);$i++) {
			$userID = $customerID[$i];
			if ($i === count($customerID)-1) {
				$customerIDs .= "'".$userID['customerID']."'";
			} else {
				$customerIDs .= "'".$userID['customerID']."', ";
			}
		}

		if ($condition == "")
			$condition = 1;
		$query = "select customer.accountName, customer.firstName, customer.lastName, registrationFromRequest.logs from registrationFromRequest left join customer on registrationFromRequest.session_id=customer.customerID where registrationFromRequest.session_id !=0 and registrationFromRequest.session_id !=1 and ".$condition." and session_id in ($customerIDs)";
	}
	else {
		$query = "select customer.accountName, customer.firstName, customer.lastName, registrationFromRequest.logs from registrationFromRequest left join customer on registrationFromRequest.session_id=customer.customerID where registrationFromRequest.session_id !=0 and registrationFromRequest.session_id !=1";
	}
}

//echo $query;
$content = SelectMultiRecords($query);





?>


<body>
	<div class="wrapper">
		<h2 class="headtxt">Registration Logs</h2>
			<div class="container">
				<div class="row">
                	<form action="" method="post">
						<label>Search By</label>
						<select name="search">
							<option value="Customer Number">Customer Number</option>
                            <option value="Customer First Name">Customer First Name</option>
                            <option value="Customer Last Name">Customer Last Name</option>
						</select>
						<script language="JavaScript">
							SelectOption(document.forms[0].search, "<?echo $_POST['search']; ?>");
                        </script>
						<input type = "text" name="searchby" />
						<input name="searchRule" value="Search" type="submit"/>
					</form>
				</div>
<table border="1" bordercolor="#555555" cellpadding="0" cellspacing="0" width="100%">
<tr class="title">
<th>
Customer Number
</th>
<th>
Customer Name
</th>
<th>
Message Logs
</th>
<th>
File Uploaded
</th>
</tr>

<?
for($i=0; $i<count($content) ; $i++)
{
	?>
    <tr>
    <td>
    <? echo $content[$i]['accountName']; ?>
    </td>
    <td>
    <? echo $content[$i]['firstName']." ".$content[$i]['lastName']; ?>
    </td>
    <?
	$slog = "No";
	$plog = "No";
	$ulog = "No";
	$rlog = "No";
	$logsall = explode(",",$content[$i]['logs']);
	for($j=0; $j<count($logsall); $j++)
	{
		if($logsall[$j] == "S")
		{
			$slog = "Yes";
		}
		
		
		if($logsall[$j] == "P")
		{
			$plog = "Yes";
		}
		
		if($logsall[$j] == "U")
		{
			$ulog = "Yes";
		}
		if($logsall[$j] == "R")
		{
			$rlog = "Yes";
		}
		
	}
	$messageLogs = "";
	if($slog == "Yes")
	{
		$messageLogs.= "Submitting your registration | ";	
		
	}
	
	if($plog == "Yes")
	{
		$messageLogs.= "Please wait for the confirmation of your passport upload. | ";	
		
	}
	if($ulog == "Yes")
	{
		$messageLogs.= "Document successfully uploaded!! | ";	
		
	}
	if($rlog == "Yes")
	{
		$messageLogs.= "Sender is registered successfully and you will receive an activation email after authentication from administrator. | ";	
		
	}
	
	
		
		
		
	
	?>
        
        <td><? echo $messageLogs; ?></td>
        <td>
        <? if($ulog == "Yes")
		{ 
        	echo "<img src='images/premier/tick.png' title='Success' height='20px' />"; 
        
        }
        else
        {
        	echo "<img src='images/premier/cross.png' title='Failure' height='18px' />";
        
        }
		?>
        </td>
    
    
	
	</tr>
<?	
}

?>








</table>
</div>
</div>

</body>
</html>
