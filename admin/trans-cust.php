<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

include ("definedValues.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];


$query = "select *  from ". TBL_TRANSACTIONS . " as t";
if($_POST["Submit"] =="Search")
{
	$fromDate = $_POST["fYear"] . "-" . $_POST["fMonth"] . "-" . $_POST["fDay"];
	$toDate = $_POST["tYear"] . "-" . $_POST["tMonth"] . "-" . $_POST["tDay"];
	$toDate = selectFrom("select date_add('$toDate', interval 1 day) as eDate");
	//echo "select date_add('$toDate', interval 1 day) as eDate";
	$toDate = $toDate["eDate"];
	$queryDate = " and (t.transDate >= '$fromDate' and t.transDate <= '$toDate')";
	if($_POST["fieldName"] != "")
	{
		switch ($_POST["fieldName"])
		{
			case "transID":
			{
				$query .= " where (t.transID = '" . $_POST["txtSearch"] . "' OR t.refNumberIM = '" . $_POST["txtSearch"] . "')  $queryDate";
				break;
			}
			case "loginName":
			{
				$query .= ", " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and a.adminType='Agent' and (a.name like '%" . $_POST["txtSearch"] . "%' OR a.username like '%" . $_POST["txtSearch"] . "%') $queryDate";
				break;
			}
			case "custName":
			{
				$query .= ", " . TBL_CUSTOMER . " as c where t.customerID = c.customerID and (c.firstName like '%" . $_POST["txtSearch"] . "%' OR c.lastName like '%" . $_POST["txtSearch"] . "%') $queryDate";
				break;
			}
			case "benName":
			{
				$query .= ", " . TBL_BENEFICIARY . " as b where t.benID = b.benID and (b.firstName like '%" . $_POST["txtSearch"] . "%' OR b.lastName like '%" . $_POST["txtSearch"] . "%') $queryDate";
				break;
			}
			case "agentName":
			{
				$query .= ", " . TBL_ADMIN_USERS . " as a where t.custAgentID = a.userID and (a.name like '%" . $_POST["txtSearch"] . "%' OR a.username like '%" . $_POST["txtSearch"] . "%') $queryDate";
				break;
			}
		}
	}
	else
	{
		$query .= " where (t.transDate >= '$fromDate' and t.transDate <= '$toDate')";
	}

	if($_POST["transStatus"] != "")
	{
		$query .= " and t.transStatus='".$_POST["transStatus"]."' ";
	}
	else
	{
		//$query .= " and t.transStatus='Confirmed' ";
	}

}
else
{
	$todate = date("Y-m-d");
	$query .= " where t.transDate >= '$todate'";
}

if($agentType == "Branch Manager")
{
	$query .= " and t.custAgentParentID ='$parentID' ";
}

$query .= "  order by transDate DESC";

$contentsTrans = selectMultiRecords($query);

//echo $query .  count($contentsTrans);
?>
<html>
<head>
	<title>Add Promotional Code</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!--
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style2 {color: #005b90; font-weight: bold; }
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Transaction Report 
      </font></strong><font color="#FFFFFF" size="2">(By default today's confirmed 
      transaction are shown)</font> </td>
  </tr>

  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="666666">
        <form action="trans-cust.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>Search Filters 
              </strong></span></td>
        </tr>
        <tr>
        <td align="center" nowrap>
		From
		<?
		$month = date("m");

		$day = date("d");
		$year = date("Y");
		?>
		
        <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		
		<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>

        <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
        To	        <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

          <?
					  	for ($Day=1;$Day<32;$Day++)
						{
							if ($Day<10)
							$Day="0".$Day;
							echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
						}
					  ?>
        </select>
		<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

          <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
          <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
          <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
          <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
          <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
          <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
          <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
          <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
          <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
          <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
          <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
          <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
        </SELECT>

        <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

          <?
								  	$cYear=date("Y");
								  	for ($Year=2004;$Year<=$cYear;$Year++)
									{
										echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
									}
		  ?>
        </SELECT>
        <br>
        <input name="txtSearch" type="text" id="txtSearch">
        <select name="fieldName" id="fieldName">
          <option value="">- Doesn't Matter</option>
          <option value="transID">Transactio ID</option>
          <option value="loginName">Login Name</option>
          <option value="custName">Customer Name</option>
          <option value="benName">Beneficiary Name</option>
          <option value="agentName">Agent Name</option>

        </select>
        <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
          <option value=""> - Status - </option>
          <option value="Processing">Processing</option>
          <option value="Pending">Pending</option>
		  <option value="Authorize">Authorize</option>
          <option value="Recalled">Recalled</option>
          <option value="Confirmed">Confirmed</option>
          <option value="Rejected">Rejected</option>
          <option value="Cancelled">Cancelled</option>
          <option value="Cancelled">Waiting Cancellation</option>
		  <option value="Picked up">Picked up</option>
		   <option value="Delivered">Delivered</option>
		  
        </select>
        <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">

	    <tr>
            <td height="25" nowrap bgcolor="C0C0C0">&nbsp;There are <? echo count($contentsTrans)?> 
              transactions<? echo ($fromDate!= "" && $toDate != ""  ? " From <strong>$fromDate</strong> to <strong>$toDate</strong>   <strong>" . strtoupper($_POST["txtSearch"]) . "</strong>" : "")?>.</td>
        </tr>
		<?
		if(count($contentsTrans) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <td><span class="style1">Date</span></td>
			  <td width="100"><span class="style1"><? echo $systemCode; ?></span></td>
			   <td width="100"><span class="style1"><? echo $manualCode; ?> </span></td>
			  <td><span class="style1">Amount </span></td>
			  <td width="100"><span class="style1">Sender Agent</span></td>
			  <td width="100"><span class="style1">Status</span></td>
			  <td align="center">&nbsp;</td>
		  </tr>
		    <? for($i=0;$i<count($contentsTrans);$i++)
			{
				?>

				<tr bgcolor="#FFFFFF">
				  <td width="100" bgcolor="#FFFFFF"><strong><a href="#" onClick="javascript:window.open('view-transaction.php?transID=<? echo $contentsTrans[$i]["transID"]?>','viewTranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></strong></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumber"]?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["totalAmount"]?></td>
				  <? $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");?>
				  <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
				    <? echo ucfirst($agentContent["name"])?></td>
				  <td width="100" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
				  <td width="200" align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>" class="style2">Enquiry</a></td>
				</tr>
				<?
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <td>&nbsp;</td>
			  <td width="100">&nbsp;</td>
			  <td>&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			   <td align="center">&nbsp;</td>
			  </tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>
