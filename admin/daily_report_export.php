<?php 
session_start();
include ("../include/config.php");
include ("security.php");

//debug($_REQUEST);
$accountQuery=$_REQUEST["query_out"];
 $accountQuery=base64_decode($accountQuery);
$contentsTrans = selectMultiRecords($accountQuery);
//debug($contentsTrans);

header("Content-type: application/x-msexcel");
header("Content-Disposition: attachment; filename=Daily_report_export.xls"); 
header("Content-Description: PHP/MYSQL Generated Data");  
header('Content-Type: application/vnd.ms-excel');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

//error_reporting(E_ALL);
//ini_set("display_errors","On");
ini_set('memory_limit','4048M');
flush();
    



$strMainStart   = "<table border='1' >";
	$strMainEnd     = "</table>";
	$strRowStart    = "<tr>";
	$strRowEnd      = "</tr>";
	$strColumnStart ="<td style='mso-number-format:\"\@\"'>";
	$strColumnEnd   = "</td>";
	$strBoldStart 	= "<b>";
	$strBoldClose 	= "</b>";
	$strColSpan		="<td align='center'>";
	$strColSpan2		="<td align='center'>";
	
	
	if(count($contentsTrans) > 0)
	{
		
		
		$strFullHtml = "";
		
		$strFullHtml .= $strHtmlOpening;
		
		
		$strFullHtml .= $strMainStart;
	
	
		$strFullHtml .= $strRowStart;    
		
		   
		
		$strFullHtml .= $strColumnStart.$strBoldStart."Reference Code".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Pounds".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Currency Type".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."FX Rate".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Local Currency".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Currency Type".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Transaction Status".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Customer Name".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Receiver Name".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Father's Name".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Branch Name".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Place Of Transmission".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strColumnStart.$strBoldStart."Money Paid".$strBoldClose.$strColumnEnd;
		$strFullHtml .= $strRowEnd;
for($i=0;$i<count($contentsTrans);$i++)
		{	
		
		   $totalTrans = $totalTrans + $contentsTrans[$i]["transAmount"];
		   $totalLocal = $totalLocal + $contentsTrans[$i]["localAmount"];
		
			$strFullHtml .= $strRowStart;
			$strFullHtml .= $strColumnStart.$contentsTrans[$i]["refNumberIM"].$strColumnEnd;
	$strFullHtml .= $strColumnStart.number_format($contentsTrans[$i]["transAmount"],2,'.',',').$strColumnEnd;
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["currencyFrom"].$strColumnEnd;
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["exchangeRate"].$strColumnEnd;
	$strFullHtml .= $strColumnStart.number_format($contentsTrans[$i]["localAmount"],2,'.',',').$strColumnEnd;
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["currencyTo"].$strColumnEnd;
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["transStatus"].$strColumnEnd;
	
	 	if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						$CollCustomer = selectFrom("select firstName, lastName from customer where customerID='".$contentsTrans[$i]["customerID"]."'"); 
								
		$strFullHtml .= $strColumnStart.$CollCustomer["firstName"].$strColumnEnd;
           						}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER')
           						{
           	$CollCustomer = selectFrom("select FirstName, LastName from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'"); 
		
                       $strFullHtml .= $strColumnStart.$CollCustomer["FirstName"].$strColumnEnd;
           						}
           			
	
	 if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName,middleName,lastName, Phone from beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 
							
							
     	$strFullHtml .= $strColumnStart.$collben["firstName"]." ".$collben["middleName"].$strColumnEnd;
           				}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName,middleName,lastName, Phone from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 
						
						
			$strFullHtml .= $strColumnStart.$collben["firstName"]." ".$collben["middleName"].$strColumnEnd;
           				}
           		 
	       if($contentsTrans[$i]["createdBy"] != 'CUSTOMER'){
           						
           					$collben = selectFrom("select firstName,middleName,lastName, Phone from beneficiary where benID='".$contentsTrans[$i]["benID"]."'");
							$strFullHtml .= $strColumnStart.$collben["lastName"].$strColumnEnd;
           				}elseif($contentsTrans[$i]["createdBy"] == 'CUSTOMER'){
           				$collben = selectFrom("select firstName,middleName,lastName, Phone from cm_beneficiary where benID='".$contentsTrans[$i]["benID"]."'"); 
		$strFullHtml .= $strColumnStart.$collben["lastName"].$strColumnEnd;
           				}
           		
	
	
	
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["branchName"].$strColumnEnd;
	
	
	  if($contentsTrans[$i]["transType"] == "Pick up"){
          	
          	$pickup = selectFrom("select cp_id,cp_corresspondent_name,cp_branch_name,cp_branch_address,cp_city from ".TBL_COLLECTION." where cp_id = '".$contentsTrans[$i]["collectionPointID"]."'");
          	$pickupLocation = $pickup["cp_city"]." ".$pickup["cp_branch_address"] ;
 					}else{
 						$pickupLocation = '';
 						
 					
	$strFullHtml .= $strColumnStart.$pickupLocation.$strColumnEnd;
					}
	$strFullHtml .= $strColumnStart.$contentsTrans[$i]["moneyPaid"].$strColumnEnd;


			
			}
		
		
				$strFullHtml .= $strRowEnd;
					$strFullHtml .= $strRowStart; 
					
					$totalTrans = $totalTrans + $contentsTrans[$i]["transAmount"];
					
					$strFullHtml .= $strColumnStart."Total".$strColumnEnd;
				$strFullHtml .= $strColumnStart.number_format($totalTrans,2,'.',',').$strColumnEnd;
				$strFullHtml .= $strColumnStart."".$strColumnEnd;
					$strFullHtml .= $strColumnStart.number_format($totalLocal,2,'.',',').$strColumnEnd;
				
				$strFullHtml .= $strRowEnd;
				 
		$strFullHtml .= $strHtmlClosing;
		$strFullHtml .= $strMainEnd;		
		
		
		
	}
	
	
	echo $strFullHtml;
	?>
