<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
$agentType2 = getAgentType();
$parentID = $_SESSION["loggedUserData"]["userID"];
$parentuser = $_SESSION["loggedUserData"]["username"];
if ($offset == "")
	$offset = 0;
$limit=10;

if ($_GET["val"] != "")
	$_SESSION["amount_transactions"] = $_GET["val"];
//if ($_POST["l_amount"] != "")
//	$_SESSION["amount_left"] = $_POST["l_amount"];

if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = 'add-transaction.php';
	}


if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;


$agentName = ($_GET["agentName"] != "" ? $_GET["agentName"] : $_POST["agentName"]);
$qryString .= ($_GET["agentName"] != "" ? "&agentName=" . $_GET["agentName"] : "&agentName=" . $_POST["agentName"]);

$agentType = ($_GET["agentType"] != "" ? $_GET["agentType"] : $_POST["agentType"]);
$qryString .= ($_GET["agentType"] != "" ? "&agentType=" . $_GET["agentType"] : "&agentType=" . $_POST["agentType"]);

$query = "select * from ". TBL_ADMIN_USERS." where adminType='Agent' and isCorrespondent != 'ONLY' and agentType='$agentType' and agentStatus='Active' AND (username like '$agentName%' OR name like '$agentName%')";///Changed Wild Card

//echo("[".$agentType2."]");As agentType variable is overwritten here that is why $agentType2 = getAgentType(); is used.
if($agentType2 == "Branch Manager"){
	 $query .= " and parentID = '$parentID'";						
}


/**
 * In case of the agent are associated with admin staff
 * than at the create transaction only those agents will be searched which are associated with thte logged in admin staff
 * This is only case with the admin staff here
 * @Ticket #4384 
 */
if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1' && $agentType2 == "Admin") 
{
	$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$parentID."'");
	
	if($adminLinkDetails["linked_Agent"] != ",")
	{	
		$linkedAgent = substr($adminLinkDetails["linked_Agent"],0,strlen($adminLinkDetails["linked_Agent"])-1);
		$linkedAgent = str_replace(",","','",$linkedAgent);
		//debug($adminLinkDetails["linked_Agent"]);
		$linkedAgent = "'".$linkedAgent."'";
		//debug($linkedAgent);
		
		$query .= " and username in (".$linkedAgent.") ";
	}
	
}

if(CONFIG_SHARE_OTHER_NETWORK == '1')
{
 	$query .= " and userID != (select localServerId from ".TBL_SHAREDUSERS." where availableAs = 'Base Agent') ";
}

if(CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && getAgentType() == "SUPA")
{
	$query .= " and parentID = ".$_SESSION["loggedUserData"]["userID"];
}


$contentsAgent = selectMultiRecords($query);
$allCount = count($contentsAgent);

$query .= " LIMIT $offset , $limit";
$contentsAgent = selectMultiRecords($query);

//debug($query .  count($contentsTrans));
?>
<html>
<head>
	<title>Search Agents</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript">
	<!-- 
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
// end of javascript -->
</script>
<script language="JavaScript">
<!--
function CheckAll()
{

	var m = document.trans;
	var len = m.elements.length;
	if (document.trans.All.checked==true)
    {
	    for (var i = 0; i < len; i++)
		 {
	      	m.elements[i].checked = true;
	     }
	}
	else{
	      for (var i = 0; i < len; i++)
		  {
	       	m.elements[i].checked=false;
	      }
	    }
}	
-->
</script>

    <style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}
-->
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar"><strong><font color="#FFFFFF" size="2">Search Results 
      Agents</font></strong></td>
  </tr>
  
  <tr>
    <td align="center"><br>
      <table border="1" cellpadding="5" bordercolor="#666666">
        <form action="search-agent.php" method="post" name="Search">
      <tr>
            <td nowrap bgcolor="#C0C0C0"><span class="tab-u"><strong>Search the 
              agents </strong></span></td>
        </tr>
        <tr>
        <td nowrap>
		  Agent Name or Number 
		  <input type="text" name="agentName" value="<? echo $agentName?>">
          <select name="agentType" id="agentType" style="font-family:verdana; font-size: 11px" >
			<? if(CONFIG_CREATE_TRANSACTION_FOR_SUB_AGENT == 1 && getAgentType() == "SUPA") { ?>
			<option value="Sub">Sub Agent</option>
			<? }else{ ?>
            <option value="Supper">Super Agent</option>
            <option value="Sub">Sub Agent</option>
			<? } ?>
          </select>
            <input type="submit" name="Submit" value="Search"></td>
      </tr>
	  </form>
    </table>
      <br>
	  <? if ($_GET["msg"] == "Y")
	  {
	  ?>
      <table width="650" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
        <tr>
          <td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
          <td width="635"><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['error']."</b></font>"; ?></td>
        </tr>
      </table>
	  <?
	  }
	  ?>
      <br>
      <br>
      <table width="700" border="1" cellpadding="0" bordercolor="#666666">
        <form action="manage-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">

	    <tr>
            <td height="25" nowrap bgcolor="#333333">
<table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
            <tr>
              <td><?php 
              	if(count($contentsAgent) > 0 && CONFIG_ADMIN_ASSOCIATE_AGENT != '1') {;?>
      Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($contentsAgent));?></b> of
      <?=$allCount; ?>
      <?php } ;
      ?>
              </td>
              <?php if ($prv >= 0) { ?>
              <td width="50"><a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"].$qryString;?>&r=<?=$_GET["r"]?>"><font color="#000000">First</font></a> </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"].$qryString;?>&r=<?=$_GET["r"]?>"><font color="#000000">Previous</font></a> </td>
              <?php } ?>
              <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>

              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"].$qryString;?>&r=<?=$_GET["r"]?>"><font color="#000000">Next</font></a>&nbsp; </td>
              <td width="50" align="right"><a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"].$qryString;?>&r=<?=$_GET["r"]?>"><font color="#000000">Last</font></a>&nbsp; </td>
              <?php } ?>
            </tr>
          </table></td>
        </tr>
		<?
		if(count($contentsAgent) > 0)
		{
		?>
        <tr>
          <td nowrap bgcolor="#EFEFEF"><table width="700" border="0" bordercolor="#EFEFEF">
			<tr bgcolor="#FFFFFF">
			  <!--td><span class="style1">Agent Number</span></td-->
			  <td><span class="style1">Agent username</span></td>
			  <td><span class="style1">Contact Person</span></td>
			  <td width="100" bgcolor="#FFFFFF"><span class="style1">Company</span></td>
			  <td width="100"><span class="style1">Agent Name </span></td>
			  <td align="center"><span class="style1">Status</span></td>
			  <td align="center">&nbsp;</td>
		  </tr>
		    <? for($i=0;$i < count($contentsAgent);$i++)
			{
					$flag = False;
					if(CONFIG_ADMIN_ASSOCIATE_AGENT == '1')
					{
						$adminLinkDetails = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID='".$parentID."'");
						
						$linkedAgent = explode(",",$adminLinkDetails["linked_Agent"]);	
						if (in_array($contentsAgent[$i]["username"], $linkedAgent)) 
						{ 
						   $flag = True;
						}
						if(strpos(CONFIG_ASSOCIATED_ADMIN_TYPE, $agentType2.",") === false){
							$flag = True;
						}
	
						//$query .= " and username in '".$linkedAgent."'";
					}else{
						$flag = True;
						}
				if($flag)
				{		
					?>
					<tr bgcolor="#FFFFFF">
					  <!--td width="75"><strong><font color="#006699"><? //echo $contentsAgent[$i]["agentNumber"]?></font></strong></td-->
					  <td width="75"><? echo $contentsAgent[$i]["username"];?></td>
					  <td width="100"><? echo $contentsAgent[$i]["agentContactPerson"]?></td>
					  <td width="100" bgcolor="#FFFFFF"><? echo $contentsAgent[$i]["agentCompany"]?></td>
					  
					  <td width="100">
					  <? echo ucfirst($contentsAgent[$i]["name"])?></td>
					  
					  <td align="center"><? if($contentsAgent[$i]["agentStatus"] == "Active") { ?><a href="#" onClick="javascript:opener.document.location = '<?=$returnPage?>?aID=<? echo $contentsAgent[$i]["userID"]?>&transID=<? echo $_GET["transID"]?>'; window.close();" class="style3">Select</a><? } ?></td>
					</tr>
					<?
				}
			}
			?>
			<tr bgcolor="#FFFFFF">
			  <!--td>&nbsp;</td-->
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td bgcolor="#FFFFFF">&nbsp;</td>
			  <td width="100" align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  <td align="center">&nbsp;</td>
			  </tr>
			<?
			} // greater than zero
			?>
          </table></td>
        </tr>
		</form>
      </table></td>
  </tr>

</table>
</body>
</html>