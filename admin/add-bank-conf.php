<?php
session_start();

include ("../include/config.php");
include ("security.php");

$_SESSION["name1"] = $_POST["name"];
$_SESSION["account1"] = $_POST["account"];

/**In case adding sending bank sort code is stored in bankId and branchCode field in the table**/
 
if($_GET["moneyPaid"] == "By Bank Transfer")
{
	if(!empty($_REQUEST["id"]))
		$_SESSION["branchCode1"] = $_POST["branchCode"];	
	else
	{
		$_SESSION["branchCode1"] = $_POST["bankId"];
		unset($_POST["bankId"]);
	}
}
else
	$_SESSION["branchCode1"] = $_POST["branchCode"];

$_SESSION["branchAddress1"] = $_POST["branchAddress"];
$_SESSION["swiftCode1"] = $_POST["swiftCode"];
$_SESSION["accountType1"] = $_POST["accountType"];
$_SESSION["country1"] = $_POST["country"];
$_SESSION["bankId1"] = $_POST["bankId"];
$_SESSION["branchName1"] = $_POST["branchName"];
$_SESSION["district1"] = $_POST["district"];
$_SESSION["state1"] = $_POST["state"];



$backURL = "add-bank.php?bankID=".$_POST["bankID"]."&msg=Y&moneyPaid=".$_GET["moneyPaid"]."&custCountry=".$_GET['custCountry'];



if($_POST["Save"])
{
	
	//$branchCode = $_SESSION["branchCode1"];
	//if(empty($branchCode))
	//	$branchCode = 0;
		
	
	if ($_POST["id"] == "")
	{
		$dbBankId = $_POST["bankId"];
		/**
		 * Checking, if empty bankId than get a uniq id and
		 * check if any id in db exisits against newly generated id
		 * This will be used instead of manual bankId
		 */
		if(empty($dbBankId))
		{
			/**
			 * Generate a new id, and keep generating new id untill no db entry matched
			 */
			do
			{
				$dbBankId = uniqid();
				$dbBankIdData = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
			}	
			while(!empty($dbBankIdData["bankId"]));
		}
		/**
		 * If bankId is provided via from
		 * Than check the uniqness of bankId provided
		 */
		else
		{
			
			$checkBankId = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' ");
			
			/** 
			This config added by Niaz Ahmad at 17-04-2009 @4720
			For Muthoot client bank id is not unique it can be duplicate.
			*/
			if(CONFIG_ADD_BEN_BANK_ON_CREATE_TRANSACTION != "1"){ 
			
			if(!empty($checkBankId["bankId"]))
			{
				insertError(B1);
				redirect($backURL);
			}
		 }
		}
		$benBankQry = "Insert into ".TBL_BANKS. " (bankId, name, country, branchCode, branchAddress, swiftCode,extra2,extra3)
										values ('".$dbBankId."','".$_POST["name"]."','".$_POST["country"]."','".$_SESSION["branchCode1"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."','".$_POST["district"]."','".$_POST["state"]."')";

		if(isset($_POST["branchName"])){
			$benBankQry = "Insert into ".TBL_BANKS. " (bankId, name, country, branchCode, branchAddress, swiftCode,branchName,extra2,extra3)
											values ('".$dbBankId."','".$_POST["name"]."','".$_POST["country"]."','".$_SESSION["branchCode1"]."','".$_POST["branchAddress"]."','".$_POST["swiftCode"]."','".$_POST["branchName"]."','".$_POST["district"]."','".$_POST["state"]."')";
		}
		
		insertInto($benBankQry);
		$lastId = @mysql_insert_id();

		$_SESSION["name1"] = "";
		$_SESSION["branchCode1"] = "";
		$_SESSION["branchAddress1"] = "";
		$_SESSION["swiftCode1"] = "";
		$_SESSION["country1"] = "";
		$_SESSION["bankId1"] = "";
		$_SESSION["branchName1"] = "";
		$_SESSION["district1"] = "";
		$_SESSION["state1"] = "";
	
		$_SESSION["error"] = "Bank Added successfully";
		
		if($_GET["moneyPaid"] == "By Bank Transfer")
				$returnUrl = "add-transaction.php?msg=Y&success=Y&transID=".$_POST['transID']."&moneyPaid=".$_GET['moneyPaid']."&custCountry=".$_GET['custCountry']."&senderBank=".$_POST['bankId'];
		else
				$returnUrl = "add-transaction.php?msg=Y&success=Y&ben_bank_id=".$lastId."&transID=".$_POST['transID']."&moneyPaid=".$_GET['moneyPaid']."&custCountry=".$_GET['custCountry']."&senderBank=".$_POST['bankId'];
		
		?>
	
		<script language="javascript">
			<?
			
				if(isset($_REQUEST["opener"]) && $_REQUEST["opener"] == "left")
				{
					echo "\ndocument.location = \"add-bank.php?msg=Y&success=Y&moneyPaid=".$_GET['moneyPaid']."&custCountry=".$_GET['custCountry']."&opener=".$_REQUEST['opener']."\";\n";
				} else {
				     
					echo "\nopener.document.location = \"".$returnUrl."\";\n";
					echo "window.close();\n";
				}
			?>
		</script>
	<?
		
	
	} 
	else 
	{
		$dbBankId = $_REQUEST["bankId"];
		
		$checkBankId = selectFrom("SELECT bankId from ".TBL_BANKS. " WHERE bankId ='".$dbBankId."' and id != ".$_REQUEST["id"]);
			
		if(!empty($checkBankId["bankId"]))
		{
			insertError(B1);
			$backURL = "edit-bank.php?id=".$_REQUEST["id"]."&msg=Y&moneyPaid=".$_GET["moneyPaid"]."&custCountry=".$_GET['custCountry'];

			redirect($backURL);
		}
			
		$benBankUpdateQry = "update ".TBL_BANKS. " set 
									bankId = '".$dbBankId."', 
									name = '".$_POST["name"]."', 
									country = '".$_POST["country"]."', 
									branchCode = '".$_SESSION["branchCode1"]."',
									branchAddress = '".$_POST["branchAddress"]."',
									branchName = '".$_POST["branchName"]."',
									extra2 = '".$_POST["district"]."',
									extra3 = '".$_POST["state"]."'
									where id = ".$_REQUEST["id"];
		
		update($benBankUpdateQry);
		
		$_SESSION['error'] = "Bank details has been successfully updated!";
		$successUrl = "edit-bank.php?success=Y&msg=Y&moneyPaid=".$_GET["moneyPaid"]."&id=".$_REQUEST["id"];
		redirect($successUrl);
	/*
	?>
	
		<script language="javascript">
			opener.document.location = "<?=$transactionPage?>?msg=Y&success=Y&ben_bank_id=<?=$bankID?>&transID=<?=$_POST['transID']?>&moneyPaid=<?=$_GET['moneyPaid']?>&custCountry=<?=$_GET['custCountry']?>&senderBank=<?=$_POST['bankId']?>";
			window.close();
		</script>
	<?
	*/
	}
}
?>
