<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
include ("calculateBalance.php");
$agentType = getAgentType();
$agentCountry = $_SESSION["loggedUserData"]["IDAcountry"];
$changedBy = $_SESSION["loggedUserData"]["userID"];
$username  = $_SESSION["loggedUserData"]["username"];


session_register("grandTotal");
session_register("CurrentTotal");
session_register("fMonth");
session_register("fDay");
session_register("fYear");
session_register("tMonth");
session_register("tDay");
session_register("tYear");
if($_POST["Submit"]=="Search")
{
	$_SESSION["fMonth"]="";
	$_SESSION["fDay"]="";
	$_SESSION["fYear"]="";
	
	$_SESSION["tMonth"]="";
	$_SESSION["tDay"]="";
	$_SESSION["tYear"]="";
			
		
	$_SESSION["fMonth"]=$_POST["fMonth"];
	$_SESSION["fDay"]=$_POST["fDay"];
	$_SESSION["fYear"]=$_POST["fYear"];
	
	$_SESSION["tMonth"]=$_POST["tMonth"];
	$_SESSION["tDay"]=$_POST["tDay"];
	$_SESSION["tYear"]=$_POST["tYear"];
	
}

$_SESSION["agentID2"] = "";
if($_REQUEST["agentID"]!="")
	$_SESSION["agentID2"] =$_REQUEST["agentID"];

?>
<html>
<head>
<title>Dual Ledger Report</title>
<script>
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}
</SCRIPT>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {color: #005b90}
.style3 {
	color: #3366CC;
	font-weight: bold;
}

.red-row {
	background-color:#CCCCCC;
	font-family:Arial, Helvetica, sans-serif;
	color: #FF0033;
	font-size:13px;
}
.black-row {
	background-color:#ffffff;
	font-family:Arial, Helvetica, sans-serif;
	color: #555555;
	font-size:13px;
}

.hover-row{
	background-color:#000000;
	font-family:Arial, Helvetica, sans-serif;
	color: #ffffff;
	font-size:13px;
}
.sumup-row td {
	color:#FFFFFF; 
	font-weight:bold; 
	font-size:12px;
}

-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="100%" border="1" cellspacing="1" cellpadding="5">
	 <tr>
    <td class="topbar">Dual Ledger Report</td>
  </tr>
	<tr>
      <td width="100%">
		<form action="dualLedgerReport.php" method="post" name="Search">
  			<div align="center">From 
        		<? 
				$month = date("m");
		
				$day = date("d");
				$year = date("Y");
				?>
                
                <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">
                	<?
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}
		  			?>
                </select>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].fDay, "<?=$_SESSION["fDay"]; ?>");
        		</script>
		
				<SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         			SelectOption(document.forms[0].fMonth, "<?=$_SESSION["fMonth"]; ?>");
        		</script>
                
				<SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">
                <?
				$cYear=date("Y");
				for ($Year=2004;$Year<=$cYear;$Year++)
				{	
					echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
				}
		  		?>
                </SELECT>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].fYear, "<?=$_SESSION["fYear"]; ?>");
        		</script>
                
				To 
               
                <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">
                  	<?
					for ($Day=1;$Day<32;$Day++)
					{
						if ($Day<10)
						$Day="0".$Day;
						echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
					}	
		  			?>
                </select>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].tDay, "<?=$_SESSION["tDay"]; ?>");
        		</script>
		 		
				<SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">
                  <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                  <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                  <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                  <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                  <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                  <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                  <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                  <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                  <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                  <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                  <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                  <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                </SELECT>
                <script language="JavaScript">
         			SelectOption(document.forms[0].tMonth, "<?=$_SESSION["tMonth"]; ?>");
        		</script>
               
			    <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">
                	<?
					$cYear=date("Y");
					for ($Year=2004;$Year<=$cYear;$Year++)
					{
						echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
					}
		  			?>
                </SELECT>
                <script language="JavaScript">
		         	SelectOption(document.forms[0].tYear, "<?=$_SESSION["tYear"]; ?>");
        		</script>
                
				<br>
                &nbsp;&nbsp; 
                <?    
				if($agentType == "admin" || $agentType == "Admin Manager" || $agentType == "COLLECTOR" || $agentType == "Branch Manager")//101
				{
				?>
                	<select name="agentID" style="font-family:verdana; font-size: 11px">
                  		<option value="">- Select One-</option>
                  		<?
		                $agentQuery  = "select userID,username, agentCompany, name, agentContactPerson from ".TBL_ADMIN_USERS." where parentID > 0 and adminType='Agent' and isCorrespondent = 'Y'";
					
						if($agentType == "Branch Manager")
						{
							$agentQuery .= " and parentID = '$changedBy'";				
									
						}
						$agentQuery .= "order by agentCompany";
						$agents = selectMultiRecords($agentQuery);
						for ($i=0; $i < count($agents); $i++){
						?>
	                  		<option value="<?=$agents[$i]["userID"]; ?>"><? echo($agents[$i]["agentCompany"]." [".$agents[$i]["username"]."]"); ?></option>
                  		<?
						}
						?>
                	</select>
                	<script language="JavaScript">
			         	SelectOption(document.forms[0].agentID, "<?=$_SESSION["agentID2"]; ?>");
                    </script>
                 <? 
				 } 
				 ?>
          
          		<? 
          
          		if($agentType == "SUPAI")
          		{
          			$_SESSION["agentID2"] = $_SESSION["loggedUserData"]["userID"];
          		}
          
          
				?>
				<input type="submit" name="Submit" value="Search">
				<br />
				<br />
				<div align="left" style="width:50%; color:#666666">
					This report containst the A&D's transaction related to the account ledger. So this report does not 
					calculate and display the manual deposit entry between transactions.
				</div>
            </div>
			<tr>
		<td>
		
      <?
	 if($_POST["Submit"]=="Search"||$_GET["search"]=="search")
	 {
		$slctID=$_REQUEST["agentID"];
			
		$fromDate = $_SESSION["fYear"] . "-" . $_SESSION["fMonth"] . "-" . $_SESSION["fDay"];
		$toDate = $_SESSION["tYear"] . "-" . $_SESSION["tMonth"] . "-" . $_SESSION["tDay"];
		$queryDate = "(dated >= '$fromDate 00:00:00' and dated <= '$toDate 23:59:59')";
			 
		if ($slctID !="")
		{
				
			$strDualAccountSql = "select 
									a.agentID,
									a.dated,
									a.type,
									a.amount,
									a.currency,
									a.actAs,
									t.transID,
									t.refNumberIM,
									t.exchangeRate,
									t.localAmount,
									t.transAmount,
									t.currencyFrom,
									t.currencyTo,
									t.transDate,
									t.fromCountry,
									t.toCountry,
									t.faxNumber,
									b.firstName,
									b.lastName
								from
									agent_Dist_account as a,
									beneficiary as b,
									transactions as t
								where
										(a.agentID = t.custAgentID or a.agentID = t.benAgentID)
									and a.status != 'Provisional'
									and b.benID   = t.benID
									and (a.Dated >= '$fromDate 00:00:00' and a.Dated <= '$toDate 23:59:59')
									and a.agentID = $slctID
									and a.TransID = t.transID	
									and ((t.fromCountry = 'Canada' and t.toCountry = 'United Kingdom') or (t.toCountry = 'Canada' and t.fromCountry = 'United Kingdom'))
									";
						
			$contentsAcc = selectMultiRecords($strDualAccountSql);
		}
		?>	
		  <table width="90%" border="1" cellpadding="0" bordercolor="#666666" align="center">
			<tr>
				<td height="25" nowrap bgcolor="#6699CC">
					<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
						<tr>
							<td align="left">
								<?php if (count($contentsAcc) > 0) {;?>
								Found <b><?php print count($contentsAcc);?></b> Records.
								<?php } ;?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<?
			if(count($contentsAcc) > 0)
			{
			?>
			<tr>
			  <td nowrap bgcolor="#EFEFEF">
				<table width="100%" border="0" bordercolor="#EFEFEF">
					<tr bgcolor="#FFFFFF">
						<td align="center" width="8%"><span class="style1">Date & Refrence no.</span></td>
						<td align="left" width="5%"><span class="style1">Fax Serial Number</span></td>
						<td align="center" width="10%"><span class="style1">Beneficiary Name</span></td>
						<td align="center" width="10%"><span class="style1">Transaction Amount (Canada -> UK)</span></td>
						<td align="center" width="10%"><span class="style1">Local Amount (UK -> Canada)</span></td>
						<td align="center" width="10%"><span class="style1">Opening Balance (CAD)</span></td>
						<td align="center" width="10%"><span class="style1">Exchange Rate (GBP - CAD)</span></td>
						<td align="center" width="10%"><span class="style1">Exchange Rate (CAD - GBP)</span></td>
						<td align="center" width="10%"><span class="style1">Transaction Amount (UK -> Canada)</span></td>
						<td align="center" width="10%"><span class="style1">Local Amount (Canada -> UK)</span></td>
						<td align="center" width="10%"><span class="style1">Opening Balance (GBP)</span></td>
					</tr>
					<? 
					
					$totalTransAmountCtU = 0;
					$totalLocalAmountCtU = 0;
					$totalTransAmountUtC = 0;
					$totalLocalAmountUtC = 0;
										
					$openingBalaceGBP = agentNdistributorBalance($slctID,"GBP",$fromDate);
					$openingBalaceCAD = agentNdistributorBalance($slctID,"CAD",$fromDate);
					
					$runungBalanceCAD = $openingBalaceCAD;
					$runungBalanceGBP = $openingBalaceGBP;
					
					for($i=0;$i < count($contentsAcc);$i++)
					{
						
						if($contentsAcc[$i]["fromCountry"] == "United Kingdom" && $contentsAcc[$i]["toCountry"] == "Canada")
						{
							if($contentsAcc[$i]["type"] == "DEPOSIT" )
							{
								$contentsAcc[$i]["localAmount"] = $contentsAcc[$i]["localAmount"] * (-1);
								$contentsAcc[$i]["transAmount"] = $contentsAcc[$i]["transAmount"] * (-1);
							}
							
							$runungBalanceCAD = $runungBalanceCAD - $contentsAcc[$i]["localAmount"];
							$runungBalanceGBP = $runungBalanceGBP + $contentsAcc[$i]["transAmount"];
							
							$totalTransAmountUtC += $contentsAcc[$i]["transAmount"];
							$totalLocalAmountUtC += $contentsAcc[$i]["localAmount"];
							
							/*
							if($contentsAcc[$i]["type"] == "WITHDRAW")
								echo "===".$contentsAcc[$i]["amount"] = $contentsAcc[$i]["amount"] * (-1);
							
							$runungBalanceCAD = $runungBalanceCAD - $contentsAcc[$i]["localAmount"];
							$runungBalanceGBP = $runungBalanceGBP + $contentsAcc[$i]["transAmount"];
							
							$totalTransAmountUtC += $contentsAcc[$i]["transAmount"];
							$totalLocalAmountUtC += $contentsAcc[$i]["amount"];
							*/
					?>
							<tr class="red-row" onMouseOver="this.className='hover-row'" onMouseOut="this.className='red-row'">
								<td align="center"><?=$contentsAcc[$i]["dated"]?> <br /> <?=$contentsAcc[$i]["refNumberIM"]?></td>
								<td align="center"><?=$contentsAcc[$i]["faxNumber"]?></td>					
								<td align="center"><?=$contentsAcc[$i]["firstName"]." ".$contentsAcc[$i]["lastName"]?></td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($contentsAcc[$i]["localAmount"],2)?> CAD</td>
								<td align="center"><?=round($runungBalanceCAD,2)?> CAD</td>
								<td align="center"><?=round($contentsAcc[$i]["exchangeRate"],2)?></td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($contentsAcc[$i]["transAmount"],2)?> GBP</td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($runungBalanceGBP,2)?> GBP</td>
							</tr>
					<?
						}
						elseif($contentsAcc[$i]["fromCountry"] == "Canada" && $contentsAcc[$i]["toCountry"] == "United Kingdom")
						{
							if($contentsAcc[$i]["type"] == "DEPOSIT" )
							{
								$contentsAcc[$i]["transAmount"] = $contentsAcc[$i]["transAmount"] * (-1);
								$contentsAcc[$i]["localAmount"] = $contentsAcc[$i]["localAmount"] * (-1);
							}					
							
							$runungBalanceCAD = $runungBalanceCAD + $contentsAcc[$i]["transAmount"];
							$runungBalanceGBP = $runungBalanceGBP - $contentsAcc[$i]["localAmount"];
							
							$totalTransAmountCtU += $contentsAcc[$i]["transAmount"];
							$totalLocalAmountCtU += $contentsAcc[$i]["localAmount"];
							
							/*
							if($contentsAcc[$i]["type"] == "WITHDRAW")
								$contentsAcc[$i]["amount"] = $contentsAcc[$i]["amount"] * (-1);
							
							$runungBalanceCAD = $runungBalanceCAD + $contentsAcc[$i]["amount"];
							$runungBalanceGBP = $runungBalanceGBP - $contentsAcc[$i]["localAmount"];
							
							$totalTransAmountCtU += $contentsAcc[$i]["amount"];
							$totalLocalAmountCtU += $contentsAcc[$i]["localAmount"];
							*/
							
					?>
							<tr class="black-row" onMouseOver="this.className='hover-row'" onMouseOut="this.className='black-row'">
								<td align="center"><?=$contentsAcc[$i]["dated"]?> <br /> <?=$contentsAcc[$i]["refNumberIM"]?></td>
								<td align="center"><?=$contentsAcc[$i]["faxNumber"]?></td>					
								<td align="center"><?=$contentsAcc[$i]["firstName"]." ".$contentsAcc[$i]["lastName"]?></td>
								<td align="center"><?=round($contentsAcc[$i]["transAmount"],2)?> CAD</td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($runungBalanceCAD,2)?> CAD</td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($contentsAcc[$i]["exchangeRate"],2)?></td>
								<td align="center">&nbsp;</td>
								<td align="center"><?=round($contentsAcc[$i]["localAmount"],2)?> GBP</td>
								<td align="center"><?=round($runungBalanceGBP,2)?> GBP</td>
							</tr>
					<?
						}
					}
					?>
					<tr bgcolor="#FFFFFF">
						<td colspan="11">&nbsp;</td>
					</tr>
					<tr bgcolor="#0000CC" class="sumup-row">
						<td colspan="3">Total:</td>
						<td align="center"><?=$totalTransAmountCtU?> CAD</td>
						<td align="center"><?=$totalLocalAmountUtC?> CAD</td>
						<td align="center"><?=$runungBalanceCAD?> CAD</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td align="center"><?=$totalTransAmountUtC?> GBP</td>
						<td align="center"><?=$totalLocalAmountCtU?> GBP</td>
						<td align="center"><?=$runungBalanceGBP?> GBP</td>
					</tr>	
					
			</table>		
            <?
			} 
			else
			{
			?>
			<tr>
				<td bgcolor="#FFFFCC" align="center">
					<font color="#FF0000">No data to view for Selected Filter. Select Proper Date and Agent </font>
				</td>
			</tr>
			<?
			}
			?>
       </table>
		<?	
		}
		?>
		</td>
	</tr>
	<? if($slctID!=''){?>
		<tr>
		  	<td> 
				<div align="center">
					<input type="button" name="Submit2" value="Print This Report" onClick="print()">
				</div>
			</td>
		  </tr>
	<? }?>
	</form>
	</td>
	</tr>
</table>
</body>
</html>
