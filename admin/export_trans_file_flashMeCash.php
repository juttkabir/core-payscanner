<?php
session_start();
include ("../include/config.php");
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$agentType = getAgentType();
$currentDate = date('d/m/Y');

$username  = $_SESSION["loggedUserData"]["username"];
$userID  = $_SESSION["loggedUserData"]["userID"];
$date_time = date('d-m-Y-h-i-s-A');

if($_GET["fileID"] != ""){
	
$fileID = $_GET["fileID"];

}elseif($_POST["fileID"] != ""){
	
	$fileID = $_GET["fileID"];
	
}

$condition = "";
$value = "";
if(CONFIG_EXPORT_TRANS_OLD == '1')
{
	if ($_GET['old'] == 'Y') {
		$condition = " and is_exported = 'Y' ";
		$value = "old";
	} else {
		$condition = " and is_exported = '' ";
		$value = "current";
	}
}

//$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");



if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	
	$FileConditionQuery = selectFrom("select conditions from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
		
	$condition .= " and transStatus = 'Authorize' ".$FileConditionQuery["conditions"]." ";
}else{
	
	 $condition .= " and benAgentID = '100268'";
	 $condition .= " and transStatus = 'Authorize'";
	}

 //$condition .= " and transStatus = 'Authorize'";

 $queryCnt = "select COUNT(*) from ". TBL_TRANSACTIONS." where 1 $condition ";
$countTrans = countRecords($queryCnt);

$disticntCurrency = selectMultiRecords ("select DISTINCT(currencyTo) from ".TBL_TRANSACTIONS." where 1 $condition");
$curr = explode(',',$disticntCurrency);




if ($countTrans <= 0) {
	if ($value != "") {
		insertError("There is no ".$value." transaction to export.");
	} else {
		insertError("There is no new transaction to export.");
	}
	redirect("main.php?msg=Y");
}

if (CONFIG_EXPORT_TRANS_OPAL == '1') {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and id = '".$fileID."'");
} else {
	$fileFormatQuery = selectFrom("select * from ".TBL_EXPORT_FILE." where 1 and File_Name = 'export_trans_file_flashMeCash.php'");
}

$lineBreak = $fileFormatQuery["lineBreak"];
$fieldSpacing = $fileFormatQuery["fieldSpacing"];
if ($fieldSpacing == 'tab') {
	$fieldSpace = "\t";	
} else {
	$fieldSpace = " ";
}
$fileFormat = $fileFormatQuery["Format"];
$fileID = $fileFormatQuery["id"];
$fileNamed = $username."_".$date_time."_FM";

	if ($fileFormat == "txt") {
		$appType = "txt";
	} else if ($fileFormat == "csv") {
		$appType = "csv";
	} else {
		$appType = "x-msexcel";	
	}
	
	header ("Content-type: application/$appType");
	header ("Content-Disposition: attachment; filename=$fileNamed.$fileFormat"); 
	header ("Content-Description: PHP/INTERBASE Generated Data");
	
 $query = "select * from ". TBL_TRANSACTIONS." where 1 $condition ";

if ($agentType == "SUPI" || $agentType == "SUBI")
{
	$query .= " and benAgentID = '".$userID."' ";
}
		
$query .= " order by transDate DESC";

$contentTrans = selectMultiRecords($query);

if ($fileFormat == "xls")
{

	
	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	
if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
	$data = "<table width='900' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	$data .="<tr>";
	
	
	
		$data .= "<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["heading"]."</font>\n";	
		
		
			if($labelQuery["address"] == 'Y'){
		
			$data .= "<font face='Verdana' color='#000000' size='2'><b>".COMPANY_ADDR."</font></td>\n";
		}
		
		
		
		$data .="</tr>";
		$data .="<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'>&nbsp;</td></tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label1"]."</font></td>\n</tr>
						<tr><td colspan='4'><font face='Verdana' color='#000000' size='2'><b>".$labelQuery["label2"]." ".$currentDate." ".$labelQuery["label3"]."</font></td>\n</tr>
						<tr><td>&nbsp;</td></tr>
						</table>";
	
		
	}
	$data .= "<table width='900' border='1' cellspacing='0' cellpadding='0' bordercolor='#000000'>"; 
	$data .="<tr>";
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
		$data .= "<td><font face='Verdana' color='#000000' size='2'><b>".$fieldQuery[$j]["Lable"]."</font></td>\n";	
	
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		//$data .="</tr>";
	}
	$data .="</tr>";

	
	for($i=0;$i < count($contentTrans);$i++)
	{
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select * from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select * from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];			
						$accountType = $bankDetails["accountType"];		
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
					}
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
/*	if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= " <tr>
		
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$refNumberIM</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$transDate</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;Express</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$accountName</font></td>";
		
		$data .="
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$customerName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$collectionPointBen</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$beneName</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$foriegnAmount</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$currencyTo</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$IBAN</font></td>
		<td><font color='#000000'  size='1' face='Verdana'>&nbsp;$Remarks</font></td>
		</tr>";
		  	
	
	$data.="<tr>	
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>";
			
			$data.="<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
	</tr>";
}  else {	*/
		//$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		//$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		
	$data .= " <tr>";
		for($k = 0; $k < count($dataVariable); $k ++)
		{
			$value = "";
			$field = explode(',',$dataVariable[$k]); // this it to break a string on any index of the array[array containing database fields]
		
      if($fixedValue[$k] != '')
      {
      	$value .= $fixedValue[$k]; 
      }else{
								

			/*
values for the fields are pulled from the respective tables in the database

*/
					if($dataTable[$k] == 'transactions')
						{
							for($ex = 0; $ex < count($field); $ex++){
								$value .= " ".$contentTrans[$i][$field[$ex]];
								
								if($field[$ex] == "totalAmount")
								{
									$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
									$amount +=  $contentTrans[$i]["totalAmount"];
									$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
								}
								
								}
											
						}elseif($dataTable[$k] == 'beneficiary'){
							for($ex = 0; $ex < count($field); $ex++)
										$value .= " ".$beneContent[$field[$ex]];
										
							}elseif($dataTable[$k] == 'customer'){
							
							for($ex = 0; $ex < count($field); $ex++)
										$value .= " ".$customerContent[$field[$ex]];					
							
						}elseif($dataTable[$k] == 'cm_collection_point'){
							
							for($ex = 0; $ex < count($field); $ex++)
										$value .= " ".$collectionPoint[$field[$ex]];					
							
						}
						if($value == "")
						{
								$value = "None";
							}
				}	
					if($dataVariable[$k] == 'transDate')
					{
						$value = dateFormat($value, 1);
						}	
									$data .="<td><font color='#000000' size='1' face='Verdana'>&nbsp;$value</font></td>";
			}
			$data .="</tr>";
		
	
	//}
	}
	if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME != $fileFormatQuery["FileLable"]){
	
$data .="<tr><td colspan='6'>&nbsp;</td>
					<tr><td><font color='#000000' size='1' face='Verdana'>Total</font></td>";
$z=0;
				foreach($currencyArray as $keys => $values){
											
						$data .="<td>&nbsp;</td>";
					if($z > 0){
				
						$data .="<tr><td colspan='6'>&nbsp;</td></tr>
						<tr><td colspan='6'>&nbsp;</td></tr>
						<tr><td colspan='6'>&nbsp;</td></tr>";
						
						}	
						
						if($z > 0){
				
					$data .="<td><font color='#000000' size='1' face='Verdana'>Total</font></td>
									<td>&nbsp;</td>";
					}
				
			$data .="<td><font color='#000000' size='1' face='Verdana'>'".$values."'</font></td>
			<td><font color='#000000' size='1' face='Verdana'>'".$keys."'</font></td>
			<td colspan='2'>&nbsp;</td>
			</tr>";
	
			
			$z++;
			
			}
		}
	
$data.="</table>";
}
elseif ($fileFormat == "csv")
{

	$labelQuery = selectFrom("select * from ".TBL_EXPORT_LABELS." where 1 and client like '%".CLIENT_NAME.",%' order by `id`");
	
	if($labelQuery["Enable"] == 'Y' && $labelQuery["fileID"]== $fileID){
		
		$data .= $labelQuery["heading"].",";	
		$data .= "\r\n";
		$data .= COMPANY_ADDR;
		$data .= $labelQuery["label1"].",";	
		$data .= "\r\n";
		$data .= $labelQuery["label2"].",".$currentDate. $labelQuery["label3"];	
		$data .= "\r\n";
	
		}

	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
				$data .= $fieldQuery[$j]["Lable"].",";	
	
	
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
		
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		
	}
	$data .= "\r\n";
//	$data .= "\r\n";
	
	
	
	
	for($i=0;$i < count($contentTrans);$i++)
	{
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];
						$accountType = $bankDetails["accountType"];						
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
					}	
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
	/* if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= "$refNumberIM,,$transDate,Express";
		
	  	
		$data .= ",$accountName,$customerName,,,,,,,$collectionPointBen,$beneName,$foriegnAmount,$currencyTo,$IBAN,$Remarks\r\n";
	} else*/ { 	
		
			for($k = 0; $k < count($dataVariable); $k ++)
		{
			$value = "";
			$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
		
      if($fixedValue[$k] != '')
      {
      	$value .= $fixedValue[$k]; 
      }else{
								
/*
values for the fields are pulled from the respective tables in the database

*/
			
					if($dataTable[$k] == 'transactions')
						{
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
									$value .= $contentTrans[$i][$field[$ex]];
									
									if($field[$ex] == "totalAmount")
								{
									$amount = $currencyArray[$contentTrans[$i]["currencyTo"]];
									$amount +=  $contentTrans[$i]["totalAmount"];
									$currencyArray[$contentTrans[$i]["currencyTo"]] = $amount;
								}
							}	
						
						}elseif($dataTable[$k] == 'beneficiary'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
										$value .= $beneContent[$field[$ex]];
							}
						
						}elseif($dataTable[$k] == 'customer'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
										$value .= $customerContent[$field[$ex]];					
							}
						}elseif($dataTable[$k] == 'cm_collection_point'){
														
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
									
										$value .= " ";
									}
										$value .= $collectionPoint[$field[$ex]];					
							}
						}
						if($value == "")
						{
								$value = "None";
							}
				}	
					if($dataVariable[$k] == 'transDate')
					{
						$value = dateFormat($value, 1);
					}
		
		/* values are now displayed here*/
		$data .= "$value,";
			
	}
	
		
	}
	$data .= "\n\r";
	
	
		
	if(CONFIG_LABEL_ON_REPORT == '1' && CONFIG_REPORT_NAME != $fileFormatQuery["FileLable"]){
		
		$z=0;
		$data .= "\n\r";
				foreach($currencyArray as $keys => $values){
				
					$data .= "$values";
					
					}
		
		
		
		}	
	
	}
}
elseif ($fileFormat == "txt")
{
	$fieldQuery = selectMultiRecords("select * from ".TBL_EXPORT_FIELDS." where 1 and fileID = '".$fileID."' order by `id`");
	
	for ($j = 0; $j < count($fieldQuery); $j++)
	{
		
		$data .= $fieldQuery[$j]["Lable"]."$fieldSpace";	
		
		$dataVariable[$j] = $fieldQuery[$j]["payex_field"];
	
	/* this is if any prededined value is to be printed against a respective field*/
	
	if($fieldQuery[$j]["isFixed"]== 'Y'){
		$fixedValue[$j] = $fieldQuery[$j]["Value"];
	}else{
		$fixedValue[$j] = '';
		}
		$dataTable[$j] = $fieldQuery[$j]["tableName"];
		
		
	}
	
	
//	$data .= "$lineBreak\n\r";
	$data .= "\n";
	for($i=0;$i < count($contentTrans);$i++)
	{
		if (CONFIG_EXPORT_TRANS_OLD == '1') {
			update("update transactions set is_exported = 'Y' where transID = '".$contentTrans[$i]["transID"]."'");	
		}
		$refNumberIM = $contentTrans[$i]["refNumberIM"];
		$transDate = $contentTrans[$i]["transDate"];
		$companyCode = $contentTrans[$i]["refNumber"];
		$customerID = $contentTrans[$i]["customerID"];
		$customerName = "";
		$beneName = "";
		if($contentTrans[$i]["createdBy"] == "CUSTOMER")			  	
		{		  
		   $customerContent = selectFrom("select FirstName, LastName, username from cm_customer where c_id ='".$contentTrans[$i]["customerID"]."'");  
		   $beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from cm_beneficiary where benID ='".$contentTrans[$i]["benID"]."'");
		   
		   $customerName = $customerContent["FirstName"]." ".$customerContent["LastName"];
		   $beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		   $accountName = $customerContent["username"];
		   $beneAddress = $beneContent["Address"];
		   $beneCity = $beneContent["City"];
		   $beneState = $beneContent["State"];
		   $benePhone = $beneContent["Phone"];
		   $beneCPF = $beneContent["CPF"];
		}else
			  {
		  	$customerContent = selectFrom("select firstName, lastName, accountName, payinBook from " . TBL_CUSTOMER . " where customerID='".$contentTrans[$i]["customerID"]."'");
		  	$beneContent = selectFrom("select firstName, lastName, Address, City, State, Phone, CPF from " . TBL_BENEFICIARY . " where benID='".$contentTrans[$i]["benID"]."'");
		  	
		  	$customerName = $customerContent["firstName"]." ".$customerContent["lastName"];
		  	$beneName = $beneContent["firstName"]." ".$beneContent["lastName"];
		  	$accountName = $customerContent["accountName"];
		  	$beneAddress = $beneContent["Address"];
		    $beneCity = $beneContent["City"];
		    $beneState = $beneContent["State"];
		    $benePhone = $beneContent["Phone"];
		    $beneCPF = $beneContent["CPF"];
		  }
		$gbpAmount = $contentTrans[$i]["transAmount"];
		$exchRate = $contentTrans[$i]["exchangeRate"];
		$foriegnAmount = $contentTrans[$i]["localAmount"];
		$toCountry = $contentTrans[$i]["toCountry"];
		$currencyFrom = $contentTrans[$i]["currencyFrom"];
		$currencyTo = $contentTrans[$i]["currencyTo"];
		$type = $contentTrans[$i]["transType"];
		$transTotal += $gbpAmount;
		$localTotal += $foriegnAmount;
		$collection = '';
		$bankName = '';						
		$account = '';						
		$brCode = '';						
		$brAddress = '';			
		if($contentTrans[$i]["transType"]=='Pick up')
		{
		$collectionPoint = selectFrom("select cp_corresspondent_name, cp_branch_address, cp_city, cp_state from " . TBL_COLLECTION . " where cp_id='".$contentTrans[$i]["collectionPointID"]."'");
		$collection = $collectionPoint["cp_branch_address"].$collectionPoint["cp_city"].$collectionPoint["cp_state"];
		$collectionPointBen = $collectionPoint["cp_corresspondent_name"];
		}elseif($contentTrans[$i]["transType"]=='Bank Transfer')
			{
				if($contentTrans[$i]["createdBy"] == "CUSTOMER")
					{
						$bankDetails = selectFrom("select * from ".TBL_CM_BANK_DETAILS." where benID = '".$contentTrans[$i]["benID"]."'");
					
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];	
						$accountType = $bankDetails["accountType"];					
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
						}else{
						$bankDetails = selectFrom("select * from ".TBL_BANK_DETAILS." where transID = '".$contentTrans[$i]["transID"]."'");
						
						$bankName = $bankDetails["bankName"];						
						$account = $bankDetails["accNo"];		
						$accountType = $bankDetails["accountType"];				
						$brCode = $bankDetails["branchCode"];						
						$brAddress = $bankDetails["branchAddress"];
						$IBAN = $bankDetails["IBAN"];
						$Remarks = $bankDetails["Remarks"];
					}		
					$bankNumQuery = selectFrom("select bankCode from imbanks where bankName = '".$bankName."'");
					$bankNumber	= $bankNumQuery["bankCode"];				
			}
	if (CONFIG_EXPORT_TRANS_OPAL == '1') {
		$data .= $refNumberIM ."$fieldSpace".$transDate."$fieldSpace"."Express";
		
	  	
		$data .= "$fieldSpace"."$accountName $fieldSpace"."$customerName $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"." $fieldSpace"."$collectionPointBen $fieldSpace"."$beneName $fieldSpace"."$foriegnAmount $fieldSpace"."$currencyTo $fieldSpace"."$IBAN $fieldSpace"."$Remarks $lineBreak\r\n";
	} else {
			for($k = 0; $k < count($dataVariable); $k ++)
		{
			$value = "";
			$field = explode(',',$dataVariable[$k]);// this it to break a string on any index of the array[array containing database fields]
		
      if($fixedValue[$k] != '')
      {
      	$value .= $fixedValue[$k]; 
      }else{
								
/*
values for the fields are pulled from the respective tables in the database

*/
			
					if($dataTable[$k] == 'transactions')
						{
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
									$value .= $contentTrans[$i][$field[$ex]];
									
							}
											
						}elseif($dataTable[$k] == 'beneficiary'){
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
							
										$value .= $beneContent[$field[$ex]];
							}			
							}elseif($dataTable[$k] == 'customer'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
							
										$value .= $customerContent[$field[$ex]];					
							}
						}elseif($dataTable[$k] == 'cm_collection_point'){
							
							for($ex = 0; $ex < count($field); $ex++)
							{
								if($ex > 0){
										$value .= " ";
									}
							
										$value .= $collectionPoint[$field[$ex]];					
							}
						}
						if($value == "")
						{
								$value = "None";
							}
				}	
					if($dataVariable[$k] == 'transDate')
					{
						$value = dateFormat($value, 1);
						}	
		
		/* values are now displayed here*/
		$data .= $value."$fieldSpace";
		
		
	}
	}
	
	$data .= "$lineBreak";
	$data .= "\n";
	}
}
echo $data;

?>