<?
session_start();
include ("../include/config.php");

$date_time = date('d-m-Y  h:i:s A');
include ("security.php");
if(CONFIG_SECURE_ALL_PAGES == '0' )
    include_once ("secureAllPages.php");

$agentType = getAgentType();
$userID  = $_SESSION["loggedUserData"]["userID"];
$agentID = $_SESSION["loggedUserData"]["userID"];
include("connectOtherDataBase.php");
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;

$countOnlineRec = 0;
$limit = CONFIG_MAX_TRANSACTIONS;
if ($offset == "")
    $offset = 0;

if($limit == 0)
    $limit=50;

if ($_GET["newOffset"] != "") {
    $offset = $_GET["newOffset"];
}

$nxt = $offset + $limit;
$prv = $offset - $limit;


$sortBy = $_GET["sortBy"];
if ($sortBy == "")
    $sortBy = " transDate";


$transType   = "";
$transStatus = "";
$Submit      = "";
$transID     = "";
$by          = "";
$moneyPaid   = "";
$fMonth      = "";
$fDay        = "";
$fYear       = "";
$tMonth		= "";
$tDay		= "";
$tYear   	= "";
$fhr         = "";
$fmin        = "";
$fsec        = "";
$thr         = "";
$tmin        = "";
$tsec        = "";
/**
 *  @var $flag type boolean
 *  This varibale is used to check the status ( enable,disable ) of the config
 *  CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS
 */
$flag        = false ;

if($_POST["transType"]!="")
    $transType = $_POST["transType"];
elseif($_GET["transType"]!="")
    $transType=$_GET["transType"];

if($_POST["transStatus"]!="")
    $transStatus = $_POST["transStatus"];
elseif($_GET["transStatus"]!="")
    $transStatus = $_GET["transStatus"];

if($_POST["Submit"]!="")
    $Submit = $_POST["Submit"];
elseif($_GET["Submit"]!="")
    $Submit=$_GET["Submit"];

if($_POST["transID"]!="")
    $transID = trim($_POST["transID"]);
elseif($_GET["transID"]!="")
    $transID = trim($_GET["transID"]);

//echo $transID;

if($_POST["searchBy"]!="")
    $by = $_POST["searchBy"];
elseif($_GET["searchBy"]!="")
    $by = $_GET["searchBy"];

if ($_POST["moneyPaid"] != "") {
    $moneyPaid = $_POST["moneyPaid"];
} else if ($_GET["moneyPaid"] != "") {
    $moneyPaid = $_GET["moneyPaid"];
}


if ($_POST["nameType"] != "") {
    $nameType = $_POST["nameType"];
} else if ($_GET["nT"] != "") {
    $nameType = $_GET["nT"];
}


if ($_POST["agentNameType"] != "") {
    $agentNameType = $_POST["agentNameType"];
} else if ($_GET["agentNameType"] != "") {
    $agentNameType = $_GET["agentNameType"];
}
if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
{
    if($_POST["transSource"]!="")
    {
        $transSource = $_POST["transSource"];

    }
    else if($_GET["transSource"]!="")
    {
        $transSource = $_GET["transSource"];

    }

}

if ($_POST["referenceNumberType"] != "") {
    $referenceNumberType = $_POST["referenceNumberType"];
} else if ($_GET["referenceNumberType"] != "") {
    $referenceNumberType = $_GET["referenceNumberType"];
}

if(!empty($_REQUEST["custCategory"]))
    $custCategory = $_REQUEST["custCategory"];

if(!empty($_REQUEST["customerParentID"]))
    $customerParentID = $_REQUEST["customerParentID"];

if($_POST["fMonth"] != ""){
    $fMonth=$_POST["fMonth"];
}elseif($_GET["fMonth"] != ""){
    $fMonth=$_GET["fMonth"];
}else{
    $fMonth = date("m");

}
if($_POST["fDay"] != ""){
    $fDay=$_POST["fDay"];
}elseif($_GET["fDay"] != ""){
    $fDay=$_GET["fDay"];
}else{
    $fDay = date("d");
}

if($_POST["fYear"] != ""){
    $fYear=$_POST["fYear"];
}elseif($_GET["fYear"] != ""){
    $fYear=$_GET["fYear"];
}else{
    $fYear = date("Y");
}

if($_POST["tMonth"] != ""){
    $tMonth=$_POST["tMonth"];
}elseif($_GET["tMonth"] != ""){
    $tMonth=$_GET["tMonth"];
}else{
    $tMonth = date("m");
}

if($_POST["tDay"] != ""){
    $tDay=$_POST["tDay"];
}elseif($_GET["tDay"] != ""){
    $tDay=$_GET["tDay"];
}else{
    $tDay = date("d");
}

if($_POST["tYear"] != ""){
    $tYear=$_POST["tYear"];
}elseif($_GET["tYear"] != ""){
    $tYear=$_GET["tYear"];
}else{
    $tYear = date("Y");

}

if($_POST["fhr"] != ""){
    $fhr=$_POST["fhr"];
}elseif($_GET["fhr"] != ""){
    $fhr=$_GET["fhr"];
}else{
    $fhr = "00";

}
if($_POST["fmin"] != ""){
    $fmin=$_POST["fmin"];
}elseif($_GET["fmin"] != ""){
    $fmin=$_GET["fmin"];
}else{
    $fmin = "00";
}

if($_POST["fsec"] != ""){
    $fsec=$_POST["fsec"];
}elseif($_GET["fsec"] != ""){
    $fsec=$_GET["fsec"];
}else{
    $fsec = "00";
}

if($_POST["thr"] != ""){
    $thr=$_POST["thr"];
}elseif($_GET["thr"] != ""){
    $thr=$_GET["thr"];
}else{
    $thr = "23";
}

if($_POST["tmin"] != ""){
    $tmin=$_POST["tmin"];
}elseif($_GET["tmin"] != ""){
    $tmin=$_GET["tmin"];
}else{
    $tmin = "59";
}

if($_POST["tsec"] != ""){
    $tsec=$_POST["tsec"];
}elseif($_GET["tsec"] != ""){
    $tsec=$_GET["tsec"];
}else{
    $tsec = "59";
}

$fromDate = $fYear . "-" . $fMonth . "-" . $fDay;
$toDate = $tYear . "-" . $tMonth . "-" . $tDay;

$fromTime = $fhr . ":" . $fmin . ":" . $fsec;
$toTime = $thr . ":" . $tmin . ":" . $tsec;

$acManagerFlagLabel = "Account Manager";
$acManagerFlag = false;
if(CONFIG_POPULATE_USERS_DROPDOWN=="1" && defined("CONFIG_ACCOUNT_MANAGER_COLUMN") && CONFIG_ACCOUNT_MANAGER_COLUMN!="0"){
    $custExtraFields = ", c.parentID as custParentID, am.name as accountManagerName ";
    $acManagerFlagLabel = CONFIG_ACCOUNT_MANAGER_COLUMN;
    $extraJoin = " LEFT JOIN ".TBL_ADMIN_USERS." as am ON am.userID = c.parentID  ";
    $acManagerFlag = true;
}

$chequeAmountField = "";
if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
{
    $chequeAmountField = "chequeAmount,";
}

$query = "select t.benID,transID,transDate,totalAmount,custAgentID," . $chequeAmountField . " t.benAgentID,transType,transAmount,localAmount,refNumber,refNumberIM,transStatus,t.customerID,t.collectionPointID,createdBy,moneyPaid, clientRef,t.custDocumentProvided,t.trans_source ".$custExtraFields."
		from ".
    TBL_TRANSACTIONS . " as t
			LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
    $extraJoin."  
			WHERE 1 ";
$queryCnt = "select count(*) from ".
    TBL_TRANSACTIONS . " as t
				LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
    $extraJoin."   
				WHERE 1 ";

if(CONFIG_CUSTOMER_CATEGORY == "1" && !empty($_POST["custCategory"]))
{
    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,totalAmount,moneyPaid," . $chequeAmountField . " custAgentID,transType,transAmount,localAmount,transStatus,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields."
		from 
			". TBL_TRANSACTIONS . " as t
			LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
        $extraJoin."  
			where t.createdBy != 'CUSTOMER' ";
    $queryCnt = "select count(*) 
		from 
			". TBL_TRANSACTIONS . " as t
			LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
        $extraJoin."  
			where t.createdBy != 'CUSTOMER' ";

    $queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
    $queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

    $query .= " and (c.cust_category_id='".$custCategory."')";
    $queryCnt .= " and (c.cust_category_id='".$custCategory."')";
}

//echo("Variables are by=$by,,,transID=$transID,,,,Submit=$Submit,,,transStatus=$transStatus,,,transType=$transType");

$createdByLabel = "Created By";
$createdByN = $_arrLabels[$createdByLabel];
if(!empty($createdByN))
    $createdByLabel = $createdByN;

if($Submit == "Search")
{


    if($transID != "" && $by!="")
    {


        switch($by)
        {
            case 0:
                {
                    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,custAgentID,totalAmount," . $chequeAmountField . " transType,transAmount,moneyPaid,localAmount,transStatus,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields." 
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
					where 1 ";
                    $queryCnt = "select count(*) 
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
					where 1 ";

                    if(CONFIG_OPTIMIZE_REFERENCE_NO == "1"){

                        if($referenceNumberType == 'referenceNumber'){

                            $query .= " and (t.refNumber = '".$transID."' ) ";
                            $queryCnt .= " and (t.refNumber = '".$transID."') ";


                        }elseif($referenceNumberType == 'referenceNumberIM'){

                            $query .= " and (t.refNumberIM = '".$transID."') ";
                            $queryCnt .= " and (t.refNumberIM = '".$transID."') ";


                        }

                    }else{
                        $query .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
                        $queryCnt .= " and (t.refNumber = '".$transID."' OR t.refNumberIM = '".$transID."') ";
                    }


                    break;
                }
            case 1:
                {
                    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,totalAmount," . $chequeAmountField . " custAgentID,transType,moneyPaid,transAmount,localAmount,transStatus,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields."
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
						where t.createdBy != 'CUSTOMER' ";
                    $queryCnt = "select count(*) 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
						where t.createdBy != 'CUSTOMER' ";

                    $queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
                    $queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

                    if(CONFIG_CUSTOMER_CATEGORY == '1' && !empty($custCategory)){
                        $query .= " and (c.cust_category_id='".$custCategory."')";
                        $queryCnt .= " and (c.cust_category_id='".$custCategory."')";
                    }
                    if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){


                        if($nameType == "fullName"){
                            $name = split(" ",$transID);
                            $nameClause = "c.firstName LIKE '$name[0]' and c.lastName LIKE  '$name[1]' ";
                            $nameClauseOn = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";
                        }else{
                            $nameClause = " c.".$nameType." LIKE '".$transID."%'";
                            $nameClauseOn = " ".$nameType." LIKE '".$transID."%'";
                        }

                        $query .= " and $nameClause ";
                        $queryCnt .= " and $nameClause ";

                        $queryonline .= " and $nameClauseOn ";
                        $queryonlineCnt .= " and $nameClauseOn ";


                    }else{
                        // searchName function start (Niaz)
                        $fn="FirstName";
                        $mn="MiddleName";
                        $ln="LastName";
                        $alis="cm";
                        $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
                        $queryonline .=$q;
                        $queryonlineCnt.=$q;
                        // searchName function end (Niaz)

                        //$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
                        //	$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";

                        if($agentType == "Branch Manager"){
                            $queryonline .= " and t.custAgentParentID ='$agentID' ";
                            $queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
                        }
                        // searchNameMultiTables function start (Niaz)
                        $fn="firstName";
                        $mn="middleName";
                        $ln="lastName";
                        $alis="c";
                        $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
                        $query .=$q;
                        $queryCnt .=$q;
                        // searchNameMultiTables function end (Niaz)




                        //	$query .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
                        //$queryCnt .= "  and (c.firstName like '".$transID."%' OR c.lastName like '".$transID."%')";
                    }
                    break;
                }
            case 2:
                {

                    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,transStatus,custAgentID,transType,totalAmount," . $chequeAmountField . " transAmount,localAmount,moneyPaid,t.customerID,b.benID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields."
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
                        $extraJoin."  
					where 1 ";
                    $queryCnt = "select count(*) 
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
					LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
                        $extraJoin."  
					where 1 ";

                    $queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy = 'CUSTOMER' ";
                    $queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_BENEFICIARY ." as b where t.benID = b.benID and t.createdBy = 'CUSTOMER' ";

                    if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){


                        if($nameType == "fullName"){

                            $name = split(" ",$transID);
                            $nameClause = "b.firstName LIKE '$name[0]' and b.lastName LIKE  '$name[1]' ";
                            $nameClauseOn = "firstName LIKE '$name[0]' and lastName LIKE  '$name[1]' ";

                        }else{

                            $nameClause = " b.".$nameType." LIKE '".$transID."%'";
                            $nameClauseOn = " ".$nameType." LIKE '".$transID."%'";

                        }


                        $query .= " and $nameClause ";
                        $queryCnt .= " and $nameClause ";

                        $queryonline .= " and $nameClauseOn ";
                        $queryonlineCnt .= " and $nameClauseOn ";


                    }else{


                        // searchNameMultiTables function start (Niaz)
                        $fn="firstName";
                        $mn="middleName";
                        $ln="lastName";
                        $alis="b";
                        $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
                        $queryonline .=$q;
                        $queryonlineCnt .=$q;
                        // searchNameMultiTables function end (Niaz)


                        //$queryonline .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";
                        //$queryonlineCnt .= " and (cb.firstName like '".$transID."%' OR cb.lastName like '".$transID."%')";

                        if($agentType == "Branch Manager"){
                            $queryonline .= " and t.custAgentParentID ='$agentID' ";
                            $queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
                        }
                        // searchNameMultiTables function start (Niaz)
                        $fn="firstName";
                        $mn="middleName";
                        $ln="lastName";
                        $alis="b";
                        $q=searchNameMultiTables($transID,$fn,$mn,$ln,$alis);
                        $query .=$q;
                        $queryCnt .=$q;
                        // searchNameMultiTables function end (Niaz)
                        //$query .= " and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
                        //$queryCnt .=" and (b.firstName like '".$transID."%' OR b.lastName like '".$transID."%')";
                    }
                    break;
                }
            case 3:
                {
                    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,transStatus,custAgentID,transType,totalAmount," . $chequeAmountField . " transAmount,localAmount,moneyPaid,totalAmount,transAmount,localAmount,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields."
				from 
					". TBL_TRANSACTIONS . " as t
					LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
					where 1 ";
                    $queryCnt = "select count(*) 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID ".
                        $extraJoin."  
						where 1 ";

                    $queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
                    $queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

                    $queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
                    $queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";

                    if($agentType == "Branch Manager"){
                        $queryonline .= " and t.custAgentParentID ='$agentID' ";
                        $queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
                    }

                    $query .= "  and (c.accountName like '".$transID."%')";
                    $queryCnt .= "  and (c.accountName like '".$transID."%')";
                    break;
                }
            case 4:
                {
                    $query = "select t.benID,transID,transDate,refNumber,refNumberIM,transStatus,custAgentID,transType,totalAmount," . $chequeAmountField . " transAmount,moneyPaid,localAmount,t.customerID,t.collectionPointID,createdBy,t.custDocumentProvided,t.trans_source ".$custExtraFields." 
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
                        $extraJoin."  
						where t.createdBy != 'CUSTOMER'  ";
                    $queryCnt = "select count(*)  
					from 
						". TBL_TRANSACTIONS . " as t
						LEFT JOIN ". TBL_CUSTOMER ." as c ON t.customerID = c.customerID 
						LEFT JOIN ". TBL_ADMIN_USERS ." as a ON t.custAgentID = a.userID 
						LEFT JOIN ". TBL_BENEFICIARY ." as b ON t.benID = b.benID ".
                        $extraJoin."  
						where t.createdBy != 'CUSTOMER'  ";

                    $queryonline = "select * from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";
                    $queryonlineCnt = "select count(*) from ". TBL_TRANSACTIONS . " as t, ". TBL_CM_CUSTOMER ." as cm where t.customerID =  cm.c_id and t.createdBy = 'CUSTOMER' ";

                    //$queryonline .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";
                    //$queryonlineCnt .= " and (cm.FirstName like '".$transID."%' OR cm.LastName like '".$transID."%')";

                    if($agentType == "Branch Manager"){
                        $queryonline .= " and t.custAgentParentID ='$agentID' ";
                        $queryonlineCnt .= " and t.custAgentParentID ='$agentID' ";
                    }

                    if(CONFIG_OPTIMISE_AGENT_SEARCH == "1"){

                        if($agentNameType == 'agentName'){

                            $query .= "  and (a.name like '".$transID."%')";
                            $queryCnt .= "  and (a.name like '".$transID."%' )";


                        }elseif($agentNameType == 'agentCode' ){

                            $query .= "  and ( a.username like '".$transID."%')";
                            $queryCnt .= "  and ( a.username like '".$transID."%')";

                        }



                    }else{

                        $query .= "  and (a.name like '".$transID."%' or a.username like '".$transID."%')";
                        $queryCnt .= "  and (a.name like '".$transID."%' or a.username like '".$transID."%')";


                    }
                    break;
                }
        }
    }
}
if($transType != "")
{
    if($_POST["searchBy"] != 0 || $_POST["searchBy"] == ""){
        $query .= " and (t.transType='".$transType."')";
        $queryCnt .= " and (t.transType='".$transType."')";
    }
    if($transID != "" && $by != 0)
    {
        $queryonline .= " and (t.transType='".$transType."')";
        $queryonlineCnt .= " and (t.transType='".$transType."')";
    }

}
if($transStatus != "")
{
    if($by != 0 || $by == ""){
        $query .= " and (t.transStatus='".$transStatus."')";
        $queryCnt .= " and (t.transStatus='".$transStatus."')";
    }
    if($transID != "" && $by != 0)
    {
        $queryonline .= " and (t.transStatus='".$transStatus."')";
        $queryonlineCnt .= " and (t.transStatus='".$transStatus."')";
    }

}
if ($moneyPaid != "") {

    $query .= " and (t.moneyPaid='".$moneyPaid."')";
    $queryCnt .= " and (t.moneyPaid='".$moneyPaid."')";

    $queryonline .= " and (t.moneyPaid='".$moneyPaid."')";
    $queryonlineCnt .= " and (t.moneyPaid='".$moneyPaid."')";

}
if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
{
    if($transSource!= "")
    {
        if($transSource == "O")
        {
            $query.= " and t.trans_source = 'O' ";
            $queryCnt .= " and t.trans_source = 'O' ";
        }
        else if ($transSource == "P")
        {
            $query.= " and trans_source != 'O' ";
            $queryCnt .= " and trans_source != 'O' ";
        }


    }
}
/**
 *  #9923: Premier Exchange: Admin Manager View Transaction Enhancements
 *  This condition executes when admin manager or branch manager logs in,
 *  keeping the check of showing the logged in users only the transactions of
 *  their associated senders.
 */
if( defined("CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS") && strstr(CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS,$agentType) )
{
    /**
     *  @var $flag type boolean
     *  This varibale is used to check the status ( enable,disable ) of the config
     *  CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS
     */
    $flag      = true;
    /**
     *  Adding the clause in queries
     */
    $query    .= " and ( am.userID = '".$agentID."' )";
    $queryCnt .= " and ( am.userID = '".$agentID."' )";
}
/*switch ($agentType)
{
	case "SUPA":
	case "SUPAI":
	case "SUBA":
	case "SUBAI":
		$query .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	if($transID != "")
		{
		$queryonline .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
		$queryonlineCnt .= " and (t.custAgentID = '".$_SESSION["loggedUserData"]["userID"]."' OR t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."')";
	}
}*/


if($agentType == "SUPA" || $agentType == "SUPAI"){

    $query .= " and t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."'";
    $queryCnt .= " and t.custAgentID ='".$_SESSION["loggedUserData"]["userID"]."'";

}elseif($agentType == "SUBA" || $agentType == "SUBAI"){

    $query .= " and t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."'";
    $queryCnt .= " and t.custAgentParentID ='".$_SESSION["loggedUserData"]["userID"]."'";


}elseif($agentType == "Branch Manager"){

    $query .= " and t.custAgentParentID ='$agentID' ";
    $queryCnt .= " and t.custAgentParentID ='$agentID' ";
}


if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == '1' && ($agentType == "SUPI Manager" )){




    $linkedDistQuery = selectFrom("select linked_Distibutor from admin where userID = '".$agentID."'");

    $linkedDistArray = explode("," , $linkedDistQuery["linked_Distibutor"]);



    for($i = 0; $i < count($linkedDistArray); $i++){


        $distIDArrayQuery = selectFrom("select userID from admin where username = '".$linkedDistArray[$i]."'");
        $distIDArray[$i] = $distIDArrayQuery["userID"];
        $distIDArray[$i] = "'".$distIDArray[$i]."'";
    }

    $linkedDist = implode("," , $distIDArray );

    //echo count($distIDArray);
    if(count($distIDArray) > 0){

        $query .= " and (";
        $queryCnt .= " and ( ";

        for($j = 0; $j < count($distIDArray); $j++){


            if($j> 0){

                $query .= " or ";
                $queryCnt .= " or ";


            }

            $query .= "  t.benAgentID = (".$distIDArray[$j].") ";
            $queryCnt .= "  t.benAgentID = (".$distIDArray[$j].") ";

        }

        $query .= ")";
        $queryCnt .= ")";
    }



}


if( CONFIG_VIEW_ASSOCIATED_AGENT_TRANS == 1 )
{
    $agentLinkQuery = selectFrom("select linked_Agent from ".TBL_ADMIN_USERS." where userID = '".$agentID."'");
    $linkedAgentArray = explode(",", $agentLinkQuery["linked_Agent"]);
    for($i = 0; $i < count($linkedAgentArray); $i++){
        if(!empty($linkedAgentArray[$i]))
        {
            $agentIDArrayQuery = selectFrom("select userID from admin where username = '".$linkedAgentArray[$i]."'");
            $agentIDArray[$i] = $agentIDArrayQuery["userID"];
            $agentIDArray[$i] = "'".$agentIDArray[$i]."'";
        }
    }

    if(count($agentIDArray) > 0){

        $query .= " and (";
        $queryCnt .= " and ( ";
        for($j = 0; $j < count($agentIDArray); $j++){

            if($j> 0){

                $query .= " or ";
                $queryCnt .= " or ";
            }

            $query .= "  t.custAgentID = (".$agentIDArray[$j].") ";
            $queryCnt .= "  t.custAgentID = (".$agentIDArray[$j].") ";
        }

        $query .= ")";
        $queryCnt .= ")";
    }

}
if(!empty($customerParentID)){
    $query .= " and (c.parentID='".$customerParentID."')";
    $queryCnt .= " and (c.parentID='".$customerParentID."')";
}
if(CONFIG_TIME_FILTER == '1'){
    if($by != 0 || $by == ""){
        //$queryDate = "(t.transDate >= '$fromDate $fromTime' and t.transDate <= '$toDate $toTime')";
        $queryDate = "(t.transDate BETWEEN '$fromDate $fromTime' AND '$toDate $toTime')";
        $query .=  " and $queryDate";
        $queryCnt .=  " and $queryDate";

        $queryonline .= " and (transDate >= '$fromDate $fromTime' and transDate <= '$toDate $toTime')";
        $queryonline.=  " and $queryDate";

    }
}

if(SYSTEM=="Premier FX")
{
    if($by != 0 || $by == ""){
        $query .= "order by (t.refNumberIM*t.transDate) Desc";
    }
}else
{
    if($by != 0 || $by == ""){
        $query .= " order by t.transDate DESC";
    }


}

if($by == "" && $transID != ""){

    $errorMessage="Select Proper filters to Search";


}else{


//$queryCnt = "select count(*) from ". TBL_TRANSACTIONS."";
    $query .= " LIMIT $offset , $limit";
    //debug($query);
    $contentsTrans = selectMultiRecords($query);

    $allCount = countRecords($queryCnt);
}


if($transID != "" && $by != 0)
{

    $onlinecustomerCount = countRecords($queryonlineCnt );

    $allCount = $allCount + $onlinecustomerCount;

    $other = $limit;
    if($other > count($contentsTrans))
    {

        if($offset < count($contentsTrans))
        {
            $offset2 = 0;
            $limit2 = $offset + $limit - count($contentsTrans);
        }elseif($offset >= count($contentsTrans))
        {
            $offset2 = $offset - $countOnlineRec;
            $limit2 = $limit;
        }
        $queryonline .= " order by t.transDate DESC";
        $queryonline .= " LIMIT $offset2 , $limit2";
        $onlinecustomer = selectMultiRecords($queryonline);
    }

}

?>
<html>
<head>
    <title>View Transactions</title>
    <script language="javascript" src="./javascript/functions.js"></script>
    <script language="javascript" src="./javascript/datetimepicker.js"></script>
    <link href="images/interface.css" rel="stylesheet" type="text/css">
    <script language="javascript">

        <!--
        function SelectOption(OptionListName, ListVal)
        {
            for (i=0; i < OptionListName.length; i++)
            {
                if (OptionListName.options[i].value == ListVal)
                {
                    OptionListName.selectedIndex = i;
                    break;
                }
            }
        }

        function checkForm()
        {

            if(document.forms[0].searchBy.value != "" && document.forms[0].transID.value == "")
            {
                alert("Please put some value to be searched.");
                document.forms[0].transID.focus();
                return false;
            }
        }
        // end of javascript -->
    </script>
    <script language="JavaScript">
        <!--
        function CheckAll()
        {

            var m = document.trans;
            var len = m.elements.length;
            if (document.trans.All.checked==true)
            {
                for (var i = 0; i < len; i++)
                {
                    m.elements[i].checked = true;
                }
            }
            else{
                for (var i = 0; i < len; i++)
                {
                    m.elements[i].checked=false;
                }
            }
        }
        -->
    </script>

    <script language="JavaScript">
        <!--

        function nameTypeCheck(){

            if(document.getElementById('searchBy').value == '1' || document.getElementById('searchBy').value == '2'){
                document.getElementById('nameTypeRow').style.display = '';
            }else{
                document.getElementById('nameTypeRow').style.display = 'none';
            }
            if(document.getElementById('searchBy').value == '4'){
                document.getElementById('agentTypeRow').style.display = '';
            }else{
                document.getElementById('agentTypeRow').style.display = 'none';
            }
            if(document.getElementById('searchBy').value == '0'){
                document.getElementById('referenceTypeRow').style.display = '';
            }else{
                document.getElementById('referenceTypeRow').style.display = 'none';
            }
        }

        -->
    </script>

    <style type="text/css">
        <!--
        .style1 {color: #005b90}
        .style2 {color: #005b90; font-weight: bold; }
        -->
    </style>
</head>
<body onLoad="nameTypeCheck();">
<script type="text/javascript" src="wz_tooltip.js"></script>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
    <tr>
        <td bgcolor="#C0C0C0"><strong><font color="#FFFFFF" size="2">View Transactions</font></strong></td>
    </tr>

    <tr>
        <td align="center"><br>
            <table border="1" cellpadding="5" bordercolor="#666666">
                <form action="view-transactions.php" method="post" name="Search" onSubmit="return checkForm();">
                    <tr>
                        <td nowrap bgcolor="C0C0C0" colspan="2"><span class="tab-u"><strong>Search</strong></span></td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2"> Search Transactions
                            <input name="transID" type="text" id="transID" value=<?=$transID?>>
                            <select name="transType" style="font-family:verdana; font-size: 11px; width:100">
                                <option value=""> - Type - </option>
                                <option value="Pick up">Pick up</option>
                                <option value="Bank Transfer">Bank Transfer</option>
                                <option value="Home Delivery">Home Delivery</option>
                            </select><script language="JavaScript">SelectOption(document.forms[0].transType, "<?=$transType?>");</script>
                            <select name="transStatus" style="font-family:verdana; font-size: 11px; width:100">
                                <option value=""> - Status - </option>

                                <option value="Pending">Pending</option>
                                <option value="Processing">Verify</option>
                                <option value="Recalled">Recalled</option>
                                <option value="Rejected">Rejected</option>
                                <option value="Suspended">Suspended</option>
                                <option value="amended">Amended</option>
                                <option value="Cancelled">Cancelled</option>
                                <? if(CONFIG_RETURN_CANCEL == "1"){ ?>
                                    <option value="Cancelled - Returned">Cancelled - Returned</option>
                                <? } ?>
                                <option value="AwaitingCancellation">Awaiting Cancellation</option>
                                <option value="Authorize">Authorized</option>
                                <option value="Failed">Undelivered</option>
                                <option value="Delivered">Delivered</option>
                                <option value="Out for Delivery">Out for Delivery</option>
                                <option value="Picked up">Picked up</option>
                                <option value="Credited">Credited</option>
                            </select>
                            <script language="JavaScript">SelectOption(document.forms[0].transStatus, "<?=$transStatus?>");
                            </script>
                            <br>
                            <select name="searchBy" id="searchBy" onChange="nameTypeCheck();">
                                <option value=""> - Search By - </option>
                                <option value="0" <? echo ($_POST["searchBy"] == 0 ? "selected" : "")?>>By Reference No/<?=$manualCode?></option>
                                <option value="1" <? echo ($_POST["searchBy"] == 1 ? "selected" : "")?>>By <?=__("Sender");?> Name</option>
                                <option value="2" <? echo ($_POST["searchBy"] == 2 ? "selected" : "")?>>By Beneficiary Name</option>
                                <option value="3" <? echo ($_POST["searchBy"] == 3 ? "selected" : "")?>>By Payin Book Number</option>
                                <option value="4" <? echo ($_POST["searchBy"] == 4 ? "selected" : "")?>>By <?=__("Agent");?> Name/Code</option>
                            </select>



                            <script language="JavaScript">SelectOption(document.forms[0].searchBy, "<?=$by?>");
                            </script>
                            <?	if (CONFIG_PAYMENT_MODE_FILTER == "1") {  ?>
                                <select name="moneyPaid" id="moneyPaid" style="font-family:verdana; font-size: 11px;">
                                    <option value="">-Select Payment Mode-</option>
                                    <? if(CONFIG_MONEY_PAID_OPTIONS == "1"){ ?>
                                        <option value="By Cash HCD">By Cash HCD</option>
                                        <option value="By Cash HBT">By Cash HBT</option>
                                        <option value="By Cash HQD">By Cash HQD</option>
                                        <option value="By Cash BCD">By Cash BCD</option>
                                        <option value="By Cash BBT">By Cash BBT</option>
                                        <option value="By Cash BQD">By Cash BQD</option>
                                        <option value="By Cash WICC">By Cash WICC</option>
                                    <? } ?>
                                    <option value="By Cash">By Cash</option>
                                    <option value="By Cheque">By Cheque</option>
                                    <option value="By Bank Transfer">By Bank Transfer</option>
                                    <option value="By Debit Card">By Debit card</option>
                                </select>
                                <script language="JavaScript">
                                    SelectOption(document.forms[0].moneyPaid, "<?=$moneyPaid; ?>");
                                </script>
                            <?	}  ?>
                        </td></tr>

                    <? if(CONFIG_OPTIMISE_NAME_SEARCH == "1"){ ?>

                        <tr align="center" id="nameTypeRow"><td>
                                Name Type: <select name="nameType">
                                    <option value="firstName" <? echo ($nameType == "firstName" ? "selected" : "")?>>First Name</option>
                                    <option value="lastName" <? echo ($nameType == "lastName" ? "selected" : "")?>>Last Name</option>
                                    <option value="fullName" <? echo ($nameType == "fullName" ? "selected" : "")?>>First Name & Last Name</option>
                            </td></tr>
                    <? } ?>





                    <? if(CONFIG_OPTIMISE_AGENT_SEARCH == "1"){ ?>

                        <tr align="center" id="agentTypeRow"><td>
                                Name Type: <select name="agentNameType">
                                    <option value="agentName" <? echo ($agentNameType == "agentName" ? "selected" : "")?>><?=__("Agent");?> Name</option>
                                    <option value="agentCode" <? echo ($agentNameType == "agentCode" ? "selected" : "")?>><?=__("Agent");?> Code</option>


                            </td></tr>
                    <? } ?>

                    <? if(CONFIG_OPTIMIZE_REFERENCE_NO == "1"){ ?>

                        <tr align="center" id="referenceTypeRow"><td>
                                Name Type: <select name="referenceNumberType">
                                    <option value="referenceNumber" <? echo ($referenceNumberType == "referenceNumber" ? "selected" : "")?>><?=$manualCode?></option>
                                    <option value="referenceNumberIM" <? echo ($referenceNumberType == "referenceNumberIM" ? "selected" : "")?>>Reference Number</option>


                            </td></tr>
                    <? } ?>

                    <?
                    /**
                     *  This condition executes when CONFIG_SHOW_ASSOCIATED_SENDER_TRANSACTIONS is
                     *  not defined,disabled or the logged in user is not the one defined in this config
                     */


                    //debug(CONFIG_POPULATE_USERS_DROPDOWN_SENDER);
                    if( $flag == false )
                    {
                        if(CONFIG_CUSTOMER_CATEGORY == "1" || CONFIG_POPULATE_USERS_DROPDOWN_SENDER=="1"){?>
                            <tr align="center" id="senderType"><td>
                        <?php
                        //work  by Mudassar Ticket #11425
                        if(CONFIG_AGENT_WITH_ACTIVE_STATUS==1){
                            $agentStatusQuery = selectFrom("select agentStatus from ".TBL_ADMIN_USERS." where agentStatus='Active' ");
                            $arguments = array("flag"=>"populateUsers","selectedID"=>$customerParentID,"hideSelectMain"=>true,"agentStatusQuery"=>$agentStatusQuery);
                        }

                        else if (CONFIG_AGENT_WITH_ALL_STATUS==1){

                            $arguments = array("flag"=>"populateUsers","selectedID"=>$customerParentID,"hideSelectMain"=>true);
                        }

                        //work end by Mudassar Ticket #11425

                        $populateUser = gateway("CONFIG_POPULATE_USERS_DROPDOWN_SENDER",$arguments,CONFIG_POPULATE_USERS_DROPDOWN_SENDER);

                        //debug($populateUser);
                        if(!empty($populateUser)){
                        ?>
                            <select name="customerParentID" id="customerParentID" style="font-family:verdana; font-size: 11px">
                                <option value="">-- Select <?=$acManagerFlagLabel?> --</option>
                                <?php echo $populateUser;?>
                            </select>&nbsp;&nbsp;
                        <? }
                        //debug($populateUser);
                        //work end by Mudassar Ticket #11425

                        ?>

                        <? if(CONFIG_SHOW_TRANSACTION_SOURCE == "1")
                        {

                        ?>
                            <select style="font-family:verdana; font-size: 11px" name="transSource">
                                <option value=""> - Transaction Source - </option>
                                <option value="O">Online</option>
                                <option value="P">Payex</option>


                            </select>
                            <script language="JavaScript">SelectOption(document.forms[0].transSource, "<?=$transSource?>");</script>
                        <? } ?>
                        <?php
                        if(CONFIG_CUSTOMER_CATEGORY == "1"){?>
                            <select name="custCategory" id="custCategory" style="font-family:verdana; font-size: 11px">
                                <option value="">-Select Customer Type-</option>
                                <?
                                $custCatSql = "select id,name from ".TBL_CUSTOMER_CATEGORY." where enabled != 'N'";
                                $category = selectMultiRecords($custCatSql);
                                for($k =0; $k <count($category); $k++){
                                    ?>
                                    <option value="<?=$category[$k]["id"]; ?>" <? echo ($custCategory == $category[$k]["id"] ? "selected" : "")?>>
                                        <?  echo($category[$k]["name"]); ?></option>
                                <?  }
                                }?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.forms[0].custCategory, "<?=$custCategory ; ?>");
                            </script>
                        <?php }?>
                        </td></tr>
                    <? } ?>
                    <?
                    if(CONFIG_TIME_FILTER == '1'){

                    ?>
                    <tr>
                        <td align="center" nowrap colspan="2">
                            Date &nbsp;<b>From</b>
                            <?
                            $month = date("m");

                            $day = date("d");
                            $year = date("Y");
                            ?>

                            <SELECT name="fDay" size="1" id="fDay" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($Day=1;$Day<32;$Day++)
                                {
                                    if ($Day<10)
                                        $Day="0".$Day;
                                    echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.fDay, "<?=$fDay; ?>");
                            </script>
                            <SELECT name="fMonth" size="1" id="fMonth" style="font-family:verdana; font-size: 11px">
                                <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                                <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                                <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                                <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                                <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                                <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                                <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                                <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                                <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                                <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                                <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                                <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.Search.fMonth, "<?=$fMonth; ?>");
                            </script>
                            <SELECT name="fYear" size="1" id="fYear" style="font-family:verdana; font-size: 11px">

                                <?
                                $cYear=date("Y");
                                for ($Year=2004;$Year<=$cYear;$Year++)
                                {
                                    echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
                                }
                                ?>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.Search.fYear, "<?=$fYear; ?>");
                            </script>
                            <b>To</b>
                            <SELECT name="tDay" size="1" id="tDay" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($Day=1;$Day<32;$Day++)
                                {
                                    if ($Day<10)
                                        $Day="0".$Day;
                                    echo "<option value='$Day'" .  ($day == $Day ? "selected" : "") . ">$Day</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.tDay, "<?=$tDay; ?>");
                            </script>
                            <SELECT name="tMonth" size="1" id="tMonth" style="font-family:verdana; font-size: 11px">

                                <OPTION value="01" <? echo ($month =="01" ? "selected" : "")?>>Jan</OPTION>
                                <OPTION value="02" <? echo ($month =="02" ? "selected" : "")?>>Feb</OPTION>
                                <OPTION value="03" <? echo ($month =="03" ? "selected" : "")?>>Mar</OPTION>
                                <OPTION value="04" <? echo ($month =="04" ? "selected" : "")?>>Apr</OPTION>
                                <OPTION value="05" <? echo ($month =="05" ? "selected" : "")?>>May</OPTION>
                                <OPTION value="06" <? echo ($month =="06" ? "selected" : "")?>>Jun</OPTION>
                                <OPTION value="07" <? echo ($month =="07" ? "selected" : "")?>>Jul</OPTION>
                                <OPTION value="08" <? echo ($month =="08" ? "selected" : "")?>>Aug</OPTION>
                                <OPTION value="09" <? echo ($month =="09" ? "selected" : "")?>>Sep</OPTION>
                                <OPTION value="10" <? echo ($month =="10" ? "selected" : "")?>>Oct</OPTION>
                                <OPTION value="11" <? echo ($month =="11" ? "selected" : "")?>>Nov</OPTION>
                                <OPTION value="12" <? echo ($month =="12" ? "selected" : "")?>>Dec</OPTION>
                            </SELECT>
                            <script language="JavaScript">
                                SelectOption(document.Search.tMonth, "<?=$tMonth; ?>");
                            </script>
                            <SELECT name="tYear" size="1" id="tYear" style="font-family:verdana; font-size: 11px">

                                <?
                                $cYear=date("Y");
                                for ($Year=2004;$Year<=$cYear;$Year++)
                                {
                                    echo "<option value='$Year' " .  ($Year == $year ? "selected" : "") . ">$Year</option>\n";
                                }
                                ?>
                            </SELECT>

                            <script language="JavaScript">
                                SelectOption(document.Search.tYear, "<?=$tYear; ?>");
                            </script>
                            <br><br>

                            Time &nbsp;<b>From</b>
                            <?
                            $defaultMin = "00";
                            $defaultHr = "00";
                            $defaultSec = "00";
                            ?>

                            <SELECT name="fhr" size="1" id="fhr" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($hr=0;$hr<25;$hr++)
                                {
                                    if ($hr<10)
                                        $hr="0".$hr;
                                    echo "<option value='$hr'" .  ($hr == $defaultHr ? "selected" : "") . ">$hr</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.fhr, "<?=$fhr; ?>");
                            </script>
                            <i>hr</i>:

                            <SELECT name="fmin" size="1" id="fmin" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($min=0;$min<61;$min++)
                                {
                                    if ($min<10)
                                        $min="0".$min;
                                    echo "<option value='$min'" .  ($min == $defaultMin ? "selected" : "") . ">$min</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.fmin, "<?=$fmin; ?>");
                            </script>
                            <i>mm</i>:
                            <SELECT name="fsec" size="1" id="fsec" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($vsec=0;$vsec<61;$vsec++)
                                {
                                    if ($vsec<10)
                                        $vsec="0".$vsec;
                                    echo "<option value='$vsec'" .  ($vsec == $defaultSec ? "selected" : "") . ">$vsec</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.fsec, "<?=$fsec; ?>");
                            </script>
                            <i>sec</i>


                            <b>To</b>

                            <?
                            $defaultToMin = "23";
                            $defaultToHr = "59";
                            $defaultToSec = "59";
                            ?>
                            <SELECT name="thr" size="1" id="thr" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($hr=0;$hr<25;$hr++)
                                {
                                    if ($hr<10)
                                        $hr="0".$hr;
                                    echo "<option value='$hr'" .  ($hr == "23" ? "selected" : "") . ">$hr</option>\n";

                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.thr, "<?=$thr; ?>");
                            </script>
                            <i>hr</i>:

                            <SELECT name="tmin" size="1" id="tmin" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($min=0;$min<61;$min++)
                                {
                                    if ($min<10)
                                        $min="0".$min;
                                    //echo "<option value='$min'" .  ($min == $defaultToMin ? "selected" : "") . ">$min</option>\n";
                                    echo "<option value='$min'" .  ($min == "59" ? "selected" : "") . ">$min</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.tmin, "<?=$tmin; ?>");
                            </script>
                            <i>mm</i>:
                            <SELECT name="tsec" size="1" id="tsec" style="font-family:verdana; font-size: 11px">

                                <?
                                for ($sec=0;$sec<61;$sec++)
                                {
                                    if ($sec<10)
                                        $sec="0".$sec;
                                    echo "<option value='$sec'" .  ($sec == "59" ? "selected" : "") . ">$sec</option>\n";
                                }
                                ?>
                            </select>
                            <script language="JavaScript">
                                SelectOption(document.Search.tsec, "<?=$tsec; ?>");
                            </script>
                            <i>sec</i>
                            <? }?>
                    </tr>
                    <tr><td align= "center" colspan="2">
                            <input type="submit" name="Submit" value="Search"></td>
                    </tr>
                </form>
            </table>
            <br>
            <br>
            <table width="100%" border="1" cellpadding="0" bordercolor="#666666">
                <form action="view-transactions.php?action=<? echo $_GET["action"]?>" method="post" name="trans">


                    <?
                    if ($allCount > 0){
                        ?>
                        <tr>
                            <td  bgcolor="#000000" >
                                <table  width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                    <tr>
                                        <td>
                                            <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                                                Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                                                of
                                                <?=$allCount; ?>
                                            <?php } ;?>
                                        </td>
                                        <?php if ($prv >= 0) { ?>
                                            <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&customerParentID=$customerParentID";?>"><font color="#005b90">First</font></a>
                                            </td>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&customerParentID=$customerParentID";?>"><font color="#005b90">Previous</font></a>
                                            </td>
                                        <?php } ?>
                                        <?php
                                        if ( ($nxt > 0) && ($nxt < $allCount) ) {
                                            $alloffset = (ceil($allCount / $limit) - 1) * $limit;
                                            ?>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&customerParentID=$customerParentID";?>"><font color="#005b90">Next</font></a>&nbsp;
                                            </td>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&customerParentID=$customerParentID";?>"><font color="#005b90">Last</font></a>&nbsp;
                                            </td>
                                        <?php } ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>



                        <tr>
                            <td height="25" nowrap bgcolor="C0C0C0"><span class="tab-u"><strong>&nbsp;There
              are <? echo (count($contentsTrans) + count($onlinecustomer))?> records to View.</span></td>
                        </tr>
                        <?
                        if(count($contentsTrans) > 0 || count($onlinecustomer) > 0)
                        {
                            ?>
                            <tr>
                            <td nowrap bgcolor="#EFEFEF"><table width="100%" border="0" bordercolor="#EFEFEF">
                            <tr bgcolor="#FFFFFF">
                                <td><span class="style1">Date</span></td>
                                <td><span class="style1"><? echo $systemCode; ?></span></td>
                                <td><span class="style1"><? echo $manualCode; ?></span></td>
                                <td><span class="style1">Transaction Type</span></td>
                                <td><span class="style1">Transaction Source</span></td>
                                <td><span class="style1">Money Paid</span></td>
                                <td><span class="style1">Status</span></td>
                                <?
                                if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1)
                                {
                                    ?>
                                    <td><span class="style1">Cash Amount</span></td>
                                    <td><span class="style1">Cheque Amount</span></td>
                                    <?
                                }
                                ?>

                                <td width="100" bgcolor="#FFFFFF"><span class="style1">Total Amount </span></td>
                                <td><span class="style1"><?=__("Sender");?> Name </span></td>
                                <td><span class="style1">Beneficiary Name </span></td>
                                <td width="100"><span class="style1"><?=$createdByLabel?></span></td>
                                <? if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1") {?>
                                    <td width="100"><span class="style1">Document Provided</span></td>
                                <? } if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == "1" && ($agentType == "SUPI Manager" || $agentType == "Admin")) { ?>
                                    <td width="100"><span class="style1">Distributor Name</span></td>
                                <? } ?>
                                <? if(CONFIG_SHOW_COLLECTIONPOINTS == "1" ) { ?>
                                    <td><span class="style1">Collection Point</span></td>
                                <? } ?>
                                <? if(CONFIG_CLIENT_REF == "1" ) { ?>
                                    <td><span class="style1">Client Reference</span></td>
                                <? } ?>
                                <?php if($acManagerFlag){?>
                                    <td width="80"><span class="style1"><b><?=$acManagerFlagLabel?></b> </span></td>
                                <?php }?>
                                <? if(CONFIG_VIEW_TRANSACTIONS_DOCUMENTS == "1") { ?>
                                    <td bgcolor="#FFFFFF" align="center">&nbsp;

                                    </td>
                                <? } ?>
                                <td width="74" align="center">&nbsp;</td>
                            </tr>
                            <? for($i=0;$i < count($contentsTrans);$i++)
                        {


                            if(CONFIG_MANAGE_TRANSACTION_FOR_DISABLE_SENDER == "1"){
                                $customerDetails = selectFrom("select customerStatus from ".TBL_CUSTOMER." where customerID = ".$contentsTrans[$i]["customerID"]."");
                                $onlineCustomerDetails = selectFrom("select status from cm_customer where c_id = ".$contentsTrans[$i]["customerID"]."");

                                if ($customerDetails["customerStatus"] == "Disable"){

                                    $fontColor = "#CC000";
                                    $custStatus = "Disable";
                                }elseif ($customerDetails["customerStatus"] == "Enable" || $customerDetails["customerStatus"]== ""){

                                    $fontColor = "#006699";
                                    $custStatus = "Enable";

                                }

                            }elseif(CONFIG_COLOR_OF_LINK == '1'){
                                $senderRemarks = selectFrom("select remarks from ".TBL_CUSTOMER." where customerID='".$contentsTrans[$i]["customerID"]."'");
                                $enquiry = selectFrom("select cmpID from ".TBL_COMPLAINTS." where transID='".$contentsTrans[$i]["transID"]."' and status = 'New'");

                                if(($enquiry["cmpID"] != "")&&($contentsTrans[$i]["tip"] != "" || $contentsTrans[$i]["internalRemarks"] != "" || $senderRemarks["remarks"] != "")){
                                    $fontColor = "red";
                                }elseif($enquiry["cmpID"] != ""){
                                    $fontColor = "red";
                                }elseif($contentsTrans[$i]["tip"] != "" || $contentsTrans[$i]["internalRemarks"] != "" || $senderRemarks["remarks"] != ""){
                                    $fontColor = "#FFF00";
                                }else{
                                    $fontColor = "#006699";
                                }

                            }else{
                                $fontColor = "#006699";
                            }
                            $toolText = "";
                            if(CONFIG_ENABLE_EX_RATE_LIMIT == '1')
                            {

                                $rateLimitQuery = "select id, minRateAlert, maxRateAlert, isAlertProcessed from ".TBL_TRANSACTION_EXTENDED." where transID = '".$contentsTrans[$i]["transID"]."' and isAlertProcessed = 'N'";
                                $rateLimitContent = selectFrom($rateLimitQuery);
                                if($rateLimitContent["minRateAlert"] > 0 || $rateLimitContent["maxRateAlert"] > 0)
                                {
                                    $toolText = "Waiting till Exchange Rate Min = ".$rateLimitContent["minRateAlert"]." and Max = ".$rateLimitContent["maxRateAlert"];
                                    $fontColor = "#CC0000";
                                }
                            }
                            ?>

                            <tr bgcolor="#FFFFFF">
                                <td width="100" bgcolor="#FFFFFF"><a href="#" <? if($toolText != ""){ ?>onmouseover="Tip('<?=$toolText?>')"; onMouseOut="UnTip()";<? } ?> onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $contentsTrans[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong>
                                            <font color="<? echo $fontColor;?>"><? echo dateFormat($contentsTrans[$i]["transDate"], "2")?></font></strong></a></td>
                                <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["refNumberIM"]?></td>
                                <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["refNumber"]; ?></td>
                                <td width="75" bgcolor="#FFFFFF">

                                    <?php if($contentsTrans[$i]["transType"] == 'Topup'){
                                        echo 'Prepaid';
                                    }else{
                                        echo $contentsTrans[$i]["transType"];
                                    }
                                    ?>


                                </td>
                                <td width="75" bgcolor="#FFFFFF"><?php if($contentsTrans[$i]["trans_source"] == 'O'){ echo 'Online';}else{ echo 'Payex';}?></td>
                                <td width="75" bgcolor="#FFFFFF" ><? echo $contentsTrans[$i]["moneyPaid"]; ?></td>
                                <td width="75" bgcolor="#FFFFFF"><? echo $contentsTrans[$i]["transStatus"]?></td>
                                <?
                                if ( defined("CONFIG_PART_CASH_PART_CHEQUE_TRANS") && CONFIG_PART_CASH_PART_CHEQUE_TRANS == 1 )
                                {
                                    $cashAmount = $contentsTrans[$i]["totalAmount"] - $contentsTrans[$i]["chequeAmount"];
                                    $chequeAmt = ($contentsTrans[$i]["chequeAmount"] > 0 ? $contentsTrans[$i]["chequeAmount"] : 0);
                                    ?>
                                    <td><?=number_format($cashAmount, 4) .  " " . $contentsTrans[$i]["currencyFrom"]; ?></td>
                                    <td><?=number_format($chequeAmt, 4) .  " " . $contentsTrans[$i]["currencyFrom"];?></td>
                                    <?
                                }
                                ?>
                                <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($contentsTrans[$i]["totalAmount"]) .  " " . $contentsTrans[$i]["currencyFrom"]?></td>
                                <? if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
                                {

                                    $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$contentsTrans[$i]["customerID"]."'");
                                    $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$contentsTrans[$i]["benID"]."'");
                                    ?>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
                                    <?
                                }else
                                {
                                    if(CONFIG_SHARE_OTHER_NETWORK == '1')
                                    {
                                        $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$contentsTrans[$i]["transID"]."' and generatedLocally = 'N'");
                                    }
                                    if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
                                    {
                                        $jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");
                                        $otherClient = new connectOtherDataBase();
                                        $otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);

                                        $customerContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentsTrans[$i]["customerID"]."'");
                                        $beneContent = $otherClient->selectOneRecord("select firstName, lastName from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentsTrans[$i]["benID"]."'");
                                        $otherClient->closeConnection();
                                        dbConnect();
                                    }else{
                                        $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$contentsTrans[$i]["customerID"]."'");
                                        $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$contentsTrans[$i]["benID"]."'");
                                    }
                                    ?>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
                                    <?
                                }
                                ?>

                                <?
                                if($contentsTrans[$i]["createdBy"] == "CUSTOMER")
                                {
                                    $custContent = selectFrom("select * from cm_customer where c_id='".$contentsTrans[$i]["customerID"]."'");
                                    $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
                                }
                                else

                                {
                                    $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["custAgentID"]."'");
                                    $createdBy = ucfirst($agentContent["name"]);
                                }
                                ?>

                                <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
                                    <? echo $createdBy; ?>

                                </td>
                                <? if(CONFIG_SENDER_DOCUMENT_AT_CREATE_TRANS == "1") { ?>
                                    <td align="center" bgcolor="#FFFFFF">
                                        <?
                                        $arrDocument = unserialize($contentsTrans[$i]["custDocumentProvided"]);
                                        $catName = "";
                                        if(!empty($arrDocument)){
                                            foreach($arrDocument as $key => $val){
                                                $categoryQuery = selectFrom("select id,description from ".TBL_CATEGORY." where id = '".$key."'");
                                                $catName .= $categoryQuery["description"].",";
                                            }
                                        }
                                        echo $catName;
                                        ?>
                                    </td>
                                <? } ?>

                                <? if(CONFIG_DISTRIBUTOR_ASSOCIATED_MANAGEMENT == "1" && ($agentType == "SUPI Manager" || $agentType == "Admin")) {

                                    $distributorContent = selectFrom("select name, username from " . TBL_ADMIN_USERS . " where userID='".$contentsTrans[$i]["benAgentID"]."'");
                                    ?>
                                    <td width="75" bgcolor="#FFFFFF"><? echo $distributorContent["name"]?></td>

                                <? } ?>
                                <!-- Added by Niaz Ahmad againist ticket # 2531 at 09-10-2007 -->
                                <? if(CONFIG_SHOW_COLLECTIONPOINTS == "1") {
                                $collectionPoint = selectFrom("select cp_id,cp_corresspondent_name,cp_city from " . TBL_COLLECTION . " where cp_id='".$contentsTrans[$i]["collectionPointID"]."' and '".$contentsTrans[$i]["transType"]."' = 'Pick up' ");
                                ?>
                                <td bgcolor="#FFFFFF" title="<? if($collectionPoint["cp_city"]!= ''){ echo $collectionPoint["cp_corresspondent_name"]."-".$collectionPoint["cp_city"]; } else { echo $collectionPoint["cp_corresspondent_name"]; } ?>">
                                    <? if($collectionPoint["cp_city"]!= ''){ echo $collectionPoint["cp_corresspondent_name"]."-".$collectionPoint["cp_city"]; } else { echo $collectionPoint["cp_corresspondent_name"]; }
                                    } ?>
                                </td>

                                <? if(CONFIG_CLIENT_REF == "1" ) { ?>
                                    <td bgcolor="#FFFFFF"><? echo stripslashes($contentsTrans[$i]["clientRef"])?></td>
                                <? } ?>
                                <?php if($acManagerFlag){?>
                                    <td bgcolor="#FFFFFF"><?=ucfirst($contentsTrans[$i]["accountManagerName"])?></td>
                                <?php }?>
                                <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $contentsTrans[$i]["transID"]?>&viewTrans=Y" class="style2">Enquiry</a></td>
                                <? if(CONFIG_VIEW_TRANSACTIONS_DOCUMENTS == "1") {
                                    ?>
                                    <td bgcolor="#FFFFFF" align="center">
                                        <? if($contentsTrans[$i]["createdBy"] != "CUSTOMER"){?>
                                            <a class="style2" onClick="javascript:window.open('viewDocument.php?customerID=<?=$contentsTrans[$i]["customerID"]?>', 'ViewDocuments', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=420,width=740')" href="#">
                                                View Documents
                                            </a>
                                        <? }else{?>
                                            &nbsp;
                                        <? }?>
                                    </td>
                                <? } ?>
                            </tr>
                            <?
                        }
                            ?>

                            <? for($i=0;$i < count($onlinecustomer);$i++)
                        {
                            ?>

                            <tr bgcolor="#FFFFFF">
                                <td width="100" bgcolor="#FFFFFF"><a href="#" onClick="javascript:window.open('view-transaction.php?action=<? echo $_GET["action"]; ?>&transID=<? echo $onlinecustomer[$i]["transID"]?>','<? echo $_GET["action"];?>TranDetails', 'scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=yes,height=420,width=740')"><strong><font color="#006699"><? echo dateFormat($onlinecustomer[$i]["transDate"], "2")?></font></strong></a></td>
                                <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["refNumberIM"]?></td>
                                <td width="75" bgcolor="#FFFFFF" ><? echo $onlinecustomer[$i]["refNumber"]; ?></td>
                                <td width="75" bgcolor="#FFFFFF"><? echo $onlinecustomer[$i]["transStatus"]?></td>
                                <td width="100" bgcolor="#FFFFFF"><? echo customNumberFormat($onlinecustomer[$i]["totalAmount"]) .  " " . $onlinecustomer[$i]["currencyFrom"]?></td>
                                <? if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
                                {

                                    $customerContent = selectFrom("select FirstName, LastName from cm_customer where c_id ='".$onlinecustomer[$i]["customerID"]."'");
                                    $beneContent = selectFrom("select firstName, lastName from cm_beneficiary where benID ='".$onlinecustomer[$i]["benID"]."'");
                                    ?>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["FirstName"]). " " . ucfirst($customerContent["LastName"]).""?></td>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
                                    <?
                                }else
                                {
                                    $customerContent = selectFrom("select firstName, lastName from " . TBL_CUSTOMER . " where customerID='".$onlinecustomer[$i]["customerID"]."'");
                                    $beneContent = selectFrom("select firstName, lastName from " . TBL_BENEFICIARY . " where benID='".$onlinecustomer[$i]["benID"]."'");
                                    ?>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($customerContent["firstName"]). " " . ucfirst($customerContent["lastName"]).""?></td>
                                    <td width="100" bgcolor="#FFFFFF"><? echo ucfirst($beneContent["firstName"]). " " . ucfirst($beneContent["lastName"]).""?></td>
                                <?}
                                ?>

                                <?
                                if($onlinecustomer[$i]["createdBy"] == "CUSTOMER")
                                {
                                    $custContent = selectFrom("select * from cm_customer where c_id='".$onlinecustomer[$i]["customerID"]."'");
                                    $createdBy = ucfirst($custContent["username"]);//." ".ucfirst($custContent["LastName"]);
                                }
                                else

                                {
                                    $agentContent = selectFrom("select name, agentCity, agentCountry from " . TBL_ADMIN_USERS . " where userID='".$onlinecustomer[$i]["custAgentID"]."'");
                                    $createdBy = ucfirst($agentContent["name"]);
                                }
                                ?>

                                <td width="100" bgcolor="#FFFFFF" title="<? echo ucfirst($agentContent["name"]). " " . ucfirst($agentContent["agentCity"])." " . ucfirst($agentContent["agentCountry"])?>">
                                    <? echo $createdBy; ?>


                                </td>





                                <!-- Added by Niaz Ahmad againist ticket # 2531 at 09-10-2007 -->
                                <? if(CONFIG_SHOW_COLLECTIONPOINTS == "1") {
                                $collectionPoint = selectFrom("select cp_id,cp_corresspondent_name from " . TBL_COLLECTION . " where cp_id='".$contentsTrans[$i]["collectionPointID"]."' and '".$contentsTrans[$i]["transType"]."' = 'Pick up' ");
                                ?>
                                <td width="100" bgcolor="#FFFFFF" title="<? echo  $collectionPoint["cp_corresspondent_name"]; ?>">
                                    <? echo $collectionPoint["cp_corresspondent_name"];
                                    } ?>
                                </td>

                                <td align="center" bgcolor="#FFFFFF"><a href="add-complaint.php?transID=<? echo $onlinecustomer[$i]["transID"]?>&viewTrans=Y" class="style2">Enquiry</a></td>
                            </tr>
                            <?
                        }

                        } // greater than zero
                        ?>
                        </table></td>
                        </tr>


                        <tr>
                            <td  bgcolor="#000000">
                                <table width="100%" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                    <tr>
                                        <td>
                                            <?php if ((count($contentsTrans) + count($onlinecustomer)) > 0) {;?>
                                                Showing <b><?php print ($offset+1) . ' - ' . ($offset+(count($contentsTrans) + count($onlinecustomer)));?></b>
                                                of
                                                <?=$allCount; ?>
                                            <?php } ;?>
                                        </td>
                                        <?php if ($prv >= 0) { ?>
                                            <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType&customerParentID=$customerParentID";?>"><font color="#005b90">First</font></a>
                                            </td>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType&customerParentID=$customerParentID";?>"><font color="#005b90">Previous</font></a>
                                            </td>
                                        <?php } ?>
                                        <?php
                                        if ( ($nxt > 0) && ($nxt < $allCount) ) {
                                            $alloffset = (ceil($allCount / $limit) - 1) * $limit;
                                            ?>

                                            <?
                                            /**
                                             * Fixing the navigation issue as when click on next button it disturb the searched query
                                             * @Ticket# 3531
                                             *
                                             * Updations in link:
                                             * Just add &agentNameType=$agentNameType to the end of the link
                                             */
                                            ?>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType&customerParentID=$customerParentID";?>"><font color="#005b90">Next</font></a>&nbsp;
                                            </td>
                                            <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"]."&transType=$transType&transStatus=$transStatus&Submit=$Submit&searchBy=$by&transID=$transID&moneyPaid=$moneyPaid&transSource=$transSource&fMonth=$fMonth&fDay=$fDay&fYear=$fYear&tMonth=$tMonth&tDay=$tDay&tYear=$tYear&fhr=$fhr&fmin=$fmin&fsec=$fsec&thr=$thr&tmin=$tmin&tsec=$tsec&nT=$nameType&agentNameType=$agentNameType&customerParentID=$customerParentID";?>"><font color="#005b90">Last</font></a>&nbsp;
                                            </td>
                                        <?php } ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <?
                    } else {
                        ?>
                        <tr>
                            <td  align="center"> <? if($errorMessage == ""  ){?>
                                    No Transaction found in the database.
                                <? }elseif($errorMessage != "") {
                                    echo $errorMessage;
                                }

                                ?>

                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </form>
            </table></td>
    </tr>

</table>
</body>
</html>
