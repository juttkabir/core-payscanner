<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
dbConnect();
$username=loggedUser();
$agentDetails = selectFrom("select * from ".TBL_COLLECTION." as c, ".TBL_TELLER." as t where c.cp_id = t.collection_point and tellerID='".$_GET["userID"]."'");

?>
<html>
<head>
	<title>View Teller Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
  <tr>
    <td class="topbar">
		<table width="100%"><tr><td><strong><font color="#FFFFFF" size="2">View Teller Details</font></strong></td><td><a href="javascript:window.close()"><font size="4" color="#ffffff"><b>X</b></font></a>&nbsp;</td></tr></table></td>
  </tr>
  <tr>
    <td align="center">
		<table width="426" border="0" cellspacing="1" cellpadding="2" align="center">
		
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Teller Name*</strong></font></td>
            <td width="256"><?=stripslashes($agentDetails["name"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong> Distributor Name</strong></font></td>
            <td><? 
            	$parantBank = selectFrom("select * from ".TBL_ADMIN_USERS." where userID='".$agentDetails["cp_ida_id"]."'");

            	echo stripslashes($parantBank["name"]);
            
            ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Address </b></font></td>
            <td><?=stripslashes($agentDetails["cp_branch_address"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Email</b></font></td>
            <td><?=stripslashes($agentDetails["email"]); ?></td>
        </tr>
         <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Teller Created Date</b></font></td>
            <td><?=stripslashes($agentDetails["created"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>Country*</b></font></td>
            <td><?=$agentDetails["cp_country"]; ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>City*</b></font></td>
            <td><?=$agentDetails["cp_city"];?></td>
        </tr>
		<tr bgcolor="#ededed">
            <td width="159" valign="top"><font color="#005b90"><b>
             
              State</b></font></td>
            <td><?=stripslashes($agentDetails["cp_state"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Phone*</strong></font></td>
            <td><?=stripslashes($agentDetails["cp_phone"]); ?></td>
        </tr>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong>Status</strong></font></td>
            <td><?=$agentDetails["is_active"];?></td>
        </tr>
	<?	if ($agentDetails["is_active"] == 'Suspended' || $agentDetails["is_active"] == 'Disabled') {  ?>
        <tr bgcolor="#ededed">
            <td width="159"><font color="#005b90"><strong><? echo ($agentDetails["is_active"] == 'Suspended' ? "Suspension" : "Disable") ?> Reason</strong></font></td>
            <td><?=$agentDetails["Reason"];?></td>
        </tr>
	<?	}  ?>
      </table>
	</td>
  </tr>
</table>
</body>
</html>
