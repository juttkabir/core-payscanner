<?
session_start();
include ("../include/config.php");
$date_time = date('d/m/Y  h:i:s A');
include ("security.php");

$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
$nowTime = date("F j, Y");
$transID = $_GET["transID"];

if(CONFIG_CUSTOM_TRANSACTION == '1')
{
	$transactionPage = CONFIG_TRANSACTION_PAGE;
}else{
	$transactionPage = "add-transaction.php";
	}
if($_GET["r"] == 'dom')///If transaction is domestic
{
$returnPage = 'add-transaction-domestic.php';
}else{
	$returnPage = $transactionPage;
	}
$buttonValue = $_GET["transSend"];	
	/**
		When selected multiple transactions from Quick Menu then All transactions are shown
		in line breaks.
		by Aslam Shahid.
	   
	  */	
	if(isset($_REQUEST["checkbox"]) && sizeof($_REQUEST["checkbox"]) > 0)
	{
		for($i=0; $i<sizeof($_REQUEST["checkbox"]); $i++)
		{
			$transID = $_REQUEST["checkbox"][$i];
			$queryTransaction[$i] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			
			if($queryTransaction[$i]["custAgentID"] != '')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[$i]["custAgentID"] . "'";
				$custAgent=$queryTransaction[$i]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}	
		
			$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[$i]["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[$i]["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
		}
			$horizontalLine = true;
	} 
	else{	
		
			if($transID!='')
			{
				$queryTransaction[0] = selectFrom("select *  from ".TBL_TRANSACTIONS." where transID ='" . $transID . "'");
			}
			if($queryTransaction[0]["custAgentID"]!='')
			{
				$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $queryTransaction[0]["custAgentID"] . "'";
				$custAgent=$queryTransaction[0]["custAgentID"];
				$senderAgentContent = selectFrom($querysenderAgent);
				$custAgentParentID = $senderAgentContent["parentID"];
			}
		$queryCust = "select accountName,customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email,dob,IDType,IDNumber, remarks  from ".TBL_CUSTOMER." where customerID ='" . $queryTransaction[0]["customerID"] . "'";
		$customerContent = selectFrom($queryCust);
		$queryBen = "select * from ".TBL_BENEFICIARY." where benID ='" . $queryTransaction[0]["benID"] . "'";
		$benificiaryContent = selectFrom($queryBen);
}				
		
?>
<html>
<head>
<title>Transaction Confirmation</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="styles/printing.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
body{
color:#000066;
}
.border {
	border-right: 1px solid #000000;
	border-left: 1px solid #000000;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
}

.labelFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
.linkFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color:#000000;
}

.tddataFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-variant:normal;
}


.headingFont {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	
}
.tddataFontsmall {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	vertical-align: baseline;
	}
	

.italics {	
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			}	

.tdsize {
			 height:50px;
			 vertical-align:top;
		   }

			
.tditalics {
			font-family:Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:bold;
			height:30px;
		   }

			
		}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<?
	for($i=0; $i<sizeof($queryTransaction); $i++)
	{

	$benBankDetails = selectFrom("select * from ". TBL_BANK_DETAILS ." where transID='".$transID."'");
?>
<table width="100%" border="0"  >
		<tr>
		<td>
		<table width="650" border="0"  align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100" class="headingFont">
			<img src="images/AMB/AMB_logo.JPG" border="0"></td>
			<td width="391" class="headingFont">
				<div id="ambHeader" style="float:left;" align="left">
					<font size="5">AMB EXCHANGE</font><br/>
					<span style="font-size:11px;">
					87,Edgware Road, London, W2 2HX 
					Tel: 0207 723 3622, Fax: 0207 723 2984<br/>
					E-mail: info@graphcrown.com
					</span><br/><br/>
					<center>
					<span style="font-size:14px;">
					REMITTANCE APPLICATION FORM<br/>
					Telegraphic / SWIFT Transfer
					</span>
					</center>
				</div>
			</td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td>
		<table width="650" border="1"  align="center" cellpadding="3" cellspacing="0" style=" border:1px; color:#333333;">
		<tr>
          <td class="tditalics" style="border:none;" colspan="2"><span style="font-size: 14px;">
            PLEASE ISSUE YOUR TT / DRAFT ON
          </span></td>
		  <td width="148" align="right" valign="top" class="tditalics" style="border:none;"> Date:- </td>
		  <td valign="top" class="tddataFont" style="border:none;"><? 
			$transDate = $queryTransaction[$i]["transDate"];
			echo date('d/m/Y H:i:s A',strtotime($transDate));
			?></td>
		  </tr>
		<tr>
			<td width="251"  class="headingFont" align="center" style="border-right:none; font-size:18px">OFFICE USE ONLY</td>
			<td colspan="2" valign="top" ><span class="tditalics">FC Amount : </span><span class="tddataFont"><? echo $queryTransaction[$i]["localAmount"];?></span></td>
			<td width="216" valign="top"><span class="tditalics">RATE  : </span><span class="tddataFont"><? echo $queryTransaction[$i]["exchangeRate"];?></span></td>
		  </tr>
		<tr>
			<td width="251" valign="top"><span class="tditalics">CURRENCY  : </span><span class="tddataFont"><? echo $queryTransaction[$i]["currencyTo"];?></span></td>
			<td colspan="2" valign="top"><span class="tditalics">COMM  : </span><span class="tddataFont"><? echo $queryTransaction[$i]['IMFee']+$queryTransaction[$i]["bankCharges"];?></span></td>
			<td valign="top"><span class="tditalics">TOTAL STG  : </span><span class="tddataFont"><? echo $queryTransaction[$i]['transAmount']."&nbsp;".$queryTransaction[$i]["currencyFrom"];?></span></td>
		  </tr>
		</table>
		<table width="650" border="1"  align="center" cellpadding="3" cellspacing="0" style=" border:1px; color:#333333; margin-top:10px;">
		<tr>
<td class="tditalics" style="border:none;" colspan="4">Reference # :&nbsp;<span style="font-size: 16px; font-weight:normal;;"><?=$queryTransaction[$i]["refNumberIM"]?></span></td>
		  </tr>
		<tr>
			<td width="125" class="tditalics">Ben Name  </td>
			<td colspan="3" valign="top" class="tddataFont"><? echo $benificiaryContent["firstName"] . " " . $benificiaryContent["middleName"] . " " . $benificiaryContent["lastName"] ?></td>
		  </tr>
		<tr>
			<td class="tditalics">Bank  Name</td>
			<td colspan="3" valign="top" class="tddataFont"><?=($benBankDetails["bankName"]!="" ? $benBankDetails["bankName"]:"&nbsp;")?></td>
		  </tr>
		<tr>
			<td class="tditalics">Bank Address</td>
			<td colspan="3" valign="top" class="tddataFont"><?=($benBankDetails["branchAddress"]!="" ? $benBankDetails["branchAddress"]:"&nbsp;")?></td>
		  </tr>
		<tr>
          <td class="tditalics">Account No.</td>
		  <td colspan="3" valign="top" class="tddataFont"><? echo $benBankDetails["accNo"]."&nbsp;"?></td>
		  </tr>
		<tr>
          <td class="tditalics">IBAN Number</td>
		  <td width="176" valign="top" class="tddataFont"><?=$benBankDetails["IBAN"]."&nbsp;";?></td>
		  <td width="128" valign="top" class="tditalics">SWIFT / ABA No.</td>
		  <td width="187" valign="top" class="tddataFont">
		  <?	
		  	$separator = "";	
		  	if($benBankDetails["swiftCode"] !="" && $benBankDetails["ABACPF"]!=""){
		  		$separator = "/";
			}
		  	echo $benBankDetails["swiftCode"]."&nbsp;".$separator."&nbsp;".$benBankDetails["ABACPF"];
			?></td>
		  </tr>
		<tr>
          <td class="tditalics">City : </td>
		  <td valign="top" class="tddataFont"><?=$benificiaryContent["City"]. "&nbsp;"?></td>
		  <td valign="top" class="tditalics">Country : </td>
		  <td valign="top" class="tddataFont"><?=$benificiaryContent["Country"]. "&nbsp;"?></td>
		  </tr>
		<tr>
          <td colspan="2" valign="top" class="tditalics" style="height:100px;">PAY THOROUGH BANK DETAILS</td>
		  <td colspan="2" valign="top" class="tditalics" style="height:100px;">SPECIAL INSTRUCTIONS FOR BANK </td>
		  </tr>
		<tr>
          <td class="tditalics">APPLICANT :</td>
		  <td colspan="3" valign="top" class="tddataFont"><span class="tditalics"> <? echo $customerContent["firstName"] . "&nbsp;" . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></span></td>
		  </tr>
		<tr>
          <td class="tditalics">Company Name :</td>
		  <td colspan="3" valign="top" class="tddataFont">AMB EXCHNANGE</td>
		  </tr>
		<tr>
			<td class="tditalics">Address</td>
			<td colspan="3" valign="top" class="tddataFont"><? echo $customerContent["Address"] . "&nbsp;" . $customerContent["Address1"]?></td>
		  </tr>
		<tr>
          <td class="tditalics">Telephone : </td>
		  <td valign="top" class="tddataFont"><?=($customerContent["Phone"]!="" ? $customerContent["Phone"]:"&nbsp;")?></td>
		  <td valign="top" class="tditalics">Post Code  : </td>
		  <td valign="top" class="tddataFont"><?=$customerContent["Zip"]. "&nbsp;"?></td>
		  </tr>
		<tr>
			<td colspan="4" class="headingFont" align="center" style="border-left:none;border-right:none; font-size:18px">TERMS AND CONDITIONS</td>
			</tr>
		<tr>
			<td colspan="4" class="tdsize">
			<?
			$termsConditionSql = selectFrom("SELECT
										company_terms_conditions
								 FROM 
								 		company_detail
								 WHERE 
								 		company_terms_conditions!='' AND 
										dated = (
												 SELECT MAX(dated)
												 FROM 
												 	company_detail 
												WHERE 
													company_terms_conditions!=''
												 )");
			if(!empty($termsConditionSql["company_terms_conditions"]))
					$tremsConditions = $termsConditionSql["company_terms_conditions"];
			
		?>
		<p style="padding:5px;">
		<? echo $tremsConditions; ?>		</p></td>
			</tr>
		<tr>
			<td colspan="4"  class="headingFont" style="padding:2px;border-bottom:none; border-left:none; border-right:none;" align="center" ><span class="headingFont" style="padding:2px;">By Signing this form, I agree and accept all above Terms and Conditions.</span></td>
		</tr>
		<tr>
			<td colspan="4" class="tditalics" style="border:none;">
<span id="customerSign" style="float:right; padding-right:80px;margin-top:30px;" class="tditalics">Customer's Sign : ...........................................</span></td>
			</tr>
</table>
</td>
</tr>
	<tr>
	<td>
	 	<? if($horizontalLine) { ?>
		 <hr noshade="noshade" size="1" />
	<?
	   }
	 } // end for loop
	 ?>			
   <table align="center" width="650" border="0" cellpadding="0"  cellspacing="0">
	<tr>
	<td>
	<div class='noPrint' align="right">
	<input type="submit" name="Submit2" value="Print this Receipt" onClick="print()">
	&nbsp;&nbsp;&nbsp;<a href="add-transaction.php?create=Y" class="linkFont">Go to Create Transaction</a>
	</div>
	</td>
	</tr>

</table>
</td>
</tr>
</table>
</body>
</html>