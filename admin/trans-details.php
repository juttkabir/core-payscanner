<?
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s A');
$limit_date = date('Y-m-d');
include ("security.php");
include ("ledgersUpdation.php");
$transID = (int) $_GET["transID"];
$systemCode = SYSTEM_CODE;
$company = COMPANY_NAME;
$systemPre = SYSTEM_PRE;
$manualCode = MANUAL_CODE;
global $isCorrespondent;
global $adminType;
$agentType = getAgentType();
$loggedUserID = $_SESSION["loggedUserData"]["userID"];
$root_path = $_SERVER['DOCUMENT_ROOT'];
include("connectOtherDataBase.php");
if(!empty($_REQUEST["srcUrl"]))
	$strBackUrl = $_REQUEST["srcUrl"];
else
	$strBackUrl = "release-trans.php";

if(defined("CONFIG_API_TRANSACTIONS"))
	require(LOCAL_DIR."api/gopi/api_functions.php");

function callApiModif($sftp, $intTransId, $_arrCountries, $_arrPayoutAgentId)
{
	if(CONFIG_API_TRANSACTIONS == "CMT" && !empty($intTransId))
	{
		$_arrIdTypes = array(
			 "Passport" => "01",
			 "National ID" => "02",
			 "Driver�s License" => "03",
			 "Driver License" => "03",
			 "Drivers License" => "03",
			 "Other Government ID" => "04",
			 "Other" => "06"
		);

		
		$strTransactionDetSql = "select sending_agent, rejectDate, deliveryDate, refNumber, benAgentID, Address, City, Country, Phone, IDType, IDNumber, partner_id  from transactions t, beneficiary b where transid = '".$intTransId."' and from_server = '".CONFIG_API_TRANSACTIONS."' and is_parsed = 'Y' and b.benID = t.benID";
		$arrTransactionDet = selectFrom($strTransactionDetSql);
		
		if(count($arrTransactionDet) > 0) //&& !empty($arrTransactionDet["IDNumber"]) && !empty($_arrIdTypes[$arrTransactionDet["IDType"]])
		{
			$intStatus = "01";
			$strUpdateDate = str_replace(" ","T",$arrTransactionDet["deliveryDate"]);
			if($_POST["changeStatusTo"] == "Rejected")
			{
				$intStatus = "02";
				$strUpdateDate = str_replace(" ","T",$arrTransactionDet["rejectDate"]);
			}
	
			$strCountry = explode(" ",$arrTransactionDet["Country"]);
			$strIsoCountry = '';
			
			while ($countryName = current($_arrCountries)) 
			{
			   if (strtoupper($countryName) == $strCountry[0])
				   $strIsoCountry =  key($_arrCountries);
			   next($_arrCountries);
			}
			
			
			$intReceiverId = $_arrIdTypes[$arrTransactionDet["IDType"]];
			if(empty($intAgentId))
				$intAgentId = "7";
			$strReceiverIdNumber = $arrTransactionDet["IDNumber"];
			if(empty($strReceiverIdNumber))
				$strReceiverIdNumber = "Any photo id";
						
			$strXmlResponse = '<?xml version="1.0" encoding="UTF-8" ?>
								<TMTPayoutUpdate xmlns="tmt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
									<Transaction>
										<partnerID>'.$arrTransactionDet["partner_id"].'</partnerID>
										<agentID>'.$arrTransactionDet["sending_agent"].'</agentID>
										<tmtTransactionNumber>'.$arrTransactionDet["refNumber"].'</tmtTransactionNumber>
										<updateDateTime>'.$strUpdateDate.'</updateDateTime>
										<updateType>'.$intStatus.'</updateType>
										<userId>'.$_SESSION["loggedUserData"]["username"].'</userId>
										<payoutAgentId>'.$_arrPayoutAgentId[$arrTransactionDet["partner_id"]].'</payoutAgentId>
										<receiverAddress1>'.$arrTransactionDet["Address"].'</receiverAddress1>
										<receiverCity>'.$arrTransactionDet["City"].'</receiverCity>
										<receiverCountry>'.$strIsoCountry.'</receiverCountry>
										<receiverPhoneNumber>'.$arrTransactionDet["Phone"].'</receiverPhoneNumber>
										<receiverIdType>'.$intAgentId.'</receiverIdType>
										<receiverIdProvided>'.$strReceiverIdNumber.'</receiverIdProvided>
										<messageDateTime>'.date("Y-m-d\TH:i:s").'</messageDateTime>
									</Transaction>
								</TMTPayoutUpdate>';
			
			$strFileName = $arrTransactionDet["partner_id"]."_TMTPUP_".date("dmYHis").".xml";
			
			/*
			if(!validateXmlFromFile($strXmlResponse, TMTPUP))
				debug(htmlentities($strXmlResponse),true);
			*/
			//do{
				$bolFileNotWritten = false;
				if(!writeFileOnSftp($sftp, $strFileName, $strXmlResponse))
				{
					/* If file not written than try it again with a sllep time of 10 seconds */
					$bolFileNotWritten = true;
					echo "Unable to write '$strFileName' file, on client system, trying it again in next 10 seconds.<br />";
					//debug("Unable to write '$strFileName' file, on client system, trying it again in next 10 seconds.<br />", true);
					//sleep(10);
				}
			//}while($bolFileNotWritten);
			
			//debug(htmlentities($strXmlResponse), true);
		}
	}
}


if (CONFIG_CUSTOM_RECIEPT == '1') {	// [by JAMSHED]
	$receiptFormURL = CONFIG_RECEIPT_FORM;
}else{
	$receiptFormURL = "reciept_form.php";
}

$receiptFormURL .= "?transID=" . $_GET["transID"];

$isAgentAnD = 'N';
$isBankAnD = 'N';
///////////////////////History is maintained via method named 'activities'

if($agentType == "TELLER")
{
	$teller = " by Teller";
	
	$userID = $_SESSION["loggedUserData"]["tellerID"];
	$parantID = $_SESSION["loggedUserData"]["cp_ida_id"];
}else
{
	$teller = " ";
$isCorrespondent  = $_SESSION["loggedUserData"]["isCorrespondent"];
$adminType  = $_SESSION["loggedUserData"]["adminType"];
$userID  = $_SESSION["loggedUserData"]["userID"];
}



$changingPerson = "$userID"."$agentType";
$get=$_GET["act"];
$today = date("Y-m-d");


$amount = selectFrom("select transStatus from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
$transStatus = $amount["transStatus"];



if ($transStatus == "Authorize" || $transStatus == "Amended"){


if($_POST["Submit"] != "")
{
	if($_POST["Submit"] == "Submit")
	{
				if( $_POST["changeStatusTo"]  == "Failed")////Deposit Agent		
				{
					transStatusChange($_GET["transID"], "Failed", $username, $_POST["remarks"]);
					////////Remote transaction status failed////
					if(CONFIG_SHARE_OTHER_NETWORK == '1')
					{
						
						
						$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
												
						if($remoteTransInfo["remoteTrans"] != '')
						{
							
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
							if($jointClient["clientId"] != '')
							{
								$otherClient = new connectOtherDataBase();
								$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
								
								
								$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
								
								if($otherTransaction["transID"] != '')
								{						
									$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
													transStatus = 'Failed', 
													deliveredBy = '".$username."',
													failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													
													remarks		= '".$_POST["remarks"]."'
													where transID = '".$otherTransaction["transID"]."'
													");	
										
									$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
								
								
									if($remoteCustAgent["agentType"] == 'Sub')
									{
										$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}else{
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}				
									if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
									{
										$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
											
											if($remoteBenAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}else{
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Failed', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}	
									}
								}
								
							$otherClient->closeConnection();	
							
							dbConnect();	
							}
							
						}
							
							
						
					}
					
					
					
					//////////////////Remote transaction failed ended/////
					
				}
				if( $_POST["changeStatusTo"]  == "Suspicious")
				{
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Suspicious', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', suspeciousDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Suspicious";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
					
				}
				if( $_POST["changeStatusTo"]  == "CardDispatch")
				{
					
						$amount = selectFrom("select transID,totalAmount, custAgentID, customerID, benID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT."(tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Dispatched $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
								$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
								
								$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;				
								update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
								
								////////////////Either to insert into Bank or A&D Account////////
								if($checkBalance2["agentType"]=="Sub")
								{
									$dist=$checkBalance2["parentID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Dispatched $teller", "Distributor");
										 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Dispatched $teller", "Distributor");
								}else{
									$dist=$checkBalance2["userID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Dispatched $teller", "Distributor");
								}
								

						}
					//echo $amount["benID"];
					$atmInfo = selectFrom("select cID, expiryDate, cardNo, cardName from cm_cust_credit_card where customerID = '".$amount["benID"]."'"); 	
					if($atmInfo["cID"] == ""){
						$benInfo = selectFrom("select firstName, middleName, lastName from ".TBL_BENEFICIARY." where benID = '".$amount["benID"]."'");
						$benName = $benInfo["firstName"]." ".$benInfo["middleName"]." ".$benInfo["lastName"];
						insertInto("insert into  cm_cust_credit_card (customerID,cardNo,cardName,PIN,expiryDate,transID) values('".$amount["benID"]."','".$_POST['atmCardNo']."','".$benName."','". $_POST["PIN"]."', '". $_POST["expDate"]."','".$_GET["transID"]."')");
					}else{
						update("update  cm_cust_credit_card set cardNo = '".$_POST['atmCardNo']."',PIN = '". $_POST["PIN"]."',expiryDate = '". $_POST["expDate"]."' where customerID = '".$amount["benID"]."'");
					}
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Dispatched', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Dispatched";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 

			
					redirect($receiptFormURL);
					
					
					
					
					
				}
				
				if( $_POST["changeStatusTo"]  == "cardRevoked")
				{
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= 'Revoked', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Revoked";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
					
				}
				
				if( $_POST["changeStatusTo"]  == "Picked up")
				{
					
					if(CONFIG_DISTRIBUTION_DEAL_SHEET == "1"){
						if(strstr(CONFIG_DEAL_SHEET_VIEW_USERS,$agentType))
						{
							$payOutFlag = true;
							$action = "payout";
							$msg ="";
							include("$root_path/deal_sheet/server.php");
							$arrInput = array("action" =>$action,"transID"=>$transID,"agentType"=>$agentType,"loggedUserID"=>$loggedUserID);
							//debug( $arrInput);
							$payOutFlag = payOut($arrInput);
							//debug($payOutFlag);
						}
					  if(!$payOutFlag){
						$msg = "Please Add Deal Sheet Then try to pay out Transaction
						<a href='add_deal_sheet.php' target=_blank style='text-decoration:underline; color:blue'>[Add New Deal Sheet]</a>";
						redirect("trans-details.php?act=$get&msg=$msg&success=$success&dist=$_GET[dist]&transID=$transID");
						}
					}
					////////Remote transaction status Picked up////
					
							if(CONFIG_SHARE_OTHER_NETWORK == '1')
							{
								
								
								$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
														
								if($remoteTransInfo["remoteTrans"] != '')
								{
									
									$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
									if($jointClient["clientId"] != '')
									{
										$otherClient = new connectOtherDataBase();
										$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
										
										
										$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
										
										if($otherTransaction["transID"] != '')
										{						
											$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
															transStatus = 'Picked up', 
															deliveredBy = '".$username."',
															deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'
															where transID = '".$otherTransaction["transID"]."'
															");	
												
												
											if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1')
											{
												$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
													
													if($remoteBenAgent["agentType"] == 'Sub')
													{
														$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Picked up', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Picked up', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}else{
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Picked up', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}	
											}
										}
										
									$otherClient->closeConnection();		
									
									dbConnect();
										///
										
										///
									
									}
									
								}
									
									
								
							}
							
							
							
							//////////////////Remote transaction Picked up ended/////
					
					
					
// this is enhancement of system that assign the daily limit to teller.
					
					if($agentType == "TELLER"){
						
							
					$tellerInfo = selectFrom("select * from " . TBL_TELLER . " where tellerID = '$userID'");		
							
					
					//echo ("select * from " . TBL_TELLER_LIMIT . " where teller_id = '$userID' and limit_date = '$limit_date'");				
					//echo $limit_date;
					$checklimit = selectFrom("select * from " . TBL_TELLER_LIMIT . " where teller_id = '$userID' and limit_date = '$limit_date'");
					$amount = selectFrom("select  transAmount, customerID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
				}	
					//echo("account Limit ".$checklimit["account_limit"]);
				if($checklimit["account_limit"]){							
					
					
					
					$transBalance = $amount["transAmount"];
					
					$limit = $checklimit["used_limit"]+ $transBalance;
					
					 $limit_alert = $tellerInfo["account_limit"] - $limit;
									
					if ($limit <= $tellerInfo["account_limit"]){						
					update("update " . TBL_TELLER_LIMIT . " set used_limit = '" . $limit . "' where teller_id ='$userID'and limit_date = '$limit_date'");
				
					
// send email if limit is less then limit allert amount.					
					
					if ($limit_alert <= $tellerInfo["limit_alert"])
					{
			$emailToParant=selectFrom("select email from admin  where userID = '$parantID'");
				//echo("select email from admin  where userID = '$parantID'");
				$adminEmail=$emailToParant["email"];
				$subject = "Please extend my daily account limit";
				$fromName = $tellerInfo["loginName"];
				$fromEmail = $tellerInfo["loginName"];
				$message="<table>  
							<tr>
							    <td>Please extend my daily accunt limit</td>
							    <td>&nbsp;</td>
							  </tr>
							</table>
							";
								
				sendMail($adminEmail, $subject, $message, $fromName, $fromEmail);
						
					}			
// end mail send
					
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer,createdBy, benID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						
						if($checkQues["answer"] != $_POST["answer"])
						{
						
						$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							/////Dealing to Teller and Bank Account
							$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
							$balance = $amount["totalAmount"];
							$bank = $amount["benAgentID"];
							$transBalance = $amount["transAmount"];		
							
							if($agentType == "TELLER")
							{
								$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
								$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
								update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
								insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Picked up $teller')");
							}
										if(CONFIG_POST_PAID == '1')
										{
											transStatusChange($_GET["transID"], "Picked up", $username, '');
										}else{
											
												if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1')
												{
													transStatusChange($_GET["transID"], "Picked up", $username, '');
												}
											}
							
							
							
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
							$descript = "Transaction is Picked up";
							activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
							if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
							{
								if($checkQues["createdBy"] != 'CUSTOMER'){						
									update("update " . TBL_BENEFICIARY . " set Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
								}
								else
								{
									update("update " . TBL_CM_BENEFICIARY . " set Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
								}
							}
						
						if(CONFIG_EMAIL_ON_RELEASE == "1"){
								include ("emailSender.php");
								include ("emailBen.php");
							}
						callApiModif($sftp, $_REQUEST["transID"], $_arrCountries, $_arrPayoutAgentId);
						redirect($receiptFormURL);
						}
					}else{
						
						
						$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Picked up $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
							transStatusChange($_GET["transID"], "Picked up", $username, '');
							
						}
						
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
							$descript = "Transaction is Picked up";
							activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
							if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
							{	
								if($checkQues["createdBy"] != 'CUSTOMER'){							
									update("update " . TBL_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
								}
									else{							
									update("update " . TBL_CM_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
								}
							}
						 if(CONFIG_EMAIL_ON_RELEASE == "1"){
								include ("emailSender.php");
								include ("emailBen.php");
							}	
						callApiModif($sftp, $_REQUEST["transID"], $_arrCountries, $_arrPayoutAgentId);
						redirect($receiptFormURL);
						}				
				
				}
				else
				{
				insertError(AG40);
				redirect($strBackUrl."?msg=Y&dist=$_GET[dist]");
						
				}
				}else{					
					
						$limitQuery = "insert into  " . TBL_TELLER_LIMIT . " (teller_id, limit_date, account_limit, used_limit, limit_alert) values ( '$userID' , '$limit_date', '".$tellerInfo["account_limit"]."','".$amount["transAmount"]."', '".$tellerInfo["limit_alert"]."')";
						insertInto($limitQuery);
					
					
				
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer,createdBy, benID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						
						if($checkQues["answer"] != $_POST["answer"])
						{
						
						$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							//////Dealing with Teller and Bank Account
							$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"]	- $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Picked up $teller')");
						}
							if(CONFIG_POST_PAID == '1')
							{
								transStatusChange($_GET["transID"], "Picked up", $username, '');
								
							 }else{
										
											if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1')
											{
												transStatusChange($_GET["transID"], "Picked up", $username, '');
											}
										}
							
														
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
								$descript = "Transaction is Picked up";
								activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 

							
							if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
							{	
								if($checkQues["createdBy"] != 'CUSTOMER')
								{						
									update("update " . TBL_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
								}
								else
								{
									update("update " . TBL_CM_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
								}
							}
						
						if(CONFIG_EMAIL_ON_RELEASE == "1"){
							include ("emailSender.php");
							include ("emailBen.php");
						}
						callApiModif($sftp, $_REQUEST["transID"], $_arrCountries, $_arrPayoutAgentId);
						redirect($receiptFormURL);

						}
					}else{
						//////Dealing with Teller and Distributor Account
						$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"]	- $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Picked up $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
							transStatusChange($_GET["transID"], "Picked up", $username, '');
							
								
						}else{
									
										if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1')
										{
											transStatusChange($_GET["transID"], "Picked up", $username, '');
										}
									}
							
						
						/////////
						
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
							
							$descript = "Transaction is Picked up";
							activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
							
							if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
							{	
								if($checkQues["createdBy"] != 'CUSTOMER')
								{							
									update("update " . TBL_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
								}else{							
									update("update " . TBL_CM_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
								}
							}
							
							if(CONFIG_EMAIL_ON_RELEASE == "1"){
								include ("emailSender.php");
								include ("emailBen.php");
							}
							callApiModif($sftp, $_REQUEST["transID"], $_arrCountries, $_arrPayoutAgentId);
							redirect($receiptFormURL);
													
						}
					}
					
					
				}
				if( $_POST["changeStatusTo"]  == "Credited")
				{
					
					if(CONFIG_DISTRIBUTION_DEAL_SHEET == "1"){
						if(strstr(CONFIG_DEAL_SHEET_VIEW_USERS,$agentType))
						{
							$payOutFlag = true;
							$action = "payout";
							$msg ="";
							include("$root_path/deal_sheet/server.php");
							$arrInput = array("action" =>$action,"transID"=>$transID,"agentType"=>$agentType,"loggedUserID"=>$loggedUserID);
							//debug( $arrInput);
							$payOutFlag = payOut($arrInput);
							//debug($payOutFlag,true);
						}
					  if(!$payOutFlag){
						$msg = "Please Add Deal Sheet Then try to pay out Transaction
						<a href='add_deal_sheet.php' target=_blank style='text-decoration:underline; color:blue'>[Add New Deal Sheet]</a>";
						redirect("trans-details.php?act=$get&msg=$msg&success=$success&dist=$_GET[dist]&transID=$transID");
						}
					}
						////////Remote transaction status Picked up////
					
							if(CONFIG_SHARE_OTHER_NETWORK == '1')
							{
								
								
								$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
														
								if($remoteTransInfo["remoteTrans"] != '')
								{
									
									$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
									if($jointClient["clientId"] != '')
									{
										$otherClient = new connectOtherDataBase();
										$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
										
										
										$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
										
										if($otherTransaction["transID"] != '')
										{						
											$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
															transStatus = 'Credited', 
															deliveredBy = '".$username."',
															deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."'
															where transID = '".$otherTransaction["transID"]."'
															");	
												
												
											if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID == '1')
											{
												$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
													
													if($remoteBenAgent["agentType"] == 'Sub')
													{
														$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Credited $teller', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Credited $teller', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}else{
														$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'WITHDRAW', 'Transaction Credited $teller', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
													}	
											}
										}
										
									$otherClient->closeConnection();		
									
									dbConnect();
									}
									
								}
									
									
								
							}
							
							
							
							//////////////////Remote transaction Picked up ended/////
					
					
					if(CONFIG_SECRET_QUEST_ENABLED)
					{
						$checkQues = selectFrom("select answer, benID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
						if($checkQues["answer"] != $_POST["answer"])
						{
							$mess ="Secret Answer is wrong  ";
						}elseif($checkQues["answer"] == $_POST["answer"]){
							/////////Dealing with Teller and Distributor account
							$amount = selectFrom("select totalAmount,localAmount,custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Credited $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
							$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
							
							
							$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;				
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
							
							////////////////Either to insert into Bank or A&D Account////////
							if($checkBalance2["agentType"]=="Sub")
							{
								$dist=$checkBalance2["parentID"];
								$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
									 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
							}else{
								$dist=$checkBalance2["userID"];
								$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
							}
								
								
						}
							////////////Dealing with the Taller and Bank account
							
							update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
							$descript = "Transaction is Credited";
							activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
						if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
							{		
								if($checkQues["createdBy"] != 'CUSTOMER'){							
								update("update " . TBL_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
								}
									else{							
									update("update " . TBL_CM_BENEFICIARY . " set Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
								}
							}
							if(CONFIG_EMAIL_ON_RELEASE == "1"){
								include ("emailSender.php");
								include ("emailBen.php");
							}
							redirect($receiptFormURL);
							}
					}else{
						
						$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT."(tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Credited $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
								$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
								
								$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;				
								update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
								
								////////////////Either to insert into Bank or A&D Account////////
								if($checkBalance2["agentType"]=="Sub")
								{
									$dist=$checkBalance2["parentID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
										 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
								}else{
									$dist=$checkBalance2["userID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Credited $teller", "Distributor");
								}
								

						}
						
						
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Credited";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 

				if(CONFIG_SHARE_OTHER_NETWORK != '1' || $remoteTransInfo["remoteTrans"] == '')
				{		
					if($checkQues["createdBy"] != 'CUSTOMER'){							
							update("update " . TBL_BENEFICIARY . " set  Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',IDNumber = '". $_POST["IDNumber"]."' where benID='". $checkQues["benID"]."'");
						}
							else{							
							update("update " . TBL_CM_BENEFICIARY . " set Address = '". $_POST["Address"]."', Phone = '". $_POST["Phone"]."', IDType = '". $_POST["IDType"]."',NICNumber = '". $_POST["NICNumber"]."' where benID='". $checkQues["benID"]."'");
							
						}
					}
					if(CONFIG_EMAIL_ON_RELEASE == "1"){
						include ("emailSender.php");
						include ("emailBen.php");
					}
					redirect($receiptFormURL);
					}
				}
				if( $_POST["changeStatusTo"]  == "Sent By Courier")		
				{
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
					$balance = $amount["totalAmount"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];		
					
					if($agentType == "TELLER")
					{
						$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
						$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
						update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
						insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Sent By Courier $teller')");
					}
					if(CONFIG_POST_PAID == '1')
					{
							$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
							
							
							$currentBalanceBank = $checkBalance2["balance"] - $transBalance;				
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
							
							////////////////Either to insert into Bank or A&D Account////////
								if($checkBalance2["agentType"]=="Sub")
								{
									$dist=$checkBalance2["parentID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Sent By Courier $teller", "Distributor");
										 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Sent By Courier $teller", "Distributor");
								}else{
									$dist=$checkBalance2["userID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Sent By Courier $teller", "Distributor");
								}
								
							
												
					}
					
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Sent by Courier";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 

				
				}
				if( $_POST["changeStatusTo"]  == "Delivered")///Withdraw Bank		
				{
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
					$balance = $amount["totalAmount"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];		
					
					if($agentType == "TELLER")
					{
						$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
						$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
						update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
						insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Delivered $teller')");
					}
					if(CONFIG_POST_PAID == '1')
					{
							$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
							
							$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;				
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
							
							////////////////Either to insert into Bank or A&D Account////////
								if($checkBalance2["agentType"]=="Sub")
								{
									$dist=$checkBalance2["parentID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Delivered $teller", "Distributor");
										 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Delivered $teller", "Distributor");
								}else{
									$dist=$checkBalance2["userID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Delivered $teller", "Distributor");
								}
								
								
					}
					
					
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Delivered";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
					redirect($receiptFormURL);
				}
				if( $_POST["changeStatusTo"]  == "Out for Delivery")		
				{
					
					$amount = selectFrom("select totalAmount, custAgentID, customerID, createdBy, refNumberIM, transAmount, benAgentID from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."' and transStatus='Authorize'");
						$balance = $amount["totalAmount"];
						$bank = $amount["benAgentID"];
						$transBalance = $amount["transAmount"];		
						
						if($agentType == "TELLER")
						{
							$tellerInfo = selectFrom("select balance from " . TBL_TELLER . " where tellerID = '$userID'");		
							$currentTellerBalance = $tellerInfo["balance"] - $transBalance;				
							update("update " . TBL_TELLER . " set balance  = $currentTellerBalance where tellerID = '$userID'");
							insertInto("insert into ".TBL_TELLER_ACCOUNT." (tellerID, dated, type, amount, modified_by, TransID, description) values('$userID', '".getCountryTime(CONFIG_COUNTRY_CODE)."', 'WITHDRAW', '$transBalance', '$userID', '". $_GET["transID"]."', 'Transaction Out for Delivery $teller')");
						}
						if(CONFIG_POST_PAID == '1')
						{
								$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
								
								$currentBalanceBank = $checkBalance2["balance"]	- $transBalance;				
								update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
								////////////////Either to insert into Bank or A&D Account////////
								if($checkBalance2["agentType"]=="Sub")
								{
									$dist=$checkBalance2["parentID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Out for Delivery $teller", "Distributor");
										 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Out for Delivery $teller", "Distributor");
								}else{
									$dist=$checkBalance2["userID"];
									$q = updateAgentAccount($dist, $transBalance, $_GET["transID"], "WITHDRAW", "Transaction Out for Delivery $teller", "Distributor");
								}
								
								
						}
					
					
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', deliveryOutDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."', deliveredBy = '$changingPerson' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Out for Delivery";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);		 
				
				}
				if( $_POST["changeStatusTo"]  == "Rejected")///Deposit Agent//Cancelled		
				{
					
									////////Remote transaction status Rejected////
					if(CONFIG_SHARE_OTHER_NETWORK == '1')
					{
						
						
						$remoteTransInfo = selectFrom("select * from " . TBL_SHARED_TRANSACTIONS . " where localTrans = '".$_GET["transID"]."'");
												
						if($remoteTransInfo["remoteTrans"] != '')
						{
							
							$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$remoteTransInfo["remoteServerId"]."' and isEnabled = 'Y'");
							if($jointClient["clientId"] != '')
							{
								$otherClient = new connectOtherDataBase();
								$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
								
								
								$otherTransaction = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".transactions where transID = '".$remoteTransInfo["remoteTrans"]."' and (transStatus = 'Authorize' OR transStatus = 'Amended')");		
								
								if($otherTransaction["transID"] != '')
								{						
									$otherClient->update("update ".$jointClient["dataBaseName"].".transactions set 
													transStatus = 'Rejected', 
													deliveredBy = '".$username."',
													failedDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."',
													
													remarks		= '".$_POST["remarks"]."'
													where transID = '".$otherTransaction["transID"]."'
													");	
										
									$remoteCustAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["custAgentID"]."'");											
								
								
									if($remoteCustAgent["agentType"] == 'Sub')
									{
										$otherClient-> updateSubAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["parentID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}else{
										$q = $otherClient-> updateAgentAccount($remoteCustAgent["userID"], $otherTransaction["totalAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Agent", getCountryTime(CONFIG_COUNTRY_CODE));
									}				
									if(CONFIG_REMOTE_DISTRIBUTOR_POST_PAID != '1')
									{
										$remoteBenAgent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".admin where userID = '".$otherTransaction["benAgentID"]."'");
											
											if($remoteBenAgent["agentType"] == 'Sub')
											{
												$otherClient-> updateSubAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["parentID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}else{
												$q = $otherClient-> updateAgentAccount($remoteBenAgent["userID"], $otherTransaction["transAmount"], $otherTransaction["transID"], 'DEPOSIT', 'Transaction is Rejected', "Distributor", getCountryTime(CONFIG_COUNTRY_CODE));
											}	
									}
								}
								
							$otherClient->closeConnection();		
							dbConnect();
							}
							
						}
							
							
						
					}
					
					
					
					//////////////////Remote transaction Rejected ended/////
					
					
					
					$amount = selectFrom("select totalAmount, custAgentID, transAmount,localAmount, customerID, createdBy, refNumberIM, benAgentID, AgentComm,currencyFrom,currencyTo from " . TBL_TRANSACTIONS . " where transID = '". $_GET["transID"]."'");
					
								
					$balance = $amount["totalAmount"];
					$agent = $amount["custAgentID"];
					$bank = $amount["benAgentID"];
					$transBalance = $amount["transAmount"];
					$agentComm = $amount["AgentComm"];
					$currecyFromLedgerAgent = $amount["currencyFrom"];
									  
				  if(DEST_CURR_IN_ACC_STMNTS == "1" || CONFIG_SETTLEMENT_AMOUNT_DISTRIBUTOR_ACCOUNT_STATEMENT == "1"){
	           $amountToLedgerDist = $amount["localAmount"];
	           $currecyToLedgerDist = $amount["currencyTo"];
             }else{		
             $amountToLedgerDist = $amount["transAmount"];
             $currecyToLedgerDist = $amount["currencyFrom"];
             }
				  
					if(CONFIG_EXCLUDE_COMMISSION == '1')
					{
						$agentBalance = $balance - $agentComm;
					}else{
						$agentBalance = $balance;
						}
					
					
					update("update " . TBL_TRANSACTIONS . " set transStatus= '" . $_POST["changeStatusTo"] . "', trackingNum = '". $_POST["trackingNum"]."', remarks = '". $_POST["remarks"]."', rejectDate = '".getCountryTime(CONFIG_COUNTRY_CODE)."' where transID='". $_GET["transID"]."'");
					$descript = "Transaction is Rejected";
					activities($_SESSION["loginHistoryID"],"UPDATION",$_GET["transID"],TBL_TRANSACTIONS,$descript);	
					if($amount["createdBy"] != 'CUSTOMER')
					{
						$payin = selectFrom("select payinBook, customerID from " . TBL_CUSTOMER . " where customerID = '". $amount["customerID"]."'");

						if($payin["payinBook"] == "" || CONFIG_PAYIN_CUSTOMER != "1" || CONFIG_SETTLEMENT_CURRENCY_DROPDOWN_OFF_ACCOUNT_ON_AnD=="1")
						{
							///
							if(CONFIG_EXCLUDE_COMMISSION == '1')
							{
								$agentBalance = $balance - $commission;
							}else{
								$agentBalance = $balance;
								}
							
							$agentContents = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from " . TBL_ADMIN_USERS . " where userID = '$agent'");
							$currentBalance = $agentContents["balance"];
							
							
							$currentBalance = $agentBalance + $currentBalance;
							update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalance where userID = '$agent'");
							
							////////////////Either to insert into Bank or A&D Account////////
								if($agentContents["agentType"]=="Sub")
								{
									$agent=$agentContents["parentID"];
									$q = updateAgentAccount($agent, $agentBalance, $_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
										 updateSubAgentAccount($agentContents["userID"], $agentBalance, $_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Agent");
								}else{
									$agent=$agentContents["userID"];
									$q = updateAgentAccount($agent, $agentBalance, $_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Agent",$note,$currecyFromLedgerAgent);
								}
								if(CONFIG_CREATE_ADMIN_STAFF_LEDGERS == "1")
									updateAdminStaffLedger($_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", $note, $currecyFromLedgerAgent);
													
						}elseif($payin["payinBook"] != "" && CONFIG_PAYIN_CUSTOMER == "1")
						{
						
								if(CONFIG_SAVE_RECIEVED_AMOUNT == 1)
								{
									$strAmountSql = "select amount from agents_customer_account where tranRefNo = ".$_GET["transID"];
									$arrAmountData = selectFrom($strAmountSql);
									$balance = $arrAmountData["amount"];
								}
									
							insertInto("insert into agents_customer_account(customerID ,Date ,payment_mode,Type ,amount,tranRefNo,modifiedBy) 
						 	values('".$payin["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','".$balance."','".$_GET["transID"]."','".$agent."'
						 	)");
						}
					}elseif($amount["createdBy"] == 'CUSTOMER')
					{
						$strQuery = "insert into  customer_account (customerID ,Date ,payment_mode,Type ,amount,tranRefNo) 
						 values('".$amount["customerID"]."','".getCountryTime(CONFIG_COUNTRY_CODE)."','Transaction is Rejected','DEPOSIT','$balance','".$amount["refNumberIM"]."'
						 )";
						 insertInto($strQuery);
					}
					
					if(CONFIG_POST_PAID != '1')
					{
						$checkBalance2 = selectFrom("select userID, balance, isCorrespondent, agentType, parentID from ".TBL_ADMIN_USERS." where userID = '".$bank."'");						
						
						$currentBalanceBank = $checkBalance2["balance"]	+ $transBalance;				
						update("update " . TBL_ADMIN_USERS . " set balance  = $currentBalanceBank where userID = '".$bank."'");
						
						////////////////Either to insert into Bank or A&D Account////////
						
						
			
						if($checkBalance2["agentType"]=="Sub")
						{
							$dist=$checkBalance2["parentID"];
							$q = updateAgentAccount($dist, $amountToLedgerDist,$_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
								 updateSubAgentAccount($checkBalance2["userID"], $transBalance, $_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Distributor");
						}else{
							$dist=$checkBalance2["userID"];
							$q = updateAgentAccount($dist, $amountToLedgerDist,$_GET["transID"], "DEPOSIT", "Transaction Rejected $teller", "Distributor",$note,$currecyToLedgerDist);
						}
						
						
					}
					callApiModif($sftp, $_REQUEST["transID"], $_arrCountries, $_arrPayoutAgentId);
				}
				if(CONFIG_EMAIL_ON_RELEASE == "1"){
					include ("emailSender.php");
					include ("emailBen.php");
				}else{
					include ("email.php");
				}
	}
	//debug("", true);
	redirect("release-trans.php?act=$get&message=$mess&dist=$_GET[dist]");
}
if($transID > 0)
{
	$contentTrans = selectFrom("select * from " . TBL_TRANSACTIONS . " where transID='".$transID."'");
	$createdBy = $contentTrans["createdBy"];
}
else
{
	redirect("release-trans.php?act=$get&dist=$_GET[dist]");
}

if($createdBy != 'CUSTOMER')
{


	/**
	 * On the confirm and the print receipt client want to show 
	 * the Passport ID number of the beneficiary
	 * @Ticket #4425
	 */
	if(defined("CONFIG_USE_ID_TYPE_OF_BENEFICIARY"))
	{
		$strPassportIdSql = "select id_number from user_id_types where user_id = '".$contentTrans["benID"]."' and user_type='B' and id_type_id=(select id from id_types where title='".CONFIG_USE_ID_TYPE_OF_BENEFICIARY."')";
		$arrBeneficiaryId = selectFrom($strPassportIdSql);
		$strBeneficiaryId = $arrBeneficiaryId["id_number"];
	}



?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../styles/admin.js"></script>
<script language="javascript">
	<!-- 
var custTitle = "Testing";
function SelectOption(OptionListName, ListVal)
{
	for (i=0; i < OptionListName.length; i++)
	{
		if (OptionListName.options[i].value == ListVal)
		{
			OptionListName.selectedIndex = i;
			break;
		}
	}
}


function checkSearch(form1)
{
	
	if(document.form1.atmCardNo.value == ""  &&  document.form1.changeStatusTo.value != 'cardRevoked')
	{
		alert("Please provide Provide ATM Card No.");
		//form1.atmCardNo.focus();
		return false;
	}
	
}

	// end of javascript -->
	</script>
<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<form name="form1" method="post" action="trans-details.php?transID=<? echo $_GET["transID"]?>&dist=<?=$_GET["dist"]?>&redirect='yes'" onSubmit="return checkSearch(this);">
 
<table width="103%" border="0" cellspacing="1" cellpadding="5"> 
  <tr>
    <td bgcolor="#C0C0C0"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
   
  	<tr>
		<td>
				<? 
					if($_GET["back"]=="release-trans")
					{?>
					<a class="style2" href="release-trans.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&act=<? echo $_GET["act"]?>&dist=<?=$_GET["dist"]?> &sbname=<? echo $sbname ?>">Go Back</a>
					<? }elseif($_GET["back"]=="cancel_Transactions"){?>
				 <a class="style2" href="cancel_Transactions.php?transType=<? echo $_GET["type"];?>&searchBy=<? echo $_GET["by"];?>&transID=<? echo $_GET["id"];?>&submit=Search&act=<? echo $_GET["act"]?>">Go Back</a>
				 	<? } ?>
		</td>
	</tr>
		<? if ($_GET["msg"] != "" && CONFIG_DISTRIBUTION_DEAL_SHEET == '1'){ ?>
          <tr bgcolor="#EEEEEE" align="center">
            <td bgcolor="#EEEEEE">
            	 
                
                 <font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>
                 <? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_GET["msg"]."</b><br><br></font>"; ?>
                </td>
              
          </tr>
          <? } ?>
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table border="0">
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="196" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="153" align="right"><font color="#005b90">Status</font></td>
                <td width="181"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="220" height="20" align="right"><font class="style2"><? if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_DIST_MANUAL_REF_NUMBER != "1"){ echo CONFIG_DIST_REF_NAME; }else{ echo $systemCode;} ?> </font></td>
                <td width="196" height="20">
					<? if(SYSTEM != "cpexpress") { ?> <strong> <? } ?>
						<? 
							if(CONFIG_DIST_REF_NUMBER == '1' && CONFIG_DIST_MANUAL_REF_NUMBER != "1")
								echo $contentTrans["distRefNumber"]; 
							else
								echo $contentTrans["refNumberIM"];  
						?>
					<? if(SYSTEM != "cpexpress") { ?></strong><? } ?>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90"><? echo $manualCode; ?> </font> </td>
                <td width="181" height="20" <? if(SYSTEM == "cpexpress") { ?> style="font-size:14px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; " <? } ?>><? echo $contentTrans["refNumber"]; ?></td>
              </tr>
              <tr>
                <td width="146" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="196" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="153" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="181" height="20"><? 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$authoriseDate"));				
				?></td>
              </tr>			  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table border="0">
                <? 
			 
			 if(CONFIG_SHARE_OTHER_NETWORK == '1')
       {    
        $sharedTrans = selectFrom("select * from ".TBL_SHARED_TRANSACTIONS." where localTrans = '".$transID."' and generatedLocally = 'N'");   
       }   
			if(CONFIG_SHARE_OTHER_NETWORK == '1' && $sharedTrans["remoteTrans"] != '')
			{
				$jointClient = selectFrom("select * from ".TBL_JOINTCLIENT." where clientId = '".$sharedTrans["remoteServerId"]."'");	
				$otherClient = new connectOtherDataBase();
				$otherClient-> makeConnection($jointClient["serverAddress"],$jointClient["userName"],$jointClient["password"],$jointClient["dataBaseName"]);
				
				$customerContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".customer where customerID = '".$contentTrans["customerID"]."'");		
				$benificiaryContent = $otherClient->selectOneRecord("select * from ".$jointClient["dataBaseName"].".beneficiary where benID = '".$contentTrans["benID"]."'");		
					
				$otherClient->closeConnection();		
							dbConnect();						
			}else{
			 
			  $queryCust = "select customerID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email  from ".TBL_CUSTOMER." where customerID ='" . $contentTrans[customerID] . "'";
				$customerContent = selectFrom($queryCust);
				
				$queryBen = "select *  from ".TBL_BENEFICIARY." where benID ='" . $contentTrans["benID"] . "'";
				$benificiaryContent = selectFrom($queryBen);
			}
			if($customerContent["customerID"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["firstName"] . " " . $customerContent["middleName"] . " " . $customerContent["lastName"] ?></td>
                </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				
                <tr>
                  <td width="150" align="right"><font color="#005b90">Address</font></td>
                  <td colspan="3"><? echo $customerContent["Address"] . " " . $customerContent["Address1"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Postal / Zip Code</font></td>
                  <td width="200"><? echo $customerContent["Zip"]?></td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Phone</font></td>
                  <td width="200"><? echo $customerContent["Phone"]?></td>
                  <td width="100" align="right"><font color="#005b90">Email</font></td>
                  <td width="224"><? echo $customerContent["Email"]?></td>
                </tr>
			<?
				}
				else{
				?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">&nbsp;</font></td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="224"><? echo $customerContent["Country"]?></td>
                </tr>				
				<?
				}
			  }
			  ?>
              </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Beneficiary Agent Details</legend>
            <table border="0">
              <? 
			$querysenderAgent = "select *  from ".TBL_ADMIN_USERS." where userID ='" . $contentTrans["benAgentID"] . "'";
			$senderAgentContent = selectFrom($querysenderAgent);
			if($senderAgentContent["userID"] != "")
			{
		?>
              <tr>
                <td width="150" align="right"><font color="#005b90">Agent Name</font></td>
                <td><? echo $senderAgentContent["name"]?> </td>
                <td align="right"><font color="#005b90">Contact Person </font></td>
                <td><? echo $senderAgentContent["agentContactPerson"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Address</font></td>
                <td colspan="3"><? echo $senderAgentContent["agentAddress"] . " " . $senderAgentContent["agentAddress2"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Company</font></td>
                <td width="200"><? echo $senderAgentContent["agentCompany"]?></td>
                <td width="100" align="right"><font color="#005b90">Country</font></td>
                <td width="222"><? echo $senderAgentContent["agentCountry"]?></td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90">Phone</font></td>
                <td width="200"><? echo $senderAgentContent["agentPhone"]?></td>
                <td width="100" align="right"><font color="#005b90">Email</font></td>
                <td width="222"><? echo $senderAgentContent["email"]?></td>
              </tr>
              <?
			  }
			  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
            <table border="0" bordercolor="#006600">
              <? 
		
			
			if($benificiaryContent["benID"] != "")
			{
				if(defined("CONFIG_USE_ID_TYPE_OF_BENEFICIARY"))
				{
					$strIdType = CONFIG_USE_ID_TYPE_OF_BENEFICIARY;
					$strIdVal = $strBeneficiaryId;
				}
				else
				{
					$strIdType = $benificiaryContent["IDType"];
					$strIdVal = $benificiaryContent["IDNumber"];
				}
		?>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                <td width="186" ><? echo $benificiaryContent["firstName"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">Last Name</font></td>
                <td width="196"><? echo  $benificiaryContent["lastName"] ?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Address</font></td>
                <td width="186" ><input type="text" name="Address" value=<? echo $benificiaryContent["Address"]  ?>></td>
                
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="186" height="20"><input type="text" name="Phone" value=<? echo $benificiaryContent["Phone"]?>></td>
                <td width="138" align="right"><font color="#005b90">Email</font></td>
                <td width="196"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">ID Description </font></td>
                <td width="186" ><input type="text" name="IDType" value=<? echo $strIdType ?>></td>
                <td width="138" height="20" align="right"><font class="style2">ID Serial No.</font></td>
                <td width="196"><input type="text" name="IDNumber" value=<? echo $strIdVal ?>></td>
              </tr>
              <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
					$bolShowIbanDetail = false;
		  			$benBankDetails = selectFrom("select * from bankDetails where transID='".$transID."'");
					if(defined('CONFIG_IBAN_OR_BANK_TRANSFER') && CONFIG_IBAN_OR_BANK_TRANSFER=="1" && !empty($benBankDetails["IBAN"]))
						$bolShowIbanDetail = true;
					?>
              <tr> 
                <td width="156" height="20" class="style2">Beneficiary Bank Details </td>
                <td width="186" height="20">&nbsp;</td>
                <td width="138" height="20">&nbsp;</td>
                <td width="196" height="20">&nbsp;</td>
              </tr>
			<? if($bolShowIbanDetail) { ?>
				<tr>
					<td width="156" align="right"><font color="#005b90">IBAN</font></td>
					<td width="186"><? echo $benBankDetails["IBAN"]?></td>
					<td width="138" align="right"><font color="#005b90"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? "Remarks" : "&nbsp;") ?></font></td>
					<td width="196"><? echo ($benificiaryContent["Country"] == "GEORGIA" ? $_SESSION["ibanRemarks"] : "&nbsp;") ?></td>
				</tr>
			<? }else{ ?>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Bank 
                  Name</font></td>
                <td width="186" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                <td width="138" align="right"><font color="#005b90">Acc Number</font></td>
                <td width="196"><? echo $benBankDetails["accNo"]; ?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Branch 
                  Code</font></td>
                <td width="186" height="20"><? echo $benBankDetails["branchCode"]; ?> 
                </td>
                <td width="138" align="right"><font color="#005b90"> 
                 <?
												if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												 	echo $benBankDetails["ABACPF"];
								                     
												}
												?>
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Branch 
                  Address</font> </td>
                <td width="186" height="20"><? echo $benBankDetails["branchAddress"]; ?> 
                </td>
                <?
                if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
                {?>
                	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
                  <td width="200"><?=$benBankDetails["accountType"]?> </td>
                <? 
                }else{
                ?>	
                <td width="138" height="20"  align="right"><font color="#005b90">&nbsp; 
                  </font></td>
                <td width="196" height="20">&nbsp; 
                  <!--echo $benBankDetails["IBAN"];-->
                </td>
                <?
              }
                ?>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="186" height="20">&nbsp;
                  <!-- echo $benBankDetails["swiftCode"]; -->
                </td>
                <td width="138" height="20" align="right" title="For european Countries only">&nbsp;</td>
                <td width="196" height="20" title="For european Countries only">&nbsp;</td>
              </tr>
              <?
		}
  }
  			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			
		$queryDistributor = "select name  from " .TBL_ADMIN_USERS. " where  userID  ='" . $senderAgentContent["cp_ida_id"] . "'";
			$queryExecute = selectFrom($queryDistributor);
					?>
              <tr> 
                <td colspan="4" class="style2">Collection Point Details </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Collection point name</font></td>
                <td width="186" height="20"><? echo $senderAgentContent["cp_corresspondent_name"]; ?></td>
                <td width="138" align="right"><font class="style2">Contact Person</font></td>
                <td width="196"  valign="top"><strong><? echo $senderAgentContent["cp_contact_person_name"]; ?></strong> 
                </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">Distributor </font></td>
                <td width="186" height="20"><strong><? echo $queryExecute["name"]; ?></strong>
                </td>
                <td width="138" align="right"><font class="style2">Address</font></td>
				<td width="196" colspan="3"><strong><? echo $senderAgentContent["cp_branch_address"]?></strong></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">City </font> </td>
                <td width="186" height="20"><strong><? echo $senderAgentContent["cp_city"]; ?></strong> 
                </td>
                <td width="138" align="right"><font class="style2">Country</font></td>
                <td width="196"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>
              </tr>
              <tr> 
                <td width="156" align="right"><font class="style2">Phone</font></td>
                <td width="186"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
                <td width="138" height="20" align="right"><font class="style2">Fax</font></td>
                <td width="196" height="20"><strong><?  echo $senderAgentContent["cp_fax"]; ?></strong>
                </td>
              </tr>
              <?
  }
  ?>
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><font color="#005b90"><? echo $manualCode; ?></font></font></td>
                <td height="20" width="200" <? if(SYSTEM == "cpexpress") { ?> style="font-size:14px; font-weight:bold; font-family:Verdana, Arial, Helvetica, sans-serif; " <? } ?>><? echo $contentTrans["refNumber"]?> </td>
                	<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT != "1" && ($agentType == "SUPI" || $agentType == "SUBI")){ ?>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="225" height="20" ><? echo $contentTrans["transAmount"]." in ".$contentTrans["currencyFrom"];?>  </td>
                <? } ?>
              </tr>
			  <tr>
			  		<? if(CONFIG_SHOW_ONLY_LOCAL_AMOUNT != "1" && ($agentType == "SUPI" || $agentType == "SUBI")){ ?>
                <td width="150" height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td width="200" height="20"><? echo $contentTrans["exchangeRate"]?>                  </td>
                <? } ?>
                <td width="100" height="20" align="right"><font color="#005b90">Local Amount</font></td>
                <td width="225" height="20"><strong><? 
	
																	
				 echo $contentTrans["localAmount"]?>
								<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>

				              </strong>  </td>
			  </tr>
				<?
				if($isCorrespondent == 'N' && $adminType != 'Agent')
				{
				?>				  
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
                <td width="200" height="20"><? echo $contentTrans["IMFee"]?></td>
                <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
                <td width="225" height="20"><? echo $contentTrans["totalAmount"]?></td>
              </tr>
			  <?
			  }			  
			  ?>
			  <? if($contentTrans["transType"] == "Bank Transfer"){
            	?>
        	<tr> 
          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
                <td width="200" height="20"><? echo $contentTrans["transactionPurpose"]?>
                </td>
                <td width="100" align="right"><font color="#005b90">Funds Sources</font></td>
                                <td width="225"><? echo $contentTrans["fundSources"]?>
                </td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td width="200" height="20"><? echo $contentTrans["moneyPaid"]?>
                </td>
     <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                                <td width="225">&nbsp;<!-- echo $contentTrans["bankCharges"]-->
                </td>
              </tr>
              <tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?></font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>	
                 <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>	                         
              </tr>
              
               <? if(CONFIG_IDTYPE_PASSWORD == '1'&& $benificiaryContent["otherId"]!= "")
					  {
					  ?>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Password</font></td>
                <td width="200" height="20"><? echo $contentTrans["benIDPassword"];?></td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
              <? } ?>
            </table>
            </fieldset></td>
          </tr>
<?

//$sambaCheck = selectFrom("select * from sambaida_check");
//if($sambaCheck["IDA_id"] != $userID)
{	
$get=$_GET["act"];
	if($_GET["act"]=="" || $_GET["act"] == "authorize")
	{
		//debug($_GET["act"]);
		$_GET["act"]="amend";
		//debug($_GET["act"]);
	}
	
	if($_GET["act"]=="amend" || $_GET["act"]=="cancel")
	{
?>		  
		  
		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
			
				<? 
				if(CONFIG_SECRET_QUEST_ENABLED){	
					if ($contentTrans["question"]!=""){				
				?>
				<tr>
					<td width="150" align="right"><font color="#005b90">Secret Question </font></td>
          <td width="150" height="20" align="left" valign="top"><? echo $contentTrans["question"];?></td>
         
        </tr>
        <tr>
         <td width="150" align="right"><font color="#005b90">Answer is Compulsory to Release a Transaction. </font></td>
          <td width="196" align="left"><input type="text" name="answer"  id="answer"></td>
        </tr>
        <?
      }}
        ?>
              <tr> 
                 	<?
                	if($contentTrans["transType"] == "ATM Card"){
                	?>
                		<td>
                			<table width="100%">		
                	<?
                		$atmInfo = selectFrom("select cID, expiryDate, cardNo, cardName from cm_cust_credit_card where customerID = '".$contentTrans["benID"]."'"); 
                		
                		if($atmInfo["cID"] != ""){ 
                			if($atmInfo["expiryDate"] <= $limit_date){
                			?>
                					
                					<tr><td colspan="2"><font color="#005b90"><b>Your ATM card has expired please provide new ATM card Information.</b></font></td></tr>
	                				<tr><td width="120"><font color="#005b90">&nbsp;</td></tr>
	                				<tr>
	                   				<td height="20" colspan="2"><span class="style2">Old ATM Card Information</span></td>
	                				</tr>
	                				<tr><td width="120"><font color="#005b90">Card Name </font></td><td><? echo $atmInfo["cardName"];?></td></tr>
		              				<tr><td width="120"><font color="#005b90">Card Number</font></td><td><? echo $atmInfo["cardNo"];?></td></tr>
		              				<tr><td width="120"><font color="#005b90">Expiry Date</font></td><td><? echo $atmInfo["expiryDate"];?></td></tr>
	                				<tr><td width="120"><font color="#005b90">&nbsp;</td></tr>
	                				<tr>
	                   				<td height="20" colspan="2"><span class="style2">New ATM Card</span></td>
	                				</tr>
	                				<tr><td width="120"><font color="#005b90">ATM Card Number</font><font color="#ff0000">*</font> </td><td><input type="text" name="atmCardNo"  id="atmCardNo" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	                				<tr><td width="120"><font color="#005b90">Expiry Date</font> </td><td><input type="text" name="expDate"  id="expDate" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	                				<tr><td width="120"><font color="#005b90">PIN </font></td><td><input type="text" name="PIN"  id="PIN" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	                		<?
                			}else{
                			?>
		                			<tr><td width="120"><font color="#005b90">Card Name </font></td><td><? echo $atmInfo["cardName"];?></td></tr>
		              				<tr><td width="120"><font color="#005b90">Card Number</font></td><td><? echo $atmInfo["cardNo"];?></td></tr>
		              				<tr><td width="120"><font color="#005b90">Expiry Date</font></td><td><? echo $atmInfo["expiryDate"];?></td></tr>
		              	<?
               				} 
               			}else{
               			?>
               					<tr><td width="120"><font color="#005b90">ATM Card Number</font><font color="#ff0000">*</font> </td><td><input type="text" name="atmCardNo"  id="atmCardNo" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	                			<tr><td width="120"><font color="#005b90">Expiry Date</font> </td><td><input type="text" name="expDate"  id="expDate" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	                			<tr><td width="120"><font color="#005b90">PIN </font></td><td><input type="text" name="PIN"  id="PIN" <? if($_POST["changeStatusTo"] == "cardRevoked"){echo "readonly"; } ?>></td></tr>
	              <?		
               			}
               	?>
               			</table>
								<?               
               	}else{ ?>
               		<td width="150" height="20" align="right">
                	Change
                	<? } ?>
                	</td>
                	
                	<? if($contentTrans["transType"] == "ATM Card"){ ?>
                	
                		</tr>
                		<tr>
                	
                	<? } ?>
                <td height="20" align="left">
                	
 											               		
                		
                		
               
                <select name="changeStatusTo" class="flat" id="changeStatusTo" <? if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1'){ ?>onChange="document.form1.action='trans-details.php?transID=<? echo $_GET["transID"]?>&dist=<?=$_GET["dist"]?>'; document.form1.submit();"<? } ?>>
                <!-- <td height="20" align="left"><select name="changeStatusTo" class="flat" id="changeStatusTo" onChange="document.form1.action='trans-details.php?transID=<? echo $_GET["transID"]?>&dist=<?=$_GET["dist"]?>'; document.form1.submit();">	-->
					<?
					if($contentTrans["transType"] == "Pick up")
					{ 
						if($_GET["act"]=="amend")
						{
							?>
							
							  <option value="Picked up">Picked up</option>
							  
							  
							  
							  <?
							  if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
							  {
							  echo("
								  <option value='Failed'>Failed</option>
								  <option value='Suspicious'>Suspicious</option>
								  <option value='Rejected'>Rejected</option>");
							
							}
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Picked up">Picked up</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Bank Transfer")
					{
						if($_GET["act"]=="amend")
						{

						?>
						  <option value="Credited">Credited</option>
						  <?
						  if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
						  {
						  echo("
							  <option value='Failed'>Failed</option>
							  <option value='Suspicious'>Suspicious</option>
							   <option value='Rejected'>Rejected</option>");
						
							}
						}
						elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Credited">Credited</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}	
					
					
					elseif($contentTrans["transType"] == "ATM Card")
					{
						
						?>
							  <!--option value="Credited">Credited</option-->
							  <option value="CardDispatch">Card Dispatch</option>
							  <option value="cardRevoked">Card Revoked</option>
							
						<?
						
					}	
					
					
					
					elseif($contentTrans["transType"] == "Home Delivery")
					{
						if($_GET["act"]=="amend")
						{
						?>
						  <option value="Delivered">Delivered</option>
						  <option value="Out for Delivery">Out for Delivery</option>
						  <option value="Sent By Courier">Sent By Courier</option>					  
						  <?
						  if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
						  {
						  echo("
							  <option value='Failed'>Failed</option>
							  <option value='Suspicious'>Suspicious</option>
							  <option value='Rejected'>Rejected</option>");
						
							}
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <!--option value="Delivered">Delivered</option-->
							  <option value="Reject Cancel">Reject Cancel</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					if($_POST["changeStatusTo"] !=  "")
					{
					?>
					<!-- <option  selected><?=$_POST["changeStatusTo"]; ?></option> -->
					<?
					}					
					?>
                </select><script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script>
                	
                	
                
                	
                	
                	
                	</td>
              </tr><?
				  if($_POST["changeStatusTo"] == "Sent By Courier")
				  {?>
              <tr>
                <td width="150" height="20" align="right">
				
				  	Courier Tracking Number 
				
                  </td>
                <td height="20" align="left"><input name='trackingNum' type='text'></td>
              </tr>
			  <?
			  
			  }
				  ?>
				  <?
				  if(($_POST["changeStatusTo"] == "Suspicious" || $_POST["changeStatusTo"] == "Rejected" || $_POST["changeStatusTo"] == "On Hold" || $_POST["changeStatusTo"]=="Cancelled" || $_POST["changeStatusTo"]=="Reject Cancel")  || CONFIG_ID_MANDATORY == 1)
				  {?>
              <tr>
                <td width="150" height="20" align="right" valign="top">Transaction Release Remarks </td>
                <td width="196"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			  <?
			  }
			  
			  ?>
              <tr align="center">
                  <td height="20" colspan="2"> <input name="Submit" type="submit" class="flat" value="Submit"></td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
        </table></td>
    </tr>
<?
	}
}
?>
</table>
</body>
</html>
<?
}else{
?>
<html>
<head>
<title>View Transaction Details</title>
<script language="javascript" src="./javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<script language="javascript" src="./styles/admin.js"></script>

<style type="text/css">
<!--
.style2 {
	color: #6699CC;
	font-weight: bold;
}
.style3 {color: #990000; font-weight: bold; }
.style4 {color: #6699cc}
-->
    </style>	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body>
<table width="103%" border="0" cellspacing="1" cellpadding="5">
  <form name="form1" method="post" action="trans-details.php?transID=<? echo $_GET["transID"]?>&dist=<?=$_GET["dist"]?>&redirect='yes'">
   
  <tr>
    <td bgcolor="#6699cc"><b><font color="#FFFFFF" size="2">View transaction details.</font></b></td>
  </tr>
  
    <tr>
      <td align="center"><br>
        <table width="700" border="0" bordercolor="#FF0000">
          <tr>
            <td><fieldset>
            <legend class="style2">Transaction Details </legend>
            <table width="650" border="0">
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Type</font></td>
                <td width="200" height="20"><? echo $contentTrans["transType"]?></td>
                <td width="100" align="right"><font color="#005b90">Status</font></td>
                <td width="200"><? echo $contentTrans["transStatus"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font class="style2"><? echo $systemCode; ?> </font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["refNumberIM"]?></strong>
                </td>
                <td width="100" height="20" align="right"><font color="#005b90"><? echo $manualCode; ?></font></td>
                <td width="200" height="20"><? echo $contentTrans["refNumber"]?></td>
              </tr>
              <tr>
                <td width="150" height="20" align="right"><font color="#005b90">Transaction Date </font></td>
                <td width="200" height="20"><? 
				$TansDate = $contentTrans['transDate'];
				if($TansDate != '0000-00-00 00:00:00')
					echo date("F j, Y", strtotime("$TansDate"));
				
				?>
                </td>
                <td width="150" height="20" align="right"><font color="#005b90">Authorization Date </font></td>
                <td width="150" height="20"><? 
				 
				$authoriseDate = $contentTrans['authoriseDate'];
				if($authoriseDate != '0000-00-00 00:00:00')
				{
					if($authoriseDate != '0000-00-00 00:00:00')
						echo date("F j, Y", strtotime("$authoriseDate"));				
				}
				?></td>
              </tr>				  
            </table>
            </fieldset></td>
          </tr>
          <tr>
            <td align="center"><fieldset>
            <legend class="style2">Sender Company</legend>
            <br>
            <table border="0" align="center">
              <tr>
                <td colspan="4" align="center" bgcolor="#D9D9FF"><img height="<?=CONFIG_LOGO_HEIGHT?>" alt="" src="<?=CONFIG_LOGO?>" width="<?=CONFIG_LOGO_WIDTH?>"></td>
              </tr>
            </table>
            <br>
            </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
              <legend class="style2">Sender Details </legend>
              <table width="650" border="0">
                <? 
		
			$queryCust = "select * from cm_customer where c_id ='" . $contentTrans["customerID"] . "'";
			$customerContent = selectFrom($queryCust);
			if($customerContent["c_id"] != "")
			{
		?>
                <tr>
                  <td width="150" align="right"><font color="#005b90">Sender Name</font></td>
                  <td colspan="3"><? echo $customerContent["FirstName"] . " " . $customerContent["MiddleName"] . " " . $customerContent["LastName"] ?></td>
                </tr>
                <tr>
                  <td width="150" align="right">&nbsp;</td>
                  <td width="200">&nbsp;</td>
                  <td width="100" align="right"><font color="#005b90">Country</font></td>
                  <td width="200"><? echo $customerContent["c_country"]?></td>
                </tr>
                <?
			  }
			  ?>
            </table>
              </fieldset></td>
          </tr>
		  			  
          <tr>
            <td><fieldset>
              <legend class="style2">Beneficiary Details </legend>
                    <table width="650" border="0" bordercolor="#006600">
                      <? 
		
			$queryBen = "select benID, Title, firstName, lastName, middleName, Address, Address1, City, State, Zip,Country, Phone, Email, IDType, NICNumber  from cm_beneficiary where benID ='" . $contentTrans["benID"] . "'";
			$benificiaryContent = selectFrom($queryBen);
			if($benificiaryContent["benID"] != "")
			{
		?>
               <tr> 
                <td width="156" height="20" align="right"><font class="style2">Beneficiary Name</font></td>
                <td width="186" ><? echo $benificiaryContent["firstName"] ?></td>
                <td width="138" height="20" align="right"><font class="style2">Last Name</font></td>
                <td width="196"><? echo  $benificiaryContent["lastName"] ?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Address</font></td>
                <td width="186" ><input type="text" name="Address" value=<? echo $benificiaryContent["Address"]  ?>></td>
                
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Postal 
                  / Zip Code</font></td>
                <td width="186" height="20"><? echo $benificiaryContent["Zip"]?></td>
                <td width="138" align="right"><font color="#005b90">Country</font></td>
                <td width="196"><? echo $benificiaryContent["Country"]?> </td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font color="#005b90">Phone</font></td>
                <td width="186" height="20"><input type="text" name="Phone" value=<? echo $benificiaryContent["Phone"]?>></td>
                <td width="138" align="right"><font color="#005b90">Email</font></td>
                <td width="196"><? echo $benificiaryContent["Email"]?></td>
              </tr>
              <tr> 
                <td width="156" height="20" align="right"><font class="style2">ID Description </font></td>
                <td width="186" ><input type="text" name="IDType" value=<? echo $benificiaryContent["IDType"] ?>></td>
                <td width="138" height="20" align="right"><font class="style2">ID Serial No.</font></td>
                <td width="196"><input type="text" name="NICNumber" value=<? echo $benificiaryContent["NICNumber"] ?>></td>
              </tr>
                      <?
				}
			  if($contentTrans["transType"] == "Bank Transfer")
			  {
		  			$benBankDetails = selectFrom("select * from cm_bankdetails where benID='".$contentTrans["benID"]."'");
					?>
                      <tr>
                        <td colspan="4" class="style2">Beneficiary Bank Details </td>
                        
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Bank Name</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["bankName"]; ?></td>
                        <td width="100" align="right"><font color="#005b90">Acc Number</font></td>
                        <td width="200"><? echo $benBankDetails["accNo"]; ?>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Code</font></td>
                        <td width="200" height="20"><? echo $benBankDetails["branchCode"]; ?>                        </td>
                        <td width="100" align="right"><font color="#005b90">
                                   
												<?
												if(CONFIG_CPF_ENABLED == '1')
												{
													echo  "CPF Number*";
												}
												?>
								          &nbsp; </font></td>
								                        <td width="200"><?
												if(CONFIG_CPF_ENABLED == '1')
												{
												 	echo $benBankDetails["ABACPF"];
								                     
												}
												?>
												</td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font color="#005b90">Branch Address</font> </td>
                        <td width="200" height="20"><? echo $benBankDetails["branchAddress"]; ?>                        </td>
												<?
				                if(CONFIG_ACCOUNT_TYPE_ENABLED == '1')
				                {?>
				                	<td width="150" align="right"><font color="#005b90">Account Type</font></td>
				                  <td width="200"><?=$benBankDetails["accountType"]?> </td>
				                <? 
				                }else{
				                ?>	
				                <td width="138" height="20"  align="right"><font color="#005b90">&nbsp; 
				                  </font></td>
				                <td width="196" height="20">&nbsp; 
				                  <!--echo $benBankDetails["IBAN"];-->
				                </td>
				                <?
				              }
				                ?>

                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><!--font color="#005b90">Swift Code</font--></td>
                        <td width="200" height="20"> &nbsp;<? // echo $benBankDetails["swiftCode"]; ?>                        </td>
                        <td width="100" height="20" align="right" title="For european Countries only">&nbsp;</td>
                        <td width="200" height="20" title="For european Countries only">&nbsp;</td>
                      </tr>
 <?
  }

			  if($contentTrans["transType"] == "Pick up")
			  {
					$benAgentID = $contentTrans["benAgentID"];
					$collectionPointID = $contentTrans["collectionPointID"];
					$queryCust = "select *  from cm_collection_point where  cp_id  = $collectionPointID";
					$senderAgentContent = selectFrom($queryCust);			

					?>
                      <tr>
                        <td colspan="4" class="style2">Collection Point Details </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Agent Name</font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong></td>
                        <td width="100" align="right"><font class="style2">Address</font></td>
                        <td width="200" rowspan="2" valign="top"><strong><? echo $senderAgentContent["cp_branch_address"]; ?></strong>                        </td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Contact Person </font></td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong>                        </td>
						<td width="150" align="right"><font class="style2">Branch Code</font></td>
						<td width="200" colspan="3"><strong><? echo $senderAgentContent["cp_ria_branch_code"]?></strong></td>
                      </tr>
                      <tr>
                        <td width="150" height="20" align="right"><font class="style2">Compnay </font> </td>
                        <td width="200" height="20"><strong><? echo $senderAgentContent["cp_corresspondent_name"]; ?></strong>                        </td>
						<td width="100" align="right"><font class="style2">Country</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_country"]?></strong></td>                       
                      </tr>
					  <tr> 
						<td width="150" align="right"><font class="style2">Phone</font></td>
						<td width="200"><strong><? echo $senderAgentContent["cp_phone"]?></strong></td>
					    <td width="100" height="20" align="right"><font class="style2">City</font></td>
                        <td width="200" height="20"><strong><?  echo $senderAgentContent["cp_city"]; ?></strong></td>
					  </tr>
					  
 <?
  }
  ?>
            </table>
              </fieldset></td>
          </tr>
          <tr>
            <td><fieldset>
            <legend class="style2">Amount Details </legend>
            
              <table width="650" border="0">
              
              <tr>
                <td height="20" align="right"><font color="#005b90">Exchange Rate</font></td>
                <td height="20"><? echo $contentTrans["exchangeRate"]?> </td>
                <td width="100" height="20" align="right"><font color="#005b90">Amount</font></td>
                <td width="200" height="20" ><? echo $contentTrans["transAmount"]?>                </td>
              </tr>
             
			  <tr>
			  
			    <td height="20" align="right"><font color="#005b90"><? if(CONFIG_FEE_DEFINED == '1'){echo(CONFIG_FEE_NAME);}else{ echo ("$systemPre Fee");}?></font></td>
			    <td height="20"><? echo $contentTrans["IMFee"]?> </td>
			  
                <td width="100" height="20" align="right"><font class="style2">Local Amount</font></td>
                <td width="200" height="20"><strong><? echo $contentTrans["localAmount"]?>     				<? 
				  if(is_numeric($contentTrans["currencyTo"]))
				  	  echo "";
				  else
				  	  echo "in ".$contentTrans["currencyTo"]; 
				  ?>
           </strong></td>
			  </tr>
			  
          <tr>
            <td height="20" align="right"><font color="#005b90">Transaction Purpose</font></td>
            <td height="20"><? echo $contentTrans["transactionPurpose"]?> </td>
           
            <td width="100" height="20" align="right"><font color="#005b90">Total Amount</font></td>
            <td width="200" height="20"><? echo $contentTrans["totalAmount"]?> </td>
            
         </tr>
         <? if($contentTrans["transType"] == "Bank Transfer"){
            	?>
        	<tr> 
	          <td width="150" height="20" align="right"><font color="#005b90">Bank Charges</font></td>
	          <td width="200" height="20"><? echo $contentTrans["bankCharges"]?> </td>
	          <td width="100" height="20" align="right"><font color="#005b90">&nbsp;</font></td>
	          <td width="200" height="20">&nbsp;</td>
        </tr>
        	<?
        }
        	?>
              <tr>
                <td height="20" align="right"><font color="#005b90">Money Paid</font></td>
                <td height="20"><? echo $contentTrans["moneyPaid"]?> </td>
                <td width="100" align="right"><!--font color="#005b90">Bank Charges</font-->&nbsp;</td>
                                <td width="200"><? //echo $contentTrans["bankCharges"]?>&nbsp;
                </td>
              </tr>
			  			<tr>
                <td width="150" align="right"><font color="#005b90"><? if (CONFIG_REMARKS_ENABLED) echo(CONFIG_REMARKS); else echo 'Tip'; ?> </font></td>
                <td align="left"><? echo $contentTrans["tip"]?></td>
                 <? if(CONFIG_CURRENCY_CHARGES == '1')
			  		{?>
					  <td align="right"><font color="#005b90"> Currency Charges </font></td>
					  <td align="left"><?=$contentTrans["outCurrCharges"]?></td>
					  <? }else{
					   ?>
					   <td>&nbsp;</td>
					  <td>&nbsp;</td>			  
					  <? 
					  }
					  ?>	           	              
              </tr>
              <tr>
                <td width="150" height="20" align="right">&nbsp;</td>
                <td width="200" height="20">&nbsp;</td>
                <td width="100" align="right"><font color="#005b90">&nbsp;</font></td>
                <td width="200">&nbsp;</td>
              </tr>
            </table>
            </fieldset></td>
          </tr>
<?

//$sambaCheck = selectFrom("select * from sambaida_check");
//if($sambaCheck["IDA_id"] != $userID)
{
	$get=$_GET["act"];
	if($_GET["act"]=="")
	{
		$_GET["act"]="amend";
	}
	if($_GET["act"]=="amend")
	{
?>		  
          <tr>
            <td align="left"><fieldset>
            <legend class="style3">Change Transaction Status </legend>
            <br>
			<table border="0">
			
				<? 
				if(CONFIG_SECRET_QUEST_ENABLED){
					if ($contentTrans["question"]!=""){
				?>
				<tr>
					<td width="150" align="right"><font color="#005b90">Secret Question </font></td>
          <td width="150" height="20" align="left" valign="top"><? echo $contentTrans["question"];?></td>
         
        </tr>
        <tr>
         <td width="150" align="right"><font color="#005b90">Answer is Compulsory to Release a Transaction. </font></td>
          <td width="196" align="left"><input type="text" name="answer"  id="answer"></td>
        </tr>
				
        <?
      }}
        ?>
              <tr>
                <td width="150" height="20" align="right">Change</td>
                <td height="20" align="left"><select name="changeStatusTo" class="flat" id="changeStatusTo" <? if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1'){ ?> onChange="document.form1.action='trans-details.php?transID=<? echo $_GET["transID"]?>&dist=<?=$_GET["dist"]?>'; document.form1.submit();"<? }?>>
					<?
					if($contentTrans["transType"] == "Pick up")
					{
						
						if($_GET["act"]=="amend")
						{
							?>
							  <option value="Picked up">Picked up</option>
							  <?
						  if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
						  {
						  echo("
							  <option value='Failed'>Failed</option>
							  <option value='Suspicious'>Suspicious</option>
							  <option value='Rejected'>Rejected</option>");
							
							}
						}elseif($_GET["act"]=="cancel")
						{
						?>
						
							  <option value="Picked up">Picked up</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Bank Transfer")
					{
						if($_GET["act"]=="amend")
						{

						?>
						  <option value="Credited">Credited</option>
						 <?
						  if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
						  {
						  echo("
						  <option value='Failed'>Failed</option>
						  <option value='Suspicious'>Suspicious</option>
						  <option value='Rejected'>Rejected</option>"
						  );
						  
							}
						
						}
						elseif($_GET["act"]=="cancel")
						{
						?>
							  <option value="Credited">Credited</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					elseif($contentTrans["transType"] == "Home Delivery")
					{
						if($_GET["act"]=="amend")
						{
						?>
						  <option value="Delivered">Delivered</option>
						  <option value="Out for Delivery">Out for Delivery</option>
						  <option value="Sent By Courier">Sent By Courier</option>					  
						  <? if($agentType != "TELLER" || CONFIG_RELEASE_TRANS == '1')
						  {
						  echo("
						  <option value='Failed'>Failed</option>
						  <option value='Suspicious'>Suspicious</option>
						  <option value='Rejected'>Rejected</option>");
							}
						}elseif($_GET["act"]=="cancel")
						{
						?>
							  <option value="Delivered">Delivered</option>
							  <option value="Cancelled">Confirm Cancel</option>
							  
						<?
						}
					}
					if($_POST["changeStatusTo"] !=  "")
					{
					?>
					<option  selected><?=$_POST["changeStatusTo"]; ?></option>
					<?
					}
					?>					
                </select><script language="JavaScript">SelectOption(document.form1.changeStatusTo, "<?=$_POST["changeStatusTo"]; ?>");</script></td>
              </tr><?
				  if($_POST["changeStatusTo"] == "Sent By Courier")
				  {?>
              <tr>
                <td width="150" height="20" align="right">
				
				  	Courier Tracking Number 
				
                  </td>
                <td height="20" align="left"><input name='trackingNum' type='text'></td>
              </tr>
			  <?
			  
			  }
				  ?>
				  <?
				  if($_POST["changeStatusTo"] == "Suspicious" || $_POST["changeStatusTo"] == "Rejected" || $_POST["changeStatusTo"] == "On Hold")
				  {?>
              <tr>
                <td width="150" height="20" align="right" valign="top">Your Remarks </td>
                <td width="213"><textarea name="remarks" cols="30" rows="6" id="remarks"></textarea></td>
              </tr>
			  <?
			  
			  }
			  ?>
              <tr align="right">
                <td height="20" colspan="2">
                  <input name="Submit" type="submit" class="flat" value="Submit">
                  </td>
              </tr></form> 
            </table>
            
            <br>
            </fieldset></td>
          </tr>
		  
		  
        </table></td>
    </tr>
<?
  }
		
	
}
?>
</table>

</body>
</html>
<?
}}
else{ 
	
redirect("main.php");
}
?>