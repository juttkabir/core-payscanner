<?php
	session_start();
	
	include ("../include/config.php");
	include ("security.php");

	$agentType = getAgentType();
	$userID  = $_SESSION["loggedUserData"]["userID"];
    $date_time = date('Y-m-d  h:i:s');
	$strMainCaption = "Manage Chart of Account"; 

	$cmd = $_REQUEST["cmd"];

	if(!empty($cmd))
	{
		$bolDataSaved = false;
		/* Request to store or update the data of company */
		if($cmd == "ADD")
		{
			/**
			 * If the Id is empty than the record is new one
			 */
			$strCreatedBy = $userID."|".$agentType;
			$strInsertSql = "insert into accounts_chart 
							(
							accountName,
							accountNumber,
							description,
							createdBy,
							created
							)
							 values
								(
								'".$_REQUEST["accountName"]."',
								'".$_REQUEST["accountNumber"]."',
								'".$_REQUEST["description"]."',
								'".$strCreatedBy."',
								'".$date_time."'
								)";
				//debug($strInsertSql);			
				if(mysql_query($strInsertSql))
				$bolDataSaved = true;
			else
				$bolDataSaved = false;

			$cmd = "";
			$cID = "";
			
		}
		if($cmd == "UPDATE")
		{
			/**
			 * Record is not new and required the updation
			 */
			if(!empty($_REQUEST["cID"]))
			{
				$strUpdateSql = "update accounts_chart 
								 set
									accountName = '".$_REQUEST["accountName"]."',
									accountNumber = '".$_REQUEST["accountNumber"]."',
									description = '".$_REQUEST["description"]."',
									createdBy = '".$userID."',
									updated = '".$date_time."'
								 where
									id = '".$_REQUEST["cID"]."'	"; 
									
				if(update($strUpdateSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
					
				$cID = "";
				$cmd = "";
						
			}	
		}
		
		
		
		if($cmd == "EDIT")
		{
		
			if(!empty($_REQUEST["cID"]))
			{
				$strGetDataSql = "select * from accounts_chart  where id='".$_REQUEST["cID"]."'";
				$arrCompanyData = selectFrom($strGetDataSql);
													
				$accountName = $arrCompanyData["accountName"];
				$accountNumber = $arrCompanyData["accountNumber"];
				$description = $arrCompanyData["description"];										
				$cmd = "UPDATE";
				$cID = $_REQUEST["cID"];
				}
		}
	
		if($cmd == "DEL")
		{
			if(!empty($_REQUEST["cID"]))
			{
				$strDelSql = "delete from accounts_chart where id='".$_REQUEST["cID"]."'";
				if(mysql_query($strDelSql))
					$bolDataSaved = true;
				else
					$bolDataSaved = false;
				
				}
			else
				$bolDataSaved = false;
			$cmd = "";
		}
	}

	if(empty($cmd))
		$cmd = "ADD";
	
		
	/* Fetching the list of secret questions to display at the bottom */	
	$arrAllCompanyData = selectMultiRecords("Select * from accounts_chart order by id");

		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Chart of Account</title>
<script language="javascript" src="javascript/jquery.js"></script>
<script language="javascript" src="javascript/jquery.validate.js"></script>
<script language="javascript" src="javascript/jquery.cluetip.js"></script>
<script>
$(document).ready(function() {
	$("#companyForm").validate({
		rules: {
			accountName: "required", 
			accountNumber: "required" 
			
		},
		messages: {
			accountName:"<br />Please provide account name.",
			accountNumber:"<br />Please provide account number."
			},
		});
		
  $('img').cluetip({splitTitle:'|'});
});
	

function disableFields(strFieldName, chrVal) 
{ 
	var rad_val = '';	
	
	if(chrVal == "Y")
		document.getElementById(strFieldName).disabled = false;
	else
		document.getElementById(strFieldName).disabled = true;
	
/*	for (var i=0; i < companyForm.companyValue.length; i++)
   {
   if (companyForm.companyValue[i].checked)
      {
      	rad_val = companyForm.companyValue[i].value;
      }
	 */
   }
</script>
<link rel="stylesheet" type="text/css" href="css/jquery.cluetip.css" />
<style>
td{ 
	font-family:Arial, Helvetica, sans-serif;
	size:8px;
}
.tdDefination
{
	font-size:12px;
	text-align:right;
}
.error {
	color: red;
	font: 8pt verdana;
	font-weight:bold;
}
.feeList td {
	text-align:center;
	font-size:10px;
	font-family:Arial, Helvetica, sans-serif;
}

.feeList th {
	text-align:center;
	font-size:14px;
	font-family: Arial, Helvetica, sans-serif;
	color:#ffffff;
	background-color:#000000;
	/*background-color:#000066;*/
}


.firstRow td {
	background-color:#999999;
	font-size:12px;
	color: #000000;
}
.secondRow td {
	background-color: #cdcdcd;
	font-size:12px;
	color: #000000;
}
.style1 {color: #FF0000}
</style>
</head>
<body>
<form name="companyForm" id="companyForm" action="<?=$_SERVER["PHP_SELF"]?>" method="post">
	<table width="550" border="0" cellspacing="1" cellpadding="2" align="center" id="mainTble">
		<tr>
			<td colspan="4" align="center" bgcolor="#DFE6EA">
				<strong><?=$strMainCaption?></strong>			</td>
		</tr>
		<? if($bolDataSaved) { ?>
			<tr bgcolor="#000000">
				<td colspan="4" align="center" style="text-align:center; color:#EEEEEE; font-size:13px; font-weight:bold">
					<img src="images/info.gif" title="Information|Command Executed Successfully." />&nbsp;Command Executed Successfully!				</td>
			</tr>
		<? } ?>
		
		<tr bgcolor="#ededed">
			<td colspan="4" align="center" class="tdDefination" style="text-align:center">
			 All fields mark with <font color="red">*</font> are compulsory Fields.</td>
		</tr>
		<tr bgcolor="#ededed">
		  <td colspan="4" valign="top" class="tdDefination">
		   <table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" id="addressTble">
		     <tr bgcolor="#ededed" id="bankNameRows">
               <td class="tdDefination"><b>Account Name:&nbsp;<font color="red">*</font></b></td>
		       <td align="left" colspan="3"><input type="text" name="accountName" id="accountName" size="50" maxlength="150" value="<?=$accountName?>" /></td>
	         </tr>
		   <tr bgcolor="#ededed">
		     <td valign="top" class="tdDefination"><b>Account Number:&nbsp;<font color="red">*</font></b> </td>
		     <td align="left"><input type="text" name="accountNumber" id="accountNumber" size="50" maxlength="150" value="<?=$accountNumber?>" /></td>
		     </tr>
		   <tr bgcolor="#ededed">
		   	<td valign="top" class="tdDefination"><b>Description</b></td>
		    <td align="left"><textarea name="description" cols="40" rows="5" id="description"><?=$description?></textarea></td>
		   </tr>
	 
		<tr bgcolor="#ededed">
			<td colspan="4" align="center">
				<input type="submit" name="storeData" value="Submit" />
				&nbsp;&nbsp;
				<input type="reset" value="Clear Fields" />
				<input type="hidden" name="cID" value="<?=$cID?>" />
				<input type="hidden" name="cmd" value="<?=$cmd?>" />
				<? if(!empty($cID)) { ?>
					&nbsp;&nbsp;<a href="<?=$_SERVER['PHP_SELF']?>">Add New One</a>
				<? } ?>			</td>
		</tr>
  </table>
</form>
<br /><br />
<fieldset>
	<legend style="color:#666666; font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:18px; font-weight:bold">
		&nbsp;Chart of Account Detail &nbsp;
	</legend>
	<table width="100%" align="center" border="0" class="feeList" cellpadding="2" cellspacing="2">
		<tr>
			<th width="50%">Account Name </th>
			<th width="20%">Account Number </th>
			<th width="60%">Description </th>
			<th width="20%">Actions</th>
		</tr>
		<?php
			foreach($arrAllCompanyData as $companyVal)
			{
				
					
				if($strClass == "firstRow")
					$strClass = "secondRow";
				else
					$strClass = "firstRow";
		?>
		<tr class="<?=$strClass?>">
			<td><?=stripcslashes($companyVal["accountName"])?></td>
			<td><?=stripcslashes($companyVal["accountNumber"])?></td>
			<td><?=stripcslashes($companyVal["description"])?></td>
		<td valign="top">
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$companyVal["id"]?>&cmd=EDIT">
					<img src="images/edit_th.gif" border="0" title="Edit Chart of Account| You can edit the record by clicking on this thumbnail." />				</a>&nbsp;&nbsp;
				<a href="<?=$_SERVER['PHP_SELF']?>?cID=<?=$companyVal["id"]?>&cmd=DEL">
					<img src="images/remove_tn.gif" border="0" title="Remove Chart of Account|By clicking on this the record will be no longer available." />				</a>		  </td>
		</tr>
				
		<?
			}
		?>
</table>
</fieldset>

</body>
</html>