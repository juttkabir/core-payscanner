<?

/**
 * @package compliance
 * @subpackage Manage User Profiling
 * Short description.
 * This File is used to add the compliance rule of user profiling
 */
session_start();
include ("../include/config.php");
$date_time = date('Y-m-d h:i:s');
include ("security.php");
$agentType 		= getAgentType();
$loggedUserID 	= $_SESSION["loggedUserData"]["userID"];
extract(getHttpVars());
//Orignating and destination currency
$currencyQuery="SELECT DISTINCT currencyOrigin FROM services";
$distinctCurrency=selectMultiRecords($currencyQuery);
//$arrOrignatingCurrency=arrCurrency($distinctCurrency,'currencyOrigin');
//debug($arrOrignatingCurrency);
$currencyQuery="SELECT DISTINCT currencyRec FROM services";
$distinctCurrency=selectMultiRecords($currencyQuery);
//$arrDestinationCurrency=arrCurrency($distinctCurrency,'currencyRec');
//debug($arrDestinationCurrency);
$customerType	= $arrRecord["customerType"];
$monitoringCriteria	= $arrRecord["monitoringCriteria"];
$orignatingCurrency		= $arrRecord["orignatingCurrency"];
$destinationCurrency	  	= $arrRecord["destinationCurrency"];
$amountCriteria	= $arrRecord["amountCriteria"];
$period	= $arrRecord["period"];
$applyOn	= $arrRecord["applyOn"];
$points	= $arrRecord["points"];
$riskType	= $arrRecord["riskType"];
$status	  	= $arrRecord["status"];
if($_REQUEST['id']=="" && $_REQUEST["submit"] == "Submit")
{
	$customerType	= $_REQUEST["customerType"];
	$monitoringCriteria	= $_REQUEST["monitoringCriteria"];
	$orignatingCurrency		= $_REQUEST["orignatingCurrency"];
	$destinationCurrency	  	= $_REQUEST["destinationCurrency"];
	$amountCriteria	= $_REQUEST["amountCriteria"];
	$period	= $_REQUEST["period"];
	$applyOn	= $_REQUEST["applyOn"];
	$points	= $_REQUEST["points"];
	$riskType	= $_REQUEST["riskType"];
	$status	  	= $_REQUEST["status"];
	$value		= $_REQUEST["value"];
	$updationDate = date('d-m-Y');
	$creationDate = date('d-m-Y');
	$strInsertQuery = "	insert into 
							compliance_user_profile 
							(customerType,monitoringCriteria,orignatingCurrency, 	
							destinationCurrency,amountCriteria,period,applyOn,points
							,riskType,status,`value`,updationDate,creationDate)
						values
							('$customerType','$monitoringCriteria','$orignatingCurrency',
							'$destinationCurrency','$amountCriteria','$period'
							,'$applyOn','$points','$riskType','$status', '$value','$updationDate','$creationDate')";
	if(mysql_query($strInsertQuery))
	{
		$alertID = $id;
		////To record in History
		$descript = "Compliance user profile rule added";
		//$backURL .= "&success=Y&act=update";
		activities($_SESSION["loginHistoryID"],"INSERTION",$alertID,"compliance_user_profile",$descript);

		$message = "Your Rule Data Added Successfully.";
		$output['status']  = true;
		$_SESSION['message'] = $message;
	}
}
if($_REQUEST['id']!="" && $_REQUEST["oper"] == "edit")
{
	$id		= $_REQUEST["id"];
	$strSelectQuery = "SELECT 
								* 
							FROM 
								".compliance_user_profile." 
							WHERE 
								id='".$id."'";

	$arrRecord	= selectFrom($strSelectQuery);

	$customerType	= $arrRecord["customerType"];
	$monitoringCriteria	= $arrRecord["monitoringCriteria"];
	$orignatingCurrency		= $arrRecord["orignatingCurrency"];
	$destinationCurrency	  	= $arrRecord["destinationCurrency"];
	$amountCriteria	= $arrRecord["amountCriteria"];
	$period	= $arrRecord["period"];
	$applyOn	= $arrRecord["applyOn"];
	$points	= $arrRecord["points"];
	$riskType	= $arrRecord["riskType"];
	$status	  	= $arrRecord["status"];
	if($_REQUEST["submit"] == "Submit")
	{

		$id			= $_REQUEST["id"];
		$customerType	= $_REQUEST["customerType"];
		$monitoringCriteria	= $_REQUEST["monitoringCriteria"];
		$orignatingCurrency		= $_REQUEST["orignatingCurrency"];
		$destinationCurrency	  	= $_REQUEST["destinationCurrency"];
		$amountCriteria	= $_REQUEST["amountCriteria"];
		$period	= $_REQUEST["period"];
		$applyOn	= $_REQUEST["applyOn"];
		$points	= $_REQUEST["points"];
		$riskType	= $_REQUEST["riskType"];
		$status	  	= $_REQUEST["status"];
		$strDupQuery = "	SELECT 
								id 
							FROM 
								".compliance_user_profile." 
							WHERE 
								customerType	='".$customerType."' 	AND 
								monitoringCriteria	='".$monitoringCriteria."' 	AND 
								riskType	='".$riskType."'  AND  status	='".$status."'
							";

		$arrID	= selectFrom($strDupQuery);
		if(!empty($arrID["id"]) && $id != $arrID["id"])
		{
			$message = "! Alert Already Exist.";
			$output['status']  = false;
			$_SESSION['message'] = $message;

		}
		else
		{
			$strUpdateQuery	= " UPDATE 
										compliance_user_profile
									SET
										customerType	='".$customerType."',
										monitoringCriteria		= '".$monitoringCriteria."',
										orignatingCurrency 	='".$orignatingCurrency."',
										destinationCurrency 	='".$destinationCurrency."',
										amountCriteria 		='".$amountCriteria."',
										period 	='".$period."',
										applyOn 	='".$applyOn."',
										points 	='".$points."',
										riskType 	='".$riskType."',
										status	='".$status."',
										value ='".$value."'
									WHERE
										id = '".$id."'
									";
			if(mysql_query($strUpdateQuery))
			{

				$alertID = $id;
				////To record in History
				$descript = "Compliance user profile update";
				//$backURL .= "&success=Y&act=update";
				activities($_SESSION["loginHistoryID"],"INSERTION",$alertID,"compliance_user_profile",$descript);

				$message = "Your Rule Data Updated Successfully.";
				$output['status']  = true;
				$_SESSION['message'] = $message;

			}
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head>
	<title>Add User Profiling</title>
	<link href="images/interface.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="./javascript/functions.js"></script>
	<script type="text/javascript" src="./javascript/jquery.js"></script>
	<script type="text/javascript" src="./javascript/jquery.validate.js"></script>
	<script language="javascript">
		$(document).ready(function(){
			$("#compProfile").validate({
				rules: {
					customerType: "required",
					monitoringCriteria: "required",
					amountCriteria: "required",
					period: "required",
					points: {
						required: true,
						number: true,
						min: "1"
					},
					riskType: "required"
				},
				messages: {
					customerType: "<br>Please select Customer Type.",
					monitoringCriteria: "<br>Please select Monitoring Criteria.",
					amountCriteria: "<br>Please select Amount Criteria.",
					period: "<br>Please select Period.",
					points: {
						required: "<br>Please enter Points.",
						number: "<br>Please enter valid number.",
						min: "<br>Please enter a value greater than or equal to 1."
					},
					riskType: "<br>Please provide risk type."
				}
			});

			if($("#period").val()=='Days')
			{
				$("#compProfile").validate({
					rules: {

						applyOn: {
							required: true,
							number: true,
							min: "1"
						}
					},
					messages: {
						applyOn: {
							required: "<br>Please enter Apply On.",
							number: "<br>Please enter valid number.",
							min: "<br>Please enter a value greater than or equal to 1."
						}
					}
				});
			}

		});

		function SelectOption(OptionListName, ListVal)
		{
			for (i=0; i < OptionListName.length; i++)
			{
				if (OptionListName.options[i].value == ListVal)
				{
					OptionListName.selectedIndex = i;
					break;
				}
			}
		}
		function check_amount(selectedValue){
			if(selectedValue=='Amount')
				$('#orignatingCurrencyRow').show();
			else
				$('#orignatingCurrencyRow').hide();
			if(selectedValue=='No. of Transactions')
			{
				$('#orignatingCurrencyRow').show();
				$('#destinationCurrencyRow').show();
			}
			else{
				if(selectedValue!='Amount')
					$('#orignatingCurrencyRow').hide();
				$('#destinationCurrencyRow').hide();
			}

		}
		function check_period(selectedValue)
		{
			if(selectedValue=='Days')
				$('#applyOnRow').show();
			else
				$('#applyOnRow').hide();
		}
		function Clear_all(){
			$("#customerType").val("");
			$("#monitoringCriteria").val("");
			$("#orignatingCurrency").val("");
			$("#destinationCurrency").val("");
			$("#amountCriteria").val("");
			$("#applyOn").val("");
			$("#period").val("");
			$("#points").val("");
			$("#riskType").val("");
			$("#status").val("Disable");
		}
	</script>
	<style type="text/css">
		label.error {
			color: red;
			padding-left: 5px;
			font-weight:bold;
		}
	</style>
</head>
<body >
<table width="100%" border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td align="center">
			<table width="600" border="0" cellspacing="1" cellpadding="2" align="center">
				<tr>
					<td colspan="3" bgcolor="#000000">
						<table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
							<tr>
								<td align="center" bgcolor="#DFE6EA"> <font color="#000066" size="2"><strong>Add User Profiling</strong></font></td>
							</tr>
						</table>
					</td>
				</tr>
				<? if (isset($_SESSION['message'])){ ?>
					<tr bgcolor="#EEEEEE">
						<td colspan="2" bgcolor="#EEEEEE">
							<table width="100%" cellpadding="5" cellspacing="0" border="0">
								<tr>
									<td width="40" align="center"><font size="5" color="<? echo ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR); ?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font></td>
									<td><? echo "<font color='" . ($_GET["success"] != "" ? SUCCESS_COLOR : CAUTION_COLOR) . "'><b>".$_SESSION['message']."</b><br><br></font>"; ?></td>
								</tr>
							</table>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<?
					unset($_SESSION['message']);
				} ?>
				<form action="" method="post" name="compProfile" id="compProfile">
					<tr bgcolor="#ededed" id="customerTypeRow">
						<td><font color="#005b90"><strong>Customer Type</strong></font><font color="#ff0000">*</font></td>
						<td>
							<select name="customerType" id="customerType" >
								<option value="">Select</option>
								<?php
								foreach ($arrCustomerType as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].customerType, "<?=$customerType ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="monitoringCriteriaRow">
						<td><font color="#005b90"><strong>Monitoring Criteria</strong></font><font color="#ff0000">*</font></td>
						<td><select name="monitoringCriteria" id="monitoringCriteria" onChange="check_amount(this.value);" >
								<option value="">Select</option>
								<?php
								foreach ($arrMonitoringCriteria as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].monitoringCriteria, "<?=$monitoringCriteria ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<?php
						/*<tr bgcolor="#ededed" id="orignatingCurrencyRow"
						<?php
						if($monitoringCriteria != 'No. of Transactions' && $monitoringCriteria != 'Amount'){ ?>
							style="display:none"
						<?php } ?>
					>
						<td><font color="#005b90"><strong>Originating Currency</strong><font color="#ff0000">*</font></font></td>
						<td><select name="orignatingCurrency" id="orignatingCurrency" >

								<?php
								foreach ($arrOrignatingCurrency as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].orignatingCurrency, "<?=$orignatingCurrency ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="destinationCurrencyRow"
						<?php
						if($monitoringCriteria != 'No. of Transactions'|| !isset($monitoringCriteria)){ ?>
							style="display:none"
						<?php } ?>
					>
						<td><font color="#005b90"><strong>Destination Currency</strong></font><font color="#ff0000">*</font></td>
						<td><select name="destinationCurrency" id="destinationCurrency" >

								<?php
								foreach ($arrDestinationCurrency as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].destinationCurrency, "<?=$destinationCurrency ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>*/
					?>
					<tr bgcolor="#ededed" id="amountCriteriaRow">
						<td><font color="#005b90"><strong>Amount Criteria</strong></font><font color="#ff0000">*</font></td>
						<td><select name="amountCriteria" id="amountCriteria" >
								<option value="">Select</option>
								<?php
								foreach ($arrAmountCriteria as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].amountCriteria, "<?=$amountCriteria ?>");</script>
						</td>
						<td bgcolor="#ededed">
							<input type="number" placeholder="Amount Value" name="value">
						</td>
					</tr>
					<tr bgcolor="#ededed" id="periodRow">
						<td><font color="#005b90"><strong>Period</strong></font><font color="#ff0000">*</font></td>
						<td><select name="period" id="period" onChange="check_period(this.value);" >
								<option value="">Select</option>
								<?php
								foreach ($arrPeriod as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].period, "<?=$period ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="applyOnRow" <?php if($period!='Days'|| !isset($period)){ ?>style="display:none" <?php } ?>>
						<td><font color="#005b90"><strong>Apply On</strong></font><font color="#ff0000">*</font></td>
						<td>
							<input name="applyOn" id="applyOn" value="<?=stripslashes($applyOn); ?>">
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="pointsRow">
						<td><font color="#005b90"><strong>Points</strong></font><font color="#ff0000">*</font></td>
						<td>
							<input type="number" name="points" id="points" value="<?=stripslashes($points); ?>">
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="riskTypeRow">
						<td><font color="#005b90"><strong>Risk Type</strong></font><font color="#ff0000">*</font></td>
						<td><select name="riskType" id="riskType" >
								<option value="">Select</option>
								<?php
								foreach ($arrRiskType as $key => $value){
									?>
									<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
								<?php } ?>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].riskType, "<?=$riskType ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed" id="statusRow">
						<td><font color="#005b90"><strong>Status</strong></font><font color="#ff0000">*</font></td>
						<td><select name="status" id="status" >
								<option value="Enable">Enable</option>
								<option value="Disable">Disable</option>
							</select>
							<script language="JavaScript">SelectOption(document.forms[0].status, "<?=$status ?>");</script>
						</td>
						<td bgcolor="#ededed"></td>
					</tr>
					<tr bgcolor="#ededed">
						<td colspan="3" align="center">
							<input type="submit" name="submit" value="Submit">
							<input type="button" name="Clear" value="Clear All" onClick="Clear_all();">
						</td>
					</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</body>
</html>