<?
session_start();
include ("../include/config.php");
$date_time = date('d-m-Y  h:i:s A');
include ("security.php");

// define page navigation parameters

if($_GET["msg"]!= ""){
	$msg = stripslashes ($_GET["msg"]);
}else{
	$msg = "";
}
if($_GET["stat"] != ""){
	$stat = stripslashes ($_GET["stat"]);
}else{
	$stat = "";
}
if($_GET["AdvertID"] != ""){
	$id = stripslashes ($_GET["AdvertID"]);
}else{
	$id = "";
}

if($stat != ""){
	$updateStat = "update ".TBL_CAMPAIGN_TYPE." set isEnable = '$stat' where id = '".$id."' ";
	update($updateStat);
	
	$update_query = "update ".TBL_CAMPAIGN_TYPE." set isEnable='N' where id != '".$id."'";
	update($update_query);
}

if ($offset == "")
	$offset = 0;
$limit=50;

if ($_GET["newOffset"] != "") {
	$offset = $_GET["newOffset"];
}
$nxt = $offset + $limit;
$prv = $offset - $limit;
$sortBy = $_GET["sortBy"];
$SQL_Qry = "select * from ".TBL_CAMPAIGN_TYPE;
$advert = SelectMultiRecords($SQL_Qry);
$allCount = count($advert);
if ($sortBy !=""){
	$SQL_Qry = $SQL_Qry. " order by $sortBy ASC ";
$SQL_Qry = $SQL_Qry. " LIMIT $offset , $limit";
//echo $SQL_Qry;
$advert = SelectMultiRecords($SQL_Qry);
}
	
?>
<html>
<head>
	<title>Manage Advertisement Footer</title>
<script language="javascript" src="../javascript/functions.js"></script>
<link href="images/interface.css" rel="stylesheet" type="text/css">
<link href="include/Main.css" rel="stylesheet" type="text/css">

	<style>
		.bak2top {
				font-family: Arial;
				font-size: 10px;
				font-weight: bold;
				color: #0000FF;
				background-color: #CCCCFF;
				border: 1px solid;
				border-color: #0000FF;
				padding: 2px;
			}
	</style>
</head>
<body>
<table width="100%" border="0" cellspacing="1" cellpadding="5">
 <tr>
    <td class="topbar">Manage Advertisement Footer</td>
 </tr>
  <form method="post">
  <?
		if($msg != ""){?>
			<tr>
		  	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">
					<tr>
						<td align="center"><font size="4" color="<? echo SUCCESS_COLOR ?>"><b><i><? echo SUCCESS_MARK ?></i></b></font>&nbsp;<font color="<? echo SUCCESS_COLOR?>"><b>Your advertisement information <? echo ($stat == 'N' ? "disabled" :  "enabled")?> successfully </b><br></font></td>
					</tr>
				</table></td>
		   </tr>
<? 	}?>
    <?
    if ($_GET["msg1"] != ""){ ?>
		 <tr>
		  	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEEEEE">         
					<tr>
						<td align="center"><font size="5" color="<? echo SUCCESS_COLOR?>"><b><i><? echo ($_GET["success"]!="" ? SUCCESS_MARK : CAUTION_MARK);?></i></b></font>&nbsp;<? echo "<font color='".SUCCESS_COLOR."'><b>Your advertisement information updated successfully. </b><br></font>"; ?></td>
					</tr>
				</table></td>
		 </tr>	
<? 	} ?>
<tr>
    <td align="center">
		<table width="527" border="1" cellspacing="1" cellpadding="1" align="center">
          <?
		if ($allCount > 0){
		?>
          <tr> 
            <td colspan="4" bgcolor="#000000"> <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($advert) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($advert));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          <tr bgcolor="#DFE6EA"> 
            <td width="193"><a href="<?php print $PHP_SELF . "?sortBy=heading";?>"><font color="#005b90"><strong>Heading</strong></font></a></td>
            <td width="287"><a href="<?php print $PHP_SELF . "?sortBy=message";?>"><font color="#005b90"><b>Message</b></font></a></td>
						<td><a href="<?php print $PHP_SELF . "?sortBy=isEnable";?>"><font color="#005b90"><b>Status</b></font></a></td> 	
							 	           
          </tr>
          <?
			for ($i=0; $i<count($advert); $i++){
		?>
          <tr valign="top" bgcolor="#eeeeee"> 
            <td><a href="advert_campaign.php?page=advert_campaign_list.php&AdvertID=<?=$advert[$i]["id"]?>"><font color="#005b90"><? echo stripslashes($advert[$i]["heading"]); ?></font></a></td>
            <td> 
              <?=stripslashes($advert[$i]["message"]); ?>
            </td>
            <td>
						<? 
						if($advert[$i]["isEnable"] == 'N')
						{?>
							<a href="advert_campaign_list.php?msg=Y&stat=Y&AdvertID=<?=$advert[$i]["id"]?>"><font color="#005b90"><b>Enable</b></font></a>
				 <? }else{ ?>
				 			<a href="advert_campaign_list.php?msg=Y&stat=N&AdvertID=<?=$advert[$i]["id"]?>"><font color="#005b90"><b>Disable</b></font></a>
				 <? }?>
							 	
					</td>		
          </tr>
          <?
			}
		?>
          <tr> 
            <td colspan="4" bgcolor="#000000"> <table width="600" cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tr> 
                  <td> 
                    <?php if (count($advert) > 0) {;?>
                    Showing <b><?php print ($offset+1) . ' - ' . ($offset+count($advert));?></b> 
                    of 
                    <?=$allCount; ?>
                    <?php } ;?>
                  </td>
                  <?php if ($prv >= 0) { ?>
                  <td width="50"> <a href="<?php print $PHP_SELF . "?newOffset=0&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">First</font></a> 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$prv&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Previous</font></a>	
                  </td>
                  <?php } ?>
                  <?php 
					if ( ($nxt > 0) && ($nxt < $allCount) ) {
						$alloffset = (ceil($allCount / $limit) - 1) * $limit;
				?>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$nxt&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Next</font></a>&nbsp; 
                  </td>
                  <td width="50" align="right"> <a href="<?php print $PHP_SELF . "?newOffset=$alloffset&sortBy=".$_GET["sortBy"];?>"><font color="#005b90">Last</font></a>&nbsp; 
                  </td>
                  <?php } ?>
                </tr>
              </table></td>
          </tr>
          
          <?
			} else {
		?>
          <tr> 
            <td colspan="4" align="center"> No advertisement campaign found in the database. </td>
          </tr>
          <?
			}
		?>
        </table>
	</td>
  </tr>
</form>
</table>
	<script language="JavaScript" src="./javascript/bak2top.js"></script>
</body>
</html>
